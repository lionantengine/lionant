// New Types
//--------------------------------------------------------------------------------------------------
<h>.BOOL <s>.BUTTON <cccc>.COLOR <ddd>.DATE <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <s>.STRING <fff>.V3 

[ Properties : ? ]
{ Type:<?>  Name:s                                         Value:<Type> }
//--------  ---------------------------------------------  ------------  
  .INT       "Key\EntryList\Count"                         6
  .STRING    "Key\EntryList\Entry[0]\Global\FileName"      "GameData/General/Pieces/Piece001.png"
  .STRING    "Key\EntryList\Entry[1]\Global\FileName"      "GameData/General/Pieces/Piece002.png"
  .STRING    "Key\EntryList\Entry[2]\Global\FileName"      "GameData/General/Pieces/Piece003.png"
  .STRING    "Key\EntryList\Entry[3]\Global\FileName"      "GameData/General/Pieces/Piece004.png"
  .STRING    "Key\EntryList\Entry[4]\Global\FileName"      "GameData/General/Pieces/Piece005.png"
  .STRING    "Key\EntryList\Entry[5]\Global\FileName"      "GameData/General/Pieces/Piece006.png"
  .FLOAT     "Default\Entry\Global\ScaleX"                 0.1440625
  .FLOAT     "Default\Entry\Global\ScaleY"                 0.1440625
  .BOOL      "Default\Entry\Global\bDisableAlpha"          1
  .ENUM      "Default\Entry\Global\HotpointXMode"          MIDDLE 
  .ENUM      "Default\Entry\Global\HotpointYMode"          MIDDLE 
  .FLOAT     "Key\Main\Global\AtlasCompression"            0

