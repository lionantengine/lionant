//
//  main.cpp
//  TestGraphics
//
//  Created by Tomas Arce on 8/27/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

enum consts:s32
{
    SCREEN_WIDTH        = 1024,
    SCREEN_HEIGHT       = 768,
    GAUSSIAN_TEXSIZE    = 512,
    INDEX_BUFFER_COUNT  = 300*6*2,
    VERTEX_BUFFER_COUNT = 300*4*2
    
};

static eng_render_buffer    s_RenderToBuffer;
static eng_texture          s_TextureFromBuffer;
static eng_fshader          s_FragmentShader01;
static eng_fshader          s_FragmentShader02;
static eng_vshader          s_VertexShader;
static eng_shader_program   s_ShaderProgram01;
static eng_shader_program   s_ShaderProgram02;
static eng_vertex_desc      s_VertexDesc;
static eng_ibuffer          s_IndexBuffer;
static eng_vbuffer          s_VertexBuffer;
static xtimer               s_GameTime;
static xsafe_array<eng_sprite,6> s_SpriteList;



//static eng_texture          s_TextureEnvMap;
//static eng_texture          s_TextureBlob;

static eng_texture_rsc::ref   s_TextureBackground;
static eng_texture_rsc::ref   s_TextureEnvMap;
static eng_texture_rsc::ref   s_TextureBlob;
static eng_sprite_rsc::ref    s_SpriteAtlas;



//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_VSBlobSpriteShader[] = R"(

#ifdef GL_ES
precision mediump float;
//precision highp float;
#endif

attribute vec3      aPosition;
attribute vec2      aTexCoord;
attribute vec4      aColor;

varying vec4        Color;
varying vec2        tCurr;

uniform mat4        L2C;

void main()
{
    // Pass along the vertex color to the pixel shader
    Color       = aColor;
    tCurr       = aTexCoord;
    
    // this may be equivalent (gl_Position = ftransform();)
    gl_Position = L2C * vec4( aPosition, 1. );
}

)";

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_FSBlobHeightAccumulation[] = R"(

#ifdef GL_ES
//precision mediump float;
precision highp float;
#endif

varying vec4        Color;
varying vec2        tCurr;

uniform sampler2D   Texture;

//-------------------------------------------------------------------------------

void main( void )
{
    vec4 Height = texture2D( Texture, tCurr.xy );
    
    // Decompress blob texture
    float Decompress = dot(Height.rg, vec2(0.9961089, 0.0039063 ) );
    
    gl_FragColor = vec4( Color.xyz * Decompress * Height.ggg, Decompress );
}

)";

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_FSBlobLighting[] = R"(

#ifdef GL_ES
//precision mediump float; // precision highp float;
precision highp float;
#endif

uniform vec4        iResolution;     // viewport resolution (in pixels) and offset
uniform vec3        Light;     // viewport resolution (in pixels) and offset

varying vec4        Color;
varying vec2        tCurr;

uniform sampler2D   Texture;
uniform sampler2D   EnvTexture;


//-------------------------------------------------------------------------------

vec3 ComputeNormal01( vec2 texcoords, float center )
{
    vec2 uvStep = vec2( 5.*iResolution.x, 5.*iResolution.y );
    
    float tl = texture2D( Texture, texcoords + uvStep * vec2( 1., -1. ) ).a;
    float l  = texture2D( Texture, texcoords + uvStep * vec2( 1.,  0. ) ).a;
    float bl = texture2D( Texture, texcoords + uvStep * vec2( 1.,  1. ) ).a;
    
    float t  = texture2D( Texture, texcoords + uvStep * vec2( 0., -1. ) ).a;
    float b  = texture2D( Texture, texcoords + uvStep * vec2( 0.,  1. ) ).a;
    
    float tr = texture2D( Texture, texcoords + uvStep * vec2(-1., -1. ) ).a;
    float r  = texture2D( Texture, texcoords + uvStep * vec2(-1.,  0. ) ).a;
    float br = texture2D( Texture, texcoords + uvStep * vec2(-1.,  1. ) ).a;
    
    float dx = tr + 2.*r + br - tl - 2.*l - bl;
    float dy = bl + 2.*b + br - tl - 2.*t - tr;
    
    vec3 N = normalize( vec3(dx, dy, 0.8) );
    
    return N.xyz;
}

//-------------------------------------------------------------------------------

vec3 ComputeNormal_fast( vec2 texcoords, float center )
{
    float U      = texture2D ( Texture, texcoords + iResolution.xz * 5. ).a;	// U bump map sample
    float V      = texture2D ( Texture, texcoords + iResolution.zy * 5. ).a;	// V bump map sample
    float dHdU   = U - center;                                                 // create bump map U offset
    float dHdV   = V - center;                                                 // create bump map V offset
    vec3  normal = vec3( dHdU, dHdV, 0.20 );
    
    /*
     float U      = texture2D ( Texture, texcoords + iResolution.xz * 9. ).a;	// U bump map sample
     float V      = texture2D ( Texture, texcoords + iResolution.zy * 9. ).a;	// V bump map sample
     float dHdU   = U - center;                                                 // create bump map U offset
     float dHdV   = V - center;                                                 // create bump map V offset
     vec3  normal = vec3( dHdU, dHdV, 0.20 );
*/
     return normalize( normal );
}

//-------------------------------------------------------------------------------

void main( void )
{
    const float THRESHOLD    = 0.1;
    const float aaval        = THRESHOLD * 0.17;
    const float AmbientLight = 0.60;
    
    vec4 trash_color  = Color;
    
    // this is the same as tCurr but because it is pass by the vertex shader it does not
    // create a performance penalty
    // vec2 uv = gl_FragCoord.xy * iResolution.xy;
    // vec2 trash_tcurss = tCurr;
    
    // Compute the normal at this UV
    vec4 Pixel      = texture2D         ( Texture, tCurr.xy );
    
    
    float wclamped = clamp( (Pixel.w - THRESHOLD)/aaval, 0., 1. );
    if ( vec2(wclamped,wclamped) == iResolution.zz )
        discard;
    
    
    vec3 pixelColor = Pixel.rgb/Pixel.w;
    vec3 normal     = ComputeNormal_fast( tCurr, Pixel.a );
    vec3 EnvMap     = texture2D         ( EnvTexture, tCurr + (normal.xy * 0.5 + 0.5)*0.2 ).xxx;
    vec3 finalColor;
    
    finalColor.rgb   = vec3(clamp( dot( normal, Light *0.6), 0., 1. ));
    finalColor      += AmbientLight;
    finalColor      *= pixelColor;
    finalColor      += pow(EnvMap.xyz,vec3(2.))*AmbientLight;
    finalColor.rgb  *= clamp( (Pixel.w - THRESHOLD)/aaval, 0., 1. );
    
    gl_FragColor = vec4( finalColor, 1. );
}
)";

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void RenderBlob( draw_vertex* pVertex, f32 x, f32 y, f32 Size, xcolor Color )
{
    Size /= 2;
    f32 ySize = Size;// * 1.095;
    pVertex[0].setup( x - Size,   y - ySize, 0,   0, 1, Color );
    pVertex[1].setup( x + Size,   y - ySize, 0,   1, 1, Color );
    pVertex[2].setup( x + Size,   y + ySize, 0,   1, 0, Color );
    pVertex[3].setup( x - Size,   y + ySize, 0,   0, 0, Color );
}

//-------------------------------------------------------------------------------

struct ball
{
    xvector3 m_Pos;
    xvector3 m_Dir;
    f32      m_Size;
    xcolor   m_Color;
    s32      m_iColor;
};

xsafe_array<ball,9*9> s_Balls;

void Draw_TestLines( void )
{
    static f32 x=0;
    
    //
    // Make sure that all the resources that need to be loaded are in fact loaded
    //
    const eng_sprite_rsc*   pSpriteAtlas    = s_SpriteAtlas.getAsset();
    const eng_texture*      pEnv            = s_TextureEnvMap.getAsset();
    
    if( pSpriteAtlas == NULL || pEnv == NULL ) return;

    const eng_texture*      pBlob  = pSpriteAtlas->m_SpriteAtlas.getAsset(); //s_TextureBlob.getAsset();

    if( pBlob == NULL) return;


    //
    // Start with the rest
    //
    
    
    if( x == 0 )
    {
        s32 Index=0;
        for( ball& Ball : s_Balls )
        {
            static const xcolor PieceColors[] =
            {
                xcolor( 255, 255, 0, 200 ),         // 1 Yellow
                xcolor( 255, 0, 0, 200 ),           // 2 Red
                xcolor( 0, 255, 0, 200 ),           // 3 Green
                xcolor( 0, 0, 255, 200 ),           // 4 Blue
                xcolor( 255, 100, 0, 200 ),         // 5 Orange
                xcolor( 255, 0, 255, 200 )          // 6 Pink
            };

            
            if( 0 )
            {
                Ball.m_Size = x_frand() * 150 + 50;
                Ball.m_Pos.m_X = x_frand() * 1024;
                Ball.m_Pos.m_Y = x_frand() * 768;
                
                Ball.m_Dir.m_X = x_frand()-0.5;
                Ball.m_Dir.m_Y = x_frand()-0.5;
                
                Ball.m_Dir.Normalize();
                
                Ball.m_Color.SetFromRGBA( x_frand()*0.8 + 0.4, x_frand()*0.8 + 0.4, x_frand()*0.8 + 0.4, 255 );
                Ball.m_iColor = 0;
                
                Ball.m_iColor = x_rand()%6;
                Ball.m_Color  = ( PieceColors[Ball.m_iColor] );

            }
            else
            {
                Ball.m_Size = 35;
                
                Ball.m_Pos.m_X = 50 + (Index%9) * 73;
                Ball.m_Pos.m_Y = 35 + (Index/9) * 80;
                Ball.m_Dir.Zero();
                
                Ball.m_iColor = x_rand()%6;
                Ball.m_Color  = ( PieceColors[Ball.m_iColor] );
                
                Index++;
                
                if( Index == 35 )  Ball.m_Dir.Set( 0, 1, 0 );
                if( Index == 35+9 ) Ball.m_Dir.Set( 0, -1, 0 );
            }
        }
    }
    
    
    f32 DeltaTime = s_GameTime.TripMs();
    x += 0.00003f * DeltaTime;

    //
    // Get ready to render
    //
    const eng_view& View = eng_GetCurrentContext().GetActiveView();
    xmatrix4        L2C;
    
    //
    // Compute Matrix
    //
    s32 XRes = View.getViewport().GetWidth();
    s32 YRes = View.getViewport().GetHeight();
    
    L2C.Identity();
    
    L2C.setTranslation( xvector3( -1, 1, 0 ) );
    L2C.setScale      ( xvector3( 2.0f/XRes, -2.0f/YRes, 1 ) );
    
    //
    // Create some vertices
    //
    draw_vertex* pVertex = (draw_vertex*)s_VertexBuffer.LockData();
    s32          iVertex = 0;
    
    
    if( 0 )
    {
        for( ball& Ball : s_Balls )
        {
            Ball.m_Pos += Ball.m_Dir * DeltaTime * 0.008;
            
            const f32 MaxX = XRes - Ball.m_Size;
            const f32 MaxY = YRes - Ball.m_Size;
            
            if( Ball.m_Pos.m_X < 0 ||
                Ball.m_Pos.m_X  > MaxX )
            {
                if( Ball.m_Pos.m_X  > MaxX ) Ball.m_Pos.m_X = MaxX;
                else                         Ball.m_Pos.m_X = 0;
                Ball.m_Dir.m_X = - Ball.m_Dir.m_X;
            }
            
            if( Ball.m_Pos.m_Y < 0 ||
               Ball.m_Pos.m_Y  > MaxY )
            {
                if( Ball.m_Pos.m_Y  > MaxY ) Ball.m_Pos.m_Y = MaxY;
                else                         Ball.m_Pos.m_Y = 0;
                Ball.m_Dir.m_Y =  - Ball.m_Dir.m_Y;
            }
            
            RenderBlob( &pVertex[ iVertex ],  Ball.m_Pos.m_X,   Ball.m_Pos.m_Y,  Ball.m_Size*2,  Ball.m_Color); iVertex += 4;
        }
    }
    else if( 1 )
    {
        //
        // Render the actual sprites
        //
        for( ball& Ball : s_Balls )
        {
            Ball.m_Pos += Ball.m_Dir * DeltaTime * 0.008;
            
            const f32 MaxX = XRes - Ball.m_Size;
            const f32 MaxY = YRes - Ball.m_Size;
            
            if( Ball.m_Pos.m_X < 0 ||
               Ball.m_Pos.m_X  > MaxX )
            {
                if( Ball.m_Pos.m_X  > MaxX ) Ball.m_Pos.m_X = MaxX;
                else                         Ball.m_Pos.m_X = 0;
                Ball.m_Dir.m_X = - Ball.m_Dir.m_X;
            }
            
            if( Ball.m_Pos.m_Y < 0 ||
               Ball.m_Pos.m_Y  > MaxY )
            {
                if( Ball.m_Pos.m_Y  > MaxY ) Ball.m_Pos.m_Y = MaxY;
                else                         Ball.m_Pos.m_Y = 0;
                Ball.m_Dir.m_Y =  - Ball.m_Dir.m_Y;
            }
            
            
            eng_sprite& Sprite = s_SpriteList[ Ball.m_iColor ];
            
            Sprite.Resolve();
            
            const eng_sprite_rsc::info& SpriteInfo  = Sprite.getSpriteInfo( *pSpriteAtlas );

            for( s32 i=0; i<4; i++ )
            {
                static const s32 iMapping[] = {0,1,2,3,4};
                s32 Index = iMapping[i];
                
                pVertex[iVertex+i].setup(  xvector3d( SpriteInfo.m_Vertex[Index].m_Pos.m_X+Ball.m_Pos.m_X,
                                                      SpriteInfo.m_Vertex[Index].m_Pos.m_Y+Ball.m_Pos.m_Y,
                                                      0 ),
                                           SpriteInfo.m_Vertex[Index].m_UV.m_X,
                                           SpriteInfo.m_Vertex[Index].m_UV.m_Y,
                                           Ball.m_Color );
            }

            iVertex += 4;
        }
        
    }
    else
    {
        f32 t = x_Sin(x*8) * 250/2 + 50;
        RenderBlob( &pVertex[ iVertex ],  100 + t + 300/2,   400/2,       800/2,  xcolor( 255, 0, 0, 255)); iVertex += 4;
        RenderBlob( &pVertex[ iVertex ],  100 + t + 500/2,   t + 600/2,   600/2,  xcolor( 0, 255, 0, 255)); iVertex += 4;
        RenderBlob( &pVertex[ iVertex ],  100 + 600/2 - t,   t + 600/2,   600/2,  xcolor( 0, 0, 255, 255)); iVertex += 4;
        RenderBlob( &pVertex[ iVertex ],  100 + 700/2 - t,   300/2 - t,   1300/2,  xcolor( 0, 255, 255, 255)); iVertex += 4;
        RenderBlob( &pVertex[ iVertex ],  100 + 200/2 - t,   500/2 ,      400/2,  xcolor( 255, 0, 255, 255)); iVertex += 4;
        RenderBlob( &pVertex[ iVertex ],  100 + 500/2 +t,   100/2 - t,    300/2,  xcolor( 255, 255, 0, 255)); iVertex += 4;
    }
    
    
    //
    // Add a final rectangle for the hold screen used in the last pass
    //
    pVertex[iVertex++].setup( 0,       0,      0, 0, 0, xcolor(~0) );
    pVertex[iVertex++].setup( XRes,    0,      0, 1, 0, xcolor(~0) );
    pVertex[iVertex++].setup( XRes,    YRes,   0, 1, 1, xcolor(~0) );
    pVertex[iVertex++].setup( 0,       YRes,   0, 0, 1, xcolor(~0) );
    
    
    s_VertexBuffer.UnlockData();
    
    //
    // Must setup the right blending modes and such
    //
    
    // Which direction we are culling
    glCullFace(GL_BACK);
    
    // Additive blending
    glEnable( GL_BLEND );

    glBlendEquationSeparate( GL_FUNC_ADD,
                             GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE,
                        GL_ONE,
                        GL_ONE,
                        GL_ONE);

    // Z Buffer off
    glDepthMask( FALSE );
    glDisable( GL_DEPTH_TEST );

    
    // PASS1 - Render the blobs accummulation
    // Set the shaders and render away
    //
    s_ShaderProgram01.Activate();
    s_ShaderProgram01.setUniformVariable( 0, L2C );
    
    s_VertexBuffer.Activate();
    s_IndexBuffer.Activate();
    pBlob->Activate();
    
    s_IndexBuffer.RenderTriangles( ((iVertex/4)- 1) * 6, 0 );
    
    
    // PASS 2 - Render other stuff
    // Ok Lets render the second pass
    //
    const xvector4 Rect( 1.0/XRes, 1.0/YRes,
                         0, // Must be zero
                         0 );
    
    xvector3 Light( 1, 0, 0 );
    Light.RotateZ( x *10 );
    Light.Normalize();
    
    glDisable( GL_BLEND );
 
     // Deactive the frame buffer from before
    s_RenderToBuffer.Deactivate();

     // Set the new constants
    s_ShaderProgram02.Activate();
    s_ShaderProgram02.setUniformVariable( 0, L2C );
    s_ShaderProgram02.setUniformVariable( 1, Rect );
    s_ShaderProgram02.setUniformVariable( 2, Light );
    
     // Activate all the buffers
    s_VertexBuffer.Activate();
    s_IndexBuffer.Activate();
    
    s_TextureFromBuffer.Activate(0);
    pEnv->Activate(1);
    
    s32 iBaseIndex = ((iVertex/4)- 1) * 6;
    
    s_IndexBuffer.RenderTriangles( ((iVertex/4)- 1) * 6, iBaseIndex );

    
    //
    // Render Eyes
    //
    if( 0 )
    {
        eng_draw& Draw = eng_GetCurrentContext().getDraw();
        
        for( const ball& Ball : s_Balls )
        {
            eng_sprite& Sprite = s_SpriteList[Ball.m_iColor];
            Sprite.DrawSprite( Draw, xvector2( Ball.m_Pos.m_X, Ball.m_Pos.m_Y) );
        }
    }
    
    if( 0 )
    {
        eng_draw& Draw = eng_GetCurrentContext().getDraw();
        Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
        
        static f32 k=0;
        k +=0.003*DeltaTime;
        
        const f32 EyeSize = 5;
        
        xrandom_small Random;
        
        Random.setSeed32( 123 );
        
        for( ball& Ball : s_Balls )
        {
            f32 Offset = x_Sin( k + Random.RandF32()*100 )*3;
            Ball.m_Pos += Ball.m_Dir * DeltaTime * 0.008;
            
            Draw.DrawSolidRect( xrect( Ball.m_Pos.m_X-15 -3 + Offset,
                                          1024-Ball.m_Pos.m_Y-15,
                                          Ball.m_Pos.m_X-15 -3 +EyeSize + Offset,
                                          1024-Ball.m_Pos.m_Y-15+EyeSize), xcolor(0,0,0, 120));
            
            Draw.DrawSolidRect( xrect( Ball.m_Pos.m_X+15 -3 + Offset,
                                      1024-Ball.m_Pos.m_Y-15,
                                      Ball.m_Pos.m_X+15 -3 +EyeSize + Offset,
                                      1024-Ball.m_Pos.m_Y-15+EyeSize), xcolor(0,0,0, 120));
        }
        Draw.End();
    }
        

}

//-------------------------------------------------------------------------------

void Render( void )
{
    eng_view View;
    
    const eng_texture*      pBackGrn        = s_TextureBackground.getAsset();

    //
    // Render the background image
    //
    if( pBackGrn )
    {
        s32 W, H;
        eng_GetCurrentContext().GetScreenResolution( W, H );
        
        eng_draw& Draw = eng_GetCurrentContext().getDraw();
        Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_MISC_FLUSH );
        
        Draw.SetTexture( *pBackGrn );
        
        Draw.DrawTexturedRect( xrect( 0,0,W,H), xrect( 0,0,1,1), xcolor( ~0));
        
        Draw.End();
    }

    
    //
    // Render to texture
    //
    View.setViewport( xirect( 0,0,s_RenderToBuffer.getWidth(), s_RenderToBuffer.getHeight() ));
    
    s_RenderToBuffer.Activate( View, s_TextureFromBuffer );
    
    // Clear Screen And Depth Buffer
    glClearColor( 0, 0, 0, 0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    eng_CheckForError();
    
    Draw_TestLines();
    
    s_RenderToBuffer.Deactivate();
    
    
    
    
    return;
    
    
    
    //
    // Render from the texture to the screen
    //
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    if( 1 )
    {
        Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_TEXTURE_ON );
        
        Draw.SetTexture( s_TextureFromBuffer );//s_TextureBlob );//s_TextureFromBuffer );
        Draw.DrawTexturedRect( xrect( 0,0, 400, 400), xrect( 0,0,1,1), xcolor(~0));
        
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void Initialize( void )
{
    //
    // Initialize the engine
    //
    eng_Init();
    g_Scheduler.Init( 2 );
    
    eng_hwin MainWindow;
    
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    eng_SetCurrentContext(DisplayContext);
    
    //
    // Create temporary textures
    //
    s32 XRes, YRes;
    DisplayContext.GetScreenResolution( XRes, YRes);
    s_TextureFromBuffer.CreateTexture( XRes, YRes, xbitmap::FORMAT_R16G16B16A16_f );
    s_RenderToBuffer.CreateRenderBuffer( s_TextureFromBuffer );
    
    //
    // Create texture for the blob
    //
    {
        xbitmap Bitmap;
        
        if( 0 )
        {
            Bitmap.CreateBitmap( GAUSSIAN_TEXSIZE, GAUSSIAN_TEXSIZE );
            xcolor* pBitmap = (xcolor*)Bitmap.getMip( 0 );
            
            const f32 GAUSSIAN_HEIGHT       = 1;
            const f32 GAUSSIAN_DEVIATION    = 0.125;
            for( s32 v=0; v<GAUSSIAN_TEXSIZE; v++ )
            for( s32 u=0; u<GAUSSIAN_TEXSIZE; u++ )
            {
                const f32 dx = 2.0*u/GAUSSIAN_TEXSIZE-1.0;
                const f32 dy = 2.0*v/GAUSSIAN_TEXSIZE-1.0;
                const f32 I  = GAUSSIAN_HEIGHT * x_Exp(-(dx*dx+dy*dy)/GAUSSIAN_DEVIATION);
                
                ASSERT( I <= 1 );
                ASSERT( I >= 0 );
                
                const u16 I2 = u16(I * 0xffff);
                const u8  r  = u8(I2>>8);
                const u8  g  = u8(I2&0xff);
                
                pBitmap[ u + v*GAUSSIAN_TEXSIZE ].Set(r,g,0,0);
            }
            
            Bitmap.SaveTGA( X_STR("TempImage.tga"));
        }
        else
        {
            //xbitmap Bitmap2;
            Bitmap.Load( "Letter-T.xbmp" );
            
            
            //xcolor* pBitmap2 = (xcolor*)Bitmap2.getMip( 0 );
            // xcolor* pBitmap = (xcolor*)Bitmap.getMip( 0 );
            
            /*
            for( s32 v=0; v<GAUSSIAN_TEXSIZE; v++ )
            for( s32 u=0; u<GAUSSIAN_TEXSIZE; u++ )
            {
                pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_R = u8((pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_R/255.0) * pBitmap2[u + v*GAUSSIAN_TEXSIZE ].m_R);
                pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_G = u8((pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_G/255.0) * pBitmap2[u + v*GAUSSIAN_TEXSIZE ].m_G);
                pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_B = u8((pBitmap[ u + v*GAUSSIAN_TEXSIZE ].m_B/255.0) * pBitmap2[u + v*GAUSSIAN_TEXSIZE ].m_B);
                
               // pBitmap[ u + v*GAUSSIAN_TEXSIZE ] = pBitmap2[ u + v*GAUSSIAN_TEXSIZE ];
            }
             */
        }
        
#ifdef TARGET_OSX
     //   Bitmap.SaveTGA( X_STR("DebugComputedTExture.tga"));
#endif
        // Create the texture
        
        {
            enum my_resources:u64
            {
                OWL_SMOOTH = X_GUID( 37si_yt8o_bw9v )
            };

            s_TextureBlob.setup( OWL_SMOOTH );
        }
        
        //s_TextureBlob.CreateTexture( Bitmap );
    }
    
    //
    // Create the shaders
    //
    s_VertexDesc.DescriveVertex( "aPosition", eng_vertex_desc::DATA_DESC_F32X3, eng_vertex_desc::USE_DESC_POSITION,
                                sizeof(draw_vertex), sizeof(f32)*0 );
    
    s_VertexDesc.DescriveVertex( "aTexCoord", eng_vertex_desc::DATA_DESC_F32X2, eng_vertex_desc::USE_DESC_TEXCOORD,
                                 sizeof(draw_vertex), sizeof(f32)*3 );
    
    s_VertexDesc.DescriveVertex( "aColor",    eng_vertex_desc::DATA_DESC_U8x4,  eng_vertex_desc::USE_DESC_COLOR,
                                sizeof(draw_vertex), sizeof(f32)*5 );
    
    s_FragmentShader01.LoadFromMemory ( s_FSBlobHeightAccumulation, sizeof(s_FSBlobHeightAccumulation) );
    s_VertexShader.LoadFromMemory     ( s_VSBlobSpriteShader, sizeof(s_VSBlobSpriteShader) );
    s_FragmentShader02.LoadFromMemory ( s_FSBlobLighting, sizeof(s_FSBlobLighting) );
    
    s_ShaderProgram01.LinkShaders     ( s_VertexDesc, s_VertexShader, s_FragmentShader01 );
    s_ShaderProgram01.LinkRegisterToUniformVariable( 0, "L2C" );
    
    s_ShaderProgram02.LinkShaders     ( s_VertexDesc, s_VertexShader, s_FragmentShader02 );
    s_ShaderProgram02.LinkRegisterToUniformVariable( 0, "L2C" );
    s_ShaderProgram02.LinkRegisterToUniformVariable( 1, "iResolution" );
    s_ShaderProgram02.LinkRegisterToUniformVariable( 2, "Light" );
    
    s_ShaderProgram02.LinkTextureRegisterWithUniform( 0, "Texture" );
    s_ShaderProgram02.LinkTextureRegisterWithUniform( 1, "EnvTexture" );
    
    //
    // Create Vextex Buffers and Index Buffers for this things
    //
    
    // Create the index buffer... static because the basic index pattern never changes
    {
        xptr<u16> iBuffer;
        iBuffer.Alloc( INDEX_BUFFER_COUNT );
        
        for( s32 i=0; i<INDEX_BUFFER_COUNT; i++ )
        {
            static const u16 IndexPatern[] = {0,1,2, 0,2,3 };
            
            iBuffer[i] = IndexPatern[i%6] + (i/6)*4;
        }
        s_IndexBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, INDEX_BUFFER_COUNT, &iBuffer[0] );
    }
    
    // Create the vertex buffer
    s_VertexBuffer.CreateDynamicBuffer( s_VertexDesc, VERTEX_BUFFER_COUNT );
    
    //
    // Load the env map
    //
    {
        
        {
            enum my_resources:u64
            {
                ENVIROMENT_MAP = X_GUID( 37sc_okd3_de77 ),
                SPRITE_ATLAS   = X_GUID( 142p_a61w_ac6b ),
                BACKGROUND     = X_GUID( 2bdf_43qi_t9nn )
            };
            
            s_TextureEnvMap.setup( ENVIROMENT_MAP );
            s_SpriteAtlas.setup( SPRITE_ATLAS );
            s_TextureBackground.setup( BACKGROUND );
            
            for( s32 i=0; i<s_SpriteList.getCount(); i++ )
            {
                eng_sprite& Sprite = s_SpriteList[i];
                
                Sprite.setup( s_SpriteAtlas, (const char*)xfs("Piece%03d", i+1));
            }
            
        }

  //      xbitmap Bitmap;
//        Bitmap.Load( "EnvMapV2.xbmp" );
    //    s_TextureEnvMap.CreateTexture( Bitmap );
   //     s_TextureEnvMap.setWrappingMode( eng_texture::WRAP_MODE_TILE, eng_texture::WRAP_MODE_TILE );
    }
    
    //
    // Let the time start
    //
    s_GameTime.Start();
    
}

//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    Initialize();
    
    // Get the current context
    eng_context& DisplayContext = eng_GetCurrentContext();
    
    // Now we will sleep for a while then exit
    while( DisplayContext.HandleEvents() )
    {
        eng_SetCurrentContext(DisplayContext);
        
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();
        
        DisplayContext.SetClearColor( xcolor(~0) );
        DisplayContext.PageFlip(TRUE);
    }
}
