//
//  AppDelegate.h
//  TestGraphics-iOS
//
//  Created by Tomas Arce on 8/27/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
