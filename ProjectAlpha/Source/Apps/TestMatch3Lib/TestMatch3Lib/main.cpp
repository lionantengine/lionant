 //
//  main.cpp
//  TestMatch3Lib
//
//  Created by Tomas Arce on 8/12/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"
#include "match3_base.h"

enum consts:s32
{
    SCREEN_WIDTH   = 1024,
    SCREEN_HEIGHT  = 768
};


const s32 TileSize = 64;
const s32 XOffset  = 32;
const s32 YOffset  = 100;

static match3_board             s_M3Board;
static match3_ai                s_AI;
static s32                      s_AIMoveIndex=-1;
static u32                      s_AIMoveSeed=-1;
static xtimer                   s_ClockToAIMove;
static xtimer                   s_ClockGameTime;

static match3_board::matchlist  s_MatchList;
static match3_board::movelist   s_MoveList;

static u32                      s_Seed = 429;//418;    // 444

static s32                      s_iMap    = 1;
static const char*              s_pMaps[] =
{
    // Most basic test
    "$$$$$$$$$"
    "         "
    "         "
    "         "
    "         "
    "         "
    "         "
    "         "
    "         ",
    
    // Every feature test
    "$$$$$$$$$"
    " ab   cd "
    " ##   ## "
    " AB   CD "
    "###111###"
    "###222###"
    "###333###"
    "#########"
    "#########",

    // Test different position of spawn
    "###$####$"
    "###  ### "
    "###  ##  "
    "$###     "
    "  ##     "
    "#  #     "
    "#        "
    "##       "
    "#        ",
    
    //
    "$$$$$$$$$"
    "         "
    "###  ##  "
    "####     "
    "$###     "
    "# ##     "
    "#        "
    "##       "
    "#        ",
    
    // Teleporter test
    "         "
    "#########"
    "$A######$"
    "  B####H "
    "   C##I  "
    "    DJ   "
    "abcd  jih"
    "#########"
    "         ",
    
    // All different ways to see pieces coming down
    "$$$$$$$$$"
    "         "
    "ab     hi"
    "#### ####"
    "#### ####"
    "ABCD EFHI"
    "         "
    "         "
    "cd     ef",
    
    NULL
};

//--------------------------------------------------------------------------------

void RenderTheBoard( eng_draw& Draw )
{
    static const xcolor TeleporterColors[]=
    {
        xcolor( 255,0,0,64 ),
        xcolor( 0,255,0,64 ),
        xcolor( 0,0,255,64 ),
        xcolor( 255,255,0,64 ),
        xcolor( 0,255,255,64 ),
        xcolor( 255,0,255,64 ),
        xcolor( 128,32,34,64 ),
        xcolor( 44,128,75,64 ),
        xcolor( 10,33,128,64 ),
        xcolor( 0,0,0,64 ),
        xcolor( 0,0,0,64 ),
        xcolor( 0,0,0,64 ),
        xcolor( 0,0,0,64 ),
        xcolor( 0,0,0,64 )
    };
    
    static const xcolor DirtColors[]=
    {
        xcolor( 150, 150, 150, 128),
        xcolor( 190, 190, 190, 128),
        xcolor( 230, 230, 230, 128)
    };
    
    xsafe_array<xcolor,match3_board::MAX_BOARDSIZE*match3_board::MAX_BOARDSIZE> TeleporterColorsMapping;

    //
    // Assign Colors to all the teleporters
    //
    {
        s32 nTeleportersFound = 0;
        
        for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
        for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
        {
            const match3_board::tile& Tile = s_M3Board.getTile( X, Y );
            if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_TELEPORT_IN ) )
            {
                TeleporterColorsMapping[ Tile.m_iTeleportOut ] = TeleporterColors[nTeleportersFound++];
            }
        }
    }

    
    //
    // Render First the Titles
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
    
    // Render the tile lines
    for( s32 i=0; i<match3_board::MAX_BOARDSIZE+1; i++ )
    {
        Draw.DrawLine( draw_vertex( XOffset, YOffset + i*TileSize, 0, xcolor( 128,128,128,128) ),
                       draw_vertex( XOffset + match3_board::MAX_BOARDSIZE*TileSize, YOffset + i*TileSize, 0, xcolor( 128,128,128,128) ) );
        
        Draw.DrawLine( draw_vertex( XOffset + i*TileSize, YOffset, 0, xcolor( 128,128,128,128) ),
                       draw_vertex( XOffset + i*TileSize, YOffset + match3_board::MAX_BOARDSIZE*TileSize, 0, xcolor( 128,128,128,128) ) );
    }
    
    // Render each of the tiles
    for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
    for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
    {
        const match3_board::tile& Tile = s_M3Board.getTile( X, Y );
        
        // If we have this flag we wont render any thing
        if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_BOUNDARY ) )
            continue;
        
        f32 TileX = (f32)XOffset + X*TileSize;
        f32 TileY = (f32)YOffset + Y*TileSize;
        
        // Render a square for this tile
        Draw.DrawSolidRect( xrect( TileX,
                                   TileY,
                                   TileX + TileSize,
                                   TileY + TileSize ), xcolor(128,128,128,128) );
        
        // Is this a dirt tile?
        if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_DIRTY ) )
        {
            ASSERT(Tile.m_DirtCount < sizeof(DirtColors));
            
            Draw.DrawSolidRect( xrect( TileX + TileSize/25,
                                       TileY + TileSize/25,
                                       TileX + TileSize - TileSize/25,
                                       TileY + TileSize - TileSize/25 ), DirtColors[Tile.m_DirtCount-1] );
        }
        
        // Is this a spawn tile?
        if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_SPAWN ) )
        {
            Draw.DrawSolidRect( xrect( TileX + TileSize/2 - TileSize/4,
                                       TileY,
                                       TileX + TileSize/2 + TileSize/4,
                                       TileY + TileSize/4 ), xcolor(128,128,128,80) );
        }

        // Is this a teleporter tile?
        if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_TELEPORT_IN ) )
        {
            Draw.DrawSolidRect( xrect( TileX + TileSize/6,
                                       TileY + TileSize - TileSize/6,
                                       TileX + TileSize - TileSize/6,
                                       TileY + TileSize ), TeleporterColorsMapping[ Tile.m_iTeleportOut ] );
        }
        
        // Is this a teleporter tile?
        if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_TELEPORT_OUT ) )
        {
            Draw.DrawSolidRect( xrect( TileX + TileSize/6,
                                      TileY,
                                      TileX + TileSize - TileSize/6,
                                      TileY + TileSize/6 ), TeleporterColorsMapping[ s_M3Board.getTileIndexFromXY( X, Y ) ] );
        }

    }
    
    Draw.End();
}

//--------------------------------------------------------------------------------

void RenderPieces( eng_draw& Draw )
{
    static const xcolor PieceColors[] =
    {
        xcolor( 255, 255, 0, 200 ),
        xcolor( 255, 0, 0, 200 ),
        xcolor( 0, 255, 0, 200 ),
        xcolor( 0, 0, 255, 200 ),
        xcolor( 255, 100, 0, 200 ),
        xcolor( 255, 0, 255, 200 )
    };
    
    //
    // Render First the Titles
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
    
    // Render each of the tiles
    for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
    for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
    {
        const match3_board::tile& Tile = s_M3Board.getTile( X, Y );
        
        // Skip any empty tiles
        if( Tile.m_iPiece < 0 )
            continue;
        
        const match3_board::piece& Piece = s_M3Board.getPiece( Tile.m_iPiece );
        
        f32 TileX = (f32)XOffset + X*TileSize;
        f32 TileY = (f32)YOffset + Y*TileSize;
        
        ASSERT( Piece.m_Color < sizeof(PieceColors)/sizeof(xcolor) );
        
        Draw.DrawSolidRect( xrect( TileX + TileSize/8,
                                   TileY + TileSize/8,
                                   TileX + TileSize - TileSize/8,
                                   TileY + TileSize - TileSize/8 ), PieceColors[Piece.m_Color] );
        
    }
    
    Draw.End();
}

//--------------------------------------------------------------------------------

void RenderMatches( eng_draw& Draw )
{
    
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
    
    for( s32 i=0; i<s_MatchList.m_nMatches; i++ )
    {
        match3_board::match& Match = s_MatchList.m_Matches[i];
        
        xcolor Color( 255,255,255,255);
        
        if( x_FlagIsOn( Match.m_Type, match3_board::match::TYPE_MIX ) )
        {
            Color.Set(0,0,0,255);
        }
        
        if( x_FlagIsOn( Match.m_Type, match3_board::match::TYPE_H ) )
        {
            Draw.DrawLine( draw_vertex( XOffset + Match.m_X*TileSize + TileSize/2,
                                        YOffset + Match.m_Y*TileSize + TileSize/2,
                                        0, Color ),
                           draw_vertex( XOffset + (Match.m_X+Match.m_L-1)*TileSize + TileSize/2,
                                        YOffset + Match.m_Y*TileSize + TileSize/2,
                                        0, Color ) );
        }
        else
        {
            Draw.DrawLine( draw_vertex( XOffset + Match.m_X*TileSize + TileSize/2,
                                        YOffset + Match.m_Y*TileSize + TileSize/2,
                                        0, Color ),
                           draw_vertex( XOffset + Match.m_X*TileSize + TileSize/2,
                                        YOffset + (Match.m_Y+Match.m_L-1)*TileSize + TileSize/2,
                                        0, Color ) );
        }
    }
    
    Draw.End();
}

//--------------------------------------------------------------------------------

void RenderMoves( eng_draw& Draw )
{
    
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
    
    for( s32 i=0; i<s_MoveList.m_nMoves; i++ )
    {
        match3_board::move& Move = s_MoveList.m_Moves[i];
        
        xcolor Color(255,255,255,70);
        if( i == s_AIMoveIndex ) Color.Set(255,255,255, 180);
        
        s32 X1, Y1;
        s_M3Board.getTileXYFromIndex( X1, Y1, Move.m_iTile );
        
        s32 X2=0, Y2=0;
        switch( Move.m_Direction )
        {
            case match3_board::move::DIRECTION_LEFT:   X2=-1; break;
            case match3_board::move::DIRECTION_RIGHT:  X2=+1; break;
            case match3_board::move::DIRECTION_DOWN:   Y2=+1; break;
            case match3_board::move::DIRECTION_UP:     Y2=-1; break;
        }
        
        f32 TileX = (f32)XOffset + X1*TileSize + TileSize/2;
        f32 TileY = (f32)YOffset + Y1*TileSize + TileSize/2;
        
        Draw.DrawSolidRect( xrect( TileX + X2 * TileSize/2 - TileSize/8,
                                   TileY + Y2 * TileSize/2 - TileSize/8,
                                   TileX + X2 * TileSize/2 + TileSize/8,
                                   TileY + Y2 * TileSize/2 + TileSize/8), Color );
    }
    
    Draw.End();
}

//--------------------------------------------------------------------------------

static
f32 UpdateMPS( void )
{
    static xtick                    s_MoveMPSLastFrameTime=0;
    static s32                      s_iMovePPS=0;
    static xsafe_array<f64, 32>     s_MovePerSecondSamples={0};
    static s32                      s_MoveMPSLastFrameMoves=0;
    
    const xtick CurrentTime  = x_GetTime();
    const s32   CurrentMoves = s_AI.getTotalMovesMade();
    const xtick DeltaTime    = CurrentTime  - s_MoveMPSLastFrameTime;
    const s32   DeltaMoves   = CurrentMoves - s_MoveMPSLastFrameMoves ;
    
    s_MovePerSecondSamples[ s_iMovePPS++ ] = DeltaMoves / x_TicksToSec( DeltaTime );
    s_iMovePPS                            %= s_MovePerSecondSamples.getCount();
    
    s_MoveMPSLastFrameTime                 = CurrentTime;
    s_MoveMPSLastFrameMoves                = CurrentMoves;
    
    f64 Sum = 0;
    for( f64 i : s_MovePerSecondSamples )
    {
        Sum += i;
    }
    
    return ( f32(Sum)/s_MovePerSecondSamples.getCount() + 0.5f);
}

//--------------------------------------------------------------------------------

void Render( void )
{
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    RenderTheBoard( Draw );
    RenderPieces( Draw );
    RenderMatches( Draw );
    RenderMoves( Draw );
    x_printfxy( 0, 4, "RndSeed:    %d", s_Seed );
    x_printfxy( 0, 5, "Map:        %d", s_iMap );
    x_printfxy( 0, 6, "Moves Made: %d", s_AI.getTotalMovesMade() );
    x_printfxy( 0, 7, "Moves Per Second: %6.2f", UpdateMPS() );
}

//--------------------------------------------------------------------------------

void InitGame( void )
{
    s_MatchList.m_nMatches  = 0;
    s_MoveList.m_nMoves     = 0;
    s_M3Board.Init( s_Seed );
    
    s_M3Board.setBoardLayer( s_pMaps[s_iMap] );
    
    //
    // OK all initialize lets start it up
    //
    s_M3Board.StartItUp();
    
    //
    // AI think about the moves
    //
    s_AI.ThinkAboutTheMoves( s_M3Board );
    
    //
    // Start the game clock
    //
    s_ClockGameTime.Reset();
    s_ClockGameTime.Start();
}

//--------------------------------------------------------------------------------
eng_keyboard KeyBoard;
eng_touchpad TouchPad;

void AdvanceLogic( void )
{
    //
    // Handle Input
    //
#if defined TARGET_IOS
    if( s_ClockGameTime.ReadMs() > 30 )
#else
    if( 1 )
#endif
    {
        const xvector2 Touch = TouchPad.getValue(eng_touchpad::ANALOG_TOUCH_1_REL);
        
        if( KeyBoard.wasPressed( eng_keyboard::KEY_UP ) || Touch.m_Y > 30 )
        {
            s_Seed--;
            InitGame();
        }
        else if( KeyBoard.wasPressed( eng_keyboard::KEY_DOWN ) || Touch.m_Y < -30 )
        {
            s_Seed++;
            InitGame();
        }
        else if( KeyBoard.wasPressed( eng_keyboard::KEY_LEFT ) || Touch.m_X < -30 )
        {
            s_iMap--;
            if( s_iMap < 0 ) s_iMap = 0;
            
            InitGame();
        }
        else if( KeyBoard.wasPressed( eng_keyboard::KEY_RIGHT ) || Touch.m_X > 30 )
        {
            s_iMap++;
            if( s_pMaps[s_iMap] == NULL ) s_iMap--;
            InitGame();
        }
    }
    
    //
    // Handle the actual logic
    //
    f32 TweakTime = 0.4f;
    if( s_ClockGameTime.ReadMs() > 420*TweakTime )
    {
        s_ClockGameTime.Reset();
        s_ClockGameTime.Start();

        xbool IsStable = s_M3Board.ComputeStep();
        
        if( IsStable )
        {
            /*
            const match3_board::tile& Base = s_M3Board.getTile( 0, 0 );
            for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
            for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
            {
                match3_board::tile& Tile = s_M3Board.getRWTile( X, Y );
                
                Tile.m_iPiece = Base.m_iPiece;
            }
            */
            
            if( s_ClockToAIMove.IsRunning() == FALSE )
            {
                s_ClockToAIMove.Reset();
                s_ClockToAIMove.Start();
                s_M3Board.FindMatches( s_MatchList );
            }
            else if( s_ClockToAIMove.ReadSec() > 2*TweakTime )
            {
                if( s_ClockToAIMove.ReadSec() < 4*TweakTime )
                {
                    if( s_MatchList.m_nMatches == 0 )
                    {
                        if( s_MoveList.m_nMoves == 0 )
                        {
                            s_M3Board.FindMoves( s_MoveList );
                            
                            s_AI.getRecommendation( s_AIMoveIndex, s_AIMoveSeed );
                            s_AI.MakeMove( s_MoveList.m_Moves[s_AIMoveIndex], s_AIMoveSeed );
                        }
                        else
                        {
                            // Find the moves to show the player
                            s_M3Board.FindMoves( s_MoveList );
                        }
                    }
                    else
                    {
                        s_M3Board.SolveMatches( s_MatchList );
                        s_M3Board.FindMatches( s_MatchList );
                        s_ClockToAIMove.Stop();
                    }
                }
                else
                {
                    //
                    // Update the real board
                    //
                    {
                        s_M3Board.UpdateWithMove( s_MoveList.m_Moves[s_AIMoveIndex], s_AIMoveSeed );
                        s_M3Board.FindMoves( s_MoveList );
                        if( s_MoveList.m_nMoves == 0 )
                        {
                            s_M3Board.ResetBoard();
                        }
                    }
                    
                    s_MoveList.m_nMoves = 0;
                    s_ClockToAIMove.Stop();
                }
            }
        }
        else
        {
            s_ClockToAIMove.Stop();
        }
    }
    else
    if( KeyBoard.wasPressed( eng_keyboard::KEY_SPACE ))
    {
        s_M3Board.ComputeStep();
        x_Sleep( 600*2 );
    }
}

//--------------------------------------------------------------------------------

void Initialize( void )
{
    //
    // Initialize the engine
    //
    eng_Init();
    g_Scheduler.Init( 2 );
    
    eng_hwin MainWindow;
    
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    eng_SetCurrentContext(DisplayContext);
    
    //
    // Initialize the game
    //
    InitGame();
}

//--------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    Initialize();
    
    // Get the current context
    eng_context& DisplayContext = eng_GetCurrentContext();
    
    // Now we will sleep for a while then exit
    while( DisplayContext.HandleEvents() )
    {
        eng_SetCurrentContext(DisplayContext);
        
        AdvanceLogic();
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();
        
        DisplayContext.PageFlip(TRUE);
    }
}
