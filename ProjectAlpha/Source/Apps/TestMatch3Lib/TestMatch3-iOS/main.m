//
//  main.m
//  TestMatch3-iOS
//
//  Created by Tomas Arce on 8/22/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

/*
int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
 */
