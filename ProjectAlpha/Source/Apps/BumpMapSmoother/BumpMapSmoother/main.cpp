//
//  main.cpp
//  BumpMapSmoother
//
//  Created by Tomas Arce on 9/2/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "xbmp_tools.h"

//------------------------------------------------------------------------------------

void ProcessBitmap( const xstring& Src, const xstring& Dest )
{
    xbitmap Bitmap;
    
    if( FALSE == xbmp_Load( Bitmap, (const char*)Src ) )
    {
        x_throw( xfs("ERROR: Unable to load [%s]", (const char*) Src) );
    }
    
    xcolor* pBitmap = (xcolor*)Bitmap.getMip( 0 );
    xptr<f32>   Temp;
    
    const s32 Width  = Bitmap.getWidth();
    const s32 Height = Bitmap.getHeight();
    
    Temp.Alloc( Width*Height );
    
    // Clear the blue channel since it is not needed and we need a temp variable
    Temp.SetMemory(0);
    
    //
    // First pass removed all the antialise pixels
    //
    for( s32 v=0; v<Height; v++ )
    for( s32 u=0; u<Width; u++ )
    {
        const xcolor& Color0  = pBitmap[ u + v*Width ];
        
        // Pixel has not color we are not interested
        if( Color0.m_R == 0 )
            continue;
        
        // Find if any of the neighbords have any black pixel
        for( s32 y = x_Max( 0, v-1 ); y<x_Min(Height, v+2); y++ )
        for( s32 x = x_Max( 0, u-1 ); x<x_Min(Width, u+2); x++ )
        {
            // This is the pixel we are computing
            if( x == u && y == v )
                continue;
            
            const xcolor& Color  = pBitmap[ x + y*Width ];
            if( Color.m_R == 0 )
            {
                // Mark pixel for delition
                Temp[  u + v*Width ] = 1;
                
                // Exit loop
                x = y = Width*Height;
            }
        }
    }
    
    // Officially removed the selected pixels
    for( s32 v=0; v<Height; v++ )
    for( s32 u=0; u<Width; u++ )
    {
        xcolor& Color  = pBitmap[ u + v*Width ];
        f32&    Value  = Temp[  u + v*Width ];
        if( Value > 0 )
        {
            Color.m_R = 0;
            Value = 0;
        }
    }
    
    //
    // Try to to give higher precision to points in the picture
    // so that they are smother.
    // ** Actually this may not matter at all. Since the G
    //    does not contribute much in the final image
    //
    if( 0 )
    {
        for( s32 v=0; v<Height; v++ )
        for( s32 u=0; u<Width; u++ )
        {
            xcolor& Color0  = pBitmap[ u + v*Width ];
            
            // Pixel has not color we are not interested
            if( Color0.m_R == 0 )
                continue;
            
            // Find if any of the neighbords have any pixel values
            s32     nColors = 0;
            f32     R=0;
            for( s32 y = x_Max( 0, v-1 ); y<x_Min(Height, v+2); y++ )
            for( s32 x = x_Max( 0, u-1 ); x<x_Min(Width, u+2); x++ )
            {
                // This is the pixel we are computing
                if( x == u && y == v )
                    continue;
                
                xcolor& Color  = pBitmap[ x + y*Width ];
                if( Color.m_R > 0 )
                {
                    nColors++;
                    R += Color.m_R;
                }
            }
            
            // Try to smooth out pixel if we foudn neiboards
            if( nColors )
            {
                // Average Height of the field
                f32 Value = R / nColors;
                
                // Lets find the difference between average and our pixel H
                Value = Value - Color0.m_R;
                
                // Weight the influence of the other pixel
                Value *= 0.1;
                
                // Maximun range that we are wheeling to change is 1 up or down
                Value = x_Range(Value, -1, 1 );
                Value += Color0.m_R;
                
                // Store the value on the temp variable
                Temp[  u + v*Width ] = Value / 255;
            }
        }
    
        // Copy all the values to the bitmap
        for( s32 i=0; i<Width*Height; i++ )
        {
            if( Temp[i] == 0 )
                continue;
            
            ASSERT( Temp[i] >= 0 );
            ASSERT( Temp[i] <= 1 );
            
            //
            const u16 I2 = u16(Temp[i] * 0xffff);
            const u8  r  = u8(I2>>8);
            const u8  g  = u8(I2&0xff);
            
            // We will keep the original high order bits for higher frequency detail
            // however because the sampling is such low resolution it may not matter
            //pBitmap[i].m_R = r;
            pBitmap[i].m_G = g;
            
            Temp[i] = 0;
        }
    }
    
    //
    // Second make the gradiant hit zero
    //
    if( 1 )
    {
        const s32 MaxL = x_Max( Width, Height )/2;
        for( s32 i=0; i<MaxL; i++)
        {
            s32 bExitEarly = TRUE;
            
            // So a step on the filling
            for( s32 v=0; v<Height; v++ )
            for( s32 u=0; u<Width; u++ )
            {
                xcolor& Color0  = pBitmap[ u + v*Width ];
                
                // If we already have any value then there is not need to process the pixel
                if( Color0.m_R )
                    continue;
                
                // Find if any of the neighbords have any pixel values
                s32     nColors = 0;
                f32     R=0;
                for( s32 y = x_Max( 0, v-1 ); y<x_Min(Height, v+2); y++ )
                for( s32 x = x_Max( 0, u-1 ); x<x_Min(Width, u+2); x++ )
                {
                    // This is the pixel we are computing
                    if( x == u && y == v )
                        continue;
                    
                    xcolor& Color  = pBitmap[ x + y*Width ];
                    if( Color.m_R > 0 )
                    {
                        //if( Color.m_R > R ) R = Color.m_R ;
                        nColors++;
                        R += Color.m_R;
                    }
                }
                
                // See if we have to set any value
                if( nColors > 0 )
                {
                    s32     ShortX   = x_Min( u, Width-u );
                    s32     ShortY   = x_Min( v, Height-v );
                    s32     Shortest = x_Min( ShortX, ShortY );
                    f32&    Value    = Temp[  u + v*Width ];
                    
                    // What step do my neighbords think I should be?
                    R /= nColors;
                    
                    // But I need to hit zero when I hit the shortest path so
                    R = 15;//x_Exp( (R / (Shortest*5)) );
                    
                    const f32 GAUSSIAN_HEIGHT       = 1;
                    const f32 GAUSSIAN_DEVIATION    = 0.125;
                    const f32 dx = 2.0*u/(Width*1.1) -1.0;
                    const f32 dy = 2.0*v/(Height*1.1) -1.0;
                    const f32 I  = GAUSSIAN_HEIGHT * x_Exp(-(dx*dx+dy*dy)/GAUSSIAN_DEVIATION);

                    
                    R = I * 255 * 10;
                    if( R > 15 ) R = 15;
                    
                    // Set the final color in a temp variable
                    Value = R/0xff;
                    
                    // Hit to exit early
                    bExitEarly = FALSE;
                }
            }
            
            // Do we have to do any work?
            if( bExitEarly ) break;
            
            // Copy all the blue values into the reds
            for( s32 v=0; v<Height; v++ )
            for( s32 u=0; u<Width; u++ )
            {
                xcolor& Color  = pBitmap[ u + v*Width ];
                f32&    Value    = Temp[  u + v*Width ];
                
                ASSERT( Value >= 0 );
                if( Value > 0 )
                {
                    ASSERT( Value <= 1 );
                    
                    const u16 I2 = u16(Value * 0xffff);
                    const u8  r  = u8(I2>>8);
                    //const u8  g  = u8(I2&0xff);
                    
                    Color.m_R = r;
                    //Color.m_G = g;
                }
            }
        }
    }
    
    //
    // Lets make the bitmap to be at the right scale
    //
    Bitmap.CreateResizedBitmap(Bitmap, 84, 84 );
    
    //
    // Save the bitmap
    //
    const xstring FinalDestTGA( xstring::BuildFromFormat("%s.tga", (const char*)Dest ) );
    const xstring FinalDestXBMP( xstring::BuildFromFormat("%s.xbmp", (const char*)Dest ) );
    if( FALSE == Bitmap.SaveTGA( FinalDestTGA ) )
    {
        x_throw( xfs("ERROR: Unable to Save TGA [%s]", (const char*) FinalDestTGA) );
    }
    
    if( FALSE == Bitmap.Save( FinalDestXBMP ) )
    {
        x_throw( xfs("ERROR: Unable to Save XBMP [%s]", (const char*) FinalDestXBMP) );
    }
}

//------------------------------------------------------------------------------------

int main(int argc, const char * argv[])
{
    xcmdline    CmdLine;
    xstring     Src;
    xstring     Dest;
    xstring     Project;
    
    // Add the command switches
    CmdLine.AddCmdSwitch( "Project", 1, 1, 1, 1, FALSE, xcmdline::TYPE_STRING );
    CmdLine.AddCmdSwitch( "Src",  1, 1, 1, 1,    FALSE, xcmdline::TYPE_STRING );
    CmdLine.AddCmdSwitch( "Dest", 1, 1, 1, 1,    FALSE, xcmdline::TYPE_STRING );

    // handy way of dealing with string compares
    static u32 SrcCRC       = x_strCRC( "Src" );
    static u32 DestCRC      = x_strCRC( "Dest" );
    static u32 ProjectCRC   = x_strCRC( "Project" );
    
    // Start parsing the arguments
    CmdLine.Parse( argc, argv );
    if( CmdLine.DoesUserNeedsHelp() )
    {
        x_printf( "-Project <projectpath> -Src <filename> -Dest <filename>.tga \n" );
        return 0;
    }
    
    // Handle parameters
    for( s32 i=0; i<CmdLine.GetCommandCount(); i++ )
    {
        u32 CmdCRC = CmdLine.GetCmdCRC(i);
        if( CmdCRC == SrcCRC )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            Src = CmdLine.GetArgument( Offset );
        }
        else if( CmdCRC == DestCRC )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            Dest = CmdLine.GetArgument( Offset );
        }
        else if( CmdCRC == ProjectCRC )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            Project = CmdLine.GetArgument( Offset );
        }
    }
    
    // Ok we are ready to start processing
    x_try;
    
    Src.Format("%s/%s", (const char*)Project, (const char*)Src );
    Dest.Format("%s/%s", (const char*)Project, (const char*)Dest );
    
    ProcessBitmap( Src, Dest);
    
    x_catch_begin;
    {
        x_display_exception;
        return 1;
    }
    x_catch_end;
    
    return 0;
}

