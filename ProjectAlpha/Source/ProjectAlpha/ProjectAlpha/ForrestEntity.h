//
//  ForrestEntity.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef FORREST_ENTITY_H
#define FORREST_ENTITY_H

class forrest_entity :
    public gb_entity,
    public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1( forrest_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )
    
    void                    setup                   ( u64 gBackGround, u64 gAtlas );
    
protected:
    
    virtual void                    vSetup                  ( void ) override               {}
    virtual void                    onUpdate                ( const gb_msg& msg ) override ;
    virtual void                    vResolveDependencies    ( void ) override               {}
    virtual void                    onRunRenderJob          ( void ) const override;
    
protected:

    f32                         m_ResScale;
    f32                         m_Pos;
    eng_texture_rsc::ref        m_Background;
    xsafe_array<eng_sprite,3>   m_lSprites;
};

#endif /* defined(__ProjectAlpha__ForrestEntity__) */
