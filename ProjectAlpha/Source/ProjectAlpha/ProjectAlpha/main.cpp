//
//  main.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "projectAlpha.h"

//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    xptr<project_alpha> GameInstance;
    
    // Allocate one instance
    GameInstance.New( 1, XMEM_FLAG_ALIGN_8B );
    
    // Inialize the game
    GameInstance->Initialize();
    
    // By doing this the main thread basically becomes a worker
    while( 1 )
    {
        g_Scheduler.MainThreadRunJobs();
    }
}

