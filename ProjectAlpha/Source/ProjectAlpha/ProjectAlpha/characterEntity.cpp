//
//  characterEntity.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "gb_Base.h"
#include "characterEntity.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
GB_CLASS_TYPE( character_entity, gb_component_type::FLAGS_DYNAMIC_MASK )

//-----------------------------------------------------------------------------------

void character_entity::setup( const u64 gAtlas, const char* pWalkingAnimName, const f32 X )
{
    m_AnimSprite.setup( gAtlas, pWalkingAnimName );
    
    const s32 H = 0;
    m_Pos.m_Y = H;
    m_Pos.m_X = X;
}

//-----------------------------------------------------------------------------------

void character_entity::onUpdate( const gb_msg& msg )
{
    
    if( m_AnimSprite.BeginAdvanceTime( msg.m_DeltaTime/1000 ) == FALSE )
        return;
    
    eng_sprite_animated::event Event;
    while( m_AnimSprite.WhileAdvanceTime( Event ) )
    {
        // Handle events here
    }

   // lets set this character to render
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-----------------------------------------------------------------------------------

void character_entity::onRunRenderJob( void ) const
{
    eng_context&        Context  = eng_GetCurrentContext();
    eng_draw&           Draw     = Context.getDraw();
    
    s32 XRes, YRes;
    Context.GetScreenResolution( XRes, YRes );
    
    const f32 ResScale = x_Max( XRes/768.f, YRes/1024.f );

    const_cast<character_entity*>(this)->m_AnimSprite.DrawSprite( Draw, m_Pos*ResScale, xvector2(ResScale,ResScale) );
}
