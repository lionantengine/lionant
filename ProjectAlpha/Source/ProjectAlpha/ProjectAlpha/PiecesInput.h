//
//  BoardInput.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef PIECES_INPUT_H
#define PIECES_INPUT_H

#include "gb_base.h"

class pieces_input : public gb_input_component
{
public:
    
    GB_COMPONENT_RTTI1_PRESETS( pieces_input, gb_input_component );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

public:
    
    struct presets : public gb_component::presets
    {
        presets( void )
        {
        }
    };
    
public:
    
                                        pieces_input             ( void );
    virtual xbool                       hasPackData             ( void ) { return TRUE; }
    
protected:
    
    virtual void        vResolveDependencies    ( void );
    virtual void        vSetup                  ( void );
    virtual void		vUpdate                 ( void );
    virtual void        vSaveComponent          ( xtextfile& File );
    virtual void        vLoadComponent          ( xtextfile& File );
    
protected:
    
    xhandle             m_hNetworkPlayer;
};

#endif
