//
//  backgroundRender.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "gb_Base.h"
#include "backgroundEntity.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
GB_CLASS_TYPE( background_entity, gb_component_type::FLAGS_DYNAMIC_MASK )

//-----------------------------------------------------------------------------------

void background_entity::vUpdate( void )
{
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-----------------------------------------------------------------------------------

void background_entity::onRunRenderJob( void ) const
{
    const eng_texture*  pTexture = m_Background.getAsset();
    eng_context&        Context  = eng_GetCurrentContext();
    eng_draw&           Draw     = Context.getDraw();
    
    
    s32 W, H;
    if( pTexture == NULL )
    {
        W = 768;
        H = 768;
        pTexture = &Draw.GetWhiteTexture();
    }
    else
    {
        W = pTexture->getWidth();
        H = pTexture->getHeight();
    }
    
    //
    // Render the background
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LB
                | ENG_DRAW_MODE_MISC_FLUSH
                | ENG_DRAW_MODE_TEXTURE_ON
                | ENG_DRAW_MODE_BLEND_ALPHA   
                | ENG_DRAW_MODE_ZBUFFER_OFF );
    
    Draw.SetTexture( *pTexture );
    
    Draw.DrawTexturedRect( xrect( 0.f, 0.f, f32(W), f32(H) ), xrect( 0.f, 1.f, 1.f, 0.f ), xcolor( ~0 ) );
    
    Draw.End();
}
