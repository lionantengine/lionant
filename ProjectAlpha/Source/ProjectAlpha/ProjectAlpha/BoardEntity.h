//
//  BoardEntity.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/26/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef BOARD_ENTITY_H
#define BOARD_ENTITY_H

class pieces_entity;

class board_entity :
    public gb_entity,
    public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1( board_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )
    
                                    board_entity            ( void ) {}
            void                    setup                   ( u64 Guid, xguid gPiecesEntity )                    { m_BoardAtlas.setup( Guid, "Ref000" ); m_gPieceEntity = gPiecesEntity; }

protected:

    enum
    {
        MAX_BOARDSIZE       = 9,
        MAX_TILES           = MAX_BOARDSIZE * MAX_BOARDSIZE,
        INDEX_BUFFER_COUNT  = MAX_TILES*6*2,
        VERTEX_BUFFER_COUNT = MAX_TILES*4*2
    };
    
protected:
    
    virtual void                    vSetup                  ( void ) override {}
    virtual void                    vUpdate                 ( void ) override;
    virtual void                    vResolveDependencies    ( void ) override               {}
    virtual void                    onRunRenderJob          ( void ) const override;

protected:

            void                    onGraphicsInitialize    ( void );
            void                    onMapInitialize         ( const pieces_entity& PieceEntity );

protected:

    xguid                   m_gPieceEntity;
    eng_sprite              m_BoardAtlas;
    eng_vbuffer             m_VertexBuffer;
    eng_ibuffer             m_IndexBuffer;
    eng_fshader             m_FragmentShader;
    eng_vshader             m_VertexShader;
    eng_shader_program      m_ShaderProgram;
    eng_vertex_desc         m_VertexDesc;
    xbool                   m_bInitGraphics = FALSE;
    const char*             m_pMap = NULL;
    s32                     m_iVertex;

};

#endif

