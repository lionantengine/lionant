//
//  ForrestEntity.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "gb_Base.h"
#include "ForrestEntity.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
GB_CLASS_TYPE( forrest_entity, gb_component_type::FLAGS_DYNAMIC_MASK )

//-----------------------------------------------------------------------------------

void forrest_entity::onUpdate( const gb_msg& msg )
{
    m_Pos -= msg.m_DeltaTime*4*m_ResScale;
    
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-----------------------------------------------------------------------------------

void forrest_entity::setup( u64 gBackGround, u64 gAtlas )
{
    m_Background.setup( gBackGround );
    
    m_lSprites[0].setup( gAtlas,"BigTree01" );
    m_lSprites[1].setup( gAtlas,"BigTree02" );
    m_lSprites[2].setup( gAtlas,"BigTree03" );
    
    s32 XRes, YRes;
    eng_GetCurrentContext().GetScreenResolution( XRes, YRes );
    
    m_ResScale = x_Max( XRes/768.f, YRes/1024.f );

    m_Pos = -1000*m_ResScale;

}

//-----------------------------------------------------------------------------------

void forrest_entity::onRunRenderJob( void ) const
{
    const eng_texture*  pTexture = m_Background.getAsset();
    eng_context&        Context  = eng_GetCurrentContext();
    eng_draw&           Draw     = Context.getDraw();
    
    
    if( pTexture == NULL )
    {
        return;
    }

    //
    // Render the Background
    //
    {
        Draw.Begin( ENG_DRAW_MODE_2D_LB
                    | ENG_DRAW_MODE_TEXTURE_ON
                    | ENG_DRAW_MODE_ZBUFFER_OFF );
        
        Draw.SetTexture( *pTexture );
        
        f32 PicW = pTexture->getWidth() * 2 * m_ResScale;
        f32 PicH = pTexture->getHeight() * 4 * m_ResScale;
        f32 TreePos = m_Pos*0.6;
        f32 offset1 = 0.5*PicW + ( x_FMod( TreePos, PicW ) );
        f32 offset2 = 0.5*PicW + ( x_FMod( TreePos + PicW / 2, PicW ) );
        
        Draw.DrawTexturedRect( xrect( offset1*2.f, 0, offset1*2.f + PicW, PicH ), xrect( 0.99f, 1.f, 0.01f, 0 ), xcolor( 255, 255, 255, 255 ) );
        Draw.DrawTexturedRect( xrect( offset2*2.f, 0, offset2*2.f + PicW, PicH ), xrect( 0.01f, 1.f, 0.99f, 0 ), xcolor( 255, 255, 255, 255 ) );
        
        Draw.End();
    }
    
    
    //
    // Render trees
    //
    {
        const s32 W = 768 * m_ResScale;
        const s32 MaxTreeWidth = 400 * m_ResScale;
        const s32 NewW = MaxTreeWidth + W;
        f32 uoffset0 = W+MaxTreeWidth/2 + x_FMod( m_Pos*2.3  + MaxTreeWidth/2,      NewW );
        f32 uoffset1 = W+MaxTreeWidth/2 + x_FMod( m_Pos*2.3  + MaxTreeWidth*1.5,    NewW );
        f32 uoffset2 = W+MaxTreeWidth/2 + x_FMod( m_Pos*1.65 + MaxTreeWidth,        NewW );
        f32 uoffset3 = W+MaxTreeWidth/2 + x_FMod( m_Pos*1.65 + MaxTreeWidth*2,      NewW );

        
        const_cast<forrest_entity*>(this)->m_lSprites[2].DrawSprite( Draw, xvector2( uoffset3, 0 ),xvector2(1*m_ResScale,4.4*m_ResScale));
        const_cast<forrest_entity*>(this)->m_lSprites[2].DrawSprite( Draw, xvector2( uoffset2, 0 ),xvector2(1*m_ResScale,4.4*m_ResScale));
        const_cast<forrest_entity*>(this)->m_lSprites[1].DrawSprite( Draw, xvector2( uoffset1, 0 ),xvector2(1*m_ResScale,4.4*m_ResScale));
        const_cast<forrest_entity*>(this)->m_lSprites[0].DrawSprite( Draw, xvector2( uoffset0, 0 ),xvector2(1*m_ResScale,4.4*m_ResScale));
    }
}
