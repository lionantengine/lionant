//
//  SplashScreenRender.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef SPLASH_SCREEN_ENTITY_H
#define SPLASH_SCREEN_ENTITY_H

/////////////////////////////////////////////////////////////////////////////////
// BOARD ENTITY
/////////////////////////////////////////////////////////////////////////////////

class splash_screen_entity : public gb_entity, public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1( splash_screen_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

public:
            void                    setup                   ( u64 Guid, f32 TimeToFade ) { m_Logo.setup( Guid ); m_TimeToFade = TimeToFade; }
            xbool                   isFadeFinish            ( void ) const               { return m_Dir>0?m_TimePast*m_TimePast >= m_TimeToFade:m_TimePast<=0; }
            void                    FadeOutStart            ( f32 S )                    { m_Dir=-S; }

protected:
    
    virtual void                    vSetup                  ( void ) override               {}
    virtual void                    onUpdate                ( const gb_msg& msg ) override ;
    virtual void                    vResolveDependencies    ( void ) override               {}
    virtual void                    onRunRenderJob          ( void ) const override;
    
protected:

    eng_texture_rsc::ref    m_Logo;
    f32                     m_TimePast          = 0;        // Time past since we started to deal with the splash screen
    f32                     m_TimeToFade        = 5;       //  Time to Fade in milliseconds
    f32                     m_Dir               = 1;
};

#endif
