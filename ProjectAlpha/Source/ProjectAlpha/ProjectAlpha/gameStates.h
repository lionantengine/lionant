//
//  gameStates.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef GAME_STATES_H
#define GAME_STATES_H

#include "gb_Base.h"

/////////////////////////////////////////////////////////////////////////////////
// GAME STATE
/////////////////////////////////////////////////////////////////////////////////
struct game_states;
class proja_game_state
{
protected:
    
    virtual void    onInitialize       ( void )=0;
    virtual void    onEnterState       ( void )=0;
    virtual void    onExitState        ( void )=0;
    
protected:

    game_states*        m_pGameStates;
    
protected:
    
    friend struct game_states;
};

/////////////////////////////////////////////////////////////////////////////////
// IN GAME STATE
/////////////////////////////////////////////////////////////////////////////////

struct in_game_state : public proja_game_state
{
    virtual void    onInitialize    ( void ) override;
    virtual void    onEnterState    ( void ) override;
    virtual void    onExitState     ( void ) override;
    
    xguid                   m_gBackGround;
    xguid                   m_gPieces;
    xguid                   m_gForrest;
    xguid                   m_gBoard;
    xsafe_array<xguid,3>    m_gCharacter;
};

/////////////////////////////////////////////////////////////////////////////////
// SPLASH SCREEN STATE
/////////////////////////////////////////////////////////////////////////////////

struct splash_screen_state :
    public proja_game_state,
    public x_simple_job<1>
{
    virtual void    onInitialize        ( void ) override;
    virtual void    onEnterState        ( void ) override;
    virtual void    onExitState         ( void ) override;
    virtual void    onRun               ( void ) override;
    
    xguid   m_gSplashScreen;
};

/////////////////////////////////////////////////////////////////////////////////
// GAME STATES
/////////////////////////////////////////////////////////////////////////////////
struct game_states : public gb_cloud_state
{
public:
    
            void    Initialize      ( void );
            void    GoToNextState   ( proja_game_state& NextState ) { m_pNextState = &NextState; }

public:

    // List of game states
    splash_screen_state     m_SplashScreen;
    in_game_state           m_InGame;

protected:
    
    virtual void    onBeginFrame    ( void ) override;
    virtual void    onEndFrame      ( void ) override {}

protected:

    gb_sync_point           m_SyncInput;
    gb_sync_point           m_SyncEntities;
    gb_sync_point           m_SyncRender;
    proja_game_state*       m_pNextState    = NULL;
    proja_game_state*       m_pActiveState  = NULL;
    
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif
