//
//  BoardEntity.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/26/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "gb_Base.h"
#include "BoardEntity.h"
#include "PiecesEntity.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// SHADERS
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

static const char s_VertexShader_Board[] = R"(

#ifdef GL_ES
precision mediump float;
//precision highp float;
#endif

attribute vec3      aPosition;
attribute vec2      aTexCoord;
attribute vec4      aColor;

varying vec4        Color;
varying vec2        tCurr;

uniform mat4        L2C;

void main()
{
    // Pass along the vertex color to the pixel shader
    Color       = aColor;
    tCurr       = aTexCoord;
    
    // this may be equivalent (gl_Position = ftransform();)
    gl_Position = L2C * vec4( aPosition, 1. );
}

)";

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_FrangmentShader_Board[] = R"(

#ifdef GL_ES
//precision mediump float;
precision highp float;
#endif

varying vec4        Color;
varying vec2        tCurr;

uniform sampler2D   Texture;

//-------------------------------------------------------------------------------

void main( void )
{
    vec4 TextureColor = texture2D( Texture, tCurr.xy );
    gl_FragColor = Color * TextureColor;
}

)";

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

struct board_sprite_layout
{
    s32     m_iSprite;
    f32     m_ScaleX;
    f32     m_ScaleY;
};

// Base Pieces
// 0   1   2   3   4   5
// #.  ##  #.  #.  .#  ##
// ..  ..  .#  #.  ##  ##
static const board_sprite_layout s_BoardPieceLayout[] =
{
    { -1,   0,      0 },            // ..       // Empty
                                    // ..
    
    { 0,    1,      1 },            // #.
                                    // ..
    
    { 0,   -1,      1 },            // .#
                                    // ..
    
    { 1,    1,      1 },            // ##
                                    // ..
    
    { 0,    1,     -1 },            // ..
                                    // #.
    
    { 3,    1,      1 },            // #.
                                    // #.
    
    { 2,   -1,      1 },            // .#
                                    // #.
    
    { 4,   -1,     -1 },            // ##
                                    // #.
    
    { 0,   -1,     -1 },            // ..
                                    // .#
    
    { 2,    1,      1 },            // #.
                                    // .#
    
    { 3,   -1,      1 },            // .#
                                    // .#
    
    { 4,    1,     -1 },            // ##
                                    // .#
    
    { 1,    1,     -1 },            // ..
                                    // ##
    
    { 4,   -1,      1 },            // #.
                                    // ##
    
    { 4,    1,      1 },            // .#
                                    // ##
    
    { 5,    1,      1 }             // ##
                                    // ##
};

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
GB_CLASS_TYPE( board_entity, gb_component_type::FLAGS_DYNAMIC_MASK )

//-----------------------------------------------------------------------------------

void board_entity::vUpdate( void )
{
    // Nothing to do if we dont have the asset ready
    if( NULL == m_BoardAtlas.Resolve() )
        return;
    
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-------------------------------------------------------------------------------

void board_entity::onGraphicsInitialize( void )
{
    //eng_context&                Context = eng_GetCurrentContext();
    
    //
    // Create vertex descritors
    //
    m_VertexDesc.DescriveVertex( "aPosition", eng_vertex_desc::DATA_DESC_F32X3, eng_vertex_desc::USE_DESC_POSITION,
                                sizeof(draw_vertex), sizeof(f32)*0 );
    
    m_VertexDesc.DescriveVertex( "aTexCoord", eng_vertex_desc::DATA_DESC_F32X2, eng_vertex_desc::USE_DESC_TEXCOORD,
                                sizeof(draw_vertex), sizeof(f32)*3 );
    
    m_VertexDesc.DescriveVertex( "aColor",    eng_vertex_desc::DATA_DESC_U8x4,  eng_vertex_desc::USE_DESC_COLOR,
                                sizeof(draw_vertex), sizeof(f32)*5 );
    
    
    //
    // Initialize both programs
    //
    m_VertexShader.LoadFromMemory   ( s_VertexShader_Board, sizeof(s_VertexShader_Board) );
    m_FragmentShader.LoadFromMemory ( s_FrangmentShader_Board, sizeof(s_FrangmentShader_Board) );
    m_ShaderProgram.LinkShaders     ( m_VertexDesc, m_VertexShader, m_FragmentShader );
    
    m_ShaderProgram.LinkRegisterToUniformVariable( 0, "L2C" );
    m_ShaderProgram.LinkTextureRegisterWithUniform( 0, "Texture" );

    //
    // Create the index buffer... static because the basic index pattern never changes
    //
    {
        xptr<u16> iBuffer;
        iBuffer.Alloc( INDEX_BUFFER_COUNT );
        
        for( s32 i=0; i<INDEX_BUFFER_COUNT; i++ )
        {
            static const u16 IndexPatern[] = {0,1,2, 0,2,3 };
            
            iBuffer[i] = IndexPatern[i%6] + (i/6)*4;
        }
        
        m_IndexBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, INDEX_BUFFER_COUNT, &iBuffer[0] );
    }
    
    //
    // Create vertex buffer
    //
    m_VertexBuffer.CreateStaticBuffer( m_VertexDesc, VERTEX_BUFFER_COUNT );
    
    //
    // Officially mark it as initialized
    //
    m_bInitGraphics = TRUE;
}

//-----------------------------------------------------------------------------------

void board_entity::onMapInitialize( const pieces_entity& PieceEntity )
{
    const match3_board&         Match3Board = PieceEntity.getMatch3Board();
    const eng_sprite_rsc*       pRsc        = m_BoardAtlas.Resolve();
    const eng_sprite_rsc::info& SpriteInfo  = m_BoardAtlas.getSpriteInfo( *pRsc );

    s32 XRes, YRes;
    eng_GetCurrentContext().GetScreenResolution( XRes, YRes );
    f32 ResScale = x_Max( XRes/768.f, YRes/1024.f );

    if( ResScale * 768 > XRes )
    {
        ResScale = XRes/768.f;
    }
    
    
    // It must be an animated sprite
    ASSERT( SpriteInfo.m_Animated.m_SpecialMask == 0xffff );

    // Get the pointer for the vertices
    draw_vertex* pVertexBase = (draw_vertex*)m_VertexBuffer.LockData();

    // Zero out the count for rendering tiles
    m_iVertex = 0;
    
    for( s32 Y=0; Y<MAX_BOARDSIZE+1; Y++ )
    for( s32 X=0; X<MAX_BOARDSIZE+1; X++ )
    {
        //
        // Build Unique ID for the board tile
        //
        s32 iBoardTileID=0;
        for( s32 i=0; i<2; i++ )
        for( s32 j=0; j<2; j++ )
        {
            const s32 XTile = X - (1-i);
            const s32 YTile = Y - (1-j);
            
            if( XTile < 0 || YTile < 0 )
                continue;
            
            if( XTile >= MAX_BOARDSIZE ||
                YTile >= MAX_BOARDSIZE )
                continue;
            
            const match3_board::tile& Tile = Match3Board.getTile( XTile, YTile );
            
            // If we have this flag we wont render any thing
            if( x_FlagIsOn( Tile.m_Type, match3_board::tile::TYPE_BOUNDARY ) )
                continue;
            
            
            iBoardTileID |= 1<<(i + j*2);
        }
        
        //
        // Ok ready to render the tile now
        //
        xvector3d Pos = PieceEntity.ConverFromMatchToRender( X, Y );
        
        Pos.m_X -= pieces_entity::TILES_XSIZE/2;
        Pos.m_Y += pieces_entity::TILES_YSIZE/2;
        const board_sprite_layout& Layout = s_BoardPieceLayout[ iBoardTileID ];
        
        //
        // Setup the vertices
        //
        if( Layout.m_iSprite >= 0 )
        {
            const eng_sprite_rsc::info& Sprite = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ SpriteInfo.m_Animated.m_iBase + Layout.m_iSprite ];
            
            for( s32 j=0; j<4; j++ )
            {
                const eng_sprite_rsc::info::vertex& Vert = Sprite.m_Vertex[j];
                
                pVertexBase[m_iVertex].setup(  xvector3d( (Vert.m_Pos.m_X * Layout.m_ScaleX) + Pos.m_X,
                                                            (Vert.m_Pos.m_Y * Layout.m_ScaleY) + Pos.m_Y,
                                                                    0 ),
                                                             Vert.m_UV.m_X,
                                                             Vert.m_UV.m_Y,
                                                             xcolor( 100, 100, 170, 180) );
                
                pVertexBase[m_iVertex].m_X *= ResScale;
                pVertexBase[m_iVertex].m_Y *= ResScale;
                
                m_iVertex++;
            }
        }
    }
    
    ASSERT( m_iVertex <= VERTEX_BUFFER_COUNT );
    
    //
    // Done with the vertices
    //
    m_VertexBuffer.UnlockData();
    
    //
    // Cache this entry
    //
    m_pMap = PieceEntity.getMap();
}

//-----------------------------------------------------------------------------------

void board_entity::onRunRenderJob( void ) const
{
    //
    // Check and see if we need to initialize the graphics
    //
    if( FALSE == m_bInitGraphics )
        const_cast<board_entity*>(this)->onGraphicsInitialize();

    //
    // Check to see if we need to update the map
    //
    pieces_entity& PieceEntity = pieces_entity::SafeCast( *g_EntityMgr.getEntity( m_gPieceEntity ) );
    if( PieceEntity.getMap() != m_pMap )
        const_cast<board_entity*>(this)->onMapInitialize( PieceEntity );
 
    //
    // Flush any pending draw calls
    //
    eng_GetCurrentContext().getDraw().Flush();
    
    //
    // Render the pieces
    //
    // Which direction we are culling
    glCullFace(GL_FRONT);
    
    // Additive blending
    glEnable( GL_BLEND );
    
    glBlendEquationSeparate( GL_FUNC_ADD,
                            GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA,
                        GL_ONE_MINUS_SRC_ALPHA,
                        GL_ONE,
                        GL_ZERO);

    // Z Buffer off
    glDepthMask( FALSE );
    glDisable( GL_DEPTH_TEST );
    
    
    // PASS1 - Render the blobs accummulation
    // Set the shaders and render away
    //
    xmatrix4 L2C;
    s32 XRes, YRes;
    
    L2C.Identity();
    
    eng_GetCurrentContext().GetScreenResolution( XRes, YRes );
    
    L2C.setTranslation( xvector3( -1, -1, 0 ) );
    L2C.setScale      ( xvector3( 2.0f/XRes, 2.0f/YRes, 1 ) );

    
    
    m_ShaderProgram.Activate();
    m_ShaderProgram.setUniformVariable( 0, L2C );
    
    m_VertexBuffer.Activate();
    m_IndexBuffer.Activate();
    
    const eng_sprite_rsc& Rsc = m_BoardAtlas.Preresolved();
    Rsc.m_SpriteAtlas.getAsset()->Activate();
    
    m_IndexBuffer.RenderTriangles( (m_iVertex/4) * 6, 0 );
}
