//
//  characterEntity.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef CHARACTER_ENTITY_H
#define CHARACTER_ENTITY_H

class character_entity :
    public gb_entity,
    public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1( character_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )
    
            void                    setup                   ( const u64 gAtlas, const char* pWalkingAnimName, const f32 X );
    
protected:
    
    virtual void                    vSetup                  ( void ) override               {}
    virtual void                    onUpdate                ( const gb_msg& msg ) override ;
    virtual void                    vResolveDependencies    ( void ) override               {}
    virtual void                    onRunRenderJob          ( void ) const override;
    
protected:

    xvector2                m_Pos;
    eng_sprite_animated     m_AnimSprite;
};

#endif
