//
//  backgroundRender.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef BACKGROUND_ENTITY_H
#define BACKGROUND_ENTITY_H

class background_entity :
    public gb_entity,
    public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1( background_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

            void                    setup                   ( u64 Guid )                    { m_Background.setup( Guid ); }
    
protected:
    
    virtual void                    vSetup                  ( void ) override               {}
    virtual void                    vUpdate                 ( void ) override;
    virtual void                    vResolveDependencies    ( void ) override               {}
    virtual void                    onRunRenderJob          ( void ) const override;
    
protected:
    
    eng_texture_rsc::ref        m_Background;
};

#endif
