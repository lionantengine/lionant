//
//  gameStates.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "gameStates.h"

////////////////////////////////////////////////////////////////////////////////
// Maps
/////////////////////////////////////////////////////////////////////////////////

struct map
{
    const char* m_pMap;
};

static const s32 s_iMap = 2;
static const map s_pMaps[] =
{
    // Most basic test
    {
        "$$$$$$$$$\n"
        "         \n"
        "         \n"
        "         \n"
        "         \n"
        "         \n"
        "         \n"
        "         \n"
        "         \n"
    },
    
    // Every feature test
    {
        "$$$$$$$$$\n"
        " ab   cd \n"
        " ##   ## \n"
        " AB   CD \n"
        "###111###\n"
        "###222###\n"
        "###333###\n"
    },
    
    // Test different position of spawn
    {
        "###$####$\n"
        "###  ### \n"
        "###  ##  \n"
        "$###     \n"
        "  ##     \n"
        "#  #     \n"
        "#        \n"
        "##       \n"
        "#        \n"
    },
    
    //
    {
        "$$$$$$$\n"
        "       \n"
        "###  ##\n"
        "####   \n"
        "$###   \n"
        "# ##   \n"
        "#      \n"
        "##     \n"
        "#      \n"
    },
    
    // Teleporter test
    {
        "$A######$\n"
        "  B####H \n"
        "   C##I  \n"
        "    DJ   \n"
        "abcd  jih\n"
    },
    
    // All different ways to see pieces coming down
    {
        "$$$$$$$$$\n"
        "         \n"
        "ab     hi\n"
        "#### ####\n"
        "#### ####\n"
        "ABCD EFHI\n"
        "         \n"
        "         \n"
        "cd     ef\n"
    }
};


////////////////////////////////////////////////////////////////////////////////
// Basic render manager
/////////////////////////////////////////////////////////////////////////////////

class gb_render_mgr : public gb_default_component_mgr
{
public:
    GB_MANAGER_RTTI(gb_render_mgr, gb_default_component_mgr);
    
    gb_render_mgr( void ) : gb_default_component_mgr("RenderMgr") { setupAffinity( x_base_job::AFFINITY_MAIN_THREAD ); }
    
protected:
    
    virtual void onUpdate( void )
    {
        eng_context& DisplayContext = eng_GetCurrentContext();
        
        DisplayContext.Begin("Begin");
        x_printfxy( 1,2,"THIS IS A TEST" );
        DisplayContext.End();
        
        // rendering jobs hapens inside the pageflip as a serial work.
        DisplayContext.PageFlip(TRUE);
    }
};

static gb_render_mgr g_RenderMgr;

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// game_states
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

void game_states::Initialize( void )
{
    //
    // The Game Graph
    //
    setupBeging( "Game Graph" );
    m_SyncInput.setup     ( "Input", *this,
                           x_va_list( (xuptr)&getStartSyncP()   ),
                           x_va_list( (xuptr)&g_InputMgr        ) );
    m_SyncEntities.setup  ( "Entities", *this,
                           x_va_list( (xuptr)&m_SyncInput       ),
                           x_va_list( (xuptr)&g_EntityMgr       ) );
    m_SyncRender.setup    ( "Rendering", *this,
                           x_va_list( (xuptr)&m_SyncEntities    ),
                           x_va_list( (xuptr)&g_RenderMgr       ) );
    setupEnd();
    
    //
    // Initialize the states
    //
    m_SplashScreen.m_pGameStates    = this; m_SplashScreen.onInitialize();
    m_InGame.m_pGameStates          = this; m_InGame.onInitialize();
    
    // Set the initial state
    GoToNextState( m_SplashScreen );
}

//-----------------------------------------------------------------------------------

void game_states::onBeginFrame( void )
{
    //
    // Deal with transfering states
    //
    if( m_pNextState )
    {
        proja_game_state* pActive = m_pActiveState;
        proja_game_state* pNext   = m_pNextState;
        if( pActive ) pActive->onExitState();
        pNext->onEnterState();
        if( m_pNextState == pNext ) m_pNextState = NULL;
        m_pActiveState = pNext;
    }
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// in_game_state
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
#include "backgroundEntity.h"
#include "PiecesEntity.h"
#include "PiecesInput.h"
#include "ForrestEntity.h"
#include "characterEntity.h"
#include "BoardEntity.h"

void in_game_state::onInitialize( void )
{
    
}

//-----------------------------------------------------------------------------------

void in_game_state::onEnterState( void )
{
    //
    // Create the background instance
    //
    if( (0) )
    {
        m_gBackGround.ResetValue();
        
        // Create entity instance
        background_entity& Background = background_entity::SafeCast( gb_entity::createFromType( m_gBackGround,background_entity::s_Type ) );

        static const u64 BACKGROUND_IMAGE = X_GUID( 2az5_varb_7llv );
        
        Background.setup( BACKGROUND_IMAGE );
        Background.m_Order = 0;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Background );
    }
    
    //
    // Create the pieces instance
    //
    if( 1 )
    {
        m_gPieces.ResetValue();
        
        // Create entity instance & components
        pieces_entity& PiecesEntity = pieces_entity::SafeCast( gb_entity::createFromType( m_gPieces, pieces_entity::s_Type ) );
        pieces_input&  PiecesInput  = pieces_input::SafeCast( *pieces_input::s_Type.createComponent(xguid(0)) );

        // Setup the components inside the entity
        PiecesEntity.addComponent( PiecesInput );
        
        //
        // setup the resource dependencies
        //
        static const u64 ENVIROMENT_MAP = X_GUID( 37sc_okd3_de77 );
        static const u64 SPRITE_ATLAS = X_GUID( 142p_a61w_ac6b );
        
        PiecesEntity.setup( ENVIROMENT_MAP, SPRITE_ATLAS, s_pMaps[s_iMap].m_pMap );
        PiecesEntity.m_Order = 10;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( PiecesEntity );
    }
    
    //
    // Create the board
    //
    {
        m_gBoard.ResetValue();
        
        // Create entity instance
        board_entity& Board = board_entity::SafeCast( board_entity::createFromType( m_gBoard, board_entity::s_Type ) );
        
        static const u64 BOARD_ATLAS = X_GUID( 1o3h_3hc0_yexv );
        
        Board.setup( BOARD_ATLAS, m_gPieces );
        Board.m_Order = 5;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Board );
        
    }
    
    //
    // Create Forrest
    //
    {
        m_gForrest.ResetValue();
        
        // Create entity instance
        forrest_entity& Forrest = forrest_entity::SafeCast( gb_entity::createFromType( m_gForrest, forrest_entity::s_Type ) );
        
        static const u64 FORREST_BACKGROUND = X_GUID( 2qbs_uu4y_vuw3 );
        static const u64 FORREST_SPRITES = X_GUID( 1n0e_2jwg_cahv );
        
        Forrest.setup( FORREST_BACKGROUND, FORREST_SPRITES );
        Forrest.m_Order = 0;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Forrest );
    }
    
    //
    // Create one character
    //
    {
        m_gCharacter[0].ResetValue();
        
        // Create entity instance
        character_entity& Character = character_entity::SafeCast( gb_entity::createFromType( m_gCharacter[0], character_entity::s_Type ) );
        
        static const u64 CHARACTER_SPRITES = X_GUID( 1nge_qvbc_s1sz );
        
        Character.setup( CHARACTER_SPRITES, "ani_stjr_move000", 250 );
        Character.m_Order = 1;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Character );
    }
    
    //
    // Create two character
    //
    {
        m_gCharacter[1].ResetValue();
        
        // Create entity instance
        character_entity& Character = character_entity::SafeCast( gb_entity::createFromType( m_gCharacter[1], character_entity::s_Type ) );
        
        static const u64 CHARACTER_SPRITES = X_GUID( 1nge_qvbc_s1sz );
        
        Character.setup( CHARACTER_SPRITES, "ani_xemB_move000", 100 );
        Character.m_Order = 1;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Character );
    }
    
    //
    // Create zero character
    //
    {
        m_gCharacter[2].ResetValue();
        
        // Create entity instance
        character_entity& Character = character_entity::SafeCast( gb_entity::createFromType( m_gCharacter[2], character_entity::s_Type ) );
        
        static const u64 CHARACTER_SPRITES = X_GUID( 1nge_qvbc_s1sz );
        
        Character.setup( CHARACTER_SPRITES, "ani_zt_move000", 400 );
        Character.m_Order = 1;
        
        // Back up the guid for later
        g_EntityMgr.msgAppendEntity( Character );
    }
}

//-----------------------------------------------------------------------------------

void in_game_state::onExitState( void )
{
    
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// splash_screen_state
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
#include "SplashScreenEntity.h"

void splash_screen_state::onInitialize( void )
{
    //
    // Make sure that we choose not to run on the main thread
    //
    setupAffinity( x_base_job::AFFINITY_NOT_MAIN_THREAD );
}

//-----------------------------------------------------------------------------------

void splash_screen_state::onEnterState( void )
{
    m_gSplashScreen.ResetValue();
    
    // Create the components
    splash_screen_entity& SplashScreenEntity = splash_screen_entity::SafeCast( gb_entity::createFromType( m_gSplashScreen,splash_screen_entity::s_Type ) );
    
    ASSERT( m_gSplashScreen == SplashScreenEntity.getGuid() );
    
    static const u64 LIONANT_LOGO = X_GUID( 27i2_yhr6_br4z );
    
    SplashScreenEntity.setup( LIONANT_LOGO, 1 );
    SplashScreenEntity.m_Order = 100000;
    
    // Add the entity into the game
    g_EntityMgr.msgAppendEntity( SplashScreenEntity );
    
    //
    // Set the state into loading mode as well
    //
    g_Scheduler.StartJobChain( *this );
}

//-----------------------------------------------------------------------------------

void splash_screen_state::onExitState( void )
{

}

//-----------------------------------------------------------------------------------

void splash_screen_state::onRun( void )
{
    //
    // Inediately go to the next state
    //
    m_pGameStates->GoToNextState( m_pGameStates->m_InGame );

    //
    // Make sure that we have the splash screen
    //
    while( NULL == g_EntityMgr.getEntity( m_gSplashScreen ) )
    {
        g_Scheduler.ProcessWhileWait( 100, FALSE );
    }
    
    //
    // Make sure that the fade is done
    //
    splash_screen_entity& SplashScreen = splash_screen_entity::SafeCast( *g_EntityMgr.getEntity( m_gSplashScreen ) );
    
    while( SplashScreen.isFadeFinish() == FALSE )
    {
        g_Scheduler.ProcessWhileWait( 100, FALSE );
    }
    
    // Lets just show off a little
    g_Scheduler.ProcessWhileWait( 1000, FALSE );
    
    // Fade out now...
    SplashScreen.FadeOutStart( 0.5 );
    
    //
    // Now lets wait for the flash screen to fade out.
    //
    while(1)
    {
        g_Scheduler.ProcessWhileWait( 100, FALSE );
        
        if( SplashScreen.isFadeFinish() )
            break;
    }

    //
    // Not need to hide the main screen any more
    //
    g_EntityMgr.msgRemoveEntity( m_gSplashScreen );
}

