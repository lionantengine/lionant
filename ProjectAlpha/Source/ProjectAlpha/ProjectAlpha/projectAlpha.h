//
//  projectAlpha.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef PROJECT_ALPHA_H
#define PROJECT_ALPHA_H

#include "gb_Base.h"
#include "gameStates.h"

/////////////////////////////////////////////////////////////////////////////////
// Basic render manager
/////////////////////////////////////////////////////////////////////////////////

class project_alpha
{
public:

    enum
    {
        MOVE,
        PRESS
    };

public:
    
                project_alpha   ( void ){}
               ~project_alpha   ( void ){}
    void        Initialize      ( void );
    
protected:

    class input_device : public eng_input_vdevice
    {
    public:
        input_device( void );
        ~input_device( void ){}
        
    protected:
#ifdef TARGET_IOS
        eng_touchpad    m_TouchPad;
#elif defined TARGET_OSX || defined TARGET_PC
        eng_mouse       m_Mouse;
#endif
    };
    
    
protected:
    
    game_states             m_GameStates;
    input_device            m_InputDevice;
};

#endif
