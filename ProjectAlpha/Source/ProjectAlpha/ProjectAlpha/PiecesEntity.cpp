//
//  entityBoard.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "PiecesEntity.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ENTITY DOT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

GB_CLASS_TYPE( pieces_entity, gb_component_type::FLAGS_REALTIME_DYNAMIC)

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SHADERS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

static const char s_VSBlobSpriteShader[] = R"(

#ifdef GL_ES
precision mediump float;
//precision highp float;
#endif

attribute vec3      aPosition;
attribute vec2      aTexCoord;
attribute vec4      aColor;

varying vec4        Color;
varying vec2        tCurr;

uniform mat4        L2C;

void main()
{
    // Pass along the vertex color to the pixel shader
    Color       = aColor;
    tCurr       = aTexCoord;
    
    // this may be equivalent (gl_Position = ftransform();)
    gl_Position = L2C * vec4( aPosition, 1. );
}

)";

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_FSBlobHeightAccumulation[] = R"(

#ifdef GL_ES
//precision mediump float;
precision highp float;
#endif

varying vec4        Color;
varying vec2        tCurr;

uniform sampler2D   Texture;

//-------------------------------------------------------------------------------

void main( void )
{
    vec4 Height = texture2D( Texture, tCurr.xy );
    
    // Decompress blob texture
    float Decompress = dot(Height.rg, vec2(0.9961089, 0.0039063 ) );
    
    gl_FragColor = vec4( Color.xyz * Decompress * Height.ggg, Decompress );
}

)";


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_VSFullShader[] = R"(

#ifdef GL_ES
precision mediump float;
//precision highp float;
#endif

attribute vec3      aPosition;
attribute vec2      aTexCoord;
attribute vec4      aColor;

varying vec4        Color;
varying vec2        tCurr0;
varying vec2        tCurr1;
varying vec2        tCurr2;

uniform mat4        L2C;
uniform vec4        iResolution;     // viewport resolution (in pixels) and offset

void main()
{
    // Pass along the vertex color to the pixel shader
    Color       = aColor;
    tCurr0      = aTexCoord;
    tCurr1      = aTexCoord + iResolution.xz * 5.;
    tCurr2      = aTexCoord + iResolution.zy * 5.;
    
    // this may be equivalent (gl_Position = ftransform();)
    gl_Position = L2C * vec4( aPosition, 1. );
}

)";


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

static const char s_FSBlobLighting[] = R"(

#ifdef GL_ES
//precision mediump float; // precision highp float;
precision highp float;
#endif

uniform vec3        Light;     // viewport resolution (in pixels) and offset

varying vec4        Color;
varying vec2        tCurr0;
varying vec2        tCurr1;
varying vec2        tCurr2;

uniform sampler2D   Texture;
uniform sampler2D   EnvTexture;

//-------------------------------------------------------------------------------

vec3 ComputeNormal_fast( float center )
{
    float U      = texture2D ( Texture, tCurr1 ).a;	// U bump map sample
    float V      = texture2D ( Texture, tCurr2 ).a;	// V bump map sample
    float dHdU   = U - center;                                                 // create bump map U offset
    float dHdV   = V - center;                                                 // create bump map V offset
    vec3  normal = vec3( dHdU, dHdV, 0.20 );

    /*
    float U      = texture2D ( Texture, texcoords + iResolution.xz * 5. ).a;	// U bump map sample
    float V      = texture2D ( Texture, texcoords + iResolution.zy * 5. ).a;	// V bump map sample
    float dHdU   = U - center;                                                 // create bump map U offset
    float dHdV   = V - center;                                                 // create bump map V offset
    vec3  normal = vec3( dHdU, dHdV, 0.20 );
    */
    
    /*
     float U      = texture2D ( Texture, texcoords + iResolution.xz * 9. ).a;	// U bump map sample
     float V      = texture2D ( Texture, texcoords + iResolution.zy * 9. ).a;	// V bump map sample
     float dHdU   = U - center;                                                 // create bump map U offset
     float dHdV   = V - center;                                                 // create bump map V offset
     vec3  normal = vec3( dHdU, dHdV, 0.20 );
     */
    return normalize( normal );
}

//-------------------------------------------------------------------------------

void main( void )
{
    const float THRESHOLD    = 0.1;
    const float aaval        = THRESHOLD * 0.17;
    const float AmbientLight = 0.60;
    
    vec4 trash_color  = Color;
    
    // this is the same as tCurr but because it is pass by the vertex shader it does not
    // create a performance penalty
    // vec2 uv = gl_FragCoord.xy * iResolution.xy;
    // vec2 trash_tcurss = tCurr;
    
    // Compute the normal at this UV
    vec4 Pixel      = texture2D         ( Texture, tCurr0.xy );
    
    
    float wclamped1 = clamp( (Pixel.w - (THRESHOLD))/aaval*0.25, 0.3, 1. );
    float wclamped = clamp( (Pixel.w - THRESHOLD)/aaval, 0., 1. );
    
    vec3 pixelColor = Pixel.rgb/Pixel.w;
    vec3 normal     = ComputeNormal_fast( Pixel.a );
    vec3 EnvMap     = texture2D         ( EnvTexture, tCurr0 + (normal.xy * 0.5 + 0.5)*0.5 ).xxx;
    vec3 finalColor;
    
    finalColor.rgb   = vec3(clamp( dot( normal, Light *0.6), 0., 1. ));
    finalColor      += AmbientLight*0.7 + normal.z * 0.3;
    finalColor      *= pixelColor;
    finalColor      += pow(EnvMap.xyz,vec3(2.))*AmbientLight;
    
    finalColor      *= wclamped1;
    
    gl_FragColor.x   = clamp(finalColor.x, 0., 1. );
    gl_FragColor.y   = clamp(finalColor.y, 0., 1. );
    gl_FragColor.z   = clamp(finalColor.z, 0., 1. );
    
    gl_FragColor.a   = wclamped;
 
    //gl_FragColor = finalColor;//vec4( finalColor.xyz, wclamped );
}
)";

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// MAPS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


static u32                      s_Seed = 429;//418;    // 444


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void pieces_entity::presets::vLoad( xtextfile& File )
{
   // File.ReadField("gBulletBP:g", &m_gBullet );
}

//-------------------------------------------------------------------------------

void pieces_entity::presets::vSave( xtextfile& File )
{
    // File.WriteField("gBulletBP:g", m_gBullet.m_Guid );
}

//-------------------------------------------------------------------------------

pieces_entity::pieces_entity( void )
{
 //   m_Timer = x_frand(0.0f,20.0f);
   
    // Make sure that we have reserve the space for all the pieces (Try to minimized allocs)
    m_lPieces.GrowListBy( MAX_PIECES );
}

//-------------------------------------------------------------------------------

void pieces_entity::vResolveDependencies( void )
{
   // m_pPhysics = &physics_component::SafeCast( *(physics_component*)findInterface( physics_component::getRTTI() ));
   // ASSERT(m_pPhysics);
}

//-------------------------------------------------------------------------------

void pieces_entity::vSaveComponent( xtextfile& File )
{
    gb_entity::vSaveComponent( File );
}

//-------------------------------------------------------------------------------

void pieces_entity::vLoadComponent( xtextfile& File )
{
    gb_entity::vLoadComponent( File );
}

//-------------------------------------------------------------------------------

void pieces_entity::onGraphicsInitialize( void )
{
    eng_context&                Context = eng_GetCurrentContext();
    
    //
    // Create vertex descritors
    //
    m_VertexDesc.DescriveVertex( "aPosition", eng_vertex_desc::DATA_DESC_F32X3, eng_vertex_desc::USE_DESC_POSITION,
                                sizeof(draw_vertex), sizeof(f32)*0 );
    
    m_VertexDesc.DescriveVertex( "aTexCoord", eng_vertex_desc::DATA_DESC_F32X2, eng_vertex_desc::USE_DESC_TEXCOORD,
                                sizeof(draw_vertex), sizeof(f32)*3 );
    
    m_VertexDesc.DescriveVertex( "aColor",    eng_vertex_desc::DATA_DESC_U8x4,  eng_vertex_desc::USE_DESC_COLOR,
                                sizeof(draw_vertex), sizeof(f32)*5 );

    
    //
    // Initialize both programs
    //
    m_VertexShader[0].LoadFromMemory( s_VSBlobSpriteShader, sizeof(s_VSBlobSpriteShader) );
    m_VertexShader[1].LoadFromMemory( s_VSFullShader, sizeof(s_VSFullShader) );
    
    m_FragmentShader[0].LoadFromMemory ( s_FSBlobHeightAccumulation, sizeof(s_FSBlobHeightAccumulation) );
    m_FragmentShader[1].LoadFromMemory ( s_FSBlobLighting, sizeof(s_FSBlobLighting) );
    
    m_ShaderProgram[0].LinkShaders     ( m_VertexDesc, m_VertexShader[0], m_FragmentShader[0] );
    m_ShaderProgram[0].LinkRegisterToUniformVariable( 0, "L2C" );
    
    m_ShaderProgram[1].LinkShaders     ( m_VertexDesc, m_VertexShader[1], m_FragmentShader[1] );
    m_ShaderProgram[1].LinkRegisterToUniformVariable( 0, "L2C" );
    m_ShaderProgram[1].LinkRegisterToUniformVariable( 1, "iResolution" );
    m_ShaderProgram[1].LinkRegisterToUniformVariable( 2, "Light" );
    
    m_ShaderProgram[1].LinkTextureRegisterWithUniform( 0, "Texture" );
    m_ShaderProgram[1].LinkTextureRegisterWithUniform( 1, "EnvTexture" );

    //
    // Create the index buffer... static because the basic index pattern never changes
    //
    {
        xptr<u16> iBuffer;
        iBuffer.Alloc( INDEX_BUFFER_COUNT );
        
        for( s32 i=0; i<INDEX_BUFFER_COUNT; i++ )
        {
            static const u16 IndexPatern[] = {0,1,2, 0,2,3 };
            
            iBuffer[i] = IndexPatern[i%6] + (i/6)*4;
        }
        
        m_IndexBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, INDEX_BUFFER_COUNT, &iBuffer[0] );
    }
    
    //
    // Create vertex buffer
    //
    m_VertexBuffer.CreateDynamicBuffer( m_VertexDesc, VERTEX_BUFFER_COUNT );

    //
    // Create temporary texture
    //
    s32 XRes, YRes;
    Context.GetScreenResolution( XRes, YRes);
    m_TextureFromBuffer.CreateTexture( XRes, YRes, xbitmap::FORMAT_R16G16B16A16_f );
    m_RenderToBuffer.CreateRenderBuffer( m_TextureFromBuffer );
    
    //
    // Setup all the sprites
    //
    for( s32 i=0; i<m_SpriteList.getCount(); i++ )
    {
        eng_sprite& Sprite = m_SpriteList[i];
        
        Sprite.setup( m_SpriteAtlas, (const char*)xfs("Piece%03d", i+1));
    }
    
    //
    // Officially mark it as initialized
    //
    m_bInitGraphics = TRUE;
}

//-------------------------------------------------------------------------------

void pieces_entity::RenderPieces( const eng_texture* pEnv, const eng_texture* pSpriteAtlas ) const
{
    //
    // Do we have anything to render?
    //
    if( m_iVertex < 4 )
        return;
    
    //
    // Upload the necesary vertices
    //
    m_VertexBuffer.UploadData( 0, m_iVertex, &m_CPUVertices[0] );
    
    //
    // Must setup the right blending modes and such
    //
    
    // Which direction we are culling
    glCullFace(GL_BACK);
    
    // Additive blending
    glEnable( GL_BLEND );
    
    glBlendEquationSeparate( GL_FUNC_ADD,
                            GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE,
                        GL_ONE,
                        GL_ONE,
                        GL_ONE);
    
    // Z Buffer off
    glDepthMask( FALSE );
    glDisable( GL_DEPTH_TEST );
    glDepthFunc( GL_ALWAYS );
    
    // PASS1 - Render the blobs accummulation
    // Set the shaders and render away
    //
    m_ShaderProgram[0].Activate();
    m_ShaderProgram[0].setUniformVariable( 0, m_L2C );
    
    m_VertexBuffer.Activate();
    m_IndexBuffer.Activate();
    pSpriteAtlas->Activate();
    
    m_IndexBuffer.RenderTriangles( ((m_iVertex/4)- 1) * 6, 0 );
    
    // PASS 2 - Render other stuff
    // Ok Lets render the second pass
    //
    const xvector4 Rect( 1.0/m_XRes, 1.0/m_YRes,
                         0.f, // Must be zero
                         0.f );
    
    xvector3 Light( 1.f, 0.f, 0.f );
    Light.RotateZ( m_Time *0.5f );
    Light.Normalize();
    
    // Alpha
    glBlendEquationSeparate( GL_FUNC_ADD,
                            GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA,
                        GL_ONE_MINUS_SRC_ALPHA,
                        GL_ONE,
                        GL_ZERO);
    

    
    // Deactive the frame buffer from before
    m_RenderToBuffer.Deactivate();
    
    // Set the new constants
    m_ShaderProgram[1].Activate();
    m_ShaderProgram[1].setUniformVariable( 0, m_L2C );
    m_ShaderProgram[1].setUniformVariable( 1, Rect );
    m_ShaderProgram[1].setUniformVariable( 2, Light );
    
    // Activate all the buffers
    m_VertexBuffer.Activate();
    m_IndexBuffer.Activate();
    
    m_TextureFromBuffer.Activate(0);
    pEnv->Activate(1);
    
    s32 iBaseIndex = ((m_iVertex/4)- 1) * 6;
    
    m_IndexBuffer.RenderTriangles( 6, iBaseIndex );
}

//-------------------------------------------------------------------------------

void pieces_entity::onRunRenderJob( void ) const
{
    //
    // Make sure we are ready to render
    //
    if( m_bInitGraphics == FALSE )
        const_cast<pieces_entity*>(this)->onGraphicsInitialize();
    
    // Make sure everything is loaded and ready
    const eng_sprite_rsc*   pSpriteAtlas    = m_SpriteAtlas.getAsset();
    const eng_texture*      pEnv            = m_TextureEnvMap.getAsset();
    if( pSpriteAtlas == NULL || pEnv == NULL )
        return;
    
    //
    // First let flush any pending draw calls
    //
    eng_GetCurrentContext().getDraw().Flush();

    //
    // Set the new viewport
    //
    eng_view View;
    View.setViewport( xirect( 0,0,m_RenderToBuffer.getWidth(), m_RenderToBuffer.getHeight() ));
    m_RenderToBuffer.Activate( View, m_TextureFromBuffer );
    
    // Clear Screen And Depth Buffer
    glClearColor( 0, 0, 0, 0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    eng_CheckForError();
    
    RenderPieces( pEnv, pSpriteAtlas->m_SpriteAtlas.getAsset() );
    
    m_RenderToBuffer.Deactivate();
}

//-------------------------------------------------------------------------------

void pieces_entity::setup( u64 gEnvMap, u64 gSpritesAtlas, const char* pMap )
{
    m_TextureEnvMap.setup( gEnvMap );
    m_SpriteAtlas.setup(gSpritesAtlas);
    m_pMap = pMap;

    
    //
    // Compute the right scale for the pieces base on resolution
    //
    eng_GetCurrentContext().GetScreenResolution( m_XRes, m_YRes );

    m_ResScale = x_Max( m_XRes/768.f, m_YRes/1024.f );
    
    if( m_ResScale * 768 > m_XRes )
    {
        m_ResScale = m_XRes/768.f;
    }
    
    //--------------------------------------------------------------------------
    // AI
    //--------------------------------------------------------------------------
    
    //
    // Initialize the board
    //
    m_MatchList.m_nMatches  = 0;
    m_MoveList.m_nMoves     = 0;
    m_M3Board.Init( s_Seed );
    
    m_M3Board.setBoardLayer( m_XOffset, m_nPiecesHeight, m_pMap );
    
    m_XOffset = (SCREEN_WIDTH - ((m_XOffset-1) * TILES_XSIZE))/2;
    m_YOffset = ( m_nPiecesHeight + 1 ) * TILES_YSIZE;
    m_YOffset = ( m_YRes / m_ResScale - m_YOffset );
    
    //
    // OK all initialize lets start it up
    //
    m_M3Board.StartItUp();
    
    //
    // Lets collect all renderable pieces
    //
    CollectAllPieces();
    
    //
    // AI think about the moves
    //
    m_AI.ThinkAboutTheMoves( m_M3Board );
}

//-------------------------------------------------------------------------------

void pieces_entity::vSetup( void )
{
    //
    // Allocate a bugger with the max number of vertices
    //
    m_CPUVertices.Alloc( VERTEX_BUFFER_COUNT );
    
}

//-------------------------------------------------------------------------------

void pieces_entity::AdvanceRendering( f32 DeltaTime )
{
    const eng_sprite_rsc*   pSpriteAtlas = m_SpriteAtlas.getAsset();
    
    if( pSpriteAtlas == NULL )
    {
        eng_GetCurrentContext().qtAddRenderJob( *this );
        return;
    }

    //
    // Update the time for the renderer
    //
    m_Time += DeltaTime;
    
    //
    // Compute Matrix
    //
    eng_GetCurrentContext().GetScreenResolution( m_XRes, m_YRes );
    
    m_L2C.Identity();
    
    m_L2C.setTranslation( xvector3( -1, 1, 0 ) );
    m_L2C.setScale      ( xvector3( 2.0f/m_XRes, -2.0f/m_YRes, 1 ) );
    
    //
    // Create some vertices
    //
    draw_vertex* pVertex = &m_CPUVertices[0];
    m_iVertex = 0;
    
    //
    // This are the possible color for the pieces
    //
    static const xcolor PieceColors[] =
    {
        xcolor( 255, 255, 0, 200 ),         // 1 Yellow
        xcolor( 255, 0, 0, 200 ),           // 2 Red
        xcolor( 0, 255, 0, 200 ),           // 3 Green
        xcolor( 0, 0, 255, 200 ),           // 4 Blue
        xcolor( 255, 100, 0, 200 ),         // 5 Orange
        xcolor( 255, 0, 255, 200 )          // 6 Pink
    };
    
    //
    // Compute the final vertex positions
    //
    for( s32 i=0; i<m_lPieces.getCount(); i++ )
    {
        const piece& Piece = m_lPieces[i];
        
        eng_sprite& Sprite = m_SpriteList[ Piece.m_iColor ];
        
        Sprite.Resolve();
        
        const eng_sprite_rsc::info& SpriteInfo  = Sprite.getSpriteInfo( *pSpriteAtlas );
        
        for( s32 j=0; j<4; j++ )
        {
            f32 ExtraY=0;
            f32 OtherYScale=1;
            if( Piece.m_tBounce )
            {
                OtherYScale += x_Cos( Piece.m_tBounce)*0.005f*Piece.m_tBounce;
                ExtraY = x_Cos( Piece.m_tBounce *0.5f) *0.5f*Piece.m_tBounce;
                ExtraY = x_Abs(ExtraY);
            }

            pVertex[m_iVertex].setup(  xvector3d( (SpriteInfo.m_Vertex[j].m_Pos.m_X * Piece.m_tScale*OtherYScale) + Piece.m_Pos.m_X ,
                                                  (SpriteInfo.m_Vertex[j].m_Pos.m_Y * Piece.m_tScale) + Piece.m_Pos.m_Y+ExtraY,
                                                   0 ),
                                       SpriteInfo.m_Vertex[j].m_UV.m_X,
                                       SpriteInfo.m_Vertex[j].m_UV.m_Y,
                                       PieceColors[ Piece.m_iColor ] );
            
            pVertex[m_iVertex].m_X *= m_ResScale;
            pVertex[m_iVertex].m_Y *= m_ResScale;
            
            m_iVertex++;
        }
    }
    
    //
    // Add a final rectangle for the hold screen used in the last pass
    //
    pVertex[ m_iVertex++ ].setup( 0.f,      0.f,    0.f, 0.f, 0.f, xcolor( ~0 ) );
    pVertex[ m_iVertex++ ].setup( m_XRes,   0.f,    0.f, 1.f, 0.f, xcolor( ~0 ) );
    pVertex[ m_iVertex++ ].setup( m_XRes,   m_YRes, 0.f, 1.f, 1.f, xcolor( ~0 ) );
    pVertex[ m_iVertex++ ].setup( 0.f,      m_YRes, 0.f, 0.f, 1.f, xcolor( ~0 ) );
    
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-------------------------------------------------------------------------------

void pieces_entity::ResetTheBoard( void )
{
    m_M3Board.ResetBoard();
    CollectAllPieces();
}

//-------------------------------------------------------------------------------

void pieces_entity::AdvanceAI( f32 DeltaTime )
{
    const f32 TweakTime = 0.4f;
    
    // Make sure to reset all our moves
    m_HistoricalMoves.DeleteAllNodes();

    if ( m_bStillMoving )
        return;

    xbool IsStable = m_M3Board.ComputeStep( &m_HistoricalMoves );
    if( IsStable == FALSE ) m_bStable = FALSE;
    
    if( m_AITime < 2*TweakTime )
        return;

    if( IsStable && m_HistoricalMoves.getCount() == 0 )
    {
        if( m_bStable == FALSE && m_bStillMoving == FALSE )
        {
            m_M3Board.FindMatches( m_MatchList );
            if( m_MatchList.m_nMatches )
            {
                m_M3Board.SolveMatches( m_MatchList, &m_HistoricalMoves );
                m_AITime = 0;
            }
            else
            {
                m_bStable = TRUE;
            }
        }
        else if( m_AITime > 4*TweakTime && m_bStillMoving == FALSE )
        {
            m_M3Board.FindMoves( m_MoveList );
            if( m_MoveList.m_nMoves == 0 )
            {
                ResetTheBoard();
            }
            else
            {
                m_AI.getRecommendation( m_AIMoveIndex, m_AIMoveSeed );
                m_AI.MakeMove( m_MoveList.m_Moves[m_AIMoveIndex], m_AIMoveSeed );
                
                m_M3Board.UpdateWithMove( m_MoveList.m_Moves[m_AIMoveIndex], m_AIMoveSeed, &m_HistoricalMoves  );
            }
            
            // Reset the clock
            m_AITime  = 0;
            m_bStable = FALSE;
        }
        
    }
}

//-------------------------------------------------------------------------------

xvector3d pieces_entity::ConverFromMatchToRender ( s32 X, s32 Y ) const
{
    return xvector3d( m_XOffset +                      X  * TILES_XSIZE,
                      m_YOffset + ( m_nPiecesHeight - Y ) * TILES_YSIZE,
                      0 );
}

//-------------------------------------------------------------------------------

void pieces_entity::CollectAllPieces( void )
{
    // Clear all information about our pieces
    m_FromMatch3ToPiece.SetMemory(~0);
    m_lPieces.DeleteAllNodes();
    
    // Render each of the tiles
    for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
    for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
    {
        const match3_board::tile& Tile = m_M3Board.getTile( X, Y );
        
        // Skip any empty tiles
        if( Tile.m_iPiece < 0 )
            continue;
        
        const match3_board::piece&  Piece = m_M3Board.getPiece( Tile.m_iPiece );
        piece&                      RenderPiece = m_lPieces.append( m_FromMatch3ToPiece[Tile.m_iPiece] );
        
        // Copy the information over
        RenderPiece.m_Pos = ConverFromMatchToRender(X,Y);
        RenderPiece.m_iColor  = Piece.m_Color;
        ASSERT( RenderPiece.m_iColor  >= 0 );
        ASSERT( RenderPiece.m_iColor  < 6 );
    }
}

//-------------------------------------------------------------------------------

void pieces_entity::PlacePieces( f32 DeltaTime )
{
    s32 nMoves = m_HistoricalMoves.getCount();
    
    //
    // Initialize any possible moves
    //
    for( s32 i=0; i<nMoves; i++ )
    {
        const match3_board::historical_move&    HistoricalMove  = m_HistoricalMoves[i];
        xhandle&                                Handle          = m_FromMatch3ToPiece[ HistoricalMove.m_iPiece ];
        
        switch( HistoricalMove.m_Type )
        {
            case match3_board::historical_move::TYPE_DELETED:
            {
                ASSERT( HistoricalMove.m_iFromTile == HistoricalMove.m_iToTile );
                ASSERT( HistoricalMove.m_iFromTile == 0xff );
                
                m_lPieces.DeleteByHandle( Handle );
                Handle.SetNull();
                break;
            }
            case match3_board::historical_move::TYPE_SWAP:
            case match3_board::historical_move::TYPE_MOVE:
            {
                ASSERT( HistoricalMove.m_iFromTile != HistoricalMove.m_iToTile );
                
                piece& Piece = m_lPieces( Handle );
                
                // Get the destination Position
                s32 TileX, TileY;
                m_M3Board.getTileXYFromIndex( TileX, TileY, HistoricalMove.m_iToTile );
                
                Piece.m_DestPos = ConverFromMatchToRender( TileX, TileY );
                Piece.m_Velocity = 9;

                break;
            }
            case match3_board::historical_move::TYPE_SPAWN:
            {
                ASSERT( HistoricalMove.m_iFromTile == HistoricalMove.m_iToTile );
                
                piece&                      RenderPiece = Handle.IsNull()?m_lPieces.append( Handle ):m_lPieces( Handle );
                const match3_board::piece&  Piece       = m_M3Board.getPiece( HistoricalMove.m_iPiece );
                
                s32 X, Y;
                m_M3Board.getTileXYFromIndex( X, Y, HistoricalMove.m_iToTile );
                
                RenderPiece.m_tScale    = 0.5;
                RenderPiece.m_Pos       = ConverFromMatchToRender( X, Y );
                RenderPiece.m_Velocity  = 0;
                RenderPiece.m_iColor    = Piece.m_Color;
 
            }
            case match3_board::historical_move::TYPE_TELEPORTED:
            {
                break;
            }
            default:
                ASSERT(0);
        }
    }

    //
    // Update any possible already moving pieces
    //
    
    m_bStillMoving = FALSE;
    for( piece& Piece : m_lPieces )
    {
        if( Piece.m_Velocity )
        {
            f32 Acc = x_Min( Piece.m_Velocity*Piece.m_Velocity, 80 );
            Piece.m_Velocity += Acc*DeltaTime;
            xvector3 Dir;
            
            Dir = Piece.m_DestPos - Piece.m_Pos;
            
            f32 DistanceSquare = Dir.Dot( Dir );
            
            if ( DistanceSquare < Piece.m_Velocity*Piece.m_Velocity )
            {
                if(Piece.m_Pos.m_Y > Piece.m_DestPos.m_Y )
                    Piece.m_tBounce  = Piece.m_Velocity;
                
                Piece.m_Velocity = 0;
                Piece.m_Pos      = Piece.m_DestPos;
                Piece.m_DestPos.Zero();

                continue;
            }
            else
            {
                Dir *= x_InvSqrt( DistanceSquare );
                Piece.m_Pos += Dir * Piece.m_Velocity;
            }

            // if it is close enough lets just say we stop moving this piece
            // so that we can advance to the next movement if any
            if ( DistanceSquare < Piece.m_Velocity*Piece.m_Velocity * 2 )
            {
                continue;
            }

            m_bStillMoving = TRUE;
        }
        else if( Piece.m_tBounce )
        {
            Piece.m_tBounce -= DeltaTime*40;
            if( Piece.m_tBounce < 1.7 )
                Piece.m_tBounce = 0;
        }
        
        if( Piece.m_tScale < 1 )
        {
            Piece.m_tScale += Piece.m_tScale*Piece.m_tScale*DeltaTime*5;
            Piece.m_tScale = x_Min( 1, Piece.m_tScale );
        }
        
    }
    
    //
    // Sanity Check
    //
    xsafe_array<xbool,9*9> IndexedChecked = {0};
    s32                    nPieces = 0;
    for( s32 Y=0; Y<match3_board::MAX_BOARDSIZE; Y++ )
    for( s32 X=0; X<match3_board::MAX_BOARDSIZE; X++ )
    {
        const match3_board::tile& Tile = m_M3Board.getTile( X, Y );
        
        // Skip any empty tiles
        if( Tile.m_iPiece < 0 )
            continue;
        
        nPieces++;
        IndexedChecked[ Tile.m_iPiece ] = TRUE;
        
        const match3_board::piece&  Piece       = m_M3Board.getPiece( Tile.m_iPiece );
        piece&                      RenderPiece = m_lPieces( m_FromMatch3ToPiece[Tile.m_iPiece] );

        ASSERT( RenderPiece.m_iColor == Piece.m_Color );
        ASSERT( RenderPiece.m_iColor  >= 0 );
        ASSERT( RenderPiece.m_iColor  < 6 );
        
        xvector3 ExpectedPos = ConverFromMatchToRender( X, Y );

        if( RenderPiece.m_Velocity )
        {
            ASSERT( RenderPiece.m_DestPos == ExpectedPos );
        }
        else
        {
            ASSERT( RenderPiece.m_Pos == ExpectedPos );
        }
    }
    
    for( s32 i=0; i<IndexedChecked.getCount(); i++ )
    {
        if( IndexedChecked[i] ) ASSERT( m_FromMatch3ToPiece[i].IsValid() );
        else                    ASSERT( m_FromMatch3ToPiece[i].IsNull()  );
    }

    ASSERT( nPieces == m_lPieces.getCount() );
}

//-------------------------------------------------------------------------------

void pieces_entity::onUpdate( const gb_msg& msg )
{
    f32 Scalar = 1;//0.2;
    
    m_AITime += msg.m_DeltaTime * Scalar;
    
    AdvanceAI       ( msg.m_DeltaTime * Scalar);
    PlacePieces     ( msg.m_DeltaTime * Scalar );
    AdvanceRendering( msg.m_DeltaTime * Scalar );
}



