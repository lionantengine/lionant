//
//  projectAlpha.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "projectAlpha.h"
#include "gameStates.h"

///-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

project_alpha::input_device::input_device( void )
{
    m_ControllerID = 0;
    
    //
    // Set up the move controler
    //
    m_Logical[MOVE].m_pActionName = "MOVE";
    m_Logical[MOVE].m_IsValue.Zero();
    m_Logical[MOVE].m_WasValue = FALSE;
    
    m_Logical[PRESS].m_pActionName = "PRESS";
    m_Logical[PRESS].m_IsValue.Zero();
    m_Logical[PRESS].m_WasValue = FALSE;
    
    //
    // setup the devices mapping
    //
#ifdef TARGET_IOS
    m_Mappings[X_PLATFORM][0].m_ContextMask             = 0x1;
    m_Mappings[X_PLATFORM][0].m_pDevice                 = &m_TouchPad;
    m_Mappings[X_PLATFORM][0].m_bGadgetIsAnalog         = TRUE;
    m_Mappings[X_PLATFORM][0].m_GadgetID                = eng_touchpad::ANALOG_TOUCH_1_ABS;
    m_Mappings[X_PLATFORM][0].m_Scale                   = {1,1,1};
    m_Mappings[X_PLATFORM][0].m_iLogicalMapping         = 0;
    m_Mappings[X_PLATFORM][0].m_DimensionMode           = DIMENSION_XYZ_XYZ;
    
    m_Mappings[X_PLATFORM][1].m_ContextMask             = 0x1;
    m_Mappings[X_PLATFORM][1].m_pDevice                 = &m_TouchPad;
    m_Mappings[X_PLATFORM][1].m_bGadgetIsAnalog         = FALSE;
    m_Mappings[X_PLATFORM][1].m_GadgetID                = eng_touchpad::BTN_TOUCH_1;
    m_Mappings[X_PLATFORM][1].m_Scale                   = {1,1,1};
    m_Mappings[X_PLATFORM][1].m_iLogicalMapping         = 1;
    m_Mappings[X_PLATFORM][1].m_DimensionMode           = DIMENSION_XYZ_XYZ;

#elif defined TARGET_OSX || defined TARGET_PC
    m_Mappings[X_PLATFORM][0].m_ContextMask             = 0x1;
    m_Mappings[X_PLATFORM][0].m_pDevice                 = &m_Mouse;
    m_Mappings[X_PLATFORM][0].m_bGadgetIsAnalog         = TRUE;
    m_Mappings[X_PLATFORM][0].m_GadgetID                = eng_mouse::ANALOG_POS_ABS;
    m_Mappings[X_PLATFORM][0].m_Scale                   = {1,1,1};
    m_Mappings[X_PLATFORM][0].m_iLogicalMapping         = 0;
    m_Mappings[X_PLATFORM][0].m_DimensionMode           = DIMENSION_XYZ_XYZ;

    m_Mappings[X_PLATFORM][1].m_ContextMask             = 0x1;
    m_Mappings[X_PLATFORM][1].m_pDevice                 = &m_Mouse;
    m_Mappings[X_PLATFORM][1].m_bGadgetIsAnalog         = FALSE;
    m_Mappings[X_PLATFORM][1].m_GadgetID                = eng_mouse::BTN_0;
    m_Mappings[X_PLATFORM][1].m_Scale                   = {1,1,1};
    m_Mappings[X_PLATFORM][1].m_iLogicalMapping         = 1;
    m_Mappings[X_PLATFORM][1].m_DimensionMode           = DIMENSION_XYZ_XYZ;
#endif
    
    m_MappingCount[X_PLATFORM]  = 2;
    m_ActiveContexts            = 0x1;
    
    //
    // Initialize the input manager with our new device
    //
    g_InputMgr.setupAddVirtualDevice( *this );
}

/////////////////////////////////////////////////////////////////////////////////
// Main functions
/////////////////////////////////////////////////////////////////////////////////

static const int SCREEN_WIDTH   = 768;
static const int SCREEN_HEIGHT  = 1024;

//-------------------------------------------------------------------------------

void project_alpha::Initialize( void )
{
    xguid gMyBlueprint;
    xguid gMyBulletBlueprint;
    
    //----------------------------------------------------------------------------
    // Initialize the engine
    //----------------------------------------------------------------------------
    eng_Init();
    g_Scheduler.Init( 1 );
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.SetClearColor(xcolor(255,255,255,255));
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);
    
    //----------------------------------------------------------------------------
    // Init the game state
    //----------------------------------------------------------------------------
    m_GameStates.Initialize();
    
    //----------------------------------------------------------------------------
    // Initialize the game
    //----------------------------------------------------------------------------
    
    // TODO: This should be put in a nicer more intuitive function somewhere
    gb_base_mgr::InitAll();
    
    //
    // Get the game manager to warm up a bit
    //
    g_StateMgr.setActiveState( m_GameStates );
    g_GameMgr.msgStart();
}

