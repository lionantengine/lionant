//
//  BoardInput.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "PiecesInput.h"

//////////////////////////////////////////////////////////////////////////////////
// BOARD INPUT
//////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE( pieces_input,
              gb_component_type::FLAGS_REALTIME_DYNAMIC |
              gb_component_type::FLAGS_HAS_PACKED_DATA )




//-------------------------------------------------------------------------------

pieces_input::pieces_input( void )
{
}

//-------------------------------------------------------------------------------

void pieces_input::vUpdate( void )
{
    //
    // Let the parent update first
    //
    gb_input_component::vUpdate();
    
    //
    // Ok we update now
    //
    /*
    const presets&                      Presets = getPresets();
    const physics_component::context&   PhyContext = m_pPhysics->getReadContextTx(0);
    x_light_trigger                     Trig;
    x_light_job*                        pLightJob = NULL;
    xarray<physics_component*>          lComp;
    s32                                 State = INPUT_NONE;
    f32                                 DeltaTime = g_GameMgr.getDeltaTime();
    
    // Remind the network that we are the man
    if( m_pPhysics->getHandle().IsValid() )
    {
        g_NetworkMgr.UpdateOrigin( m_hNetworkPlayer, m_pPhysics->getReadContextTx(0).m_State.m_Transform.m_Position );
        g_NetworkMgr.UpdateData( *m_pPhysics, 100000, xvector3(0,0,0), 10000 );
    }
    
    // Dampening of the force
    m_Force *= xvector3( 0.96f * DeltaTime );
    
    // Base on the input compute the new force
    for( s32 i=0; i<m_lLogical.getCount(); i++)
    {
        const logical& Logical = m_lLogical[i];
        
        switch( Logical.m_ID )
        {
            case INPUT_FOWARD:
                if( Logical.m_IsValue)
                    m_Force.m_Z += Presets.m_ApplyForce * DeltaTime;
                break;
            case INPUT_LEFT:
                if( Logical.m_IsValue)
                    m_Force.m_X += Presets.m_ApplyForce * DeltaTime;
                break;
            case INPUT_RIGHT:
                if( Logical.m_IsValue)
                    m_Force.m_X -= Presets.m_ApplyForce * DeltaTime;
                break;
            case INPUT_BACKWARDS:
                if( Logical.m_IsValue)
                    m_Force.m_Z -= Presets.m_ApplyForce * DeltaTime;
                break;
            case INPUT_EXPLOSION:
            case INPUT_IMPLOSION:
                if( Logical.m_WasValue && pLightJob == NULL )
                {
                    pLightJob = g_PhysicsMgr.ClosestComponents( lComp, PhyContext.m_State.m_Transform.m_Position, Presets.m_ExplosionDistance, 0 );
                    
                    // Remember we are dealing with
                    State = Logical.m_ID;
                    
                    // Make the job depend on our trigger
                    pLightJob->AndThenRuns(Trig);
                    
                    // Start processing the job
                    g_Scheduler.StartLightJobChain( *pLightJob );
                }
                break;
            case INPUT_KATAMARI:
                if( Logical.m_IsValue && pLightJob == NULL )
                {
                    pLightJob = g_PhysicsMgr.ClosestComponents( lComp, PhyContext.m_State.m_Transform.m_Position, Presets.m_KatamariDistance, 0 );
                    
                    // Remember we are dealing with the explosion
                    State = Logical.m_ID;
                    
                    // Make the job depend on our trigger
                    pLightJob->AndThenRuns(Trig);
                    
                    // Start processing the job
                    g_Scheduler.StartLightJobChain( *pLightJob );
                }
                break;
            default: ASSERT(0);
        }
    }
    
    //
    // Deal with the cube movement
    //
    if( m_Force.Dot( m_Force ) > 0 )
    {
        xvector3 Axis  = xvector3(0,1,0).Cross( m_Force );
        f32      Speed = m_Force.GetLength();
        
        Axis.NormalizeSafe();
        
        // Clamp the force
        if( Speed > Presets.m_ClampForce )
        {
            Speed = Presets.m_ClampForce;
            m_Force.NormalizeSafe();
            m_Force *= Speed;
        }
        
        xvector3 Torque( Axis*Speed );
        
        // Apply the new force to the physics component
        m_pPhysics->msgApplyForce( m_Force, 0 );
        
        // Make it roll as well
        m_pPhysics->msgApplyTorque( Torque, 0 );
    }
    
    //
    // Deal with explosion
    //
    if( pLightJob && ( State == INPUT_EXPLOSION || State == INPUT_IMPLOSION ))
    {
        // First lets sync
        Trig.LocalSync();
        pLightJob = NULL;
        
        // Now we should have all the object
        for( s32 i=0; i<lComp.getCount(); i++ )
        {
            physics_component& Comp = *lComp[i];
            
            // Dont need to apply the explosion to the player
            if( &Comp == m_pPhysics )
            {
                // Just wake it up
                m_pPhysics->msgApplyForce( xvector3(0,0,0), 0 );
                continue;
            }
            
            const physics_component::context&   DotContext = Comp.getReadContextTx(0);
            xvector3                            V          = DotContext.m_State.m_Transform.m_Position - PhyContext.m_State.m_Transform.m_Position;
            f32                                 DSquare    = V.Dot( V ) / (Presets.m_ExplosionDistance*Presets.m_ExplosionDistance);
            f32                                 Magnitud   = (Presets.m_ExplosionForce/DSquare)*DeltaTime;
            
            if( Magnitud > 1000 ) Magnitud = 1000;
            V = V.NormalizeSafe() * Magnitud * Comp.getMass();
            if( State == INPUT_IMPLOSION ) V = -V;
            Comp.msgApplyForce( V, 0 );
        }
    }
    
    //
    // Deal with katamari
    //
    if( pLightJob && State == INPUT_KATAMARI )
    {
        // First lets sync
        Trig.LocalSync();
        pLightJob = NULL;
        
        // Now we should have all the object
        for( s32 i=0; i<lComp.getCount(); i++ )
        {
            physics_component& Comp = *lComp[i];
            
            // Dont need to apply the explosion to the player
            if( &Comp == m_pPhysics )
            {
                // Just wake it up
                m_pPhysics->msgApplyForce( xvector3(0,0,0), 0 );
                continue;
            }
            
            const physics_component::context&   DotContext = Comp.getReadContextTx(0);
            xvector3                            V          = DotContext.m_State.m_Transform.m_Position - PhyContext.m_State.m_Transform.m_Position;
            
            xvector3 V2 = V;
            
            V2.NormalizeSafe();
            V2 = DotContext.m_State.m_Transform.m_Position + V2 * m_pPhysics->getPresets().m_Scale;
            V = PhyContext.m_State.m_Transform.m_Position - V2;
            
            f32 DSquare    = V.Dot( V ) / (Presets.m_KatamariDistance*Presets.m_KatamariDistance);
            f32 Magnitud   = (Presets.m_KatamariForce/DSquare)*DeltaTime;
            
            V = V.NormalizeSafe() * Magnitud * Comp.getMass();
            Comp.msgApplyForce( V, 0 );
        }
    }
    
    //
    // Make sure that the job has been dealth with
    //
    ASSERT(pLightJob==NULL);
     */
}

//-------------------------------------------------------------------------------

void pieces_input::vResolveDependencies( void )
{
   // m_pPhysics = &physics_component::SafeCast( *(physics_component*)findInterface( physics_component::getRTTI() ) );
}

//-------------------------------------------------------------------------------

void pieces_input::vSaveComponent( xtextfile& File )
{
}

//-------------------------------------------------------------------------------

void pieces_input::vSetup( void )
{
    /*
    //
    // Setup all the keys
    //
    xhandle hKatamari   = setupNewLogical( "Katamari",     INPUT_KATAMARI );
    xhandle hExplosion  = setupNewLogical( "Explosion",    INPUT_EXPLOSION );
    xhandle hImplosion  = setupNewLogical( "Implosion",    INPUT_IMPLOSION );
    xhandle hFoward     = setupNewLogical( "FOWARD",       INPUT_FOWARD );
    xhandle hBackwards  = setupNewLogical( "BACKWARDS",    INPUT_BACKWARDS );
    xhandle hLeft       = setupNewLogical( "LEFT",         INPUT_LEFT );
    xhandle hRight      = setupNewLogical( "RIGHT",        INPUT_RIGHT );
    
    setupSimpleGadget( hKatamari,   0, INPUT_KBD_SPACE, TRUE, 1 );
    setupSimpleGadget( hExplosion,  0, INPUT_KBD_Q, TRUE, 1 );
    setupSimpleGadget( hImplosion,  0, INPUT_KBD_E, TRUE, 1 );
    setupSimpleGadget( hFoward,     0, INPUT_KBD_W, TRUE, 1 );
    setupSimpleGadget( hBackwards,  0, INPUT_KBD_S, TRUE, 1 );
    setupSimpleGadget( hLeft,       0, INPUT_KBD_A, TRUE, 1 );
    setupSimpleGadget( hRight,      0, INPUT_KBD_D, TRUE, 1 );
     */
    
}

//-------------------------------------------------------------------------------

void pieces_input::vLoadComponent( xtextfile& File )
{
    gb_input_component::vLoadComponent( File );
    
    //
    // Setup the component
    //
    vSetup();
    
    //
    // Register the player as the main client
    //
    //m_hNetworkPlayer = g_NetworkMgr.RegisterNetworkPlayer();
    //g_ReplayMgr.setupRecordNetworkPlayer( m_hNetworkPlayer );
}