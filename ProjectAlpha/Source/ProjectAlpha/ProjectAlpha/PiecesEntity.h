//
//  entityBoard.h
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef PIECES_ENTITY_H
#define PIECES_ENTITY_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////////////////////////////////////
#include "gb_base.h"
#include "match3_base.h"

/////////////////////////////////////////////////////////////////////////////////
// BOARD ENTITY
/////////////////////////////////////////////////////////////////////////////////

class pieces_entity :
    public gb_entity,
    public eng_renderjob
{
public:
    GB_COMPONENT_RTTI1_PRESETS( pieces_entity, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_16B )
    
                        pieces_entity               ( void );
    void                setup                       ( u64 gEnvMap, u64 gSpritesAtlas, const char* pMap );
    xvector3d           ConverFromMatchToRender     ( s32 X, s32 Y ) const;
    
    const match3_board& getMatch3Board              ( void ) const { return m_M3Board; }
    const char*         getMap                      ( void ) const { return m_pMap; }

public:
   
    enum consts:s32
    {
        TILES_XSIZE         = 72,
        TILES_YSIZE         = 80,
        SCREEN_WIDTH        = 768,
        SCREEN_HEIGHT       = 1024,
        GAUSSIAN_TEXSIZE    = 512,
        MAX_PIECES          = 200,
        INDEX_BUFFER_COUNT  = MAX_PIECES*6*2,
        VERTEX_BUFFER_COUNT = MAX_PIECES*4*2
    };
    
public:
    
    struct presets : public gb_entity::presets
    {
        virtual void    vLoad           ( xtextfile& File );
        virtual void    vSave           ( xtextfile& File );
    };
    
protected:
    
    struct piece
    {
        xvector3d   m_Pos;                  // Piece position
        f32         m_Velocity      = 0;    // If moving which velocity it has
        xvector3d   m_DestPos;              // Final Destination of the piece when moving
        s32         m_iColor;               // Piece color
        s32         m_PieceID;              // Piece ID
        f32         m_tBounce       = 0;    // Bound Effect
        f32         m_tScale        = 1;    // Scale Effect
    };

protected:
    
    virtual void                    vSetup                  ( void ) override;
    virtual void                    vResolveDependencies    ( void ) override;
    virtual void                    onUpdate                ( const gb_msg& msg ) override ;
    virtual void                    vSaveComponent          ( xtextfile& File ) override;
    virtual void                    vLoadComponent          ( xtextfile& File ) override;
    virtual void                    onRunRenderJob          ( void ) const;
            void                    onGraphicsInitialize    ( void ) ;
            void                    RenderPieces            ( const eng_texture* pEnv, const eng_texture* pSpriteAtlas ) const;
            void                    AdvanceRendering        ( f32 DeltaTime );
            void                    AdvanceAI               ( f32 DeltaTime );
            void                    PlacePieces             ( f32 DeltaTime );
            void                    ResetTheBoard           ( void );
            void                    CollectAllPieces        ( void );
            void                    RenderTheBoard          ( eng_draw& Draw ) const;

protected:
        
    eng_render_buffer                   m_RenderToBuffer;
    eng_texture_rsc::ref                m_TextureEnvMap;
    eng_sprite_rsc::ref                 m_SpriteAtlas;
    xsafe_array<eng_fshader,2>          m_FragmentShader;
    xsafe_array<eng_vshader,2>          m_VertexShader;
    eng_vertex_desc                     m_VertexDesc;
    xsafe_array<eng_shader_program,2>   m_ShaderProgram;
    xptr<draw_vertex>                   m_CPUVertices;
    eng_ibuffer                         m_IndexBuffer;
    eng_vbuffer                         m_VertexBuffer;
    eng_texture                         m_TextureFromBuffer;
    xsafe_array<eng_sprite,6>           m_SpriteList;
    xbool                               m_bInitGraphics     =   FALSE;
    xmatrix4                            m_L2C;
    s32                                 m_XRes              = 0;
    s32                                 m_YRes              = 0;
    f32                                 m_ResScale          = 0;
    s32                                 m_iVertex           = 0;
    f32                                 m_Time              = 0;
    s32                                 m_XOffset;
    s32                                 m_YOffset;
    s32                                 m_nPiecesHeight;

    xharray<piece>                        m_lPieces;
    xarray<match3_board::historical_move> m_HistoricalMoves;
    xsafe_array<xhandle,0xff>             m_FromMatch3ToPiece = {-1}; // This is an actual mapping for all the pieces
    xbool                                 m_bStillMoving    = FALSE;
    
    
    xbool                               m_bStable           = FALSE;
    f32                                 m_AITime            = 0;
    s32                                 m_AIMoveIndex       =-1;
    u32                                 m_AIMoveSeed        =-1;
    const char*                         m_pMap              = NULL;
    u32                                 m_Seed              = 429;
    match3_board::matchlist             m_MatchList;
    match3_board::movelist              m_MoveList;
    match3_board                        m_M3Board;
    match3_ai                           m_AI;
};


/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif
