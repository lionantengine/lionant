//
//  SplashScreenRender.cpp
//  ProjectAlpha
//
//  Created by Tomas Arce on 10/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "gb_Base.h"
#include "SplashScreenEntity.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
GB_CLASS_TYPE( splash_screen_entity, gb_component_type::FLAGS_DYNAMIC_MASK )

//-----------------------------------------------------------------------------------

void splash_screen_entity::onUpdate( const gb_msg& msg )
{
    const eng_texture*  pTexture = m_Logo.getAsset();
    
    //
    // Update time ones we have the texture log
    //
    if( pTexture )
    {
        // update the time
        if( ( (m_TimePast*m_TimePast) >= m_TimeToFade && m_Dir > 0 ) ||
            (  m_TimePast             <= 0            && m_Dir < 0 ) )
        {
            // Nothing to do...
        }
        else
        {
            m_TimePast += msg.m_DeltaTime * m_Dir;
        }
    }
    
    //
    // But always render something
    //
    eng_GetCurrentContext().qtAddRenderJob( *this );
}

//-----------------------------------------------------------------------------------

void splash_screen_entity::onRunRenderJob( void ) const
{
    const eng_texture*  pTexture = m_Logo.getAsset();
    eng_context&        Context  = eng_GetCurrentContext();
    const f32           Fade     = x_Range( (m_TimePast*m_TimePast)/m_TimeToFade, 0, 1 );
    xcolor              Color;
    
    Color.SetFromRGBA(1,1,1,Fade);
    
    s32 W, H;
    s32 LogoW=0, LogoH=0;
    Context.GetScreenResolution( W, H );
    
    eng_draw& Draw = Context.getDraw();
    
    // This is garanteed by the onUpdate function
    if( pTexture == NULL )
    {
        pTexture = &Draw.GetWhiteTexture();
    }
    else
    {
        LogoW = pTexture->getWidth();
        LogoH = pTexture->getHeight();
    }
    
    //
    // Render the logo
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LT         |
                ENG_DRAW_MODE_TEXTURE_ON    |
                ENG_DRAW_MODE_BLEND_ALPHA   |
                ENG_DRAW_MODE_ZBUFFER_OFF );
    
    Draw.SetTexture( Draw.GetWhiteTexture() );
    
    if( m_Dir < 0 )
        Draw.DrawTexturedRect( xrect( 0.f, 0.f, f32( W ), f32( H ) ), xrect( 0.f, 0.f, 1.f, 1.f ), Color );
    else
        Draw.DrawTexturedRect( xrect( 0.f, 0.f, f32( W ), f32( H ) ), xrect( 0.f, 0.f, 1.f, 1.f ), xcolor( ~0 ) );
    
    Draw.SetTexture( *pTexture );
    
    if( LogoW > (W-20) || LogoH > (H-20) )
    {
        f32 s1 = LogoW/(W-20.f);
        f32 s2 = LogoH/(H-20.f);
        
        f32 s = x_Max( s1, s2 );
        
        LogoW *= s;
        LogoH *= s;
    }
    
    s32 offsetX = (W - LogoW) / 2;
    s32 offsetY = (H - LogoH) / 2;
    
    LogoW += offsetX;
    LogoH += offsetY;
    
    Draw.DrawTexturedRect( xrect( f32(offsetX),f32(offsetY), f32(LogoW),f32(LogoH) ), xrect( 0.f,0.f,1.f,1.f), Color);
    
    Draw.End();
}