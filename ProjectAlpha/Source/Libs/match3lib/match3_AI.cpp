//
//  match3_AI.cpp
//  match3lib
//
//  Created by Tomas Arce on 8/22/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "match3_base.h"

//------------------------------------------------------------------------------

void match3_ai::thinker_job::RunAGame( s32 iMyIndex )
{
    // Increments
    m_pAI->m_TotalGamesRun.inc();
    
    // Get ready to start thinking
    const s32                   iMove       = iMyIndex % m_pAI->m_MoveList.m_nMoves;
    const match3_board::move&   Move        = m_pAI->m_MoveList.m_Moves[iMove];
    match3_board                BaseBoard   = m_pAI->m_BaseBoard;
    seed_score                  NewScore;
    
    //
    // Go to the future
    //
    
    // Explore this new Seed Number
    NewScore.m_Seed = m_Rnd.Rand32();
    
    // Update the board with this move
    BaseBoard.UpdateWithMove( Move, NewScore.m_Seed, NULL  );

    //
    // Do we have any matches?
    //
    match3_board::matchlist MatchList;
    
    BaseBoard.FindMatches( MatchList );
    
    f32 InitialMatches=0;
    for( s32 i=0; i<MatchList.m_nMatches; i++ )
    {
        const match3_board::match& Match = MatchList.m_Matches[i];
        
        InitialMatches += Match.m_L - 2;
        
        if( x_FlagIsOn( MatchList.m_Matches[i].m_Type, match3_board::match::TYPE_MIX ) )
        {
            i++;
            InitialMatches += MatchList.m_Matches[i].m_L-1;
        }
    }
    
    // Collect all the matches
    s32 nTotalMatches = 0;
    do
    {
        // Solve the matches
        BaseBoard.SolveMatches( MatchList, NULL );
        
        // stabilize the board
        while( BaseBoard.ComputeStep() == FALSE )
        {
            // Check if we should stop
            if( m_pAI->m_bStopWorkers )
                return;
        }
        
        // Do we have any matches?
        BaseBoard.FindMatches( MatchList );
        if( MatchList.m_nMatches == 0 )
            break;
        
        // Count total number of matches
        nTotalMatches += MatchList.m_nMatches;
        
    } while( 1 );
    
    // Determine how many moves do we have
    match3_board::movelist MoveList;
    BaseBoard.FindMoves( MoveList );
    
    //
    // Consider if what we have is better than what is already in the list
    //
    NewScore.m_Score =
        m_pAI->m_WeightMoves          * MoveList.m_nMoves +
        m_pAI->m_WeightAutoMatches    * nTotalMatches     +
        m_pAI->m_WeightInitialMatches * InitialMatches*InitialMatches;
    
    // Check if we need to set it
    match3_ai::seed_score* pQTSeedScore = &m_pAI->m_SeedScoreList[iMove];
    while( m_pAI->m_bStopWorkers == FALSE )
    {
        const match3_ai::seed_score LocalSeedScore = *pQTSeedScore;
        
        // Check if our local reality tell us to set it
        if( LocalSeedScore.m_Score >= NewScore.m_Score )
            break;
        
        // Set the new seed if we got a better score
        if( x_cas64( (u64*)pQTSeedScore, *(u64*)&LocalSeedScore, *(u64*)&NewScore ) )
            break;
    }
}

//------------------------------------------------------------------------------

void match3_ai::thinker_job::vReset( void )
{
    // Do base work to clean up the class (still in the qt world)
    x_base_job::vReset();
    
    // Done working so there is one less worker
    s32 Count = m_pAI->m_WorkersWorking.dec();
    
    // Start the next job if we need to keep thinking
    if( m_pAI->m_bStopWorkers == FALSE && Count == 0 )
    {
        // Let the worker system know that we are on the job
        m_pAI->m_WorkersWorking.inc();

        // OK lets run the next worker
        s32 iNextIndex = ( m_iMyIndex + 1 ) % m_pAI->m_Jobs.getCount();
        g_Scheduler.StartJobChain( m_pAI->m_Jobs[iNextIndex] );
    }    
}

//------------------------------------------------------------------------------

void match3_ai::thinker_job::onRun( void )
{
    // This is the standard clock for the workers
    m_iMyIndex = m_pAI->m_NextMove.inc() - 1;

    //
    // Can we think about something?
    //
    if( m_pAI->m_MoveList.m_nMoves > 0 )
    {
        // Lets try to find a better seed
        RunAGame( m_iMyIndex );
    }
}

//------------------------------------------------------------------------------

match3_ai::match3_ai( void )
{
    m_WeightInitialMatches  = 30;
    m_WeightMoves           = 10;
    m_WeightAutoMatches     = -1;
    
    for( thinker_job& Job : m_Jobs )
    {
        Job.m_pAI = this;
    }
}

//------------------------------------------------------------------------------

void match3_ai::StartTheJobs( void )
{
    // Make sure that this is the case
    while( m_WorkersWorking.get() > 0 ) m_bStopWorkers = TRUE;
    
    //
    // First lets find out what are our possible moves
    //
    m_BaseBoard.FindMoves( m_MoveList );
    if( m_MoveList.m_nMoves == 0 )
    {
        ASSERT( 0 );
        //Board.ResetBoard();
        return;
    }

    //
    // Prapare the jobs
    //
    for( thinker_job& Job : m_Jobs )
    {
        Job.m_Rnd.setSeed32( m_Random.Rand32() );
        Job.m_pAI = this;
    }

    // Reset all the job stuff
    m_NextMove.setup( 0 );
    m_bStopWorkers = FALSE;

    // Lets clean up all the scores
    for( s32 i=0; i<m_MoveList.m_nMoves; i++ )
    {
        seed_score& SeedScore = m_SeedScoreList[i];
        
        SeedScore.m_Seed  = 0;
        SeedScore.m_Score = 0;
    }

    //
    // Run a Job
    //
    ASSERT( m_WorkersWorking.get() == 0 );
    
    // Let the worker system know that we are on the job
    m_WorkersWorking.inc();

    // Lets start the next job
    g_Scheduler.StartJobChain( m_Jobs[m_NextMove.get()] );
}


//------------------------------------------------------------------------------

void match3_ai::ThinkAboutTheMoves( match3_board& Board )
{
    // Make sure we tell everyone to stop working
    m_bStopWorkers = TRUE;
    
    // Wait untill everyone stop working
    //TODO: This is a big sync we may consider removing this later
    while( m_WorkersWorking.get() > 0 );
    
    //
    // OK Lets copy the new board
    //
    m_BaseBoard = Board;

    //
    // Make sure the board is up to date
    //
    ThinkForward();
    
    //
    // Run the Jobs
    //
    StartTheJobs();
}

//------------------------------------------------------------------------------

void match3_ai::getRecommendation( s32& iMove, u32& Seed )
{
    //
    // Find the best score
    //
    iMove = 0;
    for( s32 i=1; i<m_MoveList.m_nMoves; i++ )
    {
        seed_score& SeedScore = m_SeedScoreList[i];
        
        if( SeedScore.m_Score > m_SeedScoreList[iMove].m_Score )
        {
            iMove = i;
        }
    }
    
    // Set the seed
    Seed = m_SeedScoreList[iMove].m_Seed;
}

//------------------------------------------------------------------------------

void match3_ai::ThinkForward( void )
{
    // stabilize the board
    while( m_BaseBoard.ComputeStep() == FALSE )
    {
    }
    
    // Collect all the matches
    match3_board::matchlist MatchList;
    do
    {
        // Do we have any matches?
        m_BaseBoard.FindMatches( MatchList );
        if( MatchList.m_nMatches == 0 )
            break;
        
        // Solve the matches
        m_BaseBoard.SolveMatches( MatchList, NULL );
        
        // stabilize the board
        while( m_BaseBoard.ComputeStep() == FALSE )
        {
        }
        
    } while( 1 );
    
    //
    // Determine all the moves
    //
    m_BaseBoard.FindMoves( m_MoveList );
    
    if( m_MoveList.m_nMoves == 0 )
    {
        m_BaseBoard.ResetBoard();
        ThinkForward();
    }
}

//------------------------------------------------------------------------------

void match3_ai::MakeMove( match3_board::move& Move, u32 Seed )
{
    // Not need to think any more
    m_bStopWorkers = TRUE;
    
    // Hard sync
    while( m_WorkersWorking.get() > 0 );
    
    //
    // Make our official move
    //
    m_BaseBoard.UpdateWithMove( Move, Seed, NULL );

    //
    // Solve the matches that this move created
    //
    match3_board::matchlist MatchList;
    m_BaseBoard.FindMatches( MatchList );
    ASSERT( MatchList.m_nMatches > 0 );
    m_BaseBoard.SolveMatches( MatchList, NULL );
    
    //
    // Start thinking about it right away
    //
    ThinkForward();
    
    // Start the jobs
    StartTheJobs();
}