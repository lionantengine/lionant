//
//  match3_board.h
//
//  Created by Tomas Arce on 8/12/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef MATCH3_BOARD_H
#define MATCH3_BOARD_H

class match3_board
{
public:
    
    enum
    {
        MAX_BOARDSIZE = 9,
    };
        
    struct piece
    {
        enum type:u8
        {
            TYPE_SIMPLE,
            TYPE_BULL,
            TYPE_OCTOPUS,
            TYPE_DRAGON,
            TYPE_PHOENIX,
            TYPE_DIAMOND_PRINCESS,
            TYPE_DIAMOND_ASSCHER,
            TYPE_DIAMOND_PEAR,
            TYPE_DIAMOND_HEART,
            TYPE_MAX
        };
        
        enum color:u8
        {
            COLOR_YELLOW,
            COLOR_RED,
            COLOR_GREEN,
            COLOR_BLUE,
            COLOR_ORANGE,
            COLOR_PURPLE,
            COLOR_NORMAL_RANGE,
            COLOR_RARE = COLOR_NORMAL_RANGE,        // This is the color of rare diamonds
            COLOR_MAX
        };
        
        type    m_Type;         // Type of piece
        color   m_Color;        // Color of the piece OR diamond shape
        s8      m_iBlocker;     // Index to the blocker
    };
    
    struct tile
    {
        enum type:u8
        {
            TYPE_BOUNDARY       = X_BIT(1), // '#'      0 - Not boundary
            TYPE_TELEPORT_IN    = X_BIT(2), // 'a-z'    0 - Not Teleporter (IN) This one used the TeleportOut
            TYPE_TELEPORT_OUT   = X_BIT(3), // 'A-Z'    0 - Not Teleporter (OUT)
            TYPE_SPAWN          = X_BIT(4), // '$'      0 - Not a spawning tile
            TYPE_ENEMY          = X_BIT(5), // '*'      0 - Frienly Territory
            TYPE_DIRTY          = X_BIT(6), // '1-3'    0 - Not dirty, id 1 then m_Param is the level of dirty
            TYPE_SHADOW         = X_BIT(7), //          0 - Not in shadow. Tiles in shadow can be fill by pieces falling diagonal
            TYPE_MAX
        };
        
        u8      m_Type;         // Mask of types for this tile
        s8      m_iPiece;       // Index of the piece is currently sitting in this tile
        u8      m_iTeleportOut; // Which is the Tile Index which this teleport exits
        u8      m_DirtCount;    // Count for things like the dirt
    };
    
    struct blocker
    {
        enum type:u8
        {
            TYPE_CAGE,
            TYPE_CYCLES,
            TYPE_INFECTION,
            TYPE_BRIGHT_LIGHT,
            TYPE_HOLDING,
            TYPE_DESTROYER,
            TYPE_MAX
        };
        
        type    m_Type;         // Type of blocker
        u8      m_Param;        // Depending of the blocker this will mean different things
    };

    struct blocker_stack
    {
        u8                                       m_StackCount;
        xsafe_array<blocker, blocker::TYPE_MAX>  m_Stack;
    };
    
    struct move
    {
        enum direction:u8
        {
            DIRECTION_UP,
            DIRECTION_DOWN,
            DIRECTION_LEFT,
            DIRECTION_RIGHT
        };
        
        u8          m_iTile;
        direction   m_Direction;
        u32         m_Score;
    };
    
    struct historical_move
    {
        enum type:u8
        {
            TYPE_SWAP,
            TYPE_SPAWN,
            TYPE_MOVE,
            TYPE_DELETED,
            TYPE_TELEPORTED
        };
        
        type        m_Type;
        u8          m_iFromTile;
        u8          m_iToTile;
        s8          m_iPiece;
    };
    
    struct match
    {
        enum types:u8
        {
            TYPE_H      = X_BIT(0),     // If not Horizontal then Vertical
            TYPE_MIX    = X_BIT(1)      // This piece is part of a V+H combo
        };
        
        u8 m_X;
        u8 m_Y;
        u8 m_L;
        u8 m_Type;
        static inline s32 Compare( const match* pA, const match* pB ) { if( pA->m_L > pB->m_L ) return -1; return pA->m_L < pB->m_L; }
    };
    
    struct movelist
    {
        enum
        {
            MAX_MOVES     = MAX_BOARDSIZE*MAX_BOARDSIZE*4
        };
      
        s32                             m_nMoves;
        xsafe_array<move, MAX_MOVES>    m_Moves;
    };
    
    struct matchlist
    {
        enum
        {
            MAX_MATCHES   = MAX_BOARDSIZE*MAX_BOARDSIZE
        };

        s32                            m_nMatches;
        xsafe_array<match,MAX_MATCHES> m_Matches;
    };
    
public:
    
    void            Init                        ( u32 RandSeed );
    void            setSeed                     ( u32 RandSeed ) { m_Random.setSeed32(RandSeed); }
    void            setBoardLayer               ( s32& MaxX, s32& MaxY, const char* pBoardLayer  );
    void            StartItUp                   ( xbool bFillPieces = TRUE );
    void            ResetBoard                  ( void );
    
    xbool           UpdateWithMove              ( const move& Move, const u32 Seed, xarray<historical_move>* pMoveList );

    
    xbool           isValidMove                 ( move& Move ) const;
    
    xbool           ComputeStep                 ( xarray<historical_move>* pMoveList = NULL );
    
    void            FindMatches                 ( matchlist& MatchList );
    s32             SolveMatches                ( const matchlist& MatchList, xarray<historical_move>* pMoveList );
    
    void            FindMoves                   ( movelist& Moves ) const;
    
    u8              getTileIndexFromXY          ( s32 X, s32 Y ) const;
    void            getTileXYFromIndex          ( s32& X, s32& Y, s32 Index ) const;
    const tile&     getTile                     ( s32 X, s32 Y ) const;
    tile&           getRWTile                   ( s32 X, s32 Y );
    const piece&    getPiece                    ( s8 Index ) const;
    u32             getSeed                     ( void ) const;
    s32             getPieceIndex               ( const piece& Piece ) const { return s32( &Piece - &m_Pieces[0]); }
    
protected:

    xbool           UpdateWithMove              ( const move& Move, xarray<historical_move>* pMoveList );

    void            ComputeShadowTiles          ( void );
    void            ComputeMoves                ( movelist& MoveList );
    
    void            SpawnPiece                  ( tile& Tile );
    void            SpawnPiece                  ( tile& Tile, xarray<historical_move>* pMoveList );
    void            MovePiece                   ( tile& Tile, s8 iToTile );
    void            MovePiece                   ( tile& Tile, s8 iToTile, xarray<historical_move>* pMoveList );
    void            DeletePiece                 ( tile& Tile );
    void            DeletePiece                 ( tile& Tile, xarray<historical_move>* pMoveList );
    void            SwapPiece                   ( tile& TileA, tile& TileB );
    
    xbool           HelperCheckMatchWithColor   ( const piece::color Color, const s32 iTile, const s32 T, const s32 Stride ) const;
    xbool           CheckIfPossiblePositiveMove ( const s32 iTile, const s32 S, const s32 T, const s32 Stride  ) const;
    
protected:
    
    xrandom_small                                              m_Random;
    s8                                                         m_iFreePieces;
    u32                                                        m_RandomSeed;
    xsafe_array<blocker_stack, MAX_BOARDSIZE*MAX_BOARDSIZE>    m_Blockers;
    xsafe_array<piece,         MAX_BOARDSIZE*MAX_BOARDSIZE>    m_Pieces;
    xsafe_array<tile,          MAX_BOARDSIZE*MAX_BOARDSIZE>    m_Tiles;
};

//===============================================================================
// INLINE
//===============================================================================


//-------------------------------------------------------------------------------
inline
xbool match3_board::UpdateWithMove( const move& Move, const u32 Seed, xarray<historical_move>* pMoveList )
{
    setSeed( Seed );
    return UpdateWithMove( Move, pMoveList );
}

//-------------------------------------------------------------------------------
inline
u32 match3_board::getSeed( void ) const
{
    return 0;//return m_Random.getSeed();
}

//-------------------------------------------------------------------------------
inline
const match3_board::piece& match3_board::getPiece( s8 Index ) const
{
    return m_Pieces[Index];
}

//-------------------------------------------------------------------------------
inline
void match3_board::getTileXYFromIndex  ( s32& X, s32& Y, s32 Index ) const
{
    ASSERT( Index >= 0 && Index < MAX_BOARDSIZE*MAX_BOARDSIZE );
    X = Index%MAX_BOARDSIZE;
    Y = Index/MAX_BOARDSIZE;
    
    ASSERT( X >= 0 );
    ASSERT( Y >= 0 );
    ASSERT( X < MAX_BOARDSIZE );
    ASSERT( Y < MAX_BOARDSIZE );
}

//-------------------------------------------------------------------------------
inline
u8 match3_board::getTileIndexFromXY( s32 X, s32 Y ) const
{
    ASSERT( X >= 0 && X < MAX_BOARDSIZE );
    ASSERT( Y >= 0 && Y < MAX_BOARDSIZE );
    
    s32 Index = X + Y*MAX_BOARDSIZE;
    
    return u8(Index);
}

//-------------------------------------------------------------------------------
inline
const match3_board::tile& match3_board::getTile( s32 X, s32 Y ) const
{
    return m_Tiles[ getTileIndexFromXY(X, Y) ];
}


//-------------------------------------------------------------------------------
inline
match3_board::tile& match3_board::getRWTile( s32 X, s32 Y )
{
    return m_Tiles[ getTileIndexFromXY(X, Y) ];
}

//===============================================================================
// END
//===============================================================================
#endif
