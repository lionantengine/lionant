//
//  match3_AI.h
//  match3lib
//
//  Created by Tomas Arce on 8/22/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

class match3_ai
{
public:
    
            match3_ai          ( void );
    void    ThinkAboutTheMoves ( match3_board& Board );
    void    MakeMove           ( match3_board::move& Move, u32 Seed );
    void    getRecommendation  ( s32& iMove, u32& Seed );
    s32     getTotalMovesMade  ( void ) const { return m_TotalGamesRun.get(); }
    
protected:
    
    X_MS_ALIGNMENT(16)
    struct seed_score
    {
        u32         m_Seed;
        f32         m_Score;
    }
    X_SN_ALIGNMENT(16);
    
    struct thinker_job : public x_base_job
    {
        virtual void vReset     ( void );
        virtual void onRun      ( void );
                void RunAGame   ( s32 iMyIndex );
        
        match3_ai*          m_pAI;
        xrandom_small       m_Rnd;
        s32                 m_iMyIndex;
    };
    
    typedef xsafe_array<seed_score, match3_board::movelist::MAX_MOVES> seed_score_list;
    
protected:
    
    void    StartTheJobs        ( void );
    void    ThinkForward        ( void );
    
protected:
    
    match3_board::movelist          m_MoveList;
    match3_board                    m_BaseBoard;
    xrandom_large                   m_Random;
    
    xsafe_array<thinker_job,8>      m_Jobs;
    x_qt_counter                    m_NextMove;
    x_qt_counter                    m_WorkersWorking;
    xbool                           m_bStopWorkers;
    seed_score_list                 m_SeedScoreList;
    
    f32                             m_WeightMoves;
    f32                             m_WeightAutoMatches;
    f32                             m_WeightInitialMatches;
    
    x_qt_counter                    m_TotalGamesRun;
    
protected:
    
    friend struct match3_ai::thinker_job;
};