//
//  match3_board.cpp
//
//  Created by Tomas Arce on 8/12/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "match3_base.h"

//------------------------------------------------------------------------------

void match3_board::Init( u32 RandomSeed )
{
    m_RandomSeed = RandomSeed;
    m_Random.setSeed32( m_RandomSeed );
    
    m_Blockers.SetMemory(0);
    m_Pieces.SetMemory(0);
    m_Tiles.SetMemory(0);

    // Mark all the tiles not to have any pieces attach to them
    for( s32 i=0 ; i<m_Tiles.getCount(); i++ )
    {
        m_Tiles[i].m_iPiece = -1;
    }
    
    // Create a link list of empty pieces
    m_iFreePieces = 0;
    for( s32 i=0; i<m_Pieces.getCount(); i++ )
    {
        m_Pieces[i].m_iBlocker = i+1;
    }
    m_Pieces[m_Pieces.getCount()-1].m_iBlocker = -1;
}

//------------------------------------------------------------------------------

void match3_board::StartItUp( xbool bFillPieces )
{
    //
    // This may need to be done dynamically if blockers changed
    //
    ComputeShadowTiles();
}

//------------------------------------------------------------------------------

void match3_board::ResetBoard( void )
{
    for( s32 i=0 ; i<m_Tiles.getCount(); i++ )
    {
        if( m_Tiles[i].m_iPiece >= 0 )
            DeletePiece( m_Tiles[i] );
    }
}

//------------------------------------------------------------------------------

void match3_board::SwapPiece( tile& TileA, tile& TileB )
{
    x_Swap( TileA.m_iPiece, TileB.m_iPiece );
}

//------------------------------------------------------------------------------

xbool match3_board::UpdateWithMove( const move& Move, xarray<match3_board::historical_move>* pMoveList )
{
    ASSERT( Move.m_iTile >= 0 );
    ASSERT( Move.m_iTile < MAX_BOARDSIZE*MAX_BOARDSIZE );
    
    s32 iSwapTile = Move.m_iTile;
    
    switch( Move.m_Direction )
    {
        case move::DIRECTION_UP:    iSwapTile -= MAX_BOARDSIZE; break;
        case move::DIRECTION_DOWN:  iSwapTile += MAX_BOARDSIZE; break;
        case move::DIRECTION_LEFT:  iSwapTile --; break;
        case move::DIRECTION_RIGHT: iSwapTile ++; break;
        default: ASSERT( 0 );
    }

    ASSERT( iSwapTile >= 0 );
    ASSERT( iSwapTile < MAX_BOARDSIZE*MAX_BOARDSIZE );

    SwapPiece( m_Tiles[Move.m_iTile], m_Tiles[iSwapTile] );
    
    if( pMoveList )
    {
        match3_board::historical_move& A = pMoveList->append();
        match3_board::historical_move& B = pMoveList->append();
        
        A.m_Type      = historical_move::TYPE_SWAP;
        A.m_iToTile   = iSwapTile;
        A.m_iFromTile = Move.m_iTile;
        A.m_iPiece    = m_Tiles[iSwapTile].m_iPiece;

        B.m_Type      = historical_move::TYPE_SWAP;
        B.m_iToTile   = Move.m_iTile;
        B.m_iFromTile = iSwapTile;
        B.m_iPiece    = m_Tiles[Move.m_iTile].m_iPiece;
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------

void match3_board::ComputeShadowTiles( void )
{
    for( s32 y=0; y<MAX_BOARDSIZE; ++y )
    for( s32 x=0; x<MAX_BOARDSIZE; ++x )
    {
        tile& Tile = m_Tiles[ getTileIndexFromXY( x, y ) ];
        
        // We mark boundaries as always in shadow
        if( x_FlagIsOn( Tile.m_Type, tile::TYPE_BOUNDARY ) )
        {
            x_FlagOn( Tile.m_Type, tile::TYPE_SHADOW );
            continue;
        }
        
        // The following are always assume to be always in the light
        if( x_FlagIsOn( Tile.m_Type, tile::TYPE_SPAWN | tile::TYPE_TELEPORT_OUT ) )
        {
            x_FlagOff( Tile.m_Type, tile::TYPE_SHADOW );
            continue;
        }
        
        // If this is the first lane there is not point to look farther
        if( y == 0 )
        {
            // Anything else at the top that is not light must be a shadow
            x_FlagOn( Tile.m_Type, tile::TYPE_SHADOW );
            continue;
        }
        
        // If the tile inmediately in top of us is in shadow then we are also in shadow
        tile& CheckTile = m_Tiles[ getTileIndexFromXY( x, y-1 ) ];
        
        if( x_FlagIsOn( CheckTile.m_Type, tile::TYPE_BOUNDARY ) )
        {
            x_FlagOn( Tile.m_Type, tile::TYPE_SHADOW );
            continue;
        }
        
        // Anything else must be in light
        x_FlagOff( Tile.m_Type, tile::TYPE_SHADOW );
    }
}

//------------------------------------------------------------------------------

void match3_board::setBoardLayer( s32& MaxX, s32& MaxY, const char* pBoardLayer )
{
    s32 x = 0;
    s32 y = 0;
    
    MaxX = 0;
    MaxY = 0;
    for( s32 i=0; pBoardLayer[i]; i++, x++ )
    {
        if( pBoardLayer[i] == '\n')
        {
            // If there are any other blanks left make sure that they are mark as boundary
            for( ;x<MAX_BOARDSIZE; x++)
            {
                // Mark clearly that it is a boundary tile
                x_FlagOn( m_Tiles[ x + y * MAX_BOARDSIZE ].m_Type, tile::TYPE_BOUNDARY );
                
                // Make this tile as unable to have pieces in it
                m_Tiles[ x + y * MAX_BOARDSIZE ].m_iPiece = -2;
            }
            x =-1;
            y++;
            continue;
        }
        //
        // Handle the rest of the board
        //
        const s32 Index = x + y * MAX_BOARDSIZE;
        tile&     Tile  = m_Tiles[ Index ];
        
        ASSERT( x < MAX_BOARDSIZE );
        ASSERT( y < MAX_BOARDSIZE );
        
        if( pBoardLayer[i] == '#' )
        {
            // Mark clearly that it is a boundary tile
            x_FlagOn( Tile.m_Type, tile::TYPE_BOUNDARY );
            
            // Make this tile as unable to have pieces in it
            Tile.m_iPiece = -2;
            
            continue;
        }
        
        // Find the bbox of the board
        MaxX = x_Max( MaxX, x+1 );
        MaxY = x_Max( MaxY, y+1 );
        
        if( pBoardLayer[i] == '$' ){ x_FlagOn( Tile.m_Type, tile::TYPE_SPAWN ); }
        else if( pBoardLayer[i] == '1' ){ x_FlagOn( Tile.m_Type, tile::TYPE_DIRTY ); Tile.m_DirtCount = 1; }
        else if( pBoardLayer[i] == '2' ){ x_FlagOn( Tile.m_Type, tile::TYPE_DIRTY ); Tile.m_DirtCount = 2; }
        else if( pBoardLayer[i] == '3' ){ x_FlagOn( Tile.m_Type, tile::TYPE_DIRTY ); Tile.m_DirtCount = 3; }
        else if( pBoardLayer[i] >= 'a' && pBoardLayer[i] <= 'z'  )
        {
            // It is not possible to have two teleporters in the same tile
            ASSERT( x_FlagIsOn( Tile.m_Type, tile::TYPE_TELEPORT_IN ) == FALSE );
            
            xbool bOutTeleporterFound = FALSE;
            
            // Find the out teleporter
            s32 xx = 0;
            s32 yy = 0;
            for( s32 j=0; pBoardLayer[j]; j++, xx++ )
            {
                if( pBoardLayer[j] == '\n')
                {
                    xx=-1;
                    yy++;
                    continue;    
                }
                
                // Skip current tile
                if( i == j ) continue;
                
                const s32 iOut = xx + yy * MAX_BOARDSIZE;
                
                // We dont want to see the same lable of that teleporter again
                ASSERT( pBoardLayer[i] != pBoardLayer[j] );
                
                // We found the exit for this teleporter
                if( pBoardLayer[i] == x_tolower( pBoardLayer[j] ) )
                {
                    // Should only be one out for any given teleporter
                    ASSERT( bOutTeleporterFound== FALSE );
                    
                    x_FlagOn( Tile.m_Type, tile::TYPE_TELEPORT_IN );
                    x_FlagOn( m_Tiles[iOut].m_Type, tile::TYPE_TELEPORT_OUT );
                    Tile.m_iTeleportOut = iOut;
                    
                    bOutTeleporterFound = TRUE;
                }
            }
            
            // We did not found the out teleporter
            ASSERT( bOutTeleporterFound );
        }
    }
    
    //
    // Fill any additional information as a boundary
    //
    for( ;y<MAX_BOARDSIZE; y++)
    {
        // Resume where the last x was
        for( s32 xx = x; xx<MAX_BOARDSIZE; xx++ )
        {
            const s32 Index = xx + y * MAX_BOARDSIZE;
            tile&     Tile  = m_Tiles[ Index ];
            
            // Mark clearly that it is a boundary tile
            x_FlagOn( Tile.m_Type, tile::TYPE_BOUNDARY );
            
            // Make this tile as unable to have pieces in it
            Tile.m_iPiece = -2;
        }
        
        // In the future we will start from zero
        x = 0;
    }
}

//------------------------------------------------------------------------------

void match3_board::SpawnPiece( tile& Tile )
{
    // There should always be at least one empty piece
    ASSERT( m_iFreePieces >= 0 );

    // Pull one of the piece out of the free list
    Tile.m_iPiece = m_iFreePieces;
    m_iFreePieces = m_Pieces[Tile.m_iPiece].m_iBlocker;
    
    // Init the piece
    piece&      Piece  = m_Pieces[ Tile.m_iPiece ];
    
    Piece.m_Color    = piece::color(m_Random.Rand32(0, piece::COLOR_NORMAL_RANGE ));
    Piece.m_iBlocker = -1;
    Piece.m_Type     = piece::TYPE_SIMPLE;
}


//------------------------------------------------------------------------------

void match3_board::SpawnPiece( tile& Tile, xarray<historical_move>* pMoveList )
{
    SpawnPiece( Tile );
    
    // If we are require to record the new piece spawn lets do so
    if( pMoveList )
    {
        historical_move& HMove = pMoveList->append();
        HMove.m_Type        = historical_move::TYPE_SPAWN;
        HMove.m_iFromTile   = s8(&Tile - m_Tiles);
        HMove.m_iToTile     = HMove.m_iFromTile;
        HMove.m_iPiece      = Tile.m_iPiece;
    }
}

//------------------------------------------------------------------------------

void match3_board::MovePiece( tile& Tile, s8 iToTile )
{
    ASSERT( m_Tiles[iToTile].m_iPiece == -1 );
    
    // Move piece to the new tile
    m_Tiles[iToTile].m_iPiece = Tile.m_iPiece;
    
    // Remove the piece from the old tile
    Tile.m_iPiece = -1;
}

//------------------------------------------------------------------------------

void match3_board::MovePiece( tile& Tile, s8 iToTile, xarray<historical_move>* pMoveList )
{
    MovePiece( Tile, iToTile );
    if( pMoveList )
    {
        historical_move& HMove = pMoveList->append();
        HMove.m_Type        = historical_move::TYPE_MOVE;
        HMove.m_iPiece      = m_Tiles[iToTile].m_iPiece;
        HMove.m_iFromTile   = s8(&Tile - m_Tiles);
        HMove.m_iToTile     = iToTile;
    }
}

//------------------------------------------------------------------------------

void match3_board::DeletePiece( tile& Tile )
{
    ASSERT( Tile.m_iPiece >= 0 );
    
    // TODO: In the future we need to also free the blocker before we free the piece
    m_Pieces[ Tile.m_iPiece ].m_iBlocker = m_iFreePieces;
    m_iFreePieces = Tile.m_iPiece;
    
    // Out poor empty tile...
    Tile.m_iPiece = -1;
}

//------------------------------------------------------------------------------

void match3_board::DeletePiece( tile& Tile, xarray<historical_move>* pMoveList )
{
    const s32 iPiece = Tile.m_iPiece;
    
    DeletePiece( Tile );
    if( pMoveList )
    {
        historical_move& HMove = pMoveList->append();
        HMove.m_Type        = historical_move::TYPE_DELETED;
        HMove.m_iPiece      = iPiece;
        HMove.m_iFromTile   = 0xff;
        HMove.m_iToTile     = 0xff;
    }
}


//------------------------------------------------------------------------------

xbool match3_board::HelperCheckMatchWithColor( const piece::color Color, const s32 iTile, const s32 T, const s32 Stride ) const
{
    s32 Matches = 0;
    
    if( T >= 1 )
    {
        const tile&  TileA  = m_Tiles[ iTile - Stride*1 ];
        
        if( TileA.m_iPiece >= 0 )
        {
            const piece& PieceA = m_Pieces[ TileA.m_iPiece ];
            if( Color == PieceA.m_Color )
            {
                Matches++;
                
                if( T >= 2 )
                {
                    const tile&  TileB  = m_Tiles[ iTile - Stride*2 ];
                    
                    if( TileB.m_iPiece >= 0 )
                    {
                        const piece& PieceB = m_Pieces[ TileB.m_iPiece ];
                        
                        if( Color == PieceB.m_Color )
                        {
                            return TRUE;
                        }
                    }
                }
            }
        }
    }
    
    if( T < (MAX_BOARDSIZE-1) )
    {
        const tile&  TileA  = m_Tiles[ iTile + Stride*1 ];
        
        if( TileA.m_iPiece >= 0 )
        {
            const piece& PieceA = m_Pieces[ TileA.m_iPiece ];
            if( Color == PieceA.m_Color )
            {
                Matches++;
                if( Matches == 2 ) return TRUE;
                
                if( T < (MAX_BOARDSIZE-2) )
                {
                    const tile&  TileB  = m_Tiles[ iTile + Stride*2 ];
                    
                    if( TileB.m_iPiece >= 0 )
                    {
                        const piece& PieceB = m_Pieces[ TileB.m_iPiece ];
                        if( Color == PieceB.m_Color )
                        {
                            return TRUE;
                        }
                    }
                }
            }
        }
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------

xbool match3_board::CheckIfPossiblePositiveMove( const s32 iTile, const s32 S, const s32 T, const s32 Stride  ) const
{
    // Get the key tile
    const match3_board::tile& Tile  = m_Tiles[ iTile ];
    
    // If the tile does not have a piece then there is nothing to do
    if( Tile.m_iPiece < 0 )
        return FALSE;
    
    // Get the base piece
    const piece&        BasePiece = m_Pieces[ Tile.m_iPiece ];
    
    //
    // Swap with piece on the right
    //
    if( T >= (MAX_BOARDSIZE-1) )
        return FALSE;
    
    // Get the piece on the right
    const tile&  SwapTile  = m_Tiles[ iTile + 1 * Stride ];
    if( SwapTile.m_iPiece <0 )
        return FALSE;
    
    const piece& SwapPiece = m_Pieces[ m_Tiles[ iTile + 1 * Stride ].m_iPiece ];
    
    //
    // Check match on the right
    //
    if( T < (MAX_BOARDSIZE-3) )
    {
        const tile&  FarTileA  = m_Tiles[ iTile + 2 * Stride ];
        if( FarTileA.m_iPiece >= 0 )
        {
            const piece& FarPieceA = m_Pieces[ FarTileA.m_iPiece ];
            if( FarPieceA.m_Color == BasePiece.m_Color )
            {
                const tile&  FarTileB  = m_Tiles[ iTile + 3 * Stride ];
                
                if( FarTileB.m_iPiece >= 0 )
                {
                    const piece& FarPieceB = m_Pieces[ FarTileB.m_iPiece ];
                    if( FarPieceB.m_Color == BasePiece.m_Color )
                    {
                        return TRUE;
                    }
                }
            }
        }
    }
    
    //
    // Check match on the left
    //
    if( T >= 2 )
    {
        const tile&  FarTileA  = m_Tiles[ iTile - 1 * Stride ];
        
        if( FarTileA.m_iPiece >= 0 )
        {
            const piece& FarPieceA = m_Pieces[ FarTileA.m_iPiece ];
            
            if( FarPieceA.m_Color == SwapPiece.m_Color )
            {
                const tile&  FarTileB  = m_Tiles[ iTile - 2 * Stride ];
                
                if( FarTileB.m_iPiece >= 0 )
                {
                    const piece& FarPieceB = m_Pieces[ FarTileB.m_iPiece ];
                    if( FarPieceB.m_Color == SwapPiece.m_Color )
                    {
                        return TRUE;
                    }
                }
            }
        }
    }
    
    //
    // Check same coloumn as the tile in question
    //
    if( HelperCheckMatchWithColor( SwapPiece.m_Color, iTile, S, (MAX_BOARDSIZE+1) - Stride ) )
    {
        return TRUE;
    }
    
    if( HelperCheckMatchWithColor( BasePiece.m_Color, iTile + 1 * Stride, S, (MAX_BOARDSIZE+1) - Stride ) )
    {
        return TRUE;
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------

void match3_board::FindMoves( movelist& MoveList ) const
{
    MoveList.m_nMoves = 0;
    
    for( s32 Y=0; Y<MAX_BOARDSIZE; Y++ )
    for( s32 X=0; X<MAX_BOARDSIZE; X++ )
    {
        const s32           iTile = getTileIndexFromXY( X, Y );
        
        // Check if we can switch with the one on the right
        if( CheckIfPossiblePositiveMove( iTile, Y, X, 1 ) )
        {
            move& Move = MoveList.m_Moves[MoveList.m_nMoves++];
            Move.m_iTile = iTile;
            Move.m_Direction = move::DIRECTION_RIGHT;
        }

        // Check if we can switch with the one on below
        if( CheckIfPossiblePositiveMove( iTile, X, Y, MAX_BOARDSIZE ) )
        {
            move& Move = MoveList.m_Moves[MoveList.m_nMoves++];
            Move.m_iTile = iTile;
            Move.m_Direction = move::DIRECTION_DOWN;
        }
    }
}

//------------------------------------------------------------------------------

s32 match3_board::SolveMatches( const matchlist& MatchList, xarray<historical_move>* pMoveList  )
{
    for( s32 i=0; i<MatchList.m_nMatches; i++ )
    {
        const match& Match = MatchList.m_Matches[i];

        // Remove this piece
        if( x_FlagIsOn( Match.m_Type, match::TYPE_H ) )
        {
            s32 iTile = getTileIndexFromXY( Match.m_X, Match.m_Y );
            
            for( s32 j = 0; j<Match.m_L; j++ )
            {
                DeletePiece( m_Tiles[ iTile + j ], pMoveList );
            }
        }
        else
        {
            s32 iTile = getTileIndexFromXY( Match.m_X, Match.m_Y );
            
            for( s32 j = 0; j<Match.m_L; j++ )
            {
                if( x_FlagIsOn( Match.m_Type, match::TYPE_MIX ) )
                {
                    // For Mix type of pieces we may have errase one piece before
                    if( m_Tiles[ iTile + j * MAX_BOARDSIZE ].m_iPiece == -1 )
                        continue;
                }
                DeletePiece( m_Tiles[ iTile + j * MAX_BOARDSIZE ], pMoveList );
            }
        }
    }
    
    // TODO: In the near future we will score these
    return 0;
}

//------------------------------------------------------------------------------
static inline
void FilterFunction( xsafe_array<match3_board::match,match3_board::matchlist::MAX_MATCHES/2>& Maches,
                     s32 iOffset, s32& nMaches, const match3_board::match& TheMatch )
{
    const match3_board::match* pA = NULL;
    const match3_board::match* pB = NULL;
    
    if( x_FlagIsOn( TheMatch.m_Type, match3_board::match::TYPE_H ) ) pA = &TheMatch;
    else                                                             pB = &TheMatch;
    
    for( s32 i=iOffset; i<nMaches; i++ )
    {
        if( x_FlagIsOn( TheMatch.m_Type, match3_board::match::TYPE_H ) ) pB = &Maches[i];
        else                                                             pA = &Maches[i];
        
        // See if the overlap on the X direction
        const s32 MaxX = pA->m_X + pA->m_L;
        
        if( pB->m_X < pA->m_X )
            continue;
        
        if( pB->m_X >= MaxX )
            continue;
        
        // Seems to overlap on the Y Direction
        const s32 MaxY = pB->m_Y + pB->m_L;
        
        if( pA->m_Y < pB->m_Y )
            continue;
        
        if( pA->m_Y >= MaxY )
            continue;
        
        // We found a match
        // TODO: We should potencially split any other piece larger than 3...
        if( pB->m_L >= 3 )
        {
            nMaches--;
            for( s32 j=i; j<nMaches; j++ ) Maches[j] = Maches[j+1];
            i--;
        }
    }
}

//------------------------------------------------------------------------------

void match3_board::FindMatches( matchlist& FinalMatchList )
{
    s32                                         nHMaches=0;
    xsafe_array<match,matchlist::MAX_MATCHES/2> HMaches;
    
    s32                                         nVMaches=0;
    xsafe_array<match,matchlist::MAX_MATCHES/2> VMaches;
    
    FinalMatchList.m_nMatches = 0;
    
    //
    // Horizontal Matches
    //
    for( s32 Y=0; Y<MAX_BOARDSIZE; ++Y )
    {
        const s32   iTile   = getTileIndexFromXY( 0, Y );
        s32         X       = 0;
        
        //
        // Try to find a horizontal match
        //
        do
        {
            // Skip bad tiles
            for( ; X<MAX_BOARDSIZE; X++ )
            {
                if( m_Tiles[iTile + X].m_iPiece >= 0 )
                    break;
            }

            // Impossible to match anything now
            if( (MAX_BOARDSIZE - X) < 3 )
                break;
            
            const s32          iBase      = X++;
            const piece::color BaseColor  = m_Pieces[ m_Tiles[iTile+iBase].m_iPiece ].m_Color;
            
            // Try to count same color pieces
            while( X < MAX_BOARDSIZE && m_Tiles[iTile + X].m_iPiece >= 0 )
            {
                const s32           iPiece = m_Tiles[ iTile + X ].m_iPiece;
                const piece&        Piece  = m_Pieces[ iPiece ];
                
                if( Piece.m_Color != BaseColor )
                    break;
               
                X++;
            }
            
            // Do we have a match?
            s32 nMatches = (X - iBase);
            if( nMatches >= 3 )
            {
                match& Match = HMaches[nHMaches++];
            
                Match.m_X       = iBase;
                Match.m_Y       = Y;
                Match.m_L       = nMatches;
                Match.m_Type    = match::TYPE_H;        // Horizontal
            }
            
        } while( X < MAX_BOARDSIZE );
    }
    
    //
    // Vertical Matches
    //
    for( s32 X=0; X<MAX_BOARDSIZE; ++X )
    {
        const s32   iTile   = getTileIndexFromXY( X, 0 );
        s32         Y       = 0;
        
        //
        // Try to find a horizontal match
        //
        do
        {
            // Skip bad tiles
            for( ; Y<MAX_BOARDSIZE; Y++ )
            {
                if( m_Tiles[iTile + Y*MAX_BOARDSIZE].m_iPiece >= 0 )
                    break;
            }
            
            // Impossible to match anything now
            if( (MAX_BOARDSIZE - Y) < 3 )
                break;
            
            const s32           iBase      = Y++;
            const piece::color  BaseColor  = m_Pieces[ m_Tiles[ iTile+iBase*MAX_BOARDSIZE ].m_iPiece ].m_Color;
            
            // Try to count same color pieces
            while( Y < MAX_BOARDSIZE && m_Tiles[iTile + Y*MAX_BOARDSIZE].m_iPiece >= 0 )
            {
                const s32           iPiece = m_Tiles[ iTile + Y*MAX_BOARDSIZE ].m_iPiece;
                const piece&        Piece  = m_Pieces[ iPiece ];
                
                if( Piece.m_Color != BaseColor )
                    break;

                Y++;
            }
            
            // Do we have a match?
            s32 nMatches = (Y - iBase);
            if( nMatches >= 3 )
            {
                match& Match = VMaches[nVMaches++];
                
                Match.m_X       = X;
                Match.m_Y       = iBase;
                Match.m_L       = nMatches;
                Match.m_Type    = 0;            // Vertical
            }
            
        } while( Y < MAX_BOARDSIZE );
    }
    
    //
    // Short both list base on maximun length
    // This garatees that we are choosing the best matches for the player
    //
    x_qsort( &HMaches[0], nHMaches, sizeof(match), (x_compare_fn*)match::Compare );
    x_qsort( &VMaches[0], nVMaches, sizeof(match), (x_compare_fn*)match::Compare );
    
    //
    // Now lets find out how many of these matches overlap (V/H)
    //
    for( s32 h=0; h<nHMaches; h++ )
    {
        match& HMatch      = HMaches[h];
        s32    iMatch      = -1;
        
        for( s32 v=0; v<nVMaches; v++ )
        {
            const match& VMatch = VMaches[v];
            
            // See if the overlap on the X direction
            const s32 MaxX = HMatch.m_X + HMatch.m_L;
            
            if( VMatch.m_X < HMatch.m_X )
                continue;
            
            if( VMatch.m_X >= MaxX )
                continue;
            
            // Seems to overlap on the Y Direction
            const s32 MaxY = VMatch.m_Y + VMatch.m_L;
            
            if( HMatch.m_Y < VMatch.m_Y )
                continue;
            
            if( HMatch.m_Y >= MaxY )
                continue;
            
            // We found a match
            iMatch = v;
            break;
        }
        
        // Do we have a complex match?
        if( iMatch != -1 )
        {
            // We have a complex match for this one
            HMatch.m_Type          |= match::TYPE_MIX;
            VMaches[iMatch].m_Type |= match::TYPE_MIX;
            
            // Copy both candidates
            FinalMatchList.m_Matches[FinalMatchList.m_nMatches++] = HMatch;
            FinalMatchList.m_Matches[FinalMatchList.m_nMatches++] = VMaches[iMatch];
            
            //
            // Remove both matches from their list
            //
            nHMaches--;
            for( s32 j=h; j<nHMaches; j++ ) HMaches[j] = HMaches[j+1];
            
            nVMaches--;
            for( s32 j=iMatch; j<nVMaches; j++ ) VMaches[j] = VMaches[j+1];
            
            //
            // Any vertical ramaining maches which intersect our horizontal
            // match must be removed or split.
            //
            FilterFunction( VMaches, iMatch, nVMaches, FinalMatchList.m_Matches[FinalMatchList.m_nMatches-2] );
            FilterFunction( HMaches, h,      nHMaches, FinalMatchList.m_Matches[FinalMatchList.m_nMatches-1] );
            
            // Since we have deleted our current entry we will have to rewind h one step
            --h;
        }
    }

    //
    // Finally we copy any remaining HMatches
    //
    for( s32 h=0; h<nHMaches; h++ )
    {
        const match& HMatch = HMaches[h];
        FinalMatchList.m_Matches[FinalMatchList.m_nMatches++] = HMatch;
    }
    
    //
    // Finally we copy any remaining VMatches
    //
    for( s32 v=0; v<nVMaches; v++ )
    {
        const match& VMatch = VMaches[v];
        FinalMatchList.m_Matches[FinalMatchList.m_nMatches++] = VMatch;
    }
}

//------------------------------------------------------------------------------
struct just_teleported
{
    s8 m_iPiece;
    s8 m_iTile;
};

xbool match3_board::ComputeStep( xarray<historical_move>* pMoveList )
{
    s32                              nTeleportedPieces=0;
    xsafe_array<just_teleported, 32> TeleportedPieces;

    xbool bStable = TRUE;

    //
    // Try to move pieces base on gravity
    //
    for( s32 Y=MAX_BOARDSIZE-1; Y>=0; --Y )
    for( s32 X=0; X<MAX_BOARDSIZE; X++ )
    {
        const s32   iTile = getTileIndexFromXY( X, Y );
        tile&       Tile  = m_Tiles[iTile];
        
        //
        // Spawn a piece if there is nothing there
        //
        if( Tile.m_iPiece == -1 )
        {
            // We should spawn a piece if we dont have one and we are a spawner
            if( x_FlagIsOn( Tile.m_Type, tile::TYPE_SPAWN ) )
            {
                SpawnPiece( Tile, pMoveList );
                bStable = FALSE;
            }
            continue;
        }
     
        //
        // We can not do anything for boundary tiles
        //
        if( Tile.m_iPiece < 0 )
            continue;
        
        //
        // Check if this tile is ready to teleport pieces
        //
        if( x_FlagIsOn( Tile.m_Type, tile::TYPE_TELEPORT_IN ) )
        {
            tile& TileTo  = m_Tiles[ Tile.m_iTeleportOut ];
            
            if( TileTo.m_iPiece == -1 )
            {
                MovePiece( Tile, Tile.m_iTeleportOut, pMoveList );
                
                just_teleported& Teleported = TeleportedPieces[nTeleportedPieces++];
                
                Teleported.m_iPiece = m_Tiles[Tile.m_iTeleportOut].m_iPiece;
                Teleported.m_iTile  = Tile.m_iTeleportOut;
                
                m_Tiles[Tile.m_iTeleportOut].m_iPiece = -1;
                bStable = FALSE;
                continue;
            }
        }
    
        //
        // Check for any blockers
        //
        piece& Piece = m_Pieces[ Tile.m_iPiece ];
        
        if( Piece.m_iBlocker )
        {
            // If the piece is in the cage then it can not be teleported so move this up
            
        }
        
        //
        // Move the piece
        //
        if( Y < (MAX_BOARDSIZE-1) )
        {
            if( m_Tiles[iTile+MAX_BOARDSIZE].m_iPiece == -1 )
            {
                MovePiece( Tile, iTile+MAX_BOARDSIZE, pMoveList );
                bStable = FALSE;
            }
            else if( (X > 0) &&
                     m_Tiles[iTile+MAX_BOARDSIZE-1].m_iPiece == -1 &&
                     x_FlagIsOn( m_Tiles[iTile+MAX_BOARDSIZE-1].m_Type, tile::TYPE_SHADOW ) )
            {
                MovePiece( Tile, iTile+MAX_BOARDSIZE-1, pMoveList );
                bStable = FALSE;
            }
            else if( (X < MAX_BOARDSIZE-1) &&
                     m_Tiles[iTile+MAX_BOARDSIZE+1].m_iPiece == -1 &&
                     x_FlagIsOn( m_Tiles[iTile+MAX_BOARDSIZE+1].m_Type, tile::TYPE_SHADOW ))
            {
                MovePiece( Tile, iTile+MAX_BOARDSIZE+1, pMoveList );
                bStable = FALSE;
            }
        }
        
        //
        // Spawn a piece if there is nothing there
        //
        if( Tile.m_iPiece == -1 )
        {
            if( x_FlagIsOn( Tile.m_Type, tile::TYPE_SPAWN ) )
            {
                X--;
            }
        }
    }

    //
    // Check for any matches
    //
    for( s32 i=0; i<nTeleportedPieces; i++ )
    {
        just_teleported& Teleported = TeleportedPieces[i];
        m_Tiles[ Teleported.m_iTile ].m_iPiece = Teleported.m_iPiece;
    }
    
    return bStable;
 }
