//
//  main.cpp
//  TCPHTTP-Test
//
//  Created by Tomas Arce on 8/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"


xnet_tcp_server     m_Server;
xnet_tcp_client     m_Client;
xbool               m_bServer;


int main(int argc, const char * argv[])
{
    m_Client.OpenSocket( 4000 );
    
    s32 nRetries=10;
    while( FALSE == m_Client.ConnectToServer( xnet_address( xnet_socket::getLocalIP(), 3000 ) ) )
    {
        nRetries--;
        x_Sleep(100);
        if( nRetries<0)
        {
            ASSERT(0);
            break;
        }
    }
    
    x_Sleep( 600 );
    
    // Send a packet
    const char* SomeString =
    
"GET /upload HTTP/1.1\n"
"Referer: http://www.httprecipes.com/1/1/\n"
"Accept-Language: en-us\n"
"UA-CPU: x86\n"
"Accept-Encoding: gzip, deflate\n"
"User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)\n"
"Host: www.httprecipes.com\n"
"Connection: Keep-Alive\n"
"\n";

    
/*
        //"GET /path/index.html HTTP/1.0\n"
        "GET /upload HTTP/1.0\n"
        "From: someuser@jmarshall.com\n"
        "User-Agent: HTTPTool/1.0\n"
        "\n";
  */
    
    /*
        "POST /path/script.cgi HTTP/1.0\n"
        "From: frog@jmarshall.com\n"
        "User-Agent: HTTPTool/1.0\n"
        "Content-Type: application/x-www-form-urlencoded\n"
        "Content-Length: 32\n"
        "home=Cosby&favorite+flavor=flies\n";
    */
    
    
    
    
    xbool bSuccess = m_Client.Send( SomeString, x_strlen(SomeString)+1 );
    ASSERT( bSuccess );
    
    //
    // Deal with reciving packets
    //
    const s32       AllocSize   = 1024;
    xptr<xbyte>     xPacket;
    
    
    // Alloc the packet
    xPacket.Alloc( AllocSize );
    
    while( 1 )
    {
        s32             Size = AllocSize;
        
        while( m_Client.Recive( &xPacket[0], Size, TRUE ) )
        {
            if( Size )
            {
                x_printf("Client: I got this message: %s\n",&xPacket[0] );
            }
            
            // Get more packets
            Size   = AllocSize;
        }
        
        x_Sleep( 600 );
    }
    
    return 0;
}

