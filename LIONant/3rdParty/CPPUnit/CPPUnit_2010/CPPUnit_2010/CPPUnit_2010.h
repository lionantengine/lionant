//
// CTR - Library : CPPUnit_2010
//

#ifndef	__CPPUnit_2010_H__
#define	__CPPUnit_2010_H__

//
// global symbol declared.
//
extern int g_CPPUnit_2010;

//
// function prototypes.
//
void	CPPUnit_2010(void);

//
// class definition.
//
class CCPPUnit_2010
{
public:
	CCPPUnit_2010();
};

#endif	// __CPPUnit_2010_H__
