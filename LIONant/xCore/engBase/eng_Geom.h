//
//  engGeom.h
//  engBase
//
//  Created by Tomas Arce on 8/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef ENG_GEOM_H
#define ENG_GEOM_H

//=========================================================================
// ENG_GEOM
//=========================================================================
//
// A single "geom" can be made of multiple "meshes".
// This allows multiple objects to be stored in the same file.
// For example: all LOD levels, a single character object with different heads.
// 
// A "mesh" represents a complete object within a geom.
// For example: an LOD or a model of a head.
//    
// Each mesh is made from one or more "submeshs".
// There is a submesh for every material used by the mesh.
//

struct eng_geom_rsc : public eng_resource_base
{
public:

    enum
    {
        UID = 4,
    };
    
    struct geom_rsc;
    typedef eng_resource_ref<eng_geom_rsc, geom_rsc> ref;

    enum version_details
    {
        MAJOR_VERSION_NUMBER        = 1,
        MINOR_VERSION_NUMBER        = 0,

        SIZE_MESH                   = 64,
        SIZE_BONE                   = 24,
        SIZE_VERT_DESC              = 12,
        SIZE_TEMP_ISTREAM           = 16,
        SIZE_TEMP_VSTREAM           = 8*1 + 4*1 + 2*1 + 1*2,
        SIZE_CMD                    = 1*1 + 1*3 + 4*1,
        SIZE_STREAM_REF             = 5 * 4,
        SIZE_SUBMESH                = 24 + SIZE_STREAM_REF,
        SIZE_INFORMED_REFERENCE     = 8*1,
        SIZE_GEOM_RSC               = 8*8 + sizeof(xbbox) + 4*1 + 2*7 + 1*6,
        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) | ( MINOR_VERSION_NUMBER 
        ^ SIZE_MESH
        ^ SIZE_BONE
        ^ SIZE_SUBMESH
        ^ SIZE_TEMP_VSTREAM
        ^ SIZE_TEMP_ISTREAM
        ^ SIZE_CMD
        ^ SIZE_STREAM_REF
        ^ SIZE_INFORMED_REFERENCE )
    };

    //-------------------------------------------------------------------------

    struct bone
    {
        xbbox           m_BBox;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_BONE );
            SerialFile.Serialize( m_BBox.m_Min );
            SerialFile.Serialize( m_BBox.m_Max );
        };
    };

    struct mesh
    {
        xbbox                       m_BBox;
        u16                         m_nSubMeshs;
        u16                         m_iSubMesh;
        u16                         m_nBones;
        u16                         m_nFaces;
        xsafe_array<char, 32>       m_Name;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_MESH );
            SerialFile.Serialize( m_BBox.m_Min );
            SerialFile.Serialize( m_BBox.m_Max );
            SerialFile.Serialize( m_nSubMeshs );
            SerialFile.Serialize( m_iSubMesh );
            SerialFile.Serialize( m_nBones );
            SerialFile.Serialize( m_nFaces );
            SerialFile.Serialize( m_Name );
        }
    };

    struct streams_ref
    {
        u16         m_iVStream;
        u16         m_iIStream;
        s32         m_iVBase;
        s32         m_iIBase;
        s32         m_VCount;
        s32         m_ICount;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_STREAM_REF );
            SerialFile.Serialize( m_iVStream );
            SerialFile.Serialize( m_iIStream );
            SerialFile.Serialize( m_iVBase );
            SerialFile.Serialize( m_iIBase );
            SerialFile.Serialize( m_VCount );
            SerialFile.Serialize( m_ICount );
        }
    };

    enum cmd_type :u8
    {
        CMD_LOAD_MATRIX,                    
        CMD_LOAD_MATRICES,                    
        CMD_RENDER,                        // Arg0 = CacheIndex, Arg1 = SourceBoneIndex, 
                                            //        (ArgX1=cmd+1) = IndexOffset from ibase, 
                                            //        (ArgX2=cmd+2) = nIndices   
    };

    struct cmd
    {
        cmd_type    m_Type;              // Upper bit of the command means that it this structure is valid
        u8          m_Pad[3];

        union
        {
            u32     m_ArgX;

            struct // load matrix/matrices
            {
                u16         m_iSrcMatrix;
                u8          m_iMatrixCacheOffset;   //
                u8          m_nMatrices;            //
            };

            struct // render command
            {
                s32         m_IndexCount;
            };
        };

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_CMD );

            SerialFile.SerializeEnum( m_Type );

            switch ( m_Type )
            {
            case CMD_LOAD_MATRIX:
                SerialFile.Serialize( m_iSrcMatrix );
                SerialFile.Serialize( m_iMatrixCacheOffset );
                break;
            case CMD_LOAD_MATRICES:
                SerialFile.Serialize( m_iSrcMatrix );
                SerialFile.Serialize( m_iMatrixCacheOffset );
                SerialFile.Serialize( m_nMatrices );
                break;
            case CMD_RENDER:
                SerialFile.Serialize( m_IndexCount );
                break;
            default: ASSERT(0);
            }
        }
    };

    typedef eng_material_rsc::technique_type tech_type;
    struct submesh
    {
        streams_ref     m_StreamRef;            // Information about the vertex and index streams
        f32             m_WorldPixelSize;       // Average World Pixel size for this SubMesh
        s32             m_nFaces;               // Number of facets in this submesh
        s32             m_iColor;               // Index to a global list of colors
        u16             m_iInformed;            // Index of the Informed Material that this SubMesh uses
        u16             m_iCmd;                 // Command Index offset for this submesh
        u16             m_nCmds;                // Number of commands used to render, if zero then upload all and draw.
        u16             m_iMesh;                // Index to the mesh which this submesh belongs
        tech_type       m_TechniqueType;        // This is used to determine which technique it should use to render
        u8              m_nWeights;             // Number of weights for this submesh (from 0 to 4)
        u8              m_iInformedTech;        // index to the informed material reference to the material technique
        u8              m_Pad[1];

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_SUBMESH );
            SerialFile.Serialize( m_StreamRef );
            SerialFile.Serialize( m_WorldPixelSize );
            SerialFile.SerializeEnum( m_TechniqueType );
            SerialFile.Serialize( m_nFaces );
            SerialFile.Serialize( m_iColor );
            SerialFile.Serialize( m_iInformed );
            SerialFile.Serialize( m_iCmd );
            SerialFile.Serialize( m_nCmds );
            SerialFile.Serialize( m_iMesh );
            SerialFile.SerializeEnum( m_TechniqueType );
            SerialFile.Serialize( m_nWeights );
            SerialFile.Serialize( m_iInformedTech );
        }
    };

    struct v_pos_f32
    {
        f32     m_Position[ 3 ];
    };

    struct v_uv_f32
    {
        f32     m_UV[ 2 ];              // Full range
    };

    struct v_uv_u16
    {
        u16     m_UV[ 2 ];              // UVs Must fall in the range [0-1]
    };

    struct v_4_bone_index
    {
        u8     m_BoneIndex[ 4 ];        // to be decompress as intergers
    };

    struct v_4_bone_weights
    {
        u8    m_BoneWeights[ 4 ];       // to be decompress as floating points normalized
    };

    struct v_2_bone_and_weight          // to be decompress as floating point normalize
    {
        u8    m_BoneInfo[ 4 ];          // BoneIndex[0]=X, BoneWeight[0]=Y; BoneIndex[1]=Z; BoneWeight[1]=W
    };

    struct v_normal                     // 3*4 = 12 byte BTNs
    {
        s8     m_Normal[ 4 ];
    };

    struct v_color                     // 3*4 = 12 byte BTNs
    {
        s8     m_Color[ 4 ];
    };

    struct temp_vertex_stream
    {
        xserialfile::ptr<u8>            m_pVertices;
        s32                             m_nVertices;
        u16                             m_VertSize;
        u8                              m_Pad[2];
        
        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_TEMP_VSTREAM );
            SerialFile.Serialize( m_nVertices );
            SerialFile.Serialize( m_VertSize );
            SerialFile.Serialize( m_pVertices, m_VertSize*m_nVertices, xserialfile::FLAGS_TEMP_MEMORY );
        }
    };

    struct temp_index_stream
    {
        union
        {
            xserialfile::ptr<u16>       m_p16Indices;
            xserialfile::ptr<u32>       m_p32Indices;
        };

        s32                             m_nIndices;
        s32                             m_IndexSize;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_TEMP_ISTREAM );
            SerialFile.Serialize( m_IndexSize );
            SerialFile.Serialize( m_nIndices );

            if ( m_IndexSize == 2 )
                SerialFile.Serialize( m_p16Indices, m_nIndices, xserialfile::FLAGS_TEMP_MEMORY );
            else if ( m_IndexSize == 4 )
                SerialFile.Serialize( m_p32Indices, m_nIndices, xserialfile::FLAGS_TEMP_MEMORY );
            else
            {
                ASSERT( 0 );
            }
        }
    };

    //-------------------------------------------------------------------------
    
    struct informed_ref
    {
        u64     m_Guid;
        
        void    SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_INFORMED_REFERENCE );
            SerialFile.Serialize( m_Guid );
        }

        eng_informed_material_rsc::ref& getInformedRef( void ) { return *(eng_informed_material_rsc::ref*)&m_Guid; }
    };

    //-------------------------------------------------------------------------

    struct geom_rsc
    {
        geom_rsc( void ){}
        geom_rsc( xserialfile& SerialFile );

        void    SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.SetResourceVersion( VERSION );
            ASSERTCT( sizeof( *this ) == SIZE_GEOM_RSC );

            SerialFile.Serialize( m_BBox.m_Min );
            SerialFile.Serialize( m_BBox.m_Max );

            SerialFile.Serialize( m_nFaces );

            SerialFile.Serialize( m_nBones );
            SerialFile.Serialize( m_pBone, m_nBones );

            SerialFile.Serialize( m_nMeshes );
            SerialFile.Serialize( m_pMesh, m_nMeshes );

            SerialFile.Serialize( m_nSubMeshs );
            SerialFile.Serialize( m_pSubMesh, m_nSubMeshs );

            SerialFile.Serialize( m_nVertexStreams );
            SerialFile.Serialize( m_pTempVertexStream, m_nVertexStreams, xserialfile::FLAGS_TEMP_MEMORY );
            SerialFile.Serialize( m_pVertDesc, m_nVertexStreams );

            SerialFile.Serialize( m_nIndexStreams );
            SerialFile.Serialize( m_pTempIndexStream, m_nIndexStreams, xserialfile::FLAGS_TEMP_MEMORY );

            SerialFile.Serialize( m_nCmds );
            SerialFile.Serialize( m_pCmds, m_nCmds );

            SerialFile.Serialize( m_nInformeds );
            SerialFile.Serialize( m_pInformed, m_nInformeds );
        }

        struct runtime_vertex_buffer
        {
            eng_vbuffer                         m_VBuffer;
        };

        xserialfile::ptr<bone>                  m_pBone;
        xserialfile::ptr<mesh>                  m_pMesh;
        xserialfile::ptr<submesh>               m_pSubMesh;
        xserialfile::ptr<cmd>                   m_pCmds;
        xserialfile::ptr<eng_vertex_desc>       m_pVertDesc;
        xserialfile::ptr<informed_ref>          m_pInformed;
        
        union
        {
            xserialfile::ptr<temp_vertex_stream>    m_pTempVertexStream;
            runtime_vertex_buffer*                  m_pVertexBuffer;            // Runtime structures
        };

        union
        {
            xserialfile::ptr<temp_index_stream>     m_pTempIndexStream;
            eng_ibuffer*                            m_pIndexBuffer;             // Runtime structures
        };

        xbbox                                   m_BBox;
        s32                                     m_nFaces;       // including all meshes/lods
        u16                                     m_nBones;
        u16                                     m_nMeshes;
        u16                                     m_nSubMeshs;
        u16                                     m_nCmds;
        u16                                     m_nVertexStreams;
        u16                                     m_nIndexStreams;
        u16                                     m_nInformeds;
        u8                                      m_Pad[6];
    };

public:

                               ~eng_geom_rsc ( void );
    static eng_resource_type&   getStaticType( void );

protected:

    geom_rsc*                       m_pGeomRSC  =   NULL;

protected:

    friend class eng_geom;
};

//=============================================================================
// RIGID GEOMETRY
//=============================================================================
// Great aticle from Apple about how to deal with vertex buffers and such
// https://developer.apple.com/library/ios/documentation/3DDrawing/Conceptual/OpenGLES_ProgrammingGuide/TechniquesforWorkingwithVertexData/TechniquesforWorkingwithVertexData.html#//apple_ref/doc/uid/TP40008793-CH107-SW4


class eng_geom
{
public:

    void                            setup       ( const eng_geom_rsc::ref& GeomRef );
    void                            RenderGeom  ( const xmatrix4& L2C, const xvector3d& LocalSpaceLightDir );
    void                            RenderGeom  ( const xmatrix4* pL2C, const xvector3d* pLocalSpaceLightDir, s32 nBones );
    const eng_geom_rsc::geom_rsc*   Resolve     ( void );

protected:

    eng_geom_rsc::ref       m_GeomRscRef;
};

#endif
