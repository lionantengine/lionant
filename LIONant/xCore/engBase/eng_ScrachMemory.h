//
//  eng_SrachMemory.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef ENG_SCRACH_MEMORY_H
#define ENG_SCRACH_MEMORY_H

//===============================================================================
// INCLUDES
//===============================================================================

#include "x_base.h"

//===============================================================================
// FUNCTIONS
//===============================================================================
//  
//              +---Buffer Start                       Stack Start---+
//              |                                                    |
//              |                   Stacks Instances []-----+--+--+  |
//              |                                           |  |  |  |
//              V                                       ... v  v  v  V
//  Storage---> +----------------------------------------------------+
//              |                                                    |
//              |    Buffer -->                         <-- Stack    |
//              |                                                    |
//              +----------------------------------------------------+
//  
//===============================================================================

class eng_scrach_memory
{
public:

    enum
    {
        DEFAULT_STACK_SIZE      = 1024*4,
    };

    //-------------------------------------------------------------------------------
    class xscope_stack
    {
    public:

                    xscope_stack        ( eng_scrach_memory& ScrachMem, s32 StackSize = DEFAULT_STACK_SIZE ) 
                                        : m_ScrachMem(ScrachMem) { ScrachMem.CreateStack( *this, StackSize ); }
        template< class T >
        T*          Push                ( s32 Count, s32 Aligment = xalingof(T) )   { return (T*)PushBytes( sizeof(T)*Count, Aligment ); }
        u8*         PushBytes           ( s32 nBytes, s32 Aligment = 4 );
        void        Pop                 ( void );
        s32         getMaxMemoryUsed    ( void ) const                              { return m_MaxMemUsed; }
                   ~xscope_stack        ( void )                                    { m_ScrachMem.FreeStack(*this); }

    protected:

        void        PushMarker          ( void );
        void        PopToMarker         ( void );
                    xscope_stack        ( void ) = delete;
    protected:

        eng_scrach_memory&      m_ScrachMem;
        s32                     m_MaxSize           = -1;   // Size of the stack
        s32                     m_iBase             = -1;   // Base of the stack where info about the stack is
        s32                     m_iStack            =  0;   // Current stack index (local to the stack itself) 
        s32                     m_iPrev             = -1;   // Previous location of the link list node   
        s32                     m_nCurMarkers       =  0;   // Number of markers used
        s32                     m_MaxMemUsed        = -1;
        xsafe_array<s32, 16>    m_lMarkers;

    protected:
        friend class eng_scrach_memory;
        friend class xscope_marker;
    };

    //-------------------------------------------------------------------------------
    class xscope_marker
    {
        xscope_marker( xscope_stack& Stack ) : m_Stack(Stack)  { Stack.PushMarker(); }
        ~xscope_marker(  )                                     { m_Stack.PopToMarker(); }

    protected:
        xscope_marker( void )  = delete;

    protected:
        xscope_stack& m_Stack;
    };

public: 

    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    template< class T >
    T*                  BufferAlloc         ( s32 Count, s32 Alingment = xalingof(T) ) { return (T*)BufferAlloc( Count * sizeof( T ), Alingment ); }
    void                PopStack            ( xscope_stack& StackHandle );
    s32                 getMaxMemoryUsed    ( void ) const;
    s32                 getBufferSize       ( void ) const;
    void                SanityCheck         ( void ) const;
    void                Initialize          ( s32 ByteSize );
    void                Test                ( void );
                       ~eng_scrach_memory   ( void );

protected:

    typedef xptr<u8> buffer;

    struct stats
    {
        x_qt_counter    m_qtMaxBufferSize;      // Stats Maximum size for the buffer ever use
        s32             m_MaxStackSize;         // Stats Maximum size for the stack ever use
        s32             m_MaxStackCount;        // How many stacks we have at its peak
    };

    struct stack_base
    {
        xscope_stack*   m_pStack;               // Pointer to the stack 
        s32             m_TotalSize;            // total size of this entry 
        s32             m_iBase;                // base for this entry
        xbool           m_bFree;                // is it free?
    };

protected:

    void        CreateStack         ( xscope_stack& Stack, s32 MaxSize );
    void        FreeStack           ( xscope_stack& Stack );
    void        Kill                ( void );
    void        PageFlip            ( void );
    u8*         BufferAlloc         ( s32 nBytes, s32 Alingment );
    buffer&     getActive           ( void );

protected:

    s32                             m_iCurrBuffer           = 0;    // Which of the two buffers we are using
    xsafe_array<buffer,2>           m_Buffer;                       // Double buffers
    x_qt_counter                    m_qtBufferIndexBuffer;          // Current Position for the Buffer global consumption
    stats                           m_Stats;

    xcritical_section               m_CriticalSection;
    s32                             m_StackListCount        = 0;
    s32                             m_StackIndexBuffer      = -1;   // Current Position for the stack global consumption
    xsafe_array<stack_base,16>      m_StackList;

protected:

    friend struct eng_context;
};

//===============================================================================
// END
//===============================================================================

template<class T>
class eng_scrach_buffer_cache
{
public:

    T*          NewAlloc            ( s32 Count );
    xbool       isCachedThisFrame   ( void ) const;
    xbool       isValid             ( void ) const;
    T*          getCache            ( void );
    const T*    getCache            ( void ) const;
    void        setup               ( T* Pointer, u32 Frame );
    void        Reset               ( void );
    
protected:

    mutable T*      m_pPointer      = NULL;
    u32             m_Frame         = 0;
};

//===============================================================================
// END
//===============================================================================

#endif

