//
//  eng_Base.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef ENG_BASE_H
#define ENG_BASE_H

#include "eng_Target.h"

//===============================================================================
// FUNCTIONS
//===============================================================================
void                eng_CheckForError               ( void );

void                eng_Init                        ( void );
void                eng_Kill                        ( void );
eng_context&        eng_CreateContext               ( void );
void                eng_KillContext                 ( eng_context& );
void                eng_SetCurrentContext           ( eng_context& Context );
eng_context&        eng_GetCurrentContext           ( void );
xcritical_section&  eng_getCriticalSection          ( void );
void                eng_SetActiveThreadContext      ( void );

// Create a rendering compatible window
eng_hwin            eng_CreateWindow                ( s32 X, s32 Y, s32 Width, s32 Height );
void                eng_KillWindow                  ( eng_hwin Handle );

#ifdef TARGET_3DS
void*               eng_AllocateDevice              ( s32 Size, s32 Alignment );
void                eng_FreeDevice                  ( void* Pointer );
#endif

//===============================================================================
// INLINES
//===============================================================================
#include "Implementation/eng_Base_inline.h"
//===============================================================================
// END
//===============================================================================
#endif