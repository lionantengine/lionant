//
//  eng_Target.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

//===============================================================================
// SYSTEM INCLUDES
//===============================================================================
#include "x_target.h"

#ifdef TARGET_PC
    #ifndef STRICT
        #define STRICT
    #endif

    #define VC_EXTRALEAN
    #include <windows.h>
    #define GLEW_STATIC
    #include "Implementation/PC/Glew/glew.h"

    #define ENG_OPEN_GL

    typedef GLuint GLhandleARB;

#elif defined TARGET_OSX
    #include <Opengl/gl.h>
    #include <Opengl/glext.h>
    #define ENG_OPEN_GL

#elif defined TARGET_IOS
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
    #define ENG_OPEN_GLES

    typedef GLuint GLhandleARB;
#elif defined (TARGET_3DS)
    #include <nn.h>
    #define ENG_OPEN_GLES

    typedef GLuint GLhandleARB;

#endif

#include "x_Base.h"

//===============================================================================
// Engine Includes
//===============================================================================
class eng_view;
class eng_draw;

#include "eng_ScrachMemory.h"
#include "eng_Context.h"
#include "eng_ResourceMgr.h"
#include "eng_View.h"
#include "eng_Texture.h"
#include "eng_Primitives.h"
#include "eng_Draw.h"
#include "eng_Input.h"
#include "eng_Sprite.h"
#include "eng_Font.h"
#include "eng_material.h"
#include "eng_Geom.h"
#include "eng_Skeleton.h"
#include "eng_AnimGroup.h"

//===============================================================================
// Engine Inline Includes
//===============================================================================
#include "Implementation/eng_scrachMemory_inline.h"
#include "Implementation/eng_Material_inline.h"
#include "Implementation/eng_AnimGroup_inline.h"
