//
//  File.h
//  engBase
//
//  Created by Tomas Arce on 8/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef ENG_MOUSE_H
#define ENG_MOUSE_H

//===============================================================================
// ENG INPUT DEVICE
//===============================================================================
class eng_input_device
{
public:
    
    virtual xbool       isPressed           ( s32 GadgetID )=0;
    virtual xbool       wasPressed          ( s32 GadgetID )=0;
    virtual xvector3d   getValue            ( s32 GadgetID )=0;
};

//===============================================================================
// ENG KEYBOARD
//===============================================================================
class eng_keyboard : public eng_input_device
{
public:
    
    // TODO: If you change any of this please make sure to update the osx_engine.m
    enum digital_gadget
#ifndef TARGET_3DS
        : u8
#endif
    {
        KEY_ESCAPE           = 0x01 ,
        KEY_1                = 0x02 ,
        KEY_2                = 0x03 ,
        KEY_3                = 0x04 ,
        KEY_4                = 0x05 ,
        KEY_5                = 0x06 ,
        KEY_6                = 0x07 ,
        KEY_7                = 0x08 ,
        KEY_8                = 0x09 ,
        KEY_9                = 0x0A ,
        KEY_0                = 0x0B ,
        KEY_MINUS            = 0x0C ,   // - on main keyboard
        KEY_EQUALS           = 0x0D ,
        KEY_PLUS             = 0x0D ,
        KEY_BACKSPACE        = 0x0E ,    // backspace
        KEY_TAB              = 0x0F ,
        KEY_Q                = 0x10 ,
        KEY_W                = 0x11 ,
        KEY_E                = 0x12 ,
        KEY_R                = 0x13 ,
        KEY_T                = 0x14 ,
        KEY_Y                = 0x15 ,
        KEY_U                = 0x16 ,
        KEY_I                = 0x17 ,
        KEY_O                = 0x18 ,
        KEY_P                = 0x19 ,
        KEY_LBRACKET         = 0x1A ,
        KEY_RBRACKET         = 0x1B ,
        KEY_RETURN           = 0x1C ,   // Enter on main keyboard
        KEY_ENTER            = 0x1C ,
        KEY_LCONTROL         = 0x1D ,
        KEY_A                = 0x1E ,
        KEY_S                = 0x1F ,
        KEY_D                = 0x20 ,
        KEY_F                = 0x21 ,
        KEY_G                = 0x22 ,
        KEY_H                = 0x23 ,
        KEY_J                = 0x24 ,
        KEY_K                = 0x25 ,
        KEY_L                = 0x26 ,
        KEY_SEMICOLON        = 0x27 ,    // ;
        KEY_APOSTROPHE       = 0x28 ,    // '
        KEY_SINGLEQUOTE      = 0x28 ,
        KEY_GRAVE            = 0x29 ,    // ' accent grave
        KEY_TILDE            = 0x29 ,    // ~
        KEY_LSHIFT           = 0x2A ,
        KEY_BACKSLASH        = 0x2B ,
        KEY_Z                = 0x2C ,
        KEY_X                = 0x2D ,
        KEY_C                = 0x2E ,
        KEY_V                = 0x2F ,
        KEY_B                = 0x30 ,
        KEY_N                = 0x31 ,
        KEY_M                = 0x32 ,
        KEY_COMMA            = 0x33 ,
        KEY_PERIOD           = 0x34 ,    // . on main keyboard
        KEY_SLASH            = 0x35 ,    // / on main keyboard
        KEY_RSHIFT           = 0x36 ,
        KEY_MULTIPLY         = 0x37 ,    // * on numeric keypad
        KEY_LMENU            = 0x38 ,    // left Alt
        KEY_LALT             = 0x38 ,
        KEY_SPACE            = 0x39 ,
        KEY_CAPITAL          = 0x3A ,
        KEY_CAPSLOCK         = 0x3A ,
        KEY_F1               = 0x3B ,
        KEY_F2               = 0x3C ,
        KEY_F3               = 0x3D ,
        KEY_F4               = 0x3E ,
        KEY_F5               = 0x3F ,
        KEY_F6               = 0x40 ,
        KEY_F7               = 0x41 ,
        KEY_F8               = 0x42 ,
        KEY_F9               = 0x43 ,
        KEY_F10              = 0x44 ,
        KEY_NUMLOCK          = 0x45 ,
        KEY_SCROLL           = 0x46 ,    // Scroll Lock
        KEY_SCROLLLOCK       = 0x46 ,
        KEY_NUMPAD7          = 0x47 ,
        KEY_NUMPAD8          = 0x48 ,
        KEY_NUMPAD9          = 0x49 ,
        KEY_SUBTRACT         = 0x4A ,    // - on numeric keypad
        KEY_NUMPAD4          = 0x4B ,
        KEY_NUMPAD5          = 0x4C ,
        KEY_NUMPAD6          = 0x4D ,
        KEY_ADD              = 0x4E ,    // + on numeric keypad
        KEY_NUMPAD1          = 0x4F ,
        KEY_NUMPAD2          = 0x50 ,
        KEY_NUMPAD3          = 0x51 ,
        KEY_NUMPAD0          = 0x52 ,
        KEY_DECIMAL          = 0x53 ,    // . on numeric keypad
        KEY_OEM_102          = 0x56 ,    // < > | on UK/Germany keyboards
        KEY_F11              = 0x57 ,
        KEY_F12              = 0x58 ,
        KEY_F13              = 0x64 ,    //                     (NEC PC98,
        KEY_F14              = 0x65 ,    //                     (NEC PC98,
        KEY_F15              = 0x66 ,    //                     (NEC PC98,
        KEY_KANA             = 0x70 ,    // (Japanese keyboard,
        KEY_ABNT_C1          = 0x73 ,    // / ? on Portugese (Brazilian, keyboards
        KEY_CONVERT          = 0x79 ,    // (Japanese keyboard,
        KEY_NOCONVERT        = 0x7B ,    // (Japanese keyboard,
        KEY_YEN              = 0x7D ,    // (Japanese keyboard,
        KEY_ABNT_C2          = 0x7E ,    // Numpad . on Portugese (Brazilian, keyboards
        KEY_NUMPADEQUALS     = 0x8D ,    // = on numeric keypad (NEC PC98,
        KEY_PREVTRACK        = 0x90 ,    // Previous Track (DEFINE_GADGET_VALUE n Japanese keyboard,
        KEY_AT               = 0x91 ,    //                     (NEC PC98,
        KEY_COLON            = 0x92 ,    //                     (NEC PC98,
        KEY_UNDERLINE        = 0x93 ,    //                     (NEC PC98,
        KEY_KANJI            = 0x94 ,    // (Japanese keyboard,
        KEY_STOP             = 0x95 ,    //                     (NEC PC98,
        KEY_AX               = 0x96 ,    //                     (Japan AX,
        KEY_UNLABELED        = 0x97 ,    //                        (J3100,
        KEY_NEXTTRACK        = 0x99 ,    // Next Track
        KEY_NUMPADENTER      = 0x9C ,    // Enter on numeric keypad
        KEY_RCONTROL         = 0x9D ,
        KEY_MUTE             = 0xA0 ,    // Mute
        KEY_CALCULATOR       = 0xA1 ,    // Calculator
        KEY_PLAYPAUSE        = 0xA2 ,    // Play / Pause
        KEY_MEDIASTOP        = 0xA4 ,    // Media Stop
        KEY_VOLUMEDOWN       = 0xAE ,    // Volume -
        KEY_VOLUMEUP         = 0xB0 ,    // Volume +
        KEY_WEBHOME          = 0xB2 ,    // Web home
        KEY_NUMPADCOMMA      = 0xB3 ,    //  , on numeric keypad (NEC PC98,
        KEY_DIVIDE           = 0xB5 ,    // / on numeric keypad
        KEY_SYSRQ            = 0xB7 ,
        KEY_RMENU            = 0xB8 ,    // right Alt
        KEY_RALT             = 0xB8 ,
        KEY_PAUSE            = 0xC5 ,    // Pause
        KEY_PAUSEBREAK       = 0xC5 ,
        KEY_HOME             = 0xC7 ,    // Home on arrow keypad
        KEY_UP               = 0xC8 ,    // UpArrow on arrow keypad
        KEY_PRIOR            = 0xC9 ,    // PgUp on arrow keypad
        KEY_PAGEUP           = 0xC9 ,
        KEY_LEFT             = 0xCB ,    // LeftArrow on arrow keypad
        KEY_RIGHT            = 0xCD ,    // RightArrow on arrow keypad
        KEY_END              = 0xCF ,    // End on arrow keypad
        KEY_DOWN             = 0xD0 ,    // DownArrow on arrow keypad
        KEY_NEXT             = 0xD1 ,    // PgDn on arrow keypad
        KEY_PAGEDOWN         = 0xD1 ,
        KEY_INSERT           = 0xD2 ,    // Insert on arrow keypad
        KEY_DELETE           = 0xD3 ,    // Delete on arrow keypad
        KEY_LWIN             = 0xDB ,    // Left Windows key
        KEY_LCOMMAND         = 0xDB ,
        KEY_RWIN             = 0xDC ,    // Right Windows key
        KEY_RCOMMAND         = 0xDC ,
        KEY_APPS             = 0xDD ,    // AppMenu key
        KEY_POWER            = 0xDE ,    // System Power
        KEY_SLEEP            = 0xDF ,    // System Sleep
        KEY_WAKE             = 0xE3 ,    // System Wake
        KEY_WEBSEARCH        = 0xE5 ,    // Web Search
        KEY_WEBFAVORITES     = 0xE6 ,    // Web Favorites
        KEY_WEBREFRESH       = 0xE7 ,    // Web Refresh
        KEY_WEBSTOP          = 0xE8 ,    // Web Stop
        KEY_WEBFORWARD       = 0xE9 ,    // Web Forward
        KEY_WEBBACK          = 0xEA ,    // Web Back
        KEY_MYCOMPUTER       = 0xEB ,    // My Computer
        KEY_MAIL             = 0xEC ,    // Mail
        KEY_MEDIASELECT      = 0xED ,    // Media Select
        KEY_PRINTSCRN,
        
        KEY_END_ENUMERATION
    };

public:
    
            xbool       BindDevice          ( void );
            xbool       isPressed           ( digital_gadget GadgetID );
            xbool       wasPressed          ( digital_gadget GadgetID );

protected:
    
    virtual xbool       isPressed           ( s32 GadgetID );
    virtual xbool       wasPressed          ( s32 GadgetID );
    virtual xvector3d   getValue            ( s32 GadgetID );
};

//===============================================================================
// ENG MOUSE
//===============================================================================
class eng_mouse : public eng_input_device
{
public:
    
    enum digital_gadget
    {
        BTN_LEFT ,
        BTN_MIDDLE ,
        BTN_RIGHT ,
        
        BTN_0 ,
        BTN_1 ,
        BTN_2 ,
        BTN_3 ,
        BTN_4 ,
        
        BTN_END_ENUMERATION
    };
    
    enum analog_gadget
    {
        ANALOG_POS_REL ,
        ANALOG_WHEEL_REL ,
        ANALOG_POS_ABS ,
        
        ANALOG_END_ENUMERATION
    };
    
public:
    
            xbool       BindDevice          ( void );
            xbool       isPressed           ( digital_gadget GadgetID );
            xbool       wasPressed          ( digital_gadget GadgetID );
            xvector2    getValue            ( analog_gadget  GadgetID );
    
protected:
    
    virtual xbool       isPressed           ( s32 GadgetID );
    virtual xbool       wasPressed          ( s32 GadgetID );
    virtual xvector3d   getValue            ( s32 GadgetID );
};

//===============================================================================
// ENG TOUCH PAD
//===============================================================================
class eng_touchpad : public eng_input_device
{
public:
    
    enum
    {
        MAX_TOUCHES = 5
    };
    
    enum digital_gadget
    {
        BTN_TOUCH_1 ,
        BTN_TOUCH_2 ,
        BTN_TOUCH_3 ,
        BTN_TOUCH_4 ,
        BTN_TOUCH_5 ,
        
        BTN_END_ENUMERATION
    };
    
    enum analog_gadget
    {
        ANALOG_TOUCH_1_REL ,
        ANALOG_TOUCH_2_REL ,
        ANALOG_TOUCH_3_REL ,
        ANALOG_TOUCH_4_REL ,
        ANALOG_TOUCH_5_REL ,
        
        ANALOG_TOUCH_1_ABS ,
        ANALOG_TOUCH_2_ABS ,
        ANALOG_TOUCH_3_ABS ,
        ANALOG_TOUCH_4_ABS ,
        ANALOG_TOUCH_5_ABS ,
        
        ANALOG_END_ENUMERATION
    };
    
public:
    
            xbool           BindDevice          ( void );
            digital_gadget  getTouchBtn         ( s32 iThouch );
            analog_gadget   getTouchRel         ( s32 iThouch );
            analog_gadget   getTouchAbs         ( s32 iThouch );
            xbool           isPressed           ( digital_gadget GadgetID );
            xbool           wasPressed          ( digital_gadget GadgetID );
            xvector2        getValue            ( analog_gadget  GadgetID );
    
protected:
    
    virtual xbool           isPressed           ( s32 GadgetID );
    virtual xbool           wasPressed          ( s32 GadgetID );
    virtual xvector3d       getValue            ( s32 GadgetID );
};

//===============================================================================
// ENG GAME PAD
//===============================================================================
class eng_gamepad : public eng_input_device
{
public:
    enum digital_gadget
    {
        BTN_PAD_1       = 0,
        BTN_PAD_2,
        BTN_PAD_3,
        BTN_PAD_4,
        BTN_PAD_5,
        BTN_PAD_6,
        BTN_PAD_7,
        BTN_PAD_8,
        BTN_PAD_9,
        BTN_PAD_10,
        BTN_PAD_11,
        BTN_PAD_12,
        BTN_PAD_13,
        BTN_PAD_14,
        BTN_PAD_15,
        BTN_PAD_16,

        BTN_END_ENUMERATION,

        BTN_PAD_LUP     = BTN_PAD_1,
        BTN_PAD_LDOWN   = BTN_PAD_2,
        BTN_PAD_LLEFT   = BTN_PAD_3,
        BTN_PAD_LRIGHT  = BTN_PAD_4,
        BTN_PAD_L1      = BTN_PAD_5,
        BTN_PAD_L2      = BTN_PAD_6,
        BTN_PAD_R1      = BTN_PAD_7,
        BTN_PAD_R2      = BTN_PAD_8,
        BTN_PAD_RUP     = BTN_PAD_9,
        BTN_PAD_RDOWN   = BTN_PAD_10,
        BTN_PAD_RLEFT   = BTN_PAD_11,
        BTN_PAD_RRIGHT  = BTN_PAD_12,
        BTN_PAD_A       = BTN_PAD_13,
        BTN_PAD_B       = BTN_PAD_14,
        BTN_PAD_START   = BTN_PAD_15,
        BTN_PAD_SELECT  = BTN_PAD_16,

#ifdef TARGET_3DS
        BTN_3DS_UP      = BTN_PAD_1,
        BTN_3DS_DOWN    = BTN_PAD_2,
        BTN_3DS_LEFT    = BTN_PAD_3,
        BTN_3DS_RIGHT   = BTN_PAD_4,

        BTN_3DS_L       = BTN_PAD_5,
        BTN_3DS_R       = BTN_PAD_7,

        BTN_3DS_X       = BTN_PAD_9,
        BTN_3DS_Y       = BTN_PAD_10,

        BTN_3DS_A       = BTN_PAD_13,
        BTN_3DS_B       = BTN_PAD_14,
        BTN_3DS_START   = BTN_PAD_15,
        BTN_3DS_SELECT  = BTN_PAD_16,
#endif
    };

    enum analog_gadget
    {
        ANALOG_PAD_1_REL ,
        ANALOG_PAD_2_REL ,

        ANALOG_PAD_1_ABS ,
        ANALOG_PAD_2_ABS ,

        ANALOG_END_ENUMERATION
    };

public:

    xbool           BindDevice          ( void );
    xbool           isPressed           ( digital_gadget GadgetID );
    xbool           wasPressed          ( digital_gadget GadgetID );
    xvector2        getValue            ( analog_gadget  GadgetID );

protected:

    virtual xbool           isPressed           ( s32 GadgetID );
    virtual xbool           wasPressed          ( s32 GadgetID );
    virtual xvector3d       getValue            ( s32 GadgetID );
};

//===============================================================================
// ENG_INPUT CLASS
//===============================================================================
/*
class eng_input
{
public:
    
    // ENUM GADGETS
    #include "eng_MOUSE_gadgets.h"
    
    // Enable force feedback for the current controller. A duration or intensity of 0 will disable feedback.
    struct feedback_envelope
    {
        f32     m_Intensity;        // Intensity is a value between 0 and 1.0
        f32     m_Duration;         // duration is time in seconds for the feedback to run
        s32     m_Mode;
    };
    
public:
    
            xbool       isPressed           ( gadget GadgetID, s32 ControllerID = 0 );
            xbool       wasPressed          ( gadget GadgetID, s32 ControllerID = 0 );
            f32         getValue            ( gadget GadgetID, s32 ControllerID = 0 );

            gadget      LookupGadget        ( const char* pName );

            void        ForceFeedback       ( f32 Duration, f32 Intensity, s32 ControllerID = 0 );
            void        ForceFeedback       ( s32 Count, feedback_envelope* pEnvelope, s32 ControllerID = 0 );
            void        EnableForceFeedback ( xbool State, s32 ControllerID = 0 );
};
*/

//===============================================================================
// ENG INPUT VIRTUAL DEVICE
//===============================================================================

class eng_input_vdevice
{
public:

    enum dimension_mode
    {
        DIMENSION_X_X = (1<<0),
        DIMENSION_X_Y = (1<<2),
        DIMENSION_X_Z = (1<<4),
        DIMENSION_Y_X = (2<<0),
        DIMENSION_Y_Y = (2<<2),
        DIMENSION_Y_Z = (2<<4),
        DIMENSION_Z_X = (3<<0),
        DIMENSION_Z_Y = (3<<2),
        DIMENSION_Z_Z = (3<<4),

        DIMENSION_XYZ_XYZ = (DIMENSION_X_X | DIMENSION_Y_Y | DIMENSION_Z_Z),

        DIMENSION_PADDING   = 0xffffffff
    };
    
    struct logical_gadget
    {
        const char*         m_pActionName;              // Action Name
        xvector3d           m_IsValue;                  // Current Value of the gadget
        xbool               m_WasValue;                 // Tells if a digital gadget was press anytime during the frame
    };

    struct physical_to_logical_map
    {
        u32                 m_ContextMask       = 0;                // In which contexts this mapping should work
        eng_input_device*   m_pDevice           = NULL;             // Which device are we talking about
        xbool               m_bGadgetIsAnalog   = FALSE;            // Use to indicate wether is a stick or a button
        s32                 m_GadgetID          = 0;                // Use to map to the physical input gadget
        xvector3d           m_Scale             = { 1, 1, 1 };      // Use for sensibility and revert axis
        s32                 m_iLogicalMapping   = 0;                // Which logical gadget will this mapping affect
        dimension_mode      m_DimensionMode     = DIMENSION_XYZ_XYZ;// How to copy values from physical to logical
    };
    
public:
    
                                eng_input_vdevice   ( void );
    const logical_gadget&       getLogicalGaget     ( s32 iGadget ) const;

    void                        EnableContexts      ( u32 Contexts );
    void                        DisableContexts     ( u32 Contexts );
    
    void                        SetControllerID     ( s32 ControllerID ){ m_ControllerID = ControllerID; }
    s32                         GetControllerID     ( void ) const      { return m_ControllerID; }
    void                        Update              ( xplatform Platform = X_PLATFORM_NULL );
    
protected:

    enum
    {
        MAX_LOGICAL         = 32,
        MAX_MAPPINGS        = 64,
    };

    s32                         GetPlatformIndex    ( xplatform Platform ) const;
    physical_to_logical_map&    CreateMapping       ( xplatform Platform );

    s32                                         m_ControllerID              = 0;

    xsafe_array<logical_gadget,MAX_LOGICAL>     m_Logical                   = {0};
    physical_to_logical_map                     m_Mappings[ X_PLATFORM_COUNT ][ MAX_MAPPINGS ];
    xsafe_array<s32,X_PLATFORM_COUNT>           m_MappingCount              = {0};
    u32                                         m_ActiveContexts            = {0};
};

//===============================================================================
// END
//===============================================================================
#endif