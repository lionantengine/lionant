//
//  eng_Context.h
//  engBase
//
//  Created by Huang Yi on 8/29/13.
//  Copyright (c) 2013 Huang Yi. All rights reserved.
//
#ifndef ENG_CONTEXT_H
#define ENG_CONTEXT_H

#ifdef TARGET_3DS

    enum eng_hwin
    {
        SCREEN_TOP = 0,         // Top screen
        SCREEN_BOTTOM,          // Bottom screen
        SCREEN_EXT              // For stereo display only
    };

#elif defined(TARGET_PC)

    typedef HWND eng_hwin;

#else

    typedef void* eng_hwin;

#endif


////////////////////////////////////////////////////////////////////////////////
// Render JOBS
////////////////////////////////////////////////////////////////////////////////

class eng_renderjob
{
public:
    
    f32         m_Order;
    
    virtual void onRunRenderJob ( void ) const =0;
};


//===============================================================================
// CONTEXT
//===============================================================================

struct eng_context
{
    //===========================================================================
    // Preset functions
    // Call these functions before you call Init() if you need.
    //===========================================================================

    // Set resolution
    void                PresetSetRes                    ( s32 Width,  s32 Height );
    void                PresetSetMultisampling          ( u32 NumSamples, u32 Quality );
    void                PresetWindowMode                ( xbool bFullScreen = FALSE );
    void                PresetWindowHandle              ( eng_hwin Handle );
    void                PresetSetupScrachMemSize        ( s32 MemSize );
    eng_hwin            getWindowHandle                 ( void ) const;
    void                Init                            ( void );
    void                Kill                            ( void );
    void                Begin                           ( const char* pTaskName );
    void                End                             ( void );
    void                ClearScreen                     ( void );
    void                PageFlip                        ( xbool WaitSync );
    u32                 GetCurrentFrameNumber           ( void );
    f32				    GetFPS						    ( void );
    void                SetActiveView                   ( const eng_view& View );
    const eng_view&	    GetActiveView                   ( void );
    void                SetClearColor                   ( xcolor Color );
    void                GetScreenResolution             ( s32& X, s32& Y );
    void			    SetFPSDisplay				    ( xbool FPS_On );
    void			    PopViewport					    ( void );
    void			    PushViewport				    ( void );
    void			    SetViewport					    ( s32 Top, s32 Left, s32 Width, s32 Height );
    xbool               HandleEvents                    ( void );
    eng_draw&           getDraw                         ( void );
    eng_scrach_memory&  getScrachMem                    ( void );
    void                qtAddRenderJob                  ( const eng_renderjob& Job );
};

//===============================================================================
// END
//===============================================================================
#endif
