//
//  eng_Draw.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef ENG_DRAW_H
#define ENG_DRAW_H

//===============================================================================
// Draw vertex
//===============================================================================

struct draw_vertex
{
                draw_vertex ( void ) {}
                draw_vertex ( f32 x, f32 y, f32 z, f32 u, f32 v, xcolor Color );
                draw_vertex ( const xvector3d& V, f32 u, f32 v, xcolor Color );
                draw_vertex ( const xvector3d& V, xcolor Color );
                draw_vertex ( const xvector3d& V );
                draw_vertex ( f32 x, f32 y, f32 z );
                draw_vertex ( f32 x, f32 y, f32 z, xcolor Color );

    void        setup       ( const xvector3d& V, f32 u, f32 v, xcolor Color );
    void        setup       ( const xvector3d& V, xcolor Color );
    void        setup       ( const xvector3d& V );
    void        setup       ( f32 x, f32 y, f32 z );
    void        setup       ( f32 x, f32 y, f32 z, xcolor Color );
    void        setup       ( f32 x, f32 y, f32 z, f32 u, f32 v, xcolor Color );

    f32         m_X, m_Y, m_Z;
    f32         m_U, m_V;
    xcolor      m_Color;
};

//===============================================================================
// Draw modes
//===============================================================================

#define ENG_DRAW_MODE_DEFAULT  0            // Use default settings

// Choose one 
#define ENG_DRAW_MODE_3D                    // Default: Render primitives as 3D. L2W matrix matters
#define ENG_DRAW_MODE_2D_LT                 // Render primitives as 2D (Origin at Left Top). This is resolution dependent
#define ENG_DRAW_MODE_2D_LB                 // Render primitives as 2D (Origin at Left Bottom). This is resolution dependent
#define ENG_DRAW_MODE_2D_PARAMETRIC         // Render primitices as 2D. But resolution is always from 0 to 1

// Choose one
#define ENG_DRAW_MODE_BLEND_OFF             // Default: No blending modes enable
#define ENG_DRAW_MODE_BLEND_ALPHA           // Blend using the alpha channel         
#define ENG_DRAW_MODE_BLEND_ADD             // Blend to render target as additive mode
#define ENG_DRAW_MODE_BLEND_SUB             // Blend to render target as subtractive mode

// Choose one
#define ENG_DRAW_MODE_ZBUFFER_ON            // Default: Use the ZBuffer
#define ENG_DRAW_MODE_ZBUFFER_OFF           // Ignore the ZBuffer
#define ENG_DRAW_MODE_ZBUFFER_READ_ONLY     // Read but dont write to the Zbuffer

// Combine
#define ENG_DRAW_MODE_RASTER_SOLID          // Default: Renders the primitives as solid       
#define ENG_DRAW_MODE_RASTER_WIRE_FRAME     // Renders the primitives in wite frame
#define ENG_DRAW_MODE_RASTER_CULL           // Default: Only CCW wise triangle will be render (front facing)
#define ENG_DRAW_MODE_RASTER_CULL_NONE      // Render all triangles independenly of where they are facing

/*

// Choose one
#define ENG_DRAW_MODE_TEXADDR_WRAP          // Default: Wrap mode for textures
#define ENG_DRAW_MODE_TEXADDR_MIRROR        // Mirror mode for textures
#define ENG_DRAW_MODE_TEXADDR_CLAMP         // Clamp mode for textures

// Choose one
#define ENG_DRAW_MODE_TEXFILTER_BILINEAR    // Default: Bilinear filtering for texture
#define ENG_DRAW_MODE_TEXFILTER_POINT       // Point Filtering for texture

*/

// Choose one
#define ENG_DRAW_MODE_TEXTURE_OFF           // Default: Renders without textures
#define ENG_DRAW_MODE_TEXTURE_ON            // Renders with texture

// Combine
#define ENG_DRAW_MODE_MISC_FLUSH            // Forces a flush
#define ENG_DRAW_MODE_MISC_CUSTOM           // Ignores seting modes

//===============================================================================
// ENG DRAW
//===============================================================================

class eng_draw
{
public:
    // Flush all draw data
    void    PageFlip            ( void );
    void    Flush               ( void );

    // Block functions
    void    Begin               ( u32 Mode = ENG_DRAW_MODE_DEFAULT );
    void    End                 ( void );

    // Simple draw functions
    void    DrawTriangles       ( const draw_vertex* pVertex, s32 nVertices, const u16* pIndex, s32 nIndices );
    void    DrawLines           ( const draw_vertex* pVertex, s32 nVertices, const u16* pIndex, s32 nIndices );
    void    DrawTriangle        ( const draw_vertex& Vertex1, const draw_vertex& Vertex2, const draw_vertex& Vertex3 );
    void    DrawLine            ( const draw_vertex& Vertex1, const draw_vertex& Vertex2 );

    // Standard functions
    void    GetBuffers          ( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices );
    void    DrawBufferTriangles ( void );
    void    DrawBufferLines     ( void );

    // State functions
    void    SetL2W              ( const xmatrix4& L2W );
    void    ClearL2W            ( void );

    void    SetTexture          ( const eng_texture& Texture );
    void    ClearTexture        ( void );
    
    void    SetProgram          ( const eng_shader_program& Program );
    void    ClearProgram        ( void );

    // Getters for the shader program
    const eng_vertex_desc&      GetVertDesc         ( void );
    const eng_vshader&          GetVShader          ( void );
    const eng_fshader&          GetFShader          ( void );
    const eng_texture&          GetWhiteTexture     ( void );
    const eng_shader_program&   GetShaderProgram    ( void );
    
    // Render a always visible marker (Doesn't need a eng_DrawBegin)
    void    DrawMarker          ( const xvector3& Pos, xcolor Color );

    // Draw a bounding box Use alpha in the DrawBegin for best results
    void    DrawBBox            ( const xbbox& BBox, xcolor UserColor, xbool bSolid = TRUE );

    // Draw rectangle
    void    DrawShadedRect      ( const xrect& Rect, xcolor TL, xcolor TR, xcolor BL, xcolor BR );
    void    DrawSolidRect       ( const xrect& Rect, xcolor Color );
    void    DrawTexturedRect    ( const xrect& Rect, const xrect& TexCoord, xcolor Color );

    // Draw sphere
    void    DrawSphere          ( xcolor Color );

protected:
            eng_draw            ( void );
            ~eng_draw           ( void );
};

//===============================================================================
// INCLUDE INLINES
//===============================================================================

#include "Implementation/eng_Draw_inline.h"

//===============================================================================
// END
//===============================================================================

#endif