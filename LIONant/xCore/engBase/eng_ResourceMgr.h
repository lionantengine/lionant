#ifndef ENG_RESOURCE_MGR_H
#define ENG_RESOURCE_MGR_H

//////////////////////////////////////////////////////////////////////////////////
//
// The Guids of resources have 64 bits where bit 0 is actually always 1
// The filename has the fallowing parts: Guid_QualityNumber.Type Where:
//   Guid          - Is the guid of the resource
//   QualityNumber - Is a number that ranges from 0(Minimun Quality) to 9(Maximun Quality)
//   type          - Is a string extension that matches with the eng_resource_type
//
// The resources have the concept of quality with the hoppe to speedup loading.
// Resources with quality zero may be the only resources that exists.
// Resources of quality zero don't have the quality number in the file name.
// So the file name of quality zero resources should look like this: Guid.Type
// Quality 0 is the bare minimun quality for the game to run.
// Ones a quality 0 is loaded the next quality level may be requested.
//////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// CLASSES
/////////////////////////////////////////////////////////////////////////////////
class eng_resource_base;
class eng_resource_type;

//==============================================================================
// RESOURCE GUID
//==============================================================================
// Author:
//     Tomas Arce
// Description:
//     The RSC Guid consist in a U64 Unique Identifier where is broken up as follows.
//     0xTTGGGGGGGGGGGGG1
//       T = 7 Bits worth of a "sequencial" global resource type
//       G = 64 - 7 - 1 Bits worth of Evenly distributed global bits
//       1 = The very first bit is a flag that signifies that it is a resource guid.
//           This flag later own is used to fiderenciate between a pointer which the
//           fist bit is always 0 vs a Guid which is always 1.
//
//==============================================================================
struct eng_resource_guid
{
    void        Create              ( void )       { xguid G; G.ResetValue(); m_UID = 1 | ((G.m_Guid<<1)&0x01ffffffffffffff);    }
    void        Create              ( u32 Type )   { xguid G; G.ResetValue(); m_UID = 1 |  (G.m_Guid<<1); setType( Type );       }
    void        setType             ( u32 Type )   { ASSERT(Type < 127 && Type ); m_UID &= 0x01ffffffffffffff; m_UID |= 1|(u64(Type)<<(64-7)); }
    xguid       getXGuid            ( void ) const { ASSERT(m_UID&1); return xguid(m_UID);   }
    u32         getType             ( void ) const { ASSERT(m_UID&1); return m_UID>>(64-7);  } // Up to 127 different types
    static u32  getType             ( u64 UID )    { ASSERT(UID&1); return UID>>(64-7);  } // Up to 127 different types
    xbool       isValid             ( void ) const { return m_UID & 0x1;                     } // Always bit 1 needs to be set
    xbool       isNull              ( void ) const { return !m_UID;                          }
    void        setNull             ( void )       { m_UID = 0;                              }
    void        SerializeIO         ( xserialfile& SerialFile  ) { SerialFile.Serialize(m_UID); }
    void        setup               ( u64 Guid )   { m_UID = Guid; ASSERT(isValid()); ASSERT(getType()>0 && getType()<127); }
    void        setup               ( u64 Guid, u32 Type ){ m_UID=Guid; setType( Type ); ASSERT(isValid()); }
    void        setup               ( eng_resource_guid Guid, u32 Type ){ setup( Guid.m_UID ); setType( Type ); }
    xstring     getAlphaString      ( void ) const { return getXGuid().GetAlphaString(); }
    
    u64     m_UID;
};

//-------------------------------------------------------------------------------

class eng_resource_mgr
{
public:
                                        eng_resource_mgr    ( void );
    virtual                            ~eng_resource_mgr    ( void );
            eng_resource_base&          getResource         ( eng_resource_guid Guid );
            void                        AbortLoading        ( void );
    static  xguid                       createGuid          ( void );
            xstring                     createFileName      ( eng_resource_guid Guid, s32 iQuality, const eng_resource_type& Type );
            xstring                     createFileName      ( eng_resource_guid Guid, s32 iQuality, const char* pExtension );
            void                        setupRootDirectory  ( const char* pRootDir ) { m_pRootDir = pRootDir; }
            xbool                       isAborting          ( void ) { return m_AbortFlag; }
    
protected:

    struct link
    {
        xstring     m_RscName;
    };
    
protected:

            void                        AddNotification         ( eng_resource_base* pBase );
            void                        MarkResourceForDeletion ( eng_resource_base& Base  );
    
protected:
    
            eng_resource_type*          FindResourceType    ( u32 UID );
            void                        Initialize          ( void );
            void                        PageFlip            ( void );

protected:

    xcritical_section                   m_HashCriticalSection;  //
    xghash<eng_resource_base*>          m_ResourceHash;         // The actual hash
    xbool                               m_AbortFlag;            // Abort flags will try to stop only loading tasks
    x_qt_counter                        m_qtDiskLock;           // This is just a flag which lets the system knows whether it is corrently working on something.
    x_qt_fober_queue                    m_qtDiskQueue;          // This is a queue of notifications waiting for the disk
    xsafe_array< x_qt_fober_queue, 4>   m_qtDeleteQueue;        // This is a list of notification for resources that can be deleted
    const char*                         m_pRootDir;
    
    // This should be for debug only in the future
    xghash<link*>                       m_GuidToFileName;       // Hash table to find the finename
    xptr<link>                          m_LinkToRscNames;       // This is a buffer containing all the resource names
    s32                                 m_iFrame          = 0;  // Used to know which deletion queue to empty
    
protected:

    friend struct notification_request;
    friend class eng_resource_base;
    template<class RSC_BASE,class RSC_DATA> friend class eng_resource_ref;
    friend void eng_Init( void );
    friend struct eng_context;
};

//-------------------------------------------------------------------------------

class eng_resource_type
{
public:
                               ~eng_resource_type   ( void );
            const char*         getTypeString       ( void ) const { return m_pType; }
    virtual u32                 getTypeUID          ( void ) const { return m_UID;   }

protected:

                                eng_resource_type   ( const char* pType, u32 UID );
    virtual eng_resource_base*  CreateInstance      ( void )=0;
    virtual void                KillInstance        ( eng_resource_base* pEntry )=0;

    
protected:

    eng_resource_type*  m_pNext;            // next type
    const char*         m_pType;            // String type added in the filename as an extension
    u8                  m_UID;              // UniqueID of the type. The type need to be globally unique.
                                            // Check the RscBuild Config file for to check all the known types

private:
                                eng_resource_type   ( void ) {}

protected:

    friend class eng_resource_mgr;
};

//-------------------------------------------------------------------------------

class eng_resource_base : public x_qt_ptr
{
public:
                                eng_resource_base   ( void );
    virtual                    ~eng_resource_base   ( void );
    s32                         getMaxQuality       ( void ){ return m_MaxQuality; }
    s32                         getCurMaxQuality    ( void ){ return m_CurMaxQuality; }
    void                        incRefCount         ( void );
    eng_resource_guid           decRefCount         ( void );
    void*                       getAsset            ( void );
    virtual eng_resource_type&  getType             ( void ) const = 0;
    void                        ForceMaxQuality     ( s32 MaxQuality );

protected:

    enum flags
    {
        FLAGS_WORKING   =   X_BIT(0),       // Currently working on loading a resource
        FLAGS_DELETE    =   X_BIT(1),       // Mark for delete
    };
    
protected:

    //virtual void                AbortLoading        ( void )=0;

    virtual void*               onQTGetAsset        ( void )=0;
    virtual xbool               onQTLoadResource    ( s32 iQuality )=0;
    virtual xbool               onQTReduceQuality   ( s32 iQuality )=0;

protected:

    eng_resource_guid   m_Guid;                 // Guid of the asset

    x_qt_fober_queue    m_qtNotificationsQueue; // This is a queue of notifications waiting for the disk
    x_qt_flags          m_qtFlags;              // This is just a flag which lets the system knows whether it is corrently working on something.
    x_qt_counter        m_qtQualityTarget;      // [0-n] The actual quality target.

    s32                 m_MaxQuality;           // [0-n] Maximun quality that this asset can become
    s32                 m_CurMaxQuality;        // [-1] no quality loaded [0-n] Current max quality that it can handle
    x_qt_counter        m_qtRefCounting;        // Reference Number


protected:

    friend struct notification_request;
    friend class eng_resource_mgr;
};

//-------------------------------------------------------------------------------

extern eng_resource_mgr g_RscMgr;

//-------------------------------------------------------------------------------

template<class RSC_BASE,class RSC_DATA>
class eng_resource_ref
{
public:
    
    eng_resource_ref( void )
    {
        m_Union.m_Guid.setNull();
    }
    
    eng_resource_ref( u64 Guid )
    {
        setup( Guid );
    }
        
    eng_resource_ref& operator = ( const eng_resource_ref& Ref )
    {
        if( Ref.isPointer() )
        {
            Ref.m_Union.m_pResource->incRefCount();
        }
        
        m_Union.m_Guid = Ref.m_Union.m_Guid;
        
        return *this;
    }
    
    xbool isPointer( void ) const
    {
        return isValid() && !m_Union.m_Guid.isValid();
    }

    xbool isValid( void ) const
    {
        return m_Union.m_Guid.m_UID > 1;
    }
    
    void setup( u64 Guid )
    {
        ASSERT( Guid );
        
        m_Union.m_Guid.m_UID = Guid;
        
        // Lets make sure that the GUID type matches with the resource ref type
#ifdef X_DEBUG
    {
        auto TypeA = m_Union.m_Guid.getType();
        auto TypeB = RSC_BASE::getStaticType().getTypeUID();
        ASSERT( TypeA == TypeB );
    }
#endif
        
        // All resource GUIDs have the lowe bit set to 1
        ASSERT( m_Union.m_Guid.isValid() );
    }
    
    void setup( eng_resource_guid Guid )
    {
        setup( Guid.m_UID );
    }
    
    const RSC_DATA* getAsset  ( void ) const
    {
        // Should not be null
        ASSERT( m_Union.m_pResource );
        
        if( m_Union.m_Guid.isValid() )
        {
            m_Union.m_pResource = (RSC_BASE*)&g_RscMgr.getResource( m_Union.m_Guid );
            ASSERT( m_Union.m_pResource );
        }
        else
        {
            // ifdef editor check to see if this file has change
            // Note when in editor mode this structure will keep a copy of the guid and the pointer boths.
            // so that it can check with the resource manager.
        }

        return (RSC_DATA*)m_Union.m_pResource->getAsset();
    }
    
    void Release( void )
    {
        if( m_Union.m_Guid.isValid() )
        {
        }
        else
        {
            m_Union.m_Guid = m_Union.m_pResource->decRefCount();
        }
    }

    ~eng_resource_ref( void )
    {
        Release();
    }

protected:

    eng_resource_ref( const eng_resource_ref& Ref )
    {
        // You should pass references to functions as references to as objects
        // this will avoid all the extra copy stuff.
        ASSERT( 0 );
        if ( Ref.isPointer() )
        {
            Ref.m_Union.m_pResource->incRefCount();
        }

        m_Union.m_Guid = Ref.m_Guid;
    }

protected:
    
    union ptr_guid
    {
        mutable eng_resource_guid   m_Guid;
        mutable RSC_BASE*           m_pResource;
    };
    
protected:

    ptr_guid m_Union;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif