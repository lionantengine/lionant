//
//  eng_Font.h
//  engBase
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef ENG_MATERIAL_H
#define ENG_MATERIAL_H

// Skinning Dual quaternions: http://www.ogre3d.org/forums/viewtopic.php?f=13&t=63952
// http://citeseerx.ist.psu.edu/showciting?cid=5237045

struct eng_shader_rsc : public eng_resource_base
{
public:

    enum
    {
        UID = 7,
    };

    struct shader_rsc;
    typedef eng_resource_ref<eng_shader_rsc, eng_shader_rsc::shader_rsc> ref;

    enum shader_type:u8
    {
        SHADER_TYPE_NULL,
        SHADER_TYPE_VERTEX,
        SHADER_TYPE_FRAGMENT,
        SHADER_TYPE_GEOMETRY
    };

    enum
    {
        MAJOR_VERSION_NUMBER        = 1,
        MINOR_VERSION_NUMBER        = 0,

        SIZE_UNIFORM_TYPE           = 8*1 + 4*1 + 1*1 + 1*3,
        SIZE_SHADER_RSC             = 8*3 + 8*2 + 1*3 + 1*5,
        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) | ( MINOR_VERSION_NUMBER 
        ^ SIZE_SHADER_RSC
        ^ SIZE_UNIFORM_TYPE
        )

    };

    struct uniform_type
    {
        xserialfile::ptr<const char>                        m_pName;                    // Uniform Variable Name
        eng_shader_program::uniform_type                    m_Type;                     // Type of the uniform
        u8                                                  m_Pad[ 7 ];

        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct shader_rsc
    {
        union
        {
            xserialfile::ptr<const char>                    m_pShader;              // Shader Program
            u64                                             m_StaticAlloc[3];       // Pre allocate the run-time structure of the shader
        };

        eng_vshader&        getVShader( void )          { ASSERT( m_ShaderType == SHADER_TYPE_VERTEX );   ASSERTCT( sizeof(eng_vshader)<=sizeof(m_StaticAlloc)); return *(eng_vshader*)&m_StaticAlloc; }
        eng_fshader&        getFShader( void )          { ASSERT( m_ShaderType == SHADER_TYPE_FRAGMENT ); ASSERTCT( sizeof(eng_fshader)<=sizeof(m_StaticAlloc)); return *(eng_fshader*)&m_StaticAlloc; }
        eng_gshader&        getGShader( void )          { ASSERT( m_ShaderType == SHADER_TYPE_GEOMETRY ); ASSERTCT( sizeof(eng_gshader)<=sizeof(m_StaticAlloc)); return *(eng_gshader*)&m_StaticAlloc; }

        const eng_vshader&  getVShader( void ) const    { ASSERT( m_ShaderType == SHADER_TYPE_VERTEX );   ASSERTCT( sizeof(eng_vshader)<=sizeof(m_StaticAlloc)); return *(eng_vshader*)&m_StaticAlloc; }
        const eng_fshader&  getFShader( void ) const    { ASSERT( m_ShaderType == SHADER_TYPE_FRAGMENT ); ASSERTCT( sizeof(eng_fshader)<=sizeof(m_StaticAlloc)); return *(eng_fshader*)&m_StaticAlloc; }
        const eng_gshader&  getGShader( void ) const    { ASSERT( m_ShaderType == SHADER_TYPE_GEOMETRY ); ASSERTCT( sizeof(eng_gshader)<=sizeof(m_StaticAlloc)); return *(eng_gshader*)&m_StaticAlloc; }

        xserialfile::ptr<eng_vertex_desc::attribute_link>   m_pAttributeLink;
        xserialfile::ptr<uniform_type>                      m_pUniform;
        u8                                                  m_nUniforms;
        u8                                                  m_nAttributes;
        shader_type                                         m_ShaderType;
        u8                                                  m_Pad[5];

             shader_rsc ( void ) = default;
             shader_rsc ( xserialfile& SerialFile );
        void SerializeIO( xserialfile& SerialFile ) const;
    };

protected:

    shader_rsc*     m_pShaderRSC = NULL;;
};

//=========================================================================
// eng_material_rsc
// Material contains all the vertex and fragment shaders need to render the material.
// additinally it has link information so that it will work and vertex buffers and
// informed materials.
//=========================================================================
struct eng_material_rsc : public eng_resource_base
{
public:

    enum
    {
        UID = 5,
    };

    struct material_rsc;
    typedef eng_resource_ref<eng_material_rsc, material_rsc> ref;

    enum technique_type:u8
    {
        TECH_TYPE_RIGID,
        TECH_TYPE_MATRIX_SKIN,
        TECH_TYPE_DQUATERNION_SKIN,
        TECH_TYPE_ENUM_COUNT
    };

    enum weight_types :u8
    {
        WEIGHTS_0,
        WEIGHTS_1,
        WEIGHTS_2,
        WEIGHTS_3,
        WEIGHTS_4,
        WEIGHTS_ENUM_COUNT
    };

    enum version:u16
    {
        MAJOR_VERSION_NUMBER        = 1,
        MINOR_VERSION_NUMBER        = 0,

        SIZE_SYSTEM_CMDS            = 1*2,
        SIZE_TECH_TYPE              = 1*3,
        SIZE_SHADER_TECHNIQUE       = 8*18 + 8*3,
        SIZE_MATERIAL_RSC           = 8*2 + 2*1 + 1*1 + TECH_TYPE_ENUM_COUNT*WEIGHTS_ENUM_COUNT*SIZE_TECH_TYPE,
        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) | ( MINOR_VERSION_NUMBER 
        ^ SIZE_SHADER_TECHNIQUE
        ^ SIZE_MATERIAL_RSC
        )
    };

    struct technique
    {
        u64                 m_StaticAlloc[18];          // Allocation for eng_shader_program
        xsafe_array<u64,3>  m_gShader;                  // 0==Vertex, 1=Fragment, 2==Geometry

        eng_shader_rsc::ref&        getVShaderRef       ( void )        { ASSERT(m_gShader[0]); return *(eng_shader_rsc::ref*)&m_gShader[0]; }
        eng_shader_rsc::ref&        getFShaderRef       ( void )        { ASSERT(m_gShader[1]); return *(eng_shader_rsc::ref*)&m_gShader[1]; }
        eng_shader_rsc::ref&        getGShaderRef       ( void )        { ASSERT(m_gShader[2]); return *(eng_shader_rsc::ref*)&m_gShader[2]; }
        eng_shader_program&         getShaderProgram    ( void )        { ASSERTCT( sizeof(eng_shader_program)<=sizeof(m_StaticAlloc)); return *(eng_shader_program*)&m_StaticAlloc; }

        const eng_shader_rsc::ref&  getVShaderRef       ( void ) const  { ASSERT(m_gShader[0]); return *(eng_shader_rsc::ref*)&m_gShader[0]; }
        const eng_shader_rsc::ref&  getFShaderRef       ( void ) const  { ASSERT(m_gShader[1]); return *(eng_shader_rsc::ref*)&m_gShader[1]; }
        const eng_shader_rsc::ref&  getGShaderRef       ( void ) const  { ASSERT(m_gShader[2]); return *(eng_shader_rsc::ref*)&m_gShader[2]; }
        const eng_shader_program&   getShaderProgram    ( void ) const  { ASSERTCT( sizeof(eng_shader_program)<=sizeof(m_StaticAlloc)); return *(eng_shader_program*)&m_StaticAlloc; }
        
        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct tech_type
    {
        s8      m_iTechnique;
        u8      m_iSystemCmd;
        u8      m_nSystemCmds;

        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct system_cmds
    {
        eng_shader_program::uniform_type    m_Type;                 // Type for the system
        u8                                  m_iRegister;            // This is the register we are using the in program 

        void SerializeIO( xserialfile& SerialFile ) const;
    };

    
    struct material_rsc
    {
        xserialfile::ptr<technique>                     m_pTechnique;
        xserialfile::ptr<system_cmds>                   m_pSystemCmd;           // System commands for all the different technique types
        u16                                             m_nSystemCmds;
        u8                                              m_nTechniques;
        
        xsafe_array<xsafe_array<tech_type,WEIGHTS_ENUM_COUNT>,TECH_TYPE_ENUM_COUNT>     m_TechType;

                material_rsc() = default;
                material_rsc    ( xserialfile& SerialFile );
        void    SerializeIO     ( xserialfile& SerialFile ) const;
    };

protected:

    material_rsc* m_pMaterialRSC = NULL;;
};

//=========================================================================
// eng_informed_material_rsc
// This resource gives specific values to any uniform vars in the shader such
// constants and textures. It also can bring things linke uv animations and such.
// Geoms are render in material order which really means in "informed material" order.
//=========================================================================
struct eng_informed_material_rsc : public eng_resource_base
{
public:

    enum
    {
        UID = 6,
    };

    struct informed_rsc;
    typedef eng_resource_ref<eng_informed_material_rsc, informed_rsc> ref;

    //---------------------------------------------------------------------------------

    enum version :u16
    {
        MAJOR_VERSION_NUMBER        = 1,
        MINOR_VERSION_NUMBER        = 0,

        SIZE_UNIFORM_VALUE          = 4*4 + 4*1 + 1*1 + 1*3,
        SIZE_COMMANDS               = 2*1 + 1*2,
        SIZE_TECHNIQUE              = 2*2,
        SIZE_INFORMED_RSC           = 8*4 + 2*3 + 1*1 + 1*1,
        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) | ( MINOR_VERSION_NUMBER 
        ^ SIZE_UNIFORM_VALUE
        ^ SIZE_COMMANDS
        ^ SIZE_TECHNIQUE
        ^ SIZE_INFORMED_RSC
        )
    };

    struct uniform_value
    {
        union
        {
            u64                             m_Guid;
            f32                             m_F[ 4 ];
            xbool                           m_Bool;
        };

        s32                                 m_xCallbackGUID;                // Guid for a callback to get this value {0==no call back, <0 Guid, >0 Index)
        eng_shader_program::uniform_type    m_Type;                         // Type of the value
        u8                                  m_Pad[ 3 ];                     // Making the padding visible

        xbool isTexture( void ) const
        {
            return ( m_Type >= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA &&
                     m_Type <= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL );
        };

        const eng_texture_rsc::ref& getTexture( void ) const 
        {
            ASSERT( isTexture() );
            return *( eng_texture_rsc::ref* )&m_Guid;
        }

        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct cmds
    {
        u16                                 m_iValue;               // Index to our value pallete
        u8                                  m_iRegister;            // This is the register we are using the in program 
        u8                                  m_iShader;              // This is for debug. Shader Index { 0=V, 1=F, 2=G } 

        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct technique
    {
        u16                                 m_iCmd;                 // Index to the first command
        u16                                 m_nCmds;                // Number of commands 
        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct informed_rsc
    {
        xserialfile::ptr<technique>         m_pTechnique;           // The technique index mirror exactly the material
        xserialfile::ptr<uniform_value>     m_pUniformValue;        // List of all uniform values for all techniques
        xserialfile::ptr<cmds>              m_pCmd;                 // List of all commands values for all techniques
        u64                                 m_rMaterial;            // GUID of the material to be use 
        u16                                 m_nUniformValues;
        u16                                 m_nCmds;
        u8                                  m_nTechniques;
        u8                                  m_Pad[1];
        

                informed_rsc( void ) = default;
                informed_rsc( xserialfile& SerialFile );
        void    SerializeIO ( xserialfile& SerialFile ) const;
        const eng_material_rsc::ref& getMaterialRef( void ) const { return *(eng_material_rsc::ref*)&m_rMaterial; }
    };

protected:

    informed_rsc* m_pInformedRSC = NULL;
   
};

//=========================================================================
// eng_material_instance
//=========================================================================

class eng_material_instance
{
public:
    
    void                        setup               ( const u64 InformedMaterialGuid );
    void                        AdvanceAnimation    ( f32 DeltaTime );
    
protected:
    
    eng_informed_material_rsc::ref       m_InformedRef;
};


#endif
