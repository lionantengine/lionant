//
//  eng_Texture.h
//  engBase
//
//  Created by Tomas Arce on 9/16/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef ENG_TEXTURE_H
#define ENG_TEXTURE_H

///////////////////////////////////////////////////////////////////////////////
// TEXTURE
////////////////////////////////////////////////////////////////////////////////

class eng_texture
{
public:
    
    enum wrapping_mode:u8
    {
        WRAP_MODE_IGNORE,                   // Won't change anything
        WRAP_MODE_MIRROR,                   // will wrap around the coordinates making the odd int numbers a mirror mapping of the even
        WRAP_MODE_TILE,                     // Will wrap around the coordinates ignoring the interger part
        WRAP_MODE_CLAMP,                    // Makes the last pixel in the border to repeat forever
        WRAP_MODE_CLAMP_TO_BORDER,          // Ones it goes out of the bounds it will use the color of a register (border)
        WRAP_MODE_COUNT
    };
    
    enum filter_mode:u8
    {
        FILTER_MINMAG_NEAREST,
        FILTER_MINMAG_LINEAR,
        FILTER_MINMAG_NEAREST_MIPMAP_NEAREST,
        FILTER_MINMAG_LINEAR_MIPMAP_NEAREST,
        FILTER_MINMAG_NEAREST_MIPMAP_LINEAR,
        FILTER_MINMAG_LINEAR_MIPMAP_LINEAR,
        FILTER_MINMAG_COUNT,
        FILTER_MINMAG_IGNORE,
    };

    enum anisotropic:u8
    {
        ANISOTROPIC_LOWEST,
        ANISOTROPIC_LOW,
        ANISOTROPIC_MEDIAN,  
        ANISOTROPIC_HIGH,
        ANISOTROPIC_HIGHEST,
    };
    
public:
                        eng_texture                 ( void );
                       ~eng_texture                 ( void );
    xbool               isValid                     ( void ) const;
    void                CreateTexture               ( const xbitmap& Bitmap, xbool bBuildMipMaps = TRUE );
    void                CreateTexture               ( s32 Width, s32 Height, xbitmap::format Format = xbitmap::FORMAT_R8G8B8A8 );
    void                setWrappingMode             ( wrapping_mode U, wrapping_mode V, wrapping_mode W = WRAP_MODE_IGNORE );
    void                setMinMagFilterMode         ( filter_mode Mig, filter_mode Mag );
    void                setAnisotropicMode          ( anisotropic Anisotropy );
    void                Destroy                     ( void );
    
    s32                 getWidth                    ( void ) const { return m_W; }
    s32                 getHeight                   ( void ) const { return m_H; }
    
    void                Activate                    ( s32 iChannel = 0 ) const;
    void                Deactivate                  ( void ) const;
    
    static const eng_texture&  getDefaultTexture   ( void );
    static eng_resource_type&  getStaticType       ( void );

protected:
#if defined ENG_OPEN_GL || defined ENG_OPEN_GLES
	GLuint                      m_IDBuffer;
    s32                         m_W, m_H;
#endif
    
    friend class eng_render_buffer;
};

///////////////////////////////////////////////////////////////////////////////
// TEXTURE RSC
////////////////////////////////////////////////////////////////////////////////

struct eng_texture_rsc : public eng_resource_base
{
public:
    
    enum
    {
        UID = 2,
    };
    
    typedef eng_resource_ref<eng_texture_rsc,eng_texture> ref;
    
    struct info
    {
                            info( void ){}
                            info( xserialfile& File ) : m_Bitmap(File) {}
        
        void SerializeIO( xserialfile& SerialFile ) const
        {
            // Make sure that it is the first version
            SerialFile.SetResourceVersion( 1 );
            ASSERTCT( sizeof(*this) == 40 );
            
            SerialFile.SerializeEnum( m_uWrapping );
            SerialFile.SerializeEnum( m_vWrapping );
            SerialFile.SerializeEnum( m_wWrapping );
            SerialFile.SerializeEnum( m_Mig );
            SerialFile.SerializeEnum( m_Mag );
            
            SerialFile.Serialize( m_Bitmap );
        }
        
        // NOTE!! Make sure that the structure is always padded properly if you change it make sure to test it with all the platforms.
        xbitmap                                 m_Bitmap;       // +32
        eng_texture::wrapping_mode              m_uWrapping;    // +1   eng_texture::wrapping_mode
        eng_texture::wrapping_mode              m_vWrapping;    // +1
        eng_texture::wrapping_mode              m_wWrapping;    // +1
        eng_texture::filter_mode                m_Mig;          // +1   eng_texture::filter_minmag_mode
        eng_texture::filter_mode                m_Mag;          // +1   eng_texture::filter_minmag_mode
        u8                                      m_Pad[3];       // +3   Just padding to make sure it works in OSX and iOS
                                                                // =40 Total
    };
    
public:
                               eng_texture_rsc   ( void );
                              ~eng_texture_rsc   ( void );
    static eng_resource_type&  getStaticType     ( void );
    
public:
    eng_texture     m_Texture;
};


#endif
