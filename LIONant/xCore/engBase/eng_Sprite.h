//
//  eng_sprite.h
//  engBase
//
//  Created by Tomas Arce on 9/23/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

//------------------------------------------------------------------------------------------

struct eng_sprite_rsc : public eng_resource_base
{
public:
    
    enum
    {
        UID = 1,
    };

    typedef eng_resource_ref<eng_sprite_rsc,eng_sprite_rsc> ref;
    
    union info
    {
        enum
        {
            ANIMPLAYBACK_ONES,
            ANIMPLAYBACK_ONES_RESET,
            ANIMPLAYBACK_LOOP,
            ANIMPLAYBACK_PINGPONG
        };

        struct v2d
        {
            f32         m_X;
            f32         m_Y;
        };
        
        struct vertex
        {
            v2d         m_Pos;
            v2d         m_UV;
        };
        
        struct animated_event
        {
            u8          m_UID;
            u8          m_iFrame;
        };
        
        struct animated
        {
            u16                             m_SpecialMask;
            u16                             m_iBase;
            u16                             m_nFrames;
            u8                              m_FPS;
            u8                              m_PlayBack;     
            u8                              m_nEvents;
            u8                              m_Padding;
            xsafe_array<animated_event,27>  m_Event;
        };
        
        vertex     m_Vertex[4];
        animated   m_Animated;
        
        //
        // Used to save the data
        //
        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof(m_Animated) == sizeof(m_Vertex) );
            ASSERTCT( sizeof(info) <= 64 );
            
            if( m_Animated.m_SpecialMask == 0xffff )
            {
                SerialFile.Serialize(m_Animated.m_SpecialMask );
                SerialFile.Serialize(m_Animated.m_iBase );
                SerialFile.Serialize(m_Animated.m_FPS );
                SerialFile.Serialize(m_Animated.m_nFrames );
                SerialFile.Serialize(m_Animated.m_PlayBack );
                SerialFile.Serialize(m_Animated.m_nEvents );
                
                for( s32 i=0; i< m_Animated.m_Event.getCount(); i++ )
                {
                    SerialFile.Serialize( m_Animated.m_Event[i].m_UID );
                    SerialFile.Serialize( m_Animated.m_Event[i].m_iFrame );
                }
            }
            else
            {
                // Save all the vertices
                for( s32 i=0; i<4; i++ )
                {
                    SerialFile.Serialize( m_Vertex[i].m_Pos.m_X );
                    SerialFile.Serialize( m_Vertex[i].m_Pos.m_Y );
                    SerialFile.Serialize( m_Vertex[i].m_UV.m_X  );
                    SerialFile.Serialize( m_Vertex[i].m_UV.m_Y  );
                }
            }
        }
    };
    
    struct group
    {
        enum
        {
            SRC_VERSION = 1
        };
        
        xserialfile::ptr<info>              m_SpriteInfo;           // Sorted Array of sprites (Sorted by the FileName Hash Key)
        xserialfile::ptr<u16>               m_SpriteNameHash;       // This is a list of the file name for each sprite
        s32                                 m_nCores;               // Number of actual sprites that we have (animated frames and regular sprites)
        s32                                 m_nSprites;             // Number of sprites that we have
        // This is a sorted list so that it can be seached quickly
        // In the same Index where the Hash can be found the same
        // index can be used to get the sprite information.
        
        group( void ){}
        group( xserialfile& SerialFile ) {ASSERT(SerialFile.GetResourceVersion()==SRC_VERSION);}
        
        //
        // Used to save the data
        //
        void SerializeIO( xserialfile& SerialFile ) const
        {
            // Make sure that it is the first version
            SerialFile.SetResourceVersion( SRC_VERSION );
            ASSERTCT( sizeof(*this) == 24 );
            
            SerialFile.Serialize( m_nCores );
            SerialFile.Serialize( m_nSprites );
            SerialFile.Serialize( m_SpriteInfo, m_nCores );
            SerialFile.Serialize( m_SpriteNameHash, m_nSprites );
        }
    };
    
public:
    
                              ~eng_sprite_rsc   ( void );
    static eng_resource_type&  getStaticType    ( void );
    
public:
    
    group*                  m_pGroup        = NULL;
    eng_texture_rsc::ref    m_SpriteAtlas;
};

//------------------------------------------------------------------------------------------

class eng_sprite
{
public:
    
    void                        setup               ( eng_resource_guid AtlastGuid, const char* pSpriteName );
    void                        setup               ( const eng_sprite_rsc::ref& Ref, const char* pSpriteName );
    void                        DrawSprite          ( eng_draw& Draw, const xvector2& Pos, const xvector2& Scale = xvector2(1,1), const s32 iFrame = -1 ) const;
    void                        setFrameIndex       ( s32 iFrame ) { m_iFrame = iFrame; }
    
    const eng_sprite_rsc&       Preresolved         ( void ) const { ASSERT(m_SpriteRscRef.getAsset()); return *m_SpriteRscRef.getAsset(); }
    const eng_sprite_rsc*       Resolve             ( void );
    const eng_sprite_rsc::info& getSpriteInfo       ( const eng_sprite_rsc& SpriteRsc ) const;
    static u16                  ResolveKey          ( u16 Key, const eng_sprite_rsc* pRsc );
    
protected:
    
    eng_sprite_rsc::ref     m_SpriteRscRef;             // Resource for the sprite
    u16                     m_Key           = 0xffff;   // Hash Key for the particular sprite
    u16                     m_iSprite       = 0xffff;   // Index inside the group for this particular sprite
    u16                     m_iFrame        = 0;        // Which frame is currently selected in case of animated sprites
    u16                     m_Flags         = 0;        // Flags for the sprite
    
protected:
    
    friend class eng_sprite_render_2dgroup;
    friend class eng_sprite_animated;
};

//------------------------------------------------------------------------------------------

class eng_sprite_animated : public eng_sprite
{
public:
    
    typedef eng_sprite_rsc::info::animated_event event;
    
    xbool                   BeginAdvanceTime    ( f32 DeltaTime );
    xbool                   WhileAdvanceTime    ( event& Event );
    void                    StartPlaying        ( xbool bFoward )   { m_Direction = bFoward?1:-1;   }
    void                    StopPlaying         ( void )            { m_Direction = 0;              }
    xbool                   isStoped            ( void ) const      { return !m_Direction;          }
    xbool                   isPlaying           ( void ) const      { return !!m_Direction;         }
    void                    Reset               ( void )            { m_DetaTimeRemeninder=0; m_iLastEventRead=0; m_Direction=0; }
    
protected:
    
    f32                     m_DetaTimeRemeninder = 0;
    s16                     m_iLastEventRead     = 0;
    s16                     m_Direction          = 1;
    
protected:
    
    friend class eng_sprite_render_2dgroup;
};

//------------------------------------------------------------------------------------------

class eng_sprite_render_2dgroup
{
public:
                                eng_sprite_render_2dgroup   ( void );
    void                        Init                        ( s32 MaxSrites, f32 JobOrder, f32 MinZ, f32 MaxZ, xbool bPerfectShorting );
    void                        qtAddSprite                 ( eng_sprite& Sprite, const xvector2& Pos, xcolor xColor );
    void                        qtAddSprite                 ( eng_sprite& Sprite, const xmatrix4& Matrix, xcolor xColor );
    void                        Flush                       ( void );
    void                        Begin                       ( void );
    
protected:
    
    struct vertex
    {
        xvector3d               m_Pos;
        xvector2                m_UV;
        u32                     m_Color;
    };
    
    struct node
    {
        const eng_texture*      m_pTexture;             // Index to the texture array
        f32                     m_Z;                    // Parametric Z value
        s32                     m_iNext;                // 0xffffffff terminated (used in z shorting)
    };
    
    struct drawcalls
    {
        u16                     m_IndexOffset;          // Which offset in the index list
        u16                     m_Count;                // How many sprites to render
        const eng_texture*      m_pTexture;             // Which texture group does this belong
        u16                     m_iNext;                // Next node in the list
        u16                     m_Padding;              // Nothing to do here
    };
    
    struct hashtable_node
    {
        x_qt_counter            m_Head;                 // Head of the linklist
    };
    
    struct drawnode : public x_simple_job<1,x_light_job>
    {
        enum { NODES_TO_DRAW = 5 };
        x_qt_counter                m_qtNSprites;       // Number of sprites in this range
        s32                         m_IndexOffset;      // Offset for the indices
        s32                         m_Start;            // Where to start processing
        s32                         m_Count;            // Where we need to finish
        eng_sprite_render_2dgroup*  m_pThis;            // Pointer to the sprite class
        u16                         m_iFirstDrawCall;   // Index of the first draw call
        
        virtual void onRun( void );
    };

    struct drawall : public eng_renderjob
    {
        eng_sprite_render_2dgroup*  m_pThis;
        virtual void onRunRenderJob( void ) const;
    };
    
protected:
    
    void                        Clear               ( void );
    s32                         ResolveIndices      ( const eng_texture& Texture );
    void                        AddToHashTable      ( s32 iNode, f32 Z );
    void                        SetupShaderProgram  ( void );

protected:
    
    f32                         m_MaxZ;
    f32                         m_MinZ;

    xbool                       m_bPerfectShorting;
    s32                         m_DrawNodeBlockSize;             // This is used to determine which draw node to use from a ZShort Index
    
    // First pass deals with transforming vertices and inserting them to the these arrays
    // Note that this can be done is parallel if needed
    x_qt_counter                m_qtNumRenderCalls;              // Current Node which is empty in the drawcall buffer
    x_qt_counter                m_qtNumNodes;                    // By knowing how many nodes we have we know how many vertices we have
    xptr<vertex>                m_qtVertexBuffer;                // The shorting of these vertices is random. So may be optimization is possible here.
    xptr<u16>                   m_qtIndices;                     // Global List of indices to render in shorted order
    xptr<node>                  m_qtSpritesNodes;
    xptr<drawcalls>             m_qtDrawCalls;                   // Buffer containing all the draw calls.
    xptr<drawnode>              m_qtDrawNode;
    
    //  by a pointer to create a new render group
    
    // Ones all the nodes have been inserted it is time to compute the shorting
    // However in parallel the we are computing how many render groups do we have
    xptr<hashtable_node>        m_qtZShort;
    
    eng_ibuffer                 m_IndexBuffer;
    eng_vbuffer                 m_VertexBuffer;
    
    drawall                     m_DrawAll;
    
    friend struct drawnode;
    friend struct temp_sort;
    friend struct drawall;
};

//-----------------------------------------------------------------------------------

inline
const eng_sprite_rsc::info& eng_sprite::getSpriteInfo( const eng_sprite_rsc& SpriteRsc ) const
{
    // Did you forget to resolved the sprite (Call ResolveSprite - Only needs to get done ones)?
    ASSERT(m_iSprite!=0xffff);
    return SpriteRsc.m_pGroup->m_SpriteInfo.m_Ptr[m_iSprite];
}

