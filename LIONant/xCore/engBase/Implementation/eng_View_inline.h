
//-------------------------------------------------------------------------------

inline
eng_view::eng_view( void )
{
    // Reset all the flags
    m_Flags = 0xffffffff;
	//NOTE! The setting of the above flags also causes infinite clipping to be used!
	//So, infinite clipping ON is the default. You must turn it off, if you want it off.


    // Set the Clip matrix
    setNearZ    ( 0.1f );
    setFarZ     ( 100 );
    setFov      ( (f32)PI * 0.25f );//X_RADIAN(95) );
    setAspect   ( 1 );

    // Set the view matrix
    LookAt      ( xvector3(0,0, 30), xvector3(0,0,0) ); 
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedView( void ) const
{
    x_FlagOn( m_Flags, FLAGS_W2V |
                       FLAGS_W2C |
                       FLAGS_W2S |
                       FLAGS_V2C |
                       FLAGS_V2S |
                       FLAGS_C2W |
                       FLAGS_C2V );
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedClip( void ) const
{
    x_FlagOn( m_Flags, FLAGS_W2C |
                       FLAGS_W2S |
                       FLAGS_V2C |
                       FLAGS_V2S |
                       FLAGS_C2W |
                       FLAGS_C2V );
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedScreen( void ) const
{
    x_FlagOn( m_Flags,  FLAGS_W2S |
                        FLAGS_V2S |
                        FLAGS_C2S |
                        FLAGS_W2C |
                        FLAGS_W2S |
                        FLAGS_V2C |
                        FLAGS_V2S |
                        FLAGS_C2W |
                        FLAGS_C2V );

}

//-------------------------------------------------------------------------------
inline
const xmatrix4& eng_view::getW2V( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_W2V) )
    {
        x_FlagOff(m_Flags, FLAGS_W2V);
        m_W2V    = m_V2W;
        m_W2V.InvertSRT();
    }
    return m_W2V;
}

//-------------------------------------------------------------------------------
inline
const xmatrix4& eng_view::getV2W( void ) const
{
    return m_V2W;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getV2CScales ( xbool bInfiniteClipping ) const
{
    xvector3      Scales;
    const xirect& ViewPort = getViewport();

#ifdef DIRECTX
    if( x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
    {
        //Field-of-view based projection matrix
        f32 HalfVPHeight = (ViewPort.GetHeight() + 1) * 0.5f;
        f32 HalfVPWidth  = (ViewPort.GetWidth()  + 1) * 0.5f;
        f32 Distance     = HalfVPWidth / x_Tan( m_Fov *0.5f );
        f32 YFov         = x_ATan( HalfVPHeight / Distance ) * 2.0f;
        f32 XFov         = x_ATan(m_Aspect * x_Tan(m_Fov * 0.5f)) * 2.0f;

        // computing the fovx from yfov
        Scales.m_X = (f32)( 1.0f / x_Tan( XFov*0.5f ));
        Scales.m_Y = (f32)( 1.0f / x_Tan( YFov*0.5f ));

        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -m_FarZ/( m_FarZ - m_NearZ );
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)

        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );

        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -m_FarZ/( m_FarZ - m_NearZ );
    }
#elif defined(TARGET_3DS)
    if( x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
    {
        f32 Aspect = ((f32)ViewPort.GetWidth()) / (f32)(ViewPort.GetHeight());
        const f32 Angle = m_Fov * 0.5f;

        const f32 Cot = 1.0f / x_Tan(Angle);

        Scales.m_X = Cot / Aspect;
        Scales.m_Y = Cot;
        Scales.m_Z = m_FarZ / ( m_FarZ - m_NearZ);
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)

        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );

        Scales.m_Z = m_FarZ/( m_FarZ - m_NearZ );
    }
#else
    if( x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
    {
        //Field-of-view based projection matrix
        f32 HalfVPHeight = (ViewPort.GetHeight() + 1) * 0.5f;
        f32 HalfVPWidth  = (ViewPort.GetWidth()  + 1) * 0.5f;
        f32 Distance     = HalfVPWidth / x_Tan( m_Fov *0.5f );
        f32 YFov         = x_ATan( HalfVPHeight / Distance ) * 2.0f;
        f32 XFov         = x_ATan(m_Aspect * x_Tan(m_Fov * 0.5f)) * 2.0f;
        
        // computing the fovx from yfov
        Scales.m_X = (f32)( 1.0f / x_Tan( XFov*0.5f ));
        Scales.m_Y = (f32)( 1.0f / x_Tan( YFov*0.5f ));
        
        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -(m_FarZ+m_NearZ)/( m_FarZ - m_NearZ );
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)
        
        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );
        
        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -(m_FarZ+m_NearZ)/( m_FarZ - m_NearZ );
    }
#endif
    return Scales;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getV2C( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_V2C) )
    {
        x_FlagOff(m_Flags,FLAGS_V2C);
        m_V2C.Zero();
        m_V2C.setScale( getV2CScales( m_Flags&FLAGS_INF_CLIP ) );
        if( !x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
        {
            //if using a custom frustum, need to take possible off-centerness into account
            m_V2C(0,2) = (m_FrustumRight + m_FrustumLeft)/(m_FrustumRight - m_FrustumLeft);
            m_V2C(1,2) = (m_FrustumTop + m_FrustumBottom)/(m_FrustumTop - m_FrustumBottom);
        }

#ifdef DIRECTX

		  // now finish the rest
        m_V2C(2,3) = m_V2C(2,2) * m_NearZ;
        m_V2C(3,2) = -1;
#elif defined(TARGET_3DS)
        m_V2C(2,3) = m_FarZ * m_NearZ / (m_FarZ - m_NearZ);
        m_V2C(3,2) = -1;
#else
        m_V2C(2,3) = -(2 * m_FarZ * m_NearZ) / (m_FarZ - m_NearZ);
        m_V2C(3,2) = -1;
#endif
    }

    return m_V2C;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2S( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_C2S) )
    {
        x_FlagOff(m_Flags,FLAGS_C2S);
        const xirect& Viewport = getViewport();
	    f32 W = Viewport.GetWidth()*0.5f;
	    f32 H = Viewport.GetHeight()*0.5f;
        m_C2S.Identity();
	    m_C2S(0,0) =  W;
	    m_C2S(1,1) = -H;
	    m_C2S(0,3) =  W + Viewport.m_Left;
	    m_C2S(1,3) =  H + Viewport.m_Top;
    }
    return m_C2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2V( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_C2V) )
    {
        x_FlagOff(m_Flags,FLAGS_C2V);
        m_C2V = getV2C();
        m_C2V.FullInvert();
    }
    return m_C2V;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4&  eng_view::getV2S( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_V2S) )
    {
        x_FlagOff( m_Flags, FLAGS_V2S );
        m_V2S    = getC2S() * getV2C();        
    }
    return m_V2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4&  eng_view::getW2C( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_W2C) )
    {
        x_FlagOff( m_Flags, FLAGS_W2C );
        m_W2C    = getV2C() * getW2V();
    }
    return m_W2C;

}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getW2S( void ) const
{
    if( x_FlagIsOn(m_Flags,FLAGS_W2S) )
    {   
        x_FlagOff( m_Flags, FLAGS_W2S );
        m_W2S    = getV2S() * getW2V();
    }
    return m_W2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2W( void ) const
{
    if( x_FlagIsOn(m_Flags, FLAGS_C2W) )
    {   
        x_FlagOff( m_Flags, FLAGS_C2W );
        m_C2W    = getW2C();
        m_C2W.FullInvert();
    }
    return m_C2W;
}

//-------------------------------------------------------------------------------

inline
eng_view& eng_view::LookAt( const xvector3& From, const xvector3& To )
{
    return LookAt( From, To, xvector3(0,1,0) );
}

//-------------------------------------------------------------------------------

inline
eng_view& eng_view::LookAt( const xvector3& From, const xvector3& To, const xvector3& Up )
{
    UpdatedView();
    m_W2V.LookAt( From, To, Up );
    m_V2W = m_W2V;
    m_V2W.InvertSRT();
    x_FlagOff( m_Flags, FLAGS_W2V );
    return *this;

}



//-------------------------------------------------------------------------------

inline
void eng_view::setNearZ( f32 NearZ )
{
    m_NearZ = NearZ;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32  eng_view::getNearZ( void ) const
{
    return m_NearZ;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setFarZ( f32 FarZ )
{
    m_FarZ = FarZ;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32  eng_view::getFarZ( void ) const
{
    return m_FarZ;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setFov( f32 Fov )
{
	//enable use of FOV
    x_FlagOn(m_Flags, FLAGS_USE_FOV);
    m_Fov = Fov;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32  eng_view::getFov( void ) const
{
    return m_Fov;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setAspect( f32 Aspect )
{
	//enable use of FOV
	 x_FlagOn(m_Flags, FLAGS_USE_FOV);
    m_Aspect = Aspect;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32  eng_view::getAspect( void ) const
{
    return m_Aspect;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setCustomFrustum( f32 Left, f32 Right, f32 Bottom, f32 Top )
{
	//disable use of FOV
	x_FlagOff(m_Flags, FLAGS_USE_FOV);

	m_FrustumLeft = Left;
	m_FrustumRight = Right;
	m_FrustumBottom = Bottom;
	m_FrustumTop = Top;

	UpdatedClip();

}



//-------------------------------------------------------------------------------

inline
void eng_view::DisableCustomFrustum()
{
	//reenable use of FOV
	x_FlagOn(m_Flags, FLAGS_USE_FOV);

	UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
void eng_view::setViewport( const xirect& Rect )
{
    m_Viewport = Rect;
    x_FlagOff( m_Flags, FLAGS_VIEWPORT );
    UpdatedScreen();
}

//-------------------------------------------------------------------------------

inline
void eng_view::RefreshViewport( void )
{
    UpdatedScreen();
}

//-------------------------------------------------------------------------------
eng_context&        eng_GetCurrentContext           ( void );

inline
const xirect& eng_view::getViewport( void ) const
{
    if( x_FlagIsOn( m_Flags, FLAGS_VIEWPORT ) )
    {
        // If the view port is not set just return the screen resolution
        m_Viewport.m_Left = 0;
        m_Viewport.m_Top  = 0;
        eng_GetCurrentContext().GetScreenResolution( m_Viewport.m_Right, m_Viewport.m_Bottom );
    }

    return m_Viewport;
}

//-------------------------------------------------------------------------------
inline
eng_view& eng_view::LookAt( f32 Distance, const xradian3& aAngles, const xvector3& At )
{
    xvector3 From   ( 0, 0, Distance );
    xvector3 WorldUp( 0, 1, 0);

    // Clamp the angles
    xradian3 Angles( aAngles );
    Angles.m_Pitch = x_Max( Angles.m_Pitch, -X_RADIAN(89) );
    Angles.m_Pitch = x_Min( Angles.m_Pitch,  X_RADIAN(89) );
    

    // Set the origin of the camera
    From.RotateX( Angles.m_Pitch );
    From.RotateY( Angles.m_Yaw );
    From = At + From;

    // set the world up
    WorldUp.RotateZ( Angles.m_Roll );

    // build the matrix
    LookAt( From, At, WorldUp );

    return *this;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getPosition( void ) const
{
    return getV2W().getTranslation();
}

//-------------------------------------------------------------------------------
inline
void eng_view::setCubeMapView( s32 Face, const xmatrix4& L2W )
{
    xvector3 vLookDir;
    xvector3 vUpDir;
    xvector3 vEye(0,0,0);

    switch( Face )
    {
        case 1: // D3DCUBEMAP_FACE_POSITIVE_X
            vLookDir.Set( 1.0f, 0.0f, 0.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;

        case 0: // D3DCUBEMAP_FACE_NEGATIVE_X
            vLookDir.Set(-1.0f, 0.0f, 0.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;

        case 2: // D3DCUBEMAP_FACE_POSITIVE_Y
            vLookDir.Set( 0.0f, 1.0f, 0.0f );
            vUpDir.Set  ( 0.0f, 0.0f,-1.0f );
            break;

        case 3: // D3DCUBEMAP_FACE_NEGATIVE_Y
            vLookDir.Set( 0.0f,-1.0f, 0.0f );
            vUpDir.Set  ( 0.0f, 0.0f, 1.0f );
            break;

        case 4: // D3DCUBEMAP_FACE_POSITIVE_Z
            vLookDir.Set( 0.0f, 0.0f, 1.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;

        case 5: // D3DCUBEMAP_FACE_NEGATIVE_Z
            vLookDir.Set( 0.0f, 0.0f,-1.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;

        default:
            ASSERT( 0 );
    }

    // Make sure that the FOV is set to 90
    // as well as the aspect ratio is set to 1
    // We will let the user set the near and far
    setFov      ( X_RADIAN(90) );
    setAspect   ( 1 );

    // Set the view transform for this cubemap surface
    xmatrix4  NoScales( L2W );
    NoScales.ClearScale();
    vEye     = NoScales * vEye ;
    vLookDir = NoScales * vLookDir;
    vUpDir   = NoScales.RotateVector( vUpDir );
    LookAt( vEye, vLookDir, vUpDir );
}

//-------------------------------------------------------------------------------
inline
void eng_view::setParabolicView( s32 Side, const xmatrix4& L2W )
{
    xvector3 vLookDir;
    xvector3 vUpDir;
    xvector3 vEye(0,0,0);

    switch( Side )
    {
        case 0:
            vLookDir.Set( 0.0f, 0.0f, 1.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;
        case 1: 
            vLookDir.Set( 0.0f, 0.0f,-1.0f );
            vUpDir.Set  ( 0.0f, 1.0f, 0.0f );
            break;

        default:
            ASSERT( 0 );
    }

    // Make sure that the FOV is set to 180
    // as well as the aspect ratio is set to 1
    // We will let the user set the near and far
    setFov      ( X_RADIAN(90) );
    setAspect   ( 1 );

    // Set the view transform for this cubemap surface
    xmatrix4  NoScales( L2W );
    NoScales.ClearScale();
    vEye     = NoScales * vEye;
    vLookDir = NoScales * vLookDir;
    vUpDir   = NoScales.RotateVector( vUpDir );
    LookAt( vEye, vLookDir, vUpDir );
}

//-------------------------------------------------------------------------------
inline
void eng_view::setInfiniteClipping( xbool bInfinite )
{
    if( x_FlagIsOn( m_Flags, FLAGS_INF_CLIP ) == bInfinite )
        return;

    // Set the flag
    if( bInfinite ) x_FlagOn ( m_Flags, FLAGS_INF_CLIP );
    else            x_FlagOff( m_Flags, FLAGS_INF_CLIP );

    // update the cache entries
    UpdatedClip();
}

//-------------------------------------------------------------------------------
inline
xbool eng_view::getInfiniteClipping( void ) const
{
	return x_FlagIsOn( m_Flags, FLAGS_INF_CLIP );
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldZVector( void ) const
{
    const xmatrix4& V2W = getV2W();
    xvector3 Normal( V2W(0,2), V2W(1,2), V2W(2,2) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldXVector( void ) const
{
    const xmatrix4& V2W = getV2W();
    xvector3 Normal( V2W(0,0), V2W(1,0), V2W(2,0) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldYVector( void ) const
{
    const xmatrix4& V2W = getV2W();
    xvector3 Normal( V2W(0,1), V2W(1,1), V2W(2,1) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
void eng_view::setPosition( xvector3& Position  )
{
    m_V2W.setTranslation( Position );
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
void eng_view::setRotation( xradian3& Rotation )
{
    m_V2W.setRotation( Rotation );
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
void eng_view::Translate( xvector3& DeltaPos )
{
    m_V2W.Translate( DeltaPos );    
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::RayFromScreen( f32 ScreenX, f32 ScreenY ) const
{
    xvector3      Distance = getV2CScales( TRUE );
    const xirect& Viewport = getViewport();

    // Build ray in viewspace.
    f32 HalfW = (Viewport.m_Left + Viewport.m_Right) * 0.5f;
    xvector3 Ray( -(ScreenX - HalfW ),
                 -(ScreenY - (Viewport.m_Top  + Viewport.m_Bottom) * 0.5f) * m_Aspect,
                 -HalfW*Distance.m_X );

    // Take the ray into the world space
    Ray = getV2W().RotateVector( Ray );
    Ray.Normalize();

    return Ray;
}

//-------------------------------------------------------------------------------
inline
f32 eng_view::getScreenSize( const xvector3& Position, f32 Radius ) const
{
    const xmatrix4& V2W = getV2W();

    xvector3 vPos  = Position - V2W.getTranslation();
    f32     ViewZ = V2W(0,2) * vPos.m_X + V2W(1,2) * vPos.m_Y + V2W(2,2) * vPos.m_Z;
 
    // Is it completly behind the camera?
    if( ViewZ < -Radius )
        return 0;

    // get the closest distance of the sphere to the camera
    ViewZ = x_Min( ViewZ, Radius );

    // get the biggest scale such the circle will be conservative
    const xmatrix4& V2C = getV2C();
    f32 ViewPortScale = x_Max( V2C(0,0), V2C(1,1) );

    // get the diameter of the circle in screen space
    f32 ScreenRadius  = ( 2 * Radius * ViewPortScale)/ViewZ;

    return ScreenRadius;
}

