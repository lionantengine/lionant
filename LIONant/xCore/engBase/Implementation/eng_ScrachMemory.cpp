//===============================================================================
// INCLUDES
//===============================================================================

#include "eng_Base.h"

//===============================================================================
// DEFINES
//===============================================================================

#define DEBUG_PADDING   16

//===============================================================================
// TYPES
//===============================================================================

struct debug_info
{
    u8    m_PadEnd[DEBUG_PADDING];
    s32   m_iPrev;
    s32   m_iStart;
    s32   m_UserSize;
    s32   m_Aligment;
    union
    {
        u8      m_PadBegin[DEBUG_PADDING];    // Pad for the begging of the next node
        s32     m_StackLastNode;              // When doing the last node of the stack the padding is not needed
    };                                        // Stead we use it to indicate which was the last node. That way 
};                                            // we avoid the link-list walk whenever we add a new node in the stack.  

//===============================================================================
// FUNCTION
//===============================================================================


//-------------------------------------------------------------------------------

eng_scrach_memory::~eng_scrach_memory( void )
{

}

//-------------------------------------------------------------------------------

void eng_scrach_memory::Initialize( s32 ByteSize )
{
    // Make sure that nothing is initialize yet
    ASSERT( m_Buffer[0].getCount() == 0 );       
    ASSERT( m_Buffer[1].getCount() == 0 );

    // Allocate both buffers
    ByteSize = x_Align( x_Max( ByteSize, 1024 ), 16 );
    m_Buffer[0].Alloc( ByteSize, XMEM_FLAG_ALIGN_16B );
    m_Buffer[1].Alloc( ByteSize, XMEM_FLAG_ALIGN_16B );

    // set the top index
    m_StackIndexBuffer = ByteSize;
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::Kill( void )
{
    // Make sure that nothing is initialize yet
    m_Buffer[0].Destroy();
    m_Buffer[1].Destroy();
}

//-------------------------------------------------------------------------------

eng_scrach_memory::buffer& eng_scrach_memory::getActive( void )
{
    return m_Buffer[m_iCurrBuffer&1];
}

//-------------------------------------------------------------------------------

void  eng_scrach_memory::PageFlip( void )
{
    //
    // switch the buffers
    //
    m_iCurrBuffer    = 1-m_iCurrBuffer;

    //
    // Reset lists
    //
    ASSERT(m_StackListCount==0);
    m_qtBufferIndexBuffer.setup(0);
}

//-------------------------------------------------------------------------------

u8* eng_scrach_memory::BufferAlloc( s32 nBytes, s32 Alingment )
{
    ASSERT( nBytes >= 0 );

    buffer&     Buffer     = getActive();
    s32         NewBase;

    do
    {
        s32 LocalReality = m_qtBufferIndexBuffer.get();
        
        NewBase          = x_Align( LocalReality, Alingment ); 
        s32 NewReality   = NewBase + nBytes;

        ASSERT( &Buffer[ NewReality ] );
        ASSERT( NewReality < m_StackIndexBuffer );

        // try to set the new bottom
        if( m_qtBufferIndexBuffer.set( LocalReality, NewReality ) )
            break;

    } while (1);

    // Update stats
    m_Stats.m_qtMaxBufferSize.Max( m_qtBufferIndexBuffer.get() );

    return &Buffer[ NewBase ];
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::CreateStack( eng_scrach_memory::xscope_stack& Stack, s32 MaxSize )
{
    xscope_atomic( m_CriticalSection );

    ASSERT( MaxSize > 16 );

    MaxSize = x_Align( x_Max( DEFAULT_STACK_SIZE, MaxSize ), 16 );

    //
    // Find possible candidates first
    //
    s32 iCandidate = -1;
    for ( s32 i = 0; i < m_StackListCount; i++ )
    {
        stack_base& StackBase = m_StackList[i];

        if ( StackBase.m_bFree == FALSE )
            continue;

        if( StackBase.m_TotalSize < MaxSize )
            continue;

        iCandidate = i;
        break;
    }

    //
    // Create new entry if we have to
    //
    if ( iCandidate == -1 ) 
    {
        stack_base& StackBase = m_StackList[m_StackListCount];

        // Allocate the new memory from the actual buffer
        m_StackIndexBuffer -= MaxSize;
        ASSERT( m_StackIndexBuffer > m_qtBufferIndexBuffer.get() );
        ASSERT( x_IsAlign( m_StackIndexBuffer, 16 ) );

        // Update stats
        m_Stats.m_MaxStackSize = x_Max( m_Stats.m_MaxStackSize, m_Buffer[0].getCount() - m_StackIndexBuffer );

        // We are allocating the very first stack
        StackBase.m_TotalSize   = MaxSize;
        StackBase.m_iBase       = m_StackIndexBuffer;
        ASSERT( x_IsAlign( StackBase.m_iBase, 16 ) );
        ASSERT( x_IsAlign( &m_Buffer[0][StackBase.m_iBase], 16 ) );
        ASSERT( x_IsAlign( &m_Buffer[1][StackBase.m_iBase], 16 ) );

        // Set the new info
        iCandidate = m_StackListCount;
        m_StackListCount++;

        // Update stats
        m_Stats.m_MaxStackCount = x_Max( m_Stats.m_MaxStackCount, m_StackListCount );
    }
    else
    {
        stack_base&     StackBase = m_StackList[iCandidate];
        const s32       SplitSize = StackBase.m_TotalSize - MaxSize; 
        
        // is it worth splitting?
        if ( SplitSize >= DEFAULT_STACK_SIZE )
        {
            const s32 Count = m_StackListCount - (iCandidate + 1);
            x_memmove( &m_StackList[iCandidate+2], &m_StackList[iCandidate+1], Count * sizeof(stack_base) );
            m_StackListCount++;

            // Create new node
            stack_base& NewStackBase = m_StackList[iCandidate+1];
            NewStackBase.m_bFree     = TRUE;
            NewStackBase.m_iBase     = StackBase.m_iBase - MaxSize; 
            NewStackBase.m_TotalSize = SplitSize;
            NewStackBase.m_pStack    = NULL;

            // Update our current node
            StackBase.m_TotalSize = MaxSize;
        }
    }

    //
    // Update our node
    //
    stack_base& StackBase   = m_StackList[iCandidate];
    StackBase.m_bFree       = FALSE;
    StackBase.m_pStack      = &Stack;

    //
    // Now create the stack
    //
    Stack.m_MaxSize     = StackBase.m_TotalSize;
    Stack.m_iBase       = StackBase.m_iBase;
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::FreeStack( xscope_stack& Stack )
{
    xscope_atomic( m_CriticalSection );

    //
    // Find our entry in the list
    //
    s32 iList = -1;
    for ( s32 i = 0; i < m_StackListCount; ++i )
    {   
        if( m_StackList[ i ].m_pStack == &Stack )
        {
            iList = i;
            break;
        }
    }
    ASSERT( iList != -1 );

    //
    // Mark entry as now free
    //
    m_StackList[ iList ].m_bFree    = TRUE;
    m_StackList[ iList ].m_pStack   = NULL;

    //
    // See if we can collase this entry with the above 
    //
    s32 iSrc  = S32_MIN;
    s32 iDst  = S32_MAX;
    if ( (iList < (m_StackListCount - 2)) && m_StackList[ iList + 1 ].m_bFree )
    {
        stack_base& StackBase  = m_StackList[ iList ];
        StackBase.m_TotalSize += m_StackList[ iList + 1 ].m_TotalSize;

        // Collase array
        iSrc = iList + 2;
        iDst = iList + 1;
    }

    //
    // See if we can collase this entry with the one below 
    //
    if ( iList > 0 && m_StackList[ iList - 1 ].m_bFree )
    {
        stack_base& StackBase  = m_StackList[ iList - 1 ];
        StackBase.m_TotalSize += m_StackList[ iList ].m_TotalSize;

        // Collase array
        iSrc = x_Max( iSrc, iList + 1);
        iDst = x_Min( iDst, iList    );
    }

    //
    // Do any list collapsing that we have to
    //
    if ( iSrc >= 0 )
    {
        const s32 Count = m_StackListCount - iSrc;
        x_memmove( &m_StackList[ iDst ], &m_StackList[ iSrc ], sizeof(stack_base)*Count );
        m_StackListCount -= (iSrc-iDst);
    }

    //
    // Try to remove entries from the array
    //
    while ( m_StackListCount && m_StackList[ m_StackListCount - 1 ].m_bFree )
    {
        stack_base& StackBase  = m_StackList[ m_StackListCount - 1 ];
        
        // give up the memory to the pool! yeah!!!
        m_StackIndexBuffer    += StackBase.m_TotalSize;

        // Mark entry as now free
        StackBase.m_bFree      = TRUE;
        StackBase.m_pStack     = NULL;

        // officially remove entry from the list
        --m_StackListCount;
    }
}

//-------------------------------------------------------------------------------

u8* eng_scrach_memory::xscope_stack::PushBytes( s32 nBytes, s32 Aligment )
{
    eng_scrach_memory::buffer&  Buffer          = m_ScrachMem.getActive();

    const s32                   iNewBase        = x_Align( m_iStack, Aligment );
    const s32                   iNewStack       = (s32)x_Align( iNewBase + nBytes + sizeof(s32), 4 );
    const s32                   iNewPrev        = iNewStack - sizeof(s32);

    // Make sure there is room in the stack for this allocation
    ASSERT( iNewStack < m_MaxSize );

    // Create the link list (The link list node is on the top)
    *(s32*)(&Buffer[m_iBase + iNewPrev]) = m_iPrev;
    m_iPrev                              = iNewPrev; 
    
    // Set the new stack (iStack is always at the top)
    m_iStack        = iNewStack; 
    m_MaxMemUsed    = x_Max( m_MaxMemUsed, m_iStack );

    // Make sure everything looks good and return the pointer
    ASSERT( x_IsAlign( &Buffer[m_iBase + iNewBase], Aligment) );
    return &Buffer[m_iBase + iNewBase];
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::xscope_stack::Pop( void )
{
    ASSERT(m_iPrev>0);
    ASSERT(m_iStack>0);

    // Update the indices
    eng_scrach_memory::buffer&  Buffer = m_ScrachMem.getActive();
    m_iPrev  = *(s32*)(&Buffer[m_iBase + m_iPrev]);
    if ( m_iPrev == -1 )
    {
        m_iStack      = 0;
        m_nCurMarkers = 0;
    }
    else
    {
        m_iStack = m_iPrev + sizeof(s32);
    }
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::xscope_stack::PushMarker( void )
{
    m_lMarkers[m_nCurMarkers] = m_iPrev;
    m_nCurMarkers++;
}

//-------------------------------------------------------------------------------

void eng_scrach_memory::xscope_stack::PopToMarker( void )
{
    m_nCurMarkers--;
    m_iPrev  = m_lMarkers[m_nCurMarkers];
    m_iStack = m_iPrev + sizeof(s32);
}

//-------------------------------------------------------------------------------

s32 eng_scrach_memory::getMaxMemoryUsed( void ) const
{
    return m_Stats.m_qtMaxBufferSize.get( ) + m_Stats.m_MaxStackSize;
}

//-------------------------------------------------------------------------------

s32 eng_scrach_memory::getBufferSize( void ) const
{
    return m_Buffer[0].getByteCount();
}


//-------------------------------------------------------------------------------

void eng_scrach_memory::SanityCheck( void ) const
{

}

//-------------------------------------------------------------------------------

void eng_scrach_memory::Test( void )
{
#ifdef X_DEBUG
    x_inline_light_jobs_block<8> JobBlock;

    //
    // Allocate some random block
    //
    for( s32 t=0; t<18; t++ )
    {
        // Allocate blocks
        for( s32 b=0; b<32; b++ )
        {
            s32 Size  = x_irand( 0, 1024 );
            u8* pData = BufferAlloc<u8>( Size );
            x_memset( pData, 0xBB, Size );
        }

        // Allocate stack
        for ( s32 m = 0; m < 16000; m++ ) JobBlock.SubmitJob( [&]()
        {
            xrandom_small Rnd;
            xscope_stack  Stack( *this, 40*1024 );

            Rnd.setSeed64( (u64)&Rnd );

            Stack.PushMarker();
            s32 StackMarkers=0;
            for( s32 s=0; s<32; s++ )
            {
                if( Rnd.Rand32(0,10) < 3 )
                {
                    Stack.PushMarker();   
                    StackMarkers++;
                }

                if ( Rnd.Rand32( 0, 1000 ) == 0 ) x_Sleep(1);

                s32 Size  = Rnd.Rand32( 0, 1024 );
                u8* pData = Stack.PushBytes( Size );
                x_memset( pData, 0xCC, Size );
                if( Rnd.Rand32(0,10) < 3 )
                {
                    if( StackMarkers > 0 )
                    {
                        Stack.PopToMarker();   
                        StackMarkers--;
                    }
                }
            }

            SanityCheck();
        } );

        // Finish up
        JobBlock.FinishJobs();
        SanityCheck();
        PageFlip();
    }

#endif
}