//===============================================================================
// Defines
//===============================================================================

#undef ENG_DRAW_MODE_3D
#undef ENG_DRAW_MODE_2D_PARAMETRIC  
#undef ENG_DRAW_MODE_2D_LT
#undef ENG_DRAW_MODE_2D_LB

// Choose one

#undef ENG_DRAW_MODE_BLEND_OFF             
#undef ENG_DRAW_MODE_BLEND_ALPHA           
#undef ENG_DRAW_MODE_BLEND_ADD             
#undef ENG_DRAW_MODE_BLEND_SUB             

// Choose one

#undef ENG_DRAW_MODE_ZBUFFER_ON            
#undef ENG_DRAW_MODE_ZBUFFER_OFF           
#undef ENG_DRAW_MODE_ZBUFFER_READ_ONLY     

// Combine

#undef ENG_DRAW_MODE_RASTER_SOLID          
#undef ENG_DRAW_MODE_RASTER_WIRE_FRAME     
#undef ENG_DRAW_MODE_RASTER_CULL           
#undef ENG_DRAW_MODE_RASTER_CULL_NONE      

// Choose one

#undef ENG_DRAW_MODE_TEXADDR_WRAP          
#undef ENG_DRAW_MODE_TEXADDR_MIRROR        
#undef ENG_DRAW_MODE_TEXADDR_CLAMP         

// Choose one

#undef ENG_DRAW_MODE_TEXFILTER_BILINEAR    
#undef ENG_DRAW_MODE_TEXFILTER_POINT       

// Choose one

#undef ENG_DRAW_MODE_TEXTURE_OFF           
#undef ENG_DRAW_MODE_TEXTURE_ON            

// Combine

#undef ENG_DRAW_MODE_MISC_FLUSH            
#undef ENG_DRAW_MODE_MISC_CUSTOM           

//===============================================================================
// Set actual values
//===============================================================================

/*

// Zbuffer states (ID3D10DepthStencilState)

    ZBUFFER
    NO_ZBUFFER
    NO_ZWRITE

// Blend states (CreateBlendState)

    NO_ALPHA
    BLEND_ADD
    BLEND_SUB
    ALPHA

// General state (ID3D10RasterizerState)

    CULL_NONE       WIRE_FRAME  WIRE_SOLID
    CULL_CCW        WIRE_FRAME  WIRE_SOLID

// Textures addressing (CreateSamplerState)

    TEXTURE_WRAP          TEXTURE_POINT_FILTER        TEXTURE_BILINIAR_FILTER
    TEXTURE_MIRROR        TEXTURE_POINT_FILTER        TEXTURE_BILINIAR_FILTER
    TEXTURE_MIRROR        TEXTURE_POINT_FILTER        TEXTURE_BILINIAR_FILTER
    TEXTURE_CLAMP         TEXTURE_POINT_FILTER        TEXTURE_BILINIAR_FILTER

*/

#define ENG_DRAW_MODE_SHIFT                     ( 0 )
#define ENG_DRAW_MODE_BIT_COUNT                 ( 3 )
#define ENG_DRAW_MODE_MASK                      ( (1<<ENG_DRAW_MODE_BIT_COUNT)-1 )
#define ENG_DRAW_MODE_3D                        ( 0 << ENG_DRAW_MODE_SHIFT )
#define ENG_DRAW_MODE_2D_ABS                    ( 1 << ENG_DRAW_MODE_SHIFT )
#define ENG_DRAW_MODE_2D_PARAMETRIC             ( 2 << ENG_DRAW_MODE_SHIFT )
#define ENG_DRAW_MODE_Y_TOP_LEFT                ( 4 << ENG_DRAW_MODE_SHIFT )
#define ENG_DRAW_MODE_BLEND_SHIFT               ( ENG_DRAW_MODE_SHIFT + ENG_DRAW_MODE_BIT_COUNT )
#define ENG_DRAW_MODE_BLEND_BIT_COUNT           ( 2 )
#define ENG_DRAW_MODE_BLEND_MASK                ( (1<<ENG_DRAW_MODE_BLEND_BIT_COUNT)-1 )
#define ENG_DRAW_MODE_BLEND_OFF                 ( 0 << ENG_DRAW_MODE_BLEND_SHIFT )
#define ENG_DRAW_MODE_BLEND_ALPHA               ( 1 << ENG_DRAW_MODE_BLEND_SHIFT )
#define ENG_DRAW_MODE_BLEND_ADD                 ( 2 << ENG_DRAW_MODE_BLEND_SHIFT )
#define ENG_DRAW_MODE_BLEND_SUB                 ( 3 << ENG_DRAW_MODE_BLEND_SHIFT )
#define ENG_DRAW_MODE_ZBUFFER_SHIFT             ( ENG_DRAW_MODE_BLEND_SHIFT + ENG_DRAW_MODE_BLEND_BIT_COUNT )
#define ENG_DRAW_MODE_ZBUFFER_BIT_COUNT         ( 2 )
#define ENG_DRAW_MODE_ZBUFFER_MASK              ( (1<<ENG_DRAW_MODE_ZBUFFER_BIT_COUNT)-1 )
#define ENG_DRAW_MODE_ZBUFFER_ON                ( 0 << ENG_DRAW_MODE_ZBUFFER_SHIFT )
#define ENG_DRAW_MODE_ZBUFFER_OFF               ( 1 << ENG_DRAW_MODE_ZBUFFER_SHIFT )
#define ENG_DRAW_MODE_ZBUFFER_READ_ONLY         ( 2 << ENG_DRAW_MODE_ZBUFFER_SHIFT )
#define ENG_DRAW_MODE_RASTER_SHIFT              ( ENG_DRAW_MODE_ZBUFFER_SHIFT + ENG_DRAW_MODE_ZBUFFER_BIT_COUNT )
#define ENG_DRAW_MODE_RASTER_BIT_COUNT          ( 2 )
#define ENG_DRAW_MODE_RASTER_MASK               ( (1<<ENG_DRAW_MODE_RASTER_BIT_COUNT)-1 )
#define ENG_DRAW_MODE_RASTER_SOLID              ( 0             << ENG_DRAW_MODE_RASTER_SHIFT )
#define ENG_DRAW_MODE_RASTER_WIRE_FRAME         ( X_BIT( 0 )    << ENG_DRAW_MODE_RASTER_SHIFT )
#define ENG_DRAW_MODE_RASTER_CULL               ( 0             << ENG_DRAW_MODE_RASTER_SHIFT )
#define ENG_DRAW_MODE_RASTER_CULL_NONE          ( X_BIT( 1 )    << ENG_DRAW_MODE_RASTER_SHIFT )
#define ENG_DRAW_MODE_TEXADDR_SHIFT             ( ENG_DRAW_MODE_RASTER_SHIFT + ENG_DRAW_MODE_RASTER_BIT_COUNT )
#define ENG_DRAW_MODE_TEXADDR_BIT_COUNT         ( 2 )
#define ENG_DRAW_MODE_TEXADDR_WRAP              ( 0 << ENG_DRAW_MODE_TEXADDR_SHIFT )
#define ENG_DRAW_MODE_TEXADDR_MIRROR            ( 1 << ENG_DRAW_MODE_TEXADDR_SHIFT )
#define ENG_DRAW_MODE_TEXADDR_CLAMP             ( 2 << ENG_DRAW_MODE_TEXADDR_SHIFT )
#define ENG_DRAW_MODE_TEXFILTER_SHIFT           ( ENG_DRAW_MODE_TEXADDR_SHIFT + ENG_DRAW_MODE_TEXADDR_BIT_COUNT )
#define ENG_DRAW_MODE_TEXFILTER_BIT_COUNT       ( 2 )
#define ENG_DRAW_MODE_TEXFILTER_BILINEAR        ( 0 << ENG_DRAW_MODE_TEXFILTER_SHIFT )
#define ENG_DRAW_MODE_TEXFILTER_POINT           ( 1 << ENG_DRAW_MODE_TEXFILTER_SHIFT )
#define ENG_DRAW_MODE_TEXTURE_SHIFT             ( ENG_DRAW_MODE_TEXFILTER_SHIFT + ENG_DRAW_MODE_TEXFILTER_BIT_COUNT )
#define ENG_DRAW_MODE_TEXTURE_BIT_COUNT         ( 2 )
#define ENG_DRAW_MODE_TEXTURE_OFF               ( 0 << ENG_DRAW_MODE_TEXTURE_SHIFT )
#define ENG_DRAW_MODE_TEXTURE_ON                ( 1 << ENG_DRAW_MODE_TEXTURE_SHIFT )
#define ENG_DRAW_MODE_MISC_SHIFT                ( ENG_DRAW_MODE_TEXTURE_SHIFT + ENG_DRAW_MODE_TEXTURE_BIT_COUNT )
#define ENG_DRAW_MODE_MISC_BIT_COUNT            ( 2 )
#define ENG_DRAW_MODE_MISC_FLUSH                ( X_BIT(0) << ENG_DRAW_MODE_MISC_SHIFT )
#define ENG_DRAW_MODE_MISC_CUSTOM               ( X_BIT(1) << ENG_DRAW_MODE_MISC_SHIFT )
#define ENG_DRAW_MODE_CUSTOM_PROGRAM_SHIFT      ( ENG_DRAW_MODE_MISC_SHIFT + ENG_DRAW_MODE_MISC_BIT_COUNT )
#define ENG_DRAW_MODE_CUSTOM_PROGRAM_BIT_COUNT  ( 1 )
#define ENG_DRAW_MODE_CUSTOM_PROGRAM            ( 1 << ENG_DRAW_MODE_CUSTOM_PROGRAM_SHIFT )
#define ENG_DRAW_PRIMITIVE_TYPE_SHIFT           ( ENG_DRAW_MODE_CUSTOM_PROGRAM_SHIFT + ENG_DRAW_MODE_CUSTOM_PROGRAM_BIT_COUNT )
#define ENG_DRAW_PRIMITIVE_TYPE_BIT_COUNT       ( 2 )
#define ENG_DRAW_PRIMITIVE_TYPE_MASK            ( (1<<ENG_DRAW_PRIMITIVE_TYPE_BIT_COUNT)-1 )
#define ENG_DRAW_PRIMITIVE_TYPE_TRIANGLE        ( 0 << ENG_DRAW_PRIMITIVE_TYPE_SHIFT )
#define ENG_DRAW_PRIMITIVE_TYPE_LINE            ( 1 << ENG_DRAW_PRIMITIVE_TYPE_SHIFT )
#define ENG_DRAW_MODE_2D_LT ( ENG_DRAW_MODE_2D_ABS | ENG_DRAW_MODE_RASTER_CULL_NONE | ENG_DRAW_MODE_Y_TOP_LEFT )
#define ENG_DRAW_MODE_2D_LB ( ENG_DRAW_MODE_2D_ABS | ENG_DRAW_MODE_RASTER_CULL_NONE )

//===============================================================================
// draw_vertex
//===============================================================================

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( f32 x, f32 y, f32 z, f32 u, f32 v, xcolor Color ) : 
    m_X(x), m_Y(y), m_Z(z), m_U(u), m_V(v), m_Color(Color)
{ }

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( const xvector3d& V, f32 u, f32 v, xcolor Color ) : 
    m_X(V.m_X), m_Y(V.m_Y), m_Z(V.m_Z), m_U(u), m_V(v), m_Color(Color)
{ }

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( const xvector3d& V, xcolor Color ) : 
    m_X(V.m_X), m_Y(V.m_Y),m_Z(V.m_Z), m_U(0), m_V(0), m_Color(Color)
{ }

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( const xvector3d& V ) : 
    m_X(V.m_X), m_Y(V.m_Y),m_Z(V.m_Z), m_U(0), m_V(0), m_Color(0xff0000ff)
{ }

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( f32 x, f32 y, f32 z ) : 
    m_X(x), m_Y(y),m_Z(z), m_U(0), m_V(0), m_Color(0xff0000ff)
{ }

//-------------------------------------------------------------------------------

inline
draw_vertex::draw_vertex( f32 x, f32 y, f32 z, xcolor Color ) : 
    m_X(x), m_Y(y),m_Z(z), m_U(0), m_V(0), m_Color(Color)
{ }

//-------------------------------------------------------------------------------

inline
void draw_vertex::setup( const xvector3d& V, f32 u, f32 v, xcolor Color )
{
    m_X     = V.m_X;
    m_Y     = V.m_Y;
    m_Z     = V.m_Z;
    m_U     = u;
    m_V     = v;
    m_Color = Color;
}

//-------------------------------------------------------------------------------

inline
void draw_vertex::setup( const xvector3d& V, xcolor Color )
{
    m_X     = V.m_X;
    m_Y     = V.m_Y;
    m_Z     = V.m_Z;
    m_U     = 0;
    m_V     = 0;
    m_Color = Color;
}

//-------------------------------------------------------------------------------

inline
void draw_vertex::setup( const xvector3d& V )
{
    m_X     = V.m_X;
    m_Y     = V.m_Y;
    m_Z     = V.m_Z;
    m_U     = 0;
    m_V     = 0;
    m_Color.Set(~0);
}

//-------------------------------------------------------------------------------
inline
void draw_vertex::setup( f32 x, f32 y, f32 z )
{
    m_X     = x;
    m_Y     = y;
    m_Z     = z;
    m_U     = 0;
    m_V     = 0;
    m_Color.Set(~0);
}

//-------------------------------------------------------------------------------

inline
void draw_vertex::setup( f32 x, f32 y, f32 z, xcolor Color )
{
    m_X     = x;
    m_Y     = y;
    m_Z     = z;
    m_U     = 0;
    m_V     = 0;
    m_Color = Color;
}

//-------------------------------------------------------------------------------

inline
void draw_vertex::setup( f32 x, f32 y, f32 z, f32 u, f32 v, xcolor Color )
{
    m_X     = x;
    m_Y     = y;
    m_Z     = z;
    m_U     = u;
    m_V     = v;
    m_Color = Color;
}

