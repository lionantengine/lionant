
#include "eng_Base.h"
#include "tchar.h"
#include "eng_DX10Private.h"

#pragma comment( lib, "d3dx10d" ) 
#pragma comment( lib, "d3d10" ) 
#pragma comment( lib, "d3d10" )
#pragma comment( lib, "dxgi" )

/*
#pragma comment( lib, "dsound" ) 
#pragma comment( lib, "dinput8" ) 
#pragma comment( lib, "dxerr9" ) 
#pragma comment( lib, "d3dxof" ) 
#pragma comment( lib, "dxguid" ) 
#pragma comment( lib, "winmm" ) 
#pragma comment( lib, "kernel32" ) 
#pragma comment( lib, "user32" ) 
#pragma comment( lib, "gdi32" ) 
#pragma comment( lib, "winspool" ) 
#pragma comment( lib, "comctl32" ) 
#pragma comment( lib, "comdlg32" ) 
#pragma comment( lib, "advapi32" ) 
#pragma comment( lib, "shell32" ) 
#pragma comment( lib, "ole32" ) 
#pragma comment( lib, "oleaut32" ) 
#pragma comment( lib, "uuid" )
*/
//-------------------------------------------------------------------------------
// defines
//-------------------------------------------------------------------------------

#define ENG_FONT_SIZEX      7
#define ENG_FONT_SIZEY      14
#define safe_release( A )   if( A ){ A->Release(); A = NULL; }

//-------------------------------------------------------------------------------
// pc_swap_chain
//-------------------------------------------------------------------------------
X_MS_ALIGNMENT(16) struct pc_swap_chain
{
                         pc_swap_chain   ( void );
                        ~pc_swap_chain   ( void );

    void                Init                    ( HWND hWnd, DXGI_FORMAT BackBufferFormat, xbool bFullScreen );
    void                UpdateFPS               ( void );
    f32                 GetFPS                  ( void );
    void                BeginScene              ( void );
    void                EndScene                ( void );
    void                CreateBuffers           ( void );
    void                ResizeBuffers           ( s32 Width, s32 Height );
    u32                 GetCurrentFrameNumber   ( void ) { return m_FrameNumber; }
    void                SetActiveView           ( const eng_view& View );
    void                PageFlip                ( void );
    void                SetActive               ( void );

    eng_view                        m_ActiveView;
    xcolor                          m_BackgroundColor;
    DXGI_FORMAT                     m_BackBufferFormat;
    IDXGISwapChain*                 m_pSwapChain;
    ID3D10Texture2D*                m_pBackBufferTexture;
    ID3D10RenderTargetView*         m_pBackBufferRTView;
    ID3D10Texture2D*                m_pDepthStencil;
    ID3D10DepthStencilView*         m_pDepthStencilView;
    HWND                            m_hWnd;
    u32                             m_FrameNumber;
    s32                             m_XRes;
    s32                             m_YRes;

    //
    // Stats
    //
    xbool                           m_bPrintFPS;
    xtick                           m_FPSFrameTime[8];
    xtick                           m_FPSLastTime;
    s32                             m_FPSIndex;
    xtimer                          m_CPUTIMER;
    f64                             m_CPUMS;
    f64                             m_IMS;

    //
    // States
    // 
    xbool                           m_bBeginScene;
    xbool                           m_bEngBegin;
};

//-------------------------------------------------------------------------------
// pc_engine
//-------------------------------------------------------------------------------
struct pc_engine
{
                    pc_engine                       ( void );

    //-------------------------------------------------------------------------------
    // Operational functions
    //-------------------------------------------------------------------------------

    void            PageFlip                        ( void );
    void            SetActiveView                   ( const eng_view& View );
    eng_view&		GetActiveView                   ( void);
    void            ResizeBuffers                   ( s32 Width, s32 Height );
    u32             GetCurrentFrameNumber           ( void );
    f32             GetFPS					        ( void );
    void            BeginScene                      ( void );
    void            EndScene                        ( void );
    void            SetActiveSwapChain              ( xhandle hChain );
    void            SetClearColor                   ( xcolor Color );
	void			SetFPSDisplay					( xbool FPS_On );
	ID3D10Texture2D* GetCurrentBackBuffer           ( void );
	ID3D10DepthStencilView* GetCurrentDepthStencilView (void );

    //-------------------------------------------------------------------------------
    // Swap chain functions
    //-------------------------------------------------------------------------------


    xhandle         CreateSwapChain                 ( HWND hWnd );
    void            ReleaseSwapChain                ( xhandle hChain );
	void			RebindCurrentSwapChain			( void );

    //-------------------------------------------------------------------------------
    // Initialization
    //-------------------------------------------------------------------------------

    void            PresetSetRes                    ( s32 Width, s32 Height );
    void            PresetGetRes                    ( s32& XRes, s32& YRes );
    void            PresetMode                      ( preset_mode Mode );
    void            PresetSetBackBufferFormat       ( u32 BackBufferFormat );
	void			PresetSetMultisampling			( u32 NumSamples, u32 Quality );
    void            Init                            ( void );
    void            Kill                            ( void );

    HWND            CreateMainWindow                ( int nCmdShow );
    HRESULT         CreateBuffers                   ( void );
    void            BindBackBufferToDevice          ( ID3D10RenderTargetView* pRTV, xbool bClear );
    void            CreateDrawlayer                 ( void );
    void            Initialize                      ( void );

    //-------------------------------------------------------------------------------
    // Entry
    //-------------------------------------------------------------------------------

    void            CreateDevice                    ( void );
    void            ConvertComandLine               ( s32* pargc, char* argv[], LPSTR lpCmdLine );
    void            EntryPoint                      ( s32& argc, char**& argv, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow );
    s32             ExitPoint                       ( void );

    //-------------------------------------------------------------------------------
    // Variables
    //-------------------------------------------------------------------------------

    //
    // Global app state
    //
    HINSTANCE                       m_hInstance;            // Application Instance
    s32                             m_Argc;                 // Argument count
    char*                           m_argvBuff[256];        // Arguments past to the entry function
    int                             m_nCmdShow;             // Initial show arguments

    //
    // Presets
    //
    preset_mode                     m_PresetMode;           
    s32                             m_PresetXRes;           
    s32                             m_PresetYRes;
    u32                             m_PresetBackBufferFormat;     // The format for the back buffer
	u32								m_PresetMultisampleNumSamples; // Number of samples for multisample AA
	u32								m_PresetMultisampleQuality; // Quality for multisample AA

    //
    // swap chain
    //
    xharray<pc_swap_chain>          m_SwapChainList;        // List of known swap_chains
    xhandle                         m_hActiveChain;

    //
    // text 
    //
    ID3DX10Font*                                m_pFont; 
    ID3D10RasterizerState*                      m_pTextRasterState;

};

//-------------------------------------------------------------------------------
// Vars
//-------------------------------------------------------------------------------
static pc_engine*   s_pEng          = NULL;
ID3D10Device*       g_pd3dDevice    = NULL;

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// pc_swap_chain Functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

pc_swap_chain::pc_swap_chain( void ) : 
    m_pSwapChain( NULL ), 
    m_pBackBufferTexture( NULL ), 
    m_pBackBufferRTView( NULL ), 
    m_pDepthStencil( NULL ), 
    m_pDepthStencilView( NULL ),
    m_FrameNumber( 0 )
{
    ASSERT( x_IsAlign( this, 16 ) );

    //
    // Initialize the fps
    //
    m_FPSFrameTime[0] = 0;
    m_FPSFrameTime[1] = 0;
    m_FPSFrameTime[2] = 0;
    m_FPSFrameTime[3] = 0;
    m_FPSFrameTime[4] = 0;
    m_FPSFrameTime[5] = 0;
    m_FPSFrameTime[6] = 0;
    m_FPSFrameTime[7] = 0;

    m_bPrintFPS   = TRUE;
    m_FPSLastTime = x_GetTime();
    m_FPSIndex    = 0;
    m_CPUMS       = 0;
    m_IMS         = 0;

    //
    // Scene state
    //
    m_bBeginScene = FALSE;
    m_bEngBegin   = FALSE;

    //
    // Set the background color
    //
    m_BackgroundColor.SetFromRGBA( 0.5f, 0.5f, 0.5f, 1.0f );
}

//-------------------------------------------------------------------------------

pc_swap_chain::~pc_swap_chain( void )
{
    // Release the back buffer texture
    safe_release( m_pBackBufferTexture )

    // Release the depth stencil view
    safe_release( m_pDepthStencilView )

    // Release the backbuffer view
    safe_release( m_pBackBufferRTView )

    // Release the depth stencil texture
    safe_release( m_pDepthStencil )

    // Release the chain
	// We have to be a little careful here.
	// If the swap chain is in full-screen mode, it must be switched to window mode before being released
	if( m_pSwapChain )
	{
		BOOL FullScreenState;
		IDXGIOutput *pTarget; //we don't need this, but whatever
		m_pSwapChain->GetFullscreenState( &FullScreenState, &pTarget );
		if( FullScreenState )
		{
			m_pSwapChain->SetFullscreenState( FALSE, NULL );
		}

		//ok, we're sure we're not in full-screen state now.
		//can finally release the swap chain
		safe_release( m_pSwapChain )
	}
}

//-------------------------------------------------------------------------------

void pc_swap_chain::UpdateFPS( void )
{
    xtick CurrentTime;

    CurrentTime                     = x_GetTime();
    m_FPSFrameTime[ m_FPSIndex ]    = CurrentTime - m_FPSLastTime;
    m_FPSLastTime                   = CurrentTime;
    m_FPSIndex                     += 1;
    m_FPSIndex                     &= 0x07;
}

//-------------------------------------------------------------------------------

f32 pc_swap_chain::GetFPS( void )
{
    xtick Sum = m_FPSFrameTime[0] +
                m_FPSFrameTime[1] +
                m_FPSFrameTime[2] +
                m_FPSFrameTime[3] +
                m_FPSFrameTime[4] +
                m_FPSFrameTime[5] +
                m_FPSFrameTime[6] +
                m_FPSFrameTime[7];

    return( (f32)(s32)(((8.0f / x_TicksToMs( Sum )) * 1000.0f) + 0.5f) );
}       

//-------------------------------------------------------------------------------

void pc_swap_chain::Init( HWND hWnd, DXGI_FORMAT BackBufferFormat, xbool bFullScreen )
{
    HRESULT hr;

    // Initialize the window
    m_hWnd             = hWnd;
    m_BackBufferFormat = (DXGI_FORMAT)BackBufferFormat;
    
    // Set the swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof(sd) );

	ASSERT( s_pEng );

	// If we're doing full-screen, we should get our size from the presets,
	// Not the window size. Otherwise, we'll get weird resolutions like 1280x1000 instead of 1280x1024
	if( bFullScreen )
	{
		ASSERT( s_pEng != NULL );
		m_XRes = s_pEng->m_PresetXRes;
		m_YRes = s_pEng->m_PresetYRes;
	}else
	{
	    // Get the window rectangle
	    RECT Rect;
		GetClientRect( hWnd, &Rect );

		m_XRes = x_Max( Rect.right  - Rect.left, (long)4 );
		m_YRes = x_Max( Rect.bottom - Rect.top,  (long)4 );
	}

    sd.BufferCount              = 1;
    sd.BufferDesc.Width         = m_XRes;
    sd.BufferDesc.Height        = m_YRes;
    sd.BufferDesc.Format        = m_BackBufferFormat;
    sd.BufferDesc.RefreshRate.Numerator   = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage              = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow             = hWnd;
    sd.SampleDesc.Count         = s_pEng->m_PresetMultisampleNumSamples;
    sd.SampleDesc.Quality       = s_pEng->m_PresetMultisampleQuality;
    sd.Windowed                 = !bFullScreen;
	sd.Flags					= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH ;

    IDXGIFactory* pFactory;
    hr = CreateDXGIFactory( __uuidof(pFactory), reinterpret_cast< void ** >( &pFactory ) );

    if( FAILED(hr ) )
    {
        x_throw( "DX ERROR: Unable to create a factory" );
    }

    hr = pFactory->CreateSwapChain( g_pd3dDevice, &sd, &m_pSwapChain );
    safe_release( pFactory );

    if( FAILED(hr)  )   
    {
        x_throw( "DX ERROR: Unable to create the DX10 Device" );
    }


    //
    // Create the back buffer and zbuffer
    //
    CreateBuffers();
}

//-------------------------------------------------------------------------------

void pc_swap_chain::ResizeBuffers( s32 Width, s32 Height )
{
	// IMPORTANT NOTE:
	// ResizeBuffers will be called by the system even when there is no device,
	// and no swap chain. A very common example of this is when first switching
	// to full-screen mode. The CreateSwapChain call itself triggers a RESIZE
	// message to be sent to WndProc. But, obviously, the swap chain hasn't
	// been created yet.
	// THEREFORE, we silently return from this function if the swap chain
	// or device are not ready.
	if( m_pSwapChain == NULL || g_pd3dDevice == NULL)
	{
		return;
	}

    //
    // Handle a few crazy cases from MFC
    //
    if( Width  > 2048 || Width  <= 0 )  Width  = 4;
    if( Height > 2048 || Height <= 0 )  Height = 4;

    //
    // Make sure everything is release
    //

    // unbound everything
    g_pd3dDevice->OMSetRenderTargets( 0, NULL, NULL );

    // Release the back buffer texture
    safe_release( m_pBackBufferTexture );

    // Release the depth stencil view
    safe_release( m_pDepthStencilView );

    // Release the backbuffer view
    safe_release( m_pBackBufferRTView );

    // Release the depth stencil texture
    safe_release( m_pDepthStencil );

    //
    // Resize chain
    //
    m_XRes = Width;
    m_YRes = Height;
    m_ActiveView.RefreshViewport();
    if( m_pSwapChain )
    {
        m_pSwapChain->ResizeBuffers( 1, Width, Height, m_BackBufferFormat, 0 );
    }

    //
    // Recreate the back-buffer and the zbuffer
    //
    CreateBuffers();
}

//-------------------------------------------------------------------------------

void pc_swap_chain::CreateBuffers( void )
{
    HRESULT hr;
    
    //
    // Do we have an active chain?
    //
    ASSERT( m_pSwapChain );
	ASSERT( s_pEng );

    //
    // Create a render target view
    //
    hr = m_pSwapChain->GetBuffer( 0, __uuidof( ID3D10Texture2D ), (LPVOID*)&m_pBackBufferTexture );
    if( FAILED(hr) )
    {
        x_throw( "DX ERROR: Unable to get the back buffer from the swapchain. " );
    }

    // Create the render target view
    hr = g_pd3dDevice->CreateRenderTargetView( m_pBackBufferTexture, NULL, &m_pBackBufferRTView );
    if( FAILED( hr ) )
    {
        x_throw( "DX ERROR: Unable to create the render target view. " );
    }

    //
    // Create depth stencil texture
    //
    D3D10_TEXTURE2D_DESC descDepth;
    descDepth.Width             = m_XRes;
    descDepth.Height            = m_YRes;
    descDepth.MipLevels         = 1;
    descDepth.ArraySize         = 1;
    descDepth.Format            = DXGI_FORMAT_D24_UNORM_S8_UINT;//DXGI_FORMAT_D32_FLOAT;
    descDepth.SampleDesc.Count   = s_pEng->m_PresetMultisampleNumSamples;
    descDepth.SampleDesc.Quality = s_pEng->m_PresetMultisampleQuality;
    descDepth.Usage             = D3D10_USAGE_DEFAULT;
    descDepth.BindFlags         = D3D10_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags    = 0;
    descDepth.MiscFlags         = 0;

    hr = g_pd3dDevice->CreateTexture2D( &descDepth, NULL, &m_pDepthStencil );
    if( FAILED(hr) )
    {
        x_throw( "DX ERROR: Unable to create the zbuffer. " );
    }

    // Create the depth stencil view
    D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
    descDSV.Format              = descDepth.Format;
	//ViewDimension type depends on multisampling
	if(s_pEng->m_PresetMultisampleNumSamples > 1 )
	{
		descDSV.ViewDimension       = D3D10_DSV_DIMENSION_TEXTURE2DMS;
	}else
	{
		descDSV.ViewDimension       = D3D10_DSV_DIMENSION_TEXTURE2D;
	}
    descDSV.Texture2D.MipSlice  = 0;

    hr = g_pd3dDevice->CreateDepthStencilView( m_pDepthStencil, &descDSV, &m_pDepthStencilView );
    if( FAILED(hr) )
    {
        x_throw( "DX ERROR: Unable to create the zbuffer view. " );
    }

    //
    // Setup the viewport
    //
    SetActive();
}

//--------------------------------------------------------------------------------------

void pc_swap_chain::SetActiveView( const eng_view& View )
{
    //
    // Set the new active viewport
    //
    m_ActiveView = View;

    //
    // Set viewport
    //
    const xirect Rect = View.GetViewport();

    D3D10_VIEWPORT vp;
    vp.Width    = Rect.GetWidth();
    vp.Height   = Rect.GetHeight();
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = Rect.m_Left;
    vp.TopLeftY = Rect.m_Top;
    g_pd3dDevice->RSSetViewports( 1, &vp );

    //
    // Set the Scissor rect
    //
    D3D10_RECT rects[1];
    rects[0].left   = Rect.m_Left;
    rects[0].right  = Rect.m_Right;
    rects[0].top    = Rect.m_Top;
    rects[0].bottom = Rect.m_Bottom;

    g_pd3dDevice->RSSetScissorRects( 1, rects );    
}

//--------------------------------------------------------------------------------------

void pc_swap_chain::PageFlip( void )
{
    m_CPUTIMER.Stop();
    m_CPUMS = m_CPUTIMER.ReadMs();

    xtimer InternalTime;
    InternalTime.Start();

    // Let draw handle its drawing
    _eng_DrawPageFlip();


    //
    // Handle the FPS
    //
    if( m_bPrintFPS ) 
    {
		 if(m_XRes < 256)
		 {
			 // if the screen res is really small (as in GI preprocessing), only display FPS
        x_printfxy( 0, 0, "FPS:%1.1f\n", GetFPS() );
		 }else
		 {
        x_printfxy( 0, 0, "CPU:%1.1f  Pageflip:%1.1f  FPS:%1.1f\n",
            m_CPUMS, 
            m_IMS,
            GetFPS() );
		 }
    }

    //
    // Do actual page flip
    //
    m_pSwapChain->Present( 0, 0 );
    m_bEngBegin   = FALSE;

    //
    // Update stats
    //
    UpdateFPS();

    InternalTime.Stop();
    m_IMS = InternalTime.ReadMs();
    m_CPUTIMER.Reset();
    m_CPUTIMER.Start();

    //
    // increment frame number
    //
    m_FrameNumber++;
}

//--------------------------------------------------------------------------------------

void pc_swap_chain::BeginScene( void )
{
    ASSERT( m_bEngBegin == FALSE );
    if( m_bBeginScene == FALSE )
    {
        // Clear color buffer
        //float ClearColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f }; // red,green,blue,alpha
        xvector4 V;
        m_BackgroundColor.BuildRGBA( V.m_X, V.m_Y, V.m_Z, V.m_W );
        g_pd3dDevice->ClearRenderTargetView( m_pBackBufferRTView, (f32*)&V );

        // Clear the depth buffer to 1.0 (max depth)
        g_pd3dDevice->ClearDepthStencilView( m_pDepthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0 );

        // Okay we are in
        m_bBeginScene = TRUE;
    }

    m_bEngBegin   = TRUE;
}

//--------------------------------------------------------------------------------------

void pc_swap_chain::EndScene( void )
{
    ASSERT( m_bBeginScene );
    m_bBeginScene = FALSE;
}

//--------------------------------------------------------------------------------------

void pc_swap_chain::SetActive( void )
{
    //
    // Get window size
    //
    RECT Rect;
    GetClientRect( m_hWnd, &Rect );

    s32 XRes = x_Max( Rect.right  - Rect.left, (long)4 );
    s32 YRes = x_Max( Rect.bottom - Rect.top,  (long)4 );

    if( XRes  > 2048 || XRes  <= 0 )  XRes = 4;
    if( YRes  > 2048 || YRes  <= 0 )  YRes = 4;

    if( m_XRes != XRes || YRes != m_YRes )
    {
        ResizeBuffers( XRes, YRes );
        return;
    }

    //
    // Set the rt view to render
    //
    g_pd3dDevice->OMSetRenderTargets( 1, &m_pBackBufferRTView, m_pDepthStencilView );

    // Set active view
    SetActiveView( m_ActiveView );
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// pc_engine Functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

pc_engine::pc_engine( void ) :
    m_hInstance( 0 ),
    m_nCmdShow( 0 ),
    m_PresetXRes( 1024 ),
    m_PresetYRes( 768 ),
    m_PresetMode( PRESET_MODE_DEFAULTS ),
    m_PresetBackBufferFormat(DXGI_FORMAT_R8G8B8A8_UNORM),
	m_PresetMultisampleNumSamples( 1 ),
	m_PresetMultisampleQuality( 0 ),
    m_pFont( NULL ),
    m_pTextRasterState( NULL ),
    m_SwapChainList( XMEM_FLAG_ALIGN_16B )
{
    ASSERT( x_IsAlign( this, 16 ) );

    m_hActiveChain.SetNull();
}

//-------------------------------------------------------------------------------

void pc_engine::ConvertComandLine( s32* pargc, char* argv[], LPSTR lpCmdLine )
{
    s32 argc = 0;

    if( *lpCmdLine )
    {
        do
        {
            // Skip any spaces in between
            while( *lpCmdLine == ' ' ) 
            {
                *lpCmdLine = 0;
                lpCmdLine++;
            }

            // We are done
            if( *lpCmdLine == 0 )
                break;

            if( *lpCmdLine == '"' )
            {
                *lpCmdLine = 0;
                lpCmdLine++;

                if( *lpCmdLine != 0 )
                {
                    argv[argc] = lpCmdLine;
                    argc++;

                    do
                    {
                        lpCmdLine++;
                    } while( *lpCmdLine !='"' && *lpCmdLine );
                }

                if( *lpCmdLine == '"' )
                {
                    *lpCmdLine = 0;
                    lpCmdLine++;
                    if( *lpCmdLine != 0 )
                    {
                        *lpCmdLine=0;
                        lpCmdLine++;
                    }
                }
            }
            else
            {            
                argv[argc] = lpCmdLine;
                argc++;

                while( *lpCmdLine != ' ' && *lpCmdLine ) 
                    lpCmdLine++;

                if( *lpCmdLine != 0 )
                {
                    *lpCmdLine = 0;
                    lpCmdLine++;
                }
            }

        } while( 1 );
    }

    *pargc = argc;
}


\
//-------------------------------------------------------------------------------

void pc_engine::CreateDevice( void )
{
    HRESULT hr;
    UINT    createDeviceFlags = 0;

#ifdef _DEBUG
    createDeviceFlags |= D3D10_CREATE_DEVICE_DEBUG;
#endif

    D3D10_DRIVER_TYPE driverTypes[] = 
    {
        D3D10_DRIVER_TYPE_HARDWARE,
        D3D10_DRIVER_TYPE_REFERENCE,
    };
    s32     numDriverTypes = sizeof(driverTypes) / sizeof(driverTypes[0]);

    // Now try to create the device
    for( s32 i=0; i<numDriverTypes; i++ )
    {
        hr = D3D10CreateDevice( 
            NULL,                               // IDXGIAdapter*            pAdapter        - 
            driverTypes[i],                     // D3D10_DRIVER_TYPE        DriverType      - 
            NULL,                               // HMODULE                  Software        - Reserved. Should always be NULL. 
            createDeviceFlags,                  // UINT                     Flags           - D3D10_CREATE_DEVICE_SWITCH_TO_REF 
            D3D10_SDK_VERSION,                  // UINT                     SDKVersion      -
            &g_pd3dDevice );                    // ID3D10Device**           ppDevice        - 

        if( SUCCEEDED( hr ) )
            break;
    }

    //
    // Create a font
    //
    hr = D3DX10CreateFontA( g_pd3dDevice,               // D3D Device
                            ENG_FONT_SIZEY,             // Font height
                            0,                          // Font width
                            FW_NORMAL,                  // Font Weight
                            1,                          // MipLevels
                            false,                      // Italic
                            DEFAULT_CHARSET,            // CharSetT, 
                            OUT_DEFAULT_PRECIS,         // OutputPrecision, 
                            ANTIALIASED_QUALITY,        // QualityALITY, 
                            FIXED_PITCH|FF_DONTCARE,    // PitchAndFamily 
                            "Courier New",              // pFacename,
                            &m_pFont );                 // ppFont
    if( FAILED(hr) )
    {
        x_throw( "Failed to create text" );
    }

    x_SetFunctionPrintFXY( OutputTextXY );

    // Create a raster stage for the text
    D3D10_RASTERIZER_DESC rasterizerState;
    rasterizerState.FrontCounterClockwise   = true;
    rasterizerState.DepthBias               = false;
    rasterizerState.DepthBiasClamp          = 0;
    rasterizerState.SlopeScaledDepthBias    = 0;
    rasterizerState.DepthClipEnable         = true;
    rasterizerState.ScissorEnable           = true;
    rasterizerState.MultisampleEnable       = false;
    rasterizerState.AntialiasedLineEnable   = false;
    rasterizerState.CullMode                = D3D10_CULL_NONE;
    rasterizerState.FillMode                = D3D10_FILL_SOLID;
    g_pd3dDevice->CreateRasterizerState( &rasterizerState, &m_pTextRasterState );        
}

//-------------------------------------------------------------------------------

xhandle pc_engine::CreateSwapChain( HWND hWnd )
{
    xhandle hSwapChain;

    pc_swap_chain& SwapChain = m_SwapChainList.append( hSwapChain );

    // Make it as the active chain
    m_hActiveChain = hSwapChain;

    // Initialize it
    SwapChain.Init( hWnd, (DXGI_FORMAT)m_PresetBackBufferFormat, x_FlagIsOn( m_PresetMode, PRESET_MODE_FULL_SCREEN ) );

    return hSwapChain;
}


//-------------------------------------------------------------------------------

void pc_engine::EntryPoint( s32& argc, char**& argv, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    //
    // Set the initial variables
    //
    m_nCmdShow  = nCmdShow;
    m_hInstance = hInstance;

    //
    // Convert the command line to something that people can used
    //
    ConvertComandLine( &argc, m_argvBuff, (LPSTR)GetCommandLine() );
    m_Argc = argc;
	 argv = m_argvBuff;
    
    //
    // Create the DX device
    //
    CreateDevice();

    //
    // Okay we should be ready to kick the control back to the user
    //
}

//-------------------------------------------------------------------------------

s32 pc_engine::ExitPoint( void )
{
    //
    // Lets set the device to a ready to be kill state
    //
    if( g_pd3dDevice ) g_pd3dDevice->ClearState();

    //
    // Free up all the chains
    //
    m_SwapChainList.DeleteAllNodes();

    //
    // Take care of the text
    //    
    safe_release( m_pFont );
    safe_release( m_pTextRasterState );

    //
    // Kill the device
    //
    safe_release( g_pd3dDevice );

    return 0;
}

//-------------------------------------------------------------------------------

void pc_engine::PresetSetRes( s32 Width, s32 Height )
{
    m_PresetXRes = Width;
    m_PresetYRes = Height;
}

//-------------------------------------------------------------------------------

void pc_engine::PresetGetRes( s32& Width, s32& Height )
{
    Width  = m_PresetXRes;
    Height = m_PresetYRes;
}

//-------------------------------------------------------------------------------

void pc_engine::PresetMode( preset_mode Mode )
{
    m_PresetMode = Mode;
}

//-------------------------------------------------------------------------------

void pc_engine::PresetSetBackBufferFormat( u32 BackBufferFormat )
{
    m_PresetBackBufferFormat = BackBufferFormat;
}

//-------------------------------------------------------------------------------
void pc_engine::PresetSetMultisampling( u32 NumSamples, u32 Quality )
{
	//numsamples cannot be less than 1
	if(NumSamples < 1) 
	{
		NumSamples = 1;
	}
	//quality must be zero if NumSamples = 1
	if(NumSamples == 1)
	{
		Quality = 0;
	}

	m_PresetMultisampleNumSamples = NumSamples;
	m_PresetMultisampleQuality = Quality;
}


//-------------------------------------------------------------------------------

u32 pc_engine::GetCurrentFrameNumber( void )
{
    ASSERT( m_hActiveChain.IsValid() );
    return m_SwapChainList(m_hActiveChain).m_FrameNumber;
}

//-------------------------------------------------------------------------------

f32 pc_engine::GetFPS( void )
{
	ASSERT( m_hActiveChain.IsValid() );
	return m_SwapChainList(m_hActiveChain).GetFPS();
}

//-------------------------------------------------------------------------------

void pc_engine::Init( void )
{
    //
    // Create our own window if we have to
    //
    if( x_FlagIsOn( m_PresetMode, PRESET_MODE_CUSTOM_WINDOW ) == FALSE )
    {
        HWND hWnd;

        hWnd = CreateMainWindow( m_nCmdShow );
        if( hWnd == 0 )
        {
            x_throw( "Unable to create the Main Window" );
        }

        //
        // Create the DX device
        //
        CreateSwapChain( hWnd );
    }


    //
    // Activate other sub-systems
    //
    _eng_DrawInit();
}

//-------------------------------------------------------------------------------

void pc_engine::Kill( void )
{
    //
    // DeActivate other sub-systems
    //
    _eng_DrawKill();
}

//Custm wndproc handler (optional)
static wndproc_callback sCustomWndProc = NULL;
void _eng_SetCustomWndProc(wndproc_callback CustomWndProc)
{
	sCustomWndProc = CustomWndProc;
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    PAINTSTRUCT ps;
    HDC hdc;

	//call custom handler first if it exists
	if( sCustomWndProc )
	{
		sCustomWndProc(hWnd, message, wParam, lParam);
	}

    switch (message) 
    {
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    
    case WM_SIZE:
        {
            s32 Width  = LOWORD(lParam);
            s32 Height = HIWORD(lParam);

            // Resize the buffers
            _eng_ResizeBuffers ( Width, Height );
        }    
        // *** FALL THROW
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

//-------------------------------------------------------------------------------

HWND pc_engine::CreateMainWindow( int nCmdShow )
{
    HWND Hwnd;

    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize         = sizeof(WNDCLASSEX); 
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = m_hInstance;
    wcex.hIcon          = 0;//LoadIcon( wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = _T("DX10-APP");
    wcex.hIconSm        = 0;//LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
    if( !RegisterClassEx(&wcex) )
        return 0;

    // Create window
    RECT rc = { 0, 0, m_PresetXRes, m_PresetYRes };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    Hwnd = CreateWindow( wcex.lpszClassName, _T("Direct3Dx10 App"), WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, wcex.hInstance, NULL);
    if( !Hwnd )
        return 0;

    ShowWindow( Hwnd, nCmdShow );

    return Hwnd;
}

//--------------------------------------------------------------------------------------

eng_view& pc_engine::GetActiveView( void )
{
    ASSERT( m_hActiveChain.IsValid() );
    return m_SwapChainList( m_hActiveChain ).m_ActiveView;
}

//--------------------------------------------------------------------------------------

void pc_engine::BeginScene( void )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).BeginScene();
}

//--------------------------------------------------------------------------------------

void pc_engine::EndScene( void )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).EndScene();
}

//--------------------------------------------------------------------------------------

void pc_engine::PageFlip( void )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).PageFlip();
}

//--------------------------------------------------------------------------------------

void pc_engine::SetActiveView( const eng_view& View )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).SetActiveView( View );
}

//--------------------------------------------------------------------------------------

void pc_engine::ResizeBuffers( s32 Width, s32 Height )
{
    if( m_hActiveChain.IsValid() == FALSE )
        return;
    m_SwapChainList( m_hActiveChain ).ResizeBuffers( Width, Height );
}

//--------------------------------------------------------------------------------------

void pc_engine::SetActiveSwapChain( xhandle hChain )
{
    m_hActiveChain = hChain;
    m_SwapChainList( m_hActiveChain ).SetActive();
}

//--------------------------------------------------------------------------------------

void pc_engine::ReleaseSwapChain( xhandle hChain )
{
    if( m_hActiveChain == hChain )
    {
        m_hActiveChain.SetNull();
        g_pd3dDevice->OMSetRenderTargets( 0, NULL, NULL );
    }
    else
    {
        ASSERT( m_hActiveChain.IsNull() );
    }
    
    m_SwapChainList.DeleteByHandle( hChain );
}

//--------------------------------------------------------------------------------------
void pc_engine::RebindCurrentSwapChain( void )
{
   ASSERT( m_hActiveChain.IsValid() );
   m_SwapChainList( m_hActiveChain ).SetActive();
}

//--------------------------------------------------------------------------------------

void pc_engine::SetClearColor( xcolor Color )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).m_BackgroundColor = Color;

}

//--------------------------------------------------------------------------------------

void pc_engine::SetFPSDisplay( xbool FPS_On )
{
    ASSERT( m_hActiveChain.IsValid() );
    m_SwapChainList( m_hActiveChain ).m_bPrintFPS = FPS_On;
}

//--------------------------------------------------------------------------------------

//Get the current backbuffer texture from the active swap-chain.
//Returns NULL, if none.
//Meant for "local" use and so does not increment reference counter
ID3D10Texture2D *pc_engine::GetCurrentBackBuffer( void )
{
    ASSERT( m_hActiveChain.IsValid() );
	
	ID3D10Texture2D *pBackBuffer = m_SwapChainList( m_hActiveChain ).m_pBackBufferTexture;

	return pBackBuffer;
}

//Get the current depth-stencil view from the active swap-chain.
//Returns NULL, if none
ID3D10DepthStencilView *pc_engine::GetCurrentDepthStencilView( void )
{
	ASSERT( m_hActiveChain.IsValid() );
	
	return m_SwapChainList( m_hActiveChain ).m_pDepthStencilView;
}

//===============================================================================
//===============================================================================
//===============================================================================
// Public functions
//===============================================================================
//===============================================================================
//===============================================================================

//--------------------------------------------------------------------------------------

void eng_PresetSetRes( s32 Width,  s32 Height )
{
    ASSERT( s_pEng );
    s_pEng->PresetSetRes(Width,Height);
	eng_SetViewport( 0,0, Width,Height );
}

//--------------------------------------------------------------------------------------

void eng_PresetGetRes( s32& Width, s32& Height )
{
    ASSERT( s_pEng );
    s_pEng->PresetGetRes(Width,Height);
}

//--------------------------------------------------------------------------------------

void eng_PresetSetBackBufferFormat( eng_surface_format BackBufferFormat )
{
    ASSERT( s_pEng );
    s_pEng->PresetSetBackBufferFormat( eng_SurfaceFormatConvertToNative( BackBufferFormat ) );
}

//--------------------------------------------------------------------------------------
void eng_PresetSetMultisampling( u32 NumSamples, u32 Quality )
{
	ASSERT( s_pEng );
	s_pEng->PresetSetMultisampling(NumSamples,Quality);
}

//--------------------------------------------------------------------------------------

void eng_PresetMode( preset_mode Mode )
{
    ASSERT( s_pEng );
    s_pEng->PresetMode(Mode);
}

//--------------------------------------------------------------------------------------

void eng_Init( void )
{
    ASSERT( s_pEng );
    s_pEng->Init();
}

//--------------------------------------------------------------------------------------

void eng_Kill( void )
{
    ASSERT( s_pEng );
    s_pEng->Kill();
}

//--------------------------------------------------------------------------------------

void eng_PageFlip( void )
{
    ASSERT( s_pEng );
    s_pEng->PageFlip();
}

void eng_SetFPSDisplay( xbool FPS_On )
{
	ASSERT( s_pEng );
	s_pEng->SetFPSDisplay( FPS_On );
}

//--------------------------------------------------------------------------------------

u32 eng_GetCurrentFrameNumber( void )
{
    ASSERT( s_pEng );
    return s_pEng->GetCurrentFrameNumber();
}

//--------------------------------------------------------------------------------------

f32 eng_GetFPS( void )
{
	ASSERT( s_pEng );
	return s_pEng->GetFPS();
}

//--------------------------------------------------------------------------------------

void _eng_EntryPoint( s32& argc, char**& argv, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    ASSERT( s_pEng == NULL );

    //
    // Allocate memory for the engine
    //
    s_pEng = x_new( pc_engine, 1, XMEM_FLAG_ALIGN_16B );
    if( s_pEng == NULL )
        x_throw( "Out of memory" );

    ASSERT( x_IsAlign( s_pEng, 16 ) );  

    //
    // Lets call the entry point
    //
    s_pEng->EntryPoint( argc, argv, hInstance, hPrevInstance, lpCmdLine, nCmdShow );
}

//--------------------------------------------------------------------------------------

s32 _eng_ExitPoint( void )
{
    s32 Ret = 0;

    //
    // Free memory
    //
    if( s_pEng )
    {
        Ret = s_pEng->ExitPoint();
        x_delete(s_pEng);
        s_pEng=NULL;
    }

    return Ret;
}

//--------------------------------------------------------------------------------------

void eng_SetActiveView( const eng_view& View )
{
    ASSERT( s_pEng );
    s_pEng->SetActiveView( View );
}

//--------------------------------------------------------------------------------------

eng_view& eng_GetActiveView( void )
{
    ASSERT( s_pEng );
    return s_pEng->GetActiveView();
}

//--------------------------------------------------------------------------------------

void _eng_ResizeBuffers ( s32 Width, s32 Height )
{
    ASSERT( s_pEng );
    return s_pEng->ResizeBuffers( Width, Height );
}

//--------------------------------------------------------------------------------------

void eng_Begin( const char* pTaskName )
{
    ASSERT( s_pEng );
    ASSERT( s_pEng->m_hActiveChain.IsValid() );
    ASSERT( s_pEng->m_SwapChainList(s_pEng->m_hActiveChain).m_bEngBegin == FALSE );
    static const char* s_LastTask = pTaskName;
    s_pEng->BeginScene();           
}

//--------------------------------------------------------------------------------------

void eng_End( void )
{
    ASSERT( s_pEng );
    s_pEng->EndScene();           
}

//--------------------------------------------------------------------------------------

void eng_GetScreenResolution( s32& X, s32& Y )
{
    ASSERT( s_pEng );
    ASSERT( s_pEng->m_hActiveChain.IsValid() );

    pc_swap_chain& SwapChain = s_pEng->m_SwapChainList(s_pEng->m_hActiveChain);
    X = SwapChain.m_XRes;
    Y = SwapChain.m_YRes;

    ASSERT( X > 1 );
    ASSERT( Y > 1 );
}

//--------------------------------------------------------------------------------------

void eng_SetClearColor( xcolor Color )
{
    ASSERT( s_pEng );
    s_pEng->SetClearColor( Color );
}

//--------------------------------------------------------------------------------------

xhandle _eng_MFCCreateSwapChain( HWND hWindow )
{
    ASSERT( s_pEng );
    xhandle hSwapChain = s_pEng->CreateSwapChain( hWindow );

    // Make sure to set the active to null
    s_pEng->m_hActiveChain.SetNull();

    return hSwapChain;
}

//--------------------------------------------------------------------------------------

void _eng_MFCSetActiveSwapChain( xhandle hChain )
{
    ASSERT( s_pEng );
    ASSERT( s_pEng->m_hActiveChain.IsNull() );
    s_pEng->SetActiveSwapChain( hChain );
}

//--------------------------------------------------------------------------------------

void _eng_MFCReleaseSwapChain( xhandle hChain )
{
    ASSERT( s_pEng );
    s_pEng->ReleaseSwapChain( hChain );
}

//--------------------------------------------------------------------------------------

void _eng_MFCDoneWithSwapChain( xhandle hChain )
{
    ASSERT( s_pEng->m_hActiveChain == hChain );

    // Make sure to set the active to null
    s_pEng->m_hActiveChain.SetNull();

    // Set the render target to null
    g_pd3dDevice->OMSetRenderTargets( 0, NULL, NULL );
}

//--------------------------------------------------------------------------------------

void _eng_MFCInitialize( void )
{
    //
    // Allocate memory for the engine
    //
    s_pEng = x_new( pc_engine, 1, XMEM_FLAG_ALIGN_16B );
    if( s_pEng == NULL )
        x_throw( "Out of memory" );

    //
    // lets create the device
    //
    s_pEng->CreateDevice();

    //
    // Start the engine
    //
    eng_PresetMode( PRESET_MODE_CUSTOM_WINDOW );
    eng_Init();

}

//--------------------------------------------------------------------------------------

void _eng_MFCKill( void )
{
    _eng_ExitPoint();
}

//--------------------------------------------------------------------------------------

//Get the current backbuffer texture from the active swap-chain.
//Returns NULL, if none.
ID3D10Texture2D* _eng_DX10GetCurrentBackBuffer( void )
{
    ASSERT( s_pEng );
	return s_pEng->GetCurrentBackBuffer();
}


//--------------------------------------------------------------------------------------

//Get the current backbuffer depth/stencil view from the active swap-chain
//Returns NULL, if none.
ID3D10DepthStencilView* _eng_DX10GetCurrentDepthStencilView( void )
{
	ASSERT( s_pEng );
	return s_pEng->GetCurrentDepthStencilView();
}

//--------------------------------------------------------------------------------------

//Re-bind the current swap chain's backbuffer and depth-texture to the device
void _eng_DX10RebindCurrentSwapChain( void )
{
	ASSERT( s_pEng );
	s_pEng->RebindCurrentSwapChain();
}


struct vp
{
	s32 T, L;
	s32 W, H;
};

xsafe_array<vp,32> s_VP;
s32				   s_cIndex=0;

//--------------------------------------------------------------------------------------

void eng_PopViewport( void )
{
	s_cIndex--;
	eng_SetViewport( s_VP[ s_cIndex ].T, s_VP[ s_cIndex ].L, s_VP[ s_cIndex ].W, s_VP[ s_cIndex ].H );
}

//--------------------------------------------------------------------------------------

void eng_PushViewport( void )
{
	s_cIndex++;
}

//--------------------------------------------------------------------------------------

void eng_SetViewport( s32 Top, s32 Left, s32 Width, s32 Height )
{
	s_VP[ s_cIndex ].L = Left;
	s_VP[ s_cIndex ].T = Top;
	s_VP[ s_cIndex ].W = Width;
	s_VP[ s_cIndex ].H = Height;

	static xbool Hack=0;

	if( Hack )
	{
		s_pEng->GetActiveView().SetViewport( xirect(Left, Top, Left+Width, Top+Height) );
	
		eng_SetActiveView( s_pEng->GetActiveView() );
	}
	Hack=1;
/*
D3D10_VIEWPORT vp;
    vp.Width    = Rect.GetWidth();
    vp.Height   = Rect.GetHeight();
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = Rect.m_Left;
    vp.TopLeftY = Rect.m_Top;
    g_pd3dDevice->RSSetViewports( 1, &vp );

    //
    // Set the Scissor rect
    //
    D3D10_RECT rects[1];
    rects[0].left   = Rect.m_Left;
    rects[0].right  = Rect.m_Right;
    rects[0].top    = Rect.m_Top;
    rects[0].bottom = Rect.m_Bottom;

    g_pd3dDevice->RSSetScissorRects( 1, rects ); 
	*/
}

//--------------------------------------------------------------------------------------

native_surface_format eng_SurfaceFormatConvertToNative( eng_surface_format SurfaceFormat )
{
	switch( SurfaceFormat )
	{
	case ESF_R8G8B8A8:			return DXGI_FORMAT_R8G8B8A8_UNORM;
	case ESF_R8:				return DXGI_FORMAT_R8_UNORM;
	case ESF_D32:				return DXGI_FORMAT_D32_FLOAT;
	case ESF_R32:				return DXGI_FORMAT_R32_UINT;
	case ESF_R8G8:				return DXGI_FORMAT_R8G8_UNORM;
	case ESF_R16G16B16A16:		return DXGI_FORMAT_R16G16B16A16_UNORM;
	case ESF_R10G10B10A2:		return DXGI_FORMAT_R10G10B10A2_UNORM;
	case ESF_D24S8:				return DXGI_FORMAT_D24_UNORM_S8_UINT;
	case ESF_R11G11B10:			return DXGI_FORMAT_R11G11B10_FLOAT;
	}

	return (native_surface_format)~0;
}

//--------------------------------------------------------------------------------------

eng_surface_format eng_SurfaceFormatConvertToNative( native_surface_format SurfaceFormat )
{
	switch( SurfaceFormat )
	{
	case DXGI_FORMAT_R8G8B8A8_UNORM:		return ESF_R8G8B8A8;		
	case DXGI_FORMAT_R8_UNORM:				return ESF_R8;				
	case DXGI_FORMAT_D32_FLOAT:				return ESF_D32;				
	case DXGI_FORMAT_R32_UINT:				return ESF_R32;				
	case DXGI_FORMAT_R8G8_UNORM:			return ESF_R8G8;			
	case DXGI_FORMAT_R16G16B16A16_UNORM:	return ESF_R16G16B16A16;	
	case DXGI_FORMAT_R10G10B10A2_UNORM:		return ESF_R10G10B10A2;		
	case DXGI_FORMAT_D24_UNORM_S8_UINT:		return ESF_D24S8;			
	case DXGI_FORMAT_R11G11B10_FLOAT:		return ESF_R11G11B10;		
	}

	return ESF_NULL;
}

//--------------------------------------------------------------------------------------

eng_surface_format eng_PresetGetBackBufferFormat( void )
{
	return eng_SurfaceFormatConvertToNative( (native_surface_format)s_pEng->m_PresetBackBufferFormat );
}

//--------------------------------------------------------------------------------------
void eng_PresetGetMultisampling( u32 NumSamples, u32 Quality )
{
	
}
