#ifndef ENG_PRIVATE_H
#define ENG_PRIVATE_H

//-------------------------------------------------------------------------------

#include "eng_Base.h"

//-------------------------------------------------------------------------------

struct engine
{
    void*   m_pDraw;


};

extern engine* g_pEngine;

//-------------------------------------------------------------------------------

//Get the current backbuffer texture from the active swap-chain.
//Returns NULL, if none.
ID3D10Texture2D* _eng_DX10GetCurrentBackBuffer( void );

//Get the current backbuffer depth/stencil view from the active swap-chain
ID3D10DepthStencilView* _eng_DX10GetCurrentDepthStencilView( void );

//Re-bind the current swap chain's backbuffer and depth-texture to the device
void _eng_DX10RebindCurrentSwapChain( void );

//set a custom Windows message handler that will be called from the default message-handler
typedef LRESULT (CALLBACK *wndproc_callback)( HWND , UINT , WPARAM , LPARAM  );
void _eng_SetCustomWndProc(wndproc_callback CustomWndProc); 

//===============================================================================
// END
//===============================================================================
#endif