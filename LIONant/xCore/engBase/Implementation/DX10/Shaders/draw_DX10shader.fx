//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
matrix L2C;

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Pos   : POSITION;
    float2 UV    : TEXCOORD;
    float4 Color : COLOR;
};

struct PS_INPUT
{
    float4 Pos   : SV_POSITION;
    float2 UV    : TEXCOORD0;
    float4 Color : COLOR;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output;
    
    output.Pos = mul( input.Pos, L2C );
        
    output.UV.xy      = input.UV.xy;
    output.Color      = input.Color;
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( PS_INPUT input) : SV_Target
{
    return input.Color;
}

//--------------------------------------------------------------------------------------

technique10 draw_2d
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}


