//===============================================================================
// INCLUDES
//===============================================================================

#include "eng_Base.h"
#include "tchar.h"

//===============================================================================
// DEFINES
//===============================================================================

#define VERTEX_BUFFER_COUNT        4000
#define INDEX_BUFFER_COUNT         (VERTEX_BUFFER_COUNT*3) 
#define CMD_BUFFER_COUNT           4000 

//===============================================================================
// STRUCTURES
//===============================================================================

struct draw_cmd
{
    u32                                     m_Mode;                 // Draw mode used for this primitive
    u16                                     m_iPrimitiveList;       // Primitive list for this draw command
    u16                                     m_VertexOffset;         // offset from the vertex buffer to access these primitive vertices
    u16                                     m_IndexOffset;          // offset from the index buffer to access these primitive indices
    u16                                     m_nVertices;            // Number of vertices that this primitive uses
    u16                                     m_nIndices;             // Number of indices that this primitive uses
    s16                                     m_iMatrix;              // Index to the matrix been used
};

//-------------------------------------------------------------------------------

struct draw_primitive_list 
{
                                            draw_primitive_list( void );
                                           ~draw_primitive_list( void );
    u32                                     m_iFrame;               // What frame number was this primitive list last used
    ID3D10Buffer*                           m_pVertexBuffer;        // Vertices
    ID3D10Buffer*                           m_pIndexBuffer;         // Indices
};

//-------------------------------------------------------------------------------

struct draw_shaders
{
                                            draw_shaders( void );
                                           ~draw_shaders( void );
    ID3D10InputLayout*                      m_pVertexLayout;        // Vertex description
    ID3D10EffectTechnique*                  m_pTechnique;           // Technique
};

//-------------------------------------------------------------------------------

struct draw_stats
{
                                            draw_stats( void );
    s32                                     m_nCmds;
    s32                                     m_nTriangles;
    s32                                     m_nLines;
};

//-------------------------------------------------------------------------------

class draw_local
{
public:
                                            draw_local              ( void );
                                           ~draw_local              ( void );
    void                                    Init                    ( void );
    void                                    Kill                    ( void );

    void                                    DrawGetBuffers          ( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices );
    void                                    DrawBufferTriangles     ( void );
    void                                    DrawBufferLines         ( void );

    void                                    Begin                   ( u32 Mode );
    void                                    End                     ( void );
    void                                    PageFlip                ( void );
    void                                    SetL2W                  ( const xmatrix4& M );
    void                                    ClearL2W                ( void );

protected:

    xhandle                                 GetReadyToDumpData      ( s32 nVertices, s32 nIndices, u32 ExtraMode );

protected:

    //
    // Global state variables
    //
    ID3D10Effect*                               m_pEffectFile;              // Effect file
    ID3D10EffectMatrixVariable*                 m_pShaderL2C;               // Local to clip-matrix for the shader

    // Sample state: xsafe_array<ID3D10RasterizerState*, 4>      m_RasterState;
    xsafe_array<ID3D10RasterizerState*, 4>      m_RasterState;
    xsafe_array<ID3D10DepthStencilState*, 3>    m_StencilState;
    xsafe_array<ID3D10BlendState*, 4>           m_BlendState;

    //
    // Lists    
    //
    xsafe_array<draw_shaders,1>                 m_Shaders;                  // List of shaders
    xsafe_array<draw_primitive_list, 8>         m_PrimitiveList;            // List of primitives

    //
    // Commands
    //
    xsafe_array<draw_cmd, CMD_BUFFER_COUNT>     m_Cmd;                  // List of commands
    xsafe_array<xmatrix4, CMD_BUFFER_COUNT>     m_L2W;                  // L2W matrix buffer
    s32                                         m_nCmd;                 // Cmd count used in this list
    s32                                         m_nMatrices;            // Number of matrices used so far

    //
    // Active lists 
    //
    s32                                         m_iActiveCmdList;           // Index of the CmdList that we are trying to use
    u16                                         m_iActivePrimList;          // Which primitive list we are using
    s32                                         m_nActiveVertices;          // Number of Vertices used
    s32                                         m_nActiveIndices;           // Number of Indices used
    u16*                                        m_pActiveIndexBufferMap;    // Access to the active index buffer
    draw_vertex*                                m_pActiveVertexBufferMap;   // Access to the active vertex buffer

    //
    // Mode registers
    //
    xbool                                       m_bDrawBegging;             // Whether we are inside of a begging or not
    u32                                         m_ActiveMode;               // Current active mode
    xhandle                                     m_ActiveCmdHandle;

    //
    // Stats 
    //
    s32                                         m_iStat;                    // index to the next statistic entry
    xsafe_array<draw_stats,8>                   m_Stats;                    // Stats for draw
};


//===============================================================================
// VARIABLES
//===============================================================================

static draw_local* s_pDraw=NULL;

//===============================================================================
// FUNCTIONS
//===============================================================================

//-------------------------------------------------------------------------------

draw_primitive_list::draw_primitive_list( void ) :
    m_iFrame( 100000 ),
    m_pVertexBuffer( NULL ),
    m_pIndexBuffer( NULL )
{ }

//-------------------------------------------------------------------------------

draw_primitive_list::~draw_primitive_list( void )
{
    if( m_pVertexBuffer ) m_pVertexBuffer->Release();
    if( m_pIndexBuffer  ) m_pIndexBuffer->Release();
}

//-------------------------------------------------------------------------------

draw_shaders::draw_shaders( void ) :
    m_pVertexLayout( NULL ),
    m_pTechnique( NULL )
{ }

//-------------------------------------------------------------------------------

draw_shaders::~draw_shaders( void )
{
    if( m_pVertexLayout ) m_pVertexLayout->Release();
}

//-------------------------------------------------------------------------------

draw_stats::draw_stats( void ) :
    m_nCmds( 0 ),
    m_nTriangles( 0 ),
    m_nLines( 0 )
{ }

//-------------------------------------------------------------------------------

draw_local::draw_local( void ) : 

    m_pEffectFile( NULL ),
    m_pShaderL2C( NULL ),

    m_bDrawBegging( FALSE ),
    m_ActiveMode( ENG_DRAW_MODE_DEFAULT ),
    m_iActiveCmdList( 0 ),
    m_pActiveIndexBufferMap(NULL),
    m_pActiveVertexBufferMap(NULL),
    m_iActivePrimList( 0 ),
    m_nActiveVertices( 0 ),
    m_nActiveIndices( 0 ),

    m_nCmd( 0 ),
    m_nMatrices( 0 ),

    m_iStat( 0 )
{ 
    s32 i;

    for( i=0; i<m_RasterState.getCount(); i++ )
    {
        m_RasterState[i] = NULL;
    }

    for( i=0; i<m_StencilState.getCount(); i++ )
    {
        m_StencilState[i] = NULL;
    }

    for( i=0; i<m_BlendState.getCount(); i++ )
    {
        m_BlendState[i] = NULL;
    }

    m_ActiveCmdHandle.SetNull();    
}

//-------------------------------------------------------------------------------

draw_local::~draw_local( void )
{
    s32 i;

    if( m_pEffectFile        ) m_pEffectFile->Release();    

    // Clear all the render states
    for( i=0; i<m_RasterState.getCount(); i++ )
    {
        if( m_RasterState[i]       ) m_RasterState[i]->Release();    
    }

    // Clear all the depth stencil states
    for( i=0; i<m_StencilState.getCount(); i++ )
    {
        if( m_StencilState[i] ) m_StencilState[i]->Release();    
    }

    // Clear all the blend states
    for( i=0; i<m_BlendState.getCount(); i++ )
    {
        if( m_BlendState[i] ) m_BlendState[i]->Release();    
    }    
}

//-------------------------------------------------------------------------------

void draw_local::Init( void )
{
    HRESULT hr = S_OK;
    s32     i;

    //
    // Initialize local variables
    //

    // Temporary memory to initialize the buffers
    xptr<draw_vertex> Data;
    Data.Alloc( VERTEX_BUFFER_COUNT );
    Data.SetMemory( 0 );

    //
    // Create all the primitive buffers
    //
    for( i=0; i<m_PrimitiveList.getCount(); i++ )
    {
        draw_primitive_list& PrimList = m_PrimitiveList[i];

        // Describe the vertex buffer
        D3D10_BUFFER_DESC bd;
        bd.Usage            = D3D10_USAGE_DYNAMIC;
        bd.ByteWidth        = sizeof( draw_vertex ) * VERTEX_BUFFER_COUNT;
        bd.BindFlags        = D3D10_BIND_VERTEX_BUFFER;
        bd.CPUAccessFlags   = D3D10_CPU_ACCESS_WRITE;
        bd.MiscFlags        = 0;

        // Create the vertex buffer
        D3D10_SUBRESOURCE_DATA InitData;
        InitData.pSysMem = &Data[0];
        hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &PrimList.m_pVertexBuffer );
        if( FAILED( hr ) )
        {
            x_throw( "DX ERROR: Unable to create the vertex buffer for the draw layer" );
        }

        // Describe the Index buffer
        bd.Usage            = D3D10_USAGE_DYNAMIC;
        bd.ByteWidth        = sizeof( u16 ) * INDEX_BUFFER_COUNT;
        bd.BindFlags        = D3D10_BIND_INDEX_BUFFER;
        bd.CPUAccessFlags   = D3D10_CPU_ACCESS_WRITE;
        bd.MiscFlags        = 0;

        // Create the Index buffer
        hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &PrimList.m_pIndexBuffer );
        if( FAILED( hr ) )
        {
            x_throw( "DX ERROR: Unable to create the index buffer for the draw layer" );
        }
    }

    //
    // Create the vertex descriptions
    //

    // Define the input layout
    static D3D10_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(f32)*0, D3D10_INPUT_PER_VERTEX_DATA, 0 },  
        { "TEXCOORD",   0, DXGI_FORMAT_R32G32_FLOAT,    0, sizeof(f32)*3, D3D10_INPUT_PER_VERTEX_DATA, 0 },  
        { "COLOR",      0, DXGI_FORMAT_R8G8B8A8_UNORM,  0, sizeof(f32)*5, D3D10_INPUT_PER_VERTEX_DATA, 0 },  
    };
    UINT numElements = sizeof(layout)/sizeof(layout[0]);

    //
    // Create shaders
    //
    {
#include "Implementation\DX10\Shaders\draw_DX10shader.h"

		DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
		#if defined( DEBUG ) || defined( _DEBUG )
		// Set the D3D10_SHADER_DEBUG flag to embed debug information in the shaders.
		// Setting this flag improves the shader debugging experience, but still allows 
		// the shaders to be optimized and to run exactly the way they will run in 
		// the release configuration of this program.
		dwShaderFlags |= D3D10_SHADER_DEBUG;
		#endif

        // Load the effect file
        ID3D10Blob* pErrors = NULL;
        hr = D3DX10CreateEffectFromMemory( g_main, sizeof(g_main), NULL, NULL, NULL, NULL, 0, D3D10_SHADER_ENABLE_STRICTNESS, g_pd3dDevice, NULL, NULL, &m_pEffectFile, &pErrors, NULL );

		//hr = D3DX10CreateEffectFromFile( "C:\\ctg\\Development\\engBase\\Implementation\\DX10\\Shaders\\draw_DX10shader.fx", NULL, NULL, "fx_4_0", dwShaderFlags, 0, 
		//								 g_pd3dDevice, NULL, NULL, &m_pEffectFile, NULL, NULL );
		if( FAILED( hr ) )
		{
			//MessageBox( NULL, "The FX file cannot be located.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK );
            ASSERT(0);
			return;// false;
		}

        if( FAILED( hr ) )
        {
            if( pErrors )
            {
                x_DebugMsg( "%s", pErrors->GetBufferPointer() );
                pErrors->Release();
            }
            x_throw( "DX ERROR: There was a problem loading the draw shader" );
        }
    }

    // Get the shader constants
    m_pShaderL2C = m_pEffectFile->GetVariableByName( "L2C" )->AsMatrix();

    // Create the input description for each shader
    for( i=0; i<m_Shaders.getCount(); i++ )
    {
        draw_shaders& Shader = m_Shaders[i];

        // Get the technique
        Shader.m_pTechnique = m_pEffectFile->GetTechniqueByName( "draw_2d" );
        if( Shader.m_pTechnique == NULL )
        {
            x_throw( "DX ERROR: Unable to find the desire technique in the fx file" );
        }

        // Get the zero pass
        ID3D10EffectPass* pPass = Shader.m_pTechnique->GetPassByIndex( 0 );
        if( pPass == NULL )
        {
            x_throw( "DX ERROR: Unable to find the expected pass in the fx file" );
        }
            
        // Create the input layout
        D3D10_PASS_DESC PassDesc;
        ZeroMemory( &PassDesc, sizeof(D3D10_PASS_DESC) );
        pPass->GetDesc( &PassDesc );

        hr = g_pd3dDevice->CreateInputLayout( layout, numElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &Shader.m_pVertexLayout );
        if( FAILED( hr ) )
        {
            x_throw( "DX ERROR: Unable to setup the input description for the draw shader." );
        }
    }

    //
    // Create the different raster stages
    //

    for( i=0; i<m_RasterState.getCount(); i++ )
    {
        D3D10_RASTERIZER_DESC rasterizerState;
        rasterizerState.FrontCounterClockwise   = true;
        rasterizerState.DepthBias               = false;
        rasterizerState.DepthBiasClamp          = 0;
        rasterizerState.SlopeScaledDepthBias    = 0;
        rasterizerState.DepthClipEnable         = true;
        rasterizerState.ScissorEnable           = false;
        rasterizerState.MultisampleEnable       = true;
        rasterizerState.AntialiasedLineEnable   = false;
    
        //
        // Handle combos
        //
        u32 iRaster = i<<ENG_DRAW_MODE_RASTER_SHIFT;

        if( iRaster & ENG_DRAW_MODE_RASTER_WIRE_FRAME ) rasterizerState.FillMode = D3D10_FILL_WIREFRAME;
        else                                            rasterizerState.FillMode = D3D10_FILL_SOLID;

        if( iRaster & ENG_DRAW_MODE_RASTER_CULL_NONE )  rasterizerState.CullMode = D3D10_CULL_NONE;
        else                                            rasterizerState.CullMode = D3D10_CULL_BACK;

        //
        // Create state
        //
        g_pd3dDevice->CreateRasterizerState( &rasterizerState, &m_RasterState[i] );        
    }
	//set the default state
	if(m_RasterState.getCount()>0)
	{
		g_pd3dDevice->RSSetState( m_RasterState[0] );
	}

    //
    // Depth stencil
    //
    for( i=0; i<m_StencilState.getCount(); i++ )
    {
        D3D10_DEPTH_STENCIL_DESC dsDesc;

        // Depth test parameters
        dsDesc.DepthEnable      = true;
        dsDesc.DepthWriteMask   = D3D10_DEPTH_WRITE_MASK_ALL;
        dsDesc.DepthFunc        = D3D10_COMPARISON_LESS;

        // Stencil test parameters
        dsDesc.StencilEnable    = true;
        dsDesc.StencilReadMask  = 0xFF;
        dsDesc.StencilWriteMask = 0xFF;

        // Stencil operations if pixel is front-facing
        dsDesc.FrontFace.StencilFailOp      = D3D10_STENCIL_OP_KEEP;
        dsDesc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_INCR;
        dsDesc.FrontFace.StencilPassOp      = D3D10_STENCIL_OP_KEEP;
        dsDesc.FrontFace.StencilFunc        = D3D10_COMPARISON_ALWAYS;

        // Stencil operations if pixel is back-facing
        dsDesc.BackFace.StencilFailOp       = D3D10_STENCIL_OP_KEEP;
        dsDesc.BackFace.StencilDepthFailOp  = D3D10_STENCIL_OP_DECR;
        dsDesc.BackFace.StencilPassOp       = D3D10_STENCIL_OP_KEEP;
        dsDesc.BackFace.StencilFunc         = D3D10_COMPARISON_ALWAYS;

        //
        // Handle the combos
        //
        u32 iZBuffer = i<<ENG_DRAW_MODE_ZBUFFER_SHIFT;

        if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_ON )
        {
            dsDesc.DepthEnable      = true;
        }
        else if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_OFF )
        {
            dsDesc.DepthEnable      = false;
        }
        else if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_READ_ONLY )
        {
            dsDesc.DepthEnable      = true;
            dsDesc.DepthWriteMask   = D3D10_DEPTH_WRITE_MASK_ZERO;
        }
        else
        {
            ASSERT( 0 );
        }

        //
        // Create depth stencil state
        //
        g_pd3dDevice->CreateDepthStencilState( &dsDesc, &m_StencilState[i] );
    }
	if(m_StencilState.getCount()>0)
	{
		g_pd3dDevice->OMSetDepthStencilState( m_StencilState[0], 0 );
	}

    //
    //
    //
    for( i=0; i<m_BlendState.getCount(); i++ )
    {
        D3D10_BLEND_DESC BlendState;
        ZeroMemory(&BlendState, sizeof(D3D10_BLEND_DESC));
		  for(int rt = 0;rt<8;rt++)
		  {
		      BlendState.RenderTargetWriteMask[rt] = D3D10_COLOR_WRITE_ENABLE_ALL;
		  }

        //
        // Handle the combos
        //
        u32 iBlend = i<<ENG_DRAW_MODE_BLEND_SHIFT;

        if( iBlend == ENG_DRAW_MODE_BLEND_OFF )
        {
		     for(int rt = 0;rt<8;rt++)
			  {
			      BlendState.BlendEnable[rt]           = FALSE;
			  }
        }
        else if( iBlend == ENG_DRAW_MODE_BLEND_ALPHA )
        {
            for(int rt = 0;rt<8;rt++)
		      {
                BlendState.BlendEnable[rt]    = TRUE;
				}
            BlendState.SrcBlend          = D3D10_BLEND_SRC_ALPHA;
            BlendState.DestBlend         = D3D10_BLEND_INV_SRC_ALPHA;
            BlendState.BlendOp           = D3D10_BLEND_OP_ADD;
            BlendState.SrcBlendAlpha     = D3D10_BLEND_ONE;
            BlendState.DestBlendAlpha    = D3D10_BLEND_ZERO;
            BlendState.BlendOpAlpha      = D3D10_BLEND_OP_ADD;
        }
        else if( iBlend == ENG_DRAW_MODE_BLEND_ADD )
        {
            for(int rt = 0;rt<8;rt++)
		      {
                BlendState.BlendEnable[rt]    = TRUE;
				}
            BlendState.SrcBlend          = D3D10_BLEND_ONE;
            BlendState.DestBlend         = D3D10_BLEND_ONE;
            BlendState.BlendOp           = D3D10_BLEND_OP_ADD;
            BlendState.SrcBlendAlpha     = D3D10_BLEND_ONE;
            BlendState.DestBlendAlpha    = D3D10_BLEND_ZERO;
            BlendState.BlendOpAlpha      = D3D10_BLEND_OP_ADD;
        }
        else if( iBlend == ENG_DRAW_MODE_BLEND_SUB )
        {
            for(int rt = 0;rt<8;rt++)
		      {
                BlendState.BlendEnable[rt]    = TRUE;
				}
            BlendState.SrcBlend          = D3D10_BLEND_ONE;
            BlendState.DestBlend         = D3D10_BLEND_ONE;
            BlendState.BlendOp           = D3D10_BLEND_OP_SUBTRACT;
            BlendState.SrcBlendAlpha     = D3D10_BLEND_ONE;
            BlendState.DestBlendAlpha    = D3D10_BLEND_ZERO;
            BlendState.BlendOpAlpha      = D3D10_BLEND_OP_ADD;
        }
        else
        {
            ASSERT( 0 );
        }

        //
        // Create blend state
        //
        g_pd3dDevice->CreateBlendState( &BlendState, &m_BlendState[i] );

    }
	if( m_BlendState.getCount()>0 )
	{
		const FLOAT BlendFactor[4] = {1.0,1.0,1.0,1.0};
		g_pd3dDevice->OMSetBlendState( m_BlendState[0], BlendFactor, 0xFFFFFFFF );
	}
}

//-------------------------------------------------------------------------------

void draw_local::Kill( void )
{

}

//-------------------------------------------------------------------------------

void draw_local::Begin( u32 Mode )
{
    ASSERT( m_bDrawBegging == FALSE );
    m_ActiveMode = Mode;
    m_bDrawBegging = TRUE;
}

//-------------------------------------------------------------------------------

void draw_local::End( void )
{
    ASSERT( m_bDrawBegging );
    m_bDrawBegging = FALSE;
}

//-------------------------------------------------------------------------------
//
// If it can't dump all vertices in the same vertex buffer 
//
xhandle draw_local::GetReadyToDumpData( s32 nVertices, s32 nIndices, u32 ExtraMode )
{
    HRESULT     hr      = S_OK;
    u32         iFrame  = eng_GetCurrentFrameNumber();

    ASSERT( nVertices < VERTEX_BUFFER_COUNT );
    ASSERT( nIndices  < INDEX_BUFFER_COUNT );

    //
    // Make sure that all the limits are good
    //
    {
        // Do we need to change the active primitive list?
        if( (VERTEX_BUFFER_COUNT - m_nActiveVertices) < nVertices ||
            (INDEX_BUFFER_COUNT  - m_nActiveIndices)  < nIndices )
        {
            // Unmap current vertex/index buffers
            if( m_pActiveVertexBufferMap )
            {
                draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];
                
                PrimList.m_pIndexBuffer->Unmap();
                PrimList.m_pVertexBuffer->Unmap();

                m_pActiveVertexBufferMap = NULL;
                m_pActiveIndexBufferMap  = NULL;
            }

            // Set the next index
            m_iActivePrimList = (m_iActivePrimList + 1)%m_PrimitiveList.getCount();

            // Make sure that the buffer is not been use the previous frame
            ASSERT( m_PrimitiveList[m_iActivePrimList].m_iFrame != (iFrame-1) );

            // Also update the iframe 
            m_PrimitiveList[m_iActivePrimList].m_iFrame = iFrame;

            // Okay now we can reset a few things
            m_nActiveVertices = 0;
            m_nActiveIndices  = 0; 
        }
    }

    //
    // Lock buffers
    //
    if( m_pActiveVertexBufferMap == NULL )
    {
        ASSERT( m_pActiveIndexBufferMap == NULL );

        draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];

        // lock vertex buffer
        hr = PrimList.m_pVertexBuffer->Map( D3D10_MAP_WRITE_DISCARD , 0, reinterpret_cast< void** >( &m_pActiveVertexBufferMap ));
        if( FAILED( hr ) )
        {
            x_throw( "DX ERROR: Unable to lock the vertex buffer for draw." );
        }

        // lock index buffer
        hr = PrimList.m_pIndexBuffer->Map( D3D10_MAP_WRITE_DISCARD , 0, reinterpret_cast< void** >( &m_pActiveIndexBufferMap ));
        if( FAILED( hr ) )
        {
            x_throw( "DX ERROR: Unable to lock the index buffer for draw." );
        }
    }
    else
    {
        ASSERT( m_pActiveIndexBufferMap );
    }

    //
    // Initialize the command
    //
    xhandle        Handle;
    Handle.Set( m_nCmd );        

    draw_cmd&      Cmd        = m_Cmd[ m_nCmd++ ];
    
    Cmd.m_Mode              = m_ActiveMode | ExtraMode;
    Cmd.m_iPrimitiveList    = m_iActivePrimList;
    Cmd.m_VertexOffset      = m_nActiveVertices;
    Cmd.m_IndexOffset       = m_nActiveIndices;
    Cmd.m_nVertices         = nVertices;
    Cmd.m_nIndices          = nIndices;
    Cmd.m_iMatrix           = m_nMatrices-1;
    
    return Handle;
}

//-------------------------------------------------------------------------------

void draw_local::PageFlip( void )
{
    s32 iCachePrimtList = ~0;
    u32 CacheShader     = ~0;
    s32 PrimitiveType   = ~0;
    s32 RasterState     = ~0;
    s32 DepthState      = ~0;
    s32 BlendState      = ~0;
    s32 DrawMode        = ~0;
    s32 iMatrix         = ~0;
    xmatrix4 L2C_2DParametric;
    xmatrix4 L2C_2D;
    xmatrix4 W2C_3D;

    ASSERT( m_bDrawBegging == FALSE );

    //
    // Build 2D Matrices
    //
    {
        L2C_2DParametric.Identity();
        L2C_2D.Identity();
        L2C_2DParametric.SetTranslation( xvector3( -1, -1, 0 ) );
        L2C_2DParametric.SetScale      ( xvector3( 2, 2, 1 ) );

        s32 XRes, YRes;
        eng_GetScreenResolution( XRes, YRes );

        L2C_2D.SetTranslation( xvector3( -1, -1, 0 ) );
        L2C_2D.SetScale      ( xvector3( 2.0f/XRes, 2.0f/YRes, 1 ) );
    }

    //
    // Get the world to clip matrix from the active view
    //
    const eng_view& View = eng_GetActiveView();
    W2C_3D = View.GetW2C();

    //
    // Do we have anything to do here?
    //
    if( m_iActiveCmdList == 0 && m_nCmd == 0 ) 
        return;
    ASSERT(m_pActiveVertexBufferMap);

    //
    // Any map buffers must be unmap
    //
    draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];
    PrimList.m_pIndexBuffer->Unmap();
    PrimList.m_pVertexBuffer->Unmap();

    //
    // Set global states
    //
    for( s32 j=0; j<m_nCmd; j++ )
    {
        draw_cmd& Cmd = m_Cmd[j];

        //
        // Set the rater stage
        //
        if( RasterState != ((Cmd.m_Mode>>ENG_DRAW_MODE_RASTER_SHIFT)&ENG_DRAW_MODE_RASTER_MASK) )
        {
            RasterState = (Cmd.m_Mode>>ENG_DRAW_MODE_RASTER_SHIFT)&ENG_DRAW_MODE_RASTER_MASK;
            g_pd3dDevice->RSSetState( m_RasterState[RasterState] );
        }

        //
        // Set the rater Depth stage
        //
        if( DepthState != ((Cmd.m_Mode>>ENG_DRAW_MODE_ZBUFFER_SHIFT)&ENG_DRAW_MODE_ZBUFFER_MASK) )
        {
            DepthState = (Cmd.m_Mode>>ENG_DRAW_MODE_ZBUFFER_SHIFT)&ENG_DRAW_MODE_ZBUFFER_MASK;
            g_pd3dDevice->OMSetDepthStencilState( m_StencilState[DepthState], 1 );
        }

        //
        // Set the Blend stage
        //
        if( BlendState != ((Cmd.m_Mode>>ENG_DRAW_MODE_BLEND_SHIFT)&ENG_DRAW_MODE_BLEND_MASK) )
        {
            BlendState = (Cmd.m_Mode>>ENG_DRAW_MODE_BLEND_SHIFT)&ENG_DRAW_MODE_BLEND_MASK;
            g_pd3dDevice->OMSetBlendState( m_BlendState[BlendState], 0, 0xffffffff);
        }

        //
        // Set the right matrix
        //
        if( DrawMode != ((Cmd.m_Mode>>ENG_DRAW_MODE_SHIFT)&ENG_DRAW_MODE_MASK) )
        {
            DrawMode = ((Cmd.m_Mode>>ENG_DRAW_MODE_SHIFT)&ENG_DRAW_MODE_MASK);

            switch(DrawMode)
            {
                case ENG_DRAW_MODE_3D:
                    m_pShaderL2C->SetMatrix( (float*)&( W2C_3D * m_L2W[ Cmd.m_iMatrix ] ) );                    
                    break;

                case ENG_DRAW_MODE_2D:
                    m_pShaderL2C->SetMatrix( (float*)&L2C_2D );
                    break;

                case ENG_DRAW_MODE_2D_PARAMETRIC:
                    m_pShaderL2C->SetMatrix( (float*)&L2C_2DParametric );
                    break;

                default:
                    {
                        ASSERT( 0 );
                    }
            };
        }
        else if ( DrawMode == ENG_DRAW_MODE_3D && iMatrix != Cmd.m_iMatrix )
        {
            iMatrix = Cmd.m_iMatrix;
            m_pShaderL2C->SetMatrix( (float*)&( W2C_3D * m_L2W[ Cmd.m_iMatrix ] ) );                    
        }

        //
        // Handle setting the right primitive
        //
        if( PrimitiveType != ((Cmd.m_Mode>>ENG_DRAW_PRIMITIVE_TYPE_SHIFT)&ENG_DRAW_PRIMITIVE_TYPE_MASK) )
        {
            // Update the cache
            PrimitiveType = (Cmd.m_Mode>>ENG_DRAW_PRIMITIVE_TYPE_SHIFT)&ENG_DRAW_PRIMITIVE_TYPE_MASK;

            // Setup the right primitive
            switch( PrimitiveType )
            {
            case (ENG_DRAW_PRIMITIVE_TYPE_TRIANGLE>>ENG_DRAW_PRIMITIVE_TYPE_SHIFT):
                {
                    g_pd3dDevice->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
                    break;                    
                }
            case (ENG_DRAW_PRIMITIVE_TYPE_LINE>>ENG_DRAW_PRIMITIVE_TYPE_SHIFT):
                {
                    g_pd3dDevice->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_LINELIST );
                    break;
                }
            default:
                {
                    ASSERT(0);
                }
            }
        }

        //
        // Handle setting the right vertex/index buffer
        //
        if( iCachePrimtList != Cmd.m_iPrimitiveList )
        {
            // Update the cache entry for vertex/index buffers
            iCachePrimtList = Cmd.m_iPrimitiveList;

            // Active new vertex index buffers
            draw_primitive_list& PrimList = m_PrimitiveList[ iCachePrimtList ];

            // Active the vertex buffer
            UINT stride = sizeof( draw_vertex );
            UINT offset = 0;
            g_pd3dDevice->IASetVertexBuffers( 0, 1, &PrimList.m_pVertexBuffer, &stride, &offset );

            // Activate the index buffer
            g_pd3dDevice->IASetIndexBuffer( PrimList.m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0 );
        }

        //
        // Set the shader and render
        //

        D3D10_TECHNIQUE_DESC techDesc;
        m_Shaders[0].m_pTechnique->GetDesc( &techDesc );

        // Set the input layout
        g_pd3dDevice->IASetInputLayout( m_Shaders[0].m_pVertexLayout );

        for( UINT p = 0; p < techDesc.Passes; ++p )
        {
            m_Shaders[0].m_pTechnique->GetPassByIndex( p )->Apply(0);
            g_pd3dDevice->DrawIndexed( Cmd.m_nIndices, Cmd.m_IndexOffset, Cmd.m_VertexOffset );
        }
    }

    //
    // Force the system to flush the active variables
    //
    m_nActiveVertices        = VERTEX_BUFFER_COUNT;
    m_pActiveVertexBufferMap = NULL;
    m_pActiveIndexBufferMap  = NULL;
    m_iActiveCmdList         = 0;
    
    m_nCmd      = 0;
    m_nMatrices = 0;
}

//-------------------------------------------------------------------------------

void draw_local::SetL2W( const xmatrix4& M )
{
    ASSERT( m_bDrawBegging );

    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );

    // Set the L2W
    m_L2W[ m_nMatrices ] = M;

    // Set the system ready for the next matrix
    m_nMatrices++;
}

//-------------------------------------------------------------------------------

void draw_local::ClearL2W( void )
{
    ASSERT( m_bDrawBegging );

    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );

    // Clear the L2W
    m_L2W[ m_nMatrices ].Identity();

    // Set the system ready for the next matrix
    m_nMatrices++;
}

//-------------------------------------------------------------------------------

void draw_local::DrawGetBuffers( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices )
{
    ASSERT( m_ActiveCmdHandle.IsNull() );
    ASSERT( pVertex );
    ASSERT( pIndex );
    ASSERT( nVertices >= 2 );
    ASSERT( nIndices  >= 2 );

    m_ActiveCmdHandle = GetReadyToDumpData( nVertices, nIndices, 0 );

    // Set pointers
    *pVertex = &m_pActiveVertexBufferMap[m_nActiveVertices];
    *pIndex  = &m_pActiveIndexBufferMap[m_nActiveIndices];

    // Advance the indices
    m_nActiveVertices += nVertices;
    m_nActiveIndices  += nIndices;
}

//-------------------------------------------------------------------------------

void draw_local::DrawBufferTriangles( void )
{
    ASSERT( m_ActiveCmdHandle.IsValid() );
    draw_cmd& Cmd = m_Cmd[ m_ActiveCmdHandle.Get() ];
    Cmd.m_Mode |= ENG_DRAW_PRIMITIVE_TYPE_TRIANGLE;
    m_ActiveCmdHandle.SetNull();
}

//-------------------------------------------------------------------------------

void draw_local::DrawBufferLines( void )
{
    ASSERT( m_ActiveCmdHandle.IsValid() );
    draw_cmd& Cmd = m_Cmd[ m_ActiveCmdHandle.Get() ];
    Cmd.m_Mode |= ENG_DRAW_PRIMITIVE_TYPE_LINE;
    m_ActiveCmdHandle.SetNull();
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// Public functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

void _eng_DrawInit( void )
{
    ASSERT( s_pDraw == NULL );
    //
    // Allocate the local memory
    //
    s_pDraw = x_new( draw_local, 1, XMEM_FLAG_ALIGN_16B );
    ASSERT( s_pDraw );

    // Initialize draw
    s_pDraw->Init();
}

//-------------------------------------------------------------------------------

void _eng_DrawKill( void )
{
    //
    // Release the memory
    //
    if( s_pDraw ) 
    {
        s_pDraw->Kill();
        x_delete( s_pDraw );
        s_pDraw = NULL;
    }
}

//-------------------------------------------------------------------------------

void eng_DrawBegin( u32 Mode )
{
    ASSERT( s_pDraw );
    s_pDraw->Begin( Mode );
}

//-------------------------------------------------------------------------------

void eng_DrawEnd( void )
{
    ASSERT( s_pDraw );
    s_pDraw->End();
}

//-------------------------------------------------------------------------------

void _eng_DrawPageFlip( void )
{
    ASSERT( s_pDraw );
    s_pDraw->PageFlip();
}

//-------------------------------------------------------------------------------

void eng_DrawGetBuffers( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices )
{
    ASSERT( s_pDraw );
    s_pDraw->DrawGetBuffers( pVertex, nVertices, pIndex, nIndices );
}

//-------------------------------------------------------------------------------

void eng_DrawBufferTriangles( void )
{
    ASSERT( s_pDraw );
    s_pDraw->DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_DrawBufferLines( void )
{
    ASSERT( s_pDraw );
    s_pDraw->DrawBufferLines();
}

//-------------------------------------------------------------------------------

void eng_DrawSetL2W( const xmatrix4& L2W )
{
    ASSERT( s_pDraw );
    s_pDraw->SetL2W( L2W );
}

//-------------------------------------------------------------------------------

void eng_DrawClearL2W( void )
{
    ASSERT( s_pDraw );
    s_pDraw->ClearL2W();
}


