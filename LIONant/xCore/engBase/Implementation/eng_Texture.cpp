//
//  eng_Texture.cpp
//  engBase
//
//  Created by Tomas Arce on 9/16/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "eng_base.h"

// OpenGL ES does not have this
#ifdef ENG_OPEN_GLES
    #define GL_CLAMP_TO_BORDER          ~0
#endif

// This is because movile platforms
#ifndef GL_HALF_FLOAT_OES
    #define GL_HALF_FLOAT_OES GL_HALF_FLOAT
#endif

// Most platforms dont support this formats
#ifndef GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG
    #define GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG  -1
    #define GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG -1
    #define GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG  -1
    #define GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG -1
#endif

// These are nintendo DS focus
#ifndef GL_ETC1_RGB8_NATIVE_DMP
    #define GL_ETC1_RGB8_NATIVE_DMP             -1
    #define GL_ETC1_ALPHA_RGB8_A4_NATIVE_DMP    -1
#endif

// becasuse of nintendo DS
#ifdef GL_RGB_NATIVE_DMP
    #define GL_RGBA_NATIVE  GL_RGBA_NATIVE_DMP
    #define GL_RGB_NATIVE   GL_RGB_NATIVE_DMP
#else
    #define GL_RGBA_NATIVE  GL_RGBA
    #define GL_RGB_NATIVE   GL_RGB
#endif

#ifndef GL_RGBA8
    #define GL_RGBA8 GL_RGBA_NATIVE
#endif

#if defined TARGET_IOS //|| defined TARGET_OSX
    #undef GL_RGBA4
    #define GL_RGBA4 GL_RGBA_NATIVE
#endif

#if !defined TARGET_3DS && !defined TARGET_IOS
    #ifndef GL_EXT_texture_compression_s3tc
        #define GL_EXT_texture_compression_s3tc 1
        #define __GLEE_GL_EXT_texture_compression_s3tc 1
        // Constants
        #define GL_COMPRESSED_RGB_S3TC_DXT1_EXT                    0x83F0
        #define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT                   0x83F1
        #define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT                   0x83F2
        #define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT                   0x83F3
    #endif

    #ifndef GL_EXT_texture_sRGB
        #define GL_COMPRESSED_SRGB_S3TC_DXT1_EXT                    0x8C4C
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT              0x8C4D
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT              0x8C4E
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT              0x8C4F
    #endif

#else
    #define GL_COMPRESSED_RGB_S3TC_DXT1_EXT                    -1
    #define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT                   -1
    #define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT                   -1
    #define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT                   -1

    #ifndef GL_EXT_texture_sRGB
        #define GL_COMPRESSED_SRGB_S3TC_DXT1_EXT                   -1
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT             -1
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT             -1
        #define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT             -1
    #endif

#endif

#if !defined TARGET_3DS && !defined TARGET_IOS
    #ifndef GL_EXT_texture_sRGB
        #define GL_SRGB_EXT             0x8C40
        #define GL_SRGB8_EXT            0x8C41
        #define GL_SRGB_ALPHA_EXT       0x8C42
        #define GL_SRGB8_ALPHA8_EXT     0x8C43
    #endif
#else
    #define GL_SRGB_EXT             -1
    #define GL_SRGB8_EXT            -1
    #define GL_SRGB_ALPHA_EXT       -1
    #define GL_SRGB8_ALPHA8_EXT     -1
#endif

struct eng_xbitmap_to_gl_desc
{
    xbitmap::format     m_XBaseFormat;
    GLint               m_GLInternalFormatGamma;
    GLint               m_GLInternalFormat;
    GLint               m_GLFormat;
    GLint               m_GLPixelType;
    xbool               m_bCompressed;
};

static const eng_xbitmap_to_gl_desc s_XBitmapToGLFormat[]=
{
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_A4B4G4R4,             GL_RGBA4,                                   GL_RGBA4,                           GL_RGBA_NATIVE,                     GL_UNSIGNED_SHORT_4_4_4_4,      FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_B5G6R5,               GL_SRGB_EXT,                                GL_RGB_NATIVE,                      GL_RGB_NATIVE,                      GL_UNSIGNED_SHORT_5_6_5,        FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL    ,             -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_A1B5G5R5,             GL_RGB5_A1,                                 GL_RGB5_A1,                         GL_RGBA_NATIVE,                     GL_UNSIGNED_SHORT_5_5_5_1,      FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R8G8B8,               GL_SRGB8_ALPHA8_EXT,                        GL_RGBA8,                           GL_RGB_NATIVE,                      GL_UNSIGNED_BYTE,               FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R8G8B8U8,             GL_SRGB8_ALPHA8_EXT,                        GL_RGBA8,                           GL_RGBA_NATIVE,                     GL_UNSIGNED_BYTE,               FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R8G8B8A8,             GL_SRGB8_ALPHA8_EXT,                        GL_RGBA8,                           GL_RGBA_NATIVE,                     GL_UNSIGNED_BYTE,               FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_XCOLOR_END,           -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_PAL4,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_PAL8,                 -1                                          -1                                  -1                                  -1,                             FALSE },
    { xbitmap::FORMAT_DXT1_RGBA,            GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT,     GL_COMPRESSED_RGBA_S3TC_DXT1_EXT,   -1,                                 -1,                             TRUE  },// No idea about the PixelType
    { xbitmap::FORMAT_DXT3_RGBA,            GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT,     GL_COMPRESSED_RGBA_S3TC_DXT3_EXT,   -1,                                 -1,                             TRUE  },// No idea about the PixelType
    { xbitmap::FORMAT_DXT5_RGBA,            GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT,     GL_COMPRESSED_RGBA_S3TC_DXT5_EXT,   -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_R8,                   -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_D32,                  -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R32,                  -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R8G8,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R16G16B16A16,         -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R16G16B16A16_f,       GL_RGBA_NATIVE,                             GL_RGBA_NATIVE,                     GL_RGBA_NATIVE,                     GL_HALF_FLOAT_OES,              FALSE },
    { xbitmap::FORMAT_R10G10B10A2,          -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_D24S8,                -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_R11G11B10,            -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_16DU16DV,             -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_16_DOT3_COMPRESSED,   -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_A8,                   -1,                                         -1,                                 -1,                                 -1,                             FALSE },
    { xbitmap::FORMAT_360_DXN,              -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_360_DXT3A,            -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_360_DXT5A,            -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_360_CTX1,             -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_ETC1,                 GL_ETC1_RGB8_NATIVE_DMP,                    GL_ETC1_RGB8_NATIVE_DMP,            -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_ETC2,                 GL_ETC1_ALPHA_RGB8_A4_NATIVE_DMP,           GL_ETC1_ALPHA_RGB8_A4_NATIVE_DMP,   -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR1_2RGB,            GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG,         GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG, -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR1_2RGBA,           GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG,        GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG,-1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR1_4RGB,            GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG,         GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG, -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR1_4RGBA,           GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG,        GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG,-1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR2_2RGBA,           -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_PVR2_4RGBA,           -1,                                         -1,                                 -1,                                 -1,                             TRUE  },
    { xbitmap::FORMAT_NULL,                 -1,                                         -1,                                 -1,                                 -1,                             FALSE }
};

//-----------------------------------------------------------------------------------

eng_texture::eng_texture( void )
{
    m_IDBuffer = 0;
    m_W        = 0;
    m_H        = 0;
}

//-----------------------------------------------------------------------------------

eng_texture::~eng_texture( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

xbool eng_texture::isValid( void ) const
{
    return m_IDBuffer != 0;
}

//-----------------------------------------------------------------------------------

void eng_texture::CreateTexture( const xbitmap& Bitmap, xbool bBuildMipMaps )
{
    eng_CheckForError( );
    Destroy();
    const xbitmap::format Format = Bitmap.getFormat();
    
    //
    // Validate the bitmap
    //
    ASSERT( Bitmap.isValid() );
    ASSERT( Bitmap.getFormat() > 0 );
    ASSERT( Bitmap.getFormat() < sizeof(s_XBitmapToGLFormat)/sizeof(eng_xbitmap_to_gl_desc) );
    
    // May be this format is not supported in this platform?
    ASSERT( s_XBitmapToGLFormat[Format].m_GLInternalFormat != -1 );
    ASSERT( s_XBitmapToGLFormat[Format].m_XBaseFormat == Format );
    
    m_W = Bitmap.getWidth();;
    m_H = Bitmap.getHeight();;
    
    glGenTextures( 1, &m_IDBuffer );
    glBindTexture( GL_TEXTURE_2D, m_IDBuffer );
    eng_CheckForError( );
    
    //
    // Copy data to GL
    //
    s32                             W           = m_W;
    s32                             H           = m_H;
    const eng_xbitmap_to_gl_desc&   GLFormat    = s_XBitmapToGLFormat[ Format ];
    
    for( s32 i=0; i<Bitmap.getMipCount(); i++ )
    {
        ASSERT( W > 0 );
        ASSERT( H > 0 );
        
        if( GLFormat.m_bCompressed )
        {
            const s32 MipSize = Bitmap.getMipSize(i);
            glCompressedTexImage2D(
                                   GL_TEXTURE_2D,
                                   i,
                                   Bitmap.isGamma()?GLFormat.m_GLInternalFormatGamma:GLFormat.m_GLInternalFormat,   // Internal format
                                   W,
                                   H,
                                   0,                             // Must always be zero (not used)
                                   MipSize,                       // Size of the mipmap
                                   Bitmap.getMip(i)               // The actual data
                                   );
            eng_CheckForError();
        }
        else
        {
            glTexImage2D( GL_TEXTURE_2D,
                         i,
                         Bitmap.isGamma()?GLFormat.m_GLInternalFormatGamma:GLFormat.m_GLInternalFormat,   // Internal format
                         W,
                         H,
                         0,                             // Must always be zero (not used)
                         GLFormat.m_GLFormat,           // Format
                         GLFormat.m_GLPixelType,        // Pixel Type...
                         Bitmap.getMip(i)               // The actual data
                         );
            eng_CheckForError();
        }
        
        // Get ready in case there are more mips
        W>>=1;
        H>>=1;
    }
    eng_CheckForError();
    
    //
    // build mipmaps
    //
    if( bBuildMipMaps && Bitmap.getMipCount() == 1)
    {
        glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
        
        glGenerateMipmap( GL_TEXTURE_2D );
        eng_CheckForError();
    }
    
    //
    // Set some defaults
    //
    if( bBuildMipMaps || Bitmap.getMipCount() > 1 )
    {
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
        eng_CheckForError();
    }
    else
    {
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        eng_CheckForError();
    }
    
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::CreateTexture( s32 Width, s32 Height, xbitmap::format Format )
{
    Destroy();
    
    //
    // Validate the bitmap
    //
    ASSERT( Format > 0 );
    ASSERT( Format < sizeof(s_XBitmapToGLFormat)/sizeof(eng_xbitmap_to_gl_desc) );
    ASSERT( s_XBitmapToGLFormat[Format].m_GLFormat != -1 );
    
    //
    //
    //
    m_W = Width;
    m_H = Height;
    const eng_xbitmap_to_gl_desc&   GLFormat    = s_XBitmapToGLFormat[Format];
    
    glGenTextures( 1, &m_IDBuffer );
    glBindTexture( GL_TEXTURE_2D, m_IDBuffer );
    glTexImage2D( GL_TEXTURE_2D,
                 0,
                 GLFormat.m_GLFormat,
                 Width,
                 Height,
                 0,
                 GL_RGBA,
                 GLFormat.m_GLPixelType,
                 NULL );
    eng_CheckForError();
    
    //
    // Set some defaults
    //
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::setWrappingMode( wrapping_mode U, wrapping_mode V, wrapping_mode W )
{
    static const GLint ConvertToGL[] = { -1, GL_MIRRORED_REPEAT, GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER };
    
    ASSERT(m_IDBuffer);
    
    glBindTexture( GL_TEXTURE_2D, m_IDBuffer );
    
    if( U != WRAP_MODE_IGNORE )
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ConvertToGL[U] );
    
    if( V != WRAP_MODE_IGNORE )
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ConvertToGL[V] );
    
#ifndef ENG_OPEN_GLES
    if( W != WRAP_MODE_IGNORE )
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, ConvertToGL[W] );
#endif
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::setAnisotropicMode( anisotropic Anisotropy )
{
    GLfloat Largest; 
    glGetFloatv( GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &Largest );
    eng_CheckForError();

    if( Anisotropy == 0 ) Largest = 0;
    else                  Largest = Largest / ( Anisotropy / (f32)ANISOTROPIC_HIGHEST );

    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Largest );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::setMinMagFilterMode( filter_mode Mig, filter_mode Mag )
{
    static const GLint ConvertToGL[] = { GL_NEAREST, GL_LINEAR,
        GL_NEAREST_MIPMAP_NEAREST,
        GL_LINEAR_MIPMAP_NEAREST,
        GL_NEAREST_MIPMAP_LINEAR,
        GL_LINEAR_MIPMAP_LINEAR };
    ASSERT(m_IDBuffer);
    
    glBindTexture( GL_TEXTURE_2D, m_IDBuffer );
    
    if( Mig != FILTER_MINMAG_IGNORE )
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, ConvertToGL[Mig] );
    
    if( Mag != FILTER_MINMAG_IGNORE )
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, ConvertToGL[Mag] );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::Destroy( void )
{
    if( m_IDBuffer )
    {
        glDeleteTextures(1, &m_IDBuffer);
    }
    
    m_IDBuffer = 0;
    m_W        = 0;
    m_H        = 0;
}

//-----------------------------------------------------------------------------------

void eng_texture::Activate( s32 iChannel ) const
{
    static const GLint ConvertToGL[] = {
        GL_TEXTURE0,  GL_TEXTURE1,  GL_TEXTURE2,  GL_TEXTURE3,
        GL_TEXTURE4,  GL_TEXTURE5,  GL_TEXTURE6,  GL_TEXTURE7,
        GL_TEXTURE8,  GL_TEXTURE9,  GL_TEXTURE10, GL_TEXTURE11,
        GL_TEXTURE12, GL_TEXTURE13, GL_TEXTURE14, GL_TEXTURE15 };
    
    ASSERT(m_IDBuffer);
    
    glActiveTexture( ConvertToGL[ iChannel ] );
    glBindTexture( GL_TEXTURE_2D, m_IDBuffer );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_texture::Deactivate( void ) const
{
    glActiveTexture( 0 );
    glBindTexture( GL_TEXTURE_2D, 0 );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

const eng_texture& eng_texture::getDefaultTexture( void )
{
    static eng_texture  Texture;
    
    if( Texture.m_IDBuffer == 0 )
    {
        Texture.CreateTexture( xbitmap::getDefaultBitmap() );
    }
    
    return Texture;
}

//-----------------------------------------------------------------------------------

eng_resource_type&  eng_texture::getStaticType( void )
{
    return eng_texture_rsc::getStaticType();
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// eng_texture_type definition
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

struct eng_texture_type : public eng_resource_type
{
    eng_texture_type( const char* pType ) : eng_resource_type( pType, eng_texture_rsc::UID ) {}
    
    virtual eng_resource_base*  CreateInstance      ( void );
    virtual void                KillInstance        ( eng_resource_base* pEntry );
};

static eng_texture_type s_TextureType( "texture" );


//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_texture_rsc_base
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

struct eng_texture_rsc_base : public eng_texture_rsc, public x_simple_job<1, x_light_job>
{
    eng_texture_rsc::info*  m_pInfo = NULL;

    eng_texture_rsc_base( void )
    {
        setupAffinity( AFFINITY_MAIN_THREAD );
    }

    virtual eng_resource_type&  getType             ( void ) const { return s_TextureType; }
    virtual void*               onQTGetAsset        ( void )
    {
        // If we are loaded quality zero then we should return the data
        if ( m_Texture.isValid() )// m_CurMaxQuality == 0 )
            return &m_Texture;
        
        x_LogWarning( "ENG_TEXTURE", "Waiting to Load Texture" );
        return NULL;
    }
    
    virtual xbool onQTLoadResource ( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the info data
        //
        xserialfile SerialFile;
        xstring TexturePath = g_RscMgr.createFileName( m_Guid, iQuality, s_TextureType.getTypeString( ) );

        SerialFile.Load( TexturePath, m_pInfo );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pInfo )
            {
                x_delete( m_pInfo );
                m_pInfo = NULL;
            }
            return FALSE;
        }

        // Set a render job so that it runs in the main thread
        g_Scheduler.StartJobChain( *this );

        return TRUE;
    }
    
    virtual xbool onQTReduceQuality ( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0);
        return TRUE;
    }

    //--------------- 
    // From here down will run in the mainthread
    virtual void onRun( void )
    {
        m_Texture.CreateTexture( m_pInfo->m_Bitmap, FALSE );

        m_Texture.setMinMagFilterMode( m_pInfo->m_Mig, m_pInfo->m_Mag );
        m_Texture.setWrappingMode( m_pInfo->m_uWrapping, m_pInfo->m_vWrapping );
        m_Texture.setAnisotropicMode( eng_texture::ANISOTROPIC_MEDIAN );

        // This is not longer needed
        x_delete( m_pInfo );

        m_pInfo = NULL;

        x_LogMessage( "ENG_TEXTURE", "Load Done" );
    }
};

//-----------------------------------------------------------------------------------

eng_texture_rsc::eng_texture_rsc( void )
{
    
}

//-----------------------------------------------------------------------------------
eng_texture_rsc::~eng_texture_rsc( void )
{
    
}

//-----------------------------------------------------------------------------------
eng_resource_type&  eng_texture_rsc::getStaticType( void )
{
    return s_TextureType;
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_texture_type
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

eng_resource_base* eng_texture_type::CreateInstance( void )
{
    return x_new( eng_texture_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_texture_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_texture_rsc_base*)pEntry );
}

