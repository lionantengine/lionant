//===============================================================================
// INCLUDES
//===============================================================================

#include "eng_Base.h"

//===============================================================================
// FUNCTION
//===============================================================================

//-------------------------------------------------------------------------------

void eng_draw::DrawMarker( const xvector3& Pos, xcolor Color )
{
    const eng_view& View = eng_GetCurrentContext().GetActiveView();

    //
    // Project position on the screen
    //

    // project point into the screen
    xvector3        ScreenPos;
    const xirect&   ViewPortSize = View.getViewport();

    ScreenPos = View.getW2S().Transform3D(Pos);

    // Is marker behind the screen?
    xbool        bBehind = ScreenPos.GetZ() < 0;

    // Clamp the position of the marker in the screen
    xvector3 Min( (f32)ViewPortSize.m_Left,  (f32)ViewPortSize.m_Top,    0 );
    xvector3 Max( (f32)ViewPortSize.m_Right, (f32)ViewPortSize.m_Bottom, 0 );

    ScreenPos = ScreenPos.GetMax( Min ).GetMin( Max );

    // Make screen pos relative to viewport
    ScreenPos -= Min;

    //
    // Allocate vertices
    //
    draw_vertex* pVertex = NULL;
    u16*         pIndex  = NULL;

    Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_ZBUFFER_OFF );

    if( bBehind )
    {
        GetBuffers( &pVertex, 8, &pIndex, 4*3 );
    }
    else
    {
        GetBuffers( &pVertex, 4, &pIndex, 2*3 );
    }

    //
    // Set vertices
    //
    {
        pVertex[0].setup( ScreenPos - xvector3(  0, -8, 0 ), Color );
        pVertex[1].setup( ScreenPos - xvector3( -8,  0, 0 ), Color );
        pVertex[2].setup( ScreenPos - xvector3(  0,  8, 0 ), Color );
        pVertex[3].setup( ScreenPos - xvector3(  8,  0, 0 ), Color );

        pIndex[0] = 0;
        pIndex[1] = 2;
        pIndex[2] = 1;
        pIndex[3] = 2;
        pIndex[4] = 0;
        pIndex[5] = 3;

        // If behind the screen, add a black center.
        if( bBehind )
        {
            xcolor Black(0,0,0,255);

            pVertex[4].setup( ScreenPos - xvector3(  0, -4, 0 ), Black );
            pVertex[5].setup( ScreenPos - xvector3( -4,  0, 0 ), Black );
            pVertex[6].setup( ScreenPos - xvector3(  0,  4, 0 ), Black );
            pVertex[7].setup( ScreenPos - xvector3(  4,  0, 0 ), Black );

            pIndex[6]  = 0+4;
            pIndex[7]  = 2+4;
            pIndex[8]  = 1+4;
            pIndex[9]  = 2+4;
            pIndex[10] = 0+4;
            pIndex[11] = 3+4;
        }
    }

    //
    // Render triangles
    //
    DrawBufferTriangles();
    End();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawBBox( const xbbox& BBox, xcolor UserColor, xbool bSolid )
{
    //
    // Render the wire-frame version
    //
    {
        static const u16    Index[] = { 1,5, 5,7, 7,3, 3,1, 0,4, 4,6, 6,2, 2,0, 3,2, 7,6, 5,4, 1,0} ;
        draw_vertex*        pVertex  = NULL;
        u16*                pIndex   = NULL;
        const s32           nVerts   = 8;
        const s32           nIndices = sizeof(Index)/sizeof(s16);
        xcolor              Color(UserColor);

        // Render lines solid
        Color.m_A = 255;

        GetBuffers( &pVertex, nVerts, &pIndex, nIndices );

        pVertex[0].setup(  BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), Color );
        pVertex[1].setup(  BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), Color );
        pVertex[2].setup(  BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), Color );
        pVertex[3].setup(  BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), Color );
        pVertex[4].setup(  BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), Color );
        pVertex[5].setup(  BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), Color );
        pVertex[6].setup(  BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), Color );
        pVertex[7].setup(  BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), Color );

        x_memcpy( pIndex, Index, sizeof(Index) );

        DrawBufferLines();
    }

    //
    // Render the solid version
    //

    if( bSolid )
    {
        static const u16    Index[]  = {
            0,  1,  2,      0,  2,  3,    // front
            4,  5,  6,      4,  6,  7,    // back
            8,  9,  10,     8,  10, 11,   // top
            12, 13, 14,     12, 14, 15,   // bottom
            16, 17, 18,     16, 18, 19,   // right
            20, 21, 22,     20, 22, 23    // left
        };
        
        draw_vertex*        pVertex  = NULL;
        u16*                pIndex   = NULL;
        const s32           nVerts   = 24;
        const s32           nIndices = sizeof(Index)/sizeof(s16);
        xcolor              Color(UserColor);

        // Render lines solid
        Color.m_A = 128;
        GetBuffers( &pVertex, nVerts, &pIndex, nIndices );
        
        // Front face
        pVertex[0].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 1, Color );
        pVertex[1].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 1, Color );
        pVertex[2].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color );
        pVertex[3].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 0, 0, Color );
       
        // Back face
        pVertex[4].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color );
        pVertex[5].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 1, Color );
        pVertex[6].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 0, Color );
        pVertex[7].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 0, Color );
        
        // Top face
        pVertex[8].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 1, Color );
        pVertex[9].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 1, Color );
        pVertex[10].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color );
        pVertex[11].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 0, Color );
        
        // Bottom face
        pVertex[12].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color );
        pVertex[13].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 1, 1, Color );
        pVertex[14].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 0, Color );
        pVertex[15].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 0, Color );
        
        // Right face
        pVertex[16].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color );
        pVertex[17].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 1, Color );
        pVertex[18].setup( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color );
        pVertex[19].setup( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 0, Color );
        
        // Left face
        pVertex[20].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color );
        pVertex[21].setup( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 1, Color );
        pVertex[22].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color );
        pVertex[23].setup( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 0, Color );
        
        x_memcpy( pIndex, Index, sizeof(Index) );

        DrawBufferTriangles();
    }

}

//-------------------------------------------------------------------------------

void eng_draw::DrawTriangle( const draw_vertex& Vertex1, const draw_vertex& Vertex2, const draw_vertex& Vertex3 )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;
    const s32    nVerts   = 3;
    const s32    nIndices = 3;

    GetBuffers( &pVertex, nVerts, &pIndex, nIndices );

    //
    // Add vertices into the vertex buffer
    //
    pVertex[ 0 ] = Vertex1;
    pVertex[ 1 ] = Vertex2;
    pVertex[ 2 ] = Vertex3;

    //
    // Add indices into the index buffer
    //
    pIndex[ 0 ] = 0;
    pIndex[ 1 ] = 1;
    pIndex[ 2 ] = 2;

    DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawLine( const draw_vertex& Vertex1, const draw_vertex& Vertex2 )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;
    const s32    nVerts   = 2;
    const s32    nIndices = 2;

    GetBuffers( &pVertex, nVerts, &pIndex, nIndices );

    //
    // Add vertices into the vertex buffer
    //
    pVertex[ 0 ] = Vertex1;
    pVertex[ 1 ] = Vertex2;

    //
    // Add indices into the index buffer
    //
    pIndex[ 0 ] = 0;
    pIndex[ 1 ] = 1;

    DrawBufferLines();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawShadedRect( const xrect& Rect, xcolor TL, xcolor TR, xcolor BL, xcolor BR )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;
    const s32    nVerts   = 4;
    const s32    nIndices = 6;

    GetBuffers( &pVertex, nVerts, &pIndex, nIndices );
    
    pVertex[0].setup( Rect.m_Left,  Rect.m_Top,    0, 0, 0, TL );
    pVertex[1].setup( Rect.m_Right, Rect.m_Top,    0, 1, 0, TR );
    pVertex[2].setup( Rect.m_Right, Rect.m_Bottom, 0, 1, 1, BR );
    pVertex[3].setup( Rect.m_Left,  Rect.m_Bottom, 0, 0, 1, BL );

    pIndex[0] = 0;
    pIndex[1] = 1;
    pIndex[2] = 2;
    pIndex[3] = 0;
    pIndex[4] = 2;
    pIndex[5] = 3;

    DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawSolidRect( const xrect& Rect, xcolor Color )
{
    DrawShadedRect( Rect, Color, Color, Color, Color );
}

//-------------------------------------------------------------------------------

void eng_draw::DrawTexturedRect( const xrect& Rect, const xrect& TexCoord, xcolor Color )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;
    const s32    nVerts   = 4;
    const s32    nIndices = 6;

    GetBuffers( &pVertex, nVerts, &pIndex, nIndices );
    
    pVertex[0].setup( Rect.m_Left,  Rect.m_Top,    0, TexCoord.m_Left,  TexCoord.m_Top,     Color );
    pVertex[1].setup( Rect.m_Right, Rect.m_Top,    0, TexCoord.m_Right, TexCoord.m_Top,     Color );
    pVertex[2].setup( Rect.m_Right, Rect.m_Bottom, 0, TexCoord.m_Right, TexCoord.m_Bottom,  Color );
    pVertex[3].setup( Rect.m_Left,  Rect.m_Bottom, 0, TexCoord.m_Left,  TexCoord.m_Bottom,  Color );

    pIndex[0] = 0;
    pIndex[1] = 2;
    pIndex[2] = 1;
    pIndex[3] = 0;
    pIndex[4] = 3;
    pIndex[5] = 2;

    DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawTriangles( const draw_vertex* pUserVertex, s32 nVertices, const u16* pUserIndex, s32 nIndices )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;

    GetBuffers( &pVertex, nVertices, &pIndex, nIndices );

    x_memcpy( pVertex, pUserVertex, sizeof(draw_vertex)*nVertices );
    x_memcpy( pIndex, pUserIndex, sizeof(u16)*nIndices );

    DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawLines( const draw_vertex* pUserVertex, s32 nVertices, const u16* pUserIndex, s32 nIndices )
{
    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;

    GetBuffers( &pVertex, nVertices, &pIndex, nIndices );

    x_memcpy( pVertex, pUserVertex, sizeof(draw_vertex)*nVertices );
    x_memcpy( pIndex, pUserIndex, sizeof(u16)*nIndices );

    DrawBufferLines();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawSphere( xcolor aColor )
{
    static const xcolor Color(0xffffffff);
    struct vertex{ f32 x,y,z,u,v; };
    const f32 S = 1/100.0f;
    static const draw_vertex s_vSphere[] = 
    {
                   //  POSITION                                     UV
                   //  -----------------------------------------    ---------------------
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.050000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    40.4508f*S,    29.3893f*S,   0.000000f,  0.200000f, Color ),
        draw_vertex(    17.2746f*S,    40.4508f*S,    23.7764f*S,   0.100000f,  0.200000f, Color ),
        draw_vertex(    27.9509f*S,    40.4508f*S,     9.0818f*S,   0.200000f,  0.200000f, Color ),
        draw_vertex(    27.9508f*S,    40.4508f*S,    -9.0818f*S,   0.300000f,  0.200000f, Color ),
        draw_vertex(    17.2746f*S,    40.4508f*S,   -23.7764f*S,   0.400000f,  0.200000f, Color ),
        draw_vertex(    -0.0000f*S,    40.4508f*S,   -29.3893f*S,   0.500000f,  0.200000f, Color ),
        draw_vertex(   -17.2746f*S,    40.4508f*S,   -23.7764f*S,   0.600000f,  0.200000f, Color ),
        draw_vertex(   -27.9509f*S,    40.4508f*S,    -9.0818f*S,   0.700000f,  0.200000f, Color ),
        draw_vertex(   -27.9508f*S,    40.4508f*S,     9.0818f*S,   0.800000f,  0.200000f, Color ),
        draw_vertex(   -17.2746f*S,    40.4508f*S,    23.7764f*S,   0.900000f,  0.200000f, Color ),
        draw_vertex(     0.0000f*S,    15.4508f*S,    47.5528f*S,   0.000000f,  0.400000f, Color ),
        draw_vertex(    27.9509f*S,    15.4508f*S,    38.4710f*S,   0.100000f,  0.400000f, Color ),
        draw_vertex(    45.2254f*S,    15.4508f*S,    14.6946f*S,   0.200000f,  0.400000f, Color ),
        draw_vertex(    45.2254f*S,    15.4508f*S,   -14.6946f*S,   0.300000f,  0.400000f, Color ),
        draw_vertex(    27.9508f*S,    15.4508f*S,   -38.4711f*S,   0.400000f,  0.400000f, Color ),
        draw_vertex(    -0.0000f*S,    15.4508f*S,   -47.5528f*S,   0.500000f,  0.400000f, Color ),
        draw_vertex(   -27.9509f*S,    15.4508f*S,   -38.4710f*S,   0.600000f,  0.400000f, Color ),
        draw_vertex(   -45.2254f*S,    15.4508f*S,   -14.6946f*S,   0.700000f,  0.400000f, Color ),
        draw_vertex(   -45.2254f*S,    15.4508f*S,    14.6946f*S,   0.800000f,  0.400000f, Color ),
        draw_vertex(   -27.9508f*S,    15.4508f*S,    38.4711f*S,   0.900000f,  0.400000f, Color ),
        draw_vertex(     0.0000f*S,   -15.4509f*S,    47.5528f*S,   0.000000f,  0.600000f, Color ),
        draw_vertex(    27.9509f*S,   -15.4509f*S,    38.4710f*S,   0.100000f,  0.600000f, Color ),
        draw_vertex(    45.2254f*S,   -15.4509f*S,    14.6946f*S,   0.200000f,  0.600000f, Color ),
        draw_vertex(    45.2254f*S,   -15.4509f*S,   -14.6946f*S,   0.300000f,  0.600000f, Color ),
        draw_vertex(    27.9508f*S,   -15.4508f*S,   -38.4711f*S,   0.400000f,  0.600000f, Color ),
        draw_vertex(    -0.0000f*S,   -15.4508f*S,   -47.5528f*S,   0.500000f,  0.600000f, Color ),
        draw_vertex(   -27.9509f*S,   -15.4508f*S,   -38.4710f*S,   0.600000f,  0.600000f, Color ),
        draw_vertex(   -45.2254f*S,   -15.4509f*S,   -14.6946f*S,   0.700000f,  0.600000f, Color ),
        draw_vertex(   -45.2254f*S,   -15.4509f*S,    14.6946f*S,   0.800000f,  0.600000f, Color ),
        draw_vertex(   -27.9508f*S,   -15.4509f*S,    38.4711f*S,   0.900000f,  0.600000f, Color ),
        draw_vertex(     0.0000f*S,   -40.4509f*S,    29.3893f*S,   0.000000f,  0.800000f, Color ),
        draw_vertex(    17.2746f*S,   -40.4509f*S,    23.7764f*S,   0.100000f,  0.800000f, Color ),
        draw_vertex(    27.9508f*S,   -40.4509f*S,     9.0818f*S,   0.200000f,  0.800000f, Color ),
        draw_vertex(    27.9508f*S,   -40.4509f*S,    -9.0818f*S,   0.300000f,  0.800000f, Color ),
        draw_vertex(    17.2746f*S,   -40.4509f*S,   -23.7764f*S,   0.400000f,  0.800000f, Color ),
        draw_vertex(    -0.0000f*S,   -40.4509f*S,   -29.3893f*S,   0.500000f,  0.800000f, Color ),
        draw_vertex(   -17.2746f*S,   -40.4509f*S,   -23.7764f*S,   0.600000f,  0.800000f, Color ),
        draw_vertex(   -27.9508f*S,   -40.4509f*S,    -9.0818f*S,   0.700000f,  0.800000f, Color ),
        draw_vertex(   -27.9508f*S,   -40.4509f*S,     9.0818f*S,   0.800000f,  0.800000f, Color ),
        draw_vertex(   -17.2746f*S,   -40.4509f*S,    23.7764f*S,   0.900000f,  0.800000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.050000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.150000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.250000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.350000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.450000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.550000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.650000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.750000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.850000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    50.0000f*S,     0.0000f*S,   0.950000f,  0.000000f, Color ),
        draw_vertex(     0.0000f*S,    40.4508f*S,    29.3893f*S,   1.000000f,  0.200000f, Color ),
        draw_vertex(     0.0000f*S,    15.4508f*S,    47.5528f*S,   1.000000f,  0.400000f, Color ),
        draw_vertex(     0.0000f*S,   -15.4509f*S,    47.5528f*S,   1.000000f,  0.600000f, Color ),
        draw_vertex(     0.0000f*S,   -40.4509f*S,    29.3893f*S,   1.000000f,  0.800000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.150000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.250000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.350000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.450000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.550000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.650000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.750000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.850000f,  1.000000f, Color ),
        draw_vertex(     0.0000f*S,   -50.0000f*S,    -0.0000f*S,   0.950000f,  1.000000f, Color )
    };

    static const u16 s_iSphere[] = 
    {
        0,   1,   2,
        42,   2,   3,
        43,   3,   4,
        44,   4,   5,
        45,   5,   6,
        46,   6,   7,
        47,   7,   8,
        48,   8,   9,
        49,   9,  10,
        50,  10,  51,
        1,  11,  12,
        1,  12,   2,
        2,  12,  13,
        2,  13,   3,
        3,  13,  14,
        3,  14,   4,
        4,  14,  15,
        4,  15,   5,
        5,  15,  16,
        5,  16,   6,
        6,  16,  17,
        6,  17,   7,
        7,  17,  18,
        7,  18,   8,
        8,  18,  19,
        8,  19,   9,
        9,  19,  20,
        9,  20,  10,
        10,  20,  52,
        10,  52,  51,
        11,  21,  22,
        11,  22,  12,
        12,  22,  23,
        12,  23,  13,
        13,  23,  24,
        13,  24,  14,
        14,  24,  25,
        14,  25,  15,
        15,  25,  26,
        15,  26,  16,
        16,  26,  27,
        16,  27,  17,
        17,  27,  28,
        17,  28,  18,
        18,  28,  29,
        18,  29,  19,
        19,  29,  30,
        19,  30,  20,
        20,  30,  53,
        20,  53,  52,
        21,  31,  32,
        21,  32,  22,
        22,  32,  33,
        22,  33,  23,
        23,  33,  34,
        23,  34,  24,
        24,  34,  35,
        24,  35,  25,
        25,  35,  36,
        25,  36,  26,
        26,  36,  37,
        26,  37,  27,
        27,  37,  38,
        27,  38,  28,
        28,  38,  39,
        28,  39,  29,
        29,  39,  40,
        29,  40,  30,
        30,  40,  54,
        30,  54,  53,
        41,  32,  31,
        55,  33,  32,
        56,  34,  33,
        57,  35,  34,
        58,  36,  35,
        59,  37,  36,
        60,  38,  37,
        61,  39,  38,
        62,  40,  39,
        63,  54,  40
    };

    const s32 NUM_FACETS_SPHERE   = 80;
    const s32 NUM_VERTICES_SPHERE = 64;
    const s32 NUM_INDICES_SHERE   = NUM_FACETS_SPHERE*3;

    draw_vertex* pVertex  = NULL;
    u16*         pIndex   = NULL;

    GetBuffers( &pVertex, NUM_VERTICES_SPHERE, &pIndex, NUM_INDICES_SHERE );

    x_memcpy( pVertex, s_vSphere, sizeof(draw_vertex)*NUM_VERTICES_SPHERE );
    x_memcpy( pIndex, s_iSphere, sizeof(u16)*NUM_INDICES_SHERE );

    // User defined its own color
    if( aColor != Color )
    {
        for( s32 i=0; i<NUM_VERTICES_SPHERE; i++ )
        {
            pVertex[i].m_Color = aColor;
        }
    }

    DrawBufferTriangles();
}

//-------------------------------------------------------------------------------
/*
void draw_Frustum( const view& View, xcolor Color, f32 Dist )
{
    static u16 Index[] = {0,1,0,2,0,3,0,4,1,2,2,3,3,4,4,1};
    xvector3 P[6];
    s32      X0,X1,Y0,Y1;

    View.GetViewport(X0,Y0,X1,Y1);

    P[0] = View.GetPosition();
    P[1] = View.RayFromScreen( (f32)X0, (f32)Y0, view::VIEW );
    P[2] = View.RayFromScreen( (f32)X0, (f32)Y1, view::VIEW );
    P[3] = View.RayFromScreen( (f32)X1, (f32)Y1, view::VIEW );
    P[4] = View.RayFromScreen( (f32)X1, (f32)Y0, view::VIEW );

    // Normalize so that Z is Dist and move into world
    for( s32 i=1; i<=4; i++ )
    {
        P[i] *= Dist / P[i].GetZ();
        P[i]  = View.ConvertV2W( P[i] );
    }

    P[5] = (P[1] + P[2] + P[3] + P[4]) * 0.25f;

    draw_Begin( DRAW_LINES );
        draw_Color( Color );
        draw_Verts( P, 5 );
        draw_Execute( Index, 8*2 );
        draw_Color( XCOLOR_GREY );
        draw_Vertex(P[0]);
        draw_Vertex(P[5]);
    draw_End();
}
*/

