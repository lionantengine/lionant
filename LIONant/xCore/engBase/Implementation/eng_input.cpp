//
//  File.cpp
//  engBase
//
//  Created by Tomas Arce on 8/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "eng_Base.h"

extern "C" int          _eng_IsKeyDown           ( int KeyCode );
extern "C" int          _eng_WasKeyDown          ( int KeyCode );
extern "C" const int*   _eng_MouseGetIsButtons   ( void );
extern "C" const int*   _eng_MouseGetWasButtons  ( void );
extern "C" void         _eng_MouseGetAnalog      ( int KeyCode, float* X, float* Y );
extern "C" int          _eng_ThouchPadIsButtons  ( int KeyCode );
extern "C" int          _eng_ThouchPadWasButtons ( int KeyCode );
extern "C" void         _eng_ThouchPadValue      ( int KeyCode, float* X, float* Y );

extern "C" int          _eng_IsButtonDown        ( int KeyCode );
extern "C" int          _eng_WasButtonDown       ( int KeyCode );
extern "C" void         _eng_GamePadValue        ( int KeyCode, float* X, float* Y );

//===============================================================================
//===============================================================================
//===============================================================================
// FOR IOS THERE IS NOT SUPPORT FOR KEYBOARD OR MOUSE FOR NOW
//===============================================================================
//===============================================================================
//===============================================================================

#if defined(TARGET_IOS) || defined(TARGET_3DS)
    static int s_EmptyArray[256]={0};
    int          _eng_IsKeyDown           ( int KeyCode ){ return 0; }
    int          _eng_WasKeyDown          ( int KeyCode ){ return 0; }
    const int*   _eng_MouseGetIsButtons   ( void ) { return s_EmptyArray; }
    const int*   _eng_MouseGetWasButtons  ( void ) { return s_EmptyArray; }
    void         _eng_MouseGetAnalog      ( int KeyCode, float* X, float* Y ) { }
#endif


#if defined TARGET_OSX || defined TARGET_IOS

#if defined TARGET_OSX
int    _eng_ThouchPadIsButtons  ( int KeyCode ) { return 0; }
int    _eng_ThouchPadWasButtons ( int KeyCode ) { return 0; }
void   _eng_ThouchPadValue      ( int KeyCode, float* X, float* Y ){}
#endif

void         _eng_GamePadValue        ( int KeyCode, float* X, float* Y ) {}
int          _eng_IsButtonDown        ( int KeyCode ) {return 0;}
int          _eng_WasButtonDown       ( int KeyCode ) {return 0;}

#endif

//===============================================================================
//===============================================================================
//===============================================================================
// ENG KEYBOARD
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------

xbool eng_keyboard::BindDevice( void )
{
    return FALSE;
}

//-------------------------------------------------------------------------------

xbool eng_keyboard::isPressed( digital_gadget GadgetID )
{
    return _eng_IsKeyDown( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_keyboard::wasPressed( digital_gadget GadgetID )
{
    return _eng_WasKeyDown( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_keyboard::isPressed( s32 GadgetID )
{
    return isPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_keyboard::wasPressed( s32 GadgetID )
{
    return wasPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector3d eng_keyboard::getValue( s32 GadgetID )
{
    // Keyboards dont have analog gadggets
    ASSERT( 0 );
    return xvector3d(0);
}

//===============================================================================
//===============================================================================
//===============================================================================
// ENG MOUSE
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------

xbool eng_mouse::BindDevice( void )
{
    return FALSE;
}

//-------------------------------------------------------------------------------

xbool eng_mouse::isPressed( digital_gadget GadgetID )
{
    return _eng_MouseGetIsButtons()[ GadgetID ];
}

//-------------------------------------------------------------------------------

xbool eng_mouse::wasPressed( digital_gadget GadgetID )
{
    return _eng_MouseGetWasButtons()[ GadgetID ];
}

//-------------------------------------------------------------------------------

xvector2 eng_mouse::getValue( analog_gadget GadgetID )
{
    xvector2 Value;
    _eng_MouseGetAnalog( GadgetID, &Value.m_X, &Value.m_Y );
    return Value;
}

//-------------------------------------------------------------------------------

xbool eng_mouse::isPressed( s32 GadgetID )
{
    return isPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_mouse::wasPressed( s32 GadgetID )
{
    return wasPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector3d eng_mouse::getValue( s32 GadgetID )
{
    const xvector2 Val( getValue( analog_gadget(GadgetID) ) );
    return xvector3d( Val.m_X, Val.m_Y, 0 );
}

//===============================================================================
//===============================================================================
//===============================================================================
// ENG THOUCHPAD
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------

xbool eng_touchpad::BindDevice( void )
{
    return FALSE;
}

//-------------------------------------------------------------------------------

xbool eng_touchpad::isPressed( digital_gadget GadgetID )
{
    return _eng_ThouchPadIsButtons( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_touchpad::wasPressed( digital_gadget GadgetID )
{
    return _eng_ThouchPadWasButtons( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector2 eng_touchpad::getValue( analog_gadget GadgetID )
{
    xvector2 Values;
    _eng_ThouchPadValue( GadgetID, &Values.m_X, &Values.m_Y );
    return Values;
}

//-------------------------------------------------------------------------------

eng_touchpad::analog_gadget eng_touchpad::getTouchRel( s32 iThouch )
{
    return eng_touchpad::analog_gadget(iThouch+ANALOG_TOUCH_1_REL);
}

//-------------------------------------------------------------------------------

eng_touchpad::analog_gadget eng_touchpad::getTouchAbs( s32 iThouch )
{
    return eng_touchpad::analog_gadget(iThouch+ANALOG_TOUCH_1_ABS);
}

//-------------------------------------------------------------------------------

eng_touchpad::digital_gadget eng_touchpad::getTouchBtn( s32 iThouch )
{
    return eng_touchpad::digital_gadget(iThouch+BTN_TOUCH_1);
}

//-------------------------------------------------------------------------------

xbool eng_touchpad::isPressed( s32 GadgetID )
{
    return isPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_touchpad::wasPressed( s32 GadgetID )
{
    return wasPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector3d eng_touchpad::getValue( s32 GadgetID )
{
    const xvector2 Val( getValue( analog_gadget(GadgetID) ) );
    return xvector3d( Val.m_X, Val.m_Y, 0 );
}

//===============================================================================
//===============================================================================
//===============================================================================
// ENG GAMEPAD
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------

xbool eng_gamepad::BindDevice( void )
{
    return FALSE;
}

//-------------------------------------------------------------------------------

xbool eng_gamepad::isPressed( digital_gadget GadgetID )
{
    return _eng_IsButtonDown( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_gamepad::wasPressed( digital_gadget GadgetID )
{
    return _eng_WasButtonDown( int(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector2 eng_gamepad::getValue( analog_gadget GadgetID )
{
    xvector2 Values;
    _eng_GamePadValue( GadgetID, &Values.m_X, &Values.m_Y );
    return Values;
}

//-------------------------------------------------------------------------------

xbool eng_gamepad::isPressed( s32 GadgetID )
{
    return isPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xbool eng_gamepad::wasPressed( s32 GadgetID )
{
    return wasPressed( digital_gadget(GadgetID) );
}

//-------------------------------------------------------------------------------

xvector3d eng_gamepad::getValue( s32 GadgetID )
{
    const xvector2 Val( getValue( analog_gadget(GadgetID) ) );
    return xvector3d( Val.m_X, Val.m_Y, 0 );
}


//===============================================================================
//===============================================================================
//===============================================================================
// ENG VIRTUAL DEVICE
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------
eng_input_vdevice::eng_input_vdevice( void )
{
    x_memset(m_Logical, 0, sizeof(m_Logical));
    x_memset(m_Mappings, 0, sizeof(m_Mappings));

    for ( s32 i = 0; i < X_PLATFORM_COUNT; i++ )
    {
        m_MappingCount[i]   = 0;
    }

    m_ActiveContexts    = 0;
    m_ControllerID      = 0;
}

//-------------------------------------------------------------------------------
const eng_input_vdevice::logical_gadget& eng_input_vdevice::getLogicalGaget( s32 iGadget ) const
{
    ASSERT(iGadget >= 0 && iGadget < MAX_LOGICAL);
    return m_Logical[iGadget];
}

//-------------------------------------------------------------------------------
eng_input_vdevice::physical_to_logical_map& eng_input_vdevice::CreateMapping( xplatform Platform )
{
    s32 Index = GetPlatformIndex(Platform);
    ASSERT(Index > -1 );
    ASSERT(m_MappingCount[Index] >= 0 && m_MappingCount[Index] < MAX_MAPPINGS);

    m_Mappings[Index][m_MappingCount[Index]].m_DimensionMode = DIMENSION_XYZ_XYZ;
    return m_Mappings[Index][m_MappingCount[Index]++];
}

//-------------------------------------------------------------------------------
void eng_input_vdevice::EnableContexts( u32 Contexts )
{
    m_ActiveContexts |= Contexts;
}

//-------------------------------------------------------------------------------
void eng_input_vdevice::DisableContexts( u32 Contexts )
{
    m_ActiveContexts = (m_ActiveContexts & ~Contexts);
}

//-------------------------------------------------------------------------------
s32 eng_input_vdevice::GetPlatformIndex( xplatform Platform ) const
{
    ASSERT(Platform > X_PLATFORM_NULL && Platform < X_PLATFORM_COUNT);
    ASSERT(0 == (Platform & (Platform - 1)));

    s32 Index = -1;
    for ( s32 i = 0; i < sizeof(xplatform); i++ )
    {
        if ( Platform == (1 << i) )
        {
            Index = i;
        }
    }
    return Index;
}

//-------------------------------------------------------------------------------

void eng_input_vdevice::Update( xplatform Platform )
{
    // if the user did not choose a platform we assume to be the current build platform
    const s32 Index = (Platform == X_PLATFORM_NULL) ? X_PLATFORM : Platform ;
    
    //
    // Clear old values
    //
    for ( s32 i = 0; i < m_MappingCount[Index]; i++ )
    {
        const physical_to_logical_map&  Map     = m_Mappings[Index][i];
        
        if( m_ActiveContexts & Map.m_ContextMask )
            continue;
        
        m_Logical[Map.m_iLogicalMapping].m_IsValue.Zero();
    }
    
    //
    // Set new values
    //
    for ( s32 i = 0; i < m_MappingCount[Index]; i++ )
    {
        const physical_to_logical_map&  Map     = m_Mappings[Index][i];
        
        // Should we deal with this map?
        if( m_ActiveContexts & Map.m_ContextMask )
            continue;
        
        logical_gadget&                 Logical = m_Logical[Map.m_iLogicalMapping];
        xvector3                        Value;
        
        ASSERT(NULL != Map.m_pDevice);
        
        if ( Map.m_bGadgetIsAnalog )
        {
            Value  = Map.m_pDevice->getValue(Map.m_GadgetID);
        }
        else
        {
            Value.Zero();
            Value.m_X = Map.m_pDevice->isPressed(Map.m_GadgetID) ? 1.0f : 0.0f;
        }
        Logical.m_WasValue = Map.m_pDevice->wasPressed(Map.m_GadgetID);

        if ( Map.m_DimensionMode & (DIMENSION_X_X|DIMENSION_Y_X|DIMENSION_Z_X) )
        {
            const f32 DimensionValue = Value(((Map.m_DimensionMode>>0)&3)-1);
            if ( x_Abs(Logical.m_IsValue.m_X) < x_Abs(DimensionValue) )
            {
                Logical.m_IsValue.m_X           = DimensionValue * Map.m_Scale.m_X;
            }
        }

        if ( Map.m_DimensionMode & (DIMENSION_X_Y|DIMENSION_Y_Y|DIMENSION_Z_Y) )
        {
            const f32 DimensionValue = Value(((Map.m_DimensionMode>>2)&3)-1);
            if ( x_Abs(Logical.m_IsValue.m_Y) < x_Abs(DimensionValue) )
            {
                Logical.m_IsValue.m_Y           = DimensionValue * Map.m_Scale.m_Y;
            }
        }

        if ( Map.m_DimensionMode & (DIMENSION_X_Z|DIMENSION_Y_Z|DIMENSION_Z_Z) )
        {
            const f32 DimensionValue = Value(((Map.m_DimensionMode>>4)&3)-1);
            if ( x_Abs(Logical.m_IsValue.m_Z) < x_Abs(DimensionValue) )
            {
                Logical.m_IsValue.m_Z           = DimensionValue * Map.m_Scale.m_Z;
            }
        }
    }
}


