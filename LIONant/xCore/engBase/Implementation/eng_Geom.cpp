//
//  eng_Geom.cpp
// 
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_geom_type
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void ForceMaterialLinking( void );

struct eng_geom_type : public eng_resource_type
{
    eng_geom_type( const char* pType ) : eng_resource_type( pType, eng_geom_rsc::UID ) { ForceMaterialLinking(); }

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

static eng_geom_type s_GeomType( "mesh" );


//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_geom_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_geom_rsc_base : public eng_geom_rsc, public x_simple_job<1, x_light_job>
{
    void* m_pTempData = NULL;

    enum loading_state
    {
        LOADING_STATE_NOT_STARTED,
        LOADING_STATE_COMPILING,
        LOADING_STATE_PULLING_RESOURCES,
        LOADING_STATE_READY
    };

    loading_state   m_LoadingState = LOADING_STATE_NOT_STARTED; 

    eng_geom_rsc_base( void )
    {
        setupAffinity( AFFINITY_MAIN_THREAD );
    }

    virtual eng_resource_type&  getType( void ) const { return s_GeomType; }
    virtual void*               onQTGetAsset( void )
    {
        if ( m_LoadingState == LOADING_STATE_READY )
            return m_pGeomRSC;

        if ( m_LoadingState == LOADING_STATE_PULLING_RESOURCES )
        {
            for ( s32 i = 0; i < m_pGeomRSC->m_nInformeds; i++ )
            {
                if( NULL == m_pGeomRSC->m_pInformed.m_Ptr[ i ].getInformedRef( ).getAsset( ) )
                    return NULL;
            }

            m_LoadingState = LOADING_STATE_READY;
            return m_pGeomRSC;
        }

        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the geom data
        //
        xserialfile SerialFile;
        xstring GeomPath = g_RscMgr.createFileName( m_Guid, iQuality, s_GeomType.getTypeString( ) );

        // Dont let the serial file free the temp data in the constructor
        SerialFile.DontFreeTempData();

        SerialFile.Load( GeomPath, m_pGeomRSC );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pGeomRSC )
            {
                x_delete( m_pGeomRSC );
                m_pGeomRSC = NULL;
            }
            return FALSE;
        }

        //
        // Tell the system to start loading the materials
        //
        for ( s32 i = 0; i < m_pGeomRSC->m_nInformeds; i++ )
        {
            u8 Type = eng_resource_guid::getType( m_pGeomRSC->m_pInformed.m_Ptr[ i ].m_Guid );
            ASSERT( Type == eng_informed_material_rsc::UID );
            m_pGeomRSC->m_pInformed.m_Ptr[ i ].getInformedRef( ).getAsset( );
        }

        //
        // Get the temp data
        // 
        m_pTempData = SerialFile.getTempData();
        ASSERT( m_pTempData );

        // Set a render job so that it runs in the main thread
        m_LoadingState = LOADING_STATE_COMPILING;
        g_Scheduler.StartJobChain( *this );

        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }

    virtual void onRun( void )
    {
        //
        // Copy to VRam all the Indices
        //
        eng_ibuffer* pIBuffer = x_new( eng_ibuffer, m_pGeomRSC->m_nIndexStreams, 0 );
        for ( s32 i = 0; i < m_pGeomRSC->m_nIndexStreams; i++ )
        {
            eng_ibuffer&                Buffer = pIBuffer[ i ];
            const temp_index_stream&    TIBuffer = m_pGeomRSC->m_pTempIndexStream.m_Ptr[ i ];

            const eng_ibuffer::index_data_desc  Desc = m_pGeomRSC->m_pTempIndexStream.m_Ptr->m_IndexSize == 2 ? eng_ibuffer::DATA_DESC_U16 : eng_ibuffer::DATA_DESC_U32;
            Buffer.CreateStaticBuffer( Desc, TIBuffer.m_nIndices, TIBuffer.m_p16Indices.m_Ptr );
        }

        //
        // Copy to VRam all the Vertices
        //
        geom_rsc::runtime_vertex_buffer* pVBuffer = x_new( geom_rsc::runtime_vertex_buffer, m_pGeomRSC->m_nVertexStreams, 0 );
        for ( s32 i = 0; i < m_pGeomRSC->m_nVertexStreams; i++ )
        {
            geom_rsc::runtime_vertex_buffer&        Buffer = pVBuffer[ i ];
            const temp_vertex_stream&               TVBuffer = m_pGeomRSC->m_pTempVertexStream.m_Ptr[ i ];

            // Copy all the verts to vram
            Buffer.m_VBuffer.CreateStaticBuffer( TVBuffer.m_VertSize, TVBuffer.m_nVertices, TVBuffer.m_pVertices.m_Ptr );
        }
        
        //
        // set the new pointers
        //
        m_pGeomRSC->m_pVertexBuffer = pVBuffer;
        m_pGeomRSC->m_pIndexBuffer  = pIBuffer;

        //
        // Free the temp data
        //
        x_delete( (xbyte*)m_pTempData );

        // By setting this pointer to NULL we signal the system we are ready with the geom
        m_pTempData     = NULL;
        m_LoadingState  = LOADING_STATE_PULLING_RESOURCES;

        x_LogMessage( "ENG_GEOM", "Load Done" );
    }
};

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_resource_type
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

eng_resource_base* eng_geom_type::CreateInstance( void )
{

    return x_new( eng_geom_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_geom_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_geom_rsc_base*)pEntry );
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// geom_system
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

static const char s_DefaultGeomFragmentShader[ ] = R"(

#ifdef GL_ES
    precision mediump float; // precision highp float;
    varying lowp vec4   vaColor;
#else
    varying vec4        vaColor;
#endif

    void main( void )
    {
        gl_FragColor = vaColor;
    }
)";

static const char s_DefaultGeomVertexShader[] = R"(

#ifdef GL_ES
    precision highp float;
#endif

    attribute vec3      aPosition;
    attribute vec3      aNormal;

    varying vec4        vaColor;

    uniform mat4        uL2C;
    uniform vec3        uLightDirLocalSpace;

    void main()
    {
        float     I = clamp( dot( uLightDirLocalSpace, aNormal ), 0., 1. );
        vaColor     = vec4(I,I,I,1);

        // this may be equivalent (gl_Position = ftransform();)
        gl_Position = uL2C * vec4( aPosition, 1. );
    }
)";

//---------------------------------------------------------------------------------
struct geom_system;
static geom_system* s_pGeomSystem = NULL;

struct geom_system
{
    eng_shader_program  m_Program;
    eng_fshader         m_fShader;
    eng_vshader         m_vShader;
    eng_vertex_desc     m_VertDesc;
    
    xsafe_array<eng_vertex_desc::attribute_link,2>  m_AttrLink;

    geom_system( void ) {}

    void init( void )
    {
        m_AttrLink[0].Setup( "aPosition", eng_vertex_desc::ATTR_USAGE_POSITION );
        m_AttrLink[1].Setup( "aNormal",   eng_vertex_desc::ATTR_USAGE_NORMAL );
        
        m_vShader.LoadFromMemory( s_DefaultGeomVertexShader, sizeof( s_DefaultGeomVertexShader ), &m_AttrLink[0], 2, FALSE );

        m_fShader.LoadFromMemory( s_DefaultGeomFragmentShader, sizeof(s_DefaultGeomFragmentShader) );

        m_Program.LinkShaders( m_vShader, m_fShader );

        m_Program.LinkRegisterToUniformVariable( 0, "uL2C" );
        m_Program.LinkRegisterToUniformVariable( 1, "uLightDirLocalSpace" );

        s_pGeomSystem = this;
    }

    ~geom_system( void )
    {
        m_Program.Destroy( );
        m_fShader.Destroy( );
        s_pGeomSystem = NULL;
    }
};


//---------------------------------------------------------------------------------

void* _eng_InitGeomSystem( void )
{
    geom_system* pGeomSystem = x_new( geom_system, 1, 0 );

    if ( pGeomSystem )
        pGeomSystem->init( );

    return pGeomSystem;
}

//---------------------------------------------------------------------------------

void _eng_KillGeomSystem( void* pPtr )
{
    x_delete( (geom_system*)pPtr );
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// geom_rsc
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

eng_geom_rsc::geom_rsc::geom_rsc( xserialfile& SerialFile )
{ 
    ASSERT( SerialFile.GetResourceVersion( ) == VERSION );

}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_geom_rsc
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

eng_geom_rsc::~eng_geom_rsc( void )
{
    if ( m_pGeomRSC )
    {
        x_delete( m_pGeomRSC );
        m_pGeomRSC = NULL;
    }
}

//---------------------------------------------------------------------------

eng_resource_type& eng_geom_rsc::getStaticType( void )
{
    return s_GeomType;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_geom
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

const eng_geom_rsc::geom_rsc* eng_geom::Resolve( void )
{
    const eng_geom_rsc::geom_rsc* pGeomRsc = m_GeomRscRef.getAsset( );

    return pGeomRsc;
}

//---------------------------------------------------------------------------

void eng_geom::setup( const eng_geom_rsc::ref& GeomRef )
{
    m_GeomRscRef = GeomRef;
}

//---------------------------------------------------------------------------

void eng_geom::RenderGeom( const xmatrix4* pL2W, const xvector3d* pLocalSpaceLightDir, s32 nBones )
{
    const eng_geom_rsc::geom_rsc* const pGeomRSC = m_GeomRscRef.getAsset();
    if ( pGeomRSC == NULL )
        return;

    const eng_geom_rsc::geom_rsc& GeomRSC = *pGeomRSC;
    
    for ( s32 m = 0; m < GeomRSC.m_nMeshes; m++ )
    {
        const eng_geom_rsc::mesh& Mesh = GeomRSC.m_pMesh.m_Ptr[ m ];
        
        for ( s32 s = 0; s<Mesh.m_nSubMeshs; s++ )
        {
            const eng_geom_rsc::submesh& Submesh = GeomRSC.m_pSubMesh.m_Ptr[ Mesh.m_iSubMesh + s ];
            ASSERT( Submesh.m_iMesh == m );

            const eng_geom_rsc::streams_ref& Ref = Submesh.m_StreamRef;

            // Active Index buffer
            const eng_ibuffer& IBuffer = GeomRSC.m_pIndexBuffer[ Ref.m_iIStream ];
            IBuffer.Activate( );

            // Activate the shader
            ASSERT( Submesh.m_iInformed >= 0 );
            ASSERT( Submesh.m_iInformed < GeomRSC.m_nInformeds );
            const eng_informed_material_rsc::informed_rsc&  Informed            = *GeomRSC.m_pInformed.m_Ptr[ Submesh.m_iInformed ].getInformedRef().getAsset();
            const eng_material_rsc::material_rsc&           Material            = *Informed.getMaterialRef( ).getAsset();

            ASSERT( Submesh.m_iInformedTech >= 0 );
            ASSERT( Submesh.m_iInformedTech < Informed.m_nTechniques );
            const eng_informed_material_rsc::technique&     InformedTechnique   = Informed.m_pTechnique.m_Ptr[ Submesh.m_iInformedTech ];
            const eng_material_rsc::tech_type&              MaterialTechInfo    = Material.m_TechType[ Submesh.m_TechniqueType ][ Submesh.m_nWeights ];
            
            ASSERT( MaterialTechInfo.m_iTechnique >= 0 );
            ASSERT( MaterialTechInfo.m_iTechnique < 100 );
            const eng_material_rsc::technique&              MaterialTechnique   = Material.m_pTechnique.m_Ptr[ MaterialTechInfo.m_iTechnique ];
            const eng_shader_program&                       Program             = MaterialTechnique.getShaderProgram( );           

            // Activate the program
            Program.Activate();

            // Active Vertex Buffer
            const eng_geom_rsc::geom_rsc::runtime_vertex_buffer& RunTimeaVBuffer = GeomRSC.m_pVertexBuffer[ Submesh.m_StreamRef.m_iVStream ];
            RunTimeaVBuffer.m_VBuffer.Activate( 
                GeomRSC.m_pVertDesc.m_Ptr[Submesh.m_StreamRef.m_iVStream], 
                MaterialTechnique.getVShaderRef( ).getAsset( )->getVShader(),
                Submesh.m_StreamRef.m_iVBase );

            //
            // Set the system/material commands
            //
            s32 iL2CRegister = -1;
            s32 iLightRegister = -1;
            for ( s32 i = 0; i < MaterialTechInfo.m_nSystemCmds; i++ )
            {
                const s32 iCmd = MaterialTechInfo.m_iSystemCmd + i;
                ASSERT( iCmd < Material.m_nSystemCmds );
                
                eng_material_rsc::system_cmds& Cmd = Material.m_pSystemCmd.m_Ptr[iCmd];
                switch ( Cmd.m_Type )
                {
                case eng_shader_program::UNIFORM_TYPE_SYSTEM_L2C:
                    if ( Submesh.m_TechniqueType == eng_material_rsc::TECH_TYPE_RIGID )
                    {
                        Program.setUniformVariable( Cmd.m_iRegister, *pL2W );
                    }
                    else
                    {
                        iL2CRegister = Cmd.m_iRegister;
//                        Program.setUniformVariableArray( Cmd.m_iRegister, 0, pL2W, nBones );
                    }
                    break;
                case eng_shader_program::UNIFORM_TYPE_SYSTEM_L2V:
                    // Program.setUniformVariable( InformedCmd.m_iRegister, View.getW2V( ) * pL2W[0] );
                ASSERT(0 );
                    break;
                case eng_shader_program::UNIFORM_TYPE_SYSTEM_V2C:
                    //Program.setUniformVariable( InformedCmd.m_iRegister, View.getV2C( ) );
                    ASSERT(0 );
                    break;
                case eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_L:
                if ( Submesh.m_TechniqueType == eng_material_rsc::TECH_TYPE_RIGID )
                {
                    Program.setUniformVariable( Cmd.m_iRegister, *pLocalSpaceLightDir );
                }
                else
                {
                    iLightRegister = Cmd.m_iRegister;
                //    Program.setUniformVariableArray( Cmd.m_iRegister, 0, pLocalSpaceLightDir, nBones );
                }
                    break;
                case eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_V:
                    //Program.setUniformVariable( InformedCmd.m_iRegister, (View.getW2V( ) * pL2W[0] )*LighDir );
                ASSERT(0 );
                    break;
                default: 
                    ASSERT(0);
                }
            }

            //
            // Set the inform commands
            //
            for ( s32 i = 0; i < InformedTechnique.m_nCmds; i++ )
            {
                const s32 iCmd = InformedTechnique.m_iCmd + i;
                ASSERT( iCmd < Informed.m_nCmds );

                const eng_informed_material_rsc::cmds&  InformedCmd = Informed.m_pCmd.m_Ptr[ iCmd ]; 

                ASSERT( InformedCmd.m_iShader >= 0 );
                ASSERT( InformedCmd.m_iShader < 3  );
                ASSERT( InformedCmd.m_iValue >= 0 );
                ASSERT( InformedCmd.m_iValue < Informed.m_nUniformValues );
                const eng_informed_material_rsc::uniform_value& Value = Informed.m_pUniformValue.m_Ptr[ InformedCmd.m_iValue ];
                
                if ( Value.m_xCallbackGUID )
                {
                    // Send the register, the program, and the type.
                    ASSERT(0);
                }
                else
                {
                /*
                    static eng_texture s_DefaultTexture;
                    if( s_DefaultTexture.isValid() == FALSE )
                    {
                       s_DefaultTexture.CreateTexture( xbitmap::getDefaultBitmap() ); 
                    }
                */
                    switch ( Value.m_Type )
                    {
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA:
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGB:
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL:
                        //s_DefaultTexture.Activate( InformedCmd.m_iRegister );
                        Value.getTexture().getAsset()->Activate( InformedCmd.m_iRegister );
                        break;
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT:
                        Program.setUniformVariable( InformedCmd.m_iRegister, Value.m_F[0] );
                        break;
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_V2:
                        Program.setUniformVariable2F( InformedCmd.m_iRegister, Value.m_F );
                        break;
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_V3:
                        Program.setUniformVariable3F( InformedCmd.m_iRegister, Value.m_F );
                        break;
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_V4:
                        Program.setUniformVariable4F( InformedCmd.m_iRegister, Value.m_F );
                        break;
                    case eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL:
                        Program.setUniformVariable( InformedCmd.m_iRegister, (s32)Value.m_Bool );
                        break;
                    default:
                        ASSERT(0 );
                    }
                }
            }
            
            // Render the Triangles
            if ( Submesh.m_TechniqueType == eng_material_rsc::TECH_TYPE_RIGID )
            {
                IBuffer.RenderTriangles( Ref.m_ICount, Ref.m_iIBase );
            }
            else
            {
                auto* pCmd    = &GeomRSC.m_pCmds.m_Ptr[ Submesh.m_iCmd ];
                s32   iOffset = Ref.m_iIBase;
                for ( s32 i = 0; i < Submesh.m_nCmds; i++ )
                {
                    auto& Cmd = pCmd[i];

                    switch ( Cmd.m_Type )
                    {
                        case eng_geom_rsc::CMD_LOAD_MATRIX:
                            ASSERT( Cmd.m_iSrcMatrix>=0 );
                            ASSERT( Cmd.m_iSrcMatrix < nBones );
                            ASSERT( Cmd.m_iMatrixCacheOffset>=0 );
                            ASSERT( Cmd.m_iMatrixCacheOffset < 64 );
                            Program.setUniformVariableArray( iL2CRegister,   Cmd.m_iMatrixCacheOffset, &pL2W[Cmd.m_iSrcMatrix], 1 );
                            Program.setUniformVariableArray( iLightRegister, Cmd.m_iMatrixCacheOffset, &pLocalSpaceLightDir[Cmd.m_iSrcMatrix], 1 );
                        break;
                        case eng_geom_rsc::CMD_RENDER:
                            IBuffer.RenderTriangles( Cmd.m_IndexCount, iOffset );
                            iOffset += Cmd.m_IndexCount;
                        break;
                        default:
                        ASSERT(0);
                    }
                }
            }
        }
    }
}

//---------------------------------------------------------------------------

void eng_geom::RenderGeom( const xmatrix4& L2W, const xvector3d& LocalSpaceLightDir )
{
    RenderGeom( &L2W, &LocalSpaceLightDir, 1 );
}