//===============================================================================
// INCLUDES
//===============================================================================
#include "eng_Base.h"

extern "C" int _eng_DrawInCorrectContext( void );

#ifdef TARGET_3DS

    extern "C" void _eng_SetupShaderProgram2D( GLuint Program );

#else

    #define _eng_SetupShaderProgram2D(Program)

#endif


//===============================================================================
// DEFINES
//===============================================================================

#define VERTEX_BUFFER_COUNT        30000
#define INDEX_BUFFER_COUNT         (VERTEX_BUFFER_COUNT*3)
#define CMD_BUFFER_COUNT           40000

//===============================================================================
// FRAGMENT SHADER
//===============================================================================

#ifdef TARGET_3DS
static const char s_DrawFragmentShader[] =
{
    0
};
#elif defined(TARGET_PC)
static const char s_DrawFragmentShader[] =
"   #ifdef GL_ES\n"
"   precision mediump float; // precision highp float;\n"
"   varying lowp vec4   Color;\n"
"   #else\n"
"       varying vec4   Color;\n"
"   #endif\n"
"   varying vec2        Texcoord;\n"
"   uniform sampler2D   Texture;\n"
"   void main( void )\n"
"   {\n"
"       gl_FragColor = texture2D( Texture, Texcoord.xy ) * Color;\n"
"   }\n";
#else
static const char s_DrawFragmentShader[] = R"(

    #ifdef GL_ES
    precision mediump float; // precision highp float;

    varying lowp vec4   Color;

    #else

    varying vec4   Color;

    #endif


    varying vec2        Texcoord;
    uniform sampler2D   Texture;

    void main( void )
    {
        gl_FragColor = texture2D( Texture, Texcoord.xy ) * Color;
    }

)";
#endif

//===============================================================================
// VERTEX SHADER
//===============================================================================

#ifdef TARGET_3DS

__align(8) static const unsigned char s_DrawVertexShader[] =
{
    0x44, 0x56, 0x4c, 0x42, 0x01, 0x00, 0x00, 0x00, 0xa0, 0x00, 0x00, 0x00, 0x44, 0x56, 0x4c, 0x50,
    0x02, 0x0d, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x58, 0x00, 0x00, 0x00,
    0x06, 0x00, 0x00, 0x00, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x88, 0x00, 0x00, 0x00,
    0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe2, 0x0b, 0x01, 0x10, 0xe2, 0x0b, 0x02, 0x20, 0xe2, 0x0b,
    0x03, 0x30, 0xe2, 0x0b, 0x04, 0xf0, 0x01, 0x4c, 0x00, 0x00, 0x00, 0x84, 0x05, 0x00, 0x00, 0x90,
    0x05, 0x80, 0x02, 0x3a, 0x04, 0x28, 0x20, 0x20, 0x04, 0x10, 0x40, 0x4c, 0x00, 0x00, 0x00, 0x88,
    0x00, 0x00, 0x00, 0x84, 0x68, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x64, 0xc3, 0x06, 0x80,
    0x07, 0x00, 0x00, 0x00, 0x62, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x61, 0xc3, 0x06, 0x80,
    0x07, 0x00, 0x00, 0x00, 0x6f, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x80,
    0x03, 0x00, 0x00, 0x00, 0x56, 0x53, 0x68, 0x61, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x73, 0x68, 0x00,
    0x44, 0x56, 0x4c, 0x45, 0x02, 0x0d, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
    0x07, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
    0x54, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x84, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
    0x9c, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0xbc, 0x00, 0x00, 0x00, 0x45, 0x00, 0x00, 0x00,
    0x02, 0x00, 0x08, 0x00, 0x00, 0xfe, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x06, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0x0b, 0x00, 0x00, 0x00, 0x02, 0x00, 0x01, 0x00, 0x0c, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x02, 0x00, 0x01, 0x00,
    0x0f, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x36, 0x00, 0x00, 0x00,
    0x02, 0x00, 0x02, 0x00, 0x41, 0x00, 0x00, 0x00, 0x10, 0x00, 0x13, 0x00, 0x6c, 0x5f, 0x70, 0x6f,
    0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x65, 0x6e, 0x64, 0x6d,
    0x61, 0x69, 0x6e, 0x00, 0x61, 0x50, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x78, 0x79,
    0x7a, 0x77, 0x00, 0x61, 0x54, 0x65, 0x78, 0x43, 0x6f, 0x6f, 0x72, 0x64, 0x2e, 0x78, 0x79, 0x7a,
    0x77, 0x00, 0x61, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x2e, 0x78, 0x79, 0x7a, 0x00, 0x4c, 0x32, 0x43,
    0x00, 0x00, 0x00, 0x00
};

#elif defined(TARGET_PC)

static const char s_DrawVertexShader[] =
"   #ifdef GL_ES\n"
"   precision highp float;\n"
"   #endif\n"
"   attribute vec3      aPosition;\n"
"   attribute vec2      aTexCoord;\n"
"   attribute vec4      aColor;\n"
"   varying vec4        Color;\n"
"   varying vec2        Texcoord;\n"
"   uniform mat4        L2C;\n"
"   void main()\n"
"   {\n"
"       // Pass along the vertex color to the pixel shader\n"
"       Color       = aColor;\n"
"       Texcoord    = aTexCoord;\n"
"       // this may be equivalent (gl_Position = ftransform();)\n"
"       gl_Position = L2C * vec4( aPosition, 1. );\n"
"   }\n";

#else

static const char s_DrawVertexShader[] = R"(

    #ifdef GL_ES
    precision highp float;
    #endif

    attribute vec3      aPosition;
    attribute vec2      aTexCoord;
    attribute vec4      aColor;

    varying vec4        Color;
    varying vec2        Texcoord;

    uniform mat4        L2C;

    void main()
    {
        // Pass along the vertex color to the pixel shader
        Color       = aColor;
        Texcoord    = aTexCoord;
        
        // this may be equivalent (gl_Position = ftransform();)
        gl_Position = L2C * vec4( aPosition, 1. );
    }

)";

#endif

//===============================================================================
// STRUCTURES
//===============================================================================

struct draw_cmd
{
    u32                                     m_Mode;                 // Draw mode used for this primitive
    u16                                     m_iPrimitiveList;       // Primitive list for this draw command
    u16                                     m_VertexOffset;         // offset from the vertex buffer to access these primitive vertices
    u16                                     m_IndexOffset;          // offset from the index buffer to access these primitive indices
    u16                                     m_nVertices;            // Number of vertices that this primitive uses
    u16                                     m_nIndices;             // Number of indices that this primitive uses
    u16                                     m_iMatrix;              // Index to the matrix been used
    u16                                     m_iTexture;             // Index to the texture been used
    u16                                     m_iProgram;             // Index to the program been used
};

//-------------------------------------------------------------------------------

struct draw_primitive_list
{
    eng_vbuffer                             m_VertexBuffer;
    eng_ibuffer                             m_IndexBuffer;
    u32                                     m_iFrame            = 100000;               // What frame number was this primitive list last used
    xbool                                   m_bLocked           = FALSE;
};

//-------------------------------------------------------------------------------

struct draw_stats
{
    s32                                     m_nCmds         = 0;
    s32                                     m_nTriangles    = 0;
    s32                                     m_nLines        = 0;
};

//-------------------------------------------------------------------------------

struct raster_states
{
                                                raster_states( void );
                                               ~raster_states( void );
    
    void                                        WireFrameAndCull ( s32 iRaster );
    void                                        ZBuffer          ( s32 iZbuffer );
    void                                        BlendModes       ( s32 iBlend );

    xsafe_array<u32, 4>                         m_RasterStateMask;
    xsafe_array<u32, 3>                         m_DepthStateMask;
    xsafe_array<u32, 4>                         m_BlendStateMask;
    
#ifdef ENG_OPEN_GL
    xsafe_array<s32, 4>                         m_RasterState;
    xsafe_array<s32, 3>                         m_DepthState;
    xsafe_array<s32, 4>                         m_BlendState;
#endif
};

//-------------------------------------------------------------------------------

class draw_local : public eng_draw
{
public:
    //                                        draw_local              ( void );
    //                                       ~draw_local              ( void );
    void                                    Init                    ( void );
    void                                    Kill                    ( void );
    
    void                                    GetBuffers              ( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices );
    void                                    DrawBufferTriangles     ( void );
    void                                    DrawBufferLines         ( void );
    void                                    Flush                   ( void );
    
    void                                    Begin                   ( u32 Mode );
    void                                    End                     ( void );
    void                                    PageFlip                ( void );
    void                                    SetL2W                  ( const xmatrix4& M );
    void                                    ClearL2W                ( void );
    void                                    SetTexture              ( const eng_texture& Texture );
    void                                    ClearTexture            ( void );
    
    void                                    SetProgram              ( const eng_shader_program& program );
    void                                    ClearProgram            ( void );
    
    eng_vshader&                            GetVertexShader         ( void ) { return m_VertexShader;   }
    eng_fshader&                            GetFragmentShader       ( void ) { return m_FragmentShader; }
    eng_shader_program&                     GetShaderProgram        ( void ) { return m_ShaderProgram;  }
    eng_vertex_desc&                        GetVertexDesc           ( void ) { return m_VertexDesc;     }
    eng_texture&                            GetWhiteTexture         ( void ) { return m_WhiteTexture;   }
    
protected:
    
    xhandle                                 GetReadyToDumpData      ( s32 nVertices, s32 nIndices, u32 ExtraMode );
    
protected:
    
    typedef eng_vertex_desc vdesc;
    
    //
    // Global state variables
    //
    eng_fshader                                 m_FragmentShader;
    eng_vshader                                 m_VertexShader;
    eng_shader_program                          m_ShaderProgram;
    xsafe_array<vdesc::attribute, 3>            m_VertAttr;                             // Vertex Attributes for the description
    xsafe_array<vdesc::attribute_link, 3>       m_VShaderAttrLinks;
    eng_vertex_desc                             m_VertexDesc;
    
    raster_states                               m_RasterStates;
    
    //
    // Lists
    //
    xsafe_array<draw_primitive_list, 8>         m_PrimitiveList;            // List of primitives
    
    //
    // Commands
    //
    xsafe_array<draw_cmd, CMD_BUFFER_COUNT>                     m_Cmd;                                  // List of commands
    xsafe_array<xmatrix4, CMD_BUFFER_COUNT>                     m_L2W;                                  // L2W matrix buffer
    xsafe_array<const eng_texture*, CMD_BUFFER_COUNT>           m_lTexture;                             // User's texture
    xsafe_array<const eng_shader_program*, CMD_BUFFER_COUNT>    m_lProgram;                             // User's Program
    
    s32                                         m_nCmd                      = 0;                        // Cmd count used in this list
    s32                                         m_nMatrices                 = 0;                        // Number of matrices used so far
    s32                                         m_nTextures                 = 0;                        // Number of texture used so far
    s32                                         m_nPrograms                 = 0;
    
    eng_texture                                 m_WhiteTexture;                                         // Draw always uses a texture...
    
    //
    // Active lists
    //
    s32                                         m_iActiveCmdList            = 0;                        // Index of the CmdList that we are trying to use
    u16                                         m_iActivePrimList           = 0;                        // Which primitive list we are using
    s32                                         m_nActiveVertices           = 0;                        // Number of Vertices used
    s32                                         m_nActiveIndices            = 0;                        // Number of Indices used
    u16*                                        m_pActiveIndexBufferMap     = NULL;                     // Access to the active index buffer
    draw_vertex*                                m_pActiveVertexBufferMap    = NULL;                     // Access to the active vertex buffer
    
    //
    // Mode registers
    //
    xbool                                       m_bDrawBegging              = FALSE;                    // Whether we are inside of a begging or not
    u32                                         m_ActiveMode                = ENG_DRAW_MODE_DEFAULT;    // Current active mode
    xhandle                                     m_ActiveCmdHandle           = xhandle(xhandle::HNULL);
    
    //
    // Stats
    //
    s32                                         m_iStat                     = 0;                        // index to the next statistic entry
    xsafe_array<draw_stats,8>                   m_Stats;                                                // Stats for draw
};

//===============================================================================
// DRAW STATS FUNCTIONS
//===============================================================================

//-------------------------------------------------------------------------------

raster_states::raster_states( void )
{
    //
    // Build all the nice display lists which the standard gl supports
    //
#ifdef ENG_OPEN_GL    
    m_RasterState.SetMemory(0);
    m_DepthState.SetMemory(0);
    m_BlendState.SetMemory(0);
#endif
    
    //
    // Set the masks as well
    //
    m_RasterStateMask.SetMemory(0);
    m_DepthStateMask.SetMemory(0);
    m_BlendStateMask.SetMemory(0);
    
    //
    // Culling and wire frame
    //
    for( s32 i=0; i<m_RasterStateMask.getCount(); i++ )
    {
        m_RasterStateMask[i] = i<<ENG_DRAW_MODE_RASTER_SHIFT;
        
#ifdef ENG_OPEN_GL
        s32 dList = glGenLists(1);
        glNewList( dList, GL_COMPILE);
        
        WireFrameAndCull( i );
        
        glEndList();
        m_RasterState[i] = dList;
#endif
    }
    
    //
    // Depth
    //
    for( s32 i=0; i<m_DepthStateMask.getCount(); i++ )
    {
        m_DepthStateMask[i] = i<<ENG_DRAW_MODE_ZBUFFER_SHIFT;
        
#ifdef ENG_OPEN_GL
        s32 dList = glGenLists(1);
        glNewList( dList, GL_COMPILE);
        
        ZBuffer( i );
        
        glEndList();
        m_DepthState[i] = dList;
#endif
    }
    
    //
    // Blend Modes
    //
    for( s32 i=0; i<m_BlendStateMask.getCount(); i++ )
    {
        m_BlendStateMask[i] = i<<ENG_DRAW_MODE_BLEND_SHIFT;
        
#ifdef ENG_OPEN_GL
        s32 dList = glGenLists(1);
        glNewList( dList, GL_COMPILE);
        
        BlendModes( i );
        
        glEndList();
        m_BlendState[i] = dList;
#endif
    }
    
    eng_CheckForError();
}

//-------------------------------------------------------------------------------

raster_states::~raster_states( void )
{
    
}

//-------------------------------------------------------------------------------

void raster_states::WireFrameAndCull( s32 i )
{
#ifdef ENG_OPEN_GL
    if(  m_RasterState[i] )
    {
        glCallList( m_RasterState[i] );
        return;
    }
#endif

    u32 iRaster = m_RasterStateMask[i];

    // This behavior does not exist in OPENGLES
#ifdef ENG_OPEN_GL
    if( iRaster & ENG_DRAW_MODE_RASTER_WIRE_FRAME ) glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    else                                            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
#endif
    
    if( iRaster & ENG_DRAW_MODE_RASTER_CULL_NONE )
    {
        glDisable( GL_CULL_FACE );
    }
    else
    {
        glEnable( GL_CULL_FACE );
        /*
        if( iRaster & ENG_DRAW_MODE_Y_TOP_LEFT )
        {
            // If we flip the Y axis we need to flip which direction we are culling
            glCullFace(GL_FRONT);
        }
        else
         */
        {
            glCullFace(GL_BACK);
        }
    }
}

//-------------------------------------------------------------------------------

void raster_states::ZBuffer( s32 i )
{
#ifdef ENG_OPEN_GL
    if(  m_DepthState[i] )
    {
        glCallList( m_DepthState[i] );
        return;
    }
#endif
    
    u32 iZBuffer = m_DepthStateMask[i];
    
    if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_ON )
    {
        glDepthMask( TRUE );            // Write to the depth buffer
        glEnable( GL_DEPTH_TEST );      // Enable Z Compare
        glDepthFunc( GL_LEQUAL );       // <=
    }
    else if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_OFF )
    {
        glDepthMask( FALSE );
        glDisable( GL_DEPTH_TEST );
        glDepthFunc( GL_ALWAYS );
    }
    else if( iZBuffer == ENG_DRAW_MODE_ZBUFFER_READ_ONLY )
    {
        glDepthMask( FALSE );
        glEnable( GL_DEPTH_TEST );
        glDepthFunc( GL_LEQUAL );
    }
    else
    {
        ASSERT( 0 );
    }
}

//-------------------------------------------------------------------------------

void raster_states::BlendModes( s32 i )
{
#ifdef ENG_OPEN_GL
    if(  m_BlendState[i] )
    {
        glCallList( m_BlendState[i] );
        return;
    }
#endif
    
    u32 iBlend = m_BlendStateMask[i];
    
    if( iBlend == ENG_DRAW_MODE_BLEND_OFF )
    {
        glDisable( GL_BLEND );
    }
    else if( iBlend == ENG_DRAW_MODE_BLEND_ALPHA )
    {
        glEnable( GL_BLEND );

#ifdef TARGET_PC
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#else
        
        glBlendEquationSeparate( GL_FUNC_ADD,
                                GL_FUNC_ADD);
        glBlendFuncSeparate(GL_SRC_ALPHA,
                            GL_ONE_MINUS_SRC_ALPHA,
                            GL_ONE,
                            GL_ZERO);
#endif
    }
    else if( iBlend == ENG_DRAW_MODE_BLEND_ADD )
    {
        glEnable( GL_BLEND );
        
#ifdef TARGET_PC
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ONE, GL_ONE);
#else
        glBlendEquationSeparate( GL_FUNC_ADD,
                                GL_FUNC_ADD);
        glBlendFuncSeparate(GL_ONE,
                            GL_ONE,
                            GL_ONE,
                            GL_ZERO);
#endif
    }
    else if( iBlend == ENG_DRAW_MODE_BLEND_SUB )
    {
        glEnable( GL_BLEND );
        
#ifdef TARGET_PC
        glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
        glBlendFunc(GL_ONE, GL_ONE);
#else
        glBlendEquationSeparate( GL_FUNC_SUBTRACT,
                                GL_FUNC_ADD);
        glBlendFuncSeparate(GL_ONE,
                            GL_ONE,
                            GL_ONE,
                            GL_ZERO);
#endif
    }
    else
    {
        ASSERT( 0 );
    }
}


//===============================================================================
// DRAW LOCAL FUNCTIONS
//===============================================================================


//-------------------------------------------------------------------------------

void draw_local::Init( void )
{
    s32     i;
    
    //
    // Create the vertex desctiption
    //
    m_VertAttr[0].Setup( eng_vertex_desc::ATTR_USAGE_POSITION,          eng_vertex_desc::ATTR_SRC_F32x3, X_MEMBER_OFFSET( draw_vertex, m_X ) );
    m_VertAttr[1].Setup( eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV,   eng_vertex_desc::ATTR_SRC_F32x2, X_MEMBER_OFFSET( draw_vertex, m_U ) );
    m_VertAttr[2].Setup( eng_vertex_desc::ATTR_USAGE_00_RGBA,           eng_vertex_desc::ATTR_SRC_U8x4_F,X_MEMBER_OFFSET( draw_vertex, m_Color ) );
    m_VertexDesc.SubmitAttributes( sizeof(draw_vertex), 3, m_VertAttr, FALSE );
    
    //
    // Create all the primitive buffers
    //
    for( i=0; i<m_PrimitiveList.getCount(); i++ )
    {
        draw_primitive_list& PrimList = m_PrimitiveList[i];
        
        // Create index buffer
        PrimList.m_IndexBuffer.CreateDynamicBuffer( eng_ibuffer::DATA_DESC_U16, INDEX_BUFFER_COUNT );
        
        // Create the vertex buffer
        PrimList.m_VertexBuffer.CreateDynamicBuffer( m_VertexDesc.getVertSize(), VERTEX_BUFFER_COUNT );
    }
    
    //
    // Create the shader
    //
    m_VShaderAttrLinks[0].Setup( "aPosition",   eng_vertex_desc::ATTR_USAGE_POSITION );
    m_VShaderAttrLinks[1].Setup( "aTexCoord",   eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV );
    m_VShaderAttrLinks[2].Setup( "aColor",      eng_vertex_desc::ATTR_USAGE_00_RGBA );
    
    m_VertexShader.LoadFromMemory( s_DrawVertexShader, sizeof( s_DrawVertexShader ), m_VShaderAttrLinks, 3, FALSE );

    m_FragmentShader.LoadFromMemory( s_DrawFragmentShader, sizeof(s_DrawFragmentShader) );
    m_ShaderProgram.LinkShaders( m_VertexShader, m_FragmentShader );
    m_ShaderProgram.LinkRegisterToUniformVariable( 0, "L2C" );
    _eng_SetupShaderProgram2D( m_ShaderProgram.getHandle() );
    
    //
    // Create the Draw white texture
    //
    {
        s32         Size=16;
        xbitmap     Bitmap;
        
        Bitmap.CreateBitmap( Size, Size );
        x_memset( (void*)Bitmap.getMip(0), 0xff, sizeof(u32)*Size*Size );
        
        m_WhiteTexture.CreateTexture( Bitmap, FALSE );
        m_WhiteTexture.setWrappingMode( eng_texture::WRAP_MODE_TILE, eng_texture::WRAP_MODE_TILE );
    }
    
}

//-------------------------------------------------------------------------------

void draw_local::Kill( void )
{
    
}

//-------------------------------------------------------------------------------

void draw_local::Begin( u32 Mode )
{
    ASSERT( m_bDrawBegging == FALSE );
    m_ActiveMode = Mode;
    m_bDrawBegging = TRUE;
    ClearTexture();
    ClearProgram();
}

//-------------------------------------------------------------------------------

void draw_local::End( void )
{
    ASSERT( m_bDrawBegging );
    m_bDrawBegging = FALSE;

    //
    // If the user wants us to flash so we must...
    //
    if( x_FlagIsOn( m_Cmd[ m_nCmd -1 ].m_Mode, ENG_DRAW_MODE_MISC_FLUSH))
        PageFlip();
    
}

//-------------------------------------------------------------------------------

void draw_local::PageFlip( void )
{
    s32 iCachePrimtList = -5;
    s32 TextureType     = -5;
    s32 ProgramType     = -5;
    s32 CacheiTexture   = -5;
    s32 CacheiProgram   = -5;
    s32 RasterState     = -5;
    s32 DepthState      = -5;
    s32 BlendState      = -5;
    s32 DrawMode        = -5;
    s32 iMatrix         = -5;
    
    const eng_view& View = eng_GetCurrentContext().GetActiveView();

    xmatrix4 L2C_2DParametric;
    xmatrix4 L2C_2D;
    xmatrix4 W2C_3D;
    xmatrix4 L2C_2D_Y_Flip;
    xmatrix4 W2C;
    
    ASSERT( m_bDrawBegging == FALSE );
    
    //
    // Build 2D Matrices
    //
    {
        s32 XRes = View.getViewport().GetWidth();
        s32 YRes = View.getViewport().GetHeight();
        
        L2C_2DParametric.Identity();
        L2C_2D.Identity();
        L2C_2D_Y_Flip.Identity();
        
        L2C_2DParametric.setTranslation( xvector3( 0, 0, 0 ) );
        L2C_2DParametric.setScale      ( xvector3( 1, 1, 1 ) );
        
        L2C_2D.setTranslation( xvector3( -1, -1, 0 ) );
        L2C_2D.setScale      ( xvector3( 2.0f/XRes, 2.0f/YRes, 1 ) );

        L2C_2D_Y_Flip.setTranslation( xvector3( -1, 1, 0 ) );
        L2C_2D_Y_Flip.setScale      ( xvector3( 2.0f/XRes, -2.0f/YRes, 1 ) );
    }

    //
    // Get the world to clip matrix from the active view
    //
    W2C_3D = View.getW2C();
    
    //
    // Do we have anything to do here?
    //
    if( m_iActiveCmdList == 0 && m_nCmd == 0 )
        return;
    ASSERT(m_pActiveVertexBufferMap);
    
    //
    // Any maped buffers must be unmap
    //
    {
        draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];
        
        ASSERT( PrimList.m_bLocked == TRUE );
        ASSERT(m_pActiveVertexBufferMap);
        PrimList.m_VertexBuffer.UnlockData();
        PrimList.m_IndexBuffer.UnlockData();
        m_pActiveVertexBufferMap = NULL;
        m_pActiveIndexBufferMap  = NULL;
        PrimList.m_bLocked = FALSE;
    }
    
    //
    // Activate the shader here
    //
    // m_ShaderProgram.Activate();
    
    //
    // Set global states
    //
    draw_primitive_list* pPrimList=NULL;
    
    // Note that due to OpenGL ES draw does not work in multiple threads
    // Because vertex buffers are not share across contexts
    if ( _eng_DrawInCorrectContext() ) for( s32 j=0; j<m_nCmd; j++ )
    {
        draw_cmd& Cmd = m_Cmd[j];
        
        //
        // Handle Texture
        //
        if( ProgramType != (Cmd.m_Mode&ENG_DRAW_MODE_CUSTOM_PROGRAM) )
        {
            // Update the cache
            ProgramType = (Cmd.m_Mode&ENG_DRAW_MODE_CUSTOM_PROGRAM);
            
            if( ProgramType )
            {
                if( CacheiProgram != Cmd.m_iProgram )
                {
                    CacheiProgram = Cmd.m_iProgram;
                    
                    m_lProgram[CacheiProgram]->Activate();
                    eng_CheckForError();
                }
            }
            else
            {
                 m_ShaderProgram.Activate();
            }
        }
        else if( (Cmd.m_Mode&ENG_DRAW_MODE_CUSTOM_PROGRAM) && CacheiProgram != Cmd.m_iProgram )
        {
            CacheiProgram = Cmd.m_iProgram;
            
            m_lProgram[CacheiProgram]->Activate();
        }

        
        //
        // Set the rater stage
        //
        if( RasterState != ((Cmd.m_Mode>>ENG_DRAW_MODE_RASTER_SHIFT)&ENG_DRAW_MODE_RASTER_MASK) )
        {
            RasterState = (Cmd.m_Mode>>ENG_DRAW_MODE_RASTER_SHIFT)&ENG_DRAW_MODE_RASTER_MASK;
            
            m_RasterStates.WireFrameAndCull( RasterState );
        }
        
        //
        // Set the rater Depth stage
        //
        if( DepthState != ((Cmd.m_Mode>>ENG_DRAW_MODE_ZBUFFER_SHIFT)&ENG_DRAW_MODE_ZBUFFER_MASK) )
        {
            DepthState = (Cmd.m_Mode>>ENG_DRAW_MODE_ZBUFFER_SHIFT)&ENG_DRAW_MODE_ZBUFFER_MASK;
            
            m_RasterStates.ZBuffer( DepthState );
        }
        
        //
        // Set the Blend stage
        //
        if( BlendState != ((Cmd.m_Mode>>ENG_DRAW_MODE_BLEND_SHIFT)&ENG_DRAW_MODE_BLEND_MASK) )
        {
            BlendState = (Cmd.m_Mode>>ENG_DRAW_MODE_BLEND_SHIFT)&ENG_DRAW_MODE_BLEND_MASK;
            
            m_RasterStates.BlendModes( BlendState );
        }
        
        //
        // Set the right matrix
        //
        if( DrawMode != ((Cmd.m_Mode>>ENG_DRAW_MODE_SHIFT)&ENG_DRAW_MODE_MASK) )
        {
            DrawMode = ((Cmd.m_Mode>>ENG_DRAW_MODE_SHIFT)&ENG_DRAW_MODE_MASK);
            
            switch(DrawMode)
            {                    
                case ENG_DRAW_MODE_3D:
                {
                    W2C = W2C_3D;
                    break;
                }
                case ENG_DRAW_MODE_2D_ABS:
                    W2C = L2C_2D;
                    break;
                    
                case (ENG_DRAW_MODE_2D_ABS | ENG_DRAW_MODE_Y_TOP_LEFT):
                    W2C = L2C_2D_Y_Flip;
                    break;
                    
                case ENG_DRAW_MODE_2D_PARAMETRIC:
                    W2C = L2C_2DParametric;
                    break;
                    
                default:
                {
                    ASSERT( 0 );
                }
            };
            
            //
            // Set the active matrix and handle if the user did not put any matrix at all
            //
            iMatrix = Cmd.m_iMatrix;
            if( Cmd.m_iMatrix != 0xffff )
            {
                const xmatrix4 L2C = W2C * m_L2W[ Cmd.m_iMatrix ];
                m_ShaderProgram.setUniformVariable( 0, L2C );
            }
            else
            {
                m_ShaderProgram.setUniformVariable( 0, W2C );
            }
            
        }
        else if ( iMatrix != Cmd.m_iMatrix )
        {
            iMatrix = Cmd.m_iMatrix;
            xmatrix4 L2C = W2C * m_L2W[ Cmd.m_iMatrix ];
            m_ShaderProgram.setUniformVariable( 0, L2C );
        }
        
        //
        // Handle Texture
        //
        if( TextureType != (Cmd.m_Mode&ENG_DRAW_MODE_TEXTURE_ON) )
        {
            // Update the cache
            TextureType = (Cmd.m_Mode&ENG_DRAW_MODE_TEXTURE_ON);
            
            if( TextureType )
            {
                if( CacheiTexture != Cmd.m_iTexture )
                {
                    CacheiTexture = Cmd.m_iTexture;
                    
                    m_lTexture[CacheiTexture]->Activate();
                    eng_CheckForError();
                }
            }
            else
            {
                m_WhiteTexture.Activate();
            }
        }
        else if( (Cmd.m_Mode&ENG_DRAW_MODE_TEXTURE_ON) && CacheiTexture != Cmd.m_iTexture )
        {
            CacheiTexture = Cmd.m_iTexture;
            
            m_lTexture[CacheiTexture]->Activate();
        }

        //
        // Handle setting the right vertex/index buffer
        //
        if( iCachePrimtList != Cmd.m_iPrimitiveList )
        {
            // Update the cache entry for vertex/index buffers
            iCachePrimtList = Cmd.m_iPrimitiveList;
            
            // Active new vertex index buffers
            pPrimList = &m_PrimitiveList[ iCachePrimtList ];
        }
        
        //
        // Render the data
        //
        eng_CheckForError();
        pPrimList->m_VertexBuffer.Activate( m_VertexDesc, m_VertexShader, Cmd.m_VertexOffset );
        
        pPrimList->m_IndexBuffer.Activate();
        if( x_FlagIsOn( Cmd.m_Mode, ENG_DRAW_PRIMITIVE_TYPE_LINE ) )
        {
            pPrimList->m_IndexBuffer.RenderLines( Cmd.m_nIndices, Cmd.m_IndexOffset );
        }
        else
        {
            pPrimList->m_IndexBuffer.RenderTriangles( Cmd.m_nIndices, Cmd.m_IndexOffset );
        }
        
        eng_CheckForError();
    }
    
    //
    // Deactivate the shader here
    //
    m_ShaderProgram.Deactivate();
    
    if( pPrimList )
    {
        pPrimList->m_VertexBuffer.Deactivate( m_VertexDesc );
        pPrimList->m_IndexBuffer.Deactivate();
    }
    
    //
    // Force the system to flush the active variables
    //
    m_nActiveVertices        = VERTEX_BUFFER_COUNT; // Forces the system to use a different vertex buffer
    m_pActiveVertexBufferMap = NULL;
    m_pActiveIndexBufferMap  = NULL;
    m_iActiveCmdList         = 0;
    
    m_nCmd      = 0;
    m_nMatrices = 0;
    m_nTextures = 0;
    m_nPrograms = 0;
}

//-------------------------------------------------------------------------------

void draw_local::SetL2W( const xmatrix4& M )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );
    
    // Set the L2W
    m_L2W[ m_nMatrices ] = M;
    
    // Set the system ready for the next matrix
    m_nMatrices++;
}

//-------------------------------------------------------------------------------

void draw_local::SetTexture( const eng_texture& Texture )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );

    m_lTexture[ m_nTextures++ ] = &Texture;
}

//-------------------------------------------------------------------------------

void draw_local::SetProgram( const eng_shader_program& program )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );
    
    m_lProgram[ m_nPrograms++ ] = &program;
}

//-------------------------------------------------------------------------------

void draw_local::ClearTexture( void )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );
    if( m_nTextures > 0 )
    {
        if( m_lTexture[ m_nTextures -1 ] != &m_WhiteTexture )
            SetTexture( m_WhiteTexture );
    }
}

//-------------------------------------------------------------------------------

void draw_local::ClearProgram( void )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );
    if( m_nPrograms > 0 )
    {
        if( m_lProgram[ m_nPrograms -1 ] != &m_ShaderProgram )
            SetProgram( m_ShaderProgram );
    }
}

//-------------------------------------------------------------------------------

void draw_local::ClearL2W( void )
{
    ASSERT( m_bDrawBegging );
    
    // Must call the this function before getting the buffers
    ASSERT( m_ActiveCmdHandle.IsNull() );
    
    // Clear the L2W
    m_L2W[ m_nMatrices ].Identity();
    
    // Set the system ready for the next matrix
    m_nMatrices++;
}


//-------------------------------------------------------------------------------
//
// If it can't dump all vertices in the same vertex buffer
//
xhandle draw_local::GetReadyToDumpData( s32 nVertices, s32 nIndices, u32 ExtraMode )
{
    u32 iFrame  = eng_GetCurrentContext().GetCurrentFrameNumber();
    
    ASSERT( nVertices < VERTEX_BUFFER_COUNT );
    ASSERT( nIndices  < INDEX_BUFFER_COUNT );
    
    eng_CheckForError();
    
    //
    // Make sure that all the limits are good
    // Do we need to change the active primitive list?
    //
    if( (VERTEX_BUFFER_COUNT - m_nActiveVertices) < nVertices ||
        (INDEX_BUFFER_COUNT  - m_nActiveIndices)  < nIndices )
    {
        // Unmap current vertex/index buffers
        if( m_pActiveVertexBufferMap )
        {
            draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];
            
            ASSERT( PrimList.m_bLocked );
            PrimList.m_IndexBuffer.UnlockData();
            PrimList.m_VertexBuffer.UnlockData();
            PrimList.m_bLocked = FALSE;
            
            m_pActiveVertexBufferMap = NULL;
            m_pActiveIndexBufferMap  = NULL;
        }
        
        // Set the next index
        m_iActivePrimList = (m_iActivePrimList + 1)%m_PrimitiveList.getCount();
        
        // Make sure that the buffer is not been use the previous frame
        ASSERT( m_PrimitiveList[m_iActivePrimList].m_iFrame != (iFrame-1) );
        
        // Also update the iframe
        m_PrimitiveList[m_iActivePrimList].m_iFrame = iFrame;
        
        // Okay now we can reset a few things
        m_nActiveVertices = 0;
        m_nActiveIndices  = 0;
    }
    
    //
    // Lock buffers
    //
    if( m_pActiveVertexBufferMap == NULL )
    {
        ASSERT( m_pActiveIndexBufferMap == NULL );
        
        draw_primitive_list& PrimList = m_PrimitiveList[ m_iActivePrimList ];
        
        ASSERT(m_pActiveVertexBufferMap==NULL);
        ASSERT( PrimList.m_bLocked == FALSE );
        PrimList.m_bLocked       = TRUE;
        m_pActiveVertexBufferMap = (draw_vertex*)PrimList.m_VertexBuffer.LockData();
        m_pActiveIndexBufferMap  = (u16*)PrimList.m_IndexBuffer.LockData();
        
        if( !m_pActiveVertexBufferMap || !m_pActiveIndexBufferMap )
        {
            ASSERT(0);
            PrimList.m_VertexBuffer.UnlockData();
            PrimList.m_IndexBuffer.UnlockData();
            m_pActiveVertexBufferMap = NULL;
            m_pActiveIndexBufferMap  = NULL;
            PrimList.m_bLocked = FALSE;
        }
    }
    else
    {
        ASSERT( m_pActiveIndexBufferMap );
    }
    
    
    //
    // Initialize the command
    //
    xhandle        Handle;
    Handle.Set( m_nCmd );
    
    draw_cmd&      Cmd        = m_Cmd[ m_nCmd++ ];
    
    Cmd.m_Mode              = m_ActiveMode | ExtraMode;
    Cmd.m_iPrimitiveList    = m_iActivePrimList;
    Cmd.m_VertexOffset      = m_nActiveVertices;
    Cmd.m_IndexOffset       = m_nActiveIndices;
    Cmd.m_nVertices         = nVertices;
    Cmd.m_nIndices          = nIndices;
    Cmd.m_iMatrix           = m_nMatrices-1;
    Cmd.m_iTexture          = m_nTextures-1;
    Cmd.m_iProgram          = m_nPrograms-1;
    
    return Handle;
}

//-------------------------------------------------------------------------------

void draw_local::Flush( void )
{
    PageFlip();
}

//-------------------------------------------------------------------------------

void draw_local::GetBuffers( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices )
{
    ASSERT( m_ActiveCmdHandle.IsNull() );
    ASSERT( pVertex );
    ASSERT( pIndex );
    ASSERT( nVertices >= 2 );
    ASSERT( nIndices  >= 2 );
    
    m_ActiveCmdHandle = GetReadyToDumpData( nVertices, nIndices, 0 );
    
    // Set pointers
    *pVertex = &m_pActiveVertexBufferMap[m_nActiveVertices];
    *pIndex  = &m_pActiveIndexBufferMap[m_nActiveIndices];
    
    // Advance the indices
    m_nActiveVertices += nVertices;
    m_nActiveIndices  += nIndices;
}

//-------------------------------------------------------------------------------

void draw_local::DrawBufferTriangles( void )
{
    ASSERT( m_ActiveCmdHandle.isValid() );
    draw_cmd& Cmd = m_Cmd[ m_ActiveCmdHandle.Get() ];
    Cmd.m_Mode |= ENG_DRAW_PRIMITIVE_TYPE_TRIANGLE;
    m_ActiveCmdHandle.setNull();

    // Did you forgot to set the program before calling this function?
    if( m_nPrograms > 0 )
    {
        ASSERT( !(x_FlagIsOn( Cmd.m_Mode, ENG_DRAW_MODE_CUSTOM_PROGRAM ) ^ (m_lProgram[ m_nPrograms -1 ] != &m_ShaderProgram) ) );
    }
    else
    {
        ASSERT( !(x_FlagIsOn( Cmd.m_Mode, ENG_DRAW_MODE_CUSTOM_PROGRAM ) ^ (Cmd.m_iProgram != 0xffff)) );
    }

    
    // Did you forgot to set the texture before calling this function?
    if( m_nTextures > 0 )
    {
        // ASSERT( !(x_FlagIsOn( Cmd.m_Mode, ENG_DRAW_MODE_TEXTURE_ON ) ^ (m_lTexture[ m_nTextures -1 ] != &m_WhiteTexture) ) );
    }
    else
    {
        ASSERT( !(x_FlagIsOn( Cmd.m_Mode, ENG_DRAW_MODE_TEXTURE_ON ) ^ (Cmd.m_iTexture != 0xffff)) );
    }
    
}

//-------------------------------------------------------------------------------

void draw_local::DrawBufferLines( void )
{
    ASSERT( m_ActiveCmdHandle.isValid() );
    draw_cmd& Cmd = m_Cmd[ m_ActiveCmdHandle.Get() ];
    Cmd.m_Mode |= ENG_DRAW_PRIMITIVE_TYPE_LINE;
    m_ActiveCmdHandle.setNull();
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// eng_draw Implementation
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------

eng_draw::eng_draw()
{
}

//-------------------------------------------------------------------------------

eng_draw::~eng_draw()
{
}

//-------------------------------------------------------------------------------

void eng_draw::Begin( u32 Mode )
{
    draw_local& This = *(draw_local*)this;
    This.Begin( Mode );
}

//-------------------------------------------------------------------------------

void eng_draw::End( void )
{
    draw_local& This = *(draw_local*)this;
    This.End();
}

//-------------------------------------------------------------------------------

void eng_draw::PageFlip( void )
{
    draw_local& This = *(draw_local*)this;
    This.PageFlip();
}

//-------------------------------------------------------------------------------

void eng_draw::GetBuffers( draw_vertex** pVertex, s32 nVertices, u16** pIndex, s32 nIndices )
{
    draw_local& This = *(draw_local*)this;
    This.GetBuffers( pVertex, nVertices, pIndex, nIndices );
}

//-------------------------------------------------------------------------------

void eng_draw::DrawBufferTriangles( void )
{
    draw_local& This = *(draw_local*)this;
    This.DrawBufferTriangles();
}

//-------------------------------------------------------------------------------

void eng_draw::DrawBufferLines( void )
{
    draw_local& This = *(draw_local*)this;
    This.DrawBufferLines();
}

//-------------------------------------------------------------------------------

void eng_draw::SetL2W( const xmatrix4& L2W )
{
    draw_local& This = *(draw_local*)this;
    This.SetL2W( L2W );
}

//-------------------------------------------------------------------------------

void eng_draw::ClearL2W( void )
{
    draw_local& This = *(draw_local*)this;
    This.ClearL2W();
}

//-------------------------------------------------------------------------------

void eng_draw::SetTexture( const eng_texture& Texture )
{
    draw_local& This = *(draw_local*)this;
    This.SetTexture( Texture );
}

//-------------------------------------------------------------------------------

void eng_draw::ClearTexture( void )
{
    draw_local& This = *(draw_local*)this;
    This.ClearTexture();
}

//-------------------------------------------------------------------------------

void eng_draw::SetProgram( const eng_shader_program& Program )
{
    draw_local& This = *(draw_local*)this;
    This.SetProgram( Program );
}

//-------------------------------------------------------------------------------

void eng_draw::ClearProgram( void )
{
    draw_local& This = *(draw_local*)this;
    This.ClearProgram();
}

//-------------------------------------------------------------------------------

const eng_vertex_desc& eng_draw::GetVertDesc( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    return pDrawLocal->GetVertexDesc();
}

//-------------------------------------------------------------------------------

const eng_vshader& eng_draw::GetVShader( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    return pDrawLocal->GetVertexShader();
}

//-------------------------------------------------------------------------------

const eng_fshader& eng_draw::GetFShader( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    return pDrawLocal->GetFragmentShader();
}

//-------------------------------------------------------------------------------

const eng_texture& eng_draw::GetWhiteTexture ( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    return pDrawLocal->GetWhiteTexture();
}

//-------------------------------------------------------------------------------

const eng_shader_program& eng_draw::GetShaderProgram( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    return pDrawLocal->GetShaderProgram();
}

//-------------------------------------------------------------------------------

void eng_draw::Flush( void )
{
    draw_local* pDrawLocal = (draw_local*)this;
    pDrawLocal->Flush();
}

//-------------------------------------------------------------------------------

eng_draw& _eng_CreateDraw( void )
{
    //
    // Allocate the local memory
    //
    draw_local* pContext = x_new( draw_local, 1, XMEM_FLAG_ALIGN_16B );
    ASSERT( pContext );

    // Initialize draw
    pContext->Init();

    return *pContext;
}

//-------------------------------------------------------------------------------

void _eng_KillDraw( eng_draw& Draw )
{
    draw_local* pDrawLocal = (draw_local*)&Draw;
    //
    // Release the memory
    //
    pDrawLocal->Kill();
    x_delete( pDrawLocal );
}
