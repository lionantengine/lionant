//
//  eng_SrachMemory.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef ENG_SCRACH_MEMORY_INLINE_H
#define ENG_SCRACH_MEMORY_INLINE_H

//---------------------------------------------------------------------------------------

template<class T> inline
T* eng_scrach_buffer_cache<T>::NewAlloc( s32 Count )
{
    auto& C     = eng_GetCurrentContext();
    m_pPointer  = C.getScrachMem().BufferAlloc<T>(Count);
    m_Frame     = C.GetCurrentFrameNumber();
    return m_pPointer;
}

//---------------------------------------------------------------------------------------

template<class T> inline
xbool eng_scrach_buffer_cache<T>::isCachedThisFrame( void ) const
{
    const s32 Frame = eng_GetCurrentContext().GetCurrentFrameNumber();
    return m_pPointer && (m_Frame == Frame);
}

//---------------------------------------------------------------------------------------

template<class T> inline
xbool eng_scrach_buffer_cache<T>::isValid( void ) const
{
    const s32 Frame = eng_GetCurrentContext().GetCurrentFrameNumber();
    if( m_pPointer && ((m_Frame == Frame || m_Frame == (Frame-1))) ) return TRUE;
    m_pPointer=NULL;
    return FALSE;
}

//---------------------------------------------------------------------------------------

template<class T> inline
T* eng_scrach_buffer_cache<T>::getCache( void )
{
    ASSERT(isValid());
    return m_pPointer;
}

//---------------------------------------------------------------------------------------

template<class T> inline
const T* eng_scrach_buffer_cache<T>::getCache( void ) const
{
    ASSERT(isValid());
    return m_pPointer;
}

//---------------------------------------------------------------------------------------

template<class T> inline
void eng_scrach_buffer_cache<T>::setup( T* Pointer, u32 Frame )
{
    ASSERT( Pointer );
    m_pPointer = Pointer;
    m_Frame = Frame;
    ASSERT(isValid());
}

//---------------------------------------------------------------------------------------

template<class T> inline
void eng_scrach_buffer_cache<T>::Reset( void )
{
    m_pPointer = NULL;
}

//===============================================================================
// END
//===============================================================================
#endif

