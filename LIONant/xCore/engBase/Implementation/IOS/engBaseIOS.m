//
//  engBaseIOS.m
//  engBaseIOS
//
//  Created by Tomas Arce on 7/21/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#import "engBaseIOS.h"

#import <QuartzCore/CAEAGLLayer.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES1/glext.h>

#include <pthread.h>

//@class UIWindow;

UIWindow*               m_pUIWindow;
UIScreen*               m_pUIScreen;
eng_uiviewcontroller*   m_pEngViewControler;
eng_uiopenglview*       m_pEngView;
float                   m_Scale;
CGRect                  m_Bounds;

static int               s_nThreadContexts=0;
static EAGLContext*      s_ThreadContext[8];
static pthread_t         s_pThreadID[8];

int     s_argc;
char**  s_argv;
void (*s_UserAppMain)( int argc, char* argv[] );

@implementation eng_uiviewcontroller
@end


//---------------------------------------------------------------------------------------
static 
EAGLContext* GetCurrentContext( void )
{
    // Get the thread id
    pthread_t ThreadID = pthread_self();
    
    //
    // Find the opengl context for this thread
    //
    int i;
    for( i=0; i<s_nThreadContexts; i++ )
    {
        int ret = pthread_equal( ThreadID, s_pThreadID[i] );
        if( ret != 0 ) // 0 means not equal when using pthread_equal()
        {
            return s_ThreadContext[i];
        }
    }
    
    //
    // Create a new context for the new thread
    // Note that the first entry should be the main thread
    //
    s_pThreadID[s_nThreadContexts]     = ThreadID;
    s_ThreadContext[s_nThreadContexts] = [[EAGLContext alloc] initWithAPI:[ s_ThreadContext[0] API]
                                                               sharegroup:[ s_ThreadContext[0] sharegroup]];
    return s_ThreadContext[s_nThreadContexts++];
}

//---------------------------------------------------------------------------------------

void _eng_PageFlip( void )
{
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, m_pEngView->m_viewRenderbuffer);
    [ GetCurrentContext() presentRenderbuffer:GL_RENDERBUFFER_OES];
}

/*
//---------------------------------------------------------------------------------------

void _eng_RenderTexture( void )
{
    [ GetCurrentContext() presentRenderbuffer:GL_RENDERBUFFER];
}

//---------------------------------------------------------------------------------------

void _eng_RenderBuffer( void )
{
    [ GetCurrentContext() renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)m_pEngView.layer ];
}
*/

//---------------------------------------------------------------------------------------

void _eng_PumpEvents( void )
{
    // Let the run loop run for a short amount of time: long enough for
    // touch events to get processed (which is important to get certain
    // elements of Game Center's GKLeaderboardViewController to respond
    // to touch input), but not long enough to introduce a significant
    // delay in the rest of the app.
    const CFTimeInterval seconds = 0.000002;
    
    // Pump most event types.
    SInt32 result;
    do
    {
        result = CFRunLoopRunInMode(kCFRunLoopDefaultMode, seconds, TRUE);
        
    } while (result == kCFRunLoopRunHandledSource);
    
    // Make sure UIScrollView objects scroll properly.
    do
    {
        result = CFRunLoopRunInMode((CFStringRef)UITrackingRunLoopMode, seconds, TRUE);
        
    } while(result == kCFRunLoopRunHandledSource);
}

//---------------------------------------------------------------------------------------

bool eng_IsDisplayLandscape( void )
{
    if ( m_pUIScreen == [UIScreen mainScreen])
    {
        return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    }
    else
    {
        CGSize size = [m_pUIScreen bounds].size;
        return (size.width > size.height);
    }
}

//---------------------------------------------------------------------------------------

CGRect _eng_GetNativeResolution( void )
{
    CGRect  bounds;
    CGRect  screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat screenScale  = [[UIScreen mainScreen] scale];
    CGSize  screenSize   = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
    
    bounds.origin.x = bounds.origin.y = 0;
    bounds.size     = screenSize;

    return bounds;
}


//---------------------------------------------------------------------------------------

CGRect _eng_GetWindowBounds( void )
{
    CGRect  bounds;
    
    // Seems this function wont return the device native resolution rather it uses something in between
    bounds = _eng_GetNativeResolution();
    
    return bounds;

    /*
    if( 1 )
    {
        // This is for the hold screen
        bounds = [m_pUIScreen bounds];
    }
    else
    {
        // This is hold screen minus the top bar of the iphone
        bounds = [ m_pUIScreen applicationFrame ];
    }
    
    return bounds;
     */
}

//---------------------------------------------------------------------------------------
// TODO: Need to know how the m_pUIScreen is created
static
void CreateWindow( void )
{
    //
    // Create the UI window
    //
    
    // ignore the size user requested, and make a fullscreen window
    // TODO: May need to fix this in the future
    
    CGRect bounds;
    bounds = _eng_GetWindowBounds();
    
    m_pUIWindow = [UIWindow alloc];
    m_pUIWindow = [m_pUIWindow initWithFrame:bounds ];
    
    // Make the window visible
    [m_pUIWindow makeKeyAndVisible];
    
    // TODO: Need to find how to get the scale
    float scale=1;
    int width  = (int)(bounds.size.width  * scale );
    int height = (int)(bounds.size.height * scale );
    
    if( eng_IsDisplayLandscape() == (width > height) )
    {
        //... Something here????
    }
    
    //
    // Create the controler
    //
    m_pEngViewControler = [eng_uiviewcontroller alloc];
    [m_pEngViewControler setTitle:@"LIONant App"];
    
    // Make sure to tell GL that there is no context
    [EAGLContext setCurrentContext: nil];
    
    //
    // Hide the menu bar of the ios device
    //
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    
    // When dealing with UIKit all coordinates are specified in terms of
    // what Apple refers to as points. On earlier devices without the
    // so called "Retina" display, there is a one to one mapping between
    // points and pixels. In other cases [UIScreen scale] indicates the
    // relationship between points and pixels. Since the engine has no notion
    // of points, we must compensate in all cases where dealing with such
    // units.
    if ([UIScreen instancesRespondToSelector:@selector(scale)])
    {
        // iOS >= 4.0
        m_Scale = [m_pUIScreen scale];
    }
    else
    {
        // iOS < 4.0
        m_Scale = 1.0f;
    }
}

//---------------------------------------------------------------------------------------

void eng_CreateDisplay( void )
{
    m_pUIScreen = [UIScreen mainScreen];
    
    CreateWindow();
    
    //
    // Create the opengl Window
    //
    CGRect frame;
    frame = _eng_GetWindowBounds();
    
    m_pEngView = [[eng_uiopenglview alloc] initWithFrame: frame
                                                   scale: 1
                                           retainBacking: 1
                                                   rBits: 1
                                                   gBits: 1
                                                   bBits: 1
                                                   aBits: 1
                                               depthBits: 1
                                             stencilBits: 1
                                            majorVersion: 1 ];
    
    [ m_pEngViewControler setView:m_pEngView];
    //[ m_pEngViewControler retain];
    
    [m_pUIWindow addSubview:m_pEngView ];
    
    
    // The view controller needs to be the root in order to control rotation on iOS 6.0
    if( m_pUIWindow.rootViewController == nil )
    {
        m_pUIWindow.rootViewController = m_pEngViewControler;
    }
    
    // Set the current opengl context
    [EAGLContext setCurrentContext:GetCurrentContext() ];
    
    //rdata->glViewport(renderer->viewport.x, renderer->viewport.y,
    //                  renderer->viewport.w, renderer->viewport.h);
    
    
}

//----------------------------------------------------------------------------------

void _eng_CreateWindow( int x0, int y0, int w, int h )
{
    m_Bounds.origin.x = x0;
    m_Bounds.origin.y = y0;
    m_Bounds.size.width  = w;
    m_Bounds.size.height = h;
    
    eng_CreateDisplay();
}

//----------------------------------------------------------------------------------

void _eng_GetWindowSize( void* pWnd, int* wid, int* hei )
{
    *wid = m_pEngView->m_backingWidth;
    *hei = m_pEngView->m_backingHeight;
}

//---------------------------------------------------------------------------------------

void _eng_HandleEvents( void )
{
    //
    // Get ready for more events
    //
    m_pEngView->m_WasPressIndexBuffer = 1 - m_pEngView->m_WasPressIndexBuffer;
    
    int i;
    for( i=0; i<MAX_SIMULTANEOUS_TOUCHES; i++ )
    {
        m_pEngView->m_KeyWasPress[m_pEngView->m_WasPressIndexBuffer][i] = 0;
    }

    //
    // Pump new events
    //
    _eng_PumpEvents();
}

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

@implementation eng_uiopenglview

//@synthesize m_context;

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

- (id)initWithFrame:(CGRect)frame
              scale:(CGFloat)scale
      retainBacking:(BOOL)retained
              rBits:(int)rBits
              gBits:(int)gBits
              bBits:(int)bBits
              aBits:(int)aBits
          depthBits:(int)depthBits
        stencilBits:(int)stencilBits
       majorVersion:(int)majorVersion
{
    //
    // Initialize the view
    //
    self = [super initWithFrame:frame];
    if( self == nil ) return nil;
    
    //
    // Determine the colorformat
    //
    NSString* colorFormat = nil;
    
    if( 1 )
    {
        // if user specifically requests rbg888 or some color format higher than 16bpp
        colorFormat = kEAGLColorFormatRGBA8;
    }
    else
    {
        // default case (faster)
        colorFormat = kEAGLColorFormatRGB565;
    }
    
    //
    // Determine the depth format
    //
    GLenum depthBufferFormat;
    
    if( 1 )
    {
        depthBufferFormat = GL_DEPTH24_STENCIL8_OES;
    }
    else
    {
        // iOS only has 24-bit depth buffers, even with GL_DEPTH_COMPONENT16_OES
        depthBufferFormat = GL_DEPTH_COMPONENT24_OES;
    }
    
    //
    // Get the layer.......... TODO: (not sure what this does....)
    //
    CAEAGLLayer* pEAglLayer = (CAEAGLLayer *)self.layer;
    
    pEAglLayer.opaque = YES;
    pEAglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithBool: retained], kEAGLDrawablePropertyRetainedBacking, colorFormat, kEAGLDrawablePropertyColorFormat, nil];
    
    
    //
    // Create the GL context
    //
    s_pThreadID[s_nThreadContexts]       = pthread_self();
    s_ThreadContext[s_nThreadContexts++] = [[EAGLContext alloc] initWithAPI: kEAGLRenderingAPIOpenGLES2];
    
    EAGLContext* pOpenGLContex = GetCurrentContext();
    
    // Set current opengl context
    [EAGLContext setCurrentContext:pOpenGLContex];
    
    //
    // Set the appropriate scale (for retina display support)
    //
    if ([self respondsToSelector:@selector(contentScaleFactor)])
        self.contentScaleFactor = scale;
    
    //
    // Initialize all the gl stuff
    //
    glGenFramebuffersOES    (1, &m_viewFramebuffer);
    glGenRenderbuffersOES   (1, &m_viewRenderbuffer);
    
    glBindFramebufferOES    (GL_FRAMEBUFFER_OES, m_viewFramebuffer);
    glBindRenderbufferOES   (GL_RENDERBUFFER_OES, m_viewRenderbuffer);
    
    [ pOpenGLContex renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(CAEAGLLayer*)self.layer];
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, m_viewRenderbuffer);
    
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &m_backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &m_backingHeight);
    
    glGenRenderbuffersOES   ( 1, &m_depthRenderbuffer );
    glBindRenderbufferOES   ( GL_RENDERBUFFER_OES, m_depthRenderbuffer );
    
    glRenderbufferStorageOES( GL_RENDERBUFFER_OES, depthBufferFormat, m_backingWidth, m_backingHeight );
    
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, m_depthRenderbuffer);
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_STENCIL_ATTACHMENT_OES, GL_RENDERBUFFER_OES, m_depthRenderbuffer);
    
    if( glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
        return NO;
    
    glFlush();
    
    //
    // Handle view flags
    //
    self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.autoresizesSubviews = YES;
    
    
    return self;
}

//----------------------------------------------------------------------------------

void _eng_SetCurrentContext( void )
{
    [EAGLContext setCurrentContext:GetCurrentContext() ];
}

//----------------------------------------------------------------------------------

void _eng_BindMainBuffer( void )
{
    // We must be sure that the context is set as well...
    
    if(m_pEngView) glBindFramebuffer( GL_FRAMEBUFFER, m_pEngView->m_viewFramebuffer );
}

//----------------------------------------------------------------------------------

int _eng_DrawInCorrectContext( void )
{
    // IN order for draw to work it must be render in the main context
    // because opengl es does not support vertex buffers from different contexts
    return GetCurrentContext() == s_ThreadContext[0];
}

@end
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

@implementation AppDelegate

//----------------------------------------------------------------------------------
// Tomas:
// I added this function. This function will call the user main to do the normal stuff
//----------------------------------------------------------------------------------

- (void)postFinishLaunch
{
    s_UserAppMain( s_argc, s_argv );
}

//----------------------------------------------------------------------------------
// Tomas:
// This function is key it needs to do two things.
// 1. Display the splash screen.... (Will see how we render that graphics)
//    I've just discovered that you can do this in XCode4! Which makes this a rather simple process now.
//    a. Select your project in the navigation view
//    b. under Targets select your application
//    c, Select the Summary tab
//    d. Scroll down and you'll see a place to add your splash images
//    e. Right click on the boxes to Select File
// 2. Call the engine Main (so that the engine can do whatever it needs to do)
//
//----------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // TOMAS: Set working directory to resource path
    [[NSFileManager defaultManager] changeCurrentDirectoryPath: [[NSBundle mainBundle] resourcePath]];
    
    // TOMAS: This may be an async call to (postFinishLaunch) Actually is here where the real user main gets call...
    [self performSelector:@selector(postFinishLaunch) withObject:nil afterDelay:0.0];
    
    
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


//----------------------------------------------------------------------------------
// TOMAS:
// This function gets call when the application becomes active for the first time
// as well when it get selected
//----------------------------------------------------------------------------------

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// BASE VIEW
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

@implementation eng_uibaseview

//---------------------------------------------------------------------------------------

- (void)dealloc
{
   // [super dealloc];
}

//---------------------------------------------------------------------------------------

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame: frame];
    
#if SDL_IPHONE_KEYBOARD
    [self initializeKeyboard];
#endif
    
    self.multipleTouchEnabled = YES;
    
    if( self )
    {
        m_WasPressIndexBuffer=0;
        int i;
        for( i=0; i<MAX_SIMULTANEOUS_TOUCHES; i++ )
        {
            m_KeyIsPress[i] = 0;
            m_KeyWasPress[0][i] = 0;
            m_KeyWasPress[1][i] = 0;
            m_ThouchValueABS[i].x = m_ThouchValueABS[i].y = 0;
            m_ThouchValueLast[i].x = m_ThouchValueLast[i].y = 0;
        }
    }
    
    return self;
    
}

//---------------------------------------------------------------------------------------

-(id)init
{
    self = [ super init];
    if( self )
    {
        m_WasPressIndexBuffer=0;
        int i;
        for( i=0; i<MAX_SIMULTANEOUS_TOUCHES; i++ )
        {
            m_KeyIsPress[i] = 0;
            m_KeyWasPress[0][i] = 0;
            m_KeyWasPress[1][i] = 0;
            m_ThouchValueABS[i].x = m_ThouchValueABS[i].y = 0;
            m_ThouchValueLast[i].x = m_ThouchValueLast[i].y = 0;
        }
    }
    
    return self;
}

//---------------------------------------------------------------------------------------

- (CGPoint)touchLocation:(UITouch *) pTouch shouldNormalize:(BOOL)bNormalize
{
    CGPoint Point = [ pTouch locationInView:self ];
    
    if( bNormalize )
    {
        CGRect Bounds = [self bounds];
        Point.x /= Bounds.size.width;
        Point.y /= Bounds.size.height;
    }
    else
    {
        CGRect Bounds = [self bounds];
        Point.x = Point.x * m_Scale;
        Point.y = (Bounds.size.height - Point.y) * m_Scale;
    }
    
    return Point;
}

//---------------------------------------------------------------------------------------

- (void)touchesBegan:(NSSet *)Touches withEvent:(UIEvent *)event
{
    NSEnumerator*   pEnumerator  = [Touches objectEnumerator];
    UITouch*        pTouch       = (UITouch*)[ pEnumerator nextObject ];
    
    //
    // Go though all the new touches
    //
    while( pTouch )
    {
        CGPoint LocationInView = [ self touchLocation:pTouch shouldNormalize:NO ];
        
        //
        // Look for a place to add the new touch
        //
        int i;
        for( i = 0; i < MAX_SIMULTANEOUS_TOUCHES; i++ )
        {
            if( m_pFinger[i] == NULL )
            {
                m_ThouchValueABS[i]  = LocationInView;
                m_ThouchValueLast[i] = LocationInView;
                m_pFinger[i]         = pTouch;
                m_KeyIsPress[i]      = 1;
                m_KeyWasPress[m_WasPressIndexBuffer][i] = 1;
                break;
            }
        }
        
        // Get the next possible touch
        pTouch = (UITouch*)[ pEnumerator nextObject ];
    }
}

//---------------------------------------------------------------------------------------

- (void)touchesMoved:(NSSet *)pTouches withEvent:(UIEvent *)pEvent
{
    NSEnumerator*   pEnumerator = [ pTouches objectEnumerator ];
    UITouch*        pTouch      = (UITouch*)[ pEnumerator nextObject ];
    
    while( pTouch )
    {
        CGPoint LocationInView = [ self touchLocation:pTouch shouldNormalize:NO ];
        
        int i;
        for( i = 0; i < MAX_SIMULTANEOUS_TOUCHES; i++)
        {
            if( m_pFinger[i] == pTouch )
            {
                m_ThouchValueLast[i] = m_ThouchValueABS[i];
                m_ThouchValueABS[i]  = LocationInView;
                m_KeyIsPress[i]      = 1;
                break;
            }
        }

        // Get the next finger
        pTouch = (UITouch*)[ pEnumerator nextObject ];
    }
}

//---------------------------------------------------------------------------------------

- (void)touchesEnded:(NSSet *)pTouches withEvent:(UIEvent *)pEvent
{
    NSEnumerator*   pEnumerator = [ pTouches objectEnumerator ];
    UITouch*        pTouch      = (UITouch*)[ pEnumerator nextObject ];
    
    while( pTouch )
    {
        int i;
        for( i = 0; i < MAX_SIMULTANEOUS_TOUCHES; i++ )
        {
            if( m_pFinger[i] == pTouch )
            {
                m_ThouchValueLast[i] = m_ThouchValueABS[i];
                m_pFinger[i]         = NULL;
                m_KeyIsPress[i]      = 0;
                break;
            }
        }

        pTouch = (UITouch*)[ pEnumerator nextObject ];
    }
}

//---------------------------------------------------------------------------------------

- (void)touchesCancelled:(NSSet *)pTouches withEvent:(UIEvent *)event
{
    // this can happen if the user puts more than 5 touches on the screen
    //  at once, or perhaps in other circumstances.  Usually (it seems)
    //  all active touches are canceled.
    [ self touchesEnded:pTouches withEvent:event ];
}

//---------------------------------------------------------------------------------------
// END
//---------------------------------------------------------------------------------------
@end

//---------------------------------------------------------------------------------------

int _eng_ThouchPadIsButtons( int KeyCode )
{
    return m_pEngView->m_KeyIsPress[ KeyCode ];
}

//---------------------------------------------------------------------------------------

int _eng_ThouchPadWasButtons( int KeyCode )
{
    return m_pEngView->m_KeyWasPress[1-m_pEngView->m_WasPressIndexBuffer][ KeyCode ];
}

//---------------------------------------------------------------------------------------

void _eng_ThouchPadValue( int KeyCode, float* X, float* Y )
{
    if( KeyCode < 5 )
    {
        *X = m_pEngView->m_ThouchValueABS[ KeyCode ].x - m_pEngView->m_ThouchValueLast[ KeyCode ].x;
        *Y = m_pEngView->m_ThouchValueABS[ KeyCode ].y - m_pEngView->m_ThouchValueLast[ KeyCode ].y;
    }
    else
    {
        KeyCode -= 5;
        
        *X = m_pEngView->m_ThouchValueABS[ KeyCode ].x;
        *Y = m_pEngView->m_ThouchValueABS[ KeyCode ].y;
    }
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// ENGINE ENTRY FUNCTIONS
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
// Main Entry for the engine
//---------------------------------------------------------------------------------------
void _eng_EntryPoint    ( int argc, char** argv );
int _eng_EntryPointIOS  ( void (*UserAppMain)( int argc, char* argv[] ), int argc, char** argv )
{
    _eng_EntryPoint( argc, argv );
    
    s_argc = argc;
    s_argv = argv;
    s_UserAppMain = UserAppMain;
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
