//
//  engBaseIOS.h
//  engBaseIOS
//
//  Created by Tomas Arce on 7/21/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//@property (strong, nonatomic) UIWindow *window;

@end

//-------------------------------------------------------------------------------------

@interface eng_uiviewcontroller : UIViewController
{
@private
    //    SDL_Window *window;
    
}
@end

//-------------------------------------------------------------------------------------
#define MAX_SIMULTANEOUS_TOUCHES 5

// Last part has to do with making the view a TEXT field so that we can open the keyboard anytime...
// will see if we need this later on...
@interface eng_uibaseview : UIView //<UITextFieldDelegate>
{
@public    
    UITouch*            m_pFinger[MAX_SIMULTANEOUS_TOUCHES];
    UITextField*        m_textField;
    BOOL                m_keyboardVisible;
    
    CGPoint             m_ThouchValueABS[MAX_SIMULTANEOUS_TOUCHES];
    CGPoint             m_ThouchValueLast[MAX_SIMULTANEOUS_TOUCHES];
    int                 m_KeyIsPress[MAX_SIMULTANEOUS_TOUCHES];
    int                 m_KeyWasPress[2][MAX_SIMULTANEOUS_TOUCHES];
    int                 m_WasPressIndexBuffer;
}

@end

//-------------------------------------------------------------------------------------

@interface eng_uiopenglview : eng_uibaseview
{
@public
    // Dimensions of the rendering buffers
    GLint           m_backingWidth;
    GLint           m_backingHeight;
    
    // OpenGL names for the renderbuffer and framebuffers used to render to this view
    GLuint          m_viewRenderbuffer;
    GLuint          m_viewFramebuffer;
    
    // OpenGL name for the depth buffer that is attached to viewFramebuffer, if it exists (0 if it does not exist)
    GLuint          m_depthRenderbuffer;
}

//@property (nonatomic, retain, readonly) EAGLContext* m_context;

- (id)initWithFrame:(CGRect)frame
              scale:(CGFloat)scale
      retainBacking:(BOOL)retained
              rBits:(int)rBits
              gBits:(int)gBits
              bBits:(int)bBits
              aBits:(int)aBits
          depthBits:(int)depthBits
        stencilBits:(int)stencilBits
       majorVersion:(int)majorVersion;

@end
