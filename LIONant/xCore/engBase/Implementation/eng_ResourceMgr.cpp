#include "eng_base.h"

/////////////////////////////////////////////////////////////////////////////////
// GLOBALS
/////////////////////////////////////////////////////////////////////////////////

eng_resource_mgr            g_RscMgr;
static eng_resource_type*   g_pRscType=NULL;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RESOURCE WORKER LOADER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

struct notification_request : public x_base_job
{
    enum
    {
        FLAGS_IS_LOADING    = X_BIT(0),
        FLAGS_IS_A_LIST     = X_BIT(1),
        FLAGS_DONE
    };

            void        SubmitNotification      ( void );
    virtual void        onRun                   ( void );
    static  void        HandleRsc               ( eng_resource_base& Rsc );

    u32                 m_Flags;            // LOADING | REDUCING
    eng_resource_base*  m_pRsc;
    s32                 m_Quality;
};

//-------------------------------------------------------------------------------

void notification_request::HandleRsc( eng_resource_base& Rsc )
{
    // The resource needs to be lock when we get here
    ASSERT( Rsc.m_qtFlags.isFlagOn( eng_resource_base::FLAGS_WORKING ) );

    notification_request* pNotify;

GOTO_TRY_AGAIN:

    // Try to get a notification from the resource
    pNotify = (notification_request*)Rsc.m_qtNotificationsQueue.pop();            
    if( pNotify == NULL )
    {
        // nothing to do so release the lock for the resource
        Rsc.m_qtFlags.qtSetFlagsOff( eng_resource_base::FLAGS_WORKING );
        return;
    }

    // handle the new notification
    s32 locQualityTarget = pNotify->m_pRsc->m_qtQualityTarget.get();
    if( g_RscMgr.m_AbortFlag == FALSE && locQualityTarget > pNotify->m_pRsc->m_CurMaxQuality )
    {
        pNotify->m_Flags    = FLAGS_IS_LOADING;
        pNotify->m_Quality  = locQualityTarget;
        g_RscMgr.m_qtDiskQueue.push( pNotify );

        // Try to lock the disk
        if( g_RscMgr.m_qtDiskLock.set( 0, 1 ) )
        {
            notification_request* pDskNotify = (notification_request*)g_RscMgr.m_qtDiskQueue.pop();

            // Disk job are always high priority
            pDskNotify->setupPriority( PRIORITY_ABOVE_NORMAL );
            g_Scheduler.StartJobChain( *pDskNotify );
        }
        else
        {
            // Nothing to do
        }
    }
    else if( locQualityTarget < pNotify->m_pRsc->m_CurMaxQuality )
    {
        pNotify->m_Flags    = 0;
        pNotify->m_Quality  = locQualityTarget;
        
        // For downgrades of resources they have low priority
        pNotify->setupPriority( PRIORITY_BELOW_NORMAL );
        g_Scheduler.StartJobChain( *pNotify );
    }
    else
    {
        // Nothing to notify to the resource
        x_delete( pNotify );
        goto GOTO_TRY_AGAIN;
    }
}

//-------------------------------------------------------------------------------

void notification_request::SubmitNotification( void )
{
    // Push this notification
    m_pRsc->m_qtNotificationsQueue.push( this );    

    // Try to lock the resource
    if( m_pRsc->m_qtFlags.qtSetFlagsOn( eng_resource_base::FLAGS_WORKING ) )
    {
        // Handle rsc
        notification_request::HandleRsc( *m_pRsc );
    }
}

//-------------------------------------------------------------------------------

void notification_request::onRun( void )
{
    // We only reduce here
    if( (m_Flags&FLAGS_IS_LOADING) == FLAGS_IS_LOADING )
    {
        ASSERT( g_RscMgr.m_qtDiskLock.get() == 1 );

        // Check to see if it is the first load |
        if( g_RscMgr.m_AbortFlag == FALSE && m_Quality == m_pRsc->m_qtQualityTarget.get() )
        {
            // We must load this not matter what
            if( m_pRsc->onQTLoadResource( m_Quality ) )
            {
                m_pRsc->m_CurMaxQuality = m_Quality;
            }
        }
        else
        {
            // We don't need to load since the user has change his mind... just ignore
        }

        // Make sure it was locked
        ASSERT( g_RscMgr.m_qtDiskLock.get() == 1 );

        // If the system want us to abort then we must kill all other pending notifications
        if( g_RscMgr.m_AbortFlag == TRUE )
        {
            while( notification_request* pNotify = (notification_request*)g_RscMgr.m_qtDiskQueue.pop() )
            {
                x_delete( pNotify );
            }
        }
        else
        {
            // Get the next job and submitted
            notification_request* pNotify = (notification_request*)g_RscMgr.m_qtDiskQueue.pop();
            if( pNotify )
            {
                // Start the next job
                g_Scheduler.StartJobChain( *pNotify );
            }
            else
            {
                // Release the lock because not more jobs for the disk
                g_RscMgr.m_qtDiskLock.setup( 0 );
            }
        }
    }
    else
    {
        // Check whether since the job was created the user has changed his mind
        if( m_Quality == m_pRsc->m_qtQualityTarget.get() )
        {
            // Lets ask the user to deal with this case
            if( m_pRsc->onQTReduceQuality( m_Quality ) )
            {
                m_pRsc->m_CurMaxQuality = m_Quality;
            }
        }
        else
        {
            // Nothing todo since the user has change his mind... there must be another
            // notification on its way
        }
    }

    //
    // Try to see if there is more notifications for this rsc
    //
    notification_request::HandleRsc( *m_pRsc );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RESOURCE MGR
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

void eng_resource_mgr::Initialize( void )
{
    xstring GuidFileNamePath;
    GuidFileNamePath.Format( "%s/GuidFileName.Link", m_pRootDir );
    
    if( x_io::FileExists( GuidFileNamePath) )
    {
        xtextfile File;
        File.OpenForReading( GuidFileNamePath );
        
        // Read the recrod header
        VERIFY( File.ReadRecord() );
        m_LinkToRscNames.New( File.GetRecordCount() );
        
        // Reach each record
        xguid   Guid;
        xstring FileName;
        s32     Count = File.GetRecordCount();
        for( s32 i=0; i<Count; i++ )
        {
            File.ReadLine();
            File.ReadField( "Guid:g", &Guid );
            File.ReadFieldXString("Name:s", FileName);
            
            //
            // Create the link for this file name
            //
            link& Link = m_LinkToRscNames[i];
            if( !FileName.IsEmpty() )
            {
                Link.m_RscName.Format( "%s--%s", (const char*)FileName, (const char*)Guid.GetAlphaString() );
            }
            else
            {
                Link.m_RscName.Format( "%s", (const char*)Guid.GetAlphaString() );
            }
            
            //
            // Now add it to our global pool of Guid to File names
            //
            m_GuidToFileName.Add( Guid, &Link );
        }
    }
}

//------------------------------------------------------------------------------

eng_resource_mgr::eng_resource_mgr( void )
{
    m_pRootDir = ".";
}

//------------------------------------------------------------------------------

eng_resource_mgr::~eng_resource_mgr( void )
{

}

//------------------------------------------------------------------------------

void eng_resource_mgr::PageFlip( void )
{
    const s32 iDelete = 3-(m_iFrame&3);
    
    //
    // Delete all pending resources
    // NOTE: The original plan was to leave the bases always in memory and nuke the attached resources
    //       but I am going to try to delete even the bases and see how it goes. Since good bases should
    //       only contain references to the actual resources.
    //
    // Spin Lock for hash lookup
    m_HashCriticalSection.BeginAtomic();

    for( x_qt_ptr* pNode = m_qtDeleteQueue[iDelete].pop(); pNode; pNode = m_qtDeleteQueue[iDelete].pop() )
    {
        eng_resource_base& Base = *(eng_resource_base*)pNode;
        
        // Make sure it was not re-requested since we marked it for deleteion.
        // if it was then lets remove it from the delete list.
        if( Base.m_qtRefCounting.get() > 0 )
        {
            Base.m_qtFlags.qtSetFlagsOff( eng_resource_base::FLAGS_DELETE );
            continue;
        }
        
        // Remove resource from the hash
        m_ResourceHash.Delete( Base.m_Guid.getXGuid() );

        // Now is safe to nucke it
        Base.getType().KillInstance( &Base );
    }
    
    // End the atomic section
    m_HashCriticalSection.EndAtomic();
    
    //
    // Now we can move the deletion index
    //
    m_iFrame++;
}

//------------------------------------------------------------------------------

void eng_resource_mgr::MarkResourceForDeletion( eng_resource_base& Base )
{
    ASSERT( Base.m_qtRefCounting.get() == 0 );

    // Mark for deletion
    Base.m_qtFlags.setFlags( eng_resource_base::FLAGS_DELETE );
    
    // Push the node to be deleted into the delete queue
    m_qtDeleteQueue[m_iFrame&3].push( &Base );
}

//------------------------------------------------------------------------------

xstring eng_resource_mgr::createFileName( eng_resource_guid Guid, s32 iQuality, const char* pExtension )
{
    xstring RscPath;
    
    //
    // If we loaded the link file it means we are loading debug resources so make sure we use their naming convention
    //
    if( m_GuidToFileName.getCount() )
    {
        link* pLink = m_GuidToFileName.GetValue( Guid.getXGuid() );
        ASSERT( pLink );
        
        
        if( iQuality ) RscPath.Format( "%s/%s_%d.%s", m_pRootDir, (const char*)pLink->m_RscName, iQuality, pExtension );
        else           RscPath.Format( "%s/%s.%s",    m_pRootDir, (const char*)pLink->m_RscName, pExtension );
    }
    else
    {
        // Use the release naming convention
        RscPath.Format( "%s/%s", m_pRootDir, (const char*)Guid.getAlphaString() );
    }
    
    return RscPath;
}

//------------------------------------------------------------------------------

xstring eng_resource_mgr::createFileName( eng_resource_guid Guid, s32 iQuality, const eng_resource_type& Type )
{
    return createFileName( Guid, iQuality, Type.getTypeString() );
}

//------------------------------------------------------------------------------

xguid eng_resource_mgr::createGuid( void )
{
    xguid Guid;
    Guid.ResetValue();
    Guid.m_Guid |= 1;
    return Guid;
}

//------------------------------------------------------------------------------

eng_resource_base& eng_resource_mgr::getResource( eng_resource_guid Guid )
{
    //
    // Get the resource first
    // TODO: This code is very ineficient (2 hash lookups, 64flags, etc)
    //
    eng_resource_base*  pResource=NULL;
    xbool               bNewAsset=FALSE;

    // Spin Lock for hash lookup
    m_HashCriticalSection.BeginAtomic();

    if( m_ResourceHash.IsAlloc( Guid.getXGuid() ) )
    {
        // Get the resource base
        pResource = m_ResourceHash.GetValue( Guid.getXGuid() );
        
        // Any new references must be counted
        pResource->incRefCount();
        
        // Make sure that this resource has not been marked for deletion
        // Problem that if it is while the fist case can survive the problem is if
        // the resource is mark for deletion again the x_qr_ptr now will have to be in two list
        // and get overwritten... bad stuff... if we added an if statment to prevent the insertion again
        // then the time of the deletion will be wrong and it could cause a crash or a bad rendering.
        // It is better for the user to decide clearly what they want and do necesary preparation for it.
        ASSERT( pResource->m_qtFlags.isFlagOn( eng_resource_base::FLAGS_DELETE) == FALSE );
    }
    else
    {
        // Append new resource
        pResource = FindResourceType( Guid.getType() )->CreateInstance();
        m_ResourceHash.Add( Guid.getXGuid(), pResource );

        // Set the guid of the resource we are dealing with
        pResource->m_Guid = Guid;
        
        // local reality get to request to load the default asset
        bNewAsset = TRUE;
    }

    // Release spin lock
    m_HashCriticalSection.EndAtomic();

    // If the resource is not loading the we need to add a job
    if( bNewAsset && !m_AbortFlag )
    {
        // Note that here the we are asking the resource to load its data
        // because he is the only one that should request for new data to be loaded
        // as it contains atomic flags inside of it.
        pResource->getAsset();
    }

    return *pResource;
}

//------------------------------------------------------------------------------

void eng_resource_mgr::AddNotification( eng_resource_base* pBase )
{
    notification_request* pNotify = x_new( notification_request, 1, XMEM_FLAG_ALIGN_16B );
    pNotify->m_Flags    = 0;
    pNotify->m_pRsc     = pBase;
    pNotify->m_Quality  = -1;

    // Submit the notification
    pNotify->SubmitNotification();
}

//------------------------------------------------------------------------------

void eng_resource_mgr::AbortLoading( void )
{
    m_AbortFlag = TRUE;
    // The loading worker/job is the one in charge of empty the loading queue 
    // and reset this flag to false
}


//------------------------------------------------------------------------------

eng_resource_type* eng_resource_mgr::FindResourceType( u32 UID )
{
    for( eng_resource_type* pNext = g_pRscType; pNext; pNext = pNext->m_pNext )
    {
        if( UID == pNext->m_UID ) return pNext;
    }

    ASSERT(0);
    return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RESOURCE TYPE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

eng_resource_type::eng_resource_type( const char* pType, u32 UID )
{
    // List it in the list
    m_pNext    = g_pRscType;
    g_pRscType = this;
    
    // Set the guid and extension
    m_pType = pType;
    m_UID   = UID;

    // TODO: Do sanity check to make sure that there are not collisions in the types
}

//------------------------------------------------------------------------------

eng_resource_type::~eng_resource_type( void )
{

}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RESOURCE BASE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

eng_resource_base::eng_resource_base( void )
{
    // Clear the guid until the right initialization process
    m_Guid.setNull();

    // At the begging there is not quality target
    m_qtQualityTarget.setup( -1 );

    // Setup our computation flag as not been locked
    m_qtFlags.setup( 0 );

    // I have been created so I must have a reference
    m_qtRefCounting.setup( 1 );

    // Set the current quality to -1 which means nothing loaded yet
    m_CurMaxQuality = -1;

    // The default max quality is zero which is the minimun quality any resource can have
    m_MaxQuality = 0;       
}

//------------------------------------------------------------------------------

eng_resource_base::~eng_resource_base( void )
{

}

//------------------------------------------------------------------------------

void eng_resource_base::incRefCount( void )
{
    m_qtRefCounting.Inc();
}

//------------------------------------------------------------------------------

eng_resource_guid eng_resource_base::decRefCount( void )
{
    if( m_qtRefCounting.Dec() == 0 )
    {
        // may be we should delete this resource
        // One frame delay?
        g_RscMgr.MarkResourceForDeletion( *this );
    }
    
    return m_Guid;
}

//------------------------------------------------------------------------------

void* eng_resource_base::getAsset( void )
{
    //
    // Do we have to load the asset?
    //
    if( m_CurMaxQuality < 0 )
    {
        // Get local reality for the loading state
        s32 locQualityTarget = m_qtQualityTarget.get();

        // If we are not loading we should be because people are requesting the resource
        if( locQualityTarget == -1 )
        {
            // If we own the local reality of loading the resource then make it happen
            if( m_qtQualityTarget.set( locQualityTarget, 0 ) )
            {
                g_RscMgr.AddNotification( this );
            }
        }

        // Since we don't have any quality already loaded we must return null
        return NULL;
    }

    //
    // Ok give the user a chance to reply
    //
    return onQTGetAsset();
}

//------------------------------------------------------------------------------

void eng_resource_base::ForceMaxQuality( s32 MaxQuality )
{
    // Make sure we clamp the maxium quality to the highest possible
    if( MaxQuality > m_MaxQuality )
    {
        // If we dont have anything loaded yet we will take the user's word
        // about the possible max quality
        if( m_CurMaxQuality == -1 )
        {
            // We dont have enough information to decide what to do in this case
            // so we will play it safe for now
            MaxQuality = 0;
        }
        else
        {
            // clamp the user max quality
            MaxQuality = m_MaxQuality;
        }
    }

    // We we have the same quality then there is nothing to do...
    if( MaxQuality == m_CurMaxQuality ) return ;

    //
    // Set the new target
    //
    s32 locQualityTarget = m_qtQualityTarget.get();
    s32 QT               = locQualityTarget;

    // If we don't have a target lets assume it is the same as our current quality
    if( locQualityTarget == -1 ) QT = m_CurMaxQuality;

    if( MaxQuality != QT )
    {
        if( m_qtQualityTarget.set( locQualityTarget, MaxQuality ) )
            g_RscMgr.AddNotification( this );
        // else someone has beat me at setting the quality... ho well we will go with his then
    }
}

