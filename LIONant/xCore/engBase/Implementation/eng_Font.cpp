//
//  eng_Font.cpp
//  engBase
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

struct eng_font_type : public eng_resource_type
{
    eng_font_type( const char* pType ) : eng_resource_type( pType, eng_font_rsc::UID ) {}
    
    virtual eng_resource_base*  CreateInstance      ( void );
    virtual void                KillInstance        ( eng_resource_base* pEntry );
};

static eng_font_type s_FontType( "font" );

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_font_rsc
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------

eng_font_rsc::~eng_font_rsc( void )
{
    if( m_pFontRsc )
    {
        x_delete( m_pFontRsc );
        m_pFontRsc = NULL;
    }
}

//------------------------------------------------------------------------------------------

eng_resource_type& eng_font_rsc::getStaticType( void )
{
    return s_FontType;
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_font_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_font_rsc_base : public eng_font_rsc
{
    virtual eng_resource_type&  getType             ( void ) const { return s_FontType; }
    virtual void*               onQTGetAsset        ( void )
    {
        // If we are loaded quality zero then we should return the data
        if( m_CurMaxQuality == 0 && m_SpriteAtlas.getAsset() )
            return this;
        
        x_LogWarning( "ENG_FONT", "Waiting to Load Font" );
        return NULL;
    }
    
    virtual xbool onQTLoadResource ( s32 iQuality )
    {
        ASSERT( iQuality == 0 );
        
        //
        // Lets setup the texture name
        // We want to start this as soon as possible
        //
        eng_resource_guid Guid;
        Guid.setup( m_Guid, eng_sprite_rsc::UID );
        m_SpriteAtlas.setup( Guid );
        m_SpriteAtlas.getAsset();
        
        //
        // Load the font data
        //
        xserialfile SerialFile;
        xstring FontPath = g_RscMgr.createFileName( m_Guid, iQuality, s_FontType.getTypeString() );
        
        SerialFile.Load( FontPath, m_pFontRsc );
        if( g_RscMgr.isAborting() )
        {
            if( m_pFontRsc )
            {
                x_delete( m_pFontRsc );
                m_pFontRsc = NULL;
            }
            return FALSE;
        }
        
        x_LogMessage( "ENG_FONT", "Load Done" );
        return TRUE;
    }
    
    virtual xbool onQTReduceQuality ( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0);
        return TRUE;
    }
};

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_resource_type
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

eng_resource_base* eng_font_type::CreateInstance( void )
{
    
    return x_new( eng_font_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_font_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_font_rsc_base*)pEntry );
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// _eng_fontsystem
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

static const char s_DistanceFieldFragmentShader[] = R"(

#ifdef GL_ES
    #extension GL_OES_standard_derivatives : enable
    //precision mediump   float;
    precision highp   float;
    varying lowp vec4   Color;
#else
    varying vec4        Color;
#endif

    varying vec2        Texcoord;
    uniform sampler2D   Texture;

       const vec4 Color2 = vec4( 1.0, 0.8 ,0.7, 1.0 );

    float sampleDistance( vec2 uv ) 
    {
        return texture2D( Texture, uv ).r;
    }

    vec2 blendFactors ( vec2 range, vec2 range2, float distance ) 
    {
        vec2 bf;
		bf.x = smoothstep( range.x,  range.y,  distance );
		bf.y = smoothstep( range2.x, range2.y, distance );
    	return bf;
    }

    vec4 sampleColor( vec2 bf ) 
    {
        vec4 newColor;
        newColor = mix( Color2, Color, bf.y );
        newColor.a *= bf.x;
        return newColor;
    }

    vec4 superSample( vec2 UV, vec2 range, vec2 range2 ) 
    {
	    vec2 deltaUV = 0.3535534 * ( dFdx(UV) + dFdy(UV) );
	    vec4 boxUV   = vec4( UV + deltaUV, UV - deltaUV );

        // Blending entire samples produces much better results than only blending distance samples.
        vec4 r;
        vec4 g;
        g  = sampleColor( blendFactors( range, range2, sampleDistance(UV.xy) ));
	    r  = sampleColor( blendFactors( range, range2, sampleDistance(boxUV.xy) ));
	    r += sampleColor( blendFactors( range, range2, sampleDistance(boxUV.xw) ));
	    r += sampleColor( blendFactors( range, range2, sampleDistance(boxUV.zy) ));
	    r += sampleColor( blendFactors( range, range2, sampleDistance(boxUV.zw) ));
	
	    return (r * 0.25 + g)*0.5;
    }

    const float outlineWith         = 0.15;
    const float outerEdgeCenter     = 0.5 - outlineWith;

    float uBevelLow     = 1.0;
    float uBevelHigh    = 0.63;
    float uBevelLow2    = 0.0;
    float uBevelHigh2   = 0.6;
    float uBevelScale   = 0.005;

    vec3 sampleNormal( vec2 UV ) 
    {
        // compute the pixel size
	    vec2 texelSize      = 2. * 0.3535534 * abs( dFdx(UV) + dFdy(UV) );

        // Using 2-pixel offset to smooth out precision terracing due to 8-bit precision of texture samples.
        vec3 uvOffset       = vec3( texelSize.x, texelSize.y, 0 ) * 4.;
	    vec4 uvA            = UV.xyxy - uvOffset.xzzy;
	    vec4 uvB            = UV.xyxy + uvOffset.xzzy;
	    vec4 crossSamples   = vec4( sampleDistance( uvA.xy ), // left
		                            sampleDistance( uvB.xy ), // right
		                            sampleDistance( uvA.zw ), // bottom
		                            sampleDistance( uvB.zw )  // top
	                               );
		
	    // Perform manual smoothstep.
	    vec4 heights = clamp( (crossSamples - uBevelLow) / (uBevelHigh - uBevelLow), 0., 1.);
	    heights = heights * heights * (3.0 - (2.0 * heights));
		
	    // Branch on uniform variables to avoid second bevel computation.
	    if( uBevelLow2 != uBevelHigh2 ) 
        {
		    vec4 heights2 = clamp( (crossSamples - uBevelLow2) / (uBevelHigh2 - uBevelLow2), 0., 1.);
		    heights += heights2 * heights2 * (3.0 - (2.0 * heights2));
	    }
		
	    vec2 heightData = (heights.xz - heights.yw) * uBevelScale * texelSize.yx;
		
	    // Derivation of bevel normal computation.
	    // lhs = (_MainTex_TexelSize.x, 0, heights.y - heights.x)
	    // rhs = (0, _MainTex_TexelSize.y, heights.w - heights.z)
	    // cross product
	    // lhs.y * rhs.z - lhs.z * rhs.y,
	    // lhs.z * rhs.x - lhs.x * rhs.z,
	    // lhs.x * rhs.y - lhs.y * rhs.x
	    // 0 * (heights.w - heights.z) - (heights.y - heights.x) * _MainTex_TexelSize.y,
	    // (heights.y - heights.x) * 0 - _MainTex_TexelSize.x * (heights.w - heights.z),
	    // _MainTex_TexelSize.x * _MainTex_TexelSize.y - 0 * 0
	    // (heights.x - heights.y) * _MainTex_TexelSize.y,
	    // (heights.z - heights.w) * _MainTex_TexelSize.x,
	    // _MainTex_TexelSize.x * _MainTex_TexelSize.y
		
	    vec3 bevel = normalize( vec3(heightData.x, heightData.y, texelSize.x * texelSize.y * 4.));
		
	    return bevel;
    }

    vec3 NormalBlend( vec3 base, vec3 tangent )
    {
        return normalize( base + tangent );
    }

    vec3 LightDir = vec3( 1., 0., 1. );
    void main( void )
    {
        // method1 - automatic width
        float dist      = sampleDistance( Texcoord );
        float width     = fwidth(dist);
        vec2  range     = vec2( outerEdgeCenter - width, outerEdgeCenter + width  );
	    vec2  range2    = vec2( 0.5             - width, 0.5             + width  );
        vec4  Color     = superSample( Texcoord, range, range2 );
        vec3  Normal    = sampleNormal( Texcoord );

        
        // method1 - automatic width
        gl_FragColor.rgb    =  min( 1., (max( 0., 0.5 * dot( normalize( LightDir ), Normal ) ) + 0.5 ) ) * Color.rgb;
        gl_FragColor.a      = Color.a;
    }


    /*
    // with outline
    const float outlineWith = 3.0/16.;
    const float outerEdgeCenter = 0.5 - outlineWith;
    const vec3  outlineColor = vec3( .0,0.,0. ); 
    void main( void )
    {
        vec4  color = texture2D( Texture, Texcoord.xy );
        float dist  = color.r;
        
        // method1 - automatic width
        float width  = fwidth(dist);
        float alpha  = smoothstep(outerEdgeCenter-width, outerEdgeCenter+width, dist);
        float border = smoothstep(0.5-width, 0.5+width, dist);
        

        gl_FragColor = vec4( mix( outlineColor, Color.rgb, border), alpha * Color.a);
    }
    */

/*
    // normal
    varying vec2        Texcoord;
    uniform sampler2D   Texture;
    const vec2          Gamma = vec2(0.,1.);  // This is for the future so that we can force a particular gamma. Gamma.x = gamma * (1-w), Gamma.y=w

    void main( void )
    {
        vec4  color = texture2D( Texture, Texcoord.xy );
        float dist  = color.r;
        
        // method1 - automatic width
        float width = Gamma.x + Gamma.y*fwidth(dist);
        float alpha = smoothstep(0.5-width, 0.5+width, dist);
        
        // method2 - aa width
        //float width = dist - 0.5;
        //float aa    = 0.75 * length( vec2( dFdx( width ), dFdy( width ) ));
        //float alpha = smoothstep(0.5-aa, 0.5+aa, dist);

        gl_FragColor = vec4( Color.rgb, alpha * Color.a);
    }

    */
)";

static const char s_DistanceFieldSupperSampleFragmentShader[] = R"(

#ifdef GL_ES
    #extension GL_OES_standard_derivatives : enable
    //precision mediump   float;
    precision highp     float;
    varying lowp vec4   Color;
#else
    varying vec4        Color;
#endif

    varying vec2        Texcoord;
    uniform sampler2D   Texture;
    const vec2          Gamma = vec2(0.,1.);  // This is for the future so that we can force a particular gamma. Gamma.x = gamma * (1-w), Gamma.y=w

    float contour( in float d, in float w )
    {
        return smoothstep(0.5 - w, 0.5 + w, d);
    }

    float samp( in vec2 uv, float w )
    {
        return contour( texture2D(Texture,uv).r, w );
    }

    void main(void)
    {
        vec2  uv    = Texcoord.xy;
        float dist  = texture2D( Texture, uv ).r;
        float width = Gamma.x + Gamma.y*fwidth(dist);
        float alpha = contour( dist, width );

        // ------- (comment this block out to get your original behavior)
        // Supersample, 4 extra points
        float   dscale  = 0.354; // half of 1/sqrt2; you can play with this
        vec2    duv     = dscale * (dFdx(uv) + dFdy(uv));
        vec4    box     = vec4(uv-duv, uv+duv);
        float   asum    = samp( box.xy, width )
                            + samp( box.zw, width )
                            + samp( box.xw, width )
                            + samp( box.zy, width );

        // weighted average, with 4 extra points having 0.5 weight each,
        // so 1 + 0.5*4 = 3 is the divisor. I am div by 2 to give a color boost
        alpha = (alpha + 0.5 * asum) / 2.;
        // -------

        gl_FragColor = vec4(Color.rgb, alpha * Color.a );   
    }
)";

//---------------------------------------------------------------------------------
struct font_system;
static font_system* s_pFontSystem=NULL;

struct font_system
{
    eng_shader_program m_Program;
    eng_shader_program m_ProgramSmall;
    eng_fshader        m_fShader;
    eng_fshader        m_fShaderSmall;

    font_system( void ) {}

    void init( eng_draw& Draw )
    {
        m_fShader.LoadFromMemory( s_DistanceFieldFragmentShader, sizeof( s_DistanceFieldFragmentShader ) );
        m_fShaderSmall.LoadFromMemory( s_DistanceFieldSupperSampleFragmentShader, sizeof( s_DistanceFieldSupperSampleFragmentShader ) );
        m_Program.LinkShaders( Draw.GetVShader( ), m_fShader );
        m_ProgramSmall.LinkShaders( Draw.GetVShader(), m_fShaderSmall );
        s_pFontSystem = this;
    }

    ~font_system( void )
    {
        m_Program.Destroy( );
        m_ProgramSmall.Destroy();
        m_fShader.Destroy();
        m_fShaderSmall.Destroy( );
    }
};

//---------------------------------------------------------------------------------

void* _eng_InitFontSystem( eng_draw& Draw )
{
    font_system* pFontSystem = x_new( font_system, 1, 0 );
    
    if ( pFontSystem ) 
        pFontSystem->init( Draw );

    return pFontSystem;
}

//---------------------------------------------------------------------------------

void _eng_KillFontSystem( void* pPtr )
{
    x_delete( (font_system*)pPtr );
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_font
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

void eng_font::setup( const u64 Guid, const char* pFontName )
{
    m_FontRscRef.setup( Guid );
    m_Key = (u16)x_strHash( xfs("%s000",pFontName), 0xffff );
}

//------------------------------------------------------------------------------------------

void eng_font::DrawText( eng_draw& Draw, const xvector2& Pos, const char* pString, const f32 Scale, xcolor Color )
{
    const eng_font_rsc* pFontRsc = Resolve();
    if( pFontRsc == NULL )
        return;
    
    //
    // Ok we should be ready to render at this point
    //
    const eng_sprite_rsc* pRsc = pFontRsc->m_SpriteAtlas.getAsset();
    ASSERT(pRsc);
    
    const eng_sprite_rsc::info&     SpriteInfo  = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ m_iSprite ];
    const eng_font_rsc::font&       Font        = pFontRsc->m_pFontRsc->m_Font.m_Ptr[ m_iSprite ];
    const eng_font_rsc::char_set&   CharSet     = pFontRsc->m_pFontRsc->m_CharSet.m_Ptr[ Font.m_iCharSet ];
    
    ASSERT( SpriteInfo.m_Animated.m_SpecialMask == 0xffff);
    ASSERT( pRsc->m_pGroup->m_SpriteNameHash.m_Ptr[ m_iSprite ] == Font.m_FontHashName );
    
    const s32 L = x_strlen(pString);
    
    //
    // Start the render loop
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LB | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_CUSTOM_PROGRAM );
    Draw.SetTexture( *pRsc->m_SpriteAtlas.getAsset() );
    if( Scale < 1 )
        Draw.SetProgram( s_pFontSystem->m_ProgramSmall );
    else
        Draw.SetProgram( s_pFontSystem->m_Program );

    draw_vertex* pBaseVertex;
    u16*         pBaseIndex;
    Draw.GetBuffers( &pBaseVertex, 4*L, &pBaseIndex, 6*L );
    
    
    xvector2 Cur = Pos;
    for( s32 i=0; i<L; i++ )
    {
        const u16 Key    = pString[i];
        const s32 Row    = Key / CharSet.m_PHTableSize;
        const s32 Column = Key % CharSet.m_PHTableSize;
        const s32 iHash  = Column + CharSet.m_PHTable.m_Ptr[Row];
        const s32 iGlyph = CharSet.m_HashTable.m_Ptr[ iHash ];
        
        if( iGlyph == 0xffff )
            continue;
        
        // Glypth not found
        ASSERT( iGlyph != 0xffff );
        
        const eng_font_rsc::character_info&  FontInfo = Font.m_CharacterInfo.m_Ptr[iGlyph];
        
        // Make sure everything is in bounds
        ASSERT( iHash  < CharSet.m_HashSize );
        ASSERT( iGlyph < CharSet.m_nCharacters );
        
        // Do we have that character?
        ASSERT( FontInfo.m_Original == Key );
        
        // Make sure that the glyph also matches the atlas range
        ASSERT( iGlyph < SpriteInfo.m_Animated.m_nFrames );
        ASSERT( iGlyph >= 0 );
        const eng_sprite_rsc::info& Sprite = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ SpriteInfo.m_Animated.m_iBase + iGlyph ];
        
        const s32    voffset = i*4;
        draw_vertex* pVertex = &pBaseVertex[voffset];
        u16*         pIndex  = &pBaseIndex[i*6];
        
        for( s32 v=0; v<4; v++ )
        {
            const eng_sprite_rsc::info::vertex& Vertex = Sprite.m_Vertex[v];
            
            pVertex[v].setup( xvector3d( Vertex.m_Pos.m_X*Scale + Cur.m_X,
                                         Vertex.m_Pos.m_Y*Scale + Cur.m_Y,
                                         0 ),
                              Vertex.m_UV.m_X,
                              Vertex.m_UV.m_Y, Color );
        }
        
        pIndex[0] = voffset + 0;
        pIndex[1] = voffset + 1;
        pIndex[2] = voffset + 2;
        
        pIndex[3] = voffset + 0;
        pIndex[4] = voffset + 2;
        pIndex[5] = voffset + 3;
        
        //
        // Move the cursor forward
        //
        Cur.m_X += FontInfo.m_XAdvance*Scale;
        Cur.m_Y += FontInfo.m_YAdvance*Scale;
    }
    
    Draw.DrawBufferTriangles();
    Draw.End();
}

//------------------------------------------------------------------------------------------

void eng_font::DrawText( eng_draw& Draw, const xmatrix4& Matrix, const xvector2& Pos, const xvector2& Scale, const char* pString, xcolor Color )
{
    const eng_font_rsc* pFontRsc = Resolve();
    if ( pFontRsc == NULL )
        return;

    //
    // Ok we should be ready to render at this point
    //
    const eng_sprite_rsc*           pRsc        = pFontRsc->m_SpriteAtlas.getAsset();
    ASSERT( pRsc );

    const eng_sprite_rsc::info&     SpriteInfo  = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ m_iSprite ];
    const eng_font_rsc::font&       Font        = pFontRsc->m_pFontRsc->m_Font.m_Ptr[ m_iSprite ];
    const eng_font_rsc::char_set&   CharSet     = pFontRsc->m_pFontRsc->m_CharSet.m_Ptr[ Font.m_iCharSet ];

    ASSERT( SpriteInfo.m_Animated.m_SpecialMask == 0xffff );
    ASSERT( pRsc->m_pGroup->m_SpriteNameHash.m_Ptr[ m_iSprite ] == Font.m_FontHashName );

    const s32 L = x_strlen( pString );

    //
    // Start the render loop
    //
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_RASTER_CULL_NONE | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_CUSTOM_PROGRAM );
    
    Draw.SetTexture( *pRsc->m_SpriteAtlas.getAsset() );
    if ( Scale.m_X < 1 || Scale.m_Y < 1 )
        Draw.SetProgram( s_pFontSystem->m_ProgramSmall );
    else
        Draw.SetProgram( s_pFontSystem->m_Program );

    Draw.SetL2W( Matrix );

    draw_vertex* pBaseVertex;
    u16*         pBaseIndex;
    Draw.GetBuffers( &pBaseVertex, 4 * L, &pBaseIndex, 6 * L );

    const s32 H =
    [&]{
        s32 W,H;
        eng_GetCurrentContext().GetScreenResolution(W,H);
        return H;
    }();

    
    xvector2 Cur( Pos.m_X, Pos.m_Y + Font.m_FontSize * Scale.m_Y );
    for ( s32 i = 0; i<L; i++ )
    {
        const u16 Key       = pString[ i ];
        const s32 Row       = Key / CharSet.m_PHTableSize;
        const s32 Column    = Key % CharSet.m_PHTableSize;
        const s32 iHash     = Column + CharSet.m_PHTable.m_Ptr[ Row ];
        const s32 iGlyph    = CharSet.m_HashTable.m_Ptr[ iHash ];

        if ( iGlyph == 0xffff )
            continue;

        // Glypth not found
        ASSERT( iGlyph != 0xffff );

        const eng_font_rsc::character_info&  FontInfo = Font.m_CharacterInfo.m_Ptr[ iGlyph ];

        // Make sure everything is in bounds
        ASSERT( iHash  < CharSet.m_HashSize );
        ASSERT( iGlyph < CharSet.m_nCharacters );

        // Do we have that character?
        ASSERT( FontInfo.m_Original == Key );

        // Make sure that the glyph also matches the atlas range
        ASSERT( iGlyph < SpriteInfo.m_Animated.m_nFrames );
        ASSERT( iGlyph >= 0 );
        const eng_sprite_rsc::info& Sprite = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ SpriteInfo.m_Animated.m_iBase + iGlyph ];

        const s32    voffset = i * 4;
        draw_vertex* pVertex = &pBaseVertex[ voffset ];
        u16*         pIndex  = &pBaseIndex[ i * 6 ];

        for ( s32 v = 0; v<4; v++ )
        {
            const eng_sprite_rsc::info::vertex& Vertex = Sprite.m_Vertex[ v ];
       //     const eng_sprite_rsc::info::vertex& VertexUV = Sprite.m_Vertex[ (v + 2)%4 ];

            pVertex[ v ].setup( xvector3d( Cur.m_X + Vertex.m_Pos.m_X * Scale.m_X,
                                           Cur.m_Y - Vertex.m_Pos.m_Y * Scale.m_Y,
                                                    0 ),
                                Vertex.m_UV.m_X,
                                Vertex.m_UV.m_Y, Color );
        }

        pIndex[ 0 ] = voffset + 0;
        pIndex[ 1 ] = voffset + 1;
        pIndex[ 2 ] = voffset + 2;

        pIndex[ 3 ] = voffset + 0;
        pIndex[ 4 ] = voffset + 2;
        pIndex[ 5 ] = voffset + 3;

        //
        // Move the cursor forward
        //
        Cur.m_X += FontInfo.m_XAdvance * Scale.m_X;
        Cur.m_Y += FontInfo.m_YAdvance * Scale.m_Y;
    }

    Draw.DrawBufferTriangles();
    Draw.End();
}

//------------------------------------------------------------------------------------------

const eng_font_rsc* eng_font::Resolve( void )
{
    const eng_font_rsc* pFontRsc = m_FontRscRef.getAsset();
    
    if( pFontRsc == NULL )
    {
        // Make sure that this remains null
        m_iSprite = 0xffff;
        return NULL;
    }
    
    if( m_iSprite == 0xffff )
    {
        m_iSprite = eng_sprite::ResolveKey( m_Key, pFontRsc->m_SpriteAtlas.getAsset() );
    }
    
    return pFontRsc;
}

