#include "eng_base.h"

void ForceLink( void ) {}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// eng_skeleton_type definition
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

struct eng_skeleton_type : public eng_resource_type
{
    eng_skeleton_type( const char* pType ) : eng_resource_type( pType, eng_skeleton_rsc::UID ) {}

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

static eng_skeleton_type s_SkeletonType( "skeleton" );


//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_skeleton_rsc_base
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

struct eng_skeleton_rsc_base : public eng_skeleton_rsc
{
    virtual eng_resource_type&  getType( void ) const { return s_SkeletonType; }
    virtual void*               onQTGetAsset( void )
    {
        return m_pSkeletonRSC;

        x_LogWarning( "ENG_SKELETON", "Waiting to Load skeleton" );
        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the info data
        //
        xserialfile SerialFile;
        xstring SkeletonPath = g_RscMgr.createFileName( m_Guid, iQuality, s_SkeletonType.getTypeString() );

        skeleton_rsc* pSkeletonRSC = NULL;
        SerialFile.Load( SkeletonPath, pSkeletonRSC );
        if ( g_RscMgr.isAborting() )
        {
            if ( pSkeletonRSC )
            {
                x_delete( pSkeletonRSC );
                pSkeletonRSC = NULL;
            }
            return FALSE;
        }

        m_pSkeletonRSC = pSkeletonRSC;
        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }
};

//-----------------------------------------------------------------------------------
eng_skeleton_rsc::~eng_skeleton_rsc( void )
{
    if ( m_pSkeletonRSC )
    {
        x_delete( m_pSkeletonRSC );
        m_pSkeletonRSC = NULL;
    }

}

//-----------------------------------------------------------------------------------
eng_resource_type&  eng_skeleton_rsc::getStaticType( void )
{
    return s_SkeletonType;
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_skeleton_type
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

eng_resource_base* eng_skeleton_type::CreateInstance( void )
{
    return x_new( eng_skeleton_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_skeleton_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_skeleton_rsc_base*)pEntry );
}
