//
//  eng_Geom.cpp
// 
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

extern void ForceLink( void );
void jack( void ) {ForceLink();}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// ANIM_GROUP_SYSTEM
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void eng_anim_group_cache::RemoveAllEntries( animgroup& AnimGroup )
{
    //
    // Remove all nodes from inside the cache
    // Minimize the time locking the cache
    //
    {
        // See if there is someone that is working with the cache right now
        xscope_atomic( m_Semaphore );

        // remove all blocks from the cache
        for( s32 i=0; i<AnimGroup.m_nTotalKeyBlocks; i++ )
        {
            auto& Block = AnimGroup.m_pKeyBlock.m_Ptr[i];
            
            if( Block.m_pNext.m_Ptr == Block.m_pPrev.m_Ptr )
            {
                ASSERT( Block.m_pNext.m_Ptr == NULL );
                continue;
            }
            RemoveFromList( Block );
        }
    }

    //
    // Free up all the memory created by the cache
    //
    for( s32 i=0; i<AnimGroup.m_nTotalKeyBlocks; i++ )
    {
        auto& Block = AnimGroup.m_pKeyBlock.m_Ptr[i];
            
        // TODO: I should free the block before releasing the memory to make sure not one is accessing it
                
        if( Block.m_pDecompressStream.m_Ptr )
        {
            x_delete( Block.m_pDecompressStream.m_Ptr );
            Block.m_pDecompressStream.m_Ptr = NULL;
        }
    }
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::InterpolateKeyFrames(
    xtransform*                         pDestination,
    const animgroup&                    AnimGroup, 
    ianim                               iAnim, 
    f32                                 fCurrFrame,
    f32                                 Rate )
{
    cpKeyFrames( AnimGroup, iAnim, fCurrFrame, Rate, [ this, &pDestination ]( 
        keyframe*                       pKeyFrame0, 
        keyframe*                       pKeyFrame1, 
        f32                             T, 
        s32                             nBones,
        xbool                           bLooping )
    {
        InterpolateKeyFrames( pDestination, pKeyFrame0, pKeyFrame1, T, nBones );
    } );

}

//---------------------------------------------------------------------------

void eng_anim_group_cache::InterpolateKeyFrames( 
    xtransform*                     pDestination,
    keyframe*                       pKeyFrame0, 
    keyframe*                       pKeyFrame1, 
    f32                             T, 
    s32                             nBones )
{
    for( s32 i=0; i<nBones; i++ )
    {
        auto& KeyFrame = pDestination[i];

        KeyFrame.m_Scale        = pKeyFrame0->m_Scale       + T * (pKeyFrame1->m_Scale       - pKeyFrame0->m_Scale);
        KeyFrame.m_Rotation     = pKeyFrame0->m_Rotation.BlendAccurate( T, pKeyFrame1->m_Rotation );
        KeyFrame.m_Translation  = pKeyFrame0->m_Translation + T * (pKeyFrame1->m_Translation - pKeyFrame0->m_Translation);
            
        pKeyFrame0++;
        pKeyFrame1++;
    }

    /*
        pMatrix[i].setup( S, R, T );

        // Concatenate with parent
        if( pAnimGroup->m_pBone.m_Ptr[i].m_iParent != -1 )
        {
            ASSERT( pAnimGroup->m_pBone.m_Ptr[i].m_iParent < i );
            pMatrix[i] = pMatrix[ pAnimGroup->m_pBone.m_Ptr[i].m_iParent ] * pMatrix[i];
        }
        */
}

//---------------------------------------------------------------------------
static
s32 getNextFrame( const eng_anim_group_rsc::anim_info& AnimInfo, s32 T0, f32 Rate, xbool& bWrappingAroundTheAnim )
{
    ASSERT( T0 < AnimInfo.m_AnimKeys.m_nFrames );
    ASSERT( T0 >= 0 );

    if( Rate == 0 ) return T0;

    const s32 LastFrame = AnimInfo.m_AnimKeys.m_nFrames-1; 
    
    bWrappingAroundTheAnim = FALSE;
    if( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_PING_PONG ) )
    {
        if( Rate < 0 )
        {
            if ( T0 == 0 ) 
            {
                return T0 + 1;
            }
            return T0 - 1;
        }

        if ( T0 == LastFrame ) 
        {
            return T0-1;
        }
        return T0+1;
    }
    else if( x_FlagIsOn( AnimInfo.m_Flags,eng_anim_group_rsc::anim_info::FLAGS_LOOP ) )
    {
        if( Rate < 0 ) 
        {
            if (T0 == 0 ) 
            {
                bWrappingAroundTheAnim = TRUE;
                return LastFrame;
            }

            return T0 - 1;
        } 

        if( T0 == LastFrame )
        {
            bWrappingAroundTheAnim = TRUE;
            return 0;
        }

        return T0 + 1;
    }

    if( Rate < 0 )
    {
        if ( T0 == 0 ) return 0;
        return T0 - 1;
    }
    
    if( T0 == LastFrame ) return T0;
    return T0 + 1; 
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::cpKeyFrames( 
    const animgroup&                    AnimGroup, 
    ianim                               iAnim, 
    f32                                 FrameTime,
    f32                                 Rate,
    keyf_compute_function               Callback )
{
    ASSERT( iAnim.m_Value >= 0 );
    ASSERT( iAnim.m_Value < AnimGroup.m_nTotalAnims );

    auto&           KeyInfo                 = AnimGroup.m_pAnimInfo.m_Ptr[ iAnim.m_Value ];
    const s32       T0                      = s32(FrameTime);
    const f32       T                       = FrameTime - T0;
    xbool           bWrappingAroundTheAnim  = FALSE;
    const igframe   igFrameT[2]             = { igframe(T0), igframe( getNextFrame( KeyInfo, T0, Rate, bWrappingAroundTheAnim ) ) };
    const ilblock   ilBlock[2]              = { igFrameT[0], igFrameT[1] };
    const s32       nDifferentBlocks        = ilBlock[0].m_Value != ilBlock[1].m_Value? 2 : 1;
    const s32       nBones                  = AnimGroup.m_nTotalBones;
    const keyframe* pF[2]; 

    ASSERT(igFrameT[0].m_Value < KeyInfo.m_AnimKeys.m_nFrames);
    ASSERT(igFrameT[1].m_Value < KeyInfo.m_AnimKeys.m_nFrames);
    ASSERT(igFrameT[0].m_Value >= 0);
    ASSERT(igFrameT[1].m_Value >= 0);

    if( nDifferentBlocks == 2 )
    {
        x_inline_light_jobs_block<2> BlockJob;
        for( s32 i=0; i<2; i++ )
        {
            const igblock       igBlock( AnimGroup, iAnim, ilBlock[i] );
            auto&               KeyBlock = AnimGroup.m_pKeyBlock.m_Ptr[ igBlock.m_Value ];
            const ilframe       ilFrame( igFrameT[i] );

            ASSERT(ilFrame.m_Value  < KeyBlock.m_nFrames);
            ASSERT(ilFrame.m_Value  >= 0);

            if( KeyBlock.m_pDecompressStream.m_Ptr == NULL )
            {
                BlockJob.SubmitJob( [ this, &AnimGroup, &KeyBlock, i, &pF, ilFrame, nBones ]()
                { 
                    qtDecompressFrames( AnimGroup, KeyBlock ); 
                    pF[i] = &KeyBlock.m_pDecompressStream.m_Ptr[ ilFrame.m_Value*nBones ];
                });
            }
            else
            {
                qtUpdateCacheForKeyblock( KeyBlock );
                pF[i] = &KeyBlock.m_pDecompressStream.m_Ptr[ ilFrame.m_Value*nBones ];
            }
        }

        BlockJob.FinishJobs();
    }
    else
    {
        const igblock   igBlock( AnimGroup, iAnim, ilBlock[0] );
        auto&           KeyBlock = AnimGroup.m_pKeyBlock.m_Ptr[ igBlock.m_Value ];
        const ilframe   ilFrame0( igFrameT[0] );
        const ilframe   ilFrame1( igFrameT[1] );

        ASSERT(ilFrame0.m_Value  < KeyBlock.m_nFrames);
        ASSERT(ilFrame1.m_Value  < KeyBlock.m_nFrames);
        ASSERT(ilFrame0.m_Value  >= 0);
        ASSERT(ilFrame1.m_Value  >= 0);

        if( KeyBlock.m_pDecompressStream.m_Ptr == NULL )
            qtDecompressFrames( AnimGroup, KeyBlock );
        else
            qtUpdateCacheForKeyblock( KeyBlock );

        pF[0] = &KeyBlock.m_pDecompressStream.m_Ptr[ ilFrame0.m_Value*nBones ];
        pF[1] = &KeyBlock.m_pDecompressStream.m_Ptr[ ilFrame1.m_Value*nBones ];
    }

    //
    // Finally call the user to do whatever he needs to
    //
    Callback( pF[0], pF[1], T, nBones, bWrappingAroundTheAnim );
}

//---------------------------------------------------------------------------

const xtransform& eng_anim_group_cache::getKeyFrame( 
    const animgroup&                    AnimGroup,
    ianim                               iAnim,
    s32                                 T0 )
{
    const keyframe* pKeyFrame = NULL;
    cpKeyFrames( AnimGroup, iAnim, f32(T0), 0, [&](keyframe* pKeyFrame0, keyframe* pKeyFrame1, f32 T, s32 nBones, xbool bLooping )
    {
        pKeyFrame = pKeyFrame0;
    });
   
    return *pKeyFrame;
}

//---------------------------------------------------------------------------

xbool eng_anim_group_cache::getInterpolatedRoot(
    xtransform&                         KeyFrame,
    const animgroup&                    AnimGroup,
    ianim                               iAnim,
    f32                                 T0,
    f32                                 Rate )
{
    xbool abWrappingAroundTheAnim;

    cpKeyFrames( AnimGroup, iAnim, T0, Rate, [&]( keyframe* pKeyFrame0, keyframe* pKeyFrame1, f32 T, s32 nBones, xbool bWrappingAroundTheAnim )
    {
        abWrappingAroundTheAnim = bWrappingAroundTheAnim;

        // For looping forward animations (non-pingpong loops) (not matter of going forward or backwards)
        // there is a potential issue because there are different types of such loops that are usually not enumerated.
        // ex:
        // 1) Continues loops - is an animation were the last frame and frame 0 are 100% compatible which could be
        //    something like a ball bouncing. Been compatible does not mean that they are the same frame but that they work
        //    together to create a continues motion. Having the same frame at the end which is a common practice in 
        //    engines is not necessarily the right thing, I will talk about this later.
        // 2) Non-continuous loops - This is usually the case for mocap animations. You can think for instance that walking 
        //    forward animation where the translations keeps getting larger. The last frame and frame zero are no compatible
        //    since the translations are so far away from each other. A worse example of this will be a turn animation which
        //    not only the translations but also the rotations are incompatible. To make them compatible the animation player
        //    must transforms the root as well as rotated so that it will align with the next cycle.         
        // For continuous loops missing information can be easily derived. For instance if we think back on 
        // the example of the bouncing ball. If Frame zero is the ball in in the ground, consequence frames the ball going up
        // and last frame where the ball is about to touch the ground again. The last frame does not need to be the same frame as
        // frame zero because we already have that information. However the assumed/derived in formation is that the last translation 
        // is the difference between the last frame and frame zero. For non-continuous loops this is a bit more complex, since the 
        // the translation is non-continuous. Even if we layout the frames in a global time line, there is not clear how much
        // translation we should have. Going back to the mocap walk forward animation the last frame and frame zero 's translations
        // don't match. we can assume that (LastFrame + 1).Rotations = (FrameZero).Rotations, Translations we have to derived somehow.
        // We can for instance use the linear velocity between the last two frames. LinearVel = (Last Frame).Translations - (Last Frame-1).Translation 
        // So the new (LastFrame+1).Translation = (LastFrame).Translation + LinearVel. However this is a guess/assumption. 
        // if we look back to the example of the bouncing ball this for instance wont be true. Just like translation there is 
        // a missing information in the rotations as well. We can fill the gaps in a similar fashion as with the translation by
        // finding the angular velocity and adding it to the last frame as well. However this is also an assumption and can also be wrong.
        // This is where having an extra frame at the end to fix this missing information is the usual way to fix this.
        // Another way to fix this is that frame zero could contain that information. For instance if the translation of the walking 
        // animation stead of starting at (0,y,0) will start at (linearvelx,y,linearvelz) then the translation missing information
        // is fully solved. For a turn animation then it becomes an issue of aligning it properly since the translation offset is already
        // solved as just described. Rotations are a bit more thorny since there is not an abs value that we can reference there. 
        // The easy way out will be to ask the animator to enter the rotation alignment manually.So if it is an 90 turn then we ask
        // the animation to enter/give us that abs coordinate, then that will mean that framezero will contain just the angular velocity 
        // then since the final rotation will be. (last frame + 1).rotation = (abs rotation provided by artist) + (frame 0).rotation.
        // Note that the abs rotation give us by the artist will only be needed if we are playing back to back the full animation
        // with not extra data merged. However this almost never happens so there is not need to be so pure. Stead we will 
        // assume that the root bone orientation contains the actual desire alignment orientation. So the final formula will be:
        // ( last frame + 1).Rotation = ( last frame + 1).Rotation + (frame zero).Rotation, in other words the last frame root node
        // contains the rotation needed for the alignment, will does require frame zero rotations to be continue from the last frame
        // root's rotation.  
        //
        auto& AnimInfo = AnimGroup.m_pAnimInfo.m_Ptr[ iAnim.m_Value ];

        // Not need to deal with discontinuities here so just interpolate and return
        if( !(abWrappingAroundTheAnim && x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_LOOP )) )
        {
            KeyFrame.Blend( pKeyFrame0[ 0 ], T, pKeyFrame1[ 0 ] );
            return;
        }

        //
        // Deal with the ugliness of the regular animation loop
        //
        const auto& K0              = pKeyFrame0[ 0 ];
        const auto& K1              = pKeyFrame1[ 0 ];
        const xbool bAccTranslation = x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATIONS );
        const xbool bAccRotation    = x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATIONS );

        // Scale is handle normally
        KeyFrame.m_Scale        = K0.m_Scale + T*( K1.m_Scale - K0.m_Scale );

        // Accumulation of rotation is a clear message that the animation must be rotationally discontinuous  
        if( bAccRotation )
        {
            // Because it makes everything easier we snap the rotation of the root the the last frame
            // TODO: Pre-orient the animation with the proper angular velocity to match the 0,0,0 rotation
            const xquaternion       KNew = K1.m_Rotation.getDeltaRotation( K0.m_Rotation );
            xradian3                Rot  = KNew.getRotation();

            // Compute the angle depending on configuration
            if ( !x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_PITCH ) )
                Rot.m_Pitch = 0;

            if ( !x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_YAW ) )
                Rot.m_Yaw = 0;

            if ( !x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_ROLL ) )
                Rot.m_Roll = 0;

            xquaternion XX =  xquaternion(Rot) * K1.m_Rotation;
            KeyFrame.m_Rotation = K0.m_Rotation.BlendAccurate( T, XX );

            if ( bAccTranslation )
            {
                // This is the example of a turn in a mocap animation
                // Now filter base on per component
                if ( Rate >= 0 )
                {
                    const xvector3  NewK1Translation = xquaternion( Rot ) * K1.m_Translation;

                    // Now deal with transforms
                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_X ) )
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T * NewK1Translation.m_X;
                    else
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*( NewK1Translation.m_X - K0.m_Translation.m_X );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Y ) )
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T * NewK1Translation.m_Y;
                    else
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*( NewK1Translation.m_Y - K0.m_Translation.m_Y );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Z ) )
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T * NewK1Translation.m_Z;
                    else
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*( NewK1Translation.m_Z - K0.m_Translation.m_Z );
                }
                else
                {
                    const xvector3 NewK1Translation = K0.m_Translation;

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_X ) )
                        KeyFrame.m_Translation.m_X = (1-T)*NewK1Translation.m_X;
                    else
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*( NewK1Translation.m_X - K0.m_Translation.m_X );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Y ) )
                        KeyFrame.m_Translation.m_Y = (1-T)*NewK1Translation.m_Y;
                    else
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*( NewK1Translation.m_Y - K0.m_Translation.m_Y );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Z ) )
                        KeyFrame.m_Translation.m_Z = (1-T)*NewK1Translation.m_Z;
                    else
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*( NewK1Translation.m_Z - K0.m_Translation.m_Z );
                }
            }
            else
            {
                xquaternion       DQ               = K1.m_Rotation.getDeltaRotation( K0.m_Rotation );
                const xvector3    NewK1Translation = DQ * K1.m_Translation;

                // This is the example of a gunship turret  
                KeyFrame.m_Translation = K0.m_Translation + T*( NewK1Translation - K0.m_Translation );
            }
        }
        else
        {
            // If we dont need to accumulate then should be safe to assume to it is continuous
            KeyFrame.m_Rotation = K0.m_Rotation.BlendAccurate( T, K1.m_Rotation );

            if ( bAccTranslation )
            {
                // This is the example of a running forward animation
                // Now filter base on per component
                if( Rate >= 0 )
                {
                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_X ) )
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*K1.m_Translation.m_X;
                    else
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*( K1.m_Translation.m_X - K0.m_Translation.m_X );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Y ) )
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*K1.m_Translation.m_Y;
                    else
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*( K1.m_Translation.m_Y - K0.m_Translation.m_Y );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Z ) )
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*K1.m_Translation.m_Z;
                    else
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*( K1.m_Translation.m_Z - K0.m_Translation.m_Z );
                }
                else
                {
                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_X ) )
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*K0.m_Translation.m_X;
                    else
                        KeyFrame.m_Translation.m_X = K0.m_Translation.m_X + T*( K1.m_Translation.m_X - K0.m_Translation.m_X );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Y ) )
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*K0.m_Translation.m_Y ;
                    else
                        KeyFrame.m_Translation.m_Y = K0.m_Translation.m_Y + T*( K1.m_Translation.m_Y - K0.m_Translation.m_Y );

                    if ( x_FlagIsOn( AnimInfo.m_Flags, eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Z ) )
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*K0.m_Translation.m_Z;
                    else
                        KeyFrame.m_Translation.m_Z = K0.m_Translation.m_Z + T*( K1.m_Translation.m_Z - K0.m_Translation.m_Z );
                }
            }
            else
            {
                // This example is the Regular blending such the case of a idle
                KeyFrame.m_Translation = K0.m_Translation + T*( K1.m_Translation - K0.m_Translation );
            }
        }
         
    } );

    return abWrappingAroundTheAnim;
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::qtDecompressFrames( animgroup& AnimGroup, eng_anim_group_rsc::anim_key_block& Block )
{
    // Add our memory to the pool 
    m_MemoryAllocated.Add( Block.m_nFrames * Block.m_nStreams * sizeof(keyframe) );

    //
    // If we run out of memory then just delete a few
    //
    if( m_MemoryAllocated.get() > m_MaxMemAllowed )
    {
        // we will be touching the link list so we need to lock
        xscope_atomic( m_Semaphore );

        do
        {
            auto pEntry = m_pBlockTail;
            ASSERT( pEntry );

            RemoveFromList( *pEntry );
            m_MemoryAllocated.Sub( pEntry->m_KeyCount * sizeof(keyframe) );

            xscope_atomic( *pEntry );

            auto Ptr = pEntry->m_pDecompressStream.m_Ptr;
            pEntry->m_pDecompressStream.m_Ptr = NULL;
            x_delete( Ptr );

        } while( m_MemoryAllocated.get() > m_MaxMemAllowed );
    }

    //
    // Finally decompress the data
    //
    xscope_atomic( Block );

    // If someone beat us to it then....
    if( Block.m_pDecompressStream.m_Ptr )
    {
        m_MemoryAllocated.Sub( Block.m_KeyCount * sizeof(keyframe) );
        return;
    }        

    // Ok finally we get to do our job
    xtransform* pData  = x_new( xtransform, Block.m_KeyCount, XMEM_FLAG_ALIGN_16B );
    eng_anim_group_compressor::AnimationDecompress( pData, Block, AnimGroup );
    Block.m_pDecompressStream.m_Ptr = pData;

    // Finally add him to the list
    xscope_atomic( m_Semaphore );
    AddToFront( Block );
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::qtUpdateCacheForKeyblock( eng_anim_group_rsc::anim_key_block& Block )
{
    xscope_atomic( m_Semaphore );
    ASSERT( Block.m_pDecompressStream.m_Ptr );
    RemoveFromList( Block );
    AddToFront( Block );
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::AddToFront( eng_anim_group_rsc::anim_key_block& Block )
{
    Block.m_pNext.m_Ptr = m_pBlockHead;
    Block.m_pPrev.m_Ptr = NULL;
    if( m_pBlockHead )         m_pBlockHead->m_pPrev.m_Ptr = &Block;
    if( m_pBlockTail == NULL ) m_pBlockTail                = &Block;
    m_pBlockHead = &Block;
}

//---------------------------------------------------------------------------

void eng_anim_group_cache::RemoveFromList( eng_anim_group_rsc::anim_key_block& Block )
{
    if( Block.m_pNext.m_Ptr ) Block.m_pNext.m_Ptr->m_pPrev.m_Ptr = Block.m_pPrev.m_Ptr;
    if( Block.m_pPrev.m_Ptr ) Block.m_pPrev.m_Ptr->m_pNext.m_Ptr = Block.m_pNext.m_Ptr;
    if( m_pBlockHead == &Block ) m_pBlockHead = Block.m_pNext.m_Ptr;
    if( m_pBlockTail == &Block ) m_pBlockTail = Block.m_pPrev.m_Ptr;
    Block.m_pNext.m_Ptr = Block.m_pPrev.m_Ptr = NULL;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


eng_anim_group_cache g_AnimGroupSystem;

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_geom_type
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void    UnregisterAnimGroup ( eng_anim_group_rsc& AnimGroup );


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_geom_type
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void ForceMaterialLinking( void );

struct eng_anim_group_type : public eng_resource_type
{
    eng_anim_group_type( const char* pType ) : eng_resource_type( pType, eng_anim_group_rsc::UID ) { ForceMaterialLinking(); }

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

static eng_anim_group_type s_AnimGroupType( "animgroup" );


//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_anim_group_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_anim_group_rsc_base : public eng_anim_group_rsc
{
    enum loading_state
    {
        LOADING_STATE_NOT_STARTED,
        LOADING_STATE_READY
    };

    loading_state   m_LoadingState = LOADING_STATE_NOT_STARTED; 

    virtual eng_resource_type&  getType( void ) const { return s_AnimGroupType; }
    virtual void*               onQTGetAsset( void )
    {
        if ( m_LoadingState == LOADING_STATE_READY )
            return m_pAnimRSC;

        if( m_pAnimRSC && m_LoadingState != LOADING_STATE_READY )
            if( m_pAnimRSC->getSkeletonRef().getAsset() ) m_LoadingState = LOADING_STATE_READY;

        if ( m_LoadingState == LOADING_STATE_READY )
            return m_pAnimRSC;

        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the geom data
        //
        xserialfile SerialFile;
        xstring GeomPath = g_RscMgr.createFileName( m_Guid, iQuality, s_AnimGroupType.getTypeString( ) );

        SerialFile.Load( GeomPath, m_pAnimRSC );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pAnimRSC )
            {
                x_delete( m_pAnimRSC );
                m_pAnimRSC = NULL;
            }
            return FALSE;
        }

        // Start Pulling the skeleton
        m_pAnimRSC->getSkeletonRef().getAsset();

        x_LogMessage( "ENG_ANIM_GROUP", "Load Done" );
        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }
};

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_resource_type
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

eng_resource_base* eng_anim_group_type::CreateInstance( void )
{
    return x_new( eng_anim_group_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_anim_group_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_anim_group_rsc_base*)pEntry );
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_anim_group_rsc
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

eng_anim_group_rsc::~eng_anim_group_rsc( void )
{
    if ( m_pAnimRSC )
    {
        x_delete( m_pAnimRSC );
        m_pAnimRSC = NULL;
    }
}

//---------------------------------------------------------------------------

eng_resource_type& eng_anim_group_rsc::getStaticType( void )
{
    return s_AnimGroupType;
}




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_animgroup_player
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void eng_anim_group_player::setup( const eng_anim_group_rsc::ref& AnimGroupRef, u32 Seed )
{
    m_AnimGroupRef = AnimGroupRef;
    m_Random.setSeed32( Seed );
}

//---------------------------------------------------------------------------

void eng_anim_group_player::TransitionToAnimation( ianimhash HashName )
{
    // Set the next animation hash name
    m_NextAnimHashName = HashName;

    // First make sure if we have it
    auto* pAnimGroup = Resolved();
    if( pAnimGroup == NULL )
        return;

    auto& AnimGroup  = *pAnimGroup;
    ianim iAnimation( -1 );

    // Set the next animation
    {
        eng_anim_group_rsc::hash_entry Key( m_NextAnimHashName );
        
        const eng_anim_group_rsc::hash_entry* pEntry = (const eng_anim_group_rsc::hash_entry*)x_bsearch( 
            &Key,
            AnimGroup.m_pHashTable.m_Ptr, 
            AnimGroup.m_nTotalAnims, 
            sizeof(*AnimGroup.m_pHashTable.m_Ptr),
            eng_anim_group_rsc::hash_entry::CompareFunction );

        ASSERT( pEntry );
        iAnimation = ianim(pEntry->m_iAnim);
    }

    // Make sure that we have set the next taken into consideration each animation weight
    const s32 nSameAnims = AnimGroup.m_pAnimInfo.m_Ptr[ iAnimation.m_Value ].m_nSameAnims; 
    if( nSameAnims > 1 )
    {
        const f32   OneOverTotalWeight  = 1.0f/AnimGroup.m_pAnimInfo.m_Ptr[ iAnimation.m_Value ].m_AnimsWeight;
        f32         Random              = m_Random.RandF32();
        
        for( s32 i=0; i<nSameAnims; i++ )
        {
           auto&        AnimInfo = AnimGroup.m_pAnimInfo.m_Ptr[ iAnimation.m_Value + i ];
           const f32    MyHeight = AnimInfo.m_Weight * OneOverTotalWeight;
           
           if( MyHeight >= Random )
           {
               iAnimation.m_Value += i;
               break;
           }

           // Remove my height from the current random level
           Random -= MyHeight;
        }
    }

    //
    // Set the animation
    //
    if( m_iCurrAnim.m_Value == ianim::INVALID ) 
    {
        m_iCurrAnim = iAnimation;
    }
    else
    {
        m_iNextAnim = iAnimation; 
    }
}

//---------------------------------------------------------------------------

const eng_anim_group_rsc::anim_group_rsc* eng_anim_group_player::Resolved( void )
{
    if(m_AnimGroupRef.isValid() == FALSE )
        return NULL;

    auto* pAnimGroup = m_AnimGroupRef.getAsset();
    if( m_bResolved )
    {
        ASSERT(pAnimGroup);
        return pAnimGroup;
    }

    if( pAnimGroup == NULL )
        return NULL;

    m_bResolved = TRUE;
    if( m_NextAnimHashName.m_Value != ianimhash::INVALID )
    {
        TransitionToAnimation( m_NextAnimHashName );
    }

    return pAnimGroup;
}

//---------------------------------------------------------------------------

xbool eng_anim_group_player::BeginAdvanceTime( f32 DeltaTime )
{
    m_LastFrameTime  = m_FrameTime;
    m_FrameTime     += DeltaTime;
    m_bReachedEnd    = FALSE;

    auto* pAnimGroup = Resolved();
    if( pAnimGroup == NULL )
        return TRUE;

    auto& AnimGroup = *pAnimGroup;
    
    // Make sure that we have initialize our animation
    if( m_iCurrAnim.m_Value == ianim::INVALID )
    {
        if( m_iNextAnim.m_Value == ianim::INVALID )
            return TRUE;

        m_iCurrAnim = m_iNextAnim;
        m_iNextAnim.m_Value = ianim::INVALID;
    }

    f32 m_LoopDelay = 0;

    auto&       AnimInfo            = AnimGroup.m_pAnimInfo.m_Ptr[ m_iCurrAnim.m_Value ];
    const f32   nFramesToStep       = DeltaTime * AnimInfo.m_FPS;
    const f32   nFramesWithDelay    = AnimInfo.m_AnimKeys.m_nFrames + m_LoopDelay * AnimInfo.m_FPS;

    if( m_bStopAtEnd && (m_FrameTime + nFramesToStep > (nFramesWithDelay-2)))
    {    
        m_FrameTime   = nFramesWithDelay - 2;
        m_bReachedEnd = TRUE;
    }
    else if( m_bStopAtEnd && ((m_FrameTime + nFramesToStep) < 0) )
    {    
        m_FrameTime     = 0;
        m_bReachedEnd   = TRUE;
    }
    else
    {
        m_FrameTime += nFramesToStep;

        // Handle going fowards in time
        while( m_FrameTime > (nFramesWithDelay-1) )
        {
            if( !m_bLooping )
            {
                m_FrameTime = nFramesWithDelay - 1;
                m_Cycle = 0;
            }
            else
            {
                m_FrameTime -= nFramesWithDelay - 1;
                m_Cycle++;
            }
        }
    
        // handle going backwards in time
        while( m_FrameTime < 0 )
        {
            if( !m_bLooping )
            {
                m_FrameTime = 0;
                m_Cycle     = 0;
            }
            else
            {
                m_FrameTime += nFramesWithDelay - 1;
                m_Cycle--;
            }
        }
    }

    ASSERT( ( m_FrameTime >= 0 ) && ( m_FrameTime < nFramesWithDelay ) );

    return TRUE;
}

//---------------------------------------------------------------------------

xbool eng_anim_group_player::WhileAdvanceTime( event& Event )
{
 //   auto* pAnimGroup = Resolved();
    /*
    if( pAnimGroup == NULL || m_iCurrFrame.m_Value == -1 )
        return FALSE;

    auto& AnimGroup = *pAnimGroup;
    auto& AnimInfo  = AnimGroup.m_pAnimInfo.m_Ptr[ m_iCurrAnim.m_Value ];

    if( AnimInfo.m_nEvents == 0 )
        return FALSE;

    const event* pEvent = &AnimGroup.m_pEvent.m_Ptr[ AnimInfo.m_iEvent ];

    // Do event stuff here
    */
    return FALSE;
}


//---------------------------------------------------------------------------

xmatrix4* eng_anim_group_player::getMatrices( void )
{
    eng_scrach_memory& ScrachMem = eng_GetCurrentContext().getScrachMem();

    //
    // Handle the edge case of not having a selected animation or not having the asset loaded
    //
    auto* pAnimGroup = Resolved();
    if( pAnimGroup == NULL || m_iCurrAnim.m_Value == ianim::INVALID)
    {
        s32 nBones = 1;
        if( pAnimGroup )
            nBones = pAnimGroup->m_nTotalBones;

        xmatrix4* pMatrix = ScrachMem.BufferAlloc<xmatrix4>( nBones );
        
        for( s32 i=0; i<nBones; i++ )
            pMatrix[i].Identity();
        
        return pMatrix;
    }

    auto&       Skeleton    = *pAnimGroup->getSkeletonRef().getAsset();
    const s32   nBones      = Skeleton.m_nBones;
    xmatrix4*   pMatrix     = ScrachMem.BufferAlloc<xmatrix4>( nBones );
    
    g_AnimGroupSystem.cpKeyFrames( *pAnimGroup, m_iCurrAnim, m_FrameTime, m_Rate, [ this, &Skeleton, &pMatrix, &pAnimGroup ]( 
        keyframe*                       pF0, 
        keyframe*                       pF1, 
        f32                             T, 
        s32                             nBones,
        xbool                           bLooping )
    {
        for( s32 i=0; i<nBones; i++ )
        {
            xquaternion Rotation    = pF0->m_Rotation.BlendAccurate( T, pF1->m_Rotation );
            xvector3    Scale = pF0->m_Scale       + T*(pF1->m_Scale       - pF0->m_Scale);
            xvector3    Translation = pF0->m_Translation + T*(pF1->m_Translation - pF0->m_Translation);
            pF0++;
            pF1++;

            pMatrix[i].setup( Scale, Rotation, Translation );

            // Concatenate with parent
            if( Skeleton.m_pBone.m_Ptr[i].m_iParent != -1 )
            {
                ASSERT( Skeleton.m_pBone.m_Ptr[i].m_iParent < i );
                pMatrix[i] = pMatrix[ Skeleton.m_pBone.m_Ptr[i].m_iParent ] * pMatrix[i];
            }
        }

        // Apply bind matrices
        for( s32 i=0; i<nBones; i++ )
        {
            auto& Bone   = Skeleton.m_pBone.m_Ptr[i];
            auto& Matrix = pMatrix[i];

            if( 1 )
            {
                Matrix.setTranslation( Matrix * Bone.m_BindMatrixInv.getTranslation() );
            }
            else
            {
                Matrix = Matrix * Bone.m_BindMatrixInv; 
            }
        }
    } );


    return pMatrix;
}
