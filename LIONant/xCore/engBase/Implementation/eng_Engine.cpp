//===============================================================================
// INCLUDES
//===============================================================================
#include "eng_Base.h"
//#include <GLUT/glut.h>

//===============================================================================
// EXTERNALLY DECLARE OBJECTIVE-C FUNCTIONS
//===============================================================================

extern "C"
{
    eng_hwin    _eng_CreateWindow           ( s32 X, s32 Y, s32 Width, s32 Height );
    void        _eng_KillWindow             ( eng_hwin Handle );
    void        _eng_GetWindowSize          ( eng_hwin Handle, int* wid, int* hei );
    xbool       _eng_HandleEvents           ( void );
    void        _eng_PageFlip               ( eng_context* Context, xbool WaitSync );
    void        _eng_SetCurrentContext      ( eng_context* Context );
    void        _eng_BindMainBuffer         ( void );
    void        _eng_Init                   ( void );
    void        _eng_Kill                   ( void );
    void        _eng_InitContext            ( eng_context* Context );
    void        _eng_KillContext            ( eng_context* Context );
}

eng_draw&       _eng_CreateDraw             ( void );
void            _eng_KillDraw               ( eng_draw& Draw );
void*           _eng_InitFontSystem         ( eng_draw& Draw );
void            _eng_KillFontSystem         ( void* pPtr );
void*           _eng_InitSpriteSystem       ( void );
void            _eng_KillSpriteSystem       ( void* pPtr );
void*           _eng_InitGeomSystem         ( void );
void            _eng_KillGeomSystem         ( void* pPtr );



#if defined TARGET_OSX || defined TARGET_IOS
void        _eng_InitContext            ( eng_context* Context ){}
void        _eng_KillContext            ( eng_context* Context ){}
void        _eng_Init                   ( void ){}
void        _eng_Kill                   ( void ){}
void        _eng_KillWindow             ( eng_hwin Handle ){}
#endif

//===============================================================================
// FUNCTIONS
//===============================================================================


struct eng_context_internal : public eng_context
{
    void HandleRenderJobs( void );
    
    //
    // Presets
    //
    s32             m_PreSetWidth       = 800;
    s32             m_PreSetHeight      = 600;
    eng_hwin        m_WindowHandle      = NULL;
    s32             m_ScrachMemSize     = 128*1024;

    //
    // Stats
    //
    s64             m_FrameNumber       = 0;
    xbool           m_bPrintFPS         = TRUE;
    xtick           m_FPSFrameTime[ 8 ];
    xtick           m_FPSLastTime       = 0;
    s32             m_FPSIndex          = 0;
    xtimer          m_CPUTIMER;
    f64             m_CPUMS             = 0;
    f64             m_IMS               = 0;

    //
    // State
    //
    eng_view        m_ActiveView;
    xbool           m_bBeginScene       = FALSE;
    xbool           m_bEngBegin         = FALSE;
    xcolor          m_ClearColor        = 0x0f0ff5ff;

    eng_draw*       m_Draw              = NULL;
    void*           m_pFontSys          = NULL;
    void*           m_pSpriteSys        = NULL;
    void*           m_pGeomSys          = NULL;

    x_qt_counter                            m_nRenderJobs;
    xsafe_array<const eng_renderjob*, 64>   m_lRenderJobs;
    
    eng_scrach_memory   m_ScrachMem;

    eng_context_internal()
    {
        m_nRenderJobs.setup(0);
    }
    
    void UpdateViewport( void )
    {
        const xirect& ViewPort = m_ActiveView.getViewport();
        glViewport( ViewPort.m_Top, ViewPort.m_Left, ViewPort.GetWidth(), ViewPort.GetHeight() );
    }

};

static eng_context*         s_pEng = NULL;
static xcritical_section    s_CritalSection;

///////
void _eng_DrawString( const char* pData );
void _eng_DrawString(const char *s, s32 x, s32 y, xcolor Color );
//-------------------------------------------------------------------------------
static
void OutputTextXY( s32 X, s32 Y, const char* pString )
{
    _eng_DrawString( pString, X, Y, xcolor(~0) );
}

//-------------------------------------------------------------------------------

void _eng_EntryPoint( s32 argc, char** argv )
{
    //
    // Set the printf function
    //
    x_SetFunctionPrintFXY( OutputTextXY );
}


//-------------------------------------------------------------------------------
s32 _eng_ExitPoint ( void )
{
    return 0;
}

//-------------------------------------------------------------------------------

eng_context& eng_CreateContext ( void )
{
    eng_context_internal* pContext = x_new(eng_context_internal, 1, XMEM_FLAG_ALIGN_16B);
    
#ifdef TARGET_NDS
    if( g_pDraw == NULL )
        g_pDraw  = &_eng_CreateDraw();
#endif
    
    return *pContext;
}

//-------------------------------------------------------------------------------
void eng_KillContext( eng_context& Context )
{
    Context.Kill();
    x_delete(&Context);
}

//-------------------------------------------------------------------------------

void eng_SetCurrentContext ( eng_context& Context )
{
    s_pEng = &Context;
}

//-------------------------------------------------------------------------------

eng_context& eng_GetCurrentContext ( void )
{
    return *s_pEng;
}

//-------------------------------------------------------------------------------

void eng_context::SetClearColor( xcolor C )

{
    eng_context_internal& This = *(eng_context_internal*)this;
    This.m_ClearColor = C;
    xvector4 Col = C.BuildRGBA();

    glClearColor( Col.m_X, Col.m_Y, Col.m_Z, Col.m_W );

}

//-------------------------------------------------------------------------------
void _eng_ResizeBuffers ( s32 Width, s32 Height )
{
    
    
}

//-------------------------------------------------------------------------------
u32 eng_context::GetCurrentFrameNumber( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    return (u32)This.m_FrameNumber;
}

//-------------------------------------------------------------------------------
xbool eng_context::HandleEvents ( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    
    xbool ret = _eng_HandleEvents();
    
    //
    // In certain system after we handle the events the screen resolution may have change
    // so we will be nice and update the view so the user can be lazy
    //
    s32 X, Y;
    GetScreenResolution( X, Y );
    This.m_ActiveView.setViewport( xirect(0,0,X,Y) );
    This.UpdateViewport();
    
    return ret;
}

//-------------------------------------------------------------------------------

void eng_context::PresetSetRes( s32 Width,  s32 Height )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    This.m_PreSetWidth = Width;
    This.m_PreSetHeight = Height;
}

//-------------------------------------------------------------------------------

void eng_context::PresetSetupScrachMemSize( s32 MemSize )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    This.m_ScrachMemSize = MemSize;
}

//-------------------------------------------------------------------------------

void eng_context::PresetSetMultisampling( u32 NumSamples, u32 Quality )
{
    
}

//-------------------------------------------------------------------------------
void eng_context::PresetWindowMode( xbool bFullScreen )
{
}

//-------------------------------------------------------------------------------
void eng_context::PresetWindowHandle( eng_hwin Handle )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    This.m_WindowHandle = Handle;
}

//-------------------------------------------------------------------------------
eng_hwin eng_context::getWindowHandle( void ) const
{
    eng_context_internal& This = *(eng_context_internal*)this;
    return This.m_WindowHandle;
}

//-------------------------------------------------------------------------------

static
void UpdateFPS( void )
{
    xtick CurrentTime;
    
    CurrentTime                                 = x_GetTime();
    eng_context_internal* pCurrent = (eng_context_internal*)s_pEng;
    pCurrent->m_FPSFrameTime[ pCurrent->m_FPSIndex ]    = CurrentTime - pCurrent->m_FPSLastTime;
    pCurrent->m_FPSLastTime                         = CurrentTime;
    pCurrent->m_FPSIndex                           += 1;
    pCurrent->m_FPSIndex                           &= 0x07;
}

//-------------------------------------------------------------------------------

f32 eng_context::GetFPS( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    xtick Sum = This.m_FPSFrameTime[0] +
                This.m_FPSFrameTime[1] +
                This.m_FPSFrameTime[2] +
                This.m_FPSFrameTime[3] +
                This.m_FPSFrameTime[4] +
                This.m_FPSFrameTime[5] +
                This.m_FPSFrameTime[6] +
                This.m_FPSFrameTime[7];
    
    return( (f32)(s32)(((8.0f / x_TicksToMs( Sum )) * 1000.0f) + 0.5f) );
}

//-------------------------------------------------------------------------------

void eng_context::Init( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;

    //
    // Initialize the fps
    //
    This.m_FPSFrameTime[0] = 0;
    This.m_FPSFrameTime[1] = 0;
    This.m_FPSFrameTime[2] = 0;
    This.m_FPSFrameTime[3] = 0;
    This.m_FPSFrameTime[4] = 0;
    This.m_FPSFrameTime[5] = 0;
    This.m_FPSFrameTime[6] = 0;
    This.m_FPSFrameTime[7] = 0;
    
    This.m_FrameNumber = 0;
    This.m_bPrintFPS   = TRUE;
    This.m_FPSLastTime = x_GetTime();
    This.m_FPSIndex    = 0;
    This.m_CPUMS       = 0;
    This.m_IMS         = 0;
    
    //
    // Scene state
    //
    This.m_bBeginScene = FALSE;
    This.m_bEngBegin   = FALSE;

    _eng_InitContext(this);

    This.m_Draw = &_eng_CreateDraw();

    This.m_pFontSys     = _eng_InitFontSystem( *This.m_Draw );
    This.m_pSpriteSys   = _eng_InitSpriteSystem();
    This.m_pGeomSys     = _eng_InitGeomSystem();

    This.m_ScrachMem.Initialize( This.m_ScrachMemSize );
}

//-------------------------------------------------------------------------------
void eng_context::Kill( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;

    // Deal with the font system
    if ( This.m_pGeomSys )
        _eng_KillGeomSystem( This.m_pGeomSys );
    This.m_pGeomSys = NULL;

    // Deal with the font system
    if ( This.m_pFontSys )
        _eng_KillFontSystem( This.m_pFontSys );
    This.m_pFontSys = NULL;

    // Deal with the sprite system
    if ( This.m_pSpriteSys )
        _eng_KillSpriteSystem( This.m_pSpriteSys );
    This.m_pSpriteSys = NULL;

    // Deal with draw
    _eng_KillDraw( *This.m_Draw );

    // Finally with the context
    _eng_KillContext( this );
}

//-------------------------------------------------------------------------------

eng_scrach_memory& eng_context::getScrachMem( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    return This.m_ScrachMem;
}


//-------------------------------------------------------------------------------

void eng_context::GetScreenResolution( s32& X, s32& Y )
{
    _eng_GetWindowSize( getWindowHandle(), &X, &Y );
}

//-------------------------------------------------------------------------------

void eng_context::SetActiveView( const eng_view& View )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    //
    // Set the new active viewport
    //
    This.m_ActiveView = View;
    
    //
    // Deal with the cliping region and the C2S matrix
    //
    This.UpdateViewport();
}

//-------------------------------------------------------------------------------

const eng_view& eng_context::GetActiveView( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    return This.m_ActiveView;
}

//-------------------------------------------------------------------------------

void eng_context::Begin( const char* pTaskName )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    ASSERT( This.m_bEngBegin == FALSE );

    if( This.m_bBeginScene == FALSE )
    {
        // Make sure that it is protected against other threads
        _eng_SetCurrentContext( s_pEng );
        s_CritalSection.BeginAtomic();
        
        // Set some defaults
        _eng_BindMainBuffer();
        
        // zbuffer on
        glDepthMask( TRUE );            // Write to the depth buffer
        glEnable( GL_DEPTH_TEST );      // Enable Z Compare
        glDepthFunc( GL_LEQUAL );       // <=
        
        // cull faces
        glCullFace(GL_BACK);
        
        // Clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );	// Clear Screen And Depth Buffer

        
        // Okay we are in
        This.m_bBeginScene = TRUE;
    }
    
    This.m_bEngBegin   = TRUE;
}

//-------------------------------------------------------------------------------

void eng_context::End( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    ASSERT( This.m_bBeginScene );
    This.m_bBeginScene = FALSE;
}

//-------------------------------------------------------------------------------

void eng_context::qtAddRenderJob( const eng_renderjob& Job )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    
    // Insert job in to our buffer
    const s32 lCount = This.m_nRenderJobs.Inc() - 1;
    This.m_lRenderJobs[ lCount ] = &Job;
}

//-------------------------------------------------------------------------------

void eng_context_internal::HandleRenderJobs( void )
{
    const s32 nJobs = m_nRenderJobs.get();
    
    //
    // Short all the jobs pointers
    //
    x_qsort( &m_lRenderJobs[0],                 // Address of first item in array.
              nJobs,                            // Number of items in array.
              sizeof(m_lRenderJobs[0]),         // Size of one item.
            [](const void* paA, const void* paB)->s32
            {
                const eng_renderjob* pA = *(const eng_renderjob**) paA;
                const eng_renderjob* pB = *(const eng_renderjob**) paB;
                
                if( pA->m_Order < pB->m_Order ) return -1;
                return pA->m_Order > pB->m_Order;
            });
    
    //
    // Render all of it in order
    //
    for( s32 i=0; i<nJobs; i++ )
    {
        const eng_renderjob& Job = *m_lRenderJobs[i];
        Job.onRunRenderJob();
    }
    
    // Clear the link list
    m_nRenderJobs.setup(0);
}

//-------------------------------------------------------------------------------

void eng_context::PageFlip( xbool WaitSync )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    
    //
    // Handle running all the jobs
    //
    if( This.m_nRenderJobs.get() )
    {
        This.HandleRenderJobs();
    }
    
    //
    // Handle the CPU timer
    //
    This.m_CPUTIMER.Stop();
    This.m_CPUMS = This.m_CPUTIMER.ReadMs();
    
    xtimer InternalTime;
    InternalTime.Start();
    
    //
    // Handle the FPS
    //
    if( This.m_bPrintFPS )
    {
        s32 X,Y;
        _eng_GetWindowSize( getWindowHandle(), &X, &Y );
        
        if(X < 256)
        {
            // if the screen res is really small (as in GI preprocessing), only display FPS
            x_printfxy( 0, 0, "FPS:%6.1f", GetFPS() );
        }
        else
        {
            x_printfxy( 0, 0, "CPU:%5.1f  Pageflip:%5.1f  FPS:%6.1f",
                       This.m_CPUMS,
                       This.m_IMS,
                       GetFPS() );
        }
    }
    
    //
    // Let draw deal with the rendering
    //
    getDraw().PageFlip();
    g_RscMgr.PageFlip();
    This.m_ScrachMem.PageFlip();
    glFinish();
    
    //
    // Do actual page flip
    //
    _eng_PageFlip( this, WaitSync );
    
    This.m_bEngBegin   = FALSE;
    s_CritalSection.EndAtomic();
    
    //
    // Update stats
    //
    UpdateFPS();
    
    InternalTime.Stop();
    This.m_IMS = InternalTime.ReadMs();
    This.m_CPUTIMER.Reset();
    This.m_CPUTIMER.Start();
    
    //
    // increment frame number
    //
    This.m_FrameNumber++;
    
}

//-------------------------------------------------------------------------------

eng_draw& eng_context::getDraw( void )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    return *This.m_Draw;
}

//-------------------------------------------------------------------------------
void eng_context::SetFPSDisplay( xbool FPS_On )
{
    eng_context_internal& This = *(eng_context_internal*)this;
    This.m_bPrintFPS = FPS_On;
}

//-------------------------------------------------------------------------------
void eng_Init( void )
{
    g_RscMgr.Initialize();
    
    _eng_Init();
}

//-------------------------------------------------------------------------------
void eng_Kill( void )
{
    _eng_Kill();
}

//-------------------------------------------------------------------------------

xcritical_section& eng_getCriticalSection( void )
{
    return s_CritalSection;
}

//-------------------------------------------------------------------------------

void eng_SetActiveThreadContext( void )
{
    _eng_SetCurrentContext( s_pEng );
}

//-------------------------------------------------------------------------------
eng_hwin eng_CreateWindow( s32 X, s32 Y, s32 Width, s32 Height )
{
    return _eng_CreateWindow(X, Y, Width, Height);
}

//-------------------------------------------------------------------------------
void eng_KillWindow( eng_hwin Handle )
{
    return _eng_KillWindow(Handle);
}
