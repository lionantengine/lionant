#include <Windows.h>
#include "eng_Base.h"

#pragma comment (lib, "opengl32.lib")
//#pragma comment (lib, "glew32s.lib")

extern "C"
{
    //////////////////////////////////////////////////////////////////////////////////////////
    // TYPES
    //////////////////////////////////////////////////////////////////////////////////////////

    struct gl_window                                    // Contains Information Vital To A Window
    {
        HWND				Window;						// Window Handle
        HDC					DeviceContext;				// Device Context
        HGLRC				RenderContext;				// Rendering Context

        gl_window()
        {
            Window          = NULL;
            DeviceContext   = NULL;
            RenderContext   = NULL;
        }
    };

    struct keyboard_pc
    {
        int m_KeyIsDown[eng_keyboard::KEY_END_ENUMERATION];
        int m_KeyWasDown[2][eng_keyboard::KEY_END_ENUMERATION];
        int m_KeyWasDownIndex;
    };

    struct mouse_pc
    {
        s32                 m_ButtonIndex;
        s32                 m_ButtonIsDown[eng_mouse::BTN_END_ENUMERATION];
        s32                 m_ButtonWasDown[2][eng_mouse::BTN_END_ENUMERATION];
        POINT               m_Analog[eng_mouse::ANALOG_END_ENUMERATION];
    };

    //////////////////////////////////////////////////////////////////////////////////////////
    // VARIABLES
    //////////////////////////////////////////////////////////////////////////////////////////

    static HINSTANCE    g_ApplicationInstance   = NULL;
    static const char*  g_WindowClassName       = "LIONant Engine Framework";
    static gl_window*   g_DummyWindow           = NULL;
    static gl_window*   g_OffscreenWindow       = NULL;
    static mouse_pc     g_Mouse                 = {0};
    static keyboard_pc  g_Keyboard              = {0};
  
    //////////////////////////////////////////////////////////////////////////////////////////
    // FUNCTIONS
    //////////////////////////////////////////////////////////////////////////////////////////
    //
    // Mapping from unicode to actual engine key
    //
    static const int s_NormalKeyCode[256]=
    {
        0,                      // 0
        0,                      // 1
        0,                      // 2
        0,                      // 3
        0,                      // 4
        0,                      // 5
        0,                      // 6
        0,                      // 7
        0,                      // 8
        eng_keyboard::KEY_TAB,  // 9
        0,                      // 10
        0,                      // 11
        0,                      // 12
        eng_keyboard::KEY_RETURN,// 13
        0,                      // 14
        0,                      // 15
        0,                      // 16
        0,                      // 17
        0,                      // 18
        0,                      // 19
        0,                      // 20
        0,                      // 21
        0,                      // 22
        0,                      // 23
        0,                      // 24
        0,                      // 25
        0,                      // 26
        eng_keyboard::KEY_ESCAPE,// 27
        0,                      // 28
        0,                      // 29
        0,                      // 30
        0,                      // 31
        eng_keyboard::KEY_SPACE,// 32
        0,                      // 33
        0,                      // 34
        eng_keyboard::KEY_END, // 35
        eng_keyboard::KEY_HOME,  // 36
        eng_keyboard::KEY_LEFT, // 37
        eng_keyboard::KEY_UP,   // 38
        eng_keyboard::KEY_RIGHT,// 39
        eng_keyboard::KEY_DOWN, // 40
        0,                      // 41
        0,                      // 42
        eng_keyboard::KEY_PLUS,               // 43
        eng_keyboard::KEY_COMMA,              // 44
        eng_keyboard::KEY_INSERT,              // 45
        eng_keyboard::KEY_DELETE,             // 46
        eng_keyboard::KEY_SLASH,              // 47
        eng_keyboard::KEY_0,                  // 48
        eng_keyboard::KEY_1,                  // 49
        eng_keyboard::KEY_2,                  // 50
        eng_keyboard::KEY_3,                  // 51
        eng_keyboard::KEY_4,                  // 52
        eng_keyboard::KEY_5,                  // 53
        eng_keyboard::KEY_6,                  // 54
        eng_keyboard::KEY_7,                  // 55
        eng_keyboard::KEY_8,                  // 56
        eng_keyboard::KEY_9,                  // 57
        eng_keyboard::KEY_SEMICOLON,          // 58
        0,                      // 59
        0,                      // 60
        0,                      // 61
        0,                      // 62
        0,                      // 63
        0,                      // 64
        eng_keyboard::KEY_A,                  // 65
        eng_keyboard::KEY_B,                  // 66
        eng_keyboard::KEY_C,                  // 67
        eng_keyboard::KEY_D,                  // 68
        eng_keyboard::KEY_E,                  // 69
        eng_keyboard::KEY_F,                  // 70
        eng_keyboard::KEY_G,                  // 71
        eng_keyboard::KEY_H,                  // 72
        eng_keyboard::KEY_I,                  // 73
        eng_keyboard::KEY_J,                  // 74
        eng_keyboard::KEY_K,                  // 75
        eng_keyboard::KEY_L,                  // 76
        eng_keyboard::KEY_M,                  // 77
        eng_keyboard::KEY_N,                  // 78
        eng_keyboard::KEY_O,                  // 79
        eng_keyboard::KEY_P,                  // 80
        eng_keyboard::KEY_Q,                  // 81
        eng_keyboard::KEY_R,                  // 82
        eng_keyboard::KEY_S,                  // 83
        eng_keyboard::KEY_T,                  // 84
        eng_keyboard::KEY_U,                  // 85
        eng_keyboard::KEY_V,                  // 86
        eng_keyboard::KEY_W,                  // 87
        eng_keyboard::KEY_X,                  // 88
        eng_keyboard::KEY_Y,                  // 89
        eng_keyboard::KEY_Z,                  // 90
        eng_keyboard::KEY_LBRACKET,           // 91
        eng_keyboard::KEY_BACKSLASH,          // 92
        eng_keyboard::KEY_RBRACKET,           // 93
        0,                      // 94
        0,                      // 95
        eng_keyboard::KEY_0,                      // 96
        eng_keyboard::KEY_1,                  // 97
        eng_keyboard::KEY_2,                  // 98
        eng_keyboard::KEY_3,                  // 99
        eng_keyboard::KEY_4,                  // 100
        eng_keyboard::KEY_5,                  // 101
        eng_keyboard::KEY_6,                  // 102
        eng_keyboard::KEY_7,                  // 103
        eng_keyboard::KEY_8,                  // 104
        eng_keyboard::KEY_9,                  // 105
        eng_keyboard::KEY_MULTIPLY,           // 106
        eng_keyboard::KEY_PLUS,               // 107
        -1,                                   // 108
        eng_keyboard::KEY_MINUS,              // 109
        eng_keyboard::KEY_PERIOD,             // 110
        eng_keyboard::KEY_SLASH,              // 111
        eng_keyboard::KEY_P,                  // 112
        eng_keyboard::KEY_Q,                  // 113
        eng_keyboard::KEY_R,                  // 114
        eng_keyboard::KEY_S,                  // 115
        eng_keyboard::KEY_T,                  // 116
        eng_keyboard::KEY_U,                  // 117
        eng_keyboard::KEY_V,                  // 118
        eng_keyboard::KEY_W,                  // 119
        eng_keyboard::KEY_X,                  // 120
        eng_keyboard::KEY_Y,                  // 121
        eng_keyboard::KEY_Z,                  // 122
        eng_keyboard::KEY_LBRACKET,           // 123
        0,                      // 124
        eng_keyboard::KEY_RBRACKET,           // 125
        eng_keyboard::KEY_TILDE,              // 126
        eng_keyboard::KEY_BACKSPACE,          // 127
        0,                      // 128
        0,                      // 129
        0,                      // 130
        0,                      // 131
        0,                      // 132
        0,                      // 133
        0,                      // 134
        0,                      // 135
        0,                      // 136
        0,                      // 137
        0,                      // 138
        0,                      // 139
        0,                      // 140
        0,                      // 141
        0,                      // 142
        0,                      // 143
        0,                      // 144
        0,                      // 145
        0,                      // 146
        0,                      // 147
        0,                      // 148
        0,                      // 149
        0,                      // 150
        0,                      // 151
        0,                      // 152
        0,                      // 153
        0,                      // 154
        0,                      // 155
        0,                      // 156
        0,                      // 157
        0,                      // 158
        0,                      // 159
        0,                      // 160
        0,                      // 161
        0,                      // 162
        0,                      // 163
        0,                      // 164
        0,                      // 165
        0,                      // 166
        0,                      // 167
        0,                      // 168
        0,                      // 169
        0,                      // 170
        0,                      // 171
        0,                      // 172
        0,                      // 173
        0,                      // 174
        0,                      // 175
        0,                      // 176
        0,                      // 177
        0,                      // 178
        0,                      // 179
        0,                      // 180
        0,                      // 181
        0,                      // 182
        0,                      // 183
        0,                      // 184
        0,                      // 185
        0,                      // 186
        0,                      // 187
        0,                      // 188
        eng_keyboard::KEY_COMMA,// 189
        eng_keyboard::KEY_PERIOD,// 190
        eng_keyboard::KEY_SLASH,// 191
        eng_keyboard::KEY_TILDE,// 192
        0,                      // 193
        0,                      // 194
        0,                      // 195
        0,                      // 196
        0,                      // 197
        0,                      // 198
        0,                      // 199
        0,                      // 200
        0,                      // 201
        0,                      // 202
        0,                      // 203
        0,                      // 204
        0,                      // 205
        0,                      // 206
        0,                      // 207
        0,                      // 208
        0,                      // 209
        0,                      // 210
        0,                      // 211
        0,                      // 212
        0,                      // 213
        0,                      // 214
        0,                      // 215
        0,                      // 216
        0,                      // 217
        0,                      // 218
        0,                      // 219
        0,                      // 220
        0,                      // 221
        0,                      // 222
        0,                      // 223
        0,                      // 224
        0,                      // 225
        0,                      // 226
        0,                      // 227
        0,                      // 228
        0,                      // 229
        0,                      // 230
        0,                      // 231
        0,                      // 232
        0,                      // 233
        0,                      // 234
        0,                      // 235
        0,                      // 236
        0,                      // 237
        0,                      // 238
        0,                      // 239
        0,                      // 240
        0,                      // 241
        0,                      // 242
        0,                      // 243
        0,                      // 244
        0,                      // 245
        0,                      // 246
        0,                      // 247
        0,                      // 248
        0,                      // 249
        0,                      // 250
        0,                      // 251
        0,                      // 252
        0,                      // 253
        0,                      // 254
        0                       // 255
    };

    //----------------------------------------------------------------------------------
    s32 PCKeyToKeyCode(s32 Key)
    {
        return s_NormalKeyCode[Key];
    }

    // Process Window Message Callbacks
    LRESULT CALLBACK WindowProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
    {
        switch(uMsg)
        {
        case WM_CLOSE:
            PostMessage (hWnd, WM_QUIT, 0, 0);
            return 0;
        case WM_MOUSEMOVE:
            {
                s32 x = (int)(lParam & 0xffff);
                s32 y = (int)(lParam >> 16);
            
                g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].x = x - g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].x;
                g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].y = y - g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].y;

                g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].x = x;
                g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].y = y;

                g_Mouse.m_Analog[eng_mouse::ANALOG_WHEEL_REL].x = 0;
                g_Mouse.m_Analog[eng_mouse::ANALOG_WHEEL_REL].y = 0;
                break;
            }
        case WM_MBUTTONDOWN:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_MIDDLE] = 1;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].x = (int)(lParam & 0xffff);
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].y = (int)(lParam >> 16);

            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].x = 0;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].y = 0;
            break;
        case WM_MBUTTONUP:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_MIDDLE] = 0;
            g_Mouse.m_ButtonWasDown[g_Mouse.m_ButtonIndex][eng_mouse::BTN_MIDDLE] = 1;
            break;
        case WM_LBUTTONDOWN:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_LEFT] = 1;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].x = (int)(lParam & 0xffff);
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].y = (int)(lParam >> 16);

            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].x = 0;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].y = 0;
            break;
        case WM_LBUTTONUP:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_LEFT] = 0;
            g_Mouse.m_ButtonWasDown[g_Mouse.m_ButtonIndex][eng_mouse::BTN_LEFT] = 1;
            break;
        case WM_RBUTTONDOWN:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_RIGHT] = 1;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].x = (int)(lParam & 0xffff);
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_ABS].y = (int)(lParam >> 16);

            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].x = 0;
            g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].y = 0;
            break;
        case WM_RBUTTONUP:
            g_Mouse.m_ButtonIsDown[eng_mouse::BTN_RIGHT] = 0;
            g_Mouse.m_ButtonWasDown[g_Mouse.m_ButtonIndex][eng_mouse::BTN_RIGHT] = 1;
            break;
        case WM_KEYDOWN:
            {
                s32 Code = PCKeyToKeyCode( (s32)wParam );
                if ( Code >= 0 && Code < eng_keyboard::KEY_END_ENUMERATION )
                {
                    g_Keyboard.m_KeyIsDown[Code] = 1;
                }
                break;
            }
        case WM_KEYUP:
            {
                s32 Code = PCKeyToKeyCode( (s32)wParam );
                if ( Code >= 0 && Code < eng_keyboard::KEY_END_ENUMERATION )
                {
                    g_Keyboard.m_KeyIsDown[Code] = 0;
                    g_Keyboard.m_KeyWasDown[g_Keyboard.m_KeyWasDownIndex][Code] = 1;
                }
                break;
            }
        }
        return DefWindowProc (hWnd, uMsg, wParam, lParam);					// Pass Unhandled Messages To DefWindowProc
    }

    //----------------------------------------------------------------------------------

    static BOOL _eng_RegisterWindowClass (HINSTANCE Instance, const char* ClassName)
    {																		// TRUE If Successful
        // Register A Window Class
        WNDCLASSEX windowClass;												// Window Class
        ZeroMemory (&windowClass, sizeof (WNDCLASSEX));						// Make Sure Memory Is Cleared
        windowClass.cbSize			= sizeof (WNDCLASSEX);					// Size Of The windowClass Structure
        windowClass.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraws The Window For Any Movement / Resizing
        windowClass.lpfnWndProc		= (WNDPROC)(WindowProc);				// WindowProc Handles Messages
        windowClass.hInstance		= Instance;				                // Set The Instance
        windowClass.hbrBackground	= (HBRUSH)(COLOR_APPWORKSPACE);			// Class Background Brush Color
        windowClass.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
        windowClass.lpszClassName	= ClassName;				            // Sets The Applications Classname
        windowClass.cbWndExtra      = 10;
        if (RegisterClassEx (&windowClass) == 0)							// Did Registering The Class Fail?
        {
            // NOTE: Failure, Should Never Happen
            MessageBox (HWND_DESKTOP, "RegisterClassEx Failed!", "Error", MB_OK | MB_ICONEXCLAMATION);
            return FALSE;													// Return False (Failure)
        }
        return TRUE;														// Return True (Success)
    }

    //----------------------------------------------------------------------------------

    xbool _eng_InitOpenGL( HWND Window, gl_window* WindowInfo );

    HWND _eng_CreateWindow( s32 X, s32 Y, s32 Width, s32 Height )	// This Code Creates Our OpenGL Window
    {
        //DWORD windowStyle = WS_OVERLAPPEDWINDOW;							// Define Our Window Style
        DWORD windowStyle = WS_POPUP|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_VISIBLE;
        DWORD windowExtendedStyle = WS_EX_APPWINDOW;						// Define The Window's Extended Style

        int wid=GetSystemMetrics(SM_CXSCREEN);  
        int hei=GetSystemMetrics(SM_CYSCREEN);  
        int winx = ( wid - Width ) / 2;
        int winy = ( hei - (Height+30) ) / 2;

        RECT windowRect = {0, 0, Width, Height};	// Define Our Window Coordinates

        //if (window->init.isFullScreen == TRUE)								// Fullscreen Requested, Try Changing Video Modes
        //{
        //    if (ChangeScreenResolution (window->init.width, window->init.height, window->init.bitsPerPixel) == FALSE)
        //    {
        //        // Fullscreen Mode Failed.  Run In Windowed Mode Instead
        //        MessageBox (HWND_DESKTOP, "Mode Switch Failed.\nRunning In Windowed Mode.", "Error", MB_OK | MB_ICONEXCLAMATION);
        //        window->init.isFullScreen = FALSE;							// Set isFullscreen To False (Windowed Mode)
        //    }
        //    else															// Otherwise (If Fullscreen Mode Was Successful)
        //    {
        //        ShowCursor (FALSE);											// Turn Off The Cursor
        //        windowStyle = WS_POPUP;										// Set The WindowStyle To WS_POPUP (Popup Window)
        //        windowExtendedStyle |= WS_EX_TOPMOST;						// Set The Extended Window Style To WS_EX_TOPMOST
        //    }																// (Top Window Covering Everything Else)
        //}
        //else																// If Fullscreen Was Not Selected
        {
            // Adjust Window, Account For Window Borders
            AdjustWindowRectEx (&windowRect, windowStyle, 0, windowExtendedStyle);
        }

        // Create The OpenGL Window
        HWND Window = CreateWindowEx (windowExtendedStyle,			        // Extended Style
            g_WindowClassName,	                                            // Class Name
            "LIONant Engine",					                            // Window Title
            windowStyle,							                        // Window Style
            winx, winy,								                        // Window X,Y Position
            windowRect.right - windowRect.left,	                            // Window Width
            windowRect.bottom - windowRect.top,	                            // Window Height
            HWND_DESKTOP,						                            // Desktop Is Window's Parent
            0,									                            // No Menu
            g_ApplicationInstance,                                          // Pass The Window Instance
            NULL);

        if ( NULL == Window )										        // Was Window Creation A Success?
        {
            return NULL;													// If Not Return False
        }

        ShowWindow (Window, SW_NORMAL);							            // Make The Window Visible
        SetForegroundWindow( Window );
        SetFocus( Window );

        return Window;											            // Window Creating Was A Success
        // Initialization Will Be Done In WM_CREATE
    }

    HWND _eng_CreateDummyWindow( void )	                                    // This Code Creates Our OpenGL Window
    {
        //DWORD windowStyle = WS_OVERLAPPEDWINDOW;							// Define Our Window Style
        DWORD windowStyle = WS_POPUP|WS_CAPTION;
        DWORD windowExtendedStyle = WS_EX_APPWINDOW;						// Define The Window's Extended Style

        int wid=GetSystemMetrics(SM_CXSCREEN);  
        int hei=GetSystemMetrics(SM_CYSCREEN);  
        int winx=(64)/2;
        int winy=(64)/2; 

        RECT windowRect = {0, 0, 64, 64};
        AdjustWindowRectEx (&windowRect, windowStyle, 0, windowExtendedStyle);

        // Create The OpenGL Window
        HWND Window = CreateWindowEx (windowExtendedStyle,			        // Extended Style
            g_WindowClassName,	                                            // Class Name
            "LIONant Engine",					                            // Window Title
            windowStyle,							                        // Window Style
            winx, winy,								                        // Window X,Y Position
            windowRect.right - windowRect.left,	                            // Window Width
            windowRect.bottom - windowRect.top,	                            // Window Height
            HWND_DESKTOP,						                            // Desktop Is Window's Parent
            0,									                            // No Menu
            g_ApplicationInstance,                                          // Pass The Window Instance
            NULL);

        if ( NULL == Window )										        // Was Window Creation A Success?
        {
            return NULL;													// If Not Return False
        }
        return Window;											            // Window Creating Was A Success
        // Initialization Will Be Done In WM_CREATE
    }

    //---------------------------------------------------------------------------------------

    void _eng_KillWindow( eng_hwin Handle )
    {
    }

    //----------------------------------------------------------------------------------

    xbool _eng_HandleEvents( void )
    {
        xbool Continue = TRUE;

        g_Mouse.m_ButtonIndex = (g_Mouse.m_ButtonIndex + 1) % 2;
        for ( s32 i = 0; i < eng_mouse::BTN_END_ENUMERATION; i++ )
        {
            g_Mouse.m_ButtonWasDown[g_Mouse.m_ButtonIndex][i] = 0;
        }

        g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].x = 0;
        g_Mouse.m_Analog[eng_mouse::ANALOG_POS_REL].y = 0;
        g_Mouse.m_Analog[eng_mouse::ANALOG_WHEEL_REL].x = 0;
        g_Mouse.m_Analog[eng_mouse::ANALOG_WHEEL_REL].y = 0;

        g_Keyboard.m_KeyWasDownIndex = 1 - g_Keyboard.m_KeyWasDownIndex;
        for ( s32 i = 0; i < eng_keyboard::KEY_END_ENUMERATION; i++ )
        {
            g_Keyboard.m_KeyWasDown[g_Keyboard.m_KeyWasDownIndex][i] = 0;
        }

        MSG Message;
        eng_context& pContext = eng_GetCurrentContext();
        if ( PeekMessage(&Message, pContext.getWindowHandle(), 0, 0, PM_REMOVE) )
        {
            if ( WM_QUIT == Message.message )
            {
                Continue = FALSE;
            }
            DispatchMessage(&Message);
        }
        return Continue;
    }

    //----------------------------------------------------------------------------------

    void _eng_GetWindowSize   ( eng_hwin Handle, int* wid, int* hei )
    {
        RECT Rect;
        GetClientRect(Handle, &Rect);
        *wid = Rect.right - Rect.left;
        *hei = Rect.bottom - Rect.top;
    }

    //---------------------------------------------------------------------------------------

    int _eng_ThouchPadIsButtons( int KeyCode )
    {
        return 0;
    }

    //---------------------------------------------------------------------------------------

    int _eng_ThouchPadWasButtons( int KeyCode )
    {
        return 0;
    }

    //---------------------------------------------------------------------------------------

    void _eng_ThouchPadValue( int KeyCode, float* X, float* Y )
    {
        *X = 0;
        *Y = 0;
    }

    void _eng_PreferedPixelFormat(PIXELFORMATDESCRIPTOR& Format)
    {
        Format.nSize            = sizeof (PIXELFORMATDESCRIPTOR);
        Format.nVersion         = 1;
        Format.dwFlags          = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        Format.iPixelType       = PFD_TYPE_RGBA;
        Format.cColorBits       = 32;
        Format.cRedBits         = 0;
        Format.cRedShift        = 0;
        Format.cGreenBits       = 0;
        Format.cGreenShift      = 0;
        Format.cBlueBits        = 0;
        Format.cBlueShift       = 0;
        Format.cAlphaBits       = 0;
        Format.cAlphaShift      = 0;
        Format.cAccumBits       = 0;
        Format.cAccumRedBits    = 0;
        Format.cAccumGreenBits  = 0;
        Format.cAccumBlueBits   = 0;
        Format.cAccumAlphaBits  = 0;
        Format.cDepthBits       = 16;
        Format.cStencilBits     = 0;
        Format.cAuxBuffers      = 0;
        Format.iLayerType       = PFD_MAIN_PLANE;
        Format.bReserved        = 0;
        Format.dwLayerMask      = 0;
        Format.dwVisibleMask    = 0;
        Format.dwDamageMask     = 0;
    }

    //---------------------------------------------------------------------------------------

    xbool _eng_InitOpenGL( HWND Window, gl_window* WindowInfo )
    {
        WindowInfo->Window = Window;
        WindowInfo->DeviceContext = GetDC(Window);
        if (WindowInfo->DeviceContext == 0)
        {
            ASSERTS(FALSE, "Failed to get device context from window!");
            delete WindowInfo;
            return FALSE;
        }

        PIXELFORMATDESCRIPTOR PixelFormatDes;
        _eng_PreferedPixelFormat(PixelFormatDes);

        GLuint PixelFormat = ChoosePixelFormat (WindowInfo->DeviceContext, &PixelFormatDes);
        if (PixelFormat == 0)												// Did We Find A Compatible Format?
        {
            // Failed
            ReleaseDC (WindowInfo->Window, WindowInfo->DeviceContext);		// Release Our Device Context
            delete WindowInfo;
            ASSERTS(FALSE, "Failed to choose pixel format on window device context!");
            return FALSE;
        }

        if (SetPixelFormat (WindowInfo->DeviceContext, PixelFormat, &PixelFormatDes) == FALSE)		// Try To Set The Pixel Format
        {
            // Failed
            ReleaseDC (WindowInfo->Window, WindowInfo->DeviceContext);		// Release Our Device Context
            delete WindowInfo;
            ASSERTS(FALSE, "Failed to set pixel format on device context!");
            return FALSE;
        }

        WindowInfo->RenderContext = wglCreateContext (WindowInfo->DeviceContext);
        if (WindowInfo->RenderContext == 0)									// Did We Get A Rendering Context?
        {
            // Failed
            ReleaseDC (WindowInfo->Window, WindowInfo->DeviceContext);		// Release Our Device Context
            delete WindowInfo;
            ASSERTS(FALSE, "Failed to create rendering context!");
            return FALSE;
        }

        // Make The Rendering Context Our Current Rendering Context
        if (wglMakeCurrent (WindowInfo->DeviceContext, WindowInfo->RenderContext) == FALSE)
        {
            // Failed
            wglDeleteContext (WindowInfo->RenderContext);					// Delete The Rendering Context
            ReleaseDC (WindowInfo->Window, WindowInfo->DeviceContext);		// Release Our Device Context
            delete WindowInfo;
            ASSERTS(FALSE, "Failed to set current rendering context!");
            return FALSE;
        }

        return TRUE;
    }

    //---------------------------------------------------------------------------------------

    void _eng_Init( void )
    {
        g_OffscreenWindow = new gl_window();
        g_OffscreenWindow->Window = _eng_CreateDummyWindow();

        if ( !_eng_InitOpenGL(g_OffscreenWindow->Window, g_OffscreenWindow) )
        {
            ASSERTS(false, "Failed to initialize OpenGL!");
        }

        GLenum Error = glewInit();
        if ( GLEW_OK != Error )
        {
            ASSERTS(false, "Initialize GLEW failed!");
        }

        const GLubyte *oglVersion = glGetString(GL_VERSION);
    }

    //---------------------------------------------------------------------------------------

    void _eng_Kill( void )
    {
        if ( NULL != g_OffscreenWindow )
        {
            if ( NULL != g_OffscreenWindow->DeviceContext )
            {
                wglMakeCurrent(g_OffscreenWindow->DeviceContext, NULL);

                if ( NULL != g_OffscreenWindow->RenderContext )
                {
                    wglDeleteContext (g_OffscreenWindow->RenderContext);
                    g_OffscreenWindow->RenderContext = NULL;
                }

                ReleaseDC(NULL, g_OffscreenWindow->DeviceContext);
                g_OffscreenWindow->DeviceContext = NULL;
            }
            delete g_OffscreenWindow;
            g_OffscreenWindow = NULL;
        }
    }

    //---------------------------------------------------------------------------------------

    void _eng_InitContext (  eng_context* Context )
    {
        ASSERT( NULL != Context );
        eng_hwin Window = Context->getWindowHandle();
        ASSERT( NULL != Window );

        gl_window* pWindowInfo = new gl_window();
        
        if ( !_eng_InitOpenGL(Window, pWindowInfo) )
        {
            return;
        }

        SetWindowLongPtr( pWindowInfo->Window, GWLP_USERDATA + 0, (LONG_PTR)( pWindowInfo ) );
    }

    //---------------------------------------------------------------------------------------

    void _eng_KillContext ( eng_context* Context )
    {
        ASSERT( NULL != Context );
        eng_hwin Window = Context->getWindowHandle();
        ASSERT( NULL != Window );

        gl_window* WindowInfo = (gl_window*)GetWindowLongPtr( Window, GWLP_USERDATA + 0 );
        if ( NULL != WindowInfo )
        {
            if ( NULL != WindowInfo->DeviceContext )
            {
                wglMakeCurrent(WindowInfo->DeviceContext, NULL);

                if ( NULL != WindowInfo->RenderContext )
                {
                    wglDeleteContext(WindowInfo->RenderContext);
                    WindowInfo->RenderContext = NULL;
                }

                ReleaseDC(WindowInfo->Window, WindowInfo->DeviceContext);
                SetWindowLongPtr( WindowInfo->Window, GWLP_USERDATA + 0, 0 );
                delete WindowInfo;
            }
        }
    }

    //---------------------------------------------------------------------------------------

    void _eng_PageFlip( eng_context* Context, xbool WaitSync )
    {
        eng_hwin Window = Context->getWindowHandle();
        ASSERT( NULL != Window );
        gl_window* WindowInfo = (gl_window*)GetWindowLongPtr( Window, GWLP_USERDATA + 0 );
        ASSERT( NULL != WindowInfo );
        if ( NULL != WindowInfo && NULL != WindowInfo->DeviceContext )
        {
            SwapBuffers(WindowInfo->DeviceContext);
        }
    }

    //----------------------------------------------------------------------------------

    void _eng_SetCurrentContext( eng_context* Context )
    {
        eng_hwin Window = Context->getWindowHandle();
        ASSERT( NULL != Window );
        gl_window* WindowInfo = (gl_window*)GetWindowLongPtr( Window, GWLP_USERDATA + 0 );
        ASSERT( NULL != WindowInfo );
        wglMakeCurrent(WindowInfo->DeviceContext, WindowInfo->RenderContext);
    }

    //----------------------------------------------------------------------------------

    void _eng_BindMainBuffer( void )
    {
    }







    //----------------------------------------------------------------------------------------

    int _eng_IsKeyDown( int KeyCode )
    {
        return g_Keyboard.m_KeyIsDown[KeyCode];
    }

    //----------------------------------------------------------------------------------------

    int _eng_WasKeyDown( int KeyCode )
    {
        return g_Keyboard.m_KeyWasDown[1 - g_Keyboard.m_KeyWasDownIndex][KeyCode];
    }

    //----------------------------------------------------------------------------------------

    const int* _eng_MouseGetIsButtons( void )
    {
        return g_Mouse.m_ButtonIsDown;
    }

    //----------------------------------------------------------------------------------------

    const int* _eng_MouseGetWasButtons( void )
    {
        return g_Mouse.m_ButtonWasDown[g_Mouse.m_ButtonIndex];
    }

    //----------------------------------------------------------------------------------------

    void _eng_MouseGetAnalog( int KeyCode, float* X, float* Y )
    {
        *X = (float)g_Mouse.m_Analog[KeyCode].x;
        *Y = (float)g_Mouse.m_Analog[KeyCode].y;
    }

    //----------------------------------------------------------------------------------------

    int _eng_DrawInCorrectContext( void )
    {
        return 1;
    }

    //----------------------------------------------------------------------------------------
    int _eng_IsButtonDown(int KeyCode )
    {
        return 0;
    }

    //----------------------------------------------------------------------------------------
    int _eng_WasButtonDown(int KeyCode )
    {
        return 0;
    }

    //----------------------------------------------------------------------------------------
    void _eng_GamePadValue( int KeyCode, float* X, float* Y )
    {
        *X = 0;
        *Y = 0;
    }
}

//----------------------------------------------------------------------------------------
extern void _eng_EntryPoint( s32 argc, char** argv );
void _eng_EntryPoint( s32& argc, char**& argv, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    g_ApplicationInstance = hInstance;
    _eng_RegisterWindowClass(hInstance, g_WindowClassName);

    _eng_EntryPoint(argc, argv);
}