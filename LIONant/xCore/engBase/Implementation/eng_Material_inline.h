//
//  eng_Material_inline.h
//  engBase
//
//  Created by Tomas Arce on 3/7/15.
//  Copyright (c) 2015 Tomas Arce. All rights reserved.
//

#ifndef eng_Material_inline_h
#define eng_Material_inline_h

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_material_rsc - saving functions
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
inline
void eng_shader_rsc::uniform_type::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof( *this ) == SIZE_UNIFORM_TYPE );
    SerialFile.Serialize    ( m_pName, x_strlen( m_pName.m_Ptr ) + 1 );
    SerialFile.SerializeEnum( m_Type );
}

//---------------------------------------------------------------------------
inline
eng_shader_rsc::shader_rsc::shader_rsc( xserialfile& SerialFile )
{
     ASSERT( SerialFile.GetResourceVersion( ) == VERSION );
}

//---------------------------------------------------------------------------
inline
void eng_shader_rsc::shader_rsc::SerializeIO( xserialfile& SerialFile ) const
{
    SerialFile.SetResourceVersion( VERSION );
    ASSERTCT( sizeof( *this ) == SIZE_SHADER_RSC );

    SerialFile.Serialize( m_pShader, x_strlen(m_pShader.m_Ptr) + 1, xserialfile::FLAGS_TEMP_MEMORY );

    SerialFile.Serialize( m_nAttributes );
    SerialFile.Serialize( m_pAttributeLink, m_nAttributes );

    SerialFile.Serialize( m_nUniforms );
    SerialFile.Serialize( m_pUniform, m_nUniforms );

    SerialFile.SerializeEnum( m_ShaderType );
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_material_rsc - saving functions
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
inline
void eng_material_rsc::tech_type::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERT( sizeof(*this) == SIZE_TECH_TYPE );
    SerialFile.Serialize( m_iTechnique );
    SerialFile.Serialize( m_iSystemCmd );
    SerialFile.Serialize( m_nSystemCmds );
}

//---------------------------------------------------------------------------
inline
void eng_material_rsc::system_cmds::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERT( sizeof(*this) == SIZE_SYSTEM_CMDS );
    SerialFile.SerializeEnum( m_Type );
    SerialFile.Serialize( m_iRegister );
}

inline
void eng_material_rsc::technique::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof( *this ) == SIZE_SHADER_TECHNIQUE );
    
    SerialFile.Serialize( m_StaticAlloc, sizeof(m_StaticAlloc)/sizeof(u64) );
    SerialFile.Serialize( m_gShader );
}

//---------------------------------------------------------------------------
inline
eng_material_rsc::material_rsc::material_rsc( xserialfile& SerialFile )
{
    ASSERT( SerialFile.GetResourceVersion( ) == VERSION );
}

//---------------------------------------------------------------------------
inline
void eng_material_rsc::material_rsc::SerializeIO( xserialfile& SerialFile ) const
{
    SerialFile.SetResourceVersion( VERSION );
    ASSERTCT( sizeof( *this ) == SIZE_MATERIAL_RSC );
    
    SerialFile.Serialize( m_nTechniques );
    SerialFile.Serialize( m_pTechnique, m_nTechniques, xserialfile::FLAGS_TEMP_MEMORY );

    SerialFile.Serialize( m_nSystemCmds );
    SerialFile.Serialize( m_pSystemCmd, m_nSystemCmds );

    for( s32 i=0; i<m_TechType.getCount(); i++) SerialFile.Serialize( m_TechType[i] );
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_informed_material_rsc - saving functions
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
inline
void eng_informed_material_rsc::uniform_value::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof( *this ) == SIZE_UNIFORM_VALUE );
    
    switch ( m_Type )
    {
        case eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL:      SerialFile.Serialize( m_Bool );   break;
        case eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT:     SerialFile.Serialize( m_F, 1 );   break;
        case eng_shader_program::UNIFORM_TYPE_INFORMED_V2:        SerialFile.Serialize( m_F, 2 );   break;
        case eng_shader_program::UNIFORM_TYPE_INFORMED_V3:        SerialFile.Serialize( m_F, 3 );   break;
        case eng_shader_program::UNIFORM_TYPE_INFORMED_V4:        SerialFile.Serialize( m_F, 4 );   break;
        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL:
        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGB:
        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA:   SerialFile.Serialize( m_Guid );         break;
        
        // system values
        case eng_shader_program::UNIFORM_TYPE_SYSTEM_L2C:
        case eng_shader_program::UNIFORM_TYPE_SYSTEM_L2V:
        case eng_shader_program::UNIFORM_TYPE_SYSTEM_V2C:
        case eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_L:
        case eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_V:
            break;
        default:
        {
            ASSERT( 0 );
        }
    }
    
    SerialFile.Serialize( m_xCallbackGUID );
    SerialFile.SerializeEnum( m_Type );
}

//---------------------------------------------------------------------------
inline
void eng_informed_material_rsc::cmds::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof( *this ) == SIZE_COMMANDS );
    SerialFile.Serialize( m_iValue );
    SerialFile.Serialize( m_iShader );
    SerialFile.Serialize( m_iRegister );
}

//---------------------------------------------------------------------------
inline
void eng_informed_material_rsc::technique::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof( *this ) == SIZE_TECHNIQUE );
    SerialFile.Serialize( m_iCmd );
    SerialFile.Serialize( m_nCmds );
}

//---------------------------------------------------------------------------
inline
eng_informed_material_rsc::informed_rsc::informed_rsc( xserialfile& SerialFile )
{
    ASSERT( SerialFile.GetResourceVersion( ) == VERSION );
}

//---------------------------------------------------------------------------
inline
void eng_informed_material_rsc::informed_rsc::SerializeIO( xserialfile& SerialFile ) const
{
    SerialFile.SetResourceVersion( VERSION );
    ASSERTCT( sizeof( *this ) == SIZE_INFORMED_RSC );

    SerialFile.Serialize( *(u64*)&m_rMaterial );
    
    SerialFile.Serialize( m_nTechniques );
    SerialFile.Serialize( m_pTechnique, m_nTechniques );
    
    SerialFile.Serialize( m_nUniformValues );
    SerialFile.Serialize( m_pUniformValue, m_nUniformValues );

    SerialFile.Serialize( m_nCmds );
    SerialFile.Serialize( m_pCmd, m_nCmds );
}

#endif
