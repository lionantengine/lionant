//
//  eng_Primitives.cpp
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

#ifdef ENG_OPEN_GLES
    #define glShaderSourceARB           glShaderSource
    #define glCompileShaderARB          glCompileShader
    #define glDetachObjectARB           glDetachShader
    #define glGetInfoLogARB             glGetProgramInfoLog   // glGetShaderInfoLog
    #define glDeleteObjectARB           glDeleteShader
    #define glCreateProgramObjectARB    glCreateProgram
    #define glAttachObjectARB           glAttachShader
    #define glBindAttribLocationARB     glBindAttribLocation
    #define glLinkProgramARB            glLinkProgram
    #define glUseProgramObjectARB       glUseProgram
    #define glGenBuffersARB             glGenBuffers
    #define glBindBufferARB             glBindBuffer
    #define glBufferDataARB             glBufferData
    #define glMapBufferARB              glMapBufferOES
    #define glGetUniformLocationARB     glGetUniformLocation
    #define glUnmapBufferARB            glUnmapBufferOES
    #define glUniform1fARB              glUniform1f
    #define glUniform1iARB              glUniform1i
    #define glUniform2fARB              glUniform2f
    #define glUniform3fvARB             glUniform3fv
    #define glUniform4fvARB             glUniform4fv
    #define glUniformMatrix4fvARB       glUniformMatrix4fv
    #define glDeleteBuffersARB          glDeleteBuffers

    #define GL_GEOMETRY_SHADER_EXT      GL_GEOMETRY_SHADER
    #define GL_WRITE_ONLY_ARB           GL_WRITE_ONLY_OES
    #define GL_ARRAY_BUFFER_ARB         GL_ARRAY_BUFFER
    #define GL_CLAMP_TO_BORDER          ~0
#endif


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// HELPFULL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

void eng_CheckForError( void )
{
#ifdef X_DEBUG
    GLenum       Err         = glGetError();
    const char*  pErr        = NULL;
    
    switch( Err )
    {
        case GL_NO_ERROR: break; //          pErr = "GL_NO_ERROR - No error has been recorded. The value of this symbolic constant is guaranteed to be 0."; break;
            
        case GL_INVALID_ENUM:       pErr = "GL_INVALID_ENUM - An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag."; break;
            
        case GL_INVALID_VALUE:      pErr = "GL_INVALID_VALUE - A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag."; break;
            
        case GL_INVALID_OPERATION:  pErr = "GL_INVALID_OPERATION - The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag."; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION: pErr = "GL_INVALID_FRAMEBUFFER_OPERATION - The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag."; break;
            
        case GL_OUT_OF_MEMORY:      pErr = "GL_OUT_OF_MEMORY - There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded."; break;

#ifndef ENG_OPEN_GLES
        case GL_STACK_UNDERFLOW:    pErr = "GL_STACK_UNDERFLOW - An attempt has been made to perform an operation that would cause an internal stack to underflow."; break;
            
        case GL_STACK_OVERFLOW:     pErr = "GL_STACK_OVERFLOW - An attempt has been made to perform an operation that would cause an internal stack to overflow."; break;
#endif
        default: ASSERT(0); // What is this???????
    };
    
            
    if( pErr )
    {
        x_throw( "OPENGL ERROR: [%s]", pErr );
    }
#endif
}

//-----------------------------------------------------------------------------------
static 
void LoadShaderTextFile( const char* pFileName, xptr<xbyte>& Buffer )
{
    xfile       File;
    
    File.Open( pFileName, "rt" );
    
    s32 ShaderLength = File.GetFileLength();

    Buffer.Alloc( ShaderLength + 1 );
    
    File.Read( &Buffer[0], ShaderLength );
    
    Buffer[ ShaderLength ] = '\0';
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// VERTEX DESCRIPTION
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct gl_vertex_desc
{
    eng_vertex_desc::attribute_src      m_AttrDataType;
    s32                                 m_nElements;
    s32                                 m_glType;
    xbool                               m_bNormalize;
};

static const gl_vertex_desc s_GLVertDesc[]=
{
    { eng_vertex_desc::ATTR_SRC_NULL,           0, 0,                  GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_U8x4_F,         4, GL_UNSIGNED_BYTE,   GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_U8x4_I,         4, GL_UNSIGNED_BYTE,   GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_S8x4_F,         4, GL_BYTE,            GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_S8x4_I,         4, GL_BYTE,            GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_U16x2_F,        2, GL_UNSIGNED_SHORT,  GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_U16x2_I,        2, GL_UNSIGNED_SHORT,  GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_U16x4_F,        4, GL_UNSIGNED_SHORT,  GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_U16x4_I,        4, GL_UNSIGNED_SHORT,  GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_S16x2_F,        2, GL_SHORT,           GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_S16x2_I,        2, GL_SHORT,           GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_S16x4_F,        4, GL_SHORT,           GL_TRUE  },
    { eng_vertex_desc::ATTR_SRC_S16x4_I,        4, GL_SHORT,           GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_F32x1,          1, GL_FLOAT,           GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_F32x2,          2, GL_FLOAT,           GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_F32x3,          3, GL_FLOAT,           GL_FALSE },
    { eng_vertex_desc::ATTR_SRC_F32x4,          4, GL_FLOAT,           GL_FALSE }
};

//-----------------------------------------------------------------------------------

eng_vertex_desc::~eng_vertex_desc( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_vertex_desc::Destroy( void )
{
    m_nAttributes = 0;
    m_VertexSize  = 0;
    if(m_bFreeData)
    {
        m_bFreeData = 0;
        if(m_pAttribute.m_Ptr) x_free(m_pAttribute.m_Ptr);
    }
    m_pAttribute.m_Ptr = NULL;
}

//-----------------------------------------------------------------------------------

void eng_vertex_desc::SubmitAttributes( s32 VertSize, s32 nAttributes, attribute* pAttribute, xbool bFreeDataWhenDone )
{
    ASSERT( nAttributes > 0 );
    ASSERT( pAttribute );

    //
    // First we make sure you have everything sorted
    //
    x_qsort( pAttribute, nAttributes, sizeof( attribute ), []( const void* pA, const void* pB ) -> s32
    {
        const attribute& A = *(const attribute *)pA;
        const attribute& B = *(const attribute *)pB;
        if ( A.m_UsageType < B.m_UsageType ) return -1;
        ASSERT(A.m_UsageType != B.m_UsageType);
        return A.m_UsageType > B.m_UsageType;
    });
    
    m_VertexSize        = u16(VertSize);
    m_nAttributes       = nAttributes;
    m_pAttribute.m_Ptr  = pAttribute;
    m_bFreeData         = bFreeDataWhenDone;
}

//-----------------------------------------------------------------------------------

void eng_vertex_desc::Activate( s32 Offset ) const
{
    eng_CheckForError();
    
    for( s32 i=0; i<m_nAttributes; i++ )
    {
        const attribute&        Attribute = m_pAttribute.m_Ptr[i];
        const gl_vertex_desc&   GLDesc    = s_GLVertDesc[ Attribute.m_DataType ];
        
        glEnableVertexAttribArray( i );
        glVertexAttribPointer(
            i,
            GLDesc.m_nElements,
            GLDesc.m_glType,
            GLDesc.m_bNormalize,
            m_VertexSize,
            (void*)(xuptr)(Offset * m_VertexSize + Attribute.m_Offset )
        );
    }
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_vertex_desc::Deactivate( void ) const
{
    for( s32 i=0; i<m_nAttributes; i++ )
    {
        glDisableVertexAttribArray( i );
    }
    
    eng_CheckForError();
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// VERTEX SHADER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

eng_vshader::~eng_vshader( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_vshader::LoadFromFile( const char* VertexShaderName, const eng_vertex_desc::attribute_link* pLink, s32 nAttrLinks, xbool bFreeAttrLnk )
{
    xptr<xbyte> Buffer;
    
    LoadShaderTextFile( VertexShaderName, Buffer );
    LoadFromMemory( &Buffer[0], Buffer.getCount(), pLink, nAttrLinks, bFreeAttrLnk  );
}

//-----------------------------------------------------------------------------------

void eng_vshader::LoadFromMemory( const void* pVertexShader, s32 ShaderLength, const eng_vertex_desc::attribute_link* pLink, s32 nAttrLinks, xbool bFreeAttrLnk )
{
    Destroy();
    
    //
    // Create the shader
    //
    m_GLHandle = (GLhandleARB)(xuptr)glCreateShader( GL_VERTEX_SHADER );
    ASSERT(m_GLHandle);
    
#ifdef TARGET_3DS
    // 3DS doesn't support compile shader at runtime. so the input data must be binary shader code.
    glShaderBinary( 1, &m_GLHandle, GL_PLATFORM_BINARY_DMP, pVertexShader, ShaderLength );
#else
    // Load it into GL
    glShaderSourceARB( m_GLHandle, 1, (const char**)&pVertexShader, &ShaderLength );
    
    //
    // Compile shaders
    //
    xsafe_array<char,1000>  Buffer;
    GLsizei                 StringLength = 1000;
    
    glCompileShaderARB( m_GLHandle );
    eng_CheckForError();

#ifdef ENG_OPEN_GL
    glGetInfoLogARB( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
#else
    glGetShaderInfoLog( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
#endif
    if(StringLength )
    {
        Buffer[ StringLength ] = 0;
        x_throw( "ERROR: Vertex Shader Reports: %s", &Buffer[0] );
        return;
    }
#endif       
 
    //
    // Handle the attribute links
    //
    {
        ASSERT( pLink );
        ASSERT( nAttrLinks > 0 );
        
        m_pAttrbuteLink     = pLink;
        m_nAttributeLinks   = nAttrLinks;
        m_bFreeAttrLinks    = u16(bFreeAttrLnk);
        
        x_qsort( pLink, nAttrLinks, sizeof( eng_vertex_desc::attribute_link ), []( const void* pA, const void* pB ) -> s32
        {
            const eng_vertex_desc::attribute_link& A = *( const eng_vertex_desc::attribute_link * )pA;
            const eng_vertex_desc::attribute_link& B = *( const eng_vertex_desc::attribute_link * )pB;
            if ( A.m_UsageType < B.m_UsageType ) return -1;
            ASSERT(A.m_UsageType != B.m_UsageType);
            return A.m_UsageType > B.m_UsageType;
        } );
    }
}

//-----------------------------------------------------------------------------------

void eng_vshader::Destroy( void )
{
    if( m_GLHandle )
    {
		glDeleteObjectARB( m_GLHandle );
        m_GLHandle = 0;
    }
    
    eng_CheckForError();
    
    if( m_bFreeAttrLinks )
    {
        if( m_pAttrbuteLink )
            x_free( const_cast<eng_vertex_desc::attribute_link*>(m_pAttrbuteLink) );
    }
    
    m_pAttrbuteLink   = NULL;
    m_nAttributeLinks = 0;
    m_bFreeAttrLinks  = 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

eng_fshader::~eng_fshader( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_fshader::LoadFromFile( const char* FragmentShaderName )
{
#ifdef TARGET_3DS
    m_GLHandle = GL_DMP_FRAGMENT_SHADER_DMP;
#else
    xptr<xbyte> Buffer;
    
    LoadShaderTextFile( FragmentShaderName, Buffer );
    
    LoadFromMemory( &Buffer[0], Buffer.getCount() );
#endif
}

//-----------------------------------------------------------------------------------

void eng_fshader::LoadFromMemory( const void* pFragmentShader, s32 ShaderLength )
{
    Destroy();
    
#ifdef TARGET_3DS
    m_GLHandle = GL_DMP_FRAGMENT_SHADER_DMP;
#else
    //
    // Create the shader
    //
    m_GLHandle = (GLhandleARB)(xuptr)glCreateShader( GL_FRAGMENT_SHADER );
    ASSERT(m_GLHandle);
    
    // Load it into GL
    glShaderSourceARB( m_GLHandle, 1, (const char**)&pFragmentShader, &ShaderLength );
    
    //
    // Compile shaders
    //
    xsafe_array<char,1000>  Buffer;
    GLsizei                 StringLength = 0;
    
    glCompileShaderARB( m_GLHandle );
    
#ifdef ENG_OPEN_GL
    glGetInfoLogARB( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
#else
    glGetShaderInfoLog( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
#endif
    if(StringLength )
    {
        Buffer[ StringLength ] = 0;
        x_throw( "ERROR: Fragment Shader Reports: %s", &Buffer[0] );
        return;
    }
    
    eng_CheckForError();
#endif
}

//-----------------------------------------------------------------------------------

void eng_fshader::Destroy( void )
{
#ifndef TARGET_3DS
    if( m_GLHandle )
    {
		glDeleteObjectARB( m_GLHandle );
        m_GLHandle = 0;
    }
#endif
    
    eng_CheckForError();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// GEOMETRY SHADER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef ENG_OPEN_GLES

//-----------------------------------------------------------------------------------

eng_gshader::~eng_gshader( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_gshader::LoadFromFile( const char* GeometryShaderName )
{
    xptr<xbyte> Buffer;
    
    LoadShaderTextFile( GeometryShaderName, Buffer );
    
    LoadFromMemory( (const char*)&Buffer[0], Buffer.getCount() );
}

//-----------------------------------------------------------------------------------

void eng_gshader::LoadFromMemory( const char* pGeometryShader, s32 ShaderLength )
{
    Destroy();
    
    //
    // Create the shader
    //
    m_GLHandle = (GLhandleARB)(xuptr)glCreateShader( GL_GEOMETRY_SHADER_EXT );
    
    // Load it into GL
    glShaderSourceARB( m_GLHandle, 1, (const char**)&pGeometryShader, &ShaderLength );
    
    //
    // Compile shaders
    //
    xsafe_array<char,1000>  Buffer;
    GLsizei                 StringLength = 0;
    
    glCompileShaderARB( m_GLHandle );
    
    glGetInfoLogARB( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
    if(StringLength )
    {
        Buffer[ StringLength ] = 0;
        x_throw( "ERROR: Geometry Shader Reports: %s", &Buffer[0] );
        ASSERT(0);
        return;
    }
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_gshader::Destroy( void )
{
    if( m_GLHandle )
    {
		glDeleteObjectARB( m_GLHandle );
        m_GLHandle = 0;
    }
    
    eng_CheckForError();
}

#elif defined ENG_OPEN_GLES

//-----------------------------------------------------------------------------------

eng_gshader::~eng_gshader( void )
{
    //Destroy();
}

//-----------------------------------------------------------------------------------

void eng_gshader::LoadFromFile( const char* GeometryShaderName )
{
    // We dont support this dudes
    ASSERT( 0 );
}

//-----------------------------------------------------------------------------------

void eng_gshader::LoadFromMemory( const char* pGeometryShader, s32 ShaderLength )
{
    // We dont support this dudes
    ASSERT( 0 );
}

//-----------------------------------------------------------------------------------

void eng_gshader::Destroy( void )
{
    // We dont support this dudes
    ASSERT( 0 );
}

#endif


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// SHADER PROGRAM
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

eng_shader_program::~eng_shader_program( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::Destroy( void )
{
    if( m_GLHandle )
    {
        glDeleteObjectARB( m_GLHandle );
        m_GLHandle = 0;
    }
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::LinkShaders( const eng_vshader&        VertexShader,
                                      const eng_fshader&        FragmentShader )
{
    ASSERT( VertexShader.m_nAttributeLinks > 0 );
    ASSERT( VertexShader.m_GLHandle );
    ASSERT( FragmentShader.m_GLHandle );
    
    // Make sure everything is clear
    Destroy();
    
    // Reset all the registers
    m_NameRegisters.SetMemory(0xff);
    m_NameTextures.SetMemory(0xff);
    
    // Create the shader
    m_GLHandle = glCreateProgramObjectARB();
    
    // Link all the shaders
    glAttachObjectARB( m_GLHandle, VertexShader.m_GLHandle );
    glAttachObjectARB( m_GLHandle, FragmentShader.m_GLHandle );
    eng_CheckForError();
    
    // Link all the vertex desctriptors
    for ( s32 i = 0; i<VertexShader.m_nAttributeLinks; i++ )
    {
        glBindAttribLocationARB( m_GLHandle, i, VertexShader.m_pAttrbuteLink[i].m_pVarName.m_Ptr );
        eng_CheckForError();
    }
    
    // Link everything
    glLinkProgramARB( m_GLHandle );
    //glValidateProgram( m_GLHandle );
    eng_CheckForError();
    
#ifndef TARGET_3DS
    
    //GLint result;
    
    //Compilation checking.
	//glGetObjectParameterivARB( m_GLHandle, GL_OBJECT_LINK_STATUS_ARB, &result);
	
	// If an error was detected.
	//if (!result)
	{
        //
        // Check for errors
        //
        xsafe_array<char,1000>  Buffer;
        GLsizei                 StringLength = 0;

        glGetInfoLogARB( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
        if(StringLength )
        {
            Buffer[ StringLength ] = 0;
            x_throw( "ERROR: Program Shader Linking: %s \n", &Buffer[0] );
            return;
        }
    }
#endif
    
    eng_CheckForError();
    
    // Lets detach the shader since we already compile
    glDetachObjectARB( m_GLHandle, VertexShader.m_GLHandle );
    glDetachObjectARB( m_GLHandle, FragmentShader.m_GLHandle );
    eng_CheckForError();
    
    // Active program just in case we want to create links to variables and such
    glUseProgramObjectARB( m_GLHandle );
    eng_CheckForError();

}

//-----------------------------------------------------------------------------------
#ifdef ENG_OPEN_GL
void eng_shader_program::LinkShaders( const eng_vshader&        VertexShader,
                                      const eng_gshader&        GeometryShader,
                                      const eng_fshader&        FragmentShader )
{
    ASSERT( VertexShader.m_nAttributeLinks > 0 );
    ASSERT( VertexShader.m_GLHandle );
    ASSERT( FragmentShader.m_GLHandle );
    ASSERT( GeometryShader.m_GLHandle );
    
    
    // Make sure everything is clear
    Destroy();
    
    // Reset all the registers
    m_NameRegisters.SetMemory(0xff);
    m_NameTextures.SetMemory(0xff);
    
    // Create the shader
    m_GLHandle = glCreateProgramObjectARB();
    
    // Link everything
    glAttachObjectARB( m_GLHandle, VertexShader.m_GLHandle );
    glAttachObjectARB( m_GLHandle, GeometryShader.m_GLHandle );
    glAttachObjectARB( m_GLHandle, FragmentShader.m_GLHandle );
    eng_CheckForError();
    
    // Link all the vertex desctriptors
    for ( s32 i = 0; i<VertexShader.m_nAttributeLinks; i++ )
    {
        glBindAttribLocationARB( m_GLHandle, i, VertexShader.m_pAttrbuteLink[i].m_pVarName.m_Ptr );
        eng_CheckForError();
    }
    
    // Link all
    glLinkProgramARB( m_GLHandle );
    
    //
    // Check for errors
    //
    xsafe_array<char,1000>  Buffer;
    GLsizei                 StringLength = 0;
    
    glGetInfoLogARB( m_GLHandle, Buffer.getCount()-1, &StringLength, &Buffer[0] );
    if(StringLength )
    {
        Buffer[ StringLength ] = 0;
        x_throw( "ERROR: Program Shader Linking: %s ", &Buffer[0] );
        ASSERT(0);
        return;
    }
    
    eng_CheckForError();

    //
    // Add some parameters for the geometry shader
    //
    
    glProgramParameteriEXT( (GLint)(GLint64)m_GLHandle, GL_GEOMETRY_OUTPUT_TYPE_EXT,  GL_POINTS);
    glProgramParameteriEXT( (GLint)(GLint64)m_GLHandle, GL_GEOMETRY_VERTICES_OUT_EXT, GL_POINTS);
    eng_CheckForError();
    
    GLint temp;
    glGetIntegerv( GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT, &temp);
    glProgramParameteriEXT( (GLint)(GLint64)m_GLHandle, GL_GEOMETRY_VERTICES_OUT_EXT, temp );
    eng_CheckForError();

    // Make sure everything is working
    glUseProgramObjectARB( m_GLHandle );
    eng_CheckForError();
    
    // Lets detach the shader since we already compile
    glDetachObjectARB( m_GLHandle, VertexShader.m_GLHandle );
    glDetachObjectARB( m_GLHandle, FragmentShader.m_GLHandle );
    glDetachObjectARB( m_GLHandle, GeometryShader.m_GLHandle );
    eng_CheckForError();
    
    // Active program just in case we want to create links to variables and such
    glUseProgramObjectARB( m_GLHandle );
    eng_CheckForError();
}

#elif defined ENG_OPEN_GLES

void eng_shader_program::LinkShaders( const eng_vshader&        VertexShader,
                                      const eng_gshader&        GeometryShader,
                                      const eng_fshader&        FragmentShader )
{
    // it does not support this
    ASSERT(0);
}
#endif

//-----------------------------------------------------------------------------------

void eng_shader_program::Activate( void ) const
{
    ASSERT( m_GLHandle );
    glUseProgramObjectARB( m_GLHandle );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::Deactivate( void ) const
{
    ASSERT( m_GLHandle );
    glUseProgramObjectARB( 0 );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::LinkRegisterToUniformVariable( s32 iRegister, const char* pUniformVariableName )
{
    ASSERT( m_GLHandle );
    ASSERT( pUniformVariableName );
    ASSERT( m_NameRegisters[iRegister] == 0xffffffff );
    
    m_NameRegisters[iRegister] = glGetUniformLocationARB( m_GLHandle, pUniformVariableName );
    eng_CheckForError();
    ASSERT( m_NameRegisters[iRegister] >= 0 );
}

//-----------------------------------------------------------------------------------

void eng_shader_program::LinkTextureRegisterWithUniform( s32 iRegister, const char* pUniformVariableName )
{
    ASSERT( m_GLHandle );
    ASSERT( pUniformVariableName );
    ASSERT( m_NameTextures[iRegister] == 0xffffffff );
    
    m_NameTextures[iRegister] = glGetUniformLocationARB( m_GLHandle, pUniformVariableName );
    eng_CheckForError();
    ASSERT( m_NameTextures[iRegister] >= 0 );
        
    // Actually do the gl link as well
    glUniform1i( m_NameTextures[iRegister], iRegister );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, f32 Value ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform1fARB( m_NameRegisters[iRegister], Value );
    eng_CheckForError();
}


//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, s32 Value) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );

    glUniform1iARB( m_NameRegisters[iRegister], Value );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, const xvector2& V ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform2fARB( m_NameRegisters[iRegister], V.m_X, V.m_Y );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, const xvector3d& V ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform3fvARB( m_NameRegisters[iRegister], 1, (f32*)&V );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, const xvector3& V ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform3fvARB( m_NameRegisters[iRegister], 1, (f32*)&V );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, const xvector4&  V  ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform4fvARB( m_NameRegisters[iRegister], 1, (f32*)&V );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable4F( s32 iRegister, const f32* F  ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform4fvARB( m_NameRegisters[iRegister], 1, F );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariableArray( s32 iRegister, s32 Offset, const xmatrix4*  pM, s32 Count ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
#ifndef TARGET_PC
    Offset *= 4;
#endif
    
    glUniformMatrix4fvARB( m_NameRegisters[iRegister]+Offset, Count, GL_FALSE, (f32*)pM );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariableArray( s32 iRegister, s32 Offset, const xvector3d* pV, s32 Count ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform3fvARB( m_NameRegisters[iRegister]+Offset, Count, (f32*)pV );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable3F( s32 iRegister, const f32* F  ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniform3fvARB( m_NameRegisters[iRegister], 1, F );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable2F( s32 iRegister, const f32* F  ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );

#ifdef TARGET_PC
    glUniform2fvARB( m_NameRegisters[iRegister], 1, F );
#else
    glUniform2fv( m_NameRegisters[iRegister], 1, F );
#endif
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_shader_program::setUniformVariable( s32 iRegister, const xmatrix4& M ) const
{
    ASSERT( m_GLHandle );
    ASSERT( m_NameRegisters[iRegister] != 0xffffffff );
    
    glUniformMatrix4fvARB( m_NameRegisters[iRegister], 1, GL_FALSE, (f32*)&M );
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

GLhandleARB eng_shader_program::getHandle( void ) const
{
    return m_GLHandle;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// VERTEX BUFFER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

eng_vbuffer::~eng_vbuffer( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::Destroy( void )
{
    if( m_IDBuffer )
        glDeleteBuffersARB(1, &m_IDBuffer);
    
#if defined(ENG_OPEN_GLES) && !defined(TARGET_3DS)
    if( m_VertexArray )
    {
        glDeleteVertexArraysOES(1, &m_VertexArray);
        m_VertexArray = 0;
    }
#endif
    m_IDBuffer  = 0;
    m_nVertices = 0;
    m_VertSize  = 0;
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::CreateDynamicBuffer( const s32 VertSize, const s32 Count, const void* pData )
{
    Destroy();
    
#if defined(ENG_OPEN_GLES) && !defined(TARGET_3DS)
    glGenVertexArraysOES(1, &m_VertexArray);
    glBindVertexArrayOES(m_VertexArray);
#endif
    
    // Create a new buffer
    glGenBuffersARB(1, &m_IDBuffer );
    
    // Set the vertex buffer as they "active" buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_IDBuffer );
    
    // Now lets allocate the memory for it
    glBufferDataARB(
        GL_ARRAY_BUFFER,
        VertSize * Count,
        pData,
#ifdef TARGET_3DS
        GL_STATIC_DRAW
#else
        GL_DYNAMIC_DRAW
#endif
    );//GL_STREAM_DRAW );
    
    // Set the array info
    m_nVertices = Count;
    m_VertSize  = VertSize;
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::CreateStaticBuffer( const s32 VertSize, const s32 Count, const void* pData )
{
    Destroy();
    
    // Create a new buffer
    glGenBuffersARB(1, &m_IDBuffer );
    
    // Set the vertex buffer as they "active" buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_IDBuffer );
    
    // Now lets allocate the memory for it
    glBufferDataARB( GL_ARRAY_BUFFER, VertSize * Count, pData, GL_STATIC_DRAW );
    
    // Set the array info
    m_nVertices = Count;
    m_VertSize  = VertSize;
}

//-----------------------------------------------------------------------------------

void* eng_vbuffer::LockData( void ) const
{
    ASSERT( m_IDBuffer );
#ifdef X_DEBUG
    ASSERT( ++m_LockCount == 1 );
#endif
    
    eng_CheckForError();
    
#ifdef TARGET_3DS
    ASSERT( NULL == m_LockBuffer );
    m_LockBuffer = x_malloc(m_pVertDesc->m_VertexSize, m_nVertices, XMEM_FLAG_ALIGN_8B);
    void* pBuff = m_LockBuffer;
#else
    // Set the vertex buffer as they "active" buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_IDBuffer );
    void* pBuff = (void*)glMapBufferARB( GL_ARRAY_BUFFER, GL_WRITE_ONLY_ARB );
#endif
    
    eng_CheckForError();
    return pBuff;
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::UnlockData( void ) const
{
    ASSERT( m_IDBuffer );
#ifdef X_DEBUG
    ASSERT( --m_LockCount == 0 );
#endif
    
    eng_CheckForError();
    
    glBindBufferARB(GL_ARRAY_BUFFER, m_IDBuffer );
#ifdef TARGET_3DS
    ASSERT( NULL != m_LockBuffer );
    glBufferSubData(GL_ARRAY_BUFFER, 0, m_pVertDesc->m_VertexSize * m_nVertices, m_LockBuffer);
    x_free(m_LockBuffer);
    m_LockBuffer = NULL;
#else
    glUnmapBufferARB(GL_ARRAY_BUFFER_ARB);
#endif
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::UploadData( s32 iOffset, s32 Count, const void* pData ) const
{
    xbyte* pPtr = (xbyte*)LockData();
    ASSERT( (iOffset+Count) <= m_nVertices);
    ASSERT( pPtr );
    x_memcpy( &pPtr[ m_VertSize*iOffset ], pData, m_VertSize*Count );
    UnlockData();
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::Activate( const eng_vertex_desc& VertDesc, const eng_vshader& vShader, const s32 VertexOffset ) const
{
    eng_CheckForError();
    
    glBindBufferARB(GL_ARRAY_BUFFER, m_IDBuffer );           // for vertex coordinates
    eng_CheckForError();
  
#if defined(ENG_OPEN_GLES) && !defined(TARGET_3DS)
    glBindVertexArrayOES( m_VertexArray );
    eng_CheckForError();
#endif

    s32 iAttribute = 0;
    for( s32 i = 0; i< VertDesc.m_nAttributes; i++ )
    {
        const eng_vertex_desc::attribute& Attribute = VertDesc.m_pAttribute.m_Ptr[ i ];

        if( Attribute.m_UsageType == vShader.m_pAttrbuteLink[ iAttribute ].m_UsageType ||
            ( Attribute.m_UsageType >=  eng_vertex_desc::ATTR_USAGE_00_PARAMETRIC_UV && 
              Attribute.m_UsageType <=  eng_vertex_desc::ATTR_USAGE_09_PARAMETRIC_UV &&
              vShader.m_pAttrbuteLink[ iAttribute ].m_UsageType >= eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV &&
              vShader.m_pAttrbuteLink[ iAttribute ].m_UsageType <= eng_vertex_desc::ATTR_USAGE_09_FULLRANGE_UV ) )
        {
            const gl_vertex_desc&  GLDesc = s_GLVertDesc[ Attribute.m_DataType ];
            ASSERT( GLDesc.m_AttrDataType == Attribute.m_DataType );

            // Set element
            glEnableVertexAttribArray( iAttribute );
            glVertexAttribPointer(
                iAttribute,
                GLDesc.m_nElements,
                GLDesc.m_glType,
                GLDesc.m_bNormalize,
                VertDesc.m_VertexSize,
                (void*)(xuptr)( VertexOffset * VertDesc.m_VertexSize + Attribute.m_Offset )
                );

            // Get ready for the next one
            iAttribute++;

            // Bail if we have reach the maximum number of useful elements
            if ( iAttribute >= vShader.m_nAttributeLinks )
                break;
        }

        ASSERT( Attribute.m_UsageType <= vShader.m_pAttrbuteLink[ iAttribute ].m_UsageType );
    }

    eng_CheckForError( );
}

//-----------------------------------------------------------------------------------

void eng_vbuffer::Deactivate( const eng_vertex_desc& VertDesc ) const
{
    glBindBufferARB(GL_ARRAY_BUFFER, 0 );
    
    VertDesc.Deactivate( );
    
    eng_CheckForError();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// INDEX BUFFER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct eng_index_gl
{
    eng_ibuffer::index_data_desc    m_EngDesc;
    GLuint                          m_GLDesc;
    s32                             m_nBytes;
};

static const eng_index_gl s_IndexDesc[]=
{
    { eng_ibuffer::DATA_DESC_U8,  GL_UNSIGNED_BYTE,  1 },
    { eng_ibuffer::DATA_DESC_U16, GL_UNSIGNED_SHORT, 2 },
    { eng_ibuffer::DATA_DESC_U32, GL_UNSIGNED_INT,   4 }
};

//-----------------------------------------------------------------------------------

eng_ibuffer::~eng_ibuffer( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::Destroy( void )
{
    if( m_IDBuffer )
        glDeleteBuffersARB(1, &m_IDBuffer);
    
    m_IDBuffer = 0;
    m_nIndices = 0;

#ifdef TARGET_3DS
    if ( NULL != m_LockBuffer )
    {
        x_free(m_LockBuffer);
        m_LockBuffer = NULL;
    }
#endif
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::CreateDynamicBuffer( index_data_desc DataDesc, s32 Count, const void* pData )
{
    Destroy();
    
    // Make sure that our local table maches with the description
    // if it does not match we either need to change the table or
    // the user enter some wrong value...
    ASSERT( s_IndexDesc[DataDesc].m_EngDesc == DataDesc );
      
    // Create a new buffer
    glGenBuffersARB(1, &m_IDBuffer );
    
    // Set the buffer as they "active" buffer
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_IDBuffer );
    
    // Now lets allocate the memory for it
    glBufferDataARB(
        GL_ELEMENT_ARRAY_BUFFER,
        Count * s_IndexDesc[DataDesc].m_nBytes,
        pData,
#ifdef TARGET_3DS
        GL_STATIC_DRAW
#else
        GL_DYNAMIC_DRAW
#endif
    );//GL_STREAM_DRAW );
    
    // Set the array info
    m_nIndices = Count;
    m_DataDesc = DataDesc;
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::CreateStaticBuffer( index_data_desc DataDesc, s32 Count, const void* pData )
{
    Destroy();
    
    // Make sure that our local table maches with the description
    // if it does not match we either need to change the table or
    // the user enter some wrong value...
    ASSERT( s_IndexDesc[DataDesc].m_EngDesc == DataDesc );
    
    // Create a new buffer
    glGenBuffersARB(1, &m_IDBuffer );
    
    // Set the buffer as they "active" buffer
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_IDBuffer );
    
    // Now lets allocate the memory for it
    glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER, Count * s_IndexDesc[DataDesc].m_nBytes, pData, GL_STATIC_DRAW );
    
    // Set the array info
    m_nIndices = Count;
    m_DataDesc = DataDesc;
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void* eng_ibuffer::LockData( void )
{
    ASSERT( m_IDBuffer );
#ifdef X_DEBUG
    ASSERT( ++m_LockCount == 1 );
#endif
    
    eng_CheckForError();
    
    // Set the vertex buffer as they "active" buffer
    glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER, m_IDBuffer );
    eng_CheckForError();
    
#ifdef TARGET_3DS
    ASSERT( NULL == m_LockBuffer );
    m_LockBuffer = x_malloc(s_IndexDesc[m_DataDesc].m_nBytes, m_nIndices, XMEM_FLAG_ALIGN_8B);
    void* pBuff = m_LockBuffer;
#else
    void* pBuff = (void*)glMapBufferARB( GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY_ARB );
#endif
    
    eng_CheckForError();
    
    return pBuff;
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::UnlockData( void )
{
    ASSERT( m_IDBuffer );
#ifdef X_DEBUG
    ASSERT( --m_LockCount == 0 );
#endif
    
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_IDBuffer );
#ifdef TARGET_3DS
    ASSERT( NULL != m_LockBuffer );
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, s_IndexDesc[m_DataDesc].m_nBytes * m_nIndices, m_LockBuffer);
    x_free(m_LockBuffer);
    m_LockBuffer = NULL;
#else
    glUnmapBufferARB(GL_ELEMENT_ARRAY_BUFFER);
#endif
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::UploadData( s32 iOffset, s32 Count, const void* pData )
{
    xbyte*      pPtr    = (xbyte*)LockData();
    const s32   nBytes  = s_IndexDesc[m_DataDesc].m_nBytes;
    
    ASSERT( (iOffset+Count) <= m_nIndices );
    ASSERT( pPtr );

    x_memcpy( &pPtr[ nBytes*iOffset ], pData, nBytes*Count );
    
    UnlockData();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::Activate( void ) const
{
    ASSERT( m_IDBuffer );
#ifdef X_DEBUG
    ASSERT( m_LockCount == 0 );
#endif
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_IDBuffer );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::Deactivate( void ) const
{
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0 );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::RenderTriangles( void ) const
{
    RenderTriangles( m_nIndices, 0 );
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::RenderTriangles( s32 nIndices, s32 StartIndexOffset ) const
{
    const eng_index_gl& DataDesc = s_IndexDesc[m_DataDesc];
    
    glDrawElements( GL_TRIANGLES,
                   nIndices,
                   DataDesc.m_GLDesc,
                   (void*)(xuptr)(StartIndexOffset*DataDesc.m_nBytes) );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::RenderLines( void ) const
{
    RenderLines( m_nIndices, 0 );
}

//-----------------------------------------------------------------------------------

void eng_ibuffer::RenderLines( s32 nIndices, s32 StartIndexOffset ) const
{
#ifdef TARGET_3DS
    ASSERTS( false, "RenderLines is not supported yet on 3DS." );
#else
    glDrawElements( GL_LINES,
                   nIndices,
                   s_IndexDesc[m_DataDesc].m_GLDesc,
                   (void*)(xuptr)(StartIndexOffset*s_IndexDesc[m_DataDesc].m_nBytes) );
#endif
    
    eng_CheckForError();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// RENDER BUFFER
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------------

void eng_render_buffer::ActivateDefaultFrameBuffer( void )
{
#ifdef ENG_OPEN_GLES
    // iphone requires this to be set to 1... not sure why
    glBindFramebuffer( GL_FRAMEBUFFER, 1 );
#else
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
#endif
}

//-----------------------------------------------------------------------------------

eng_render_buffer::eng_render_buffer( void )
{
    m_IDColorBuffer  = 0;
    m_IDFrameBuffer  = 0;
    m_W              = 0;
    m_H              = 0;
}

//-----------------------------------------------------------------------------------

eng_render_buffer::~eng_render_buffer( void )
{
    Destroy();
}

//-----------------------------------------------------------------------------------

void eng_render_buffer::CreateRenderBuffer( const eng_texture& Texture )
{
    CreateRenderBuffer( Texture.getWidth(), Texture.getHeight() );
}

//-----------------------------------------------------------------------------------

void eng_render_buffer::CreateRenderBuffer( s32 Width, s32 Height )
{
    m_W = Width;
    m_H = Height;

    glGenFramebuffers(1, &m_IDFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, m_IDFrameBuffer);
    
    glGenRenderbuffers(1, &m_IDColorBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_IDColorBuffer);
#ifdef ENG_OPEN_GLES
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8_OES, m_W, m_H);
#else
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, m_W, m_H);
#endif
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_IDColorBuffer );
    
    glGenRenderbuffers(1, &m_IDZBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_IDZBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, m_W, m_H);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_IDZBuffer);
    
    ASSERT( glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    eng_CheckForError();

    //
    // Set back the default frame buffer
    //
    ActivateDefaultFrameBuffer();
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_render_buffer::Destroy( void )
{
    if( m_IDFrameBuffer )
        glDeleteFramebuffers( 1, &m_IDFrameBuffer );
    
    if( m_IDColorBuffer )
        glDeleteRenderbuffers( 1, &m_IDColorBuffer );
    
    m_IDColorBuffer  = 0;
    m_IDFrameBuffer  = 0;
    m_W              = 0;
    m_H              = 0;
}

//-----------------------------------------------------------------------------------

void eng_render_buffer::Activate( const eng_view& View, const eng_texture& Texture ) const
{
    ASSERT( m_IDFrameBuffer);
    ASSERT( Texture.m_IDBuffer );
    ASSERT( Texture.getHeight() == m_H );
    ASSERT( Texture.getWidth() == m_W );
    ASSERT( View.getViewport().GetWidth() <= m_W );
    ASSERT( View.getViewport().GetHeight() <= m_H );
    
    //
    // Bind the the texture into the frame buffer
    //
    glBindFramebuffer       ( GL_FRAMEBUFFER, m_IDFrameBuffer );
    eng_CheckForError();
    
    // attach the texture to FBO color attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER
                           GL_COLOR_ATTACHMENT0,  // 2. attachment point
                           GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
                           Texture.m_IDBuffer,    // 4. tex ID
                           0);                    // 5. mipmap level: 0(base)
    eng_CheckForError();

    
    ASSERT( glCheckFramebufferStatus( GL_FRAMEBUFFER ) == GL_FRAMEBUFFER_COMPLETE );
    
 
    //
    // Handle the view
    //
    
    // Backup the engine view
    m_BackUpEngView = eng_GetCurrentContext().GetActiveView();
    
    // Set the new view
    eng_GetCurrentContext().SetActiveView( View );
    
    eng_CheckForError();
}

//-----------------------------------------------------------------------------------

void eng_render_buffer::Deactivate( void ) const
{
    ActivateDefaultFrameBuffer();
    
    // TODO: Not sure if we need to deactivate the associate texture...
    eng_GetCurrentContext().SetActiveView( m_BackUpEngView );
    
    eng_CheckForError();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// PRIMITES
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

eng_simple_shader::attrlnk eng_simple_shader::m_VAttrLnk[] =
{
    { xserialfile::ptr<const char>((const char*)&"aPosition"), eng_vertex_desc::ATTR_USAGE_POSITION },
    { xserialfile::ptr<const char>((const char*)&"aTexCoord"), eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV },
    { xserialfile::ptr<const char>((const char*)&"aColor"),    eng_vertex_desc::ATTR_USAGE_00_RGBA }
};

eng_simple_shader::vattr eng_simple_shader::m_VAttr[] =
{
    { u16(X_MEMBER_OFFSET( draw_vertex, m_X )),         eng_vertex_desc::ATTR_SRC_F32x3,    eng_vertex_desc::ATTR_USAGE_POSITION           },
    { u16(X_MEMBER_OFFSET( draw_vertex, m_U )),         eng_vertex_desc::ATTR_SRC_F32x2,    eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV    },
    { u16(X_MEMBER_OFFSET( draw_vertex, m_Color )),     eng_vertex_desc::ATTR_SRC_U8x4_F,   eng_vertex_desc::ATTR_USAGE_00_RGBA            }
};

//-----------------------------------------------------------------------------------

void eng_simple_shader::CompileProgram( void )
{
    xbool bOurVertexShader = FALSE;

    if( FALSE == m_FShader.isValid() )
    {
        static const char pFShader[] = R"(
            #ifdef GL_ES
                precision mediump float; // precision highp float;
                varying lowp vec4   Color;
            #else
                varying vec4   Color;
            #endif
            varying vec2        Texcoord;
            uniform sampler2D   Texture;

            void main( void )
            {
                gl_FragColor = texture2D( Texture, Texcoord.xy ) * Color;
            }
        )";
        static const s32 ShaderLength = x_strlen( pFShader) + 1;

        m_FShader.LoadFromMemory( pFShader, ShaderLength );
    }

    if ( FALSE == m_VShader.isValid() )
    {
        static const char pVShader[] = R"(
            #ifdef GL_ES
                precision highp float;
            #endif

            attribute vec3      aPosition;
            attribute vec2      aTexCoord;
            attribute vec4      aColor;

            varying vec4        Color;
            varying vec2        Texcoord;

            uniform mat4        L2C;

            void main()
            {
                // Pass along the vertex color to the pixel shader
                Color       = aColor;
                Texcoord    = aTexCoord;
        
                // this may be equivalent (gl_Position = ftransform();)
                gl_Position = L2C * vec4( aPosition, 1. );
            }
        )";
        static const s32 ShaderLength = x_strlen( pVShader ) + 1;

        m_VShader.LoadFromMemory( pVShader, ShaderLength, m_VAttrLnk, sizeof(m_VAttrLnk)/sizeof(attrlnk), FALSE );

        bOurVertexShader = TRUE;
    }

    // Link shaders
    if( m_Program.isValid() )
        m_Program.Destroy();

    m_Program.LinkShaders( m_VShader, m_FShader );

    // User must link everything else
    if( bOurVertexShader )
        m_Program.LinkRegisterToUniformVariable( m_iL2CRegister, "L2C" );
}

//-----------------------------------------------------------------------------------

void SetupBBOX( eng_vbuffer& VBuffer, eng_ibuffer& IBuffer, const xbbox& BBox, xcolor Color )
{
    static const u16    Index[] = {
        0, 1, 2, 0, 2, 3,    // front
        4, 5, 6, 4, 6, 7,    // back
        8, 9, 10, 8, 10, 11,   // top
        12, 13, 14, 12, 14, 15,   // bottom
        16, 17, 18, 16, 18, 19,   // right
        20, 21, 22, 20, 22, 23    // left
    };

    const draw_vertex pVertex[] =
    {
        // Front face
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 0, 0, Color ),

        // Back face
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 0, Color ),

        // Top face
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 0, Color ),

        // Bottom face
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 0, Color ),

        // Right face
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Max.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 0, 0, Color ),

        // Left face
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Min.GetZ(), 0, 1, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Min.GetY(), BBox.m_Max.GetZ(), 1, 1, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Max.GetZ(), 1, 0, Color ),
        draw_vertex( BBox.m_Min.GetX(), BBox.m_Max.GetY(), BBox.m_Min.GetZ(), 0, 0, Color )
    };

    IBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, sizeof( Index ) / sizeof( s16 ), Index );
    VBuffer.CreateStaticBuffer( sizeof(draw_vertex), 24, pVertex );
}

//-----------------------------------------------------------------------------------

void SetupSphere( eng_vbuffer& VBuffer, eng_ibuffer& IBuffer, xcolor Color )
{
    const f32 S = 1/100.0f;
    const draw_vertex s_vSphere[] =
    {
        //  POSITION                                     UV
        //  -----------------------------------------    ---------------------
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.050000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 40.4508f*S, 29.3893f*S, 0.000000f, 0.200000f, Color ),
        draw_vertex( 17.2746f*S, 40.4508f*S, 23.7764f*S, 0.100000f, 0.200000f, Color ),
        draw_vertex( 27.9509f*S, 40.4508f*S, 9.0818f*S, 0.200000f, 0.200000f, Color ),
        draw_vertex( 27.9508f*S, 40.4508f*S, -9.0818f*S, 0.300000f, 0.200000f, Color ),
        draw_vertex( 17.2746f*S, 40.4508f*S, -23.7764f*S, 0.400000f, 0.200000f, Color ),
        draw_vertex( -0.0000f*S, 40.4508f*S, -29.3893f*S, 0.500000f, 0.200000f, Color ),
        draw_vertex( -17.2746f*S, 40.4508f*S, -23.7764f*S, 0.600000f, 0.200000f, Color ),
        draw_vertex( -27.9509f*S, 40.4508f*S, -9.0818f*S, 0.700000f, 0.200000f, Color ),
        draw_vertex( -27.9508f*S, 40.4508f*S, 9.0818f*S, 0.800000f, 0.200000f, Color ),
        draw_vertex( -17.2746f*S, 40.4508f*S, 23.7764f*S, 0.900000f, 0.200000f, Color ),
        draw_vertex( 0.0000f*S, 15.4508f*S, 47.5528f*S, 0.000000f, 0.400000f, Color ),
        draw_vertex( 27.9509f*S, 15.4508f*S, 38.4710f*S, 0.100000f, 0.400000f, Color ),
        draw_vertex( 45.2254f*S, 15.4508f*S, 14.6946f*S, 0.200000f, 0.400000f, Color ),
        draw_vertex( 45.2254f*S, 15.4508f*S, -14.6946f*S, 0.300000f, 0.400000f, Color ),
        draw_vertex( 27.9508f*S, 15.4508f*S, -38.4711f*S, 0.400000f, 0.400000f, Color ),
        draw_vertex( -0.0000f*S, 15.4508f*S, -47.5528f*S, 0.500000f, 0.400000f, Color ),
        draw_vertex( -27.9509f*S, 15.4508f*S, -38.4710f*S, 0.600000f, 0.400000f, Color ),
        draw_vertex( -45.2254f*S, 15.4508f*S, -14.6946f*S, 0.700000f, 0.400000f, Color ),
        draw_vertex( -45.2254f*S, 15.4508f*S, 14.6946f*S, 0.800000f, 0.400000f, Color ),
        draw_vertex( -27.9508f*S, 15.4508f*S, 38.4711f*S, 0.900000f, 0.400000f, Color ),
        draw_vertex( 0.0000f*S, -15.4509f*S, 47.5528f*S, 0.000000f, 0.600000f, Color ),
        draw_vertex( 27.9509f*S, -15.4509f*S, 38.4710f*S, 0.100000f, 0.600000f, Color ),
        draw_vertex( 45.2254f*S, -15.4509f*S, 14.6946f*S, 0.200000f, 0.600000f, Color ),
        draw_vertex( 45.2254f*S, -15.4509f*S, -14.6946f*S, 0.300000f, 0.600000f, Color ),
        draw_vertex( 27.9508f*S, -15.4508f*S, -38.4711f*S, 0.400000f, 0.600000f, Color ),
        draw_vertex( -0.0000f*S, -15.4508f*S, -47.5528f*S, 0.500000f, 0.600000f, Color ),
        draw_vertex( -27.9509f*S, -15.4508f*S, -38.4710f*S, 0.600000f, 0.600000f, Color ),
        draw_vertex( -45.2254f*S, -15.4509f*S, -14.6946f*S, 0.700000f, 0.600000f, Color ),
        draw_vertex( -45.2254f*S, -15.4509f*S, 14.6946f*S, 0.800000f, 0.600000f, Color ),
        draw_vertex( -27.9508f*S, -15.4509f*S, 38.4711f*S, 0.900000f, 0.600000f, Color ),
        draw_vertex( 0.0000f*S, -40.4509f*S, 29.3893f*S, 0.000000f, 0.800000f, Color ),
        draw_vertex( 17.2746f*S, -40.4509f*S, 23.7764f*S, 0.100000f, 0.800000f, Color ),
        draw_vertex( 27.9508f*S, -40.4509f*S, 9.0818f*S, 0.200000f, 0.800000f, Color ),
        draw_vertex( 27.9508f*S, -40.4509f*S, -9.0818f*S, 0.300000f, 0.800000f, Color ),
        draw_vertex( 17.2746f*S, -40.4509f*S, -23.7764f*S, 0.400000f, 0.800000f, Color ),
        draw_vertex( -0.0000f*S, -40.4509f*S, -29.3893f*S, 0.500000f, 0.800000f, Color ),
        draw_vertex( -17.2746f*S, -40.4509f*S, -23.7764f*S, 0.600000f, 0.800000f, Color ),
        draw_vertex( -27.9508f*S, -40.4509f*S, -9.0818f*S, 0.700000f, 0.800000f, Color ),
        draw_vertex( -27.9508f*S, -40.4509f*S, 9.0818f*S, 0.800000f, 0.800000f, Color ),
        draw_vertex( -17.2746f*S, -40.4509f*S, 23.7764f*S, 0.900000f, 0.800000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.050000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.150000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.250000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.350000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.450000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.550000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.650000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.750000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.850000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 50.0000f*S, 0.0000f*S, 0.950000f, 0.000000f, Color ),
        draw_vertex( 0.0000f*S, 40.4508f*S, 29.3893f*S, 1.000000f, 0.200000f, Color ),
        draw_vertex( 0.0000f*S, 15.4508f*S, 47.5528f*S, 1.000000f, 0.400000f, Color ),
        draw_vertex( 0.0000f*S, -15.4509f*S, 47.5528f*S, 1.000000f, 0.600000f, Color ),
        draw_vertex( 0.0000f*S, -40.4509f*S, 29.3893f*S, 1.000000f, 0.800000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.150000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.250000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.350000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.450000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.550000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.650000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.750000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.850000f, 1.000000f, Color ),
        draw_vertex( 0.0000f*S, -50.0000f*S, -0.0000f*S, 0.950000f, 1.000000f, Color )
    };

    static const u16 s_iSphere[] =
    {
        0, 1, 2,
        42, 2, 3,
        43, 3, 4,
        44, 4, 5,
        45, 5, 6,
        46, 6, 7,
        47, 7, 8,
        48, 8, 9,
        49, 9, 10,
        50, 10, 51,
        1, 11, 12,
        1, 12, 2,
        2, 12, 13,
        2, 13, 3,
        3, 13, 14,
        3, 14, 4,
        4, 14, 15,
        4, 15, 5,
        5, 15, 16,
        5, 16, 6,
        6, 16, 17,
        6, 17, 7,
        7, 17, 18,
        7, 18, 8,
        8, 18, 19,
        8, 19, 9,
        9, 19, 20,
        9, 20, 10,
        10, 20, 52,
        10, 52, 51,
        11, 21, 22,
        11, 22, 12,
        12, 22, 23,
        12, 23, 13,
        13, 23, 24,
        13, 24, 14,
        14, 24, 25,
        14, 25, 15,
        15, 25, 26,
        15, 26, 16,
        16, 26, 27,
        16, 27, 17,
        17, 27, 28,
        17, 28, 18,
        18, 28, 29,
        18, 29, 19,
        19, 29, 30,
        19, 30, 20,
        20, 30, 53,
        20, 53, 52,
        21, 31, 32,
        21, 32, 22,
        22, 32, 33,
        22, 33, 23,
        23, 33, 34,
        23, 34, 24,
        24, 34, 35,
        24, 35, 25,
        25, 35, 36,
        25, 36, 26,
        26, 36, 37,
        26, 37, 27,
        27, 37, 38,
        27, 38, 28,
        28, 38, 39,
        28, 39, 29,
        29, 39, 40,
        29, 40, 30,
        30, 40, 54,
        30, 54, 53,
        41, 32, 31,
        55, 33, 32,
        56, 34, 33,
        57, 35, 34,
        58, 36, 35,
        59, 37, 36,
        60, 38, 37,
        61, 39, 38,
        62, 40, 39,
        63, 54, 40
    };

    IBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, sizeof( s_iSphere ) / sizeof( s16 ), s_iSphere );
    VBuffer.CreateStaticBuffer( sizeof( draw_vertex ), sizeof( s_vSphere ) / sizeof( draw_vertex ), s_vSphere );
}

//-----------------------------------------------------------------------------------

void SetupPlane( eng_vbuffer& VBuffer, eng_ibuffer& IBuffer, xcolor Color )
{
    const draw_vertex Vertex[] =
    {
        draw_vertex( -1, 0, -1, 0, 1, Color ),
        draw_vertex( -1, 0,  1, 1, 1, Color ),
        draw_vertex(  1, 0,  1, 1, 0, Color ),
        draw_vertex(  1, 0, -1, 0, 0, Color )
    };

    static const u16 Index[] = 
    {
        0, 1, 2, 0, 2, 3,   
    };
    
    IBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, sizeof( Index ) / sizeof( s16 ), Index );
    VBuffer.CreateStaticBuffer( sizeof( draw_vertex ), sizeof( Vertex ) / sizeof( draw_vertex ), Vertex );
}

//-----------------------------------------------------------------------------------

void eng_simple_shader::CompileGeometry( geometry_type GeomType, xcolor Color )
{
    if( m_VertexBufer.isValid() ) m_VertexBufer.Destroy();
    if( m_IndexBuffer.isValid() ) m_IndexBuffer.Destroy();

    switch( GeomType )
    {
    case GEOM_TYPE_CUBE:        SetupBBOX( m_VertexBufer, m_IndexBuffer, xbbox( xvector3d(0,0,0), 1), Color ); break;
    case GEOM_TYPE_SPHERE:      SetupSphere( m_VertexBufer, m_IndexBuffer, Color ); break;
    case GEOM_TYPE_XZ_PLANE:    SetupPlane( m_VertexBufer, m_IndexBuffer, Color ); break;
    default:ASSERT(0);
    }

    m_VertDesc.SubmitAttributes( sizeof(draw_vertex), sizeof(m_VAttr)/sizeof(vattr), (eng_vertex_desc::attribute*)m_VAttr, FALSE );
}

//-----------------------------------------------------------------------------------

void eng_simple_shader::Render( const xmatrix4& L2W ) const
{
    m_Program.Activate();
    m_Program.setUniformVariable( m_iL2CRegister, eng_GetCurrentContext().GetActiveView().getW2C() * L2W );
    m_IndexBuffer.Activate();

    m_VertexBufer.Activate( m_VertDesc, m_VShader, 0 );
    m_IndexBuffer.RenderTriangles();

    m_IndexBuffer.Deactivate();
    m_VertexBufer.Deactivate( m_VertDesc );
    m_Program.Deactivate();
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// RENDER_STATE
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------

void eng_render_state::setup( blend Blend, zbuffer ZBuffer, raster_cull RasterCull, raster_fill RasterFill )
{
    m_Blend         = Blend;
    m_ZBuffer       = ZBuffer;
    m_RasterCull    = RasterCull;
    m_RasterFill    = RasterFill;
}

//-----------------------------------------------------------------------------------

void eng_render_state::Activate( void ) const
{
    
#ifdef ENG_OPEN_GL
    switch(m_RasterFill)
    {
        case RASTER_FILL_FULL:          glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); break;
        case RASTER_FILL_WIRE_FRAME:    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); break;
        default: ASSERT(0);
    }
    eng_CheckForError();
#endif
    
    switch( m_RasterCull )
    {
        case RASTER_CULL_NONE:      glDisable( GL_CULL_FACE ); break;
        case RASTER_CULL:           glEnable( GL_CULL_FACE ); break;
        case RASTER_CULL_FRONT:     glCullFace(GL_BACK); break;
        default: ASSERT(0);
    }
    eng_CheckForError();

    
    switch( m_ZBuffer )
    {
        case ZBUFFER_ON:
        {
            glDepthMask( TRUE );            // Write to the depth buffer
            glEnable( GL_DEPTH_TEST );      // Enable Z Compare
            glDepthFunc( GL_LEQUAL );       // <=
            break;
        }
        case ZBUFFER_OFF:
        {
            glDepthMask( FALSE );
            glDisable( GL_DEPTH_TEST );
            glDepthFunc( GL_ALWAYS );
            break;
        }
        case ZBUFFER_READ_ONLY:
        {
            glDepthMask( FALSE );
            glEnable( GL_DEPTH_TEST );
            glDepthFunc( GL_LEQUAL );
            break;
        }
        default: ASSERT(0);
    }
    eng_CheckForError();

    
    switch( m_Blend )
    {
        case BLEND_OFF:
        {
            glDisable( GL_BLEND );
            break;
        }
            
        case BLEND_ALPHA:
        {
            glEnable( GL_BLEND );
            
#ifdef TARGET_PC
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#else
            
            glBlendEquationSeparate( GL_FUNC_ADD,
                                    GL_FUNC_ADD);
            glBlendFuncSeparate(GL_SRC_ALPHA,
                                GL_ONE_MINUS_SRC_ALPHA,
                                GL_ONE,
                                GL_ZERO);
#endif
            break;
        }
            
        case BLEND_ADD:
        {
            glEnable( GL_BLEND );
            
#ifdef TARGET_PC
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_ONE, GL_ONE);
#else
            glBlendEquationSeparate( GL_FUNC_ADD,
                                    GL_FUNC_ADD);
            glBlendFuncSeparate(GL_ONE,
                                GL_ONE,
                                GL_ONE,
                                GL_ZERO);
#endif
            break;
        }
            
        case BLEND_SUB:
        {
            glEnable( GL_BLEND );
            
#ifdef TARGET_PC
            glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
            glBlendFunc(GL_ONE, GL_ONE);
#else
            glBlendEquationSeparate( GL_FUNC_SUBTRACT,
                                    GL_FUNC_ADD);
            glBlendFuncSeparate(GL_ONE,
                                GL_ONE,
                                GL_ONE,
                                GL_ZERO);
#endif
            break;
        }
            
        default: ASSERT(0);
    }
    eng_CheckForError();
    
}



