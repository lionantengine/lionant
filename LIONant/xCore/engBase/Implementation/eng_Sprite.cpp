//
//  eng_sprite.cpp
//  engBase
//
//  Created by Tomas Arce on 9/23/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

struct eng_sprite_type : public eng_resource_type
{
    eng_sprite_type( const char* pType ) : eng_resource_type( pType, eng_sprite_rsc::UID ) {}
    
    virtual eng_resource_base*  CreateInstance      ( void );
    virtual void                KillInstance        ( eng_resource_base* pEntry );
};

static eng_sprite_type s_SpriteType( "iatlas" );

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite_rsc_base
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

struct eng_sprite_rsc_base : public eng_sprite_rsc
{
    virtual eng_resource_type&  getType             ( void ) const { return s_SpriteType; }
    virtual void*               onQTGetAsset        ( void )
    {
        // If we are loaded quality zero then we should return the data
        if( m_CurMaxQuality == 0 && m_SpriteAtlas.getAsset() )
            return this;
        
        x_LogWarning( "ENG_SPRITE", "Waiting to Load Sprite" );
        return NULL;
    }
    
    virtual xbool onQTLoadResource ( s32 iQuality )
    {
        ASSERT( iQuality == 0 );
        
        //
        // Lets setup the texture name
        // We want to start this as soon as possible
        //
        eng_resource_guid Guid;
        Guid.setup( m_Guid, eng_texture_rsc::UID );
        m_SpriteAtlas.setup( Guid );
        m_SpriteAtlas.getAsset();
        
        //
        // Load the info data
        //
        xserialfile SerialFile;
        xstring SpriteAtlas = g_RscMgr.createFileName( m_Guid, 0, s_SpriteType.getTypeString() );
        
        SerialFile.Load( SpriteAtlas, m_pGroup );
        if( g_RscMgr.isAborting() )
        {
            if( m_pGroup )
            {
                x_delete( m_pGroup );
                m_pGroup = NULL;
            }
            return FALSE;
        }
        
        x_LogMessage( "ENG_SPRITE", "Load Done" );
        return TRUE;
    }
    
    virtual xbool onQTReduceQuality ( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0);
        return TRUE;
    }
};

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite_type
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

eng_resource_base* eng_sprite_type::CreateInstance( void )
{
    return x_new( eng_sprite_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_sprite_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_sprite_rsc_base*)pEntry );
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite_rsc
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------

eng_resource_type& eng_sprite_rsc::getStaticType( void )
{
    return s_SpriteType;
}

//---------------------------------------------------------------------------------

eng_sprite_rsc::~eng_sprite_rsc( void )
{
    if( m_pGroup )
    {
        x_delete( m_pGroup );
        m_pGroup = NULL;
    }
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

static
s32 u16compare( u16* pA, u16* pB )
{
    if( *pA < *pB ) return -1;
    return *pA > *pB;
}

//---------------------------------------------------------------------------------

void eng_sprite::setup( const eng_sprite_rsc::ref& Ref, const char* pSpriteName )
{
    ASSERT( Ref.isValid() );
    m_SpriteRscRef = Ref;
    m_Key = (u16)x_strHash( pSpriteName, 0xffff );
}

//---------------------------------------------------------------------------------

void eng_sprite::setup( eng_resource_guid Guid, const char* pSpriteName )
{
    m_SpriteRscRef.setup( Guid );
    setup( m_SpriteRscRef, pSpriteName );
}

//---------------------------------------------------------------------------------

u16 eng_sprite::ResolveKey( u16 Key, const eng_sprite_rsc* pRsc )
{
    const u16* pItem  = (const u16*)x_bsearch( &Key,
                                              pRsc->m_pGroup->m_SpriteNameHash.m_Ptr,
                                              pRsc->m_pGroup->m_nSprites,
                                              sizeof(u16),
                                              (x_compare_fn*)u16compare
                                              );
    ASSERT( pItem );
    return u16(pItem - pRsc->m_pGroup->m_SpriteNameHash.m_Ptr);
}

//---------------------------------------------------------------------------------

const eng_sprite_rsc* eng_sprite::Resolve( void )
{
    //
    // Make sure we are ready to render
    //
    const eng_sprite_rsc* pRsc = m_SpriteRscRef.getAsset();
    if( pRsc == NULL )
    {
        // If for some reason we have not loaded the resource then we must
        // make sure that this sprite index is also set to null.
        m_iSprite = 0xffff;
        return NULL;
    }
    
    //
    // If we just finish loading the resource then link it with the sprite
    //
    if( m_iSprite == 0xffff )
    {
        m_iSprite = ResolveKey( m_Key, pRsc );
    }
    
    return pRsc;
}

//---------------------------------------------------------------------------------

void eng_sprite::DrawSprite( eng_draw& Draw, const xvector2& Pos, const xvector2& Scale, const s32 iFrame ) const
{
    const eng_sprite_rsc* pRsc = const_cast<eng_sprite*>(this)->Resolve();
    if( pRsc == NULL )
        return;

    // Determine if the user wants us to overwrite the iframe
    const s32 iFinalFrame = iFrame==-1?m_iFrame:iFrame;
    
    //
    // Ok we should be ready to render at this point
    //
    const eng_sprite_rsc::info& SpriteInfo  = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[m_iSprite];
    const eng_sprite_rsc::info& Sprite      = SpriteInfo.m_Animated.m_SpecialMask == 0xffff?pRsc->m_pGroup->m_SpriteInfo.m_Ptr[SpriteInfo.m_Animated.m_iBase + iFinalFrame]:SpriteInfo;
    
    draw_vertex* pVertex;
    u16*         pIndex;
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_TEXTURE_ON |ENG_DRAW_MODE_BLEND_ALPHA);
    Draw.SetTexture( *pRsc->m_SpriteAtlas.getAsset() );
    Draw.GetBuffers( &pVertex, 4, &pIndex, 6 );
    
    for( s32 i=0; i<4; i++ )
    {
        const eng_sprite_rsc::info::vertex& Vertex = Sprite.m_Vertex[i];
      //  const eng_sprite_rsc::info::vertex& VertexUV = Sprite.m_Vertex[ ( v + 2 ) % 4 ];

        pVertex[i].setup( xvector3d( Pos.m_X + Vertex.m_Pos.m_X * Scale.m_X,
                                     Pos.m_Y - ( Vertex.m_Pos.m_Y - Sprite.m_Vertex[ 2 ].m_Pos.m_Y ) * Scale.m_Y,
                                     0 ),
                         Vertex.m_UV.m_X,
                         Vertex.m_UV.m_Y, xcolor(~0) );
    }
    
    pIndex[0] = 0;
    pIndex[1] = 1;
    pIndex[2] = 2;
    
    pIndex[3] = 0;
    pIndex[4] = 2;
    pIndex[5] = 3;
    
    Draw.DrawBufferTriangles();
    Draw.End();
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite_animated
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

xbool eng_sprite_animated::BeginAdvanceTime( f32 DeltaTime )
{
    const eng_sprite_rsc* pRsc = Resolve();
    if( pRsc == NULL )
        return FALSE;
    
    m_DetaTimeRemeninder += DeltaTime;
    
    return TRUE;
}

//---------------------------------------------------------------------------------

xbool eng_sprite_animated::WhileAdvanceTime( event& Event )
{
    const eng_sprite_rsc&                   Rsc                     = *m_SpriteRscRef.getAsset();
    const eng_sprite_rsc::info::animated&   SpriteInfo              = Rsc.m_pGroup->m_SpriteInfo.m_Ptr[m_iSprite].m_Animated;
    const f32                               ExpectedMilliseconds    = 1.f/(SpriteInfo.m_FPS*1000);
    
    // make sure that is animated
    ASSERT( SpriteInfo.m_SpecialMask == 0xffff );
    
    while( m_DetaTimeRemeninder > ExpectedMilliseconds )
    {
        //
        // Deal with events
        //
        if( SpriteInfo.m_nEvents && m_iLastEventRead != -1 )
        {
            if( m_Direction > 0 )
            {
                if( SpriteInfo.m_Event[m_iLastEventRead].m_iFrame <= m_iFrame )
                {
                    // Save the event
                    Event = SpriteInfo.m_Event[m_iLastEventRead];
                    
                    // Move the event list forward
                    m_iLastEventRead++;
                    
                    // Deal with edge case
                    if( m_iLastEventRead > SpriteInfo.m_nEvents )
                    {
                        m_iLastEventRead = -1;
                    }
                    
                    return TRUE;
                }
                else
                {
                    // Not need to update the event we should be updating the frame count now
                }
            }
            else
            {
                if( SpriteInfo.m_Event[m_iLastEventRead].m_iFrame >= m_iFrame )
                {
                    // Save the event
                    Event = SpriteInfo.m_Event[m_iLastEventRead];
                    
                    // Move the event list forward
                    m_iLastEventRead--;
                    
                    // Deal with edge case
                    if( m_iLastEventRead < 0 )
                    {
                        m_iLastEventRead = -1;
                    }
                    
                    return TRUE;
                }
                else
                {
                    // Not need to update the event we should be updating the frame count now
                }
            }
        }
        
        
        //
        // Deal with frames
        //
        m_DetaTimeRemeninder -= ExpectedMilliseconds;

        m_iFrame += m_Direction;
        
        // Deal with edge cases
        if( m_iFrame >= SpriteInfo.m_nFrames )
        {
            switch( SpriteInfo.m_PlayBack )
            {
                case eng_sprite_rsc::info::ANIMPLAYBACK_ONES_RESET:
                {
                    m_iFrame             = 0;
                    m_DetaTimeRemeninder = 0;
                    m_Direction          = 0;
                    return FALSE;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_ONES:
                {
                    m_iFrame--;
                    m_DetaTimeRemeninder = 0;
                    m_Direction          = 0;
                    return FALSE;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_PINGPONG:
                {
                    m_iFrame--;
                    m_Direction = -1;
                    m_iLastEventRead = SpriteInfo.m_nEvents-1;
                    break;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_LOOP:
                {
                    m_iFrame = 0;
                    m_iLastEventRead = 0;
                    break;
                }
            }
        }
        else if( m_iFrame < 0 )
        {
            switch( SpriteInfo.m_PlayBack )
            {
                case eng_sprite_rsc::info::ANIMPLAYBACK_ONES_RESET:
                {
                    m_iFrame             = SpriteInfo.m_nFrames-1;
                    m_DetaTimeRemeninder = 0;
                    m_Direction          = 0;
                    return FALSE;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_ONES:
                {
                    m_iFrame++;
                    m_DetaTimeRemeninder = 0;
                    m_Direction          = 0;
                    return FALSE;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_PINGPONG:
                {
                    m_iFrame++;
                    m_Direction = 1;
                    m_iLastEventRead = 0;
                    break;
                }
                case eng_sprite_rsc::info::ANIMPLAYBACK_LOOP:
                {
                    m_iFrame = SpriteInfo.m_nFrames-1;
                    m_iLastEventRead = SpriteInfo.m_nEvents-1;
                    break;
                }
            }
        }
    }
    
    return FALSE;
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// eng_sprite_render_2dgroup
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
/*******************
 //
 // Really cool example on how to make light jobs even easier to use....
 //
#include <functional>
using namespace std;

struct simpleJob : public x_simple_job<1,x_light_job>
{
    typedef void parallel_fn( void );
    
    simpleJob( void ){}
    
    simpleJob( x_light_trigger& Trigger, std::function<void(simpleJob* SimpleJob)> Function )
    {
        m_pFunction = Function;
        AndThenRuns(Trigger);
        g_Scheduler.StartLightJobChain( *this );
    }

    void SetJob( x_light_trigger& Trigger, std::function<void(simpleJob* SimpleJob)> Function )
    {
        m_pFunction = Function;
        AndThenRuns(Trigger);
        g_Scheduler.StartLightJobChain( *this );
    }
    
    std::function<void(simpleJob* SimpleJob)> m_pFunction;
    
    virtual void onRun( void )
    {
        m_pFunction(this);
    };
};

void eng_sprite_render_2dgroup::Clear( void )
{
    x_light_trigger LightTrigger;
    simpleJob JobCleanArray( LightTrigger, [this]( simpleJob* SimpleJob )
    {
        for( auto& Entry:m_qtTexture )
        {
            Entry.m_Texture.setNull();
        }
    } );
                                                     
    // Clear the number of nodes
    m_qtNumNodes.setup( 0 );
    
    // Local sync eveything
    LightTrigger.LocalSync();
}

************************************/

//===============================================================================
// FRAGMENT SHADER
//===============================================================================

#ifdef TARGET_3DS
    static const char s_DrawFragmentShader[] =
    {
        0
    };
#elif defined(TARGET_PC)
    static const char s_DrawFragmentShader[] =
    "   #ifdef GL_ES\n"
    "   precision mediump float; // precision highp float;\n"
    "   varying lowp vec4   Color;\n"
    "   #else\n"
    "       varying vec4   Color;\n"
    "   #endif\n"
    "   varying vec2        Texcoord;\n"
    "   uniform sampler2D   Texture;\n"
    "   void main( void )\n"
    "   {\n"
    "       gl_FragColor = texture2D( Texture, Texcoord.xy ) * Color;\n"
    "   }\n";
#else
    static const char s_DrawFragmentShader[] = R"(

    #ifdef GL_ES
        precision mediump float; // precision highp float;

        varying lowp vec4   Color;

    #else
        varying vec4   Color;
    #endif


    varying vec2        Texcoord;
    uniform sampler2D   Texture;

    void main( void )
    {
        gl_FragColor = texture2D( Texture, Texcoord.xy ) * Color;
    }

    )";
#endif

//===============================================================================
// VERTEX SHADER
//===============================================================================

#ifdef TARGET_3DS

    __align(8) static const unsigned char s_DrawVertexShader[] =
    {
        0x44, 0x56, 0x4c, 0x42, 0x01, 0x00, 0x00, 0x00, 0xa0, 0x00, 0x00, 0x00, 0x44, 0x56, 0x4c, 0x50,
        0x02, 0x0d, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x58, 0x00, 0x00, 0x00,
        0x06, 0x00, 0x00, 0x00, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x88, 0x00, 0x00, 0x00,
        0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe2, 0x0b, 0x01, 0x10, 0xe2, 0x0b, 0x02, 0x20, 0xe2, 0x0b,
        0x03, 0x30, 0xe2, 0x0b, 0x04, 0xf0, 0x01, 0x4c, 0x00, 0x00, 0x00, 0x84, 0x05, 0x00, 0x00, 0x90,
        0x05, 0x80, 0x02, 0x3a, 0x04, 0x28, 0x20, 0x20, 0x04, 0x10, 0x40, 0x4c, 0x00, 0x00, 0x00, 0x88,
        0x00, 0x00, 0x00, 0x84, 0x68, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x64, 0xc3, 0x06, 0x80,
        0x07, 0x00, 0x00, 0x00, 0x62, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x61, 0xc3, 0x06, 0x80,
        0x07, 0x00, 0x00, 0x00, 0x6f, 0xc3, 0x06, 0x80, 0x07, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x80,
        0x03, 0x00, 0x00, 0x00, 0x56, 0x53, 0x68, 0x61, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x73, 0x68, 0x00,
        0x44, 0x56, 0x4c, 0x45, 0x02, 0x0d, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
        0x07, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
        0x54, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x84, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
        0x9c, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0xbc, 0x00, 0x00, 0x00, 0x45, 0x00, 0x00, 0x00,
        0x02, 0x00, 0x08, 0x00, 0x00, 0xfe, 0x46, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x06, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
        0x0b, 0x00, 0x00, 0x00, 0x02, 0x00, 0x01, 0x00, 0x0c, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
        0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x02, 0x00, 0x01, 0x00,
        0x0f, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x36, 0x00, 0x00, 0x00,
        0x02, 0x00, 0x02, 0x00, 0x41, 0x00, 0x00, 0x00, 0x10, 0x00, 0x13, 0x00, 0x6c, 0x5f, 0x70, 0x6f,
        0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x00, 0x6d, 0x61, 0x69, 0x6e, 0x00, 0x65, 0x6e, 0x64, 0x6d,
        0x61, 0x69, 0x6e, 0x00, 0x61, 0x50, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x78, 0x79,
        0x7a, 0x77, 0x00, 0x61, 0x54, 0x65, 0x78, 0x43, 0x6f, 0x6f, 0x72, 0x64, 0x2e, 0x78, 0x79, 0x7a,
        0x77, 0x00, 0x61, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x2e, 0x78, 0x79, 0x7a, 0x00, 0x4c, 0x32, 0x43,
        0x00, 0x00, 0x00, 0x00
    };

#elif defined(TARGET_PC)

    static const char s_DrawVertexShader[] =
    "   #ifdef GL_ES\n"
    "   precision highp float;\n"
    "   #endif\n"
    "   attribute vec3      aPosition;\n"
    "   attribute vec2      aTexCoord;\n"
    "   attribute vec4      aColor;\n"
    "   varying vec4        Color;\n"
    "   varying vec2        Texcoord;\n"
    "   uniform mat4        L2C;\n"
    "   void main()\n"
    "   {\n"
    "       // Pass along the vertex color to the pixel shader\n"
    "       Color       = aColor;\n"
    "       Texcoord    = aTexCoord;\n"
    "       // this may be equivalent (gl_Position = ftransform();)\n"
    "       gl_Position = L2C * vec4( aPosition, 1. );\n"
    "   }\n";

#else

    static const char s_DrawVertexShader[] = R"(

    #ifdef GL_ES
    precision highp float;
    #endif

    attribute vec3      aPosition;
    attribute vec2      aTexCoord;
    attribute vec4      aColor;

    varying vec4        Color;
    varying vec2        Texcoord;

    uniform mat4        L2C;

    void main()
    {
        // Pass along the vertex color to the pixel shader
        Color       = aColor;
        Texcoord    = aTexCoord;
        
        // this may be equivalent (gl_Position = ftransform();)
        gl_Position = L2C * vec4( aPosition, 1. );
    }

    )";

#endif

//===============================================================================
// STATIC VARIABLES IN THE CLASS
//===============================================================================

struct sprite_system;
static sprite_system* s_pSpriteSystem = NULL;

struct sprite_system
{
    typedef eng_vertex_desc vdesc;

    eng_fshader                                 m_FragmentShader;
    eng_vshader                                 m_VertexShader;
    eng_shader_program                          m_ShaderProgram;
    xsafe_array<vdesc::attribute, 3>            m_VertAttr;                             // Vertex Attributes for the description
    xsafe_array<vdesc::attribute_link, 3>       m_VShaderAttrLinks;
    eng_vertex_desc                             m_VertexDesc;
    
    sprite_system( void ){ s_pSpriteSystem = this;  }
    ~sprite_system( void ){ s_pSpriteSystem = NULL; }

    void init( void )
    {
        //
        // Create the vertex desctiption
        //
        m_VertAttr[0].Setup( eng_vertex_desc::ATTR_USAGE_POSITION,          eng_vertex_desc::ATTR_SRC_F32x3, X_MEMBER_OFFSET( draw_vertex, m_X ) );
        m_VertAttr[1].Setup( eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV,   eng_vertex_desc::ATTR_SRC_F32x2, X_MEMBER_OFFSET( draw_vertex, m_U ) );
        m_VertAttr[2].Setup( eng_vertex_desc::ATTR_USAGE_00_RGBA,           eng_vertex_desc::ATTR_SRC_U8x4_F,X_MEMBER_OFFSET( draw_vertex, m_Color ) );
        m_VertexDesc.SubmitAttributes( sizeof(draw_vertex), 3, m_VertAttr, FALSE );
        
        //
        // Create the shader
        //
        m_VShaderAttrLinks[0].Setup( "aPosition",   eng_vertex_desc::ATTR_USAGE_POSITION );
        m_VShaderAttrLinks[1].Setup( "aTexCoord",   eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV );
        m_VShaderAttrLinks[2].Setup( "aColor",      eng_vertex_desc::ATTR_USAGE_00_RGBA );

        m_VertexShader.LoadFromMemory( s_DrawVertexShader, sizeof( s_DrawVertexShader ), m_VShaderAttrLinks, 3, FALSE );

        m_FragmentShader.LoadFromMemory( s_DrawFragmentShader, sizeof( s_DrawFragmentShader ) );
        m_ShaderProgram.LinkShaders( m_VertexShader, m_FragmentShader );
        m_ShaderProgram.LinkRegisterToUniformVariable( 0, "L2C" );
    }
};

//---------------------------------------------------------------------------------

void* _eng_InitSpriteSystem( void )
{
    sprite_system* pSystem = x_new( sprite_system, 1, 0 );
    
    if( pSystem )
        pSystem->init();

    return pSystem;
}

//---------------------------------------------------------------------------------

void _eng_KillSpriteSystem( void* pPtr )
{
    if ( pPtr ) 
        x_delete( (sprite_system*)pPtr );
}

//===============================================================================
// FUNCTIONS
//===============================================================================

//---------------------------------------------------------------------------------

eng_sprite_render_2dgroup::eng_sprite_render_2dgroup( void )
{
    m_MaxZ = m_MinZ = 0;
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::Clear( void )
{
    // Clear all the ZShort
    for( auto& Entry:m_qtZShort )
    {
        Entry.m_Head.setup(0xffffffff);
    }
    
    for( auto& Entry:m_qtDrawNode )
    {
        Entry.m_qtNSprites.setup( 0 );
    }
    
    // Clear all the key index buffers
    m_qtNumNodes.setup( 0 );
    m_qtNumRenderCalls.setup( 0 );
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::SetupShaderProgram( void )
{
    
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::Init( s32 MaxSprites, f32 JobOrder, f32 MinZ, f32 MaxZ, xbool bPerfectShorting )
{
    ASSERT( MinZ < MaxZ );
    
    m_qtSpritesNodes.New( MaxSprites );
    m_qtVertexBuffer.New( MaxSprites * 4 );
    m_qtIndices.New     ( MaxSprites * 6 );
    m_qtDrawCalls.New   ( MaxSprites );
    m_qtDrawNode.New    ( x_Max( 1, MaxSprites/drawnode::NODES_TO_DRAW ), XMEM_FLAG_ALIGN_8B );
    
    // We must make sure that the divisor spreads perfectly each section
    // Because we will be using that number of index into the nodes later
    ASSERT( m_qtDrawNode.getCount() > 0 );
    s32 nZNodes = (MaxSprites)/m_qtDrawNode.getCount();
    
    m_qtZShort.New( nZNodes * m_qtDrawNode.getCount() );
    m_MaxZ = MaxZ;
    m_MinZ = MinZ;
    
    // Handle the drawing of everything
    m_DrawAll.m_pThis = this;
    
    // Set some basic data
    {
        s32 Offset = 0;
        
        m_DrawNodeBlockSize = m_qtZShort.getCount()/m_qtDrawNode.getCount();

        ASSERT( ((m_qtZShort.getCount()-1)/m_DrawNodeBlockSize) == (m_qtDrawNode.getCount()-1) );
        for( auto& Entry:m_qtDrawNode )
        {
            Entry.m_pThis = this;
            Entry.m_Count = m_DrawNodeBlockSize;
            Entry.m_Start = Offset;
            Offset += Entry.m_Count;
        }
        
        if( Offset != m_qtZShort.getCount() )
        {
            auto& Entry = m_qtDrawNode[m_qtDrawNode.getCount()-1];
            Entry.m_Count = m_qtZShort.getCount() - Entry.m_Start;
        }
    }
    
    // Set the job order
    m_DrawAll.m_Order = JobOrder;

    m_bPerfectShorting = bPerfectShorting;

    // Create our program
    SetupShaderProgram();
    
    // Create the Index and Vertex buffer
    m_VertexBuffer.CreateDynamicBuffer( s_pSpriteSystem->m_VertexDesc.getVertSize(), MaxSprites*4 );
    m_IndexBuffer.CreateDynamicBuffer( eng_ibuffer::DATA_DESC_U16, MaxSprites*6 );
    
    // Make sure everything else is clear
    Clear();
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::Begin( void )
{
    Clear();
}

//---------------------------------------------------------------------------------

s32 eng_sprite_render_2dgroup::ResolveIndices( const eng_texture& Texture )
{
    //
    // Allocate the sprite in our list
    //
    s32   iNode = m_qtNumNodes.Inc() - 1;
    node& Node  = m_qtSpritesNodes[iNode];

    Node.m_pTexture = &Texture;
    Node.m_iNext    = -1;
    
    return iNode;
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::AddToHashTable( s32 iNode, f32 Z )
{
    node&           Node     = m_qtSpritesNodes[ iNode ];
    
    Node.m_Z = x_Min( 1, (Z - m_MinZ)/m_MaxZ );
    ASSERT( Node.m_Z >= 0 );
    ASSERT( Node.m_Z < m_MaxZ );
    
    const s32       iHash    = s32(Node.m_Z*(m_qtZShort.getCount()-1));
    hashtable_node& HashNode = m_qtZShort[iHash];
    
    // So we are going to say that inside this range there is one sprite
    m_qtDrawNode[ iHash/m_DrawNodeBlockSize ].m_qtNSprites.Inc();
    
    // Nowe we can add the sprite inside the hash table
    do
    {
        s32 Local = HashNode.m_Head.get();
        
        // We will assume we are going to be sucessfull and set our next equal to the first node
        Node.m_iNext = Local;
        
        // Now we are going to try to insert ourselves into the hash table
        if( HashNode.m_Head.set( Local, iNode ) )
            break;
        
        // If we fail we must try the holdthing again
    } while (1);
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::qtAddSprite( eng_sprite& Sprite, const xmatrix4& Matrix, xcolor Color )
{
    //
    // Make sure that the sprite can be resolved.
    //
    const eng_sprite_rsc* pRsc = Sprite.Resolve();
    if( pRsc == NULL )
        return;
    
    //
    // Todo: Add culling at this point
    //
    
    //
    // Get the Node index
    //
    const eng_texture*  pTexture = pRsc->m_SpriteAtlas.getAsset();
    const s32           iNode    = ResolveIndices( *pTexture );
    
    //
    // Transform the vertices
    //
    const eng_sprite_rsc::info::animated&   Animated    = pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ Sprite.m_iSprite ].m_Animated;
    vertex*                                 pDestVertex = &m_qtVertexBuffer[ iNode * 4 ];
    const eng_sprite_rsc::info&             Info        = Animated.m_SpecialMask==0xffff?
                                                              pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ Animated.m_iBase + Sprite.m_iFrame ] :
                                                              pRsc->m_pGroup->m_SpriteInfo.m_Ptr[ Sprite.m_iSprite ];
    f32 Z=0;
    for( s32 i=0; i<4; i++ )
    {
        const eng_sprite_rsc::info::vertex& Src  = Info.m_Vertex[i];
        vertex&                             Dest = pDestVertex[i];
        
        Dest.m_Pos      = Matrix * xvector3( Src.m_Pos.m_X, Src.m_Pos.m_Y, 0 );
        Z              += Dest.m_Pos.m_Z;
        Dest.m_Pos.m_Z  = 0;

        Dest.m_UV.Set( Src.m_UV.m_X, Src.m_UV.m_Y );
        Dest.m_Color = Color;
    }
    
    //
    // Add sprite in hash table
    //
    AddToHashTable( iNode, Z/4.f );
}

//---------------------------------------------------------------------------------
struct temp_sort
{
    xbool operator < ( const temp_sort& Node ) const
    {
        if( m_pNode->m_Z == Node.m_pNode->m_Z )
        {
            return m_pNode->m_pTexture < Node.m_pNode->m_pTexture;
        }
        
        return m_pNode->m_Z < Node.m_pNode->m_Z ;
    }
    eng_sprite_render_2dgroup::node*       m_pNode;
};

void eng_sprite_render_2dgroup::drawnode::onRun( void )
{
    eng_sprite_render_2dgroup& This = *m_pThis;
    
    xarray<temp_sort> TempMemory;

    TempMemory.Grow( 100 );

    //
    // Create the index buffer
    //
    m_iFirstDrawCall = 0xffff;
    const node* const   pSpriteNodeBase = &This.m_qtSpritesNodes[0];
    s32                 iPrev           = m_iFirstDrawCall;
    s32                 nIndices        = m_IndexOffset;                // Offset to the right place for our indices
    s32                 LastIndex       = nIndices;
    const eng_texture*  pTexture        = NULL;
    
    for( s32 i=0; i<m_Count; i++ )
    {
        hashtable_node& HashNode = This.m_qtZShort[m_Start + i];
        
        // Do we have anything to do?
        if( HashNode.m_Head.get() == 0xffffffff )
            continue;
        
        //
        // Make sure we have the perfect shorting
        //
        if( This.m_bPerfectShorting )
        {
            //
            // Copy index to the temp table
            //
            TempMemory.DeleteAllNodes();
            for( u32 j=HashNode.m_Head.get(); j != 0xffffffff ; j = This.m_qtSpritesNodes[j].m_iNext )
            {
                TempMemory.append().m_pNode = &This.m_qtSpritesNodes[j];
            }
            
            //
            // Sort it
            //
            TempMemory.Sort();
            
            //
            // Reinsert all the nodes in the correct order
            //
            const s32 iTempLasNode = TempMemory.getCount()-1;
            for( s32 j=0; j<iTempLasNode; j++ )
            {
                const s32   iNode       = s32(TempMemory[j].m_pNode - pSpriteNodeBase);
                node&       SpriteNode  = This.m_qtSpritesNodes[ iNode ];
                const s32   iNextNode   = s32(TempMemory[j+1].m_pNode - pSpriteNodeBase);
                
                SpriteNode.m_iNext = iNextNode;
            }
            
            // Deal with the last node
            node& SpriteNode = *TempMemory[iTempLasNode].m_pNode;
            SpriteNode.m_iNext = 0xffffffff;
            
            // Set the head to point to the new first node
            HashNode.m_Head.setup( s32(TempMemory[0].m_pNode - pSpriteNodeBase) );
        }
        
        //
        // Collect all the data
        //
        for( s32 j=HashNode.m_Head.get(); j != 0xffffffff ; j = This.m_qtSpritesNodes[j].m_iNext )
        {
            const node& Node = This.m_qtSpritesNodes[j];
            
            //
            // Make sure the right texture is turn on
            //
            if( pTexture != Node.m_pTexture )
            {
                //
                // Flash out a drawcall if we are changing textures
                //
                if( pTexture )
                {
                    const s32  iCurrent = This.m_qtNumRenderCalls.Inc() - 1;
                    drawcalls& DrawCall = This.m_qtDrawCalls[ iCurrent ];
                    
                    // Make sure to link the previous node to our node
                    if( iPrev != 0xffff )
                    {
                        This.m_qtDrawCalls[ iPrev ].m_iNext = (u16)iCurrent;
                    }
                    else
                    {
                        // This is the very first one in the list
                        m_iFirstDrawCall = (u16)iCurrent;
                    }
                    
                    // Ok now we should be able to fill our stuff
                    DrawCall.m_pTexture    = pTexture;
                    DrawCall.m_IndexOffset = LastIndex;
                    DrawCall.m_Count       = nIndices-LastIndex;
                    
                    // Prepare for the next batch
                    LastIndex = nIndices;
                    iPrev     = iCurrent;
                }
                
                //
                // Se the new texture index
                //
                pTexture = Node.m_pTexture;
            }
            
            //
            // Copy indices
            //
            const s32 VOffset = j*4;
            
            const xvector3d& A = This.m_qtVertexBuffer[ VOffset + 0 ].m_Pos;
            const xvector3d& B = This.m_qtVertexBuffer[ VOffset + 1 ].m_Pos;
            const xvector3d& C = This.m_qtVertexBuffer[ VOffset + 2 ].m_Pos;
            
            if( A.IsRightHanded(B,C) )
            {
                This.m_qtIndices[nIndices++] = 0 + VOffset;
                This.m_qtIndices[nIndices++] = 1 + VOffset;
                This.m_qtIndices[nIndices++] = 2 + VOffset;
                This.m_qtIndices[nIndices++] = 0 + VOffset;
                This.m_qtIndices[nIndices++] = 2 + VOffset;
                This.m_qtIndices[nIndices++] = 3 + VOffset;
            }
            else
            {
                This.m_qtIndices[nIndices++] = 0 + VOffset;
                This.m_qtIndices[nIndices++] = 2 + VOffset;
                This.m_qtIndices[nIndices++] = 1 + VOffset;
                This.m_qtIndices[nIndices++] = 0 + VOffset;
                This.m_qtIndices[nIndices++] = 3 + VOffset;
                This.m_qtIndices[nIndices++] = 2 + VOffset;
            }
        }
    }

    //
    // Last batch
    //
    if( pTexture )
    {
        const s32  iCurrent = This.m_qtNumRenderCalls.Inc() - 1;
        drawcalls& DrawCall = This.m_qtDrawCalls[ iCurrent ];
        
        // Make sure to link the previous node to our node
        if( iPrev != 0xffff )
        {
            This.m_qtDrawCalls[ iPrev ].m_iNext = (u16)iCurrent;
        }
        else
        {
            // This is the very first one in the list
            m_iFirstDrawCall = (u16)iCurrent;
        }
        
        DrawCall.m_pTexture     = pTexture;
        DrawCall.m_IndexOffset  = LastIndex;
        DrawCall.m_Count        = nIndices-LastIndex;
        DrawCall.m_iNext        = 0xffff;
    }
}


//---------------------------------------------------------------------------------
// The plan is to parallelizice this function into two.
// One function starting from the bottom and another from the top of the zshort list
void eng_sprite_render_2dgroup::Flush( void )
{
    x_light_trigger LightTrigger;
    
    //
    // Setup the trigger and pre-allocate all the indices
    //
    s32 nSprites = 0;
    for( drawnode& DrawNode:m_qtDrawNode )
    {
        const s32 nSprintesInList = DrawNode.m_qtNSprites.get();
        
        // Do we have anything to do in this batch?
        if( nSprintesInList == 0 )
            continue;
        
        DrawNode.m_IndexOffset = nSprites*6;
        DrawNode.AndThenRuns( LightTrigger );
        
        nSprites += nSprintesInList;
    }
    
    // If we got nothing to do then leave
    if( nSprites == 0 )
        return;
    
    //
    // Ok time to tell the system to start working
    //
    for( drawnode& DrawNode:m_qtDrawNode  )
    {
        const s32 nSprintesInList = DrawNode.m_qtNSprites.get();
        
        // Do we have anything to do in this batch?
        if( nSprintesInList == 0 )
            continue;
        
        g_Scheduler.StartLightJobChain( DrawNode );
    }
    
    // Wait untill we have created all the sprites
    LightTrigger.LocalSync();
    
    //
    // Now we are ready to render
    //
    eng_GetCurrentContext().qtAddRenderJob( m_DrawAll );
}

//---------------------------------------------------------------------------------

void eng_sprite_render_2dgroup::drawall::onRunRenderJob( void ) const
{
    eng_sprite_render_2dgroup& This = *m_pThis;
    
    const eng_view& View = eng_GetCurrentContext().GetActiveView();
    
    xmatrix4 L2C_2D;
    
    //
    // Build 2D Matrices
    //
    {
        s32 XRes = View.getViewport().GetWidth();
        s32 YRes = View.getViewport().GetHeight();
        
        L2C_2D.Identity();
        L2C_2D.setTranslation( xvector3( -1, -1, 0 ) );
        L2C_2D.setScale      ( xvector3( 2.0f/XRes, 2.0f/YRes, 1 ) );
    }

    
    //
    // Upload Verts and Indices
    //
    {
        const s32 nNodes = This.m_qtNumNodes.get();
        const s32 nVerts = nNodes*4;
        const s32 nIndex = nNodes*6;
        
        // Quick sanity check
        if( 0 )
        {
            for( s32 i=0; i<nIndex; i++ )
            {
                ASSERT( This.m_qtIndices[i] < nVerts );
                ASSERT( This.m_qtIndices[i] >= 0 );
            }
        }
        
        
        This.m_VertexBuffer.UploadData ( 0, nVerts, &This.m_qtVertexBuffer[0] );
        This.m_IndexBuffer.UploadData  ( 0, nIndex, &This.m_qtIndices[0] );
    }
    
    glEnable( GL_BLEND );
    
    glBlendEquationSeparate( GL_FUNC_ADD,
                            GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA,
                        GL_ONE_MINUS_SRC_ALPHA,
                        GL_ONE,
                        GL_ZERO);
    
    glDepthMask( FALSE );
    glDisable( GL_DEPTH_TEST );
    glDepthFunc( GL_ALWAYS );
    
    //
    // Activate the shader here
    //
    s_pSpriteSystem->m_ShaderProgram.Activate( );
    s_pSpriteSystem->m_ShaderProgram.setUniformVariable( 0, L2C_2D );
    
    //
    // Set all the vertex arrays
    //
    This.m_VertexBuffer.Activate( s_pSpriteSystem->m_VertexDesc, s_pSpriteSystem->m_VertexShader );
    This.m_IndexBuffer.Activate();
    
    //
    // Start the rendering
    //
    for( const drawnode& DrawNode:This.m_qtDrawNode )
    {
        const s32 nSprintesInList = DrawNode.m_qtNSprites.get();
        
        // Do we have anything to do in this batch?
        if( nSprintesInList == 0 )
            continue;
        
        for( u16 iNext = DrawNode.m_iFirstDrawCall; iNext != 0xffff; iNext = This.m_qtDrawCalls[iNext].m_iNext )
        {
            const drawcalls& DrawCall = This.m_qtDrawCalls[iNext];
            
            //
            // Activate the texture
            //
            DrawCall.m_pTexture->Activate();
            
            //
            // Render
            //
            This.m_IndexBuffer.RenderTriangles( DrawCall.m_Count, DrawCall.m_IndexOffset );
        }
    }
    This.m_IndexBuffer.Deactivate();
    This.m_VertexBuffer.Deactivate( s_pSpriteSystem->m_VertexDesc );
    s_pSpriteSystem->m_ShaderProgram.Deactivate( );
}

