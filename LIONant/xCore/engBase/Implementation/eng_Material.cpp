//
//  eng_Material.cpp
// 
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_base.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Types
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

struct eng_material_type : public eng_resource_type
{
    eng_material_type( const char* pType ) : eng_resource_type( pType, eng_material_rsc::UID ) {}

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

struct eng_shader_type : public eng_resource_type
{
    eng_shader_type( const char* pType ) : eng_resource_type( pType, eng_shader_rsc::UID ) {}

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

struct eng_informed_type : public eng_resource_type
{
    eng_informed_type( const char* pType ) : eng_resource_type( pType, eng_informed_material_rsc::UID ) {}

    virtual eng_resource_base*  CreateInstance( void );
    virtual void                KillInstance( eng_resource_base* pEntry );
};

static eng_material_type s_MaterialType ( "material" );
static eng_shader_type   s_ShaderType   ( "shader" );
static eng_informed_type s_InformedType ( "informed" );

void ForceMaterialLinking( void )
{

}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_material_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_material_rsc_base : public eng_material_rsc, public x_simple_job<1, x_light_job>
{
    enum loading_state
    {
        LOADING_STATE_NOT_STARTED,
        LOADING_STATE_COMPILING_PROGRAM,
        LOADING_STATE_PULLING_SHADERS,
        LOADING_STATE_READY
    };

    loading_state   m_LoadingState = LOADING_STATE_NOT_STARTED; 

                                eng_material_rsc_base       ( void )        { setupAffinity( AFFINITY_MAIN_THREAD ); } 
    virtual eng_resource_type&  getType                     ( void ) const  { return s_MaterialType; }
    virtual void*               onQTGetAsset                ( void )
    {
        if ( m_LoadingState == LOADING_STATE_READY )
            return m_pMaterialRSC;

        if ( m_LoadingState == LOADING_STATE_PULLING_SHADERS )
        {
            for ( s32 i = 0; i < m_pMaterialRSC->m_nTechniques; i++ )
            {
                const technique& Technique = m_pMaterialRSC->m_pTechnique.m_Ptr[i];
                if ( Technique.m_gShader[ 0 ] ) if( Technique.getVShaderRef( ).getAsset() == NULL ) return NULL;
                if ( Technique.m_gShader[ 1 ] ) if( Technique.getFShaderRef( ).getAsset() == NULL ) return NULL;
                if ( Technique.m_gShader[ 2 ] ) if( Technique.getGShaderRef( ).getAsset() == NULL ) return NULL;
            }

            // Start the next phase             
            m_LoadingState = LOADING_STATE_COMPILING_PROGRAM;
            g_Scheduler.StartJobChain( *this );
        }

        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the geom data
        //
        xserialfile SerialFile;
        xstring     MaterialPath = g_RscMgr.createFileName( m_Guid, iQuality, s_MaterialType.getTypeString( ) );

        // Dont let the serial file free the temp data in the constructor
        SerialFile.DontFreeTempData();

        SerialFile.Load( MaterialPath, m_pMaterialRSC );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pMaterialRSC )
            {
                x_delete( m_pMaterialRSC );
                m_pMaterialRSC = NULL;
            }
            return FALSE;
        }

        //
        // Start loading all its dependencies
        //
        for ( s32 i = 0; i < m_pMaterialRSC->m_nTechniques; i++ )
        {
            technique& Technique = m_pMaterialRSC->m_pTechnique.m_Ptr[i];

            for ( s32 s = 0; s < 3; s++ )
            {
                if ( Technique.m_gShader[ s ] ) 
                {
                    ASSERT( eng_resource_guid::getType(Technique.m_gShader[ s ]) == eng_shader_rsc::UID );
                    ASSERT( Technique.m_gShader[ s ]&1 );
                    if ( s==0 ) Technique.getVShaderRef( ).getAsset();
                    if ( s==1 ) Technique.getFShaderRef( ).getAsset();
                    if ( s==2 ) Technique.getGShaderRef( ).getAsset();
                }
            }
        }
        m_LoadingState = LOADING_STATE_PULLING_SHADERS;

        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }

    virtual void onRun( void )
    {
        //
        // Create Programs and Link Uniforms to registers
        //
        for ( s32 i = 0; i < m_pMaterialRSC->m_nTechniques; i++ )
        {
            technique&                          Technique   = m_pMaterialRSC->m_pTechnique.m_Ptr[i];
            eng_shader_program&                 Program     = Technique.getShaderProgram();
            const eng_shader_rsc::shader_rsc*   pVShader    = Technique.getVShaderRef( ).getAsset();
            const eng_shader_rsc::shader_rsc*   pFShader    = Technique.getFShaderRef( ).getAsset();
            const eng_shader_rsc::shader_rsc*   pGShader    = Technique.m_gShader[ 2 ]?Technique.getGShaderRef( ).getAsset():NULL;
            ASSERT( pVShader  );
            ASSERT( pFShader  );

            //
            // Make sure that we initialize the memory properly
            //
            x_Construct( &Program );

            //
            // Link shaders
            //
            if ( pGShader )
            {
                ASSERT( pGShader );
                Program.LinkShaders( pVShader->getVShader(), pGShader->getGShader(), pFShader->getFShader() );
            }
            else
            {
                Program.LinkShaders( pVShader->getVShader(), pFShader->getFShader());
            }

            //
            // Link Fragment Uniforms First
            //
            s32 UniformRegisterOffset = 0;
            for ( s32 i = 0; i < pFShader->m_nUniforms; i++ )
            {
                const eng_shader_rsc::uniform_type& Uniform = pFShader->m_pUniform.m_Ptr[ i ];

                if ( Uniform.m_Type >= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA &&
                     Uniform.m_Type <= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL )
                {
                    Program.LinkTextureRegisterWithUniform( UniformRegisterOffset + i, Uniform.m_pName.m_Ptr );
                }
                else
                {
                    Program.LinkRegisterToUniformVariable( UniformRegisterOffset + i, Uniform.m_pName.m_Ptr );
                }
            }

            //
            // Link Vertex Uniforms 
            //
            UniformRegisterOffset += pFShader->m_nUniforms;
            for ( s32 i = 0; i < pVShader->m_nUniforms; i++ )
            {
                const eng_shader_rsc::uniform_type& Uniform = pVShader->m_pUniform.m_Ptr[ i ];

                Program.LinkRegisterToUniformVariable( UniformRegisterOffset + i, Uniform.m_pName.m_Ptr );
            }

            //
            // Link Geometry Uniforms 
            //
            if ( pGShader )
            {
                UniformRegisterOffset += pVShader->m_nUniforms;
                for ( s32 i = 0; i < pGShader->m_nUniforms; i++ )
                {
                    const eng_shader_rsc::uniform_type& Uniform = pGShader->m_pUniform.m_Ptr[ i ];

                    Program.LinkRegisterToUniformVariable( UniformRegisterOffset + i, Uniform.m_pName.m_Ptr );
                }
            }
        }

        //
        // Change the state
        //
        m_LoadingState = LOADING_STATE_READY;
    }
};

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_shader_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_shader_rsc_base : public eng_shader_rsc, public x_simple_job<1, x_light_job>
{
    void* m_pTempData = NULL;

    eng_shader_rsc_base( void )
    {
        setupAffinity( AFFINITY_MAIN_THREAD );
    }

    virtual eng_resource_type&  getType( void ) const { return s_MaterialType; }
    virtual void*               onQTGetAsset( void )
    {
        // If we are loaded quality zero then we should return the data
        if ( m_CurMaxQuality == 0 && m_pTempData == NULL )
            return m_pShaderRSC;

        x_LogWarning( "ENG_MATERIAL", "Waiting to Load the Material" );
        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the geom data
        //
        xserialfile SerialFile;
        xstring ShaderPath = g_RscMgr.createFileName( m_Guid, iQuality, s_ShaderType.getTypeString( ) );

        // Dont let the serial file free the temp data in the constructor
        SerialFile.DontFreeTempData();

        SerialFile.Load( ShaderPath, m_pShaderRSC );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pShaderRSC )
            {
                x_delete( m_pShaderRSC );
                m_pShaderRSC = NULL;
            }
            return FALSE;
        }

        // Get the temp data 
        m_pTempData = SerialFile.getTempData();
        ASSERT( m_pTempData );

        // Set a render job so that it runs in the main thread
        g_Scheduler.StartJobChain( *this );

        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }

    virtual void onRun( void )
    {
        switch ( m_pShaderRSC->m_ShaderType )
        {
        case eng_shader_rsc::SHADER_TYPE_VERTEX:
        {
            const char*     pShader = m_pShaderRSC->m_pShader.m_Ptr;
            eng_vshader&    VShader = m_pShaderRSC->getVShader();

            x_Construct( &VShader );

            VShader.LoadFromMemory( 
                pShader,  
                x_strlen(pShader)+1, 
                m_pShaderRSC->m_pAttributeLink.m_Ptr,
                m_pShaderRSC->m_nAttributes,
                FALSE  
                );
            break;
        }
        case eng_shader_rsc::SHADER_TYPE_FRAGMENT:
        {
            const char*     pShader = m_pShaderRSC->m_pShader.m_Ptr;
            eng_fshader&    FShader = m_pShaderRSC->getFShader();

            x_Construct( &FShader );

            FShader.LoadFromMemory( 
                pShader,  
                x_strlen(pShader)+1
                );
            break;
        }
        case eng_shader_rsc::SHADER_TYPE_GEOMETRY:
        {
            const char*     pShader = m_pShaderRSC->m_pShader.m_Ptr;
            eng_gshader&    GShader = m_pShaderRSC->getGShader();

            x_Construct( &GShader );

            GShader.LoadFromMemory( 
                pShader,  
                x_strlen(pShader)+1
                );
            break;
        }
        default:
            ASSERT(0);
        }

        //
        // Free the temp data
        //
        x_delete( (xbyte*)m_pTempData );

        // By setting this pointer to NULL we signal the system we are ready with the geom
        m_pTempData = NULL;

        x_LogMessage( "ENG_SHADER", "Load Done" );
    }
};

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// eng_material_rsc_base
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

struct eng_informed_rsc_base : public eng_informed_material_rsc
{
    enum loading_state
    {
        LOADING_STATE_NOT_STARTED,
        LOADING_STATE_PULLING_RESOURCES,
        LOADING_STATE_READY
    };

    loading_state   m_LoadingState = LOADING_STATE_NOT_STARTED; 

    virtual eng_resource_type&  getType                     ( void ) const  { return s_InformedType; }
    virtual void*               onQTGetAsset                ( void )
    {
        if ( m_LoadingState == LOADING_STATE_READY )
            return m_pInformedRSC;

        if ( m_LoadingState == LOADING_STATE_PULLING_RESOURCES )
        {
            //
            // Start loading texture dependencies
            //
            for ( s32 i = 0; i < m_pInformedRSC->m_nUniformValues; i++ )
            {
                uniform_value& UniformValue = m_pInformedRSC->m_pUniformValue.m_Ptr[i];

                if ( UniformValue.isTexture() )
                {
                    if( NULL == UniformValue.getTexture().getAsset() ) 
                        return NULL;
                }
            }

            //
            // Start loading texture dependencies
            //
            if( NULL == m_pInformedRSC->getMaterialRef().getAsset() )
                return NULL;

            // Start the next phase             
            m_LoadingState = LOADING_STATE_READY;
        }

        return NULL;
    }

    virtual xbool onQTLoadResource( s32 iQuality )
    {
        ASSERT( iQuality == 0 );

        //
        // Load the geom data
        //
        xserialfile SerialFile;
        xstring     MaterialPath = g_RscMgr.createFileName( m_Guid, iQuality, s_InformedType.getTypeString( ) );

        // Dont let the serial file free the temp data in the constructor
        SerialFile.DontFreeTempData();

        SerialFile.Load( MaterialPath, m_pInformedRSC );
        if ( g_RscMgr.isAborting( ) )
        {
            if ( m_pInformedRSC )
            {
                x_delete( m_pInformedRSC );
                m_pInformedRSC = NULL;
            }
            return FALSE;
        }

        //
        // Start loading texture dependencies
        //
        for ( s32 i = 0; i < m_pInformedRSC->m_nUniformValues; i++ )
        {
            uniform_value& UniformValue = m_pInformedRSC->m_pUniformValue.m_Ptr[i];

            if ( UniformValue.isTexture() )
            {
                UniformValue.getTexture().getAsset();
            }
        }

        //
        // Start loading texture dependencies
        //
        m_pInformedRSC->getMaterialRef().getAsset();

        m_LoadingState = LOADING_STATE_PULLING_RESOURCES;

        return TRUE;
    }

    virtual xbool onQTReduceQuality( s32 iQuality )
    {
        //
        // For now we have not quality that we can change to
        //
        ASSERT( iQuality == 0 );
        return TRUE;
    }
};


//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// types
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

eng_resource_base* eng_material_type::CreateInstance( void )
{

    return x_new( eng_material_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_material_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_material_rsc_base*)pEntry );
}

//---------------------------------------------------------------------------------

eng_resource_base* eng_shader_type::CreateInstance( void )
{

    return x_new( eng_shader_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_shader_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_shader_rsc_base*)pEntry );
}

//---------------------------------------------------------------------------------

eng_resource_base* eng_informed_type::CreateInstance( void )
{

    return x_new( eng_informed_rsc_base, 1, XMEM_FLAG_ALIGN_16B );
}

//---------------------------------------------------------------------------------

void eng_informed_type::KillInstance( eng_resource_base* pEntry )
{
    x_delete( (eng_informed_rsc_base*)pEntry );
}

