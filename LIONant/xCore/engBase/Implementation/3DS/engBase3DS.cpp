#include <nn.h>
#include "x_base.h"
#include "eng_Base.h"
#include "engBase3DS.h"

extern "C"
{
    struct Point
    {
        int         x;
        int         y;

        Point()
        {
            x       = 0;
            y       = 0;
        }
    };

    struct DisplayBuffer
    {
        GLenum      m_TargetDisplay;
        GLuint      m_BufferId[2];
        u32         m_CurrentBufferIndex;

        DisplayBuffer()
        {
            m_TargetDisplay         = NN_GX_DISPLAY0;
            m_BufferId[0]           = 0;
            m_BufferId[1]           = 0;
            m_CurrentBufferIndex    = 0;
        }
    };

    struct input_3ds
    {
        nn::hid::PadStatus              m_PadStatus;
        nn::hid::CTR::TouchPanelStatus  m_TouchPanelStatus;
        nn::hid::CTR::TouchPanelReader  m_TouchPanelReader;
        nn::hid::PadReader              m_PadReader;
    };

    // Touch pad related stuff
    static int              s_TouchPadDown          = 0;
    static int              s_TouchPadWasPress      = 0;
    static Point            s_TouchValueABS;
    static Point            s_TouchValueLast;

    // Rendering related stuff
    static DisplayBuffer    s_DisplayBuffers[3];
    static GLuint           s_MainFrameBuffer       = 0;
    static GLuint           s_MainRenderBuffers[2]  = { 0 };
    static xbool            s_LCDReady              = FALSE;
    static GLuint           s_CommandList           = 0;
    static xbool            s_UseStereoDisplay      = FALSE;
    static nn::fnd::ThreadSafeExpHeap
                            s_DeviceHeap;

    static input_3ds*       s_Input3DS = NULL;

    const s32               PAD_BUTTON_MAP[32]      =
    {
        eng_gamepad::BTN_3DS_A,         // BUTTON_A
        eng_gamepad::BTN_3DS_B,         // BUTTON_B
        -1,                             // PSEUDO_BUTTON_SELECT
        eng_gamepad::BTN_3DS_START,     // BUTTON_START
        eng_gamepad::BTN_3DS_RIGHT,     // BUTTON_RIGHT
        eng_gamepad::BTN_3DS_LEFT,      // BUTTON_LEFT
        eng_gamepad::BTN_3DS_UP,        // BUTTON_UP
        eng_gamepad::BTN_3DS_DOWN,      // BUTTON_DOWN
        eng_gamepad::BTN_3DS_R,         // BUTTON_R
        eng_gamepad::BTN_3DS_L,         // BUTTON_L
        eng_gamepad::BTN_3DS_X,         // BUTTON_X
        eng_gamepad::BTN_3DS_Y,         // BUTTON_Y
    };

    struct gamepad
    {
        xbool               m_ButtonIsDown[eng_gamepad::BTN_END_ENUMERATION];
        xbool               m_ButtonWasDown[eng_gamepad::BTN_END_ENUMERATION];
        f32                 m_Stick[2];
    };

    static gamepad          s_GamePad = {0};

    //----------------------------------------------------------------------------------

    int _eng_DrawInCorrectContext( void )
    {
        // IN order for draw to work it must be render in the main context
        // because opengl es does not support vertex buffers from different contexts
        return &x_GetMainThread() == &x_GetCurrentThread();
    }

    //----------------------------------------------------------------------------------

    void _eng_BindMainBuffer( void )
    {
        // We must be sure that the context is set as well...
        glBindFramebuffer(GL_FRAMEBUFFER, s_MainFrameBuffer);
    }

    //----------------------------------------------------------------------------------

    void _eng_GetWindowSize   ( eng_hwin Handle, int* wid, int* hei )
    {
        *wid = 420;
        *hei = 240;
    }

    //---------------------------------------------------------------------------------------

    xbool _eng_HandleEvents( void )
    {
        s_Input3DS->m_TouchPanelReader.ReadLatest(&s_Input3DS->m_TouchPanelStatus);
        s_Input3DS->m_PadReader.ReadLatest(&s_Input3DS->m_PadStatus);

        if (s_Input3DS->m_TouchPanelStatus.touch)
        {
            s_TouchValueLast = s_TouchValueABS;
            if ( !s_TouchPadDown )
            {
                s_TouchValueLast.x = s_Input3DS->m_TouchPanelStatus.x;
                s_TouchValueLast.y = s_Input3DS->m_TouchPanelStatus.y;
            }
            s_TouchPadDown = TRUE;
            s_TouchValueABS.x = s_Input3DS->m_TouchPanelStatus.x;
            s_TouchValueABS.y = s_Input3DS->m_TouchPanelStatus.y;

        }
        else
        {
            if ( s_TouchPadDown )
            {
                s_TouchPadWasPress = 1;
            }
            else
            {
                s_TouchPadWasPress= 0;
            }
            s_TouchPadDown = 0;
            s_TouchValueLast = s_TouchValueABS;
        }

        s32 PadButtonCount = (s32)(sizeof(PAD_BUTTON_MAP) / sizeof(s32));
        for ( s32 i = 0; i < PadButtonCount; i++ )
        {
            if ( -1 != PAD_BUTTON_MAP[i] )
            {
                s_GamePad.m_ButtonIsDown[PAD_BUTTON_MAP[i]] =  0 != (s_Input3DS->m_PadStatus.hold & (1 << i));
                s_GamePad.m_ButtonWasDown[PAD_BUTTON_MAP[i]] =  (0 != (s_Input3DS->m_PadStatus.release & (1 << i)));
            }
        }

        s_GamePad.m_Stick[0] = (f32)s_Input3DS->m_PadStatus.stick.x / (f32)nn::hid::CTR::LIMIT_OF_STICK_CLAMP_MAX;
        s_GamePad.m_Stick[1] = (f32)s_Input3DS->m_PadStatus.stick.y / (f32)nn::hid::CTR::LIMIT_OF_STICK_CLAMP_MAX;

        return TRUE;
    }

    //---------------------------------------------------------------------------------------

    void _eng_PageFlip( eng_context* Context, xbool WaitSync )
    {
        eng_hwin Handle = Context->getWindowHandle();
        DisplayBuffer& CurrentBuffer = s_DisplayBuffers[Handle];

        nngxActiveDisplay(CurrentBuffer.m_TargetDisplay);
        eng_CheckForError();
        nngxBindDisplaybuffer(CurrentBuffer.m_BufferId[CurrentBuffer.m_CurrentBufferIndex]);
        eng_CheckForError();
        // Copy image from color buffer to display buffer
        nngxTransferRenderImage(
            CurrentBuffer.m_BufferId[CurrentBuffer.m_CurrentBufferIndex],   // buffer id
            NN_GX_ANTIALIASE_NOT_USED,                                      // mode
            GL_FALSE,                                                       // yflip
            0,                                                              // colorx
            0                                                               // colory
        );

        nngxWaitCmdlistDone();

        if ( s_UseStereoDisplay && NN_GX_DISPLAY0_EXT == CurrentBuffer.m_TargetDisplay )
        {
            nngxSwapBuffers(NN_GX_DISPLAY0);
        }
        else
        {
            nngxSwapBuffers(CurrentBuffer.m_TargetDisplay);
        }
        eng_CheckForError();
        CurrentBuffer.m_CurrentBufferIndex = (CurrentBuffer.m_CurrentBufferIndex + 1) % 2;

        if ( WaitSync )
        {
            nngxSwapBuffers(NN_GX_DISPLAY_BOTH);
            nngxWaitVSync(NN_GX_DISPLAY_BOTH);
            nngxClearCmdlist();
            nngxRunCmdlist();
        }
        if ( !s_LCDReady )
        {
            s_LCDReady = TRUE;
            nngxStartLcdDisplay();
        }
        eng_CheckForError();
    }

    //----------------------------------------------------------------------------------

    void _eng_SetCurrentContext( eng_context* Context )
    {
        ASSERTS(false, "Rendering on other thread is not supported on 3DS");
    }

    //---------------------------------------------------------------------------------------

    int _eng_ThouchPadIsButtons( int KeyCode )
    {
        if (KeyCode == eng_touchpad::BTN_TOUCH_1)
        {
             return s_TouchPadDown;
        }
        else
        {
            return 0;
        }
       
    }

    //---------------------------------------------------------------------------------------

    int _eng_ThouchPadWasButtons( int KeyCode )
    {
        if ( KeyCode == eng_touchpad::BTN_TOUCH_1 )
        {
            return s_TouchPadWasPress;
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------------------------------

    void _eng_ThouchPadValue( int KeyCode, float* X, float* Y )
    {
        if ( KeyCode == eng_touchpad::ANALOG_TOUCH_1_REL )
        {
            *X = s_TouchValueABS.x - s_TouchValueLast.x;
            *Y = s_TouchValueABS.y - s_TouchValueLast.y;
        }
        else if ( KeyCode == eng_touchpad::ANALOG_TOUCH_1_ABS )
        {
            *X = s_TouchValueABS.x;
            *Y = s_TouchValueABS.y;
        }
        else
        {
            *X = 0;
            *Y = 0;
        }
    }

    //---------------------------------------------------------------------------------------
    uptr s_VramAAddress = NULL;
    uptr s_VramBAddress = NULL;

    void _eng_InitializeAllocator()
    {
        s_VramAAddress = nn::gx::GetVramStartAddr( nn::gx::MEM_VRAMA );
        s_VramBAddress = nn::gx::GetVramStartAddr( nn::gx::MEM_VRAMB );

        s_DeviceHeap.Initialize( nn::os::GetDeviceMemoryAddress(), nn::os::GetDeviceMemorySize() );
    }

    //---------------------------------------------------------------------------------------
    void* _eng_AllocateDevice(size_t Size, s32 Alignment)
    {
        return s_DeviceHeap.Allocate(Size, Alignment);
    }

    //---------------------------------------------------------------------------------------
    void _eng_FreeDevice(void* Pointer)
    {
        return s_DeviceHeap.Free(Pointer);
    }

    inline uptr _eng_RoundupPtr(uptr Address, int Base)
    {
        return ((Address) + ((Base)-1)) & ~((Base)-1);
    }

    void* _eng_AllocateVram(GLenum area, GLenum aim, GLuint id, GLsizei size)
    {
        NN_UNUSED_VAR( id );

        if( size == 0 )
        {
            return NULL;
        }

        int     addrAlign   = 8;
        void *  resultAddr  = NULL;
        u32     Flags       = 0;
        // Take into account the alignment restrictions on each data set's position
        switch( aim )
        {
        case NN_GX_MEM_SYSTEM:          
            addrAlign = 4;
            Flags = XMEM_FLAG_ALIGN_4B;
            break;
        case NN_GX_MEM_TEXTURE:         
            addrAlign = 128;
            Flags = XMEM_FLAG_ALIGN_128B;
            break;
        case NN_GX_MEM_VERTEXBUFFER:
            addrAlign = 16;
            Flags = XMEM_FLAG_ALIGN_16B;
            break;
        case NN_GX_MEM_RENDERBUFFER:
            addrAlign = 64;
            Flags = XMEM_FLAG_ALIGN_64B;
            break;
        case NN_GX_MEM_DISPLAYBUFFER:
            addrAlign = 16;
            Flags = XMEM_FLAG_ALIGN_16B;
            break;
        case NN_GX_MEM_COMMANDBUFFER:
            addrAlign = 16;
            Flags = XMEM_FLAG_ALIGN_16B;
            break;
        default:
            NN_ASSERTMSG( 0,"invalid parameter. (0x%X)\n", aim );
            break;
        }

        switch( area )
        {
        case NN_GX_MEM_FCRAM:
            // When using FCRAM, no need to manage this because it is allocated from the expanded heap that is supported by the SDK
            resultAddr = _eng_AllocateDevice(size,addrAlign);
            break;

        case NN_GX_MEM_VRAMA:
            {
                if( _eng_RoundupPtr( s_VramAAddress, addrAlign ) + size > nn::gx::GetVramEndAddr( nn::gx::MEM_VRAMA ) )
                {
                    NN_ASSERTMSG( 0,"Lack of resources in VRAM-A.\n" );
                }

                s_VramAAddress = _eng_RoundupPtr( s_VramAAddress, addrAlign );
                resultAddr = reinterpret_cast<void*>( s_VramAAddress );
                s_VramAAddress += size;
            }
            break;

        case NN_GX_MEM_VRAMB:
            {
                if( _eng_RoundupPtr( s_VramBAddress, addrAlign ) + size > nn::gx::GetVramEndAddr( nn::gx::MEM_VRAMB ) )
                {
                    NN_ASSERTMSG( 0,"Lack of resources in VRAM-B.\n" );
                }

                s_VramBAddress = _eng_RoundupPtr( s_VramBAddress, addrAlign );
                resultAddr = reinterpret_cast<void*>( s_VramBAddress );                    
                s_VramBAddress += size;
            }
            break;

        default:
            NN_ASSERTMSG( 0, "invalid parameter. (0x%X)\n", area );
            break;
        }

        NN_ASSERT( resultAddr );

        return resultAddr;
    }

    //------------------------------------------------------------------------------

    bool _eng_IsDeviceMemory(const void* memory)
    {
        static u32 start    = nn::os::GetDeviceMemoryAddress();
        static u32 end      = nn::os::GetDeviceMemoryAddress() + nn::os::GetDeviceMemorySize();
        return (start <= (u32)memory) && ((u32)memory <= end);
    }

    //----------------------------------------------------------------------------------------
    void _eng_DeallocateVram(GLenum area, GLenum aim, GLuint id, void *addr)
    {
        NN_UNUSED_VAR( aim );
        NN_UNUSED_VAR( id );

        switch( area )
        {
        case NN_GX_MEM_FCRAM:
            if ( _eng_IsDeviceMemory(addr) )
            {
               _eng_FreeDevice(addr);
            }
            break;
        }
    }
#undef new
#undef delete
    //----------------------------------------------------------------------------------------
    void _eng_Init( void )
    {
        _eng_InitializeAllocator();
        nngxInitialize(_eng_AllocateVram, _eng_DeallocateVram);

        nngxGenCmdlists(1, &s_CommandList);
        nngxBindCmdlist(s_CommandList);
        nngxCmdlistStorage(0x80000, 128);
        nngxRunCmdlist();

        // Initialize main frame buffer and render buffer
        glGenFramebuffers(1, &s_MainFrameBuffer);
        glGenRenderbuffers(2, s_MainRenderBuffers);
        glBindFramebuffer(GL_FRAMEBUFFER, s_MainFrameBuffer);

        glBindRenderbuffer(GL_RENDERBUFFER, s_MainRenderBuffers[0]);
        glRenderbufferStorage(GL_RENDERBUFFER | NN_GX_MEM_VRAMA, GL_RGBA8_OES, nn::gx::DISPLAY0_WIDTH, nn::gx::DISPLAY0_HEIGHT);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, s_MainRenderBuffers[0]);

        glBindRenderbuffer(GL_RENDERBUFFER, s_MainRenderBuffers[1]);
        glRenderbufferStorage(GL_RENDERBUFFER | NN_GX_MEM_VRAMB, GL_DEPTH24_STENCIL8_EXT, nn::gx::DISPLAY0_WIDTH, nn::gx::DISPLAY0_HEIGHT);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, s_MainRenderBuffers[1]);

        nn::hid::CTR::Initialize();

        s_Input3DS = new input_3ds();
    }

    //----------------------------------------------------------------------------------------
    void _eng_Kill( void )
    {
        nn::hid::CTR::Finalize();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDeleteFramebuffers(1, &s_MainFrameBuffer);
        s_MainFrameBuffer = 0;

        glDeleteRenderbuffers(2, s_MainRenderBuffers);

        nngxStopCmdlist();
        nngxDeleteCmdlists(1, &s_CommandList);

        delete s_Input3DS;
        s_Input3DS = NULL;
    }

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER
    //----------------------------------------------------------------------------------------
    void _eng_InitContext (  eng_context* Context )
    {
        eng_hwin Handle = Context->getWindowHandle();

        GLenum DisplayTarget = NN_GX_DISPLAY0;
        switch (Handle)
        {
        case SCREEN_TOP:
            DisplayTarget = NN_GX_DISPLAY0;
            break;
        case SCREEN_BOTTOM:
            DisplayTarget = NN_GX_DISPLAY1;
            break;
        case SCREEN_EXT:
            DisplayTarget = NN_GX_DISPLAY0_EXT;
            break;
        default:
            ASSERTS(false, "Invalid eng_hwin is found when initializing context!");
            break;
        }

        s32 ScreenWidth = 0;
        s32 ScreenHeight = 0;
        Context->GetScreenResolution(ScreenWidth, ScreenHeight);

        s_DisplayBuffers[Handle].m_TargetDisplay = DisplayTarget;

        nngxGenDisplaybuffers(2, s_DisplayBuffers[Handle].m_BufferId);
        nngxActiveDisplay(DisplayTarget);
        nngxDisplayEnv(0, 0);
        nngxBindDisplaybuffer(s_DisplayBuffers[Handle].m_BufferId[0]);
        nngxDisplaybufferStorage(GL_RGB8_OES, ScreenWidth, ScreenHeight, NN_GX_MEM_FCRAM);
        nngxBindDisplaybuffer(s_DisplayBuffers[Handle].m_BufferId[1]);
        nngxDisplaybufferStorage(GL_RGB8_OES, ScreenWidth, ScreenHeight, NN_GX_MEM_FCRAM);

        if ( Handle == SCREEN_EXT )
        {
            nngxSetDisplayMode( NN_GX_DISPLAYMODE_STEREO );
            s_UseStereoDisplay = TRUE;
        }
        else if ( !s_UseStereoDisplay )
        {
            nngxSetDisplayMode( NN_GX_DISPLAYMODE_NORMAL );
        }
    }

    //----------------------------------------------------------------------------------------
    void _eng_KillContext ( eng_context* Context )
    {
        eng_hwin Handle = Context->getWindowHandle();

        GLenum DisplayTarget = NN_GX_DISPLAY0;
        switch (Handle)
        {
        case SCREEN_TOP:
            DisplayTarget = NN_GX_DISPLAY0;
            break;
        case SCREEN_BOTTOM:
            DisplayTarget = NN_GX_DISPLAY1;
            break;
        case SCREEN_EXT:
            DisplayTarget = NN_GX_DISPLAY0_EXT;
            break;
        default:
            ASSERTS(false, "Invalid eng_hwin is found when initializing context!");
            break;
        }

        nngxDeleteDisplaybuffers(2, s_DisplayBuffers[Handle].m_BufferId);
    }

    //----------------------------------------------------------------------------------------
    eng_hwin _eng_CreateWindow( s32 X, s32 Y, s32 Width, s32 Height )
    {
        ASSERTS(false, "eng_CreateWindow is not supported on 3DS!");
        return eng_hwin(-1);
    }

    //----------------------------------------------------------------------------------------
    void _eng_KillWindow( eng_hwin Handle )
    {
    }

    //----------------------------------------------------------------------------------------
    void _eng_SetupShaderProgram2D( GLuint Program )
    {
        glUseProgram(Program);
        glUniform1i(glGetUniformLocation(Program, "dmp_TexEnv[2].combineRgb"), GL_MULT_ADD_DMP);
        glUniform1i(glGetUniformLocation(Program, "dmp_TexEnv[2].combineAlpha"), GL_MULT_ADD_DMP);
        glUniform3i(glGetUniformLocation(Program, "dmp_TexEnv[2].operandRgb"), GL_SRC_COLOR, GL_SRC_COLOR, GL_SRC_COLOR);
        glUniform3i(glGetUniformLocation(Program, "dmp_TexEnv[2].operandAlpha"), GL_SRC_ALPHA, GL_SRC_ALPHA, GL_SRC_ALPHA);
        glUniform3i(glGetUniformLocation(Program, "dmp_TexEnv[2].srcRgb"), GL_PRIMARY_COLOR, GL_TEXTURE0, GL_CONSTANT);
        glUniform3i(glGetUniformLocation(Program, "dmp_TexEnv[2].srcAlpha"), GL_PRIMARY_COLOR, GL_TEXTURE0, GL_CONSTANT);
        glUniform4f(glGetUniformLocation(Program, "dmp_TexEnv[2].constRgba"), 0.0f, 0.0f, 0.0f, 0.0f);
        glUniform1i(glGetUniformLocation(Program, "dmp_Texture[0].samplerType"), GL_TEXTURE_2D);
        glUseProgram(0);
    }

    //----------------------------------------------------------------------------------------
    int _eng_IsButtonDown(int KeyCode )
    {
        return s_GamePad.m_ButtonIsDown[KeyCode];
    }

    //----------------------------------------------------------------------------------------
    int _eng_WasButtonDown(int KeyCode )
    {
        return s_GamePad.m_ButtonWasDown[KeyCode];
    }

    //----------------------------------------------------------------------------------------
    void _eng_GamePadValue( int KeyCode, float* X, float* Y )
    {
        *X = s_GamePad.m_Stick[0];
        *Y = s_GamePad.m_Stick[1];
    }
};