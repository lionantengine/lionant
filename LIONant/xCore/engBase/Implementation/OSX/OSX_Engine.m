#import <Cocoa/Cocoa.h>
//#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>

//////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES
//////////////////////////////////////////////////////////////////////////////////////////


//
// Copy and Past from the eng_input.h
//
enum keyboard_digital_gadget
{
    KEY_ESCAPE           = 0x01 ,
    KEY_1                = 0x02 ,
    KEY_2                = 0x03 ,
    KEY_3                = 0x04 ,
    KEY_4                = 0x05 ,
    KEY_5                = 0x06 ,
    KEY_6                = 0x07 ,
    KEY_7                = 0x08 ,
    KEY_8                = 0x09 ,
    KEY_9                = 0x0A ,
    KEY_0                = 0x0B ,
    KEY_MINUS            = 0x0C ,   // - on main keyboard
    KEY_EQUALS           = 0x0D ,
    KEY_PLUS             = 0x0D ,
    KEY_BACKSPACE        = 0x0E ,    // backspace
    KEY_TAB              = 0x0F ,
    KEY_Q                = 0x10 ,
    KEY_W                = 0x11 ,
    KEY_E                = 0x12 ,
    KEY_R                = 0x13 ,
    KEY_T                = 0x14 ,
    KEY_Y                = 0x15 ,
    KEY_U                = 0x16 ,
    KEY_I                = 0x17 ,
    KEY_O                = 0x18 ,
    KEY_P                = 0x19 ,
    KEY_LBRACKET         = 0x1A ,
    KEY_RBRACKET         = 0x1B ,
    KEY_RETURN           = 0x1C ,   // Enter on main keyboard
    KEY_ENTER            = 0x1C ,
    KEY_LCONTROL         = 0x1D ,
    KEY_A                = 0x1E ,
    KEY_S                = 0x1F ,
    KEY_D                = 0x20 ,
    KEY_F                = 0x21 ,
    KEY_G                = 0x22 ,
    KEY_H                = 0x23 ,
    KEY_J                = 0x24 ,
    KEY_K                = 0x25 ,
    KEY_L                = 0x26 ,
    KEY_SEMICOLON        = 0x27 ,    // ;
    KEY_APOSTROPHE       = 0x28 ,    // '
    KEY_SINGLEQUOTE      = 0x28 ,
    KEY_GRAVE            = 0x29 ,    // ' accent grave
    KEY_TILDE            = 0x29 ,    // ~
    KEY_LSHIFT           = 0x2A ,
    KEY_BACKSLASH        = 0x2B ,
    KEY_Z                = 0x2C ,
    KEY_X                = 0x2D ,
    KEY_C                = 0x2E ,
    KEY_V                = 0x2F ,
    KEY_B                = 0x30 ,
    KEY_N                = 0x31 ,
    KEY_M                = 0x32 ,
    KEY_COMMA            = 0x33 ,
    KEY_PERIOD           = 0x34 ,    // . on main keyboard
    KEY_SLASH            = 0x35 ,    // / on main keyboard
    KEY_RSHIFT           = 0x36 ,
    KEY_MULTIPLY         = 0x37 ,    // * on numeric keypad
    KEY_LMENU            = 0x38 ,    // left Alt
    KEY_LALT             = 0x38 ,
    KEY_SPACE            = 0x39 ,
    KEY_CAPITAL          = 0x3A ,
    KEY_CAPSLOCK         = 0x3A ,
    KEY_F1               = 0x3B ,
    KEY_F2               = 0x3C ,
    KEY_F3               = 0x3D ,
    KEY_F4               = 0x3E ,
    KEY_F5               = 0x3F ,
    KEY_F6               = 0x40 ,
    KEY_F7               = 0x41 ,
    KEY_F8               = 0x42 ,
    KEY_F9               = 0x43 ,
    KEY_F10              = 0x44 ,
    KEY_NUMLOCK          = 0x45 ,
    KEY_SCROLL           = 0x46 ,    // Scroll Lock
    KEY_SCROLLLOCK       = 0x46 ,
    KEY_NUMPAD7          = 0x47 ,
    KEY_NUMPAD8          = 0x48 ,
    KEY_NUMPAD9          = 0x49 ,
    KEY_SUBTRACT         = 0x4A ,    // - on numeric keypad
    KEY_NUMPAD4          = 0x4B ,
    KEY_NUMPAD5          = 0x4C ,
    KEY_NUMPAD6          = 0x4D ,
    KEY_ADD              = 0x4E ,    // + on numeric keypad
    KEY_NUMPAD1          = 0x4F ,
    KEY_NUMPAD2          = 0x50 ,
    KEY_NUMPAD3          = 0x51 ,
    KEY_NUMPAD0          = 0x52 ,
    KEY_DECIMAL          = 0x53 ,    // . on numeric keypad
    KEY_OEM_102          = 0x56 ,    // < > | on UK/Germany keyboards
    KEY_F11              = 0x57 ,
    KEY_F12              = 0x58 ,
    KEY_F13              = 0x64 ,    //                     (NEC PC98,
    KEY_F14              = 0x65 ,    //                     (NEC PC98,
    KEY_F15              = 0x66 ,    //                     (NEC PC98,
    KEY_KANA             = 0x70 ,    // (Japanese keyboard,
    KEY_ABNT_C1          = 0x73 ,    // / ? on Portugese (Brazilian, keyboards
    KEY_CONVERT          = 0x79 ,    // (Japanese keyboard,
    KEY_NOCONVERT        = 0x7B ,    // (Japanese keyboard,
    KEY_YEN              = 0x7D ,    // (Japanese keyboard,
    KEY_ABNT_C2          = 0x7E ,    // Numpad . on Portugese (Brazilian, keyboards
    KEY_NUMPADEQUALS     = 0x8D ,    // = on numeric keypad (NEC PC98,
    KEY_PREVTRACK        = 0x90 ,    // Previous Track (DEFINE_GADGET_VALUE n Japanese keyboard,
    KEY_AT               = 0x91 ,    //                     (NEC PC98,
    KEY_COLON            = 0x92 ,    //                     (NEC PC98,
    KEY_UNDERLINE        = 0x93 ,    //                     (NEC PC98,
    KEY_KANJI            = 0x94 ,    // (Japanese keyboard,
    KEY_STOP             = 0x95 ,    //                     (NEC PC98,
    KEY_AX               = 0x96 ,    //                     (Japan AX,
    KEY_UNLABELED        = 0x97 ,    //                        (J3100,
    KEY_NEXTTRACK        = 0x99 ,    // Next Track
    KEY_NUMPADENTER      = 0x9C ,    // Enter on numeric keypad
    KEY_RCONTROL         = 0x9D ,
    KEY_MUTE             = 0xA0 ,    // Mute
    KEY_CALCULATOR       = 0xA1 ,    // Calculator
    KEY_PLAYPAUSE        = 0xA2 ,    // Play / Pause
    KEY_MEDIASTOP        = 0xA4 ,    // Media Stop
    KEY_VOLUMEDOWN       = 0xAE ,    // Volume -
    KEY_VOLUMEUP         = 0xB0 ,    // Volume +
    KEY_WEBHOME          = 0xB2 ,    // Web home
    KEY_NUMPADCOMMA      = 0xB3 ,    //  , on numeric keypad (NEC PC98,
    KEY_DIVIDE           = 0xB5 ,    // / on numeric keypad
    KEY_SYSRQ            = 0xB7 ,
    KEY_RMENU            = 0xB8 ,    // right Alt
    KEY_RALT             = 0xB8 ,
    KEY_PAUSE            = 0xC5 ,    // Pause
    KEY_PAUSEBREAK       = 0xC5 ,
    KEY_HOME             = 0xC7 ,    // Home on arrow keypad
    KEY_UP               = 0xC8 ,    // UpArrow on arrow keypad
    KEY_PRIOR            = 0xC9 ,    // PgUp on arrow keypad
    KEY_PAGEUP           = 0xC9 ,
    KEY_LEFT             = 0xCB ,    // LeftArrow on arrow keypad
    KEY_RIGHT            = 0xCD ,    // RightArrow on arrow keypad
    KEY_END              = 0xCF ,    // End on arrow keypad
    KEY_DOWN             = 0xD0 ,    // DownArrow on arrow keypad
    KEY_NEXT             = 0xD1 ,    // PgDn on arrow keypad
    KEY_PAGEDOWN         = 0xD1 ,
    KEY_INSERT           = 0xD2 ,    // Insert on arrow keypad
    KEY_DELETE           = 0xD3 ,    // Delete on arrow keypad
    KEY_LWIN             = 0xDB ,    // Left Windows key
    KEY_LCOMMAND         = 0xDB ,
    KEY_RWIN             = 0xDC ,    // Right Windows key
    KEY_RCOMMAND         = 0xDC ,
    KEY_APPS             = 0xDD ,    // AppMenu key
    KEY_POWER            = 0xDE ,    // System Power
    KEY_SLEEP            = 0xDF ,    // System Sleep
    KEY_WAKE             = 0xE3 ,    // System Wake
    KEY_WEBSEARCH        = 0xE5 ,    // Web Search
    KEY_WEBFAVORITES     = 0xE6 ,    // Web Favorites
    KEY_WEBREFRESH       = 0xE7 ,    // Web Refresh
    KEY_WEBSTOP          = 0xE8 ,    // Web Stop
    KEY_WEBFORWARD       = 0xE9 ,    // Web Forward
    KEY_WEBBACK          = 0xEA ,    // Web Back
    KEY_MYCOMPUTER       = 0xEB ,    // My Computer
    KEY_MAIL             = 0xEC ,    // Mail
    KEY_MEDIASELECT      = 0xED ,    // Media Select
    KEY_PRINTSCRN,
    
    KEY_END_ENUMERATION
};

//
// Mapping from unicode to actual engine key
//
static const int s_NormalKeyCode[256]=
{
	0,                 // 0
	0,                 // 1
	0,                 // 2
	0,                 // 3
	0,                 // 4
	0,                 // 5
	0,                 // 6
	0,                 // 7
	0,                 // 8
	KEY_TAB,           // 9
	0,                 // 10
	0,                 // 11
	0,                 // 12
	KEY_RETURN,        // 13
	0,                 // 14
	0,                 // 15
	0,                 // 16
	0,                 // 17
	0,                 // 18
	0,                 // 19
	0,                 // 20
	0,                 // 21
	0,                 // 22
	0,                 // 23
	0,                 // 24
	0,                 // 25
	0,                 // 26
	KEY_ESCAPE,        // 27
	0,                 // 28
	0,                 // 29
	0,                 // 30
	0,                 // 31
	KEY_SPACE,         // 32
	0,                 // 33
	0,                 // 34
	0,                 // 35
	0,                 // 36
	0,                 // 37
	0,                 // 38
	KEY_APOSTROPHE,    // 39
	0,                 // 40
	0,                 // 41
	0,                 // 42
	KEY_PLUS,          // 43
	KEY_COMMA,         // 44
	KEY_MINUS,         // 45
	KEY_PERIOD,        // 46
	KEY_SLASH,         // 47
	KEY_0,             // 48
	KEY_1,             // 49
	KEY_2,             // 50
	KEY_3,             // 51
	KEY_4,             // 52
	KEY_5,             // 53
	KEY_6,             // 54
	KEY_7,             // 55
	KEY_8,             // 56
	KEY_9,             // 57
	KEY_SEMICOLON,     // 58
	0,                 // 59
	0,                 // 60
	0,                 // 61
	0,                 // 62
	0,                 // 63
	0,                 // 64
	KEY_A,             // 65
	KEY_B,             // 66
	KEY_C,             // 67
	KEY_D,             // 68
	KEY_E,             // 69
	KEY_F,             // 70
	KEY_G,             // 71
	KEY_H,             // 72
	KEY_I,             // 73
	KEY_J,             // 74
	KEY_K,             // 75
	KEY_L,             // 76
	KEY_M,             // 77
	KEY_N,             // 78
	KEY_O,             // 79
	KEY_P,             // 80
	KEY_Q,             // 81
	KEY_R,             // 82
	KEY_S,             // 83
	KEY_T,             // 84
	KEY_U,             // 85
	KEY_V,             // 86
	KEY_W,             // 87
	KEY_X,             // 88
	KEY_Y,             // 89
	KEY_Z,             // 90
	KEY_LBRACKET,      // 91
	KEY_BACKSLASH,     // 92
	KEY_RBRACKET,      // 93
	0,                 // 94
	0,                 // 95
	0,                 // 96
	KEY_A,             // 97
	KEY_B,             // 98
	KEY_C,             // 99
	KEY_D,             // 100
	KEY_E,             // 101
	KEY_F,             // 102
	KEY_G,             // 103
	KEY_H,             // 104
	KEY_I,             // 105
	KEY_J,             // 106
	KEY_K,             // 107
	KEY_L,             // 108
	KEY_M,             // 109
	KEY_N,             // 110
	KEY_O,             // 111
	KEY_P,             // 112
	KEY_Q,             // 113
	KEY_R,             // 114
	KEY_S,             // 115
	KEY_T,             // 116
	KEY_U,             // 117
	KEY_V,             // 118
	KEY_W,             // 119
	KEY_X,             // 120
	KEY_Y,             // 121
	KEY_Z,             // 122
	KEY_LBRACKET,      // 123
	0,                 // 124
	KEY_RBRACKET,      // 125
	KEY_TILDE,         // 126
	KEY_BACKSPACE,     // 127
	0,                 // 128
	0,                 // 129
	0,                 // 130
	0,                 // 131
	0,                 // 132
	0,                 // 133
	0,                 // 134
	0,                 // 135
	0,                 // 136
	0,                 // 137
	0,                 // 138
	0,                 // 139
	0,                 // 140
	0,                 // 141
	0,                 // 142
	0,                 // 143
	0,                 // 144
	0,                 // 145
	0,                 // 146
	0,                 // 147
	0,                 // 148
	0,                 // 149
	0,                 // 150
	0,                 // 151
	0,                 // 152
	0,                 // 153
	0,                 // 154
	0,                 // 155
	0,                 // 156
	0,                 // 157
	0,                 // 158
	0,                 // 159
	0,                 // 160
	0,                 // 161
	0,                 // 162
	0,                 // 163
	0,                 // 164
	0,                 // 165
	0,                 // 166
	0,                 // 167
	0,                 // 168
	0,                 // 169
	0,                 // 170
	0,                 // 171
	0,                 // 172
	0,                 // 173
	0,                 // 174
	0,                 // 175
	0,                 // 176
	0,                 // 177
	0,                 // 178
	0,                 // 179
	0,                 // 180
	0,                 // 181
	0,                 // 182
	0,                 // 183
	0,                 // 184
	0,                 // 185
	0,                 // 186
	0,                 // 187
	0,                 // 188
	0,                 // 189
	0,                 // 190
	0,                 // 191
	0,                 // 192
	0,                 // 193
	0,                 // 194
	0,                 // 195
	0,                 // 196
	0,                 // 197
	0,                 // 198
	0,                 // 199
	0,                 // 200
	0,                 // 201
	0,                 // 202
	0,                 // 203
	0,                 // 204
	0,                 // 205
	0,                 // 206
	0,                 // 207
	0,                 // 208
	0,                 // 209
	0,                 // 210
	0,                 // 211
	0,                 // 212
	0,                 // 213
	0,                 // 214
	0,                 // 215
	0,                 // 216
	0,                 // 217
	0,                 // 218
	0,                 // 219
	0,                 // 220
	0,                 // 221
	0,                 // 222
	0,                 // 223
	0,                 // 224
	0,                 // 225
	0,                 // 226
	0,                 // 227
	0,                 // 228
	0,                 // 229
	0,                 // 230
	0,                 // 231
	0,                 // 232
	0,                 // 233
	0,                 // 234
	0,                 // 235
	0,                 // 236
	0,                 // 237
	0,                 // 238
	0,                 // 239
	0,                 // 240
	0,                 // 241
	0,                 // 242
	0,                 // 243
	0,                 // 244
	0,                 // 245
	0,                 // 246
	0,                 // 247
	0,                 // 248
	0,                 // 249
	0,                 // 250
	0,                 // 251
	0,                 // 252
	0,                 // 253
	0,                 // 254
	0                  // 255
};

//
// Key mapping between the special mac key codes and the engine
//
static const int s_SpecialKeyCode[256]=
{
	KEY_UP,           // 0
	KEY_DOWN,         // 1
	KEY_LEFT,         // 2
	KEY_RIGHT,        // 3
	KEY_F1,           // 4
	KEY_F2,           // 5
	KEY_F3,           // 6
	KEY_F4,           // 7
	KEY_F5,           // 8
	KEY_F6,           // 9
	KEY_F7,           // 10
	KEY_F8,           // 11
	KEY_F9,           // 12
	KEY_F10,          // 13
	KEY_F11,          // 14
	KEY_F12,          // 15
	KEY_F1,           // 16 0x10
	KEY_F2,           // 17
	KEY_F3,           // 18
	KEY_F4,           // 19
	KEY_F5,           // 20
	KEY_F6,           // 21
	KEY_F7,           // 22
	KEY_F8,           // 23
	KEY_F9,           // 24
	KEY_F10,          // 25
	KEY_F11,          // 26
	KEY_F12,          // 27
	KEY_F1,           // 28
	KEY_F2,           // 29
	KEY_F3,           // 30
	KEY_F4,           // 31
	KEY_F5,           // 32 0x20
	KEY_F6,           // 33
	KEY_F7,           // 34
	KEY_F8,           // 35
	KEY_F9,           // 36
	KEY_F10,          // 37
	KEY_F11,          // 38
	KEY_INSERT,       // 39
	KEY_DELETE,       // 40
	KEY_HOME,         // 41
	0,                // 42 Begin
	KEY_END,          // 43
	KEY_PAGEUP,       // 44
	KEY_PAGEDOWN,     // 45
	KEY_PRINTSCRN,    // 46
	KEY_SCROLLLOCK,   // 47
	KEY_PAUSEBREAK,   // 48 0x30
	0,                // 49
	KEY_PAUSEBREAK,   // 50
	0,                // 51 Reset
	0,                // 52 Stop
	0,                // 53 Menu
	0,                // 54 User
	0,                // 55 System
	KEY_PRINTSCRN,    // 56
	0,                // 57 Clear line
	0,                // 58 Clear display
	0,                // 59 Insert line
	0,                // 60 Delete line
	KEY_INSERT,       // 61
	KEY_DELETE,       // 62
	KEY_PAGEUP,       // 63
	KEY_PAGEDOWN,     // 64
	0,                  // 65 Select
	0,                  // 66 Execute
	0,                  // 67 Undo
	0,                  // 68 Redo
	0,                  // 69 Find
	0,                  // 70 Help
	0,                  // 71 Mode Switch
	0,                  // 72
	0,                  // 73
	0,                  // 74
	0,                  // 75
	0,                  // 76
	0,                  // 77
	0,                  // 78
	0,                  // 79
	0,                  // 80
	0,                  // 81
	0,                  // 82
	0,                  // 83
	0,                  // 84
	0,                  // 85
	0,                  // 86
	0,                  // 87
	0,                  // 88
	0,                  // 89
	0,                  // 90
	0,                  // 91
	0,                  // 92
	0,                  // 93
	0,                  // 94
	0,                  // 95
	0,                  // 96
	0,                  // 97
	0,                  // 98
	0,                  // 99
	0,                  // 100
	0,                  // 101
	0,                  // 102
	0,                  // 103
	0,                  // 104
	0,                  // 105
	0,                  // 106
	0,                  // 107
	0,                  // 108
	0,                  // 109
	0,                  // 110
	0,                  // 111
	0,                  // 112
	0,                  // 113
	0,                  // 114
	0,                  // 115
	0,                  // 116
	0,                  // 117
	0,                  // 118
	0,                  // 119
	0,                  // 120
	0,                  // 121
	0,                  // 122
	0,                  // 123
	0,                  // 124
	0,                  // 125
	0,                  // 126
	0,                  // 127
	0,                  // 128
	0,                  // 129
	0,                  // 130
	0,                  // 131
	0,                  // 132
	0,                  // 133
	0,                  // 134
	0,                  // 135
	0,                  // 136
	0,                  // 137
	0,                  // 138
	0,                  // 139
	0,                  // 140
	0,                  // 141
	0,                  // 142
	0,                  // 143
	0,                  // 144
	0,                  // 145
	0,                  // 146
	0,                  // 147
	0,                  // 148
	0,                  // 149
	0,                  // 150
	0,                  // 151
	0,                  // 152
	0,                  // 153
	0,                  // 154
	0,                  // 155
	0,                  // 156
	0,                  // 157
	0,                  // 158
	0,                  // 159
	0,                  // 160
	0,                  // 161
	0,                  // 162
	0,                  // 163
	0,                  // 164
	0,                  // 165
	0,                  // 166
	0,                  // 167
	0,                  // 168
	0,                  // 169
	0,                  // 170
	0,                  // 171
	0,                  // 172
	0,                  // 173
	0,                  // 174
	0,                  // 175
	0,                  // 176
	0,                  // 177
	0,                  // 178
	0,                  // 179
	0,                  // 180
	0,                  // 181
	0,                  // 182
	0,                  // 183
	0,                  // 184
	0,                  // 185
	0,                  // 186
	0,                  // 187
	0,                  // 188
	0,                  // 189
	0,                  // 190
	0,                  // 191
	0,                  // 192
	0,                  // 193
	0,                  // 194
	0,                  // 195
	0,                  // 196
	0,                  // 197
	0,                  // 198
	0,                  // 199
	0,                  // 200
	0,                  // 201
	0,                  // 202
	0,                  // 203
	0,                  // 204
	0,                  // 205
	0,                  // 206
	0,                  // 207
	0,                  // 208
	0,                  // 209
	0,                  // 210
	0,                  // 211
	0,                  // 212
	0,                  // 213
	0,                  // 214
	0,                  // 215
	0,                  // 216
	0,                  // 217
	0,                  // 218
	0,                  // 219
	0,                  // 220
	0,                  // 221
	0,                  // 222
	0,                  // 223
	0,                  // 224
	0,                  // 225
	0,                  // 226
	0,                  // 227
	0,                  // 228
	0,                  // 229
	0,                  // 230
	0,                  // 231
	0,                  // 232
	0,                  // 233
	0,                  // 234
	0,                  // 235
	0,                  // 236
	0,                  // 237
	0,                  // 238
	0,                  // 239
	0,                  // 240
	0,                  // 241
	0,                  // 242
	0,                  // 243
	0,                  // 244
	0,                  // 245
	0,                  // 246
	0,                  // 247
	0,                  // 248
	0,                  // 249
	0,                  // 250
	0,                  // 251
	0,                  // 252
	0,                  // 253
	0,                  // 254
	0                   // 255
};


#define INPUTLOG_BUFFER_SIZE 256

//
// Keyboard stuff
//
struct keyboard
{
    int m_KeyIsDown[KEY_END_ENUMERATION];
    int m_KeyWasDown[2][KEY_END_ENUMERATION];
    int m_KeyWasDownIndex;

    int m_nKeyBufUsed;
    int m_KeyBuffer[INPUTLOG_BUFFER_SIZE];
    int m_nCharBufUsed;
    int m_CharBuffer[INPUTLOG_BUFFER_SIZE];
};

static struct keyboard s_Keyboard={0};

//
// Mouse Stuff
//
enum
{
    BTN_LEFT ,
    BTN_MIDDLE ,
    BTN_RIGHT ,
    
    BTN_0 ,
    BTN_1 ,
    BTN_2 ,
    BTN_3 ,
    BTN_4 ,
    
    BTN_END_ENUMERATION
};

enum
{
    ANALOG_POS_REL   ,
    ANALOG_WHEEL_REL ,
    ANALOG_POS_ABS ,
    
    ANALOG_END_ENUMERATION
};

enum
{
	MOUSEEVENT_NONE,
	MOUSEEVENT_LBUTTONDOWN,
	MOUSEEVENT_LBUTTONUP,
	MOUSEEVENT_MBUTTONDOWN,
	MOUSEEVENT_MBUTTONUP,
	MOUSEEVENT_RBUTTONDOWN,
	MOUSEEVENT_RBUTTONUP,
	MOUSEEVENT_MOVE,
    MOUSEEVENT_SCROLLWHEEL
};


struct mouse
{
    struct event_log
    {
        int     m_EventType;
        int     m_LB, m_MB, m_RB;
        CGPoint m_Rel;
        CGPoint m_Wheel;
        CGPoint m_Abs;
    };
    
    int                 m_KeyIsDown[BTN_END_ENUMERATION];
    int                 m_KeyWasDown[2][BTN_END_ENUMERATION];
    int                 m_KeyWasDownIndex;

    int                 m_nLogBufUsed;
    struct event_log    m_LogBuffer[INPUTLOG_BUFFER_SIZE];
    
    CGPoint             m_Analog[ANALOG_END_ENUMERATION];
};

static struct mouse s_Mouse={0};

void _eng_SetCurrentContext( void );

//////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////////////



//--------------------------------------------------------------------------------

static
int MacUnicodeToKeyCode( int uni )
{
	if(0<=uni && uni<256)
	{
		return s_NormalKeyCode[uni];
	}
	else if(0xf700<=uni && uni<0xf800)
	{
		return s_SpecialKeyCode[uni-0xf700];
	}
	return 0;
}

//--------------------------------------------------------------------------------

static int exposure=0;


@interface YsMacDelegate : NSObject /* < NSApplicationDelegate > */
/* Example: Fire has the same problem no explanation */
{
}
/* - (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication; */
@end

@implementation YsMacDelegate
- (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
	return YES;
}
@end


@interface YsOpenGLWindow : NSWindow
{
}

@end

@implementation YsOpenGLWindow
- (id) initWithContentRect: (NSRect)rect styleMask:(NSUInteger)wndStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)deferFlg
{
	(void)[super initWithContentRect:rect styleMask:wndStyle backing:bufferingType defer:deferFlg];
    
	[[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(windowDidResize:)
     name:NSWindowDidResizeNotification
     object:self];
    
	[[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(windowWillClose:)
     name:NSWindowWillCloseNotification
     object:self];
    
	[self setAcceptsMouseMovedEvents:YES];
    
	printf("%s\n",__FUNCTION__);
	return self;
}

- (void) windowDidResize: (NSNotification *)notification
{
}

- (void) windowWillClose: (NSNotification *)notification
{
	[NSApp terminate:nil];	// This can also be exit(0);
}

@end


@interface YsOpenGLView : NSOpenGLView
{
}
- (void) drawRect: (NSRect) bounds;
@end


@implementation YsOpenGLView
-(void) drawRect: (NSRect) bounds
{
	printf("%s\n",__FUNCTION__);
    _eng_SetCurrentContext();
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	exposure=1;
}

-(void) prepareOpenGL
{
	printf("%s\n",__FUNCTION__);
}

-(NSMenu *)menuForEvent: (NSEvent *)theEvent
{
	printf("%s\n",__FUNCTION__);
	return [NSView defaultMenu];
}

//--------------------------------------------------------------------------------

- (void) flagsChanged: (NSEvent *)theEvent
{
	NSUInteger flags = [theEvent modifierFlags];
    
    //
    // Special Key KEY_CAPSLOCK
    //
	if ( flags & NSAlphaShiftKeyMask )
	{
        s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = KEY_CAPSLOCK;
        s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;
        
		s_Keyboard.m_KeyIsDown[KEY_CAPSLOCK]=1;
        s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][KEY_CAPSLOCK] = 1;
	}
	else
	{
		s_Keyboard.m_KeyIsDown[KEY_CAPSLOCK]=0;
	}
    
    //
    // Special Key KEY_SHIFT
    //
	if ( flags & NSShiftKeyMask )
	{
        s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = KEY_LSHIFT;
        s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;
        
        s_Keyboard.m_KeyIsDown[KEY_LSHIFT]=1;
        s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][KEY_LSHIFT] = 1;

	}
	else
	{
		s_Keyboard.m_KeyIsDown[KEY_LSHIFT]=0;
	}
    
    //
    // Special key KEY_CTRL
    //
	if( flags & NSControlKeyMask )
	{
        s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = KEY_LCONTROL;
        s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;
        
        s_Keyboard.m_KeyIsDown[KEY_LCONTROL]=1;
        s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][KEY_LCONTROL] = 1;
	}
	else
	{
		s_Keyboard.m_KeyIsDown[KEY_LCONTROL]=0;
	}
    
    //
    // Special key KEY_ALT
    //
	if( flags & NSAlternateKeyMask )
	{
        s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = KEY_LALT;
        s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;
        
        s_Keyboard.m_KeyIsDown[KEY_LALT]=1;
        s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][KEY_LALT] = 1;
	}
	else
	{
		s_Keyboard.m_KeyIsDown[KEY_LALT]=0;
	}
    
    //
    // Special key KEY_COMMAND
    //
    if( flags & NSCommandKeyMask )
	{
        s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = KEY_LCOMMAND;
        s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;

        s_Keyboard.m_KeyIsDown[KEY_LCOMMAND]=1;
        s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][KEY_LCOMMAND] = 1;
	}
	else
	{
		s_Keyboard.m_KeyIsDown[KEY_LCOMMAND]=0;
	}
    
	// Other possible key masks
	// NSNumericPadKeyMask
	// NSHelpKeyMask
	// NSFunctionKeyMask
	// NSDeviceIndependentModifierFlagsMask
}

//--------------------------------------------------------------------------------

- (void) keyDown:(NSEvent *)theEvent
{
	NSUInteger flags = [theEvent modifierFlags];
    
    //
    // Collect all the characters
    //
	NSString*  chrs  = [theEvent characters];
	if( (0 == (flags & NSCommandKeyMask)) && ([chrs length] > 0) )
	{
		int unicode;
		unicode=[chrs characterAtIndex:0];
        
		if(32<=unicode && unicode<128)
		{
			s_Keyboard.m_CharBuffer[s_Keyboard.m_nCharBufUsed] = unicode;
            s_Keyboard.m_nCharBufUsed = (s_Keyboard.m_nCharBufUsed+1)%INPUTLOG_BUFFER_SIZE;
		}
	}
    
    //
    // Collect all the keys
    //
    NSString* chrsNoMod = [theEvent charactersIgnoringModifiers];
	if( [chrsNoMod length]>0 )
	{
		int unicode = [chrsNoMod characterAtIndex:0];
        int key     = MacUnicodeToKeyCode(unicode);
        
		if( key != 0 )
		{
			s_Keyboard.m_KeyIsDown[key] = 1;
            s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][key] = 1;
            
            s_Keyboard.m_KeyBuffer[s_Keyboard.m_nKeyBufUsed] = key;
            s_Keyboard.m_nKeyBufUsed = (s_Keyboard.m_nKeyBufUsed+1)%INPUTLOG_BUFFER_SIZE;
		}
	}
}

//--------------------------------------------------------------------------------

- (void) keyUp:(NSEvent *)theEvent
{
    //
    // Jus for debugging... can check the key
    //
	NSString* chrs = [theEvent characters];
	if([chrs length]>0)
	{
		int unicode;
		unicode=[chrs characterAtIndex:0];
	}
    
    //
    // Update the state of the keys
    //
	NSString* chrsNoMod = [theEvent charactersIgnoringModifiers];
	if([chrsNoMod length]>0)
	{
		int unicode = [chrsNoMod characterAtIndex:0];
		int key     = MacUnicodeToKeyCode(unicode);
        
		if(key!=0)
		{
			s_Keyboard.m_KeyIsDown[key]=0;
		}
	}
}

//--------------------------------------------------------------------------------

-(void) CreateMouseLogEvent:(NSEvent*) theEvent EventType:(int)EventType
{
    //  NSRect rect = [self frame];
    
    s_Mouse.m_Analog[ ANALOG_POS_ABS ].x = (int)[theEvent locationInWindow].x;
    s_Mouse.m_Analog[ ANALOG_POS_ABS ].y = /*rect.size.height-1-(int)*/[theEvent locationInWindow].y;
    
    // TODO Handle the wheel
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_EventType = EventType;
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_Abs = s_Mouse.m_Analog[ ANALOG_POS_ABS ];
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_Rel = s_Mouse.m_Analog[ ANALOG_POS_REL ];
    
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_LB   = s_Mouse.m_KeyIsDown[ BTN_LEFT ];
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_RB   = s_Mouse.m_KeyIsDown[ BTN_RIGHT ];
    s_Mouse.m_LogBuffer[s_Mouse.m_nLogBufUsed].m_MB   = s_Mouse.m_KeyIsDown[ BTN_MIDDLE ];
    
    s_Mouse.m_nLogBufUsed = (s_Mouse.m_nLogBufUsed+1)%INPUTLOG_BUFFER_SIZE;
}

//--------------------------------------------------------------------------------

-(void) HandleMouseWheel:(NSEvent*) theEvent EventType:(int)EventType
{
    s_Mouse.m_Analog[ ANALOG_POS_REL ].x = 0;
    s_Mouse.m_Analog[ ANALOG_POS_REL ].y = 0;
    
    s_Mouse.m_Analog[ ANALOG_WHEEL_REL ].x = theEvent.deltaX;
    s_Mouse.m_Analog[ ANALOG_WHEEL_REL ].y = -theEvent.deltaY;
    
    [ self CreateMouseLogEvent:theEvent EventType:EventType ];
}

//--------------------------------------------------------------------------------

-(void) HandleMouseEvent:(NSEvent*) theEvent EventType:(int)EventType
{
    s_Mouse.m_Analog[ ANALOG_POS_REL ].x = theEvent.deltaX;
    s_Mouse.m_Analog[ ANALOG_POS_REL ].y = -theEvent.deltaY;

    s_Mouse.m_Analog[ ANALOG_WHEEL_REL ].x = 0;
    s_Mouse.m_Analog[ ANALOG_WHEEL_REL ].y = 0;
    
    [ self CreateMouseLogEvent:theEvent EventType:EventType ];
}

//--------------------------------------------------------------------------------

- (void) mouseMoved:(NSEvent *)theEvent
{
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_MOVE ];
}

//--------------------------------------------------------------------------------

- (void) mouseDragged:(NSEvent *)theEvent
{
    [self mouseMoved:theEvent];
}

//--------------------------------------------------------------------------------

- (void) rightMouseDragged:(NSEvent *)theEvent
{
    [self mouseMoved:theEvent];
}

//--------------------------------------------------------------------------------

- (void) otherMouseDragged:(NSEvent *)theEvent
{
    [self mouseMoved:theEvent];
}

//--------------------------------------------------------------------------------

- (void) mouseDown:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_LEFT ] = 1;
    s_Mouse.m_KeyWasDown[s_Mouse.m_KeyWasDownIndex][ BTN_LEFT ] = 1;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_LBUTTONDOWN ];
}

//--------------------------------------------------------------------------------

- (void) mouseUp:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_LEFT ] = 0;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_LBUTTONUP ];
}

//--------------------------------------------------------------------------------

- (void) rightMouseDown:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_RIGHT ] = 1;
    s_Mouse.m_KeyWasDown[s_Mouse.m_KeyWasDownIndex][ BTN_RIGHT ] = 1;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_RBUTTONDOWN ];
}

//--------------------------------------------------------------------------------

- (void) rightMouseUp:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_RIGHT ] = 0;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_RBUTTONUP ];
}

//--------------------------------------------------------------------------------

- (void) otherMouseDown:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_MIDDLE ] = 1;
    s_Mouse.m_KeyWasDown[s_Mouse.m_KeyWasDownIndex][ BTN_MIDDLE ] = 1;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_MBUTTONDOWN ];
}

//--------------------------------------------------------------------------------

- (void) otherMouseUp:(NSEvent *)theEvent
{
    s_Mouse.m_KeyIsDown[ BTN_MIDDLE ] = 0;
    
    [ self HandleMouseEvent:theEvent EventType:MOUSEEVENT_MBUTTONUP ];
}

//--------------------------------------------------------------------------------

- (void) scrollWheel:(NSEvent *)theEvent
{
    [ self HandleMouseWheel:theEvent EventType:MOUSEEVENT_SCROLLWHEEL ];
}


@end



void YsAddMenu(void)
{
    //	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    
    @autoreleasepool
    {
        NSMenu *mainMenu;
        
        mainMenu=[NSMenu alloc];
        (void)[mainMenu initWithTitle:@"Minimum"];
        
        NSMenuItem *fileMenu;
        fileMenu=[[NSMenuItem alloc] initWithTitle:@"File" action:NULL keyEquivalent:[NSString string]];
        [mainMenu addItem:fileMenu];
        
        NSMenu *fileSubMenu;
        fileSubMenu=[[NSMenu alloc] initWithTitle:@"File"];
        [fileMenu setSubmenu:fileSubMenu];
        
        NSMenuItem *fileMenu_Quit;
        fileMenu_Quit=[[NSMenuItem alloc] initWithTitle:@"Quit"  action:@selector(terminate:) keyEquivalent:@"q"];
        [fileMenu_Quit setTarget:NSApp];
        [fileSubMenu addItem:fileMenu_Quit];
        
        [NSApp setMainMenu:mainMenu];
        
    }
	//[pool release];
}

void YsTestApplicationPath(void)
{
 	// NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    
    @autoreleasepool
    {
        char cwd[256];
        getcwd(cwd,255);
        printf("CWD(Initial): %s\n",cwd);
        
        NSString *path;
        path=[[NSBundle mainBundle] bundlePath];
        printf("BundlePath:%s\n",[path UTF8String]);
        
        [[NSFileManager defaultManager] changeCurrentDirectoryPath:path];
        
        getcwd(cwd,255);
        printf("CWD(Changed): %s\n",cwd);
        
    }
	//[pool release];
}

NSOpenGLContext* glContext=nil;
CGLContextObj    MainContext=nil;

void _eng_SetCurrentContext( void )
{
   // [glContext makeCurrentContext];
    if( MainContext != nil) CGLSetCurrentContext( MainContext );
    
   // GSEventOpenGLContextCreated();
    //[glContext flushBuffer];
    
 //   CGLSetCurrentContext( CGLGetCurrentContext() );
    //[makeCurrentContext];
    
   // [EAGLContext setCurrentContext:context];
   // glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
}

//----------------------------------------------------------------------------------------

int _eng_DrawInCorrectContext( void )
{
    return 1;
}

//----------------------------------------------------------------------------------------

void _eng_BindMainBuffer( void )
{
}

//----------------------------------------------------------------------------------------

static YsOpenGLWindow*  ysWnd  = nil;
static YsOpenGLView*    ysView = nil;

void _eng_CreateWindow( int x0, int y0, int w, int h )
{
    @autoreleasepool
    {
        bool bOK;
        
        NSApplicationLoad();
        
        
        // Create the window
        NSRect contRect;
        contRect = NSMakeRect(x0,y0,w,h);

        unsigned int winStyle=
            NSTitledWindowMask          |
            NSClosableWindowMask        |
            NSMiniaturizableWindowMask  |
            NSResizableWindowMask;

        ysWnd = [YsOpenGLWindow alloc];
        assert(ysWnd);
        bOK = [ysWnd
               initWithContentRect : contRect
               styleMask           : winStyle
               backing             : NSBackingStoreBuffered
               defer               : NO];
        assert(bOK);
        
        // Create the open gl part
        NSOpenGLPixelFormat*           format;
        NSOpenGLPixelFormatAttribute   formatAttrib[]=
        {
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAWindow,
            NSOpenGLPFAAccelerated,
  //          NSOpenGLPFADoubleBuffer,          // Does not seem to work with this one on
            NSOpenGLPFAColorSize,       24,
            NSOpenGLPFAAlphaSize,       8,
            NSOpenGLPFADepthSize,       24,
            NSOpenGLPFAStencilSize,     8,
            
            // These are for AA
         /*   NSOpenGLPFAAccumSize,       0,
            NSOpenGLPFASampleBuffers,   1,
            NSOpenGLPFASamples,         4, */
            0,0
            
            // These are the original flags
            /*
            NSOpenGLPFAWindow,
            NSOpenGLPFADepthSize,(NSOpenGLPixelFormatAttribute)32,
            NSOpenGLPFADoubleBuffer,
            0
             */
        };
        
        format = [NSOpenGLPixelFormat alloc];
        bOK = [format initWithAttributes: formatAttrib];
        assert(bOK);
        
        glContext = [[NSOpenGLContext alloc] initWithFormat: format shareContext: nil];
        [glContext makeCurrentContext];
        
        ysView   = [YsOpenGLView alloc];
        contRect = NSMakeRect(0,0,w,h);
        
        bOK = [ ysView
               initWithFrame   : contRect
               pixelFormat     : format];
        assert( bOK );
        
        [ysWnd setContentView      : ysView];
        [ysWnd makeFirstResponder  : ysView];
        
        [ysWnd makeKeyAndOrderFront:nil];
        [ysWnd makeMainWindow];
        
        [NSApp activateIgnoringOtherApps:YES];
        
        // Create the menu for it
        YsAddMenu();
        
        // Make sure it is visible
        ProcessSerialNumber psn;
        GetCurrentProcess(&psn);
        TransformProcessType(&psn, kProcessTransformToForegroundApplication);
        SetFrontProcess(&psn);
         
        
    }
	//[pool release];
    
    //
    // Initialize all the keys
    //
	int i;
	for(i=0; i<KEY_END_ENUMERATION; i++)
	{
		s_Keyboard.m_KeyIsDown[i]     = 0;
        s_Keyboard.m_KeyWasDown[0][i] = 0;
        s_Keyboard.m_KeyWasDown[1][i] = 0;
	}
    
    
    glClearColor(0.2f,0.2f,0.2f,0.0f);
    glClearDepth(1.0F);
	glDisable(GL_DEPTH_TEST);
    
	glViewport(0,0,w,h);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,(float)w-1,(float)h-1,0,-1,1);
    
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
	glShadeModel(GL_FLAT);
	glPointSize(1);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glColor3ub(0,0,0);
    
    // Enable mulithreaded opengl
    CGLError err = 0;
    CGLContextObj ctx = CGLGetCurrentContext();
    // Enable the multithreading
    err =  CGLEnable( ctx, kCGLCEMPEngine);
    assert(err == kCGLNoError );
    
    MainContext = CGLGetCurrentContext();
    
    // Disable vSync for now...
    // To enable make the interval = 1
    {
        CGLContextObj   ctx = CGLGetCurrentContext();
        const GLint   interval = 0;
        
        CGLSetParameter(ctx, kCGLCPSwapInterval, &interval);
    }
    
    //rootContext = CGLGetCurrentContext();
    //CGLPixelFormatObj pf = CGLGetPixelFormat(rootContext);
    //CGLCreateContext(pf, rootContext, &childContext);
   // (NSGraphicsContext *)currentContext
    
}

//----------------------------------------------------------------------------------------

void _eng_GetWindowSize( void* pPtr, int* wid, int* hei )
{
	NSRect rect;
	rect = [ysView frame];
    
	*wid = rect.size.width;
	*hei = rect.size.height;
}

//----------------------------------------------------------------------------------------

void _eng_HandleEvents(void)
{
    //
    // Swap the mouse buffer and prepare them for next time
    //
    {
        s_Mouse.m_KeyWasDownIndex = 1 - s_Mouse.m_KeyWasDownIndex;
        int i;
        for(i=0; i<KEY_END_ENUMERATION; i++)
        {
            s_Mouse.m_KeyWasDown[s_Mouse.m_KeyWasDownIndex][i] = 0;
        }
        
        s_Mouse.m_Analog[ANALOG_POS_REL].x = 0;
        s_Mouse.m_Analog[ANALOG_POS_REL].y = 0;
        
        s_Mouse.m_Analog[ANALOG_WHEEL_REL].x = 0;
        s_Mouse.m_Analog[ANALOG_WHEEL_REL].y = 0;
    }
    
    //
    // Swap the keyboard buffer and prepare them for next time
    //
    {
        s_Keyboard.m_KeyWasDownIndex = 1 - s_Keyboard.m_KeyWasDownIndex;
        int i;
        for(i=0; i<KEY_END_ENUMERATION; i++)
        {
            s_Keyboard.m_KeyWasDown[s_Keyboard.m_KeyWasDownIndex][i] = 0;
        }
    }
    
    //
    // Start collecting events
    //
    @autoreleasepool
    {
        while(1)
        {
            NSEvent *event;
            event=[NSApp
                   nextEventMatchingMask:NSAnyEventMask
                   untilDate: [NSDate distantPast]
                   inMode: NSDefaultRunLoopMode
                   dequeue:YES];
            
            if(event!=nil)
            {
                [NSApp sendEvent:event];
                [NSApp updateWindows];
                
                if([event type]==NSRightMouseDown)
                {
                    printf("R mouse down event\n");
                }
            }
            else
            {
                break;
            }
        }
    }
}

//----------------------------------------------------------------------------------------

int _eng_IsKeyDown( int KeyCode )
{
    return s_Keyboard.m_KeyIsDown[ KeyCode ];
}

//----------------------------------------------------------------------------------------

int _eng_WasKeyDown( int KeyCode )
{
    return s_Keyboard.m_KeyWasDown[1-s_Keyboard.m_KeyWasDownIndex][ KeyCode ];
}

//----------------------------------------------------------------------------------------

const int* _eng_MouseGetIsButtons( void )
{
    return s_Mouse.m_KeyIsDown;
}

//----------------------------------------------------------------------------------------

const int* _eng_MouseGetWasButtons( void )
{
    return s_Mouse.m_KeyWasDown[1-s_Keyboard.m_KeyWasDownIndex];
}

//----------------------------------------------------------------------------------------

void _eng_MouseGetAnalog( int KeyCode, float* X, float* Y )
{
    *X = s_Mouse.m_Analog[KeyCode].x;
    *Y = s_Mouse.m_Analog[KeyCode].y;
}


void FsSleepC(int ms)
{
	if(ms>0)
	{
		double sec;
		sec=(double)ms/1000.0;
		[NSThread sleepForTimeInterval:sec];
	}
}

int FsPassedTimeC(void)
{
	int ms;
    
    //	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    @autoreleasepool
    {
        
        static NSTimeInterval last=0.0;
        NSTimeInterval now;
        
        now=[[NSDate date] timeIntervalSince1970];
        
        NSTimeInterval passed;
        passed=now-last;
        ms=(int)(1000.0*passed);
        
        if(ms<0)
        {
            ms=1;
        }
        last=now;
        
    }
	//[pool release];
    
	return ms;
}


void _eng_PageFlip( void )
{
    glFlush();
	//[[ysView openGLContext] flushBuffer];
}

void FsChangeToProgramDirC(void)
{
	NSString *path;
	path=[[NSBundle mainBundle] bundlePath];
	printf("BundlePath:%s\n",[path UTF8String]);
    
	[[NSFileManager defaultManager] changeCurrentDirectoryPath:path];
}

int FsCheckExposureC(void)
{
	int ret;
	ret=exposure;
	exposure=0;
	return ret;
}

/* int main(int argc, char *argv[])
 {
 YsTestApplicationPath();
 
 YsOpenWindow();
 
 printf("Going into the event loop\n");
 
 double angle;
 angle=0.0;
 while(1)
 {
 YsPollEvent();
 
 DrawTriangle(angle);
 angle=angle+0.05;
 
 YsSleep(20);
 }
 
 return 0;
 } */
