
#include "eng_base.h"

//--------------------------------------------------------------------------------------------------

using anim_group        = eng_anim_group_rsc::anim_group_rsc;
using decomp_fn         = void (*)( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

//--------------------------------------------------------------------------------------------------

static void DecompS_Identity        ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Identity        ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompT_Identity        ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

static void DecompS_Const           ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Const           ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompT_Const           ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

static void DecompT_Dictionary      ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

static void DecompS_Delta_Low       ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Delta_Low       ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Delta_Mid       ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Delta_High      ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompT_Delta_Low       ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

static void DecompV3_Fullrange      ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );
static void DecompQ_Fullrage        ( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData );

//--------------------------------------------------------------------------------------------------

static decomp_fn s_ScaleDecompFnptr[] = 
{
    DecompS_Identity,
    DecompS_Const,
    NULL,
    DecompS_Delta_Low,
    NULL,
    NULL,
    DecompV3_Fullrange,
};

//--------------------------------------------------------------------------------------------------

static decomp_fn s_RotationDecompFnptr[] = 
{
    DecompQ_Identity,
    DecompQ_Const,
    NULL,
    DecompQ_Delta_Low,
    DecompQ_Delta_Mid,
    DecompQ_Delta_High,
    DecompQ_Fullrage,
};

//--------------------------------------------------------------------------------------------------
static decomp_fn s_TranslationDecompFnptr[] = 
{
    DecompT_Identity,
    DecompT_Const,
    DecompT_Dictionary,
    DecompT_Delta_Low,
    NULL,
    NULL,
    DecompV3_Fullrange,
};

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// DECOMPRESSORS
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
static
void DecompS_Identity( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)Bitstream;

    for( s32 i=0; i<nFrames; i++ )
    {
        xvector3& V = *((xvector3*)pData);

        V.Set(1.0f);

        pData += Stride; 
    }
}

//--------------------------------------------------------------------------------------------------
static
void DecompT_Identity( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)Bitstream;

    for( s32 i=0; i<nFrames; i++ )
    {
        xvector3& V = *((xvector3*)pData);

        V.Set(0.0f);

        pData += Stride; 
    }
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Identity( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)Bitstream;

    for( s32 i=0; i<nFrames; i++ )
    {
        xquaternion& Q = *((xquaternion*)pData);

        Q.Identity();

        pData += Stride; 
    }
}

//--------------------------------------------------------------------------------------------------
static
void DecompV3_Fullrange( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;

    ASSERT( x_IsAlign(pData,16) );
    for( s32 i=0; i<nFrames; i++ )
    {
        xvector3& V = *((xvector3*)pData);

        Bitstream.SerializeIn( V.m_X );
        Bitstream.SerializeIn( V.m_Y );
        Bitstream.SerializeIn( V.m_Z );

        pData += Stride; 
    }
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Fullrage( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;

    ASSERT( x_IsAlign(pData,16) );
    for( s32 i=0; i<nFrames; i++ )
    {
        xquaternion& Q = *((xquaternion*)pData);

        Bitstream.SerializeIn( Q.m_X );
        Bitstream.SerializeIn( Q.m_Y );
        Bitstream.SerializeIn( Q.m_Z );
        Bitstream.SerializeIn( Q.m_W );

        pData += Stride;
    }
}

//--------------------------------------------------------------------------------------------------
static
void DecompS_Const( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)nFrames;

    s32 iX,iY,iZ;

    ASSERT( x_IsAlign(pData,16) );
    xvector3& V = *((xvector3*)pData);

    Bitstream.SerializeIn( iX );
    Bitstream.SerializeIn( iY );
    Bitstream.SerializeIn( iZ );

    V.m_X = iX / f32(1<<7);
    V.m_Y = iY / f32(1<<7);
    V.m_Z = iZ / f32(1<<7);

  //  ASSERT( x_Abs(V.GetLength() - 1) < 0.01f );

    // Replicate across all the frames
    for( s32 i=1; i<nFrames; i++ )
    {
        xvector3& V2 = *((xvector3*)&pData[ i * Stride ]);
        V2 = V;
    }    
}

//--------------------------------------------------------------------------------------------------
static
void DecompT_Dictionary( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)Bitstream;
    (void)nFrames;

    ASSERT( x_IsAlign(pData,16) );
    xvector3& V = *((xvector3*)pData);
    
    ASSERT( iStream >= 0 );
    ASSERT( iStream < AnimGroup.m_nTotalBones );
    auto& Skeleton = *AnimGroup.getSkeletonRef().getAsset();
    
    V = Skeleton.m_pBone.m_Ptr[iStream].m_LocalTranslation;

    // Replicate the quaternion across all the frames
    for( s32 i=1; i<nFrames; i++ )
    {
        xvector3& V2 = *((xvector3*)&pData[ i * Stride ]);
        V2 = V;
    }    
}

//--------------------------------------------------------------------------------------------------
static
void DecompT_Const( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)nFrames;

    s32 iX,iY,iZ;

    ASSERT( x_IsAlign(pData,16) );
    xvector3& V = *((xvector3*)pData);

    Bitstream.SerializeIn( iX );
    Bitstream.SerializeIn( iY );
    Bitstream.SerializeIn( iZ );

    V.m_X = iX / f32(1<<7);
    V.m_Y = iY / f32(1<<7);
    V.m_Z = iZ / f32(1<<7);

    // Replicate across all the frames
    for( s32 i=1; i<nFrames; i++ )
    {
        xvector3& V2 = *((xvector3*)&pData[ i * Stride ]);
        V2 = V;
    }    
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Const( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    (void)AnimGroup;
    (void)iStream;
    (void)nFrames;

    s32 iX,iY,iZ,iW;
    Bitstream.SerializeIn( iX, -(1<<10), (1<<10) );
    Bitstream.SerializeIn( iY, -(1<<10), (1<<10) );
    Bitstream.SerializeIn( iZ, -(1<<10), (1<<10) );
    Bitstream.SerializeIn( iW, -(1<<10), (1<<10) );

    ASSERT( x_IsAlign(pData,16) ); 
    xquaternion& Q = *((xquaternion*)pData);

    Q.m_X = iX / f32(1<<10);
    Q.m_Y = iY / f32(1<<10);
    Q.m_Z = iZ / f32(1<<10);
    Q.m_W = iW / f32(1<<10);

    // Replicate across all the frames
    for( s32 i=1; i<nFrames; i++ )
    {
        xquaternion& Q2 = *((xquaternion*)&pData[ Stride*i ]);
        Q2 = Q;
    }    
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// DELTA COMPRESSION
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
static
void DeltaDecompress( xbitstream& Bitstream, s32 nSamples, s32 Stride, xbyte* pData, f32 Prec )
{
    const f32   OneOverPrec = 1.0f / Prec;
    s32         FirstSample;
    s32         MinDelta;
    u32         nDeltaBits;

    Bitstream.SerializeIn( FirstSample );
    Bitstream.SerializeIn( MinDelta );
    Bitstream.SerializeIn( nDeltaBits, 5 );

    // Create first sample
    f32* pSample = (f32*)pData;
    *pSample = FirstSample * OneOverPrec;

    // Create other samples
    f32 Prev = *pSample;
    for( s32 i=1; i<nSamples; i++ )
    {
        u32 D;
        Bitstream.SerializeIn( D, nDeltaBits );

        pSample  = (f32*)&pData[ Stride * i ];
        *pSample = Prev + (((s32)D + MinDelta)*OneOverPrec);
        Prev = *pSample; 
    }
}

//--------------------------------------------------------------------------------------------------
static
void DeltaDecompressQ( xbitstream& Bitstream, s32 nSamples, s32 Stride, xbyte* pData, f32 Prec )
{
    const f32   OneOverPrec = 1.0f / Prec;
    s32         FirstSample;
    s32         MinDelta;
    u32         nDeltaBits;

    Bitstream.SerializeIn( FirstSample );
    Bitstream.SerializeIn( MinDelta );
    Bitstream.SerializeIn( nDeltaBits, 5 );

    // Create first sample
    f32 PrevSample = FirstSample * OneOverPrec;
    ASSERT( (PrevSample>=-1.0f) && (PrevSample<=1.0f) );

    u16* pSample = (u16*)pData;
    *pSample = (u16)((PrevSample+1.0f)*0.5f*65535.0f);

    // Create other samples
    for( s32 i=1; i<nSamples; i++ )
    {
        u32 D;
        Bitstream.SerializeIn( D, nDeltaBits );

        PrevSample += (((s32)D + MinDelta)*OneOverPrec);
        ASSERT( (PrevSample>=-1.0f) && (PrevSample<=1.0f) );

        pSample  = (u16*)&pData[ Stride*i ];
        *pSample = (u16)((PrevSample+1.0f)*0.5f*65535.0f);
    }
}

//--------------------------------------------------------------------------------------------------
static inline
void DecompV_Delta_Common( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData, f32 Precision )
{
    (void)AnimGroup;
    (void)iStream;

    ASSERT( x_IsAlign(pData,16) );
    DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xvector3*)pData)->m_X, Precision );
    DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xvector3*)pData)->m_Y, Precision );
    DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xvector3*)pData)->m_Z, Precision );
}

//--------------------------------------------------------------------------------------------------
static inline
void DecompQ_Delta_Common( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData, f32 Precision )
{
    (void)AnimGroup;
    (void)iStream;

    if( 1 )
    {
        ASSERT( x_IsAlign(pData,16) );
        DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xquaternion*)pData)->m_X, Precision );
        DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xquaternion*)pData)->m_Y, Precision );
        DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xquaternion*)pData)->m_Z, Precision );
        DeltaDecompress( Bitstream, nFrames, Stride, (xbyte*)&((xquaternion*)pData)->m_W, Precision );
        
        for( s32 i=0; i<nFrames; i++ )
        {
            ((xquaternion*)&pData[i*Stride])->Normalize();
        }
    }
    else
    {
        // Decompress quaternions as 16bits
        DeltaDecompressQ( Bitstream, nFrames, Stride, (xbyte*)&((u16*)pData)[0], Precision );
        DeltaDecompressQ( Bitstream, nFrames, Stride, (xbyte*)&((u16*)pData)[1], Precision );
        DeltaDecompressQ( Bitstream, nFrames, Stride, (xbyte*)&((u16*)pData)[2], Precision );
        DeltaDecompressQ( Bitstream, nFrames, Stride, (xbyte*)&((u16*)pData)[3], Precision );

    }
}

//--------------------------------------------------------------------------------------------------
static 
void DecompS_Delta_Low( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    DecompV_Delta_Common( Bitstream, AnimGroup, iStream, nFrames, Stride, pData, f32(1<<7) );
}

//--------------------------------------------------------------------------------------------------
static
void DecompT_Delta_Low( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    DecompV_Delta_Common( Bitstream, AnimGroup, iStream, nFrames, Stride, pData, f32(1<<5) );
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Delta_Low( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    DecompQ_Delta_Common( Bitstream, AnimGroup, iStream, nFrames, Stride, pData, f32(1<<10) );
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Delta_Mid( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    DecompQ_Delta_Common( Bitstream, AnimGroup, iStream, nFrames, Stride, pData, f32(1<<14) );
}

//--------------------------------------------------------------------------------------------------
static
void DecompQ_Delta_High( xbitstream& Bitstream, const anim_group& AnimGroup, s32 iStream, s32 nFrames, s32 Stride, xbyte* pData )
{
    DecompQ_Delta_Common( Bitstream, AnimGroup, iStream, nFrames, Stride, pData, f32(1<<16) );
}


//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// ANIM_DECOMPRESS
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void eng_anim_group_compressor::AnimationDecompress( 
    xtransform*                                     pData,
    const eng_anim_group_rsc::anim_key_block&       KeyBlock,
    const eng_anim_group_rsc::anim_group_rsc&       AnimGroup )
{
    compression_type    iSDecompressor;
    compression_type    iQDecompressor;
    compression_type    iTDecompressor;
    xbitstream          Bitstream;
    const xptr<xbyte>   DataSrc( (xbyte*)&KeyBlock.m_pCompressStream.m_Ptr[0], KeyBlock.m_CompressedDataSize, xptr<u8>::FLAG_STATIC_MEMORY | xptr<u8>::FLAG_READ_ONLY );
    
    Bitstream.setMode( FALSE );
    Bitstream.setPackData( DataSrc, KeyBlock.m_CompressedDataSize );

    ASSERTCT( sizeof(s_ScaleDecompFnptr)/sizeof(void*)        == TYPE_ENUM_COUNT );
    ASSERTCT( sizeof(s_RotationDecompFnptr)/sizeof(void*)     == TYPE_ENUM_COUNT );
    ASSERTCT( sizeof(s_TranslationDecompFnptr)/sizeof(void*)  == TYPE_ENUM_COUNT );

    const s32 Stride = sizeof(xtransform) * KeyBlock.m_nStreams; 
    for( s32 i=0; i<KeyBlock.m_nStreams; i++ )
    {
        SerializeTypeIn( Bitstream, iSDecompressor );
        ASSERT(s_ScaleDecompFnptr[ iSDecompressor ]);
        s_ScaleDecompFnptr[ iSDecompressor ]( Bitstream, AnimGroup, i, KeyBlock.m_nFrames, Stride, (xbyte*)&pData[i].m_Scale );

        SerializeTypeIn( Bitstream, iQDecompressor );
        ASSERT(s_RotationDecompFnptr[ iQDecompressor ]);
        s_RotationDecompFnptr[ iQDecompressor ]( Bitstream, AnimGroup, i, KeyBlock.m_nFrames, Stride, (xbyte*)&pData[i].m_Rotation );

        SerializeTypeIn( Bitstream, iTDecompressor );
        ASSERT(s_TranslationDecompFnptr[ iTDecompressor ]);
        s_TranslationDecompFnptr[ iTDecompressor ]( Bitstream, AnimGroup, i, KeyBlock.m_nFrames, Stride, (xbyte*)&pData[i].m_Translation );
    }

    //
    // Dump frames for debugging 
    //
    if( 0 )
    {
        static xfile File;
        static xbool bOpended = TRUE;
        if(bOpended) { bOpended = FALSE; File.Open( "temp:/Keys.txt", "wt" ); }
        
        for( s32 j=0; j<KeyBlock.m_nFrames; j++ )
        for( s32 i=0; i<KeyBlock.m_nStreams; i++ )
        {
            s32 iFrame = j*KeyBlock.m_nStreams + i;
         //   File.Printf("[%d %d %d]    ", s32(Compressor[i].m_X), s32(Compressor[i].m_Y), s32(Compressor[i].m_Z) ); 
            File.Printf("%d %d %d      ", iFrame, i, j ); 
            File.Printf("%f %f %f      ", pData[iFrame].m_Scale.m_X, pData[iFrame].m_Scale.m_Y, pData[iFrame].m_Scale.m_Z ); 
            File.Printf("%f %f %f %f   ", pData[iFrame].m_Rotation.m_X, pData[iFrame].m_Rotation.m_Y, pData[iFrame].m_Rotation.m_Z, pData[iFrame].m_Rotation.m_W ); 
            File.Printf("%f %f %f      \n", pData[iFrame].m_Translation.m_X, pData[iFrame].m_Translation.m_Y, pData[iFrame].m_Translation.m_Z ); 
        }
        File.Flush();
    }

}

//--------------------------------------------------------------------------------------------------
