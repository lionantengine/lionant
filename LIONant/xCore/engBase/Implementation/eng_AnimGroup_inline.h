
//=========================================================================
// SAVING FUNCTIONS
//=========================================================================

//-------------------------------------------------------------------------

inline
void eng_anim_group_rsc::anim_key_block::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof(*this) == SIZE_ANIM_KEY_BLOCK );
    SerialFile.Serialize( m_CompressedDataSize );
    SerialFile.Serialize( m_pCompressStream, m_CompressedDataSize );
    SerialFile.Serialize( *((u64*)&m_pDecompressStream) );
    SerialFile.Serialize( m_pNext, 0 );
    SerialFile.Serialize( m_pPrev, 0 );
    SerialFile.Serialize( m_Processing );
    SerialFile.Serialize( m_KeyCount );
    SerialFile.Serialize( m_nFrames );
    SerialFile.Serialize( m_nStreams );
    SerialFile.Serialize( m_igBlock.m_Value );
}

//-------------------------------------------------------------------------
inline
void eng_anim_group_rsc::hash_entry::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof(*this) == SIZE_HASH );
    SerialFile.Serialize( m_Hash.m_Value );
    SerialFile.Serialize( m_iAnim.m_Value );
    SerialFile.Serialize( m_iBone.m_Value );
}

//-------------------------------------------------------------------------
inline
s32 eng_anim_group_rsc::hash_entry::CompareFunction( const void* pA, const void* pB )
{
    auto& A = *(const eng_anim_group_rsc::hash_entry*)pA;
    auto& B = *(const eng_anim_group_rsc::hash_entry*)pB;

    if( A.m_Hash.m_Value < B.m_Hash.m_Value ) return -1;
    return ( A.m_Hash.m_Value > B.m_Hash.m_Value );
}

//-------------------------------------------------------------------------
inline
void eng_anim_group_rsc::anim_keys::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof(*this) == SIZE_ANIM_KEYS );
    SerialFile.Serialize( m_nFrames );
    SerialFile.Serialize( m_nBones );
    SerialFile.Serialize( m_nProps );
    SerialFile.Serialize( m_nKeyBlocks );
    SerialFile.Serialize( m_igKeyBlockBase.m_Value );
}

//-------------------------------------------------------------------------
inline
void eng_anim_group_rsc::anim_info::SerializeIO( xserialfile& SerialFile ) const
{
    ASSERTCT( sizeof(*this) == SIZE_ANIM_INFO );
           
    if( m_pBoneFlags.m_Ptr) SerialFile.Serialize( m_pBoneFlags, m_AnimKeys.m_nBones + m_AnimKeys.m_nProps );
    else                    SerialFile.Serialize( m_pBoneFlags, 0 );
             
    SerialFile.Serialize( m_TotalTranslation );
    SerialFile.Serialize( m_AnimsWeight );
    SerialFile.Serialize( m_Weight );
    SerialFile.Serialize( m_nEvents );
    SerialFile.Serialize( m_iEvent );
    SerialFile.Serialize( m_nProps );
    SerialFile.Serialize( m_iProp );
    SerialFile.Serialize( m_HandleAngle );
    SerialFile.Serialize( m_TotalYaw );
    SerialFile.Serialize( m_TotalMoveDir );
    SerialFile.Serialize( m_AnimKeys );
    SerialFile.Serialize( m_nSameAnims );
    SerialFile.Serialize( m_Flags );
    SerialFile.Serialize( m_FPS );
    SerialFile.Serialize( m_Name );
}

//-------------------------------------------------------------------------
inline
void eng_anim_group_rsc::anim_group_rsc::SerializeIO( xserialfile& SerialFile ) const
{
    SerialFile.SetResourceVersion( VERSION );
    ASSERTCT( sizeof( *this ) == SIZE_ANIM_GROUP );

    SerialFile.Serialize( m_nTotalAnims );
    SerialFile.Serialize( m_pHashTable, m_nTotalAnims );

    SerialFile.Serialize( m_nTotalBones );
    SerialFile.Serialize( m_SkeletonGuid );

    SerialFile.Serialize( m_nTotalAnims );
    SerialFile.Serialize( m_pAnimInfo, m_nTotalAnims );

    SerialFile.Serialize( m_nTotalProps );
    SerialFile.Serialize( m_pProp, m_nTotalProps );

    SerialFile.Serialize( m_nTotalEvents );
    SerialFile.Serialize( m_pEvent, m_nTotalEvents );

    SerialFile.Serialize( m_nEventDatas );
    SerialFile.Serialize( m_pEventData, m_nEventDatas );

    SerialFile.Serialize( m_nTotalKeyBlocks );
    SerialFile.Serialize( m_pKeyBlock, m_nTotalKeyBlocks );

    SerialFile.Serialize( m_BBox );

    SerialFile.Serialize( m_nTotalFrames );
    SerialFile.Serialize( m_nTotalKeys );
}

//=========================================================================
// ENG_ANIM_COMPRESSOR
//=========================================================================

struct eng_anim_group_compressor
{
    enum compression_type
    {
        TYPE_IDENTITY,
        TYPE_CONST_VALUE,
        TYPE_DICTIONARY,
        TYPE_DELTA_LOW,
        TYPE_DELTA_MID,
        TYPE_DELTA_HIGH,
        TYPE_FULLRANGE,
        TYPE_ENUM_COUNT
    };

    static void AnimationDecompress( 
        xtransform*                                   pData,
        const eng_anim_group_rsc::anim_key_block&     KeyBlock,
        const eng_anim_group_rsc::anim_group_rsc&     AnimGroup );

    inline static void SerializeTypeIn ( xbitstream& Bitstream, compression_type& Type ) 
    { 
        Bitstream.SerializeIn( Type, 0, TYPE_ENUM_COUNT );
        ASSERT( Type >= 0 );
        ASSERT( Type < TYPE_ENUM_COUNT );  
    }

    inline static void SerializeTypeOut( xbitstream& Bitstream, compression_type  Type ) 
    { 
        ASSERT( Type >= 0 );
        ASSERT( Type < TYPE_ENUM_COUNT );  
        Bitstream.SerializeOut( Type, 0, TYPE_ENUM_COUNT ); 
    }
};
