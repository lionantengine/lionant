//-------------------------------------------------------------------------------
// Private functions
//-------------------------------------------------------------------------------

#ifdef TARGET_PC
    void            _eng_MFCInitialize               ( void );
    void            _eng_MFCKill                     ( void );
    xhandle         _eng_MFCCreateSwapChain          ( HWND hWindow );
    void            _eng_MFCSetActiveSwapChain       ( xhandle hChain );
    void            _eng_MFCDoneWithSwapChain        ( xhandle hChain );
    void            _eng_MFCReleaseSwapChain         ( xhandle hChain );
#endif

//===============================================================================
// Magical macro which defines the entry point of the app. Make sure that 

// the use has the entry point: void AppMain( s32 argc, char* argv[] ) 

// define somewhere. MFC apps don't need the user entry point.

//===============================================================================

#if defined _CONSOLE || defined TARGET_OSX

    void _eng_EntryPoint    ( int argc, char** argv );
    s32  _eng_ExitPoint     ( void );
    void _eng_ResizeBuffers ( s32 Width, s32 Height );

	 //Console mode on the PC requires a different entry point

    #define AppMain AppMain( s32 argc, char* argv[] );                          \
	int main( int argc, char *argv[] )                                          \
    {                                                                           \
	  x_try;                                                                    \
	    _eng_EntryPoint( argc, argv );                                          \
        AppMain( argc, argv );                                                  \
      x_catch_display;                                                          \
	  return _eng_ExitPoint();                                                  \
	} void AppMain

#elif defined TARGET_IOS

    extern "C"
    {
        void _eng_EntryPoint    ( int argc, char** argv );
        int _eng_EntryPointIOS  ( void (*)( int argc, char* argv[] ), int argc, char** argv );
        s32  _eng_ExitPoint     ( void );
        int main( int argc, char *argv[] );
        void AppMain( int argc, char* argv[] );
    }
    #define AppMain  __dummy(void);                                             \
    int main( int argc, char *argv[] )                                          \
    {                                                                           \
        x_try;                                                                  \
        _eng_EntryPointIOS( AppMain, argc, argv );                              \
        x_catch_display;                                                        \
        return _eng_ExitPoint();                                                \
    } void AppMain
#elif defined TARGET_3DS

void _eng_EntryPoint    ( int argc, char** argv );
s32  _eng_ExitPoint     ( void );

#define AppMain AppMain3DS( s32 argc, char* argv[] );                       \
    void nnMain()                                                           \
    {                                                                       \
        x_try;                                                              \
        _eng_EntryPoint( 0, NULL );                                         \
        AppMain3DS( 0, NULL );                                              \
        x_catch_display;                                                    \
        _eng_ExitPoint();                                                   \
    }  void AppMain3DS

#else

void _eng_EntryPoint    ( s32& argc, char**& argv, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow );
s32  _eng_ExitPoint     ( void );
void _eng_ResizeBuffers ( s32 Width, s32 Height );

#define AppMain AppMain( s32 argc, char* argv[] );                                                         \
    int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )    \
    {                                                                                                      \
      s32 argc; char **argv;                                                                               \
      x_try;                                                                                               \
      _eng_EntryPoint( argc, argv, hInstance, hPrevInstance, lpCmdLine, nCmdShow );                        \
      AppMain( argc, argv );                                                                               \
      x_catch_display;                                                                                     \
      return _eng_ExitPoint();                                                                             \
    } void AppMain                                                                                               
#endif

//===============================================================================
// INLINE FUNCTIONS
//===============================================================================
