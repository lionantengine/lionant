//
//  eng_View.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef ENG_VIEW_H
#define ENG_VIEW_H

//===============================================================================
// eng_view
//===============================================================================
class eng_view
{
public:
                        eng_view            ( void );
        void            setViewport         ( const xirect& Rect );
        const xirect&   getViewport         ( void ) const;
        void            RefreshViewport     ( void );

        xvector3        getPosition         ( void ) const;
        xvector3        getV2CScales        ( xbool bInfiniteClipping = TRUE ) const;
        void            setCubeMapView      ( s32 Face, const xmatrix4& L2W );
        void            setParabolicView    ( s32 Side, const xmatrix4& L2W );
        void            setInfiniteClipping ( xbool bInfinite );
		xbool			getInfiniteClipping ( ) const;

        void            setNearZ            ( f32 NearZ );
        f32             getNearZ            ( void ) const;
        void            setFarZ             ( f32 FarZ );
        f32             getFarZ             ( void ) const;
        void            setFov              ( f32 Fov );
        f32             getFov              ( void ) const;
        void            setAspect           ( f32 Aspect );
        f32             getAspect           ( void ) const;

        //using SetFov or SetAspect counteracts this (and vice/versa):

        void            setCustomFrustum    ( f32 Left, f32 Right, f32 Bottom, f32 Top);
        void            DisableCustomFrustum(); //this goes back to the FOV method with currently set FOV & Aspect

        eng_view&       LookAt              ( const xvector3& From, const xvector3& To);            
        eng_view&       LookAt              ( const xvector3& From, const xvector3& To, const xvector3& Up ); 
        eng_view&       LookAt              ( f32 Distance, const xradian3& Angles, const xvector3& At );

        void            setPosition         ( xvector3& Position );
        void            setRotation         ( xradian3& Rotation );
        void            Translate           ( xvector3& DeltaPos );

        xvector3        RayFromScreen       ( f32 ScreenX, f32 ScreenY ) const;
        xvector3        getWorldZVector     ( void ) const;
        xvector3        getWorldXVector     ( void ) const;
        xvector3        getWorldYVector     ( void ) const;

        const xmatrix4& getW2V              ( void ) const;
        const xmatrix4& getW2C              ( void ) const;
        const xmatrix4& getW2S              ( void ) const;
        const xmatrix4& getV2C              ( void ) const;
        const xmatrix4& getV2W              ( void ) const;
        const xmatrix4& getV2S              ( void ) const;
        const xmatrix4& getC2S              ( void ) const;
        const xmatrix4& getC2W              ( void ) const;
        const xmatrix4& getC2V              ( void ) const;

        f32             getScreenSize       ( const xvector3& Position, f32 Radius ) const;

//-------------------------------------------------------------------------------

protected:

        void            UpdatedView     ( void ) const;
        void            UpdatedClip     ( void ) const;
        void            UpdatedScreen   ( void ) const;

//-------------------------------------------------------------------------------

protected:

  enum flag_masks
    {
        FLAGS_W2V               = X_BIT(0),
        FLAGS_W2C               = X_BIT(1),
        FLAGS_W2S               = X_BIT(2),
        FLAGS_V2C               = X_BIT(3),
        FLAGS_V2S               = X_BIT(4),
        FLAGS_C2V               = X_BIT(5),
        FLAGS_C2S               = X_BIT(6),
        FLAGS_C2W               = X_BIT(7),
        FLAGS_VIEWPORT          = X_BIT(8),
        FLAGS_INF_CLIP          = X_BIT(15),
        FLAGS_USE_FOV           = X_BIT(16),
        FLAGS_PAD               = 0xffffffff
    };
        xmatrix4    m_V2W;
mutable xmatrix4    m_W2V;
mutable xmatrix4    m_W2C;
mutable xmatrix4    m_W2S;
mutable xmatrix4    m_V2C;
mutable xmatrix4    m_V2S;
mutable xmatrix4    m_C2S;
mutable xmatrix4    m_C2W;
mutable xmatrix4    m_C2V;
mutable xirect      m_Viewport;         // Viewport relative to the screen.
mutable u32         m_Flags;
        f32         m_NearZ;
        f32         m_FarZ;
        f32         m_Fov;
        f32         m_Aspect;

		  //when not using FOV, these are used instead

		  f32         m_FrustumLeft;
		  f32         m_FrustumRight;
		  f32         m_FrustumBottom;
		  f32         m_FrustumTop;
};

//===============================================================================
// INLINE
//===============================================================================

#include "Implementation/eng_view_inline.h"

//===============================================================================
// END
//===============================================================================
#endif