//
//  engAnimGroup.h
//  engBase
//
//  Created by Tomas Arce on 8/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef ENG_ANIM_GROUP_H
#define ENG_ANIM_GROUP_H

//=========================================================================
// ENG_ANIM_GROUP_RSC
//=========================================================================

struct eng_anim_group_rsc : public eng_resource_base
{
public:

    enum
    {
        UID = 8,
    };

    struct anim_group_rsc;
    using ref = eng_resource_ref<eng_anim_group_rsc, anim_group_rsc> ;

    enum version_details
    {
        MAJOR_VERSION_NUMBER        = 1,
        MINOR_VERSION_NUMBER        = 0,

        SIZE_HASH                   = 4*1 + 2*2,
        SIZE_ANIM_BONE              = sizeof(xmatrix4) + sizeof(xvector3d)*2 + 2*3 + 1*32 + 1*2, 
        SIZE_ANIM_KEY_BLOCK         = 8*5 + 4*2 + 2*3 + 1*2,
        SIZE_ANIM_KEYS              = 2*5,
        SIZE_ANIM_GROUP             = 8*7 + sizeof(xbbox) + 4*7 + 4*1,
        SIZE_ANIM_INFO              = 8*1 + 4*5 + 2*8 + SIZE_ANIM_KEYS + 1*2 + 1*32,

        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) + ( MINOR_VERSION_NUMBER 
        + SIZE_HASH
        + SIZE_ANIM_BONE
        + SIZE_ANIM_KEY_BLOCK
        + SIZE_ANIM_KEYS
        + SIZE_ANIM_GROUP
        + SIZE_ANIM_INFO )
    };

    //-------------------------------------------------------------------------

public:
    
    #define ENG_ANIM_GROUP_LOCAL_TYPES                                             \
        using mutable_keyframe  = xtransform;                                      \
        using keyframe          = const xtransform;                                \
        using animgroup         = const eng_anim_group_rsc::anim_group_rsc;        \
        using animinfo          = const eng_anim_group_rsc::anim_info;             \
        using event             = const eng_anim_group_rsc::anim_event;            \
        using block             = const eng_anim_group_rsc::anim_key_block;        \
        using ibone             = eng_anim_group_rsc::ibone;                       \
        using ianim             = eng_anim_group_rsc::ianim;                       \
        using ilframe           = eng_anim_group_rsc::ilframe;                     \
        using igframe           = eng_anim_group_rsc::igframe;                     \
        using ilblock           = eng_anim_group_rsc::ilblock;                     \
        using igblock           = eng_anim_group_rsc::igblock;                     \
        using ianimhash         = eng_anim_group_rsc::ianimhash;                   \
        using skeleton          = const eng_skeleton_rsc::skeleton_rsc;            \
        using skeleton_ref      = eng_skeleton_rsc::ref;

                                           
    // index to one of the animations note about invalid
    x_units( s16, ianim, enum { INVALID = -1 };  );

    // index to bones
    x_units( s16, ibone, enum { INVALID = -1 };  );

    // the hash id for the animations
    struct ianimhash { enum { INVALID = 0 }; ianimhash( void )=default; explicit ianimhash( u32 Value ) : m_Value( Value << 8 ) {} u32 m_Value; };

    // global frame to the animation
    x_units( s32, igframe ); 

    // local block to the animation
    x_units( s32, ilblock, ilblock( igframe igFrame ):m_Value(igFrame.m_Value>>anim_keys::MAX_FRAMES_PER_BLOCK_SHIFT) {} );

    // local frame to the block
    x_units( s32, ilframe, ilframe( igframe igFrame ):m_Value(anim_keys::MAX_FRAMES_PER_BLOCK_MASK&igFrame.m_Value){}  );
    
    // index to a global block inside the anim group list
    x_units( u16, igblock, igblock( const anim_group_rsc& AnimGroup, ianim iAnim, ilblock ilBlock )    
    { 
        ASSERT( iAnim.m_Value >= 0 );
        ASSERT( iAnim.m_Value < AnimGroup.m_nTotalAnims ); 

        const auto&  AnimInfo        = AnimGroup.m_pAnimInfo.m_Ptr[iAnim.m_Value];
        ASSERT( ilBlock.m_Value < AnimInfo.m_AnimKeys.m_nKeyBlocks );
        ASSERT( ilBlock.m_Value >= 0 );

        m_Value = u16(AnimInfo.m_AnimKeys.m_igKeyBlockBase.m_Value + ilBlock.m_Value);
        ASSERT( m_Value < AnimGroup.m_nTotalKeyBlocks );
     });

    using const_string = xsafe_array<char,32>;

    struct anim_key_block
    {
    protected:

        xserialfile::ptr<xtransform>        m_pDecompressStream;        // Points to decompressed data if available
        xserialfile::ptr<xbyte>             m_pCompressStream;          // Points to decompressed data if available
        xserialfile::ptr<anim_key_block>    m_pNext;                    // Global Link for the Global Anim Cache
        xserialfile::ptr<anim_key_block>    m_pPrev;                    // Global Link for the Global Anim Cache
        u64                                 m_Processing;               // This is a special flags used at runtime for the cache
        s32                                 m_CompressedDataSize;       // How big is the compress stream
        s32                                 m_KeyCount;                 // How many anim keys we need to allocate ones in decompress
        u16                                 m_nFrames;                  // How many frames are actually in this block
        u16                                 m_nStreams;                 // Number of bones + props
        igblock                             m_igBlock;                  // global Index of this block
        u8                                  m_Pad[2];
    
    public:

        void SerializeIO    ( xserialfile& SerialFile ) const;
        void BeginAtomic    ( void )                            { (*(xsemaphore_lock*)&m_Processing).BeginAtomic(); }
        void BeginAtomic    ( void ) const                      { (*(xsemaphore_lock*)&m_Processing).BeginAtomic(); }
        void EndAtomic      ( void )                            { (*(xsemaphore_lock*)&m_Processing).EndAtomic(); }
        void EndAtomic      ( void ) const                      { (*(xsemaphore_lock*)&m_Processing).EndAtomic(); }

    protected:

        friend struct eng_anim_group_compressor;
        friend class eng_anim_group_cache;
        friend class animation_compiler_base;
    };

    struct anim_event
    {
        u16                                 m_iEventDataOffset;
    	u16                                 m_DataFormat;
        u16                                 m_iFrame;
        u8                                  m_Pad[2];
        void SerializeIO( xserialfile& SerialFile ) const
        { ASSERT(0); }
    };

    struct hash_entry
    {
        ianimhash                           m_Hash;             // Hash ID with the encoded # of similar anims
        ianim                               m_iAnim;            // actual index to the animation
        ibone                               m_iBone;            // Index to the bone????

                        hash_entry          ( void ) = default;
        explicit        hash_entry          ( ianimhash Hash ) : m_Hash( Hash ){}
        void            SerializeIO         ( xserialfile& SerialFile ) const;
        static s32      CompareFunction     ( const void* pA, const void* pB );
    };

    struct anim_prop
    {
        ibone                               m_iParentBone;      // Which bone this prop is attached to
        const_string                        m_Type;
        void SerializeIO( xserialfile& SerialFile ) const
        { ASSERT(0); }
    };

    struct anim_keys
    {
        enum
        {
            MAX_FRAMES_PER_BLOCK_SHIFT    = 5,            
            MAX_FRAMES_PER_BLOCK          = 1<<MAX_FRAMES_PER_BLOCK_SHIFT,
            MAX_FRAMES_PER_BLOCK_MASK     = MAX_FRAMES_PER_BLOCK - 1
        };

        u16                                 m_nFrames;
        u16                                 m_nBones;
        u16                                 m_nProps;
        u16                                 m_nKeyBlocks;
        igblock                             m_igKeyBlockBase;
        
        void SerializeIO( xserialfile& SerialFile ) const;
    };

    struct anim_info
    {
        enum flags:u16
        {
            FLAGS_LOOP                      = X_BIT(0),
            FLAGS_PING_PONG                 = X_BIT(1),
            FLAGS_HAS_MASKS                 = X_BIT(2),
            FLAGS_ACCUM_TRANSLATION_X       = X_BIT(3),
            FLAGS_ACCUM_TRANSLATION_Y       = X_BIT(4),
            FLAGS_ACCUM_TRANSLATION_Z       = X_BIT(5),
            FLAGS_ACCUM_TRANSLATIONS        = FLAGS_ACCUM_TRANSLATION_X | FLAGS_ACCUM_TRANSLATION_Y | FLAGS_ACCUM_TRANSLATION_Z,
            FLAGS_ACCUM_ROTATION_PITCH      = X_BIT(6),
            FLAGS_ACCUM_ROTATION_YAW        = X_BIT(7),
            FLAGS_ACCUM_ROTATION_ROLL       = X_BIT(8),
            FLAGS_ACCUM_ROTATIONS           = FLAGS_ACCUM_ROTATION_PITCH | FLAGS_ACCUM_ROTATION_YAW | FLAGS_ACCUM_ROTATION_ROLL,
            FLAGS_ACCUM_RT                  = FLAGS_ACCUM_TRANSLATIONS | FLAGS_ACCUM_ROTATIONS,
            FLAGS_GRAVITY                   = X_BIT(9),
            FLAGS_WORLD_COLLISION           = X_BIT(10),
            FLAGS_LOOP_INTERPOLATION        = X_BIT(11)
        };

        enum bone_flags:u32
        {
            BONE_FLAG_MASKED     =       X_BIT(0),
        };

        xserialfile::ptr<u8>                m_pBoneFlags;           // Bone Flags for this animation, can be NULL
        xvector3d                           m_TotalTranslation;     // Total movement
        f32                                 m_AnimsWeight ;         // Total weight of all consecutive anims with this name
        f32                                 m_Weight ;              // Influence random select weight
        s16                                 m_nEvents;              // Number of events
        s16                                 m_iEvent;               // Index of first event
        s16                                 m_nProps;               // Number of props
        s16                                 m_iProp;                // Index of first prop
        u16                                 m_HandleAngle;          // Handle yaw
        u16                                 m_TotalYaw;             // Total yaw
        u16                                 m_TotalMoveDir;         // Total move yaw
        u16                                 m_Flags;                // Flags
        anim_keys                           m_AnimKeys;             // List of keys
        u8                                  m_nSameAnims ;          // Number of consecutive anims with this name
        u8                                  m_FPS;                  // Frames per second
        const_string                        m_Name;                 // Reference name

        void SerializeIO( xserialfile& SerialFile ) const;
    };


    struct anim_group_rsc
    {
                                anim_group_rsc      ( void ) = default;
                                anim_group_rsc      ( xserialfile& SerialFile )  {ASSERT(SerialFile.GetResourceVersion()==VERSION);}
        void                    SerializeIO         ( xserialfile& SerialFile ) const;
        eng_skeleton_rsc::ref&  getSkeletonRef      ( void ) const { return *(eng_skeleton_rsc::ref*)&m_SkeletonGuid; }

        xserialfile::ptr<hash_entry>        m_pHashTable;
        xserialfile::ptr<anim_info>         m_pAnimInfo;
        xserialfile::ptr<anim_prop>         m_pProp;
        xserialfile::ptr<anim_event>        m_pEvent;
        xserialfile::ptr<u32>               m_pEventData;               // temp storage for events during save
        xserialfile::ptr<anim_key_block>    m_pKeyBlock;
        u64                                 m_SkeletonGuid;
        xbbox                               m_BBox;                    
        s32                                 m_nEventDatas;
        s32                                 m_nTotalFrames;
        s32                                 m_nTotalKeys;
        s32                                 m_nTotalBones;
        s32                                 m_nTotalAnims;
        s32                                 m_nTotalProps;
        s32                                 m_nTotalEvents;
        s32                                 m_nTotalKeyBlocks;
    };

public:

                               ~eng_anim_group_rsc  ( void );
    static eng_resource_type&   getStaticType       ( void );

protected:

    anim_group_rsc*                         m_pAnimRSC  =   NULL;

protected:

    friend class eng_anim_group_system;
};

//=========================================================================
// ENG_ANIM_GROUP_BASIC_PLAYER
//=========================================================================

class eng_anim_group_cache
{
public:    
        ENG_ANIM_GROUP_LOCAL_TYPES

        // wrapping around the anim is for normal looping not pingpong. 
        using keyf_compute_function = x_function<void( keyframe* pKeyFrame0, keyframe* pKeyFrame1, f32 T, s32 nBones, xbool bWrappingAroundTheAnim )>;

public:

    void RemoveAllEntries( animgroup& AnimGroup );

    void InterpolateKeyFrames(
        xtransform*                         pDestination,
        const animgroup&                    AnimGroup, 
        ianim                               iAnim, 
        f32                                 fCurrFrame,
        f32                                 Rate  );

    void InterpolateKeyFrames( 
        xtransform*                     pDestination,
        keyframe*                       pKeyFrame0, 
        keyframe*                       pKeyFrame1, 
        f32                             T, 
        s32                             nBones );

    void cpKeyFrames( 
        const animgroup&                    AnimGroup, 
        ianim                               iAnim, 
        f32                                 FrameTime,
        f32                                 Rate,
        keyf_compute_function               Callback );

    const keyframe& getKeyFrame( const animgroup&                    AnimGroup,
                                 ianim                               iAnim,
                                 s32                                 T0 );

    xbool getInterpolatedRoot( 
        xtransform&                         RootKey,
        const animgroup&                    AnimGroup,
        ianim                               iAnim,
        f32                                 T0,
        f32                                 Rate );

protected:

    void        qtDecompressFrames              ( animgroup& AnimGroup, eng_anim_group_rsc::anim_key_block& Block );
    void        qtUpdateCacheForKeyblock        ( eng_anim_group_rsc::anim_key_block& Block );
    void        AddToFront                      ( eng_anim_group_rsc::anim_key_block& Block );
    void        RemoveFromList                  ( eng_anim_group_rsc::anim_key_block& Block );

protected:

    xsemaphore_lock                                 m_Semaphore;
    x_qt_counter                                    m_MemoryAllocated;
    const s32                                       m_MaxMemAllowed         = 1024*1024;
    eng_anim_group_rsc::anim_key_block*             m_pBlockHead            = NULL;
    eng_anim_group_rsc::anim_key_block*             m_pBlockTail            = NULL;
};

extern eng_anim_group_cache g_AnimGroupSystem;

//=========================================================================
// ENG_ANIM_GROUP_BASIC_PLAYER
//=========================================================================

class eng_anim_group_player
{
public:
    ENG_ANIM_GROUP_LOCAL_TYPES

public:

    void                    setup                   ( const eng_anim_group_rsc::ref& AnimGroupRef, u32 Seed );
    void                    TransitionToAnimation   ( ianimhash HashName );
    xbool                   BeginAdvanceTime        ( f32 DeltaTime );
    xbool                   WhileAdvanceTime        ( event& Event );
    xmatrix4*               getMatrices             ( void );

//protected:

    animgroup*                     Resolved        ( void );

//protected:

    xbool                           m_bResolved         = FALSE;
    xrandom_small                   m_Random;
    eng_anim_group_rsc::ref         m_AnimGroupRef;
    f32                             m_FrameTime         =   0;
    f32                             m_LastFrameTime     =   0;
    s32                             m_Cycle             =   0;
    s32                             m_LastCycle         =   0;
    f32                             m_Rate              =   1;
    ianimhash                       m_NextAnimHashName  =   ianimhash( ianimhash::INVALID );
    ianim                           m_iCurrAnim         =   ianim( ianim::INVALID );
    ianim                           m_iNextAnim         =   ianim( ianim::INVALID );
    xbool                           m_bReachedEnd       =   FALSE;
    xbool                           m_bStopAtEnd        =   FALSE;
    xbool                           m_bLooping          =   TRUE;
};

#endif
