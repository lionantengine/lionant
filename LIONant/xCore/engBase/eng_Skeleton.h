

struct eng_skeleton_rsc : public eng_resource_base
{
public:
    enum
    {
        UID = 9,
    };

    struct skeleton_rsc;
    using ref = eng_resource_ref<eng_skeleton_rsc, skeleton_rsc>;

    enum version_details
    {
        MAJOR_VERSION_NUMBER = 1,
        MINOR_VERSION_NUMBER = 0,

        SIZE_HASH = 4 * 1 + 2 * 2,
        SIZE_BONE = sizeof(xmatrix4)+sizeof(xvector3d)* 2 + 2 * 3 + 1 * 32 + 1 * 2,
        SIZE_SKELETON_RSC = 8*2 + 2*2 + 1*4,

        VERSION = ( MAJOR_VERSION_NUMBER << 13 ) + ( MINOR_VERSION_NUMBER
        + SIZE_HASH
        + SIZE_BONE
        + SIZE_SKELETON_RSC )
    };

    using const_string = xsafe_array<char, 32>;

    struct bone
    {
        xmatrix4                            m_BindMatrixInv;
        xvector3d                           m_LocalTranslation;
        xvector3d                           m_BindTranslation;
        s16                                 m_iBone;
        s16                                 m_iParent;
        u16                                 m_nChildren;
        const_string                        m_Name;
        u8                                  m_Pad[ 2 ];

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_BONE );
            SerialFile.Serialize( m_BindMatrixInv );
            SerialFile.Serialize( m_LocalTranslation );
            SerialFile.Serialize( m_BindTranslation );
            SerialFile.Serialize( m_iBone );
            SerialFile.Serialize( m_iParent );
            SerialFile.Serialize( m_nChildren );
            SerialFile.Serialize( m_Name );
        }
    };

    x_units( u16, ihash, explicit ihash( u32 Hash ) : m_Value( Hash ^ (Hash>>16) ) {} );

    struct hash_entry
    {
        ihash                               m_iHash;            // Hash ID with the encoded # of similar anims
        u16                                 m_iBone;            // Index to the bone????

        hash_entry( void ) = default;
        explicit        hash_entry( ihash Hash ) : m_iHash( Hash.m_Value ) {}
        void            SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_iHash.m_Value );
            SerialFile.Serialize( m_iBone );
        }

        static s32 CompareFunction( const void* pA, const void* pB )
        {
            auto& A = *(const hash_entry*)pA;
            auto& B = *(const hash_entry*)pB;
            if( A.m_iHash.m_Value < B.m_iHash.m_Value ) return -1;
            return A.m_iHash.m_Value > B.m_iHash.m_Value;
        }
    };

    struct skeleton_rsc
    {
        enum flags
        {
            ZEOR
        };

        xserialfile::ptr<bone>              m_pBone;
        xserialfile::ptr<hash_entry>        m_pHashTable;
        u16                                 m_Flags;
        u16                                 m_nBones;
        u8                                  m_Pad[4];

        skeleton_rsc( void ) = default;
        skeleton_rsc( xserialfile& SerialFile )
        {
            ASSERT( SerialFile.GetResourceVersion() == VERSION );
        }

        void    SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.SetResourceVersion( VERSION );
            ASSERTCT( sizeof( *this ) == SIZE_SKELETON_RSC );

            SerialFile.Serialize( m_nBones );
            SerialFile.Serialize( m_pBone, m_nBones );
            SerialFile.Serialize( m_pHashTable, m_nBones );
            SerialFile.Serialize( m_Flags );
            
        }
    };

public:

    ~eng_skeleton_rsc( void );
    static eng_resource_type&   getStaticType( void );

protected:

    skeleton_rsc*                           m_pSkeletonRSC = NULL;

protected:

};
