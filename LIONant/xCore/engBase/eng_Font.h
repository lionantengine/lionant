//
//  eng_Font.h
//  engBase
//
//  Created by Tomas Arce on 10/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef ENG_FONT_H
#define ENG_FONT_H

struct eng_font_rsc : public eng_resource_base
{
public:
    
    enum
    {
        UID = 3,
    };

    
    typedef eng_resource_ref<eng_font_rsc,eng_font_rsc> ref;
    

    enum version:u16
    {
        SIZE_CHAR_INFO  = 4,
        SIZE_CHAR_SET   = 24,
        SIZE_FONT       = 16,
        SIZE_FONT_RSC   = 24,
        VERSION = ( 1 << 13 )
                    + SIZE_CHAR_INFO
                    + SIZE_CHAR_SET
                    + SIZE_FONT
                    + SIZE_FONT_RSC
    };

    struct character_info
    {
        u16                                 m_Original;         // Origincal unicode for this entry
        s8                                  m_XAdvance;         // X Advance the characters for writting sentences
        s8                                  m_YAdvance;         // Y Advance the characters for writting sentences

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_CHAR_INFO );
            SerialFile.Serialize( m_Original );
            SerialFile.Serialize( m_XAdvance );
            SerialFile.Serialize( m_YAdvance );
        }
    };
    
    struct char_set
    {
        xserialfile::ptr<const u16>         m_PHTable;          // Table utilized to compute the index to the sprite frame that has our character
        xserialfile::ptr<const u16>         m_HashTable;        // Hash table to get the index of the characters
        s32                                 m_HashSize;         // Hash Table size for the characters
        u16                                 m_PHTableSize;      // Size of the PHTable
        u16                                 m_nCharacters;      // Number of characters in our origincal character list
        
        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_CHAR_SET );
            SerialFile.Serialize( m_PHTable, m_PHTableSize );
            SerialFile.Serialize( m_HashTable, m_HashSize );
            SerialFile.Serialize( m_HashSize );
            SerialFile.Serialize( m_PHTableSize );
            SerialFile.Serialize( m_nCharacters );
        }
    };
    
    struct font
    {
        xserialfile::ptr<character_info>    m_CharacterInfo;    // Information about the character itself. Must be access via the PH Key
        u16                                 m_nCharacterInfos;  // Number of characters infos that we have (should be equal to the nCharacters)
        u16                                 m_FontHashName;     // The name of the font in a hash form
        u8                                  m_iCharSet;         // Index to the char set
        u8                                  m_FontSize;         // How big are the characters in this font
        u8                                  m_Flags;            // Things like if the font is BOLD, UNDERLINE, etc.
        u8                                  m_Pad;
        
        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == SIZE_FONT );
            
            SerialFile.Serialize( m_CharacterInfo, m_nCharacterInfos );
            SerialFile.Serialize( m_nCharacterInfos );
            SerialFile.Serialize( m_FontHashName );
            SerialFile.Serialize( m_iCharSet );
            SerialFile.Serialize( m_FontSize );
            SerialFile.Serialize( m_Flags );
        }
    };
    
    struct font_rsc
    {
        xserialfile::ptr<char_set>          m_CharSet;          // Character set for the fonts
        xserialfile::ptr<font>              m_Font;             // Pointer to an list of font structures (Shorted by: hash-name, Size, then Flags )
        u16                                 m_nFonts;           // Number of fonts in the resource
        u16                                 m_nCharSets;        // Number of character sets
        u8                                  m_Pad[4];
        
        font_rsc( void ){}
        font_rsc( xserialfile& SerialFile ) { ASSERT( SerialFile.GetResourceVersion( ) == VERSION ); }

        void SerializeIO( xserialfile& SerialFile ) const
        {
            // Make sure that it is the first version
            SerialFile.SetResourceVersion( VERSION );
            ASSERTCT( sizeof(*this) == SIZE_FONT_RSC );
            
            SerialFile.Serialize( m_nFonts );
            SerialFile.Serialize( m_Font, m_nFonts );
            SerialFile.Serialize( m_nCharSets );
            SerialFile.Serialize( m_CharSet, m_nCharSets );
        }
    };
    
public:
    
                              ~eng_font_rsc     ( void );
    static eng_resource_type&  getStaticType    ( void );
    
protected:
    
    font_rsc*               m_pFontRsc      = NULL;
    eng_sprite_rsc::ref     m_SpriteAtlas;
    
protected:
    
    friend class eng_font;
};

//------------------------------------------------------------------------------------------

class eng_font
{
public:
    
    void                        setup               ( const u64 Guid, const char* pFontName );
    void                        DrawText            ( eng_draw& Draw, const xvector2& Pos, const char* pString, const f32 Scale = 1, xcolor Color = (~0) );
    void                        DrawText            ( eng_draw& Draw, const xmatrix4& Matrix, const xvector2& Pos, const xvector2& Scale, const char* pString, xcolor Color = ( ~0 ) );
    const eng_font_rsc*         Resolve             ( void );

protected:
    
    eng_font_rsc::ref       m_FontRscRef;
    u16                     m_Key               = 0xffff;
    u16                     m_iSprite           = 0xffff;
};

//------------------------------------------------------------------------------------------

#endif
