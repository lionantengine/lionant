//
//  eng_Primitives.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 LIONant. All rights reserved.
//

////////////////////////////////////////////////////////////////////////////////
// VERTEX DESCRIPTION
////////////////////////////////////////////////////////////////////////////////

class eng_vertex_desc
{
public:
    
    enum attribute_src:u8
    {
        ATTR_SRC_NULL,
        ATTR_SRC_U8x4_F,
        ATTR_SRC_U8x4_I,
        ATTR_SRC_S8x4_F,
        ATTR_SRC_S8x4_I,
        ATTR_SRC_U16x2_F,
        ATTR_SRC_U16x2_I,
        ATTR_SRC_U16x4_F,
        ATTR_SRC_U16x4_I,
        ATTR_SRC_S16x2_F,
        ATTR_SRC_S16x2_I,
        ATTR_SRC_S16x4_F,
        ATTR_SRC_S16x4_I,
        ATTR_SRC_F32x1,
        ATTR_SRC_F32x2,
        ATTR_SRC_F32x3,
        ATTR_SRC_F32x4,
        ATTR_SRC_ENUM_COUNT
    };
    
    // Decoding the attr_usage: First element is the "unique id", then the description.
    // note that it is invalid to have two elements with the same unique id.
    enum attribute_usage:u8
    {
        ATTR_USAGE_NULL,
        ATTR_USAGE_POSITION,
        ATTR_USAGE_2_COMPACT_WEIGHTS,            // A 4d vector with upto 2 weight and 2 bone indices
        ATTR_USAGE_4_WEIGHTS,                    // A 4d vector with upto 4 weights 
        ATTR_USAGE_4_INDICES,                    // A 4d vector with upto 4 indices 
        ATTR_USAGE_TANGENT,
        ATTR_USAGE_BINORMAL,
        ATTR_USAGE_NORMAL,
        ATTR_USAGE_00_PARAMETRIC_UV,
        ATTR_USAGE_01_PARAMETRIC_UV,
        ATTR_USAGE_02_PARAMETRIC_UV,
        ATTR_USAGE_03_PARAMETRIC_UV,
        ATTR_USAGE_04_PARAMETRIC_UV,
        ATTR_USAGE_05_PARAMETRIC_UV,
        ATTR_USAGE_06_PARAMETRIC_UV,
        ATTR_USAGE_07_PARAMETRIC_UV,
        ATTR_USAGE_08_PARAMETRIC_UV,
        ATTR_USAGE_09_PARAMETRIC_UV,
        ATTR_USAGE_00_FULLRANGE_UV,
        ATTR_USAGE_01_FULLRANGE_UV,
        ATTR_USAGE_02_FULLRANGE_UV,
        ATTR_USAGE_03_FULLRANGE_UV,
        ATTR_USAGE_04_FULLRANGE_UV,
        ATTR_USAGE_05_FULLRANGE_UV,
        ATTR_USAGE_06_FULLRANGE_UV,
        ATTR_USAGE_07_FULLRANGE_UV,
        ATTR_USAGE_08_FULLRANGE_UV,
        ATTR_USAGE_09_FULLRANGE_UV,
        ATTR_USAGE_00_RGBA,
        ATTR_USAGE_01_RGBA,
        ATTR_USAGE_02_RGBA,
        ATTR_USAGE_03_RGBA,
        ATTR_USAGE_04_RGBA,
        ATTR_USAGE_05_RGBA,
        ATTR_USAGE_06_RGBA,
        ATTR_USAGE_00_GENERIC_V1,
        ATTR_USAGE_01_GENERIC_V1,
        ATTR_USAGE_02_GENERIC_V1,
        ATTR_USAGE_03_GENERIC_V1,
        ATTR_USAGE_04_GENERIC_V1,
        ATTR_USAGE_00_GENERIC_V2,
        ATTR_USAGE_01_GENERIC_V2,
        ATTR_USAGE_02_GENERIC_V2,
        ATTR_USAGE_03_GENERIC_V2,
        ATTR_USAGE_04_GENERIC_V2,
        ATTR_USAGE_00_GENERIC_V3,
        ATTR_USAGE_01_GENERIC_V3,
        ATTR_USAGE_02_GENERIC_V3,
        ATTR_USAGE_03_GENERIC_V3,
        ATTR_USAGE_04_GENERIC_V3,
        ATTR_USAGE_00_GENERIC_V4,
        ATTR_USAGE_01_GENERIC_V4,
        ATTR_USAGE_02_GENERIC_V4,
        ATTR_USAGE_03_GENERIC_V4,
        ATTR_USAGE_04_GENERIC_V4,
        ATTR_USAGE_ENUM_COUNT
    };

    struct attribute
    {
        u16                             m_Offset;
        attribute_src                   m_DataType;
        attribute_usage                 m_UsageType;

        void Setup  ( attribute_usage UsageType, attribute_src DataType, s32 Offset )
        {
            ASSERT(Offset >= 0 );
            
            m_Offset    = u16(Offset);
            m_DataType  = DataType;
            m_UsageType = UsageType;
        }

        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == 4 );
            SerialFile.Serialize    ( m_Offset );
            SerialFile.SerializeEnum( m_DataType );
            SerialFile.SerializeEnum( m_UsageType );
        }
    };
    
    struct attribute_link
    {
        xserialfile::ptr<const char>    m_pVarName;
        attribute_usage                 m_UsageType;
        u8                              m_Pad[ 7 ];

        void Setup      ( const char* pVarName, attribute_usage UsageType )
        {
            m_pVarName  = pVarName;
            m_UsageType = UsageType;
        }
        
        void SerializeIO( xserialfile& SerialFile ) const
        {
            ASSERTCT( sizeof( *this ) == 8*2 );
            SerialFile.Serialize( m_pVarName, x_strlen( m_pVarName.m_Ptr ) + 1 );
            SerialFile.SerializeEnum( m_UsageType );
        }
    };
    
public:
    
                        eng_vertex_desc                 ( void ) {}
                       ~eng_vertex_desc                 ( void );
    void                SubmitAttributes                ( s32 VertSize, s32 nAttributes, attribute* pAttribute, xbool bFreeDataWhenDone );
    void                Destroy                         ( void );
    s32                 getVertSize                     ( void ) const { return m_VertexSize; }
    s32                 getAttributeCount               ( void ) const { return m_nAttributes; }
    const attribute&    getAttribute                    ( s32 Index ) const { return m_pAttribute.m_Ptr[ Index ]; }
    attribute&          getAttribute                    ( s32 Index )  { return m_pAttribute.m_Ptr[ Index ]; }
    void                SerializeIO                     ( xserialfile& SerialFile ) const
    {
        ASSERTCT( sizeof( *this ) == 8*2 );
        
        SerialFile.Serialize( m_nAttributes );
        SerialFile.Serialize( m_pAttribute, m_nAttributes );
        SerialFile.Serialize( m_VertexSize );
        SerialFile.Serialize( m_bFreeData );
    }

protected:
    
    void                Activate                        ( s32 Offset ) const;
    void                Deactivate                      ( void ) const;
    
protected:
    
    xserialfile::ptr<attribute>     m_pAttribute      { NULL };
    u16                             m_nAttributes   = 0;
    u16                             m_VertexSize    = 0;
    u8                              m_bFreeData     = FALSE;
    u8                              m_Padding[3];
    
    friend class eng_vbuffer;
    friend class eng_shader_program;
};

////////////////////////////////////////////////////////////////////////////////
// VERTEX BUFFER
////////////////////////////////////////////////////////////////////////////////
class eng_vshader;
class eng_vbuffer
{
public:
    
                   ~eng_vbuffer                     ( void );

    xbool           isValid                         ( void ) const { return m_IDBuffer != 0; }    
    void            CreateDynamicBuffer             ( const s32 VertSize, const s32 Count, const void* pData = NULL );
    void            CreateStaticBuffer              ( const s32 VertSize, const s32 Count, const void* pData = NULL );
    void            Destroy                         ( void );

    void            UploadData                      ( s32 iOffset, s32 Count, const void* pData ) const;
    void*           LockData                        ( void ) const;
    void            UnlockData                      ( void ) const;
        
    void            Activate                        ( const eng_vertex_desc& VertDesc, const eng_vshader& vShader, const s32 VertexOffset = 0 ) const;
    void            Deactivate                      ( const eng_vertex_desc& VertDesc ) const;
    
protected:
    
	GLuint                      m_IDBuffer          = 0;
    s32                         m_nVertices         = 0;
    s32                         m_VertSize          = 0;
    
#if defined(ENG_OPEN_GLES) && !defined(TARGET_3DS)
    GLuint                      m_VertexArray;
#endif

#ifdef X_DEBUG
    mutable s32                 m_LockCount         = 0;
#endif

    friend class eng_vbuffer;
};

////////////////////////////////////////////////////////////////////////////////
// INDEX BUFFER
////////////////////////////////////////////////////////////////////////////////

class eng_ibuffer
{
public:
    
    enum index_data_desc
    {
        DATA_DESC_U8,
        DATA_DESC_U16,
        DATA_DESC_U32,
    };
    
                   ~eng_ibuffer                     ( void );
    
    xbool           isValid                         ( void ) const { return m_IDBuffer != 0; }
    void            CreateDynamicBuffer             ( index_data_desc DataDesc, s32 Count, const void* pData = NULL );
    void            CreateStaticBuffer              ( index_data_desc DataDesc, s32 Count, const void* pData = NULL );
    void            Destroy                         ( void );
    
    void            UploadData                      ( s32 iOffset, s32 Count, const void* pData );
    void*           LockData                        ( void );
    void            UnlockData                      ( void );
    
    void            Activate                        ( void ) const;
    void            Deactivate                      ( void ) const;

    void            RenderTriangles                 ( void ) const;
    void            RenderTriangles                 ( s32 nIndices, s32 StartIndexOffset=0 ) const;
    void            RenderLines                     ( void ) const;
    void            RenderLines                     ( s32 nIndices, s32 StartIndexOffset=0 ) const;
    
protected:

	GLuint                      m_IDBuffer          = 0;
    index_data_desc             m_DataDesc          = DATA_DESC_U16;
    s32                         m_nIndices          = 0;

#ifdef X_DEBUG
    mutable s32                 m_LockCount         = 0;
#endif
};

////////////////////////////////////////////////////////////////////////////////
// VERTEX SHADER
////////////////////////////////////////////////////////////////////////////////
class eng_vshader
{
public:
           ~eng_vshader             ( void );

	void 	LoadFromFile            ( const char* VertexShaderName, const eng_vertex_desc::attribute_link* pLink, s32 nAttrLinks, xbool bFreeAttrLnk );
	void    LoadFromMemory          ( const void* pVertexShader, s32 ShaderLength, const eng_vertex_desc::attribute_link* pLink, s32 nAttrLinks, xbool bFreeAttrLnk );
    void    Destroy                 ( void );
    xbool   isValid                 ( void ) const { return m_GLHandle != 0; }
    
protected:
    
    void    Setup                   ( eng_vertex_desc::attribute_link* pLink, s32 nAttrLinks, xbool bFreeAttrLnk );
    
protected:
    
	GLhandleARB 	                                m_GLHandle          = 0;
    const eng_vertex_desc::attribute_link*          m_pAttrbuteLink     = NULL;
    u16                                             m_nAttributeLinks   = 0;
    u16                                             m_bFreeAttrLinks    = FALSE;
    
    friend class eng_shader_program;
    friend class eng_vbuffer;
};

////////////////////////////////////////////////////////////////////////////////
// FRAGMENT SHADER
////////////////////////////////////////////////////////////////////////////////
class eng_fshader
{
public:
           ~eng_fshader             ( void );
    
	void 	LoadFromFile            ( const char* FragmentShaderName );
	void    LoadFromMemory          ( const void* pFragmentShader, s32 ShaderLength );
    void    Destroy                 ( void );
	xbool   isValid                 ( void ) const { return m_GLHandle != 0; }

protected:
    
	GLhandleARB 	m_GLHandle     = 0;
    
    friend class eng_shader_program;
};

////////////////////////////////////////////////////////////////////////////////
// GEOMETRY SHADER
////////////////////////////////////////////////////////////////////////////////
class eng_gshader
{
public:
           ~eng_gshader             ( void );
    
	void 	LoadFromFile            ( const char* GeometryShaderName );
	void    LoadFromMemory          ( const char* pGeometryShader, s32 ShaderLength );
    void    Destroy                 ( void );
    xbool   isValid                 ( void ) const { return m_GLHandle != 0; }
	
protected:
    
	GLhandleARB 	m_GLHandle      = 0;
    
    friend class eng_shader_program;
};

////////////////////////////////////////////////////////////////////////////////
// RENDER_STATE
////////////////////////////////////////////////////////////////////////////////
class eng_render_state
{
public:
    
    // Choose one
    enum blend
    {
        BLEND_OFF,             // Default: No blending modes enable
        BLEND_ALPHA,           // Blend using the alpha channel
        BLEND_ADD,             // Blend to render target as additive mode
        BLEND_SUB,             // Blend to render target as subtractive mode
    };
    
    // Choose one
    enum zbuffer
    {
        ZBUFFER_OFF,           // Ignore the ZBuffer
        ZBUFFER_ON,            // Default: Use the ZBuffer
        ZBUFFER_READ_ONLY,     // Read but dont write to the Zbuffer
    };
    
    // Choose one
    enum raster_cull
    {
        RASTER_CULL_NONE,
        RASTER_CULL,
        RASTER_CULL_FRONT,
    };
    
    enum raster_fill
    {
        RASTER_FILL_FULL,
        RASTER_FILL_WIRE_FRAME,
    };

public:
    
    void setup      ( blend Blend = BLEND_ALPHA, zbuffer = ZBUFFER_ON, raster_cull RasterCull = RASTER_CULL, raster_fill RasterFill = RASTER_FILL_FULL );
    void Activate   ( void ) const;
    void Deactivate ( void ) const;

protected:
    
    blend           m_Blend         = BLEND_ALPHA;
    zbuffer         m_ZBuffer       = ZBUFFER_ON;
    raster_cull     m_RasterCull    = RASTER_CULL;
    raster_fill     m_RasterFill    = RASTER_FILL_FULL;
};

////////////////////////////////////////////////////////////////////////////////
// SHADER PROGRAM
//------------------------------------------------------------------------------
// Source Vertices -> VertexShader -> Geometry Shader -> Fragment Shader -> Frame Buffer
//
// TODO:
// * Make sure that when you link avaraible with a register it is the right type.
//  (Not sure it is possible in opengl)
////////////////////////////////////////////////////////////////////////////////
class eng_texture;

class eng_shader_program
{
public:

    enum uniform_type:u8
    {
        UNIFORM_TYPE_NULL,
        UNIFORM_TYPE_INFORMED_TEXTURE_RGBA,
        UNIFORM_TYPE_INFORMED_TEXTURE_RGB,
        UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL,
        UNIFORM_TYPE_INFORMED_BOOL,
        UNIFORM_TYPE_INFORMED_FLOAT,
        UNIFORM_TYPE_INFORMED_V2,
        UNIFORM_TYPE_INFORMED_V3,
        UNIFORM_TYPE_INFORMED_V4,
        UNIFORM_TYPE_INFORMED_M3,
        UNIFORM_TYPE_INFORMED_M4,
        UNIFORM_TYPE_SYSTEM_BASE,
        UNIFORM_TYPE_SYSTEM_L2C,
        UNIFORM_TYPE_SYSTEM_L2V,
        UNIFORM_TYPE_SYSTEM_V2C,
        UNIFORM_TYPE_SYSTEM_LIGHT_IN_L,
        UNIFORM_TYPE_SYSTEM_LIGHT_IN_V,
        UNIFORM_TYPE_ENUM_COUNT
    };


public:
           ~eng_shader_program              ( void );
    
    xbool   isValid                         ( void ) const                                  { return m_GLHandle != 0; }
    void    LinkShaders                     ( const eng_vshader&    VertexShader,
                                              const eng_gshader&    GeometryShader,
                                              const eng_fshader&    FragmentShader );
    
    void    LinkShaders                     ( const eng_vshader&    VertexShader,
                                              const eng_fshader&    FragmentShader );
    
    void    LinkTextureRegisterWithUniform  ( s32 iRegsiter, const char* pUniformVariableName );
    void    LinkRegisterToUniformVariable   ( s32 iRegister, const char* pUniformVariableName );
    void    Destroy                         ( void );
    
	void 	Activate                        ( void ) const;
    void    Deactivate                      ( void ) const;
    
	void 	setUniformVariable              ( s32 iRegister, f32 Value ) const;
	void 	setUniformVariable              ( s32 iRegister, s32 Value ) const;
	void 	setUniformVariable              ( s32 iRegister, const xvector2&  V ) const;
	void 	setUniformVariable              ( s32 iRegister, const xvector3d& V ) const;
    void 	setUniformVariable              ( s32 iRegister, const xvector3&  V ) const;
	void 	setUniformVariable              ( s32 iRegister, const xvector4&  V ) const;
	void 	setUniformVariable              ( s32 iRegister, const xmatrix4&  M ) const;
    void    setUniformVariable4F            ( s32 iRegister, const f32* F  ) const;
    void    setUniformVariable3F            ( s32 iRegister, const f32* F  ) const;
    void    setUniformVariable2F            ( s32 iRegister, const f32* F  ) const;
    void    setUniformVariableArray         ( s32 iRegister, s32 Offset, const xmatrix4*  pM, s32 Count  ) const;
    void    setUniformVariableArray         ( s32 iRegister, s32 Offset, const xvector3d* pV, s32 Count  ) const;

    GLhandleARB getHandle                   ( void ) const;
    
protected:

	GLhandleARB                 m_GLHandle              = 0;
    xsafe_array<GLint, 16>      m_NameRegisters;
    xsafe_array<GLint, 16>      m_NameTextures;
};

////////////////////////////////////////////////////////////////////////////////
// RENDER BUFFER
////////////////////////////////////////////////////////////////////////////////

class eng_render_buffer
{
public:
    
                        eng_render_buffer           ( void );
                       ~eng_render_buffer           ( void );
    void                CreateRenderBuffer          ( const eng_texture& Texture );
    void                CreateRenderBuffer          ( s32 Width, s32 Height );
    void                Destroy                     ( void );
    
    void                Activate                    ( const eng_view& View, const eng_texture& Texture ) const;
    void                Deactivate                  ( void ) const;
    
    s32                 getWidth                    ( void ) const { return m_W; }
    s32                 getHeight                   ( void ) const { return m_H; }
    
    static void         ActivateDefaultFrameBuffer  ( void );
    
protected:

    GLuint                      m_IDZBuffer;
	GLuint                      m_IDColorBuffer;
	GLuint                      m_IDFrameBuffer;
    s32                         m_W, m_H;
    mutable eng_view            m_BackUpEngView;
};

////////////////////////////////////////////////////////////////////////////////
// SIMPLE_SHADER
////////////////////////////////////////////////////////////////////////////////
struct draw_vertex;

class eng_simple_shader
{
public:

    using vertex    = draw_vertex;
    using attrlnk   = const eng_vertex_desc::attribute_link;
    using vattr     = const eng_vertex_desc::attribute;

    enum geometry_type   
    {
        GEOM_TYPE_CUBE,
        GEOM_TYPE_XZ_PLANE,
        GEOM_TYPE_SPHERE
    };

public:

    void CompileProgram     ( void );
    void CompileGeometry    ( geometry_type GeomType, xcolor Color );
    void Render             ( const xmatrix4& L2W ) const;

public:

    static attrlnk                          m_VAttrLnk[];
    static vattr                            m_VAttr[];
    eng_vertex_desc                         m_VertDesc;
    eng_vbuffer                             m_VertexBufer;
    eng_ibuffer                             m_IndexBuffer;
    eng_fshader                             m_FShader;
    eng_vshader                             m_VShader;
    eng_shader_program                      m_Program;
    s32                                     m_iL2CRegister = 5;
};
