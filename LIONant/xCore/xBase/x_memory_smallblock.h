//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
// WARNING:
// One issue with how new constructs is that if there is another type with the same
// name lets say inside a cpp file. Then there is a chance that it may not call
// the constructor this seems to be a bug in the ms compiler.
//==============================================================================

class small_block
{
public:

                    small_block             ( void );
                   ~small_block             ( void );
    void            Initialize              ( const char* pSizeString, void* pBlock=NULL, s32 Count=0 );

    void*           Malloc                  ( s32 Size );
    xbool           Free                    ( void* pPtr );

    xbool           SanityCheck             ( void ) const;
    s32             GetTotalMemory          ( void ) const;
    s32             GetTotalUsed            ( void ) const;
    s32             GetTotalFree            ( void ) const;
    s32             FragmentedMemory        ( void ) const;            
    s32             GetNumberOfAllocations  ( void ) const;

protected:

    enum
    {
        MAX_PAGE_SIZE = 8*1024
    };

protected:

    void            DonateBlock             ( void* pBlock, s32 Count );
    void*           AllocatePage            ( s32 iSize );
    s32             GetPageFromHash         ( const void* pPtr ) const;
    s32             ComputeHashEntry        ( const void* pData )const;
    s32             GetTypeSizeFromUserSize ( s32 UserSize ) const;

protected:

    struct page_entry
    {
        u16     m_UsedMemory;           // Total memory in used inside the page
        s16     m_iNextFreeEntry;       // Next free entry inside the page
        s16*    m_pPage;                // Pointer to the actual page
        s16     m_iNextPage;            // NextPage from the PagePool
        s16     m_iPrevPage;            // PrevPage from the PagePool
        s16     m_iNextHash;            // NextHash entry in case of hash collisions
        s16     m_iSize;                // Index to the size structure
    };

    struct sizes
    {
        s32     m_Size;                 // Actual size for entries inside of pages
        s32     m_TotalEntryCount;      // Total number of entries available
        s32     m_FreeEntryCount;       // Total free entries in the list
        s16     m_iNextFreePage;        // Link List of free pages with free entries
        s16     m_iNextFullPage;        // Link List of pages completely fill up
    };

protected:

    s32                             m_BlockSize;        // size of the donated block
    xbyte*                          m_pBlock;           // Main block donation
    xbool                           m_bFreePages;       // Tells the system whether you want to release pages back to the memory manager

    s32                             m_nSizes;           // Number of district size of entries
    xsafe_array<sizes, 16>          m_Sizes;            // Array of entry sizes

    s16                             m_iLastEmptyPage;   // This is the last page which was empty
    s16                             m_iNextFreePage;    // Link list of free Pages inside the pool
    xsafe_array<page_entry, 10000>  m_PagePool;         // Page pool containing all the data necessary for each page
    xsafe_array<s16, 10000>         m_PageHash;         // Hash table for quick lookups
};
