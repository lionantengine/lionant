//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//==============================================================================
// Classes
//==============================================================================
typedef s64 xtick;

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The time class is a handy way to measure how long has past since a particular event.
//     It is most offend used for performance measurement but could also be use for other 
//     things as well. Usually the game time is base on one of these and then the delta
//     time is pass to the OnAdvanceLogic to each of the objects. 
//
//<P>  The functions work as spected. The only tricky ones are the Trip function
//     which are similar to a Stop, Reset, Start sequence of calls. This strip 
//     function is specially handy to measure performance.
//
// Example:
//<CODE>
//      void main( void )
//      {
//          s32    i;
//          xticks Acc=0;
//          xtimer Timmer;
//          xtimer HoldTime;
//
//          // Start counting how long the hold thing is going to last
//          HoldTime.Start();
//          
//          for( i=0; i<100; i++ )
//          {
//              Acc += Timmer.Trip();   // Note that the first trip will be zero
//              SomeFunction();
//          }
//          
//          x_printf( "Hold Time %f average time %f", HoldTime.StopSec(), x_TicksToSec( Acc ) / (i-1) );
//      }
//</CODE>
//------------------------------------------------------------------------------
class xtimer
{
public:

                xtimer              ( void );

    void        Start               ( void );
    void        Reset               ( void );

    xtick       Stop                ( void );
    f64         StopMs              ( void );
    f64         StopSec             ( void );

    xtick       Read                ( void ) const;
    f64         ReadMs              ( void ) const;
    f64         ReadSec             ( void ) const;

    xtick       Trip                ( void );   // Similar to Stop, Reset, Start
    f64         TripMs              ( void );
    f64         TripSec             ( void );

    s32         GetSampleCount      ( void ) const;
    f64         GetAverageMs        ( void ) const;
    xbool       IsRunning           ( void ) const; 

protected:
    
    xtick   m_StartTime;
    xtick   m_TotalTime;
    xbool   m_bRunning;
    s32     m_nSamples;
};


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      Provides a Date class which represents dates as Julian day numbers
//      ( days since 1 Jan 4713 BC ).
//      This class can handle all dates from  1 Jan 4713BC to 31 Dec 9999AD.
//
//<P>   Note: Years AD are positive, years BC are negative. There is
//      no year 0AD, it goes from 1BC to 1AD. A year of 0 will be treated
//      as 1BC. No attempt is made to validate ranges. Physical errors
//      will not result from insane day-month combinations like the 87th
//      day of the 45th month, but the results will obviously not make
//      much sense.
//
//------------------------------------------------------------------------------
class xdate
{
public:

                    xdate           ( void );
    void            SetDay          ( s32 Day );
    void            SetMonth        ( s32 Month );
    void            SetYear         ( s32 Year );

    s32             GetDay          ( void ) const;
    s32             GetMonth        ( void ) const;
    s32             GetYear         ( void ) const;

    void            GetDate         ( s32& Moth, s32& Day, s32& Year ) const;
    void            SetDate         ( s32 Moth, s32 Day, s32 Year );
    s32             GetDate         ( void ) const;
    void            SetDate         ( s32 Date );

    void            SetToString     ( xstring& Date ) const;

    s32             GetDayOfTheWeek ( void ) const;
    s32             DayOfYear       ( void ) const;
    static s32      DaysInYear      ( s32 Year );

    static s32      DaysInMoth      ( s32 Month, s32 Year );
    static xbool    IsLeapYear      ( s32 Year );
    xbool           IsLeapYear      ( void ) const;

    xbool           operator ==     ( const xdate& Right ) const;
    xbool           operator !=     ( const xdate& Right ) const;
    xbool           operator <      ( const xdate& Right ) const;
    xbool           operator <=     ( const xdate& Right ) const;

    xdate&          operator +=     ( const s32 Right );
    xdate&          operator -=     ( const s32 Right );
    s32             operator -      ( const xdate& Right ) const;

    friend xdate    operator +      ( const xdate& Left, const s32 Right );
    friend xdate    operator +      ( const s32 Left, const xdate& Right );
    friend xdate    operator -      ( const xdate& Left, const s32 Right );
    friend xdate    operator -      ( const s32 Left, const xdate& Right );

protected:

    static void     GetJulianDate   ( s32 Julian, s32& Month, s32& Day, s32& Year );
    static s32      SetJulianDate   ( s32 Month, s32 Day, s32 Year );
    xdate&          Add             ( s32 Days );
    xdate&          Subtract        ( s32 Days );
    s32             Subtract        ( const xdate Other ) const;

protected:

    s32         m_Date;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This class is a standard wrapper class around the Coordinated Universal 
//      Time (UTC). In other words it contains how many seconds have elapsed 
//      since midnight (00:00:00), January 1, 1970. So there for it is not very 
//      accurate only seconds accuracy. The size of the structure is 64 bits to 
//      avoid failures after January 18, 2038. Make sure to remember that this 
//      class doesn't deal with any time prior to Jan 1, 1970.
//
//------------------------------------------------------------------------------
class xtime_utc
{
public:

    void                UpdateTime          ( void );
    void                Clear               ( void );

    s32                 GetSeconds          ( void ) const;
    s32                 GetMinutes          ( void ) const;
    s32                 GetHour24           ( void ) const;
    s32                 GetHour12           ( void ) const;
    xdate               GetDate             ( void ) const;
    u64                 GetRawData          ( void ) const { return m_Time; }
    void                SetRawData          ( u64 Data )   { m_Time = Data; }

    xbool               operator  <         ( const xtime_utc Time ) const;
    xbool               operator  <=        ( const xtime_utc Time ) const;
    xbool               operator  >         ( const xtime_utc Time ) const;
    xbool               operator  >=        ( const xtime_utc Time ) const;
    xbool               operator  ==        ( const xtime_utc Time ) const;

    xtime_utc           operator +          ( const xtime_utc Time ) const;
    xtime_utc           operator -          ( const xtime_utc Time ) const;

    const xtime_utc&    operator +=         ( const xtime_utc Time );
    const xtime_utc&    operator -=         ( const xtime_utc Time );

protected:

    u64         m_Time;
};

//==============================================================================
// FUNCTIONS
//==============================================================================

xtick       x_GetTime           ( void );
inline f64  x_GetTimeSec        ( void );

inline f64  x_TicksToMs         ( xtick Ticks );
inline f64  x_TicksToSec        ( xtick Ticks );

s64         x_GetTicksPerMs     ( void );
s64         x_GetTicksPerSecond ( void );

