#if 0

//==============================================================================
//
//  x_network.hpp
//
//==============================================================================
#pragma once		// Include this file only once
#ifndef X_NETWORK_H
#define X_NETWORK_H

//==============================================================================
//  INCLUDES
//==============================================================================

#ifndef X_PLUS_H
#include "x_plus.h"
#endif

//==============================================================================
// Classes
//==============================================================================
#ifdef TARGET_360
    #define XNET_DEFAULT_GAME_PORT 1000	    // using port 1000 on XBOX saves 2-4 bytes per packet
#else
	#define XNET_DEFAULT_GAME_PORT 4200
#endif

//------------------------------------------------------------------------------
// net address contains the address of a connection IP and the port number.
//------------------------------------------------------------------------------
class xnet_address
{
public:

                        xnet_address        ( void );
                        xnet_address        ( u32 IP, s32 Port ) { m_Port = Port; m_IP = IP; }
        void            Clear               ( void );
        xbool           isValid             ( void ) const;

        void            Setup               ( u32 IP, s32 Port ); 
        void            SetupLocalIP        ( void );
        void            SetIP               ( u32 IP );
        void            SetPortNumber       ( s32 Port );
        const char*     SetStrIP            ( const char* pIPStr   );
        const char*     SetStrAddress       ( const char* pAddrStr );

        u32             GetIP               ( void ) const;
        s32             GetPort             ( void ) const;
        xstring         GetStrIP            ( void ) const;
        xstring         GetStrAddress       ( void ) const;

        xbool           operator ==         ( const xnet_address& A ) const;
        xbool           operator !=         ( const xnet_address& A ) const;

private:    

        u32             m_IP;
        s32             m_Port;
};

//------------------------------------------------------------------------------

enum xnet_send_flags
{
    XNET_FLAG_RELIABLE      = X_BIT(12),        // Set this flag for reliable packets
    XNET_FLAG_SENDSIZE      = X_BIT(13),        // Set this flag when you want to get the size of the packet in the other end.
    XNET_FLAG_IN_ORDER      = X_BIT(14),        // NO IMPLEMENTED YET
    XNET_FLAG_RETRANSMITTED = X_BIT(15),        // SYSTEM ONLY
    XNET_FLAG_MASK          = X_BIN(1111)*X_BIT(12)
};

//------------------------------------------------------------------------------

enum xnet_system_headers
{
    XNET_HEADER_INVALID   = -1,
    XNET_HEADER_HANDSHAKE = 1025,               // Header received when someone is trying to connect to you
    XNET_HEADER_ACK,                            // Header received when there is an ack of a reliable message
    XNET_HEADER_DISCONNECT,                     // Header received when someone disconnected us
    XNET_HEADER_HEARTBEAT,                      // Header received by the low level system when a a certain time passes and the client didn't send us anything
    XNET_HEADER_MAX_SYSTEM_HEADERS,
    XNET_HEADER_MASK      = ~XNET_FLAG_MASK
};

//------------------------------------------------------------------------------

struct xnet_ack_receipt 
{
    s32         m_AckHeader;        // The header that we are acknowledging
    xhandle     m_ClientID;         // The client that we sent the packet
    void*       m_pUser1;           // User data 1
    void*       m_pUser2;           // User data 2
};

//------------------------------------------------------------------------------

X_HANDLE_TYPE( xnet_global_clientid );

//------------------------------------------------------------------------------
// Author:
//     Tomas
// Summary:
//     Low level network class
// Description:
//     xnet is used as a direct replacement for sockets. The concept of the class
//     is to give the basic framework that most games and applications will need.
//     It still a very low level layer and only provides the simplest things,
//     such reliable packets, packing packets, support for very large packets, and
//     a very simple client management. 
//
//<P>  The overhead of a package is 3 bytes. There is a CRC for the hold package is the first
//     2 bytes. This makes sure that the remainder of the package is good, then one byte for time.
//     This time counter resets it self every 3 minutes or so. This time is also part of the CRC so
//     it is impossible to corrupt the package without the CRC knowing it. (Unless the CRC it self fails)
//     The time is used to filter older packagers and also help have a sense of sequence. Another way
//     to think of it is the upper 8 bytes of a 16 bits sequence number but factor out in the top. But
//     of course the clock is better because it also gives a sense of time. The CRC will use a special 
//     key to help compute the current CRC. This key is design to create a 24 bit time sequence for packages.
//     Basically is the upper 2 bytes of the netclock these two bytes are roll back in the CRC which allows
//     to reject very old packages.
//
//<P>  The reliable protocol is done trying hard to minimize the over head. The principal is simple
//     when a packet of such type is send a single bit will identify it, and a byte is added to
//     give a unnique ID. The UID gets incremented for every reliable packet sent. The hoppe is to spread UIDs
//     so that the client gets a unique but in sequence UIDs. Because of the slowness of incrementation
//     UID it should make the system take a small while before it has to roll over. 
//
//<P>  The reliable protocol has a resent packet filter. What this means is when an ACK packet is lost
//     in the network the client which originally sent the reliable packet will think it didn't arrive so
//     it will send it again, althought the original packet did arrive. Here is when the filter kicks in,  
//     base on some previously store information about the original packet the client can check whether 
//     the newly resent packet matches an exiting receipt. If it does then it is discarded. 
//
//<P>  The reliable system fails under the fallowing situations:
//<P><B>Situation 1:</B>
//<P>  If the server sends a reliable packet and it is received fine. Then the client sends back an ACK but 
//     before the ACK get there the server resends the packet again. The client will think the ACK was lost
//     and will send another ACK. So the server may get multiple ACKs for the same packet. Althought the 
//     server has also a multiple ACK filter which should filter most duplicated ACK.
//
//<P>  * Currently that is the only situation that I can think of how it will fail. I am sure there are other
//       and if you can think of them I will like to hear them.
//
//<P>  The usage is pretty simple. You first call Open this will open the connection in your machine.
//     Then you can call ConnectToClient to connect to an exiting server if you want. If not you dont have to do anything.
//     At this point you can send/receive packets. Send is very simple you must have either a server or an active client.
//     To received you do the fallowing sequence in order:
//     First you PullNextHeader this will try to get anything from the socket layer. It will return a header to a packet.
//     After that you can call any of the Receive functions but the final function call must be ReceivedPacket.
//     After that you can call PullNextHeader again for the next packet. 
//
//<P><B>Connecting to another computer:</B>
//<P>  Ones you call to ConnectToClient a XNET_HEADER_HANDSHAKE reliable packet will be send.
//     This packet means someone is trying to connect to you. If you dont want that client connecting to you call
//     DisconnectClient. At that point a XNET_HEADER_DISCONNECT non reliable packet will be sent to the client.
//     and the client will be removed from your local list. 
//
// Example:
//<CODE>
//     void Test( void )
//     {
//           xnet_connection Connection[2];
//           char Hello[] ="Hello World!!!";
//           char Free[256];
//           Connection[0].Open();
//           Connection[1].Open();
//           
//           xhandle ClientID;
//           Connection[0].ConnectToClient( Connection[1].GetLocalAddress(), ClientID, Hello, x_strlen(Hello)+1 );
//           Connection[0].FlushAll();
//           
//           s32 Header = Connection[1].PullNextHeader();
//           s32 Size   = Connection[1].ReceiveSize();
//           
//           Connection[1].ReceivedPacket( Header, Free, Size );
//           Connection[1].FlushAll();
//           
//           xnet_ack_receipt  Receipt;
//           s32 Header2 = Connection[0].PullNextHeader();
//      }    Connection[0].ReceivedPacket( Header2, &Receipt, sizeof(Receipt) );
//</CODE>     
// TODO:
//     * I need to find what the band width for the clients are
//     * Still need to know when to Flush the packets out
//     * Need to have a way to access low level stats
//     * Need to implement the XNET_FLAG_IN_ORDER
//     * Need to implement broudcast
//     * Need to set level of debug to avoid too much log data
//     * Need to have an option to block on receive
//     * Need to add a function to flash the client buffer per client
//     * We may need an option to stop hardbeats which are apps that dont care for it
//     * Need to add the block send block received so threads dont spend their lives pulling and sucking cpu resources
//
// See Also:
//     ....empty add or delete
//------------------------------------------------------------------------------
class xnet_connection
{
public:

    xbool                   Open                    ( s32 Port = XNET_DEFAULT_GAME_PORT, xbool bDoAudio = FALSE );
    void                    Close                   ( void );

    void                    Update                  ( void );

    xbool                   ConnectToClient         ( const xnet_address& Address, xhandle& ClientID, const void* pData=NULL, s32 Length=0, const void* pUser1=NULL, const void* pUser2=NULL );
    xbool                   DisconnectClient        ( xhandle ClientID, void* pData=NULL, s32 Length=0 );

    xbool                   SendToClient            ( xhandle ClientID, s32 Header, const void* pData=NULL, s32 Length=0, u16 Flags=0, const void* pUser1=NULL, const void* pUser2=NULL );        
    xbool                   SendToClient            ( xhandle ClientID, s32 Header, xbitstream& Bitstream, xbool bReliable=FALSE, const void* pUser1=NULL, const void* pUser2=NULL );        

    s32                     PullNextHeader          ( xbool bBlock = FALSE );
    s32                     ReceiveSize             ( void );
    xbool                   ReceivedPacket          ( s32 Header, void* Buf, s32 Len );
    xbool                   ReceivedPacket          ( s32 Header, xbitstream& BitStream );
    xbool                   SkipSystemPacket        ( s32 Header );
    xhandle                 ReceicedClient          ( void );
    void                    FlushAll                ( void );

    xhandle                 AddressToClient         ( const xnet_address& Address ) const;
    void                    GetClientAddress        ( xhandle ClientID, xnet_address& Address ) const;
    s32                     GetClientPackageSize    ( xhandle ClientID ) const;
    xnet_global_clientid    GetGlobalClientID       ( xhandle ClientID ) const;

    xnet_address            GetLocalAddress         ( void ) { return m_LocalAddress; }
                            xnet_connection         ( void );
                           ~xnet_connection         ( void );
protected:

    struct reliable
    {
        u8                                  m_NetTime;          // Time when it was sent
        xtick                               m_RealTime;         // High resolution clock
        s32                                 m_RetryTimes;       // How many times have we try to send this packet

        u8                                  m_UID;              // Unique ID for this package
        s32                                 m_iNext;            // Link list of free nodes
        xhandle                             m_ClientID;         // This is the ID of the client that we have sent the package
        s32                                 m_Header;           // Header that was sent as reliable 
        const void*                         m_pUserData1;        // Void pointers for the user to do what ever he wants
        const void*                         m_pUserData2;        // Void pointers for the user to do what ever he wants
        
        u16                                 m_Flags;            // Back up the flags as well
        s32                                 m_Length;           // Length of the packet
        xsafe_array<xbyte,1024>             m_BackupPacket;     // Backup Packet in case we need to resend

        void                Clear               ( void );

    };

    // We make sure that the ack gets to the other end. The way to achive that is that we keep a record
    // of the reliable packet. If the client doesnt get the ack it will sent it again. By having the CRC
    // of the original packet we should be able to notice that we got the sameone again.
    // So that we will send back only the receipt and throw away the packet.
    struct ack_receipt_sent
    {
        u32                                 m_CRC;              // CRC of the packet 
        u16                                 m_Length;           // The length of the package
        u16                                 m_Header;           // Header
        u8                                  m_Time;             // Time when the ack was sent the first time updated as necesary
    };

    // Based on conformance testing by microsoft, the max payload size of packet should be
    // no larger than 1304.  Reliable packet size is reduced to required less buffer space
    // for a fixed number of reliable packets.
    struct package
    {
        xhandle                             m_ClientID;         // This is the identifier of the client that we are sending the package for. -1 if it is a broudcast.
        xsafe_array<u8,1024>                m_Buffer;           // Buffer of the data for the package
        mutable s32                         m_iCursor;          // Current writting cursor
        xbool                               m_bInUsed;          // Whether it is been used corrently
        s32                                 m_Length;           // Length of the data inside the buffer
        xtick                               m_StartTime;        // When the package was first created and started to be fill in

        u16                                 GetCRC                  ( void ) const;
        u8                                  GetClock                ( void ) const;
        u16                                 ComputeCRC              ( void ) const;
        void                                Write                   ( const void* pSrc, s32 Length );
        void                                Read                    ( void* pSrc, s32 Length ) const;
        void                                Peek                    ( s32 At, void* pSrc, s32 Length ) const;
        s32                                 GetHeaderSize           ( void ) const { return 1 + 2; } // 1 for the clock + 2 for the CRC
        void                                Reset                   ( void );
        void                                WriteTime               ( u8 Time );
        void                                WriteCRC                ( u16 CRC );

        template<class T> inline
        void                                Write                   ( T Data ){ Data = x_NetworkEndian(Data); Write( &Data, sizeof(T) ); }

        template<class T> inline
        void                                Read                    ( T& Data ) const { Read( &Data, sizeof(T) ); Data = x_NetworkEndian(Data); }

        template<class T> inline
        void                                Peek                    ( s32 At, T& Data ) const { Peek( At, &Data, sizeof(T) ); Data = x_NetworkEndian(Data); }
    };

    enum client_state
    {
        XNET_CLIENTSTATE_UNUSED,        // Currently all the data in the client structure is invalid
        XNET_CLIENTSTATE_DISCONNECTED,  // The client has disconnected us. It is upto the user to delete the client
        XNET_CLIENTSTATE_TIMEOUT,       // We can't seem to get any packet from this dude.
        XNET_CLIENTSTATE_CONNECTING,    // The client is in the process of connecting
        XNET_CLIENTSTATE_CONNECTED,     // The client got an ack about the handshake
    };

    struct client
    {
        xnet_address                            m_Address;               // Net address includes the port
        client_state                            m_State;                // Current state of the client
        s32                                     m_iOutBound;            // Index to an outbound package
        xsafe_array<ack_receipt_sent,256>       m_AckReceiptSent;       // This is a table used to make sure we dont get the same packet twice if he didnt get the ack
        u8                                      m_LastUID;              // A running unique id for reliable data. The lowest bits are for the UID. The next 2 bytes are the CRC mix number.
        u32                                     m_NetClockDelta;        // This is used to reset the net clock to this client connection.
        s32                                     m_iGlobalClient;        // Index to the global client

        void                Clear               ( void );
    };

    enum
    {
        MAX_NETCLOCK_DISTANCE           = 5,                // Maximun age that the network packages can be from each other
        WEAKUP_INTERVAL                 = 60,               // How many millisecond should pass before we do a check on things
        NET_RETRYTIME	                = 750,              // Time after sending before we resend (for acks) (milliseconds)
        MAX_RETRY_TIMES                 = 10,               // Maximun times that we will retry sending the reliable packet
        MAX_TIME_BEFORE_SENDING_PACKAGE = 100,              // Maximun time before we need to Flush the package out
        NET_TIME_STEP                   = (3*60*1000)/255,  // This is +-3 minutes with 60 sec each converted to millisecons...etc. How fast one unique of net time is.  
    };

    struct real_time_ticks
    {
        xtick       m_WeakUpInterval;               // Same as WEAKUP_INTERVAL
        xtick       m_RetryTime;                    // Same as NET_RETRYTIME
        xtick       m_MaxTimeToFlushOutPackage;     // Same as MAX_TIME_BEFORE_SENDING_PACKAGE
        xtick       m_NetTimeStep;                  // Same as NET_TIME_STEP
    };

protected:

    u32                     GetClientClock      ( xhandle ClientID ) const;
    u32                     GetLocalIP          ( void ) const;
    void                    Clear               ( void );
    xbool                   SysSendToClient     ( xhandle ClientID, s32 Header, const void* pData, s32 Length, u16 Flags=0, const void* pUser1=NULL, const void* pUser2=NULL, s32 Sequence=0  );        
    s32                     GetFreePackage      ( void );
    void                    DeletePackage       ( s32 Index );
    xbool                   FlushPackage        ( s32 iOutBound );
    xbool                   NeedBufferForClient ( xhandle ClientID, s32 Length, u16 Flags );
    xbool                   SendTo              ( xnet_address Address, void* pData, s32 Length );
    xbool                   ReadFromSocket      ( void* pData, s32 MaxLength, s32& Length, xnet_address& FromAddr, xbool bBlock );
    s32                     GetTimeAge          ( u8 CurrentTime, u8 aTime ) const;
    xbool                   PullPackage         ( xbool bBlock  );
    s32                     AllocReliableEntry  ( xhandle ClientID );
    void                    FreeReliableEntry   ( s32 iReliable );
    s32                     GetReliableEntry    ( xhandle ClientID, s32 UID );
    xhandle                 AddClient           ( const xnet_address& Address );
    void                    DeleteClient        ( xhandle ClientID );
    void                    UpdateHardbeat      ( xtick RealTime );
        
protected:

    real_time_ticks                         m_Ticks;
    xtick                                   m_NextUpdate;           // This is the next time when the system will weak up and check on things
    u32                                     m_CurNetTime;           // This counter gets updated every 2 seconds or so
    xtick                                   m_NextNetTime;          // This is the next time nettime needs to be updated

    s32                                     m_iReliableFree;        // Head for the reliable link list of reliable packets
    xsafe_array<reliable,128>               m_ReliableTable;        // This table contains all the reliable data sent and waiting to be acknowleage

    xsafe_array<client, 128>                m_Client;               // Client list

    xsafe_array<package, 8>                 m_OutboundData;         // Data that is pending to be send
    package                                 m_InboundData;          // Data that is pending to be read

    xnet_address                            m_LocalAddress;         // Local address
    s32                                     m_Socket;               // Socket 
};
    
//------------------------------------------------------------------------------
// Global Functions
//------------------------------------------------------------------------------

s32 x_NetGetAvgSentBytesPerSec      ( xnet_global_clientid GlobalClientID );
s32 x_NetGetAvgReceivedBytesPerSec  ( xnet_global_clientid GlobalClientID );
f32 x_NetGetClientPing              ( xnet_global_clientid GlobalClientID );

//------------------------------------------------------------------------------
// Lag Simulator
//------------------------------------------------------------------------------

void x_NetEnableLagSimulator ( s32 PercentagePackageLost=10, s32 PercentageCurruptedData=2, s32 PercentageLagNoise=30, s32 PercentageLagDistance = 4 );
void x_NetDisableLagSimulator( void );

//==============================================================================
// PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! 
//==============================================================================

void x_NetInit( void );
void x_NetKill( void );

//==============================================================================
// INLINES
//==============================================================================
//#include "x_network_inline.h"

//==============================================================================
// END X_NETWORK_H
//==============================================================================
#endif


#endif