//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The command line class is used to help process command line options. These
//     options usually come from a DOS pront but they can also come from any other source.
//     The command handling is very verbose the help handle any time of commands. 
//
//<P>  There is two steps needed for the use of the class. First is to define valid commands.
//     This is done with the AddCmdSwitch switch. These will define all valid switchs that the
//     program can understand. Each switch in turn can be of different types. The list can 
//     be found here xcmdline::type.  The class supports response files. This means that if the your 
//     enters @FileName it will  read command switches from that file. This is very handy to create 
//     scripts and very large command line lists. Inside the file the user can also enter other 
//     response file switches.
//
//<P>  One of the key commands which by default is supported is the questions mark '?'.
//     This will indicate to the class that the user needs help. This can get gatter by
//     calling the function. DoesUserNeedsHelp right after the Parse function.
//
//<P>  The second step is to call the apropiate Parse function. This function will start the  
//     process of pasing. The function GetCommandCount will allow you to loop throw all the switches.
//     Each switch will return any parameters as an xstring. You are responsable for converting 
//     those arguments to the actual type. Via x_atof32 or what ever other function you may need.
//     The class is design to allow reprocessing of the command line. Simply call ClearArguments
//     and ask the class to Parse some more.
//
//<P>  Explaining AddCmdSwitch(        // * The return value of the AddCmdSwitch is the ID of the switch
//         const char* pName,          // First is the name of the switch
//         s32 MinArgCount=0,          // The switch itself may contain arguments so this is min count of them
//         s32 MaxArgCount=-1,         // This is MAX count of the arguments. Note -1 is infinity
//         s32 nMinTimes=0,            // Number of times this switch can appear in the arguments
//         s32 nMaxTimes=-1,           // This how the maximun of times... and again -1 == infinity
//         xbool MustFallowOrder=FALSE,// This indicate if the switch must follow the order of its declaration via (AddCmdSwitch)
//         type Type = TYPE_STRING,    // The type that the arguments will be in
//         xbool bMainSwitch = FALSE,  // There is only 1 main switch allowed at a time. So in a way it to make the switch exclusive.
//                                     // Main switches may have children switches that are not main switches.
//         s32 iParentID=-1 );         // Switches can be hirarchical so one switch may have other children switches
//
// Example:
//<CODE>
// void main( void )
// {
//     xcmdline        CmdLine;
//     const char*     pString = "-FileName SomeFile.txt -Res 640 480";
//  
//     // Add the command switches
//     CmdLine.AddCmdSwitch( "FileName", 1, 1, 1, 1, FALSE, xcmdline::TYPE_STRING );
//     CmdLine.AddCmdSwitch( "Res", 2, 2, 1, 1, FALSE, xcmdline::TYPE_INT );
//  
//     // handy way of dealing with string compares
//     static u32 FileNameCRC = x_strCRC( "FileName" );
//     static u32 ResCRC      = x_strCRC( "Res" );
//  
//     // Parse the command line
//     CmdLine.Parse( pString );
//     if( CmdLine.DoesUserNeedsHelp() )
//     {
//         x_printf( "-FileName <filename> -Res <xres> <yres> \n" );
//         return;
//     }
//  
//     // Handle parameters
//     for( s32 i=0; i<CmdLine.GetCommandCount(); i++ )
//     {
//         u32 CmdCRC = CmdLine.GetCmdCRC(i);
//         if( CmdCRC == FileNameCRC )
//         {
//             s32 Offset = CmdLine.GetCmdArgumentOffset(i);
//             const xstring& String = CmdLine.GetArgument( Offset );
//         }
//         else if( CmdCRC == ResCRC )
//         {
//             s32 Offset = CmdLine.GetCmdArgumentOffset(i);
//             s32 XRes   = x_atoi32( CmdLine.GetArgument( Offset + 0 ) );
//             s32 YRes   = x_atoi32( CmdLine.GetArgument( Offset + 1 ) );
//         }
//     }
// }
//</CODE>                         
//------------------------------------------------------------------------------
class xcmdline
{
public:
    
    enum type
    {
        TYPE_NONE,
        TYPE_INT,
        TYPE_HEX,
        TYPE_FLOAT,
        TYPE_GUID,
        TYPE_STRING,
        TYPE_STRING_RETAIN_QUOTES,


        TYPE_PADDING        = 0xffffffff
    };

public:
                    xcmdline            ( void );

    void            ClearArguments      ( void );

    s32             AddCmdSwitch        ( const char* pName, s32 MinArgCount=0, s32 MaxArgCount=-1, 
                                          s32 nMinTimes=0, s32 nMaxTimes=-1, xbool MustFallowOrder=FALSE, 
                                          type Type = TYPE_STRING, xbool bMainSwitch = FALSE, s32 iParentID=-1 );

    xbool           DoesUserNeedsHelp   ( void );

    void            Parse               ( s32 argc, const char** argv );
    void            Parse               ( const char* pString );

    s32             GetCommandCount     ( void ) const;
    s32             GetArgumentCount    ( void ) const;

    u32             GetCmdCRC           ( s32 Index ) const;
    const xstring&  GetCmdName          ( s32 Index ) const;
    s32             GetCmdArgumentOffset( s32 Index ) const;
    s32             GetCmdArgumentCount ( s32 Index ) const;

    const xstring&  GetArgument         ( s32 Index ) const;

protected:

    struct cmd_def
    {
        xstring         m_Name;             // switch name
        u32             m_crcName;          // CRC of the switch name    
        s32             m_MinArgCount;      // Minimum number of arguments that this switch can have
        s32             m_MaxArgCount;      // Max number of arguments that this switch can have    
        s32             m_nMaxTimes;        // MAximun of times that this switch can exits (-1) infinite
        s32             m_nMinTimes;        // Minimum of times that this switch can exits (-1) infinite
        s32             m_RefCount;         // Number of instances at this time pointing at this type
        xbool           m_bFallowOrder;     // This argument is in the proper order can't happen before
        type            m_Type;             // Type of arguments that this switch can have
        xbool           m_bMainSwitch;      // This to show whether is a main switch. The user can only enter one switch from this category
        s32             m_iParentID;        // This indicates that this switch can only exits if iParentID command is already active
    };

    struct cmd_entry
    {
        s32             m_iCmdDef;
        s32             m_iArg;
        s32             m_ArgCount;
    };

protected:

    void            AppendCmd           ( s32 iCmd, xstring& String );
    void            AppendCmd           ( s32 iCmd );
    void            ProcessResponseFile ( xstring& PathName );
    void            Parse               ( xarray<xstring>& Arguments );

protected:

    xbool                   m_bNeedHelp;
    xarray<cmd_def>         m_CmdDef;
    xarray<xstring>         m_Arguments;
    xarray<cmd_entry>       m_Command;
};
