//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     xptr_ref is a very safe way to pass a reference to a block of memory. 
//     Because it will check for the bounce when accessing to it. This allows things
//     like xsafe_array and xptr to pass them selfs to functions.
//
// Example:
//<CODE>
//      void TestFunction( xptr_ref<s32> Test )
//      {
//          for( s32 i=0; i<Test.GetCount(); i++ )
//          {
//              Test[i] = i;
//          }
//      }
//
//      void main( void )
//      {
//          xsafe_array<s32,32>  Array;
//          xptr<s32>            Ptr;
//          Ptr.Alloc( 32 );
//          
//          TestFunction( Array );
//          TestFunction( Ptr );
//      }
//</CODE>
// See Also:
//     xptr_lock xarray xharray xptr xsafe_array
//------------------------------------------------------------------------------
template<class T>
struct xptr_ref
{
public:
                              template< class C >
                              xptr_ref                ( C& A ) : m_pPtr(A), m_Count(A.getCount()) {}
                              xptr_ref                ( T* pPtr, s32 Count ) : m_pPtr(pPtr), m_Count(Count) {}

    inline	  s32             getCount                ( void ) const	{ return m_Count; }
    inline			  		  operator T*             ( void )			{ return m_pPtr;  }
    inline		              operator const T*       ( void ) const	{ return m_pPtr;  }
    inline    T&              operator[]              ( s32 Index );
    inline    T&              operator[]              ( u16 Index );
    inline    T&              operator[]              ( s16 Index );
    inline    T&              operator[]              ( u8 Index );

protected:

    s32 m_Count;
    T*  m_pPtr;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     xptr is a very safe way to create buffers. It has a reference count so it knows
//     when to delete it self. You can also have xptr point to other xptr. Which is 
//     handy when returning an xptr from a function. 
//
//<P>  xptr can either work with New or Alloc the difference is that one calls the 
//     constructors and the other doesnt. Additionally you can copy to the buffer
//     using its own Copy function which is totally safe.
//
//<P>  You can use xptr_lock with xptr which adds another layer of safety.
//
// Example:
//<CODE>
//      void main( void )
//      {
//          xptr<s32>   MyBuffer;
//
//          // Allocate the buffer
//          MyBuffer.Alloc( 1024 );
//          for( s32 i=0; i<MyBuffer.GetCount(); i++ )
//          {
//              MyBuffer[i] = x_rand();
//          }
//          
//          // Dont need to Free the buffer it knows when it has to do it
//      }
//</CODE>
// See Also:
//     xptr_lock xarray xharray xptr_ref
//------------------------------------------------------------------------------

// For movable memory it should allowcate a temporary structure
// For a memory reference count it should also be in a different structure
template< class T >
class xptr
{
public:
    
    enum flags
    {
            FLAG_STATIC_MEMORY = X_BIT(0),      // It wont be deleted or changed in size
            FLAG_READ_ONLY     = X_BIT(1)       // Can Not Write to this memory
    };
    
    typedef T type;
    
public:
    
    
    
                            xptr                ( void );
                            xptr			    ( xptr<T>& Ptr );
                   explicit xptr			    ( const xptr<T>& Ptr );
                            xptr                ( T* pPtr, s32 Count, u32 Flags );
                            template< class K, s32 Count >
                   explicit xptr                ( xsafe_array<K, Count>& Ptr );
                            template< class K, s32 Count >
                   explicit xptr                ( const xsafe_array<K, Count>& Ptr );
                           ~xptr                ( void );
        void                StartAccess         ( void ) const;
        void                EndAccess           ( void ) const;

        template< class J >
        T&                  operator []         ( J Index );

        template< class J >
        const T&            operator []         ( J Index ) const;

        const T*            operator ->         ( void ) const;
        T*                  operator ->         ( void );
        const xptr<T>&	    operator =		    ( const xptr<T>& Ptr );
                            operator const T*   ( void ) const;
                            operator T*         ( void );
     		                operator const xptr<T> ( void ) const	{ return xptr<T>(*this, FALSE, FLAG_READ_ONLY );  }
        void			    Alloc               ( s32 Count=1, xmem_flags Flags = XMEM_FLAGS_DEFAULT );
        void			    New                 ( s32 Count=1, xmem_flags Flags = XMEM_FLAGS_DEFAULT );
        void                Resize              ( s32 NewCount );
        void                Destroy             ( void );
        xbool               isValid			    ( void ) const;
        s32                 getCount            ( void ) const;
        s32                 getByteCount        ( void ) const;
        s32                 getXPtrCount        ( void ) const;
        s32                 getXRefCount        ( void ) const;
        void                Copy                ( s32 DestinationOffset, const xptr<T>& SourceRef, s32 SourceOffset, s32 ElementCount );
        void                Copy                ( const xptr<T>& SourceRef );
        void                SetMemory           ( s32 Value );
    
        xiter< xptr<T>, T >                begin    ( void )          { return xiter< xptr<T>, T >(0,*this); }
        xiter< xptr<T>, T >                end      ( void )          { return xiter< xptr<T>, T >(getCount(),*this); }
        xiter< const xptr<T>, const T >    begin    ( void ) const    { return xiter< const xptr<T>, const T >(0,*this); }
        xiter< const xptr<T>, const T >    end      ( void ) const    { return xiter< const xptr<T>, const T >(getCount(),*this); }
    
    

protected:

    struct destructor				// Handy class make sure that T have a destructor 		
    {  
        inline ~destructor(){} 
        T	    m_Item; 
    };     

protected:
    
        struct x_smart_xptr
        {
            x_qt_counter    m_RefCount;
#if defined X_DEBUG
            x_qt_counter    m_nAccesses;
#endif
            u32             m_bStaticMemory:1,     // Tells this pointer whether he is the owner of the memory
                            m_bIsUnresizable:1,    // It tells the pointer whether it can change its size to something else
                            m_bWasAlloc:1,         // Wether we need to call the destructors
                            m_bIsReadOny:1;        // Tells the pointer whether this memory is read only
        };
    
        T*                  getPtr          ( void ) const;
        x_smart_xptr&       getSmartPointer ( void ) const;

protected:

        enum count_mask:u32
        {
            COUNT_MASK = 0x0fffffff
        };

        T*              m_pPtr;                 // The actual pointer
        u32             m_Count;                // Number of elements after & with mask
    
    union
    {
        u32             m_Flags;

        struct
        {
            u8          m_bStaticMemory:1,     // Tells this pointer that we did not allocate the memory.
                        m_bIsUnresizable:1,    // It tells the pointer whether it can change its size to something else
                        m_bWasAlloc:1,         // Wether we need to call the destructors
                        m_bIsReadOny:1;        // Tells the pointer whether this memory is read only
        };
    };
    
    
protected:

    friend class xptr_lock;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     xptr_lock is a safety feature that you can have for your data. It is used to lock
//     a buffer from changing, either because of a reallocation or because of a free.
//     It is used mostly in conjunction with x_ptr, and containers such x_array.
//     xptr_lock has optimum performance because a release time they all go away. So this
//     is one of those things that you can put as many as you like.
//
//<P>  The xptr_lock can only be use in functions and therefor its scope is limit to the
//     function it self. It is very handy to detect problems such: You have a reference
//     of an object, and another function tries to append another object in a container
//     which may cause to reallocte which will cause the first reference to be invalid.
//
//<P>  If you create a class with a container or an xptr and you would like to support
//     xptr it is a very simple process. You need to create two functions:
//<P>          void StartAccess( void )
//<P>          void EndAccess( void )
//<P>  Inside of them you just need to call the equivalent functions to any of your
//     containers or xptr. Remember thought that at release time this function calls
//     will never happen. 
//
// Example:
//<B>  Something that can be dangerous </B>
//<CODE>
//      void main( void )
//      {
//          xarray<s32> List;
//          s32&        FirtEntry   = List.Append();
//
//          // This could end in a possible bug because after the append is executed 
//          // it may realloc making the first reference invalid.
//          s32&        SecondEntry = List.Append();
//      }
//</CODE>
//<P><B> Something allot safer </B>
//<CODE>
//      void main( void )
//      {
//          xarray<s32> List;
//          s32         Index1, Index2;
//          List.Append( Index1 );
//          List.Append( Index2 );
//
//          // We add the xptr_lock here to make sure no one else is a smart boy and try
//          // to reallocate our buffer.
//          xptr_lock LockBuffer( List );
//
//          // Now we can get the references in a safe manner
//          s32&        FirtEntry   = List[Index1];
//          s32&        SecondEntry = List[Index2];
//      }
//</CODE>
//<P><B> Example on how to support xptr_lock in your classes </B>
//<CODE>
//      class myclass
//      {
//          xptr<s32>   Buffer;
//        
//       public:    // You can make them public so other people can call them
//                  // if you dont want people to access them you can just make 
//                  // xptr_lock a friend of your class.
//      
//          void StartAccess( void )
//          {
//              Buffer.StartAccess();
//          }
//          void EndAccess( void )
//          {
//              Buffer.EndAccess();
//          }
//          
//          // Rest of the code
//      };
//</CODE>
// See Also:
//     xptr xarray xharray
//------------------------------------------------------------------------------
class xptr_lock 
{
public:

     template<class T>
     xptr_lock( T& X ) : m_pThis( &X ), m_pFunction( (functype)&T::EndAccess ) { X.StartAccess(); }
     ~xptr_lock( void )                                                        { (((loper*)m_pThis)->*m_pFunction)(); }

private:

    typedef xptr<s32> loper;
    typedef  void (loper::*functype)( void );

private:

    void*              m_pThis;
    functype           m_pFunction;
};
