//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
#ifdef TARGET_PC
    #include <intrin.h>
#elif defined(TARGET_IOS) || defined TARGET_OSX
    #include <libkern/OSAtomic.h>
#elif defined(TARGET_ANDROID)
    #include <sys/atomics.h>
    #include <pthread.h>
#endif

//==============================================================================
// CLASSES
//==============================================================================

X_MS_ALIGNMENT(16)
struct x_qt_ptr
{               
                x_qt_ptr        ( void );
    explicit    x_qt_ptr        ( const x_qt_ptr& Q ) { *this = Q; }
    explicit    x_qt_ptr        ( const x_qt_ptr& Q, void* pPtr ) { *this = Q; SetPtr( pPtr); }
                x_qt_ptr        ( void* pPtr );
    xbool       casPointTo      ( const x_qt_ptr& LocalPointer, const x_qt_ptr* const pNewPtr );
    xbool       casCopyPtr      ( const x_qt_ptr& LocalPointer, const x_qt_ptr& NextPointer );
    xbool       isValid         ( void ) const;
    xbool       IsNull          ( void ) const;
    x_qt_ptr&   operator =      ( const x_qt_ptr& Ptr );
    xbool       operator ==     ( const x_qt_ptr& Ptr ) const;
    xbool       operator !=     ( const x_qt_ptr& Ptr ) const;
    x_qt_ptr&   DereferencePtr  ( void ) const;
    void*       GetPtr          ( void ) const;
    void        SetPtr          ( void* pPtr );
    void        setNull         ( void );
    void        Zero            ( void );
    void        LinkListSetNext ( x_qt_ptr& Node );
    
protected:
#ifdef X_TARGET_64BIT
    
    enum flags:u64 
    {
        PTR_MASK = 0xffffffffffffUL
    };
    
    union compressed_ptr
    {
        u64                m_Data;         // bits of the qt pointer
        struct
        {
            u16            m_Pad[3];       // Reserve for the actual pointer
            u16            m_Tag;          // Guard/Tag
        };
    };
    
#else
    
    union compressed_ptr
    {
        u64                m_Data;         // bits of the qt pointer
        
        struct
        {
            x_qt_ptr*      m_pPtr;         // Actual 32 bits pointer
            u32            m_Tag;          // Guard/Tag
        };
    };
    
#endif
    
    volatile compressed_ptr  m_Pointer;
    
} X_SN_ALIGNMENT(16);

//-------------------------------------------------------------------------------

class x_qt_counter
{
public:
                x_qt_counter    ( void );
    void        Zero            ( void );
    s32         Inc             ( void );
    s32         Dec             ( void );
    s32         Add             ( s32 Count );
    s32         Sub             ( s32 Count );
    s32         Max             ( s32 Number );
    s32         Min             ( s32 Number );
    s32         get             ( void ) const;
    xbool       set             ( s32 localreality, s32 NewCounter );
    s32         setup           ( s32 Counter ) { return mCounter = Counter; }

protected:

    volatile s32         mCounter;
};

//-------------------------------------------------------------------------------

X_MS_ALIGNMENT(16)
class x_qt_flags
{
public:
                x_qt_flags          ( void )        { m_qtFlags = 0; }
    void        setup               ( u32 Flags );
    xbool       qtSetFlagsOn        ( u32 Flags, x_qt_flags& LocalReality );
    xbool       qtSetFlagsOff       ( u32 Flags, x_qt_flags& LocalReality );
    xbool       qtSetFlagsOn        ( u32 Flags );
    xbool       qtSetFlagsOff       ( u32 Flags );
    xbool       qtSetFlags          ( u32 Flags, x_qt_flags& LocalReality );
    void        qtForceSetFlagsOn   ( u32 Flags );
    void        qtForceSetFlagsOff  ( u32 Flags );
    xbool       areFlagOn           ( u32 Flags ) const;
    xbool       isFlagOn            ( u32 Flag ) const;
    u32         getFlags            ( void ) const;
    void        setFlags            ( u32 Flags );

protected:

    volatile u64     m_qtFlags;        
} X_SN_ALIGNMENT(16);

//------------------------------------------------------------------------------
// Authors:
//      Tomas Arce
//
// Description:
//      Just a filo also know as just a stack.
//      Lockless implementation and fixed sized 
//      T is assume to be derived from x_qt_ptr || have the same aligment as a derived object
//------------------------------------------------------------------------------
template< class T >
class x_qt_fixed_pool
{
public:

    virtual void        Init            ( s32 Count );
    virtual void        Kill            ( void );
    void                Clear           ( void );
    x_qt_ptr*           Alloc           ( void );
    void                Free            ( x_qt_ptr* pNode );
    s32                 getCount        ( void ) const { return m_Counter.get(); }

protected:

    x_qt_counter            m_Counter;
    x_qt_ptr                m_Head;
    xptr<T>                 m_Mem;
};

//------------------------------------------------------------------------------
// Authors:
//      Tomas Arce
//
// Description:
//      Just a file also know as just a stack.
//      Lockless implementation and fixed sized
//      T is assume to be derived from x_qt_ptr || have the same alignment as a derived object
//------------------------------------------------------------------------------
template< class T, s32 Count >
class x_qt_fixed_pool_static_mem
{
public:
    
                        x_qt_fixed_pool_static_mem( void ) { Clear(); }
    void                Clear           ( void );
    x_qt_ptr*           AllocByPtr      ( void );
    xhandle             AllocByHandle   ( void );
    void                FreeByPtr       ( x_qt_ptr* pNode );
    void                FreeByHandle    ( xhandle Handle );
    T&                  getEntry        ( xhandle Handle );
    s32                 getCount        ( void ) const { return m_Counter.get(); }
    xbool               isFull          ( void ) const { return m_Head.IsNull(); }
    
protected:
    
    x_qt_counter            m_Counter;
    x_qt_ptr                m_Head;
    xsafe_array<T,Count>    m_Mem;          // we always leave one empty node in there
};

//------------------------------------------------------------------------------
// Authors:
//      Dominique Fober, Yann Orlarey, St´ephane Letz
//      Tomas Arce, Randall Tunner
//
// Description:
//      Lockless Queue MPMC
//      Basically a 2005 Fober implementation.
//      Detail of the implementation (Optimized Lock-Free FIFO Queue continued) linked here:
//      http://www.grame.fr/ressources/publications/TR-050523.pdf
//-------------------------------------------------------------------------------
class x_qt_fober_queue
{
public:
                        x_qt_fober_queue        ( void );
    void                push                    ( x_qt_ptr* pNode );
    void                push                    ( x_qt_ptr* pNodeBegin, x_qt_ptr* pNodeEnd );
    x_qt_ptr*           pop                     ( void );
    xbool               unsafeIsEmpty           ( void ) const;
    void                clear                   ( void );

#ifdef X_DEBUG
public:
    x_qt_ptr*           getHead                 ( void )    { return &mHead; }
    x_qt_ptr*           getTail                 ( void )    { return &mTail; }
    x_qt_ptr*           getDummyNode            ( void )    { return &mDummyNode; }

    s32                 getCount                ( void );
#endif

protected:

#ifdef TARGET_ANDROID
    x_qt_counter       m_Lock;
#endif
    x_qt_ptr           mHead;
    x_qt_ptr           mTail;
    x_qt_ptr           mDummyNode;
};

//------------------------------------------------------------------------------
// Authors:
//      Tomas Arce
//
// Description:
// This is a queue that elements can not be pop out in quantum space
// But you can add nodes and traverse the list freely.
//------------------------------------------------------------------------------
class x_qt_popless_queue
{
public:

    x_qt_popless_queue( void ) = default;
    x_qt_popless_queue( const x_qt_popless_queue& PopLess ) : m_Head( PopLess.m_Head ), m_Tail( PopLess.m_Tail ) {}

    void                Push                    ( x_qt_ptr& Node );
    void                Clear                   ( void );

    x_qt_ptr           m_Head;
    x_qt_ptr           m_Tail;
};

//------------------------------------------------------------------------------
// Authors:
//      Tomas Arce
//
// Description:
// This hash table is meant to be use in a multicore enviroment. There are key events
// were the hash table must lock parts of it. However it does this in the least disruptive way possible.
// For example one reason to lock is when the user is mutable accessing an entry.
// we lock only that entry in the hash previting other simultaneuous access which would result in 
// unpredictable behaviors. In other words we must take that entry into a linear world because untill the mutation
// is complete is could be in a bad state.
// The only other reason why we lock is when an entry must be deleted. In that case we lock a hash table index entry.
// So that we can access that index table link list and fix up the pointers. All the other hash index table entries remain lockfree.
// Note that in most cases index table entries and entries may have a 1:1 relation ship which means there is not much locking at all.
//
// Everytime a user pops and entry it must remember to reinsert it ones is done using it.
// This will update any locks that it may have.
//
// TODO: Right now the Adding is Relax, which means that more of one similar key could be added because the addint is lockless
//       To garanteed that I can add a restrictive add and leave the relax adding as an option.
//------------------------------------------------------------------------------
template< class ENTRY, class KEY >
class x_qt_hash
{
public:

    using entry             = ENTRY;
    using key               = KEY;
    using function          = x_function<void (entry&)>;
    using const_function    = x_function<void (const entry&)>;

public:

    void                Initialize              ( s32 MaxEntries );
    key                 getKeyFromEntry         ( const entry& Entry ) const;
    xbool               isEntryInHash           ( key Key ) const;
    void                DeleteEntry             ( key Key );
    s32                 getCount                ( void ) const                              { return m_EntryMemPool.getCount(); }
    void                cpFindOrAddPopEntry     ( key Key, function Function );
    void                cpAddEntry              ( key Key, function Function );
    void                cpGetEntry              ( key Key, function Function );
    void                cpGetEntry              ( key Key, const_function Function ) const;
    xbool               cpGetOrFailEntry        ( key Key, function Function );
    xbool               cpGetOrFailEntry        ( key Key, const_function Function ) const;

    void                cpIterateGetEntry       ( function Function );
    void                cpIterateGetEntry       ( const_function Function ) const;

protected:

    struct hash_entry : public x_qt_ptr
    {
        xsemaphore_lock     m_Semaphore;
        key                 m_Key;
        entry               m_UserData;
    };

    struct hash_table_entry
    {
        xsemaphore_lock     m_Semaphore;
        x_qt_popless_queue  m_LinkList;
    };


protected:

    hash_entry&         getHashEntryFromEntry   ( entry& Entry );
    const hash_entry&   getHashEntryFromEntry   ( const entry& Entry ) const;
    s32                 KeyToIndex              ( key Key ) const;

    entry&              FindOrAddPopEntry       ( key Key );
    entry&              AddPopEntry             ( key Key );
    entry*              getPopEntry             ( key Key );
    const entry*        getPopEntry             ( key Key ) const;
    void                PushEntry               ( entry& Entry );
    void                PushEntry               ( const entry& Entry ) const;

    hash_entry&         FindOrAddHashEntry      ( hash_table_entry& TableEntry, key Key );
    hash_entry&         AddHashEntry            ( hash_table_entry& TableEntry, key Key );
    hash_entry*         FindEntry               ( const hash_table_entry& TableEntry, key Key ) const;

    xptr<hash_table_entry>          m_qtHashTable;
    x_qt_fixed_pool<hash_entry>     m_EntryMemPool;
};

//------------------------------------------------------------------------------
// Authors:
//      Tomas Arce
//
// Circular queue works as expected.
//------------------------------------------------------------------------------

template< s32 SIZE >
class x_qt_circular_queue
{
public:

    enum
    {
        ENTRY_COUNT = SIZE
    };

public:
    
    template< class J >
    J& Alloc( s32 Count = 1 );
    void Free( void* pVoidData );
    x_qt_circular_queue( void );

protected:

    xbyte* ByteAlloc( s32 Count, s32 Aligment );
    
#ifndef X_DEBUG

protected:

    x_qt_counter                                    m_Head;
    X_MS_ALIGNMENT(16) xsafe_array<xbyte, SIZE>     m_Ptr;

#else

protected:

    struct tail 
    {
        s32 m_LockCount     = 0;
        s32 m_iTail         = 0;
    };

    struct head
    {
        s32 m_Cycle         = 0;
        s32 m_iHead         = 0;
    };

protected:

    void UpdateTailLock( s32 Inc );
    s32  MoveTailForward( void );

protected:

    volatile tail                                   m_Tail;
    volatile head                                   m_Head;
    X_MS_ALIGNMENT(16) xsafe_array<xbyte, SIZE>     m_Ptr;
#endif
};

//-------------------------------------------------------------------------------
// Utility functions
//-------------------------------------------------------------------------------
inline xbool   x_cas32     ( volatile u32*  pPointer, u32 Local, u32 New );
inline xbool   x_cas64     ( volatile u64*  pPointer, u64 Local, u64 New );
inline xbool   x_cas64     ( volatile x_qt_ptr& Pointer, const x_qt_ptr& Local, const x_qt_ptr& New );

inline s32     x_casInc32  ( volatile s32* pPointer );
inline s32     x_casDec32  ( volatile s32* pPointer );


