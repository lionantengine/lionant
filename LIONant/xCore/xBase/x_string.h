//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The classes xfs and xvfs provide for "efficient temporary formatted 
//     strings".  That is, they are used to create formatted strings that will only
//     be needed in an isolated scope (like a single function).  The run-time 
//     overhead of xfs/xvfs is very low.  When possible, xfs/xvfs should be used 
//     rather than a large local fixed size character buffer (which wastes stack 
//     space). xstring is a good alternative to xfs if need be but unline xfs it doesn't
//     support in place string creation.
//
//<P>  Generally, xfs/xvfs should be instantiated anonymously.
//
//<P>  Here are example uses of xfs:
//<CODE>  
//      pFile = File.Open( xfs( "%s\%s", pPath, pFileName ), "rt" );
//      ASSERTS( pFile, xfs( "Failed to open file: %s", pFileName ) );
//</CODE>      
//<P>  In the first line, an anonymous instance of xfs is used to create a complete
//     file specification from a path and a file name.  A temporary string is 
//     created, used for the File.Open() function, and then destroyed.
//  
//<P>  In the second line, xfs is used in a similar manner to provide a meaningful
//     message for the the ASSERTS verification.
//  
//<P>  Class xvfs is very similar to xfs except that it uses x_va_args rather than
//     an ellipsis (...) to receive the variable argument list.
//
//<P>  The xfs/xvfs classes provide only a few public functions:
//         - GetLength to optain the length of the string
//         - A constructor which creates the string.
//         - A destructor.
//         - And a "const char*" cast operator.
//  
//<P>  When using xfs/xvfs, you should only directly deal with the constructor.  
//     The destructor and cast should be automatic.  In fact, you do not even need
//     to use an explicit cast when an xfs/xvfs is used in a variable argument list
//     expecting a "const char*".
//  
//<P>  All other functionality is denied or private.  In particular, the following
//     operations are NOT permitted:
//         - Default (or void) construction.
//         - Copy construction.
//         - Assignment.
//  
//------------------------------------------------------------------------------
class xfs
{
public:
                xfs                     ( const char* pFormatString, ... );
               ~xfs                     ( void );
                operator const char*    ( void ) const;
    s32         GetLength               ( void ) const;
                
protected:        
                xfs                     ( void );
                xfs                     ( const xfs& XFS );
    const xfs&  operator =              ( const xfs& XFS );
    char*       m_pString;
};

// <COMBINE xfs>
class xvfs
{
public:
                xvfs                    ( const char* pFormatString, xva_list Args );
               ~xvfs                    ( void );
                operator const char*    ( void ) const;
    s32         GetLength               ( void ) const;
                
protected:        
                xvfs                    ( void );
                xvfs                    ( const xvfs& xvfs );
    const xvfs& operator =              ( const xvfs& xvfs );
    char*       m_pString;
};

//==============================================================================
//  C++ STYLE STRING CLASS
//==============================================================================
//  
//  xstring( s32 Reserve )  - Construct an empty string, but reserve enough
//                            storage for a string 'Reserve' characters long.
//                            
//  operator const char*    - Cast to "const char*" function.  Usually called
//                            implicitly.  Allows xstrings to be passed to 
//                            functions which take "const char*" parameters.
//                            For example:
//                            
//                              xstring FileName = "Example.txt"; 
//                              pFile = x_fopen( FileName, "rt" );
//                            
//  Format          - Like x_sprintf  into the xstring.
//  FormatV         - Like x_vsprintf into the xstring.
//  AddFormat       - Like appending x_sprintf  onto the xstring.
//  AddFormatV      - Like appending x_vsprintf onto the xstring.
//  
//  IndexToRowCol   - Imagine the contents of the string in a text editor where 
//                    each '\n' in the string starts a new line.  This function,
//                    given an index into the string, reports the row and column
//                    numbers in the imaginary editor.  The very first character
//                    is always at 1,1.  Values of -1,-1 are returned when the
//                    Index exceeds the size of the string.
//  
//  Find            - Search for given character/string/xstring within the 
//                    xstring.  Return index if found, -1 otherwise.  An 
//                    optional starting offset is available.
//  
//  FindI           - Same thing as Find but case insensitive.
//  
//==============================================================================

#if defined(TARGET_IOS) || defined(TARGET_ANDROID) || defined(TARGET_OSX)
    #define X_STR(A)  ( const xstring_xchar*  )(&("$" A)[1])
    #define X_WSTR(A) ( const xwstring_xchar* )(&(u"$" A)[1])
#else
    #define X_STR(A)  ( const xstring_xchar*  )(&("$"##A)[1])
    #define X_WSTR(A) ( const xwstring_xchar* )(&(u"$"##A)[1])
#endif




//------------------------------------------------------------------------------

template< s32 Pow > class xstring_fixed_size;
class xstring_xchar;
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Why to use the xstring class? It provides a very safe way to manipulate strings
//     as well as memory efficient and friendly with other systems. If you don't want
//     to find yourself debugging strings as well as witting special string functions
//     this is the class for you. 
//  
//<P>  Most important thing to remember is that xstring is just a smart <B>POINTER</B>.
//     The good thing about the xstring is that it works dynamically as well.
//     Meaning that it can create its own buffer to store a string in there. But don't
//     worry the class is very careful about abussing the memory manager so stead of 
//     allocating individual strings it allocate pools of them except when very large
//     strings are created in those cases it does do an allocation as spected but this
//     never happens in a game.
//     
//<P>  The xstring class has a "cast to const char*" function which is usually 
//     called implicitly.  Furthermore, xstring has been designed to work in place
//     of standard C style NULL terminated strings as transparently as possible.
//     
//<P>  In fact, the xstring even works in cases where most other C++ style strings
//     fail.  Consider:
//<CODE [lang= "C++"]>
//      xstring     XString   = X_STR("ABC");       // Good old xstring!
//      string      CPPString = "ABC";              // C++ standard string.
//      s32         Integer   =  123;               // Just an integer.
//
//      x_printf( "(%s)(%d)", XString,   Integer ); // (ABC)(123)
//      x_printf( "(%s)(%d)", CPPString, Integer ); // Failure!
//</CODE>
//     The first x_printf will work!  The second x_printf will crash!
//     
//<P>  Note that an xstring also works on double NULL (or '\0\0') mode. So you may 
//     be able to find multiple strings within the class.
//  
//<P>  While the internal buffers of the xstring could have been done such the 
//     memory system could moved the memory. I choose not to for the sake of 
//     compatibility with things like x_printf/system strings and fixed strings.
//     
//     The class have some complexities that are hidden from the user. The user doesnt
//     need to be aware of them but I will write them here for advance users.
//     First the class deals with 3 general types of strings:
//
//<P><B>1) The system string.</B>
//<P>     String that are created with the macro X_STR().
//        The class is smart and will know not to overwrite those strings. If you have an existing xstring
//        with this type of string and another one is assign to the first the string actually is not duplicated as one would expect.
//        You still can do all the all non-writable operation on this type of strings without a problem.
//        But if you do a writable opearion such an append then the xstring will assert on you (as expected).
//
//<P><B>2) Fixed sized strings</B>
//<P>     These are created with the classes xstring_fix8, xstring_fix16, etc.
//        This string can be manipulated as long as the size never gets greated than the fixed string.
//        When assigning one xstring of this type to another xstring the new one will refere also to the fixed memory.
//        Only when using a Copy it will do a deep copy on the string into another memory location.
//
//<P><B>3) Dynamic allocated string.</B>
//<P>     While this type is dynamic there is not a malloc call per string.
//        Rather there is a pool of strings that is allocated, so memory in general is happy.
//        Here you can append as much data as you like without woring about buffer sizes. When you assign one xstring
//        of this type to another a reference count is created and the memory size gets lock. This is done
//        so that multiple xstring can refere to the same memory. This is stremeally usefull when returnning a xstring
//        from a function for example, or when you pass the xstring around such a file open where it keeps a duplicate.
//
//<P>  Finally as you will spect all this different types of strings have to play nice with each other.
//     Which they do. So the user has allot of things done for him, althought he may need to be aware of a few details.
//     
//<P>  The overhead of using the xstring memory wise is one byte per string. Except for fully
//     dynamic strings which are 2 bytes. So it is very efficient when it comes down to memory.
//     Be aware thought it doesn't store the size of the string so if you want to get the length it 
//     does it a full x_strlen (similar to the old fashion way).
//
// Todo:
//     * In the actual implementation of the xstring we may want to remove the xblock_array in favor of the 
//        small block allocator in the memory manager as the functionality/speed/fragmentation/etc are nearly identical.
//
//     * I need to think whether it is worth it to make an xstring read only if there is more than one reference to it.
//
// Example:
//<CODE [lang= "C++"]>
//  void Test( void )
//  {
//      xstring         String = X_STR("Hello");
//
//      // This is an example of a 32 characters fixed size buffer.
//      xstring_fix32   FixedString;
//
//      // Finding the legnth of a fixed string just need to use the "->" operator
//      s32             Size   = FixedString->GetLength();
//
//      // The copy is very simple and clear
//      FixedString->Copy( String );
//
//      // nothing complex about using xstring
//      Size    = Size - String.GetLength();
//
//      // Remember xstring is just a pointer so we can just point at the fixed data as well
//      String = FixedString;
//  }
//</CODE>
//------------------------------------------------------------------------------
class xstring
{
public:
                        xstring                 ( const xstring_xchar* pStr );
               explicit xstring                 ( s32 Reserve );
                        xstring                 ( void );
                       ~xstring                 ( void );
                        xstring                 ( const xstring& String);
    static xstring      BuildFromFormat         ( const char* pFormat, ... );

    void                Clear                   ( void );
    xbool               IsEmpty                 ( void ) const;

    s32                 Format                  ( const char* pFormat, ... );
    s32                 FormatV                 ( const char* pFormat, xva_list ArgList );
    s32                 AppendFormat            ( const char* pFormat, ... );
    s32                 AppendFormatV           ( const char* pFormat, xva_list ArgList );
          
    s32                 Find                    ( const char* pSubString, s32 StartIndex = 0 ) const;
    s32                 FindI                   ( const char* pSubString, s32 StartIndex = 0 ) const;
          
    void                IndexToRowCol           ( s32 Index, s32& Row, s32& Col ) const;
          
    void                MakeLower               ( void );
    void                MakeUpper               ( void );

    void                Copy                    ( const char* pString, s32 Count );
    s32                 Copy                    ( const char* pString );
    s32                 Copy                    ( const xstring& String );
    s32                 GetLength               ( void ) const;
    void                SearchAndReplace        ( const char* pFindString, const char* pReplaceString );
          
    xbool               IsDoubleNull            ( void ) const;
          
    void                SetModeDoubleNull       ( void );

    char&               operator []             ( s32 Index );
    const char&         operator []             ( s32 Index )  const;
    xstring*            operator ->             ( void )                { return this; }
                        operator const char*    ( void ) const          { return GetPointer(); }
    const xstring&      operator =              ( const xstring& Str );
    xbool               operator ==             ( const xstring& Str ) const;
    xbool               operator !=             ( const xstring& Str ) const {return !(*this == Str);}
    inline xbool        operator ==             ( const char* pStr ) const;
    xbool               operator !=             ( const char* pStr ) const   {return !(*this == pStr);}

    template< s32 Pow > 
    const xstring&      operator =              ( xstring_fixed_size<Pow>& Class ){ m_pString = (char*)Class; return *this; }


    friend  xbool       operator ==             ( const char* pStr, const xstring& Str );

protected:    

    // There are 3 types of strings that stand out:
    // 1) a system string which can be created like this: X_STR("Hello");
    // 2) A fixed size string which is done by using a class call xstring_fix..
    // 3) A dynamic string which are created and use like if it was allocated.
    enum
    {
        FLAGS_SUPER_LARGE       = 0*X_BIT(0),   // Means that it is been allocated due to the string been too large
        FLAGS_FIXED_SIZE_8      = 1*X_BIT(0),   // 8   characters fixed size strings
        FLAGS_FIXED_SIZE_16     = 2*X_BIT(0),   // 16  characters fixed size strings
        FLAGS_FIXED_SIZE_32     = 3*X_BIT(0),   // 32  characters fixed size strings
        FLAGS_FIXED_SIZE_64     = 4*X_BIT(0),   // 64  characters fixed size strings
        FLAGS_FIXED_SIZE_128    = 5*X_BIT(0),   // 128 characters fixed size strings
        FLAGS_FIXED_SIZE_256    = 6*X_BIT(0),   // 256 characters fixed size strings
        FLAGS_FIXED_SIZE_MASK   = X_BIN(111),   // This is all the bits that descrive the different sizes
        FLAGS_CAN_GROW          =   X_BIT(3),   // This string can be reallocated if needed so not a system or a fixed string
        FLAGS_UNUSED1           =   X_BIT(4),   // Nothing yet
        FLAGS_UNUSED2           =   X_BIT(5),   // ??????Still needs to be consider (Factor out?)
        FLAGS_DOUBLE_NULL       =   X_BIT(6),   // This means that the string finish only ones it finds a double NULL ('\0')
        FLAGS_NON_SYSTEM        =   X_BIT(7)    // Means that it can be written to it
    };

protected:    

    char*   GetPointer      ( void ) const;
    s32     GetWritableSize ( void ) const;
    u8      GetFlags        ( void ) const;
    void    SetEmptyString  ( void );
    xbool   WorkWithBuffer  ( void ) const;
    s32     GetRefCount     ( void ) const;

protected:    

    char*   m_pString;    

protected:

    template< s32 Pow > friend class xstring_fixed_size;
    friend char* xAllocString  ( s32 Length );
    friend void  xReleaseString( char* );

    template< class T > friend class xstring_global_pool;

public:
    static const char* s_pEmptyString;
};

//------------------------------------------------------------------------------

typedef xstring_fixed_size<3>      xstring_fix8;
typedef xstring_fixed_size<4>      xstring_fix16; 
typedef xstring_fixed_size<5>      xstring_fix32; 
typedef xstring_fixed_size<6>      xstring_fix64; 
typedef xstring_fixed_size<7>      xstring_fix128;
typedef xstring_fixed_size<8>      xstring_fix256;


class xstring_dictionary
{
public:
    
                    xstring_dictionary      ( void );
                   ~xstring_dictionary      ( void );
    
    void            Reset                   ( void );
    
    s32             Find                    ( const char* pString ) const;          // Find entry in dictionary, -1 = failed, otherwise index
    s32             Add                     ( const char* pString );                // Add entry to dictionary, -1 = failed, otherwise index
    
    const char*     getString               ( s32 iEntry ) const;                   // Get string given index
    s32             getOffset               ( s32 iEntry ) const;                   // Get offset into dictionary given index
    s32             getCount                ( void ) const;                         // Get count of entries in dictionary
    s32             getSaveSize             ( void ) const;                         // Get number of bytes to save dictionary
    
    s32             Save                    ( xfile& File ) const;                  // Save dictionary to file
    s32             Save                    ( const char* pFileName  ) const;       // Save dictionary to file
    
    xbool           Load                    ( xfile& File , s32 nBytes );           // Load dictionary from file
    xbool           Load                    ( const char* pFileName );              // Load dictionary from file
    
    void            Export                  ( xbitstream& BitStream );              // Save the dictionary to a bitstream
    void            Import                  ( xbitstream& BitStream );              // Load the dictionary from a bitstream
    void            Dump                    ( void );                               // Dump the dictionary contents
    
protected:
    
    struct entry
    {
        s32         Offset;                 // Offset of this entry into the buffer
        u32         Hash;                   // Hash key for this entry
    };
    
protected:
    
    void            ResizeHashTable ( s32 NewSize );
    void            HashString      ( const char* pString, u32& Hash, s32& Length ) const;
    s32             Find            ( const char* pString, u32& Hash, s32& Length ) const;

protected:
    
    xarray<entry>   m_Entries;              // Array of entries
    
    char*           m_pBuffer;              // Pointer to buffer for storing strings
    s32             m_BufferLength;         // Length of buffer
    s32             m_NextOffset;           // Next offset to write to in buffer
    
    s32*            m_pHashTable;           // Pointer to hash table
    s32             m_HashTableSize;        // Number of entries in hash table (each entry is an s32)
};

//==============================================================================
//==============================================================================
// string functions
//==============================================================================
//==============================================================================
// TODO: 
//       * I need to explain all the different options for formated strings
//       * inline things properly
//
//  Type Field Characters
//  Character       Output format
//  =========       --------------------------------------------------------
//     %%	        a percent sign
//     %c	        a character with the given number
//     %s	        a string
//     %d	        a signed integer, in decimal
//     %u	        an unsigned integer, in decimal
//     %o	        an unsigned integer, in octal
//     %x	        an unsigned integer, in hexadecimal (lower case)
//     %e	        a floating-point number, in scientific notation
//     %f	        a floating-point number, in fixed decimal notation
//     %g	        a floating-point number, in %e or %f notation
//     %X	        like %x, but using upper-case letters
//     %E	        like %e, but using an upper-case "E"
//     %G	        like %g, but with an upper-case "E" (if applicable)
//     %p	        a pointer (outputs the Perl value's address in hexadecimal)
//     %n	        special: *stores* the number of characters output so far into the next variable in the parameter list
//
//  Size flags
//  Character    Output
//  =========    ------------------------------------------------------------
//   h           interpret integer as s16 or u16
//   l           interpret integer as s32 or u32
//   q, L        interpret integer as s64 or u64
//   L           interpret floating point as higher precision adds a few extra floating point numbers
//
// Examples:
//
//      x_printf( "%Lf %Lf %Lf", Vec.X, Vec.Y, Vec.Z );  // Note that we use the higher precision floating point representation
//      x_printf( "%Ld" (s64)123 );                      // This is a way to print an s64 bit number
//      x_printf( "%d" 123 );                            // Nothing special here printing an 32bit integer
//      x_printf( "%f" 123.0 );                          // Nothing special here printing a floating point number
//      x_printf( "%+010.4f", 123.456 );                 // The printf works like you will expect
//
//==============================================================================

char*   x_strcpy    ( char* pDest, s32 MaxChars, const char* pSrc );
char*   x_strncpy   ( char* pDest, const char* pSrc, s32 Count, s32 DestSize );
s32     x_strlen    ( const char* pStr );
s32     x_strcmp    ( const char* pStr1, const char* pStr2 );
s32     x_stricmp   ( const char* pStr1, const char* pStr2 );
s32     x_strncmp   ( const char* pStr1, const char* pStr2, s32 Count );
char*   x_stristr   ( const char* pStr1, const char* pStr2 );
char*   x_strstr    ( const char* pStr1, const char* pStr2 );
void*   x_memchr    ( void* pBuf, s32 C, s32 Count );
char*   x_strcat    ( char* pFront, s32 MaxChars, const char* pBack );
u32     x_strHash   ( const char* pStr, u32 Range = 0xffffffff, u32 hVal = 0x811c9dc5 );
u32     x_strIHash  ( const char* pStr, u32 Range = 0xffffffff, u32 hVal = 0x811c9dc5 );
u64     x_strIHash64( const char* pStr, u64 hVal = ((u64)0xcbf29ce484222325ULL) );
u32     x_strCRC    ( const char* pStr, u32 crcSum = 0xffffffff );

s32     x_sscanf    ( const char* pInStr, const char* pFormatStr, ... );
s32     x_vsscanf   ( const char* pInStr, char const* pFormatStr, xva_list Args );
s32     x_sprintf   ( char* pBuffer, s32 MaxChars, const char* pFormatStr, ... );
s32     x_vsprintf  ( char* pBuffer, s32 MaxChars, const char* pFormatStr, xva_list Args );
s32     x_atoi32    ( const char* pStr );
s64     x_atoi64    ( const char* pStr );
f32     x_atof32    ( const char* pStr );
f64     x_atof64    ( const char* pStr );
s32     x_dtoa      ( s32 Val, char* pStr, int SizeOfString, int Base );
s32     x_dtoa      ( u32 Val, char* pStr, int SizeOfString, int Base );
s32     x_dtoa      ( s64 Val, char* pStr, int SizeOfString, int Base );
s32     x_dtoa      ( u64 Val, char* pStr, int SizeOfString, int Base );
s32     x_atod32    ( const char* pStr, s32 Base );
s64     x_atod64    ( const char* pStr, s32 Base );

inline s32   x_toupper( s32 C ) { return( (C >= 'a') && (C <= 'z') )? C + ('A' - 'a') : C; }
inline s32   x_tolower( s32 C ) { return( (C >= 'A') && (C <= 'Z') )? C + ('a' - 'A') : C; }
inline xbool x_isspace( s32 C ) { return( (C == 0x09) || (C == 0x0A) || (C == 0x0D) || (C == ' ') ); }
inline xbool x_isdigit( s32 C ) { return( (C >=  '0') && (C <= '9') ); }
inline xbool x_isalpha( s32 C ) { return( ((C >= 'A') && (C <= 'Z')) || ((C >= 'a') && (C <= 'z')) ); }
inline xbool x_isupper( s32 C ) { return( (C >=  'A') && (C <= 'Z') ); }
inline xbool x_islower( s32 C ) { return( (C >=  'a') && (C <= 'z') ); }
inline xbool x_ishex  ( s32 C ) { return( ((C >= 'A') && (C <= 'F')) || ((C >= 'a') && (C <= 'f')) || ((C >=  '0') && (C <= '9')) ); }

xbool   x_isstrhex      ( const char* pStr );
xbool   x_isstrint      ( const char* pStr );
xbool   x_isstrfloat    ( const char* pStr );
xbool   x_isstrguid     ( const char* pStr );

void x_splitpath    ( const char* pPath, char* pDrive,
                     char* pDir,
                     char* pFName,
                     char* pExt );

void x_makepath     ( char* pPath, const char* pDrive,
                     const char* pDir,
                     const char* pFName,
                     const char* pExt );


//==============================================================================
// INLINE
//==============================================================================
#include "Implementation/x_string_inline.h"
