//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

class xfile_device_i;
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      The xfile class is design to be a direct replacement to the fopen. The class
//      supports most features of the fopen standard plus a few more. The Open function
//      has been change significantly in order to accommodate the new options. The access
//      modes are similar BUT not identical to the fopen. Some of the functionality like
//      the append has drastically change from the standard. Here are the access modes.
//
//<TABLE>
//     Access Mode         Description
//     =================  ----------------------------------------------------------------------------------------
//         "r"            for read only, the file must exits. Usefull when accessing DVDs or other read only media
//         "r+"           for reading and writing the file must exits
//         "w" or "w+"    for writing + reading to a file. The file will be created. Note here that the + is automatic. 
//         "a" or "a+"    this is actually different to the standard io. This is just "r+" with a SeekEnd(0) in both cases.
//  
//         "@"            for asynchronous access.
//         "c"            for writing compress files. Reading is detected automatically.
//  
//         "b"            This is always the default so you don't need to put it really. I added this just in case 
//                         people like to put it, but it is not necessary at all.
//         "t"            To write/read text files. If you don't add this assumes you are doing binary files.             
//                         This command basically writes an additional character '\\r' whenever it finds a '\\n' and       
//                         when reading it removes it when it finds '\\r\\n' 
//</TABLE>
//
//      To illustrade different possible combinations and their meaning we put toggetter a table 
//      to show a few examples.
//<TABLE>
//     Examples of Modes  Description
//     =================  ----------------------------------------------------------------------------------------
//          "r"           Good old fasion read a file. Note that you never need to do "rc" as the file system detects that automatically.
//          "wc"          Means that we want to write a compress file       
//          "wc@"         Means to create a compress file and we are going to access it asynchronous
//          "r+@"         Means that we are going to read and write to an already exiting file asynchronously.
//</TABLE>
//
//     Other key change added was the ability to have more type of devices beyond the standard set.
//     Some of the devices such the net device can be use in congointion with other devices.
//     The default device is ofcourse the hard-disk in the PC.
//<TABLE>
//     Known devices      Description
//     =================  ----------------------------------------------------------------------------------------
//          ram:          To use ram as a file device
//          dvd:          To use the dvd system of the console
//          net:          To access across the network
//          temp:         To the temporary folder/drive for the machine
//          memcard:      To access memory card file system
//          localhd:      To access local hard-drives such the ones found in the XBOX
//          buffer:       User provided buffer data
//      c: d: e: ...etc   PC devices
//</TABLE>
//
//     Example of open strings:
//<CODE>
//     Open( "net:\\\\c:\\test.txt", "r" );         // Reads a file across the network
//     Open( "ram:\\\\name doesnt matter", "w" );   // Creates a ram file which you can read/write
//     Open( "c:\\dumpfile.bin", "wc" );            // Creates a compress file in you c: drive
//     Open( "UseDefaultPath.txt", "r@" );          // No device specify so it reads the file from the default path
//</CODE>
//
//      Also note the read/write functions as they are devided into two categories. There are the
//      WriteRaw/ReadRaw which work identically to fwrite/fread and then there is the Write/Read
//      functions which are templates. The key difference is that the raw versions will not endian 
//      swap anything vs the other set will.
//
// TODO:
//      * Revise and test the swap endian
//  
//------------------------------------------------------------------------------
#ifdef MAX_PATH
#undef MAX_PATH
#endif

class xfile
{
public:

    enum general
    {
        XEOF       = -1
    };

    enum sync_state:s32
    { 
        SYNC_UNKNOWN_ERR    = -2,
        SYNC_EOF            = XEOF,
        SYNC_INCOMPLETE     = 0,
        SYNC_COMPLETED      = 1
    };
    
    enum lengths:s32
    {
        MAX_DRIVE = 32,         // How long a drive name can be
        MAX_DIR   = 256,        // How long a path or directory
        MAX_FNAME = 256,        // File name
        MAX_EXT   = 256,        // Extension
        MAX_PATH  = MAX_DRIVE + MAX_DIR + MAX_FNAME + MAX_EXT
    };

public:
                            xfile           ( void );
                           ~xfile           ( void );
    xbool                   Open            ( const xstring& FileName, const char* pMode );             
    xbool                   Open            ( const char*   pFileName, const char* pMode ); 
    void                    Close           ( void );                                     
    xbool                   ReadRaw         (       void* pBuffer, s32 Size, s32 Count );  
    xbool                   WriteRaw        ( const void* pBuffer, s32 Size, s32 Count );
    void                    ToFile          ( xfile& File );
    void                    ToMemory        ( void* pData, s32 BufferSize );
    xfile::sync_state       Synchronize     ( xbool bBlock );
    void                    AsyncAbort      ( void );
    s32                     Printf          ( const char* pFormatStr, ... );
    void                    ForceFlush      ( xbool bOnOff ); 
    void                    Flush           ( void );                                     
    void                    SeekOrigin      ( s32 Offset );             
    void                    SeekEnd         ( s32 Offset );             
    void                    SeekCurrent     ( s32 Offset );             
    s32                     Tell            ( void );
    xbool                   IsEOF           ( void );
    s32                     GetC            ( void );
    void                    PutC            ( s32 C, s32 Count=1, xbool bUpdatePos = TRUE ); 
    void                    AlignPutC       ( s32 C, s32 Count=0, s32 Aligment=4, xbool bUpdatePos = TRUE ); 
    s32                     GetFileLength   ( void );

    static xbool            GetFileTime     ( const char* pFileName, u64& FileTime );

    xbool                   Write           ( const xstring& Val );
    xbool                   Read            ( xstring& Val );

    template<class T> xbool Write           ( const T& Val );
    template<class T> xbool Write           ( const T* const pVal, s32 Count );
    template<class T> xbool Read            ( T& Val );
    template<class T> xbool Read            ( T* pVal, s32 Count );

    xbool                   isBinaryMode    ( void ) const;
    xbool                   isReadMode      ( void ) const;
    xbool                   isWriteMode     ( void ) const;

protected:

    void                    Clear           ( void );
    void                    SwapEndian      ( xbool bOnOff ); 

protected:

    void*                   m_pFile;
    xfile_device_i*         m_pDevice;
    u32                     m_Flags;
    xstring                 m_FileName;

    friend xbool x_SetCompressDevice( xfile& File, xbool bCompressAndText );
};

//==============================================================================
// GENERIC IO FUNCTIONS
//==============================================================================
// These are intended for tools and such and they deal with native file systems.
//==============================================================================

namespace x_io
{
    struct file_info
    {
        char  m_Path[xfile::MAX_PATH];           // Full path of the file
        char  m_FileName[xfile::MAX_FNAME];      // Incudes extension
        char* m_pExtension;                      // Extension offset inside the filename
        xbool m_bDir:1,                          // is this entry a directory?
              m_bRegular:1;                      // is this a regular file?
    };
    
    struct file_details
    {
        xbool   m_bDir;                         // is this a directory?
        u64     m_FileSize;                     // if it is a file how large is it?
        double  m_MSTimeCreated;                // The time in milliseconds of creation
        double  m_MSTimeModified;               // The time in millisecond of the last modification
    };
    
    xhandle     DirOpenPath     ( const char* pPath );
    xbool       DirGetNext      ( file_info& Info, xhandle Handle );
    void        DirClose        ( xhandle Handle );
    
    xbool       FileDetails     ( const char* pPath, file_details& FileDetails );
    xbool       PathExists      ( const char* pPath );
    xbool       FileExists      ( const char* pFile );
    xbool       MakeDir         ( const char* pPath, s32 LevelsUp = 0 );
};

//==============================================================================
// FORMATTED STRING FUNCTIONS
//==============================================================================
//
//  x_printf    
//
//      Formatted print to "standard text output".  This is straight forward for
//      text mode programs and is handled by the xBase.  Graphical programs
//      must register a function to handle this operation.
//
//  x_printfxy
//
//      Like x_printf with the addition that the text will be printed at the
//      given character cell coordinates.  (0,0) represents the upper left most 
//      character available.  The xBase will provide versions of this function 
//      for each text mode target supported.  Graphical programs must register 
//      a function to handle this operation.
//      
//==============================================================================

//@@
s32     x_printf    (               const char* pFormatStr, ... );
s32     x_printfxy  ( s32 X, s32 Y, const char* pFormatStr, ... );

//=========================================================================
// This types are using the system components to overwrite the behavior of printf/printfxy
//=========================================================================

typedef void  xprint_fn     ( const char* pString );
typedef void  xprint_xy_fn  ( s32 X, s32 Y, const char* pString );

void x_SetFunctionPrintF  ( xprint_fn Printf );
void x_SetFunctionPrintFXY( xprint_xy_fn Printfxy );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is the most low level class for the file system. This class deals
//     with the most low level platform specific API to do its job. For most part
//     users will never deal with this class directly. It is intended to be used
//     by more expert users and low level people. This class defines a device to be 
//     access by the xfile class.
// See Also:
//     xfile
//------------------------------------------------------------------------------
class xfile_device_i
{
public:

    enum seek_mode
    {
        SKM_ORIGIN,
        SKM_CURENT,
        SKM_END
    };

    enum access_types
    {
        ACC_CREATE      = X_BIT(0),     // If not create file then we are accessing an existing file
        ACC_READ        = X_BIT(2),     // Has read permissions or not
        ACC_WRITE       = X_BIT(3),     // Has write permissions or not
        ACC_ASYNC       = X_BIT(4),     // Async enable?
        ACC_COMPRESS    = X_BIT(5),     // Do compress files (been compress)
        ACC_TEXT        = X_BIT(6),     // This is a text file. Note that this is handle at the top layer.
        ACC_FORCE_FLUSH = X_BIT(7),     // Forces to flush constantly (good for debugging). Note that this is handle at the top layer.
        ACC_SWAP_ENDIAN = X_BIT(8),     // Swaps the data endian. Note this is handle in the top layer right now.
        ACC_LAST,
        ACC_MASK        = ((ACC_LAST-1)<<1)-1
    };

public:
                                xfile_device_i  ( const char* pName ) : m_pName(pName) {}
    virtual void*               Open            ( const char* pFileName, u32 Flags )=0;
    virtual void                Close           ( void* pFile )=0;
    virtual xbool               Read            ( void* pFile, void* pBuffer, s32 Count )=0;
    virtual void                Write           ( void* pFile, const void* pBuffer, s32 Count )=0;
    virtual void                Seek            ( void* pFile, seek_mode Mode, s32 Pos )=0;
    virtual s32                 Tell            ( void* pFile )=0;
    virtual void                Flush           ( void* pFile )=0;
    virtual s32                 Length          ( void* pFile )=0;
    virtual xbool               IsEOF           ( void* pFile )=0;
    virtual xfile::sync_state   Synchronize     ( void* pFile, xbool bBlock )=0;
    virtual void                AsyncAbort      ( void* pFile )=0;
    virtual void                Init            ( const void* ){}
    virtual void                Kill            ( void ) {}

public:

    xfile_device_i*     m_pNext;
    const char*         m_pName;
};

//==============================================================================

void            x_FileSystemAddNetHost  ( const xnet_address& Address );
void            x_FileSystemAddDevice   ( xfile_device_i&   Device );
xfile_device_i* x_FileSystemFindDevice  ( const char*       pDeviceName );
void			x_FileSystemCacheFile	( const char*		pFileName );

