//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
// WARNING:
// One issue with how new constructs is that if there is another type with the same
// name lets say inside a cpp file. Then there is a chance that it may not call
// the constructor this seems to be a bug in the ms compiler.
//==============================================================================
enum xmem_flags
{
    XMEM_FLAGS_DEFAULT      =           0, 
    XMEM_FLAG_OKAY_TO_FAIL  =    X_BIT(0),          // If this flag is not set it will assert in case of out of memory failure
    XMEM_FLAG_STATIC        =    X_BIT(1),          // Tells the memory manager that this allocation is going to stay for a while

    XMEM_FLAG_ALIGN_MASK    =    X_BIN(111100),     // *** Choose one alignment for the allocation. 
    XMEM_FLAG_ALIGN_4B      =  0*X_BIT(2),          // Align to 4 bytes this is the Default
    XMEM_FLAG_ALIGN_8B      =  1*X_BIT(2),          // Align to 8 bytes
    XMEM_FLAG_ALIGN_16B     =  2*X_BIT(2),          // Align to 16 bytes
    XMEM_FLAG_ALIGN_32B     =  3*X_BIT(2),          // Align to 32 bytes
    XMEM_FLAG_ALIGN_64B     =  4*X_BIT(2),          // Align to 64 bytes
    XMEM_FLAG_ALIGN_128B    =  5*X_BIT(2),          // Align to 128 bytes
    XMEM_FLAG_ALIGN_256B    =  6*X_BIT(2),          // Align to 256 bytes
    XMEM_FLAG_ALIGN_512B    =  7*X_BIT(2),          // Align to 512 bytes
    XMEM_FLAG_ALIGN_1K      =  8*X_BIT(2),          // Align to 1K
    XMEM_FLAG_ALIGN_2K      =  9*X_BIT(2),          // Align to 2K
    XMEM_FLAG_ALIGN_4K      = 10*X_BIT(2),          // Align to 4K
    XMEM_FLAG_ALIGN_8K      = 11*X_BIT(2),          // Align to 8K
    XMEM_FLAG_ALIGN_16K     = 12*X_BIT(2),          // Align to 16K
    XMEM_FLAG_ALIGN_32K     = 13*X_BIT(2),          // Align to 32K
    XMEM_FLAG_ALIGN_64K     = 14*X_BIT(2),          // Align to 64K

    XMEM_FLAG_NO_VIRTUAL    =    X_BIT(6),          // Don't use virtual memory in this block
    XMEM_FLAG_MOVABLE       =    X_BIT(7),          // Tells the system that it can move memory around if he wants 
                                                    // (The user get a handle after to the actual block)
    XMEM_FLAG_MIX_THREADS   =    X_BIT(8),          // Different threads will access the block of memory.
    XMEM_FLAG_NO_HEADER     =    X_BIT(9),          // Will try to allocate the block without adding the over head of a header
    XMEM_FLAG_UNDEF_TYPE    =    X_BIT(10),         // The type is undefined so skip all checks respect to that.
    XMEM_FLAG_UNUSED3       =    X_BIT(11),         // currently unused

    // Private flags
    XMEM_FLAG_BEEN_NEW      =    X_BIT(12),         // Using new vs malloc such the free method needs to match
    XMEM_FLAG_BEEN_REALLOC  =    X_BIT(13),         // This block has been reallocated/moved at some point
    XMEM_FLAG_READ_ONLY     =    X_BIT(14),         // The block is read only
    XMEM_FLAG_FREE          =    X_BIT(15),         // The block is currently free
    XMEM_FLAG_PAD           =    0xffffffff
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     returns the aligment of the type
// Arguments:
//     the type
// Description:
//     returns the aligment of the type
// See Also:
//     x_Align x_AlignLower xalingof
//------------------------------------------------------------------------------
template<class T> inline 
xmem_flags _x_mem_aligment( void )
{
    switch ( xalingof(T) )
    {
    case 1:  case 2:  case 4:  return XMEM_FLAG_ALIGN_4B;
    case 8:  return XMEM_FLAG_ALIGN_8B;
    case 16: return XMEM_FLAG_ALIGN_16B;
    case 32: return XMEM_FLAG_ALIGN_32B;
    case 64: return XMEM_FLAG_ALIGN_64B;
    case 128: return XMEM_FLAG_ALIGN_128B;
    case 256: return XMEM_FLAG_ALIGN_256B;
    case 512: return XMEM_FLAG_ALIGN_512B;
    }

    return XMEM_FLAGS_DEFAULT;
}
#define xmem_aligment(a) _x_mem_aligment<a>()

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     returns the aligment of the type
// Arguments:
//     the type
// Description:
//     returns the aligment of the type
// See Also:
//     x_Align x_AlignLower xalingof
//------------------------------------------------------------------------------
inline
u32 xmem_UpdateAligtmentForA( u32 A, u32 B )
{
    const u32 B_A = (B&XMEM_FLAG_ALIGN_MASK);
    // Choose highest aligment
    if( (A&XMEM_FLAG_ALIGN_MASK) >= B_A ) return A;
    return (A & ~XMEM_FLAG_ALIGN_MASK) | B_A;
}

//==============================================================================
//  FUNCTIONS
//==============================================================================

void*       x_malloc                ( s32 ElementSize, s32 ElementCount, xmem_flags Flags );
void        x_free                  ( void* pPtr );
void*       x_realloc               ( void* pPtr, s32 ElementSize, s32 NewElementCount );
void        x_MemSanityCheck        ( void );
void        x_SetMemoryReadOnly     ( void* pPtr, xbool bReadOnly = TRUE );
s32         x_GetElementCount       ( void* pPtr );
void        x_SetDebugingMode       ( xmem_debug_mode Mode );
xmem_flags  x_MemAligmentToEnum     ( s32 Aligment );
void        x_MemDump               ( const char *pFileName = "MemoryDump.txt" );

//==============================================================================

s32         x_GetTotalMemory        ( void );  
s32         x_GetTotalMemoryFree    ( void );  
s32         x_GetTotalMemUsed       ( void );
s32         x_GetLargestBlockFree   ( void );
u32         x_GetMemorySequence     ( void );
void        x_DonateBlock           ( void* pBlockAddr, s32 nBytes );
void        x_GetMemHashStats       ( s32* pData, s32 Count );
s32         x_GetWastedMemory       ( void );
s32         x_GetNumberOfAllocations( void );

