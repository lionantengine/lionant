//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
#ifdef TARGET_3DS
#include "nn/assert.h"
#endif

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Debug level works as a run time tunable checking level. It is not compile 
//     dependent so there is an actual variable that contains the level. This is
//     convenient because when debugging you can turn on higher levels and lower levels
//     at any time. 
//
//<P>  There are asserts that work directly with the level these are:
//<P>     ASSERTSL(level,expr,str)
//<P>     ASSERTL(level,expr)     
//<P>  These asserts go away just like you will expect in release mode.
//  
//<P>  A user can have its on debug levels. This debug levels should fall into the 
//     ranges that given by xdebug_level. Here is an example of it:
//<CODE>
//      enum my_debug_levels
//      {
//          MYDEBUG_FAST        = XDEBUG_LEVEL_FAST,
//          MYDEBUG_SOMECHECKS,
//          MYDEBUG_AFEWCHECKS,
//          MYDEBUG_BASIC       = XDEBUG_LEVEL_BASIC,
//          MYDEBUG_MOSTCHECKS, 
//          MYDEBUG_FULL        = XDEBUG_LEVEL_FORCE_FULL, 
//          MYDEBUG_INSANE
//      };
//</CODE>
//<P>  Global levels always win over local levels. Winning is define on how much 
//     debugging is turn on. The more debugging the more power. This allows someone like a 
//     programmer lead to set debugging to max and make sure everything holds up good.
//
//<P>  When in crises mode there is a way to overwrite the global level. By you 
//     own levels you can do that by using the FORCE enums. This means you can 
//     focus on debugging a piece of code independently of the global levels. But
//     unlike the normal enums this should never be using in an ASSERTSL, ASSERTL or
//     in an if stamen such if( x_ChooseDebugLevel( level ) > MYDEBUG_BASIC ) ...
//
//<P>  The are 3 functions for this system although you will most likely only use one.
//     "x_ChooseDebugLevel". The other 2 are more for completeness than anything else.
//
//------------------------------------------------------------------------------
enum xdebug_level
{
    XDEBUG_LEVEL_FAST        =    0,
    XDEBUG_LEVEL_BASIC       =  100,
    XDEBUG_LEVEL_FULL        =  200,

    XDEBUG_LEVEL_FORCE_FAST  = 1000,
    XDEBUG_LEVEL_FORCE_BASIC = 1100,
    XDEBUG_LEVEL_FORCE_FULL  = 1200
};

enum xmem_debug_mode
{
    XMEM_DEBUG_FAST         = XDEBUG_LEVEL_FAST,
    XMEM_DEBUG_BASIC        = XDEBUG_LEVEL_BASIC,
    XMEM_DEBUG_CHECKALL     = XDEBUG_LEVEL_FULL
};

xdebug_level x_GetDebugLevel    ( void );
void         x_SetDebugLevel    ( xdebug_level Level );
xdebug_level x_ChooseDebugLevel ( s32 LocalLevel );

//==============================================================================
//  LOG
//==============================================================================
//
//==============================================================================
#ifdef X_DEBUG
    void    x_LogMessage   ( const char* pChannel, const char* pFormatStr, ... );
    void    x_LogWarning   ( const char* pChannel, const char* pFormatStr, ... );
    void    x_LogError     ( const char* pChannel, const char* pFormatStr, ... );
    void    x_LogFlush     ( void );
#else
    #define x_LogMessage    x_DebugNull
    #define x_LogWarning    x_DebugNull
    #define x_LogError      x_DebugNull
    #define x_LogFlush      x_DebugNull
#endif

//==============================================================================
// MISCELANEOUS FUNCTIONS
//==============================================================================
// This are functions that dont fall into any particular section.
// So they are left here.
//
// The function x_DebugMsg works like x_printf except that (a) the output goes
// to the debugger if possible, and (b) it only works when X_DEBUG is defined.
//==============================================================================

char* x_DebugGetCallStack   ( void );
void  x_DebugMsg            ( const char* pFormatStr, ... );

//==============================================================================
// BREAK & NOP
//==============================================================================
// This two macros are use mainly for debugging. The nop is used more to
// separate an block of code from another. Very useful when looking at the 
// assembly code.
//==============================================================================
#ifdef X_BREAK
#undef X_BREAK
#endif

#ifdef TARGET_PC
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Stop the program with a break point.
// Description:
//     The macro BREAK will cause a debugger breakpoint if possible on any given 
//     platform.  If a breakpoint cannot be caused, then a divide by zero will be 
//     forced.  Note that the BREAK macro is highly platform specific.  The 
//     implementation of BREAK on some platforms prevents it from being used 
//     syntactically as an expression.  It can only be used as a statement.
// See Also:
//     NOP ASSERT
//------------------------------------------------------------------------------
#define X_BREAK      { __debugbreak(); }
#endif

#ifdef TARGET_360
#define X_BREAK      { __asm int 3 }
#endif

#ifdef TARGET_PS3
#define X_BREAK       asm(".long 0");
#endif

#ifdef TARGET_OSX
#define X_BREAK abort();//__asm__("int $3")
#endif

#ifdef TARGET_IOS
#define X_BREAK { __builtin_trap(); }
#endif
// http://iphone.m20.nl/wp/2010/10/xcode-iphone-debugger-halt-assertions/

#ifdef TARGET_3DS
#define X_BREAK { nndbgBreak(NN_DBG_BREAK_REASON_ASSERT); }
#endif

// Generic BREAK to be used if no proper version can be created.
//DOM-IGNORE-BEGIN
#ifndef X_BREAK
extern volatile s32 g_xDDBZ;   // Debug Divide By Zero
#define X_BREAK      {g_xDDBZ=0;g_xDDBZ=1/g_xDDBZ;}
#endif
//DOM-IGNORE-END

#define PRINT_SIZEOF(A) char(print_sizeof_as_warning<sizeof( A )>( )( ));

//------------------------------------------------------------------------------

#ifdef X_NOP
#undef X_NOP
#endif

#ifdef TARGET_PC
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Sets a NOP operation in the code assembly.
// Description:
//     Sets a NOP operation in the code assembly. This is commonly used for 
//     debugging. By adding nops allows to see the assembly clearly in code.
// See Also:
//     BREAK
//------------------------------------------------------------------------------
    #ifdef X_TARGET_64BIT
        #define X_NOP        __nop() 
    #else
        #define X_NOP        { __asm nop   } //{ __emit(0x00000000); }
    #endif
#endif

#ifdef TARGET_360
    #define X_NOP        { __asm nop   }
#endif

//==============================================================================
// SCOPE
//==============================================================================

#ifdef X_DEBUG
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Assert handler function type declaration.
// Arguments:
//     Flags                - Reference to a local copy of flags for the assert.
//     pFileName            - File name in which the assert happen
//     LineNumber           - Line number where the assert happen
//     pExprString          - Expression of the assert
//     pMessageString       - Additional string containing information about the 
//                            assert.
// Returns:
//     xbool - TRUE when the program should be halted FALSE other wise
// Description:
//     If the user wants to overwrite the default assert handler his function must
//     be of this type. 
// See Also:
//     ASSERTS VERIFY x_SetAssertHandler x_GetAssertHandler
//------------------------------------------------------------------------------
    #define X_DEBUG_SCOPE(A) static const xscope_node Node_##A( __FILE__, #A, __LINE__); xdebug_scope Scope_##A( Node_##A );
#else
    #define X_DEBUG_SCOPE(A)
#endif


//==============================================================================
// ASSERT/VERIFY
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Assert handler function type declaration.
// Arguments:
//     Flags                - Reference to a local copy of flags for the assert.
//     pFileName            - File name in which the assert happen
//     LineNumber           - Line number where the assert happen
//     pExprString          - Expression of the assert
//     pMessageString       - Additional string containing information about the 
//                            assert.
// Returns:
//     xbool - TRUE when the program should be halted FALSE other wise
// Description:
//     If the user wants to overwrite the default assert handler his function must
//     be of this type. 
// See Also:
//     ASSERTS VERIFY x_SetAssertHandler x_GetAssertHandler
//------------------------------------------------------------------------------
typedef xbool xassert_fn( u32&           Flags,
                          const char*    pFileName,
                          s32            LineNumber,
                          const char*    pExprString,
                          const char*    pMessageString );

//==============================================================================
//  Runtime validation support.  
//==============================================================================
//
//  Most of the run-time validations are one form or another of an ASSERT.  So,
//  for lack of a better name, the presence of the compile time macro X_ASSERT 
//  activates the optional run-time validations.  (And, of course, the absence 
//  of X_ASSERT deactivates them.)
//
//  The following macros and functions are all designed to perform validation of
//  expected conditions at run-time.  Each takes an expression as the first 
//  parameter.  The expression (expr) is expected to be true whenever evaluated.
//  
//      ASSERT  ( expr )
//      ASSERTS ( expr, message )
//
//      VERIFY  ( expr )
//      VERIFYS ( expr, message )
//  
//  The macros ASSERT and ASSERTS completely evaporate in when X_ASSERT is not
//  defined.  Macros VERIFY and VERIFYS, lacking X_ASSERT, still evaluate the 
//  expression, but do not validate the result.  Consider:
//  
//      ASSERT( CriticalInitialization() );    // EVIL without X_ASSERT!
//      VERIFY( CriticalInitialization() );    // Safe without X_ASSERT.
//  
//  The ASSERTS and VERIFYS macros accept a message string to assist problem
//  diagnosis.  Upon a run-time failure, the message is displayed.  For example:
//  
//      ASSERTS( Radius >= 0.0f, "Radius must be non-negative." );
//  
//  To place formatted strings within ASSERTS and VERIFYS, use the xfs class 
//  from x_string.hpp.  For example:
//  
//      pFile = x_fopen( pFileName, "rt" );
//      ASSERTS( pFile, xfs( "Failed to open file '%s'.", pFileName ) );
//
//  Available options:
//
//    - As previously mentioned, the macro X_ASSERT enables the validation 
//      macros.  X_ASSERT should be always be present in debug configurations.
//
//==============================================================================
#ifdef ASSERT
#undef ASSERT
#endif

#ifdef ASSERTS
#undef ASSERTS
#endif

#ifdef VERIFY
#undef VERIFY
#endif

#ifdef VERIFYS
#undef VERIFYS
#endif

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is the assert handler.
//------------------------------------------------------------------------------

#if defined TARGET_OSX || defined TARGET_IOS
    #define _XTEST(A) (__builtin_expect( !(A), 0 ) )
    xbool x_AssertHandler(
                          u32&           Flags,
                          const char*    pFileName,
                          s32            LineNumber,
                          const char*    pExprString,
                          const char*    pMessageString )  __attribute__((analyzer_noreturn));
#else
    #define _XTEST(A) (!(A))
    xbool x_AssertHandler(
                          u32&           Flags,
                          const char*    pFileName,
                          s32            LineNumber,
                          const char*    pExprString,
                          const char*    pMessageString );
#endif


#ifdef X_ASSERT

    #ifndef TARGET_3DS
        #define ASSERTCT(expr)   static_assert( expr, "" );
    #else
        #define ASSERTCT(expr)   do{ struct assertct {u8 xCT_ASSERT[(_XTEST(expr))?1:-1];}; } while(FALSE)
    #endif

    #define ASSERTSL(level,expr,str)    do{ static u32 assert_Flags = 0; if( x_ChooseDebugLevel(level) && _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, str  ) )  X_BREAK; } while( FALSE )
    #define ASSERTL(level,expr)         do{ static u32 assert_Flags = 0; if( x_ChooseDebugLevel(level) && _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, NULL ) )  X_BREAK; } while( FALSE )
    #define ASSERT(expr)                do{ static u32 assert_Flags = 0; if( _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, NULL ) )  X_BREAK; } while( FALSE )
    #define ASSERTS(expr,str)           do{ static u32 assert_Flags = 0; if( _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, str  ) )  X_BREAK; } while( FALSE )
    #define VERIFY(expr)                do{ static u32 assert_Flags = 0; if( _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, NULL ) )  X_BREAK; } while( FALSE )
    #define VERIFYS(expr,str)           do{ static u32 assert_Flags = 0; if( _XTEST(expr) && x_AssertHandler( assert_Flags, __FILE__, __LINE__, #expr, str  ) )  X_BREAK; } while( FALSE )

#else

  #if defined( TARGET_PC ) || defined( TARGET_360 )
    #define ASSERTCT(expr)              (__assume(expr))
    #define ASSERT(expr)                (__assume(expr))
    #define ASSERTS(expr,str)           (__assume(expr))
    #define ASSERTSL(level,expr,str)    (__assume(expr))
    #define ASSERTL(level,expr)         (__assume(expr))
  #else
    #define ASSERTCT(expr)              ((void)   0  )
    #define ASSERT(expr)                ((void)   0  )
    #define ASSERTS(expr,str)           ((void)   0  )
    #define ASSERTSL(level,expr,str)    ((void)   0  )
    #define ASSERTL(level,expr)         ((void)   0  )
  #endif
    #define VERIFY(expr)                ((void)(expr))
    #define VERIFYS(expr,str)           ((void)(expr))

#endif

//------------------------------------------------------------------------------

void            x_SetAssertHandler( xassert_fn* pFunction );
xassert_fn*     x_GetAssertHandler( void );

//@@Errors and Exceptions
//     First order of business is to define what is an error. An error is a unpreventable 
//     state of the run time code. It is unpreventable because it is cause by an external
//     source, usually the user, or a disk drive, etc. An Error it is not a preventable
//     state of the code such someone passing a null pointer to a function. That is consider
//     preventable and there is other ways to deal with that most commonly ASSERT.
//
//<P>  There are two ways to handle errors in code. One way is having your function return 
//     a value. Typically an xbool telling the user whether the function was successfully or not.
//     It is highly recommended that you only use this type of errors when it most cases can be
//     safely ignore as client to the code tent to no handle them. The second more reliable method
//     is to throw an exception. This first and for most forces the user to deal with it. The
//     second advantage is that transparently returns from functions. Finally it can be use in
//     constructors/destructor/etc if needed be. One thing to keep in mind is that exceptions 
//     turns into ASSERTS in game code. So for functions that need work both in tools and in game 
//     you should use a return value.
//
//     For more information about this topic look at <LINK x_try> 
#ifdef TARGET_PC
    #define E_PARAM int     // Let developer studio handle some of the exceptions
#else
    #define E_PARAM ...     
#endif

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     These macros are the replacement for the standard exceptions functions.
//     <b>Please remember</b> that all exceptions are converted into ASSERT ones they
//     are compile in game code. Exceptions in general are mainly used in editor and tools.
//     
//<TABLE>
//    x_try              Enable the exception handling from here down. It also 
//                        creates an scope. Such any variable declare between 
//                        this and a "terminator" will only exits inside that scope.
//</TABLE>
//
//<P> * After the x_try you must put one of the fallowing "terminators":
//
//<TABLE>
//    x_catch_display    This terminates the exception handling, and displays
//                        the message to the user. It doesn't throw the
//                        exception anywhere else.
//
//    x_catch_append     Appends a string and make the exception continue.
//
//    x_catch_begin      Terminates the exception handling, and begins a block
//                        where the exceptions that are thrown between x_trys
//                        and x_catch_begin can be handle. This requires x_catch_end.
//
//    x_catch_end        This command comes after the x_catch_begin and it is used
//                        to indicate that the handling block is over. This doesn't
//                        continue the exception. 
//
//    x_catch_end_ret    This command comes after the x_catch_begin and it is used
//                        to indicate that the handling block is over. Then it 
//                        continues the exception up.
//
//    x_append_throw     This command can only be use inside the x_catch_ begin/end. It is
//                        use to continue the exception going up the stack. But also
//                        allows to add more information about an exception been
//                        thrown. For instance you can put the function name where
//                        the block is so the user can get a call-stack back. 
//                        <B>EX:</B><CODE>
//                        x_append_throw( xfs( "CString::malloc( %d )", nBytes) ); 
//                            // or you can simple use
//                        x_append_throw( NULL );
//                            // is you have nothing to add for the exception.</CODE>
//</TABLE>
//
//<P>  You can throw any error any time you want, must you must pay attention
//     where you actually throw them. If you doing between the x_trys and any 
//     of the "terminators" you will be able to handle your own throw, if not it
//     will go up to the next function. 
//
//<TABLE>
//     x_throw         This command is the only one that the system understand 
//                      for getting errors. The use is simple: x_throw( Error message );
//                      The error message must be a String. You can of course use the 
//                      fs class to build a more complex message. x_throw should 
//                      not be use inside a x_catch_begin unless your intencion is to 
//                      terminate an ongoing exception and start a new one.
//</TABLE>
//    
// Example:
//<CODE>
//  void Test01( void )
//  {
//      char* pAlloc01 = NULL;  // Keep variables that may need to be 
//      char* pAlloc02 = NULL;  // clean up at the top of the file.
//  
//      
//  x_try;                // From here down we can catch exceptions
//
//      pAlloc01 = x_new( char, 1000, 0);
//      if( pAlloc01 == NULL ) 
//          x_throw( "Meaning full message here" );
//               
//      pAlloc02 = x_new( char, 1, 0);
//      if( pAlloc02 == NULL ) 
//          x_throw( "Meaning full message here" );
//
//      
//  x_catch_begin;              // Begin handle the errors
//
//      if( pAlloc01 ) x_delete( pAlloc01 );
//      if( pAlloc02 ) x_delete( pAlloc02 );
//
//  x_catch_end_ret;            // End and throw the exception up
//
//      x_memset( pAlloc01, 0, 1000 );
//  }
//
//  void Main( void )
//  {
//
//  x_try;                  // We want to catch any errors
//
//      Test01();           // We want to stop app if this ones fails
//      Test01();
//                          // Handle the next one individually
//      x_try;              // Lets catch the error if it throws any
//         Test01();
//      x_catch_begin;      // Begin handle 
//         ReleaseMemory();
//      x_catch_end;        // Must always finish the block
//      
//
//  x_catch_display;        // Nothing to clean up. This is the top level so
//                          // Lets display any error
//
//      x_printf( "We didn't crash!"); 
//  }  
//</CODE>
//------------------------------------------------------------------------------
#define x_try              

// <COMBINE x_try>
#define x_catch_display          

// <COMBINE x_try>
#define x_display_exception      

// <COMBINE x_try>
#define x_append_exception_msg(S)

// <COMBINE x_try>
#define x_catch_begin    

// <COMBINE x_try>
#define x_catch_end      

// <COMBINE x_try>
#define x_throw(S)       

// <COMBINE x_try>
#define x_append_throw(S)

// <COMBINE x_try>
#define x_catch_append(S)

// <COMBINE x_try>
#define x_catch_end_ret

//------------------------------------------------------------------------------
extern  xbool g_bSkipAllThrowDialogs;
extern  xbool g_bExceptionBreak;
xbool       x_ExceptionHandler( const char* pFileName, s32 LineNum, const char* pMessage, u32& Flags );
void        x_ExceptionDisplay( void );
const char* x_ExceptionMessage( void );

//Error code
/*
struct x_error_msg_struct
{
    enum
    {
        X_DATAFILE_ERR_CAN_NOT_OPEN  = -4000,
        X_DATAFILE_ERR_CAN_NOT_WRITE, 
        X_DATAFILE_ERR_READING_FIELD,
        X_DATAFILE_ERR_READING_RECORD,

        X_DATAFILE_ERR_UNDEFINED
    };
    s32 m_iErrCode;
    xstring m_strMsg;

    x_error_msg_struct()    {};
    x_error_msg_struct( s32 iCode, const xstring& strMsg ):m_iErrCode(iCode), m_strMsg(strMsg) {};
};
*/

//==============================================================================
//==============================================================================
// HARDWARE EXCEPTION HANDLER
//==============================-================================================
//==============================================================================

typedef void xdebug_crash_fn   ( char *pBuffer, s32 Length );

//------------------------------------------------------------------------------

void               x_DebugSetCrashFunction( xdebug_crash_fn* pCallbackFn );
xdebug_crash_fn*   x_DebugGetCrashFunction( void );
