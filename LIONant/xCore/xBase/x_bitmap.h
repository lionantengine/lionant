//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
///------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class serves as a container for a texture and a bitmap. The class can
//     handle cubemaps as well as multiple frame animations. Note that there is not
//     information about the playback of the animation. The class may also contain
//     mip levels for any texture.
//
//<CODE>
//
// Note that the Offsets is from after the offset table (begging of the actual data)
//
//                                     Memory Layout
// m_pMip ---------------------------> +-------------------+
//                                     | mip offset array  |
//          mip offset array           +-------------------+ <-- offset base
// m_pMip -> +------------+            |frame 0 (bitmap 1) |
//           | s32 Offset |  ------->  | Mip0 Data         |
//           +------------+            |                   |
//           | s32 Offset |  ------->  | Mip1 Data         |
//           +------------+            |                   |
//           | s32 Offset |  ------->  | Mip2 Data         |
//           +------------+            |                   |
//           | s32 Offset |  ------->  | Mip3 Data         |
//           +------------+            |                   |
//           | s32 Offset |  ------->  | Mip4 Data         |
//           +------------+            |                   |
//                                     +-------------------+
//     *  Note that previous offsets   |frame 1            |
//        Still work in bitmap 2       | ..                |
//        as well (same size)          | ...               |
//                                     |                   |
//     *  If the bitmap will have a    +-------------------+
//        palette it will be part of   +-------------------+
//        the mip data as well,        +-------------------+
//        at the top.
//
// There are a few compressed texture types supported. Here is a quick over view of some of them:
//
//      * PVR1 also known as PVRTC - This is Imagination’s version one of its widely used PowerVR texture compression.
//        It supports a 2/4bpp versions and RGBA/RGB as well. It is not block base rather it has 2 low rest texture
//        that are combine with a larger gray scale texture. More info:
//        http://blog.imgtec.com/powervr/pvrtc-the-most-efficient-texture-compression-standard-for-the-mobile-graphics-world
//
//      * PVR2 also know as PVRTC2 - This is Imagination's newly updated compression formats. When possible use this over version one.
//        http://blog.imgtec.com/powervr/pvrtc2-taking-texture-compression-to-a-new-dimension
//
//      * ETCn - ETC1 (Ericsson texture compression) and ETC2/EAC (backwards compatible with ETC1 and mandatory in the OpenGL ES 3.0 graphics standard)
//        http://en.wikipedia.org/wiki/Ericsson_Texture_Compression
//
//      * ASTC - (Adaptive scalable texture compression), an upcoming optional extension for both OpenGL and OpenGL ES
//        http://en.wikipedia.org/wiki/Adaptive_Scalable_Texture_Compression
//
//      * ATITC - (ATI texture compression)
//
//      * S3TC also know as DXTn - (S3 texture compression), also called DXTn, DXTC or BCn
//        http://en.wikipedia.org/wiki/S3_Texture_Compression
//
//</CODE>
//
// Todo:
//     The class in unfinish
//
// See Also:
//     xcolor
//==============================================================================
class xbitmap
{
public:

    // Bit wise formatting for the enumeration.
    // FORMAT_(LOW BITS elements first then moving to HIGH BITS)
    enum format:u8
    {
        FORMAT_NULL,

	    FORMAT_A4B4G4R4     = xcolor::FMT_16_ABGR_4444,
        FORMAT_B5G6R5       = xcolor::FMT_16_BGR_565,
        FORMAT_A1B5G5R5     = xcolor::FMT_16_ABGR_1555,
        FORMAT_R8G8B8       = xcolor::FMT_24_RGB_888,
        FORMAT_R8G8B8U8     = xcolor::FMT_32_RGBU_8888,
        FORMAT_R8G8B8A8     = xcolor::FMT_32_RGBA_8888,         // PRIMARY FORMAT (xcolor)
        FORMAT_XCOLOR       = FORMAT_R8G8B8A8,
        FORMAT_A8R8G8B8     = xcolor::FMT_32_ARGB_8888,
        FORMAT_U8R8G8B8     = xcolor::FMT_32_URGB_8888,
        
        FORMAT_XCOLOR_END   = xcolor::FMT_END,                  // end of the range of xcolor
        
	    FORMAT_PAL4,                                            // 4 bpp Index + 16  RGBA8888 palette
	    FORMAT_PAL8,                                            // 8 bpp Index + 256 RGBA8888 palette
        
	    FORMAT_DXT1_RGBA,                                            // 4 bpp (bits per pixel) including 1bit alpha
	    FORMAT_DXT3_RGBA,                                            // 8 bpp RGBA for quick discontinuities
	    FORMAT_DXT5_RGBA,                                            // 8 bpp RGBA for alpha gradients
        
        FORMAT_R8,
        FORMAT_D32,
        FORMAT_R32,
        FORMAT_R8G8,
        FORMAT_R16G16B16A16,
        FORMAT_R16G16B16A16_f,
        FORMAT_R10G10B10A2,
        FORMAT_D24S8,
        FORMAT_R11G11B10,

	    FORMAT_16DU16DV,
	    FORMAT_16_DOT3_COMPRESSED,          // ??? DXT1? or ATI version?
	    FORMAT_A8,

	    // Xenon formats here
	    FORMAT_360_DXN,	                    // normal map compression ... 8bpp
	    FORMAT_360_DXT3A,	                // dxt3a
	    FORMAT_360_DXT5A,	                // dxt5a
	    FORMAT_360_CTX1,	                // normal map compression ... 4bpp

        FORMAT_ETC1,                        // 4pp RGB - Nintendo compression modes
        FORMAT_ETC2,
        
        FORMAT_PVR1_2RGB,                   // PVR compression modes
        FORMAT_PVR1_2RGBA,
        FORMAT_PVR1_4RGB,
        FORMAT_PVR1_4RGBA,

        FORMAT_PVR2_2RGBA,
        FORMAT_PVR2_4RGBA,
        
        FORMAT_TOTAL
    };

public:

                                   ~xbitmap             ();
                                    xbitmap             ( xserialfile& ){ASSERTCT(sizeof(*this)==32);}
                                    xbitmap             ( void );
            void                    Kill                ( void );
            xbool                   Load                ( const char* FileName );
            xbool                   Save                ( const char* FileName ) const;
            xbool                   SaveTGA             ( const xstring FileName ) const;
            void                    SerializeIO         ( xserialfile& SerialFile ) const;
    
            void                    setOwnMemory        ( xbool bOwnMemory );
            void                    setHasAlphaInfo     ( xbool bAlphaInfo );
            void                    ComputeHasAlphaInfo ( void );
            xbool                   hasAlphaInfo        ( void ) const;
            xbool                   hasAlphaChannel     ( void ) const;
    
            xbool                   isValid             ( void ) const { return !!m_pData; }
            xbool                   isSquare            ( void ) const;
            xbool                   isPowerOfTwo        ( void ) const;
            xbool                   isGamma             ( void ) const { return !!(m_Flags&FLAGS_IS_GAMMA); }
            s32                     getWidth            ( void ) const;
            s32                     getHeight           ( void ) const;
            format                  getFormat           ( void ) const;
            void                    setFormat           ( format Format ){ m_Format = Format; }
            void                    setColorSpace       ( xbool isGamma ) { if(isGamma) m_Flags |=FLAGS_IS_GAMMA; else m_Flags &=~FLAGS_IS_GAMMA;  }
            s32                     getFrameSize        ( void ) const { return m_FrameSize; }
            void                    Copy                ( const xbitmap& Src ) { CreateFromMips( &Src, 1 ); }

            s32                     getDataSize         ( void ) const;
            s32                     getMipCount         ( void ) const;
            const void*             getMip              ( s32 Mip, s32 Frame=0 ) const;
            s32                     getMipSize          ( s32 Mip ) const;
            s32                     getFullMipChainCount( void ) const;
    
            void                    CreateResizedBitmap ( xbitmap& Dest, s32 FinalWidth, s32 FinalHeight ) const;
    
            void                    setDefaultTexture   ( void );
    static  const xbitmap&          getDefaultBitmap    ( void );
    
            void                    CreateBitmap        ( s32 Width, s32 Height );
    
            void                    CreateFromMips      ( const xbitmap* pMipList, s32 Count = 1 );
    
            void                    setupFromColor      ( s32             Width                           ,
                                                          s32             Height                          ,
                                                          void*           pData                           ,
                                                          xbool           bFreeMemoryOnDestruction = TRUE );
    
            void                    setup               ( s32             Width                         ,
                                                          s32             Height                        ,
                                                          xbitmap::format BitmapFormat                  ,
                                                          s32             DataSize                      ,
                                                          s32             FrameSize                     ,
                                                          void*           pData                         ,
                                                          xbool           bFreeMemoryOnDestruction      ,
                                                          s32             nMips                         ,
                                                          s32             nFrames                       );

    

/*
    void                    ConvertBitmap       ( s32 Bpp, xcolor::format Format );
    void                    ConvertBitmap       ( xbitmap& Bitmap, s32 Bpp, xcolor::format Format ) const;    

    u32                     GetPixel            ( s32 X, s32 Y, s32 Mip = 0 ) const;
    void                    SetPixel            ( s32 X, s32 Y, u32 Pixel, s32 Mip = 0 );
    xcolor                  GetPixelColor       ( s32 X, s32 Y, s32 Mip = 0 ) const;
    xcolor                  GetBilinearColor    ( f32 ParamU, f32 ParamV, xbool Clamp=FALSE, s32 Mip = 0 ) const;
    void                    SetPixelColor       ( xcolor Color, s32 X, s32 Y, s32 Mip = 0 );
*/
    
//==============================================================================
protected:

    enum flags
    {
        FLAGS_CUBE_MAP           = X_BIT(0),
        FLAGS_OWN_MEMORY         = X_BIT(1),
        FLAGS_ALPHA_INFORMATION  = X_BIT(2),        // Tells if it has an alpha channel it says whether it contains actual information
        FLAGS_IS_GAMMA           = X_BIT(3),        // Tells which color space the texture is in... gamma space or linear space
        /*
        FLAGS_U_TILE             = X_BIN(00000100),
        FLAGS_U_MIRROR           = X_BIN(00001000),
        FLAGS_U_CLAMP            = X_BIN(00001100),
        FLAGS_U_MASK             = X_BIN(00001100),
        FLAGS_V_TILE             = X_BIN(00010000),
        FLAGS_V_MIRROR           = X_BIN(00100000),
        FLAGS_V_CLAMP            = X_BIN(00110000),
        FLAGS_V_MASK             = X_BIN(00110000),
        FLAGS_W_TILE             = X_BIN(01000000),
        FLAGS_W_MIRROR           = X_BIN(10000000),
        FLAGS_W_CLAMP            = X_BIN(11000000),
        FLAGS_W_MASK             = X_BIN(11000000)
         */
    };

    struct mip
    {
        s32     m_Offset;       // Offset in xbitmap::m_pData for a mip's data
    };

//==============================================================================
protected:

    union
    {
        mip*                    m_pData;
        xserialfile::ptr<u8>    m_RawData;      // +8 pointer to the data
    };
    
    s32                         m_DataSize;     // +4 total data size in bytes
    s32                         m_FrameSize;    // +4 Size of one frame of data.(one bitmap)
    s32                         m_Height;       // +4 height in pixels
    s32                         m_Width;        // +4 width in pixels
    u8                          m_Flags;        // +1 miscellaneous flags
    u8                          m_nMips;        // +1 Number of mips
    format                      m_Format;       // +1 type of bitmap format.
    u8                          m_nFrames;      // +1 Number of bitmaps in here (example cube maps == 6)
    u8                          m_Pad[4];       // 32 bytes total
};
