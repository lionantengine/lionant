//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is useful when ever you want to have an array of unknown number
//     of elements, or a dynamic amount of them. The class will construct only
//     elements that are been used even thought its capacity may reflect other wise.
//     So full usage of void constructors and destructors is supported.
//
//<P>  Note that when deleting an element of the array the other elements store
//     higher than the index will be moved down to close the gap. This is useful
//     for sorted elements. Although there is an obvious performance hit. If you
//     need faster deletes you may want to look at the xharray.
//
//<P>  When deleting elements from the xarray the system does not provide any
//     additional protection for its users. It is upto the user to know
//     that an index may become invalid or that a reference of an element
//     has been destroy.
//
//<P>  The xarray class tries to minimize the number of dynamic memory operations
//     performed.  However, it relies on information provided by the SetCapacity
//     function.
//
//<P>  An xarray can be prevented from automatically adjusting its capacity by
//     "locking" it with SetGrowAmount( 0 ).  The xarray will behave normally as
//     long as no alterations to the capacity are required. The xarray can be
//     unlocked at any time by changing the grow amount again.
//
//<P>  The xarray uses the xptr which provides unprecedented security and flexibility
//     to the user and the system. All memory allocated via the xarray can be moved
//     by the system in order to keep memory fragmentation down. In addition the
//     xarray will be able to catch the user from making the typical bug of
//     dereferencing an array element while appending another.
//
//<P>  It should be clear to the user that having a pointer to any of the elements in
//     the array will be at least doubly bad. First the array could grow which could
//     case the memory location to change. Second the system may decided to move
//     the memory for defragmentation proposes and again nuke the user. Having
//     pointers as a genral rule is a bad idea. The array allows the user to have
//     indices which are always valid. Not only that but the user may choose to
//     store their indices as u16 there by cutting the memory usage in comparison to
//     a pointer which could be up to 8 bytes.
//
//<P>  To access an element of the array you must first create a reference with xptr_lock
//     then you can access any of the elements in the xarray via the [] operator function.
//
// Example:
//<CODE>
//      void Test( void )
//      {
//          s32             Index;
//          xarray<entity>  lEntity;
//          entity&         Entity = lEntity.append( Index );
//
//          // Create an xptr_lock for safety
//          xptr_lock LockArray( lEntity );
//
//          // You can access your elements like this
//          Entity.m_pNext = NULL;
//          // or like this
//          lEntity[Index].m_pNext = NULL;
//      }
//</CODE>
// See also:
//     xptr_lock xptr xharray xsafe_array xblock_array xcmd_array
//==============================================================================

template< typename T, typename D > class xarray2;

//---------------------------------------------------------------------------------------

template< typename T, typename D >
class xarray2_interface 
{
public:

    using   this_type           = xarray2<T,D>;
    using   smart_lock          = D;
    using   entry_type          = T;
    using   public_interface    = xarray2_interface< entry_type, smart_lock >;
                        
public:

    s32                 getCount            ( void ) const              { return m_Count; }
    s32                 getCapacity         ( void ) const              { return m_Buffer.getCount(); }
    void                appendList          ( s32 Count );
    T&                  append              ( void );
    T&                  append              ( s32& Index );
    void                Grow                ( s32 NewItems );
    void                Copy                ( const this_type& Array );
    void                DeleteAllEntries    ( void );
    void                DeleteWithCollapse  ( s32 Index, s32 Count=1 );
    void                DeleteWithSwap      ( s32 Index, s32 Count=1 );
    T&                  Insert              ( s32 Index );
    void                Pack                ( void )                    { m_Buffer.Resize( getCount() ); }
    void                ChangeBehavior      ( u32 Flags ) const         { m_Buffer.ChangeBehavior( Flags ); }
    s32                 getEntrySize        ( void ) const              { return sizeof(T); }

protected:

    enum
    {
        DEFAULT_GROWTH      = 50
    };

protected:
    
    const T&            getEntryReadOnly    ( s32 Index ) const     { ASSERT(Index < getCount() ); return m_Buffer.getEntryReadOnly( Index ); }
    T&                  getEntry            ( s32 Index )           { ASSERT(Index < getCount() ); return m_Buffer.getEntry( Index ); }
    void                DefaultAlloc        ( s32 Count )           { ASSERT(m_Buffer.isValid() == FALSE ); m_Buffer.Alloc( Count ); }
    const smart_lock&   getSmartLock        ( void ) const          { return m_Buffer.getSmartLock(); }
    smart_lock&         getSmartLock        ( void )                { return m_Buffer.getSmartLock(); }

protected:
    
    xptr2<T,D>   m_Buffer;
    s32          m_Count        = 0;

protected:

    template<class>                   friend class const_xref;
    template<class>                   friend class mutable_xref;
    template< typename, typename >    friend class xarray2_interface;
};

//---------------------------------------------------------------------------------------

template< typename T, typename D = xptr2_debug_lock >
class xarray2 : protected xarray2_interface< T, D >
{
public:

    using   this_type           = xarray2<T,D>;
    using   smart_lock          = D;
    using   entry_type          = T;
    using   public_interface    = xarray2_interface< entry_type, smart_lock >;
    
public:

    s32                         getCount            ( void ) const        { return public_interface::getCount(); }
    void                        ChangeBehavior      ( u32 Flags ) const   { public_interface::ChangeBehavior( Flags ); }
    s32                         getEntrySize        ( void ) const        { return sizeof(T); }

protected:

    public_interface*               getPublicInterface   ( void )        { return this; }
    const public_interface* const   getPublicInterface   ( void ) const  { return this; }
    
protected:

    template<class> friend class const_xref;
    template<class> friend class mutable_xref;
};

