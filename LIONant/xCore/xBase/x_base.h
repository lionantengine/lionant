//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
#pragma once		// Include this file only once
#ifndef X_BASE_H
#define X_BASE_H


//==============================================================================
// DEFINES
//==============================================================================
class xfile;
class xbitstream;

#include "x_target.h"
#include "x_types.h"
#include "x_metaprog.h"
#include "x_debug.h"
#include "x_plus.h"
#include "x_ptr2.h"
#include "x_lockless.h"
#include "x_memory_smallblock.h"
#include "x_memory.h"
#include "x_ptr.h"
#include "x_container.h"
#include "x_container2.h"
#include "x_string.h"
#include "x_math.h"
#include "x_color.h"
#include "x_thread.h"
#include "x_guid.h"
#include "x_time.h"
#include "x_networkv2.h"
#include "x_bitstream.h"
#include "x_stdio.h"
#include "x_random.h"
#include "x_datafile.h"
#include "x_cmdline.h"
#include "x_message.h"
#include "x_bitmap.h"
#include "x_command_system.h"
#include "x_scheduler.h"
#include "x_va_list.h"
#include "x_property.h"

//==============================================================================
// INLINE FUNCTIONS
//==============================================================================
#include "implementation/x_types_inline.h"
#include "Implementation/x_debug_inline.h"
#include "Implementation/x_memory_inline.h"
#include "Implementation/x_ptr_inline.h"
#include "Implementation/x_ptr2_inline.h"
#include "Implementation/x_random_inline.h"
#include "Implementation/x_plus_inline.h"
#include "Implementation/x_stdio_inline.h"
#include "Implementation/x_thread_inline.h"
#include "Implementation/x_math_general_inline.h"
#include "Implementation/x_math_r3_inline.h"
#include "Implementation/x_math_v2_inline.h"
#include "Implementation/x_math_v3d_inline.h"
#include "Implementation/x_math_v3_inline.h"
#include "Implementation/x_math_v4_inline.h"
#include "Implementation/x_math_m4_inline.h"
#include "Implementation/x_math_q_inline.h"
#include "Implementation/x_math_p_inline.h"
#include "Implementation/x_math_bbox_inline.h"
#include "Implementation/x_math_sph_inline.h"
#include "Implementation/x_math_irect_inline.h"
#include "Implementation/x_math_rect_inline.h"
#include "Implementation/x_bitstream_inline.h"
#include "Implementation/x_bitmap_inline.h"
#include "Implementation/x_color_inline.h"
#include "Implementation/x_container_inline.h"
#include "Implementation/x_container2_inline.h"
#include "Implementation/x_datafile_inline.h"
#include "Implementation/x_time_inline.h"
#include "Implementation/x_guid_inline.h"
#include "Implementation/x_lockless_inline.h"
#include "Implementation/x_message_inline.h"
#include "Implementation/x_property_inline.h"

//==============================================================================
// TODO:
// Missing Items:
// * xbitmap
// * xwstring
// * string pool
// * console (cmd line ingame)      <- May be in engine?
// * debug menus                    <- May be in engine?
// * scheduler  
// * perfmon                        
// * remove the name space
// * os
// * write test for all the modules
// * work on documentation
// * Do a pass for name convention
//      - Types xtype
//      - Functions x_Function
//      - Upper case all functions?
//==============================================================================

//==============================================================================
// FUNCTIONS
//==============================================================================

void x_BaseInitInstance( void );
void x_BaseKillInstance( void );
void x_BaseInit ( void );
void x_BaseKill ( void );

//==============================================================================
// INLINES
//==============================================================================

//==============================================================================
// END X_DEBUG_H
//==============================================================================
#endif
