//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This file assists with cross platform development by providing a standard
//     set of platform definitions with anticipated variations.
// Description:
//     This file assists with cross platform development by providing a standard
//     set of platform definitions with anticipated variations.  The selection of a
//     platform and variation is based on a key macro definition.
// 
//     The target macro is provided in different ways depending on the development 
//     environment. But usually it is provided by the user when he sets the 
//     build configuration in the development environment. 
//     
//     <TABLE>
//     Target Macro             Description                            MFC Safe?
//     -----------------------  -------------------------------------  =========
//     none                     Assumes TARGET_PC_EDITOR               Y
//     TARGET_PC_EDITOR         Editors, command line, etc             Y
//     TARGET_PC                Mainly use for the PC Game.            N
//     TARGET_IOS               Used for iOS devices                   N
//     TARGET_ANDROID           Used for Android devices               N
//     TARGET_PS3_DEV           Sony PlayStation 3 DevKit              N
//     TARGET_PS3_CLIENT        Sony PlayStation 3 "Debug Station"     N
//     TARGET_PS3_DVD           Sony PlayStation 3 console             N
//     TARGET_360_DEV           Microsoft X-Box 360 DevKit             N
//     TARGET_360_CLIENT        Microsoft X-Box 360 "Debug Station"    N
//     TARGET_360_DVD           Microsoft X-Box 360 console            N
//     TARGET_RVL_DEV           Nintendo Revolution DevKit             N
//     TARGET_RVL_CLIENT        Nintendo Revolution "Debug Station"    N
//     TARGET_RVL_DVD           Nintendo Revolution console            N
//     TARGET_OSX               Apple Mac OSX                          N
//     TARGET_3DS               Nintendo 3DS                           N
//     </TABLE>
// 
//     Targets which are "MFC safe" (and have _MFC in the macro) will disable the
//     x_files version of operators new and delete.  Graphic engines prepared for
//     MFC configurations should not provide a main() function.
// 
//     When present, each of the primary target macros in the list above will, in
//     turn, cause other secondary macros to be defined. These Secondary macros 
//     are set automatically for the user. 
// 
//     <TABLE>
//     Target Macro             Secondary Macros
//     -----------------------  ----------------------------------------------
//     none                     TARGET_PC   TARGET_IOS   
//     TARGET_PC_EDITOR         TARGET_PC   TARGET_EDITOR   TARGET_MFC                 
//     TARGET_PC 
//	   TARGET_Marmalade
//     TARGET_IOS
//     TARGET_ANDROID
//     TARGET_PS3_DEV           TARGET_PS3  TARGET_DEV
//     TARGET_PS3_CLIENT        TARGET_PS3  TARGET_CLIENT
//     TARGET_PS3_DVD           TARGET_PS3  TARGET_DVD
//     TARGET_360_DEV           TARGET_360  TARGET_DEV      
//     TARGET_360_CLIENT        TARGET_360  TARGET_CLIENT
//     TARGET_360_DVD           TARGET_360  TARGET_DVD
//     TARGET_RVL_DEV           TARGET_RVL  TARGET_DEV
//     TARGET_RVL_CLIENT        TARGET_RVL  TARGET_CLIENT
//     TARGET_RVL_DVD           TARGET_RVL  TARGET_DVD
//     TARGET_OSX
//     TARGET_3DS
//     </TABLE>
// 
//     Finally the user needs to define in the configuration which kind of build 
//     is he trying to do. X_DEBUG could be use even if the compiler is set in 
//     optimization. 
//
//     <TABLE>
//     Target Macro             Description
//     -----------------------  ----------------------------------------------
//     none                     Ignores debugging features such asserts.
//     X_DEBUG                  Activates debugging features such asserts.
//     </TABLE>
// 
//     Other Macros provided to the user automatically are:
//
//     <TABLE>
//     Target Macro             Description
//     -----------------------  ----------------------------------------------
//     MS_ALIGNMENT             Microsoft structure alignment definition.  
//     SN_ALIGNMENT             SN structure alignment definition.
//     X_BIG_ENDIAN             Endianness of the hardware
//     X_LITTLE_ENDIAN          Endianness of the hardware
//     </TABLE>
//
//     Finally there is a convenient platform enumeration that can be use with 
//     logical operations. ex: (PLATFORM_PC | PLATFORM_360)
//------------------------------------------------------------------------------
#pragma once		// Include this file only once
#ifndef X_TARGET_H
#define X_TARGET_H

//==============================================================================
//  
// Hardware enumeration
//
//==============================================================================

enum xplatform
{
    X_PLATFORM_NULL,
    X_PLATFORM_PC,
    X_PLATFORM_PS3,
    X_PLATFORM_360,
    X_PLATFORM_RVL,
    X_PLATFORM_IOS,
	X_PLATFORM_MARMALADE,
    X_PLATFORM_OSX,
    X_PLATFORM_3DS,
    X_PLATFORM_ANDROID,
    X_PLATFORM_COUNT
};

//DOM-IGNORE-BEGIN
//==============================================================================
//  
//  Check for ambiguous or insufficiently qualified target specification.
//  
//  *** IF YOU GOT AN ERROR HERE ***, then you specified a target platform 
//  without sufficiently qualifying the target.
//
//==============================================================================

#ifdef TARGET_PS3
#error TARGET_PS3 is not a sufficient target specification.
#error Use either TARGET_PS3_DEV, TARGET_PS3_CLIENT, or TARGET_PS3_DVD.
#endif

#ifdef TARGET_XBOX360
#error TARGET_XBOX360 is not a sufficient target specification.
#error Use either TARGET_XBOX360_DEV, TARGET_XBOX360_CLIENT, or TARGET_XBOX360_DVD.
#endif

#ifdef PLATFORM_REVOLUTION
#error PLATFORM_REVOLUTION is not a sufficient target specification.
#error Use either PLATFORM_REVOLUTION_DEV, PLATFORM_REVOLUTION_CLIENT, or PLATFORM_REVOLUTION_DVD.
#endif

//==============================================================================
//  Playstation 3 Targets
//==============================================================================

#ifdef TARGET_PS3_DEV
    #define X_PLATFORM X_PLATFORM_PS3
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_PS3
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_PS3
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_PS3_CLIENT
    #define X_PLATFORM X_PLATFORM_PS3
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_PS3
        #define TARGET_CLIENT
        #define TARGET_PLATFORM PLATFORM_PS3
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_PS3_DVD
    #define X_PLATFORM X_PLATFORM_PS3
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_PS3
        #define TARGET_DVD
        #define TARGET_PLATFORM PLATFORM_PS3
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  Nintendo Revolution Targets
//==============================================================================

#ifdef TARGET_RVL_DEV
    #define X_PLATFORM X_PLATFORM_RVL
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_RVL
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_RVL
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_RVL_CLIENT
    #define X_PLATFORM X_PLATFORM_RVL
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_RVL
        #define TARGET_CLIENT
        #define TARGET_PLATFORM PLATFORM_RVL
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_RVL_DVD
    #define X_PLATFORM X_PLATFORM_RVL
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_RVL
        #define TARGET_DVD
        #define TARGET_PLATFORM PLATFORM_RVL
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  Microsoft X-Box 360 Targets
//==============================================================================

#ifdef TARGET_360_DEV
    #define X_PLATFORM X_PLATFORM_360
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_360
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_360
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_360_CLIENT
    #define X_PLATFORM X_PLATFORM_360
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_360
        #define TARGET_CLIENT
        #define TARGET_PLATFORM PLATFORM_360
        #define VALID_TARGET
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_360_DVD
    #define X_PLATFORM X_PLATFORM_360
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_360
        #define TARGET_DVD
        #define TARGET_PLATFORM PLATFORM_360
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  PC Targets
//==============================================================================

#ifdef TARGET_PC
    #define X_PLATFORM X_PLATFORM_PC
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_PLATFORM PLATFORM_PC
        #define VALID_TARGET
        #define SSE2_SUPPORT
        #include "emmintrin.h"
    #endif
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_PC_EDITOR
    #define X_PLATFORM X_PLATFORM_PC
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_PC
        #define TARGET_MFC
        #define TARGET_PLATFORM PLATFORM_PC
        #define TARGET_EDITOR
        #define VALID_TARGET
        #define X_USE_NATIVE_NEW_AND_DELETE
        #define SSE2_SUPPORT
//        #define __PLACEMENT_NEW_INLINE      // Tells MFC that we are dealing with the placement new/delete
    #endif
#endif

//==============================================================================
//  IOS Targets
//==============================================================================

#ifdef TARGET_IOS
    #define X_PLATFORM X_PLATFORM_IOS
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_IOS
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  Marmalade Targets
//==============================================================================
#ifdef TARGET_MARMALADE
    #define X_PLATFORM X_PLATFORM_MARMALADE
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_MARMALADE
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  Marmalade Targets
//==============================================================================
#ifdef TARGET_ANDROID
    #define X_PLATFORM X_PLATFORM_ANDROID
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_ANDROID
        #define VALID_TARGET
    #endif
#endif

//==============================================================================
//  OSX Targets
//==============================================================================

#ifdef TARGET_OSX
    #define X_PLATFORM X_PLATFORM_OSX
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_OSX
        #define VALID_TARGET
        #define SSE2_SUPPORT
        #include "emmintrin.h"
    #endif
#endif

//==============================================================================
//  3DS Targets
//==============================================================================

#ifdef TARGET_3DS
    #define X_PLATFORM X_PLATFORM_3DS
    #ifdef VALID_TARGET
        #define MULTIPLE_TARGETS
    #else
        #define TARGET_DEV
        #define TARGET_PLATFORM PLATFORM_3DS
        #define VALID_TARGET
    #endif
#endif


//------------------------------------------------------------------------------
// Check for the "target macro is <none>" case when on a PC.  For now, we will
// assume that only the Microsoft compiler is in use on the PC.
//DOM-IGNORE-BEGIN

#ifndef VALID_TARGET                        // If we haven't already got a target,
    #ifdef _MSC_VER                         // and we are using the Microsoft compiler...
        #define X_PLATFORM X_PLATFORM_PC
        #define TARGET_PC
        #define TARGET_PLATFORM PLATFORM_PC
        #define VALID_TARGET
        #define X_USE_NATIVE_NEW_AND_DELETE
//        #define __PLACEMENT_NEW_INLINE      // Tells MFC that we are dealing with the placement new/delete
    #endif
#endif


//==============================================================================
//
//  Make sure we found a proper target specification.  If you get a compilation 
//  error here, then your compilation environment is not specifying one of the
//  target macros.
//
//==============================================================================

#ifndef VALID_TARGET
#error Target specification invalid or not found.
#error The compilation environment must define one of the macros listed in x_targets.hpp.
#endif

//==============================================================================
//
//  Make sure we did not somehow get multiple tarter platform specifications.
//  *** IF YOU GOT AN ERROR HERE ***, then you have defined more than one of
//  the target specification macros.
//
//==============================================================================

#ifdef MULTIPLE_TARGETS
#error Multiple target specification definition macros were detected.
#error The compilation environment must define only one of the macros listed in x_targets.hpp.
#endif

//==============================================================================
//
//  Endian Designation
//
//  For all configurations, either BIG_ENDIAN or LITTLE_ENDIAN will be defined.
//
//==============================================================================
#if defined(TARGET_PC) || defined(TARGET_OSX) || defined(TARGET_IOS) || defined(TARGET_MARMALADE) || defined(TARGET_3DS)

    #define X_LITTLE_ENDIAN

#elif defined(TARGET_PS3) || defined(TARGET_360) || defined(TARGET_RVL) || defined(TARGET_ANDROID)

    #define X_BIG_ENDIAN

#else

    #error Endian is not defined.

#endif

//==============================================================================
//
//  Platform specific data structure alignment.
//
//==============================================================================

#if defined( VENDOR_SN )
    #define X_SN_ALIGNMENT(a)     __attribute__( (aligned(a)) )
    #define X_MS_ALIGNMENT(a)
#elif defined(TARGET_PC)
    #define X_MS_ALIGNMENT(a)     __declspec(align(a) )
    #define X_SN_ALIGNMENT(a)

	#pragma warning( disable : 4521 ) //  warning C4521: multiple copy constructors specified

#elif defined(TARGET_3DS)
    #define X_MS_ALIGNMENT(a)
    #define X_SN_ALIGNMENT(a)
#else
    #define X_MS_ALIGNMENT(a)
    #define X_SN_ALIGNMENT(a)     __attribute__( (aligned(a)) )
#endif

#define X_MEMBER_ALIGMENT(Aligment, a) X_SN_ALIGNMENT(Aligment) a X_MS_ALIGNMENT(Aligment)

//==============================================================================
//  
//  Compatibility with Microsoft's Developer Studio
//
//  DevStudio defines "_DEBUG" for debug build configurations.  If we are on a
//  PC and _DEBUG is defined, go ahead and define appropriate x_files values.
//
//==============================================================================

#if defined( TARGET_PC ) && defined( _DEBUG )
    #ifndef X_DEBUG
        #define X_DEBUG
    #endif
#endif           

//==============================================================================
//
//  Handle exceptions normally for the editor.
//
//==============================================================================

#if defined(TARGET_EDITOR)
    #define X_EXCEPTIONS
#endif

//==============================================================================
//
//  Handle configuration specific options
//
//==============================================================================

#if !defined(X_DEBUG)
    #define X_RETAIL
    #undef X_ASSERT
#else
    #ifndef X_ASSERT
    #define X_ASSERT
    #endif
#endif

//==============================================================================
//
// LWSYNC & THREAD_LOCAL
//
//==============================================================================
// cross platform way to force inlining
#if defined (TARGET_PC)
	#define X_LWSYNC_PS3
	#define X_LWSYNC            _ReadWriteBarrier();
	#define X_THREAD_LOCAL		__declspec( thread )
	#define X_FORCE_INLINE		__forceinline
#elif defined(TARGET_360)
	#define X_LWSYNC_PS3
	#define X_LWSYNC			__lwsync();
	#define X_THREAD_LOCAL		__declspec( thread )
	#define X_FORCE_INLINE		__forceinline
#elif defined(TARGET_PS3)
	#define X_LWSYNC_PS3		__lwsync();
	#define X_LWSYNC
	#define X_THREAD_LOCAL		__thread 
	#define X_FORCE_INLINE		inline
#elif defined(TARGET_IOS)
    #define X_LWSYNC_PS3
    #define X_LWSYNC            OSMemoryBarrier();
    #define X_THREAD_LOCAL
    #define X_FORCE_INLINE      inline
#elif defined(TARGET_ANDROID)
    #define X_LWSYNC_PS3
    #define X_LWSYNC
    #define X_THREAD_LOCAL
    #define X_FORCE_INLINE      inline
#elif defined(TARGET_MARMALADE)
    #define X_LWSYNC_PS3
    #define X_LWSYNC
    #define X_THREAD_LOCAL
    //#define X_THREAD_LOCAL		__declspec( thread )
    #define X_FORCE_INLINE      inline
#elif defined(TARGET_OSX)
    #define X_LWSYNC_PS3
    #define X_LWSYNC
    #define X_THREAD_LOCAL
    #define X_FORCE_INLINE      inline
#elif defined(TARGET_3DS)
    #define X_LWSYNC_PS3
    #define X_LWSYNC
    #define X_THREAD_LOCAL
    #define X_FORCE_INLINE      inline
#endif

//==============================================================================
//
//  Do a few pragmas for the pc version
//
//==============================================================================
#if defined TARGET_PC || defined(TARGET_MARMALADE)
    #define noinline   __declspec(noinline)
    //#pragma inline_recursion( on )
    #pragma inline_depth( 255 )
    #ifdef inline
        #undef inline
    #endif
    #define inline __forceinline
#elif defined(TARGET_IOS) || defined(TARGET_ANDROID) || defined(TARGET_OSX)
    #define noinline   __attribute__((noinline))
#elif defined(TARGET_3DS)
    #define noinline   __declspec(noinline)
#endif

#ifndef __LP64__
    #define __LP64__ 0
#endif

#if ( (__LP64__ == 1) || defined(__x86_64__) || defined(_M_X64) || defined(_WIN64) \
|| defined(__powerpc64__) || defined(__ppc64__) || defined(__PPC64__) \
|| defined(__64BIT__) || defined(_LP64)  \
|| defined(__ia64) || defined(__itanium__) || defined(_M_IA64) )   // Detects 64 bits mode
    #define X_TARGET_64BIT
#else
    #define X_TARGET_32BIT
#endif

#ifdef TARGET_IOS
#define X_USE_NATIVE_NEW_AND_DELETE
#endif


#ifdef TARGET_ANDROID
#define X_USE_NATIVE_NEW_AND_DELETE
#include <android/log.h>

#define LOG_TAG "show infomation"
#define LOGD(a)  __android_log_write(ANDROID_LOG_DEBUG,LOG_TAG,a)
#else
#define LOGD(a)
#endif


#if defined TARGET_PC || defined(TARGET_OSX) || defined(TARGET_3DS)
#define X_USE_NATIVE_NEW_AND_DELETE
#endif
//DOM-IGNORE-END
//==============================================================================
// END
//==============================================================================
#endif 
