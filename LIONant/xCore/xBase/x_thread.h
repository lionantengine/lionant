//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

#if defined TARGET_OSX || defined TARGET_IOS
    #include <pthread.h>
    #include <mach/semaphore.h>
#endif


//===============================================================================
// WARNING: this class is private to this module please don't use it.
// This is the allocation structures for the different xBase components
//===============================================================================
//DOM-IGNORE-BEGIN
struct xbase_instance
{
    xbase_instance( void );

    void*   m_pDebug;       // variables for the x_Debug
    void*   m_pString;      // variables for the x_String
    void*   m_pMessage;     // variables for the x_message
};
//DOM-IGNORE-END

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Like a mutex, a critical section object can be owned by only one thread at a time, 
//     which makes it useful for protecting a shared resource from simultaneous access. 
//     There is no guarantee about the order in which threads will obtain ownership of the 
//     critical section, however, the system will be fair to all threads. Unlike a mutex object, 
//     there is no way to tell whether a critical section has been abandoned.
//
//<P>  Rember that a mutex object are best used per data/class you are trying to protect.
//     Since grouping multiple classes/data inside a critical section may create an increase
//     collision rate amount threads. Which has the net result of making the program slower.
//
//<P>  It is highly recommended you use the xcritical_section with xscope_csection. Because
//     it avoid typical bugs of people forgetting to unlock a critical section.
//
// Example:
//<CODE>
//  struc pepe
//  {
//      s32                 m_Data;
//      xcritical_section   m_CriticalSection;
//
//      void                ChangeData( s32 Data )
//      {
//          xscope_csection Protect(  m_CriticalSection );
//          
//          if( Data < 32 ) 
//          {
//              m_Data += Data / 32; 
//              return;
//          }
//          
//          m_Data %= 64;
//      }
//   }
//</CODE>
// See Also:
//     xscope_csection  xthread
//------------------------------------------------------------------------------
class xcritical_section
{
public:

            xcritical_section      ( void );
           ~xcritical_section      ( void );
    void    BeginAtomic            ( void );
    void    EndAtomic              ( void );
    xbool   IsAtomic               ( void ) const;
    xbool   isValid                ( void ) { return !!m_pCriticalSection; }

protected:

    void*   m_pCriticalSection;
    s32     m_Count;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//------------------------------------------------------------------------------

class xsemaphore
{
public:
                xsemaphore  ( void );
    virtual     ~xsemaphore ( void );
    void        WaitOne     ( void );
    xbool       WaitOne     ( s32 millisecondsTimeOut );
    void        Signal      ( void );
    
protected:
    
#if defined TARGET_OSX || defined TARGET_IOS
    semaphore_t     m_Semaphore;
#else
    void*       mHandle;
#endif
};

//------------------------------------------------------------------------------

class xevent
{
public:
                xevent              ( void );
                ~xevent             ( void );
    void        Init                ( void );
    void        WaitOne             ( void );
    xbool       WaitOne             ( int millisecondsTimeOut );
    void        Set                 ( void );
    void        Reset               ( void );
    xbool       isValid()           { return !!mHandle; };
protected:
    void*       mHandle;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is used when you want to protect a function with a critical 
//     section. Stead of having to do BeginAtomic/EndAtomic this class will do it 
//     for you. When you initialize this class with the critical section it will 
//     automatically call the BeginAtomic. When the class gets destroy it will call
//     EndAtomic. 
//
//<P>  It is higly recomended for the user to use this class stead of calling 
//     BeginAtomic/EndAtomic because it solves all the bugs of people forgeting to
//     call those function in the proper order.
//
// Example:
//<CODE>
//  struc pepe
//  {
//      s32                 m_Data;
//      xcritical_section   m_CriticalSection;
//
//      void                ChangeData( s32 Data )
//      {
//          xscope_csection Protect(  m_CriticalSection );
//          
//          if( Data < 32 ) 
//          {
//              m_Data += Data / 32; 
//              return;
//          }
//          
//          m_Data %= 64;
//      }
//   }
//</CODE>
// See Also:
//     xcritical_section  xthread
//------------------------------------------------------------------------------
template< typename T = xcritical_section >
class xscope_csection
{
public:
            xscope_csection         ( T& CriticalSection ) : m_CriticalSection(CriticalSection){ BeginAtomic();}
           ~xscope_csection         ( void )                                                   { EndAtomic();  }
    void    EndAtomic               ( void )                                                   { if(m_bActive) m_CriticalSection.EndAtomic(); m_bActive = FALSE; }
    void    EndAtomic               ( void ) const                                             { if(m_bActive) m_CriticalSection.EndAtomic(); m_bActive = FALSE; }

protected:

    void    BeginAtomic             ( void )                                                   { m_CriticalSection.BeginAtomic(); m_bActive = TRUE; }
    void    BeginAtomic             ( void ) const                                             { m_CriticalSection.BeginAtomic(); m_bActive = TRUE;  }

protected:

    mutable xbool           m_bActive;
    T&                      m_CriticalSection;
};

#define linenum __LINE__
#define xscope_atomic( A ) const xscope_csection< decltype(A) > X_CREATE_UNIQUE_VARNAME(CriticalScoped) (A)

//-------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The class should be in-heritage, then overwrite the proper members.
//
//<P>	Create           - This function is use to create the thread
//<P>   	pName        - Name of the thread to be created
//<P>		bStartActive - if false you must call Resume for the thread to run. 
//<P>		Priority     - check thread::priority for more information
//<P>		StarckSize   - In bytes
//<P>		bAutoDelete  - Calls delete thread right after the ExitInstance finish executing
// 
//<P>	OnInitInstance    - Override to perform thread instance initialization.
//<P>	OnExitInstance    - Override to clean up when your thread terminates.
//<P>	OnRun             - It gets executed continiously base on priority
// Note:
//      Use x_new to allocate memory for xthread if you want to use auto-release
//------------------------------------------------------------------------------
class xthread
{
public:
    
    enum priority
    {
        PRIORITY_TIME_CRITICAL,
        PRIORITY_HIGHEST, 
        PRIORITY_ABOVE_NORMAL, 
        PRIORITY_NORMAL, 
        PRIORITY_BELOW_NORMAL, 
        PRIORITY_LOWEST, 
        PRIORITY_IDLE, 


        PRIORITY_PADDING    = 0xffffffff
    };

public:

                        xthread             ( void );
    virtual            ~xthread             ( void );
    virtual void        Create              ( const char*  pName,
                                              xbool         bStartActive = FALSE,
                                              priority      Priority     = PRIORITY_NORMAL, 
                                              s32           StackSize    = 4*1024, 
                                              xbool         bAutoDelete  = FALSE ); 
            void        Resume              ( void );
            void        Suspend             ( void );
            void        SetPriority         ( xthread::priority Priority );
            void        Sleep               ( s32 Milliseconds );
            void        Destroy             ( void );
    xcritical_section&  GetCriticalSection  ( void ) { return m_CriticalSection; }
    
            u64         GetID               ( void ) const;
    static  u64         FindThreadID        ( void );
            xbool       StillAlife          ( void ) const;
            void        WaitUntillDone      ( void ) const;
    
    static  void*       FindHandle          ( void );
            void        BeginAtomic         ( void );
            void        EndAtomic           ( void );
            xbool       IsAtomic            ( void );

protected:

    virtual void        onInitInstance      ( void );
    virtual void        onExitInstance      ( void );
    virtual void        onRun               ( void ){}

protected:

    xcritical_section       m_CriticalSection;
    xbase_instance          m_XBaseInstance;
    u64                     m_ID;
    void*                   m_Handle;
    xsafe_array<char,64>    m_Name;
    xbool                   m_bAutoDelete:1,
                            m_bInitialize:1;

protected:

#ifdef TARGET_3DS
    friend void             ExecuteFirstCall  ( void* pParam );
#else
    friend s32              ExecuteFirstCall  ( void* pParam );
#endif
    friend xbase_instance&  x_GetXBaseInstance( void );
};

//-------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//-------------------------------------------------------------------------------

class xsignal
{
public:
    
    xsignal( void );
    ~xsignal(void);
    
    void WaitOne( void );
    void Signal( void );
    
#if defined TARGET_OSX || defined TARGET_IOS
    volatile xbool      m_Signal;
    pthread_mutex_t     m_Mutex;
    pthread_cond_t      m_Condition;
#endif

#if defined TARGET_PC || defined TARGET_PC_EDITOR
	void*       m_Handle;
#endif
};

//===============================================================================
// functions
//===============================================================================
void     x_BeginAtomic      ( void );
void     x_EndAtomic        ( void );
xbool    x_IsAtomic         ( void );
xthread& x_GetMainThread    ( void );
xthread& x_GetCurrentThread ( void );
void     x_Sleep            ( s32 Milliseconds );
void     x_Yield            ( void );
s32      x_GetCPUCoreCount  ( void );


