//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is meant to be use as a way to compress a serialization of data.
//     It is most commonly use for saving games or sending packets over the network
//     It uses the an arithmetic range coder to compress the bits together.
//     There is not symbol table use for compression so it is not the most efficient 
//     it could be but it always perform as expected 
//	
//<P>  There are two ways to pack floats each way has its plus and minuses.
//     Try each and see which one you like better. 
//
//<P>  When setting the min/max the range is this [Min,Max]
//
// Example:
//<CODE> 
// void main( void )
// {
//      xbitstream  BitStream;
//      xptr<xbyte> xData;
//      s32         Length;        
//      s32         OriginalNumber = 5;
//      s32         OutputNumber;
//
//      // Pack something
//      BitStream.SetMode( TRUE );
//      BitStream.Serialize( OriginalNumber, 0, 10 );
//      BitStream.GetResult( xData, Length );
//
//      // Unpack something
//      BitStream.SetMode( FALSE );
//      BitStream.SetPackData( xData, Length );
//      BitStream.Serialize( OutputNumber, 0, 10 );
//
//      ASSERT( OutputNumber == OriginalNumber );
// }
//</CODE>                         
// TODO:
//       * Allow user to write raw data
//       * Add 64 bits support
//------------------------------------------------------------------------------
class xbitstream
{
public:

    void                setMode                 ( xbool IsPacking );

    void                Serialize               ( char* pString, s32 MaxChars );

    void                Serialize               ( f32& Float, f32 Min, f32 Max, s32 nBits=15 ){ if( m_bPack ) PackFloat( Float, Min, Max, nBits ); else Float = UnpackFloat( Min, Max, nBits );    }
    void                Serialize               ( f32& Value ) { if( m_bPack ) PackFlags( *(u32*)&Value, 32 ); else *((u32*)&Value) = (u32)UnpackFlags32( 32 ); }

    void                Serialize64             ( u64& Value, u64 Min, u64 Max )    { if( m_bPack ) Pack64( Value, Min, Max );     else Value = Unpack64( Min, Max );     }
    void                Serialize64             ( s64& Value, s64 Min, s64 Max )    { if( m_bPack ) Pack64( Value, Min, Max );     else Value = (s64)Unpack64( Min, Max );     }
    void                Serialize64             ( u64& Value, s32 MaxBits )         { if( m_bPack ) PackFlags( Value, MaxBits ); else Value = UnpackFlags64( MaxBits ); }
    void                Serialize64             ( s64& Value, s32 MaxBits )         { if( m_bPack ) PackFlags( (u64)Value, MaxBits ); else Value = UnpackFlags64( MaxBits ); }

    template< class T >
    void                Serialize               ( T& Value, s32 Min, s32 Max )      { if( m_bPack ) Pack32( Value, Min, Max );     else Value = (T)Unpack32( Min, Max );     }

    template< class T >
    void                Serialize               ( T& Value, s32 MaxBits = sizeof(T)*8 ) { if( m_bPack ) PackFlags( u32(Value), MaxBits ); else Value = (T)UnpackFlags32( MaxBits ); }


    template< class T >
    void                SerializeIn            ( T& Value, s32 Min, s32 Max )      { ASSERT( !m_bPack ); Value = (T)Unpack32( Min, Max ); }

    template< class T >
    void                SerializeIn            ( T& Value, s32 MaxBits = sizeof(T)*8 ) { ASSERT( !m_bPack ); Value = (T)UnpackFlags32( MaxBits );  }

    void                SerializeIn            ( f32& Value ) { ASSERT( !m_bPack ); *((u32*)&Value) = (u32)UnpackFlags32( 32 );  }

    template< class T >
    void                SerializeOut            ( T Value, s32 Min, s32 Max )      { ASSERT( m_bPack ); Pack32( Value, Min, Max ); }

    template< class T >
    void                SerializeOut            ( const T Value, s32 MaxBits = sizeof(T)*8 ) { ASSERT( m_bPack ); PackFlags( u32(Value), MaxBits ); }

    void                SerializeOut            ( f32 Value ) { ASSERT( m_bPack ); PackFlags( *(u32*)&Value, 32 );  }

    template< class T >
    s32                 SerializeOutRLE         ( const T* pData, s32 Count );

    template< class T >
    void                SerializeInRLE          ( T* pData, s32 Count );

    void                setupInitialBufferSize  ( s32 ByteCount );
    void                getResult               ( xptr<xbyte>& xBuffer, s32& Length );
    s32                 getPackedLength         ( void ) const { return m_Cursor; }
    void                PackFloat               ( f32 Value, f32 Min, f32 Max, s32 nBits=15 );
    void                PackFloat               ( f32 Value, xbool bRound, s32 nExponentBits, s32 nMantissaBits );
    void                Pack32                  ( s32 Value, s32 Min, s32 Max );
    void                Pack64                  ( s64 Value, s64 Min, s64 Max );
    void                PackFlags               ( u32 Flags, s32 MaxBits );
    void                PackFlags               ( u64 Flags, s32 MaxBits );
    void                PackQuaternion          ( const xquaternion& Q );

    xbool               HasAnythingBeenPacked   ( void );

    void                setPackData             ( const xptr<xbyte>& xBuffer, s32 Length );
    f32                 UnpackFloat             ( f32 Min, f32 Max, s32 nBits );
    f32                 UnpackFloat             ( s32 nExponentBits, s32 nMantissaBits );
    s32                 Unpack32                ( s32 Min, s32 Max );
    s64                 Unpack64                ( s64 Min, s64 Max );
    u32                 UnpackFlags32           ( s32 MaxBits );
    u64                 UnpackFlags64           ( s32 MaxBits );
    void                UnpackQuaternion        ( xquaternion& Q );
    xbool               HasAnyUnpackDataLeft    ( void );

                        xbitstream              ( void );
                       ~xbitstream              ( void );
protected:

    void                SysPack                 ( u64 Value, s64 Limit );
    void                PackOutputByte          ( u32 Value );
    void                PackOutputBit           ( s32 Value );
    void                PackFlush               ( void );
    void                PackRenormalize         ( void );
    void                PackCarry               ( void );

    u64                 SysUnpack               ( u64 Limit);
    u32                 UnpackNextByte          ( void );
    void                UnpackFlush             ( void );
    void                UnpackRenormalize       ( void );

    void                StartAccess             ( void );
    void                EndAccess               ( void );

protected:

    xbool                   m_bPack;
    u64                     m_Low;
    u64                     m_Range;
    u64                     m_Code;
    s32                     m_Cursor;
    s32                     m_nBytes;
    xptr<xbyte>             m_xBuffer;

    friend class xptr_lock;
};

