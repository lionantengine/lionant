//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This macro allows to have static u64 GUIDs initialize as contants.
//     Its input is an alpha numeric guid (Base36), not a Hex and not a dec value.
//     This is very handy when you know the exact value of the guid and you
//     just want a constant value. It is heavely advice that you assign this value
//     to enumerations rather than assigning it directly to a GUID.
//     Here is an Example on how to use it properly:
//
//     enum my_resources:u64
//     {
//          SPRITE_ATLAS = X_GUID( bp96_ou61_3q4w )
//     };
//
//     xguid Guid( SPRITE_ATLAS );
//
// See Also:
//     xguid
//------------------------------------------------------------------------------
#define X_GUID( GUID_BASE36 )

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class described a 64bit guid. The guid is created using unique from
//     of identifying the user as well as time and other factors. From the point 
//     of view of the xBase this defines a basic type.
// See Also:
//     xghash
//------------------------------------------------------------------------------
struct xguid
{
public:

    u64      m_Guid;

public:

             xguid              ( void );                
             xguid              ( u64 NewGuid );         

    void     setNull            ( void );                
    xbool    IsNull             ( void ) const;
    xbool    isValid            ( void ) const { return !!m_Guid; }
             operator u64       ( void ) const;          
    u32      GetLow             ( void );                
    u32      GetHigh            ( void );                
    xstring  GetAlphaString     ( void ) const;     
    xstring  GetHexString       ( void ) const;     
    void     ResetValue         ( void );                
    void     SetFromHexString   ( const char* pStrGuid );
    void     SetFromAlphaString ( const char* pStrGuid );

protected:

    template<class T> friend class xghash;
    static s32 s_HashTableSize[];
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The templatice class is used to hash xguids. So that for instance if you want to convert
//     a xguid into a pointer you can use this class to do that conversion. It is important to note
//     that you don't want too many instances of this class. Since xguids are unique there is not
//     reason why not to use a single instance to do multiple conversions. Note that the T parameter
//     could be void* for instance. 
//
//<P> The class grows using a prime number table. It has a maximun groth of about 240,000. The class
//    grow by 1000 entris at a time. Note that the classes assumes a low collision average. 
//
// Example:
//<CODE>
//      void main( void )
//      {
//          xghash<void*> GuidTable;
//          void*         pString = "Bla bla bla";
//          xguid         Guid;
//
//          // Creates a nunique guid
//          Guid.ResetValue();
//          
//          // Insert pointer into the hash
//          GuidTable.Add( Guid, pString );
//
//          // lets get back the string
//          ASSERT( GuidTable.GetValue( Guid ) == pString );
//      }
//</CODE>
// See Also:
//     xguid
//------------------------------------------------------------------------------
template<class T>
class xghash
{
public:

                xghash              ( void );
    void        GrowListBy          ( s32 nGuids );
    void        Add                 ( xguid Guid, T Data );
    void        Delete              ( xguid Guid );
    void        SetValue            ( xguid Guid, T Data );
    T           GetValue            ( xguid Guid ) const;
    T           GetValueOrNull      ( xguid Guid ) const;
    xbool       IsAlloc             ( xguid Guid ) const;
    s32         getCount            ( void ) const { return m_Count; }
    
protected:

    struct node
    {
        xguid   m_Guid;           // This is the guid in question
        T       m_Data;           // data associated with the guid
        s32     m_iNext;          // next node in the hash entry
    };

protected:

    s32         GetHashIndex        ( xguid Guid ) const;
    s32         FindNode            ( xguid Guid ) const;
    s32         AllocNode           ( xguid Guid );
    void        FreeNode            ( xguid Guid );

protected:

    s32         m_Count;        // Current number of nodes been use
    xptr<node>  m_lNode;        // List of nodes
    xptr<s32>   m_lHash;        // Hash table
    s32         m_iEmptyList;   // list of empty nodes
};
