//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is used to save and load data files formatted in a particular manner.
//     The format is simple to understand and to look at and it provides the user
//     with tools that makes versions of data easy to maintain and to structure.
//     This file format is best used for generic data such exporters, object properties,
//     etc. Because unlike a serializer it needs to get/set one field at a time.
//     The class is relatively fast and easy with memory which it makes it friendly to
//     use in game code base.
//<P>  Here is a quick list of the features:
//     * Endian support
//     * Binary support
//     * Text support
//     * Handle versions
//     * Handle random fields
//     * Handle comments
//
//     The basics of the file format is simple. It is a table base format where a table can
//     have 1 to n entries. A table is defined by a header which looks like this:
//<CODE>
//     [ TableHeader ]
//</CODE>
//<P>  The header is fallow by the actual structure definition of the data. 
//     The structure of the data is straight forward and fallows very lousy the printf format.
//     here is an example on how a table header fallow by the type structure with some data looks like:
//<CODE>
//     [ TableHeader ]
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//</CODE>
//<P>  There are two kind of headers. One that is meant to have only one data entry like the previous 
//     example and one that is mean to have 1..n entries which looks like this:
//<CODE>
//     [ TableHeader : 2 ]
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//      "jaime" 43    0.6 0.8 0.1
//</CODE>
//<P>  Each column can have one or more types associated with it. As shown in the previous
//     example. Each column has a name fallow by its type as you can see. Here is the table for the types:
//<TABLE>
//     Type    Description
//     ======  -------------------------------------------------------------------
//      f      32 bit float
//      F      64 bit double
//      d      32 bit integer
//      D      64 bit integer
//      c       8 bit integer
//      C      16 bit integer
//      s      this is a string
//      S      undefined (DONT USE)
//      h      32 bit HexInteger     
//      H      undefined (DONT USE)
//      g      64 bit Guid 
//      G      undefined (DONT USE)
//      e      enumerated keyword which is similar to a string but without spaces
//      E      undefined (DONT USE)
//</TABLE>
//<P>  A table has also the ability to redefine the type at each row. This is call dependent types.
//     This dependent types are very useful when dealing with a list of properties of different types,
//     here is an example on how to dependent types looks like:
//<CODE>
//      {  name:s        type:<?>        value:<type>   }
//      //------------   --------------  ----------------
//      "Property one"   <fff>           0.1 0.2 0.3
//      "property two"   <s>             "hello world"
//      "property 3"     <fds>           0.1 2 "3"
//</CODE>
//     We also support dynamic table sizing. This is used when the user wants to hand-edit the files but
//     he does not want to keep counting the rows for his table. In that case you can use the following:
//<CODE>
//     [ TableHeader : ? ]              // Note here how we use a '?' which tells the reader to count automatically
//     { Name:s Age:d Position:fff }
//      "pepe"  32    0.3 0.6 0.2
//      "jaime" 43    0.6 0.8 0.1
//</CODE>
// Example:
//      This is an example of writing one of the text files
//<CODE>
//        xtextfile   TextFile;
//        xstring     FileName( X_STR( "TextFileTest" ));
//
//        TextFile.OpenForWriting ( FileName );
//        TextFile.WriteRecord    ( "FistRecord", 512 );
//        for( s32 i=0; i<512; i++ )
//        {
//            TextFile.WriteField ( "Description:s", "String Test" );
//            TextFile.WriteField ( "Position:fff", x_frand(-100,100), x_frand(-100,100), x_frand(-100,100) );
//            TextFile.WriteField ( "Number:d", x_rand() );
//            TextFile.WriteField ( "EnumParam:e", "TEST1" );
//            TextFile.WriteLine  ();
//        }
//        TextFile.Close();
//</CODE>
//
//      To read the file is also very simple it looks like this:
//<CODE>
//        TextFile.OpenForReading    ( FileName );
//        VERIFY( TextFile.ReadRecord() );
//        for( s32 i=0; i<TextFile.GetRecordCount(); i++ )
//        {
//            xstring String;
//            xstring String2;
//            f32 A, B, C;
//            s32 I;
//            TextFile.ReadLine();
//            TextFile.ReadFieldXString ( "Description:s", String );
//            TextFile.ReadField        ( "Position:fff", &A, &B, &C );
//            TextFile.ReadField        ( "Number:d", &I );
//            TextFile.ReadFieldXString ( "EnumParam:e", String2 );
//        }
//        TextFile.Close();
//</CODE>
//
//     The file format also supports user types. User types always have a '.' in front
//     of their names and they are used to create aliases for atomic types. Just like a structure in 'C'
//     Here is how the file format looks like with them:
//<CODE>
//      // New Types
//      //--------------------------------------------------------------------------------------------------
//      <h>.BOOL <s>.BUTTON <cccc>.COLOR <ddd>.DATE <e>.ENUM <f>.FLOAT <g>.GUID <d>.INT <s>.STRING <fff>.V3
//
//      [ Properties : ? ]
//      { Type:<?>  Name:s                                         Value:<Type> }
//      //--------  ---------------------------------------------  ------------
//          .INT      "Key\EntryList\Count"                               3
//          .ENUM     "Default\Entry\Global\HotpointXMode"             MIDDLE
//          .STRING   "Key\EntryList\Entry[0]\Global\FileName"        "GameData/General/DeamonKingBox/xemB/ani_xemB_move000.png"
//          .INT      "Key\EntryList\Entry[0]\Global\Animation\FPS"      15
//</CODE>
//
// TODO: 
//      * Do a pass and review the use of string vs (char*)
//      * Text formatting.
//      * Can we eliminate the nTypes from the field structure? should we?
//      * Solve and test endian issues
//      * Table names should have the ability to have a unique ID for table references
//         such: [ TableHeader : 2 ] #123123::123123
//=========================================================================
class xtextfile
{
public:

    enum flags:u32
    {
        FLAGS_DEFAULTS      = 0,
        FLAGS_COMPRESS      = X_BIT(0),
        FLAGS_BINARY        = X_BIT(1),
        FLAGS_SWAP_ENDIAN   = X_BIT(2)
    };

public:
    
    static void      TransformTo        ( const xstring& InputFileName, const xstring& OutputFileName, u32 Flags = FLAGS_DEFAULTS );
    
                     xtextfile          ( void );
                    ~xtextfile          ( void );

    void             OpenForReading     ( const xstring& FileName );
    void             OpenForWriting     ( const xstring& FileName, u32 Flags = FLAGS_DEFAULTS );    

    void             ReadFromFile       ( xfile& File );
    void             WriteToFile        ( xfile& File );

    xbool            ReadRecord         ( void );
    s32              ReadField          ( const char* FieldName, ... );
    s32              ReadFieldXString   ( const char* FieldName, xstring& Str );
    void             ReadLine           ( void );
    void             ReadNextRecord     ( void );

    s32              TryReadRecord      ( void );
    s32              TryReadLine        ( void );
    s32              TryReadNextRecord  ( void );

    s32              GetNumberFields    ( void ) const;
    void             GetFieldDesc       ( s32 Index, xstring& String ) const;
    void             GetFieldName       ( s32 Index, xstring& String ) const;
    const char*      GetFieldType       ( s32 Index ) const;
    u8               getUserTypeUID     ( s32 Index ) const;

    static xbool     isAtomicString     ( char Type );
    static s32       GetAtomicSize      ( char Type );
    const xstring&   GetRecordName      ( void ) const;
    s32              GetRecordCount     ( void ) const;
    void             Close              ( void );

    s32              AddUserType        ( const char* pSystemTypes, const char* pUserType, u8 UID );
    void             WriteRecord        ( const char* pHeaderName, s32 Count = -1 );
    void             WriteField         ( const char* FieldName, ... );
    void             WriteFieldIndirect ( const char* FieldName, ... );

    void             WriteFieldV3       ( const char* FieldName, const xvector3d&   V );
    void             WriteFieldV4       ( const char* FieldName, const xvector4&    V );
    void             WriteFieldQ        ( const char* FieldName, const xquaternion& Q );
    void             WriteFieldR3       ( const char* FieldName, const xradian3&    R );
    void             WriteFieldColor    ( const char* FieldName, const xcolor&      C );

    void             WriteComment       ( const xstring& Comment );
    void             WriteLine          ( void );

//=========================================================================
protected:

    enum 
    {
        MAX_TYPES_PER_FIELD = 32
    };

    struct record_info
    {
        xstring                                 m_Name;
        s32                                     m_Count;
        xbool                                   m_bWriteCount;
    };

    struct field
    {
        xstring                                 m_Type;             // Field name such "jack:ff"
        s32                                     m_nTypes;           // Number of types for this field
        s32                                     m_TypeOffset;       // This is the offset to the string which gives the type ex:"fff"
        xsafe_array<s32,MAX_TYPES_PER_FIELD>    m_iMemory;          // The location in the memory buffer for each of these fields    
        s32                                     m_iFieldDecl;       //  * When this is -1 means this is a normal field type
                                                                    //  * When this is -2 this means is a declaration
                                                                    //  * When is positive then it is a "pepe:<test>" and
                                                                    //  this has an index to the field which contain the type declaration
        u32                                     m_Width;            // What is the width in character for this field. This is only used when writting.
        u16                                     m_TypesPreWidth;
        u16                                     m_TypesPostWidth;
        xsafe_array<u8,MAX_TYPES_PER_FIELD>     m_TypeWidth;        // This is the width that each column has per type
        xsafe_array<u8,MAX_TYPES_PER_FIELD>     m_FracWidth;        // This is the additional offset for floats/ints so that the integer part can align
        s32                                     m_DynamicTypeCount; // This is just used for debugging but its intent is a type count for dynamic types
        u16                                     m_iUserType=0xffff; // A place to put the user type
    };

    struct user
    {
        s32                                     m_iFieldType;
        s32                                     m_iFieldData;
    };
    
    struct user_type
    {
        xstring                                 m_UserType;
        xstring                                 m_SystemType;
        xbool                                   m_bSaved;
        s32                                     m_nSystemTypes;
        u8                                      m_UID;
        
                user_type   ( void )                        {}
                user_type   ( const xstring& Str )          { m_UserType = Str; }
                user_type   ( const char* pStr )            { m_UserType.Copy( pStr ); }
        xbool   operator <  ( const user_type& Type ) const { return x_strcmp( &m_UserType[0], &Type.m_UserType[0] ) < 0; }
    };

//=========================================================================
protected:

    void        BuildTypeInformation    ( const char* FieldName );
    void        WriteField              ( const char*& FieldName, xva_list& Args1, xva_list& Args2, xbool bIndirect );
    void        WriteComponent          ( s32 iField, s32 iType, s32 c, xva_list& Args, xbool bIndirect );
    void        WriteUserTypes          ( void );
    s32         ReadWhiteSpace          ( xbool bReturnError = FALSE );
    xbool       IsFloat                 ( s32 Char );
    s32         ReadComponent           ( s32 Type );
    xbool       IsValidType             ( s32 Type );
    s32         HandleDynamicTable      ( void );

//=========================================================================
protected:

    xfile*                  m_pFile;                // File pointer
    record_info             m_Record;               // This countains information about the current record
    s32                     m_iLine;                // Which line we are in the current record
    s32                     m_iField;               // Current Field that we are trying to read
    s32                     m_nTypesLine;           // Number of types per line

    xarray<user>            m_User;                 // This is used only when readind
    xarray<field>           m_Field;                // Contain a list of field names and their types
    xarray<user_type>       m_UserTypes;            // List of types created by the user
    
    xarray<s16>             m_BinReadUserTypeMap;   // This is an optimization done when loading binary files using usertypes
                                                    // this tables have indicex to types that were loaded from the file rather
                                                    // than added by the user. This table is always shorted as well.

    s32                     m_iCurrentOffet;        // Index to the next location ready to be allocated
    xarray<char>            m_Memory;               // Buffer where the fields are store (Only one line worth)

    xbool                   m_bReading:1,           // Tells the system whether we are reading or writting
                            m_bBinary:1,            // Tells the system whether we are dealing with a text file or binary
                            m_bEndianSwap:1,        // Tells whether we should be doing engian swapping
                            m_bXStrings:1,          // Tells to read strings as xstring rather than char*
                            m_bNewFile:1;           // Tells whether we have allocated the file pointer
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The xseialfile class is design for binary resources files. It is design
//     to be super fast loading, super memory efficient and very friendly to the user.
//     The class will save the data in the final format for the machine. The loading 
//     structure layouts should be identical to the saving structures. That allows the 
//     class to save and load in place without the need of any loading function from
//     the user. Note that if we ever move to windows 64bits we will have to solved the 
//     case where pointers sizes will be different from the consoles. 
//
//<P>  One of the requirements is that the user provides a SerializeIO function per structure 
//     that needs saving. There are certain cases where you can avoid this thought, check the example.
//     Having the function allows this class to recurse across the hierarchy of the user classes/structures/buffer and arrays.
//     Ones the class has finish loading there will be only one pointer that it is return which contains a 
//     the pointer of the main structure. The hold thing will have been allocated as a
//     single block of memory. This means that the pointer return is the only pointer that
//     needs to be deleted.
//
//<P>  Loading is design to be broken up into 3 stages. The Loading The Header which load minimun information
//     about resource, load object which loads the file into memory and finally resolve which calls an specific 
//     constructor which allows the object to deal with any special cases and talking to other systems.
//     The reason for it is that the constructor will be call. This constructor is a special
//     one which accepts a xserialfile structure. Here the user may want to register data with 
//     ingame managers such vram managers, or animation managers, etc. By having that executed in the
//     main thread relaxes the constrains of thread safety without sacrificing much performance at all.
//     
//<P>  There are two kinds of data the can be save/load. Unique and non-unique. When the user selects
//     pointers to be unique the system will allocate that memory as a separate buffer. Any pointer
//     which does not specify unique will be group as a single allocation. This allows the system to
//     be very efficient with memory allocations. Additionaly to the Unique Flag you can also set the
//     vram flag. The system recognices two kinds these two types of ram, but the only things that it does
//     internally with this flag is to separate non-unique memory into these two groups. When deleting the 
//     object all memory mark as non-unique doesn't need to be free up but everything mark as unique does
//     need to be free up by the user. This can happen in the destructor of the main structure. 
//
//<P>  There is only 2 types of functions to save your data. Serialize and SerializeEnum. Serialize is use
//     to safe atomic types as well as arrays and pointers. SerializeEnum is design to work for enumerations.
//     Note that endian issues will be automatically resolve as long as you use those two functions. If the 
//     user decides to save a blob of data he will need to deal with endian swaping. Please use the SwapEndian 
//     function to determine weather the system is swaping endians. When saving the data you dont need to 
//     worry about saving the data members in order. The class take care of that internally. 
// 
//<P>  Dealingss with 64 vs 32 bit pointers. For now there will be two sets of crunchers the one set
//     will compile data with 64 bits this are specifially for the PC. The others will be compiled for
//     32bits and there for the pointer sizes will remain 32. It is possible however that down the road
//     we may want to compile crunchers in 64 bits yet output 32 pointers. In this case there are two 
//     solution and both relay on a macro that looks like this: X_EXPTR( type, name) This macro will have 
//     to be used when ever the user wants to declare a pointer inside an structure. So in solution
//     one it that the pointers will remain 64 even in the target machine. The macro will create an 
//     additional 32bit dummy variable in the strucuture. In the other solution the macro will contain 
//     a smart pointer for 64 bits enviroments. That smart pointer class will have a global array of real
//     pointers where it will allocate its entries. 
//
//<P><B>Physical File layout in disk</B>
//<CODE>
//                          +----------------+      <-+
//                          | File Header    |        | File header is never allocated.
//                          +----------------+ <-+  <-+
//                          | BlockSizes +   |   |
//                          | PointerInfo +  |   |  This is temporary allocated and it gets deleted 
//                          | PackInfo       |   |  before the LoadObject function returns.
//                          |                |   |
//                          +----------------+ <-+  <-+ 
//                          |                |        | Here are a list of blocks which contain the real
//                          | Blocks         |        | data that the user saved. Blocks are compress
//                          |                |        | by the system and decompress at load time.
//                          |                |        | The system will call a user function to allocate the memory.
//                          +----------------+      <-+
//
//</CODE>
// Example:
//<CODE>
//    struct data1
//    {
//        void SerializeIO( xserialfile& SerialFile ) const
//        {
//            SerialFile.Serialize( m_A );
//        }
//        s16     m_A;
//    };
//    
//    struct data3
//    {
//        void SerializeIO( xserialfile& SerialFile ) const
//        {
//            SerialFile.Serialize( m_Count );
//            SerialFile.Serialize( m_pData, m_Count );
//        }
//
//        s32     m_Count;
//        data1*  m_pData;
//    };
//
//    struct data2 : public data1
//    {
//        // data
//        data3   m_GoInStatic;
//        data3   m_DontDynamic;
//    
//        enum
//        {
//            DYNAMIC_COUNT = 100,
//            STATIC_COUNT  = 100,
//            STATIC_MAX    = 0xffffffff
//        };
//    
//       // Initialize all the data
//        data2( void )
//        {
//            m_A = 100;
//    
//            m_DontDynamic.m_Count = DYNAMIC_COUNT;
//            m_DontDynamic.m_pData = (data1*)x_malloc( sizeof(data1), m_DontDynamic.m_Count, 0 );
//            for( s32 i=0; i<m_DontDynamic.m_Count; i++ ) { m_DontDynamic.m_pData[i].m_A = 22+i; }
//    
//            m_GoInStatic.m_Count = STATIC_COUNT;
//            m_GoInStatic.m_pData = (data1*)x_malloc( sizeof(data1), m_GoInStatic.m_Count, 0 );
//            for( s32 i=0; i<m_GoInStatic.m_Count; i++ ) { m_GoInStatic.m_pData[i].m_A = 23+i; }
//        }
//    
//        // Save the data
//        void SerializeIO( xserialfile& SerialFile ) const
//        {
//            // Make sure that it is the first version
//            SerialFile.SetResourceVersion( 1 );
//
//            // Tell the structure to save it self
//            SerialFile.Serialize( m_GoInStatic );
//    
//            // Don't always need to go into structures
//            SerialFile.Serialize ( m_DontDynamic.m_Count );
//            SerialFile.Serialize( m_DontDynamic.m_pData, m_DontDynamic.m_Count, xserialfile::FLAGS_UNIQUE );
//    
//            // Tell our parent to save it self
//            data1::SerializeIO( SerialFile );
//        }
//    
//        // This is the loading constructor by the time is call the file already loaded
//        data2( xserialfile& SerialFile )
//        {
//            ASSERT( SerialFile.GetResourceVersion() == 1 );
//    
//            // *** Only reason to have a something inside this constructor is to deal with dynamic data
//            // We move the memory to some other random place
//            data1*  pData = (data1*)x_malloc( sizeof(data1),m_DontDynamic.m_Count, 0);
//            x_memcpy( pData, m_DontDynamic.m_pData, m_DontDynamic.m_Count*sizeof(data1) );
//    
//            // Now we can overwrite the dynamic pointer without a worry
//            x_free(m_DontDynamic.m_pData);
//            m_DontDynamic.m_pData = pData;
//        }
//    
//        ~data2( void )
//        {
//            // Here to deal with dynamic stuff 
//            if(m_DontDynamic.m_pData) x_free( m_DontDynamic.m_pData );
//        }
//    };
//      
//    void main( void )
//    {
//        xstring     FileName( X_STR("SerialFile.bin") );
//
//        // Save
//        {
//            xserialfile SerialFile;
//            data2     TheData;
//            TheData.SanityCheck();
//            SerialFile.Save( FileName, TheData );
//        }
//    
//        // Load
//        {
//            xserialfile  SerialFile;
//            data2*     pTheData;
//    
//            // This hold thing could happen in one thread
//            SerialFile.Load( FileName, pTheData );
//    
//            pTheData->SanityCheck();
//    
//            // Okay one pointer to nuck
//            x_delete( pTheData );
//        }
//    }
//</CODE>
//------------------------------------------------------------------------------
class xserialfile
{
public:

    template< class T >
    union ptr
    {
        u64     m_Pad;
        T*      m_Ptr;
                    
                    ptr         ( void ) = default;
                    explicit ptr( T* p ) : m_Ptr(p) { }
        T*          operator -> ( void )            { return m_Ptr; }
        const T*    operator -> ( void ) const      { return m_Ptr; }
        ptr<T>&     operator =  ( T* pPtr )         { m_Ptr = pPtr; return *this; }
        xbool       isValid     ( void ) const      { return NULL != m_Ptr; }
        void        setNull     ( void )            { m_Ptr = NULL; }
    };
    
    enum flags:u32
    {
        FLAGS_DEFAULT  = 0,                     // -> Uses the default which is all the off options
                                                // 
        FLAGS_TEMP_MEMORY = X_BIT(2),           // -> TODO: this is memory that will be freed after the object constructor returns
                                                //          However you can overwrite this functionality by taking owership of the temp pointer.
                                                //          The good thing of using this memory type is that multiple allocations are combine into a single one.
                                                //          This flag will override the UNIQUE and VRAM flags, they are exclusive.
                                                //
        FLAGS_UNIQUE   = X_BIT(0),              // -> On  - Unique is memory is that allocated by it self and there for could be free
                                                //    Off - Common memory which can't be freed for the duration of the object.
        FLAGS_VRAM     = X_BIT(1),              // -> On  - This memory is to be allocated in vram if the hardware has it. 
                                                //    Off - Main system memory.
        FLAGS_PAD      = 0xffffffff
    };

    typedef void* allocate_memory_fn( s32 Size, flags Flags );

public:

                                xserialfile                 ( void );
    template< class T > void    Serialize                   ( const T& A );
template< class T, s32 C > void SerializeEnum               ( const xsafe_array<T, C>& A );
    template< class T > void    SerializeEnum               ( const T& A );
    template< class T > void    SerializeEnum               ( const T& A, s32 Count, flags MemoryFlags = FLAGS_DEFAULT );
    template< class T > void    Serialize                   ( const T& A, s32 Count, flags MemoryFlags = FLAGS_DEFAULT );
    template< class T > void    Serialize                   ( const ptr<T>& A, s32 Count, flags MemoryFlags = FLAGS_DEFAULT );
    template< class T > void    Serialize                   ( const ptr<const T>& A, s32 Count, flags MemoryFlags = FLAGS_DEFAULT );
template< class T, s32 C > void Serialize                   ( const xsafe_array<T,C>& A );

    template< class T > void    Save                        ( const char* pFileName, const T& Object, flags ObjectFlags=FLAGS_DEFAULT, xbool bSwapEndian = FALSE );
    template< class T > void    Save                        ( xfile& File, const T& Object, flags ObjectFlags, xbool bSwapEndian = FALSE );

    template< class T > void    Load                        ( xfile& File, T*& pObject );
    template< class T > void    Load                        ( const char* pFileName, T*& pObject );

    void                        LoadHeader                  ( xfile& File, s32 SizeOfT );
    void*                       LoadObject                  ( xfile& File );
    template< class T > void    ResolveObject               ( T*& pObject );

    void                        SetResourceVersion          ( u16 ResourceVersion );
    void                        SetSwapEndian               ( xbool SwapEndian );

    xbool                       SwapEndian                  ( void ) const;
    u16                         GetResourceVersion          ( void ) const;

    void                        SetAllocateMemoryCallBack   ( allocate_memory_fn Function );

    void                        DontFreeTempData            ( void ) { m_bFreeTempData = FALSE; }
    void*                       getTempData                 ( void ) const { ASSERT( m_bFreeTempData == FALSE );  return m_pTempBlockData; }

protected:

    enum version
    {
        VERSION_ID          = X_BIN( 00010111 ),
        MAX_BLOCK_SIZE      = 1024*127            
    };
    
    struct decompress_block
    {
        xbyte Buff[xserialfile::MAX_BLOCK_SIZE];
    };

    struct ref
    {
        s32                     m_OffSet;               // Byte offset where the pointer lives
        s32                     m_Count;                // Count of entries that this pointer is pointing to
        s32                     m_PointingAT;           // What part of the file is this pointer pointing to
        u16                     m_OffsetPack;           // Offset pack where the pointer is located
        u16                     m_PointingATPack;       // Pack location where we are pointing to
    };

    struct pack
    {
        flags                   m_PackFlags;            // Flags which tells what type of memory this pack is            
        s32                     m_UncompressSize;       // How big is this pack uncompress
    };

    struct pack_writting : public pack
    {
        xfile                   m_Data;                 // raw Data for this block
        s32                     m_BlockSize;            // size of the block for compression this pack
        s32                     m_CompressSize;         // How big is this pack compress
        xptr<u8>                m_CompressData;         // Data in compress form
    };

    struct writting
    {
        s32                     AllocatePack    ( flags DefaultPackFlags );

        xarray<s32>             m_CSizeStream;          // a in order List of compress sizes for packs and blocks
        xarray<ref>             m_PointerTable;         // Table of all the pointer written
        xarray<pack_writting>   m_Packs;                // Freeable memory + VRam/Core
        xbool                   m_bEndian;
        xfile*                  m_pFile;
    };

    struct header
    {
        u16                     m_SerialFileVersion;    // Version generated by this system
        u16                     m_PackSize;             // Pack size
        s32                     m_SizeOfData;           // Size of this hold data in disk excluding header
        s16                     m_nPointers;            // How big is the table with pointers
        s16                     m_nPacks;               // How many packs does it contain
        s16                     m_nBlockSizes;          // How many block sizes do we have
        u16                     m_ResourceVersion;      // User version of this data
        u16                     m_MaxQualities;         // Maximun number of qualities for this resource
        u16                     m_AutomaticVersion;     // The size of the main structure as a simple version of the file
    };

protected:

    template< class T > void    Handle              ( const T& A );
    template< class T > void    Array               ( const T& A, s32 Count );
    template< class T > void    HandlePtr           ( const T& A, s32 Count, flags MemoryFlags );

    void                        SaveFile            ( void );
    xfile&                      GetW                ( void ) const;
    xfile&                      GetTable            ( void ) const;
    xbool                       IsLocalVariable     ( u8* pRange );
    s32                         ComputeLocalOffset  ( u8* pItem );
    void                        HandlePtrDetails    ( u8* pA, s32 SizeofA, s32 Count, flags MemoryFlags );

    void                        Handle              ( const char& A );
    void                        Handle              ( const s8&   A );
    void                        Handle              ( const s16&  A );
    void                        Handle              ( const s32&  A );
    void                        Handle              ( const s64&  A );

    void                        Handle              ( const u8&   A );
    void                        Handle              ( const u16&  A );
    void                        Handle              ( const u32&  A );
    void                        Handle              ( const u64&  A );

    void                        Handle              ( const f32&  A );
    void                        Handle              ( const f64&  A );

    void                        Handle              ( const xmatrix4&    A );
    void                        Handle              ( const xvector3&    A );
    void                        Handle              ( const xvector3d&   A );
    void                        Handle              ( const xbbox&       A );
    void                        Handle              ( const xcolor&      A );
    void                        Handle              ( const xvector2&    A );
    void                        Handle              ( const xvector4&    A );
    void                        Handle              ( const xquaternion& A );

    void                        Handle              ( const xguid&       A );

protected:

    // non stack base variables for writting
    writting*           m_pWrite;                       // Static data for writting

    // Stack base variables for writting
    s32                 m_iPack;
    s32                 m_ClassPos;
    u8*                 m_pClass;
    s32                 m_ClassSize;

    // Loading data
    header              m_Header;                       // Header of the resource
    allocate_memory_fn* m_pMemoryCallback;              // Callback
    void*               m_pTempBlockData    = NULL;     // This is data that was saved with the flag temp_data
    xbool               m_bFreeTempData     = TRUE;
};

