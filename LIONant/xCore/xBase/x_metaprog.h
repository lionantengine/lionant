//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

// Use the CREATE_UNIQUE_NAME to create a unique name per line __COUNTER__ may also work
// You can create unique variables for instance
#define __X_CREATE_UNIQUE_VARNAME_I(A,B) A##B
#define __X_CREATE_UNIQUE_VARNAME(A,B)  __X_CREATE_UNIQUE_VARNAME_I(A,B) 
#define X_CREATE_UNIQUE_VARNAME( BASE ) __X_CREATE_UNIQUE_VARNAME( BASE, __LINE__ )

// Usefull metaprogramming macros to remove references from types and just leave the type
template<class T> struct x_unref     { typedef T type; };
template<class T> struct x_unref<T&> { typedef T type; };

// the line will be comment it out
#define xattribute  / ## /


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_has_vtable_var - These macros are used to determine if a variable or type has a virtual table
//     x_has_vtable_type
// Arguments:
//------------------------------------------------------------------------------
template< class T >
struct _xhasvtable
{
    //using  type = typename T;
    struct derived : T   { virtual void force_v( void ) {} };
    enum { VALUE = sizeof( T ) == sizeof( derived ) };
};
#define x_has_vtable_var(variable)  _xhasvtable<x_unref<decltype(variable)>::type>::VALUE 
#define x_has_vtable_type(atype)    _xhasvtable<atype>::VALUE 
