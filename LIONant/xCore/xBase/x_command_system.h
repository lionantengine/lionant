//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

struct x_undo_step
{
                x_undo_step ( void );
               ~x_undo_step ( void );

    void        Clear       ( void );

    s32         m_iStep;
    xstring     m_Command;
    xfile       m_UndoStateFile;
    xfile       m_RedoStateFile;
};

//------------------------------------------------------------------------------

class x_undo_system
{
public:
        
                   ~x_undo_system   ( void );

    void            Init            ( s32 nMaxStepCount, const char* pFilePath );
    void            Kill            ( void );
    void            Reset           ( void );
    xbool           CanUndo         ( s32 nSteps = 1 ) const;
    xbool           CanRedo         ( s32 nSteps = 1 ) const;
    x_undo_step&    UndoStep        ( void );
    x_undo_step&    RedoStep        ( void );
    x_undo_step&    GetNewUndoStep  ( void );

protected:

                    x_undo_system   ( void );

    s32             GetPrev         ( void ) const;
    s32             GetNext         ( void ) const;
    xstring         GetUndoFileName ( s32 iStep ) const;
    xstring         GetRedoFileName ( s32 iStep ) const;
    void            CreateFiles     ( x_undo_step& Step );
    void            DeleteFiles     ( x_undo_step& Step );
    void            ResetStep       ( x_undo_step& Step );

protected:

    xptr<x_undo_step>   m_Stack;
    s32                 m_iCurrent;
    xstring             m_FilePath;

    friend class x_command_system;
};

//------------------------------------------------------------------------------

class x_base_command
{
public:

                            x_base_command  ( void );
    virtual                ~x_base_command  ( void );

    virtual xbool           ValidateCommand ( const char* pCmdline )    = 0;
    virtual xbool           ExecuteCommand  ( x_undo_step& Step )       = 0;
    virtual xbool           LoadUndoState   ( xfile& File )             = 0;
    virtual xbool           LoadRedoState   ( xfile& File )             = 0;
    virtual const char*     GetHelp         ( void ) const              = 0;
    virtual const char*     GetName         ( void ) const              = 0;
};

//------------------------------------------------------------------------------

class x_command_system
{
public:

                            x_command_system    ( void );
                           ~x_command_system    ( void );
    
    virtual void            Init                (  s32 UndoSteps, const char* pUndoPath );
    virtual void            Kill                ( void );
    virtual void            Reset               ( void );
    virtual xbool           RegisterCommand     ( x_base_command* pCommand );
    virtual xbool           ExecuteCommand      ( const char* pCmdline );
    virtual xbool           Undo                ( void );
    virtual xbool           Redo                ( void );
    virtual xbool           CanUndo             ( void ) const;
    virtual xbool           CanRedo             ( void ) const;

public:

    static void             InitUndoSystem      ( s32 nUndoSize, const char* pUndoPath );
    
protected:

    virtual xbool           IsCommandRegistered ( const x_base_command* pCommand ) const;
    virtual x_base_command* FindCommand         ( const char* pName ) const;
    virtual xbool           IsHelpCommand       ( const char* pName ) const;
    virtual xbool           ExecuteHelpCommand  ( const char* pCmdline ) const;

protected:

    xarray<x_base_command*>     m_CommandArray;
    x_undo_system               m_UndoSystem;

    static s32                  m_nUndoSystemSize;
    static xstring              m_pUndoSystemPath;
};
