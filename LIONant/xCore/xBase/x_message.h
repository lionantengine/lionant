//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//@@Message system
//
//    <u>EXAMPLE OF MESSAGE:</u>
//<P> A message is a structure that get pass to all subscribed receiver classes.  
//    Each message has one member variable call m_SubID which is used to add 
//    context to a message such a unique ID of a particular gadget such a button.
//    Each message must have a Unique identifier, done by using X_MSG_UA macro.
//    It is encourage for messages to have constructors that will set all their variables
//    that way the message can be declare inline with the BroadcastMessage function. 
//<CODE>
//    struct button_press : public xmsg_base
//    {
//        X_MSG_UA( button_press, xmsg_base );            // Declaration of the unique identifier
//
//        button_press( int SubID, bool State ) : xmsg_base(SubID), m_State(State) {}
//
//        xbool m_State;
//    };
//</CODE>
//<P> <u>EXAMPLE OF MESSAGE MAP AND RECEIVER:</u>
//<P> The message map can be place either in the header or in the CPP file. 
//    The message map works very similar to the Microsoft MFC one except that it doest
//    use a switch and stead of IDs in the macro we place the type of the message. 
//    When placing the map in the CPP you must set the X_MESSAGE_MAP_DECL macro in the header.
//    Note that you don t have to use a receiver if you never want to receive indirect messages. 
//    If you only want to deal with direct messages you can always you pMyObject->SendXMessage( MyMessage );
//<CODE>
//    struct myclass2 : public xmsg_receiver                         // This class can receive indirect messages
//    {
//        X_MESSAGE_MAP_DECL                                        // Declaration of the message table
//
//        myclass2( void ) 
//        {
//              m_Button.AddReceiver( *this );                       // Similar to C# += 
//        }
//
//        void OnMsgButtonB( const button_press& Msg )
//        {
//            s32 a=5;
//        }
//
//        void OnAnyMsgButton( const button_press& Msg )
//        {
//            s32 a=5;
//        }
//      
//        button    m_Button;
//    };
//
//    -------------------------- CPP File -------------------------
//    X_BEGIN_MESSAGE_MAP( myclass2 )                               // Message declaration table
//    X_MESSAGE( button_press, OnMsgButtonB, 1 )                    // 1 is the button sub_id (when not id needed set to 0)
//    X_MESSAGE_RANGE( button_press, OnAnyMsgButton, 2, 200 )       // 2 to 200 is the range of buttons sub_ids
//    X_END_MESSAGE_MAP
//</CODE>
//
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is the base class where all messages should be base on. Unlike MFC here each
//     message is base from a base class, and it doesn't require any enumeration. 
//
//<P>  Now all messages are synchronously so when the user calls BroadcastMessage it results in 
//     the proper function calls right away.
//
//<P>  The base class has one member variables m_SubID; Usually represents a unique identifier 
//     for the sender such a button id. 
//
// Example:
//<CODE>
//    struct button_press : public xmsg_base
//    {
//        X_MSG_UA( button_press, xmsg_base );                                 // Declaration of the unique identifier
//
//        button_press( int SubID, bool State ) : xmsg_base(SubID), m_State(State) {}
//
//        xbool m_State;
//    };
//</CODE>
//
// See Also:
//     xmsg_receiver xmsg_broadcast_channel <LINK Message system>
//------------------------------------------------------------------------------
class xmsg_base
{
public:
    x_rtti_base( xmsg_base )

    virtual u64     GetMessageUA    ( void ) const =0;    
    inline          xmsg_base       ( void ) : m_SubID( 0 ){}
    inline          xmsg_base       ( u64 SubID ) : m_SubID( SubID ){}

public:

    u64             m_SubID;
};
    
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The message receiver class is design to be used as a base class. It just adds
//     the ability for a class to received broadcast messages. The class is in charged to subscribe/add
//     it self to any "channels" that wants to received messages from.  
//
// <P> After you have use this class as the base class you can create the message 
//     map for this class that will handle different messages similar to MFC.
//
//    Some of the implementation details may help understand the system flexibility with this end 
//    we will talk about it. First in how the subscription works and how it is handle internally.
//<CODE>
//                          +---------------------+  +---------------------+  +---------------------+
//                          |  Receiver Channel   |  |  Receiver Channel   |  |  Receiver Channel   |
//                          +----------+----------+  +----------+----------+  +----------+----------+
//                                     |                        |                        |
//    +------------+                   |                        |                        |
//    | Receiver   |>-----------------(@)----------------------(@)-----------------------+
//    +------------+                   |                        |                        |
//    +------------+                   |                        |                        |
//    | Receiver   |>------------------+-----------------------(@)----------------------(@)
//    +------------+                   |                        |                        |
//    +------------+                   |                        |                        |
//    | Receiver   |>-----------------(@)----------------------(@)----------------------(@)
//    +------------+                   |                        |                        |
//    +------------+                   |                        |                        |
//    | Receiver   |>-----------------(@)-----------------------+------------------------+
//    +------------+                   |                        |                        |
//    +------------+                   |                        |                        |
//    | Receiver   |>------------------+------------------------+-----------------------(@)
//    +------------+
//</CODE>
//    As you can see in the previous figure each receiver can add themselves into a broadcasting channels.
//    the broadcasting channels in turn will route messages to the proper receivers.
//  
//    Receivers can also add themselves to other receivers. This make the receivers into channels as well.
//    An example of this can a window. A window may contain several buttons which they depend on the 
//    window to send messages to. Now the window may contain other windows as well with other buttons
//    so the hold system needs to be recursive. 
//
//    For most part SendXMessage means the standard MFC SendMessage to call the functions that 
//    are associated with the message in question. BroadcastMessage means to actually broadcast something in that reciver.
//    So anyone subscrive to that reciver should get the message. 
//    Notify means to call the channels that you are subscribed to (other receivers) and send them a message.
//    
// Example:
// <CODE>
//    struct myclass2 : public xmsg_receiver                         // This class can receive indirect messages
//    {
//        X_MESSAGE_MAP_DECL                                        // Declaration of the message table
//
//        myclass2( void ) 
//        {
//              m_Button.AddReceiver( *this );                       // Similar to C# += 
//        }
//
//        void OnMsgButtonB( const button_press& Msg )
//        {
//            s32 a=5;
//        }
//
//        void OnAnyMsgButton( const button_press& Msg )
//        {
//            s32 a=5;
//        }
//      
//        button    m_Button;
//    };
//
//    -------------------------- CPP File -------------------------
//    X_BEGIN_MESSAGE_MAP( myclass2 )                               // Message declaration table
//    X_MESSAGE( button_press, OnMsgButtonB, 1 )                    // 1 is the button id (when not id needed set to 0)
//    X_MESSAGE_RANGE( button_press, OnAnyMsgButton, 2, 200 )       // 2 to 200 is the range of buttons id
//    X_END_MESSAGE_MAP
//</CODE>
//
// See Also:
//     xmsg_base xmsg_broadcast_channel <LINK Message system>
//
// TODO:
// 
//    * Will be nice if the system knew from which channel a message come from so that
//       for instance we could notify that channel of something. 
//    * The notify function name may need to be change to broadcast-to-subscribed-channels
//    * What do we do about multithead systems and the message system?
// 
//------------------------------------------------------------------------------
class xmsg_broadcast_channel;

class xmsg_receiver
{
public:

    virtual void    SendXMessage     ( const xmsg_base& ) {}
    void            NotifySender     ( const xmsg_base& Msg );
    void            BroadcastMessage ( const xmsg_base& Msg );
    void            AddReceiver      ( xmsg_receiver& Receiver );
    void            RemoveReceiver   ( xmsg_receiver& Receiver );

protected:
                    xmsg_receiver     ( void );
    virtual        ~xmsg_receiver     ( void );

private:

    u16             m_iChannel;
    u16             m_iType;

    friend class xmsg_base;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Broadcast Channels are a way to create common message distributions.
//     Such when you want to have a private way to broadcast messages.
//     That way people who subscribed to a particular instantiation of a broadcast channel
//     will be able to received messages about that particular subject from anyone. 
//
//<P>  To send messages throw a broadcast channel you can call the BroadcastMesssage
//     function of your receiver or you can use the broadcaster BroadcastMessage function.
//
// Example:
// <CODE>
//      xmsg_broadcast_channel s_MyChannel;
//
//      void Bla( void )
//      {
//          s_MyChannel.BroadcastMessage( my_message( 123 ) );
//      }
// </CODE>
//------------------------------------------------------------------------------

class xmsg_broadcast_channel : public xmsg_receiver
{
public:
                    xmsg_broadcast_channel  ( void );

protected:

    virtual void    SendXMessage            ( const xmsg_base& ) {}
};

//------------------------------------------------------------------------------

// <COMBINE xmsg_base>
#define X_MSG_UA(A,B)                                 

// <COMBINE xmsg_base>
#define X_MSG_UAS(A)

// <COMBINE xmsg_base>
//#define X_MSG_UAT(A,B,T)

// <COMBINE xmsg_receiver>
#define X_BEGIN_MESSAGE_MAP( classname, baseclass )

// <COMBINE xmsg_receiver>
#define X_MESSAGE_RANGE( type, function, subid, range )

// <COMBINE xmsg_receiver>
#define X_MESSAGE( type, function, subid )

// <COMBINE xmsg_receiver>
#define X_MESSAGE_ANY( type, function )

// <COMBINE xmsg_receiver>
#define X_END_MESSAGE_MAP 

// <COMBINE xmsg_receiver>
#define X_MESSAGE_MAP_DECL
