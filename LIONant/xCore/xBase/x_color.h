//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a color. The color has only 8 bits of precision so
//     there is not HDR support at this point. It is very handy for converting 
//     amount different color spaces, or color layouts. This class also servers
//     as a helper to other classes such the xbitmap.
// See Also:
//     xbitmap
//------------------------------------------------------------------------------
class xcolor
{
public:

    // FORMAT_(LOW BITS elements first then moving to HIGH BITS)
    enum format
    {
        FMT_NULL,           

        FMT_16_ABGR_4444,
        FMT_16_ARGB_4444,   
        FMT_16_RGBA_4444,   
        FMT_16_RGB_565  ,
        FMT_16_BGR_565  ,
        FMT_16_ARGB_1555,  
        FMT_16_RGBA_5551,  
        FMT_16_URGB_1555,  
        FMT_16_RGBU_5551,   
        FMT_16_ABGR_1555,   
        FMT_16_UBGR_1555,

        FMT_24_RGB_888  ,     
        FMT_24_ARGB_8565,   

        FMT_32_RGBU_8888,   
        FMT_32_URGB_8888,  
        FMT_32_ARGB_8888,
        FMT_32_RGBA_8888,  // THIS IS THE COLOR FORMAL
        FMT_XCOLOR = FMT_32_RGBA_8888,
        FMT_32_ABGR_8888,   
        FMT_32_BGRA_8888,       

        FMT_END,

        FMT_PADDING     = 0xffffffff
    };

    struct fmt_desc
    {
        format      m_Format; 
        u32         m_FormatMask;     // One unique bit out of the 32
        s32         m_TB;             // Total Bits( 16, 24, 32 )
        s32         m_NUB;            // Number of Used Bits( 15, 16, 24, 32 )
        s32         m_AShift;        
        s32         m_RShift;        
        s32         m_GShift;        
        s32         m_BShift;        
        u32         m_AMask;         
        u32         m_RMask;         
        u32         m_GMask;         
        u32         m_BMask;         
    };

public:

    union
    {
        struct { u8  m_R, m_G, m_B, m_A; };
        u32 m_Color;
    };

public:
                        xcolor              ( void );
                        xcolor              ( const u32 K );
                        xcolor              ( const xvector3d& C );
                        xcolor              ( const xvector4& C );
                        xcolor              ( u8 r, u8 g, u8 b, u8 a );
    void                Set                 ( u8 r, u8 g, u8 b, u8 a );
    void                Set                 ( const u32 K );

    // Access the color in different forms

    unsigned char&      operator[]          ( int Index );
    operator            u32                 ();
    inline const xcolor&
                        operator =          ( u32 C );
    const xcolor&       operator +=         ( xcolor C );
    const xcolor&       operator -=         ( xcolor C );
    const xcolor&       operator *=         ( xcolor C );
    xbool               operator ==         ( xcolor C ) const;
    xbool               operator !=         ( xcolor C ) const;


    // Color data conversions
    void                BuildColorFromData  ( u32 Data, format DataFormat );
    u32                 BuildDataFromColor  ( format DataFormat ) const;
    static format       FindClosestFormat   ( u32 FormatMask, format Match );
    static format       FindFormat          ( u32 AMask, u32 RMask, u32 GMask, u32 BMask  );
    xcolor&             BlendColors         ( const xcolor& Src1, const xcolor& Src2, f32 t);
    xcolor              ComputeNewAlpha     ( f32 Alpha ) const;
    static const fmt_desc& getFormatDesc    ( format Dest ) { return g_FormatDesc[Dest]; }
    
    // Color space conversions. 
    void                BuildYIQ            ( f32&  Y, f32&  I, f32&  Q ) const;
    void                SetFromYIQ          ( f32   Y, f32   I, f32   Q );
    void                BuildYUV            ( f32&  Y, f32&  U, f32&  V ) const;
    void                SetFromYUV          ( f32   Y, f32   U, f32   V );
    void                BuildCIE            ( f32&  C, f32&  I, f32&  E ) const;
    void                SetFromCIE          ( f32   C, f32   I, f32   E );
    void                BuildRGB            ( f32& aR, f32& aG, f32& aB ) const;
    xvector3            BuildRGB            ( void ) const;
    void                SetFromRGB          ( xvector3d& Vector );
    void                SetFromRGB          ( f32  aR, f32  aG, f32  aB );
    void                BuildRGBA           ( f32& aR, f32& aG, f32& aB, f32& aA ) const;
    xvector4            BuildRGBA           ( void ) const;
    inline void         SetFromRGBA         ( const xvector4& C );
    inline void         SetFromRGBA         ( f32  aR, f32  aG, f32  aB, f32  aA );
    void                BuildCMY            ( f32&  C, f32&  M, f32&  Y ) const;
    void                SetFromCMY          ( f32   C, f32   M, f32   Y );
    void                BuildHSV            ( f32&  H, f32&  S, f32&  V ) const;
    void                SetFromHSV          ( f32   H, f32   S, f32   V );
    void                SetFromLight        ( const xvector3d& LightDir );
    xvector3            BuildLight          ( void ) const;
    void                SetFromNormal       ( const xvector3d& LightDir );
    xvector3            BuildNormal         ( void ) const;

//==============================================================================
// PROTECTED !! PROTECTED !! PROTECTED !! PROTECTED !! PROTECTED !! 
//==============================================================================
protected:

    struct best_match
    {
        format m_Format[FMT_END];
    };

protected:

    static const fmt_desc     g_FormatDesc[];
    static const best_match   g_Match[];
};
