//==============================================================================
// INCLUDES
//==============================================================================
#include <stdlib.h>
#include <stdio.h>
#include "../x_Base.h"

//==============================================================================
// Types
//==============================================================================
//DOM-IGNORE-BEGIN
typedef xsafe_array<char, 1024*5> fs_buffer;

struct string_local
{
    fs_buffer   m_FSBuffer;
    s32         m_FSIndex;
};

//==============================================================================
// Functions
//==============================================================================

//------------------------------------------------------------------------------

static
string_local& GetStringLocal( void )
{
    xbase_instance& Instance = x_GetXBaseInstance();
    ASSERT( Instance.m_pString );
    return *((string_local*)Instance.m_pString);
}

//DOM-IGNORE-END
//==============================================================================
//==============================================================================
//==============================================================================
// ANSI functions
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN
static s32 _x_dtoa( u64 Val, char* pBuff, int SizeOfBuff, int Base, bool bHasNegative )
{
    char* p      = pBuff;
    s32   Length = 0;  
    s32   i      = 0;

    ASSERT( pBuff );
    ASSERT( Base > 2 );
    ASSERT( Base <= (26+26+10) );
    ASSERT( SizeOfBuff > 1 );

    if( bHasNegative )
    {
        *p++ = '-';
        i++;
        Length++;
    }

    do 
    {
        u32 CVal = (u32)(Val % Base);
        Val   /= Base;

        // convert to ascii and store 
        if ( CVal < 10 )
        {
            *p = ( CVal + '0' );
            p++;
        }
        else if( CVal < (10+26) )
        {
            *p = ( CVal - 10 + 'a' );
            p++;
        }
        else if( CVal < (10+26+26) )
        {
            *p = ( CVal - 10 - 26 + 'A' );
            p++;
        }
        else
        {
            // The base is too big
            ASSERT( 0 );
        }

        Length++;
        ASSERT( Length < SizeOfBuff );

    } while( Val > 0 && Length < SizeOfBuff );

    // Check if we run out of space
    if ( Length >= SizeOfBuff)
    {
        pBuff[0]=0;
        return 0;
    }

    //  terminate string; 
    *p = 0;

    // Reverse string order
    for( p--; p > &pBuff[i] ; i++, p-- )
    {
        x_Swap( *p, pBuff[i] );
    }

    return Length;
}
//DOM-IGNORE-END

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_dtoa    - Converts value to a string in whatever base is past
// Arguments:
//      Val             - Number to be converted
//	    pStr	        - Destination String 
//      SizeOfString    - Size of the destination string
//	    Base	        - Numeric base that we are converting to.
// Returns:
//	    Length of the final string
// Description:
//      Converts any 32 bit number into a string which contains the number 
//      represented in a particular base. The base for instance could be 16
//      for hex numbers.
// See Also:
//     x_atod64 x_atod32 x_atoi32 x_atoi64 x_atof32 x_atof64
//------------------------------------------------------------------------------
s32 x_dtoa( s32 Val, char* pStr, int SizeOfString, int Base )
{
    if( Val < 0 )
    {
        return _x_dtoa( u64(u32(-Val)), pStr, SizeOfString, Base, TRUE );
    }
    else
    {
        return _x_dtoa( u64(Val), pStr, SizeOfString, Base, FALSE );
    }
}

// <COMBINE x_dtoa >
s32 x_dtoa( u32 Val, char* pStr, int SizeOfString, int Base )
{
    return _x_dtoa( u64(Val), pStr, SizeOfString, Base, FALSE );
}

// <COMBINE x_dtoa >
s32 x_dtoa( s64 Val, char* pStr, int SizeOfString, int Base )
{
    if( Val < 0 )
    {
        return  _x_dtoa( u64(-Val), pStr, SizeOfString, Base, TRUE );
    }
    else
    {
        return  _x_dtoa( u64(Val), pStr, SizeOfString, Base, FALSE );
    }
}

// <COMBINE x_dtoa >
s32 x_dtoa( u64 Val, char* pStr, int SizeOfString, int Base )
{
    return _x_dtoa( u64(Val), pStr, SizeOfString, Base, FALSE );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_dtoa    - Converts a string value to integer base from a particular
// Arguments:
//	    pStr	        - Source string encoded with a particular base
//	    Base	        - Base of the numeric string
// Returns:
//	    Actual integer number
// Description:
//      Converts a string that has been encoded into an integer of a particular base
//      into a actual atomic integer of a particular size (32vs64)bits. If the
//      string contains '_' or ':' characters they will be ignore.
// See Also:
//     x_atod64 x_atod32 x_atoi32 x_atoi64 x_atof32 x_atof64
//------------------------------------------------------------------------------
s64 x_atod64( const char* pStr, s32 Base )
{
    char C;         // Current character.
    char Sign;      // If '-', then negative, otherwise positive.
    s64  Total=0;   // Current total.
    s32  ValidBase;

    ASSERT( pStr );
    ASSERT( Base > 2 );
    ASSERT( Base <= (26+26+10) );

    // Skip whitespace.
    for( ; *pStr == ' '; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == '-') || (C == '+') )
    {
        C = *pStr++;
    }

    // Decode the rest of the string
    for( ; ; C = *pStr++ )
    {
        if( (C >= '0') && (C <= '9') )  
        {
            ValidBase = C - '0';
        }
        else if( (C >= 'a') && (C <= 'z') )  
        {
            ValidBase = C - 'a' + 10;
        }
        else if( (C >= 'A') && (C <= 'Z') )  
        {
            ValidBase = C - 'A' + 10+26;
        }
        else if( C == '_' || C == ':' )  
        {
            // Ignore
            continue;
        }
        else
        {
            // Negate the total if negative.
            if( Sign == '-' ) 
                Total = -Total;

            // Any other character is bad news
            return Total;
        }

        ASSERT( ValidBase >= 0 );
        ASSERT( ValidBase <  Base );

        // Accumulate digit.
        Total = (Base * Total) + ValidBase;
    }

    // Not way to get here
    ASSERT( 0 );
}

// <COMBINE x_atod64 >
s32 x_atod32( const char* pStr, s32 Base )
{
    return (s32)x_atod64(pStr, Base );
}


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_strHash - perform a 32 bit Fowler/Noll/Vo hash on a string
// Arguments:
//	    pStr	 - string to hash
//	    hval	 - previous hash value or nothing if first call
// Returns:
//	    32 bit hash as a static hash type
// Description:
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
u32 x_strHash( const char* pStr, u32 Range, u32 hVal )
{
    u8* s = (u8*)pStr;	// unsigned string 

    //
    // FNV-1 hash each octet in the buffer
    //
    while(*s) 
    {
	    //.. multiply by the 32 bit FNV magic prime mod 2^32 
    	hVal *= 0x01000193;

	    // xor the bottom with the current octet 
	    hVal ^= (s32)*s++;
    }

    // Do we need to compute a different range?
    if( Range != 0xffffffff )
    {
        const u32 RetryLevel = (0xffffffff/Range)*Range;
        while( hVal >= RetryLevel ) 
        {
            hVal = (hVal * ((u32)16777619UL)) + ((u32)2166136261UL);
        }
        hVal %= Range;
    }

    // return our new hash value 
    return hVal;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_strHash - perform a 32 bit Fowler/Noll/Vo hash on a string
// Arguments:
//	    pStr	 - string to hash
//	    hval	 - previous hash value or nothing if first call
// Returns:
//	    32 bit hash as a static hash type
// Description:
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
u32 x_strIHash ( const char* pStr, u32 Range, u32 hVal )
{
    u8* s = (u8*)pStr;	// unsigned string 

    //
    // FNV-1 hash each octet in the buffer
    //
    while(*s) 
    {
	    //.. multiply by the 32 bit FNV magic prime mod 2^32 
    	hVal *= 0x01000193;

	    // xor the bottom with the current octet 
	    hVal ^= (s32)x_tolower(*s);

        // move to next character
        s++;
    }

    // Do we need to compute a different range?
    if( Range != 0xffffffff )
    {
        const u32 RetryLevel = (0xffffffff/Range)*Range;
        while( hVal >= RetryLevel ) 
        {
            hVal = (hVal * ((u32)16777619UL)) + ((u32)2166136261UL);
        }
        hVal %= Range;
    }

    // return our new hash value 
    return hVal;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_strIHash64 - perform a 64 bit Fowler/Noll/Vo hash on a string
// Arguments:
//	    pStr	 - string to hash
//	    hval	 - previous hash value or nothing if first call
// Returns:
//	    32 bit hash as a static hash type
// Description:
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
u64 x_strIHash64( const char* pStr, u64 hVal )
{
    unsigned char *s = (unsigned char *)pStr;	/* unsigned string */
    
    //
    // FNV-1 hash each octet of the string
    //
    while( *s )
    {
        // multiply by the 64 bit FNV magic prime mod 2^64
        hVal *= ((u64)0x100000001b3ULL);
        
        // xor the bottom with the current octet
        hVal ^= (u64)x_tolower(*s);
        
        // move to next character
        s++;
    }
    
    // return our new hash value
    return hVal;
}

//------------------------------------------------------------------------------

u32 x_strCRC( const char* pStr, u32 crcSum )
{
    s32 Length = x_strlen( pStr );
    return x_memCRC32( pStr, Length, crcSum );
}

//------------------------------------------------------------------------------

char* x_strcpy( char* pDest, s32 MaxChar, const char* pSrc )
{
    ASSERT( pDest );
    ASSERT( pSrc  );
    ASSERT( MaxChar > 0 );
    
    MaxChar--;
    s32 i;
    for( i=0; (pDest[i] = pSrc[i]) && (i < MaxChar); i++ );
    
#ifdef X_DEBUG
    for( ; i < MaxChar; i++ ) pDest[i] = 0;
#endif
    
    pDest[i] = 0;
    
    return pDest;
}

//------------------------------------------------------------------------------

s32 x_strlen( const char* pStr )
{
    const char *pEnd = pStr;

    ASSERT( pStr );

    while( *pEnd++ ) {}

    return (s32)(pEnd - pStr - 1);
}

//------------------------------------------------------------------------------

s32 x_strcmp( const char* pStr1, const char* pStr2 )
{
    s32 Result = 0;

    ASSERT( pStr1 );
    ASSERT( pStr2 );

    if( pStr1 == pStr2 ) return 0;

    while( !(Result = ((s32)*pStr1) - ((s32)*pStr2)) && *pStr1 )
        ++pStr1, ++pStr2;

    return Result;
}

//------------------------------------------------------------------------------

s32 x_stricmp( const char* pStr1, const char* pStr2 )
{
    s32 f, l;

    ASSERT( pStr1 );
    ASSERT( pStr2 );

    if( pStr1 == pStr2 ) return 0;

    do
    {
        if( ((l = (s32)(*(pStr1++))) >= 'A') && (l <= 'Z') )
            l -= ('A' - 'a');

        if( ((f = (s32)(*(pStr2++))) >= 'A') && (f <= 'Z') )
            f -= ('A' - 'a');

    } while( f && (f == l) );

    return l - f ;
}

//------------------------------------------------------------------------------

char* x_strstr( const char* pMainStr, const char* pSubStr )
{
    char* pM = (char*)pMainStr;
    char* pS1; 
    char* pS2;

    if( !*pSubStr )
        return( (char*)pMainStr );

    while( *pM )
    {
        pS1 = pM;
        pS2 = (char*)pSubStr;

        while( *pS1 && *pS2 && !(*pS1 - *pS2) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return( pM );

        pM++;
    }

    return NULL ;
}

//------------------------------------------------------------------------------

char* x_stristr( const char* pMainStr, const char* pSubStr )
{
    char* pM = (char*)pMainStr;
    char* pS1; 
    char* pS2;

    if( !*pSubStr )
        return( (char*)pMainStr );

    while( *pM )
    {
        pS1 = pM;
        pS2 = (char*)pSubStr;

        while( *pS1 && *pS2 && !(x_tolower(*pS1) - x_tolower(*pS2)) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return( pM );

        pM++;
    }

    return NULL ;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Converts a strings to a numeric value
// Arguments:
//	    pStr	 - String to be converted. 
// Returns:
//      Each function returns the value produced by interpreting the input characters as a number. 
//      The return value is 0 if the input cannot be converted to a value of that type. 
//      The return value is undefined in case of overflow.
// Description:
//      These functions convert a character string to a double-precision, floating-point value 
//      (atof64 and atof32), an integer, long integer value (atoi32, atoi64).
//
//<P>   The input string is a sequence of characters that can be interpreted as a numerical value 
//      of the specified type. The function stops reading the input string at the first character 
//      that it cannot recognize as part of a number. This character may be the null character 
//      ('\\0' or L'\\0') terminating the string.
//
//<P>   The string argument to atof32 and atof64 has the following form:
//
//<P>   [whitespace] [sign] [digits] [.digits] [ {d | D | e | E }[sign]digits]
//
//<P>   A whitespace consists of space or tab characters, which are ignored; sign is either plus (+) or minus (?; 
//      and digits are one or more decimal digits. If no digits appear before the decimal point, at least one must 
//      appear after the decimal point. The decimal digits may be followed by an exponent, which consists 
//      of an introductory letter (d, D, e, or E) and an optionally signed decimal integer.
//
//<P>   atoi32, and atoi64 do not recognize decimal points or exponents. 
//      The string argument for these functions has the form:
//
//<P>   [whitespace] [sign]digits
//
//<P>   where whitespace, sign, and digits are as described for atof32 and atof64.
//
// Example:
// <CODE>
//      s32 main( void )
//      {
//         char *s; f64 x; s32 i; s64 li;
//
//          // Test of atof64
//          s = "  -2309.12E-15";     
//          x = x_atof64( s );
//          x_printf( "atof64 test: \\"%s\\"; float:  %e\\n", s, x );
//
//          // Test of atof32 
//          s = "7.8912654773d210";  
//          x = x_atof32( s );
//          x_printf( "atof32 test: \\"%s\\"; float:  %e\\n", s, x );
//
//          // Test of atoi 
//          s = "  -9885 pigs";      
//          i = x_atoi32( s );
//          x_printf( "atoi32 test: \\"%s\\"; integer: %d\\n", s, i );
//
//          // Test of atol 
//          s = "98854 dollars";     
//          li = x_atoi64( s );
//          x_printf( "atoi64 test: \\"%s\\"; long: %Ld\\n", s, li );
//      }
// </CODE>
//
// <TABLE>
//      The Output
//      -----------------------------------------------------
//      atof64 test: "  -2309.12E-15"; float:  -2.309120e-012
//      atof32 test: "7.8912654773d210"; float:  7.891265e+210
//      atoi32 test: "  -9885 pigs"; integer: -9885
//      atoi64 test: "98854 dollars"; long: 98854
// </TABLE>
// See Also:
//     x_sprintf
//------------------------------------------------------------------------------
s32 x_atoi32( const char* pStr )
{
    char C;         // Current character.
    char Sign;      // If '-', then negative, otherwise positive.
    s32  Total;     // Current total.

    ASSERT( pStr );

    // Skip whitespace.
    for( ; *pStr == ' '; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == '-') || (C == '+') )
    {
        C = *pStr++;
    }

    Total = 0;

    while( (C >= '0') && (C <= '9') )
    {
        // Accumulate digit.
        Total = (10 * Total) + (C - '0');

        // Get next char.
        C = *pStr++;
    }

    // Negate the total if negative.
    if( Sign == '-' ) 
        Total = -Total;

    return Total;
}

//------------------------------------------------------------------------------
// <COMBINE x_atoi32 >
s64 x_atoi64( const char* pStr )
{
    char C;         // Current character.
    char Sign;      // If '-', then negative, otherwise positive.
    s64  Total;     // Current total.

    ASSERT( pStr );

    // Skip whitespace.
    for( ; *pStr == ' '; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == '-') || (C == '+') )
    {
        C = *pStr++;
    }

    Total = 0;

    while( (C >= '0') && (C <= '9') )
    {
        // Accumulate digit.
        Total = (10 * Total) + (C - '0');

        // Get next char.
        C = *pStr++;
    }

    // Negate the total if negative.
    if( Sign == '-' ) 
        Total = -Total;

    return Total;
}

//------------------------------------------------------------------------------

xbool x_isstrint( const char* pStr )
{
    if( pStr[0] == '-' )
    {
        if( pStr[1] == 0 )
            return FALSE;

        for( s32 i=1; pStr[i]; i++ )
        {
            if( pStr[i] < '0' || pStr[i] > '9' ) return FALSE;
        }
    }
    else
    {
        for( s32 i=0; pStr[i]; i++ )
        {
            if( pStr[i] < '0' || pStr[i] > '9' ) return FALSE;
        }

    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_isstrfloat( const char* pStr )
{
    static const char*  pF = "Ee.#QNABIF";

    // Does it have any other of the strange characters?
    if( pStr[0] == '-' )
    {
        if ( pStr[1] == 0 )
            return FALSE;

        for( s32 i=1 ; pStr[i]; i++ )
        {
            s32 l;

            if( pStr[i] >= '0' && pStr[i] <= '9' ) 
                continue;

            for( l=0; (pStr[i]!=pF[l]) && pF[l]; l++ );

            // Okay this is not longer a number
            if( pF[l] == 0 ) return FALSE;
        }
    }
    else
    {
        for( s32 i=0 ; pStr[i]; i++ )
        {
            s32 l;

            if( pStr[i] >= '0' && pStr[i] <= '9' ) 
                continue;

            for( l=0; (pStr[i]!=pF[l]) && pF[l]; l++ );

            // Okay this is not longer a number
            if( pF[l] == 0 ) return FALSE;
        }
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_isstrhex( const char* pStr )
{
    for( s32 i=0; pStr[i]; i++ )
    {
        if( pStr[i] >= '0' && pStr[i] <= '9' ) continue;
        if( pStr[i] >= 'A' && pStr[i] <= 'F' ) continue;
        if( pStr[i] >= 'a' && pStr[i] <= 'f' ) continue;
        return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_isstrguid( const char* pStr )
{
    s32 nCol=0;
	s32 i;
    for( i=0; pStr[i]; i++ )
    {
        if( pStr[i] >= '0' && pStr[i] <= '9' ) continue;
        if( pStr[i] >= 'A' && pStr[i] <= 'F' ) continue;
        if( pStr[i] >= 'a' && pStr[i] <= 'f' ) continue;
        if( pStr[i] == ':' ) 
        {
            nCol++;
            if( nCol > 1 ) return FALSE;
            continue;
        }
        return FALSE;
    }

    // the length of the string must be 17 characters long (16+1 for the':')
    if( i != 17 ) return FALSE;

    return TRUE;
}


//------------------------------------------------------------------------------
// simple_pow
//------------------------------------------------------------------------------
static
f32 simple_pow( f32 x, s32 p )
{
    f32 r;

    if (p == 0) return 1.0;
    if (x == 0.0) return 0.0;

    if (p < 0)
    {
        p = -p;
        x = 1.0f / x;
    }

    r = 1.0;
    for(;;)
    {
        if (p & 1) r *= x;
        if ((p >>= 1) == 0) return r;
        x *= x;
    }
}

//------------------------------------------------------------------------------
// This implementation will handle scientific notation such as "1.23e+7".
// <COMBINE x_atoi32 >
f32 x_atof32( const char* pStr )
{
    s32   ISign   = 1;    // Integer portion sign.
    s32   IValue  = 0;    // Integer portion value.
    s32   DValue  = 0;    // Decimal portion numerator.
    s32   DDenom  = 1;    // Decimal portion denominator.
    xbool Integer = TRUE; // Is it an integer?

    ASSERT( pStr );

    // Skip whitespace.
    for( ; *pStr == ' '; ++pStr )
        ; // empty body

    // Get the sign.
    if( *pStr == '-' )  { pStr++; ISign = -1; }
    if( *pStr == '+' )  { pStr++;             }

    // Handle integer portion.  Accumulate integer digits.
    while( (*pStr >= '0') && (*pStr <= '9') )
    {
        IValue = (IValue<<3) + (IValue<<1) + (*pStr-'0');
        pStr++;
    }

    // Handle decimal portion.
    if( *pStr == '.' )
    {
        // Its not an integer any more!
        Integer = FALSE;

        // Skip the decimal point.
        pStr++;

        // Accumulate decimal digits.
        while( (*pStr >= '0') && (*pStr <= '9') )
        {
            DValue = (DValue<<3) + (DValue<<1) + (*pStr-'0');
            DDenom = (DDenom<<3) + (DDenom<<1);
            pStr++;
        }
    }

    // Handle scientific notation.
    if( (*pStr == 'e') || (*pStr == 'E') || (*pStr == 'd') || (*pStr == 'D') )
    {
        s32  EValue  = 0;   // exponent as an integer
        s32  ESign   = 1;   // Exponent portion sign.
        f32  e;             // Exponent order.

        // Skip the 'e' (or 'd').
        pStr++;

        // Get the sign for the exponent.
        if( *pStr == '-' )  { pStr++; ESign = -1; }
        if( *pStr == '+' )  { pStr++;             }

        // Accumulate exponent digits.
        while( (*pStr >= '0') && ( *pStr <= '9' ) )
        {
            EValue = (EValue<<3) + (EValue<<1) + (*pStr-'0');
            pStr++;
        }

        // Compute exponent.
        static const f32 PowTable[]= {1.0f,10.0f,10e1f,10e2f,10e3f,10e4f,10e5f,10e6f,10e7f,10e8f,10e9f,10e10f,10e11f,10e12f,10e13f,10e14f,10e15f,10e16f,10e17f,10e18f,10e19f,10e20f,10e21f,10e22f,10e23f };
        if( EValue < 23 )
        {
            e = PowTable[EValue];
        }
        else
        {
            e = simple_pow( 10, EValue );
        }

        // Return final number for exponent case.
        {
            f32 Temp = ISign * (IValue + ((f32)DValue / (f32)DDenom));

            if( ESign < 0 )  return( Temp / e );
            else             return( Temp * e );
        }
    }

    // No exponent
    if( Integer ) return( (f32)(ISign * IValue) );
    else          return( ISign * (IValue + ((f32)DValue/(f32)DDenom)) );
}

//------------------------------------------------------------------------------
// <COMBINE x_atoi32 >
f64 x_atof64( const char* pStr )
{
   return ::atof( pStr );
}

//------------------------------------------------------------------------------

void* x_memchr( void* pBuf, s32 C, s32 Count )
{
    xbyte* p;
    xbyte  c;

    ASSERT( pBuf );
    ASSERT( Count >= 0 );

    p = (xbyte*)pBuf;
    c = (xbyte)C;

    while( Count && (*p != c) )
    {
        p++;
        Count--;
    }

    return Count ? (void*)p : NULL;
}

//------------------------------------------------------------------------------

char* x_strcat( char* pFront, s32 MaxChars, const char* pBack )
{
    char* p = pFront;

    ASSERT( pFront );
    ASSERT( pBack  );

    while( *p ) 
    {
        p++;
    }
    ASSERT( MaxChars > (s32)(p - pFront) );

    while( (*p++ = *pBack++) )
    {
        ASSERT( MaxChars > (s32)(p - pFront) );
    }

    ASSERT( MaxChars > (s32)(p - pFront) );

    return pFront;
}

//------------------------------------------------------------------------------
// Assumption: Letters A-Z and a-z are contiguous in the character set.
// This is true for ASCII and UniCode.  (Not so for EBCDIC!)

char* x_strtoupper( char* pStr )
{
    char* p = pStr;

    ASSERT( pStr );

    while( *p != '\0' )
    {
        if( (*p >= 'a') && (*p <= 'z') )
            *p += ('A' - 'a');
        p++;
    }

    return pStr ;
}

//------------------------------------------------------------------------------

s32 x_strncmp( const char* pStr1, const char* pStr2, s32 Count )
{
    ASSERT( pStr1 );
    ASSERT( pStr2 );
    ASSERT( Count >= 0 );
    
    if( !Count )
        return( 0 );
    
    while( --Count && *pStr1 && (*pStr1 == *pStr2) )
    {
        pStr1++;
        pStr2++;
    }
    
    return ((s32)*pStr1) - ((s32)*pStr2);
}

//------------------------------------------------------------------------------
// Assumption: Letters A-Z and a-z are contiguous in the character set.
// This is true for ASCII and UniCode.  (Not so for EBCDIC!)

char* x_strtolower( char* pStr )
{
    char* p = pStr;

    ASSERT( pStr );

    while( *p != '\0' )
    {
        if( (*p >= 'A') && (*p <= 'Z') )
            *p += ('a' - 'A');
        p++;
    }

    return pStr ;
}

//------------------------------------------------------------------------------

char* x_strncpy( char* pDest, const char* pSrc, s32 Count, s32 DestSize )
{
    ASSERT( pDest );
    ASSERT( pSrc  );
    ASSERT( Count >= 0 );
    ASSERT( Count < DestSize );

    pDest[Count--] = 0;
    while( Count >= 0 )
    {
        pDest[Count] = pSrc[Count];
        Count--;
    }
    
    /*
    char* pStart = pDest;
     
    while( Count && (*pDest++ = *pSrc++) )
        Count--;
    
    if( Count )
    {
        while( --Count )
            *pDest++ = '\0';
    }
     */
    
    return pDest;
}

//------------------------------------------------------------------------------

void x_splitpath( const char* pPath,
                 char* pDrive,
                 char* pDir,
                 char* pFName,
                 char* pExt )
{
    char* p;
    char* pLastSlash = NULL;
    char* pLastDot   = NULL;
    s32   Len;
    
    //
    // Extract drive letter and ':', if any.
    //
    
    if( (x_strlen(pPath) >= xfile::MAX_DRIVE-2) && (pPath[xfile::MAX_DRIVE-2] == ':') )
    {
        if( pDrive != NULL )
        {
            x_strncpy( pDrive, pPath, xfile::MAX_DRIVE-1, xfile::MAX_DRIVE );
            pDrive[xfile::MAX_DRIVE-1] = '\0';
        }
        pPath += xfile::MAX_DRIVE-1;
    }
    else
        if( pDrive )
        {
            // No drive.
            pDrive[0] = '\0';
        }
    
    //
    // Extract path string, if any.  pPath now points to first character of the
    // path, if present, or the filename or even the extension.  Look ahead for
    // the last path separator ('\' or '/').  If none is found, there is no
    // path.  Also note the location of the last '.', if any, to assist in
    // isolating the extension later.
    //
    
    for( p = (char*)pPath; *p; ++p )
    {
        if( (*p == '/') || (*p == '\\') )
            pLastSlash = p+1;
        else
            if( *p == '.' )
                pLastDot = p;
    }
    
    if( pLastSlash != NULL )
    {
        // There is a path.  Copy up to last slash or X_MAX_DIR chars.
        if( pDir != NULL )
        {
            Len = x_Min( s32(pLastSlash - pPath), xfile::MAX_DIR-1 );
            x_strncpy( pDir, pPath, Len, xfile::MAX_DIR );
            pDir[Len] = '\0';
        }
        pPath = pLastSlash;
    }
    else
        if ( pDir != NULL )
        {
            // No path.
            pDir[0] = '\0';
        }
    
    //
    // Extract file name and extension, if any.  pPath now points to the first
    // character of the file name, if any, or to the extension, if any.
    // pLastDot points to the beginning of the extension, if any.
    //
    
    if( (pLastDot != NULL) && (pLastDot >= pPath) )
    {
        // Found the marker for the extension.  Copy the file name through to
        // the '.' marker.
        
        if( pFName != NULL )
        {
            Len = x_Min( s32(pLastDot - pPath), xfile::MAX_FNAME-1 );
            x_strncpy( pFName, pPath, Len, xfile::MAX_FNAME );
            pFName[Len] = '\0';
        }
        
        // Now get the extension.  Note that p still points to the NULL which
        // terminates the path.
        
        if( pExt != NULL )
        {
            Len = x_Min( s32(p - pLastDot), xfile::MAX_EXT-1 );
            x_strncpy( pExt, pLastDot, Len, xfile::MAX_EXT );
            pExt[Len] = '\0';
        }
    }
    else
    {
        if( pFName != NULL )
        {
            Len = x_Min<s32>( s32(p - pPath), xfile::MAX_FNAME );
            x_strncpy( pFName, pPath, Len, xfile::MAX_FNAME );
            pFName[Len] = '\0';
        }
        if ( pExt != NULL )
        {
            pExt[0] = '\0';
        }
    }
}

//------------------------------------------------------------------------------
// Make path behavior based on Microsoft's implementation.

void x_makepath( char* pPath, const char* pDrive,
                const char* pDir,
                const char* pFName,
                const char* pExt )
{
    const char* p;
    
    // Copy the drive specification, if given.
    
    if( pDrive && *pDrive )
    {
        *pPath++ = *pDrive;
        *pPath++ = ':';
    }
    
    // Copy the directory specification, if given.
    
    if( pDir && *pDir )
    {
        p = pDir;
        do
        {
            *pPath++ = *p++;
        } while( *p );
        
        p--;
        if( (*p != '/') && (*p != '\\') )
        {
            *pPath++ = '\\';
        }
    }
    
    // Copy the file name, if given.
    
    if( pFName )
    {
        p = pFName;
        while( *p )
        {
            *pPath++ = *p++;
        }
    }
    
    // Copy the extension, if given.  Add the '.' marker if not given.
    
    if( pExt )
    {
        p = pExt;
        if( *p && (*p != '.') )
        {
            *pPath++ = '.';
        }
        while( *p )
        {
            *pPath++ = *p++;
        }
    }
    
    // Terminate the string.
    *pPath = '\0';
}

//==============================================================================
//==============================================================================
//==============================================================================
// XFS / XVFS
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------
static
void x_InitFS( void )
{
    string_local& Local = GetStringLocal();
    Local.m_FSIndex = 0;
}

//------------------------------------------------------------------------------
static
char* FS_Alloc( const char* pFormatString, xva_list Args )
{
    string_local& Local       = GetStringLocal();
    char*         pString     = &Local.m_FSBuffer[Local.m_FSIndex];

    //
    // Allocate the string
    // 
    Local.m_FSIndex += 1 + x_vsprintf( pString, Local.m_FSBuffer.getCount(), pFormatString, Args );
    ASSERT( Local.m_FSIndex < Local.m_FSBuffer.getCount() );

    return( pString );
}

//------------------------------------------------------------------------------
static
void FS_Release( const char* pString )
{
    string_local& Local    = GetStringLocal();
    s32           NewIndex = Local.m_FSIndex - (s32)(&Local.m_FSBuffer[Local.m_FSIndex] - pString);

    ASSERT( NewIndex >= 0 );
    ASSERT( NewIndex < Local.m_FSIndex );

    Local.m_FSIndex = NewIndex;
}

//------------------------------------------------------------------------------

s32 xvfs::GetLength( void ) const
{
    return x_strlen( m_pString );
}

//------------------------------------------------------------------------------

xvfs::xvfs( const char* pFormatString, xva_list Args )
{
    ASSERT( pFormatString );
    m_pString = FS_Alloc( pFormatString, Args );
}

//------------------------------------------------------------------------------

xvfs::operator const char* ( void ) const
{
    return( m_pString );
}

//------------------------------------------------------------------------------

xvfs::~xvfs( void )
{
    FS_Release( m_pString );
}

//------------------------------------------------------------------------------

xfs::xfs( const char* pFormatString, ... )
{
    ASSERT( pFormatString );

    xva_list    Args;
    x_va_start( Args, pFormatString );

    m_pString = FS_Alloc( pFormatString, Args );
}

//------------------------------------------------------------------------------

s32 xfs::GetLength( void ) const
{
    return x_strlen( m_pString );
}

//------------------------------------------------------------------------------

xfs::operator const char* ( void ) const
{
    return m_pString ;
}

//------------------------------------------------------------------------------

xfs::~xfs( void )
{
    FS_Release( m_pString );
}

//==============================================================================
//==============================================================================
//==============================================================================
// XSTRING LOCAL
//==============================================================================
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN
//------------------------------------------------------------------------------

struct xstring_global 
{
    xblock_array< xsafe_array<char,32+4>,  256>   m_Pool32;
    xblock_array< xsafe_array<char,64+4>,  128>   m_Pool64;
    xblock_array< xsafe_array<char,128+4>,  64>   m_Pool128;
    xblock_array< xsafe_array<char,256+4>,  32>   m_Pool256;
    xcritical_section                             m_CriticalSection;
};

static xstring_global* s_pXStr=NULL;

//------------------------------------------------------------------------------
char* xAllocString( s32 Length )
{
    char* pAlloc = NULL;
    u8    Flags  = xstring::FLAGS_NON_SYSTEM|xstring::FLAGS_CAN_GROW;

    //*** We have added more bytes into the buffers them selves
    // One for the null the other in case of double null
    //Length += 2;

    // For the flags
    //Length += 1;

    // Also we added a reference count
    //Length += 1;

    //
    // Handle the allocation from our pools
    //
    s_pXStr->m_CriticalSection.BeginAtomic();

         if( Length < 32 )  { pAlloc = (char*)s_pXStr->m_Pool32.append();  Flags |= xstring::FLAGS_FIXED_SIZE_32; }
    else if( Length < 64 )  { pAlloc = (char*)s_pXStr->m_Pool64.append();  Flags |= xstring::FLAGS_FIXED_SIZE_64; }
    else if( Length < 128 ) { pAlloc = (char*)s_pXStr->m_Pool128.append(); Flags |= xstring::FLAGS_FIXED_SIZE_128; }
    else if( Length < 256 ) { pAlloc = (char*)s_pXStr->m_Pool256.append(); Flags |= xstring::FLAGS_FIXED_SIZE_256; }
    else
    {
        //
        // If we made it here the string must be big so lets call malloc
        //
        pAlloc = (char*)x_malloc( sizeof(char), Length+4, 0 );
    }
    s_pXStr->m_CriticalSection.EndAtomic();

    //
    // Initialize a few things and return
    //
    pAlloc[0] = 1;              // Reference count
    pAlloc[1] = Flags;          // The flags
    pAlloc[2] = 0;              // Terminate the string
    pAlloc[3] = 0;              // Handle double string just in case
    return &pAlloc[2];
}

//------------------------------------------------------------------------------
void xReleaseString( char* pString )
{
    ASSERT( pString );

    //
    // Get the flags and make sure everything is sound
    //
    u8 Flags = pString[-1];
    ASSERT( x_FlagIsOn( Flags, xstring::FLAGS_NON_SYSTEM ) );
    ASSERT( x_FlagIsOn( Flags, xstring::FLAGS_CAN_GROW ) );

    //
    // Handle reference count here
    //        
    u8& Ref = *((u8*)&pString[-2]);

    // Decrement the reference
    Ref--;

    // We handle reference count here
    if( Ref > 0 )
        return;

    //
    // Is it allocated from one of our pools?
    //
    if( x_FlagIsOn( Flags, xstring::FLAGS_FIXED_SIZE_MASK ) )
    {
        s_pXStr->m_CriticalSection.BeginAtomic();

        switch( Flags&xstring::FLAGS_FIXED_SIZE_MASK )
        {
        default:                           ASSERT( 0 ); break;    // We dont have support for this pool
        case xstring::FLAGS_FIXED_SIZE_8:  ASSERT( 0 ); break;    // We dont have support for this pool
        case xstring::FLAGS_FIXED_SIZE_16: ASSERT( 0 ); break;    // We dont have support for this pool

        case xstring::FLAGS_FIXED_SIZE_32:  s_pXStr->m_Pool32.Delete(  (xsafe_array<char,32+4>*)&pString[-2] ); break;
        case xstring::FLAGS_FIXED_SIZE_64:  s_pXStr->m_Pool64.Delete(  (xsafe_array<char,64+4>*)&pString[-2] ); break;
        case xstring::FLAGS_FIXED_SIZE_128: s_pXStr->m_Pool128.Delete( (xsafe_array<char,128+4>*)&pString[-2] ); break;
        case xstring::FLAGS_FIXED_SIZE_256: s_pXStr->m_Pool256.Delete( (xsafe_array<char,256+4>*)&pString[-2] ); break;
        }
        s_pXStr->m_CriticalSection.EndAtomic();
    }
    else
    {
        //
        // It must have been a huge string
        //
        x_free( &pString[-2] );
    }
}
//DOM-IGNORE-END
//==============================================================================
//==============================================================================
//==============================================================================
// XSTRING CLASS
//==============================================================================
//==============================================================================
//==============================================================================

const char* xstring::s_pEmptyString = (char*)X_STR( "This xstring is not Initialize" );

//------------------------------------------------------------------------------

xbool xstring::IsEmpty( void ) const
{
    return m_pString == s_pEmptyString || !m_pString[0];
}

//------------------------------------------------------------------------------

void xstring::SetEmptyString( void )
{
    m_pString = (char*)s_pEmptyString;
}

//------------------------------------------------------------------------------

xstring::xstring( void )
{
    SetEmptyString();
}

//------------------------------------------------------------------------------

xstring::xstring( const xstring_xchar* pStr )
{
    m_pString = (char*)pStr; 
}

//------------------------------------------------------------------------------

xstring::xstring( s32 Reserve )
{
    ASSERT( Reserve > 1 );
    m_pString = xAllocString( Reserve );
}

//------------------------------------------------------------------------------

xstring::xstring( const xstring& String )
{
    SetEmptyString();
    (*this) = String;
}


//------------------------------------------------------------------------------

char& xstring::operator [] ( s32 Index )           
{   
    ASSERT( Index <= GetWritableSize() );
    ASSERT( Index >= 0 );
    return GetPointer()[Index]; 
}

//------------------------------------------------------------------------------

const char& xstring::operator []( s32 Index ) const    
{
    return GetPointer()[Index]; 
}

//------------------------------------------------------------------------------

const xstring& xstring::operator = ( const xstring& Str )
{
    // It can not copy itself
    if ( this == &Str )
        return *this;

    //
    // Handle referece count for the dynamic strings
    //
    if( Str.m_pString && Str.GetFlags() != '$' && x_FlagIsOn( Str.GetFlags(), FLAGS_CAN_GROW ) )
    {
        // Add a reference to this string
        u8& Ref = *((u8*)&Str.m_pString[-2]);
        Ref++;
    
        // Too many reference to this string... what is going on?
        // is this a bug? If not talk to Tomas about this.
        ASSERT( Ref < 0xff );

        // okay destroy our string
        Clear();
    }

    //
    // Now we are ready to copy it
    //
    m_pString = Str.m_pString;

    return *this;
}

//------------------------------------------------------------------------------

xstring::~xstring( void )
{
    Clear();
}

//------------------------------------------------------------------------------

s32 xstring::GetRefCount( void ) const
{
    ASSERT( GetFlags() != '$' );
    ASSERT( x_FlagIsOn( GetFlags(), FLAGS_CAN_GROW ) );
    return (u8)m_pString[-2];
}

//------------------------------------------------------------------------------

xbool xstring::WorkWithBuffer( void ) const
{
    u32 Flags = GetFlags();
    if( Flags=='$' ) return FALSE;
    if( !x_FlagIsOn( Flags, FLAGS_CAN_GROW ) ) return TRUE;
    if( GetRefCount() > 1 ) return TRUE;
    return FALSE; 
}

//------------------------------------------------------------------------------

void xstring::Clear( void )
{
    if( GetPointer() == NULL ) 
    {
        SetEmptyString();
        return;
    }

    u8 Flags = GetFlags();

    //
    // When we work with a buffer there is someone else which is the owner
    // usually a fixed string.
    //
    if( Flags=='$' || !x_FlagIsOn( Flags, FLAGS_CAN_GROW ) ) 
    {
        SetEmptyString();
        return;
    }

    // Okay we must be responsable for this
    xReleaseString( m_pString );
    SetEmptyString();
}

//------------------------------------------------------------------------------

char* xstring::GetPointer( void ) const
{
    return m_pString;
}

//------------------------------------------------------------------------------

u8 xstring::GetFlags( void ) const
{
    ASSERT( GetPointer() );
    return GetPointer()[-1];
}

//------------------------------------------------------------------------------

s32 xstring::GetWritableSize( void ) const
{
    // Get the buffer
    if( GetFlags() == '$' ) return 0;

    // Determine whether it is a fixed string
    u8    Flags   = GetFlags();
    s32   Size    = ( Flags & FLAGS_FIXED_SIZE_MASK);

    if( Size == 0 )
    {
        ASSERT( x_FlagIsOn( Flags, FLAGS_CAN_GROW ) );
        return x_GetElementCount( &GetPointer()[-2] );
    }
    else
    {
        Size = (1<<(Size+2))-1;
    }

    return Size;
}

//------------------------------------------------------------------------------

xbool xstring::IsDoubleNull( void ) const
{
    return x_FlagIsOn( GetFlags(), FLAGS_DOUBLE_NULL );
}

//------------------------------------------------------------------------------

s32 xstring::GetLength( void ) const
{
    char* pBuffer = GetPointer();

    if( pBuffer==NULL ) return 0;

    if( m_pString == s_pEmptyString )
        return 0;

    s32 i;
    if( IsDoubleNull() )
    {
        for( i=0; pBuffer[i]&&pBuffer[i+1]; i++ )
        {
            ASSERT( i < GetWritableSize() || GetFlags() == '$' );
        }
        i++;
    }
    else
    {
        for( i=0; pBuffer[i]; i++ )
        {
            ASSERT( i < GetWritableSize() || GetFlags() == '$' );
        }
    }

    return i;
}

//------------------------------------------------------------------------------

s32 xstring::Format( const char* pFormat, ... )
{
    ASSERT( pFormat );

    xva_list  Args;
    x_va_start( Args, pFormat );

    return FormatV( pFormat, Args );
}

//------------------------------------------------------------------------------

s32 xstring::FormatV(  const char* pFormat, xva_list Args )
{
    ASSERT( pFormat );

    const xvfs  Temp( pFormat, Args );
    const s32   DoubleNull = IsDoubleNull();
    const s32   Len        = Temp.GetLength() + DoubleNull;

    if( Len < GetWritableSize() && WorkWithBuffer() )
    {
        ASSERT( Len < GetWritableSize() );
    }
    else
    {
        // Make sure that we release the old string
        Clear();

        // Ask the system to grow the string
        m_pString = xAllocString( Len );
    }

    // Okay time to copy the results    
    char* pBuffer = GetPointer();
    x_memcpy( pBuffer, (const char*)Temp, sizeof(char)*Len );

    // make sure that is terminated
    pBuffer[Len] = 0;

    return Len;
}

//------------------------------------------------------------------------------

s32 xstring::AppendFormat( const char* pFormat, ... )
{
    ASSERT( pFormat );

    xva_list  Args;
    x_va_start( Args, pFormat );

    return AppendFormatV( pFormat, Args );
}

//------------------------------------------------------------------------------

s32 xstring::AppendFormatV( const char* pFormat, xva_list Args )
{
    ASSERT( pFormat );

    // is this a system string?
    if( m_pString == s_pEmptyString )
    {
        return FormatV( pFormat, Args );        
    }
    else
    {
        // If we were pointing to a static string since we are appending more data
        // we must become a dynamic string
        if( GetFlags() == '$' )
        {
            return FormatV( xfs("%s%s",m_pString,pFormat), Args );        
        }
    }

    // Precompute the string
    const xvfs  Temp( pFormat, Args );
    const s32   Len           = Temp.GetLength();
    const s32   CurrentLength = GetLength(); 
    const s32   DoubleNull    = IsDoubleNull();
    const s32   NewLength     = (Len+CurrentLength+DoubleNull);

    // Make sure that we fit in there
    if( NewLength < GetWritableSize() || WorkWithBuffer() )
    {
        ASSERT( Len < GetWritableSize() );
    }
    else
    {
        // Make sure that we can grow
        ASSERT( x_FlagIsOn(GetFlags(), FLAGS_CAN_GROW) );

        // Ask the system to grow the string
        char* pBuffer = xAllocString( NewLength );

        // copy the old string back
        x_strcpy( pBuffer, NewLength, m_pString );

        // Okay now we can nuke the string
        xReleaseString( m_pString );

        // set the new string on
        m_pString = pBuffer;
    }

    // Okay time to copy the results   
    char* pBuffer = GetPointer();
    x_memcpy( &pBuffer[CurrentLength-DoubleNull], (const char*)Temp, sizeof(char)*Len );

    // make sure that is terminated
    pBuffer[NewLength] = 0;

    return NewLength;
}

//------------------------------------------------------------------------------

s32 xstring::Find( const char* pSubString, s32 StartIndex ) const
{
    ASSERT( pSubString );
    ASSERT( StartIndex >= 0 );

    // If we dont have a valid string then we didnt find any thing
    char* pBuffer = GetPointer();
    if( pBuffer == NULL ) return -1;

    // Get the string that we need to find
    ASSERT( StartIndex < GetWritableSize() );
    ASSERTL( XDEBUG_LEVEL_FULL, StartIndex < GetLength() );

    char* pM = (char*)&pBuffer[StartIndex];
    char* pS1; 
    char* pS2;

    if( !*pSubString )
        return 0;

    xbool bDoubleNull = IsDoubleNull();
    while( *pM || (bDoubleNull && !pM[1]) )
    {
        pS1 = pM;
        pS2 = (char*)pSubString;

        while( *pS1 && *pS2 && !(*pS1 - *pS2) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return(s32)( pM - pBuffer);

        pM++;
    }

    return -1;
}

//------------------------------------------------------------------------------

void xstring::SearchAndReplace( const char* pFindString, const char* pReplaceString )
{
    if( IsEmpty() )
        return;
    
    const s32 LF = x_strlen( pFindString );
    const s32 LR = x_strlen( pReplaceString );
    
    if( LF >= LR )
    {
        // Not need to allocate
        s32 l=0;
        s32 o=0;
        s32 i=0;
        while( m_pString[i] )
        {
            if( m_pString[i] == pFindString[l] )
            {
                l++;
                if( l == LF )
                {
                    s32 offsetbase = (o+1)-LF;
                    
                    // Replace the string
                    for( s32 k=0; k<LR; k++ )
                        m_pString[offsetbase+k] = pReplaceString[k];
                    
                    l=0;
                    
                    o -= (LF-LR);
                }
                else
                {
                    m_pString[o] = m_pString[i];
                }
            }
            else
            {
                l=0;
                m_pString[o] = m_pString[i];
            }
            
            i++;
            o++;
        }
        
        m_pString[o]=0;
    }
    else
    {
        // Lets find how many times we are going to find the string
        s32 Count=0;
        s32 l=0;
        s32 i=0;
        for( ; m_pString[i]; i++ )
        {
            if( m_pString[i] == pFindString[l] )
            {
                l++;
                if( l == LF )
                {
                    Count++;
                    l=0;
                }
            }
            else l = 0;
        }
        
        if( Count == 0 )
            return;
        
        // Make sure that we release the old string
        const s32   L    = i + (LR-LF)*Count;
        char*       pNew = xAllocString( L );
     
        // Not need to allocate
            i=0;
            l=0;
        s32 o=0;
        while( m_pString[i] )
        {
            if( m_pString[i] == pFindString[l] )
            {
                l++;
                if( l == LF )
                {
                    s32 offsetbase = (o+1)-LF;
                    
                    // Replace the string
                    for( s32 k=0; k<LR; k++ )
                        pNew[offsetbase+k] = pReplaceString[k];
                    
                    l=0;
                    
                    o -= (LF-LR);
                }
                else
                {
                    pNew[o] = m_pString[i];
                }
            }
            else
            {
                l=0;
                pNew[o] = m_pString[i];
            }
            
            i++;
            o++;
            
            ASSERT( o <= L );
        }
        
        pNew[o]=0;
        
        //
        // OK now time to fix the string
        //
        
        // Delete the string
        Clear();
        
        m_pString = pNew;
    }
}

//------------------------------------------------------------------------------

s32 xstring::FindI( const char* pSubString, s32 StartIndex ) const
{
    ASSERT( pSubString );
    ASSERT( StartIndex >= 0 );

    // If we dont have a valid string then we didnt find any thing
    char* pBuffer = GetPointer();
    if( pBuffer == NULL ) return -1;

    // Get the string that we need to find
    ASSERTL( XDEBUG_LEVEL_FULL, StartIndex < GetLength() );

    char* pM = (char*)&pBuffer[StartIndex];
    char* pS1; 
    char* pS2;

    if( !*pSubString )
        return 0;

    xbool bDoubleNull = IsDoubleNull();
    while( *pM || (bDoubleNull && !pM[1]) )
    {
        pS1 = pM;
        pS2 = (char*)pSubString;

        while( *pS1 && *pS2 && !(x_tolower(*pS1) - x_tolower(*pS2)) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return(s32)( pM - pBuffer);

        pM++;
    }

    return -1;
}

//------------------------------------------------------------------------------

void xstring::IndexToRowCol( s32 Index, s32& Row, s32& Col ) const
{
    ASSERT( Index >= 0 );

    // Get the info
    char* pBuffer = GetPointer();
    if( pBuffer == NULL )
    {
        Row = Col = -1;
        return;
    }

    ASSERT ( Index < GetWritableSize() );
    ASSERTL( XDEBUG_LEVEL_FULL, Index<GetLength() );

    s32 Scan = 0;
    Row = 1;
    Col = 1;

    while( Scan < Index )
    {
        if( pBuffer[Scan] == '\n' )
        {
            Row++;
            Col = 1;
        }
        else
        {
            Col++;
        }
        Scan++;
    }
}

//------------------------------------------------------------------------------

void xstring::MakeUpper( void )
{
    // is this a system string?
    ASSERT( GetFlags() != '$' );

    // Get the string
    char* pBuffer = GetPointer();
    if( pBuffer == NULL ) return;

    for( s32 i = 0; 1; i++ )
    {
        char c = pBuffer[i];
        if( c == 0 )
        {
            if( IsDoubleNull() )
            {
                if( pBuffer[i+1] == 0 )
                    break;
            }
            else
            {
                break;
            }
        }

        if( (c >= 'a') && (c <= 'z') )
            c += ('A'-'a');

        pBuffer[i] = c;
    }
}

//------------------------------------------------------------------------------

void xstring::MakeLower( void )
{
    // is this a system string?
    ASSERT( GetFlags() != '$' );

    // Get the string
    char* pBuffer = GetPointer();
    if( pBuffer == NULL ) return;

    for( s32 i = 0; 1; i++ )
    {
        char c = pBuffer[i];
        if( c == 0 )
        {
            if( IsDoubleNull() )
            {
                if( pBuffer[i+1] == 0 )
                    break;
            }
            else
            {
                break;
            }
        }

        if( (c >= 'A') && (c <= 'Z') )
            c -= ('A'-'a');

        pBuffer[i] = c;
    }
}

//------------------------------------------------------------------------------

s32 xstring::Copy( const char* pString )
{
    ASSERT( pString );

    // Everyother string we need to allocate space
    s32 Len = x_strlen( pString );
    if( Len <= 0 )
    {
        (*this) = X_STR("");
    }
    else
    {
        Copy( pString, Len );
    }

    // Copy the string
    return Len;
}

//------------------------------------------------------------------------------

void xstring::Copy( const char* pString, s32 Len )
{
    // Convert the string length into a buffer size
    s32 Size = Len + 1 + IsDoubleNull();

    if( Size < GetWritableSize() || WorkWithBuffer() )
    {
        ASSERT( Size < GetWritableSize() );
    }
    else
    {
        // Make sure that we release the old string
        Clear();

        // Ask the system to grow the string
        m_pString = xAllocString( Size );
    }

    // copy string
    s32 i;
    for( i=0; i<Len; i++ )  m_pString[i] = pString[i];

    // terminate string
    m_pString[i]=0;

    // If we are double buffer terminate
    if(IsDoubleNull()) (*this)[Len+1]=0;
}

//------------------------------------------------------------------------------

s32 xstring::Copy( const xstring& String )
{
    // Lets check the space we need
    s32 DoubleNull = IsDoubleNull();
    s32 Len        = String.GetLength() + DoubleNull;

    if( String.m_pString == s_pEmptyString )
        return 0;

    if( Len < GetWritableSize() || WorkWithBuffer() )
    {
        ASSERT( Len < GetWritableSize() );
    }
    else
    {
        // Make sure that we release the old string
        Clear();

        // Static strings we dont need to copy them since they are read only
        if( String.GetFlags() == '$' )
        {
            m_pString = String.m_pString;
            return String.GetLength();
        }

        // Ask the system to grow the string
        m_pString = xAllocString( Len );
    }

    // copy all the characters
	s32 i;
    for( i=0; (m_pString[i] = String.m_pString[i]) || (DoubleNull && String.m_pString[i+1]); i++ )
    {
        ASSERT( i<Len );
    }

    // make sure to terminate for double null strings
    if(DoubleNull) i++;
    m_pString[i]=0;

    ASSERT( i == Len );
    return Len;
}

//------------------------------------------------------------------------------

xbool xstring::operator == ( const xstring& Str ) const
{
    return ( x_strcmp( *this, Str ) == 0 );
}

//==============================================================================
//==============================================================================
//==============================================================================
// STRING SYSTEM
//==============================================================================
//==============================================================================
//==============================================================================
#undef new
#undef delete 

//------------------------------------------------------------------------------

void x_StringInstanceInit( void )
{
    //
    // Create the memory for this instance
    //
    {
        xbase_instance& Instance = x_GetXBaseInstance();
        Instance.m_pString       = new string_local;
        if( Instance.m_pString == NULL )
            x_throw( "Out of memory" );
    }

    //
    // Initi sub systems
    //
    x_InitFS();
}

//------------------------------------------------------------------------------

void x_StringInstanceKill( void )
{
    xbase_instance& Instance = x_GetXBaseInstance();
    if(Instance.m_pString) delete (string_local*)Instance.m_pString;
    Instance.m_pString=NULL;
}

//------------------------------------------------------------------------------

void x_StringInit( void )
{
    s_pXStr = new xstring_global;
    ASSERT( s_pXStr );
}

//------------------------------------------------------------------------------

void x_StringKill( void )
{
    if( s_pXStr )
    {
        delete s_pXStr;
        s_pXStr = NULL;
    }
}
