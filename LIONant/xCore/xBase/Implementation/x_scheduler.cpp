#include "../x_base.h"

/////////////////////////////////////////////////////////////////////////////////
// GLOBALS
/////////////////////////////////////////////////////////////////////////////////

x_scheduler         g_Scheduler;
static xsemaphore   s_Wakeup;
static xsemaphore   s_MainThreadWakeup;
static u64          s_MainTheadID=-1;



/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// WORKERS STRUCTURES
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

struct base_worker: public xthread
{
public:
                            base_worker   ( void ) 
                                : xthread()    
                                , m_pName(NULL) 
                            {}

public:

    virtual void            initialize              ( const char* pName ){ Create( pName, TRUE /*, PRIORITY_HIGHEST*/ ); }
            void            terminate               ( void );
            void            setPriority             ( x_base_job::priority Priority );
public:

    const char*             m_pName;
    base_worker*            m_pNext;
    base_worker*            m_pPrev;
};

//-------------------------------------------------------------------------------

struct normal_worker_thread: public base_worker
{

                            normal_worker_thread    ( void ) {}
    virtual void        	onRun                   ( void );
    static  void            Main                    ( x_base_job* pJob );
    static  void            SyncRun                 ( const volatile xbool& bDone, xbool bLightJobOnly, xbool bIsMainThread );
};

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE WORKER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void base_worker::setPriority( x_base_job::priority Priority )
{
    xthread::priority autoPriority;

    switch( Priority )
    {
    case x_base_job::PRIORITY_BELOW_NORMAL:           autoPriority = PRIORITY_BELOW_NORMAL;    break;
    case x_base_job::PRIORITY_NORMAL:                 autoPriority = PRIORITY_NORMAL;          break;       
    case x_base_job::PRIORITY_ABOVE_NORMAL:           autoPriority = PRIORITY_ABOVE_NORMAL;    break;

    default:
        ASSERT( FALSE );
        autoPriority = PRIORITY_NORMAL;
        break;
    }

    xthread::SetPriority(autoPriority);
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// NORMAL WORKER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void normal_worker_thread::onRun( void )
{
    //
    // Running in an infinite loop trying to run jobs
    //
    //xtick Time;
    do
    {
        //
        // Get any kind of job avariable
        //
        //Time             = x_GetTime();
        x_base_job* pJob = g_Scheduler.getJob( FALSE, FALSE );
        if( pJob )
        {
            Main( pJob );
            continue;
        }

        //
        // Thread has nothing to do now
        //
        //if( x_TicksToMs( x_GetTime() - Time ) > 1 ) 
        s_Wakeup.WaitOne();
        
    } while( 1 );
}

//-------------------------------------------------------------------------------

void normal_worker_thread::SyncRun( const volatile xbool& bDone, xbool bLightJobOnly, xbool bIsMainThread )
{
    //
    // Running in an infinite loop trying to run jobs
    //
    do
    {
        x_base_job* pJob = g_Scheduler.getJob( bLightJobOnly, bIsMainThread );
        if( pJob ) Main( pJob );
        
    } while( bDone );
}

//-------------------------------------------------------------------------------


void normal_worker_thread::Main( x_base_job* pJob )
{
    //
    // Do we have to kill the worker
    //
    xbool   bKillWorker = pJob->m_bKillWorker;

    //
    // Set the priority of the thread
    // The worker priority is from the priority of the job
    // 
//        setPriority( pJob->m_JobPriority );

    //
    // Ready to call the user's function
    //
    pJob->onRun();

    //
    // Okay after running the logic we can consider to be trigger
    //
    pJob->m_bTiggered       = TRUE;
    pJob->m_bQuantumWorld   = FALSE;

    // Copy the hold queue (2 pointers really) since we are about the release/delete the job.
    x_qt_popless_queue NotifyQueue( pJob->m_NotifyQueue );
    
    //
    // Handle the local signal
    //
    if( pJob->m_bLocalSync )
    {
        ASSERT(pJob->m_pSignal);
        *pJob->m_pSignal  = FALSE;
    }

    // Try to free ourselves
    ASSERT( pJob->m_RefCount.get() >= 0 );
    xbool bDelete=FALSE;
    if( pJob->m_RefCount.get() == 0 )
    {
        if( pJob->m_bDelete )
            bDelete = TRUE;
        else
        {
            pJob->vReset();
        }
    }

    // Release dependencies after the reset finish
    // note that there is room for a bug here if the thread gets to run this
    // job again before the dependencies are release.
    g_Scheduler.ReleaseDependencies( NotifyQueue );

    // This is needed here because the dependencies of the job are allocated together
    // so if we release it before the dependencies boom!
    if( bDelete )
    {
        x_delete( pJob );
    }

    //
    // Kills the worker
    //
    if( bKillWorker )
        return;

    //
    // Set the priority so that other unfinished jobs can continue
    //
    //setPriority( xthread::PRIORITY_LOWEST );
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SCHEDULER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

static xbool bInit = FALSE;

void x_scheduler::Init( s32 WorkerCount )
{
    //
    // Make sure we dont init more then ones
    //
    if( bInit )
        return;
    bInit = !bInit;
    
    //
    // Get the number of hardware threads (for Windows)
    //
    if (WorkerCount == -1)
    {
        // main thread is consider a worker too
        WorkerCount = x_GetCPUCoreCount()-1;
    }

    //
    // Initially add as many worker as hardware threads
    //
    for(int i = 0; i < WorkerCount; ++i)
    {
        appendWorker();
    }
    
    //
    // Set the ID of the main thread
    //
    s_MainTheadID = xthread::FindThreadID();
}

//-------------------------------------------------------------------------------

void x_scheduler::appendWorker( void )
{
        //
        // Allocate the worker.
        //
        base_worker* pNewWorker = x_new(normal_worker_thread, 1, 0);

        //
        // Insert into the link list.
        //
        if( m_pWorker == NULL )
        {
            pNewWorker->m_pNext = pNewWorker;
            pNewWorker->m_pPrev = pNewWorker;
            m_pWorker           = pNewWorker;
        }

        pNewWorker->m_pNext             = m_pWorker;
        pNewWorker->m_pPrev             = m_pWorker->m_pPrev;
        m_pWorker->m_pPrev->m_pNext     = pNewWorker;
        m_pWorker->m_pPrev              = pNewWorker;

        m_WorkerCount++;

        //
        // Initialize and start the thread.
        //
        pNewWorker->initialize( "NormalWorker" );
}

//-------------------------------------------------------------------------------

x_base_job* x_scheduler::getJob( xbool bLightOnly, xbool bIsMainThread )
{
    for( s32 i=0; i<x_base_job::PRIORITY_COUNT; i++ )
    {
        x_base_job* pJob;
        
        // Light jobs for not main threads
        if( bIsMainThread == FALSE )
        {
            pJob = (x_base_job*)m_JobQueueNotMain[i][1].pop();
            if( pJob )
                return pJob;
        }
        
        // Handle light jobs first
        pJob = (x_base_job*)m_JobQueueNormal[i][1].pop();
        if( pJob )
            return pJob;
        
        if( bLightOnly ) 
            continue;

        // Light jobs for not main threads
        if( bIsMainThread == FALSE )
        {
            pJob = (x_base_job*)m_JobQueueNotMain[i][0].pop();
            if( pJob )
                return pJob;
        }
        
        // Handle regular jobs
        pJob = (x_base_job*)m_JobQueueNormal[i][0].pop();
        if( pJob )
            return pJob;
    }

    return NULL;
}

//-------------------------------------------------------------------------------

x_base_job* x_scheduler::getMainThreadJob( void )
{
    for( s32 i=0; i<x_base_job::PRIORITY_COUNT; i++ )
    {
        x_base_job* pJob;
        
        // Light Jobs first
        pJob = (x_base_job*)m_JobQueueUIonMainThread[i][1].pop();
        if( pJob != NULL )
            return pJob;
        
        // Normal Jobs next
        pJob = (x_base_job*)m_JobQueueUIonMainThread[i][0].pop();
        if( pJob != NULL )
            return pJob;
    }
    
    return NULL;
}

//-------------------------------------------------------------------------------

void x_scheduler::SubmitJob( x_base_job& Job )
{
    //
    // Okay so we are in quantum world from now on
    //
    ASSERT(!Job.m_bTiggered );
    ASSERT(!Job.m_bQuantumWorld);
    Job.m_bQuantumWorld = TRUE;

    //
    // Set whether there is local syncronization
    //
    volatile xbool  bLocalSyngnal  = FALSE;
    xbool           bLightJob      = Job.m_bLight;
    if( Job.m_bLocalSync )
    {
        bLocalSyngnal  = TRUE;
        Job.m_pSignal  = &bLocalSyngnal;
    }

    //
    // Insert job in queue
    //
    //s32 Count = Job.m_RefAndCount.dec();
    ASSERT( Job.m_RefAndCount.Dec() == 0 );
    
    // Send the job into the queue
    // NOT TOUCHING THE JOB AFTER THIS STAMENT
    if( Job.m_bAffinityMainThread )
    {
        m_JobQueueUIonMainThread[Job.m_JobPriority][Job.m_bLight].push( &Job );
        s_MainThreadWakeup.Signal();
    }
    else if( Job.m_bAffinityNotMainThread )
    {
        m_JobQueueNotMain[Job.m_JobPriority][Job.m_bLight].push( &Job );
        s_Wakeup.Signal();
    }
    else
    {
        m_JobQueueNormal[Job.m_JobPriority][Job.m_bLight].push( &Job );
        s_Wakeup.Signal();
    }

    //
    // Local syncronize
    //
    if( bLocalSyngnal )
    {
        normal_worker_thread::SyncRun( bLocalSyngnal, bLightJob, s_MainTheadID == xthread::FindThreadID() );
    }
}

//-------------------------------------------------------------------------------

void x_scheduler::SubmitTrigger( x_base_trigger& Trigger )
{
    ASSERT( !Trigger.m_bTiggered );
    ASSERT( !Trigger.m_bQuantumWorld );
    Trigger.m_bQuantumWorld = TRUE;

    //
    // Decrement one from the scheguler
    // note after decrementing one it is assume to be in the quantum world
    //
    //s32 Count = Trigger.m_RefAndCount.dec();
    ASSERT( Trigger.m_RefAndCount.Dec() == 0 );

    // Let the user know that we are about to trigger
    Trigger.doBeforeTrigger();

    // Collect dependencies since we are about to delete this trigger (potencially)
    x_qt_popless_queue NotifyQueue( Trigger.m_NotifyQueue );
    
    // Because regular trigger has nothing to run we can try to trigger then imediately
    Trigger.m_bTiggered = TRUE;

    // Handle part of local sync
    xbool bLocalSync = Trigger.m_bLocalSync;
    if( bLocalSync )
    {
        ASSERT(Trigger.m_pSignal);
        ASSERT(Trigger.m_bDelete==FALSE);
    }

    // Try to free ourselves
    ASSERT( Trigger.m_RefCount.get() >= 0 );
    if( Trigger.m_RefCount.get() == 0 )
    {
        if( Trigger.m_bDelete )
            x_delete( &Trigger );
        else
            Trigger.vReset();
    }

    // Release dependencies
    ReleaseDependencies( NotifyQueue );

    // This has to be here because if we release it before the reset happens
    // then our local variable may go out of scope then the reset code will run
    // then boomb (dead). Here basically signals we are 100% done
    if( bLocalSync )
    {
        *Trigger.m_pSignal = FALSE;
    }
}

//-------------------------------------------------------------------------------

void x_scheduler::ResolveDependency( x_base_trigger& Node, xbool bAnd )
{
    // Node has one less person looking at it
    s32 RefCount = Node.m_RefCount.Dec();
    (void)RefCount;
    ASSERT( RefCount >= 0 );

    // If it is an and decrement its counter
    s32 AndValue=0;
    if( bAnd ) 
    {
        AndValue = Node.m_RefAndCount.Dec();
    }

    // Can we submit to the scheduler this one?
    if( AndValue <= 1 ) 
    {
        // If it is not trigger yet... we will have to do something about it.
        if( !Node.m_bTiggered )
        {
            // Submit the trigger to the scheduler
            // And say good bye to the node because it is in the quantum world
            if( Node.m_bJob ) 
            {
                g_Scheduler.SubmitJob( *(x_base_job*)&Node );
            }
            else
            {
                g_Scheduler.SubmitTrigger( Node );
            }
        }
        else
        {
            ASSERT( AndValue == 0 );

            // The node already has been trigger and should have not AND values
            // This is the case of other nodes having OR dependencies to this node.
            // Note that when the ref count == 0 this node should be deleted.
            // However this node should be at least reset it at this point.
        }
    }
    else
    {
        // Nothing to do since other dependencies are looking at it with ANDs 
    }
}

//-------------------------------------------------------------------------------

void x_scheduler::ReleaseDependencies( x_qt_popless_queue& NotifyQueue )
{
    for( x_base_trigger::entry* pNext = (x_base_trigger::entry*)NotifyQueue.m_Head.GetPtr(); pNext; pNext = (x_base_trigger::entry*)pNext->GetPtr() )
    {
        ResolveDependency( *pNext->m_pPtr, pNext->m_bAnd );
    }
}

//-------------------------------------------------------------------------------

void x_scheduler::MainThreadRunJobs( xbool bDoMainTheadOnly, xbool bAndLightJobOnly, xbool bBlocking )
{
    x_base_job* pJob = g_Scheduler.getMainThreadJob();
    if( pJob == NULL)
    {
        if( bDoMainTheadOnly )
        {
            if( bBlocking )
                s_MainThreadWakeup.WaitOne();
            return;
        }
        
        pJob = g_Scheduler.getJob( bAndLightJobOnly, TRUE );
        if( pJob == NULL )
        {
            if( bBlocking )
                s_MainThreadWakeup.WaitOne();
            return;
        }
    }
    
    // Execute the job
    normal_worker_thread::Main( pJob );
}

//-------------------------------------------------------------------------------

void x_scheduler::ProcessWhileWait( f32 WaitTimeMilliseconds, xbool bOnlyLighJobs )
{
    xbool           bIsMainThread   = s_MainTheadID == xthread::FindThreadID();
    volatile xbool  bDone           = FALSE;

    ASSERT( WaitTimeMilliseconds >= 0 );

    if( WaitTimeMilliseconds == 0 )
    {
        normal_worker_thread::SyncRun( bDone, bOnlyLighJobs, bIsMainThread );
    }
    else
    {
        xtick T0 = x_GetTime();

        do
        {
            normal_worker_thread::SyncRun( bDone, bOnlyLighJobs, bIsMainThread );

        } while( x_TicksToMs( x_GetTime() - T0 ) < WaitTimeMilliseconds );
    }
}

//-------------------------------------------------------------------------------

void x_scheduler::ProcessWhileWait( xbool bOnlyLighJobs, x_function<xbool(void)> Function )
{
    xbool           bIsMainThread   = s_MainTheadID == xthread::FindThreadID();
    volatile xbool  bDone           = FALSE;

    do
    {
        normal_worker_thread::SyncRun( bDone, bOnlyLighJobs, bIsMainThread );
    
    } while( Function() );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE TRIGGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

x_base_trigger::x_base_trigger( void )
{
    vReset();

    m_bDelete                = FALSE;    // Default is true we will see if this is correct
    m_bKillWorker            = FALSE;    // This is done only in very special conditions
    m_bJob                   = FALSE;    // Let the job overwrite this one
    m_bLight                 = FALSE;
    m_bLocalSync             = FALSE;
    m_bAffinityMainThread    = FALSE;
    m_bAffinityNotMainThread = FALSE;
}

//-------------------------------------------------------------------------------
x_base_trigger::~x_base_trigger( void )
{
}

//-------------------------------------------------------------------------------

void x_base_trigger::vReset( void )
{
    m_NotifyQueue.Clear();
    m_RefCount.setup( 0 );              

    m_RefAndCount.setup( 1 );           // Having a reference of +1 will prevent the trigger from 
                                        // been trigger while outside the shceduler. Ones the trigger
                                        // is inserted the scheduler will remove it. Which it means that
                                        // at that instance it could be trigger.

    m_bQuantumWorld         = FALSE;
    m_bTiggered             = FALSE;
}

//-------------------------------------------------------------------------------

void x_base_trigger::appendDependency( x_base_trigger& Trigger, xbool bAnd )
{
    ASSERT(!Trigger.m_bTiggered );

    //
    // Deal with reference counting
    //

    // Inc the counter as we are going to point at it
    Trigger.m_RefCount.Inc();

    // if it is an and then we must increment its and counter as well
    if(bAnd) 
    {
        Trigger.m_RefAndCount.Inc();
    }

    //
    // Add the job reference in a place in memory
    //
    if( m_bTiggered )
    {
        g_Scheduler.ResolveDependency( Trigger, bAnd );
    }
    else
    {
        entry& Entry = appendEntry();
        Entry.m_bAnd = bAnd;
        Entry.m_pPtr = &Trigger;

        // Our entry into our queue
        m_NotifyQueue.Push(Entry);
    }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE JOB
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

x_base_job::x_base_job( void )
{
    m_bJob          = TRUE;
    m_JobPriority   = PRIORITY_NORMAL;
}

//-------------------------------------------------------------------------------

void x_base_job::vReset( void )
{
    x_base_trigger::vReset();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// LIGHT JOB
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

x_light_job::x_light_job( void )
{
    m_bLight          = TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// LIGHT JOB
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

x_light_trigger::x_light_trigger( void )
{
    m_bLight            = TRUE;
    m_bLocalSync        = TRUE;
    m_pSignal           = &m_LocalSignal;
    m_LocalSignal       = TRUE;
}

//-------------------------------------------------------------------------------

x_light_trigger::~x_light_trigger( void )
{
//    ASSERT( m_LocalSignal == FALSE );
}

//-------------------------------------------------------------------------------

void x_light_trigger::ManualReset( void )
{
    m_LocalSignal       = TRUE;
}

//-------------------------------------------------------------------------------

void x_light_trigger::LocalSync( xbool bLightJob )
{
    normal_worker_thread::SyncRun( m_LocalSignal, bLightJob, s_MainTheadID == xthread::FindThreadID() );
}
