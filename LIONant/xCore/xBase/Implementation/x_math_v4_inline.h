//==============================================================================
//
// Here are some helpfully notes to work with the sse instructions
//
// r3, r2, r1, r0
// W,  Z,  Y,  X 
//
// _mm_shuffle_ps( a, b, ( bn, bm, an, am ) )
// r0 := am
// r1 := an
// r2 := bm
// r3 := bn
//
//__m128 _mm_movehl_ps( __m128 a, __m128 b );
// r3 := a3
// r2 := a2
// r1 := b3
// r0 := b2
//
//==============================================================================

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( void )
{
    ASSERT( x_IsAlign( this, 16 ) ); 
}

//------------------------------------------------------------------------------
inline
xvector4::xvector4( f32 x )
{
    f32x4& WZYX = *((f32x4*)this);
    #ifdef SSE2_SUPPORT
        WZYX = _mm_set1_ps( x );
        m_W = 1;
    #elif defined(TARGET_360)
        WZYX =  XMVectorSet( x, x, x, x );
    #else
        WZYX.x = WZYX.y = WZYX.z = WZYX.w = x;
    #endif
}

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( const xvector4& V ) : m_WZYX( V.m_WZYX ) 
{ 
    ASSERT( x_IsAlign( this, 16 ) ); 
    ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( const xvector3d& V )  
{ 
    m_X = V.m_X;
    m_Y = V.m_Y;
    m_Z = V.m_Z;
    m_W = 1; 
    ASSERT( x_IsAlign( this, 16 ) ); 
    ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( const xvector3d& V, f32 W ) 
{ 
    m_X = V.m_X;
    m_Y = V.m_Y;
    m_Z = V.m_Z;
    m_W = W; 
    ASSERT( x_IsAlign( this, 16 ) ); 
    ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( f32 X, f32 Y, f32 Z, f32 W )
{
    ASSERT( x_IsAlign( this, 16 ) );
    m_X = X;
    m_Y = Y;
    m_Z = Z;
    m_W = W; 
    ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline 
xvector4::xvector4( const f32x4& Register ) : m_WZYX( Register ) 
{ 
    ASSERT( x_IsAlign( this, 16 ) ); 
    ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline
void xvector4::Identity( void )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_setzero_ps();
#elif defined(TARGET_360)
    m_WZYX = _XMVectorZero();
#else
    m_X = m_Y = m_Z = 0;
#endif
    m_W    = 1;
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::Set( f32 X, f32 Y, f32 Z, f32 W )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_set_ps( W, Z, Y, X );
#elif defined(TARGET_360)
    m_WZYX =  XMVectorSet( X, Y, Z, W );
#else
    m_X = X;
    m_Y = Y;
    m_Z = Z;
    m_W = W;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::Dot( const xvector4& V ) const
{
#ifdef TARGET_PC_32BIT
    f32 Dest;
    __asm 
    {
        mov         ebx, this
        mov         eax, V
        movaps      xmm0, dword ptr [ebx]
        mulps       xmm0, dword ptr [eax]      
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm1, xmm0                        
        movss       xmmword ptr [Dest], xmm1
    }
    return Dest;

#elif defined SSE2_SUPPORT

    const f32x4&      V1_WZYX = *((f32x4*)this);
    const f32x4&      V2_WZYX = *((const f32x4*)&V);

    const f32x4       A = _mm_mul_ps( V1_WZYX, V2_WZYX );
    const f32x4       B = _mm_add_ss( _mm_shuffle_ps( A, A, _MM_SHUFFLE(3,3,3,3)), _mm_add_ss(_mm_shuffle_ps( A, A, _MM_SHUFFLE(0,0,0,0)), _mm_add_ss(_mm_shuffle_ps(A, A, _MM_SHUFFLE(1,1,1,1)), _mm_shuffle_ps(A, A, _MM_SHUFFLE(2,2,2,2)))));

    return ((f32*)&B)[0];

#elif defined(TARGET_360)
		f32x4 res = XMVector4Dot( m_WZYX, V.m_WZYX );

		// can return any element, they all contain the result
		return res.x;
#else
    return m_X * V.m_X + 
           m_Y * V.m_Y +
           m_Z * V.m_Z +
           m_W * V.m_W;
#endif
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::GetLengthSquared( void ) const
{
    return Dot( *this );
}

//------------------------------------------------------------------------------
inline 
f32 xvector4::GetLength( void ) const
{
#ifdef SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_WZYX, m_WZYX );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
          a = _mm_sqrt_ss(a);

    return ((f32*)&a)[0];

#elif defined(TARGET_360)
		f32x4 res = XMVector4LengthSq( m_WZYX );
		return res.x;
#else
        return x_Sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z + m_W*m_W);
#endif
}

//------------------------------------------------------------------------------
inline 
xbool xvector4::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y) && x_isValid(m_Z) && x_isValid(m_W);
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::Normalize( void )
{
#ifdef SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_WZYX, m_WZYX );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
          a = _mm_rsqrt_ss(a);

    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_WZYX = _mm_mul_ps( m_WZYX, oneDivLen );
    return *this;

#elif defined(TARGET_360)
	return XMVector4Normalize( m_WZYX );
#else
    f32 Div = x_InvSqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W );
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::NormalizeSafe( void )
{
    const f32   Tof  = 0.00001f;

#ifdef SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_WZYX, m_WZYX );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );

    if( ((f32*)&a)[0] < Tof )
    {
        Set( 1, 0, 0, 0 );
        return *this;
    }

                  a = _mm_rsqrt_ss(a);
    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_WZYX = _mm_mul_ps( m_WZYX, oneDivLen );
    return *this;

#elif defined(TARGET_360)
	// boils down to 1 instruction, so not much waste, really
	f32x4 len_sq = XMVector4LengthSq( m_WZYX );		

	if( len_sq.x < Tof ) 
    {		
        Set( 1, 0, 0 );
        return *this;
    }
	WZYX = XMVector4Normalize( m_WZYX );
#else
    f32 dsq = m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W;
	if( dsq < Tof ) 
    {		
        Set( 1, 0, 0 );
        return *this;
    }

    f32 Div = x_InvSqrt(dsq);
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator += ( const xvector4& V )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_add_ps( m_WZYX, V.m_WZYX );
#elif defined(TARGET_360)
	m_WZYX = XMVectorAdd( m_WZYX, V.m_WZYX );
#else
    m_X += V.m_X;
    m_Y += V.m_Y;
    m_Z += V.m_Z;
    m_W += V.m_W;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator -= ( const xvector4& V )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_sub_ps( m_WZYX, V.m_WZYX );
#elif defined(TARGET_360)
	m_WZYX = XMVectorSub( m_WZYX, V.m_WZYX );
#else
    m_X -= V.m_X;
    m_Y -= V.m_Y;
    m_Z -= V.m_Z;
    m_W -= V.m_W;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator *= ( const xvector4& V )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_mul_ps( m_WZYX, V.m_WZYX );
#elif defined(TARGET_360)
	m_WZYX = XMVectorMul( m_WZYX, V.m_WZYX );
#else
    m_X *= V.m_X;
    m_Y *= V.m_Y;
    m_Z *= V.m_Z;
    m_W *= V.m_W;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator *= ( f32 Scalar )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_mul_ps( m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
	m_WZYX = XMVectorScale( m_WZYX, Scalar);
#else
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    m_W *= Scalar;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector4& xvector4::operator /= ( f32 Div )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_div_ps( m_WZYX, _mm_set1_ps( Div ) );
#elif defined(TARGET_360)
	m_WZYX = XMVectorScale( m_WZYX, 1.0f/Div);
#else
    f32 Scalar = 1.0f/Div;
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    m_W *= Scalar;
#endif
    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xbool xvector4::operator == ( const xvector4& V ) const
{
         if( x_Abs( V.m_X - m_X) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Y - m_Y) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Z - m_Z) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_W - m_W) > XFLT_TOL) return FALSE;
    return TRUE;
}

//------------------------------------------------------------------------------
inline 
xvector4 operator + ( const xvector4& V1, const xvector4& V2 )
{
#ifdef SSE2_SUPPORT
    return _mm_add_ps( V1.m_WZYX, V2.m_WZYX );
#elif defined(TARGET_360)
	return XMVectorAdd( V1.m_WZYX, V2.m_WZYX ) );
#else
    return xvector4( V1.m_X + V2.m_X, 
                     V1.m_Y + V2.m_Y, 
                     V1.m_Z + V2.m_Z, 
                     V1.m_W + V2.m_W );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator - ( const xvector4& V1, const xvector4& V2 )
{
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( V1.m_WZYX, V2.m_WZYX );
#elif defined(TARGET_360)
	return XMVectorSub( V1.m_WZYX, V2.m_WZYX );
#else
    return xvector4( V1.m_X - V2.m_X, 
                     V1.m_Y - V2.m_Y, 
                     V1.m_Z - V2.m_Z, 
                     V1.m_W - V2.m_W );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( const xvector4& V1, const xvector4& V2 )
{
#ifdef SSE2_SUPPORT
    return _mm_mul_ps( V1.m_WZYX, V2.m_WZYX );
#elif defined(TARGET_360)
	return XMVectorMul( V1.m_WZYX, V2.m_WZYX );
#else
    return xvector4( V1.m_X * V2.m_X, 
                     V1.m_Y * V2.m_Y, 
                     V1.m_Z * V2.m_Z, 
                     V1.m_W * V2.m_W );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator - ( const xvector4& V )
{
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( _mm_setzero_ps(), V.m_WZYX );
#elif defined(TARGET_360)
	return XMVectorNegate( V.m_WZYX );
#else
    return xvector4( -V.m_X, 
                     -V.m_Y,
                     -V.m_Z, 
                     -V.m_W);
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( const xvector4& V, f32 Scalar )
{
#ifdef SSE2_SUPPORT
    return _mm_mul_ps( V.m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorScale( V.m_WZYX, Scalar);
#else
    return xvector4( V.m_X * Scalar, 
                     V.m_Y * Scalar, 
                     V.m_Z * Scalar, 
                     V.m_W * Scalar );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4 operator * ( f32 Scalar, const xvector4& V )
{
#ifdef SSE2_SUPPORT
    return _mm_mul_ps( V.m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorScale( V.m_WZYX, Scalar);
#else
    return xvector4( V.m_X * Scalar, 
                     V.m_Y * Scalar, 
                     V.m_Z * Scalar, 
                     V.m_W * Scalar );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator + ( f32 Scalar, const xvector4& V )
{
#ifdef SSE2_SUPPORT
    return _mm_add_ps( V.m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorAdd( V.m_WZYX, Scalar);
#else
    return xvector4( V.m_X + Scalar,
                     V.m_Y + Scalar,
                     V.m_Z + Scalar,
                     V.m_W + Scalar );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator + ( const xvector4& V, f32 Scalar )
{
#ifdef SSE2_SUPPORT
    return _mm_add_ps( V.m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorAdd( V.m_WZYX, Scalar);
#else
    return xvector4( V.m_X + Scalar,
                     V.m_Y + Scalar,
                     V.m_Z + Scalar,
                     V.m_W + Scalar );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator - ( f32 Scalar, const xvector4& V )
{
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( _mm_set1_ps( Scalar ), V.m_WZYX );
#elif defined(TARGET_360)
    return XMVectorAdd( -V.m_WZYX, Scalar);
#else
    return xvector4( Scalar - V.m_X,
                     Scalar - V.m_Y,
                     Scalar - V.m_Z,
                     Scalar - V.m_W );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 operator - ( const xvector4& V, f32 Scalar )
{
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( V.m_WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorSub( V.m_WZYX, Scalar);
#else
    return xvector4( V.m_X - Scalar,
                     V.m_Y - Scalar,
                     V.m_Z - Scalar,
                     V.m_W - Scalar );
#endif
}


//------------------------------------------------------------------------------
inline 
xvector4 xvector4::GetMin( const xvector4& V ) const
{
#ifdef SSE2_SUPPORT
    return _mm_min_ps( m_WZYX, V.m_WZYX );
#elif defined(TARGET_360)
	return XMVectorMin( m_WZYX, V.m_WZYX );
#else
    return xvector4( x_Min(V.m_X, m_X), x_Min( V.m_Y,m_Y), x_Min( V.m_Z,m_Z), x_Min( V.m_W,m_W) );
#endif
}
 
//------------------------------------------------------------------------------
inline 
xvector4 xvector4::GetMax( const xvector4& V ) const
{
#ifdef SSE2_SUPPORT
    return _mm_max_ps( m_WZYX, V.m_WZYX );
#elif defined(TARGET_360)
    return XMVectorMax( m_WZYX, V.m_WZYX);
#else
    return xvector4( x_Max(V.m_X, m_X), x_Max( V.m_Y,m_Y), x_Max( V.m_Z,m_Z), x_Max( V.m_W,m_W) );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector4& xvector4::Homogeneous( void )
{
    *this /= m_W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector4& xvector4::Abs( void )
{
#ifdef SSE2_SUPPORT
    m_WZYX = _mm_max_ps( m_WZYX, _mm_sub_ps( _mm_setzero_ps(), m_WZYX ) );
#elif defined(TARGET_360)
    m_WZYX = XMVectorAbs( m_WZYX );
#else
    m_X = x_Abs(m_X);
    m_Y = x_Abs(m_Y);
    m_Z = x_Abs(m_Z);
    m_W = x_Abs(m_W);
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xvector4::InRange( const f32 Min, const f32 Max ) const
{
#ifdef SSE2_SUPPORT
    f32x4 t1 = _mm_add_ps( _mm_cmplt_ps( m_WZYX, _mm_set1_ps(Min) ), _mm_cmpgt_ps( m_WZYX, _mm_set1_ps(Max) ) );    
    return ((f32*)&t1)[0] == 0 && ((f32*)&t1)[1] == 0 && ((f32*)&t1)[2] == 0 && ((f32*)&t1)[3] == 0;
#elif defined(TARGET_360)
    return XMVector4GreaterOrEqual( m_WZYX, Min.m_WZYX ) &&  XMVector4LessOrEqual( m_WZYX, Max.m_WZYX );
#else
    return x_InRange( m_X, Min, Max ) && 
           x_InRange( m_Y, Min, Max ) &&  
           x_InRange( m_Z, Min, Max ) &&  
           x_InRange( m_W, Min, Max );
#endif
}

//------------------------------------------------------------------------------
inline
xvector4 xvector4::OneOver( void ) const
{
#ifdef SSE2_SUPPORT
    return _mm_rcp_ps( m_WZYX );
#elif defined(TARGET_360)
    return XMVectorReciprocal( m_WZYX );
#else
    xvector4 temp;
    temp.m_X = 1.0f/m_X;
    temp.m_Y = 1.0f/m_Y;
    temp.m_Z = 1.0f/m_Z;
    temp.m_W = 1.0f/m_W;
    return temp;
#endif
}
