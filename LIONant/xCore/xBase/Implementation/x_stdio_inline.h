//==============================================================================
// PRIVATE HEADER FOR STDIO
//==============================================================================


//==============================================================================
// PRIVATE FUNCTIONS !! PRIVATE FUNCTIONS!! PRIVATE FUNCTIONS!! PRIVATE FUNCTIONS!!
//==============================================================================

void    x_StdioInit          ( void );
void    x_StdioKill          ( void );    

//==============================================================================
// XFILE 
//==============================================================================

//------------------------------------------------------------------------------

template<class T> inline
xbool xfile::Write( const T& Val )
{
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN ) )
    {
        T Swaped = x_EndianSwap(Val);
        return WriteRaw( &Swaped, sizeof(T), 1 );
    }
    return WriteRaw( &Val, sizeof(T), 1 );
}

//------------------------------------------------------------------------------

template<class T> inline
xbool xfile::Write( const T* const pVal, s32 Count )
{
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN ) )
    {
        for( s32 i=0;i<Count;i++)
        {
            T Swaped = x_EndianSwap(pVal[i]);
            if( WriteRaw( &Swaped, sizeof(T), 1 ) == FALSE )
                return FALSE;
        }
        
        return TRUE;
    }
    
    return WriteRaw( pVal, sizeof(T), Count );
}

//------------------------------------------------------------------------------

template<class T> inline
xbool xfile::Read( T& Val )
{
    xbool bSuccess;
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN ) )
    {
        bSuccess = ReadRaw( &Val, sizeof(T), 1 );  
        Val = x_EndianSwap(Val);
    }
    else
    {
        bSuccess = ReadRaw( &Val, sizeof(T), 1 );  
    }

    return bSuccess;
}

//------------------------------------------------------------------------------

template<class T> inline
xbool xfile::Read( T* pVal, s32 Count )
{
    xbool bSuccess=FALSE;
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN ) )
    {
        for( s32 i=0;i<Count;i++)
        {
            bSuccess = ReadRaw( &pVal[i], sizeof(T), 1 );  
            if( bSuccess == FALSE ) break;
            pVal[i] = x_EndianSwap(pVal[i]);
        }
    }
    else
    {
        bSuccess = ReadRaw( pVal, sizeof(T), Count );  
    }

    return bSuccess;
}

//------------------------------------------------------------------------------
inline
xbool xfile::isBinaryMode( void ) const
{
    return !(m_Flags&xfile_device_i::ACC_TEXT);
}

//------------------------------------------------------------------------------
inline
xbool xfile::isReadMode( void ) const
{
    return !!(m_Flags&xfile_device_i::ACC_READ);
}

//------------------------------------------------------------------------------
inline
xbool xfile::isWriteMode( void ) const
{
    return !!(m_Flags&xfile_device_i::ACC_WRITE);
}
