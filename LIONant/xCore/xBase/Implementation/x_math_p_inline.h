
//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline 
xplane::xplane( void )
{
}

//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3d& P1, const xvector3d& P2, const xvector3d& P3 )
{
    Setup( P1, P2, P3 );
}

//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3d& Normal, f32 Distance ) :
    m_Normal( Normal ),
    m_D( Distance ) 
{

}

//------------------------------------------------------------------------------
inline 
xplane::xplane( f32 A, f32 B, f32 C, f32 D ) :
    m_Normal( A, B, C ),
    m_D( D ) 
{

}

//------------------------------------------------------------------------------
inline 
xplane::xplane( const xvector3d& Normal, const xvector3d& Point )
{
    Setup( Normal, Point );
}

//------------------------------------------------------------------------------
inline
void xplane::Setup( f32 A, f32 B, f32 C, f32 D )
{
    m_Normal.Set( A, B, C );
    m_Normal.Normalize();
    m_D = D;
}

//------------------------------------------------------------------------------
inline
void xplane::ComputeD( const xvector3d& P )
{
    m_D = -m_Normal.Dot( P );
}

//------------------------------------------------------------------------------
inline
void xplane::Setup( const xvector3d& P1, const xvector3d& P2, const xvector3d& P3 )
{
    m_Normal = (P2-P1).Cross(P3-P1).Normalize();
    m_D      = -m_Normal.Dot( P1 );
}

//------------------------------------------------------------------------------
inline
void xplane::Setup( const xvector3d& Normal, f32 Distance )
{
    m_Normal = Normal;
    m_Normal.Normalize();
    m_D      = Distance;
}

//------------------------------------------------------------------------------
inline
void xplane::Setup( const xvector3d& Normal, const xvector3d& Point )
{
    m_Normal = Normal;
    m_Normal.Normalize();
    m_D = -Normal.Dot( Point );
}

//------------------------------------------------------------------------------
inline
void xplane::GetOrthovectors ( xvector3d& AxisA,
                               xvector3d& AxisB ) const
{
    f32      AbsA, AbsB, AbsC;
    xvector3 Dir;

    // Get a non-parallel axis to normal.
    AbsA = x_Abs( m_Normal.m_X );
    AbsB = x_Abs( m_Normal.m_Y );
    AbsC = x_Abs( m_Normal.m_Z );

    if( (AbsA<=AbsB) && (AbsA<=AbsC) ) Dir.Set(1,0,0);
    else
        if( (AbsB<=AbsA) && (AbsB<=AbsC) ) Dir.Set(0,1,0);
        else                               Dir.Set(0,0,1);

    AxisA = m_Normal.Cross(Dir).Normalize();
    AxisB = m_Normal.Cross(AxisA).Normalize();
}

//------------------------------------------------------------------------------
inline 
f32 xplane::Dot( const xvector3d& P ) const  
{
    return m_Normal.Dot( P );
}

//------------------------------------------------------------------------------
inline 
f32 xplane::GetDistance( const xvector3d& P ) const
{
    return m_Normal.Dot( P ) + m_D;
}

//------------------------------------------------------------------------------
inline 
s32 xplane::GetWhichSide( const xvector3d& P ) const
{
    f32 Distance = m_Normal.Dot( P ) + m_D;
    if( Distance < -0.99f ) return -1;
    return Distance > 0.01f;
}

//------------------------------------------------------------------------------
inline 
xbool xplane::IntersectLineSegment( f32& T, const xvector3d& P0, const xvector3d& P1 ) const
{
    T = (P1 - P0).Dot( m_Normal );

    if( T == 0.0f ) 
        return FALSE;

    T = -GetDistance( P0 ) / T;

    return TRUE;
}

//------------------------------------------------------------------------------
inline 
xbool xplane::IntersectLine( f32& T, const xvector3d& Start, const xvector3d& Direction ) const
{
    f32 dist = GetDistance( Start );

	// behind plane
    if( dist < 0.0f ) 
        return FALSE ;

	f32 len = -Direction.Dot( m_Normal );
	if( len < dist ) 
    {
		// moving away from plane or point too far away
		return FALSE;
	}

	T = dist/len;
	return TRUE;
}

//------------------------------------------------------------------------------
inline
void xplane::GetComponents( const xvector3d&    V,
                            xvector3d&          Parallel,
                            xvector3d&          Perpendicular ) const
{
    Perpendicular = m_Normal.Dot( V ) * m_Normal;
    Parallel      = V - Perpendicular;
}

//------------------------------------------------------------------------------
inline
xvector3 xplane::GetReflection( const xvector3d& V ) const
{
    return m_Normal.GetReflection( V );
}

//------------------------------------------------------------------------------
inline 
xbool xplane::isValid( void ) const
{
    return m_Normal.isValid() && x_isValid(m_D);
}

//------------------------------------------------------------------------------
inline
xbool xplane::ClipNGon( xvector3d*       pDst, 
                        s32&             nDstVerts,                        
                        const xvector3d* pSrc, 
                        s32              nSrcVerts ) const
{
    f32   D0, D1;
    s32   P0, P1;
    xbool bClipped = FALSE;

    nDstVerts = 0;
    P1 = nSrcVerts-1;
    D1 = GetDistance( pSrc[P1] );

    for( s32 i=0; i<nSrcVerts; i++ ) 
    {
        P0 = P1;
        D0 = D1;
        P1 = i;
        D1 = GetDistance(pSrc[P1]);

        // Do we keep starting vert?
        if( D0 >= 0 )
        {
            pDst[nDstVerts++] = pSrc[P0];
        }

        // Do we need to compute intersection?
        if( ((D0>=0)&&(D1<0)) || ((D0<0)&&(D1>=0)) )
        {
            f32 d = (D1-D0);
            if( x_Abs(d) < 0.00001f )  d = 0.00001f;
            f32 t = (0-D0) / d;
            pDst[nDstVerts++] = pSrc[P0] + t*(pSrc[P1]-pSrc[P0]);
            bClipped = TRUE;
        }
    }

    return bClipped;
}

//------------------------------------------------------------------------------
inline
xbool xplane::ClipNGon( xvector3*        pDst, 
                        s32&             nDstVerts,                        
                        const xvector3*  pSrc, 
                        s32              nSrcVerts ) const
{
    f32   D0, D1;
    s32   P0, P1;
    xbool bClipped = FALSE;

    nDstVerts = 0;
    P1 = nSrcVerts-1;
    D1 = GetDistance( pSrc[P1] );

    for( s32 i=0; i<nSrcVerts; i++ ) 
    {
        P0 = P1;
        D0 = D1;
        P1 = i;
        D1 = GetDistance(pSrc[P1]);

        // Do we keep starting vert?
        if( D0 >= 0 )
        {
            pDst[nDstVerts++] = pSrc[P0];
        }

        // Do we need to compute intersection?
        if( ((D0>=0)&&(D1<0)) || ((D0<0)&&(D1>=0)) )
        {
            f32 d = (D1-D0);
            if( x_Abs(d) < 0.00001f )  d = 0.00001f;
            f32 t = (0-D0) / d;
            pDst[nDstVerts++] = pSrc[P0] + t*(pSrc[P1]-pSrc[P0]);
            bClipped = TRUE;
        }
    }

    return bClipped;
}

//------------------------------------------------------------------------------
inline
xplane& xplane::operator - ( void )
{
    m_Normal = -m_Normal;
    m_D      = -m_D;
    return *this;
}

//------------------------------------------------------------------------------
inline 
xplane operator * ( const xmatrix4& M, const xplane& Plane )
{
    xplane       Newxplane;
    xvector3     V;

    // Transform a point in the xplane by M
    V = M * ( Plane.m_Normal * - Plane.m_D );

    // Compute the transpose of the Inverse of the Matrix (adjoin)
    // and Transform the normal with it
    Newxplane.m_Normal = (M.getAdjoint() * Plane.m_Normal).Normalize();

    // Recompute D by in the transform point
    Newxplane.ComputeD( V );

    return Newxplane;
}
