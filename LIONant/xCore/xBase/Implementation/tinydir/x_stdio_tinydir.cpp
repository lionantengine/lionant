//
//  x_tinydir.cpp
//  xBase
//
//  Created by Tomas Arce on 9/8/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "tinydir.h"
#include "x_base.h"

struct my_tinydir : public x_qt_ptr
{
    tinydir_dir  m_TinyDir;
};

static x_qt_fixed_pool_static_mem<my_tinydir,16> s_DirList;

//------------------------------------------------------------------------

xhandle x_io::DirOpenPath( const char* pPath )
{
    ASSERT(pPath);
    
    xhandle         Handle( s_DirList.AllocByHandle() );
    
    // If we could not get anything then vail
    if( Handle.IsNull() )
    {
        // We may want to increase the buffer of tinydirs...
        ASSERT(0);
        return Handle;
    }
    
    // Ok get our entry and start the work
    tinydir_dir&    TinyDir = s_DirList.getEntry( Handle ).m_TinyDir;
    
    if (tinydir_open(&TinyDir, pPath) == -1)
	{
        s_DirList.FreeByHandle( Handle );
        Handle.setNull();
	}
    
    return Handle;
}

//------------------------------------------------------------------------

xbool x_io::DirGetNext( file_info& Info, xhandle Handle )
{
    tinydir_dir&    TinyDir = s_DirList.getEntry( Handle ).m_TinyDir;
    tinydir_file    File;
    
    // Do we have anything to give to the user?
    if( TinyDir.has_next == FALSE )
        return FALSE;
    
    // Get the file structure
    if( tinydir_readfile(&TinyDir, &File) == -1 )
    {
        return FALSE;
    }
    
    // Copy everything to our structure
    x_strcpy( Info.m_FileName, xfile::MAX_FNAME, File.name );
    x_strcpy( Info.m_Path,     xfile::MAX_PATH,  File.path );
    
    Info.m_bRegular     = File.is_reg;
    Info.m_bDir         = File.is_dir;
    
    // Make sure we set the right extension
    s32 Ext = s32(File.extension - File.name);

    if( Ext <= 0 || Ext > xfile::MAX_PATH )
        Info.m_pExtension   = NULL;
    else
        Info.m_pExtension   = &Info.m_FileName[ Ext ];
    
    // Get ready for next time
    tinydir_next(&TinyDir);
    
    return TRUE;
}

//------------------------------------------------------------------------

void x_io::DirClose( xhandle Handle )
{
    tinydir_dir&    TinyDir = s_DirList.getEntry( Handle ).m_TinyDir;
 
    tinydir_close(&TinyDir);
    
    s_DirList.FreeByHandle( Handle );
}
