//DOM-IGNORE-BEGIN
#pragma once		// Include this file only once

//==============================================================================

inline
const xstring& xtextfile::GetRecordName( void ) const
{
    return m_Record.m_Name;
}

//==============================================================================
inline
s32 xtextfile::GetRecordCount( void ) const
{
    return m_Record.m_Count;
}

//==============================================================================
inline
s32 xtextfile::GetAtomicSize( char Type )
{
    switch( Type )
    {
    case 'f': return 4; 
    case 'F': return 8; 
    case 'd': return 4; 
    case 'h': return 4; 
    case 'D': return 8; 
    case 'c': return 1; 
    case 'C': return 2; 
    case 'S': return sizeof(const char*);
    case 's': return sizeof(xstring);
    case 'e': return sizeof(const char*);
    case 'E': return sizeof(xstring);
    case 'g': return 8;
    }

    return -1;
}

//==============================================================================
inline
xbool xtextfile::isAtomicString( char Type )
{
    switch( Type )
    {
        case 'S': return TRUE;
        case 's': return TRUE;
        case 'e': return TRUE;
        case 'E': return TRUE;
    }
    
    return FALSE;
}


//==============================================================================
inline
s32 xtextfile::GetNumberFields( void ) const      
{ 
    return m_Field.getCount();  
}

//==============================================================================
inline
void xtextfile::GetFieldDesc( s32 Index, xstring& String ) const
{
    const field& Field = m_Field[Index];
    
    if( m_bBinary && Field.m_Type[Field.m_TypeOffset] == '.' )
    {
        String.Copy( Field.m_Type );
        String[ Field.m_TypeOffset ]=0;
        
        s32 iUserType = Field.m_iUserType;
        String.AppendFormat( m_UserTypes[ iUserType ].m_UserType );
    }
    else
    {
        String = Field.m_Type;
    }
}

//==============================================================================
inline
void xtextfile::GetFieldName( s32 Index, xstring& String ) const 
{ 
    String.Copy( m_Field[Index].m_Type );
    String[m_Field[Index].m_TypeOffset-1]=0; 
}

//==============================================================================
inline
const char* xtextfile::GetFieldType( s32 Index ) const
{ 
    s32         tof   =  m_Field[Index].m_TypeOffset;
    const char* pData = &m_Field[Index].m_Type[tof]; 

    return pData;
}

//==============================================================================
inline
u8 xtextfile::getUserTypeUID( s32 Index ) const
{
    const field&        Field       = m_Field[ Index ];
    const user_type&    UserType    =  m_UserTypes[ Field.m_iUserType ];
    return UserType.m_UID;
}


//==============================================================================
inline
s32 xtextfile::AddUserType( const char* pSystemTypes, const char* pUserType, u8 UID )
{
    user_type   Key;
    s32         Index;
    
    // Must have this in order to follow standard
    ASSERT(pUserType[0]=='.');
    
    Key.m_UserType.Copy( pUserType );
    if( m_UserTypes.BinarySearch( Key, Index ) )
    {
        // Make sure that this regitered type is fully duplicated
        ASSERT( x_strcmp( pSystemTypes, &m_UserTypes[Index].m_SystemType[0] ) == 0 );
        
        // If the user is calling to initialize the UID
        if( UID != 0xff && m_UserTypes[Index].m_UID == 0xff )
        {
            m_UserTypes[Index].m_UID = UID;
        }
        
        // we already have it so we dont need to added again
        return Index;
    }
    
    //
    // Sanity check the user types
    //
#ifdef X_DEBUG
    for( s32 i=0; pUserType[i]; i++ )
    {
        ASSERT( FALSE == x_isspace(pUserType[i]) );
    }
    
    for( s32 i=0; pSystemTypes[i]; i++ )
    {
        ASSERT( FALSE == x_isspace(pSystemTypes[i]) );
    }
#endif
    
    //
    // Fill the entry
    //
    user_type&  Entry   = m_UserTypes.Insert( Index );
    
    Entry.m_SystemType.Copy( pSystemTypes );
    Entry.m_UserType     = Key.m_UserType;
    Entry.m_bSaved       = FALSE;
    Entry.m_nSystemTypes = Entry.m_SystemType.GetLength();
    Entry.m_UID          = UID;
    
    return Index;
}

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
// xserialfile
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

inline 
xfile& xserialfile::GetW( void ) const
{
    ASSERT( m_pWrite );
    return m_pWrite->m_Packs[ m_iPack ].m_Data;
}

//------------------------------------------------------------------------------

inline 
xbool xserialfile::IsLocalVariable( u8* pRange )
{
    return (pRange >= m_pClass) && (pRange < ( m_pClass + m_ClassSize ));
}

//------------------------------------------------------------------------------

inline 
s32 xserialfile::ComputeLocalOffset( u8* pItem )
{
    ASSERT( IsLocalVariable( pItem ) );
    return (s32)(pItem - m_pClass);
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Save( const char* pFileName, const T& Object, flags ObjectFlags, xbool bEndianSwap )
{
    // Open the file to make sure we are okay
    xfile File;
    
    // TODO: Set the endian swap flag
    if( File.Open( pFileName, "wb" ) == FALSE )
        x_throw( "Unable to save file [%s]", pFileName );

    // Call the actual save
    Save( File, Object, ObjectFlags, bEndianSwap );

    // done with the file
    File.Close();
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Save( xfile& File, const T& Object, flags ObjectFlags, xbool bSwapEndian )
{
    //
    // Allocate the writting structure
    //
    m_pWrite = x_new( writting, 1, 0);
    if( m_pWrite == NULL )
        x_throw( "Out of memory" );

    // back up the pointer
    m_pWrite->m_pFile = &File;

    //
    // Initialize class members
    //
    m_iPack             = m_pWrite->AllocatePack( ObjectFlags );
    m_ClassPos          = 0;
    m_pClass            = (u8*)((void*)&Object);
    m_ClassSize         = sizeof(Object);
    m_pWrite->m_bEndian = bSwapEndian;

    // Save the initial class
    GetW().PutC( ' ', m_ClassSize, TRUE );

    // Start the saving 
    Object.SerializeIO( *this );

    // Save the file
    SaveFile();

    // clean up
    x_delete( m_pWrite );
    m_pWrite = NULL;
}

inline void xserialfile::Handle( const char& A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const s8&   A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const s16&  A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const s32&  A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const s64&  A ) { GetW().Write( A );   }

inline void xserialfile::Handle( const u8&   A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const u16&  A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const u32&  A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const u64&  A ) { GetW().Write( A );   }

inline void xserialfile::Handle( const f32&  A ) { GetW().Write( A );   }
inline void xserialfile::Handle( const f64&  A ) { GetW().Write( A );   }

//------------------------------------------------------------------------------

inline void xserialfile::Handle( const xmatrix4&    A ) { GetW().Write( (const f32*)&A, sizeof(xmatrix4)/sizeof(f32) );      }
inline void xserialfile::Handle( const xvector3&    A ) { GetW().Write( (const f32*)&A, sizeof(xvector3)/sizeof(f32) );      }
inline void xserialfile::Handle( const xvector3d&   A ) { GetW().Write( (const f32*)&A, sizeof(xvector3d)/sizeof(f32) );     }
inline void xserialfile::Handle( const xbbox&       A ) { GetW().Write( (const f32*)&A, sizeof(xbbox)/sizeof(f32) );         }
inline void xserialfile::Handle( const xcolor&      A ) { GetW().Write( (const u32*)&A, 1 );                                 }
inline void xserialfile::Handle( const xvector2&    A ) { GetW().Write( (const f32*)&A, sizeof(xvector2)/sizeof(f32)    );   }
inline void xserialfile::Handle( const xvector4&    A ) { GetW().Write( (const f32*)&A, sizeof(xvector4)/sizeof(f32)    );   }
inline void xserialfile::Handle( const xquaternion& A ) { GetW().Write( (const f32*)&A, sizeof(xquaternion)/sizeof(f32) );   }
inline void xserialfile::Handle( const xguid&       A ) { GetW().Write( A.m_Guid );                                    }

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Handle( const T& A )
{
    // Reading or writting?
    // TODO: Add support for complex reading
    if( m_pWrite == NULL ) 
        return;

    // Copy most of the data
    xserialfile File(*this);

    File.m_iPack        = m_iPack;
    File.m_ClassPos     = GetW().Tell();
    File.m_pClass       = (u8*)&A;
    File.m_ClassSize    = sizeof( A );
    A.SerializeIO( File );

    // Something very strange happen. We allocated more memory space than we expected
    //ASSERT( GetW().Tell() <= (File.m_ClassPos + sizeof( A )) );

    // Go the end of the structure 
    GetW().SeekOrigin( File.m_ClassPos + sizeof( A ));
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Array( const T& A, s32 Count )
{
    u8* pA = (u8*)((void*)A);

    ASSERT( Count >= 0 );

    // Make sure that we are at the right offset
    GetW().SeekOrigin( m_ClassPos + ComputeLocalOffset( pA ) );

    // Is an array of structures of items so we want to preallocate the space
    // Since it is an array we don't need to put the "* Count"
    GetW().PutC( ' ', sizeof(A), FALSE );

    // Loop throw all the items
    for( s32 i=0; i<Count; i++ )
    {
        Handle( A[i] );
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::HandlePtr( const T& A, s32 Count, flags MemoryFlags )
{
    // Back up the pack index
    xbool BackupPackIndex = m_iPack;

    // Handle pointer details
    HandlePtrDetails( ((u8*)&A), sizeof(*A), Count, MemoryFlags );

    //
    // Loop throw all the items
    //

    // Now loop
    for( s32 i=0; i<Count; i++ )
    {
        Handle( A[i] );

        ASSERT(  GetW().Tell()>= i*(s32)sizeof(A[0]) );
    }

    //
    // Restore the old pack
    //
    m_iPack = BackupPackIndex;
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const ptr<T>& A, s32 Count, flags MemoryFlags )
{
    HandlePtr( A.m_Ptr, Count, MemoryFlags );
}


//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const ptr<const T>& A, s32 Count, flags MemoryFlags )
{
    HandlePtr( A.m_Ptr, Count, MemoryFlags );
}

//------------------------------------------------------------------------------

template< class T, s32 C > inline
void xserialfile::Serialize( const xsafe_array<T,C>& A )
{
    Array( &A[0], C );
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const T& A, s32 Count, flags MemoryFlags )
{ 
    const u8* pA = (const u8*)((void*)A);
    const u8* pB = (const u8*)((void*)&A);

    // Check whether is an pointer
    if( pA != pB )
    {
        // This is not longer allowed. Please use xserial::ptr structure for any pointer you need to serialize
        // TODO: I may need to think whether this is correct or not...
        //       break compatibility with 64/32bits... however virtual pointers in base class already cause traouble so... 
        ASSERT(0);
        HandlePtr( A, Count, MemoryFlags );
    }
    else
    {
        Array( A, Count );  
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const T& A )
{ 
    u8* pA = (u8*)((void*)&A);
    ASSERTS( IsLocalVariable( pA ), "The variable must be a member of the class" );

    // Make sure that we are at the right offset
    GetW().SeekOrigin( m_ClassPos + ComputeLocalOffset( pA ) );

    // IF YOU GET AN ERROR HERE WHILE LINKING CHANCES ARE YOU ARE TRYING TO PASS
    // A POINTER WHEN YOU SHOULD USE --- Serialize( pPtr, Count, flags ) ---
    Handle( A );  
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::SerializeEnum( const T& A )
{ 
    ASSERT( sizeof(A) <= 4 );

    if( sizeof(A) == 1 ) Serialize( *((const u8*)&A)  );
    else if( sizeof(A) == 2 ) Serialize( *((const u16*)&A) );
    else if( sizeof(A) == 4 ) Serialize( *((const u32*)&A) );
    else
    {
        ASSERT( 0 );
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::SerializeEnum( const T& A, s32 Count, flags MemoryFlags )
{
    ASSERT( sizeof( *A ) <= 4 );

    if ( sizeof( *A ) == 1 ) Serialize( ( (const u8*)A ), Count, MemoryFlags );
    else if ( sizeof( *A ) == 2 ) Serialize( ( (const u16*)A ), Count, MemoryFlags );
    else if ( sizeof( *A ) == 4 ) Serialize( ( (const u32*)A ), Count, MemoryFlags );
    else
    {
        ASSERT( 0 );
    }
}

//------------------------------------------------------------------------------

template< class T, s32 C > inline
void xserialfile::SerializeEnum( const xsafe_array<T, C>& A )
{
    for ( const auto& S : A )
    {
        SerializeEnum( S );
    }
}

//------------------------------------------------------------------------------

template< class T > void xserialfile::ResolveObject( T*& pObject )
{
    // Initialize all
    x_Construct( (T*)pObject, *this);

    // deal with temp data
    if ( m_bFreeTempData && m_pTempBlockData )
        x_delete( (xbyte*)m_pTempBlockData );
}


//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Load( xfile& File, T*& pObject )  
{
    LoadHeader( File, sizeof(*pObject) );
    pObject = (T*)LoadObject( File );
    ResolveObject( pObject );
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Load( const char* pFileName, T*& pObject )  
{
    // Open the file
    xfile File;

    if( File.Open( pFileName, "rb" ) == FALSE ) 
        x_throw( "Unable to open file %s", pFileName );

    // Load the object
    Load( File, pObject );

    // Close the file
    File.Close();
}

//------------------------------------------------------------------------------
inline
void xserialfile::SetResourceVersion( u16 ResourceVersion )
{
    m_Header.m_ResourceVersion = ResourceVersion;
}

//------------------------------------------------------------------------------
inline
u16 xserialfile::GetResourceVersion( void ) const
{
    return m_Header.m_ResourceVersion;
}

//------------------------------------------------------------------------------
inline
xbool xserialfile::SwapEndian( void ) const
{
    return m_pWrite->m_bEndian;
}
