//DOM-IGNORE-BEGIN
#pragma once		// Include this file only once

//==============================================================================
// Global redefinition of operators new, new[], delete, and delete[].  The
// new versions of these functions use the memory manager within the x_Base.
// 
// Why is this done here?  Because while the functions work globally the macros
// dont and EVERY source file must include x_types.hpp.
//==============================================================================

//------------------------------------------------------------------------------
// Add support for auto construct new/delete 
//------------------------------------------------------------------------------
inline void* operator new       ( xalloc_size Size, void* pData, xbool bSomething )  throw() { (void)Size;    (void)bSomething; return pData; }
inline void  operator delete    ( void* pMemory,    void* pData, xbool bSomething )  throw() { (void)pMemory; (void)bSomething;(void)pData; }

//------------------------------------------------------------------------------
// Deal with the construct function
//------------------------------------------------------------------------------
template< class T >
struct x_destruct
{
    inline x_destruct(void){}
    inline ~x_destruct(void){}
    T   m_Element;
};

template< class T, class S > inline T* x_Construct( T* pPtr, S& Data )
{
    return new( pPtr, 0 ) T(Data);
}

template< class T > inline T* x_Construct( T* pPtr ) { return new( pPtr, 0 ) T; }

#if defined TARGET_PS3 
template< class T > inline void x_Destruct ( T* pPtr ) { ((x_destruct<T>*)pPtr)->x_destruct<T>::~x_destruct(); }
#else
template< class T > inline void x_Destruct ( T* pPtr ) { pPtr->~T(); }
#endif
//------------------------------------------------------------------------------
// Deal with the default new and delete
//------------------------------------------------------------------------------
#define X_NEW_HANDLER $$$$$$$_YOU_SHOULD_NOT_BE_USING_THIS_STEAD_USE__x_new__AND__x_delete__$$$$$$$
// #define new    X_NEW_HANDLER
// #define delete X_NEW_HANDLER

//------------------------------------------------------------------------------
// Deal with the defaul new and delete
//------------------------------------------------------------------------------
#define X_MALLOC_HANDLER $$$$$$$_YOU_SHOULD_NOT_BE_USING_THIS_STEAD_USE__x_malloc__AND__x_free__$$$$$$$
#ifndef TARGET_ANDROID
    #ifdef malloc
        #undef malloc
    #endif
    #ifdef free
        #undef free
    #endif
    #define malloc    X_MALLOC_HANDLER
    #define free      X_MALLOC_HANDLER
#endif

//==============================================================================
//==============================================================================
//==============================================================================
// RTTI IMPLEMENTATION
//==============================================================================
//==============================================================================
//==============================================================================

inline xrtti::xrtti( const char* pTypeName ): m_pType( pTypeName ), m_pNext1( NULL ), m_pNext2( NULL ), m_pNext3( NULL ) {}
inline xrtti::xrtti( const char* pTypeName, const xrtti& RTTI1 ) : m_pType(pTypeName), m_pNext1((xrtti*)&RTTI1), m_pNext2(NULL), m_pNext3(NULL) {}
inline xrtti::xrtti( const char* pTypeName, const xrtti& RTTI1, const xrtti& RTTI2 ) : m_pType(pTypeName), m_pNext1((xrtti*)&RTTI1), m_pNext2((xrtti*)&RTTI2), m_pNext3(NULL) {}
inline xrtti::xrtti( const char* pTypeName, const xrtti& RTTI1, const xrtti& RTTI2, const xrtti& RTTI3 ) : m_pType(pTypeName), m_pNext1((xrtti*)&RTTI1), m_pNext2((xrtti*)&RTTI2), m_pNext3((xrtti*)&RTTI3) {}

//-------------------------------------------------------------------------------

inline xbool xrtti::isKindOf( const char* pTypeName ) const
{
    if( x_strcmp( m_pType, pTypeName ) == 0 ) return TRUE;
    if( m_pNext1 ) { if( m_pNext1->isKindOf( pTypeName ) ) return TRUE; } else return FALSE;
    if( m_pNext2 ) { if( m_pNext2->isKindOf( pTypeName ) ) return TRUE; } else return FALSE;
    if( m_pNext3 ) { if( m_pNext3->isKindOf( pTypeName ) ) return TRUE; } else return FALSE;
    return FALSE;
}

//-------------------------------------------------------------------------------

inline xbool xrtti::isKindOf( const xrtti& RTTI ) const
{
    if( this == &RTTI ) return TRUE;
    if( m_pNext1 ) { if( m_pNext1->isKindOf( RTTI ) ) return TRUE; } else return FALSE;
    if( m_pNext2 ) { if( m_pNext2->isKindOf( RTTI ) ) return TRUE; } else return FALSE;
    if( m_pNext3 ) { if( m_pNext3->isKindOf( RTTI ) ) return TRUE; } else return FALSE;
    return FALSE;
}


//==============================================================================
//==============================================================================
//==============================================================================
// x_new and x_delete declaration
//==============================================================================
//==============================================================================
//==============================================================================

extern s32      x_GetElementCount   ( void* pPtr );
extern void     x_Real_Free         ( void* pPtr, s32 TypeSize );

void* x_Real_Malloc( s32 TypeSize, s32 Count, u32 Flags, const char*  pFileName, s32 LineNumber );

template< class T > inline
T* x_Hidden_XNew( s32 Count, u32 Flags, const char* pFile, s32 LineNum )
{
    T* pPtr = (T*)x_Real_Malloc( sizeof(T), Count, Flags|XMEM_FLAG_BEEN_NEW, pFile, LineNum );
    if( pPtr == NULL ) return NULL;
    for( s32 i=0; i<Count; i++ ) x_Construct( &pPtr[i] );
    return pPtr;
}

template< class T > inline
void x_Hidden_XDelete( T* pPtr, xbool bCallDestructors )
{
    void* voidPtr = (void*)pPtr;

    if( pPtr == NULL ) return;
    if( bCallDestructors )
    {
        s32 Count = x_GetElementCount( voidPtr );
        for( s32 i=0; i<Count; i++ ) x_Destruct( &pPtr[i] );
    }
    //x_Real_Free( (void*)pPtr, sizeof(T) );
    x_Real_Free( voidPtr, sizeof(T) );

}

#ifdef X_DEBUG
    #define x_new(TYPE,ENTRY_COUNT,FLAGS) x_Hidden_XNew<TYPE>   ( ENTRY_COUNT, xmem_UpdateAligtmentForA(FLAGS, xmem_aligment(TYPE)), __FILE__, __LINE__ )
    #define x_delete(PTR)           x_Hidden_XDelete      ( PTR, TRUE )
#else
    #define x_new(TYPE,COUNT,FLAGS) x_Hidden_XNew<TYPE>   ( COUNT, xmem_UpdateAligtmentForA(FLAGS, xmem_aligment(TYPE)), NULL, 0 )
    #define x_delete(PTR)           x_Hidden_XDelete      ( PTR, TRUE )
#endif


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#undef X_LL_CIRCULAR_FUNCTIONS
#define X_LL_CIRCULAR_FUNCTIONS(PREFIX,TYPE,NEXT,PREV)      \
    inline void PREFIX##Insert( TYPE*& pHead )              \
    {                                                       \
         if( pHead )                                        \
         {                                                  \
             NEXT               = pHead;                    \
             PREV               = pHead->PREV;              \
             pHead->PREV->NEXT 	= this;	                    \
             pHead->PREV        = this;	                    \
         }                                                  \
         else                                               \
         {                                                  \
             NEXT               = this;                     \
             PREV               = this;                     \
         }                                                  \
         pHead = this;                                      \
    }                                                       \
    inline void PREFIX##InsertBefore( TYPE*& pHead, TYPE* pNode )\
    {                                                       \
         ASSERT( pHead && pNode );                          \
         NEXT               = pNode;                        \
         PREV               = pNode->PREV;                  \
         pNode->PREV->NEXT 	= this;	                        \
         pNode->PREV        = this;	                        \
         if( pHead == pNode ) pHead = this;                 \
    }                                                       \
    inline void PREFIX##InsertAfter( TYPE* pNode )          \
    {                                                       \
         ASSERT( pNode );                                   \
         NEXT               = pNode->NEXT;                  \
         PREV               = pNode;                        \
         NEXT->PREV     	= this;	                        \
         pNode->NEXT        = this;	                        \
    }                                                       \
    inline void PREFIX##Append( TYPE*& pHead )              \
    {                                                       \
         if( pHead )                                        \
         {                                                  \
             NEXT               = pHead;                    \
             PREV               = pHead->PREV;              \
             pHead->PREV->NEXT 	= this;	                    \
             pHead->PREV        = this;	                    \
         }                                                  \
         else                                               \
         {                                                  \
             NEXT               = this;                     \
             PREV               = this;                     \
             pHead              = this;                     \
         }                                                  \
    }                                                       \
    inline static void PREFIX##Merge( TYPE* pHeadA, TYPE* pHeadB ) \
    {                                                       \
        if( pHeadA == NULL )                                \
        {                                                   \
            pHeadA = pHeadB;                                \
            return;                                         \
        }                                                   \
        if( pHeadB == NULL )                                \
        {                                                   \
            return;                                         \
        }                                                   \
        pHeadA->PREV->NEXT = pHeadB;                        \
        pHeadB->PREV->NEXT = pHeadA;                        \
        x_Swap( pHeadA->PREV, pHeadB->PREV );               \
    }                                                       \
    inline TYPE* PREFIX##IteratorNext( const TYPE* const pHead ) const \
    {                                                       \
        ASSERT( pHead );                                    \
        if( NEXT == pHead ) return NULL;                    \
        return NEXT;                                        \
    }                                                       \
    inline TYPE* PREFIX##IteratorPrev( const TYPE* const pHead ) const \
    {                                                       \
        ASSERT( pHead );                                    \
        if( PREV == pHead ) return NULL;                    \
        return PREV;                                        \
    }                                                       \
    inline TYPE* PREFIX##Remove( TYPE*& pHead )             \
    {                                                       \
        ASSERT( pHead );                                    \
        NEXT->PREV = PREV;                                  \
        PREV->NEXT = NEXT;                                  \
        if( pHead == this )                                 \
        {                                                   \
            if( NEXT == this )                              \
            {                                               \
                pHead = NULL;                               \
                return NULL;                                \
            }                                               \
            else                                            \
            {                                               \
                pHead = NEXT;                               \
            }                                               \
        }                                                   \
        return NEXT;                                        \
    }                                                       \

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#undef X_LL_CIRCULAR_ARRAY_FUNCTIONS
#define X_LL_CIRCULAR_ARRAY_FUNCTIONS(PREFIX,NODETYPE,INDEXTYPE,NEXT,PREV) \
    enum                                                    \
    {                                                       \
        PREFIX##_NULL = (INDEXTYPE)0xffffffff               \
    };                                                      \
    inline void PREFIX##Insert( NODETYPE* pArray, INDEXTYPE& iHead ) \
    {                                                       \
         ASSERT(pArray);                                    \
         const INDEXTYPE iThis = (INDEXTYPE)(this - pArray);\
         if( iHead != (INDEXTYPE)0xffffffff )               \
         {                                                  \
             NEXT               = iHead;                    \
             PREV               = pArray[iHead].PREV;       \
             pArray[pArray[iHead].PREV].NEXT 	= iThis;	\
             pArray[iHead].PREV              = iThis;	    \
         }                                                  \
         else                                               \
         {                                                  \
             NEXT               = iThis;                    \
             PREV               = iThis;                    \
         }                                                  \
         iHead = iThis;                                     \
    }                                                       \
    inline void PREFIX##InsertBefore( NODETYPE* pArray, INDEXTYPE& iHead, INDEXTYPE iNode ) \
    {                                                       \
         ASSERT(pArray);                                    \
         ASSERT( iHead!=(INDEXTYPE)0xffffffff && iNode!=(INDEXTYPE)0xffffffff ); \
         const INDEXTYPE iThis = (INDEXTYPE)(this - pArray);\
         NEXT               = iNode;                        \
         PREV               = pArray[iNode].PREV;           \
         pArray[pArray[iNode].PREV].NEXT 	= iThis;	    \
         pArray[iNode].PREV                 = iThis;	    \
         if( iHead == iNode ) iHead = iThis;                \
    }                                                       \
    inline void PREFIX##InsertAfter( NODETYPE* pArray, INDEXTYPE iNode ) \
    {                                                       \
         ASSERT(pArray);                                    \
         ASSERT( iNode != (INDEXTYPE)0xffffffff );          \
         const INDEXTYPE iThis = (INDEXTYPE)(this - pArray);\
         NEXT               = pArray[iNode].NEXT;           \
         PREV               = iNode;                        \
         pArray[NEXT].PREV  = iThis;	                    \
         pArray[iNode].NEXT = iThis;	                    \
    }                                                       \
    inline void PREFIX##Append( NODETYPE* pArray, INDEXTYPE& iHead )\
    {                                                       \
         ASSERT(pArray);                                    \
         const INDEXTYPE iThis = (INDEXTYPE)(this - pArray);\
         if( iHead != (INDEXTYPE)0xffffffff )               \
         {                                                  \
             NEXT               = iHead;                    \
             PREV               = pArray[iHead].PREV;       \
             pArray[pArray[iHead].PREV].NEXT = iThis;       \
             pArray[iHead].PREV              = iThis;       \
         }                                                  \
         else                                               \
         {                                                  \
             NEXT               = iThis;                    \
             PREV               = iThis;                    \
             iHead              = iThis;                    \
         }                                                  \
    }                                                       \
    inline static void PREFIX##Merge( NODETYPE* pArray, INDEXTYPE iHeadA, INDEXTYPE iHeadB ) \
    {                                                       \
        ASSERT(pArray);                                     \
        if( iHeadA == (INDEXTYPE)0xffffffff )               \
        {                                                   \
            iHeadA = iHeadB;                                \
            return;                                         \
        }                                                   \
        if( iHeadB == (INDEXTYPE)0xffffffff )               \
        {                                                   \
            return;                                         \
        }                                                   \
        pArray[pArray[iHeadA].PREV].NEXT = iHeadB;          \
        pArray[pArray[iHeadB].PREV].NEXT = iHeadA;          \
        x_Swap( pArray[iHeadA].PREV, pArray[iHeadB].PREV ); \
    }                                                       \
    inline INDEXTYPE PREFIX##IteratorNext( NODETYPE* pArray, const INDEXTYPE iHead ) const \
    {                                                       \
        ASSERT(pArray);                                     \
        ASSERT( iHead != (INDEXTYPE)0xffffffff);            \
        if( NEXT == iHead ) return (INDEXTYPE)0xffffffff;   \
        return NEXT;                                        \
    }                                                       \
    inline INDEXTYPE PREFIX##IteratorPrev( NODETYPE* pArray, const INDEXTYPE iHead ) const \
    {                                                       \
        ASSERT(pArray);                                     \
        ASSERT( iHead != (INDEXTYPE)0xffffffff);            \
        if( PREV == iHead ) return (INDEXTYPE)0xffffffff;   \
        return PREV;                                        \
    }                                                       \
    inline INDEXTYPE PREFIX##Remove( NODETYPE* pArray, INDEXTYPE& iHead ) \
    {                                                       \
        ASSERT(pArray);                                     \
        ASSERT( iHead != (INDEXTYPE)0xffffffff);            \
        const INDEXTYPE iThis = (INDEXTYPE)(this - pArray); \
        pArray[NEXT].PREV = PREV;                           \
        pArray[PREV].NEXT = NEXT;                           \
        if( iHead == iThis )                                \
        {                                                   \
            if( NEXT == iThis )                             \
            {                                               \
                iHead = (INDEXTYPE)0xffffffff;              \
                return (INDEXTYPE)0xffffffff;               \
            }                                               \
            else                                            \
            {                                               \
                iHead = NEXT;                               \
            }                                               \
        }                                                   \
        return NEXT;                                        \
    }                                                       \

//==============================================================================
// END HEADER
//==============================================================================
//DOM-IGNORE-END
