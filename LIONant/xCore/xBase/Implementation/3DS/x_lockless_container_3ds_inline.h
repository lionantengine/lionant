#ifndef _X_LOCKLESS_CONTAINER_3DS_INLINE_
#define _X_LOCKLESS_CONTAINER_3DS_INLINE_


#include "../../x_target.h"

/////////////////////////////////////////////////////////////////////////////////
// INLINE GLOBAL
/////////////////////////////////////////////////////////////////////////////////
inline xbool x_cas32( u32*,
                      u32,
                      u32 )
{
    ASSERTS( FALSE, "x_cas32 is not supported on 3DS yet!");
    return FALSE;
}

//-------------------------------------------------------------------------------

inline s32 x_casInc32( s32* )
{
    ASSERTS( FALSE, "x_casInc32 is not supported on 3DS yet!");
    return -1;
}

//-------------------------------------------------------------------------------

inline s32 x_casDec32( s32* )
{
    ASSERTS( FALSE, "x_casDec32 is not supported on 3DS yet!");
    return -1;
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( u64*,
                      u64,
                      u64 )
{
    ASSERTS( FALSE, "x_cas64 is not supported on 3DS yet!");
    return FALSE;
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( x_qt_ptr&,
                      const x_qt_ptr&,
                      const x_qt_ptr& )
{
    ASSERTS( FALSE, "x_cas64 is not supported on 3DS yet!");
    return FALSE;
}

#endif //_X_LOCKLESS_CONTAINER_3DS_INLINE_
