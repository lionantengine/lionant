#ifdef TARGET_3DS
#ifndef X_3DS_FILE_DEVICE_H
#define X_3DS_FILE_DEVICE_H

//==============================================================================
// INCLUDES
//==============================================================================
#include "../../x_Base.h"

class ctr_device : public xfile_device_i
{
public:
    ctr_device ( void ) : xfile_device_i("rom:a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:s:t:u:v:w:x:y:z:"){}
    
    static void                 sInitialize ( void );
    
protected:
    
    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock );
    virtual void                AsyncAbort  ( void* pFile );
    
    struct file
    {
        void*       mFileStream;
        s32         mFlag;
        s32         mOffset;
        char*       m_Buffer;
        s32         m_BufferReadSize;
        s32         m_BufferSize;
        s32         m_BufferPosition;
    };
    
    xharray<file>   m_lFiles;
    static char     sDefaultRomPath[256];

};

#endif //X_3DS_FILE_DEVICE_H 

#endif // TARGET_3DS
