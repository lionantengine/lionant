#include "../../x_target.h"
#ifdef TARGET_3DS
//==============================================================================
// INCLUDES
//==============================================================================
#include <nn.h>
#include "../../x_base.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================
// types
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This is the main thread class.
// Description:
//     As you can see it comes from the xthread. This allows to treat all the threads
//     the same way including the main thread. The Initialize function sets up some
//     basic variables that the xthread needs. Note that the xthread is never really
//     created since one we get to this point is already done. Note that the main thread
//     has a critical section inside of it. That is the global critical section. 
//     g_pThread is a variable which is unique per-thread although it does not looks like.
//     There is where we keep the pointer of the thread.
//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN
xthread* g_pThread = NULL;

class main_thread : public xthread
{
public:

    void Initialize( void )
    {   
        //
        // Set the basic info
        // 
        x_strcpy( m_Name, m_Name.getCount(), "MainThread" );
        m_ID        = FindThreadID();
        m_Handle    = FindHandle();

        //
        // Set the pointer for the thread
        //
        {
            nn::os::ThreadLocalStorage LocalStorage;
            LocalStorage.SetValue((uptr)this);
        }
    }
};

class custom_thread : public xthread
{
public:

    void Initialize( void )
    {   
        //
        // Set the basic info
        // 
        x_strcpy( m_Name, m_Name.getCount(), "CustomThread" );
        m_ID        = FindThreadID();
        m_Handle    = FindHandle();

        //
        // Set the pointer for the thread
        //
        {
            nn::os::ThreadLocalStorage LocalStorage;
            LocalStorage.SetValue((uptr)this);
        }
    }

    void Uninitialize( void )
    {
    }
};
//DOM-IGNORE-END

//==============================================================================
// Variables
//==============================================================================
//DOM-IGNORE-BEGIN
#if defined( TARGET_3DS )

// Make the constructor be the first ones to kick on
static main_thread s_MainThread;
#endif
//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

#ifdef TARGET_3DS
//==============================================================================
#undef new
#undef delete
xcritical_section::xcritical_section( void )
{
    m_Count            = 0;
    m_pCriticalSection = NULL;
}

//==============================================================================

xcritical_section::~xcritical_section( void )
{
    if( m_pCriticalSection != NULL )
    {
        ((nn::os::CriticalSection*)m_pCriticalSection)->Finalize();
        delete (nn::os::CriticalSection*)m_pCriticalSection;
        m_pCriticalSection =NULL;
    }
        
}

//==============================================================================

void xcritical_section::BeginAtomic( void )
{
    if ( NULL == m_pCriticalSection )
    {
        m_pCriticalSection = new nn::os::CriticalSection();
        ((nn::os::CriticalSection*)m_pCriticalSection)->Initialize();
    }
    ASSERT(m_pCriticalSection);

    ((nn::os::CriticalSection*)m_pCriticalSection)->Enter();
    m_Count++;
}

//==============================================================================

void xcritical_section::EndAtomic( void )
{
    m_Count--;
    ASSERT(m_pCriticalSection);
    ((nn::os::CriticalSection*)m_pCriticalSection)->Leave();
}

//==============================================================================

xbool xcritical_section::IsAtomic( void ) const
{
    return !!m_Count;
}

//==============================================================================

xsemaphore::xsemaphore()
{
    mHandle = new nn::os::Event(true);
    ((nn::os::Event*)mHandle)->ClearSignal();
    ASSERT(NULL != mHandle);
}

xsemaphore::~xsemaphore()
{
    if ( mHandle )
    {
        ((nn::os::Event*)mHandle)->Finalize();
        delete mHandle;
        mHandle = NULL;
    }
}

void xsemaphore::WaitOne()
{
    ASSERT(NULL != mHandle);
    ((nn::os::Event*)mHandle)->Wait();
}

xbool xsemaphore::WaitOne(int millisecondsTimeOut)
{
    ASSERT(NULL != mHandle);
    xbool Result = ((nn::os::Event*)mHandle)->Wait(nn::fnd::TimeSpan::FromMilliSeconds(millisecondsTimeOut));
    if ( Result )
    {
        ((nn::os::Event*)mHandle)->ClearSignal();
    }
    return Result;
}

void xsemaphore::Release()
{
    ASSERT(NULL != mHandle);
    ((nn::os::Event*)mHandle)->Signal();
}

//==============================================================================
xevent::xevent( void )
{
    mHandle = NULL;
}

//==============================================================================
void xevent::Init( void )
{
    mHandle = new nn::os::Event(true);
    ((nn::os::Event*)mHandle)->ClearSignal();
    ASSERT(NULL != mHandle);
}

//==============================================================================
xevent::~xevent( void )
{
    if ( mHandle )
    {
        ((nn::os::Event*)mHandle)->Finalize();
        delete mHandle;
        mHandle = NULL;
    }
}

//==============================================================================
void xevent::WaitOne( void )
{
    ASSERT(NULL != mHandle);
    ((nn::os::Event*)mHandle)->Wait();
}

//==============================================================================
xbool xevent::WaitOne(int millisecondsTimeOut)
{
    ASSERT(NULL != mHandle);
    return ((nn::os::Event*)mHandle)->Wait(nn::fnd::TimeSpan::FromMilliSeconds(millisecondsTimeOut));
}

//==============================================================================
void xevent::Set()
{
    ASSERT(NULL != mHandle);
    ((nn::os::Event*)mHandle)->Signal();
}

//==============================================================================
void xevent::Reset()
{
    ASSERT(NULL != mHandle);
    ((nn::os::Event*)mHandle)->ClearSignal();
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER
#endif

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================

xscope_csection::xscope_csection( xcritical_section& CriticalSection ) :
    m_RefCount( 0 ),
    m_CriticalSection( CriticalSection ) 
{
    BeginAtomic();
}

//==============================================================================

xscope_csection::~xscope_csection( void )
{
    ASSERT( m_RefCount >= 0 );
    for( s32 i=0; i<m_RefCount; i++ )
    {
        m_CriticalSection.EndAtomic();
    }
}

//==============================================================================

void xscope_csection::BeginAtomic( void )
{
    m_CriticalSection.BeginAtomic();
    m_RefCount++;
}

//==============================================================================

void xscope_csection::EndAtomic( void )
{
    ASSERT( m_RefCount > 0 );
    m_CriticalSection.EndAtomic();
    m_RefCount--;
}

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

xthread::xthread( void )
{
    m_ID            = 0;
    m_Handle        = 0;
    m_bAutoDelete   = FALSE;
    m_bInitialize   = FALSE;
}


//==============================================================================

xthread::~xthread( void )
{
}

//==============================================================================
#undef new
#undef delete
void ExecuteFirstCall( void* pParam )
{
    xthread* This = (xthread*)pParam;

    //
    // Keep a reference to this thread
    //
    {
        nn::os::ThreadLocalStorage LocalStorage;
        LocalStorage.SetValue((uptr)This);
    }

    //
    // for logic control lets call the user init
    //
    This->onInitInstance();

    //
    // Tell the program to run
    //
    This->onRun();

    //
    // Okay we must be done now
    //
    xbool bAutoDelete = This->m_bAutoDelete;
    This->onExitInstance();

    //
    // See whether we have to clean house
    //

    // TODO: We need to delete this memory somehow
    // deleting this here may nto be the correct thing to do...
     if( bAutoDelete ) x_delete(This);
}

//==============================================================================

void xthread::onInitInstance( void ) 
{
    x_BaseInitInstance();
}

//==============================================================================

void xthread::onExitInstance( void ) 
{
    x_BaseKillInstance();
}

//==============================================================================
inline s32 GetRealPriority(xthread::priority Priority )
{
    s32     RealPriority;

    //
    // Lookup the priority
    //
    switch( Priority )
    {
    default: ASSERT(0);             RealPriority = 16; break;
    case xthread::PRIORITY_TIME_CRITICAL:    RealPriority = 0; break;
    case xthread::PRIORITY_HIGHEST:          RealPriority = 4; break;
    case xthread::PRIORITY_ABOVE_NORMAL:     RealPriority = 8; break;
    case xthread::PRIORITY_NORMAL:           RealPriority = 16; break;
    case xthread::PRIORITY_BELOW_NORMAL:     RealPriority = 20; break;
    case xthread::PRIORITY_LOWEST:           RealPriority = 24; break;
    case xthread::PRIORITY_IDLE:             RealPriority = 32; break;
    }
    return RealPriority;
}

//==============================================================================

void xthread::Create( 
    const char*                 pName,
    xbool                       ,
    xthread::priority           Priority, 
    s32                         StackSize, 
    xbool                       bAutoDelete )
{
    ASSERT( pName );
    ASSERT( StackSize > 256 );
    ASSERT( m_bInitialize == FALSE );

    //
    // Set the auto delete flag
    // and the name
    //
    m_bAutoDelete = bAutoDelete;
    ASSERT( x_strlen( pName ) < m_Name.getCount() );
    x_strcpy( m_Name, m_Name.getCount(), pName );

    //
    // Create a windows thread
    // TODO: This hold thing will have to be moved to other platforms
    //
    m_Handle = new nn::os::ManagedThread();
   
    if( m_Handle == NULL ) 
    {
        //TODO:        x_throw( "Unable to create xthread" );
    }
    else
    {
        ((nn::os::ManagedThread*)m_Handle)->InitializeUsingAutoStack<void*, void*>(
            ExecuteFirstCall,
            reinterpret_cast<void*>(this),
            StackSize,
            GetRealPriority(Priority)
            );
        
    }
    //
    // We are initialize now
    //
    m_bInitialize   = TRUE;
    ((nn::os::ManagedThread*)m_Handle)->Start();

//TODO:    LOG_INFO( "THREAD", "Thread Created:[%s] ID:%d\n", pName, m_ID );
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

//==============================================================================

xbool xthread::StillAlife( void ) const
{
	return ((nn::os::ManagedThread*)m_Handle)->IsAlive();
}

//==============================================================================

void xthread::WaitUntillDone( void ) const
{
	return ((nn::os::ManagedThread*)m_Handle)->Join();
}


//==============================================================================

void xthread::Resume( void )
{
    ASSERTS(false, "Resume is not supported on 3DS");
}

//==============================================================================

void xthread::Suspend( void )
{
    ASSERTS(false, "Suspend is not supported on 3DS");
}

//==============================================================================

void xthread::BeginAtomic( void )
{
    m_CriticalSection.BeginAtomic();
}

//==============================================================================

void xthread::EndAtomic( void )
{
    m_CriticalSection.EndAtomic();
}

//==============================================================================

xbool xthread::IsAtomic( void )
{
    return m_CriticalSection.IsAtomic();
}

//==============================================================================

void xthread::SetPriority( xthread::priority Priority )
{
    s32     RealPriority;

    //
    // Lookup the priority
    //
    switch( Priority )
    {
    default: ASSERT(0);             RealPriority = 16; break;
    case PRIORITY_TIME_CRITICAL:    RealPriority = 0; break;
    case PRIORITY_HIGHEST:          RealPriority = 4; break;
    case PRIORITY_ABOVE_NORMAL:     RealPriority = 8; break;
    case PRIORITY_NORMAL:           RealPriority = 16; break;
    case PRIORITY_BELOW_NORMAL:     RealPriority = 20; break;
    case PRIORITY_LOWEST:           RealPriority = 24; break;
    case PRIORITY_IDLE:             RealPriority = 32; break;
    }

    //
    // if we needed more resolution than these we can look into THREAD_BASE_PRIORITY
    // 
    ((nn::os::ManagedThread*)m_Handle)->ChangePriority(RealPriority);
}

//==============================================================================

void xthread::Sleep( s32 Milliseconds )
{
    nn::os::Thread::Sleep(nn::fnd::TimeSpan::FromMilliSeconds(Milliseconds));
}

//==============================================================================
u64 xthread::FindThreadID( void )
{
    return nn::os::ManagedThread::GetCurrentId();
}

//==============================================================================
void* xthread::FindHandle( void )
{
    return (void*)nn::os::ManagedThread::GetCurrentThread();
}

#undef new
#undef delete

//==============================================================================
void xthread::Destroy( void )
{
    ((nn::os::ManagedThread*)m_Handle)->Finalize();
    delete ((nn::os::ManagedThread*)m_Handle);
    m_Handle = NULL;
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

//==============================================================================
u64 xthread::GetID( void ) const
{
    return m_ID;
}

//==============================================================================
// functions
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
void x_ThreadInit( void )
{
    nn::os::ManagedThread::InitializeEnvironment();
    //
    // Initializes the main thread
    //
    s_MainThread.Initialize();
}

//==============================================================================
void x_ThreadKill( void )
{
    //
    // TODO:
    // Go throw all the threads that may not have been terminated and
    // force them out of the system.
    //
}

//==============================================================================

void x_Sleep( s32 Milliseconds )
{
    nn::os::Thread::Sleep( nn::fnd::TimeSpan::FromMilliSeconds( Milliseconds ) );
}

void x_Yield( void )
{
    nn::os::Thread::Yield();
}

//==============================================================================

xthread& x_GetCurrentThread ( void )
{
    nn::os::ThreadLocalStorage LocalStorage;
    xthread* pThread = (xthread*)LocalStorage.GetValue();
    ASSERT(pThread);
    return *pThread;
}

//==============================================================================

xbase_instance& x_GetXBaseInstance( void )
{
    xthread& Thread = x_GetCurrentThread();
    return Thread.m_XBaseInstance;
}
//DOM-IGNORE-END

//==============================================================================

void x_BeginAtomic( void )
{
    s_MainThread.BeginAtomic();
}

//==============================================================================

void x_EndAtomic( void )
{
    s_MainThread.EndAtomic();
}

//==============================================================================

xbool x_IsAtomic( void )
{
    return s_MainThread.IsAtomic();
}

//==============================================================================

xthread& x_GetMainThread( void )
{
    return s_MainThread;
}

//==============================================================================
s32 x_GetCPUCoreCount( void )
{
    return 1;
}

#undef new
#undef delete
void x_InitXBaseForCurrentThread( void )
{
    if ( NULL == g_pThread )
    {
        custom_thread* pThreadObject = new custom_thread();
        pThreadObject->Initialize();
        g_pThread = pThreadObject;
        x_BaseInitInstance();
    }
}

void x_KillXBaseForCurrentThread( void )
{
    if ( NULL != g_pThread )
    {
        custom_thread* pThreadObject = (custom_thread*)g_pThread;
        pThreadObject->Uninitialize();
        delete pThreadObject;
        g_pThread = NULL;
    }
}
#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

#endif
