//==============================================================================

// INCLUDES

//==============================================================================

#include <nn.h>

#include "../../x_Target.h"

#include "../../x_base.h"


//==============================================================================

// VARIABLES

//==============================================================================


//==============================================================================

// Functions

//==============================================================================


//------------------------------------------------------------------------------

void x_TimeInit( void )
{

}

//------------------------------------------------------------------------------

void x_TimeKill( void )
{

}

//------------------------------------------------------------------------------

s64 x_GetTicksPerSecond( void )
{
    return (s64)nn::os::Tick::TICKS_PER_SECOND;
}

//------------------------------------------------------------------------------

s64 x_GetTicksPerMs( void )
{
    return (s64)(nn::os::Tick::TICKS_PER_SECOND / 1000) ;

}

//------------------------------------------------------------------------------

s64 x_GetTime( void )
{
    static xcritical_section sProtect;
    sProtect.BeginAtomic();
    static xtick LastTicks = 0;
    xtick           Ticks;
    nn::os::Tick now = nn::os::Tick::GetSystemCurrent();
    Ticks = (xtick)now;

    // Try to help the PC bug

    if( Ticks < LastTicks )     
        Ticks = LastTicks + 1;
    LastTicks = Ticks;
    sProtect.EndAtomic();
    return Ticks;
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

s32 xdate::GetDate( void ) const
{
   return m_Date; 
}

//------------------------------------------------------------------------------

void xdate::SetDate( s32 Date )
{
    m_Date = Date;
}

//------------------------------------------------------------------------------

void xdate::SetDay( s32 Day )
{
    ASSERT( Day >= 0 );
    ASSERT( Day < 32 );
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    m_Date = SetJulianDate( m, Day, y );
}

//------------------------------------------------------------------------------

void xdate::SetMonth( s32 Month )
{
    ASSERT( Month >= 0 );
    ASSERT( Month < 12 );
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    m_Date = SetJulianDate( Month, d, y );
}

//------------------------------------------------------------------------------

void xdate::SetYear( s32 Year )
{
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    m_Date = SetJulianDate( m, d, Year );
}

//------------------------------------------------------------------------------

s32 xdate::GetDay( void ) const
{
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    return d; 
}

//------------------------------------------------------------------------------

s32 xdate::GetMonth( void ) const
{
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    return m; 
}

//------------------------------------------------------------------------------

void xdate::GetDate( s32& Moth, s32& Day, s32& Year ) const
{
    GetJulianDate( m_Date, Moth, Day, Year );
}

//------------------------------------------------------------------------------

void xdate::SetDate( s32 Month, s32 Day, s32 Year )
{
    m_Date = SetJulianDate( Month, Day, Year );
}

//------------------------------------------------------------------------------

s32 xdate::GetYear( void ) const
{
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    return y; 
}

//------------------------------------------------------------------------------

xbool xdate::IsLeapYear( s32 Year )
{
    s32 jd1, jd2;
    jd1 = SetJulianDate( 2, 29, Year );
    jd2 = SetJulianDate( 3, 1, Year );
    return !!( jd2 - jd1 );
}

//------------------------------------------------------------------------------

xbool xdate::IsLeapYear( void ) const 
{
    return IsLeapYear( GetYear() );
}

//------------------------------------------------------------------------------

s32 xdate::DaysInMoth( s32 Month, s32 Year )
{
    static s32 MonthDays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    if( Month == 1 && IsLeapYear(Year) )
        return 29;
    return MonthDays[Month];
} 

//------------------------------------------------------------------------------

s32 xdate::DaysInYear( s32 Year )
{
    if (IsLeapYear(Year)) return 366;
    return 365;
} 

//------------------------------------------------------------------------------

s32 xdate::DayOfYear( void ) const
{
    s32 y, m, d;
    s32 soy;
    GetJulianDate( m_Date, m, d, y );
    soy = SetJulianDate( 1, 1, y );
    return m_Date - soy + 1;
} 

//------------------------------------------------------------------------------

void xdate::SetToString( xstring& Date ) const
{
    s32 y, m, d;
    GetJulianDate( m_Date, m, d, y );
    Date.Format( "%d//%d//%d", m+1, d+1, y );
}

//------------------------------------------------------------------------------

xdate::xdate( void ) 
{
    m_Date = 0;
}

//------------------------------------------------------------------------------

s32 xdate::GetDayOfTheWeek( void ) const
{
    return ( ( m_Date % 7 ) + 1 ) % 7;
}

//------------------------------------------------------------------------------

s32 xdate::SetJulianDate( s32 Month, s32 Day, s32 Year ) 
{
    ASSERT( Day >= 0 );
    ASSERT( Day < 32 );
    ASSERT( Month >= 0 );
    ASSERT( Month < 12 );
    s32 a,b;
    f32 Year_Corr;
    if ( Year < 0 )
        Year++;
    Year_Corr = ( Year > 0 ? 0.0f : 0.75f );
    if ( Month <= 2 )
    {
        Year--;
        Month += 12;
    }
    b = 0;
    if ( Year * 10000.0f + Month * 100.0f + Day >= 15821015.0f )
    {
        a = Year / 100;
        b = 2 - a + a / 4;
    }
    return  (s32) ( 365.25f * Year - Year_Corr ) +
            (s32) ( 30.6001f * ( Month + 1 ) ) + Day + 1720995 + b;
}

//------------------------------------------------------------------------------

void xdate::GetJulianDate( s32 Julian, s32& Month, s32& Day, s32& Year ) 
{
    s32 a, b, c, d, e, z, alpha;
    z = Julian;
    if ( z < 2299161 )
        a = z;
    else
    {
        alpha = (s32) ( ( z - 1867216.25f ) / 36524.25f );
        a = z + 1 + alpha - alpha / 4;
    }
    b = a + 1524;
    c = (s32) ( ( b - 122.1f ) / 365.25 );
    d = (s32) ( 365.25f * c );
    e = (s32) ( ( b - d ) / 30.6001f );
    Day = (s32) b - d - (s32) ( 30.6001f * e );
    Month = (s32) ( e < 13.5f ) ? e - 1 : e - 13;
    Year = (s32) ( Month > 2.5f ) ? ( c - 4716 ) : c - 4715;
    if ( Year <= 0 )
         Year -= 1;
} 

//------------------------------------------------------------------------------

xdate& xdate::Add( s32 Days )
{
    m_Date += Days;
    return *this;
}

//------------------------------------------------------------------------------

xdate& xdate::Subtract( s32 Days )
{
    m_Date -= Days;
    return *this;
}

//------------------------------------------------------------------------------

s32 xdate::Subtract( const xdate Other ) const
{
    return m_Date - Other.m_Date;
}

//------------------------------------------------------------------------------

xdate operator + ( const xdate& Left, const s32 Right )
{
    xdate Temp = Left;
    Temp.Add( Right );
    return Temp;
}

//------------------------------------------------------------------------------

xdate operator + ( const s32 Left, const xdate& Right )
{
    xdate Temp = Right;
    Temp.Add( Left );
    return Temp;
}

//------------------------------------------------------------------------------

xdate& xdate::operator += ( const s32 Right )
{
    Add( Right );
    return *this;
}

//------------------------------------------------------------------------------

xdate operator - ( const xdate& Left, const s32 Right )
{
    xdate Temp = Left;
    Temp.Subtract( Right );
    return Temp;
}

//------------------------------------------------------------------------------

xdate operator - ( const s32 Left, const xdate& Right )
{
    xdate Temp = Right;
    Temp.Subtract( Left );
    return Temp;
}

//------------------------------------------------------------------------------

xdate& xdate::operator -= ( const s32 Right )
{
    Subtract( Right );
    return *this;
}

//------------------------------------------------------------------------------

s32 xdate::operator - ( const xdate& Right ) const
{
    return m_Date - Right.m_Date;
} 

//------------------------------------------------------------------------------

xbool xdate::operator == ( const xdate& Right ) const
{
    return m_Date == Right.m_Date;
}

//------------------------------------------------------------------------------

xbool xdate::operator != ( const xdate& Right ) const
{
    return m_Date != Right.m_Date;
}

//------------------------------------------------------------------------------

xbool xdate::operator < ( const xdate& Right ) const
{
    return m_Date < Right.m_Date;
}

//------------------------------------------------------------------------------

xbool xdate::operator <= ( const xdate& Right ) const
{
    return m_Date <= Right.m_Date;
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void xtime_utc::UpdateTime ( void )
{
    nn::fnd::TimeSpan Now = nn::fnd::DateTime::GetNow() - nn::fnd::DateTime::MIN_DATETIME;
    m_Time = (u64)Now.GetMicroSeconds();
}

//------------------------------------------------------------------------------

void xtime_utc::Clear ( void )
{
    m_Time = 0;
}

//------------------------------------------------------------------------------

s32 xtime_utc::GetSeconds( void ) const
{
    nn::fnd::DateTime Now = nn::fnd::DateTime::MIN_DATETIME + nn::fnd::TimeSpan::FromMicroSeconds(m_Time);
    return Now.GetSecond();
}

//------------------------------------------------------------------------------

s32 xtime_utc::GetMinutes( void ) const
{
    nn::fnd::DateTime Now = nn::fnd::DateTime::MIN_DATETIME + nn::fnd::TimeSpan::FromMicroSeconds(m_Time);
    return Now.GetMinute();
}

//------------------------------------------------------------------------------

s32 xtime_utc::GetHour24( void ) const
{
    nn::fnd::DateTime Now = nn::fnd::DateTime::MIN_DATETIME + nn::fnd::TimeSpan::FromMicroSeconds(m_Time);
    return Now.GetHour();
}

//------------------------------------------------------------------------------

s32 xtime_utc::GetHour12( void ) const
{
    nn::fnd::DateTime Now = nn::fnd::DateTime::MIN_DATETIME + nn::fnd::TimeSpan::FromMicroSeconds(m_Time);
    return Now.GetHour() - 12;
}

//------------------------------------------------------------------------------

xdate xtime_utc::GetDate( void ) const
{
    nn::fnd::DateTime Now = nn::fnd::DateTime::MIN_DATETIME + nn::fnd::TimeSpan::FromMicroSeconds(m_Time);
    xdate Date;
    Date.SetDate( Now.GetMonth(), Now.GetDay(), Now.GetYear() );
    return Date;
}

//------------------------------------------------------------------------------

xbool xtime_utc::operator <  ( const xtime_utc Time ) const
{
    return m_Time < Time.m_Time;
}

//------------------------------------------------------------------------------

xbool xtime_utc::operator <= ( const xtime_utc Time ) const
{
    return m_Time <= Time.m_Time;
}

//------------------------------------------------------------------------------

xbool xtime_utc::operator >  ( const xtime_utc Time ) const
{
    return m_Time > Time.m_Time;
}

//------------------------------------------------------------------------------

xbool xtime_utc::operator >= ( const xtime_utc Time ) const
{
    return m_Time >= Time.m_Time;
}

//------------------------------------------------------------------------------

xbool xtime_utc::operator == ( const xtime_utc Time ) const
{
    return m_Time == Time.m_Time;
}

//------------------------------------------------------------------------------

xtime_utc xtime_utc::operator + ( const xtime_utc Time ) const
{
    xtime_utc T;
    T.m_Time = m_Time + Time.m_Time;
    return T;
}

//------------------------------------------------------------------------------

xtime_utc xtime_utc::operator - ( const xtime_utc Time ) const
{
    xtime_utc T;
    T.m_Time = m_Time - Time.m_Time;
    return T;
}

//------------------------------------------------------------------------------

const xtime_utc& xtime_utc::operator += ( const xtime_utc Time )
{
    m_Time += Time.m_Time;
    return *this;
}

//------------------------------------------------------------------------------

const xtime_utc& xtime_utc::operator -= ( const xtime_utc Time )
{
    m_Time -= Time.m_Time;
    return *this;
}

