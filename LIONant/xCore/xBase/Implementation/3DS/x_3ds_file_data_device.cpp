#ifdef TARGET_3DS
//==============================================================================
// INCLUDES
//==============================================================================
#include <nn.h>
#include "../../x_target.h"
#include "x_3ds_file_data_device.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void ctr_data_device::sInitialize(void)
{
    if (!MountDevice())
    {
        NN_LOG("ctr_data_device FsMount is fail");
    }
}

//-------------------------------------------------------------------------------

void* ctr_data_device::Open( const char* pFileName, u32 Flags )
{
    char    FinalName[256] = {0};
    s32     Flag = nn::fs::OPEN_MODE_WRITE;
    ASSERT( pFileName != NULL );

    if ( '/' != pFileName[0] )
    {
        x_strcpy(FinalName, 256, pFileName);
    }
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
    {
        Flag |= nn::fs::OPEN_MODE_CREATE;
    }
    else
    {
        if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
        {
            Flag   = nn::fs::OPEN_MODE_READ;
        }
    }
    ASSERTS( !x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ), "Async mode is not supported on 3DS!" );

    nn::fs::FileStream* Stream = x_new(nn::fs::FileStream, 1, 0);
    nn::Result Result = Stream->TryInitialize(FinalName, Flag);
    x_printf("filename[%s], fileDescriptor[%d]", FinalName, Stream);
    if ( Result.IsFailure() )
    {
        return NULL;
    }

    //
    // Okay we are in business
    //
    xhandle hFile;
    file&   File = m_lFiles.append( hFile );
    xptr_lock    Ref( m_lFiles );

    File.mFileStream = Stream;
    File.mFlag = Flags;
    return (void*)(u32)(hFile.m_Handle+1);
}

//------------------------------------------------------------------------------

void ctr_data_device::AsyncAbort( void* )
{
    ASSERTS(false, "AsyncAbort is not supported on 3DS!");
}

//------------------------------------------------------------------------------

xfile::sync_state ctr_data_device::Synchronize( void*, xbool )
{
    ASSERTS(false, "Synchronize is not supported on 3DS!");
    return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void ctr_data_device::Close( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );

    //
    // Close the handle
    //
    {
        xptr_lock    Ref( m_lFiles );
        file&   File = m_lFiles( hFile );
        x_printf("close!!!!!!!!, [%d]", File.mFileStream);
        if ( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_WRITE )
            || x_FlagIsOn( File.mFlag, xfile_device_i::ACC_CREATE ))
        {
            ((nn::fs::FileStream*)File.mFileStream)->Flush();
        }
        ((nn::fs::FileStream*)File.mFileStream)->Finalize();
    }

    //
    // Lets free our entry
    //
    m_lFiles.DeleteByHandle( hFile );
    Commit();
}

//------------------------------------------------------------------------------

xbool ctr_data_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    s32 ReadSize = 0;
    nn::Result result = ((nn::fs::FileStream*)File.mFileStream)->TryRead(&ReadSize, pBuffer, Count);
    return result.IsSuccess();
}

//------------------------------------------------------------------------------

void ctr_data_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    s32 WrittenSize = 0;
    ((nn::fs::FileStream*)File.mFileStream)->TryWrite(&WrittenSize, pBuffer, Count, false);
}

//------------------------------------------------------------------------------

void ctr_data_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    nn::fs::PositionBase HardwareMode;
    switch( aMode )
    {
    case SKM_ORIGIN: HardwareMode = nn::fs::POSITION_BASE_BEGIN; break;
    case SKM_CURENT: HardwareMode = nn::fs::POSITION_BASE_CURRENT; break;
    case SKM_END:    HardwareMode = nn::fs::POSITION_BASE_END; break; 
    default: ASSERT(0);               break;
    }

    ((nn::fs::FileStream*)File.mFileStream)->Seek(Pos, HardwareMode);
}

//------------------------------------------------------------------------------

s32 ctr_data_device::Tell( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    return ((nn::fs::FileStream*)File.mFileStream)->GetPosition();
}

//------------------------------------------------------------------------------

void ctr_data_device::Flush( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    ((nn::fs::FileStream*)File.mFileStream)->Flush();
}

//------------------------------------------------------------------------------

s32 ctr_data_device::Length( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    return ((nn::fs::FileStream*)File.mFileStream)->GetSize();
}

//------------------------------------------------------------------------------

xbool ctr_data_device::IsEOF( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    return ((nn::fs::FileStream*)File.mFileStream)->GetPosition() == ((nn::fs::FileStream*)File.mFileStream)->GetSize();
}

//-------------------------------------------------------------------------------

xbool ctr_data_device::MountDevice()
{
    const size_t    nMaxFiles        = 8;
    const size_t    nMaxDirectories  = 8;
    const bool      isDuplicateAll  = true;
    nn::Result      Result;

    Result = nn::fs::MountSaveData("data:");
    if(Result.IsFailure())
    {
        NN_DBG_PRINT_RESULT(Result);

        if((Result <= nn::fs::ResultNotFormatted()) ||
            (Result <= nn::fs::ResultBadFormat()) ||
            (Result <= nn::fs::ResultVerificationFailed()))
        {
            // Save data needs to be formatted
            Result = nn::fs::FormatSaveData(nMaxFiles, nMaxDirectories, isDuplicateAll);
            if(Result.IsFailure())
            {
                NN_LOG("FormatSaveData is fail");
            }
        }   
        else
        {
            // Unexpected errors
            NN_LOG("FormatSaveData is Unexpected errors");
        }

        // If mounting fails here, the save data cannot be used
        Result = nn::fs::MountSaveData("data:");
        if(Result.IsFailure())
        {
            NN_LOG("MountSaveData is fail");
        }
    }
    return true;
}

//------------------------------------------------------------------------------

void ctr_data_device::Commit()
{
    nn::Result Result = nn::fs::CommitSaveData();
    if(Result.IsFailure())
    {
        NN_LOG("WriteCommitSaveData() failed!\n");
    }
}

//------------------------------------------------------------------------------

xbool ctr_data_device::Unmount()
{
    // When duplicated, perform commit before unmounting
    nn::Result Result = nn::fs::CommitSaveData("data:");
    if(Result.IsFailure())
    {
        return false;
    }
    Result = nn::fs::Unmount("data:");
    if(Result.IsFailure())
    {
        NN_DBG_PRINT_RESULT(Result);

        if(nn::fs::ResultNotFound::Includes(Result))
        {
            NN_LOG("data: is not found!\n");
        }
        else
        {
            NN_LOG("Failed to unmount data:!\n");
        }

        return false;
    }
    return true;
}

//------------------------------------------------------------------------------

void ctr_data_device::Kill()
{
    Unmount();
}

#endif
