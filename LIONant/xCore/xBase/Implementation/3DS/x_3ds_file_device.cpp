#ifdef TARGET_3DS
//==============================================================================
// INCLUDES
//==============================================================================
#include <nn.h>
#include "../../x_target.h"
#include "x_3ds_file_device.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

char ctr_device::sDefaultRomPath[256] = {0};

//------------------------------------------------------------------------------

void ctr_device::sInitialize(void)
{
    x_strcpy(sDefaultRomPath, 256, "rom:/");
    nn::fs::Initialize();
    //Init memory
    const size_t RomfBufferSize = 1024 * 64;
    static char Buffer[RomfBufferSize];
    nn::Result Result = nn::fs::MountRom(16, 16, Buffer, RomfBufferSize);
    if (Result.IsFailure())
    {
        NN_LOG("MountRom id fail");
    }
}

//------------------------------------------------------------------------------

void* ctr_device::Open( const char* pFileName, u32 Flags )
{
    char    finalName[256] = {0};
    s32     flag = nn::fs::OPEN_MODE_READ;
    s32     Length = x_strlen(pFileName);
	ASSERT( pFileName != NULL );
    
    if ( Length < 5 ||
        ('r' != pFileName[0] && 'o' != pFileName[1] && 'm' != pFileName[2] && ':' != pFileName[3] && '/' != pFileName[4]) )
    {
        x_strcpy(finalName, 256, sDefaultRomPath);
        x_strcat(finalName, 256, pFileName);
    }
    else
    {
        x_strcpy(finalName, 256, pFileName);
    }

    xbool NeedBuffer = TRUE;
	flag    = nn::fs::OPEN_MODE_READ;
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
    {
        flag |= nn::fs::OPEN_MODE_CREATE;
        NeedBuffer = FALSE;
    }
    else
    {
        if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
        {
		    flag   = nn::fs::OPEN_MODE_READ;
        }
        else
        {
            NeedBuffer = FALSE;
        }
    }
    
    ASSERTS( !x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ), "Async mode is not supported on 3DS!" );
    
    nn::fs::FileStream* Stream = x_new(nn::fs::FileStream, 1, 0);
    nn::Result Result = Stream->TryInitialize(finalName, flag);
    x_printf("filename[%s], fileDescriptor[%d]", finalName, Stream);
    if ( Result.IsFailure() )
    {
        return NULL;
    }
    
    //
    // Okay we are in business
    //
    xhandle hFile;
    file&   File = m_lFiles.append( hFile );
    xptr_lock    Ref( m_lFiles );
    
    File.mFileStream = Stream;
    File.mFlag = Flags;
    File.m_BufferReadSize = 0;
    if ( NeedBuffer )
    {
        File.m_BufferSize = 16 * 1024;
        File.m_Buffer = x_new(char, File.m_BufferSize, 0);
    }
    else
    {
        File.m_BufferSize = 0;
        File.m_Buffer = NULL;
    }
    File.m_BufferPosition = 0;
    return (void*)(u32)(hFile.m_Handle+1);
}

//------------------------------------------------------------------------------

void ctr_device::AsyncAbort( void* )
{
    ASSERTS(false, "AsyncAbort is not supported on 3DS!");
}

//------------------------------------------------------------------------------

xfile::sync_state ctr_device::Synchronize( void*, xbool )
{
    ASSERTS(false, "Synchronize is not supported on 3DS!");
	return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void ctr_device::Close( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    
    //
    // Close the handle
    //
    {
        xptr_lock    Ref( m_lFiles );
        file&   File = m_lFiles( hFile );
        x_printf("close!!!!!!!!, [%d]", File.mFileStream);
        if ( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_WRITE )
            || x_FlagIsOn( File.mFlag, xfile_device_i::ACC_CREATE ))
        {
            ((nn::fs::FileStream*)File.mFileStream)->Flush();
        }
        ((nn::fs::FileStream*)File.mFileStream)->Finalize();

        if ( NULL != File.m_Buffer )
        {
            x_delete(File.m_Buffer);
        }
    }
    
    //
    // Lets free our entry
    //
    m_lFiles.DeleteByHandle( hFile );
}

//------------------------------------------------------------------------------

xbool ctr_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    xbool Result = TRUE;
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    s32 ReadSize = 0;
    s32 AvailableBytes = File.m_BufferReadSize - File.m_BufferPosition;
    if ( AvailableBytes < Count )
    {
        if ( AvailableBytes > 0 )
        {
            x_memcpy(pBuffer, &File.m_Buffer[File.m_BufferPosition], AvailableBytes);
            pBuffer = (void*)&((char*)pBuffer)[AvailableBytes];
            ReadSize = AvailableBytes;
            Count -= AvailableBytes;
        }
        File.m_BufferReadSize = 0;
        File.m_BufferPosition = 0;

        s32 NewReadSize = 0;
        if ( Count > File.m_BufferSize )
        {
            nn::Result result = ((nn::fs::FileStream*)File.mFileStream)->TryRead(&NewReadSize, pBuffer, Count);
            ReadSize += NewReadSize;
            if ( result.IsFailure() )
            {
                Result = FALSE;
            }
            return Result;
        }
        else
        {
            nn::Result result = ((nn::fs::FileStream*)File.mFileStream)->TryRead(&File.m_BufferReadSize, File.m_Buffer, File.m_BufferSize);
            if ( result.IsFailure() )
            {
                Result = FALSE;
            }
            else
            {
                if ( File.m_BufferReadSize < Count )
                {
                    Count = File.m_BufferReadSize;
                }
            }
        }
    }

    x_memcpy(pBuffer, &File.m_Buffer[File.m_BufferPosition], Count);
    File.m_BufferPosition += Count;
    return Result;
}

//------------------------------------------------------------------------------

void ctr_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 WrittenSize = 0;
    ((nn::fs::FileStream*)File.mFileStream)->TryWrite(&WrittenSize, pBuffer, Count, false);
}

//------------------------------------------------------------------------------

void ctr_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    nn::fs::PositionBase HardwareMode;
    switch( aMode )
    {
        case SKM_ORIGIN: HardwareMode = nn::fs::POSITION_BASE_BEGIN; break;
        case SKM_CURENT:
        {
            HardwareMode = nn::fs::POSITION_BASE_CURRENT;
            if ( File.m_BufferSize > 0 )
            {
                s32 NewPos = File.m_BufferPosition + Pos;
                if ( NewPos >= 0 && NewPos < File.m_BufferReadSize )
                {
                    return;
                }

                Pos -= (File.m_BufferReadSize - File.m_BufferPosition);
            }
            break;
        }
        case SKM_END:    HardwareMode = nn::fs::POSITION_BASE_END; break; 
        default: ASSERT(0);               break;
    }

    if ( File.m_BufferSize > 0 )
    {
        File.m_BufferPosition = 0;
        File.m_BufferReadSize = 0;
    }
    
    ((nn::fs::FileStream*)File.mFileStream)->Seek(Pos, HardwareMode);
}

//------------------------------------------------------------------------------

s32 ctr_device::Tell( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    return ((nn::fs::FileStream*)File.mFileStream)->GetPosition();
}

//------------------------------------------------------------------------------

void ctr_device::Flush( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    ((nn::fs::FileStream*)File.mFileStream)->Flush();
}

//------------------------------------------------------------------------------

s32 ctr_device::Length( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    return ((nn::fs::FileStream*)File.mFileStream)->GetSize();
}

//------------------------------------------------------------------------------

xbool ctr_device::IsEOF( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );

    xbool Result = ((nn::fs::FileStream*)File.mFileStream)->GetPosition() == ((nn::fs::FileStream*)File.mFileStream)->GetSize();
    if ( File.m_BufferSize > 0 )
    {
        Result = Result && File.m_BufferPosition >= File.m_BufferReadSize;
    }

    return Result;
}

#endif
