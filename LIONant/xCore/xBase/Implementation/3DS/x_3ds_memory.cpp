#include "x_base.h"

#undef malloc
#undef free
#include <nn.h>

extern "C" void nninitStartUp()
{	
    static const size_t nHeapSize = 20*1024 * 1024;
    nn::os::SetHeapSize(nHeapSize);
    nn::os::InitializeMemoryBlock(nn::os::GetHeapAddress(),nHeapSize);
    nn::init::InitializeAllocator(nHeapSize);
    nn::Result Result = nn::os::SetDeviceMemorySize(40* 1024 * 1024);
    if(Result.IsFailure())
    {
        NN_LOG("\nninitStartUp DeviceMemorySize failed");
    }

}

class stack_manager : public nn::os::AutoStackManager
{
public:
    virtual void* Construct(size_t nStackSize)
    {
        void* pStackBegin= x_malloc( sizeof(u8), nStackSize + 8, XMEM_FLAG_ALIGN_8B );
        void* pResult = reinterpret_cast<void*>(size_t(pStackBegin) + nStackSize);
        size_t* pStoreSize = reinterpret_cast<size_t*>(size_t(pResult));
        *pStoreSize = nStackSize;
        return pResult;
    }
    virtual void Destruct(void * pStackBottom,bool isError)
    {
        size_t nSize= *(reinterpret_cast<size_t*>(size_t(pStackBottom)));
        void* pStart = reinterpret_cast<void*>(size_t(pStackBottom) - nSize);
        if(isError)
        {
            x_free(pStart);
        }
        else
        {
            x_free(pStart);
        }
    }
};

static stack_manager* s_StackManager = NULL;

void _x_InitSystemMemory()
{
    if ( NULL == s_StackManager )
    {
        s_StackManager = new stack_manager();
        nn::os::Thread::SetAutoStackManager(s_StackManager);
    }
}

void _x_KillSystemMemory()
{
    if ( NULL != s_StackManager )
    {
        delete s_StackManager;
        s_StackManager = NULL;
    }
}

#define malloc    X_MALLOC_HANDLER
#define free      X_MALLOC_HANDLER
