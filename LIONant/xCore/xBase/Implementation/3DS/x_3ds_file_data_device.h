#ifdef TARGET_3DS
#ifndef X_3DS_FILE_DATA_DEVICE_H
#define X_3DS_FILE_DATA_DEVICE_H

//==============================================================================
// INCLUDES
//==============================================================================
#include "../../x_Base.h"

class ctr_data_device : public xfile_device_i
{
public:
    ctr_data_device ( void ) : xfile_device_i("data:"){}

    static void                 sInitialize ( void );

protected:
    static xbool                MountDevice ( void );

    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock );
    virtual void                AsyncAbort  ( void* pFile );
    virtual void                Kill        ( void );
    void                        Commit      ( void );
    xbool                       Unmount     ( void );

    struct file
    {
        void*       mFileStream;
        s32         mFlag;
        s32         mOffset;
    };

    xharray<file>   m_lFiles;

};

#endif //X_3DS_FILE_DATA_DEVICE_H 

#endif // TARGET_3DS
