#include "../x_base.h"

//-------------------------------------------------------------------------------

s32 x_sscanf( const char* ibuf, const char* fmt, ... )
{
	xva_list    ap;
	s32         ret;
	
	x_va_start(ap, fmt);
	ret = x_vsscanf(ibuf, fmt, ap);
	x_va_end(ap);
	return(ret);
}

//-------------------------------------------------------------------------------

s32 x_vsscanf( const char* pInStr, char const* pFormatStr, xva_list Args )
{
    s32  ElementsRead=0;

    //
    // Make sure that there are not pre-white spaces in the format string
    //
    while( *pFormatStr && x_isspace(*pFormatStr))
    {
        pFormatStr++;
    }
    if( *pFormatStr == 0 ) return ElementsRead;

    //
    // Start working
    //
    while( *pFormatStr )
    {
        s32 Size   = 0;
        u8  Format = 0;

        if( *pFormatStr++ != '%' ) return ElementsRead;

        //
        // Then the first character is the size
        //
        if( pFormatStr[1] != '%' && pFormatStr[1] != 0 )
        {
            switch( *pFormatStr )
            {
            case 'h': Size = 16; break;
            case 'q': case 'L': Size = 64; break;
            case 'l': Size = 32; break;
            default: 
                {
                    // We do not understand the size symble
                    ASSERT(0);
                    return ElementsRead;
                }
            }

            pFormatStr++;
        }

        //
        // Read actual data
        //
        Format = *pFormatStr++;
        switch( Format )
        {
        case 'p': 
        case 'x': 
        case 'X': Format = 'x'; goto work;
        case 'd': 
        case 'u':
        case 'U': Format = 'd'; goto work;
        case 'e':
        case 'g':
        case 'f': Format = 'f'; goto work;
work:    
            {
                xsafe_array<char,256>   Temp;

                // Skip white spaces
                while( *pInStr && x_isspace(*pInStr))
                {
                    pInStr++;
                }
                if( 0 == *pInStr ) return ElementsRead;

                // copy string to temp
                s32 i=0;
                for(;1;i++)
                {
                    Temp[i] = *pInStr++;
                    if( (Temp[i] >= '0' && Temp[i] <= '9') || Temp[i]=='-' || Temp[i]=='+' )
                        continue;

                    if( Format == 'x' )
                    {
                        switch( Temp[i] )
                        {
                        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
                        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
                        continue;
                        case 'x': case 'X':
                            // Skip the formatting of the hex number
                            if( i == 1 && Temp[0] == '0' )
                            {
                                i=-1;
                                continue;
                            }
                            else
                            {
                                return ElementsRead;
                            }
                        }
                    }
                    else if( Format == 'f' )
                    {
                        switch( Temp[i] )
                        {
                        case 'e': case 'E': case '.': continue;
                        }
                    }

                    // Copy characters
                    Temp[i] = 0;
                    break;
                }

                // read the data
                switch( Format )
                {
                case 'd': 
                    {
                        if( Size == 0 || Size == 32 )
                        {
                            *va_arg(Args, s32 *) = x_atoi32( Temp ); 
                        }
                        else if( Size == 16 )
                        {
                            *va_arg(Args, s16 *) = x_atoi32( Temp ); 
                        }
                        else if( Size == 64 )
                        {
                            *va_arg(Args, s64 *) = x_atoi64( Temp ); 
                        }
                        else
                        {
                            // Unkown size
                            ASSERT(0);
                        }
                        break;
                    }
                case 'x':
                    {
                        if( Size == 0 || Size == 32 )
                        {
                            *va_arg(Args, s32 *) = x_atod32( Temp, 16 ); 
                        }
                        else if( Size == 16 )
                        {
                            *va_arg(Args, s16 *) = x_atod32( Temp, 16 ); 
                        }
                        else if( Size == 64 )
                        {
                            *va_arg(Args, s64 *) = x_atod64( Temp, 16 ); 
                        }
                        else
                        {
                            // Unkown size
                            ASSERT(0);
                        }
                        break;
                    }
                case 'f':
                    {
                        if( Size == 0 || Size == 64 )
                        {
                            *va_arg(Args, f64 *) = x_atof64( Temp ); 
                        }
                        else if( Size == 32 )
                        {
                            *va_arg(Args, f32 *) = x_atof32( Temp ); 
                        }
                        else
                        {
                            // Unkown size
                            ASSERT(0);
                        }

                        break;
                    }
                }
                ElementsRead++;
                break;
            }
        case 's':
            {
                char* pDest           = va_arg(Args, char *);
                xbool bExitWithQuotes = FALSE;
                s32   i               = 0;

                ASSERT( Size == 0);

                // Skip white spaces
                while( *pInStr && x_isspace(*pInStr))
                {
                    pInStr++;
                }
                if( 0 == *pInStr ) return ElementsRead;

                // Check if it is a string formatted with quotes
                if( *pInStr == '"' ) 
                {
                    pInStr++;
                    bExitWithQuotes = TRUE;
                }
                
                // Start copying the string
                for(;1;i++)
                {
                    pDest[i] = *pInStr; pInStr++;
                    if( pDest[i] == 0 ) break;
                    if( bExitWithQuotes )
                    {
                        if( pDest[i] == '"' ) break;
                    }
                    else if( x_isspace( pDest[i] ) )
                    {
                        break;
                    }
                }

                // Terminate nicely
                pDest[i] = 0;
                ElementsRead++;
                break;
            }

        case 'c':
            {
                // Skip white spaces
                while( *pInStr && x_isspace(*pInStr))
                {
                    pInStr++;
                }
                if( 0 == *pInStr ) return ElementsRead;

                // Get the character data
                if( Size == 0 || Size == 32 )
                {
                    *va_arg(Args, s32 *) = *pInStr++; 
                }
                else if( Size == 16 )
                {
                    *va_arg(Args, s16 *) = *pInStr++; 
                }
                else if( Size == 64 )
                {
                    *va_arg(Args, s64 *) = *pInStr++; 
                }
                else
                {
                    // Unkown size
                    ASSERT(0);
                }

                ElementsRead++;
                break;
            }
        }
        //
        // Skip all white spaces
        //
	    while( *pFormatStr && x_isspace(*pFormatStr))
        {
            pFormatStr++;
        }
        if( *pFormatStr == 0 ) break;
    }

    return ElementsRead;

}