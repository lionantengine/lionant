//==============================================================================
// INCLUDES
//==============================================================================
#include <stdio.h>

// used for path related work
#include <iostream>
#include <sys/stat.h>

#include "../x_target.h"

#ifdef TARGET_PC
	#include <Windows.h>
	#include "Windows/x_pc_file_device.h"
#elif defined TARGET_IOS || defined TARGET_OSX
    #include <aio.h>
    #include "../x_base.h"
    #include "darwin/x_darwin_file_device.h"
#elif defined TARGET_MARMALADE
    #include "marmalade/x_file_device.h"
#elif defined TARGET_ANDROID
    #include "Android/x_android_file_device.h"
#elif defined TARGET_3DS
    #include "3DS/x_3ds_file_device.h"
    #include "3DS/x_3ds_file_data_device.h"
#endif

struct file_cache
{
	xstring			m_FileName;
	xptr<xbyte>		m_Data;
};

//==============================================================================
// EXTERNAL PROTOTYPES
//==============================================================================

xbool x_SetCompressDevice			( xfile& File );
void  x_FileSystemAddCacheDevice	( void );

//==============================================================================
// VARIABLES
//==============================================================================
static xfile_device_i*      s_pDevicelist;
static xcritical_section    s_CriticalSection;
static xharray<file_cache>	s_FileCache;

//==============================================================================
// Functions
//==============================================================================

//------------------------------------------------------------------------------

void xfile::Clear( void )
{
    m_pFile     = NULL;
    m_pDevice   = NULL;
    m_Flags     = 0;
    m_FileName.Clear();
}

//------------------------------------------------------------------------------

xfile::xfile( void )
{
    Clear();

}

//------------------------------------------------------------------------------

xfile::~xfile( void )
{
    Close();
}

//------------------------------------------------------------------------------

xbool xfile::Open( const char* pPath, const char* pMode )
{
    ASSERT( pMode && pPath );
    xstring Temp;
    Temp.Copy( pPath );
    return Open( Temp, pMode );
}

//------------------------------------------------------------------------------

xbool xfile::Open( const xstring& Path, const char* pMode )
{
    ASSERT( pMode );

    // We cant open a new file in the middle of using another
    ASSERT( m_pDevice == NULL );

    //
    // Find the device in question
    //
    xsafe_array<char,xfile::MAX_DRIVE>  DeviceName;
    s32                                 i;

    // Copy our device into a separate string
    for( i=0; ( DeviceName[i] = Path[i] )
              && ( i<(DeviceName.getCount()-1))
              && ( Path[i] != ':');
         i++ )
    {
        
    }

    xfile_device_i* pDevice = NULL;
    if( DeviceName[i] != ':' )
    {
        // We didnt find any device we are going to assume the default device
        pDevice = x_FileSystemFindDevice( NULL );
    }
    else
    {
        // lets find it!
        DeviceName[++i] = 0;
        pDevice         = x_FileSystemFindDevice( DeviceName );
    }

    // Make sure that we got a device
    if( pDevice == NULL )
        x_throw( "Unable to find device for this (%s) path", (const char*)Path );

    //
    // Okay now lets make sure that the mode is correct
    //
    u32                 AccFlags=0;
    xbool               bDoCompression   = FALSE;
    xbool               bSeekToEnd       = FALSE;
    xbool               bCompressAndText = FALSE;

    for( i=0; pMode[i]; i++ )
    {
        switch( pMode[i] )
        {
        case 'a':   AccFlags |= xfile_device_i::ACC_READ|xfile_device_i::ACC_WRITE; bSeekToEnd = TRUE; break;         
        case 'r':   AccFlags |= xfile_device_i::ACC_READ;  break;
        case '+':   AccFlags |= xfile_device_i::ACC_WRITE; break;
        case 'w':   AccFlags |= xfile_device_i::ACC_WRITE|xfile_device_i::ACC_READ|xfile_device_i::ACC_CREATE; break;
        case 'c':   AccFlags |= xfile_device_i::ACC_COMPRESS; bDoCompression = TRUE; break;
        case '@':   AccFlags |= xfile_device_i::ACC_ASYNC; break;
        case 't':   AccFlags |= xfile_device_i::ACC_TEXT; break;
        case 'b':   x_FlagOff( AccFlags, xfile_device_i::ACC_TEXT ); break;
        default:
            ASSERTS( 0, xfs("Dont understand this[%c] access mode while opening file (%s)", pMode[i], (const char*)Path ) );
        }
    }

    // next thing is to open the file using the device
    // Note this hold initialization could be VERY WORNG as opening the file 
    // may the one of the slowest part of the file access as the DVD may need to seek
    // to it. This idially should happen Async when an Async mode is requested.
    // May need to review this a bit more carefull later.
    void* pFile = NULL;    
    if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_CREATE) == FALSE )
    {
        pFile = pDevice->Open( &Path[0], AccFlags );
        if( pFile == NULL ) return FALSE;

        // Read the header
        u32 Header;
        if ( pDevice->Read( pFile, &Header, sizeof( Header ) ) == FALSE )
        {
            pDevice->Close( pFile );
            return FALSE;
        }

        // Is this a compress file?
        if( Header== 'LACF' )
        {
            // While reading a compress file we cant use the text mode.
            // We need to tell to the compress device that we want to decompress in text 
            // mode for that we use the bCompressAndText variable.
            if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_TEXT) )
            {
                bCompressAndText = TRUE;
                x_FlagOff( AccFlags, xfile_device_i::ACC_TEXT );
            }

            // Lets tell the system that it is a compress file
            AccFlags |= xfile_device_i::ACC_COMPRESS;
            bDoCompression = TRUE;
        }

        // Make sure to rewind
        pDevice->Seek( pFile, xfile_device_i::SKM_ORIGIN, 0 );
 
    }
    else
    {
        pFile = pDevice->Open( &Path[0], AccFlags );
        if( pFile == NULL ) return FALSE;
        if( bSeekToEnd ) 
        {
            pDevice->Seek( pFile, xfile_device_i::SKM_END, 0 );
        }
    }

    //
    // Set all the member variables
    //
    m_FileName = Path;
    m_pDevice  = pDevice;
    m_pFile    = pFile;
    m_Flags    = AccFlags;

    //
    // Set for compress file
    //
    if( bDoCompression )
    {
        if( x_SetCompressDevice( *this, bCompressAndText ) == FALSE )
        {
            Close();
            return FALSE;
        }
    }

    return TRUE;
}

//------------------------------------------------------------------------------

void xfile::Close( void )
{
    if( m_pFile )
    {
        ASSERT(m_pDevice);
        m_pDevice->Close( m_pFile );
    }

    //
    // Done with the file
    //
    Clear();
}

//------------------------------------------------------------------------------

xbool xfile::ReadRaw( void* pBuffer, s32 Size, s32 Count )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);
    ASSERT( pBuffer );
    ASSERT( Size > 0 );
    ASSERT( Count >= 0 );

    s32 TotalCount = Size*Count;
    if( m_pDevice->Read( m_pFile, pBuffer, TotalCount ) )
    {
        // If it is text mode try finding '\r\n' to remove the '\r'
        if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_TEXT ) )
        {
            char* pCharSrc = (char*)pBuffer;
            char* pCharDst = (char*)pBuffer;

            for( s32 i=0; i<TotalCount; i++ )
            {
                *pCharDst = *pCharSrc;
                pCharSrc++;
                if( !(*pCharSrc == '\n' && *(pCharSrc-1) == '\r') ) 
                {
                    pCharDst++;
                }
            }

            // Read any additional data that we may need
            if( pCharSrc != pCharDst )
            {
                s32 Delta = (s32)(pCharSrc - pCharDst);

                // Recurse
                return ReadRaw( &((char*)pBuffer)[TotalCount-Delta], 1, Delta );
            }

            // Sugar a bad case here. We need to read one more character to know what to do.
            if( *--pCharDst == '\r' )
            {
                u8 C;

                if( m_pDevice->Read( m_pFile, &C, 1 ) == FALSE )
                    return FALSE;

                if( C == '\n' )
                {
                    *pCharDst = '\n';
                }
                else
                {
                    // Upss the next character didnt match the sequence
                    // lets rewind one
                    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_CURENT, -1 );
                }
            }
        }

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xfile::WriteRaw( const void* pBuffer, s32 Size, s32 Count )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);
    ASSERT( pBuffer );
    ASSERT( Size > 0 );
    ASSERT( Count >= 0 );

    // If it is text mode try finding '\n' and add a '\r' in front so that it puts in the file '\r\n'
    s32 TotalCount = Size*Count;
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_TEXT ) )
    {
        char* pFound = (char*)pBuffer;
        s32   iLast  = 0;
        s32   i;

        for( i=0; i<Count; i++ )
        {
            if( pFound[i] == '\n' )
            {
                static const u16 Data = (u16)((((u16)'\n') << 8) | (((u16)'\r') << 0));

                m_pDevice->Write( m_pFile, &pFound[iLast], i-iLast );
                m_pDevice->Write( m_pFile, &Data, 2 );

                // Update the base
                iLast = i+1;
            }
        };

        // Write the remendier
        if( iLast != i ) 
        {
            m_pDevice->Write( m_pFile, &pFound[iLast], i-iLast );
        }
    }
    else
    {
        m_pDevice->Write( m_pFile, pBuffer, TotalCount );
    }

    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_FORCE_FLUSH ) )
    {
        m_pDevice->Flush( m_pFile );
    }
    
    return TRUE;
}

//------------------------------------------------------------------------------

s32 xfile::Printf( const char* pFormatStr, ... )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);
    ASSERT(pFormatStr);

    xstring     String;
    xva_list    Args;

    x_va_start( Args, pFormatStr );

    s32 NChars = String.FormatV( pFormatStr, Args );

    WriteRaw( (const void*)(const char*)String, 1, NChars );
    
    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_FORCE_FLUSH ) )
    {
        m_pDevice->Flush( m_pFile );
    }

    return NChars;
}

//------------------------------------------------------------------------------

void xfile::ForceFlush( xbool bOnOff )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    if( bOnOff ) 
    {
        x_FlagOn( m_Flags, xfile_device_i::ACC_FORCE_FLUSH );
    }
    else         
    {
        x_FlagOff( m_Flags, xfile_device_i::ACC_FORCE_FLUSH );
    }
}

//------------------------------------------------------------------------------

void xfile::AsyncAbort( void )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_ASYNC ) == FALSE ) return; 
    
    m_pDevice->AsyncAbort( m_pFile );    
}

//------------------------------------------------------------------------------

xfile::sync_state xfile::Synchronize( xbool bBlock )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    if( x_FlagIsOn( m_Flags, xfile_device_i::ACC_ASYNC ) == FALSE ) 
    {
        if( IsEOF() ) return SYNC_EOF;
        return SYNC_COMPLETED;
    }

    return m_pDevice->Synchronize( m_pFile, bBlock );    
}

//------------------------------------------------------------------------------

void xfile::SwapEndian( xbool bOnOff )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    if( bOnOff ) 
    {
        x_FlagOn( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN );
    }
    else         
    {
        x_FlagOff( m_Flags, xfile_device_i::ACC_SWAP_ENDIAN );
    }
}

//------------------------------------------------------------------------------

void xfile::Flush( void )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);
    m_pDevice->Flush( m_pFile );
}

//------------------------------------------------------------------------------

void xfile::SeekOrigin( s32 Offset )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_ORIGIN, Offset );
}

//------------------------------------------------------------------------------

void xfile::SeekEnd( s32 Offset )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_END, Offset );
}

//------------------------------------------------------------------------------

void xfile::SeekCurrent( s32 Offset )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    m_pDevice->Seek( m_pFile, xfile_device_i::SKM_CURENT, Offset );
}

//------------------------------------------------------------------------------

s32 xfile::Tell( void )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    return m_pDevice->Tell( m_pFile );
}

//------------------------------------------------------------------------------

xbool xfile::IsEOF( void )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);

    return m_pDevice->IsEOF( m_pFile );
}

//------------------------------------------------------------------------------

s32 xfile::GetC( void )
{
    u8 C;
    if( Read( C ) == FALSE ) return -1;
    return C;
}

//------------------------------------------------------------------------------

void xfile::PutC( s32 aC, s32 Count, xbool bUpdatePos )
{
    s32 iPos;
    u8  C = aC;

    if( bUpdatePos == FALSE ) iPos = Tell();
    for( s32 i=0; i<Count; i++ )
    {
        Write( C );
    }
    if( bUpdatePos == FALSE ) SeekOrigin( iPos );
}

//------------------------------------------------------------------------------

void xfile::AlignPutC( s32 C, s32 Count, s32 Aligment, xbool bUpdatePos )
{
    // First solve the alignment issue
    s32 Pos      = Tell();
    s32 PutCount = x_Align(Count+Pos, Aligment ) - Pos;

    // Put all the necessary characters
    PutC( C, PutCount, bUpdatePos );
}

//------------------------------------------------------------------------------

s32 xfile::GetFileLength( void )
{
    ASSERT(m_pFile);
    ASSERT(m_pDevice);
    return m_pDevice->Length( m_pFile );
}

//------------------------------------------------------------------------------

void xfile::ToFile( xfile& File )
{
    // Seek at the begging of the file
    SeekOrigin( 0 );
    s32 i;
    u8  Buffer[2*256];
    s32 Length = GetFileLength();

    // Copy 256 bytes at a time
    Length -= 256;
    for( i=0; i<Length; i+=256 )
    {
        ReadRaw( Buffer, 1, 256 );
        File.WriteRaw( Buffer, 1, 256 );
    }
    Length += 256;

    // Write trailing bytes
    ReadRaw( Buffer, 1, Length-i );
    File.WriteRaw( Buffer, 1, Length-i );
}

//------------------------------------------------------------------------------

void xfile::ToMemory( void* pData, s32 BufferSize )
{
    // Seek at the begging of the file
    SeekOrigin( 0 );
    //s32 Length = GetFileLength();
    ASSERT( GetFileLength() >= BufferSize );

    ReadRaw( pData, BufferSize, 1 );
}

//------------------------------------------------------------------------------

xbool xfile::Write( const xstring& Val )
{
    for( s32 i=0; Val[i]; i++ )
        PutC( Val[i] );

    PutC( 0 );
    
    return TRUE;
}

//------------------------------------------------------------------------------
// TODO: Add the fail condition
xbool xfile::Read( xstring& Val )
{
    char Buffer[256];
    s32  Times = 0;
    s32  i;

    Val.Clear();

    for( i=0; (Buffer[i] = GetC()) != '\0'; i++ )
    {
        if( i > 253 )
        {
            Buffer[255]=0;
            if( Times > 0 )
            {
                Val.AppendFormat( Buffer );
            }
            else
            {
                Val.Format( Buffer );
            }

            Times++;
            i=0;
        }
    }

    ASSERT( Buffer[i]==0 );
    if( Times > 0 )
    {
        Val.AppendFormat( Buffer );
    }
    else
    {
        Val.Format( Buffer );
    }

    return TRUE;
}

//DOM-IGNORE-END
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PRINTF FUNCTIONALITY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void DefaultPrintF( const char* pString );
void DefaultPrintFXY( s32 X, s32 Y, const char* pString );

static xprint_fn*     s_fnpPrintf   = DefaultPrintF;
static xprint_xy_fn*  s_fnpPrintfXY = DefaultPrintFXY;

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pString  - pString should never be <b>NULL</b>. This pointer contains the string 
//                 that we need to display.
// Description:
//     This is the default behavier for the x_printf. This function can be 
//     overwritten by the user by calling x_SetFunctionPrintF
// See Also:
//     DefaultPrintFXY x_SetFunctionPrintF x_printfxy x_DebugMsg x_printf x_SetFunctionPrintFXY
//------------------------------------------------------------------------------
void DefaultPrintF( const char* pString )
{
    // Call the system printf. 
    printf( "%s", pString );
    LOGD(pString);
#ifdef X_DEBUG
    x_DebugMsg( "%s", pString );
#endif
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pString  - pString should never be <b>NULL</b>. This pointer contains the string 
//                 that we need to display.
//     X        - is the horizontal cordinate offset where the text 
//                 is going to be printed. Its range goes from [0,inf]</param>
//     Y        - Y is the vertical cordinate offset where the text is 
//                 going to be printed. Its range goes from [0,inf]</param>
// Description:
//     This is the default behavier for the x_printf. This function can be 
//     overwritten by the user by calling x_SetFunctionPrintFXY
//     X,Y are the corrdinates where the text is going to be printed at. 
//     Note that the cordinates are in characters <b>not</b> in pixels.
// See Also:
//     DefaultPrintF x_SetFunctionPrintF x_printfxy x_DebugMsg x_printf x_SetFunctionPrintFXY
//------------------------------------------------------------------------------
void DefaultPrintFXY( s32 X, s32 Y, const char* pString )
{
    // Call the system printf. 
    printf( "[%d,%d] %s\n", X, Y, pString );

#ifdef X_DEBUG
    x_DebugMsg( "[%d,%d] %s\n", X, Y, pString );
#endif
}


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     xfnpPrintf - Pointer to the function that is going to be use for now on.
//                   is you pass a <b>NULL</b> pointer it will reset the default behavier.
// Description:
//     The user can overwrite the behavier of x_printf by calling this function
//     and passing a pointer to a new function which should contain the new 
//     behavier.<br>Note that printf is a per-thread function. So when you 
//     overwrite one function in one thread you don't overwrite the behavier
//     for all the threads.
// See Also:
//     DefaultPrintF DefaultPrintFXY x_printfxy x_DebugMsg x_printf x_SetFunctionPrintFXY
//------------------------------------------------------------------------------
void x_SetFunctionPrintF( xprint_fn* fnpPrintf )
{
    if( fnpPrintf )
    {
        s_fnpPrintf = fnpPrintf;
    }
    else
    {
        s_fnpPrintf = DefaultPrintF;
    }
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     xfnpPrintfxy  - pointer to the function that is going to be use for now on.
//                      is you pass a <b>NULL</b> pointer it will reset the default behavier.
// Description:
//     The user can overwrite the behavier of x_printfxy by calling this function
//     and passing a pointer to a new function which should contain the new 
//     behavier.<br>Note that printfxy is a per-thread function. So when you 
//     overwrite one function in one thread you don't overwrite the behavier
//     for all the threads.
// See Also:
//     DefaultPrintF DefaultPrintFXY x_printfxy x_DebugMsg x_printf x_SetFunctionPrintF
//------------------------------------------------------------------------------
void x_SetFunctionPrintFXY( xprint_xy_fn* fnpPrintfxy )
{
    if( fnpPrintfxy )
    {
        s_fnpPrintfXY = fnpPrintfxy;
    }
    else
    {
        s_fnpPrintfXY = DefaultPrintFXY;
    }
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Print formatted output to the standard output stream.
// Arguments:
//	    pFormatStr	 - is a specially formated string which need decoding by this function.
//                     for more reference about how the formating works check x_sprintf.
//                     You should never pass as a <B>NULL</B>.
//      ellipsis     - Optional arguments needed by the Formated string.
// Returns:
//	    Returns the number of characters printed.
// Description:
//      This function is mainly inteded for debug printing. Its main porpuse 
//      is to print in the console screen similar how a command pront works in windows.
//      The behavier of this function should reasemble closely of the standard printf.
//      This function is usually overwritten by the system using x_SetFunctionPrintF.
//      So it is not encorage for the user to overwrite its behavier. 
// See Also:
//     x_sprintf DefaultPrintFXY x_printfxy x_DebugMsg x_SetFunctionPrintF
//------------------------------------------------------------------------------
s32 x_printf( const char* pFormatStr, ... )
{
    ASSERT( pFormatStr );

    s32         NChars;
    xva_list    Args;
    x_va_start( Args, pFormatStr );

    //
    // Build the string to be printed
    //
    xvfs XVFS( pFormatStr, Args );
    NChars = XVFS.GetLength();

    //
    // Print the string
    //
    s_fnpPrintf( XVFS );
  
    //
    // Done
    //
    return NChars;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Print formatted output to the standard output stream.
// Arguments:
//      X            - It is the horizontal cordinate offset where the text 
//                     is going to be printed. Its range goes from [0,inf]
//      Y            - It is the vertical cordinate offset where the text is 
//                      going to be printed. Its range goes from [0,inf]
//	    pFormatStr	 - is a specially formated string which need decoding by this function.
//                     for more reference about how the formating works check x_sprintf.
//                     You should never pass as a <B>NULL</B>.
//      ellipsis     - Optional arguments needed by the Formated string.
// Returns:
//	    Returns the number of characters printed.
// Description:
//      This function is mainly inteded for debug printing. It main porpuse 
//      is to print in an offset in the screen. The offset is in characters 
//      as the unit. So X=3, Y=0, means print 3 character in the horizontal 
//      (left to right) and 0 chracters in the vertical (top to bottom).
//      The rest of the parameters are very similar to the standard x_printf. 
//      This function is usually overwritten by the system using x_SetFunctionPrintFXY. 
//      So it is not encorage for the user to overwrite its behavier. 
// See Also:
//     x_sprintf x_vsprintf x_printf x_SetFunctionPrintFXY
//------------------------------------------------------------------------------
s32 x_printfxy( s32 X, s32 Y, const char* pFormatStr, ... )
{
    ASSERT( pFormatStr );

    s32         NChars;
    xva_list    Args;
    x_va_start( Args, pFormatStr );

    //
    // Build the string to be printed
    //
    xvfs XVFS( pFormatStr, Args );
    NChars = XVFS.GetLength();

    //
    // Print the string
    //
    s_fnpPrintfXY( X, Y, XVFS );
  
    //
    // Done
    //
    return NChars;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// VARIABLES 
//------------------------------------------------------------------------------
#ifdef TARGET_PC
static pc_device s_FileDevice;
#elif defined TARGET_IOS || defined TARGET_OSX
static darwin_file_device s_FileDevice;
#elif defined TARGET_MARMALADE
static mmlade_device s_FileDevice;
#elif defined TARGET_ANDROID
static android_device s_FileDevice;
#elif defined TARGET_3DS
static ctr_device s_FileDevice;
static ctr_data_device s_FileDataDevice;
#endif

void x_FileSystemAddRamDevice( void );
void x_FileSystemAddNetDevice( void );

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void x_FileSystemAddNetHost( const xnet_address& Address )
{
    // Find the net device
    xfile_device_i* pNetDevice = x_FileSystemFindDevice( "net:" );
    ASSERT(pNetDevice);

    // Set the IP adress
    pNetDevice->Init( &Address );
}

//------------------------------------------------------------------------------

void x_FileSystemAddDevice( xfile_device_i& Device )
{
    if( s_CriticalSection.isValid() )
    {
       xscope_atomic( s_CriticalSection );

        Device.m_pNext = s_pDevicelist;
        s_pDevicelist  = &Device;
    }
    else
    {
        Device.m_pNext = s_pDevicelist;
        s_pDevicelist  = &Device;
    }
}

//------------------------------------------------------------------------------

xfile_device_i* x_FileSystemFindDevice( const char* pDeviceName )
{
    xscope_atomic( s_CriticalSection );

    // User must be asking for the default device
    // Or if it is a temp folder we also will use the default device
    if ( pDeviceName == NULL || x_strncmp( pDeviceName, "temp:", sizeof( "temp:" )-1 ) == 0 )
    {
        return &s_FileDevice;
    }

    // Search throw all the devices    
    for( xfile_device_i* pDevice = s_pDevicelist; pDevice; pDevice = pDevice->m_pNext )
    {
        const char* a = pDevice->m_pName;
        for( s32 i=0; pDeviceName[i] && *a; i++, a++ )
        {
            if( pDeviceName[i] == ':' && *a == ':' )
                return pDevice;

            if( x_tolower(*a) != x_tolower(pDeviceName[i]) )
            {
                // Skip character and see if it has more devices maped
                while( *a != ':' && *a ) a++;

                // Check to see if a has more devices
                if( *a == ':' )
                {
                    if( a[1] )
                    {
                        i=-1;
                        continue;                        
                    }
                    break;
                }
            }
        }
    }

    return NULL;
}

//------------------------------------------------------------------------------

void x_FileSystemCacheFile( const char*	pFileName )
{
	// Make sure we are thread safe
	xscope_atomic( s_CriticalSection );

	// Make sure that we don't already have this file
	for( s32 i=0; i<s_FileCache.getCount(); i++ )
	{
		if( x_strcmp( pFileName, &s_FileCache[i].m_FileName[0] ) == 0 )
		{
			return;
		}
	}

	// Get ready to read the file
	xfile File;
	if( File.Open( pFileName, "rb" ) == FALSE )
	{
		ASSERT(0);
		return;
	}

	// OK we must create an entry
	file_cache& Cache = s_FileCache.append();
	Cache.m_FileName.Format( "cache:%s",pFileName );
	Cache.m_Data.Alloc( File.GetFileLength() );
	File.ReadRaw( &Cache.m_Data[0], 1, Cache.m_Data.getCount() );
}

//------------------------------------------------------------------------------

void x_StdioInit( void )
{
    // Initialize the device list
    s_pDevicelist=NULL;

#if defined TARGET_IOS || defined TARGET_OSX
    s_FileDevice.Init( NULL );
#elif defined TARGET_ANDROID
    android_device::Init();
#elif defined TARGET_3DS
     ctr_device::sInitialize();
     ctr_data_device::sInitialize();
#endif
    
    // add devices
    //x_FileSystemAddNetDevice();
    x_FileSystemAddRamDevice();
    x_FileSystemAddDevice( s_FileDevice );
#ifdef TARGET_3DS
    x_FileSystemAddDevice(s_FileDataDevice);
#endif
	x_FileSystemAddCacheDevice();
}

//------------------------------------------------------------------------------

void x_StdioKill( void )
{
    for( xfile_device_i* pDevice = s_pDevicelist; pDevice; pDevice = pDevice->m_pNext )
    {
        pDevice->Kill();
    }

    s_pDevicelist = NULL;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

struct x_file_cache_instance
{
	xhandle		m_hFile;
	s32			m_Pos;
};

class xfile_cache_device : public xfile_device_i
{
public:
								xfile_cache_device ( void ) : xfile_device_i("cache:"){}

protected:

    virtual void*               Open        ( const char* pFileName, u32 Flags );
	virtual void                Close       ( void* pFile ) { x_delete( (x_file_cache_instance*)pFile); }
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
	virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count ) { ASSERT(0); }
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
	virtual void                Flush       ( void* pFile ) {}
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock ){return xfile::SYNC_COMPLETED; }
    virtual void                AsyncAbort  ( void* pFile ){};
};

static xfile_cache_device s_CacheDevice;

void x_FileSystemAddCacheDevice( void )
{
    x_FileSystemAddDevice( s_CacheDevice );
}

//------------------------------------------------------------------------------

void* xfile_cache_device::Open( const char* pFileName, u32 Flags )
{
	s32 iFile=-1;

	// Make sure we are thread safe
	xscope_atomic( s_CriticalSection );

	// Make sure that we don't already have this file
	for( s32 i=0; i<s_FileCache.getCount(); i++ )
	{
		if( x_strcmp( pFileName, &s_FileCache[i].m_FileName[0] ) == 0 )
		{
			iFile = i;
			break;
		}
	}

	if( iFile == -1 ) return NULL;

	// create the intance
	x_file_cache_instance* pInstance = x_new( x_file_cache_instance, 1, 0 );
	pInstance->m_hFile = s_FileCache.GetHandleByIndex( iFile );
	pInstance->m_Pos   = 0;

	return pInstance;
}

//------------------------------------------------------------------------------

xbool xfile_cache_device::Read( void* pFile, void* pBuffer, s32 Count )
{
	// Make sure we are thread safe
	xscope_atomic( s_CriticalSection );

	x_file_cache_instance& Instance = *(x_file_cache_instance*)pFile;

	file_cache& FileCache = s_FileCache( Instance.m_hFile );

	// Check whether we are trying to read too much data
	if( (Instance.m_Pos + Count) > FileCache.m_Data.getCount() ) return FALSE;
	
	x_memcpy( pBuffer, &FileCache.m_Data[Instance.m_Pos], Count );

	Instance.m_Pos += Count;

	return TRUE;
}

//------------------------------------------------------------------------------

void xfile_cache_device::Seek( void* pFile, seek_mode Mode, s32 Pos )
{
	// Make sure we are thread safe
	xscope_atomic( s_CriticalSection );

	x_file_cache_instance& Instance = *(x_file_cache_instance*)pFile;

	file_cache& FileCache = s_FileCache( Instance.m_hFile );

	switch( Mode )
	{
	case xfile_device_i::SKM_ORIGIN:
		{
			ASSERT( Pos >= 0 );
			ASSERT( Pos < FileCache.m_Data.getCount() );
			Instance.m_Pos = Pos;
			break;
		}
	case xfile_device_i::SKM_CURENT:
		{
			Instance.m_Pos += Pos;
			ASSERT( Instance.m_Pos >= 0 );
			ASSERT( Instance.m_Pos < FileCache.m_Data.getCount() );
			break;
		}
	case xfile_device_i::SKM_END:
		{
			ASSERT( Pos >= 0 );
			ASSERT( Pos < FileCache.m_Data.getCount() );
			Instance.m_Pos = FileCache.m_Data.getCount()-Pos;
			break;
		}
	}
}

//------------------------------------------------------------------------------

s32 xfile_cache_device::Tell( void* pFile )
{
	x_file_cache_instance& Instance = *(x_file_cache_instance*)pFile;
	return Instance.m_Pos;
}

//------------------------------------------------------------------------------

s32 xfile_cache_device::Length( void* pFile )
{
	xscope_atomic( s_CriticalSection );

	x_file_cache_instance& Instance = *(x_file_cache_instance*)pFile;

	file_cache& FileCache = s_FileCache( Instance.m_hFile );

	return FileCache.m_Data.getCount();
}

//------------------------------------------------------------------------------

xbool xfile_cache_device::IsEOF( void* pFile )
{
	xscope_atomic( s_CriticalSection );

	x_file_cache_instance& Instance = *(x_file_cache_instance*)pFile;

	file_cache& FileCache = s_FileCache( Instance.m_hFile );

	return Instance.m_Pos >= FileCache.m_Data.getCount();
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#ifdef TARGET_PC
#include <direct.h>
#endif
xbool x_io::MakeDir( const char* pPath, s32 LevelsUp )
{
    s32                     NPaths=0;
    xsafe_array<s32, 64>    PathIndex;
    s32                     StrLen=0;
    
    ASSERT(pPath);
    ASSERT(LevelsUp >= 0);
    
    LevelsUp++;
    
    // Find the length of the string as well as how many paths does it contain
    for( StrLen=0; pPath[StrLen]; StrLen++ )
    {
        const u8& Value = pPath[StrLen];
        if( Value == '/' && StrLen > 0 ) PathIndex[ NPaths++ ] = StrLen;
    }
    
    ASSERT( StrLen > 0 );
    
    if( NPaths == 0 || PathIndex[ NPaths-1 ] != StrLen - 1 )
    {
        PathIndex[ NPaths++ ] = StrLen;
    }
    
    // Ok user said that he wants to make sure we create NLevels of dir for him
    // So we start from the the Level he asked us to.
    s32 Level = x_Max( NPaths - LevelsUp, 0 );
    
    for( s32 i = Level; i<NPaths; i++ )
    {
        char TempString[xfile::MAX_PATH];
        
        // Copy the string
        x_strncpy( TempString, pPath, PathIndex[ i ], xfile::MAX_PATH );
        
        // Now we can ask the system to create the path
#ifdef TARGET_PC
        s32 err = _mkdir( TempString );
#else
        const mode_t mode = 0777;
        s32 err = mkdir(TempString, mode);
#endif
        if ( err == -1 && errno == ENOENT )
            return FALSE;
    }
    
    return TRUE;
}

//------------------------------------------------------------------------------

#if defined TARGET_OSX || defined TARGET_IOS || defined TARGET_PC
#include <sys/stat.h>
#include <iostream>

#ifndef S_ISDIR
#define S_ISDIR(mode)  (((mode) & S_IFMT) == S_IFDIR)
#endif
//------------------------------------------------------------------------------

xbool x_io::FileDetails( const char* pPath, file_details& FileDetails )
{
    ASSERT( pPath );
    
    struct stat fileInfo;
    
    if (stat( pPath, &fileInfo) != 0)
        return FALSE;
    
    FileDetails.m_bDir          = (fileInfo.st_mode & S_IFMT) == S_IFDIR;
    FileDetails.m_FileSize      = fileInfo.st_size;

#ifndef TARGET_PC 
    FileDetails.m_MSTimeCreated  = fileInfo.st_ctimespec.tv_sec * (double)1000 + fileInfo.st_ctimespec.tv_nsec / (double)1000;
    FileDetails.m_MSTimeModified = fileInfo.st_mtimespec.tv_sec * (double)1000 + fileInfo.st_mtimespec.tv_nsec / (double)1000;
#else
    FileDetails.m_MSTimeCreated  = fileInfo.st_ctime * (double)1000 + fileInfo.st_ctime / (double)1000;
    FileDetails.m_MSTimeModified = fileInfo.st_mtime * (double)1000 + fileInfo.st_mtime / (double)1000;
#endif
    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_io::PathExists( const char* pPath )
{
	// The variable that holds the file information
    // the type stat and function stat have exactly the same names, so to refer the type, we put struct before it to indicate it is an structure.
	struct stat fileAtt;
    
	//Use the stat function to get the information
    //start will be 0 when it succeeds
    //So on non-zero, throw an exception
	if (stat(pPath, &fileAtt) != 0)
		return FALSE;
	
    return S_ISDIR(fileAtt.st_mode);
}

//------------------------------------------------------------------------------

xbool x_io::FileExists( const char* pFile )
{
	// The variable that holds the file information
    // the type stat and function stat have exactly the same names, so to refer the type, we put struct before it to indicate it is an structure.
	struct stat fileAtt;
    
	//Use the stat function to get the information
    //start will be 0 when it succeeds
    //So on non-zero, throw an exception
	if (stat(pFile, &fileAtt) != 0)
		return FALSE;

	//S_ISREG is a macro to check if the filepath referers to a file.
    //If you don't know what a macro is, it's ok, you can use S_ISREG as any other function, it 'returns' a bool.
	return !S_ISDIR(fileAtt.st_mode);
}

#elif defined TARGET_PC

#include <Windows.h>
#include <iostream>

using namespace std;

xbool x_io::PathExists( const char* pPath )
{
	//This will get the file attributes bitlist of the file
	DWORD fileAtt = GetFileAttributesA(pPath);
    
	//If an error occurred it will equal to INVALID_FILE_ATTRIBUTES
    //So lets throw an exception when an error has occurred
	if(fileAtt == INVALID_FILE_ATTRIBUTES)
		return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_io::FileExists( const char* pFile )
{
	//This will get the file attributes bitlist of the file
    DWORD fileAtt = GetFileAttributesA( pFile );
    
	//If an error occurred it will equal to INVALID_FILE_ATTRIBUTES
    //So lets throw an exception when an error has occurred
	if(fileAtt == INVALID_FILE_ATTRIBUTES)
		return FALSE;
    
	//If the path referers to a directory it should also not exists.
	return ( ( fileAtt & FILE_ATTRIBUTE_DIRECTORY ) == 0 );
}

#endif


