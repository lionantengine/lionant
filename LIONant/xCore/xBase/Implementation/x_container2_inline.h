
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::Grow( s32 NewItems )
{
    if( NewItems == 0 )
        return;

    //
    // Allocate the new count
    //
    s32 CurCount = m_Buffer.getCount();
    if( CurCount == 0 )
    {
        m_Buffer.Alloc( NewItems );
    }
    else
    {
        s32 NewCapacity = m_Count + NewItems;
        m_Buffer.Resize( NewCapacity );
    }
}

//------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::DeleteWithCollapse( s32 Index, s32 Count )
{
    s32& CurCount = m_Count;

    if( CurCount <= 0)
        return;
    
    ASSERT( Index >= 0 );
    ASSERT( (Index+Count) <= CurCount );
    
    //
    // Lets free all those guys
    //
    for( s32 i=0; i<Count; i++ )
    {
        x_Destruct( &m_Buffer.getEntry(Index+i) );
    }
    
    //
    // Okay if there is any full node lets move him down
    //
    s32 EndIndex = (Index+Count);
    s32 NewCount = CurCount - EndIndex;
    m_Buffer.Copy( Index, m_Buffer, EndIndex, NewCount );
    
    //
    // update the count
    //
    CurCount -= Count;
}

//------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::DeleteWithSwap( s32 Index, s32 Count )
{
    s32& CurCount = m_Count;

    if( CurCount <= 0)
        return;
    
    ASSERT( Index >= 0 );
    ASSERT( (Index+Count) <= CurCount );
    
    //
    // Lets free all those guys
    //
    for( s32 i=0; i<Count; i++ )
    {
        x_Destruct( &m_Buffer.getEntry(Index+i) );
    }
    
    //
    // Copy all the nodes from the end to where we are
    //
    if( (Index + Count) < CurCount )
    {
        s32 iSwap = CurCount-1;
        for( s32 i = 0; i<Count; i++ )
        {
            m_Buffer.getEntry( Index+i ) = m_Buffer.getEntry( iSwap-i );
        }
    }
    
    //
    // update the count
    //
    CurCount -= Count;
}

//------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::DeleteAllEntries( void )
{
    if ( m_Buffer.isValid() == FALSE )
        return;

    s32& CurCount = m_Count;
    if( CurCount == 0 )
        return;

    for( s32 i=0; i<CurCount; i++ )
    {
        x_Destruct( &m_Buffer.getEntry(i) );
    }

    CurCount = 0;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::Copy( const xarray2<T,D>& Array )
{
    if ( m_Buffer.isValid() == FALSE )
    {
        DefaultAlloc( x_Max( DEFAULT_GROWTH, Array.getCount() ) );
    }
    else
    {
        DeleteAllEntries();
        const s32 ArraySize  = Array.getCount();
        const s32 MyCapacity = m_Buffer.getCount();
        if( MyCapacity < ArraySize ) 
            Grow( x_Max( DEFAULT_GROWTH, ArraySize - MyCapacity) );
    }
    
    // Let make all our needed entries ready
    appendList( Array.getCount() );

    const_ref( mArray, Array );
    for( const T& Entry : mArray )
    {
        getEntry(mArray.getIterator()) = Entry;
    }
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
T& xarray2_interface<T,D>::Insert( s32 Index )
{
    if ( m_Buffer.isValid() == FALSE )
        DefaultAlloc( DEFAULT_GROWTH );

    s32 CurCount = m_Count;

    ASSERT( Index >= 0 );
    ASSERT( Index <= CurCount );
    
    //
    // Make sure that we have space
    //
    if( CurCount >= m_Buffer.getCount() )
        Grow( x_Max( CurCount/2, DEFAULT_GROWTH ) );
        
    //
    // Okay lets move all the elements
    //
    s32 Count = CurCount - Index;
    m_Buffer.Copy( Index+1, m_Buffer, Index, Count );
    
    //
    // Lets construct
    //
    T& Entry = m_Buffer.getEntry(Index);

    x_Construct( &Entry );
    m_Count++;
    
    return Entry;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
T& xarray2_interface<T,D>::append( s32& Index )
{
    //
    // make sure it is valid
    //
    if ( m_Buffer.isValid() == FALSE )
        DefaultAlloc( DEFAULT_GROWTH );

    //
    // Make sure that we have memory
    //
    {
        Index = m_Count;
        m_Count++;

        // If we grow we must assume info is invalid
        if ( Index >= m_Buffer.getCount() )
            Grow( m_Count/2  );
    }
        
    T& Entry = m_Buffer.getEntry(Index);
    x_Construct<T>( &Entry );
    
    return Entry;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
T& xarray2_interface<T,D>::append( void )
{
    s32 Index;
    return append( Index );
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xarray2_interface<T,D>::appendList( s32 Count )
{
    //
    // make sure it is valid
    //
    if ( m_Buffer.isValid() == FALSE )
        DefaultAlloc( x_Max( DEFAULT_GROWTH, Count ) );

    //
    // Make sure that we have memory
    //
    s32 CurCount = m_Count; 
    if( (CurCount+Count) >= m_Buffer.getCount() )
        Grow( x_Max( DEFAULT_GROWTH, Count ) );
    
    //
    // Construct all of them
    //
    Count += CurCount;
    
    for( s32 i=CurCount; i<Count; i++ )
    {
        x_Construct<T>( &m_Buffer.getEntry(i) );
    }
    
    // Set the new count and return
    m_Count = Count;
}


