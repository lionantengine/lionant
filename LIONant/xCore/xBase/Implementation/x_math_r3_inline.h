//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xradian3::xradian3( void )
{

}

//------------------------------------------------------------------------------
inline
xradian3::xradian3( const xradian3& R )
{
    *this = R;
}

//------------------------------------------------------------------------------
inline
xradian3::xradian3( xradian Pitch, xradian Yaw, xradian Roll ) : 
    m_Pitch(Pitch),
    m_Yaw(Yaw),
    m_Roll(Roll) {}

//------------------------------------------------------------------------------
inline
void xradian3::Zero( void )
{
    Set( 0, 0, 0 );
}

//------------------------------------------------------------------------------
inline
xradian3::xradian3( const xquaternion& Q )
{
    *this = Q.getRotation();
}

//------------------------------------------------------------------------------
inline
xradian3::xradian3( const xmatrix4& M )
{
    *this = M.getRotationR3();
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::Set( xradian Pitch, xradian Yaw, xradian Roll )
{
    m_Pitch = Pitch;
    m_Yaw   = Yaw;
    m_Roll  = Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::ModAngle( void )
{
    m_Pitch = x_ModAngle( m_Pitch );
    m_Yaw   = x_ModAngle( m_Yaw );
    m_Roll  = x_ModAngle( m_Roll );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3& xradian3::ModAngle2( void )
{
    m_Pitch = x_ModAngle2( m_Pitch );
    m_Yaw   = x_ModAngle2( m_Yaw );
    m_Roll  = x_ModAngle2( m_Roll );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian3 xradian3::GetMinAngleDiff( const xradian3& R ) const
{
    return xradian3( x_MinAngleDiff( m_Pitch, R.m_Pitch ),
                     x_MinAngleDiff( m_Yaw,   R.m_Yaw   ),
                     x_MinAngleDiff( m_Roll,  R.m_Roll  ) );
}

//------------------------------------------------------------------------------
inline
f32 xradian3::Difference( const xradian3& R ) const
{
    return ( m_Pitch - R.m_Pitch ) * ( m_Pitch - R.m_Pitch ) +
           ( m_Yaw   - R.m_Yaw   ) * ( m_Yaw   - R.m_Yaw   ) +
           ( m_Roll  - R.m_Roll  ) * ( m_Roll  - R.m_Roll  );
}

//------------------------------------------------------------------------------
inline
xbool xradian3::InRange( xradian Min, xradian Max ) const
{
    return ( m_Pitch >= Min ) && ( m_Pitch <= Max ) &&
           ( m_Yaw   >= Min ) && ( m_Yaw   <= Max ) &&
           ( m_Roll  >= Min ) && ( m_Roll  <= Max );
}

//------------------------------------------------------------------------------
inline
xbool xradian3::isValid( void ) const
{
    return x_isValid( m_Pitch ) && x_isValid( m_Yaw ) && x_isValid( m_Roll );
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator += ( const xradian3& R )
{
    m_Pitch += R.m_Pitch; 
    m_Yaw   += R.m_Yaw; 
    m_Roll  += R.m_Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator -= ( const xradian3& R )
{
    m_Pitch -= R.m_Pitch; 
    m_Yaw   -= R.m_Yaw; 
    m_Roll  -= R.m_Roll;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator *= ( f32 Scalar  )
{
    m_Pitch *= Scalar; 
    m_Yaw   *= Scalar; 
    m_Roll  *= Scalar;

    return *this;
}

//------------------------------------------------------------------------------
inline
const xradian3& xradian3::operator /= ( f32 Scalar  )
{
    Scalar   = 1.0f/Scalar;
    m_Pitch *= Scalar; 
    m_Yaw   *= Scalar; 
    m_Roll  *= Scalar;

    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xradian3::operator == ( const xradian3& R ) const
{
         if( x_Abs( R.m_Pitch - m_Pitch ) > XFLT_TOL) return FALSE;
    else if( x_Abs( R.m_Yaw   - m_Yaw   ) > XFLT_TOL) return FALSE;
    else if( x_Abs( R.m_Roll  - m_Roll  ) > XFLT_TOL) return FALSE;
    return TRUE;
}

//------------------------------------------------------------------------------
inline
xradian3 operator + ( const xradian3& R1, const xradian3& R2 )
{
    return xradian3( R1.m_Pitch + R2.m_Pitch,
                     R1.m_Yaw   + R2.m_Yaw,
                     R1.m_Roll  + R2.m_Roll );
}

//------------------------------------------------------------------------------
inline
xradian3 operator - ( const xradian3& R1, const xradian3& R2 )
{
    return xradian3( R1.m_Pitch - R2.m_Pitch,
                     R1.m_Yaw   - R2.m_Yaw,
                     R1.m_Roll  - R2.m_Roll );
}

//------------------------------------------------------------------------------
inline
xradian3 operator - ( const xradian3& R )
{
    return xradian3( -R.m_Pitch,
                     -R.m_Yaw,
                     -R.m_Roll );
}

//------------------------------------------------------------------------------
inline
    xradian3 operator * ( const xradian3& R, f32 S )
{
    return xradian3( R.m_Pitch * S,
                     R.m_Yaw   * S,
                     R.m_Roll  * S );
}

//------------------------------------------------------------------------------
inline
xradian3 operator / ( const xradian3& R, f32 S )
{
    S = 1.0f/S;
    return R * S;
}

//------------------------------------------------------------------------------
inline
xradian3 operator * ( f32 S, const xradian3& R )
{
    return xradian3( R.m_Pitch * S,
                     R.m_Yaw   * S,
                     R.m_Roll  * S );
}
