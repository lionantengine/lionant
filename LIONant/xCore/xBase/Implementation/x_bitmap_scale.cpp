//=========================================================================
//
// Bicubic scale sweet!
//
// http://codeguru.earthweb.com/bitmap/SmoothBitmapResizing.html
//
//=========================================================================
// INCLUDES
//=========================================================================
#include "x_base.h"

//=========================================================================
// FUNCTIONS
//=========================================================================

//=========================================================================
static
s32* CreateCoeffInt( s32 nLen, s32 nNewLen, xbool bShrink )
{
    s32     nSum   = 0, nSum2;
    s32*    pRes   = (s32*)x_malloc( sizeof(s32), 2 * nLen, 0 );
    s32*    pCoeff = pRes;
    s32     nNorm  = (bShrink) ? (nNewLen << 12) / nLen : 0x1000;
    s32	    nDenom = (bShrink) ? nLen : nNewLen;
    
    x_memset( pRes, 0, 2 * nLen * sizeof(s32) );
    
    for( s32 i = 0; i < nLen; i++, pCoeff += 2 )
    {
        nSum2 = nSum + nNewLen;
        if(nSum2 > nLen)
        {
            *pCoeff = ((nLen - nSum) << 12) / nDenom;
            pCoeff[1] = ((nSum2 - nLen) << 12) / nDenom;
            nSum2 -= nLen;
        }
        else
        {
            *pCoeff = nNorm;
            if(nSum2 == nLen)
            {
                pCoeff[1] = -1;
                nSum2     = 0;
            }
        }
        
        nSum = nSum2;
    }
    
    return pRes;
}

//=========================================================================
static
void ShrinkDataInt(
                   const xcolor*     pInBuff,
                   const s32         Width,
                   const s32         Height,
                   xcolor*           pOutBuff,
                   const s32         NewWidth,
                   const s32         NewHeight )
{
    s32     x, y, i, ii;
    xbool   bCrossRow, bCrossCol;
    
    xbyte*  pLine       = (xbyte*)pInBuff;
    xbyte*  pOutLine    = (xbyte*)pOutBuff;
    xbyte*  pPix;
    s32     InLn        = 4 * Width;
    s32     OutLn       = 4 * NewWidth;
    
    s32*    pRowCoeff   = CreateCoeffInt( Width,
                                         NewWidth,
                                         TRUE );
    
    s32*    pColCoeff   = CreateCoeffInt( Height,
                                         NewHeight,
                                         TRUE );
    
    s32*    pXCoeff;
    s32*    pYCoeff     = pColCoeff;
    
    s32     BuffLn      = 4 * NewWidth * sizeof(s32);
    s32*    pBuff       = (s32*)x_malloc( sizeof(s32), 8 * NewWidth, 0 );
    s32*    pCurrLn     = pBuff;
    s32*    pCurrPix;
    s32*    pNextLn     = pBuff + 4 * NewWidth;
    s32     Tmp;
    s32*    pNextPix;
    
    x_memset( pBuff, 0, 2 * BuffLn );
    
    y = 0;
    while( y < NewHeight )
    {
        pPix   = pLine;
        pLine += InLn;
        
        pCurrPix = pCurrLn;
        pNextPix = pNextLn;
        
        x = 0;
        pXCoeff   = pRowCoeff;
        bCrossRow = pYCoeff[1] > 0;
        
        while( x < NewWidth )
        {
            Tmp = (*pXCoeff) * (*pYCoeff);
            
            for(i = 0; i < 4; i++)
                pCurrPix[i] += Tmp * pPix[i];
            
            bCrossCol = pXCoeff[1] > 0;
            if( bCrossCol )
            {
                Tmp = pXCoeff[1] * (*pYCoeff);
                for(i = 0, ii = 4; i < 4; i++, ii++)
                    pCurrPix[ii] += Tmp * pPix[i];
            }
            
            if( bCrossRow )
            {
                Tmp = (*pXCoeff) * pYCoeff[1];
                for(i = 0; i < 4; i++)
                    pNextPix[i] += Tmp * pPix[i];
                
                if( bCrossCol )
                {
                    Tmp = pXCoeff[1] * pYCoeff[1];
                    for(i = 0, ii = 4; i < 4; i++, ii++)
                        pNextPix[ii] += Tmp * pPix[i];
                }
            }
            
            if(pXCoeff[1])
            {
                x++;
                pCurrPix += 4;
                pNextPix += 4;
            }
            
            pXCoeff += 2;
            pPix    += 4;
        }
        
        if(pYCoeff[1])
        {
            // set result line
            pCurrPix = pCurrLn;
            pPix     = pOutLine;
            
            for( i = 4 * NewWidth; i > 0; i--, pCurrPix++, pPix++)
                *pPix = ((xbyte*)pCurrPix)[3];
            
            // prepare line buffers
            pCurrPix = pNextLn;
            pNextLn  = pCurrLn;
            pCurrLn  = pCurrPix;
            
            x_memset( pNextLn, 0, BuffLn );
            
            y++;
            pOutLine += OutLn;
        }
        
        pYCoeff += 2;
    }
    
    x_free( pRowCoeff );
    x_free( pColCoeff );
    x_free( pBuff );
}

//=========================================================================
static
void EnlargeDataInt(
                    const xcolor*     pInBuff,
                    const s32         Width,
                    const s32         Height,
                    xcolor*           pOutBuff,
                    const s32         NewWidth,
                    const s32         NewHeight )
{
    xbyte*   pLine = (xbyte*)pInBuff;
    xbyte*   pPix  = pLine;
    xbyte*   pPixOld;
    xbyte*   pUpPix;
    xbyte*   pUpPixOld;
    
    xbyte*   pOutLine = (xbyte*)pOutBuff;
    xbyte*   pOutPix;
    
    s32     InLn  = 4 * Width;
    s32     OutLn = 4 * NewWidth;
    s32     x, y, i;
    xbool   bCrossRow, bCrossCol;
    
    s32*    pRowCoeff = CreateCoeffInt( NewWidth,
                                       Width,
                                       FALSE);
    
    s32*    pColCoeff = CreateCoeffInt( NewHeight,
                                       Height,
                                       FALSE);
    s32*    pXCoeff;
    s32*    pYCoeff = pColCoeff;
    s32     Tmp, PtTmp[4];
    
    y = 0;
    while( y < Height )
    {
        bCrossRow = pYCoeff[1] > 0;
        
        x = 0;
        pXCoeff   = pRowCoeff;
        pOutPix   = pOutLine;
        pOutLine += OutLn;
        pUpPix    = pLine;
        
        if(pYCoeff[1])
        {
            y++;
            pLine += InLn;
            pPix   = pLine;
        }
        
        while(x < Width)
        {
            bCrossCol = pXCoeff[1] > 0;
            pUpPixOld = pUpPix;
            pPixOld   = pPix;
            
            if( pXCoeff[1] )
            {
                x++;
                pUpPix += 4;
                pPix   += 4;
            }
            
            Tmp = (*pXCoeff) * (*pYCoeff);
            
            for(i = 0; i < 4; i++)
                PtTmp[i] = Tmp * pUpPixOld[i];
            
            if(bCrossCol)
            {
                Tmp = pXCoeff[1] * (*pYCoeff);
                
                for(i = 0; i < 4; i++)
                    PtTmp[i] += Tmp * pUpPix[i];
            }
            
            if(bCrossRow)
            {
                Tmp = (*pXCoeff) * pYCoeff[1];
                
                for(i = 0; i < 4; i++)
                    PtTmp[i] += Tmp * pPixOld[i];
                
                if( bCrossCol )
                {
                    Tmp = pXCoeff[1] * pYCoeff[1];
                    
                    for(i = 0; i < 4; i++)
                        PtTmp[i] += Tmp * pPix[i];
                }
            }
            
            for( i = 0; i < 4; i++, pOutPix++ )
                *pOutPix = ((xbyte*)(PtTmp + i))[3];
            
            pXCoeff += 2;
        }
        
        pYCoeff += 2;
    }
    
    x_free( pRowCoeff );
    x_free( pColCoeff );
}

//=========================================================================

void xbitmap::CreateResizedBitmap( xbitmap& Dest, s32 FinalWidth, s32 FinalHeight ) const
{
    // Make sure that the source has the right format
    ASSERT( getFormat() == xbitmap::FORMAT_XCOLOR || getFormat() == xbitmap::FORMAT_R8G8B8U8 );
    ASSERT( getWidth()  > 0 );
    ASSERT( getHeight() > 0 );
    ASSERT( FinalWidth  > 0 );
    ASSERT( FinalHeight > 0 );
    
    s32         SrcW                = getWidth();
    s32         SrcH                = getHeight();
    const xbool bEnlarge            = (FinalWidth > SrcW ) || (FinalHeight > SrcH);
    const xbool bShrink             = (FinalWidth < SrcW ) || (FinalHeight < SrcH);
    
    // Pick the src data
    xcolor*         pSrcData  = (xcolor*)getMip(0);
    
    // Do we need to Enlarge + Shrink the picture?
    if( bEnlarge && bShrink )
    {
        s32             DestW       = x_Max( FinalWidth, SrcW );
        s32             DestH       = x_Max( FinalHeight, SrcH );
        xptr<xcolor>    TempBuff;
        
        TempBuff.Alloc( DestW*DestH );
        
        EnlargeDataInt( pSrcData,
                        SrcW,
                        SrcH,
                        &TempBuff[0],
                        DestW,
                        DestH );
        
   
        // Setup the new bitmap
        Dest.CreateBitmap( FinalWidth, FinalHeight );

        // Now lets strink it
        ShrinkDataInt( &TempBuff[0],
                       DestW,
                       DestH,
                       (xcolor*)Dest.getMip(0),
                       FinalWidth,
                       FinalHeight );
    }
    else
    {
        xptr<xcolor>    TempBuff;
        
        // if src and dest is the same then lets back up the src
        if( this == &Dest )
        {
            // Copy the data into a temp storage
            TempBuff.Alloc( SrcW*SrcH );
            x_memcpy( &TempBuff[0], pSrcData, sizeof(xcolor) * SrcW*SrcH );
            
            // Update where the src is coming from
            pSrcData = &TempBuff[0];
        }
        
        // Create our final buffer
        Dest.CreateBitmap( FinalWidth, FinalHeight );
        
        if( bShrink )
        {
            ShrinkDataInt( pSrcData,
                           SrcW,
                           SrcH,
                           (xcolor*)Dest.getMip(0),
                           FinalWidth,
                           FinalHeight );
        }
        else
        {
            EnlargeDataInt( pSrcData,
                            SrcW,
                            SrcH,
                            (xcolor*)Dest.getMip(0),
                            FinalWidth,
                            FinalHeight );
        }
    }
}

