//==============================================================================
// INCLUDES
//==============================================================================

#include <stdio.h>
#include "../x_base.h"

#ifdef TARGET_PC
#include <direct.h>
#include <io.h>
#endif

//===============================================================================
// DEFINES
//===============================================================================

//===============================================================================
// STATIC MEMBERS
//===============================================================================

s32     x_command_system::m_nUndoSystemSize = 0;
xstring x_command_system::m_pUndoSystemPath;

//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================
//==============================================================================
//==============================================================================
// x_command_system CLASS
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

x_command_system::x_command_system( void )
{
}

//------------------------------------------------------------------------------

x_command_system::~x_command_system( void )
{
    Kill();
}

//------------------------------------------------------------------------------

void x_command_system::Init( s32 UndoSteps, const char* pUndoPath )
{
    m_nUndoSystemSize = UndoSteps;
    m_pUndoSystemPath.Format("%s",pUndoPath);
    m_UndoSystem.Init(m_nUndoSystemSize, m_pUndoSystemPath);
}

//------------------------------------------------------------------------------

void x_command_system::Kill( void )
{
    m_UndoSystem.Kill();
}

//------------------------------------------------------------------------------

xbool x_command_system::RegisterCommand( x_base_command* pCommand )
{
    ASSERT ( pCommand );
    //ASSERT ( !IsCommandRegistered(pCommand) );

    // Register the command for current editor
    if ( !IsCommandRegistered(pCommand) )
    {
        m_CommandArray.append() = pCommand;

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool x_command_system::ExecuteCommand( const char* pCmdline )
{
    ASSERT ( pCmdline );

    // Log
#ifdef X_DEBUG
    x_printf("[Execute Command] %s\n", pCmdline);
#endif

    // Skip spaces
    s32 nLength = x_strlen(pCmdline);
    s32 iStart = 0;
    while( (iStart < nLength) && x_isspace(pCmdline[iStart]) ) 
    {
        ++iStart;
    }

    // Get command name and arguments from command line
    xstring CmdNameStr;
    xstring CmdlineStr;
    if ( iStart < nLength )
    {
        // When have command name
        s32 iEnd = iStart;
        while( (iEnd < nLength) && !x_isspace(pCmdline[iEnd]) ) // find the end
        {
            ++iEnd;
        }

        CmdNameStr.Copy(pCmdline + iStart, iEnd - iStart);
        CmdlineStr.Copy(pCmdline + iEnd);
    }
    else
    {
        // When have no command name
        x_throw( "Error: Empty command line" );
        return FALSE;
    }

    // Process for Help command
    if ( IsHelpCommand(CmdNameStr) )
    {
        ExecuteHelpCommand(CmdlineStr);
        return TRUE;
    }

    // Find command by name
    x_base_command* pCommand = FindCommand(CmdNameStr);
    if ( !pCommand )
    {
        x_throw( "Error: Can not find command:[%s]", (const char*)CmdNameStr );
        return FALSE;
    }

    // Validate command line
    if ( !pCommand->ValidateCommand(CmdlineStr) )
    {
        x_throw( "Error: Invalid command line:[%s%s]", (const char*)CmdNameStr, (const char*)CmdlineStr );
        return FALSE;
    }

    // Get new step from undo stack
    x_undo_step& Step = m_UndoSystem.GetNewUndoStep();
    Step.m_Command.Copy(pCommand->GetName());

    // Execute command and save undo/redo states to files
    pCommand->ExecuteCommand(Step);

    return TRUE;
}

//------------------------------------------------------------------------------

xbool x_command_system::Undo( void )
{
    // Undo command
    if ( m_UndoSystem.CanUndo() )
    {
        x_undo_step& Step = m_UndoSystem.UndoStep();
        x_base_command* pCommand = FindCommand(Step.m_Command);
        pCommand->LoadUndoState(Step.m_UndoStateFile);

        // Log
#ifdef X_DEBUG
        x_printf("[Undo Command] %s\n", (const char*)Step.m_Command);
#endif

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool x_command_system::Redo( void )
{
    // Redo command
    if ( m_UndoSystem.CanRedo() )
    {
        x_undo_step& Step = m_UndoSystem.RedoStep();
        x_base_command* pCommand = FindCommand(Step.m_Command);
        pCommand->LoadRedoState(Step.m_RedoStateFile);

        // Log
#ifdef X_DEBUG
        x_printf("[Redo Command] %s\n", (const char*)Step.m_Command);
#endif

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool x_command_system::IsCommandRegistered( const x_base_command* pCommand ) const
{
    ASSERT ( pCommand );

    // Check if the command is already registered
    return ( FindCommand(pCommand->GetName()) != NULL );
}

//------------------------------------------------------------------------------

x_base_command* x_command_system::FindCommand( const char* pName ) const
{
    ASSERT ( pName );

    // Find the command by name
    for ( s32 i=0; i<m_CommandArray.getCount(); ++i )
    {
        if ( x_strcmp(m_CommandArray[i]->GetName(), pName) == 0 )
        {
            return m_CommandArray[i];
        }
    }

    return NULL;
}

//------------------------------------------------------------------------------

xbool x_command_system::CanUndo( void ) const
{
    return m_UndoSystem.CanUndo();
}

//------------------------------------------------------------------------------

xbool x_command_system::CanRedo( void ) const
{
    return m_UndoSystem.CanRedo();
}

//------------------------------------------------------------------------------

xbool x_command_system::IsHelpCommand( const char* pName ) const
{
    ASSERT ( pName );

    if ( x_strcmp(pName, "Help") == 0 )
    {
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool x_command_system::ExecuteHelpCommand( const char* pCmdline ) const
{
    ASSERT ( pCmdline );

    xcmdline CmdLine;

    // Parse the command line
    CmdLine.Parse( pCmdline );

    // Handle parameters
    if ( CmdLine.GetArgumentCount() == 1 )
    {
        x_base_command* pCommand = FindCommand(CmdLine.GetArgument(0));
        if ( pCommand )
        {
            x_printf("%s", pCommand->GetHelp());
        }
        else
        {
            x_throw( "Error: Can not find this command:[%s]", (const char*)CmdLine.GetArgument(0) );
            return FALSE;
        }
    }
    else
    {
        x_throw( "Error: 1 argument is needed:[Help %s]", pCmdline);
        return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

void x_command_system::InitUndoSystem( s32 nUndoSize, const char* pUndoPath )
{
    // Make sure this function only be called once
    static xbool s_bFlag = TRUE;
    ASSERT ( s_bFlag );
    s_bFlag = FALSE;

    ASSERT ( nUndoSize > 0 );
    ASSERT ( pUndoPath );
    ASSERT ( x_strlen(pUndoPath) > 0 );

    m_nUndoSystemSize = nUndoSize;
    m_pUndoSystemPath.Copy(pUndoPath);

    // Make sure the end of path has one '/'
    s32 iLast = m_pUndoSystemPath.GetLength() - 1;
    if ( m_pUndoSystemPath[iLast] != '\\' && m_pUndoSystemPath[iLast] != '/' )
    {
        m_pUndoSystemPath.AppendFormat("/");
    }

#ifdef TARGET_PC
    // Remove all undo files under tmp path
    _finddata_t filestruct;
    intptr_t handle = _findfirst(xfs("%s*", m_pUndoSystemPath), &filestruct);
    if ( handle != -1 )
    {
        do
        {
            if ( ( filestruct.attrib & _A_SUBDIR ) == 0 )
            {
                remove(xfs("%s%s", m_pUndoSystemPath, filestruct.name));
            }
        } while ( _findnext(handle, &filestruct) != -1 );
    }
    _findclose(handle);
    
#elif defined TARGET_IOS || TARGET_ANDROID
    
#endif
}

//------------------------------------------------------------------------------

void x_command_system::Reset( void )
{
    m_UndoSystem.Reset();
}

//==============================================================================
//==============================================================================
//==============================================================================
// x_undo_system CLASS
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------
x_undo_system::x_undo_system( void )
{
    //Init(1, "");
}

//------------------------------------------------------------------------------

x_undo_system::~x_undo_system( void )
{
    //Kill();
}

//------------------------------------------------------------------------------

void x_undo_system::Init( s32 nMaxStepCount, const char* pFilePath )
{
#define IS_SLASH(c)     ( (c) == '\\' || (c) == '/' )

    ASSERT ( nMaxStepCount > 0 );
    ASSERT ( pFilePath );
    ASSERT ( x_strlen(pFilePath) > 0 );

    // Additionally allocate one node for end node when the stack is full
    m_Stack.New(nMaxStepCount + 1);

    m_iCurrent = 0;
    m_FilePath.Copy(pFilePath);

    // Make sure the end of path has one '/'
    if ( !IS_SLASH(m_FilePath[m_FilePath.GetLength()-1]) )
    {
        m_FilePath.AppendFormat("/");
    }

    // Create directory if it doesn't exist
    //if ( _mkdir(m_FilePath) == -1 && errno != EEXIST )
    //{
    //    char errorMsg[256];
    //    strerror_s(errorMsg, sizeof(errorMsg), errno);
    //    x_throw( xfs("%s:[%s]\n", errorMsg, m_FilePath) );
    //}
#ifdef TARGET_PC
    for ( s32 i=0; i<m_FilePath.GetLength(); ++i )
    {
        if ( IS_SLASH(m_FilePath[i]) && !IS_SLASH(m_FilePath[i+1]) )
        {
            xstring strTmpPath;
            strTmpPath.Copy(m_FilePath, i);
            _mkdir(strTmpPath);
        }
    }
#elif defined TARGET_IOS || TARGET_ANDROID
    
#endif
}

//------------------------------------------------------------------------------

void x_undo_system::Kill( void )
{
    // Delete all undo/redo files
    for ( s32 i=0; i<m_Stack.getCount(); ++i )
    {
        DeleteFiles(m_Stack[i]);
    }
}

//------------------------------------------------------------------------------

void x_undo_system::Reset( void )
{
    // Store initialization status
    s32     nMaxStepCount = m_Stack.getCount() - 1;
    xstring FilePath(m_FilePath);

    // Kill and Init
    Kill();
    Init(nMaxStepCount, FilePath);
}

//------------------------------------------------------------------------------

xbool x_undo_system::CanUndo( s32 nSteps /*= 1 */ ) const
{
    ASSERT ( nSteps > 0 );

    s32 iPrevStep = m_Stack[GetPrev()].m_iStep;

    // If previous step is -1, means no steps before current step(can't undo)
    if ( iPrevStep < 0 )
    {
        return FALSE;
    }

    // For single undo, already checked previous step so return TRUE
    if ( nSteps == 1 )
    {
        return TRUE;
    }

    // For multiple undo, go through the stack nodes to get the available undo sum
    s32 nSum = 0;
    for ( s32 i=0; i<m_Stack.getCount(); ++i )
    {
        if ( m_Stack[i].m_iStep >= 0 && m_Stack[i].m_iStep <= iPrevStep )
        {
            ++nSum;
        }
    }
    return ( nSum >= nSteps );
}

//------------------------------------------------------------------------------

xbool x_undo_system::CanRedo( s32 nSteps /*= 1 */ ) const
{
    ASSERT ( nSteps > 0 );

    s32 iCurStep = m_Stack[m_iCurrent].m_iStep;

    // If current step is -1, means no steps after previous step(can't redo)
    if ( iCurStep < 0 )
    {
        return FALSE;
    }

    // For single redo, already checked current step so return TRUE
    if ( nSteps == 1 )
    {
        return TRUE;
    }

    // For multiple redo, go through the stack nodes to get the available redo sum
    s32 nSum = 0;
    for ( s32 i=0; i<m_Stack.getCount(); ++i )
    {
        if ( m_Stack[i].m_iStep >= iCurStep )
        {
            ++nSum;
        }
    }
    return ( nSum >= nSteps );
}

//------------------------------------------------------------------------------

x_undo_step& x_undo_system::UndoStep( void )
{
    //ASSERT ( CanUndo() );

    // Return previous node and set it as current node
    x_undo_step& Step = m_Stack[GetPrev()];
    m_iCurrent = GetPrev();

    // Make sure the file pointer is at the beginning
    Step.m_UndoStateFile.SeekOrigin(0);

    return Step;
}

//------------------------------------------------------------------------------

x_undo_step& x_undo_system::RedoStep( void )
{
    //ASSERT ( CanRedo() );

    // Return current node and set next as current node
    x_undo_step& Step = m_Stack[m_iCurrent];
    m_iCurrent = GetNext();

    // Make sure the file pointer is at the beginning
    Step.m_RedoStateFile.SeekOrigin(0);

    return Step;
}

//------------------------------------------------------------------------------

x_undo_step& x_undo_system::GetNewUndoStep( void )
{
    s32 iPrevStep = m_Stack[GetPrev()].m_iStep;

    // Clear unused nodes after current step
    for ( s32 i=0; i<m_Stack.getCount(); ++i )
    {
        if ( m_Stack[i].m_iStep > iPrevStep )
        {
            ResetStep(m_Stack[i]);
        }
    }

    // Initialize the new node (MUST be here)
    x_undo_step& Step = m_Stack[m_iCurrent];
    Step.m_iStep = iPrevStep + 1;
    CreateFiles(Step);
    

    // Move current index to next node and make sure current node is cleared (means it's the end of stack)
    m_iCurrent = GetNext();
    ResetStep(m_Stack[m_iCurrent]);

    return Step;
}

//------------------------------------------------------------------------------

s32 x_undo_system::GetPrev( void ) const
{
    // Check and return the previous node's index
    s32 iPrev = m_iCurrent - 1;
    if ( iPrev < 0 )
    {
        iPrev = m_Stack.getCount() - 1;
    }
    return iPrev;
}

//------------------------------------------------------------------------------

s32 x_undo_system::GetNext( void ) const
{
    // Check and return the next node's index
    s32 iNext = m_iCurrent + 1;
    if ( iNext >= m_Stack.getCount() )
    {
        iNext = 0;
    }
    return iNext;
}

//------------------------------------------------------------------------------

xstring x_undo_system::GetUndoFileName( s32 iStep ) const
{
    return xstring::BuildFromFormat("%s%d_UNDO_%d.TMP", (const char*)m_FilePath, this, iStep);
}

//------------------------------------------------------------------------------

xstring x_undo_system::GetRedoFileName( s32 iStep ) const
{
    return xstring::BuildFromFormat("%s%d_REDO_%d.TMP", (const char*)m_FilePath, this, iStep);
}

//------------------------------------------------------------------------------

void x_undo_system::CreateFiles( x_undo_step& Step )
{
    // Only available for active step
    ASSERT ( Step.m_iStep >= 0 );

    // Make sure clossed the old files and cleared the xfiles before reopen
    Step.m_UndoStateFile.Close();
    Step.m_RedoStateFile.Close();

    // Create files for both read and write
    if ( !Step.m_UndoStateFile.Open(GetUndoFileName(Step.m_iStep), "w") )
    {
        x_throw( "Error: Can not create undo file:[%s]", (const char*)GetUndoFileName(Step.m_iStep) );
        return;
    }
    if ( !Step.m_RedoStateFile.Open(GetRedoFileName(Step.m_iStep), "w") )
    {
        x_throw( "Error: Can not create redo file:[%s]", (const char*)GetRedoFileName(Step.m_iStep) );
        return;
    }

    // Force flush
    Step.m_UndoStateFile.ForceFlush(TRUE);
    Step.m_RedoStateFile.ForceFlush(TRUE);

    // Make sure the file pointers are at the beginning
    Step.m_UndoStateFile.SeekOrigin(0);
    Step.m_RedoStateFile.SeekOrigin(0);

}

//------------------------------------------------------------------------------

void x_undo_system::DeleteFiles( x_undo_step& Step )
{
    // If current step is not active, DeleteFile will do nothing
    if ( Step.m_iStep < 0 )
    {
        return;
    }

    // Make sure clossed the old files and cleared the xfiles before delete
    Step.m_UndoStateFile.Close();
    Step.m_RedoStateFile.Close();

    // Delete files
    remove(GetUndoFileName(Step.m_iStep));
    remove(GetRedoFileName(Step.m_iStep));

}

//------------------------------------------------------------------------------

void x_undo_system::ResetStep( x_undo_step& Step )
{
    // Delete files and reset the step status
    DeleteFiles(Step);
    Step.Clear();
}

//==============================================================================
//==============================================================================
//==============================================================================
// x_undo_step STRUCT
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

x_undo_step::x_undo_step( void )
{
    Clear();
}

//------------------------------------------------------------------------------

x_undo_step::~x_undo_step( void )
{

}

//------------------------------------------------------------------------------

void x_undo_step::Clear( void )
{
    m_iStep = -1;
    m_Command.Clear();
    m_UndoStateFile.Close();
    m_RedoStateFile.Close();
}
//==============================================================================
//==============================================================================
//==============================================================================
// x_base_command CLASS
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

x_base_command::x_base_command( void )
{

}

//------------------------------------------------------------------------------

x_base_command::~x_base_command( void )
{

}
