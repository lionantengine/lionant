//==============================================================================
// INCLUDES
//==============================================================================
#include "../x_Base.h"

//==============================================================================
// MACROS
//==============================================================================
#define COMP_BIT(A,B) u32(((1<<(A))-1)<<(B)) 
#define COMP_SFT(A,B) (((A)-8)+(B))

#define DESC_ARGB(A,AL,R,RL,G,GL,B,BL) (A+R+G+B),                                                      \
                                       COMP_SFT(A,AL), COMP_SFT(R,RL), COMP_SFT(G,GL), COMP_SFT(B,BL), \
                                       COMP_BIT(A,AL), COMP_BIT(R,RL), COMP_BIT(G,GL), COMP_BIT(B,BL), 


#define BUILD_RGBA(R,G,B,A) DESC_ARGB( A,R+G+B,   R, 0,      G,B,     B,G+B )
#define BUILD_RGBU(R,G,B,U) DESC_ARGB( 0,R+G+B,   R, 0,      G,B,     B,G+B )

#define BUILD_ABGR(A,B,G,R) DESC_ARGB( A,0,       R,G+B+A,   G,B+A,   B,A )
#define BUILD_UBGR(U,B,G,R) DESC_ARGB( 0,0,       R,G+B+U,   G,B+U,   B,U )

#define BUILD_URGB(U,R,G,B) DESC_ARGB( 0,0,       R,U,       G,R+U,   B,G+R+U )
#define BUILD_ARGB(A,R,G,B) DESC_ARGB( A,0,       R,A,       G,R+A,   B,G+R+A )

#define BUILD_BGRA(B,G,R,A) DESC_ARGB( A,R+G+B,   R,G+B,     G,B,     B,0 )
#define BUILD_BGRU(B,G,R,U) DESC_ARGB( 0,R+G+B,   R,G+B,     G,B,     B,0 )


//==============================================================================
// VARIABLES
//==============================================================================
const xcolor::fmt_desc xcolor::g_FormatDesc[]=
{
    { FMT_NULL,                            0,   0, BUILD_ARGB(0,0,0,0)  },
    { FMT_16_ABGR_4444, (1<<FMT_16_ABGR_4444), 16, BUILD_ABGR(4,4,4,4)  },
    { FMT_16_ARGB_4444, (1<<FMT_16_ARGB_4444), 16, BUILD_ARGB(4,4,4,4)  },
    { FMT_16_RGBA_4444, (1<<FMT_16_RGBA_4444), 16, BUILD_RGBA(4,4,4,4)  },
    { FMT_16_RGB_565,   (1<<FMT_16_RGB_565  ), 16, BUILD_RGBU(5,6,5,0)  },
    { FMT_16_BGR_565,   (1<<FMT_16_BGR_565  ), 16, BUILD_BGRU(5,6,5,0)  },
    { FMT_16_ARGB_1555, (1<<FMT_16_ARGB_1555), 16, BUILD_ARGB(1,5,5,5)  },
    { FMT_16_RGBA_5551, (1<<FMT_16_RGBA_5551), 16, BUILD_RGBA(5,5,5,1)  },
    { FMT_16_URGB_1555, (1<<FMT_16_URGB_1555), 16, BUILD_URGB(1,5,5,5)  },
    { FMT_16_RGBU_5551, (1<<FMT_16_RGBU_5551), 16, BUILD_RGBU(5,5,5,1)  },
    { FMT_16_ABGR_1555, (1<<FMT_16_ABGR_1555), 16, BUILD_ABGR(1,5,5,5)  },
    { FMT_16_UBGR_1555, (1<<FMT_16_UBGR_1555), 16, BUILD_UBGR(1,5,5,5)  },
                                
    { FMT_24_RGB_888,   (1<<FMT_24_RGB_888  ), 24, BUILD_RGBU(8,8,8,0)  },
    { FMT_24_ARGB_8565, (1<<FMT_24_ARGB_8565), 24, BUILD_ARGB(8,5,6,5)  },

    { FMT_32_RGBU_8888, (1<<FMT_32_RGBU_8888), 32, BUILD_RGBU(8,8,8,8)  },
    { FMT_32_URGB_8888, (1<<FMT_32_URGB_8888), 32, BUILD_URGB(8,8,8,8)  },
    { FMT_32_ARGB_8888, (1<<FMT_32_ARGB_8888), 32, BUILD_ARGB(8,8,8,8)  },
    { FMT_32_RGBA_8888, (1<<FMT_32_RGBA_8888), 32, BUILD_RGBA(8,8,8,8)  },
    { FMT_32_ABGR_8888, (1<<FMT_32_ABGR_8888), 32, BUILD_ABGR(8,8,8,8)  },
    { FMT_32_BGRA_8888, (1<<FMT_32_BGRA_8888), 32, BUILD_BGRA(8,8,8,8)  },
};

const xcolor::best_match xcolor::g_Match[]=
{
    { FMT_NULL },
    { FMT_16_ARGB_4444, FMT_16_RGBA_4444, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_16_RGBA_4444, FMT_16_ARGB_4444, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_16_RGB_565  , FMT_16_URGB_1555, FMT_16_RGBU_5551, FMT_16_UBGR_1555, FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_END },
    { FMT_16_ARGB_1555, FMT_16_RGBA_5551, FMT_16_ABGR_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_16_RGBA_5551, FMT_16_ARGB_1555, FMT_16_ABGR_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_16_URGB_1555, FMT_16_RGBU_5551, FMT_16_UBGR_1555, FMT_16_RGB_565,   FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_END },
    { FMT_16_RGBU_5551, FMT_16_URGB_1555, FMT_16_UBGR_1555, FMT_16_RGB_565,   FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_END },
    { FMT_16_ABGR_1555, FMT_16_RGBA_5551, FMT_16_ARGB_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_24_RGB_888  , FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_END },
    { FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_24_RGB_888  , FMT_END },
    { FMT_32_URGB_8888, FMT_32_RGBU_8888, FMT_24_RGB_888  , FMT_END },
    { FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_32_RGBA_8888, FMT_32_ARGB_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_END },
    { FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_END },
    { FMT_32_BGRA_8888, FMT_32_ABGR_8888, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_END },
};

//------------------------------------------------------------------------------
// The Hue/Staturation Value system (or also called Hexcone model) was created by
// Smith in 1978. It was for an aesthetic purpose, accessing color by family,
// purety and intensity rather than by its component. With that model it becomes
// easy to produce some kind of hellish brown or some kind of
// you-know-that-color-in-between-(x) and (y).
// The H value is a 360 degree value through color families.
// The S (or Staturation) is the degree of strength of a color. Greater is S, the
// purest is the color. if S max is 100, then Hue=red and S=100 would produce an
// intense red (reproduced by RGB (max,0,0)) 
// Finally, the V, for value, is the darkness/lightness of a color. More V is
// great, more the color is close to white.
//------------------------------------------------------------------------------
void xcolor::BuildHSV( f32& H, f32& S, f32& V ) const
{
    f32 Max, Min;
    f32 h,s,v;
    s32   i;

    Min = (m_R<m_G) ? m_R : m_G;
    Min = (Min<m_B) ? Min : m_B;
    
    Max=m_R; i=0;
    if( Max < m_G ) { Max=m_G; i=1; }
    if( Max < m_B ) { Max=m_B; i=2; }

    // convert to parametric
    Max *= (1/255.0f);
    Min *= (1/255.0f);

    // compute hsv
    v = Max;  
    s = (Max != 0) ? ((Max-Min)/Max) : 0;

    if( s == 0 )
    {
        // Assume h (Undefined!)
        H = 0;
        S = s;
        V = v;
    }
    else
    {
        f32 Delta = 1.0f/(Max-Min);

        switch( i )
        {
        case 0: h = (m_G-m_B)*Delta;            break;
        case 1: h = 2.0f + (m_B-m_R)*Delta;     break;
        case 2: h = 4.0f + (m_R-m_G)*Delta;     break;
        }

        h *= X_RADIAN(60);

        if( h < 0 )
        {
            h += X_RADIAN(360);
        }

        H = h;
        S = s;
        V = v;
    }
}

//------------------------------------------------------------------------------
void xcolor::SetFromHSV( f32 H, f32 S, f32 V )
{
    f32 h,s,v;
    f32 r,g,b;

    h = H;
    s = S;
    v = V;

    if( s == 0 )
    {
        r = v;
        g = v;
        b = v;
    }
    else
    {
        f32 f,p,t,q;
        s32   i;

        if( h == (X_RADIAN(360)) ) h = 0;

        h /= (X_RADIAN(60));
        i = (s32)h;
        f = h-i;
        p = v*(1-(s));
        q = v*(1-(s*(f)));
        t = v*(1-(s*(1-f)));

        switch( i )
        {
        case 0: r = v; g = t; b = p; break;
        case 1: r = q; g = v; b = p; break;
        case 2: r = p; g = v; b = t; break;
        case 3: r = p; g = q; b = v; break;
        case 4: r = t; g = p; b = v; break;
        case 5: r = v; g = p; b = q; break;
            default: b=g=r=0; ASSERT(0);
        }
    }

    r *= 255;
    g *= 255;
    b *= 255;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}
