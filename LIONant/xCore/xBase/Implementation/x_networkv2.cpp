#include<stdio.h>

#include "../x_target.h"

#ifdef TARGET_PC
    #include <windows.h>
    #include <winsock.h>
    #include <process.h>
    #include <math.h>

typedef int socklen_t;
typedef int ssize_t;

    #pragma comment( lib, "ws2_32.lib" )
#endif

#ifdef TARGET_360
	#include <process.h>
	#include <xtl.h>
#endif

#if defined TARGET_IOS || defined TARGET_OSX
    #include <alloca.h>
    #include <netdb.h>
    #include <unistd.h>
    #include <errno.h>
    #include <sys/socket.h>
    #include <sys/ioctl.h>
    #include <sys/fcntl.h>
    #include <netinet/in.h>

    #define SOCKADDR_IN     sockaddr_in
    #define INVALID_SOCKET  (-1)
    #define SOCKET_ERROR    (-1)

    #define WSAEADDRINUSE   EADDRINUSE
    #define WSAECONNRESET   ECONNRESET
    #define WSAEWOULDBLOCK  EWOULDBLOCK

    typedef int             SOCKET;
    typedef struct sockaddr SOCKADDR;
#endif

#include "x_base.h"

/////////////////////////////////////////////////////////////////////////////////
// DEFINES
/////////////////////////////////////////////////////////////////////////////////
//DOM-IGNORE-BEGIN
#define NETLOG  "Network"
//DOM-IGNORE-END

//#define NETWORKDEBUG

/////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

s32 x_GetLastSocketError( void )
{
#ifdef TARGET_PC
    return WSAGetLastError();
#elif defined TARGET_IOS || defined TARGET_OSX
    return errno;
#endif
}

//-------------------------------------------------------------------------------
void x_NetInit( void )
{
#if defined TARGET_PC
	WSADATA     ws_data;
	WORD        ver = MAKEWORD(2,2);
	s32         Err = WSAStartup(ver, &ws_data);

	if( Err ) 
    {
		x_LogError( NETLOG, "There was an error initializing networking! Error=%d", Err );
		return;
	}
#else
    // Nothing to do for IOS/OSX
#endif
    
    // Send message
	x_LogMessage( NETLOG, "XNetwork initted successfully!" );
}

//------------------------------------------------------------------------------

void x_NetKill( void )
{
#if defined TARGET_PC
    WSACleanup();
#else
    // Nothing to do for IOS/OSX
#endif
}

//==============================================================================
//==============================================================================
//==============================================================================
// ADDRESS
//==============================================================================
//==============================================================================
//==============================================================================


//------------------------------------------------------------------------------

xnet_address::xnet_address( void )
{
    Clear();
}

//------------------------------------------------------------------------------

void xnet_address::Clear( void )
{
    m_IP   = 0;
    m_Port = 0; 
}

//------------------------------------------------------------------------------

xbool xnet_address::isValid( void ) const
{
    if( m_IP ==      0x00000000 ) return FALSE;
    if( m_IP ==      0x7F000001 ) return FALSE;
    if( m_IP == (s32)0xFFFFFFFF ) return FALSE;
    if( m_Port == 0 )             return FALSE;

    return( TRUE );
}

//------------------------------------------------------------------------------

void xnet_address::Setup( u32 IP, s32 Port )
{
    m_IP   = IP;
    m_Port = Port;
}

//------------------------------------------------------------------------------

void xnet_address::SetIP( u32 IP )
{
    m_IP = IP;
}

//------------------------------------------------------------------------------

void xnet_address::SetPortNumber( s32 Port )
{
    m_Port = Port;
}

//------------------------------------------------------------------------------

const char* xnet_address::SetStrIP( const char* pIPStr )
{
    m_IP = 0;
    for( s32 i=0; i<4; i++ )
    {
        u32  D = 0;
        char C = *pIPStr++;

        while( (C>='0') && (C<='9') )
        {
            D = D*10 + (C-'0');
            C = *pIPStr++;
            ASSERT( D < 256 );
        }

        if( i < 3 )
        {
            ASSERT( C == '.' );
        }

        m_IP = (m_IP << 8) | D;
    }

    return pIPStr;
}

//------------------------------------------------------------------------------

const char* xnet_address::SetStrAddress( const char* pAddrStr )
{
    char C;

    // Read IP.
    pAddrStr = SetStrIP( pAddrStr );

    // Read port.
    m_Port = 0;
    C = *pAddrStr;
    while( (C>='0') && (C<='9') )
    {
        m_Port = m_Port*10 + (C-'0');
        C      = *++pAddrStr;
    }

    return pAddrStr;
}
    
//------------------------------------------------------------------------------

u32 xnet_address::GetIP( void ) const
{
    return m_IP;
}

//------------------------------------------------------------------------------

s32 xnet_address::GetPort( void ) const
{
    return m_Port;
}

//------------------------------------------------------------------------------

xstring xnet_address::GetStrIP( void ) const
{
    xstring String;

    String.Format( "%d.%d.%d.%d",
                   (m_IP>>24)&0xFF,                         
                   (m_IP>>16)&0xFF,                         
                   (m_IP>> 8)&0xFF,                         
                   (m_IP>> 0)&0xFF );

    return String;
}

//------------------------------------------------------------------------------

xstring xnet_address::GetStrAddress( void ) const
{
    xstring String;

    String.Format( "%d.%d.%d.%d:%d",
                    (m_IP>>24)&0xFF,                         
                    (m_IP>>16)&0xFF,                         
                    (m_IP>> 8)&0xFF,                         
                    (m_IP>> 0)&0xFF,
                    m_Port );

    return String;
}

//------------------------------------------------------------------------------

xbool xnet_address::operator == ( const xnet_address& A ) const
{
    return( (m_IP == A.m_IP) && (m_Port == A.m_Port) );
}

//------------------------------------------------------------------------------

xbool xnet_address::operator != ( const xnet_address& A ) const
{
    return( (m_IP != A.m_IP) || (m_Port != A.m_Port) );
}

//------------------------------------------------------------------------------

void xnet_address::SetupLocalIP( void )
{
    // fill out the slot structure
    // first, determine local IP address
    char            tbuf[128];
    hostent*        pHe;
    struct in_addr  in_IP;

    gethostname( tbuf, 127 );
    pHe = gethostbyname( tbuf );

    in_IP = *(struct in_addr*)(pHe->h_addr);

    m_IP   = ntohl(in_IP.s_addr);
    m_Port = 0;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// NET SIMULATOR
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//DOM-IGNORE-BEGIN
struct lag_noise_backup
{
    xsafe_array<xbyte, 1024>    m_Data;
    xtick                       m_Wait;
    s32                         m_Lenth;
    sockaddr                    m_Addr;
    s32                         m_Size;
    s32                         m_Socket;

}; 
#define LAG_SYM_NEXTIME_INTERVAL x_GetTicksPerMs()*50

static xtick                    s_LagSimNextTime;
static xbool                    s_bLagSimulatorOn             = FALSE;
static s32                      s_PercentagePackageLost       = 0;
static s32                      s_PercentageHackedData        = 0;
static s32                      s_PercentageLagNoise          = 0;
static s32                      s_PercentageLagDistance       = 0;
static s32                      s_PercentagePacketDuplication = 0;
static xarray<lag_noise_backup> s_BackupData;
static xrandom_large            s_Random(666);
//static s32                      s_LastDelay=0;

//------------------------------------------------------------------------------
static 
xbool LagSimulatorSendTo( s32 Socket, const char* pData, s32 Length, s32 SomeThing, struct sockaddr* pAddr, s32 Size )
{ 
    static s32 PacketLost=0;
    static s32 currupted = 0;
#ifdef NETWORKDEBUG
    static s32 TotalPacketsSent=0;

    x_LogMessage( NETLOG, "LagSimulator: Lost(%d) Hacked Packets(%d) TotalSent(%d)", PacketLost, currupted,TotalPacketsSent++ );
#endif

    if( s_Random.Rand32(0, 100) < s_PercentagePackageLost )
    {
        PacketLost++;
        //x_LogMessage( NETLOG, "LagSimulator: Losing a packet");
        return TRUE;
    }

    if( s_Random.Rand32(0, 100) < s_PercentageHackedData )
    {
        currupted++;
        //x_LogMessage( NETLOG, "LagSimulator: Corrupting packet");

        s32 Count = s_Random.Rand32( 1, Length );
        for( s32 i=0; i<Count; i++ )
        {
            s32 Pos = s_Random.Rand32( 0, Length-1);

            ((xbyte*)pData)[Pos] = 0xCC;
        }
    }

    //s32 MsDelay = x_irand(0, 1+s32(5000 * s_PercentageLagDistance/100.0f) );

    if( 1 )//s_Random.Rand32(0, 100) < s_PercentagePacketDuplication )
    {
        lag_noise_backup& Data = s_BackupData.append();

        xptr_lock   Ref(s_BackupData);      

        // The wait could go as low as 0 seconds to a max of 30sec
        Data.m_Wait = (s32)(x_GetTime() + x_GetTicksPerMs() * 800 );//MsDelay);
        Data.m_Data.Copy( 0, (xbyte*)pData, Length );
        Data.m_Lenth    = Length;
        Data.m_Addr     = *pAddr;
        Data.m_Size     = Size;
        Data.m_Socket   = Socket;
    }

/*
    if( 0 ) //s_PercentageLagNoise >= x_irand(0,100) && MsDelay > 30 )
    {
        lag_noise_backup& Data = s_BackupData.append();

        xptr_lock   Ref(s_BackupData);      

        // The wait could go as low as 0 seconds to a max of 30sec
        Data.m_Wait = (s32)(x_GetTime() + x_GetTicksPerMs() * MsDelay);
        Data.m_Data.Copy( 0, (xbyte*)pData, Length );
        Data.m_Lenth    = Length;
        Data.m_Addr     = *pAddr;
        Data.m_Size     = Size;
        Data.m_Socket   = Socket;
    }
    else
    {
        s32 status = sendto( Socket, (const char*)pData, Length, 0, pAddr, Size );
        if (status<=0)
        {
            x_LogError( NETLOG, "SendTo returned an error code %d",WSAGetLastError());
            return FALSE;
        }
    }
*/
    return TRUE;
}

//------------------------------------------------------------------------------
static 
void LagSimulatorStepUpdate( void )
{
    s32 i;
    if( s_BackupData.getCount() == 0 ) return;

    xtick  Time = x_GetTime();
    xptr_lock   Ref(s_BackupData);      
    for( i=0; i<s_BackupData.getCount(); i++ )
    {
        lag_noise_backup& Data = s_BackupData[i];
        if( Time > Data.m_Wait )
        {
            long status = sendto( Data.m_Socket, (const char*)&Data.m_Data[0], Data.m_Lenth, 0, &Data.m_Addr, Data.m_Size );
            if (status<=0)
            {
                x_LogError( NETLOG, "SendTo returned an error code %d",x_GetLastSocketError());
            }

            // Delete entry
            s_BackupData.DeleteWithCollapse( i );
            i--;
        }
    }
}
//DOM-IGNORE-END

//------------------------------------------------------------------------------

void x_NetV2EnableLagSimulator( s32 PercentagePackageLost, s32 PercentageDuplicated, s32 PercentageHackedData, s32 PercentageLagNoise, s32 PercentageLagDistance )
{
    s_PercentagePackageLost         = PercentagePackageLost;
    s_PercentageHackedData          = PercentageHackedData;
    s_PercentageLagNoise            = PercentageLagNoise;
    s_PercentageLagDistance         = PercentageLagDistance;
    s_PercentagePacketDuplication   = PercentageDuplicated;
    s_bLagSimulatorOn               = TRUE;
    s_LagSimNextTime                = x_GetTime() + LAG_SYM_NEXTIME_INTERVAL;
}

//------------------------------------------------------------------------------

void x_NetV2DisableLagSimulator( void )
{
    s_bLagSimulatorOn = FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SOCKET CLASS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

xnet_socket::xnet_socket( void )
{
    m_RefCount =  0;
    m_Socket   = ~0;
    m_Port     = ~0;
}


//------------------------------------------------------------------------------

xbool xnet_socket::OpenUDPSocket( s32 Port )
{
	SOCKADDR_IN     SockAddr;

    // Set the port number
    m_Port = Port;
    m_bTCP = FALSE;

	// Set up socket
    SOCKET UDPSocket = INVALID_SOCKET;

	// Initialize the UDP socket
	UDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if( UDPSocket == INVALID_SOCKET ) 
    {
		s32 Err = x_GetLastSocketError();
        x_LogError( NETLOG, "Cannot create UDP socket (%d)!", Err );
		return FALSE;
	}
	
	// bind the socket
	x_memset( &SockAddr, 0, sizeof(SOCKADDR_IN) );
	SockAddr.sin_family         = AF_INET; 
	SockAddr.sin_addr.s_addr    = htonl(INADDR_ANY);
	SockAddr.sin_port           = htons( Port );

    if( bind( UDPSocket, (SOCKADDR*)&SockAddr, sizeof(SockAddr) ) == SOCKET_ERROR )
    {
        // increment port if that port is assigned
        if( x_GetLastSocketError() == WSAEADDRINUSE )
        {
            x_LogError( NETLOG, "Error it seems like this port(%d) is in use. Try a different one.", m_Port );
            return FALSE;
        }
        else
        {
            // if some other error, nothing we can do...abort
		    s32 Err = x_GetLastSocketError();
            x_LogError( NETLOG, "Couldn't bind UDP socket (%d)! Invalidating", Err );

            return FALSE;
        }
    }

    // set this socket to non-blocking, so we can poll it
#ifdef TARGET_PC
    u_long  dwNoBlock = TRUE;
    ioctlsocket( UDPSocket, FIONBIO, &dwNoBlock );
#else
    fcntl( UDPSocket, F_SETFL, O_NONBLOCK);
#endif 
    
	// Set the mode of the socket to allow broadcasting.  We need to be able to broadcast
	// when a game is searched for in IPX mode.
    u_long  dwBroadcast = TRUE;
#ifdef TARGET_PC
	setsockopt( UDPSocket, SOL_SOCKET, SO_BROADCAST, (LPSTR)&dwBroadcast, sizeof(dwBroadcast));
#else
	setsockopt( UDPSocket, SOL_SOCKET, SO_BROADCAST, &dwBroadcast, sizeof(dwBroadcast));
#endif
    // Set the socket
    m_Socket = (u32)UDPSocket;

    // Success!
    x_LogMessage( NETLOG, "UDP Initialized at port: %d", Port );

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xnet_socket::OpenTCPSocket( s32 Port )
{
    // Set the port number
    m_Port = Port;
    m_bTCP = TRUE;

	// Set up socket
    SOCKET TCPSocket = INVALID_SOCKET;

	// Initialize the UDP socket
	TCPSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if( TCPSocket == INVALID_SOCKET ) 
    {
		s32 Err = x_GetLastSocketError();
        x_LogError( NETLOG, "Cannot create TCP socket (%d)!", Err );
		return FALSE;
	}
	
    // set this socket to non-blocking, so we can poll it
#ifdef TARGET_PC
    u_long  dwNoBlock = TRUE;
    ioctlsocket( TCPSocket, FIONBIO, &dwNoBlock );
#else
   fcntl( TCPSocket, F_SETFL, O_NONBLOCK );
#endif
    
    // Set the socket
    m_Socket = (u32)TCPSocket;

    // Success!
    x_LogMessage( NETLOG, "TCP Initialized at port: %d", Port );

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xnet_socket::ReadFromSocket( void* pData, s32 MaxLength, s32& Length, xnet_address& FromAddr, xbool bBlock )
{
	// read socket stuff
	SOCKADDR_IN     ip_addr;				// UDP/TCP socket structure
	fd_set	        rfds;
	timeval	        TimeOut;
	socklen_t       FromLen;
		
    ASSERT( MaxLength > 0 );
    
	// check if there is any data on the socket to be read.  The amount of data that can be
	// atomically read is stored in len.
	FD_ZERO(&rfds);
	FD_SET (m_Socket, &rfds);
	TimeOut.tv_sec  = 0;
	TimeOut.tv_usec = 0;

    // If we are blocking lets wait for 10 sec maximun
    if( bBlock )
    {
        TimeOut.tv_usec  = 500;
    }

	if( select( 0, &rfds, NULL, NULL, &TimeOut) == SOCKET_ERROR ) 
    {
        s32 LastError = x_GetLastSocketError();
        x_LogError( NETLOG, "select( 0, &rfds, NULL, NULL, &TimeOut) Fail with error %d",LastError);
        return FALSE;
	}

	// if the read file descriptor is not set, then bail!
 	if( !FD_ISSET(m_Socket, &rfds) ) 
    {
      //  s32 LastError = WSAGetLastError();
      //  x_LogError( NETLOG, "!FD_ISSET(m_Socket, &rfds) Fail with error: %d\n",LastError);
	    return FALSE;
    }
		
	// Get data off the socket and process.
	FromLen = sizeof(SOCKADDR_IN);
#ifdef TARGET_PC
	Length = recvfrom( m_Socket, (char*)pData, MaxLength, 0, (SOCKADDR*)&ip_addr, &FromLen );
#else
    /*
    ssize_t	recvfrom(int, void *, size_t, int, struct sockaddr * __restrict,
                     socklen_t * __restrict) __DARWIN_ALIAS_C(recvfrom);
    */
    
    ssize_t l = recvfrom( (int)m_Socket, (void*)pData, MaxLength, 0, (SOCKADDR*)&ip_addr, &FromLen );
    
    Length = (s32)l;
    ASSERT( Length == l );
    
#endif
    if( Length == SOCKET_ERROR ) 
    {
        // We could use WSAECONNRESET to signify the socket was closed by the
        // target side. This *should* only apply to TCP sockets but it also
        // seems to apply in Windows for UDP sockets if sent to the same machine.
#ifdef NETWORKDEBUG
        s32 LastError = x_GetLastSocketError();
        if( LastError == EAGAIN )
        {
            // We simply did not get a packet
            return FALSE;
        }
        
        if ( (LastError != WSAEWOULDBLOCK) && (LastError != WSAECONNRESET) )
        {
            x_LogError( NETLOG, "RecvFrom returned an error %d",LastError);
        }
        else
        {
            x_LogError( NETLOG, "RecvFrom returned a potencial error %d",LastError);
        }
#endif

	    return FALSE;
    }

	// If this gets hit, data will be overwritten.  BAD!!	
	ASSERT( Length <= MaxLength );

    // Set the address
    FromAddr.Setup( ntohl( ip_addr.sin_addr.s_addr ), ntohs( ip_addr.sin_port ) );

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xnet_socket::SendPacket( void* pData, s32 Size, xnet_address Address )
{
    struct sockaddr_in sockto;

    // address your package and stick a stamp on it :-)
    x_memset(&sockto, 0, sizeof(struct sockaddr_in));
    sockto.sin_family       = AF_INET;
    sockto.sin_port         = htons( Address.GetPort() );
    sockto.sin_addr.s_addr  = htonl( Address.GetIP() );

    if( s_bLagSimulatorOn )
    {
        return LagSimulatorSendTo( m_Socket, (const char*)pData, Size, 0, (struct sockaddr*)&sockto, sizeof(sockto) );
    }

    ssize_t status = sendto( m_Socket, (const char*)pData, Size, 0, (struct sockaddr*)&sockto, sizeof(sockto) );
    if (status<=0)
    {
        x_LogError( NETLOG, "SendTo returned an error code %d",x_GetLastSocketError());
        return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

u32 xnet_socket::getLocalIP( void )
{
    // fill out the slot structure
    // first, determine local IP address
    char            tbuf[128];
    hostent*        pHe;
    struct in_addr  in_IP;

    gethostname( tbuf, 127 );
    pHe = gethostbyname( tbuf );

    in_IP = *(struct in_addr*)(pHe->h_addr);
    return ntohl(in_IP.s_addr);
}

//-------------------------------------------------------------------------------

xbool xnet_socket::AcceptTCPConnection( xnet_socket& NewSocket ) const
{
    //
    // Try to accept any incoming connection
    //
    sockaddr_in addr;
    socklen_t   addr_len = sizeof(addr);
    SOCKET      newConnSD;
    if ((newConnSD = accept(m_Socket, (sockaddr *)&addr, &addr_len)) == INVALID_SOCKET ) 
    {
        s32 err = x_GetLastSocketError();

        // Since we have made the socket to be non blocking
        // returning a null if nothing is happening is ok
        if( err == WSAEWOULDBLOCK ) return FALSE;

        x_LogError( NETLOG, "TCP connection accept failed! Error=%d", err ); 

        return FALSE;
    }

    s32 Port = ntohs(addr.sin_port);

    //
    // Get the port that we are open for the new client
    //
//    s32 Port = 0;
    {
        sockaddr_in addr;
        socklen_t addr_len = sizeof(addr);

        if( getsockname( m_Socket, (sockaddr *)&addr, &addr_len) < 0 ) 
        {
            s32 err = x_GetLastSocketError();
            x_LogError( NETLOG, "TCP socket fail to return the port! Error=%d", err ); 
            return FALSE;
        }
        else
        {
            Port = ntohs(addr.sin_port);
        }
    }

    //
    // Fill the return structure
    //
    NewSocket.m_bTCP     = TRUE;
    NewSocket.m_Port     = Port;
    NewSocket.m_Socket   = (u32)newConnSD;
    NewSocket.m_RefCount = 1;

    return TRUE;
}

//-------------------------------------------------------------------------------

xbool xnet_socket::ConnectTCPServer( xnet_address Address )
{
    struct sockaddr_in sockto;

    // address your package and stick a stamp on it :-)
    x_memset(&sockto, 0, sizeof(struct sockaddr_in));
    sockto.sin_family       = AF_INET;
    sockto.sin_port         = htons( Address.GetPort() );
    sockto.sin_addr.s_addr  = htonl( Address.GetIP() );

    if( connect( m_Socket, (sockaddr *) &sockto, sizeof(sockto)) < 0) 
    {
        s32 err = x_GetLastSocketError();

#ifndef TARGET_PC
    #define WSAEALREADY EALREADY
#endif
        // If the client is in the process of connecting then we are all good...
        if( err == WSAEALREADY || err == WSAEWOULDBLOCK )
        {
            return TRUE;
        }
        
#ifndef TARGET_PC
        // If already connected???
        if( err == EISCONN || err == EINPROGRESS )
        {
            return TRUE;
        }
#endif
        x_LogError( NETLOG, "TCP error connecting to the server! Error=%d", err );
        return FALSE;
    }

    return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#define BASE_COUNTER_TYPE           u32
#define BASE_COUNTER_BYTES          (sizeof(BASE_COUNTER_TYPE))
#define BASE_COUNTER_BITS           (BASE_COUNTER_BYTES*8)
#define BASE_COUNTER_PROTOCOL_SHIFT (BASE_COUNTER_BITS-2)
#define BASE_COUNTER_MASK           ((1<<BASE_COUNTER_PROTOCOL_SHIFT)-1)
#define BASE_COUNTER_PROTOCOL_MASK  (~BASE_COUNTER_MASK)

//------------------------------------------------------------------------------

void xnet_base_protocol::vResetClient( void )
{
    m_BytesSent         = 0;
    m_PacketsSent       = 0;
    m_BytesReceived     = 0;
    m_PackagesReceived  = 0;

    //
    // Remove the client from the pending client queue
    //
    {
        xnet_base_protocol* pPrev=NULL;
        for( xnet_base_protocol* pNext = m_pMesh->m_pPendingPacketClientList; pNext; pNext = pNext->m_pPendingNext )
        {
            if( pNext == this )
            {
                if( pPrev )
                {
                    pPrev->m_pPendingNext = pNext->m_pPendingNext;
                }
                else
                {
                    m_pMesh->m_pPendingPacketClientList = pNext->m_pPendingNext;
                }
                break;
            }
            pPrev = pNext;
        }
    }
}

//------------------------------------------------------------------------------

xnet_base_protocol::xnet_base_protocol( void )
{
    m_pMesh = NULL;
    m_pPendingNext = NULL;

    m_BytesSent         = 0;
    m_PacketsSent       = 0;
    m_BytesReceived     = 0;
    m_PackagesReceived  = 0;
}

//------------------------------------------------------------------------------

xbool xnet_base_protocol::setupConnection( xnet_address RemoteAddress, xnet_udp_mesh& Mesh ) 
{ 
    m_ConnectionAddress = RemoteAddress;
    m_pMesh             = &Mesh;
    
    return TRUE;
}

//------------------------------------------------------------------------------

u32 xnet_base_protocol::setPacketHeader( s32& LocalSequence, const void* pData, s32 Size, u8 Protocol )
{
    // Compute the CRCSeq
    u32 LargeCRC = x_memCRC32( pData, Size );
    u32 FinalCRC = LargeCRC^LocalSequence;
    u32 CRCSeq   = (FinalCRC&BASE_COUNTER_MASK) | (((u32)Protocol)<<BASE_COUNTER_PROTOCOL_SHIFT);

    // update the sequence
    LocalSequence = BASE_COUNTER_MASK&(LocalSequence+1);

    return CRCSeq;
}

//------------------------------------------------------------------------------

s32 xnet_base_protocol::getProtocol( const void* pData )
{
    ASSERT(pData);
    return x_NetworkEndian( *((BASE_COUNTER_TYPE*)pData) ) >> BASE_COUNTER_PROTOCOL_SHIFT;
}

//------------------------------------------------------------------------------

xbool xnet_base_protocol::getInfoPacketHeader( s32 RemoteSequence, const void* pData, s32 Size, s32& Sequence ) 
{
    // Definetly the size should be larger than 4 to play our game
    if( Size < 4 ) return FALSE;

    // Deal with the CRC
    u32 LargeCRC = x_memCRC32( &((u8*)pData)[BASE_COUNTER_BYTES],Size-BASE_COUNTER_BYTES );
    u32 CRCSeq   = x_NetworkEndian( *((BASE_COUNTER_TYPE*)pData) );
    
    Sequence = (CRCSeq^LargeCRC)&BASE_COUNTER_MASK;

    // Find the shortest distance 
    // as well as set the sequence in the same space
    // as our local sequence
    s32 Distance;
    if( Sequence < RemoteSequence ) 
    {
        s32 Distance1 = RemoteSequence - Sequence;
        s32 Distance2 = (Sequence + BASE_COUNTER_MASK+1) - RemoteSequence;
        if( Distance1 < Distance2 ) 
        {
            Distance = Distance1;
        }
        else 
        {
            Distance = Distance2;

            // Put sequence in the space as our local sequence
            Sequence += BASE_COUNTER_MASK+1;
        }
    }
    else                        
    {
        s32 Distance1 = Sequence - RemoteSequence;
        s32 Distance2 = (RemoteSequence + BASE_COUNTER_MASK+1) - Sequence;
        if( Distance1 < Distance2 ) Distance = Distance1;
        else 
        {
            Distance = Distance2;

            // Put sequence in the space as our local sequence
            Sequence = -((BASE_COUNTER_MASK+1) - Sequence);
        }
    }

    // This means that either the packet is too old or that the CRC
    // is not valid. In whatever case we don't want the packet
    if( Distance > 60 )
    {
#ifdef NETWORKDEBUG
        x_LogError( NETLOG, "CRC Did not match or Packet is too old (Probably someone hacking)");
#endif
        return FALSE;
    }

    // Okay we are done
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xnet_base_protocol::WriteToSocket( xhandle hLocalSocket, void* pData, s32 Size )
{
    return m_pMesh->WriteToSocket( hLocalSocket, pData, Size, m_ConnectionAddress );    
}

//------------------------------------------------------------------------------

void xnet_base_protocol::AddClientInPendingList( void )
{
    m_pPendingNext = m_pMesh->m_pPendingPacketClientList;
    m_pMesh->m_pPendingPacketClientList = this;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// NON-RELIABLE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

xnet_nonreliable_protocol::xnet_nonreliable_protocol( void )
{
    m_NonRealibleLocalSequence     = 0;
    m_NonRealibleRemoteSequence    = 0;
}

//------------------------------------------------------------------------------

void xnet_nonreliable_protocol::vResetClient( void )
{
    xnet_base_protocol::vResetClient();

    m_NonRealibleLocalSequence     = 0;
    m_NonRealibleRemoteSequence    = 0;
}

//------------------------------------------------------------------------------

xbool xnet_nonreliable_protocol::SendBroadcast( s32 Port, xnet_socket& LocalSocket, const void* pData, s32 Length )
{
    s32 LocalSequence=0;

    // Create the packet and get ready to send it
    s32 CRCSize      = BASE_COUNTER_BYTES;
    u32 CRCSqc       = setPacketHeader( LocalSequence, pData, Length, 1);
    s32 FinalSize    = Length + CRCSize;
    u8* pFinalPacket = reinterpret_cast<u8*>(alloca(FinalSize));

    (*(BASE_COUNTER_TYPE*)pFinalPacket) = x_NetworkEndian( CRCSqc );
    x_memcpy( &pFinalPacket[CRCSize], pData, Length );

    //
    // Send Packet over the network
    //
    xnet_address Address;
    Address.Setup( 0xffffffff, Port );
    if( LocalSocket.SendPacket( pFinalPacket, FinalSize, Address ) )
    {
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xnet_nonreliable_protocol::SendNonreliablePacket( xhandle hLocalSocket, const void* pData, s32 Length )
{
    //
    // Build Final Packet
    //

    // Create the packet and get ready to send it
    s32 CRCSize      = BASE_COUNTER_BYTES;
    u32 CRCSqc       = setPacketHeader( m_NonRealibleLocalSequence, pData, Length, 1);
    s32 FinalSize    = Length + CRCSize;
    u8* pFinalPacket = reinterpret_cast<u8*>(alloca(FinalSize));

    (*(BASE_COUNTER_TYPE*)pFinalPacket) = x_NetworkEndian( CRCSqc );
    x_memcpy( &pFinalPacket[CRCSize], pData, Length );

    //
    // Send Packet over the network
    //
    if( WriteToSocket( hLocalSocket, pFinalPacket, FinalSize ) )
    {
        // Keep track of stats
        m_BytesSent += FinalSize;
        m_PacketsSent++;

        return TRUE;
    }


    return FALSE;
}

//------------------------------------------------------------------------------

xbool xnet_nonreliable_protocol::OnRecivePacket( void* pData, s32& Size, s32 Sequence )
{
    //
    // TODO: Should the unreliable protocol throw away packets that are older?
    //
    // We do not deal with older packets
    if( m_NonRealibleRemoteSequence >= Sequence ) 
    {
        x_LogError( NETLOG, "I got an unreliable packet that had a sequence of %d but we are at %d so we throw it out",Sequence, m_NonRealibleRemoteSequence );
        return FALSE;
    }

    // Update our internal sequence
    m_NonRealibleRemoteSequence = Sequence;

    // Correct for the header
    Size -= BASE_COUNTER_BYTES;
    x_memmove( pData, &((u8*)pData)[BASE_COUNTER_BYTES], Size );

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RELIABLE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

// The more you wait the less data is transmitted however the less resistence to 
// packet lost. 1 will mean that for every one reliable packet recive we will send
// back 32/(n+1) confimations. 2 will mean 32/(2+1) confirmations. etc...
//#define MAX_PACKETS_WAIT_FOR_ACKNOWLEAGE 0

// How many packets we will wait without confirmation untill we decide to send the packet again
#define MAX_PACKETS_WAIT_FOR_RESENT      16 //12

//------------------------------------------------------------------------------

void xnet_reliable_protocol::vResetClient( void )
{
    xnet_nonreliable_protocol::vResetClient();

    for( s32 i=0; i<m_PendingPacketPool.getCount()-1; i++ )
    {
        m_PendingPacketPool[i].m_pNext = &m_PendingPacketPool[i+1]; 
    }
    m_PendingPacketPool[m_PendingPacketPool.getCount()-1].m_pNext = NULL;
    m_pReliablePoolFreeList = &m_PendingPacketPool[0];

    // Intialize all the packets
    m_TotalInQueue = 0;
    m_PendingQueue.SetMemory(0);

    // Reliable protocol 
    m_RealibleSequence    = 0;
    m_RecivedPacketsMask  = 0;
    m_bForcePing          = FALSE;  

    // Basic stats
    m_nResentPackets = 0;

    // Reset the timer
    m_Timer.Trip();
    m_TimeSinceLastRecived.Trip();
}

//------------------------------------------------------------------------------

xnet_reliable_protocol::xnet_reliable_protocol( void ) 
{
    // Allocate packets
    m_PendingPacketPool.Alloc( m_PendingQueue.getCount() );
    ASSERT( m_PendingPacketPool.isValid() );
    for( s32 i=0; i<m_PendingPacketPool.getCount()-1; i++ )
    {
        m_PendingPacketPool[i].m_pNext = &m_PendingPacketPool[i+1]; 
    }
    m_PendingPacketPool[m_PendingPacketPool.getCount()-1].m_pNext = NULL;
    m_pReliablePoolFreeList = &m_PendingPacketPool[0];

    // Intialize all the packets
    m_TotalInQueue = 0;
    m_PendingQueue.SetMemory(0);

    // Reliable Protocol
    m_RealibleSequence    = 0;
    m_RecivedPacketsMask  = 0;
    m_bForcePing          = FALSE;  

    // Basic stats
    m_nResentPackets = 0;

    // Reset the timer
    m_Timer.Start();
    m_TimeSinceLastRecived.Start();
}


//------------------------------------------------------------------------------

xbool xnet_reliable_protocol::Update( xhandle hLocalSocket )
{
    // Did we run out of memory?
    if( m_pReliablePoolFreeList == NULL )
    {
        return FALSE;
    }

    // If we have not recived noting for X seconds we will assume the other client has timed out
    if( m_TimeSinceLastRecived.ReadSec() >= 10 )
    {
        return FALSE;
    }

    // Lets make sure that we always send heart beats every second
    if( m_Timer.ReadSec() >= 1 )
    {
        m_bForcePing = TRUE;
        m_Timer.Trip();
    }

    // If we need to send a ping lets do it
    if( m_bForcePing )
    {
        if( SendReliablePacket( hLocalSocket, NULL, 0 ) == FALSE )
        {
            return FALSE;
        }
        m_bForcePing = FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xnet_reliable_protocol::SendReliablePacket( xhandle hLocalSocket, const void* pData, s32 Length )
{
    return SendReliablePacket( hLocalSocket, pData, Length, 2 );
}

//------------------------------------------------------------------------------

xbool xnet_reliable_protocol::SendReliablePacket( xhandle hLocalSocket, const void* pData, s32 Length, s32 Protocol )
{
    // We are sending a reliable packet so not need to force a ping for sure
    m_bForcePing = FALSE;
    m_Timer.Trip(); 

    //
    // If we deal with length>0 then it is not a heart beat
    //
    if( Length > 0 )
    {
        if( m_TotalInQueue >= m_PendingQueue.getCount() ||
            m_pReliablePoolFreeList == NULL )
        {
            x_LogWarning( NETLOG, "Run out of temporary memory. Returning an error on sending." );
            return FALSE;
        }

        //
        // Get a free pending packet
        //
        pending_packet& PendingPacket = *m_pReliablePoolFreeList;
        m_pReliablePoolFreeList = m_pReliablePoolFreeList->m_pNext;

        // Fill the pending packet
        PendingPacket.m_Sequence = m_RealibleSequence;
        PendingPacket.m_pNext    = NULL;
        PendingPacket.m_Size     = Length;
        PendingPacket.m_Protocol = Protocol;
        x_memcpy( &PendingPacket.m_Data[0], pData, Length );

        // Insert the pending packet in the pending queue
        m_PendingQueue[m_TotalInQueue++] = &PendingPacket;
    }

    //
    // fill the standard packet header
    //
    // Setup the final packet
    s32 HeaderSize   = 4*2;
    s32 FinalSize    = Length + HeaderSize;
    u8* pFinalPacket = reinterpret_cast<u8*>(alloca(FinalSize));

    x_memcpy( &pFinalPacket[HeaderSize], pData, Length );

    // Note that m_RealibleSequence will be changed inside the setPacketHeader function
    (*(u32*)&pFinalPacket[4*1]) = x_NetworkEndian( m_RecivedPacketsMask );
    
    u32 CRCSqc  = setPacketHeader( m_RealibleSequence, &pFinalPacket[4*1], FinalSize-4, Protocol );
    (*(u32*)&pFinalPacket[4*0]) = x_NetworkEndian( CRCSqc );

    // shift the sequence forward as we have updated the m_RealibleSequence
    m_RecivedPacketsMask <<=1;

    //
    // Send Packet over the network
    //
    if( WriteToSocket( hLocalSocket, pFinalPacket, FinalSize ) )
    {
        // Keep track of stats
        m_BytesSent += FinalSize;
        m_PacketsSent++;
        return TRUE;
    }

    ASSERT( 0 );
    return FALSE;
}

//------------------------------------------------------------------------------

void xnet_reliable_protocol::HandleAckknowleage( xhandle hLocalSocket, s32 RemoteSequence, u32 RemoteAckBits )
{
    // Now remove any pending to ack packets
    s32 TotalAck     = 0;

    // Note that this needs to be set into a local variable 
    // because the resent of the packet will up the m_totalinqueue
    // and we do not want to go though those 
    s32 TotalInQueue = m_TotalInQueue;
    for( s32 i=0; i<TotalInQueue; i++ )
    {
        pending_packet& Pending = *m_PendingQueue[i];

        // Find the actual distance between the two count
        s32 Distance;
        if( RemoteSequence < Pending.m_Sequence ) 
        {
            s32 Distance1 = Pending.m_Sequence - RemoteSequence;
            s32 Distance2 = (RemoteSequence + BASE_COUNTER_MASK) - Pending.m_Sequence;
            if( Distance2 < Distance1 ) Distance = Distance2; 
            else 
            {
                // TODO: Try to make sense of the coment below... not sure why I added right now.
                //       also make a better description of it.

                //
                // The vector of the ack bits don't overlap the vector of the pending packages
                //                                           In queue packet
                //     ack_bits    RemoteSequence                   Pending.m_Sequeue
                //     -------------------->*        distance      <*>
                //                          |<--------------------->|
                //
#ifdef NETWORKDEBUG
                x_LogWarning( NETLOG, "[1]Pendinc packet can not be ack because the remote sequence starts at (%d) and the pending packet is at %d",RemoteSequence, Pending.m_Sequence );
#endif
                continue;
            }
        }
        else                        
        {
            s32 Distance1 = RemoteSequence - Pending.m_Sequence;
            s32 Distance2 = (Pending.m_Sequence + BASE_COUNTER_MASK) - RemoteSequence;
            if( Distance1 < Distance2 ) Distance = Distance1;
            else
            {
                //
                // The vector of the ack bits don't overlap the vector of the pending packages
                //
                //     ack_bits    RemoteSequence                  Pending.m_Sequeue
                //     -------------------->*        distance      <*>
                //                          |<--------------------->|
                //
#ifdef NETWORKDEBUG
                x_LogWarning( NETLOG, "[2]Pendinc packet can not be ack because the remote sequence starts at (%d) and the pending packet is at %d",RemoteSequence, Pending.m_Sequence );
#endif
                continue;
            }
        }

        //
        // Check the ack bits to see if we have ack this packet
        //
        if( Distance<32 && (1&(RemoteAckBits>>Distance)) )
        {
            ASSERT( Distance < 32 );
            //x_LogWarning( NETLOG, "confirm with remote sequence %d pending packet sequence %d reliable sequence(%d)", RemoteSequence, Pending.m_Sequence, *((u16*)&Pending.m_Data[0]) );

            // Insert the pending packet in the empty list
            Pending.m_pNext         = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &Pending;

            // Mark the entry as empty
            m_PendingQueue[i] = NULL;
            TotalAck++;
        }
        // We will auto acknoleage packets that are too old.
        // If we waited to 12 packets probably is time to send it again
        // assume that a packet gets send at least 10 packets per second
        // TODO: This should be better done with time rather than packets
        else if( Distance > MAX_PACKETS_WAIT_FOR_RESENT )
        {
#ifdef NETWORKDEBUG
//            ASSERT( Distance < 32 );
            ASSERT( ((RemoteAckBits>>Distance)&1) == 0 );
#endif
            //
            // Send a completly new package hopefully this one will get ack
            //
            m_nResentPackets++;
            SendReliablePacket( hLocalSocket, Pending.m_Data, Pending.m_Size, Pending.m_Protocol );

#ifdef NETWORKDEBUG
            x_LogWarning( NETLOG, "Resending Packet with distance(%d) Total resent (%d)", Distance, m_nResentPackets );
#endif
            //
            // Respect to the old one we will assume it got ack from the other client
            // in essense we are going to ignore future ack for it.
            //

            // Insert the pending packet in the empty list
            Pending.m_pNext = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &Pending;

            // Mark the entry as empty
            m_PendingQueue[i] = NULL;
            TotalAck++;
        }

    }
    
    //
    // Collapse the queue
    //
    if( TotalAck > 0 )
    {
        s32 s=0;
        for( s32 i=0; i<m_TotalInQueue; i++ )
        {
            m_PendingQueue[s] = m_PendingQueue[i];
            if( m_PendingQueue[s] ) s++;
        }

        // Clear the last one
        m_PendingQueue[s] = NULL;

        // Set the new total
        ASSERT( m_TotalInQueue == (s+TotalAck) );
        m_TotalInQueue = s;
    }
}


//------------------------------------------------------------------------------

xbool xnet_reliable_protocol::OnRecivePacket( xhandle hLocalSocket, void* pData, s32& Size, s32 Sequence )
{
    //
    // Make sure our size if sufficiently big
    //
    if( Size < (BASE_COUNTER_BYTES+4) )
    {
        x_LogError( NETLOG, "We have recive a reliable packet with less size than the protocol requires [%d]", Size );
        return FALSE;
    }

    //
    // Deal with old packets and recivos
    //
    if( m_RealibleSequence >= Sequence ) 
    {
#ifdef NETWORKDEBUG
        if( m_RealibleSequence > Sequence )
        {
            x_LogWarning( NETLOG, "I got an packet out of order (old) with a sequence of %d and we are at %d",Sequence, m_RealibleSequence );
        }
#endif
        s32 Distance = m_RealibleSequence - Sequence;
        if( Distance < 32 )
        {
            if( (1<<Distance) & m_RecivedPacketsMask )
            {
                x_LogError( NETLOG, "I already got this package! It is a sequence %d and we are at %d",Sequence, m_RealibleSequence );
                return FALSE;
            }
            else
            {
                m_RecivedPacketsMask |= (1<<Distance);
            }
        }
        else
        {
            // If a reliable packet is so old that we can not send a recivo then just prettent never got here
            x_LogError( NETLOG, "I got a packet that is so old that I can not send a recivo for it. It is a sequence %d and we are at %d",Sequence, m_RealibleSequence );

            return FALSE;
        }
    }
    else
    {
        // Update the acknowleage recivo mask
        s32 Distance = Sequence - m_RealibleSequence;

        if( Distance >= 32 )
        {
            x_LogError( NETLOG, "The remote machine is far ahead of us,  It is at sequence %d and we are at %d",Sequence, m_RealibleSequence );
        }
/*
        else if( (~m_RecivedPacketsMask)>>(32-Distance))
        {
#ifdef NETWORKDEBUG
            x_LogError( NETLOG, "We are sifting bits that can not longer be ackknowleage. Distance %d", Distance );
#endif
        }
*/

        //
        // Sync counters
        //

        // Sync up with host computer
        m_RecivedPacketsMask <<= Distance;
        m_RecivedPacketsMask  |= 1;

        // Update our internal sequence
        m_RealibleSequence = Sequence;
    }

    //
    // Should we send a packet to acknowleage stuff?
    //
    m_bForcePing = TRUE;


    // Correct for the header
    u32 RemoteAckBits  = x_NetworkEndian( ((u32*)pData)[1] );

    Size -= (BASE_COUNTER_BYTES+4);
    x_memmove( pData, &((u8*)pData)[(BASE_COUNTER_BYTES+4)], Size );

    //
    // Deal with the Acknowleages
    //
    HandleAckknowleage( hLocalSocket, Sequence, RemoteAckBits );

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// IN ORDER PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#define IN_ORDER_TYPE   u16
#define IN_ORDER_BYTES (sizeof(IN_ORDER_TYPE))
#define IN_ORDER_BITS  (IN_ORDER_BYTES*8)
#define IN_ORDER_MASK  ((1<<IN_ORDER_BITS)-1)

void xnet_inorder_protocol::vResetClient( void )
{
    xnet_reliable_protocol::vResetClient();

    m_pPendingInOrder       = NULL;
    m_pPendingInOrderLast   = NULL;
    m_InOrderLocalSequence  = 0;
    m_InOrderRemoteSequence = 0;
}

//------------------------------------------------------------------------------

xnet_inorder_protocol::xnet_inorder_protocol( void )
{
    m_pPendingInOrder       = NULL;
    m_pPendingInOrderLast   = NULL;
    m_InOrderLocalSequence  = 0;
    m_InOrderRemoteSequence = 0;
}

//------------------------------------------------------------------------------

xbool xnet_inorder_protocol::SendInOrderPacket( xhandle hLocalSocket, const void* pData, s32 Length )
{
    s32 HeaderSize   = IN_ORDER_BYTES;
    s32 FinalSize    = Length + HeaderSize;
    u8* pFinalPacket = reinterpret_cast<u8*>(alloca(FinalSize));

    x_memcpy( &pFinalPacket[HeaderSize], pData, Length );

    // Save the sequence
    (*(IN_ORDER_TYPE*)pFinalPacket)  = x_NetworkEndian( (IN_ORDER_TYPE)m_InOrderLocalSequence );
    m_InOrderLocalSequence           = IN_ORDER_MASK&(m_InOrderLocalSequence+1);;

    // Ok now send the packet   
    return SendReliablePacket( hLocalSocket, pFinalPacket, FinalSize, 3 );
}

//------------------------------------------------------------------------------

xbool xnet_inorder_protocol::OnRecivePacket( xhandle hLocalSocket, void* pData, s32& Size, s32 Sequence )
{
    s32 FinalSize = Size;

    // Get the packet
    if( FALSE == xnet_reliable_protocol::OnRecivePacket( hLocalSocket, pData, FinalSize, Sequence ) )
        return FALSE;

    // Make sure our size if sufficiently big
    if( FinalSize < IN_ORDER_BYTES )
    {
        x_LogError( NETLOG, "We have recive a in order packet with less size than the protocol requires [%d]", FinalSize );
        return FALSE;
    }

    // Get the order
    s32 RemoteOrder = x_NetworkEndian( *(IN_ORDER_TYPE*)pData );
    FinalSize -= IN_ORDER_BYTES;

    // Deal with the eassy case first
    if( m_InOrderRemoteSequence == RemoteOrder ) 
    {
/*
        // This if deals with duplicates which just happen to arrive while
        // we were already in the process of collecting the packets in the recived queue
        // very rare case but still happens.
        if( m_pPendingInOrderLast )
        {
            // Here as you can see we check for the duplicates
            if( m_pPendingInOrderLast->m_Sequence == m_InOrderRemoteSequence )
            {
                x_LogError( NETLOG, "We have recive the same packet again just as we were collecting the pending packets throwing the duplicate out (%d)", RemoteOrder );

                // if we already had the packet then just ignore the new one
                if(m_pPendingNext == NULL ) 
                    AddClientInPendingList();

                // Tell the system that nothing really new has arrived
                return FALSE;
            }
        }
*/
        // Set the final package
        Size = FinalSize;
        x_memmove( pData, &((u8*)pData)[IN_ORDER_BYTES], FinalSize );

        // Get ready for the next packet
        m_InOrderRemoteSequence = IN_ORDER_MASK&(m_InOrderRemoteSequence+1);

        // Has this packet unlock pending packages if so then tell the mesh to contact us
        if( m_pPendingInOrderLast )
        {
            if( m_pPendingInOrderLast->m_Sequence == m_InOrderRemoteSequence )
                AddClientInPendingList();
        }

        return TRUE;
    }

    //
    // Mark if the sequence rolled
    //
    xbool            bRolled = FALSE;

    // Mark whether we have roll our count or not
    {
       s32 Diff = x_Abs(RemoteOrder - m_InOrderRemoteSequence);
       if( Diff > IN_ORDER_MASK/2) 
       {
           bRolled = TRUE;
       }
    }

    // Deal with old packets that come out of sequence
    // Possible duplication and old at the same time
    if( RemoteOrder < m_InOrderRemoteSequence )
    {
        s32 Diff = m_InOrderRemoteSequence - RemoteOrder;
        if( bRolled )
        {
            if( (m_InOrderRemoteSequence - RemoteOrder ) < IN_ORDER_MASK/2 )
            {
                x_LogError( NETLOG, "We have recive an inorder packet that is too old (%d) distance(%d)", RemoteOrder, Diff );
                return FALSE;
            }
        }
        else
        {
            x_LogError( NETLOG, "We have recive an inorder packet that is too old (%d) distance(%d)", RemoteOrder, Diff );
            return FALSE;
        }
    }

    //x_LogMessage( NETLOG, "[%x]InOrderProtocol: Waiting for packet (%d)", this, m_InOrderRemoteSequence );

    // Make sure that there is at least one free node in the list.
    // if not most likely the connection is really bad so we will be force to reset
    if( m_pReliablePoolFreeList == NULL )
    {
        x_LogError( NETLOG, "We have rrun out of memory buffering packages that are out of order in the reliable protocol" );
        return FALSE;
    }

    // Ok so we are not marching in order so lets insert this packet into 
    // First lets create a patcket
    // Get a free pending packet
    pending_packet& PendingPacket = *m_pReliablePoolFreeList;
    m_pReliablePoolFreeList = m_pReliablePoolFreeList->m_pNext;

    // Fill the pending packet
    PendingPacket.m_Protocol = 0;
    PendingPacket.m_Sequence = RemoteOrder;
    PendingPacket.m_pNext    = NULL;
    PendingPacket.m_Size     = FinalSize;
    x_memcpy( &PendingPacket.m_Data[0], &((u8*)pData)[IN_ORDER_BYTES], FinalSize );
    
    // Now insert it in our queue
    pending_packet*  pNext   = m_pPendingInOrder;
    pending_packet*  pPrev   = NULL;

    // Insert the node in the right place in the list
    for( ; pNext; pNext = pNext->m_pNext )
    {
        // Find a place to insert it
        if( pNext->m_Sequence == PendingPacket.m_Sequence )
        {
            x_LogError( NETLOG, "We have recive the same packet again throwing the duplicate out (%d)", PendingPacket.m_Sequence );

            // Insert our packet back into the free pool
            PendingPacket.m_pNext   = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &PendingPacket;

            return FALSE;
        }
        else if( bRolled )
        {
            // we are in the same cudrant as the pending packet
            if( pNext->m_Sequence < IN_ORDER_MASK/2 )
            {
                //smaller units go first
                if( pNext->m_Sequence < PendingPacket.m_Sequence ) 
                {
                    break;
                };
            }
            else
            {
                break;
            }
        }
        else if( (PendingPacket.m_Sequence - pNext->m_Sequence) > IN_ORDER_MASK/2 )
        {
            // keep going we are not dealing with the same range...
            // Since when we roll over the smaller numbers are first even though they
            // mean that they are they future rolled over sequence.
        }
        else if( pNext->m_Sequence < PendingPacket.m_Sequence ) 
        {
            // TODO: for making this faster We should have the the sequence in the same space
            // We want the newer packets to be at the front
            break;
        }

        // Save where we would need to insert the node
        pPrev  = pNext;
    }

    // update the node
    PendingPacket.m_pNext = pNext;
    PendingPacket.m_pPrev = pPrev;

    // update neibords
    if( pNext ) 
        pNext->m_pPrev = &PendingPacket;

    if( pPrev ) 
        pPrev->m_pNext = &PendingPacket;

    // Insert our packet into the actual queue
    if( pPrev == NULL )
        m_pPendingInOrder = &PendingPacket;

    // If the new node is the last in the sequence we put the tail there
    if( pNext == NULL )
        m_pPendingInOrderLast = &PendingPacket;

// Insert the node in the right place in the list
// This code is just for debugging
/*
if( 1 )
{
    for( pending_packet*  pNext = m_pPendingInOrder; pNext; pNext = pNext->m_pNext )
    {
        ASSERT(m_InOrderRemoteSequence <= pNext->m_Sequence ); 
        if( pNext->m_pNext ) 
        {
            ASSERT(pNext->m_Sequence != pNext->m_pNext->m_Sequence );
            ASSERT(pNext->m_Sequence > pNext->m_pNext->m_Sequence );
        }
        else
        {
            ASSERT( m_pPendingInOrderLast == pNext );
        }
    }
}
*/
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xnet_inorder_protocol::getPendingPacketData( void* pData, s32& Size )
{
    // If our queue empty? if so there is nothing to do
    ASSERT( m_pPendingInOrder );
    ASSERT( m_InOrderRemoteSequence == m_pPendingInOrderLast->m_Sequence );

    // Okay have we recive the packet that we were missing?
    pending_packet& Packet = *m_pPendingInOrderLast;

    Size = Packet.m_Size;
    x_memcpy( pData, Packet.m_Data, Size );

    // Move foward to the next entry
    m_pPendingInOrderLast = m_pPendingInOrderLast->m_pPrev;
    if( m_pPendingInOrderLast ) 
        m_pPendingInOrderLast->m_pNext = NULL;
    else
        m_pPendingInOrder = NULL;

    // Insert our packet back into the free pool
    Packet.m_pNext                   = m_pReliablePoolFreeList;
    m_pReliablePoolFreeList          = &Packet;

    // Get ready for the next packet
    m_InOrderRemoteSequence = IN_ORDER_MASK&(m_InOrderRemoteSequence+1);

    // If we have more unlocked packages we should tell the mesh
    if( m_pPendingInOrderLast )
    {
        if( m_pPendingInOrderLast->m_Sequence == m_InOrderRemoteSequence )
            return TRUE;
    }

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// UDP CLIENT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

void xnet_udp_client::vResetClient( void )
{
    xnet_inorder_protocol::vResetClient();
}

//------------------------------------------------------------------------------

void xnet_udp_client::CertifyClient( void )
{ 
    // Sorry if it already certify it is because somehow got disconnected
    // and probably is trying to connect again if that is the case we need
    // to have a hard reset
    if( m_ClientType == TYPE_CERTIFIED )
    {
        vResetClient();
    }

    m_ClientType = TYPE_CERTIFIED; 
}

//------------------------------------------------------------------------------

xbool xnet_udp_client::HandleRecivePacket( xhandle hLocalSocket, void* pData, s32& Size )
{
    ASSERT( Size > 4);
    
    s32 Protocol = getProtocol( pData );
    s32 Sequence;

    switch( Protocol )
    {
    default:
    case 0: ASSERT(0); break;

    case 1: 
            // Get information about the packet
            if( FALSE == getInfoPacketHeader( m_NonRealibleRemoteSequence, pData, Size, Sequence ) )
                return FALSE;

            return xnet_nonreliable_protocol::OnRecivePacket( pData, Size, Sequence );

    case 2: 
            // Get information about the packet
            if( FALSE == getInfoPacketHeader( m_RealibleSequence, pData, Size, Sequence ) )
                return FALSE;

            return xnet_reliable_protocol::OnRecivePacket( hLocalSocket, pData, Size, Sequence );

    case 3: 
            // Get information about the packet
            if( FALSE == getInfoPacketHeader( m_RealibleSequence, pData, Size, Sequence ) )
                return FALSE;

            return xnet_inorder_protocol::OnRecivePacket( hLocalSocket, pData, Size, Sequence );
    }

    // Everything is ok
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// UDP MESH
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

xnet_udp_mesh::xnet_udp_mesh( void )
{
    m_pPendingPacketClientList = NULL;
}

//------------------------------------------------------------------------------

xhandle xnet_udp_mesh::findClient( const xnet_address& FromAddr )
{
    for( s32 i=0; i<m_lClient.getCount(); i++ )
    {
        xnet_udp_client& Client = m_lClient[i];

        if( Client.getAddress() == FromAddr )
            return m_lClient.GetHandleByIndex(i);
    }

    xhandle NullHandle;
    NullHandle.setNull();
    return  NullHandle;
}

//------------------------------------------------------------------------------

xbool xnet_udp_mesh::getSocketPackets( xhandle hLocalSocket, void* pUserBuffer, s32& UserSize, xhandle& hClient, xbool bBlock )
{
    //
    // Try to Read packets from the client in-order protocol
    // This needs to be here first if not we get strange bugs because we are
    // trying to get this sequence while new packets keep arriving.
    // This also has the positive side effects that frees as many nodes as it can there by saving memory first
    //
    if( m_pPendingPacketClientList )
    {
        if( FALSE == m_pPendingPacketClientList->getPendingPacketData( pUserBuffer, UserSize ) )
        {
            // It has not more packets after this one
            // so remove it from the list
            xnet_base_protocol* pT     = m_pPendingPacketClientList; 
            m_pPendingPacketClientList = m_pPendingPacketClientList->m_pPendingNext;
            pT->m_pPendingNext         = NULL;
        }

        return TRUE;
    }

    //
    // Try to read a packet from the socket
    //
    const s32 MaxSize = UserSize;
    xnet_socket& Socket = m_SocketList( hLocalSocket );
    xnet_address FromAddr;
    while( Socket.ReadFromSocket( pUserBuffer, MaxSize, UserSize, FromAddr, bBlock ) )
    {
        //
        // Find Client with the specific address
        //
        hClient = findClient( FromAddr );

        // If can not find the client the client must communicate with the reliable protocol only at this point
        if( hClient.IsNull() )
        {
            // All initial connections must use the reliable protocol only
            // and the size should definetly larger than just a ping
            if( ((((u8*)pUserBuffer)[0])>>6) == 2 || UserSize > (4+4) )
            {
                // We will create a client and let the user decice whether he wants to certify him
                hClient = CreateClient( FromAddr );

                // if we have an error creating the client we will pretend we never got the packet
                if( hClient.IsNull() )
                    continue;
            }
            else
            {
                x_LogWarning( NETLOG, "A computer [%s] address try to connect using the wrong protocol", (const char*)FromAddr.GetStrAddress() );
                continue;
            }
        }

        //
        // Notify the client that a packet has arrived
        //
        xnet_base_protocol& Client = m_lClient(hClient);
        if( Client.HandleRecivePacket( hLocalSocket, pUserBuffer, UserSize ) )
        {
            // Reset the timer since we have recived data
            Client.m_TimeSinceLastRecived.Trip();

            //
            // If we got a packet that is too small then there is nothing to do
            // Probably a heart beat
            //
            if( UserSize > 0 ) 
                return TRUE;
        }
    }

    //
    // if we have not sent an ack packet lets do that
    //
    for( s32 i=0; i<m_lClient.getCount(); i++ )
    {
        // Update the heart-beat and such
        if( FALSE == m_lClient[i].Update( hLocalSocket ) )
        {
            UserSize = -1;
            hClient  = m_lClient.GetHandleByIndex(i);
            return TRUE;
        }
    }

    //
    // Do the lag simulator
    //
    if( s_bLagSimulatorOn )
    {
        LagSimulatorStepUpdate();
    }

    //
    // Output how many packets we have left
    //
#ifdef NETWORKDEBUG
    if( 1 )
    {
        struct pending_packet
        {
            s32                         m_Protocol;
            s32                         m_Sequence;
            s32                         m_Size;
            pending_packet*             m_pNext;
            pending_packet*             m_pPrev;
            xsafe_array<xbyte,1024>     m_Data;
        };

        //
        // First check how many free packets left
        // 
        for( s32 i=0; i<m_lClient.getCount(); i++ )
        {
            xnet_udp_client& Client = m_lClient[i];
            s32          Count=0;

            for( pending_packet* pNext = (pending_packet*)Client.m_pReliablePoolFreeList;
                                 pNext;
                                 pNext = pNext->m_pNext ) Count++;

            //x_LogMessage( NETLOG, "Client[%X] has (%d) Free", &Client, Count );
            
            //if( Count < 10 ) BREAK;
        }
    }
#endif

    return FALSE;
}

//------------------------------------------------------------------------------

xhandle xnet_udp_mesh::CreateClient( xnet_address Address )
{
    // Make sure that we don not have this client already
    xhandle hClient = findClient( Address );
    ASSERT( hClient.IsNull() );
    
    // create a new client
    xnet_udp_client& Client = m_lClient.append( hClient );
    if( Client.setupConnection( Address, *this ) == FALSE )
    {
        m_lClient.DeleteByHandle( hClient );
        hClient.setNull();
    }

    // Ok we are done
    return hClient;
}

//------------------------------------------------------------------------------

xhandle xnet_udp_mesh::OpenSocket( s32 Port )
{
    return FindOrCreateSocket( Port );
}

//------------------------------------------------------------------------------

xhandle xnet_udp_mesh::FindOrCreateSocket( s32 Port )
{
    xhandle hSocket;

    //
    // Find Socket
    //
    hSocket.setNull();
    for( s32 i=0; i<m_SocketList.getCount(); i++ )
    {
        if( Port == m_SocketList[i].getPort() )
        {
            m_SocketList[i].AddRef();
            hSocket = m_SocketList.GetHandleByIndex(i);
            return hSocket;
        }
    }

    //
    // Create Socket
    //
    xnet_socket& Socket = m_SocketList.append( hSocket );
    if( FALSE == Socket.OpenUDPSocket( Port ) )
    {
        m_SocketList.DeleteByHandle( hSocket );
        hSocket.setNull();
        return hSocket;
    }

    // Everything when okay
    Socket.AddRef();
    return hSocket;
}

//------------------------------------------------------------------------------

xbool xnet_udp_mesh::WriteToSocket( xhandle hLocalSocket, void* pData, s32 Size, xnet_address Address )
{
    return m_SocketList(hLocalSocket).SendPacket( pData, Size, Address );
}

//------------------------------------------------------------------------------

void xnet_udp_mesh::DeleteClient( xhandle hClient )
{
    xnet_udp_client& Client = m_lClient( hClient );

    // Disconnect the client first

    //
    // Remove the client from the pending client queue
    //
    {
        xnet_base_protocol* pPrev = NULL;
        for( xnet_base_protocol* pNext = m_pPendingPacketClientList; pNext; pNext = pNext->m_pPendingNext )
        {
            if( pNext == &Client )
            {
                if( pPrev )
                {
                    pPrev->m_pPendingNext = pNext->m_pPendingNext;
                }
                else
                {
                    m_pPendingPacketClientList = pNext->m_pPendingNext;
                }
                break;
            }
            pPrev = pNext;
        }
    }

    //
    // Now we can delete him
    //
    m_lClient.DeleteByHandle( hClient ); 
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// TCP CLIENT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

xbool xnet_tcp_client::OpenSocket( s32 Port )
{
    return m_Socket.OpenTCPSocket( Port );
}

//-------------------------------------------------------------------------------

xbool xnet_tcp_client::ConnectToServer( xnet_address Address )
{
    return m_Socket.ConnectTCPServer( Address );
}

//-------------------------------------------------------------------------------

xbool xnet_tcp_client::Send( const void* pData, s32 Length )
{
    if ( send( m_Socket.getLowLevelSocket(), (char*)pData, Length, 0) < 0) 
    {
        s32 err = x_GetLastSocketError();
        x_LogError( NETLOG, "TCP socket error while sending data! Error=%d", err ); 
        return FALSE;
    }

    return TRUE;
}

//-------------------------------------------------------------------------------

xbool xnet_tcp_client::Recive( void* pUserBuffer, s32& UserSize, xbool bBlock )
{
    xnet_address Address;
    return m_Socket.ReadFromSocket( pUserBuffer, UserSize, UserSize, Address, bBlock );
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// TCP SERVER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

xnet_tcp_server::xnet_tcp_server( void )
{

}

//-------------------------------------------------------------------------------

xbool xnet_tcp_server::OpenSocket( s32 Port )
{
    // Open the port
    if( FALSE == m_Socket.OpenTCPSocket( Port ) )
        return FALSE;

    
    //
    // Bind to that port
    //
    struct sockaddr_in sin;

    sin.sin_port = htons(Port);
    sin.sin_addr.s_addr = 0;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_family = AF_INET;

    if(bind(m_Socket.getLowLevelSocket(), (struct sockaddr *)&sin,sizeof(struct sockaddr_in) ) < 0 )
    {
        s32 err = x_GetLastSocketError();
        x_LogError( NETLOG, "TCP bind to socket error! Error=%d", err ); 
        return FALSE;
    }

    //
    // Start Listening in the port
    //
    if( listen(m_Socket.getLowLevelSocket(), 0) < 0 ) 
    {
        s32 err = x_GetLastSocketError();
        x_LogError( NETLOG, "TCP socket error while trying to listen! Error=%d", err ); 
        return FALSE;
    }

    return TRUE;
}

//-------------------------------------------------------------------------------

xbool xnet_tcp_server::AcceptConnections( void )
{
    xbool       bNewClients = FALSE;
    xnet_socket NewConnection;

    while( m_Socket.AcceptTCPConnection( NewConnection ) )
    {
        xnet_tcp_client*& pClient = m_lClient.append();
        pClient           = x_new(xnet_tcp_client,1,0);
        pClient->m_Socket = NewConnection;
        bNewClients       = TRUE;
    }

    return bNewClients;
}



