//==============================================================================
//==============================================================================
//  PRIVATE!!  PRIVATE!!  PRIVATE!!  PRIVATE!!  PRIVATE!!  PRIVATE!!  PRIVATE!!
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN

//==============================================================================
// TYPES
//==============================================================================

struct xmem_block_header
{
#ifndef X_USE_NATIVE_NEW_AND_DELETE
    xmem_block_header*      m_pHashNext;            // Pointers for the hash
    xmem_block_header*      m_pHashPrev;            // Pointers for the hash
#else
    void*                   m_pPhysicalBeginning;
#endif

    xmem_block_header*      m_pGlobalNext;          // Pointer for the global list
    xmem_block_header*      m_pGlobalPrev;          // Pointer for the global list
    s32                     m_PhysicalSize;         // Actual size of the block
    s32                     m_Count;                // Number of items in this allocation
    u16                     m_Flags;                // Flags for the block
    u16                     m_nXPtrs;		        // Number of xptr refering to this block of memory
    u16                     m_MovableIndex;         // Index to the movable entry

#ifdef X_DEBUG

    struct lining
    {
        xsafe_array<u8,16>                  m_Bytes;           // This is used to check if the memory block got overwritten
    };

    struct debug_info
    {
        u32                                 m_Sequence;        // Sequence of the block when it was allocated
        u32                                 m_MemCRC;          // Check sum for read only blocks
        const char*                         m_pFile;           // File where the allocation happen
        u16                                 m_LineNum;         // Line number where the allocation happen
        u32                                 m_TypeSize;        // Size of the typed that is been allocated
        u8                                  m_nScopes;         // Number of scoped structure which are valid
        s8		                            m_nRef;	           // Number of reference currently accessing the memory
        xsafe_array<const xscope_node*,4>   m_Scope;           // Scoped structures
        lining                              m_FrontLining;     // Lining use to check whether user overuns blocks
    };

    debug_info              m_Debug;
#endif
};

//==============================================================================
// FUNCTIONS
//==============================================================================

void                x_MemReset                  ( void );
xmem_block_header*  x_GetFirstBlock             ( void );
void                x_MemoryInit                ( void );
void                x_MemoryKill                ( void );
xmem_block_header*  x_GetBlockFromPtr           ( void* pPtr );
void                x_MoveMemory                ( void );
void*               x_GetDataPtrFromMovablePtr  ( void* pPtr );

//==============================================================================
//  These prototypes and macros are here to change the manager's
//  behavior under debug and release compiles.
//==============================================================================

void*   x_Real_Malloc   ( s32 TypeSize, s32 Count, u32 Flags,  const char*  pFileName, s32 LineNumber );
void*   x_Real_Realloc  ( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber );
void    x_Real_Free     ( void* pPtr, s32 TypeSize );

#ifdef X_DEBUG
#define x_malloc(TYPE_SIZE, ENTRY_COUNT, FLAGS)               x_Real_Malloc  ( TYPE_SIZE, ENTRY_COUNT, FLAGS, __FILE__, __LINE__ )
#define x_realloc(OLD_POINTER, TYPE_SIZE, NEWCOUNT)     x_Real_Realloc ( OLD_POINTER, TYPE_SIZE, NEWCOUNT, __FILE__, __LINE__ )
#define x_free(PTR)                                     x_Real_Free    ( PTR, 0xff0000ff );
#else
#define x_malloc(TYPE_SIZE, COUNT, FLAGS)               x_Real_Malloc  ( TYPE_SIZE, COUNT, FLAGS, "", 0 )
#define x_realloc(OLD_POINTER, TYPE_SIZE, NEWCOUNT)     x_Real_Realloc ( OLD_POINTER, TYPE_SIZE, NEWCOUNT, "", 0 )
#define x_free(PTR)                                     x_Real_Free    ( PTR, 0xff0000ff );
#endif

//==============================================================================
// END
//==============================================================================
//DOM-IGNORE-END
