//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is a general memory manager. Allows allocation of difference sizes.
//     The main concept of the manager is to keep the bottom of the memory as full
//     as possible to leave the top as free as it can. This not only minimizes 
//     fragmentation but also allows large empty block to be open at the top.
//
//<P><B>FREE HASH</B>
//<P>  ============================================================================
//<P>  The free hash is used for quick indexing of memory blocks. Each entry in the
//     hash represents a size of a block. The size increments by double. So it is a
//     power of two type of format. When memory nodes are inserted into the hash they
//     are inserted sorted by address. They are inserted bia a insert sort. This
//     is done to give priority to the lowest adress. Which serves two porpuses. 
//     The first one is to keep the lowest memory always full. The second is that
//     the most used blocks will also be closer to the front of the list.
//     Farther speed ups are possible to add floating hashes to the FreeHash table 
//     when a particular index gets too many entris. But we will wait untill later
//     to find whether we need to implement that or not.
//<CODE> 
// FreeBlock
// SizeBytes  FreeHash
//            +-------+        LOW Address                   HIGH Address
//     2      |       |------->+--------+     +--------+     +--------+
//            |       |    +-->|HashNext|---->|HashNext|---->|HashNext|----+
//            +-------+    |+--|HashPrev|<----|HashPrev|<----|HashPrev|<-+ |
//     4      |       |    ||  +--------+     +--------+     +--------+  | |
//            |       |    |+--------------------------------------------+ |
//            +-------+    +-----------------------------------------------+
//     8      |       |
//            |       |
//            +-------+
//            |       |
//     16     |       |
//            +-------+
//</CODE>                         
//<P><B>GLOBAL POOL</B>
//<P>  ============================================================================
//<P>  All blocks are part of a global pool. The pool is sorted by address this makes 
//     it very eassy to merge nodes and to walk the entire heap. It is in a cicular
//     link list so it is eassy to get to the higest memory node.
//<CODE> 
//
//                   LOW Address                       HIGH Address 
// m_pBaseBlock----->+----------+     +----------+     +----------+
//               +-->|GlobalNext|---->|GlobalNext|---->|GlobalNext|----+
//               |+--|GlobalPrev|<----|GlobalPrev|<----|GlobalPrev|<-+ |
//               ||  +----------+     +----------+     +----------+  | |
//               |+--------------------------------------------------+ |
//               +-----------------------------------------------------+
//</CODE>                         
//<P><B>BLOCK LAYOUT</B>
//<P>  ============================================================================
//<P>  Each memory block has a header which contains information about the memory block.
//     Things like the global pointers and debug information. In debug mode the header 
//     of the block gets a bit larger. This is done for debug popuses. 
//<CODE> 
//              +----------------------+
//              | header               |
//              | front lining         |
//              +----------------------+
//              | user area            |
//              |                      |
//              |                      |
//              +----------------------+
//              | end lining           |
//              +----------------------+
//</CODE>                         
//------------------------------------------------------------------------------
//
// TODO: Static realloc is not implemented
// TODO: Test movable pointers with realloc
// TODO: Better algorithum for moving blocks
// TODO: Blocks should know about which threads have access to them
// TODO: Network comunication layer for debugging
// TODO: Memory dump
// TODO: Realloc for small blocks
// TODO: Other features for small blocks such the read only flag
// TODO: Small blocks also need debugging features such who allocated the block as well as leaks
// TODO: Small blocks need to be part of the memory report as well
// TODO: Movable memory may need to exits in its own class like the small blocks (cleaner)
//==============================================================================
// INCLUDES
//==============================================================================
#include <stdio.h>
#include <string.h>
#include "../x_base.h"

//==============================================================================
// Make sure we ignore all this section if we are going to be dealing with the native memory manager
//==============================================================================
//DOM-IGNORE-BEGIN

//==============================================================================
// Define standard alightment for the memory manager
//==============================================================================
#define XMEM_STD_ALIGNMENT_DEFAULT      4

//==============================================================================
// Static vars
//==============================================================================
//static const char* const s_pLiningSequence = "XMEM LINING PAD!";   // Lining sequence used for debuging over-runs

//==============================================================================
// VARIABLES
//==============================================================================


//==============================================================================
// small_block FUNCTIONS
//==============================================================================

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
// Main memory manager
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
#ifndef X_USE_NATIVE_NEW_AND_DELETE

class xmem_manager
{
public:
    void                    Initialize                  ( void );
    void                    DonateMemory                ( void* pBlockAddr, s32 nBytes );
    void*                   Malloc                      ( s32 TypeSize, s32 Count, u16 Flags, const char* pFileName, s32 LineNumber );
    void                    Free                        ( void* pPtr, s32 TypeSize );
    void*                   Realloc                     ( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber );
    xbool                   SanityCheck                 ( void );
    void                    SetReadOnly                 ( void* pPtr, xbool bReadOnly );
    void                    SetDebugMode                ( xmem_debug_mode Mode ); 
    void                    MoveMemory                  ( void );
    void                    MemDump                     ( const char* pFileName );
    
    void                    GetMemHashStats             ( s32* pData, s32 Count ) const;
    u32                     GetMemorySequence           ( void ) const;
    s32                     GetLargestBlockFree         ( void ) const;
    s32                     GetTotalMemoryFree          ( void ) const;
    s32                     GetTotalMemory              ( void ) const;
    s32                     GetElementCount             ( void* pPtr ) const;
    s32                     GetWastedMemory             ( void ) const;
    s32                     GetNumberOfAllocations       ( void ) const;
    s32                     GetAllocatedCount           ( void ) const;
    u32                     GetDebugSequence            ( void ) const { return m_DebugSequence; }
    s32                     GetBlockCount               ( void ) const;
    xmem_block_header*      GetBlockFromPtr             ( void* pPtr ) const;
    xmem_block_header*      GetFirstBlock               ( void ) const;
    void*                   GetDataPtrFromMovablePtr    ( void* pPtr ) const;

protected:

    union movable_entry
    {
        xmem_block_header*  m_pPtr;
        s32                 m_Index;
    };

    struct info_dump
    {
        const void*         m_pPtr;
        s32                 m_Total;
        s32                 m_nAllocations;
    };

protected:

    xmem_block_header*      MergeFreeBlock              ( xmem_block_header& Block );
    void                    GlobalPoolUnlink            ( xmem_block_header& Block );
    s32                     HashCompIndex               ( s32 nBytes );
    void                    FreeHashUnlinkBlock         ( xmem_block_header& Block, u16 Flags );
    void                    FreeHashDonateBlock         ( xmem_block_header& Block );
    xmem_block_header*      FreeHashAlloc               ( s32 nBytes, u16 Flags );
    xmem_block_header*      FreeHashRealloc             ( xmem_block_header& Block, s32 nBytes );
    xmem_block_header*      FindBlockCandidate          ( s32 nBytes, u16 Flags );
    void                    TrySplitBlock               ( xmem_block_header*& pBlock, s32 nBytes );
    void                    ConformToAlignment          ( xmem_block_header*& pBlock, s32 nBytes );
    s32                     GetAligment                 ( u16 Flags );
    xbool                   IsBlockCandidate            ( xmem_block_header& Block, s32 nBytes, u16 Flags );
    xbool                   ValidateBlock               ( xmem_block_header& Block );
    void                    AddToAllocatedHash          ( xmem_block_header& Block );
    void                    UpdateBlockInAllocatedHash  ( xmem_block_header& Block );
    void                    RemoveFromAllocatedHash     ( xmem_block_header& Block );
    const char*             LimitLength                 ( const char* pString, s32 count ) const;
    const char*             ScopeDumpFunction           ( xmem_block_header& Header ) const;
    void*                   TryAllocateSmall            ( s32 nBytes, u16 Flags );
    xbool                   FreeSmall                   ( void* pPtr );

protected:

    mutable xcritical_section               m_CriticalSection;      // Local critical section
    xsafe_array<xmem_block_header*,29>      m_FreeHash;             // Hash of free memory blocks
    xsafe_array<xmem_block_header*,29>      m_AllocHash;            // This is a Hash of allocated blocks
    xmem_block_header*                      m_pBaseBlock;           // The lowest block in memory
    u32                                     m_DebugSequence;        // Sequence number of allocations
    xmem_debug_mode                         m_DebugMode;            // Tells the system what to check for
    xsafe_array<movable_entry,1024*8>       m_PtrLookup;            // This is for pointers to block of memory that can be move
    s32                                     m_iFreePtrLookup;       // Index of the next free lookup ptr
    xsafe_array<info_dump,1024>             m_InfoDump;             // This is used when we are dumping the stats for the memory manager
    s32                                     m_SmallestBigestBlock;  // Tell us how small the biggest block ever got
    s32                                     m_SmallestFreeMemory;   // This tell us what was the slowest point in free memory ever
    s32                                     m_FreeMemory;           // How much free memory is in the system
    small_block								m_SB;                   // Small block manager
};

//==============================================================================
// xmem_manager FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

void xmem_manager::Initialize( void )
{
    s32 i;

    //
    // Clear the free hash table
    //
    ASSERT( m_AllocHash.getCount() == m_FreeHash.getCount() );
    for( i=0; i<m_FreeHash.getCount(); i++ )
    {
        m_AllocHash[i] = m_FreeHash[i]  = NULL;
    }

    m_pBaseBlock            = NULL;
    m_DebugSequence         = 0;
    m_DebugMode             = XMEM_DEBUG_FAST;//XMEM_DEBUG_FAST;//XMEM_DEBUG_CHECKALL;//XMEM_DEBUG_BASIC;XMEM_DEBUG_FAST;
    m_SmallestFreeMemory    = S32_MAX;
    m_SmallestBigestBlock   = S32_MAX;
    m_FreeMemory            = 0;

    //
    // Initialize the ptr lookup for the movable blocks
    //
    m_iFreePtrLookup = 0;
    for( i=0; i<m_PtrLookup.getCount(); i++ )
    {
        m_PtrLookup[i].m_Index = i+1;
    }
    m_PtrLookup[i-1].m_Index = -1;
}

//------------------------------------------------------------------------------

void xmem_manager::SetDebugMode( xmem_debug_mode Mode )
{
    m_DebugMode = Mode;
}

//------------------------------------------------------------------------------

s32 xmem_manager::HashCompIndex( s32 nBytes )
{
    ASSERT(nBytes > 0);
    s32       Index;

    for( Index = -1;  ((nBytes > 0) && (Index < (m_FreeHash.getCount()-1))) ;  )
    {
        nBytes >>= 1;
        Index++;
    }

    return Index;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetAligment( u16 Flags )
{
    return 1<<(((Flags & XMEM_FLAG_ALIGN_MASK )>>2)+2);
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::GetFirstBlock( void ) const
{
    return m_pBaseBlock;
}

//------------------------------------------------------------------------------

xbool xmem_manager::ValidateBlock( xmem_block_header& Block )
{
#ifndef X_DEBUG
    (void)Block;
#else
    xbyte*  pMem        = ((xbyte*)&((&Block)[1]));
    s32     Alignment   = GetAligment( Block.m_Flags );

    // Make sure it has the right aligment
    ASSERT( pMem == x_Align( pMem, Alignment ) );

    // Make sure global pointers look okay
    ASSERT( Block.m_pGlobalNext->m_pGlobalPrev == &Block );
    ASSERT( Block.m_pGlobalPrev->m_pGlobalNext == &Block );

#endif

    return TRUE;
}

//------------------------------------------------------------------------------

void* xmem_manager::GetDataPtrFromMovablePtr( void* pPtr ) const
{
    ASSERT( (((u32)((movable_entry*)&pPtr)->m_Index)&0xff0000ff) == 0xff0000ff );

    s32 Index =(s32)(((((u32)((movable_entry*)&pPtr)->m_Index))>>8)&0xffff);
    xmem_block_header* pHeader = m_PtrLookup[Index].m_pPtr;
    ASSERT( pHeader );

    return &pHeader[1];
}

//------------------------------------------------------------------------------

void xmem_manager::AddToAllocatedHash( xmem_block_header& Block )
{
    // We only add movable blocks
    if( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_MOVABLE ) == FALSE )
        return;

    xmem_block_header* pBlock = &Block;                 
    s32                Index  = HashCompIndex( pBlock->m_PhysicalSize );  
    xmem_block_header* pNext  = m_AllocHash[Index];

    //
    // Set the movable index as well as set the pointer of the node into the table
    //
    ASSERT( m_iFreePtrLookup != -1 );
    ASSERT( m_iFreePtrLookup <  m_PtrLookup.getCount() );
    ASSERT( m_iFreePtrLookup <  0xffff );
    ASSERT( m_iFreePtrLookup >= 0 );

    s32 iMoveIndex = m_iFreePtrLookup;
    s32 iNewFree = m_PtrLookup[m_iFreePtrLookup].m_Index;

    ASSERT( iNewFree <  m_PtrLookup.getCount() );
    ASSERT( iNewFree <  0xffff );
    ASSERT( iNewFree >= 0 );

    ASSERT( iMoveIndex <  0xffff );
    ASSERT( iMoveIndex >= 0 );

    m_iFreePtrLookup = iNewFree;
    pBlock->m_MovableIndex = (u16)iMoveIndex;

    m_PtrLookup[iMoveIndex].m_pPtr = &Block;

    //
    // Handle the simple case first
    //
    if( pNext == NULL )
    {
        // Insert it directly
        pBlock->m_pHashPrev = pBlock->m_pHashNext = pBlock;

        // Add it as our first node
        m_AllocHash[Index] = pBlock;
        
        return;
    }

    //
    // Find the right place for the node
    //
    if( pBlock > pNext )
    {
        do
        {
            pNext = pNext->m_pHashNext;

        } while( pBlock > pNext && pNext != m_AllocHash[Index] );
    }            

    // make sure that we are doing this right
    ASSERT( pBlock < pNext || pBlock < m_AllocHash[Index] || pBlock > m_AllocHash[Index]->m_pHashPrev );

    //
    // Link the node into the hash
    //
    pBlock->m_pHashNext = pNext;
    pBlock->m_pHashPrev = pNext->m_pHashPrev;

    pNext->m_pHashPrev->m_pHashNext = pBlock;
    pNext->m_pHashPrev              = pBlock;

    //
    // Finally lets make sure to update the hash table if we have to
    //
    if( pBlock < m_AllocHash[Index] ) m_AllocHash[Index] = pBlock;

}

//------------------------------------------------------------------------------

void xmem_manager::RemoveFromAllocatedHash( xmem_block_header& Block )
{
    // We only with movable blocks
    if( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_MOVABLE ) == FALSE )
        return;

    s32 Index  = HashCompIndex( Block.m_PhysicalSize );  

    //
    // Make sure to set the entry to null
    //
    m_PtrLookup[Block.m_MovableIndex].m_Index = m_iFreePtrLookup;
    m_iFreePtrLookup = Block.m_MovableIndex;

    ASSERT( m_iFreePtrLookup <  m_PtrLookup.getCount() );
    ASSERT( m_iFreePtrLookup <  0xffff );
    ASSERT( m_iFreePtrLookup >= 0 );

#ifdef X_DEBUG
    Block.m_MovableIndex = 0xffff;
#endif

    //
    // Take care of the hash table
    //
    if( m_AllocHash[Index] == &Block )
    {
        m_AllocHash[Index] = Block.m_pHashNext;
        if( m_AllocHash[Index] == &Block ) 
        {
            m_AllocHash[Index] = NULL;
#ifdef X_DEBUG
            Block.m_pHashNext = Block.m_pHashPrev = NULL;
#endif
            return;
        }
    }

    //
    // Now lets unlink the node from the list
    //
    Block.m_pHashNext->m_pHashPrev = Block.m_pHashPrev;
    Block.m_pHashPrev->m_pHashNext = Block.m_pHashNext;

#ifdef X_DEBUG
    Block.m_pHashNext = Block.m_pHashPrev = NULL;
#endif

}

//------------------------------------------------------------------------------

void xmem_manager::UpdateBlockInAllocatedHash( xmem_block_header& Block )
{
    RemoveFromAllocatedHash( Block );
    AddToAllocatedHash( Block );
}

//------------------------------------------------------------------------------

void xmem_manager::MoveMemory( void )
{
    //
    // Find top most block to move down
    // 
    xmem_block_header* pTopMost = NULL;
    for( s32 i=0; i<m_AllocHash.getCount(); i++ )
    {
        if( m_AllocHash[i] == NULL ) continue;

        if( m_AllocHash[i]->m_pHashPrev > pTopMost ) pTopMost = m_AllocHash[i]->m_pHashPrev;
    }

    //
    // Realloc the block
    //
    if( pTopMost )
    {
        ASSERT( m_PtrLookup[pTopMost->m_MovableIndex].m_pPtr == pTopMost );
        s32 Index    = pTopMost->m_MovableIndex;

        // Back up all the debug info
#ifdef X_DEBUG
        xmem_block_header::debug_info DebugInfo = pTopMost->m_Debug;
#endif
        xmem_block_header* pHeader = FreeHashRealloc( *pTopMost, pTopMost->m_PhysicalSize );

        // Make sure we still have the same index
        ASSERT( pHeader->m_MovableIndex == Index );

        // Reallocate the block
        m_PtrLookup[Index].m_pPtr = pHeader;

        // Set all the debug info again
#ifdef X_DEBUG
        m_PtrLookup[Index].m_pPtr->m_Debug = DebugInfo;
#endif

        // Make sure nothing went wrong with the block
        ASSERT( ValidateBlock( *m_PtrLookup[Index].m_pPtr ) );
    }
}


//------------------------------------------------------------------------------

void xmem_manager::ConformToAlignment( xmem_block_header*& pBlock, s32 nBytes )
{
    // This should not be null
    ASSERT( pBlock );

    // We assume here that the block has been removed from the free list
    ASSERT( x_FlagIsOn( pBlock->m_Flags, XMEM_FLAG_FREE ) == FALSE );

    // Lets first make sure that this is actually a real candidate block
    ASSERT( IsBlockCandidate( *pBlock, nBytes, pBlock->m_Flags ) );

    // Get the type of aligment that we are looking for
    s32     Alignment   = GetAligment( pBlock->m_Flags  );

    // Now there are two real candidate here. The ones that fit perfectly and 
    // the ones that need to be splited.
    xbyte*  pMem        = ((xbyte*)&((pBlock)[1]));

    // Lets deal with the perfect candidates.
    // Do we have a perfect aligment match?
    if( x_FlagIsOn( pBlock->m_Flags, XMEM_FLAG_STATIC ) )
    {
        // Do we have a perfect aligment match?
        // Wow this will be one in a million
        // Remember the nbytes = user_size + block_header
        if( pMem == x_Align( pMem, Alignment ) && (pBlock->m_PhysicalSize - nBytes - sizeof(xmem_block_header)) < 0 ) 
            return;
    }
    else
    {
        if( pMem == x_Align( pMem, Alignment ) ) 
            return;
    }

    //
    // Okay lets deal with the ones that we need to split...
    //
    xmem_block_header*  pNewBlock  = NULL;
    s32                 OldSize    = pBlock->m_PhysicalSize;

    if( x_FlagIsOn( pBlock->m_Flags, XMEM_FLAG_STATIC ) )
    {
                            pMem            = (xbyte*)pBlock;
        xbyte*              pNewMemBlock    = (pMem + pBlock->m_PhysicalSize) - (nBytes - sizeof(xmem_block_header));
                            pNewBlock       = (xmem_block_header*)(x_AlignLower( pNewMemBlock, Alignment ) - sizeof(xmem_block_header));

        ASSERT( x_IsAlign( &pNewBlock[1], Alignment ) );
        ASSERT( pBlock->m_PhysicalSize > nBytes );
        ASSERT( pNewBlock >= &pBlock[1] );
    }
    else
    {
        // too bad about the splitting part thought as we will have to split towards
        // the bottom of the block which is what the memory manager has been trying 
        // so hard to avoid
        xbyte*             pNewMemBlock     = x_Align( pMem + sizeof(xmem_block_header), Alignment );
                           pNewBlock        = (xmem_block_header*)(pNewMemBlock - sizeof(xmem_block_header));

        ASSERT( x_IsAlign( &pNewBlock[1], Alignment ) );
    }

    // Lets first set the global pointers for out new block
    pNewBlock->m_pGlobalNext = pBlock->m_pGlobalNext;
    pNewBlock->m_pGlobalPrev = pBlock;

    // fix siblings
    pNewBlock->m_pGlobalNext->m_pGlobalPrev = pNewBlock;
    pBlock->m_pGlobalNext                   = pNewBlock;

    // Set the new sizes & Copy the flags
    pBlock->m_PhysicalSize     = (s32)(((xbyte*)pNewBlock) - ((xbyte*)pBlock));
    pNewBlock->m_PhysicalSize  = OldSize - pBlock->m_PhysicalSize;
    pNewBlock->m_Flags         = pBlock->m_Flags;

    // Okay we can give the old block back to the memory system
    FreeHashDonateBlock( *pBlock );

    // Set the new block
    pBlock = pNewBlock;

    // Lets make sure this rutine actually worked
    ASSERT( ValidateBlock( *pBlock ) );
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::FreeHashAlloc( s32 nBytes, u16 Flags )
{
    // Update the sequence
    m_DebugSequence++;

    // Get best candidate block
    xmem_block_header* pNode = FindBlockCandidate( nBytes, Flags );

    // Did we got anything? is so lets allocate it
    if( pNode )
    {
        // We founded a block lets unliked it from the hash
        FreeHashUnlinkBlock( *pNode, Flags );

        // Make sure it is hapily align
        ConformToAlignment( pNode, nBytes );

        // Split if we have to
        TrySplitBlock( pNode, nBytes );

        // Add to allocated hash
        AddToAllocatedHash( *pNode );

        return pNode;
    }

    //
    // Okay we werent able to get a node.
    // We must be out of memory at this point.
    // Here we can do sevel things:
    // 1) Call the friendly system to release as much memory as they can
    // 2) Try to move blocks around to free up some memory
    // 3) call it quits
    //
    return NULL;
}

//------------------------------------------------------------------------------

void xmem_manager::FreeHashDonateBlock( xmem_block_header& FreeBlock )
{
#ifdef X_DEBUG
    if(  x_ChooseDebugLevel( m_DebugMode ) >= XMEM_DEBUG_BASIC )
    {
        x_memset( &(&FreeBlock)[1], '?', FreeBlock.m_PhysicalSize - sizeof(FreeBlock) );
    }
#endif

    //
    // Merge block etc
    //
    xmem_block_header* pBlock = MergeFreeBlock( FreeBlock );                 
    s32                Index  = HashCompIndex( pBlock->m_PhysicalSize );  
    xmem_block_header* pNext  = m_FreeHash[Index];

    //
    // Initialize new block
    //
    pBlock->m_Flags = (u16)XMEM_FLAG_FREE;

    //
    // Handle the simple case first
    //
    if( pNext == NULL )
    {
        // Insert it directly
        pBlock->m_pHashPrev = pBlock->m_pHashNext = pBlock;

        // Add it as our first node
        m_FreeHash[Index] = pBlock;
        
        return;
    }

    //
    // Find the right place for the node
    //
    if( pBlock > pNext )
    {
        do
        {
            pNext = pNext->m_pHashNext;

        } while( pBlock > pNext && pNext != m_FreeHash[Index] );
    }            

    // Are you trying to free the same pointer twice?
    // make sure that we are doing this right
    ASSERT( pBlock < pNext || pBlock < m_FreeHash[Index] || pBlock > m_FreeHash[Index]->m_pHashPrev );

    //
    // Link the node into the hash
    //
    pBlock->m_pHashNext = pNext;
    pBlock->m_pHashPrev = pNext->m_pHashPrev;

    pNext->m_pHashPrev->m_pHashNext = pBlock;
    pNext->m_pHashPrev              = pBlock;

    //
    // Finally lets make sure to update the hash table if we have to
    //
    if( pBlock < m_FreeHash[Index] ) m_FreeHash[Index] = pBlock;
}

//------------------------------------------------------------------------------

void xmem_manager::FreeHashUnlinkBlock( xmem_block_header& Block, u16 Flags )
{
    ASSERT( x_FlagIsOn(Block.m_Flags, XMEM_FLAG_FREE)  );

    // Compute the index to the hash
    s32 Index  = HashCompIndex( Block.m_PhysicalSize );

    // Set the new flags
    Block.m_Flags = Flags;

    // make sure that the free flag is off
    x_FlagOff(Block.m_Flags, XMEM_FLAG_FREE);

    //
    // Take care of the hash table
    //
    if( m_FreeHash[Index] == &Block )
    {
        m_FreeHash[Index] = Block.m_pHashNext;
        if( m_FreeHash[Index] == &Block ) 
        {
            m_FreeHash[Index] = NULL;
#ifdef X_DEBUG
            Block.m_pHashNext = Block.m_pHashPrev = NULL;
#endif
            return;
        }
    }

    //
    // Now lets unlink the node from the list
    //
    Block.m_pHashNext->m_pHashPrev = Block.m_pHashPrev;
    Block.m_pHashPrev->m_pHashNext = Block.m_pHashNext;

#ifdef X_DEBUG
    Block.m_pHashNext = Block.m_pHashPrev = NULL;
#endif
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::MergeFreeBlock( xmem_block_header& Block )
{
    xmem_block_header* pFinalBlock = &Block;

    //
    // Try to merge with the Next block
    //
    if( x_FlagIsOn( Block.m_pGlobalNext->m_Flags, XMEM_FLAG_FREE ) && 
        Block.m_pGlobalNext == (xmem_block_header*)(((xbyte*)&Block) + Block.m_PhysicalSize) )
    {
        xmem_block_header* pNext = Block.m_pGlobalNext;

        // Unlike the node from the hash
        FreeHashUnlinkBlock( *pNext, 0 );

        // Set new size
        Block.m_PhysicalSize += pNext->m_PhysicalSize;

        // Fix pointers
        pNext->m_pGlobalNext->m_pGlobalPrev = &Block;
        Block.m_pGlobalNext                 = pNext->m_pGlobalNext;

        // Set the final block
        pFinalBlock = &Block;

        // Set memory to zero out the header
#ifdef X_DEBUG
        x_memset( pNext, '?', sizeof(xmem_block_header) );
#endif
    }

    //
    // Try to merge with the Prev block
    //
    if( x_FlagIsOn( Block.m_pGlobalPrev->m_Flags, XMEM_FLAG_FREE ) && 
        Block.m_pGlobalPrev == (xmem_block_header*)(((xbyte*)&Block) - Block.m_pGlobalPrev->m_PhysicalSize) )
    {
        xmem_block_header* pPrev = Block.m_pGlobalPrev;

        // Unlike the node from the hash
        FreeHashUnlinkBlock( *pPrev, 0 );

        // Set the new size
        pPrev->m_PhysicalSize += Block.m_PhysicalSize;

        // Fix the pointers
        Block.m_pGlobalNext->m_pGlobalPrev = pPrev;
        pPrev->m_pGlobalNext               = Block.m_pGlobalNext;

        // Set the final block
        pFinalBlock = pPrev;

        // Set memory to zero out the header
#ifdef X_DEBUG
        x_memset( &Block, '?', sizeof(xmem_block_header) );
#endif
    }

    return pFinalBlock;
}

//------------------------------------------------------------------------------

void xmem_manager::DonateMemory( void* pBlockAddr, s32 nBytes )
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    // A Few requierements
    ASSERT( pBlockAddr );
    ASSERT( x_IsAlign( pBlockAddr, 4) );
    ASSERT( x_IsAlign( nBytes, 4) );

     ////////////////////////////////
    // Lets initialize the block
    xmem_block_header& Block = *((xmem_block_header*)pBlockAddr);

    x_memset( &Block, 0, sizeof(Block) );
    Block.m_PhysicalSize = nBytes;
    Block.m_Flags        = (u16)XMEM_FLAG_FREE;

     ////////////////////////////////
    // Lets insert the block into the global pool

    // First check the eassy case
    if( m_pBaseBlock == NULL  )
    {
        m_pBaseBlock = &Block;
        Block.m_pGlobalNext = Block.m_pGlobalPrev = &Block;
    }
    else
    {
        // Find me the place to insert the block
        // We are going to send a node from high adress 
        // and another node from low
        // Who ever wins it gets set into the pNext
        xmem_block_header* pNext     = m_pBaseBlock;
        xmem_block_header* pLowPrev  = m_pBaseBlock->m_pGlobalPrev;

        do
        {
            if( &Block < pNext ) 
            {
                pLowPrev = pNext->m_pGlobalPrev;
                break;
            }
            if( &Block > pLowPrev  ) 
            {
                pNext = pLowPrev->m_pGlobalNext;
                break;
            }

            pNext     = pNext->m_pGlobalNext;
            pLowPrev  = pLowPrev->m_pGlobalPrev;

        } while(1);

        //
        // Okay do the actual insertion
        //

        // Set the new header
        Block.m_pGlobalPrev = pNext->m_pGlobalPrev;
        Block.m_pGlobalNext = pNext;

        // Fix up the sibilings
        pNext->m_pGlobalPrev->m_pGlobalNext = &Block;
        pNext->m_pGlobalPrev                = &Block;

        //
        // Set the new middle if we need to
        //
        if( &Block < m_pBaseBlock )
            m_pBaseBlock = &Block;
    }

#ifdef X_DEBUG
    // Keep track of the total memory avariable
    m_FreeMemory += Block.m_PhysicalSize;
#endif
     ////////////////////////////////
    // Give the block the the hash
    FreeHashDonateBlock( Block );        

	//
	// Init small blocks
	//
    static xbool bSmallBlockInit = FALSE;
    if( bSmallBlockInit == FALSE )
    {
        s32 InitialSmallBlockPool = 8*1024*1000;
        m_SB.Initialize( "8, 16, 32, 48, 64, 96, 128, 160, 192, 256, 320, 512, 1024", x_malloc(1,InitialSmallBlockPool,XMEM_FLAG_ALIGN_16B), InitialSmallBlockPool );
        bSmallBlockInit = TRUE;
    }

    // Thread safety
    m_CriticalSection.EndAtomic();
}


//------------------------------------------------------------------------------
// This function tries to remove any unused memory from the end of the 
// block and return it back to the free pool. 
//------------------------------------------------------------------------------
void xmem_manager::TrySplitBlock( xmem_block_header*& pBlock, s32 nBytes )
{
    ASSERT( pBlock->m_PhysicalSize >= nBytes );

    // This block should already be align to 4 bytes at least
    ASSERT( x_IsAlign(&pBlock[1], 4 ) );
    
    xmem_block_header* pFreeNode = NULL;

    //
    // Normal blocks we always want to keep the bottom most part
    // unwated space should fit the header for the future free block 
    // but also the next free block needs to be align to 4 bytes
    //
    ASSERT( pBlock->m_PhysicalSize >= nBytes );

    // This block should already be align to 4 bytes at least
    ASSERT( x_IsAlign(&pBlock[1], 4 ) );

    xbyte* pNextBlock   = x_Align( &(((xbyte*)&pBlock[1]))[nBytes], 4 );
    s32 UnwantedBytes   = (s32)(pNextBlock - ((xbyte*)pBlock)) - (s32)sizeof(xmem_block_header);

    ASSERT( UnwantedBytes >= nBytes );
    UnwantedBytes = pBlock->m_PhysicalSize - UnwantedBytes;
        
    // Nothing to do if pBlock isnt bigger.
    // Make sure that it will be always a space for the block header
    if( UnwantedBytes <= (s32)sizeof(xmem_block_header) ) 
        return;


    // Set the new header for the free block
    pFreeNode = (xmem_block_header*)&pNextBlock[ -(s32)sizeof(xmem_block_header) ];

    //
    // Set the final size for the new allocated block
    //
    pBlock->m_PhysicalSize = pBlock->m_PhysicalSize - UnwantedBytes;
    ASSERT(pBlock->m_PhysicalSize >= nBytes );

    //
    // Link free node into the global pool
    //
    pFreeNode->m_pGlobalPrev  = pBlock;
    pFreeNode->m_pGlobalNext  = pBlock->m_pGlobalNext;
    pFreeNode->m_PhysicalSize = UnwantedBytes;
    pFreeNode->m_Flags        = (u16)XMEM_FLAG_FREE;
    ASSERT( pFreeNode->m_PhysicalSize  >= (s32)sizeof(xmem_block_header) );

    // Fix up sibilings
    pBlock->m_pGlobalNext->m_pGlobalPrev = pFreeNode;
    pBlock->m_pGlobalNext                = pFreeNode;

    // Make sure that the free flag is gone
    x_FlagOff( pBlock->m_Flags, XMEM_FLAG_FREE );

    //
    // Okay lets give the free node to the hash
    //
    FreeHashDonateBlock( *pFreeNode );
}

//------------------------------------------------------------------------------

xbool xmem_manager::IsBlockCandidate( xmem_block_header& Block, s32 nBytes, u16 Flags )
{
    // Lets do the easiest test first
    if( Block.m_PhysicalSize < nBytes ) return FALSE;

    // Get the type of aligment that we are looking for
    s32     Alignment   = GetAligment( Flags );

    // First lets check for memory aligment
    xbyte*  pMem        = ((xbyte*)&((&Block)[1]));

    //
    // When a block is static then it is allocated at the top of the heap.
    //
    if( x_FlagIsOn( Flags, XMEM_FLAG_STATIC ) )
    {

        // Do we have a perfect aligment match?
        // Wow this will be one in a million
        if( pMem == x_Align( pMem, Alignment ) && (Block.m_PhysicalSize - nBytes - sizeof(xmem_block_header)) < 0 ) 
            return TRUE;

        //
        // If we dont have a perfect aligment match can we split the block in two
        // and still retain the aligment?
        //

        // is it sufficiently big to do two blocks?
        if( (nBytes+(s32)sizeof(xmem_block_header)) > Block.m_PhysicalSize )
            return FALSE;

        // Get the desire align memory
                            pMem            = (xbyte*)&Block;
        xbyte*              pNewMemBlock    = (pMem + Block.m_PhysicalSize) - (nBytes - (s32)sizeof(xmem_block_header));
        xmem_block_header*  pNewBlock       = (xmem_block_header*)(x_AlignLower( pNewMemBlock, Alignment ) - (s32)sizeof(xmem_block_header));

        // Now if we split it and then we align it do we still have sufficient memory to satisfy the request
        if( pNewBlock >= &(&Block)[1] )
        {
            // Okay we have a compatible block then
            return TRUE;
        }
    }
    else
    {
        // Do we have a perfect aligment match?
        // Wow this will be one in a million
        if( pMem == x_Align( pMem, Alignment ) ) 
            return TRUE;

        //
        // If we dont have a perfect aligment match can we split the block in two
        // and still retain the aligment?
        //

        // is it sufficiently big to do two blocks?
        if( (nBytes+(s32)sizeof(xmem_block_header)) > Block.m_PhysicalSize )
            return FALSE;

        xbyte* pNewBlock = x_Align( pMem + sizeof(xmem_block_header), Alignment ) - sizeof(xmem_block_header);

        // Now if we split it and then we align it do we still have sufficient memory to satisfy the request
        if( &pNewBlock[nBytes] < &((xbyte*)&Block)[Block.m_PhysicalSize] )
        {
            // Okay we have a compatible block then
            return TRUE;
        }
    }

    // Not luck then
    return FALSE;
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::FindBlockCandidate( s32 nBytes, u16 Flags )
{
    ASSERT( x_IsAlign(nBytes, 4) );

    xmem_block_header* pNode;

    // Get the index to the hash
    s32 Index  = HashCompIndex( nBytes );  

    // First lets walk to nodes of this size and find one where we fit
    pNode = m_FreeHash[Index];

    //
    // First fit 
    //
    if( 1 )
    {
        if( pNode ) 
        {
            // Get the last one first if it is static
            if( x_FlagIsOn( Flags, XMEM_FLAG_STATIC) )
            {
                pNode = pNode->m_pHashPrev; 

                do 
                {
                    // Is thit block a candidate?
                    if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                        break;

                    // Not compatible block yet
                    pNode = pNode->m_pHashPrev; 

                    if( pNode == m_FreeHash[Index]->m_pHashPrev )
                    {
                        pNode = NULL;
                        break;        
                    }

                }while( 1 );
            }
            else
            {
                do 
                {
                    // Is thit block a candidate?
                    if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                        break;

                    // Not compatible block yet
                    pNode = pNode->m_pHashNext; 


                    if( pNode == m_FreeHash[Index] )
                    {
                        pNode = NULL;
                        break;        
                    }

                }while( 1 );
            }

            // Start searching from the next hash entry
            Index++;
        }

        // Best candidate is the lowest possible address        
        for( s32 i = Index; i<m_FreeHash.getCount(); i++ )
        {
            if( m_FreeHash[i] == NULL ) continue;

            if( x_FlagIsOn( Flags, XMEM_FLAG_STATIC) )
            {
                if( m_FreeHash[i]->m_pHashPrev > pNode || pNode == NULL )
                {
                    xmem_block_header* pNext = m_FreeHash[i]->m_pHashPrev;
                    
                    do 
                    {
                        // We dont want higher memory blocks
                        if( pNext < pNode && pNode != NULL ) break;

                        // If it is a lower memory block && 
                        // it is a candidate then get this node
                        if( IsBlockCandidate( *pNext, nBytes, Flags )  )
                        {
                            pNode = pNext;
                            ASSERT( pNode->m_PhysicalSize >= nBytes );
                            break;
                        }

                        pNext = pNext->m_pHashNext;

                    } while( pNext != m_FreeHash[i]->m_pHashPrev );
                }
            }
            else
            {
                if( m_FreeHash[i] < pNode || pNode == NULL )
                {
                    xmem_block_header* pNext = m_FreeHash[i];

                    do 
                    {
                        // We dont want higher memory blocks
                        if( pNext > pNode && pNode != NULL ) break;

                        // If it is a lower memory block && 
                        // it is a candidate then get this node
                        if( IsBlockCandidate( *pNext, nBytes, Flags )  )
                        {
                            pNode = pNext;
                            ASSERT( pNode->m_PhysicalSize >= nBytes );
                            break;
                        }

                        pNext = pNext->m_pHashNext;

                    } while( pNext != m_FreeHash[i] );
                }
            }
        }
    }

    //
    // First fit of same size
    //
    else if( 0 )
    {
        if( pNode ) 
        {
            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                    break;

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

                if( pNode == m_FreeHash[Index] )
                {
                    pNode = NULL;
                    break;        
                }

            }while( 1 );

            // Start searching from the next hash entry
            Index++;
        }

        // Best candidate is the lowest possible address
        if( pNode == NULL )
        {
            for( s32 i = Index; i<m_FreeHash.getCount(); i++ )
            {
                if( m_FreeHash[i] == NULL ) continue;

                if( m_FreeHash[i] < pNode || pNode == NULL )
                {
                    if( IsBlockCandidate( *m_FreeHash[i], nBytes, Flags )  )
                    {
                        pNode = m_FreeHash[i];
                        ASSERT( pNode->m_PhysicalSize >= nBytes );
                        break;
                    }
                }
            }
        }
    }

    //
    // Best fit
    //
    else if( 0 )
    {
        xmem_block_header* pBest = NULL;

        if( pNode ) 
        {
            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                {
                    if( pBest == NULL || pNode->m_PhysicalSize < pBest->m_PhysicalSize  )
                    {
                        pBest = pNode;
                    }
                }

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

            } while( pNode != m_FreeHash[Index] );

            // Start searching from the next hash entry
            Index++;
        }

        // Get best fit from the next hash over
        for( s32 i = Index; pBest==NULL || i<m_FreeHash.getCount(); i++ )
        {
            if( m_FreeHash[i] == NULL ) continue;

            pNode = m_FreeHash[i];

            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                {
                    if( pBest == NULL || pNode->m_PhysicalSize < pBest->m_PhysicalSize  )
                    {
                        pBest = pNode;
                    }
                }

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

            } while( pNode != m_FreeHash[i] );
        }

        pNode = pBest;
    }

    //
    // First fit of same size best of next size
    //
    else if( 0 )
    {
        if( pNode ) 
        {
            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                    break;

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

                if( pNode == m_FreeHash[Index] )
                {
                    pNode = NULL;
                    break;        
                }

            }while( 1 );

            // Start searching from the next hash entry
            Index++;
        }

        xmem_block_header* pBest = pNode;

        // Get best fit from the next hash over
        for( s32 i = Index; pBest==NULL || i<m_FreeHash.getCount(); i++ )
        {
            if( m_FreeHash[i] == NULL ) continue;

            pNode = m_FreeHash[i];

            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                {
                    if( pBest == NULL || pNode->m_PhysicalSize < pBest->m_PhysicalSize  )
                    {
                        pBest = pNode;
                    }
                }

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

            } while( pNode != m_FreeHash[i] );
        }

        // set final
        pNode = pBest;
    }

    //
    // Best fit same size first of next size
    //
    else if( 0 )
    {
        xmem_block_header* pBest = NULL;

        if( pNode ) 
        {
            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                {
                    if( pBest == NULL || pNode->m_PhysicalSize < pBest->m_PhysicalSize  )
                    {
                        pBest = pNode;
                    }
                }

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

            } while( pNode != m_FreeHash[Index] );

            // Start searching from the next hash entry
            Index++;

            pNode = pBest;
        }

        // Get best fit from the next hash over
        if( pBest == NULL )
        {
            // Best candidate is the lowest possible address        
            for( s32 i = Index; i<m_FreeHash.getCount(); i++ )
            {
                if( m_FreeHash[i] == NULL ) continue;

                if( m_FreeHash[i] < pNode || pNode == NULL )
                {
                    xmem_block_header* pNext = m_FreeHash[i];

                    do 
                    {
                        // We dont what lower memory blocks
                        if( pNode < pNext && pNode != NULL ) break;

                        // If it is a lower memory block && 
                        // it is a candidate then get this node
                        if( IsBlockCandidate( *pNext, nBytes, Flags )  )
                        {
                            pNode = pNext;
                            ASSERT( pNode->m_PhysicalSize >= nBytes );
                            break;
                        }

                        pNext = pNext->m_pHashNext;

                    } while( pNext != m_FreeHash[i] );
                }
            }

            // set the best
            pBest = pNode;
        }

        pNode = pBest;
    }
    else if( 0 )
    {
        // *** Original
        // Do Best/First/Lowest 
        //
        if( pNode ) 
        {
            do 
            {
                // Is thit block a candidate?
                if( IsBlockCandidate( *pNode, nBytes, Flags ) )
                    break;

                // Not compatible block yet
                pNode = pNode->m_pHashNext; 

                if( pNode == m_FreeHash[Index] )
                {
                    pNode = NULL;
                    break;        
                }

            }while( 1 );

            // Start searching from the next hash entry
            Index++;
        }

        // Best candidate is the lowest possible address        
        for( s32 i = Index; i<m_FreeHash.getCount(); i++ )
        {
            if( m_FreeHash[i] == NULL ) continue;

            if( m_FreeHash[i] < pNode || pNode == NULL )
            {
                if( IsBlockCandidate( *m_FreeHash[i], nBytes, Flags ) )
                {
                    pNode = m_FreeHash[i];
                    ASSERT( pNode->m_PhysicalSize >= nBytes );
                    break;
                }
            }
        }
    }

    ASSERT( pNode == NULL || IsBlockCandidate( *pNode, nBytes, Flags ) );
    //ASSERT( pNode && pNode->m_PhysicalSize >= nBytes );
    return pNode;
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::FreeHashRealloc( xmem_block_header& Block, s32 nBytes )
{
    // Update the sequence
    m_DebugSequence++;

    // Size to copy over
    s32 SizeToCopyOver = (Block.m_PhysicalSize < nBytes ? Block.m_PhysicalSize : nBytes ) - sizeof(Block);

    // Get best candidate block
    xmem_block_header* pNode = FindBlockCandidate( nBytes, Block.m_Flags );

    // TODO: Need to implement static memory stuff when reallocating
    ASSERT( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_STATIC ) == FALSE );

    // We would really like to move the block down. So we will take the hold
    // move if the empty block is lower than us. This will be our best case
    // scenareo when talking about memory fragmentation.
    if( pNode < &Block && pNode != NULL) 
    {
        // We founded a block lets unliked it from the hash
        FreeHashUnlinkBlock( *pNode, Block.m_Flags );

        // Make sure that the alignment is taken care of
        ConformToAlignment( pNode, nBytes );

        // Split if we have to
        TrySplitBlock( pNode, nBytes );

        // Lets move all the memory to the new location
        x_memmove( &pNode[1], &(&Block)[1], SizeToCopyOver );

        // Copy some information over
        pNode->m_nXPtrs         = Block.m_nXPtrs;
        pNode->m_Count          = Block.m_Count;

        // Make sure to unlink it from the allocated hash
        RemoveFromAllocatedHash( Block );

        // Add it to the allocated hash if we have to
        AddToAllocatedHash( *pNode );

        // Lets free the previous block
        FreeHashDonateBlock( Block );

        return pNode;
    }

    // The next sweet spot if to merge towards the bottom. This requieres a memmove
    // but it also allow us to get closer to the bottom. After all this could be 
    // a non-movable block. 
    if( x_FlagIsOn( Block.m_pGlobalPrev->m_Flags, XMEM_FLAG_FREE ) &&
                    Block.m_pGlobalPrev == (xmem_block_header*)(((xbyte*)&Block) - Block.m_pGlobalPrev->m_PhysicalSize) &&
                   (Block.m_PhysicalSize + Block.m_pGlobalPrev->m_PhysicalSize) >= nBytes )
    {
        // Make sure is is going to survive any issues such aligment
        if( IsBlockCandidate( *Block.m_pGlobalPrev, nBytes-Block.m_PhysicalSize, Block.m_Flags ) )
        {
            // Make sure to unlink it from the allocated hash
            RemoveFromAllocatedHash( Block );

            // Get the pointer
            pNode = Block.m_pGlobalPrev;

            // Remove it from the free list
            FreeHashUnlinkBlock( *pNode, Block.m_Flags );

            // Merge blocks
            Block.m_pGlobalNext->m_pGlobalPrev = pNode;

            // Update basic data
            pNode->m_pGlobalNext    = Block.m_pGlobalNext;
            pNode->m_Flags          = Block.m_Flags;
            pNode->m_PhysicalSize  += Block.m_PhysicalSize;

            // Make sure that the alignment is taken care of
            ConformToAlignment( pNode, nBytes );

            // Copy additional data
            pNode->m_nXPtrs         = Block.m_nXPtrs;
            pNode->m_Count          = Block.m_Count;

            // Copy memory now
            x_memmove( &pNode[1], &(&Block)[1], SizeToCopyOver );

            // Split the block if we have to
            TrySplitBlock( pNode, nBytes );

            // Updates its place in the hash
            AddToAllocatedHash( *pNode );

            return pNode;
        }
    }

    // handle the case where the realloc is actually smaller than the exiting allocated block
    // If that is the case this is the best next move
    if( nBytes <= Block.m_PhysicalSize )
    {
        pNode = &Block;
        TrySplitBlock( pNode, nBytes );

        // Updates its place in the hash
        UpdateBlockInAllocatedHash( *pNode );

        return pNode;
    }

    // The next best is trying to grow foward. While we dont get closer to the bottom at least we 
    // are staying in the same place.
    if( x_FlagIsOn( Block.m_pGlobalNext->m_Flags, XMEM_FLAG_FREE ) &&
                    Block.m_pGlobalNext == (xmem_block_header*)(((xbyte*)&Block) + Block.m_PhysicalSize) &&
                   (Block.m_PhysicalSize + Block.m_pGlobalNext->m_PhysicalSize) > nBytes )
    {
        // Merge with next node
        pNode = Block.m_pGlobalNext;

        // We founded a block lets unliked it from the hash
        FreeHashUnlinkBlock( *pNode, Block.m_Flags );

        // Merge blocks
        pNode->m_pGlobalNext->m_pGlobalPrev = &Block;

        Block.m_pGlobalNext    = pNode->m_pGlobalNext;
        Block.m_PhysicalSize  += pNode->m_PhysicalSize;
    
        // Split if we have to
        pNode = &Block;
        TrySplitBlock( pNode, nBytes );

        // Updates its place in the hash
        UpdateBlockInAllocatedHash( *pNode );

        return pNode;
    }

    // Worse case is that we need to move the block somewhere and the actual block is 
    // higher than the current one.
    if( pNode )
    {
        // Make sure to unlink it from the allocated hash
        RemoveFromAllocatedHash( Block );

        // We founded a block lets unliked it from the hash
        FreeHashUnlinkBlock( *pNode, Block.m_Flags );

        // Make sure that the alignment is taken care of
        ConformToAlignment( pNode, nBytes );

        // Split if we have to
        TrySplitBlock( pNode, nBytes );

        // Lets move all the memory to the new location
        x_memmove( &pNode[1], &(&Block)[1], SizeToCopyOver );

        // Copy some information over
        pNode->m_nXPtrs         = Block.m_nXPtrs;
        pNode->m_Count          = Block.m_Count;

        // Lets free the previous block
        FreeHashDonateBlock( Block );

        // Add it to the allocated hash if we have to
        AddToAllocatedHash( *pNode );

        return pNode;
    }

    // out of memory!!!!
    // Note that the initial allocation still good
    return NULL;
}

//------------------------------------------------------------------------------

void* xmem_manager::TryAllocateSmall( s32 nBytes, u16 Flags )
{
	if( (XMEM_FLAG_NO_HEADER & Flags) &&
        ((nBytes <= 8 && ((Flags&XMEM_FLAG_ALIGN_MASK) <= XMEM_FLAG_ALIGN_8B))
	    ||  (nBytes <= 1024 && ((Flags&XMEM_FLAG_ALIGN_MASK) <= XMEM_FLAG_ALIGN_16B))) )
	{
        void* pPtr = m_SB.Malloc( nBytes );
        if( pPtr )
        {
            m_DebugSequence++;             
            return pPtr;
        }
	}
    
	return NULL;
}

//------------------------------------------------------------------------------

xbool xmem_manager::FreeSmall( void* pPtr )
{
    return m_SB.Free( pPtr );
}

//------------------------------------------------------------------------------

void* xmem_manager::Malloc( s32 TypeSize, s32 Count, u16 Flags, const char*  pFileName, s32 LineNumber  )
{
    ASSERT( TypeSize > 0 );
    ASSERT( Count >= 0 );
    
    // Thread safety
    m_CriticalSection.BeginAtomic();

    s32 nBytes    = TypeSize*Count;
    s32 FinalSize = nBytes + sizeof(xmem_block_header);

    //
    // Handle small allocations
    //
	void* pData = TryAllocateSmall( nBytes, Flags );
	if( pData )
	{
        m_CriticalSection.EndAtomic();
		return pData;
	}

    //
    // Make sure that movable wins over static
    //
    if( x_FlagIsOn( Flags, XMEM_FLAG_STATIC ) && x_FlagIsOn( Flags, XMEM_FLAG_MOVABLE ) )
    {
        // We just want to be movable at this point
        x_FlagOff( Flags, XMEM_FLAG_STATIC );
    }

#ifdef X_DEBUG
    FinalSize += sizeof(xmem_block_header::lining);
#endif

    // We need to make sure that the final size of 4 byte align
    FinalSize = x_Align( FinalSize, 4 );
    xmem_block_header* pNewBlock = FreeHashAlloc( FinalSize, Flags );

    if( pNewBlock == NULL ) 
    {
        // Thread safety
        m_CriticalSection.EndAtomic();

        if( !x_FlagIsOn( Flags, XMEM_FLAG_OKAY_TO_FAIL ) )
        {
            // handle this for the user if he ask us
            x_throw( "ALLOCATION FAILURE" ); 
        }

        return NULL;
    }

    //
    // Set the count for the new block
    //
    pNewBlock->m_Count = Count;
    pNewBlock->m_Flags = Flags;

    //
    // Handle all the debugging
    //
#ifdef X_DEBUG
    pNewBlock->m_Debug.m_TypeSize = TypeSize;
    pNewBlock->m_Debug.m_Sequence = m_DebugSequence;
    pNewBlock->m_Debug.m_pFile    = pFileName;
    pNewBlock->m_Debug.m_LineNum  = (u16)LineNumber;
    pNewBlock->m_Debug.m_nRef     = 0;
#else
    (void)pFileName;
    (void)LineNumber;
#endif


#ifdef X_DEBUG
    {
        xscope_info& ScopeInfo = x_GetScopeInfo();
        
        // Copy the scope for this alloc
        pNewBlock->m_Debug.m_nScopes = x_Min( pNewBlock->m_Debug.m_Scope.getCount(), ScopeInfo.m_CurScope );
        ASSERT( pNewBlock->m_Debug.m_nScopes >= 0 );
        ASSERT( pNewBlock->m_Debug.m_nScopes <= pNewBlock->m_Debug.m_Scope.getCount() );
        for( s32 i=pNewBlock->m_Debug.m_nScopes - 1; i >= 0; i-- )
        {
            pNewBlock->m_Debug.m_Scope[i] = ScopeInfo.m_Scope[ ScopeInfo.m_CurScope - 1 - i ];
        }
    }
#endif


#ifdef X_DEBUG    
    s32 i;

    // Set the final lining for debug
    // Set the linning at the front
    for( i=0; i<pNewBlock->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
    {
        pNewBlock->m_Debug.m_FrontLining.m_Bytes[i] = s_pLiningSequence[i];
    }

    // Set the linning at the end
    xmem_block_header::lining* pLining = ((xmem_block_header::lining*)(&((xbyte*)&pNewBlock[1])[nBytes]));

    for( i=0; i<pNewBlock->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
    {
        pLining->m_Bytes[i] = s_pLiningSequence[i];
    }
#endif

#ifdef X_DEBUG
    if(  x_ChooseDebugLevel( m_DebugMode ) >= XMEM_DEBUG_CHECKALL )
    {
        x_memset( &pNewBlock[1], '#', nBytes );
    }
#endif

    // Make sure nothing went wrong with the block
    ASSERT( ValidateBlock( *pNewBlock ) );

    //
    // Keep track of the free memory
    //
#ifdef X_DEBUG

    // Keep a reference of what gets donated
    m_FreeMemory -= Count*TypeSize;
    ASSERT( m_FreeMemory >= 0 );

    // Keep the stat of the smallers free memory ever
    if( m_FreeMemory < m_SmallestFreeMemory ) 
    {
        m_SmallestFreeMemory = m_FreeMemory;
    }
#endif
    // Thread safety
    m_CriticalSection.EndAtomic();

#ifdef X_DEBUG
    s32 SmallestBigestBlock = GetLargestBlockFree();
    if( SmallestBigestBlock < m_SmallestBigestBlock )
    {
        m_SmallestBigestBlock = SmallestBigestBlock;
    }
#endif

    // For movable data we want to actually return the index
    if( x_FlagIsOn( pNewBlock->m_Flags, XMEM_FLAG_MOVABLE ) )
    {
        movable_entry Entry;
        Entry.m_Index = ((((u32)pNewBlock->m_MovableIndex)<<8)|0xff0000ff);
        return (void*)Entry.m_pPtr;
    }

    return &pNewBlock[1];
}

//------------------------------------------------------------------------------

void xmem_manager::Free( void* pPtr, s32 TypeSize )
{
    ASSERT( pPtr );

    // Thread safety
    m_CriticalSection.BeginAtomic();

    //
    // Is this a movable pointer?
    //
    if( (((u32)((movable_entry*)&pPtr)->m_Index)&0xff0000ff) == 0xff0000ff )
    {
        pPtr = GetDataPtrFromMovablePtr( pPtr );
    }

	if( FreeSmall( pPtr ) )
	{
        m_CriticalSection.EndAtomic();
		return;
	}

    ASSERTS( pPtr > m_pBaseBlock, "This memory is not part of the xbase::memory" );
    ASSERTS( pPtr < ((xbyte*)m_pBaseBlock->m_pGlobalPrev)+m_pBaseBlock->m_PhysicalSize, "This memory is not part of the xbase::memory" ); 

    // Get the block
    xmem_block_header& Block = ((xmem_block_header*)pPtr)[-1];

#ifdef X_DEBUG
    // Keep a reference of what gets donated
    m_FreeMemory += Block.m_Debug.m_TypeSize*Block.m_Count;
#endif

    // Do some debugging
#ifdef X_DEBUG
    ASSERT( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_UNDEF_TYPE ) || Block.m_Debug.m_TypeSize == TypeSize || TypeSize == 0xff0000ff );
#endif

    RemoveFromAllocatedHash( Block );
    FreeHashDonateBlock( Block );

    // Thread safety
    m_CriticalSection.EndAtomic();
}

//------------------------------------------------------------------------------

void* xmem_manager::Realloc( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber )
{
    ASSERT( TypeSize > 0 );
    ASSERT( NewCount >= 0 );

    //
    // Is this a movable pointer?
    //
    if( (((u32)((movable_entry*)&pPtr)->m_Index)&0xff0000ff) == 0xff0000ff )
    {
        pPtr = GetDataPtrFromMovablePtr( pPtr );
    }

    ASSERTS( pPtr > m_pBaseBlock, "This memory is not part of the xbase::memory" );
    ASSERTS( pPtr < ((xbyte*)m_pBaseBlock->m_pGlobalPrev)+m_pBaseBlock->m_PhysicalSize, "This memory is not part of the xbase::memory" ); 
    
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header& Block = ((xmem_block_header*)pPtr)[-1];

    // Since we are reallocating we must turn off any features
    // such the read only flag
    ASSERT( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_READ_ONLY ) == FALSE );

    // Compute the new final size
    s32 nBytes    = TypeSize * NewCount;
    s32 FinalSize = nBytes + sizeof(xmem_block_header);

#ifdef X_DEBUG
    ASSERT( Block.m_Debug.m_TypeSize == TypeSize );
    FinalSize += sizeof(xmem_block_header::lining);
    s32 PreviousSize = Block.m_Debug.m_TypeSize*Block.m_Count;
#endif

    // Go ahead and allocate
    xmem_block_header* pNewBlock = FreeHashRealloc( Block, FinalSize );

    // Run out of memory it seems
    if( pNewBlock == NULL ) 
    {
        // Thread safety
        m_CriticalSection.EndAtomic();

        if( !x_FlagIsOn( Block.m_Flags, XMEM_FLAG_OKAY_TO_FAIL ) )
        {
            x_throw( "Out of memory while reallocating" );
        }

        return NULL;
    }

    //
    // Set the count for the new block
    //
    pNewBlock->m_Count = NewCount;

    //
    // Handle all the debugging
    //

#ifdef X_DEBUG
    // Mark block as been reallocated
    x_FlagOn( pNewBlock->m_Flags, XMEM_FLAG_BEEN_REALLOC );
#endif

#ifdef X_DEBUG
    pNewBlock->m_Debug.m_TypeSize = TypeSize;
    pNewBlock->m_Debug.m_Sequence = m_DebugSequence;
    pNewBlock->m_Debug.m_pFile    = pFileName;
    pNewBlock->m_Debug.m_LineNum  = (u16)LineNumber;
    pNewBlock->m_Debug.m_nRef     = 0;
#else
    (void)pFileName;
    (void)LineNumber;
#endif

#ifdef X_DEBUG
    {
        xscope_info& ScopeInfo = x_GetScopeInfo();
        
        // Copy the scope for this alloc
        pNewBlock->m_Debug.m_nScopes = x_Min( pNewBlock->m_Debug.m_Scope.getCount(), ScopeInfo.m_CurScope );
        for( s32 i=pNewBlock->m_Debug.m_nScopes - 1; i >= 0; i-- )
        {
            pNewBlock->m_Debug.m_Scope[i] = ScopeInfo.m_Scope[ ScopeInfo.m_CurScope - 1 - i ];
        }
    }
#endif

#ifdef X_DEBUG    
    s32 i;

    // Set the final lining for debug
    // Set the linning at the front
    for( i=0; i<pNewBlock->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
    {
        pNewBlock->m_Debug.m_FrontLining.m_Bytes[i] = s_pLiningSequence[i];
    }

    // Set the linning at the end
    xmem_block_header::lining* pLining = ((xmem_block_header::lining*)(&((xbyte*)&pNewBlock[1])[nBytes]));

    for( i=0; i<pNewBlock->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
    {
        pLining->m_Bytes[i] = s_pLiningSequence[i];
    }
#endif

#ifdef X_DEBUG
    if(  x_ChooseDebugLevel( m_DebugMode ) >= XMEM_DEBUG_CHECKALL )
    {
        if( nBytes > PreviousSize )
        {
            x_memset( &((xbyte*)&pNewBlock[1])[PreviousSize], '#', nBytes - PreviousSize );
        }
    }
#endif

    //
    // Keep track of the free memory
    //
#ifdef X_DEBUG
    // Keep a reference of what gets donated
    m_FreeMemory -= (NewCount*TypeSize - PreviousSize);
    ASSERT( m_FreeMemory >= 0 );

    // Keep the stat of the smallers free memory ever
    if( m_FreeMemory < m_SmallestFreeMemory ) 
    {
        m_SmallestFreeMemory = m_FreeMemory;
    }
#endif

    // Make sure nothing went wrong with the block
    ASSERT( ValidateBlock( *pNewBlock ) );

    // Thread safety
    m_CriticalSection.EndAtomic();

#ifdef X_DEBUG
    s32 SmallestBigestBlock = GetLargestBlockFree();
    if( SmallestBigestBlock < m_SmallestBigestBlock )
    {
        m_SmallestBigestBlock = SmallestBigestBlock;
    }
#endif

    //
    // For movable data we want to actually return the index
    //
    if( x_FlagIsOn( pNewBlock->m_Flags, XMEM_FLAG_MOVABLE ) )
    {
        movable_entry Entry;
        Entry.m_Index = ((((u32)pNewBlock->m_MovableIndex)<<8)|0xff0000ff);
        return (void*)Entry.m_pPtr;
    }

    return &pNewBlock[1];
}

//------------------------------------------------------------------------------

xbool xmem_manager::SanityCheck( void )
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header*  pM = m_pBaseBlock;

    if( pM == NULL || x_ChooseDebugLevel( m_DebugMode ) < XMEM_DEBUG_BASIC )
    {
        // Thread safety
        m_CriticalSection.EndAtomic();

        return TRUE;
    }

    //
    // Start checking all blocks
    //
    do
    {
        //
        // make sure that the block is okay
        //
        ASSERT( ValidateBlock( *pM ) );

        //
        // Make sure that the count and the type make sense
        //
        if( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_FREE) == FALSE )
        {
            ASSERT( pM->m_Count > 0 );
            ASSERT( pM->m_Count < 99999999 );
#ifdef X_DEBUG
            // Make sure that there is nothing strange in the debug section

            ASSERT( pM->m_Debug.m_TypeSize > 0 );
            ASSERT( pM->m_Debug.m_TypeSize < 10*1024 );

            ASSERT( pM->m_Debug.m_nScopes  < pM->m_Debug.m_Scope.getCount() );
            ASSERT( pM->m_Debug.m_LineNum  >= 0 );
            ASSERT( pM->m_Debug.m_LineNum  <= 100000 );
#endif
        }

        //
        // Check that block is aligned
        //
        if( x_IsAlign(pM,4) == FALSE ) 
        {
            ASSERTS(0, "MISALIGNED");
            break;
        }

        // Check that global ptrs are intact
        if( (pM->m_pGlobalNext==NULL) || (pM->m_pGlobalNext->m_pGlobalPrev != pM) ) 
        {
            ASSERTS(0, "GLOBAL PTRS FRIED 01");
            break;
        }

        if( (pM->m_pGlobalPrev==NULL) || (pM->m_pGlobalPrev->m_pGlobalNext != pM)) 
        {
            ASSERTS(0, "GLOBAL PTRS FRIED 02");
            break;
        }

#ifdef X_DEBUG
        if(  x_ChooseDebugLevel( m_DebugMode ) >= XMEM_DEBUG_CHECKALL )
        {
            //
            // Check liners for overwrite and read only memory
            //
            if( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_FREE) == FALSE )
            {
                s32                             i;
                xmem_block_header::lining*      pLiningPtr;

                for( i=0; i<pM->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
                {
                    if( pM->m_Debug.m_FrontLining.m_Bytes[i] != s_pLiningSequence[i] )
                    {
                        ASSERTS(0, "FRONT LINING OVERWRITTEN");
                        break;
                    }
                }

                pLiningPtr = (xmem_block_header::lining*)(((xbyte*)pM) + sizeof(xmem_block_header) + (pM->m_Count*pM->m_Debug.m_TypeSize) );

                for( i=0; i<pM->m_Debug.m_FrontLining.m_Bytes.getCount(); i++ )
                {
                    if( pLiningPtr->m_Bytes[i] != s_pLiningSequence[i] )
                    {
                        ASSERTS(0, "REAR LINING OVERWRITTEN");
                        break;
                    }
                }

                // Check the CRC for any read only blocks
                if ( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_READ_ONLY) )
                {
                    // Do a check sum for the block
                    u32 CRC = x_memCRC32( &pM[1], pM->m_PhysicalSize - sizeof(xmem_block_header) );
                    ASSERTS( pM->m_Debug.m_MemCRC == CRC, "Read only memory block has been touch" );
                }
            }
        }
#endif

        //
        // Check that list is in memory address order
        //
        if ( pM->m_pGlobalNext <= pM ) 
        {
            if( pM->m_pGlobalNext != m_pBaseBlock )
            {
                ASSERTS(0, "OUT OF ADDRESS ORDER 01");
                break;
            }
        }

        if ( pM->m_pGlobalPrev >= pM ) 
        {
            if( pM->m_pGlobalPrev != m_pBaseBlock->m_pGlobalPrev )
            {
                ASSERTS(0, "OUT OF ADDRESS ORDER 02");
                break;
            }
        }

        //
        // Check for neighboring free blocks in global list
        //
        if( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_FREE) == x_FlagIsOn(pM->m_pGlobalNext->m_Flags, XMEM_FLAG_FREE) )
        {
            if( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_FREE) && pM->m_pGlobalNext != pM )
            {
                if( &((xbyte*)pM)[pM->m_PhysicalSize] == (xbyte*)pM->m_pGlobalNext )
                {
                    ASSERTS(0, "UN-MERGED FREE BLOCKS" );
                    break;
                }
            }
		}

        //
        // Move to next block
        //
        pM = pM->m_pGlobalNext;

    } while( pM != m_pBaseBlock );

    //
    // Make sure that all the free memory has not been touched
    //
#ifdef X_DEBUG
    if(  x_ChooseDebugLevel( m_DebugMode ) >= XMEM_DEBUG_CHECKALL )
    {       
        pM = m_pBaseBlock;

        do 
        {
            // Skip blocks which are not free
            if ( x_FlagIsOn(pM->m_Flags, XMEM_FLAG_FREE) == FALSE )
            {
                pM = pM->m_pGlobalNext;
                continue;
            }

            //
            // Check the memory for our symbol
            //
            u32* pData      = (u32*)&pM[1];
            s32  TotalSize  = pM->m_PhysicalSize - sizeof(xmem_block_header);
            s32  Count      = TotalSize>>2;
            s32  i;

            // Do 4 bytes at a time
            for( i=0; i<Count; i++ )
            {
                ASSERTS( pData[i] == 0x3f3f3f3f, "FREE MEMORY OVERRUN" );
            }

            // Finish off the block
            for( i=Count<<2; i<TotalSize; i++ )
            {
                ASSERTS( pData[i] == '?', "FREE MEMORY OVERRUN" );
            }

            //
            // Move to next block
            //
            pM = pM->m_pGlobalNext;

        } while( pM != m_pBaseBlock );
    }
#endif

    //
    // Check free hash to make sure all nodes are sorted properly
    //
    for( s32 i=0; i<m_FreeHash.getCount(); i++ )
    {
        pM = m_FreeHash[i];
        if( pM == NULL ) continue;

        //
        // Make sure that hash entries are sorted and in the right index
        //
        while( pM->m_pHashNext != m_FreeHash[i] )
        {
            // Make sure that the block is in the right index
            ASSERT( HashCompIndex( pM->m_PhysicalSize ) == i );

            // make sure it is sorted
            ASSERT( pM < pM->m_pHashNext );

            // make sure that the pointers are all good
            ASSERT( pM->m_pHashNext->m_pHashPrev == pM );
            ASSERT( pM->m_pHashPrev->m_pHashNext == pM );

            pM = pM->m_pHashNext;
        };

        // Make sure that the block is in the right index
        ASSERT( HashCompIndex( pM->m_PhysicalSize ) == i );

        // make sure that the pointers are all good
        ASSERT( pM->m_pHashNext->m_pHashPrev == pM );
        ASSERT( pM->m_pHashPrev->m_pHashNext == pM );
    }

    //
    // Check Alloc hash to make sure all nodes are sorted properly
    //
    for( s32 i=0; i<m_AllocHash.getCount(); i++ )
    {
        pM = m_AllocHash[i];
        if( pM == NULL ) continue;

        // make sire it a movable node
        ASSERT( x_FlagIsOn( pM->m_Flags, XMEM_FLAG_MOVABLE ) );

        //
        // Make sure that hash entries are sorted and in the right index
        //
        while( pM->m_pHashNext != m_AllocHash[i] )
        {
            // Make sure that the block is in the right index
            ASSERT( HashCompIndex( pM->m_PhysicalSize ) == i );

            // make sure it is sorted
            ASSERT( pM < pM->m_pHashNext );

            // make sure that the pointers are all good
            ASSERT( pM->m_pHashNext->m_pHashPrev == pM );
            ASSERT( pM->m_pHashPrev->m_pHashNext == pM );

            pM = pM->m_pHashNext;
        };

        // Make sure that the block is in the right index
        ASSERT( HashCompIndex( pM->m_PhysicalSize ) == i );

        // make sure that the pointers are all good
        ASSERT( pM->m_pHashNext->m_pHashPrev == pM );
        ASSERT( pM->m_pHashPrev->m_pHashNext == pM );

        // make sire it a movable node
        ASSERT( x_FlagIsOn( pM->m_Flags, XMEM_FLAG_MOVABLE ) );
    }

    // Make sure tha the free ptr look up is fine
    ASSERT( m_iFreePtrLookup != -1 );
    ASSERT( m_iFreePtrLookup <  m_PtrLookup.getCount() );
    ASSERT( m_iFreePtrLookup <  0xffff );
    ASSERT( m_iFreePtrLookup >= 0 );
    ASSERT( m_PtrLookup[m_iFreePtrLookup].m_Index >=0  );
    ASSERT( m_PtrLookup[m_iFreePtrLookup].m_Index < m_PtrLookup.getCount()  );
    ASSERT( m_PtrLookup[m_iFreePtrLookup].m_Index < 0xffff  );

    //
    // Sanity check small blocks
    //
    m_SB.SanityCheck();

    // Thread safety
    m_CriticalSection.EndAtomic();

    return TRUE;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetTotalMemory( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;

    s32 TotalMem = 0;

    do
    {
        TotalMem += pBlock->m_PhysicalSize;
        pBlock = pBlock->m_pGlobalNext;

    }while( pBlock != m_pBaseBlock );

    // Thread safety
    m_CriticalSection.EndAtomic();

    return TotalMem;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetWastedMemory( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;

    s32 WastedMem = 0;
#ifdef X_DEBUG
    do
    {
        if( x_FlagIsOn( pBlock->m_Flags, XMEM_FLAG_FREE ) == FALSE )
        {
            WastedMem += ( pBlock->m_PhysicalSize - pBlock->m_Count*pBlock->m_Debug.m_TypeSize ) - sizeof(xmem_block_header);
        }
        pBlock = pBlock->m_pGlobalNext;

    } while( pBlock != m_pBaseBlock );
#endif

    // Thread safety
    m_CriticalSection.EndAtomic();

    return WastedMem;

}

//------------------------------------------------------------------------------

s32 xmem_manager::GetNumberOfAllocations( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;

    s32 Count = 0;

    do
    {
        if( x_FlagIsOn( pBlock->m_Flags, XMEM_FLAG_FREE ) == FALSE )
        {
            Count++;
        }
        pBlock = pBlock->m_pGlobalNext;

    } while( pBlock != m_pBaseBlock );

    // Add the small block stuff
    Count += m_SB.GetNumberOfAllocations();

    // Thread safety
    m_CriticalSection.EndAtomic();

    return Count;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetTotalMemoryFree( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;

    s32 TotalMem = 0;

    do
    {
        if( x_FlagIsOn(pBlock->m_Flags, XMEM_FLAG_FREE ) )
        {
            TotalMem += pBlock->m_PhysicalSize;
        }
        
        pBlock = pBlock->m_pGlobalNext;
    }while( pBlock != m_pBaseBlock );


    // Add the small block stuff
    TotalMem += m_SB.GetTotalFree();

    // Thread safety
    m_CriticalSection.EndAtomic();

    return TotalMem;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetLargestBlockFree( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    s32 LargestBlock = 0;

    // Find the bigest block using the hash
    for( s32 i=m_FreeHash.getCount()-1; i>=0; i-- )
    {
        if( m_FreeHash[i] == NULL ) continue;

        xmem_block_header* pNext = m_FreeHash[i];

        do
        {
            if( pNext->m_PhysicalSize > LargestBlock )
            {
                LargestBlock = pNext->m_PhysicalSize;
            }

            pNext = pNext->m_pHashNext;

        } while( pNext != m_FreeHash[i] );
    }

    // Thread safety
    m_CriticalSection.EndAtomic();

    return LargestBlock;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetAllocatedCount( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;

    s32 TotalAllocated = 0;

    do
    {
        if( x_FlagIsOn(pBlock->m_Flags, XMEM_FLAG_FREE ) == FALSE )
        {
            TotalAllocated += pBlock->m_PhysicalSize ;
        }

        pBlock = pBlock->m_pGlobalNext;

    }while( pBlock != m_pBaseBlock );

    // Add the small block stuff
    TotalAllocated += m_SB.GetTotalUsed();

    // Thread safety
    m_CriticalSection.EndAtomic();

    return TotalAllocated;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetBlockCount( void ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header* pBlock = m_pBaseBlock;
    s32 nBlocks=0;

    do
    {   
        nBlocks++;
        pBlock = pBlock->m_pGlobalNext;

    }while( pBlock != m_pBaseBlock );

    // Thread safety
    m_CriticalSection.EndAtomic();

    return nBlocks;
}

//------------------------------------------------------------------------------

u32  xmem_manager::GetMemorySequence  ( void ) const
{
    return m_DebugSequence;
}

//------------------------------------------------------------------------------

void xmem_manager::GetMemHashStats( s32* pData, s32 Count ) const
{
    // Thread safety
    m_CriticalSection.BeginAtomic();

    for( s32 i=0; i<Count; i++ )
    {
        if( i >= m_FreeHash.getCount() ) break;

        xmem_block_header* pBlock = m_FreeHash[i];
        pData[i]=0;                

        if( pBlock )
        {
            do
            {
                pData[i]++;
                pBlock = pBlock->m_pHashNext;

            } while( pBlock->m_pHashNext != m_FreeHash[i] );
        }
    }

    // Thread safety
    m_CriticalSection.EndAtomic();
}

//------------------------------------------------------------------------------

void xmem_manager::SetReadOnly( void* pPtr, xbool bReadOnly )
{
    ASSERT( pPtr );

    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header& Block = ((xmem_block_header*)pPtr)[-1];
    if( bReadOnly )
    {
        // Set the read only flag
        x_FlagOn( Block.m_Flags, XMEM_FLAG_READ_ONLY );

        // Do a check sum for the block
#ifdef X_DEBUG
        Block.m_Debug.m_MemCRC = x_memCRC32( &(&Block)[1], Block.m_PhysicalSize - sizeof(Block) );
#endif
    }
    else
    {
        // Set the read only flag
        x_FlagOff( Block.m_Flags, XMEM_FLAG_READ_ONLY );

        // Do a check sum for the block
#ifdef X_DEBUG
        Block.m_Debug.m_MemCRC = 0xffffffff;
#endif
    }

    // Okay we are done
    m_CriticalSection.EndAtomic();
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetElementCount( void* pPtr ) const
{
    ASSERT( pPtr );

    // Thread safety
    m_CriticalSection.BeginAtomic();

    xmem_block_header& Block = ((xmem_block_header*)pPtr)[-1];
    s32                Count;

    if( x_FlagIsOn( Block.m_Flags, XMEM_FLAG_UNDEF_TYPE ) )
    {
        Count = 1;
    }
    else
    {
        Count = Block.m_Count;
    }

    // Okay we are done
    m_CriticalSection.EndAtomic();

    return Count;
}

//------------------------------------------------------------------------------
// This function in the future will need to deal with movable pointers
// movable pointers have as a flag the fact that they are negative.
//------------------------------------------------------------------------------
xmem_block_header* xmem_manager::GetBlockFromPtr( void* pPtr ) const
{
    if( pPtr == NULL) return NULL;

    if( pPtr < 0 ) 
    {
        ASSERT( 0 );
    }

    return &((xmem_block_header*)pPtr)[-1];
}

//------------------------------------------------------------------------------

const char* xmem_manager::LimitLength( const char* pString, s32 count ) const
{
    if (!pString)
        return "<none>";

    if (x_strlen(pString) > count)
        return pString+x_strlen(pString)-count;

    return pString;
}
//------------------------------------------------------------------------------

const char* xmem_manager::ScopeDumpFunction( xmem_block_header& Header ) const
{
    static xsafe_array<char, 256> m_Data;

#ifdef X_DEBUG
    s32 Index=0;
    for( s32 i=0; i<Header.m_Debug.m_nScopes; i++ )
    {
        if( i > 0 )
        {
            Index += x_sprintf( &m_Data[Index], m_Data.getCount()-Index, "::" ); 
        }

        Index += x_sprintf( &m_Data[Index], m_Data.getCount() - Index, "%s(%d,%s)", 
                            Header.m_Debug.m_Scope[i]->m_pScopeName,
                            Header.m_Debug.m_Scope[i]->m_LineNumber,
                            LimitLength( Header.m_Debug.m_Scope[i]->m_pFileName, 16 ) );

        ASSERT( Index < m_Data.getCount() );
    }
#endif
    return &m_Data[0];
}

//------------------------------------------------------------------------------

void xmem_manager::MemDump( const char* pFileName )
{
#ifdef X_DEBUG
    ASSERT( pFileName );
	xfile File;
    File.Open( pFileName, "wt" );

    x_DebugMsg("Starting memory dump.....\n");
    if( m_pBaseBlock == NULL ) return;

    File.Printf( "Type Sequence Malloc    Count     Size       Addr Line File                              Scope\n" );
    File.Printf( "==== ======== ====== ======== ======== ========== ==== ================================  ================================\n" );

    s32 CurrentTotal=0;
    xmem_block_header* pHeader = m_pBaseBlock;
    do 
    {
        // Dont deal with free blocks
        if( x_FlagIsOn(pHeader->m_Flags, XMEM_FLAG_FREE) )
        {
            pHeader = pHeader->m_pGlobalNext;
            continue;
        }

        #ifdef X_DEBUG
        File.Printf( "%4d %8d %s %8d %8d 0x%08X %4d %-32s  %-32s\n", 
                          pHeader->m_Debug.m_TypeSize,
                          pHeader->m_Debug.m_Sequence, 
                          x_FlagIsOn( pHeader->m_Flags, XMEM_FLAG_BEEN_NEW) ? "new   " : "malloc",
                          pHeader->m_Count, 
                          pHeader->m_Debug.m_TypeSize*pHeader->m_Count, 
                          (u32)(u64)&pHeader[1],
                          pHeader->m_Debug.m_LineNum, 
                          LimitLength(pHeader->m_Debug.m_pFile,32),
                          ScopeDumpFunction(*pHeader) );
        #endif

        CurrentTotal += pHeader->m_PhysicalSize;

        pHeader = pHeader->m_pGlobalNext;

    } while( pHeader != m_pBaseBlock );


    File.Printf( "==== ======== ====== ======== ======== ========== ==== ================================  ================================\n" );
    File.Printf( "Type Sequence Malloc    Count     Size       Addr Line File                              Scope\n" );
    File.Printf( ".... ........ ...... ........ %8d ........ .... ....\n", CurrentTotal);

    //
    // Report scope stats
    //
    {
        s32 i,j;
        s32 nScopes = 0;
            pHeader = m_pBaseBlock;
        do
        {
            // Dont deal with free blocks
            if( x_FlagIsOn(pHeader->m_Flags, XMEM_FLAG_FREE) )
            {
                pHeader = pHeader->m_pGlobalNext;
                continue;
            }

            xsafe_array<xbool,4> bFound;

            // Mark our scopes as not found
            for( j=0; j<pHeader->m_Debug.m_nScopes; j++ ) bFound[j] = FALSE;

            // Find the scope that we need to contribute
            for( i=0; i<nScopes; i++ )
            {
                // Add the total to the right entries
                for( s32 j=0; j<pHeader->m_Debug.m_nScopes; j++ )
                {
                    if( m_InfoDump[i].m_pPtr == pHeader->m_Debug.m_Scope[j] )
                    {
                        ASSERT( bFound[j] == FALSE );
                        m_InfoDump[i].m_Total += pHeader->m_Count * pHeader->m_Debug.m_TypeSize;
                        m_InfoDump[i].m_nAllocations++;
                        bFound[j] = TRUE;

                        // Only one scope can match (if not there is a bug) and we already found it
                        // so break out
                        break;
                    }
                }
            }

            // make sure that we found all the scopes
            for( s32 j=0; j<pHeader->m_Debug.m_nScopes; j++ )
            {
                // If we didnt find the scope added
                if( bFound[j] == FALSE )
                {
                    m_InfoDump[nScopes].m_Total        = pHeader->m_Count * pHeader->m_Debug.m_TypeSize;
                    m_InfoDump[nScopes].m_pPtr         = pHeader->m_Debug.m_Scope[j];
                    m_InfoDump[nScopes].m_nAllocations = 1;
                    nScopes++;
                }
            }
        
            // Okay move on to the next header
            pHeader = pHeader->m_pGlobalNext;

        } while( pHeader != m_pBaseBlock );

        File.Printf( "\n\n\n" );
        File.Printf( "  Allocs                 TotalBytes      Comment\n" );
        File.Printf( "========                 ==========      ================================\n" );

        for( i = 0; i < nScopes; i++ )
        {
            File.Printf( "%8d ...... ........ %10d .... %s(%d,%s)\n",
                              m_InfoDump[i].m_nAllocations, 
                              m_InfoDump[i].m_Total, 
                              ((const xscope_node*)m_InfoDump[i].m_pPtr)->m_pScopeName, 
                              ((const xscope_node*)m_InfoDump[i].m_pPtr)->m_LineNumber,
                              LimitLength( ((const xscope_node*)m_InfoDump[i].m_pPtr)->m_pFileName, 16 ) );
        }

        File.Printf( "========                 ==========      ================================\n" );
        File.Printf( "  Allocs                 TotalBytes      Comment\n" );
    }

    //
    // file stats
    //
    {
        s32 i;
        s32 nFiles = 0;
            pHeader = m_pBaseBlock;
        do
        {
            // Dont deal with free blocks
            if( x_FlagIsOn(pHeader->m_Flags, XMEM_FLAG_FREE) )
            {
                pHeader = pHeader->m_pGlobalNext;
                continue;
            }

            // Find the scope that we need to contribute
            for( i=0; i<nFiles; i++ )
            {
                // Add the total to the right entries
                if( m_InfoDump[i].m_pPtr == pHeader->m_Debug.m_pFile )
                {
                    m_InfoDump[i].m_Total += pHeader->m_Count * pHeader->m_Debug.m_TypeSize;
                    m_InfoDump[i].m_nAllocations++;
                    break;
                }
            }

            if( i == nFiles )
            {
                m_InfoDump[nFiles].m_pPtr         = pHeader->m_Debug.m_pFile;
                m_InfoDump[nFiles].m_Total        = pHeader->m_Count * pHeader->m_Debug.m_TypeSize;
                m_InfoDump[nFiles].m_nAllocations = 1;
                nFiles++;
            }

            // Okay move on to the next header
            pHeader = pHeader->m_pGlobalNext;

        } while( pHeader != m_pBaseBlock );


        File.Printf( "\n\n\n" );
        File.Printf( "       Allocs                 TotalBytes      File\n" );
        File.Printf( "     ========                 ==========      ================================\n" );

        for( i=0; i<nFiles; i++ )
        {
            File.Printf( "     %8d ...... ........ %10d .... %s\n", 
                                m_InfoDump[i].m_Total, 
                                m_InfoDump[i].m_Total, 
                                LimitLength((const char*)m_InfoDump[i].m_pPtr ,64) );
        }

        File.Printf( "     ========                 ==========      ================================\n" );
        File.Printf( "       Allocs                 TotalBytes      File\n" );
    }

    //
    // Basic stats
    //
    {
        File.Printf( "\n\n\n" );
        File.Printf( "                                   Value      Summary\n" );
        File.Printf( "                              ==========      ================================\n" );
        File.Printf( "                              %10d .... Sequence\n",        GetDebugSequence()     );
        File.Printf( "                              %10d .... Current Count\n\n", GetNumberOfAllocations() );
        File.Printf( "                              %10d .... Total Bytes\n\n",   GetTotalMemory()   );
        File.Printf( "                              %10d .... Current Bytes\n",   GetAllocatedCount() );
        File.Printf( "\n" );
    }

    //
    // Basic stats
    //
    {
        File.Printf( "                              %10d .... Minimun Bytes\n",  m_SmallestFreeMemory     );
        File.Printf( "                              %10d .... Smallest block\n",   m_SmallestBigestBlock     );
        File.Printf( "                              %10d .... Free Bytes\n",      GetTotalMemoryFree()    );
        File.Printf( "                              %10d .... Largest block\n",   GetLargestBlockFree()  );
        File.Printf( "                              %10d .... Block Fragments\n", GetBlockCount()    );
        File.Printf( "\n" );
    }

    File.Close();
    x_DebugMsg("End of memory dump....\n");
#endif
}



//==============================================================================
//==============================================================================
//==============================================================================
// DEAL WITH NATIVE MEMORY MANAGER
//==============================================================================
//==============================================================================
//==============================================================================
#else  // X_USE_NATIVE_NEW_AND_DELETE

//==============================================================================
// Main memory manager for windows
// This is just a fake memory manager now. It allocates and free memories from windows
// Now the memory buffer looks like this:
//      ---------------------------------
//      |
//      |User alignment paddings
//      |
//      ---------------------------------
//      |xmem_block_header:
//      |members
//      |m_uPaddings (32 bits)  (in case of the pointer[A] overwritten header members)
//      |(Header alignment paddings: optional, may overlapped with m_uPaddings, last 4 bytes are for pointer[A] which points to the header
//      |Last of the paddings: pointer[A]
//      |--------------------------------
//      |
//      |real buffer
//      |
//      ---------------------------------
//
//      For example:
//      xmem_block_header is 0x50B with 16B(0x10) aligned, and user request is 64B(0x40) aligned
// 0xF40---------------------------------
//      |
//      | 0x30 Bytes paddings
//      |
// 0xF70---------------------------------
//      |xmem_block_header:
//      |members
//      |m_uPaddingsB (32 bits)  (in case of the pointer[B] overwriting header members)
//      |m_uPaddingsA (32 bits)  (in case of the pointer[A] overwriting header members)
//      |(Header alignment paddings: optional, may overlapped with m_uPaddings, last 4 bytes are for pointer[A] which points to the header
//      |Last of the paddings: pointer[B] points back to the header
//      |Last of the paddings: pointer[A] points back to the physical beginning of the block
// pxFC0---------------------------------
//      |
//      |real buffer
//      |
//      ---------------------------------
//            --[liushifeng]
//  So now, there are two ways to look back for the block header when you have a pointer to the buffer
//    A:  *( pRealbuffer-sizeof(void*)*2 )
//    B:  ((xmem_block_header*)pRealbuffer)[-1]        (this is for backward compatibility)
//  The way to reach the physical beginning of current memory block when you have a pointer to the buffer:
//    *( pRealbuffer-sizeof(void*) )
//  The way to reach the physical beginning of current memory block when you have a pointer to the header:
//    pHeader->m_pPhysicalBeginning
//==============================================================================

class xmem_manager
{
public:
    void                    Initialize                  ( void );
    void*                   Malloc                      ( s32 TypeSize, s32 Count, u16 Flags, const char* pFileName, s32 LineNumber );
    void                    Free                        ( void* pPtr, s32 TypeSize );
    void*                   Realloc                     ( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber );
    xmem_block_header*      GetBlockFromPtr             ( void* pPtr ) const;

//    void                    SetReadOnly                 ( void* pPtr, xbool bReadOnly );
//Functions below are fake, in order to make the wrapper function more uniform
    //Begin fake
    xbool                   SanityCheck                 ( void ) { return true; };
    void                    DonateMemory                ( void* pBlockAddr, s32 nBytes ){}
    s32                     GetElementCount             ( void* pPtr ) const;
    //End fake

protected:

    s32                     GetAligment                 ( u16 Flags );
    void                    RemoveNodeFromGlobalList    ( xmem_block_header* pNode );
    void                    AddNodeToGlobalList         ( xmem_block_header* pNode );
    void*                   AlignedMalloc               (s32 size, s32 alignment);
    void                    AlignedFree                 ( void* pPtr);
    void*                   AlignedRealloc              (void* pPtr, s32 oldSize, s32 newSize, s32 alignment);

protected:

    mutable xcritical_section               m_CriticalSection;      // Local critical section
    xmem_block_header*                      m_pHead;                // The lowest block in memory
};

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

void xmem_manager::Initialize( void )
{
    m_pHead = NULL;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetAligment( u16 Flags )
{
    return 1<<(((Flags & XMEM_FLAG_ALIGN_MASK )>>2)+2);
}

//------------------------------------------------------------------------------

xmem_block_header* xmem_manager::GetBlockFromPtr( void* pPtr ) const
{
    if( pPtr == NULL) return NULL;

    if( pPtr < 0 ) 
    {
        ASSERT( 0 );
    }

    return &( ((xmem_block_header*)pPtr)[-1] );
}

//------------------------------------------------------------------------------

void xmem_manager::RemoveNodeFromGlobalList( xmem_block_header* pNode )
{
    if( m_pHead == pNode )
    {
        m_pHead = pNode->m_pGlobalNext;
        if( m_pHead ) m_pHead->m_pGlobalPrev = NULL;
    }
    else
    {
        if( pNode->m_pGlobalNext )
        {
            pNode->m_pGlobalNext->m_pGlobalPrev = pNode->m_pGlobalPrev;
            pNode->m_pGlobalPrev->m_pGlobalNext = pNode->m_pGlobalNext;
        }
        else
        {
            pNode->m_pGlobalPrev->m_pGlobalNext = NULL;
        }
    }
}

//------------------------------------------------------------------------------

void xmem_manager::AddNodeToGlobalList( xmem_block_header* pNode )
{
    pNode->m_pGlobalNext = m_pHead;
    pNode->m_pGlobalPrev = NULL;

    if( m_pHead )
    {
        pNode->m_pGlobalPrev = NULL;

        m_pHead->m_pGlobalPrev = pNode;
        m_pHead = pNode;
    }
    else
    {
        pNode->m_pGlobalNext = NULL;
        pNode->m_pGlobalPrev = NULL;

        m_pHead = pNode;
    }
}

//------------------------------------------------------------------------------

void* xmem_manager::Malloc( s32 TypeSize, s32 Count, u16 Flags, const char*  pFileName, s32 LineNumber  )
{
    ASSERT( TypeSize > 0 );
    ASSERT( Count > 0 );

    //
    // We deal only in linear world
    //
    xscope_atomic( m_CriticalSection );

    //
    // Determine the size for everything
    //
    s32                nAlignment    = GetAligment(Flags);
    s32                nHeaderSize   = (s32)x_Align(sizeof(xmem_block_header), nAlignment);
    s32                nBytes        = TypeSize*Count;
    s32                FinalSize     = nHeaderSize + nBytes; // Final size doesn't need aligment by us -- x_Align( nHeaderSize + nBytes, nAlignment);

    //
    // Allocated aligned memory from windows
    //
    void*              pBlock        = AlignedMalloc( FinalSize, nAlignment );
    if( pBlock == NULL ) 
    {
        // Out of memory
        ASSERT( FALSE );
        return NULL;
    }

    void*              pBuffer       = (void*)((u8*)pBlock + nHeaderSize );
    xmem_block_header* pHeader       = (xmem_block_header*)( (u8*)pBuffer - sizeof(xmem_block_header) );

    //
    // Set the header information
    //
    pHeader->m_PhysicalSize         = FinalSize;
    pHeader->m_Count                = Count;
    pHeader->m_Flags                = Flags;
    pHeader->m_nXPtrs               = 1;
    pHeader->m_pPhysicalBeginning   = pBlock;

    // Update the pointers
    AddNodeToGlobalList( pHeader );

    //
    // Done return the block
    //
    return pBuffer;
}

//------------------------------------------------------------------------------

void xmem_manager::Free( void* pPtr, s32 TypeSize )
{
    ASSERT(pPtr);

    //
    // We deal only in linear world
    //
    xscope_atomic( m_CriticalSection );

    //
    // Get the real memory block
    //
    xmem_block_header* pHeader       = GetBlockFromPtr( pPtr );

    //
    // Remove form link list
    //
    RemoveNodeFromGlobalList( pHeader );

    // we can free up the memory
    AlignedFree( pHeader->m_pPhysicalBeginning );
}

//------------------------------------------------------------------------------

void* xmem_manager::Realloc( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber )
{
    ASSERT( pPtr );
    ASSERT( TypeSize > 0 );
    ASSERT( NewCount > 0 );

    //
    // We deal only in linear world
    //
    xscope_atomic( m_CriticalSection );


    //
    // Get the real memory block
    //
    xmem_block_header* pHeader       = GetBlockFromPtr( pPtr );
    xmem_block_header  HeaderBackup  = *pHeader;

    //
    // Remove it from the global link list
    //
    RemoveNodeFromGlobalList( pHeader );

    //
    // Get the size for everything again
    //
    s32                nAlignment    = GetAligment(pHeader->m_Flags);
    s32                nHeaderSize   = (s32)x_Align(sizeof(xmem_block_header), nAlignment);
    s32                nBytes        = TypeSize*NewCount;
    s32                FinalSize     = nHeaderSize + nBytes; // Final size doesn't need aligment by us -- x_Align( nHeaderSize + nBytes, nAlignment);

    //
    // Try to realloc the memory
    // 
    void* pBlock = AlignedRealloc( pHeader->m_pPhysicalBeginning, pHeader->m_PhysicalSize, FinalSize, nAlignment);
    if( pBlock == NULL ) 
    {
        // Out of memory
        ASSERT( FALSE );
        return NULL;
    }

    //
    // Compute the location of the new buffer
    //
    void*              pBuffer       = (void*)((u8*)pBlock + nHeaderSize );
                       pHeader       = (xmem_block_header*)( (u8*)pBuffer - sizeof(xmem_block_header) );

    //
    // Setup the new header
    //
    *pHeader  = HeaderBackup;

    pHeader->m_Count              = NewCount;
    pHeader->m_PhysicalSize       = FinalSize; 
    pHeader->m_pPhysicalBeginning = pBlock;

    //
    // Added back into the global link list
    //
    AddNodeToGlobalList( pHeader );

    //
    // Done
    //
    return pBuffer;
}

//------------------------------------------------------------------------------

s32 xmem_manager::GetElementCount( void* pPtr ) const
{
    s32 Count = -1;
    xmem_block_header* pHeader = GetBlockFromPtr( pPtr );

    if( x_FlagIsOn( pHeader->m_Flags, XMEM_FLAG_UNDEF_TYPE ) )
    {
        Count = 1;
    }
    else
    {
        Count = pHeader->m_Count;
    }

    return Count;
}

#undef malloc
#undef free

#include <stdlib.h>

void* xmem_manager::AlignedMalloc(s32 size, s32 alignment)
{
    ASSERT( size >= 0 );
    
#ifdef TARGET_PC
    return _aligned_malloc(size, alignment);
#elif defined TARGET_OSX
    ASSERT( alignment <= 16 );
    return malloc( size );
#else
    u8* buffer = NULL;
    if ( size > 0 )
    {
        s32 finalSize = (size + alignment);
        buffer = (u8*)malloc(finalSize);
        
        xuptr bufferValue = (xuptr)buffer;
        s32 offset = (alignment - bufferValue % alignment);
        buffer = (u8*)(bufferValue + offset);
        *(buffer - 1) = (u8)offset;
    }
    return buffer;
#endif
}

void xmem_manager::AlignedFree(void* pPtr)
{
    ASSERT(pPtr);
#ifdef TARGET_PC
    _aligned_free(pPtr);
#elif defined TARGET_OSX
    free( pPtr );
#else
    void* originalBuffer = (void*)((xuptr)pPtr - *(((u8*)pPtr) - 1));
    free(originalBuffer);
#endif
}

void* xmem_manager::AlignedRealloc(void* pPtr, s32 oldSize, s32 newSize, s32 alignment)
{
    ASSERT(pPtr);

#ifdef TARGET_PC
    return _aligned_realloc(pPtr, newSize, alignment);
#else
    void* newBuffer = AlignedMalloc(newSize, alignment);
    if ( newBuffer )
    {
        s32 copySize = oldSize > newSize ? newSize : oldSize;
        memcpy(newBuffer, pPtr, copySize);
    }

    // Not matter if success or failed, we always free the old memory block
    AlignedFree(pPtr);
    return newBuffer;
#endif
}

#define malloc X_MALLOC_HANDLER
#define free X_MALLOC_HANDLER


//------------------------------------------------------------------------------
/*
void xmem_manager_win::SetReadOnly( void* pPtr, xbool bReadOnly )
{
    ASSERT( pPtr );

    // Thread safety
    //     m_CriticalSection.BeginAtomic();

    xmem_block_header_win& Block = *GetBlockFromPtr(pPtr);
    if( bReadOnly )
    {
        // Set the read only flag
        x_FlagOn( Block.m_Flags, XMEM_FLAG_READ_ONLY );

        // Do a check sum for the block
#ifdef X_DEBUG
        Block.m_Debug.m_MemCRC = x_memCRC32( &(&Block)[1], Block.m_PhysicalSize - sizeof(Block) );
#endif
    }
    else
    {
        // Set the read only flag
        x_FlagOff( Block.m_Flags, XMEM_FLAG_READ_ONLY );

        // Do a check sum for the block
#ifdef X_DEBUG
        Block.m_Debug.m_MemCRC = 0xffffffff;
#endif
    }

    // Okay we are done
    //     m_CriticalSection.EndAtomic();
}
*/


//==============================================================================
//==============================================================================
//==============================================================================
// DEAL WITH NATIVE MEMORY MANAGER
//==============================================================================
//==============================================================================
//==============================================================================
#endif


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// INTERFACE FUNCTIONS
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static xmem_manager* s_pMemManager=NULL;

//------------------------------------------------------------------------------
#undef new
#undef delete
void x_MemoryInit( void )
{
    if ( NULL == s_pMemManager )
    {
#ifdef TARGET_3DS
        extern void _x_InitSystemMemory();
        _x_InitSystemMemory();
#endif
        s_pMemManager = new xmem_manager;
        s_pMemManager->Initialize();
    }
}

//------------------------------------------------------------------------------

void x_MemoryKill( void )
{
    if( s_pMemManager ) delete s_pMemManager;
    s_pMemManager = NULL;
#ifdef TARGET_3DS
    extern void _x_KillSystemMemory();
    _x_KillSystemMemory();
#endif
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

//DOM-IGNORE-END

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pBlockAddr  - The pointer to the memory to be donated to the x memory system. 
//                   Note that the system only likes 4 byte align data.
//     nBytes      - Size of the buffer been given to the memory system. Note that
//                   the system only likes 4 byte align data.
// Returns:
//     void
// Summary:
//     Donates a block of memory to the x_memory system
// Description:
//     The function is commonly used only at the beggining of the program execution.
//     This is how the x_memory system optains its memory to manage.
//------------------------------------------------------------------------------
void x_DonateBlock( void* pBlockAddr, s32 nBytes )
{
    ASSERT(s_pMemManager);
    s_pMemManager->DonateMemory( pBlockAddr, nBytes );
}

//------------------------------------------------------------------------------
// Author:
//     ....empty add or delete
// Arguments:
//     void....empty leave or add
// Returns:
//     void....empty leave or add
// Summary:
//     ....empty must be fill
// Description:
//     ....empty must be fill
// See Also:
//     ....empty add or delete
//------------------------------------------------------------------------------
void x_Real_Free( void* pPtr, s32 TypeSize )
{
    ASSERT(s_pMemManager);
    x_MemSanityCheck();
    if(s_pMemManager)
        s_pMemManager->Free( pPtr, TypeSize );
    x_MemSanityCheck();
}
//------------------------------------------------------------------------------
// Author:
//     ....empty add or delete
// Arguments:
//     void....empty leave or add
// Returns:
//     void....empty leave or add
// Summary:
//     ....empty must be fill
// Description:
//     ....empty must be fill
// See Also:
//     ....empty add or delete
//------------------------------------------------------------------------------
void* x_Real_Malloc( s32 TypeSize, s32 Count, u32 Flags, const char*  pFileName, s32 LineNumber )
{
    ASSERT(s_pMemManager);
    x_MemSanityCheck();
    void* pData = s_pMemManager->Malloc( TypeSize, Count, Flags, pFileName, LineNumber );
    x_MemSanityCheck();
    return pData;
}

//------------------------------------------------------------------------------
// Author:
//     ....empty add or delete
// Arguments:
//     void....empty leave or add
// Returns:
//     void....empty leave or add
// Summary:
//     ....empty must be fill
// Description:
//     ....empty must be fill
// See Also:
//     ....empty add or delete
//------------------------------------------------------------------------------
void* x_Real_Realloc( void* pPtr, s32 TypeSize, s32 NewCount, const char* pFileName, s32 LineNumber )
{
    ASSERT(pPtr);
    ASSERT(s_pMemManager);
    x_MemSanityCheck();
    void* pData = s_pMemManager->Realloc( pPtr, TypeSize, NewCount, pFileName, LineNumber );
    x_MemSanityCheck();
    return pData;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     void
// Summary:
//     Checks the memory system for any coruption. VERY HANDY FOR DEBBUING.
// Description:
//     This is the primary way to debug memory. Just call the function and it will
//     make sure that the integrity of memory is good. 
// See Also:
//     x_SetMemoryReadOnly
//------------------------------------------------------------------------------
void x_MemSanityCheck ( void )
{
    ASSERT(s_pMemManager);
    ASSERT( s_pMemManager->SanityCheck() );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pPtr      - Pointer to the memory block allocated by x_memory which is 
//                 going to be mark as read only.
//     bReadOnly - When this value is TRUE it makes the block read only other wise
//                 it makes the block Read and Write.
// Returns:
//     void
// Summary:
//     Turns on/off the read only flag in a memory block.
// Description:
//     This is a good way to protected your read only data. It allows you to see 
//     if someone is trashing your data. The time when the memory manager checks 
//     is when the x_MemSanityCheck gets call.
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
s32 x_GetElementCount( void* pPtr )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetElementCount( pPtr );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
xmem_block_header*  x_GetBlockFromPtr( void* pPtr )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetBlockFromPtr( pPtr );
}


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
void* x_GetDataPtrFromMovablePtr( void* pPtr )
{
    return pPtr;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pPtr      - Pointer to the memory block allocated by x_memory which is 
//                 going to be mark as read only.
//     bReadOnly - When this value is TRUE it makes the block read only other wise
//                 it makes the block Read and Write.
// Returns:
//     void
// Summary:
//     Turns on/off the read only flag in a memory block.
// Description:
//     This is a good way to protected your read only data. It allows you to see 
//     if someone is trashing your data. The time when the memory manager checks 
//     is when the x_MemSanityCheck gets call.
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
// void x_SetMemoryReadOnly( void* pPtr, xbool bReadOnly )
// {
//     ASSERT(s_pMemManager);
//     s_pMemManager->SetReadOnly( pPtr, bReadOnly );
// }
//#define X_USE_NATIVE_NEW_AND_DELETE
//==============================================================================
//==============================================================================
//==============================================================================
// DEAL WITH NATIVE MEMORY MANAGER
//==============================================================================
//==============================================================================
//==============================================================================
#ifndef X_USE_NATIVE_NEW_AND_DELETE

#error 0

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pBlockAddr  - The pointer to the memory to be donated to the x memory system. 
//                   Note that the system only likes 4 byte align data.
//     nBytes      - Size of the buffer been given to the memory system. Note that
//                   the system only likes 4 byte align data.
// Returns:
//     void
// Summary:
//     Donates a block of memory to the x_memory system
// Description:
//     The function is commonly used only at the beggining of the program execution.
//     This is how the x_memory system optains its memory to manage.
//------------------------------------------------------------------------------
void x_DonateBlock( void* pBlockAddr, s32 nBytes )
{
    ASSERT(s_pMemManager);
    s_pMemManager->DonateMemory( pBlockAddr, nBytes );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     void
// Summary:
//     Returns the very first block of memory in the system
// Description:
//     This is used only for debuging the memory system. The most people should
//     really dont touch this. As it is not thread safe or friendly in any way.
//------------------------------------------------------------------------------
xmem_block_header* x_GetFirstBlock( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetFirstBlock();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     Total avariable memory in the x_memory system
// Summary:
//     return the total memory given to x_memory
// Description:
//     This is a handy but slow way to get how much memory has been given to the 
//     x_memory system.
// See Also:
//     x_GetTotalMemoryFree x_GetLargestBlockFree x_GetMemorySequence x_GetMemHashStats
//------------------------------------------------------------------------------
s32 x_GetTotalMemory( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetTotalMemory();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     The avariable free memory of the system at this point.
// Summary:
//     Returns how much free memory remains in the system
// Description:
//     This is a handy but slow way to get how much free memory we have left in
//     the x_memory system.
// See Also:
//     x_GetTotalMemory x_GetLargestBlockFree x_GetMemorySequence x_GetMemHashStats x_GetTotalMemUsed
//------------------------------------------------------------------------------
s32 x_GetTotalMemoryFree( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetTotalMemoryFree();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     The largers contigious free block currently avariable in the x_memory.
// Summary:
//     Returns the largest free block in x_memory
// Description:
//     This is a handy but slow way to get the largest free block in the x_memory.
// See Also:
//     x_GetTotalMemory x_GetTotalMemoryFree x_GetMemorySequence x_GetMemHashStats
//------------------------------------------------------------------------------
s32 x_GetTotalMemUsed( void )
{
    return x_GetTotalMemory()-x_GetTotalMemoryFree();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     The largers contigious free block currently avariable in the x_memory.
// Summary:
//     Returns the largest free block in x_memory
// Description:
//     This is a handy but slow way to get the largest free block in the x_memory.
// See Also:
//     x_GetTotalMemory x_GetTotalMemoryFree x_GetMemorySequence x_GetMemHashStats
//------------------------------------------------------------------------------
s32 x_GetLargestBlockFree( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetLargestBlockFree();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     void
// Returns:
//     The current sequence number of allocation. This is also equal to how many 
//     allocation has been done since we started. But this is only true untill it
//     rolls over. But it is a 32bit integer so it will take some time before
//     that happens.
// Summary:
//     How many allocation have been made since the biggining of the program execution.
// Description:
//     Each block which is allocated or reallocated is given a nunique sequence number.
//     This number is commonly used for debug. It currently has not other uses.
// See Also:
//     x_GetTotalMemory x_GetTotalMemoryFree x_GetLargestBlockFree x_GetMemHashStats
//------------------------------------------------------------------------------
u32  x_GetMemorySequence( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetMemorySequence();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pData    - This is a pointer to a buffer containing "Count" number of elements.
//     Count    - How many elements there are in the pData. Note that the hash is currently 32 long.
// Returns:
//     void
// Summary:
//     Gets how many free blocks there are in each of the free hash entries
// Description:
//     This is a handy way to find how how many blocks there are in each hash entry.
//     This is most commonly used to determine if the memory manager needs to be 
//     upgraded in some way. It tells you how many free blocks there are for each size.
// See Also:
//     x_GetTotalMemory x_GetTotalMemoryFree x_GetLargestBlockFree x_GetMemHashStats
//------------------------------------------------------------------------------
void x_GetMemHashStats( s32* pData, s32 Count )
{
    ASSERT(s_pMemManager);
    s_pMemManager->GetMemHashStats( pData, Count );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
void x_SetDebugingMode( xmem_debug_mode Mode )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->SetDebugMode( Mode );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
s32 x_GetWastedMemory( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetWastedMemory();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
s32 x_GetNumberOfAllocations( void )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->GetNumberOfAllocations();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
void x_MoveMemory( void )
{
    ASSERT(s_pMemManager);
    x_MemSanityCheck();
    s_pMemManager->MoveMemory();
    x_MemSanityCheck();
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
xmem_flags x_MemAligmentToEnum( s32 Aligment )
{
    ASSERT(Aligment > 0);
    ASSERT( x_IsPow2( Aligment ) );
    ASSERT( Aligment < 1024*64 );

    // make sure is in rage
    if( Aligment < 4 ) 
    {
        Aligment = 4;
    }

    // okay lets 
    s32 Index;
    for( Index = 0; Aligment; Index++ )
    {
        Aligment >>= 1;
    }

    Aligment = Index - 3;
    Aligment <<= 2;

    ASSERT( Aligment >= XMEM_FLAG_ALIGN_4B );
    ASSERT( Aligment <= XMEM_FLAG_ALIGN_64K );

    return (xmem_flags)Aligment;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
void* x_GetDataPtrFromMovablePtr( void* pPtr )
{
    ASSERT(pPtr);
    ASSERT(s_pMemManager);
    return s_pMemManager->GetDataPtrFromMovablePtr( pPtr );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
// Returns:
//     void
// Summary:
// Description:
// See Also:
//     x_MemSanityCheck
//------------------------------------------------------------------------------
void x_MemDump( const char* pFileName )
{
    ASSERT(s_pMemManager);
    return s_pMemManager->MemDump( pFileName );
}

#else //#ifndef X_USE_NATIVE_NEW_AND_DELETE

void x_MemDump( const char* pFileName )
{
    ASSERT( FALSE );
}
#endif
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//  FUNCTIONS FOR C++   new  new[]  delete  delete[]
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//DOM-IGNORE-BEGING

/*
#ifndef USE_NATIVE_NEW_AND_DELETE

#ifdef new
#undef new
#endif

//==========================================================================

void* operator new( xbase::xalloc_size Size, xbase::u32 Flags, char* pFileName, s32 LineNumber )
{
    ASSERT(0);
    return( x_Real_Malloc( (s32)Size, xbase::xmem_flags(Flags), pFileName, LineNumber ) );
}

//==========================================================================
void operator delete( void* pMemory )
{
    x_free( pMemory );
}

//==========================================================================

void operator delete [] ( void* pMemory )
{
    x_free( pMemory );
}

//==========================================================================

void operator delete( void* pMemory, xbase::u32, char*, xbase::s32 )
{
    x_free( pMemory );
}

//==========================================================================

void* operator new [] ( xbase::xalloc_size Size, xbase::u32 Flags, char* pFileName, s32 LineNumber )
{
    ASSERT(0);
    return( x_Real_Malloc( (s32)Size, xbase::xmem_flags(Flags|XMEM_FLAG_BEEN_NEW), pFileName, LineNumber ) );
}

//==========================================================================

void operator delete [] ( void* pMemory, xbase::u32, char*, xbase::s32 )
{
    x_free( pMemory );
}

//==========================================================================
// END REDEFINITION OF NEW AND DELETE
//==========================================================================
#endif
*/
//DOM-IGNORE-END
