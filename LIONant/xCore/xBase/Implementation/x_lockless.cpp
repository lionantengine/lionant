//==============================================================================
// INCLUDES
//==============================================================================
#include "../x_base.h"


/////////////////////////////////////////////////////////////////////////////////
// x_qt_ptr
/////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// x_qt_fober_queue
/////////////////////////////////////////////////////////////////////////////////

x_qt_fober_queue::x_qt_fober_queue( void )
{
    mTail.SetPtr( (void*)&mDummyNode );
    mHead = mTail;
    mDummyNode.Zero();
}

//--------------------------------------------------------------------------------

void x_qt_fober_queue::push( x_qt_ptr* pNodeBegin, x_qt_ptr* pNodeEnd )
{
#ifdef TARGET_ANDROID
    while(!m_Lock.set(0, 1));
#endif
    
    // Lets make sure that our end node is pointing to NULL
    pNodeEnd->setNull();

    do
    {
        X_LWSYNC;
        x_qt_ptr Tail( mTail );
        x_qt_ptr Next( Tail.DereferencePtr() );

        X_LWSYNC;

        // if our local reality does not longer match with the
        // quatum version of the tail then we can't assume we are at the last node.
        // Someone must have inserted something new. Go lets regrab things again.
        if( Tail != mTail )
            continue;

        // If the next pointer is not NULL then we are not at the last node
        // move the tail foward.
        if( Next.isValid() )
        {
            // here, pNext is likely to be pTail->pNext
            X_LWSYNC;
            mTail.casCopyPtr( Tail, Next );
            continue;
        } 

        // If the node that the tail points to is the last node
        // then update the last node to point at the new node.
        X_LWSYNC;
        if( mTail.DereferencePtr().casPointTo( Next, pNodeBegin ) )
        {
            // If the tail points to what we thought was the last node
            // then update the tail to point to the new node.
            X_LWSYNC;
            mTail.casPointTo( Tail, pNodeEnd );
            break;
        }
        
    } while(true);
    
#ifdef TARGET_ANDROID
    m_Lock.setup(0);
#endif
}

//------------------------------------------------------------------------------

x_qt_ptr* x_qt_fober_queue::pop( void )
{
#ifdef TARGET_ANDROID
    while(!m_Lock.set(0, 1));
#endif
    
    do
    {
        X_LWSYNC;
        x_qt_ptr Head( mHead );
		x_qt_ptr Next( mHead.DereferencePtr() );

        X_LWSYNC;
        // Verify that we did not get the pointers in the middle
        // of another update.
        if( Head != mHead )
        {
            continue;
        }

        // Check if the queue is empty.
        if( Head == mTail )
        {
            if( Next.IsNull() )
            {
#ifdef TARGET_ANDROID
                m_Lock.setup(0);
#endif
                return NULL;
            }

            // Special case cause by the tail been equal to the head.
            // Help move the mTail foward because it seems to have falling behind.
            X_LWSYNC;
            mTail.casCopyPtr( Head, Next );
            X_LWSYNC;
        }
        else if( Next.isValid() )
        {
            // Move the head pointer, effectively removing the node
            X_LWSYNC;
            if( mHead.casCopyPtr( Head, Next ) )
            {
                // Well so if it turns out that we just got the dummy node.
                // We dont want this node so reinsert it again and get the next one
                X_LWSYNC;
                if( Head.GetPtr() == &mDummyNode )
                {
                    push( &mDummyNode );
                    return pop();
                }
                
                ASSERT( Head.GetPtr()  != &mDummyNode   );
                ASSERT( mTail.GetPtr() != Head.GetPtr() );
                
#ifdef TARGET_ANDROID
                m_Lock.setup(0);
#endif
                return (x_qt_ptr*)Head.GetPtr();
            }
        }
        
    } while(true);
}

//------------------------------------------------------------------------------

void x_qt_fober_queue::push( x_qt_ptr* pNode )
{
    push( pNode, pNode );
}

//-------------------------------------------------------------------------------

xbool x_qt_fober_queue::unsafeIsEmpty( void ) const
{
    do 
    {
        x_qt_ptr Tail( mTail );
        x_qt_ptr Head( mHead );
        x_qt_ptr Next( Head.DereferencePtr() );

        X_LWSYNC;
        // Verify that we did not get the pointers in the middle
        // of another update.
        if( Tail != mTail || Head != mHead )
        {
            continue;
        }

        // Check if the queue is empty.
        if( Head.GetPtr() == Tail.GetPtr() )
        {
            return Next.IsNull();
        }
        else
        {
            return FALSE;
        }
    } while ( 1 );    
}

//-------------------------------------------------------------------------------

void x_qt_fober_queue::clear( void )
{
    if (unsafeIsEmpty())
        return ;
}

//-------------------------------------------------------------------------------

#ifdef X_DEBUG
s32 x_qt_fober_queue::getCount( void )
{
    s32 Count = 0;

    x_qt_ptr* LoopPtr = &mHead.DereferencePtr();

    while (TRUE)
    {
        if (LoopPtr == mTail.GetPtr())
        {
            break;
        }

        Count++;
        LoopPtr = (x_qt_ptr*)LoopPtr->GetPtr();
    }

    return Count;
}
#endif
