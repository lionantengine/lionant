//DOM-IGNORE-BEGIN
#pragma once		// Include this file only once

#include <math.h>

//==============================================================================
// Types
//==============================================================================
union f32_forge
{
    u32     m_Bits;
    f32     m_F32;
    f32_forge( void )              {}
    f32_forge( u32 b ) : m_Bits(b) {}
    f32_forge( f32 f ) : m_F32 (f) {}
};

//==============================================================================
// Functions
//==============================================================================

// calculate the sin value of a number
#define x_Sin(f)        ((f32)sinf (f))
#define x_SinF64(f)     sinf(f)
#define x_Cos(f)        ((f32)cosf (f))
#define x_CosF64(f)     cosf(f)
#define x_Tan(f)        ((f32)tanf (f))
#define x_TanF64(f)     tanf(f)
#define x_ATan2(a,b)    ((f32)atan2f(a,b))
#define x_ATan(a)       ((f32)atanf(a))
#define x_Exp(f)		((f32)expf(f))
#define x_ExpF64(f)     expf(f)
#define x_Pow(a,b)		((f32)powf(a,b))
#define x_PowF64(a,b)	powf(a,b)
#define x_FMod(x,y)     ((f32)fmodf(x,y))
#define x_ModF(x,y)     ((f32)modff(x,y))
#define x_I2F(i)        ((f32)(i))
#define x_F2I(f)        ((s32)(f))
#define x_Log(A)        ((f32)logf( (f32)(A) ))
#define x_LogF64(A)      logf( A )
#define x_Log2(A)       (1.442695041f * (f32)logf( (f32)(A) ))
#define x_Log10(A)      ((f32)log10f( (f32)(A) ))

//------------------------------------------------------------------------------

#ifdef TARGET_360
    #define x_FSel(a,b,c)		__fsel( (a), (b), (c) )
#else
    inline f32 x_FSel( f32 a, f32 b, f32 c ) { return (((a) >= 0) ? (b) : (c)); } 
#endif

//------------------------------------------------------------------------------

template< class T > inline T        x_Abs     ( T a )                     { return ( (a) < (0) ? -(a) : (a) ); } 
template< class T > inline f32      x_Abs     ( f32 a )                   { return x_FSel(a,a,-a); } 
template< typename T1, typename T2 > inline auto x_Min( T1 a, T2 b ) -> decltype( a + b ) { return ( ( a ) < ( b ) ? ( a ) : ( b ) ); }
template< typename T1, typename T2 > inline auto x_Max( T1 a, T2 b ) -> decltype( a + b ) { return ( ( a ) > ( b ) ? ( a ) : ( b ) ); }
                    inline f32      x_Min     ( f32 a, f32 b )            { return x_FSel( ((a)-(b)), b, a); } 
                    inline f32      x_Max     ( f32 a, f32 b )            { return x_FSel( ((a)-(b)), a, b); } 
template< class T > inline xbool    x_Sign    ( T a )                     { return a >= 0 ? FALSE : TRUE; }
template< class T > inline T        x_Sqr     ( T a )                     { return a * a; }
template< class T > inline xbool    x_InRange ( T X, T Min, T Max )       { return (Min <= X) && (X <= Max);}
                    inline xbool    x_InRange ( f32 X, f32 Min, f32 Max ) { f32 a = x_FSel( X - Min, 1.0f, -10.0f ), 
                                                                                b = x_FSel( Max - X, 1.0f, -10.0f );  
                                                                                return x_F2I( x_FSel( a + b, 1, 0 ) ); }
template< class T > inline T        x_Range   ( T X, T Min, T Max )       { if( X < Min ) return Min; return(X > Max) ? Max : X; }
                    inline f32      x_Range   ( f32 X, f32 Min, f32 Max ) { f32 J = x_FSel( Min-X, Min, X ); return x_FSel( Max-J, J, Max ); }
template< class T > T               x_Lerp    ( f32 t, T a, T b )         { return (T)(a + t*(b-a)); }

//------------------------------------------------------------------------------
inline 
xbool x_Sign( f32 a )
{
    f32_forge fi(a);

    // Check the sign bit (bit 31).
    return (fi.m_Bits & ((u32)1 << 31)) ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
#if defined TARGET_PC || defined(TARGET_IOS) || defined(TARGET_MARMALADE) || defined(TARGET_ANDROID) || defined(TARGET_OSX) || defined(TARGET_3DS)
    #define x_Sqrt(f)		sqrtf(f)
    #define x_SqrtF64(f)    sqrt(f)
#elif TARGET_360
    #define x_Sqrt(f)		((f32)__fsqrts(f))
    #define x_SqrtF64(f)     sqrt(f)
#else
    #error x_Sqrt
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
void x_SinCos( xradian Angle, f32& S, f32& C )
{
    __asm 
    {         
        fld        Angle;
        fsincos;
        mov        eax,[C]    
        mov        edx,[S]    
        fstp       dword ptr[eax];
        fstp       dword ptr[edx];
    }
}
#elif defined(TARGET_360)
inline 
void x_SinCos( xradian Angle, f32& S, f32& C )
{
	XMScalarSinCos(&S, &C, Angle);  
    // XMScalarSinCosEst(&s, &c, angle); 
}
#else
inline 
void x_SinCos( xradian Angle, f32& S, f32& C )
{
    S = x_Sin( Angle );
    C = x_Cos( Angle );
}
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
f32 x_InvSqrt( f32 x )
{ 
    ASSERT( v > 0.0f );
    _asm
    {
            movss   xmm0, x
            rsqrtss xmm0, xmm0
            movss   x,    xmm0
    }

    return x;
}
#else
inline 
f32 x_InvSqrt( f32 x )
{
    // Fast invert sqrt technique similar to Jim Blinn's.
    ASSERT( x > 0.0f );

    f32 x2 = x * 0.5f;
    f32 y = x;
    s32 i = *(s32*)&y;
    i = 0x5f3759df - ( i >> 1 );
    y = *(f32*)&i;

    y = y * ( 1.5f - ( x2 * y * y ) );
    y = y * ( 1.5f - ( x2 * y * y ) );

    return y;
}
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
f32 x_ACos( f32 x )
{
    _asm
    {
            fld         x               
            fld1             
            fadd        st,st(1) 
            fld1             
            fsub        st,st(2) 
            fmulp       st(1),st 
            fsqrt            
            fxch        st(1) 
            fpatan           
            fstp        x
    }

    return x;
}
#else
#define x_ACos(f)       ((f32)acosf (f))
#endif

//------------------------------------------------------------------------------
#ifdef TARGET_PC_32BIT
inline 
f32 x_ASin( f32 x )
{
    _asm
    {
            fld         x               
            fld1             
            fadd        st,st(1)  
            fld1             
            fsub        st,st(2)  
            fmulp       st(1),st  
            fsqrt            
            fpatan        
            fstp        x
    }

    return x;
}
#else
#define x_ASin(f)       ((f32)asinf(f))
#endif

//------------------------------------------------------------------------------
inline
xradian x_ModAngle( xradian a )
{
    if( (a > X_RADIAN( 1440)) || 
        (a < X_RADIAN(-1440)) )
    {
        a = x_FMod( a, X_RADIAN(360) );
    }

    while( a >= (X_RADIAN(360)) )  a -= X_RADIAN(360);
    while( a <   X_RADIAN(0)    )  a += X_RADIAN(360);

    ASSERT( (a >= (X_RADIAN(0)-0.01f)) && (a < (X_RADIAN(360)+0.01f)) );
    return( a );
}

//------------------------------------------------------------------------------
inline
xradian x_ModAngle2( xradian a )
{
    a += X_RADIAN(180);
    a  = x_ModAngle( a );
    a -= X_RADIAN(180);

    return a;
}

//------------------------------------------------------------------------------
inline
xradian x_LerpAngle( f32 t, xradian Angle1, xradian Angle2 )
{
    return x_ModAngle(Angle1 + x_ModAngle(Angle2-Angle1)*t);
}

//------------------------------------------------------------------------------
inline
xradian x_MinAngleDiff( xradian a, xradian b )
{
    return x_ModAngle2( a - b );
}

//------------------------------------------------------------------------------
inline
f32 x_LPR( f32 a, f32 b )
{
    ASSERT( b > 0.0f );

    a = x_FMod( a, b );
    if( a < 0.0f )  
        a += b;
    return a;
}

//------------------------------------------------------------------------------
inline
xbool x_isValid( f32 a )
{
    return( ( (*((u32*)(&a))) & 0x7F800000 ) != 0x7F800000 );
}

//------------------------------------------------------------------------------
inline 
f32 x_Floor( f32 a )
{
    f32_forge f;
    s32       e;
    u32       s;

    // Watch out for trouble!
    ASSERT( x_isValid(a) );

    // Watch out for 0!
    if( a == 0.0f )  return( 0.0f );

    // We need the exponent...
    f.m_F32 = a;
    e     = (s32)((f.m_Bits & 0x7F800000) >> 23) - 127;

    // Extremely large numbers have no precision bits available for fractional
    // data, so there is no work to be done.
    if( e > 23 )  return( a );

    // We need the sign bit.
    s = f.m_Bits & 0x80000000;

    // Numbers between 1 and -1 must be handled special.
    if( e < 0 )  return( s ? -1.0f : 0.0f );

    // Mask out all mantissa bits used in storing fractional data.
    f.m_Bits &= (-1 << (23-e));

    // If value is negative, we have to be careful.
    // And otherwise, we're done!
    if( s && (a < f.m_F32) ) return f.m_F32 - 1.0f;

    return f.m_F32 ;
}

//------------------------------------------------------------------------------
inline 
f32 x_Ceil( f32 a )
{
    f32_forge f;
    s32       e;
    u32       s;

    // Watch out for trouble!
    ASSERT( x_isValid(a) );

    // Watch out for 0!
    if( a == 0.0f )  return( 0.0f );

    // We need the exponent...
    f.m_F32 = a;
    e       = (s32)((f.m_Bits & 0x7F800000) >> 23) - 127;

    // Extremely large numbers have no precision bits available for fractional
    // data, so there is no work to be done.
    if( e > 23 )  return( a );

    // We need the sign bit.
    s = f.m_Bits & 0x80000000;

    // Numbers between 1 and -1 must be handled special.
    if( e < 0 )  return( s ? 0.0f : 1.0f );

    // Mask out all mantissa bits used in storing fractional data.
    f.m_Bits &= (-1 << (23-e));

    // If value is positive, we have to be careful.
    // And otherwise, we're done!
    if( !s && (a > f.m_F32) ) return f.m_F32 + 1.0f;
    return f.m_F32;
}

//------------------------------------------------------------------------------

inline
s32 x_LRound( f32 x )
{
#ifdef SSE2_SUPPORT
    return s32(_mm_cvtss_si32(_mm_set_ss(x)));
#else
    return s32(x + 0.5f);
#endif
}

//------------------------------------------------------------------------------
inline 
f32 x_Round( f32 a, f32 b )
{
    f32 Quotient;

    ASSERT( !x_InRange( b, -0.00001f, 0.00001f ) );

    Quotient = a / b;

    if( Quotient < 0.0f ) return x_Ceil ( Quotient - 0.5f ) * b;

    return x_Floor( Quotient + 0.5f ) * b;
}

//------------------------------------------------------------------------------
template< class T > inline
s32 x_GetPowerOfTwo( T a )
{
    s32 Index;
    for( Index = -1;  a > 0 ;  )
    {
        a >>= 1;
        Index++;
    }
    return Index;
}

//------------------------------------------------------------------------------
inline
xbool x_SolvedQuadraticRoots( f32& Root1, f32& Root2, f32 a, f32 b, f32 c )
{
    if( a == 0.0f ) 
    {
        // It's not a quadratic, only one root: bx + c = 0
        if(b != 0.0f) 
        {
            Root1 = -c / b;
            Root2 = Root1;
            return TRUE;
        } 
        else 
        {
            // There are no roots!
            return FALSE;
        }
    }

    if( b == 0.0f ) 
    {
        // We won't be able to handle this case after rearranging the formula.
        // ax^2 + c = 0
        // x = +-sqrt(-c / a)
        f32 const neg_c_over_a = -c / a;

        if( neg_c_over_a >= 0.0f ) 
        {
            Root1 = x_Sqrt( neg_c_over_a );
            Root2 = -Root1;
            return TRUE;

        } 
        else 
        {
            // No real roots.
            return FALSE;
        }
    }

    // If b^2 >> 4ac, then "loss of significance" can occur because two numbers that are
    // almost the same are subtracted from each other.  If b^2 >> 4ac, then x is roughly equal
    // to -b +- |b| / 2a.  If b > 0 then "loss of significance" can happen to the root -b + |b| / 2a,
    // if b < 0 then it happens to the root -b - |b| / 2a.  We want to choose the better root and
    // derive the other root from it.  If you factor out a from ax^2 + bx + c = 0, then a(x^2 + xb/a + c/a) = 0
    // and a(x - r1)(x - r2) = 0 so r1 * r2 = c/a!  If we have one root, we can figure the other one out!!
    // We want to avoid an if statement on the sign of b, so factor it out:
    //		r1,r2 = (b / 2a)(-1 +- sqrt(1 - 4ac / b^2))
    //		A = b / 2a
    //		B = 4ac / b^2 (3 multiplies, 1 divide...can we do better?? YES!)
    //		  = 2c / b * 2a / b = 2c / b * 1 / A = 2c / 2aA * 1 / A 
    //		  = c / aA^2 (2 multiplies, 1 divide)
    //		C = -1 - sqrt(1 - B)
    // The good root is then:
    //		r1 = AC
    //	And the other root is:
    //		r1r2 = c / a
    //		r2 = c / ar1 = c / aAC = (A / A)(c / aAC) = Ac / aA^2C 
    //			= AB / C

    // A: part_1
    f32 denom = 2.0f * a;
    if(denom == 0.0f) 
    {
        return FALSE;
    }

    f32 const part_1 = b / denom;

    // B: part_2
    denom = a * part_1 * part_1;
    if(denom == 0.0f) 
    {
        return FALSE;
    }

    f32 const part_2 = c / denom;

    // C: part_3
    f32 const one_minus_part_2 = 1.0f - part_2;
    if( one_minus_part_2 < 0.0f ) 
    {
        return FALSE;
    }

    f32 const part_3 = -1.0f - x_Sqrt(one_minus_part_2);
    ASSERT( part_3 < 0.0f );

    // The good root.
    Root1 = part_1 * part_3;

    // The other root.
    Root2 = (part_1 * part_2) / part_3;

    return TRUE;
}

//------------------------------------------------------------------------------
inline 
xbool x_FEqual( f32 f0, f32 f1, f32 tol) 
{
    f32 f = f0-f1;
    return ((f>(-tol)) && (f<tol));
}

//------------------------------------------------------------------------------
inline 
xbool x_FLess( f32 f0, f32 f1, f32 tol) 
{
    return ((f0-f1)<tol);
}

//------------------------------------------------------------------------------
inline 
xbool x_FGreater( f32 f0, f32 f1, f32 tol) 
{
    return ((f0-f1)>tol);
}

//==============================================================================
// SOME PRIVATE FUNCTIONS
//==============================================================================
void x_MathInit( void );
void x_MathKill( void );

