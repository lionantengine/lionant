#ifndef X_LOCKLESS_INLINE_H
#define X_LOCKLESS_INLINE_H

/////////////////////////////////////////////////////////////////////////////////
// x_qt_ptr
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

#ifdef X_TARGET_64BIT

//-------------------------------------------------------------------------------

inline
x_qt_ptr::x_qt_ptr( void )
{
    ASSERT( x_Align( this, 8 ) == this );
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr::x_qt_ptr( void* pPointer )
{
    u16 Count = m_Pointer.m_Tag + 1;
    m_Pointer.m_Data = (u64)pPointer;
    m_Pointer.m_Tag  = Count;
    ASSERT( GetPtr() == pPointer );
}

//-------------------------------------------------------------------------------

inline
void x_qt_ptr::SetPtr( void* pPointer )
{
    u16 Count = m_Pointer.m_Tag + 1;
    m_Pointer.m_Data = (u64)pPointer;
    m_Pointer.m_Tag  = Count;
    ASSERT( GetPtr() == pPointer );
}

//-------------------------------------------------------------------------------
inline
void x_qt_ptr::Zero( void )
{
    m_Pointer.m_Data = 0;
}

//-------------------------------------------------------------------------------
inline
void x_qt_ptr::setNull( void )
{
    m_Pointer.m_Data &= ~PTR_MASK;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::IsNull( void ) const
{
    return 0 == (m_Pointer.m_Data&PTR_MASK);
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::isValid( void ) const
{
    return 0!=(m_Pointer.m_Data&PTR_MASK);
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr& x_qt_ptr::operator = ( const x_qt_ptr& Ptr )
{
    *(u64*)this = *(const u64*)&Ptr;
    return *this;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::operator == ( const x_qt_ptr& Ptr ) const
{
    return m_Pointer.m_Data == Ptr.m_Pointer.m_Data;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::operator != ( const x_qt_ptr& Ptr ) const
{
    return m_Pointer.m_Data != Ptr.m_Pointer.m_Data;
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr& x_qt_ptr::DereferencePtr( void ) const
{
    volatile x_qt_ptr* volatile pPtr = (volatile x_qt_ptr* )(m_Pointer.m_Data&PTR_MASK);
    ASSERT(pPtr);
    return (x_qt_ptr&)*pPtr;
}

//-------------------------------------------------------------------------------
inline
void* x_qt_ptr::GetPtr( void ) const
{
    return(void*)(m_Pointer.m_Data&PTR_MASK);
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::casPointTo( const x_qt_ptr& LocalPointer, const x_qt_ptr* const pNewPtr )
{
    x_qt_ptr New;
    
    ASSERT( x_Align( this, 8 ) == this );
    ASSERT( x_Align( &New, 8 ) == &New );
    ASSERT( x_Align( &LocalPointer, 8 ) == &LocalPointer );
    
    New.m_Pointer.m_Data = ((u64)pNewPtr );
    New.m_Pointer.m_Tag  = LocalPointer.m_Pointer.m_Tag+1;
	ASSERT(New.GetPtr() == pNewPtr);

    return x_cas64( *this,
                    LocalPointer,
                    New );
}

//--------------------------------------------------------------------------------
inline
xbool x_qt_ptr::casCopyPtr( const x_qt_ptr& LocalPointer, const x_qt_ptr& NextPointer )
{
    x_qt_ptr New;
    
    New.m_Pointer.m_Data = NextPointer.m_Pointer.m_Data;
    New.m_Pointer.m_Tag  = LocalPointer.m_Pointer.m_Tag+1;
	ASSERT( New.GetPtr() == NextPointer.GetPtr() );

    return x_cas64( *this,
                   LocalPointer,
                   New );
}


//--------------------------------------------------------------------------------
inline
void x_qt_ptr::LinkListSetNext( x_qt_ptr& Node )
{
    const u32 Tag = Node.m_Pointer.m_Tag + 1;
    
    do
    {
        const x_qt_ptr Local( *this );
        x_qt_ptr New;
        
        New.m_Pointer.m_Data = PTR_MASK & (xuptr)&Node;
        New.m_Pointer.m_Tag  = Local.m_Pointer.m_Tag+1;

        Node.m_Pointer.m_Data = PTR_MASK & Local.m_Pointer.m_Data;
        Node.m_Pointer.m_Tag  = Tag;
		ASSERT(Node.GetPtr() == Local.GetPtr());

        if( x_cas64( *this, Local, New ) ) break;
        
    } while (1);
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
#else

//-------------------------------------------------------------------------------

inline 
x_qt_ptr::x_qt_ptr( void ) 
{
    ASSERT( x_Align( this, 8 ) == this );
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr::x_qt_ptr( void* pPointer )
{
    // Write 64bits in one shot is safer
    m_Pointer.m_Data = (u64(m_Pointer.m_Tag+1)<<32)|u64(pPointer);
}

//-------------------------------------------------------------------------------

inline
void x_qt_ptr::SetPtr( void* pPointer )
{
    // Write 64bits in one shot is safer
    m_Pointer.m_Data = (u64(m_Pointer.m_Tag+1)<<32)|u64(pPointer);
}

//-------------------------------------------------------------------------------
inline
void x_qt_ptr::Zero( void )
{
    m_Pointer.m_Data = 0;
}

//-------------------------------------------------------------------------------
inline
void x_qt_ptr::setNull( void )
{
    m_Pointer.m_pPtr = NULL;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::IsNull( void ) const
{
    return NULL == m_Pointer.m_pPtr;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::isValid( void ) const
{
    X_LWSYNC;
    return m_Pointer.m_pPtr != NULL;
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr& x_qt_ptr::operator = ( const x_qt_ptr& Ptr )
{
    *(u64*)this = *(const u64*)&Ptr;
    return *this;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::operator == ( const x_qt_ptr& Ptr ) const
{
    X_LWSYNC;
    return m_Pointer.m_Data == Ptr.m_Pointer.m_Data;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::operator != ( const x_qt_ptr& Ptr ) const
{
    X_LWSYNC;
    return m_Pointer.m_Data != Ptr.m_Pointer.m_Data;
}

//-------------------------------------------------------------------------------
inline
x_qt_ptr& x_qt_ptr::DereferencePtr( void ) const
{
    ASSERT(m_Pointer.m_pPtr);
    return *(x_qt_ptr*)m_Pointer.m_pPtr;
}

//-------------------------------------------------------------------------------
inline
void* x_qt_ptr::GetPtr( void ) const
{
    return(void*)(m_Pointer.m_pPtr);
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_ptr::casPointTo( const x_qt_ptr& LocalPointer, const x_qt_ptr* const pNewPtr )
{
    x_qt_ptr New;
    
    New.m_Pointer.m_pPtr = (x_qt_ptr*)pNewPtr;
    New.m_Pointer.m_Tag  = LocalPointer.m_Pointer.m_Tag+1;
    
    return x_cas64( *this,
                   LocalPointer,
                   New );
}

//--------------------------------------------------------------------------------
inline
xbool x_qt_ptr::casCopyPtr( const x_qt_ptr& LocalPointer, const x_qt_ptr& NextPointer )
{
    x_qt_ptr New;
    
    New.m_Pointer.m_pPtr = NextPointer.m_Pointer.m_pPtr;
    New.m_Pointer.m_Tag  = LocalPointer.m_Pointer.m_Tag+1;
    
    return x_cas64( *this,
                   LocalPointer,
                   New );
}

//--------------------------------------------------------------------------------
inline
void x_qt_ptr::LinkListSetNext( x_qt_ptr& Node )
{
    Node.m_Pointer.m_Tag++;
    
    do
    {
        x_qt_ptr Local( *this );
        x_qt_ptr New;
        
        New.m_Pointer.m_pPtr = &Node;
        New.m_Pointer.m_Tag  = Local.m_Pointer.m_Tag+1;
        
        Node.m_Pointer.m_Data = Local.m_Pointer.m_Data;
        
        if( x_cas64( *this, Local, New ) ) break;
        
    } while (1);
}


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
#endif

/////////////////////////////////////////////////////////////////////////////////
// x_qt_counter
/////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------
inline
x_qt_counter::x_qt_counter( void )
: mCounter( 0 )
{
    
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Inc( void )
{
    return x_casInc32( &mCounter );
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Dec( void )
{
    return x_casDec32( &mCounter );
}

//--------------------------------------------------------------------------------
inline
xbool x_qt_counter::set( s32 localreality, s32 NewCounter )
{
    return x_cas32( (u32*)&mCounter, localreality, NewCounter );
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::get( void ) const
{
    return mCounter;
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Add( s32 Count )
{
    s32 LocalCount;
    s32 NewReality;
    do
    {
        LocalCount = mCounter;
        NewReality = LocalCount + Count;

    } while( !set( LocalCount, NewReality ) );
            
    return NewReality;
}

//--------------------------------------------------------------------------------
inline
void x_qt_counter::Zero( void )
{
    do
    {
        s32 LocalCount = mCounter;
        if( LocalCount == 0 )
            break;

        if( set( LocalCount, 0 ) )
            break;

    } while( 1 );
}
//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Sub( s32 Count )
{
    s32 LocalCount;
    s32 NewReality;
    do
    {
        LocalCount = mCounter;
        NewReality = LocalCount - Count;

    } while( !set( LocalCount, NewReality ) );
            
    return NewReality;
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Max( s32 Count )
{
    s32 LocalCount;
    s32 NewReality;

    do
    {
        LocalCount = mCounter;
        NewReality = x_Max( LocalCount, Count );
    
    } while( !set( LocalCount, NewReality ) );
            
    return NewReality;
}

//--------------------------------------------------------------------------------
inline
s32 x_qt_counter::Min( s32 Count )
{
    s32 LocalCount;
    s32 NewReality;

    do
    {
        LocalCount = mCounter;
        NewReality = x_Min( LocalCount, Count );
    
    } while( !set( LocalCount, NewReality ) );
            
    return NewReality;
}

/////////////////////////////////////////////////////////////////////////////////
// x_qt_flags
/////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------
inline
void x_qt_flags::setup( u32 Flags )
{
    m_qtFlags = Flags|(m_qtFlags&0xffffffff00000000ULL);
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlags( u32 Flags, x_qt_flags& LocalReality )
{
    u64 Reality     = LocalReality.m_qtFlags;
    u64 Counter     = ((u64)Reality + 0x100000000ULL)&0xffffffff00000000ULL;
    u64 NewFlags    = Flags;
    
    NewFlags        = Counter|NewFlags;
    
    return x_cas64( &m_qtFlags, Reality, NewFlags );
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOn( u32 Flags, x_qt_flags& LocalReality )
{
    u64 Reality     = LocalReality.m_qtFlags;
    u64 Counter     = ((u64)Reality + 0x100000000ULL)&0xffffffff00000000ULL;
    u64 NewFlags    = (Reality | Flags)&0xffffffff;
    
    NewFlags        = Counter|NewFlags;
    
    return x_cas64( &m_qtFlags, Reality, NewFlags );
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOff( u32 Flags, x_qt_flags& LocalReality )
{
    Flags = ~Flags;
    u64 Reality     = LocalReality.m_qtFlags;
    u64 Counter     = ((u64)Reality + 0x100000000ULL)&0xffffffff00000000ULL;
    u64 NewFlags    = Reality & u64(Flags);
    
    NewFlags        = Counter|NewFlags;
    
    return x_cas64( &m_qtFlags, Reality, NewFlags );
}

//-------------------------------------------------------------------------------
inline
u32 x_qt_flags::getFlags( void ) const
{
    return u32(m_qtFlags);
}

//-------------------------------------------------------------------------------
inline
void x_qt_flags::setFlags( u32 Flags )
{
    m_qtFlags = (m_qtFlags&0xffffffff00000000ULL)|Flags;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOn( u32 Flags )
{
    return qtSetFlagsOn( Flags, *this );
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOff( u32 Flags )
{
    return qtSetFlagsOff( Flags, *this );
}

//-------------------------------------------------------------------------------
inline
void x_qt_flags::qtForceSetFlagsOn( u32 Flags )
{
    while( !qtSetFlagsOn( Flags ) )
    {
    }
}

//-------------------------------------------------------------------------------
inline
void x_qt_flags::qtForceSetFlagsOff( u32 Flags )
{
    while( !qtSetFlagsOff( Flags ) )
    {
    }
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::areFlagOn( u32 Flags ) const
{
    u32 RealFlags = u32(m_qtFlags);
    return (RealFlags&Flags) == Flags;
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::isFlagOn( u32 Flag ) const
{
    u32 RealFlags = u32(m_qtFlags);
    return !!(RealFlags&Flag);
}

/////////////////////////////////////////////////////////////////////////////////
// x_qt_fixed_pool
/////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------
template< class T > inline
void x_qt_fixed_pool<T>::Init( s32 Count )
{
#ifdef X_TARGET_64BIT
    const xmem_flags MemFlags = XMEM_FLAG_ALIGN_8B;
#else
    const xmem_flags MemFlags = XMEM_FLAG_ALIGN_4B;
#endif
    
    m_Mem.New( Count + 1, MemFlags );
    
    //Link all the free nodes
    m_Head.SetPtr( (T*)m_Mem );
    
    m_Counter.setup( 0 );

    // Link all the nodes
    Clear();
}

//--------------------------------------------------------------------------------
template< class T > inline
void x_qt_fixed_pool<T>::Kill( void )
{
    m_Mem.Destroy();
}

//--------------------------------------------------------------------------------
template< class T > inline
void x_qt_fixed_pool<T>::Clear( void )
{
    s32 i;
    s32 Count = m_Mem.getCount() - 1;
    
    for( i=0; i<Count; i++ )
    {
        m_Mem[i].SetPtr( &m_Mem[i+1] );
    }
    
    m_Mem[i].setNull();
}

//--------------------------------------------------------------------------------
template< class T > inline
x_qt_ptr* x_qt_fixed_pool<T>::Alloc( void )
{
    do
    {
        x_qt_ptr   Head( m_Head );
        
        if( Head.IsNull() )
            return NULL;
        
        x_qt_ptr   Next( Head.DereferencePtr() );
        
        if( Next.IsNull() )
            return NULL;
        
        if( m_Head.casCopyPtr( Head, Next ) )
        {
            m_Counter.Inc();
            return (x_qt_ptr*)Head.GetPtr();
        }
        
    } while(1);
    
    return NULL;
}

//--------------------------------------------------------------------------------
template< class T > inline
void x_qt_fixed_pool<T>::Free( x_qt_ptr* pNode )
{
    ASSERT( pNode );
    
    do
    {
        x_qt_ptr  Head( m_Head );
        pNode->SetPtr( Head.GetPtr() );
        
        if( m_Head.casPointTo( Head, pNode ) )
        {
            m_Counter.Dec();
            break;
        }
        
    } while(1);
}


/////////////////////////////////////////////////////////////////////////////////
// x_qt_fixed_pool_static_mem
/////////////////////////////////////////////////////////////////////////////////


//--------------------------------------------------------------------------------
template< class T, s32 C > inline
void x_qt_fixed_pool_static_mem<T,C>::Clear( void )
{
    s32 i;
    
    //Link all the free nodes
    m_Head.SetPtr( (T*)m_Mem );
    
    // minimum requirements for the pointer
    ASSERTCT( sizeof( T ) >= xalingof( T ) );
    ASSERTCT( sizeof( T )%xalingof( T ) == 0 );
    ASSERTCT( xalingof(T) == 16 );
    ASSERTCT( sizeof(T) >=  8 );

    for( i=0; i<C-1; i++ )
    {
        //m_Mem[i].SetPtr( &m_Mem[i+1] );
        ((x_qt_ptr*)&m_Mem[ i ])->SetPtr( &m_Mem[ i + 1 ] );
    }
    
    ( (x_qt_ptr*)&m_Mem[ i ] )->setNull();

    m_Counter.setup( 0 );
}

//--------------------------------------------------------------------------------
template< class T, s32 C > inline
x_qt_ptr* x_qt_fixed_pool_static_mem<T,C>::AllocByPtr( void )
{
    do
    {
        x_qt_ptr   Head( m_Head );
        
        if( Head.IsNull() )
            return NULL;
        
        x_qt_ptr   Next( Head.DereferencePtr() );
        
   //    if( Next.IsNull() )
   //        return NULL;
        
        if( m_Head.casCopyPtr( Head, Next ) )
        {
            m_Counter.Inc();

            auto* pNode = (x_qt_ptr*)Head.GetPtr();
           // ASSERT( u64( pNode ) % sizeof( T ) == 0 );
            return pNode;
        }
        
    } while(1);
    
    return NULL;
}

//--------------------------------------------------------------------------------
template< class T, s32 C > inline
void x_qt_fixed_pool_static_mem<T,C>::FreeByPtr( x_qt_ptr* pNode )
{
    ASSERT( pNode );
    ASSERT( pNode >= (x_qt_ptr*)&m_Mem[0] );
    ASSERT( pNode <= (x_qt_ptr*)&m_Mem[ C-1 ] );
    ASSERT( u64(pNode)%xalingof(T) == 0 );

    do
    {
        x_qt_ptr  Head( m_Head );
        pNode->SetPtr( Head.GetPtr() );
        
        if( m_Head.casPointTo( Head, pNode ) )
        {
            m_Counter.Dec();
            break;
        }
        
    } while(1);
}

//--------------------------------------------------------------------------------
template< class T, s32 C > inline
void x_qt_fixed_pool_static_mem<T,C>::FreeByHandle( xhandle Handle )
{
    ASSERT( Handle.isValid() );
    ASSERT( Handle.m_Handle < C );
    FreeByPtr( &m_Mem[Handle.m_Handle] );
}

//--------------------------------------------------------------------------------
template< class T, s32 C > inline
xhandle x_qt_fixed_pool_static_mem<T,C>::AllocByHandle( void )
{
    xhandle Handle;
    x_qt_ptr* pPtr = AllocByPtr();
    if( pPtr == NULL )
    {
        Handle.setNull();
    }
    else
    {
        Handle.Set( u32( ((T*)pPtr) - &m_Mem[0]) );
    }
    
    return Handle;
}

//--------------------------------------------------------------------------------
template< class T, s32 C> inline
T& x_qt_fixed_pool_static_mem<T,C>::getEntry( xhandle Handle )
{
    ASSERT( Handle.isValid() );
    ASSERT( Handle.m_Handle < C );
    return m_Mem[Handle.m_Handle];
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// POPLESS QUEUE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
inline
void x_qt_popless_queue::Clear( void )
{
    m_Head.Zero();
    m_Tail = m_Head;
}

//-------------------------------------------------------------------------------
inline
void x_qt_popless_queue::Push( x_qt_ptr& Node )
{
    Node.setNull();

    //
    // Handle case where the HEAD and TAIL is equal to NULL
    //
    while( m_Head.IsNull() )
    {
        x_qt_ptr Tail( m_Tail );
        X_LWSYNC;

        if( Tail.isValid() )
            break;

        // If the node that the tail points to is the last node
        // then update the last node to point at the new node.
        X_LWSYNC;
        if( m_Tail.casPointTo( Tail, &Node ) )
        {
            // If we got here it means we own the Tail reality.
            X_LWSYNC;
            m_Head.SetPtr( &Node );
            return;
        }
    }

    //
    // Handle all other cases
    //
    do
    {
        X_LWSYNC;
        x_qt_ptr Tail( m_Tail );
        x_qt_ptr Next( Tail.DereferencePtr() );

        X_LWSYNC;

        // this "if" is necessary, because if the tailnode and tail are 
        // out-of-sync, guard is going to be wrong...
        // if tail has moved, start over.
        if( Tail != m_Tail )
            continue;

        if( Next.isValid() )
        {
            // here, pNext is likely to be pTail->pNext
            X_LWSYNC;
            m_Tail.casCopyPtr( Tail, Next );
            continue;
        } 

        // If the node that the tail points to is the last node
        // then update the last node to point at the new node.
        X_LWSYNC;
        if( m_Tail.DereferencePtr().casPointTo( Next, &Node ) )
        {
            // If the tail points to what we thought was the last node
            // then update the tail to point to the new node.
            X_LWSYNC;
            m_Tail.casPointTo( Tail, &Node );
            break;
        }
    } while(true);
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// QT HASH
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::Initialize( s32 MaxEntries )
{
    m_EntryMemPool.Init( MaxEntries );
    m_qtHashTable.New( MaxEntries * 10 );
    for( auto& TableEntry : m_qtHashTable )
        TableEntry.m_LinkList.Clear();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
KEY x_qt_hash<ENTRY,KEY>::getKeyFromEntry( const entry& Entry ) const
{
    return GetHashEntryFromEntry(Entry).m_Key;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
xbool x_qt_hash<ENTRY,KEY>::isEntryInHash( key Key ) const
{
    s32         Index       = KeyToIndex( Key );
    const auto& TableEntry  = m_qtHashTable[ Index ];

    TableEntry.m_Semaphore.BeginAtomic();
    xbool bAnswer = (NULL != FindEntry( TableEntry, Key ));
    TableEntry.m_Semaphore.EndAtomic();

    return bAnswer;
}

//-------------------------------------------------------------------------------
// Deleting locks one table entry in the hash table
// other wise this hold hash table is lockfree 
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::DeleteEntry( key Key )
{                                                                          
    hash_entry* pEntry;

    //
    // First lets remove the entry from the hash table
    // 
    {
        const s32               Index           = KeyToIndex( Key );
        hash_table_entry&       HashTaleEntry   = m_qtHashTable[ Index ];
        hash_entry*             pPrevEntry      = NULL;

        HashTaleEntry.m_Semaphore.BeginAtomic();
        pEntry      = (hash_entry*)HashTaleEntry.m_LinkList.m_Head.GetPtr();
        while( pEntry )
        {
            if( pEntry->m_Key == Key )
                break;

            pPrevEntry  = pEntry;
            pEntry      = (hash_entry*)pEntry->GetPtr();
        }

        // Could not find the entry
        ASSERT( pEntry );
        
        if( pPrevEntry ) 
        {
            pPrevEntry->SetPtr( pEntry->GetPtr() );
        }
        else
        {
            HashTaleEntry.m_LinkList.m_Head.SetPtr( pEntry->GetPtr() );
        }

        if( HashTaleEntry.m_LinkList.m_Tail.GetPtr() == pEntry )
        {
            // help move the tail up
            if( pEntry->GetPtr() ) HashTaleEntry.m_LinkList.m_Tail.SetPtr( pEntry->GetPtr() );
            else                   HashTaleEntry.m_LinkList.m_Tail.SetPtr( pPrevEntry ); 
        }
        
        HashTaleEntry.m_Semaphore.EndAtomic(); 
    }

    //
    // Set this entry as null
    //
    pEntry->setNull();
    x_Destruct( pEntry );
    m_EntryMemPool.Free( pEntry );
}
 
//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
typename x_qt_hash<ENTRY,KEY>::hash_entry& x_qt_hash<ENTRY,KEY>::AddHashEntry( hash_table_entry& TableEntry, key Key )
{
    ASSERT( NULL == FindEntry( TableEntry, Key ) );

    hash_entry* pHashEntry = (hash_entry*)m_EntryMemPool.Alloc();
    ASSERT( pHashEntry );

    // make sure that all the constructors have been called
    x_Construct( pHashEntry );

    pHashEntry->m_Key = Key;

    TableEntry.m_LinkList.Push( *pHashEntry );

    return *pHashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
typename x_qt_hash<ENTRY,KEY>::hash_entry* x_qt_hash<ENTRY,KEY>::FindEntry( const hash_table_entry& TableEntry, key Key ) const
{
    hash_entry* pEntry = (hash_entry*)TableEntry.m_LinkList.m_Head.GetPtr();
    while( pEntry )
    {
        if( pEntry->m_Key == Key )
            return pEntry;

        pEntry = (hash_entry*)pEntry->GetPtr();
    }

    return pEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
s32 x_qt_hash<ENTRY,KEY>::KeyToIndex( key Key ) const
{
    return s32( Key % m_qtHashTable.getCount() );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
typename x_qt_hash<ENTRY,KEY>::hash_entry& x_qt_hash<ENTRY,KEY>::FindOrAddHashEntry( hash_table_entry& TableEntry, key Key )
{
    hash_entry* pHashEntry = FindEntry( TableEntry, Key );
    if( pHashEntry ) return *pHashEntry;
    return AddHashEntry( TableEntry, Key );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
ENTRY& x_qt_hash<ENTRY,KEY>::FindOrAddPopEntry( key Key )
{
    s32          Index              = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.BeginAtomic();
    hash_entry&  HashEntry   = FindOrAddHashEntry( MutableTableEntry, Key );

    HashEntry.m_Semaphore.BeginAtomic();
    return HashEntry.m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
ENTRY& x_qt_hash<ENTRY,KEY>::AddPopEntry( key Key )
{
    s32          Index              = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.BeginAtomic();
    hash_entry&  HashEntry          = AddHashEntry( MutableTableEntry, Key );

    HashEntry.m_Semaphore.BeginAtomic();
    return HashEntry.m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
ENTRY* x_qt_hash<ENTRY,KEY>::getPopEntry( key Key )
{
    s32          Index              = KeyToIndex( Key );
    auto&        MutableTableEntry  = m_qtHashTable[ Index ];
    const auto&  ConstTableEntry    = MutableTableEntry;

    ConstTableEntry.m_Semaphore.BeginAtomic();
    hash_entry* pHashEntry = FindEntry( MutableTableEntry, Key );
    if( pHashEntry == NULL ) return NULL;

    pHashEntry->m_Semaphore.BeginAtomic();
    return &pHashEntry->m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
const ENTRY* x_qt_hash<ENTRY,KEY>::getPopEntry( key Key ) const
{
    s32          Index              = KeyToIndex( Key );
    const auto&  ConstTableEntry    = m_qtHashTable[ Index ];

    ConstTableEntry.m_Semaphore.BeginAtomic();
    const hash_entry* pHashEntry = FindEntry( ConstTableEntry, Key );
    if( pHashEntry == NULL ) return NULL;

    pHashEntry->m_Semaphore.BeginAtomic();
    return &pHashEntry->m_UserData; 
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::PushEntry( entry& Entry )
{
    hash_entry&     HashEntry        = getHashEntryFromEntry( Entry );
    s32             Index            = KeyToIndex( HashEntry.m_Key );
    const auto&     ConstTableEntry  = m_qtHashTable[ Index ];

    HashEntry.m_Semaphore.EndAtomic();
    ConstTableEntry.m_Semaphore.EndAtomic();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::PushEntry( const entry& Entry ) const
{
    const hash_entry&   HashEntry        = getHashEntryFromEntry( Entry );
    s32                 Index            = KeyToIndex( HashEntry.m_Key );
    const auto&         ConstTableEntry  = m_qtHashTable[ Index ];

    HashEntry.m_Semaphore.EndAtomic();
    ConstTableEntry.m_Semaphore.EndAtomic();
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
const typename x_qt_hash<ENTRY,KEY>::hash_entry& x_qt_hash<ENTRY,KEY>::getHashEntryFromEntry( const entry& Entry ) const
{
    const s32           nBytes    = X_MEMBER_OFFSET( hash_entry, m_UserData );
    const xbyte*        pBytes    = ((const xbyte*)&Entry) - nBytes;
    const hash_entry&   HashEntry = *(const hash_entry*)pBytes;
    
    return HashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
typename x_qt_hash<ENTRY,KEY>::hash_entry& x_qt_hash<ENTRY,KEY>::getHashEntryFromEntry( entry& Entry )
{
    const s32           nBytes    = X_MEMBER_OFFSET( hash_entry, m_UserData );
    const xbyte*        pBytes    = ((xbyte*)&Entry) - nBytes;
    hash_entry&         HashEntry = *(hash_entry*)pBytes;
    
    return HashEntry;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpFindOrAddPopEntry( key Key, function Function )
{
    entry& Entry = FindOrAddPopEntry( Key );
    Function( Entry );
    PushEntry( Entry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpAddEntry( key Key, function Function )
{
    entry& Entry = AddPopEntry( Key );
    Function( Entry );
    PushEntry( Entry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpGetEntry( key Key, function Function )
{
    entry* pEntry = getPopEntry( Key );
    ASSERT( pEntry );
    Function( *pEntry );
    PushEntry(*pEntry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpGetEntry( key Key, const_function Function ) const
{
    const entry* pEntry = getPopEntry( Key );
    ASSERT( pEntry );
    Function( *pEntry );
    PushEntry(*pEntry );
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
xbool x_qt_hash<ENTRY,KEY>::cpGetOrFailEntry( key Key, function Function )
{
    entry* pEntry = getPopEntry( Key );
    if( NULL == pEntry ) return FALSE;
    Function( *pEntry );
    PushEntry(*pEntry );
    return TRUE;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
xbool x_qt_hash<ENTRY,KEY>::cpGetOrFailEntry( key Key, const_function Function ) const
{
    const entry* pEntry = getPopEntry( Key );
    if( NULL == pEntry ) return FALSE;
    Function( *pEntry );
    PushEntry(*pEntry );
    return TRUE;
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpIterateGetEntry( function Function )
{
    for( s32 i=0; i<m_qtHashTable.getCount(); i++ )
    {
        const hash_table_entry& TableEntry = m_qtHashTable[i];
        if( TableEntry.m_LinkList.m_Head.GetPtr() )
        {
            TableEntry.m_Semaphore.BeginAtomic();
            for( hash_entry* pHashEntry = (hash_entry*)TableEntry.m_LinkList.m_Head.GetPtr();
                 pHashEntry; pHashEntry = (hash_entry*)pHashEntry->GetPtr() )
            {
                pHashEntry->m_Semaphore.BeginAtomic();
                Function( pHashEntry->m_UserData );
                pHashEntry->m_Semaphore.EndAtomic();
            }
            TableEntry.m_Semaphore.EndAtomic();
        }
    }
}

//-------------------------------------------------------------------------------
template< typename ENTRY, typename KEY > inline
void x_qt_hash<ENTRY,KEY>::cpIterateGetEntry( const_function Function ) const
{
    for( s32 i=0; i<m_qtHashTable.getCount(); i++ )
    {
        const hash_table_entry& TableEntry = m_qtHashTable[i];
        if( TableEntry.m_LinkList.m_Head.GetPtr() )
        {
            TableEntry.m_Semaphore.BeginAtomic();
            for( const hash_entry* pHashEntry = (const hash_entry*)TableEntry.m_LinkList.m_Head.GetPtr();
                 pHashEntry;       pHashEntry = (const hash_entry*)pHashEntry->GetPtr() )
            {
                pHashEntry->m_Semaphore.BeginAtomic();
                Function( pHashEntry->m_UserData );
                pHashEntry->m_Semaphore.EndAtomic();
            }
            TableEntry.m_Semaphore.EndAtomic();
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// QT CIRCULAR QUEUE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

template< s32 SIZE > inline
x_qt_circular_queue<SIZE>::x_qt_circular_queue( void )
{
#ifdef X_DEBUG
    m_Head.m_iHead = 4;
    *(s32*)&m_Ptr[ 0 ] = 0;
    m_Tail.m_iTail = m_Head.m_iHead;
#endif
}

//-------------------------------------------------------------------------------
template< s32 SIZE >
template< class J > inline
J& x_qt_circular_queue<SIZE>::Alloc( s32 Count )
{
    return *(J*)ByteAlloc( Count, xalingof(J) );
}

//-------------------------------------------------------------------------------

template< s32 SIZE > inline
void x_qt_circular_queue<SIZE>::Free( void* pVoidData )
{
#ifdef X_DEBUG
    xbyte*      pData     = (xbyte*)pVoidData;
    const s32   LocalHead = *(s32*)&pData[ -4 ];
    s32&        NewHead   = *(s32*)&m_Ptr[ LocalHead ]; 
    ASSERT( NewHead >= 0 );

    // mark as deleted
    NewHead = -NewHead;

    while( MoveTailForward() );
#endif
}

//-------------------------------------------------------------------------------
#ifndef X_DEBUG
template< s32 SIZE > inline
xbyte* x_qt_circular_queue<SIZE>::ByteAlloc( s32 Count, s32 Aligment )
{
    const s32 MaxMemory = SIZE;

    do 
    {
        const s32   LocalHead   = m_Head.get();
        s32         AllocIndex  = s32( x_Align( &m_Ptr[ 0 ] + LocalHead, Aligment ) - &m_Ptr[0] );
        s32         NewHead     = AllocIndex + Count;
            
        if( NewHead >= MaxMemory )
        {
            AllocIndex      = s32( x_Align( &m_Ptr[ 0 ], Aligment ) - &m_Ptr[0] );
            NewHead         = AllocIndex + Count;
        }

        if( m_Head.set( LocalHead, NewHead ) )
            return &m_Ptr[ AllocIndex ];

    } while( 1 );
        
    return NULL;
}
#endif

//-------------------------------------------------------------------------------
#ifdef X_DEBUG
template< s32 SIZE > inline
xbyte* x_qt_circular_queue<SIZE>::ByteAlloc( s32 Count, s32 Aligment )
{
    const s32 MaxMemory = SIZE;

    while( MoveTailForward() );

    UpdateTailLock( 1 );

    do 
    {
        const head  LocalHead   = *(head*)&m_Head;
        s32         AllocIndex  = s32( x_Align( &m_Ptr[0] + LocalHead.m_iHead + sizeof(s32)*2, Aligment ) - &m_Ptr[0] );
        head        NewHead     = LocalHead;
            
        NewHead.m_iHead         =  x_Align( AllocIndex + Count, 4 );
             
        if( NewHead.m_iHead >= MaxMemory )
        {
            AllocIndex          = s32( x_Align( &m_Ptr[ 0 + sizeof(s32)*2 ], Aligment ) - &m_Ptr[0] );
            NewHead.m_iHead     =  x_Align( AllocIndex + Count, 4 );
            NewHead.m_Cycle++;
        }

        if( x_cas64( (u64*)&m_Head, *(u64*)&LocalHead, *(u64*)&NewHead ) )
        {
            *(s32*)&m_Ptr[ LocalHead.m_iHead ]  = NewHead.m_iHead;
            *(s32*)&m_Ptr[ AllocIndex-4 ]       = LocalHead.m_iHead;
            UpdateTailLock( -1 );

            const head Head = *(head*)&m_Head;
            if( Head.m_Cycle )
            {
                // Out of memory
                ASSERT( Head.m_iHead < m_Tail.m_iTail );
            }
            else
            {
                ASSERT( Head.m_iHead >= m_Tail.m_iTail );
            }

            // x_printf("Head:%d, Cycle:%d TailLock:%d\n", NewHead.m_iHead, NewHead.m_Cycle, m_Tail.m_LockCount );
            return &m_Ptr[ AllocIndex ];
        }

    } while( 1 );
        
    UpdateTailLock( -1 );
    return NULL;
}

//-------------------------------------------------------------------------------
template< s32 SIZE > inline
void x_qt_circular_queue<SIZE>::UpdateTailLock( s32 Inc )
{
    do
    {
        const tail  LocalTail = *(tail*)&m_Tail;
        tail        NewTail   = LocalTail; 
        NewTail.m_LockCount  += Inc;
        if( x_cas64( (u64*)&m_Tail, *(u64*)&LocalTail, *(u64*)&NewTail ) )
            break;

    } while(1);
}

//-------------------------------------------------------------------------------
template< s32 SIZE > inline
s32 x_qt_circular_queue<SIZE>::MoveTailForward( void )
{
    do
    {
        const tail  LocalTail = *(tail*)&m_Tail;
        const head  LocalHead = *(head*)&m_Head;
            
        // make sure that we can change the tail
        if( LocalTail.m_LockCount != 0 )
            break;

        // we are at the end
        if( LocalTail.m_iTail == LocalHead.m_iHead )
            return 0;

        tail NewTail; 
        NewTail.m_iTail = - *(s32*)&m_Ptr[ LocalTail.m_iTail ];

        // we are at the end
        if( NewTail.m_iTail == LocalHead.m_iHead )
            return 0;

        // if this is not freed we can not move forward
        if( NewTail.m_iTail < 0 )
            return 0;

        if( x_cas64( (u64*)&m_Tail, *(u64*)&LocalTail, *(u64*)&NewTail ) )
        {
            // Decrement the loop
            if( LocalTail.m_iTail > NewTail.m_iTail )
            {
                do
                {
                    const head  LocalHead = *(head*)&m_Head;
                    head        NewHead   = LocalHead;

                    NewHead.m_Cycle--;
                    if( x_cas64( (u64*)&m_Head, *(u64*)&LocalHead, *(u64*)&NewHead ) )
                        break;

                } while(1);
            }

            //x_printf("Tail:%d Cycle:%d TailLock:%d\n", NewTail.m_iTail, m_Head.m_Cycle, m_Tail.m_LockCount );
        }  

    } while( 1 );

    return 1;
}
#endif

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE TRIGGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------

inline
x_base_trigger::entry& x_base_trigger::appendEntry( void )
{
    ASSERT(0);
    return *(entry*)(0);
}


/////////////////////////////////////////////////////////////////////////////////
// SCHEDULER
/////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------
inline
void x_scheduler::StartJobChain( x_base_job& Job )
{
    ASSERT(Job.m_RefCount.get() == 0 );
    ASSERT(Job.m_RefAndCount.get() == 1 );
    SubmitJob(Job);
}

//--------------------------------------------------------------------------------
inline
void x_scheduler::StartLightJobChain( x_light_job& Job )
{
    ASSERT(Job.m_RefCount.get() == 0 );
    ASSERT(Job.m_RefAndCount.get() == 1 );
    SubmitJob(Job);
}

//--------------------------------------------------------------------------------
inline
void x_scheduler::StartTriggerChain( x_base_trigger& Tri )
{
    ASSERT(Tri.m_RefCount.get() == 0 );
    ASSERT(Tri.m_RefAndCount.get() == 1 );
    SubmitTrigger(Tri);
}

/////////////////////////////////////////////////////////////////////////////////
// PLATFORM DEPENDENT INCLUDES
/////////////////////////////////////////////////////////////////////////////////

#ifdef TARGET_PC
	#include "windows/x_lockless_container_PC_inline.h"
#elif defined TARGET_IOS || defined TARGET_OSX
	#include "darwin/x_darwin_lockless_inline.h"
#elif defined TARGET_ANDROID
	#include "Android/x_lockless_container_Android_inline.h"
#elif defined TARGET_MARMALADE
    #include "Marmalade/x_lockless_container_Marmalade_inline.h"
#elif defined TARGET_3DS
    #include "3DS/x_lockless_container_3ds_inline.h"
#endif

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif

