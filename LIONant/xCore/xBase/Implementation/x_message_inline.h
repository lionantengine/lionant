//=========================================================================
//=========================================================================
//=========================================================================
// PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!!
//=========================================================================
//=========================================================================
//=========================================================================
#undef X_MSG_UAS
#undef X_MSG_UA
//#undef X_MSG_UAT
#undef X_BEGIN_MESSAGE_MAP
#undef X_MESSAGE_RANGE
#undef X_MESSAGE
#undef X_MESSAGE_ANY
#undef X_END_MESSAGE_MAP 
#undef X_MESSAGE_MAP_DECL

//=========================================================================

void x_MsgInitInstance( void );
void x_MsgKillInstance( void );

//=========================================================================

template< class T >
struct xmsg_table
{
    typedef void (T::*callback_fn)( const xmsg_base& Msg );

    callback_fn     m_MessageFunc;          // Function which a certain message is a sociate with
    u64             m_UA;                   // Unique identifier for a message
    u64             m_Range;                // Range of SubID that the message function is going to handle
};

//=========================================================================

#define X_MSG_UAS(A)                        \
    static void* GetMessageUAS( void )      \
    {                                       \
        static const char* pUniqueID = #A;  \
        return (void*)pUniqueID;            \
    }                                       \

//=========================================================================

#define X_MSG_UA(A,B)                               \
    x_rtti_class1(A,B)                              \
                                                    \
    virtual u64 GetMessageUA( void ) const          \
    {                                               \
        return (m_SubID + (u64)GetMessageUAS());    \
    }                                               \
                                                    \
    X_MSG_UAS(A)

//=========================================================================
/*
#define X_MSG_UAT(A,B,T)                                   \
    x_rtti_class1(A,B)                                     \
                                                           \
    virtual u64 GetMessageUA( void ) const                 \
    {                                                      \
        return (m_SubID + (u64)GetMessageUAS() );          \
    }                                                      \
                                                           \
    static void* GetMessageUAS( void )                     \
    {                                                      \
        static const char* pUniqueID = #A;                 \
        return pUniqueID + (u64)T::GetMessageUAS();        \
    }                                                     
*/
//=========================================================================
                                                           
#define X_BEGIN_MESSAGE_MAP( classname, baseclass )                     \
    void classname::SendXMessage( const xmsg_base& Msg )                \
    {                                                                   \
        typedef xmsg_table<classname> msgt;                             \
        typedef classname xclass_name;                                  \
        typedef baseclass xbase_class;                                  \
        static msgt MessageTable[] =                                    \
        {                                                               \


//=========================================================================

#define X_MESSAGE_RANGE( type, function, subid, range )                 \
            { msgt::callback_fn( &xclass_name::function ), (subid) + (u64)type::GetMessageUAS(), (range)-(subid) },

//=========================================================================

#define X_MESSAGE( type, function, subid )                              \
            { msgt::callback_fn( &xclass_name::function ), (subid) + (u64)type::GetMessageUAS(), 0 },

//=========================================================================

#define X_MESSAGE_ANY( type, function )                                 \
            { msgt::callback_fn( &xclass_name::function ), (u64)type::GetMessageUAS(), U64_MAX },

//=========================================================================

#define X_END_MESSAGE_MAP                                               \
            { NULL, 0, 0 }                                              \
        };                                                              \
                                                                        \
        u64 Type = Msg.GetMessageUA();                                  \
        for ( s32 i=0; MessageTable[i].m_UA; i++ )                      \
        {                                                               \
            if ( MessageTable[i].m_Range < U64_MAX )                    \
            {                                                           \
                if ( Type >= MessageTable[i].m_UA )                     \
                {                                                       \
                    u64 X = Type - MessageTable[i].m_UA;                \
                    if ( X <= MessageTable[i].m_Range )                 \
                    {                                                   \
                        (this->*MessageTable[i].m_MessageFunc)( Msg );  \
                        return;                                         \
                    }                                                   \
                }                                                       \
            }                                                           \
            else                                                        \
            {                                                           \
                u64 X = Type - Msg.m_SubID;                             \
                if ( X == MessageTable[i].m_UA )                        \
                {                                                       \
                    (this->*MessageTable[i].m_MessageFunc)( Msg );      \
                    return;                                             \
                }                                                       \
            }                                                           \
        }                                                               \
                                                                        \
        xbase_class::SendXMessage(Msg);                                 \
    }                                                                   \

//=========================================================================

#define X_MESSAGE_MAP_DECL     virtual void SendXMessage( const xmsg_base& pMsg );

