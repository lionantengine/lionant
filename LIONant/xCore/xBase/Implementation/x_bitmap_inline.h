

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getWidth( void ) const
{
    return m_Width;
}

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getHeight( void ) const
{
    return m_Height;
}

//-------------------------------------------------------------------------------
inline
xbitmap::format xbitmap::getFormat( void ) const
{
    return (xbitmap::format)m_Format;
}

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getDataSize( void ) const
{
    return m_DataSize;
}

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getMipCount( void ) const
{
    return m_nMips;
}

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getMipSize( s32 Mip ) const
{
    
    if( m_nMips == 0 )
    {
        return m_DataSize - sizeof(s32);
    }
    
    
    const xuptr MipOffset       = xuptr(getMip(Mip, m_nFrames-1));
    const xuptr NextMipOffset   = ( Mip < m_nMips-1 )?xuptr(getMip(Mip+1, m_nFrames-1)):xuptr(&((xbyte*)m_pData)[ m_DataSize ]);
    const s32   Size            = s32(NextMipOffset - MipOffset);
    
    return Size;
}

//-------------------------------------------------------------------------------
inline
s32 xbitmap::getFullMipChainCount( void ) const
{
    const s32 SmallerDimension  = x_Min( m_Height, m_Width );
    const s32 nMips             = (s32)x_Log2( SmallerDimension )+1;
    return nMips;
}

//-------------------------------------------------------------------------------
inline
const void* xbitmap::getMip( s32 Mip, s32 Frame ) const
{
    xbyte* pMip = (xbyte*)&m_pData[ m_nMips ];
    return &pMip[Frame*m_FrameSize + m_pData[Mip].m_Offset ];
}

//-------------------------------------------------------------------------------
inline
void xbitmap::setHasAlphaInfo( xbool bAlphaInfo )
{
    if( bAlphaInfo) x_FlagOn    ( m_Flags, FLAGS_ALPHA_INFORMATION );
    else            x_FlagOff   ( m_Flags, FLAGS_ALPHA_INFORMATION );
}


//-------------------------------------------------------------------------------
inline
xbool xbitmap::hasAlphaInfo( void ) const
{
    xbool hasAlpha = x_FlagIsOn( m_Flags, FLAGS_ALPHA_INFORMATION );
    if( hasAlpha ) return hasAlphaChannel();
    return FALSE;
}

//-------------------------------------------------------------------------------
inline
xbool xbitmap::isSquare( void ) const
{
    ASSERT( m_Width > 0 );
    ASSERT( m_Height > 0 );
    ASSERT( m_pData );
    return m_Width == m_Height;
}

//-------------------------------------------------------------------------------
inline
xbool xbitmap::isPowerOfTwo( void ) const
{
    ASSERT( m_Width > 0 );
    ASSERT( m_Height > 0 );
    ASSERT( m_pData );
    return x_IsPow2( m_Width ) && x_IsPow2( m_Height );
}

//-------------------------------------------------------------------------------
inline
void xbitmap::setOwnMemory( xbool bOwnMemory )
{
    if( bOwnMemory) x_FlagOn    ( m_Flags, FLAGS_OWN_MEMORY );
    else            x_FlagOff   ( m_Flags, FLAGS_OWN_MEMORY );
    
}


