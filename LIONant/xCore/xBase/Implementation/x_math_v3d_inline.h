//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

inline 
xvector3d::xvector3d( void )
{
}

//------------------------------------------------------------------------------

inline 
xvector3d::xvector3d( f32 X, f32 Y, f32 Z )
{
    Set( X, Y, Z );
}

//------------------------------------------------------------------------------

inline 
xvector3d::xvector3d( xradian Pitch, xradian Yaw )
{
    Set( Pitch, Yaw );
}

//------------------------------------------------------------------------------
inline 
xvector3d::xvector3d( const f32 n ) 
{
    Set( n );
}

//------------------------------------------------------------------------------
inline
f32* xvector3d::operator()( void )
{
    return &m_X;
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::operator ()( const s32 i ) const
{
    ASSERT( i >= 0 && i <= 3 );
    return ((f32*)(this))[i];
}

//------------------------------------------------------------------------------
inline
f32& xvector3d::operator ()( const s32 i )
{
    ASSERT( i >= 0 && i <= 3 );
    return ((f32*)(this))[i];
}

//------------------------------------------------------------------------------
inline 
void xvector3d::Zero( void )
{
    Set( 0, 0, 0 );
}

//------------------------------------------------------------------------------
inline
void xvector3d::Identity( void )
{
    Zero();
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Set( f32 X, f32 Y, f32 Z )
{
    m_X = X;
    m_Y = Y;
    m_Z = Z;
	ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Set( const f32 n )
{
    m_X = n;
    m_Y = n;
    m_Z = n;
	ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Set( xradian Pitch, xradian Yaw )
{
    f32 PS, PC;
    f32 YS, YC;

    x_SinCos( Pitch, PS, PC );
    x_SinCos( Yaw,   YS, YC );

    return Set( (YS * PC), -PS, (YC * PC) );
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetX( void ) const
{
    return m_X;
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetY( void ) const
{
    return m_Y;
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetZ( void ) const
{
    return m_Z;
}

//------------------------------------------------------------------------------
inline 
f32 xvector3d::GetLength( void ) const
{
    return x_Sqrt( m_X*m_X + m_Y*m_Y + m_Z*m_Z );
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetLengthSquared( void ) const
{
    return m_X*m_X + m_Y*m_Y + m_Z*m_Z;
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetDistanceSquare( const xvector3d& V ) const
{
	f32 f  = m_X - V.m_X;
	f32 r  = f * f;
	    f  = m_Y - V.m_Y;
	    r += f * f;
	    f  = m_Z - V.m_Z;
	    r += f * f;
	return r;

}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetDistance( const xvector3d& V ) const
{
	f32 f  = m_X - V.m_X;
	f32 r  = f * f;
	    f  = m_Y - V.m_Y;
	    r += f * f;
	    f  = m_Z - V.m_Z;
	    r += f * f;
    return x_Sqrt(r);
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Normalize( void )
{
	f32 imag = x_InvSqrt( m_X*m_X + m_Y*m_Y + m_Z*m_Z );
    m_X *= imag;
    m_Y *= imag;
    m_Z *= imag;
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::NormalizeSafe( void )
{
    f32 sqrdis = m_X*m_X + m_Y*m_Y + m_Z*m_Z;

    if( sqrdis < 0.0001f )
    {
        m_X = 1;
        m_Y = 0;
        m_Z = 0;
        return *this;
    }

	f32 imag = x_InvSqrt( sqrdis );

    m_X *= imag;
    m_Y *= imag;
    m_Z *= imag;
    return *this;
}

/*
//------------------------------------------------------------------------------
inline 
xvector3 v3_Normalize( const xvector3d& V )
{
	ASSERT( V.isValid() );

	f32 imag = x_InvSqrt( V.m_X*V.m_X + V.m_Y*V.m_Y + V.m_Z*V.m_Z );

    return xvector3( V.m_X * imag, V.m_Y * imag, V.m_Z * imag );
}


//------------------------------------------------------------------------------
inline 
xvector3 v3_NormalizeSafe( const xvector3d& V )
{
	ASSERT( V.isValid() );

    f32 sqrdis = V.m_X*V.m_X + V.m_Y*V.m_Y + V.m_Z*V.m_Z;

    if( sqrdis < 0.0001f ) return xvector3d(1,0,0);

	f32 imag = x_InvSqrt( sqrdis );

    return xvector3( V.m_X * imag, V.m_Y * imag, V.m_Z * imag );
}
*/
//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator += ( const xvector3d& V )
{
    Set( m_X + V.m_X, m_Y + V.m_Y, m_Z + V.m_Z );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator -= ( const xvector3d& V )
{
    Set( m_X - V.m_X, m_Y - V.m_Y, m_Z - V.m_Z );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator *= ( const xvector3d& V )
{
    Set( m_X * V.m_X, m_Y * V.m_Y, m_Z * V.m_Z );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator *= ( f32 Scalar )
{
    Set( m_X * Scalar, m_Y * Scalar, m_Z * Scalar );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator /= ( f32 Div )
{
    f32 Scalar = 1.0f/Div;
    Set( m_X * Scalar, m_Y * Scalar, m_Z * Scalar );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3d& V, f32 Div )
{
    f32 Scalar = 1.0f/Div;
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3d& V, f32 Scalar )
{
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( f32 Scalar, const xvector3d& V )
{
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
inline 
xbool xvector3d::operator == ( const xvector3d& V ) const
{
         if( x_Abs( V.m_X - m_X) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Y - m_Y) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Z - m_Z) > XFLT_TOL) return FALSE;
    return TRUE;
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::GetMin( const xvector3d& V ) const
{
    return xvector3( x_Min( V.m_X, m_X ), x_Min( V.m_Y, m_Y ), x_Min( V.m_Z, m_Z ) );
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::GetMax( const xvector3d& V ) const
{
    return xvector3( x_Max( V.m_X, m_X ), x_Max( V.m_Y, m_Y ), x_Max( V.m_Z, m_Z ) );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3d& V0, const xvector3d& V1 )
{
    return xvector3( V0.m_X + V1.m_X, V0.m_Y + V1.m_Y, V0.m_Z + V1.m_Z );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3d& V0, const xvector3d& V1 )
{
    return xvector3( V0.m_X - V1.m_X, V0.m_Y - V1.m_Y, V0.m_Z - V1.m_Z );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3d& V0, const xvector3d& V1 )
{
    return xvector3( V0.m_X * V1.m_X, V0.m_Y * V1.m_Y, V0.m_Z * V1.m_Z );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3d& V0, const xvector3d& V1 )
{
    return xvector3( V0.m_X / V1.m_X, V0.m_Y / V1.m_Y, V0.m_Z / V1.m_Z );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3d& V )
{
    return xvector3( -V.m_X, -V.m_Y, -V.m_Z );
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::Lerp( f32 t, const xvector3d& V ) const
{
    return xvector3( m_X + (( V.m_X - m_X ) * t),
                     m_Y + (( V.m_Y - m_Y ) * t),
                     m_Z + (( V.m_Z - m_Z ) * t) );
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::RotateX( xradian Rx )
{
    if( Rx )
    {
        xradian S, C;
        x_SinCos( Rx, S, C );

        f32 Y = m_Y;
        f32 Z = m_Z;
        m_Y = (C * Y) - (S * Z);
        m_Z = (C * Z) + (S * Y);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::RotateY( xradian Ry )
{
    if( Ry )
    {
        xradian S, C;
        x_SinCos( Ry, S, C );

        f32 X = m_X;
        f32 Z = m_Z;
        m_X = (C * X) + (S * Z);
        m_Z = (C * Z) - (S * X);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::RotateZ( xradian Rz )
{
    if( Rz )
    {
        xradian S, C;
        x_SinCos( Rz, S, C );

        f32 X = m_X;
        f32 Y = m_Y;
        m_X = (C * X) - (S * Y);
        m_Y = (C * Y) + (S * X);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Rotate( const xradian3& R )
{
    RotateZ( R.m_Roll  );
    RotateX( R.m_Pitch );
    RotateY( R.m_Yaw   );
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector3d::Dot( const xvector3d& V ) const
{
    return V.m_X * m_X + V.m_Y * m_Y + V.m_Z * m_Z;
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::Cross( const xvector3d& V ) const
{
    return xvector3( m_Y * V.m_Z - m_Z * V.m_Y,
                     m_Z * V.m_X - m_X * V.m_Z,
                     m_X * V.m_Y - m_Y * V.m_X );   
}

//------------------------------------------------------------------------------
inline
xbool xvector3d::IsRightHanded( const xvector3d& P1, const xvector3d& P2 ) const
{
    const f32 x0 = P1.m_X - m_X;
    const f32 y0 = P1.m_Y - m_Y;
    const f32 x1 = P2.m_X - m_X;
    const f32 y1 = P2.m_Y - m_Y;
    const f32 z0 = x0 * y1 - y0 * x1;
    return z0 < 0;
}


//------------------------------------------------------------------------------
inline
xvector3 xvector3d::GetReflection( const xvector3d& V ) const
{
    return V - 2*((*this).Dot(V)) * (*this);
}

//------------------------------------------------------------------------------
inline
xvector3d& xvector3d::GridSnap( f32 GridX, f32 GridY, f32 GridZ )
{
    Set( x_Round( m_X, GridX ), 
         x_Round( m_Y, GridY ),
         x_Round( m_Z, GridZ ) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian xvector3d::GetPitch( void ) const
{
    f32 L = (f32)x_Sqrt( m_X*m_X + m_Z*m_Z );
    return -x_ATan2( m_Y, L );

}

//------------------------------------------------------------------------------
inline
xradian xvector3d::GetYaw( void ) const
{
    return x_ATan2( m_X, m_Z );
}

//------------------------------------------------------------------------------
inline
void xvector3d::GetPitchYaw( xradian& Pitch, xradian& Yaw ) const
{
    Pitch = GetPitch();
    Yaw   = GetYaw();
}

//------------------------------------------------------------------------------
inline
xradian xvector3d::GetAngleBetween( const xvector3d& V ) const
{
    f32 D, Cos;
    
    D = GetLength() * V.GetLength();
    
    if( x_Abs(D) < 0.00001f ) return 0;
    
    Cos = Dot( V ) / D;
    
    if     ( Cos >  1.0f )  Cos =  1.0f;
    else if( Cos < -1.0f )  Cos = -1.0f;
    
    return x_ACos( Cos );
}

//------------------------------------------------------------------------------
//           * Start
//           |
//           <--------------(* this )
//           | Return Vector
//           |
//           |
//           * End
//
// Such: 
//
// this.GetClosestVToLSeg(a,b).LengthSquared(); // Is the length square to the line segment
// this.GetClosestVToLSeg(a,b) + this;          // Is the closest point in to the line segment
//
//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::GetVectorToLineSegment( const xvector3d& Start, const xvector3d& End ) const
{
    xvector3 Diff = *this - Start;
    xvector3 Dir  = End   - Start;
    f32      T    = Diff.Dot( Dir );

    if( T > 0.0f )
    {
        f32 SqrLen = Dir.Dot( Dir );

        if ( T >= SqrLen )
        {
            Diff -= Dir;
        }
        else
        {
            T    /= SqrLen;
            Diff -= T * Dir;
        }
    }

    return -Diff;
}

//------------------------------------------------------------------------------

inline
f32 xvector3d::GetSquareDistToLineSeg( const xvector3d& Start, const xvector3d& End ) const
{
    return GetVectorToLineSegment(Start,End).GetLengthSquared();
}

//------------------------------------------------------------------------------

inline
xvector3 xvector3d::GetClosestPointInLineSegment( const xvector3d& Start, const xvector3d& End ) const
{
    return GetVectorToLineSegment(Start,End) + *this;
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::GetClosestPointToRectangle( 
    const xvector3d& P0,                      // Origin from the edges. 
    const xvector3d& E0, 
    const xvector3d& E1, 
    xvector3d&       OutClosestPoint ) const
{
    xvector3  kDiff    = P0 - *this;
    f32       fA00     = E0.GetLengthSquared();
    f32       fA11     = E1.GetLengthSquared();
    f32       fB0      = kDiff.Dot( E0 );
    f32       fB1      = kDiff.Dot( E1 );
    f32       fS       = -fB0;
    f32       fT       = -fB1;
    f32       fSqrDist = kDiff.GetLengthSquared();

    if( fS < 0.0f )
    {
        fS = 0.0f;
    }
    else if( fS <= fA00 )
    {
        fS /= fA00;
        fSqrDist += fB0*fS;
    }
    else
    {
        fS = 1.0f;
        fSqrDist += fA00 + 2.0f*fB0;
    }

    if( fT < 0.0f )
    {
        fT = 0.0f;
    }
    else if( fT <= fA11 )
    {
        fT /= fA11;
        fSqrDist += fB1*fT;
    }
    else
    {
        fT = 1.0f;
        fSqrDist += fA11 + 2.0f*fB1;
    }

    // Set the closest point
    OutClosestPoint = P0 + (E0 * fS) + (E1 * fT);

    return x_Abs(fSqrDist);
}

//------------------------------------------------------------------------------
inline 
xbool xvector3d::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y) && x_isValid(m_Z);
}

/*
//------------------------------------------------------------------------------
inline 
xvector3 v3_OneOver( const xvector3d& V )
{
    return xvector3( 1.0f/V.m_X, 1.0f/V.m_Y, 1.0f/V.m_Z  );
}
*/

//------------------------------------------------------------------------------
inline
void xvector3d::GetRotationTowards( 
    const xvector3d&  DestV,
    xvector3d&        RotAxis, 
    xradian&          RotAngle ) const
{
    //
    // First get the total length of both vectors this, and dest
    //
        //
    f32 D = GetLength() * DestV.GetLength();
    if( D == 0.0f )  
    {
        RotAxis.Set( 1,0,0 );
        RotAngle = 0;
        return;
    }
    
    //
    // Now find the cos of the angle between both vectors 
    //
    f32 Dot = (*this).Dot(DestV) / D;
    
    if     ( Dot >  1.0f )  Dot =  1.0f;
    else if( Dot < -1.0f )  Dot = -1.0f;

    //
    // Get the axis to rotate those vectors by
    //
    RotAxis = Cross( DestV );

    //
    // Finally compute the final angle
    //
    RotAxis.NormalizeSafe();
    RotAngle = x_ACos(Dot);
}

//------------------------------------------------------------------------------
inline
xvector3d& xvector3d::Abs( void )
{
     m_X = x_Abs( m_X );
     m_X = x_Abs( m_Y );
     m_Z = x_Abs( m_Z );
    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xvector3d::InRange( const f32 Min, const f32 Max ) const
{
    return x_InRange( m_X, Min, Max ) && x_InRange( m_Y, Min, Max ) && x_InRange( m_Z, Min, Max );
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3d::GetOneOver( void ) const
{
    return xvector3( 1.0f/m_X, 1.0f/m_Y, 1.0f/m_Z  );
}

/*
//------------------------------------------------------------------------------
inline
xvector3d xvector3d::GetEulerZYZ( void ) const
{
    // http://vered.rose.utoronto.ca/people/david_dir/GEMS/GEMS.html
}
*/
