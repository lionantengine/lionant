//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
    xvector2 operator - ( const xvector2& V1, const xvector2& V2 )
{
    return xvector2( V1.m_X - V2.m_X, V1.m_Y - V2.m_Y );
}

/*
//------------------------------------------------------------------------------
inline
xvector2::xvector2( void )
{

}
*/

//------------------------------------------------------------------------------
inline
xvector2::xvector2( const xvector2& V ) : m_X(V.m_X), m_Y(V.m_Y) 
{ 
    ASSERT( isValid() ); 
}

//------------------------------------------------------------------------------
inline
xvector2::xvector2( f32 X, f32 Y )
{
    Set( X, Y );
}

//------------------------------------------------------------------------------
inline
xvector2::xvector2( f32 Float )
{
    //Set( x_Cos( Angle ), x_Sin( Angle ) );
    m_X = m_Y = Float;
}

//------------------------------------------------------------------------------
inline 
xvector2& xvector2::Set( f32 X, f32 Y )
{
    m_X = X; 
    m_Y = Y;
    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
void xvector2::Zero( void )
{
    m_X = 0; 
    m_Y = 0;
}

//------------------------------------------------------------------------------
inline 
f32 xvector2::GetLength( void ) const
{
    return x_Sqrt( m_X*m_X + m_Y*m_Y );
}

//------------------------------------------------------------------------------
inline 
f32 xvector2::GetLengthSquared( void ) const
{
    return m_X*m_X + m_Y*m_Y;
}

//------------------------------------------------------------------------------
inline 
xradian xvector2::GetAngle( void ) const
{
    return x_ATan2( m_Y, m_X );
}

//------------------------------------------------------------------------------
inline 
xradian xvector2::GetAngleBetween( const xvector2& V ) const
{
    f32 D, Cos;

    D = GetLength() * V.GetLength();

    if( x_Abs(D) < 0.00001f ) return 0;

    Cos = Dot( V ) / D;

    if     ( Cos >  1.0f )  Cos =  1.0f;
    else if( Cos < -1.0f )  Cos = -1.0f;

    return x_ACos( Cos );
}

//------------------------------------------------------------------------------
inline 
f32 xvector2::GetDistance( const xvector2& V ) const
{
    return x_Sqr(m_X - V.m_X) + x_Sqr(m_Y - V.m_Y);
}

//------------------------------------------------------------------------------
inline 
xbool xvector2::InRange( f32 Min, f32 Max ) const
{
    return x_InRange(m_X, Min, Max) &&
           x_InRange(m_Y, Min, Max);
}

//------------------------------------------------------------------------------
inline
xbool xvector2::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y);
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::Rotate( xradian Angle )
{
    xradian  S, C; 
    x_SinCos( Angle, S, C );

    f32 tX = m_X;
    f32 tY = m_Y;

    Set( (C * tX) - (S * tY), (C * tY) + (S * tX) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector2 xvector2::Lerp( f32 t, const xvector2& V1 ) const
{
    return xvector2( m_X + (( V1.m_X - m_X ) * t),
                     m_Y + (( V1.m_Y - m_Y ) * t) );

}


//------------------------------------------------------------------------------
// Returns the closest point on the 2D LINE defined by line_v1 and line_v2
// to the point pt.  Note that output is NOT necissarily between line_v1
// and line_v2.
//
// Reference: http://astronomy.swin.edu.au/~pbourke/geometry/pointline/
//
// out:		(output) Closest point on the line.
// pt:		Point
// line_v1:	First point on the line.
// line_v2: Second point on the line.
//------------------------------------------------------------------------------
inline
xvector2 xvector2::GetClosestPointInLine( const xvector2 &V0, const xvector2 &V1 )
{
	// safety checks
	ASSERT( (V0.m_X != V1.m_X) && (V0.m_Y != V1.m_Y) );
		
	f32 u = (m_X - V1.m_X) * (V1.m_X - V0.m_X) + (m_Y - V0.m_Y) * (V1.m_Y - V0.m_Y);
	u /= (V0 - V1).GetLengthSquared();
	
    return V0.Lerp( u, V1 );
}

//------------------------------------------------------------------------------
// Returns the closest point on the 2D LINESEGMENT defined by line_v1 and line_v2
// to the point pt.  Note that output WILL BE between line_v1 and line_v2 (or 
// equal to one of them).
//
// out:		(output) Closest point on the line segment.
// pt:		Point
// line_v1: Endpoint of the line segment.
// line_v2: Endpoint of the line segment.
//------------------------------------------------------------------------------
inline
xvector2 xvector2::GetClosestPointInLineSegment( const xvector2 &V0, const xvector2 &V1 )
{
	// degenerate case
	if( V0 == V1 ) 
    {
		return V0;
	}
		
	f32 u = (m_X - V1.m_X) * (V1.m_X - V0.m_X) + (m_Y - V0.m_Y) * (V1.m_Y - V0.m_Y);
	u /= (V0 - V1).GetLengthSquared();
	
	// cap u to the range [0..1]
	u = x_Range( u, 0.0f, 1.0f );
	
    return V0.Lerp( u, V1 );
}

//------------------------------------------------------------------------------
// Determines which side of a line a is point on.
//
// Note that the value returned divided by the distance from line_v1 to line_v2
// is the minimum distance from the point to the line.  That may be useful 
// for determining the distance relationship between points, without actually
// having to calculate the distance.
//
// pt:		Point
// line_v1: Endpoint of the line
// line_v2: Endpoint of the line
//
// returns: > 0.0f if pt is to the left of the line (line_v1->line_v2)
//				< 0.0f if pt is to the right of the line (line_v1->line_v2)
//				= 0.0f if pt is on the line
//
//------------------------------------------------------------------------------
inline
f32 xvector2::GetWhichSideOfLine( const xvector2 &V0, const xvector2& V1 )
{
	return ((m_Y - V0.m_Y) * (V1.m_X - V0.m_X) - (m_X - V0.m_X) * (V1.m_Y - V0.m_Y));
}

//------------------------------------------------------------------------------
inline
f32 xvector2::Dot( const xvector2& V ) const
{
    return (m_X*V.m_X) + (m_Y*V.m_Y);
}

//------------------------------------------------------------------------------
inline
xbool xvector2::operator == ( const xvector2& V ) const
{
         if( x_Abs( V.m_X - m_X) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Y - m_Y) > XFLT_TOL) return FALSE;
    return TRUE;
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator += ( const xvector2& V )
{
    Set( m_X + V.m_X, m_Y + V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator -= ( const xvector2& V )
{
    Set( m_X - V.m_X, m_Y - V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator *= ( const xvector2& V )
{
    Set( m_X * V.m_X, m_Y * V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator *= ( f32 Scalar )
{
    Set( m_X * Scalar, m_Y * Scalar );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator /= ( f32 Div )
{
    f32 Scalar = 1.0f/Div;
    Set( m_X * Scalar, m_Y * Scalar );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::Normalize( void )
{
    f32 div = x_InvSqrt( m_X*m_X + m_Y*m_Y );
    m_X *= div;
    m_Y *= div;
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::NormalizeSafe( void )
{
    f32 sqrdis = m_X*m_X + m_Y*m_Y;
    if( sqrdis < 0.0001f )
    {
        m_X = 1;
        m_Y = 0;
        return *this;
    }
	f32 imag = x_InvSqrt( sqrdis );

    m_X *= imag;
    m_Y *= imag;
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector2 operator + ( const xvector2& V1, const xvector2& V2 )
{
    return xvector2( V1.m_X + V2.m_X, V1.m_Y + V2.m_Y );
}

//------------------------------------------------------------------------------
inline
xvector2 operator - ( const xvector2& V )
{
    return xvector2( -V.m_X, -V.m_Y );
}

//------------------------------------------------------------------------------
inline
xvector2 operator / ( const xvector2& V, f32 S )
{
    S = 1.0f/S;
    return xvector2( V.m_X*S, V.m_Y*S );
}

//------------------------------------------------------------------------------
inline
xvector2 operator * ( const xvector2& V, f32 S )
{
    return xvector2( V.m_X*S, V.m_Y*S );
}

//------------------------------------------------------------------------------
inline
xvector2 operator * ( f32 S, const xvector2& V )
{
    return xvector2( V.m_X*S, V.m_Y*S );
}

//------------------------------------------------------------------------------
inline
xvector2 operator * ( const xvector2& V1, const xvector2& V2 )
{
    return xvector2( V1.m_X*V2.m_X, V1.m_Y*V2.m_Y );

}
