//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xrect::xrect( void )
{
}

//------------------------------------------------------------------------------
inline
xrect::xrect( const xrect& Rect ) :
    m_Left      ( Rect.m_Left ),
    m_Top       ( Rect.m_Top ),
    m_Right     ( Rect.m_Right ),
    m_Bottom    ( Rect.m_Bottom ) 
{

}

//------------------------------------------------------------------------------
inline
xrect::xrect( f32 Left, f32 Top, f32 Right, f32 Bottom )
{
    Set( Left, Top, Right, Bottom );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setup( f32 X, f32 Y, f32 Size )
{
    m_Left   = X-Size;
    m_Top    = Y-Size;
    m_Right  = X+Size;
    m_Bottom = Y+Size;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Set( f32 Left, f32 Top, f32 Right, f32 Bottom )
{
    m_Left   = Left;
    m_Top    = Top;
    m_Right  = Right;
    m_Bottom = Bottom;
    return *this;
}

//------------------------------------------------------------------------------
inline
void xrect::Zero( void )
{
    m_Left      = 0;
    m_Top       = 0;
    m_Right     = 0;
    m_Bottom    = 0;
}

//------------------------------------------------------------------------------
inline
void xrect::setMax( void )
{
    m_Left      =  F32_MAX;
    m_Top       =  F32_MAX;
    m_Right     = -F32_MAX;
    m_Bottom    = -F32_MAX;
}

//------------------------------------------------------------------------------
inline
xbool xrect::Intersect( const xrect& Rect )
{
    return  ( m_Left   <= Rect.m_Right  ) &&
            ( m_Top    <= Rect.m_Bottom ) &&
            ( m_Right  >= Rect.m_Left   ) &&
            ( m_Bottom >= Rect.m_Top    );
}

//------------------------------------------------------------------------------
inline
xbool xrect::Intersect( xrect& R, const xrect& Rect )
{
    if( Intersect( Rect ) == FALSE )
        return( FALSE );

    R.m_Left    = x_Max( m_Left,   Rect.m_Left    );
    R.m_Top     = x_Max( m_Top,    Rect.m_Top     );
    R.m_Right   = x_Min( m_Right,  Rect.m_Right   );
    R.m_Bottom  = x_Min( m_Bottom, Rect.m_Bottom  );

    return TRUE ;
}

//------------------------------------------------------------------------------
inline
xbool xrect::PointInRect( f32 X, f32 Y ) const
{
    return ((X >= m_Left) && (X <= m_Right) && (Y >= m_Top) && (Y <= m_Bottom));
}

//------------------------------------------------------------------------------
inline
xbool xrect::PointInRect( const xvector2& Pos ) const
{
    return PointInRect( Pos.m_X, Pos.m_Y );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::AddPoint( f32 X, f32 Y )
{
    m_Left   = x_Min( m_Left  , X );
    m_Top    = x_Min( m_Top   , Y );
    m_Right  = x_Max( m_Right , X );
    m_Bottom = x_Max( m_Bottom, Y );
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::AddRect( const xrect& Rect )
{
    m_Left   = x_Min( m_Left  , Rect.m_Left   );
    m_Top    = x_Min( m_Top   , Rect.m_Top    );
    m_Right  = x_Max( m_Right , Rect.m_Right  );
    m_Bottom = x_Max( m_Bottom, Rect.m_Bottom );
    return *this;
}

//------------------------------------------------------------------------------
inline
f32 xrect::GetWidth( void ) const
{
    return m_Right - m_Left;
}

//------------------------------------------------------------------------------
inline
f32 xrect::GetHeight( void ) const
{
    return m_Bottom - m_Top;
}

//------------------------------------------------------------------------------
inline
xvector2 xrect::GetSize( void ) const
{
    return xvector2( GetWidth(), GetHeight() );
}

//------------------------------------------------------------------------------
inline
xvector2 xrect::GetCenter( void ) const
{
    return xvector2( m_Left + GetWidth()/2.0f, m_Top + GetHeight()/2.0f );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::SetWidth( f32 W )
{
    m_Right = m_Left + W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::SetHeight( f32 H )
{
    m_Bottom = m_Top + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::SetSize( f32 W, f32 H )
{
    m_Right  = m_Left + W;
    m_Bottom = m_Top  + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Translate( f32 X, f32 Y )
{
    m_Left   += X;
    m_Top    += X;
    m_Right  += Y;
    m_Bottom += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Inflate( f32 X, f32 Y )
{
    m_Left    -= X;
    m_Top     -= Y;
    m_Right   += X;
    m_Bottom  += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Deflate( f32 X, f32 Y )
{
    m_Left    += X;
    m_Top     += Y;
    m_Right   -= X;
    m_Bottom  -= Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xrect::InRange( f32 Min, f32 Max ) const
{
    return (m_Left   >= Min) && (m_Left   <= Max) &&
           (m_Top    >= Min) && (m_Top    <= Max) &&
           (m_Right  >= Min) && (m_Right  <= Max) && 
           (m_Bottom >= Min) && (m_Bottom <= Max);
}

//------------------------------------------------------------------------------
inline
xbool xrect::IsEmpty( void ) const
{
    return( (m_Left>=m_Right) || (m_Top>=m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xbool xrect::operator == ( const xrect& R ) const
{
    return( (m_Left   == R.m_Left  ) &&
            (m_Top    == R.m_Top   ) &&
            (m_Right  == R.m_Right ) &&
            (m_Bottom == R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xbool xrect::operator != ( const xrect& R ) const
{
    return( (m_Left   != R.m_Left  ) ||
            (m_Top    != R.m_Top   ) ||
            (m_Right  != R.m_Right ) ||
            (m_Bottom != R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xrect xrect::Interpolate( f32 T, const xrect& Rect ) const
{
    const xvector4 V1( m_Left, m_Top, m_Right, m_Bottom );
    const xvector4 V2( m_Left, m_Top, m_Right, m_Bottom );
    
    xvector4       Ret( V1 + T * ( V2 - V1 ) );
    
    return xrect( Ret.m_X, Ret.m_Y, Ret.m_Z, Ret.m_W );
}
