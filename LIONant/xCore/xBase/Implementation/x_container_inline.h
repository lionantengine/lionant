//==============================================================================
//==============================================================================
//==============================================================================
// xarray
//==============================================================================
//==============================================================================
//==============================================================================


//------------------------------------------------------------------------------

template< class T > inline
xarray<T>::xarray( s32 StartCapacity, xmem_flags xMemFlags )
{
    m_Count         = 0;
    m_GrowAmount    = 50;
    m_MemoryFlags   = xMemFlags;
    Grow( StartCapacity );
}

//------------------------------------------------------------------------------

template< class T > inline
xarray<T>::xarray( xmem_flags xMemFlags )
{
    m_Count         = 0;
    m_GrowAmount    = 50;
    m_MemoryFlags   = xMemFlags;
}

//------------------------------------------------------------------------------

template< class T > inline
xarray<T>::~xarray( void )
{
    Kill();
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xarray<T>::getCapacity( void ) const
{
    return m_xData.getCount();
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Grow( s32 NewItems )
{
    ASSERTS( m_GrowAmount > 0, "The array was lock from growing" );
    ASSERT( NewItems >= 0 );
    
    // You cant have a reference to the memory while adding more nodes.
    // Imagine if it needs to resize. If you have a reference to a node
    // and the array grows it will be invalid by the time this function
    // returns.
    ASSERT( m_xData.getXRefCount() == 0 );
    
    if( NewItems == 0 )
        return;
    
    //
    // Allocate the new count
    //
    s32 CurCount = m_xData.getCount();
    if( CurCount == 0 )
    {
        m_xData.Alloc( NewItems, m_MemoryFlags );
    }
    else
    {
        s32 NewCapacity = CurCount + NewItems;
        m_xData.Resize( NewCapacity );
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::appendList( s32 Count )
{
    ASSERT( m_GrowAmount > 0 );
    
    // You cant have a reference to the memory while adding more nodes.
    // Imagine if it needs to resize. If you have a reference to a node
    // and the array grows it will be invalid by the time this function
    // returns.
    ASSERT( m_xData.getXRefCount() == 0 );
    
    //
    // Make sure that we have memory
    //
    if( (m_Count+Count) >= m_xData.getCount() )
        Grow( x_Max( m_GrowAmount, Count ) );
    
    //
    // Construct all of them
    //
    Count += m_Count;
    
    xptr_lock Ref(m_xData);
    for( s32 i=m_Count; i<Count; i++ )
    {
        x_Construct<T>( &m_xData[i] );
    }
    
    // Set the new count and return
    m_Count = Count;
}

//------------------------------------------------------------------------------

template< class T > inline
T& xarray<T>::append( s32& Index )
{
    ASSERT( m_GrowAmount > 0 );
    
    // You cant have a reference to the memory while adding more nodes.
    // Imagine if it needs to resize. If you have a reference to a node
    // and the array grows it will be invalid by the time this function
    // returns.
    ASSERT( m_xData.getXRefCount() == 0 );
    
    //
    // Make sure that we have memory
    //
    if( m_Count >= m_xData.getCount() )
        Grow( m_GrowAmount );
    
    Index = m_Count;
    m_Count++;
    
    x_Construct<T>( &m_xData[Index] );
    
    return m_xData[Index];
}

//------------------------------------------------------------------------------
template< class T > inline
T& xarray<T>::operator []( s32 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_xData[Index];
}

//------------------------------------------------------------------------------
template< class T > inline
const T& xarray<T>::operator []( s32 Index ) const
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_xData[Index];
}

//------------------------------------------------------------------------------

template< class T > inline
T& xarray<T>::append( void )
{
    s32 Index;
    return append( Index );
}

//------------------------------------------------------------------------------

template< class T > inline
T& xarray<T>::Insert( s32 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index <= m_Count );
    
    // You cant have a reference to the memory while adding more nodes.
    // Imagine if it needs to resize. If you have a reference to a node
    // and the array grows it will be invalid by the time this function
    // returns.
    ASSERT( m_xData.getXRefCount() == 0 );
    
    //
    // Make sure that we have space
    //
    if( m_Count >= m_xData.getCount() )
        Grow( m_GrowAmount );
    
    //
    // Okay lets move all the elements
    //
    s32 Count = m_Count - Index;
    m_xData.Copy( Index+1, m_xData, Index, Count );
    
    //
    // Lets construct
    //
    x_Construct( &m_xData[Index] );
    m_Count++;
    
    return m_xData[Index];
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::DeleteWithCollapse( s32 Index, s32 Count )
{
    if( Count <= 0)
        return;
    
    ASSERT( Index >= 0 );
    ASSERT( (Index+Count) <= m_Count );
    
    //
    // Lets free all those guys
    //
    xptr_lock Ref(m_xData);
    for( s32 i=0; i<Count; i++ )
    {
        x_Destruct( &m_xData[Index+i]);
    }
    
    //
    // Okay if there is any full node lets move him down
    //
    s32 EndIndex = (Index+Count);
    s32 NewCount = m_Count - EndIndex;
    m_xData.Copy( Index, m_xData, EndIndex, NewCount );
    
    //
    // update the count
    //
    m_Count = m_Count - Count;
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::DeleteWithSwap( s32 Index, s32 Count )
{
    if( Count <= 0)
        return;
    
    ASSERT( Index >= 0 );
    ASSERT( (Index+Count) <= m_Count );
    
    //
    // Lets free all those guys
    //
    xptr_lock Ref(m_xData);
    for( s32 i=0; i<Count; i++ )
    {
        x_Destruct( &m_xData[Index+i]);
    }
    
    //
    // Copy all the nodes from the end to where we are
    //
    if( (Index + Count) < m_Count )
    {
        s32 iSwap = m_Count-1;
        for( s32 i = 0; i<Count; i++ )
        {
            m_xData[ Index+i ] = m_xData[ iSwap-i ];
        }
    }
    
    //
    // update the count
    //
    m_Count = m_Count - Count;
}
//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::DeleteAllNodes( void )
{
    Kill();
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Kill( void )
{
    // You cant have a reference to the memory while deleting nodes.
    ASSERT( m_xData.getXRefCount() == 0 );
    
    if( m_Count > 0 )
    {
        xptr_lock Ref(m_xData);
        for( s32 i=0; i<m_Count; i++ )
        {
            x_Destruct( &m_xData[i] );
        }
    }
    m_Count = 0;
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xarray<T>::getCount( void ) const
{
    return m_Count;
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::SetGrowAmount( s32 Amt )
{
    ASSERT( Amt > 0 );
    m_GrowAmount = Amt;
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Pack( void )
{
    // You cant have a reference to the memory while adding more nodes.
    // Imagine if it needs to resize. If you have a reference to a node
    // and the array grows it will be invalid by the time this function
    // returns.
    ASSERT( m_xData.GetXRefCount() == 0 );
    
    m_xData.Resize( m_Count );
}

//------------------------------------------------------------------------------

template< class T >
template <class KEY> inline
xbool xarray<T>::BinarySearch( const KEY Key, s32& Index ) const
{
    s32 From = 0;
    s32 To   = m_Count - 1;
    
    // Search for the Node
    while( From <= To )
    {
        Index = ( To + From ) >> 1;
        
		if( Key < m_xData[ Index ] )
		{
            To = Index - 1;
		}
		else
		{
			if( m_xData[ Index ] < Key )
            {
                From = Index + 1;
            }
            else
            {
                return TRUE;
            }
		}
    }
    
    if( m_Count <= 0 )          Index = 0;
    else if( From > Index )     Index = From;
    return FALSE;
}

//------------------------------------------------------------------------------

template< class T >
template <class KEY> inline
xbool xarray<T>::BinarySearch( const KEY Key ) const
{
    s32 Index;
    return BinarySearch( Key, Index );
}

//------------------------------------------------------------------------------

template< class T > inline
xbool xarray<T>::Search( const T& Element, s32& Index ) const
{
    for(Index = 0; Index < m_Count; ++Index)
    {
        if(Element == m_xData[Index])
            return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

template< class T > inline
xbool xarray<T>::Search( const T& Element) const
{
    s32 Index;
    return Search( Element, Index );
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Quicksort(s32 lower, s32 upper )
{
	s32	   i, m;
    
    ASSERT( lower >= 0 );
    ASSERT( upper < m_Count );
	xarray<T> &Ref = (*this);
	if( lower < upper )
    {
		x_Swap( Ref[lower], Ref[(upper+lower)/2]);
		m = lower;
		for( i = lower + 1; i <= upper; i++ )
        {
			if( Ref[i] < Ref[lower] )
            {
				m++;
				x_Swap( Ref[m], Ref[i] );
			}
        }
        
		x_Swap( Ref[lower], Ref[m] );
        
		Quicksort(lower, m - 1 );
		Quicksort(m + 1, upper );
	}
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::QuicksortMaxToMin(s32 lower, s32 upper )
{
	s32	   i, m;
    
    ASSERT( lower >= 0 );
    ASSERT( upper < m_Count );
	xarray<T> &Ref = (*this);
	if( lower < upper )
    {
		x_Swap( Ref[lower], Ref[(upper+lower)/2]);
		m = lower;
		for( i = lower + 1; i <= upper; i++ )
        {
			if( Ref[lower] < Ref[i] )
            {
				m++;
				x_Swap( Ref[m], Ref[i] );
			}
        }
        
		x_Swap( Ref[lower], Ref[m] );
        
		Quicksort(lower, m - 1 );
		Quicksort(m + 1, upper );
	}
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Sort( void )
{
    if(getCount()==0) return;
    Quicksort(0, getCount()-1 );
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::SortMaxtoMin( void )
{
    if(getCount()==0) return;
    QuicksortMaxToMin(0, getCount()-1 );
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::Copy( const xarray<T>& Array )
{
    Kill();
    Grow( Array.getCount() );
    
    for( s32 i=0; i<Array.getCount(); i++ )
    {
        const T& Entry = Array[i];
        append() = Entry;
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::StartAccess( void )
{
    m_xData.StartAccess();
}

//------------------------------------------------------------------------------

template< class T > inline
void xarray<T>::EndAccess( void )
{
    m_xData.EndAccess();
}

//------------------------------------------------------------------------------

template< class T > inline
xarray<T>::operator const T* ( void ) const
{
    return &m_xData[0];
}

//------------------------------------------------------------------------------

template< class T > inline
xarray<T>::operator T* ( void )
{
    return &m_xData[0];
}

//==============================================================================
//==============================================================================
//==============================================================================
// xharray
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::xharray( xmem_flags Flags )
{
    m_nNodes      = 0;
    m_MemoryFlags = Flags;
}

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::~xharray( void )
{
    Kill();
}

//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::Kill( void )
{
    s32 i;
    
    //
    // First call the destructor for all the bussy nodes
    //
    if( m_xData.isValid() )
    {
        xptr_lock Ref(m_xData);
        for( i=0; i<m_nNodes; i++ )
        {
            x_Destruct( &m_xData[i] );
        }
    }
    
    //
    // Now free up the memory
    //
    m_nNodes = 0;
    m_xData.Destroy();
    m_xMap.Destroy();
}

//------------------------------------------------------------------------------

template< class T >
void xharray<T>::GrowListBy( s32 nNodes )
{
    s32         i;
    s32         OldCapacity = m_xData.getCount();
    s32         NewCapanity = OldCapacity + nNodes;
    
    ASSERT( nNodes > 0 );
    
    //
    // Allocate the new arrays
    //
    if( OldCapacity == 0 )
    {
        m_xMap.Alloc ( NewCapanity, m_MemoryFlags );
        m_xData.Alloc( NewCapanity, m_MemoryFlags );
    }
    else
    {
        m_xMap.Resize ( NewCapanity );
        m_xData.Resize( NewCapanity );
    }
    
    //
    // Fill in the rest of the hash entries
    //
    xptr_lock Ref(m_xMap);
    for( i = OldCapacity; i<NewCapanity; i++ )
    {
        map& Map = m_xMap[ i ];
        Map.m_IndexToHandle.Set( i );
        Map.m_HandleToIndex = i;
    }
}

//------------------------------------------------------------------------------

template< class T > inline
T& xharray<T>::operator[]( s32 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_nNodes );
    
    return m_xData[ m_xMap[ Index ].m_IndexToHandle.m_Handle ];
}

//------------------------------------------------------------------------------

template< class T > inline
const T& xharray<T>::operator[]( s32 Index ) const
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_nNodes );
    
    return m_xData[ m_xMap[ Index ].m_IndexToHandle.m_Handle ];
}

//------------------------------------------------------------------------------

template< class T > inline
T& xharray<T>::operator()( xhandle hHandle )
{
    ASSERT( hHandle.Get() >= 0 );
    ASSERT( hHandle.Get() < (u32)m_xData.getCount() );
    return m_xData[ hHandle.m_Handle ];
}

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::operator const T* ( void ) const
{
    return &m_xData[0];
}

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::operator T* ( void )
{
    return &m_xData[0];
}

//------------------------------------------------------------------------------

template< class T > inline
const T& xharray<T>::operator()( xhandle hHandle ) const
{
    ASSERT( hHandle.Get() >= 0 );
    ASSERT( hHandle.Get() < (u32)m_xData.getCount() );
    return m_xData[ hHandle.m_Handle ];
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xharray<T>::getCount( void ) const
{
    return m_nNodes;
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xharray<T>::GetCapacity( void ) const
{
    return m_xData.getCount();
}

//------------------------------------------------------------------------------

template< class T > inline
xhandle xharray<T>::GetHandleByIndex( s32 Index ) const
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_nNodes );
    
    ASSERT( m_xMap[ Index ].m_IndexToHandle.isValid() );
    xhandle Handle(m_xMap[ Index ].m_IndexToHandle);
    return Handle;
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xharray<T>::GetIndexByHandle( xhandle hHandle ) const
{
    ASSERT( hHandle.Get() >= 0 );
    ASSERT( hHandle.Get() < (u32)m_xData.getCount() );
    
    s32 Index = m_xMap[ hHandle.Get() ].m_HandleToIndex;
    return Index;
}

//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::DeleteByIndex( s32 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_nNodes );
    
    xptr_lock     rMap(m_xMap);
    map&     MapIndex = m_xMap[ Index ];
    xhandle  HandleID = MapIndex.m_IndexToHandle;
    
    m_nNodes--;
    
    // Call Destructor
    xptr_lock rData(m_xData);
    x_Destruct( &m_xData[ HandleID.Get() ] );
    
    //
    // Fix up the links
    //
    map& MapNNodes = m_xMap[ m_nNodes ];
    
    // Set the index reference
    m_xMap[ HandleID.Get() ].m_HandleToIndex                        = m_nNodes;
    m_xMap[ MapNNodes.m_IndexToHandle.Get() ].m_HandleToIndex       = Index;
    
    // Copy the last node into the deleted node. We don't care if it is the same.
    MapIndex.m_IndexToHandle  = MapNNodes.m_IndexToHandle;
    MapNNodes.m_IndexToHandle = HandleID;
}

//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::DeleteByHandle( xhandle hHandle )
{
    ASSERT( hHandle.Get() >= 0 );
    ASSERT( hHandle.Get() <(u32) m_xData.getCount() );
    
    DeleteByIndex( GetIndexByHandle( hHandle ) );
}

//------------------------------------------------------------------------------

template< class T > inline
T& xharray<T>::append( xhandle& hHandle )
{
    //
    // Grow if need it
    //
    s32 CurCapacity = m_xData.getCount();
    if( m_nNodes >= CurCapacity )
    {
        GrowListBy( x_Max( CurCapacity/2, 100 ) );
    }
    
    //
    // Lets give the new handle to the node
    //
    hHandle = m_xMap[ m_nNodes ].m_IndexToHandle;
    ASSERT( hHandle.Get() >= 0 );
    ASSERT( hHandle.Get() < (u32)m_xData.getCount() );
    
    // add the node count
    m_nNodes++;
    
    // Call the constructor
    x_Construct( &m_xData[ hHandle.m_Handle ] );
    
    return m_xData[ hHandle.m_Handle ];
}

//------------------------------------------------------------------------------

template< class T > inline
T& xharray<T>::append( void )
{
    xhandle H;
    return append(H);
}

//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::DeleteAllNodes( xbool bReorder )
{
    // Nothing to do if no nodes are here
    if(m_nNodes>0)
    {
        s32 i;
	    
        // Destruct all the remaining objects
        xptr_lock rMap(m_xMap);
        xptr_lock rData(m_xData);
        for( i=0; i<m_nNodes; i++ )
        {
            x_Destruct( &m_xData[ m_xMap[ i ].m_IndexToHandle.Get() ] );
        }
        
        // reorder if the user request it
        if( bReorder )
        {
            for( i=0; i<m_xMap.getCount(); i++ )
            {
                map& Map = m_xMap[i];
                Map.m_IndexToHandle.Set( i );
                Map.m_HandleToIndex = i;
            }
        }
    }
    
    m_nNodes = 0;
}

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::operator const xptr<T>& ( void ) const
{
    return m_xData;
}

//------------------------------------------------------------------------------

template< class T > inline
xharray<T>::operator xptr<T>& ( void )
{
    return m_xData;
}
//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::StartAccess( void )
{
    m_xData.StartAccess();
    m_xMap.StartAccess();
}

//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::EndAccess( void )
{
    m_xData.EndAccess();
    m_xMap.EndAccess();
}


//------------------------------------------------------------------------------

template< class T > inline
void xharray<T>::SwapIndex( s32 IndexA, s32 IndexB )
{
    ASSERT( IndexA >= 0 );
    ASSERT( IndexA < m_nNodes );
    ASSERT( IndexB >= 0 );
    ASSERT( IndexB < m_nNodes );
    s32 hA = m_xMap[IndexA].m_IndexToHandle.m_Handle;
    s32 hB = m_xMap[IndexB].m_IndexToHandle.m_Handle;
    
    x_Swap( m_xMap[IndexA].m_IndexToHandle.m_Handle, m_xMap[IndexB].m_IndexToHandle.m_Handle );
    x_Swap( m_xMap[hA].m_HandleToIndex, m_xMap[hB].m_HandleToIndex );
}

//==============================================================================
//==============================================================================
//==============================================================================
// xblock_array
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

template<class T, s32 S > inline
xblock_array<T,S>::xblock_array ( void )
{
    ASSERT( S > 0 );
    ASSERT( S < (0xffff-1) );
    m_pHead  = NULL;
    m_pEmpty = NULL;
}

//------------------------------------------------------------------------------

template<class T, s32 S > inline
xblock_array<T,S>::~xblock_array ( void )
{
    Kill();
}

//------------------------------------------------------------------------------

template<class T, s32 S > inline
void xblock_array<T,S>::Kill( void )
{
    //
    // Delete the hold chain
    //
    for( block* pBlock = m_pHead; pBlock; )
    {
        block* pNext = pBlock->m_pNext;
        
        for( s32 i=pBlock->m_iUsed; i!=0xffff; i = pBlock->m_Node[i].m_iNext )
        {
            // call destructor
            pBlock->m_Node[i].~node();
        }
        
        // delete block and move to the next one
        if( pBlock ) x_free(pBlock);
        pBlock = pNext;
    }
    
    //
    // Reset the pointers
    //
    m_pHead  = NULL;
    m_pEmpty = NULL;
}

//------------------------------------------------------------------------------

template<class T, s32 S > inline
void xblock_array<T,S>::Grow( void )
{
    ASSERT( m_pEmpty == NULL );
    //
    // Allocate the block
    //
    block* pBlock = (block*)x_malloc( sizeof(block), 1, 0 );
    if( pBlock == NULL )
        x_throw( "out of memory" );
    
    //
    // Initialize the block variables
    //
    pBlock->m_pNext  = m_pHead;
    pBlock->m_pEmpty = m_pEmpty;
    pBlock->m_iUsed  = 0xffff;
    pBlock->m_iEmpty = 0;
    
    // Create the link list of empty nodes
	s32 i;
    for( i=0; i<S; i++ )
    {
        pBlock->m_Node[i].m_iNext = i+1;
    }
    pBlock->m_Node[i-1].m_iNext = 0xffff;
    
    //
    // Add officially the block
    //
    m_pHead  = pBlock;
    m_pEmpty = pBlock;
}

//------------------------------------------------------------------------------

template<class T, s32 S > inline
T* xblock_array<T,S>::append( void )
{
    // Make sure that there is node for us to use
    if( m_pEmpty == NULL ) Grow();
    
    // unlink node from the empty list
    u16 Index = m_pEmpty->m_iEmpty;
    ASSERT( Index != 0xffff );
    m_pEmpty->m_iEmpty = m_pEmpty->m_Node[ Index ].m_iNext;
    
    // link node to the use list
    m_pEmpty->m_Node[ Index ].m_iNext = m_pEmpty->m_iUsed;
    m_pEmpty->m_iUsed = Index;
    
    // check if this was the last node.
    // if so we must remove the block from the empty link list
    block* pTemp = m_pEmpty;
    if( m_pEmpty->m_iEmpty == 0xffff )
    {
        m_pEmpty = m_pEmpty->m_pEmpty;
        
        // set to null the pointer for the empty block
        pTemp->m_pEmpty = NULL;
    }
    
    // initialize the node
    T* pNode = &pTemp->m_Node[ Index ].m_Node;
    x_Construct( pNode );
    
    // return the newly allocate node
    return pNode;
}

//------------------------------------------------------------------------------

template<class T, s32 S > inline
void xblock_array<T,S>::Delete( T* pNode )
{
    ASSERT( pNode );
    
    //
    // Loop throw the block and try to find the block that
    // contains this pointer
    //
	block* pBlock;
    for( pBlock = m_pHead; pBlock; pBlock = pBlock->m_pNext )
    {
        if( ((u8*)pNode) > ((u8*)pBlock) &&
           ((u8*)pNode) < (((u8*)pBlock)+sizeof(block)) ) break;
    }
    
    // Did we found the block?
    ASSERT( pBlock );
    
    // Okay now get the index
    u16 Index = (u16)((((u8*)pNode) - ((u8*)&pBlock->m_Node[0].m_Node))/sizeof(node));
    ASSERT( &pBlock->m_Node[Index].m_Node == pNode );
    
    //
    // unlink node from the use list
    //
    if( pBlock->m_iUsed == Index )
    {
        pBlock->m_iUsed = pBlock->m_Node[Index].m_iNext;
    }
    else
    {
        // find the previous node the brute force way
		u16 iPrev = pBlock->m_iUsed;
        
        // If this happens then there is something wrong.
        // Such we are trying to delete a node that not longer exits
        ASSERT( iPrev != 0xffff );
        
        for(; pBlock->m_Node[iPrev].m_iNext != Index; )
        {
            // goto the next node
            iPrev = pBlock->m_Node[iPrev].m_iNext;
            
            // If this happens then there is something wrong.
            // Such we are trying to delete a node that not longer exits
            ASSERT( iPrev != 0xffff );
        }
        
        // Set the next full node
        pBlock->m_Node[iPrev].m_iNext = pBlock->m_Node[Index].m_iNext;
    }
    
    //
    // See whether we should add this block into the empty chain
    //
    if( pBlock->m_iEmpty == 0xffff )
    {
        pBlock->m_pEmpty = m_pEmpty;
        m_pEmpty = pBlock;
    }
    
    //
    // Add node to the empty list
    //
    pBlock->m_Node[Index].m_iNext = pBlock->m_iEmpty;
    pBlock->m_iEmpty = Index;
    
    //
    // Now Call the destructor
    //
    pBlock->m_Node[Index].~node();
}

//==============================================================================
//==============================================================================
//==============================================================================
// xbitarray
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------
inline
xbitarray::xbitarray( void )
{
    
}

//------------------------------------------------------------------------------
inline
xbitarray::~xbitarray( void )
{
    
}

//------------------------------------------------------------------------------
inline
void xbitarray::Init( s32 Size )
{
    m_Buffer.Alloc( (Size + 7) >> 3 );
}

//------------------------------------------------------------------------------
inline
void xbitarray::SetAllBits( xbool BitValue )
{
    if( BitValue )
    {
        m_Buffer.SetMemory( 0xffffffff );
    }
    else
    {
        m_Buffer.SetMemory( 0x00000000 );
    }
}

//------------------------------------------------------------------------------
// Retrieve the value of a given array element.
//
// index: index of the array element to set.
//
// returns: the value of that array element, either 0 or 1.
//
inline
xbool xbitarray::Get( s32 Index ) const
{
    ASSERT( Index < (m_Buffer.getCount()<<3) );
    return 1&(m_Buffer[Index>>3]>>(Index&7));
}

//------------------------------------------------------------------------------
// set will set the index'th bit in the array to the given value.  (well, if value is greater than 1, it will still
// set it to 1 ;) ).
//
// index: index of the array element to set.
// value: value to set it to.
inline
void xbitarray::Set( s32 Index, xbool Value )
{
    ASSERT( Index < (m_Buffer.getCount()<<3) );
    ASSERT( Value > -1 );
    ASSERT( Value <  2 );
    
    if( Value )
    {
        m_Buffer[Index>>3] |= (1<<(Index&7));
    }
    else
    {
        m_Buffer[Index>>3] &= ~(1<<(Index&7));
    }
}

//==============================================================================
//==============================================================================
//==============================================================================
// xcmd_array
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------
inline
xcmd_array::xcmd_array( void ) :
m_GrowAmount(1024),
m_iCurrent(0),
m_iPrevious(0)
{ }

//------------------------------------------------------------------------------
inline
xcmd_array::~xcmd_array( void )
{
}

//------------------------------------------------------------------------------
inline
void xcmd_array::SetGrowAmount( s32 nBytes )
{
    ASSERT( nBytes >= sizeof(cmd_entry) );
    m_GrowAmount =      nBytes;
}

//------------------------------------------------------------------------------
inline
void xcmd_array::Grow( s32 nBytes )
{
    ASSERT( nBytes > 0 );
    s32 NewSize = nBytes > m_GrowAmount ? nBytes:m_GrowAmount;
    if( m_Buffer.getCount() == 0 )
    {
        m_Buffer.Alloc( NewSize );
    }
    else
    {
        m_Buffer.Resize( m_Buffer.getCount()+NewSize );
    }
}

//------------------------------------------------------------------------------
inline
void xcmd_array::Kill( void )
{
    Reset();
    m_Buffer.Destroy();
}

//------------------------------------------------------------------------------
inline
void xcmd_array::Reset( void )
{
    m_iCurrent  = (0);
    m_iPrevious = (0);
}

//------------------------------------------------------------------------------
template< class T > inline
T& xcmd_array::append( void )
{
    // Check whether we need to grow
    s32 NewSize = m_iCurrent + sizeof(T);
    if( NewSize > m_Buffer.getCount() )
        Grow( sizeof(T) );
    
    // Transfer the pointer to the allocater type
    T* pEntry = ((T*)&m_Buffer[m_iCurrent] );
    
    // Make sure to call the constructor
    x_Construct( pEntry );
    
    // Set the next index to zero
    T& NextPtr = T::SafeCast( *((cmd_entry*)&m_Buffer[m_iCurrent] ) );
    NextPtr.SetNextCmd(-1);
    
    // Transfer the previous Index to this new one
    if( m_iCurrent > 0 )
    {
        cmd_entry* pPrevPtr = (cmd_entry*)&m_Buffer[m_iPrevious];
        pPrevPtr->SetNextCmd( NewSize );
    }
    
    // Set the index forward.
    m_iPrevious = m_iCurrent;
    m_iCurrent  = NewSize;
    
    // Good to go
    return *pEntry;
}

//------------------------------------------------------------------------------
template< class T > inline
T& xcmd_array::Get( s32 Index )
{
    ASSERT( Index<m_iCurrent );
    
    // Transfer the pointer to the allocater type
    T& Ptr = T::SafeCast( *((cmd_entry*)&m_Buffer[Index]) );
    
    // Good to go
    return Ptr;
}

//------------------------------------------------------------------------------
template< class T > inline
const T& xcmd_array::Get( s32 Index ) const
{
    ASSERT( Index<m_iCurrent );
    
    // Transfer the pointer to the allocater type
    T& Ptr = T::SafeCast( *((cmd_entry*)&m_Buffer[Index]) );
    
    // Good to go
    return Ptr;
}

//------------------------------------------------------------------------------
inline
s32 xcmd_array::GetEndIndex( void )
{
    return m_iCurrent;
}

//------------------------------------------------------------------------------
inline
xcmd_array::cmd_entry& xcmd_array::GetEntry( s32 Index )
{
    return Get<xcmd_array::cmd_entry>(Index);
}

//------------------------------------------------------------------------------
inline
const xcmd_array::cmd_entry& xcmd_array::GetEntry( s32 Index ) const
{
    return Get<const xcmd_array::cmd_entry>(Index);
}

//------------------------------------------------------------------------------
inline
void xcmd_array::StartAccess( void )
{
    m_Buffer.StartAccess();
}

//------------------------------------------------------------------------------
inline
void xcmd_array::EndAccess( void )
{
    m_Buffer.EndAccess();
}

//------------------------------------------------------------------------------
inline
s32 xcmd_array::GetNextIndex( s32 Index )
{
    return GetEntry( Index ).GetNextCmd();
}


//==============================================================================
//==============================================================================
//==============================================================================
// xpage_array
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

template<class T> inline
s32 xparray<T>::getCount( void ) const 
{ 
    return m_Count; 
}
    
//------------------------------------------------------------------------------

template<class T> 
template<class I> inline
T& xparray<T>::operator[] ( I index ) 
{ 
    return (*this)( m_Iterator[index].m_hEntry );
}

//------------------------------------------------------------------------------

template<class T> 
template<class I> inline
const T& xparray<T>::operator[] ( I index ) const 
{ 
    return (*this)( m_Iterator[index].m_hEntry );
}

//------------------------------------------------------------------------------

template<class T> inline
T& xparray<T>::operator() ( xhandle   Handle  )
{
    const s32     iPage   = Handle.m_Handle >> HANDLE_PAGE_SHIFT;
    const s32     iEntry  = Handle.m_Handle & HANDLE_ENTRY_MASK;
    page&         Page    = m_lPages[ iPage ];
    T&            Entry   = Page.m_Entry[ iEntry ];

    return Entry;
}

//------------------------------------------------------------------------------

template<class T> inline
const T& xparray<T>::operator() ( xhandle   Handle  ) const
{
    const s32     iPage   = Handle.m_Handle >> HANDLE_PAGE_SHIFT;
    const s32     iEntry  = Handle.m_Handle & HANDLE_ENTRY_MASK;
    page&         Page    = m_lPages[ iPage ];
    T&            Entry   = Page.m_Entry[ iEntry ];

    return Entry;
}

//------------------------------------------------------------------------------

template<class T> 
s32 xparray<T>::HandleToIndex( xhandle Handle ) const
{
    return m_Iterator[ Handle.m_Handle ].m_iHandle;
}

//------------------------------------------------------------------------------

template<class T> inline
xhandle xparray<T>::IndexToHandle( s32 Index ) const
{
    return m_Iterator[ Index ].m_hEntry;
}

//------------------------------------------------------------------------------

template<class T> inline
void xparray<T>::SanityCheck( void ) const
{
    //
    // Sanity check the pages
    //
    for( auto& Page : m_lPages )
    {
        // Make sure that the number of allocated entries for this page is proper
        s32 nFree = 0;
        for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            nFree++;

        ASSERT( (ENTRIES_PER_PAGE - nFree) == Page.m_nAlloced );
    }

    //
    // Sanity check the iterators
    //
    for( const auto& Iterator : m_Iterator )
    {
        const u32   Index  = s32(&Iterator - &m_Iterator[0]);
        const s32   iPage  = Iterator.m_hEntry.m_Handle >> HANDLE_PAGE_SHIFT;
        const s32   iEntry = Iterator.m_hEntry.m_Handle & HANDLE_ENTRY_MASK;
        const s32   iIter  = Iterator.m_hEntry.m_Handle;
        const auto& Iter   = m_Iterator[ iIter ];
        const auto& Page   = m_lPages[iPage];
        const auto& Entry  = Page.m_Entry[iEntry];

        ASSERT( Iter.m_iHandle == Index );

        // Lets make sure that that is properly free/allocated
        if( Index < m_Count )
        {
            for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            {
                ASSERT( &Entry != &Page.m_Entry[iFree] );
            }
        }
        else
        {
            xbool bFound = FALSE;
            for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            {
                if( &Entry == &Page.m_Entry[iFree] )
                {
                    bFound = TRUE;
                    break;
                }
            }
            ASSERT(bFound);
        }
    }
}

//------------------------------------------------------------------------------
    
template<class T> inline
T& xparray<T>::append( void )
{
    xhandle Handle;
    return append( Handle );
}

//------------------------------------------------------------------------------

template<class T> inline
T& xparray<T>::append( xhandle& Handle )
{
    if( m_iFreePage == 0xffff )
    {
        AllocPage();
        ASSERT( m_iFreePage != 0xffff );
    }

    page&       Page     = m_lPages[ m_iFreePage ];
    const s32   iEntry   = Page.m_iFreeHead;
    const s32   iPage    = m_iFreePage;

    Page.m_iFreeHead = *((u16*)&Page.m_Entry[ iEntry ]); 
    Handle.m_Handle  = iEntry | ( iPage << HANDLE_PAGE_SHIFT );
    Page.m_nAlloced++;

    if( Page.m_nAlloced >= ENTRIES_PER_PAGE )
    {
        m_iFreePage = Page.m_iNextFreePage;
    }

    //
    // Deal with iterator
    //
    {
        const s32   iLastEntry    = m_Count;
        auto&       Iterator      = m_Iterator[ Handle.m_Handle ];
        const s32   Index         = Iterator.m_iHandle;

        x_Swap( m_Iterator[ iLastEntry ].m_hEntry, m_Iterator[Index].m_hEntry );
        Iterator.m_iHandle        = iLastEntry;  

        const s32 LastIndex  = m_Iterator[Index].m_hEntry.m_Handle; 
        ASSERT( m_Iterator[ LastIndex ].m_iHandle == iLastEntry );

        m_Iterator[ LastIndex ].m_iHandle = Index;
    }

    m_Count++;  

    x_Construct( &Page.m_Entry[ iEntry ] );

    return Page.m_Entry[ iEntry ];
}

//------------------------------------------------------------------------------

template<class T> inline
void xparray<T>::Delete( s32 index ) 
{ 
    Delete( m_Iterator[index].m_hEntry ); 
}

//------------------------------------------------------------------------------

template<class T> inline
void xparray<T>::Delete( xhandle Handle ) 
{
    ASSERT( m_Count > 0 );

    const s32   iPage   = Handle.m_Handle >> HANDLE_PAGE_SHIFT;
    const s32   iEntry  = Handle.m_Handle & HANDLE_ENTRY_MASK;
    page&       Page    = m_lPages[ iPage ];
        
    //
    // Deal with page and entry
    //
    x_Destruct( &Page.m_Entry[ iEntry ] );

    *((u16*)&Page.m_Entry[ iEntry ]) = Page.m_iFreeHead;
    Page.m_iFreeHead = iEntry;
        
    ASSERT(Page.m_nAlloced>0);
    Page.m_nAlloced--;
        
    //
    // Deal with iterator
    //
    if( m_Count > 1 )
    {
        const s32   iLastEntry    = (m_Count-1);
        auto&       Iterator      = m_Iterator[ Handle.m_Handle ];
        const s32   Index         = Iterator.m_iHandle;

        x_Swap( m_Iterator[ iLastEntry ].m_hEntry, m_Iterator[Index].m_hEntry );
        Iterator.m_iHandle        = iLastEntry;  

        const s32 LastIndex  = m_Iterator[Index].m_hEntry.m_Handle;
        ASSERT( m_Iterator[ LastIndex ].m_iHandle == iLastEntry );

        m_Iterator[ LastIndex ].m_iHandle = Index;
    }

    //
    // Deal with the main class
    //
    if( Page.m_nAlloced == ENTRIES_PER_PAGE )
    {
        Page.m_iNextFreePage = m_iFreePage;
        m_iFreePage = iPage;
    }

    m_Count--;
}

//------------------------------------------------------------------------------

template<class T> inline
void xparray<T>::AllocPage( void )
{
    u16 nPages;

    //
    // Grow the page numbers
    //
    if( m_lPages.isValid() )
    {
        nPages = m_lPages.getCount();
        m_lPages.Resize     ( nPages + 1 );
        m_Iterator.Resize   ( m_Iterator.getCount() + ENTRIES_PER_PAGE );
    }
    else
    {
        nPages = 0;
        m_lPages.Alloc(1);
        m_Iterator.Alloc( ENTRIES_PER_PAGE );
    }

    //
    // Initialize the new page
    //
    page& Page = m_lPages[ nPages ];
    
    Page.m_nAlloced           = 0;
    Page.m_iNextFreePage      = 0xffff;
    Page.m_iFreeHead          = 0;

    for( s32 i=0; i<ENTRIES_PER_PAGE; i++ )
        *((u16*)&Page.m_Entry[i]) = i + 1;

    *((u16*)&Page.m_Entry[ ENTRIES_PER_PAGE - 1 ]) = 0xffff;

    Page.m_iNextFreePage    = m_iFreePage;
    m_iFreePage             = nPages;

    //
    // Fill the iterator info
    //
    for( s32 i=0; i<ENTRIES_PER_PAGE; i++ )
    {
        const s32   IteratorIndex    = nPages * ENTRIES_PER_PAGE + i;
        auto&       Iterator         = m_Iterator[ IteratorIndex ];
        xhandle     Handle;
            
        Handle.Set( (nPages << HANDLE_PAGE_SHIFT) | i );

        Iterator.m_iHandle = IteratorIndex;
        Iterator.m_hEntry   = Handle;
    }
}
