//=========================================================================
// INCLUDES
//=========================================================================
#include "../x_base.h"

//=========================================================================
//=========================================================================
//=========================================================================
// LOCAL VARS
//=========================================================================
//=========================================================================
//=========================================================================

struct broadcast_node
{
    X_LL_CIRCULAR_ARRAY_FUNCTIONS( BC,   broadcast_node, u16, m_iNextBN,   m_iPrevBN   );
    X_LL_CIRCULAR_ARRAY_FUNCTIONS( Type, broadcast_node, u16, m_iTypeNext, m_iTypePrev );

    u16                   m_iChannel;         // Which channel are we
    xmsg_receiver*        m_Receiver;         // The class that just subscriber to the channel
    xmsg_receiver*        m_Notifier;         // Pointer to the class that the subscriber just subscribed to
    u16                   m_iNextBN;          // next for the same broadcast receiver
    u16                   m_iPrevBN;          // prev for the same broadcast receiver
    u16                   m_iTypeNext;        // reference for for the receiver when deleting themselves  
    u16                   m_iTypePrev;        // reference for for the receiver when deleting themselves  
};

//DOM-IGNORE-BEGIN

struct message_local
{
    xharray<broadcast_node> m_BroadcastNodes;
    xcritical_section       m_CriticalSection;
};

//=========================================================================

static inline
message_local& GetMessageLocal( void )
{
    xbase_instance& Instance = x_GetXBaseInstance();
    ASSERT( Instance.m_pMessage );
    return *((message_local*)Instance.m_pMessage);
}
//DOM-IGNORE-END

//=========================================================================
//=========================================================================
//=========================================================================
// xmsg_broadcast_channel
//=========================================================================
//=========================================================================
//=========================================================================

//=========================================================================

xmsg_broadcast_channel::xmsg_broadcast_channel( void )
{
}

//=========================================================================
//=========================================================================
//=========================================================================
// RECEIVER
//=========================================================================
//=========================================================================
//=========================================================================

//=========================================================================

xmsg_receiver::xmsg_receiver( void ) : m_iChannel( broadcast_node::BC_NULL ), m_iType( broadcast_node::Type_NULL )
{
}

//=========================================================================

xmsg_receiver::~xmsg_receiver( void )
{
    xhandle Handle;
    xscope_atomic( GetMessageLocal().m_CriticalSection );
    message_local& Local = GetMessageLocal();

    // Remove node from all the link lists
    for( u16 Index = m_iType; Index != broadcast_node::Type_NULL ;   )
    {
        Handle.Set( Index );
        broadcast_node& Node = Local.m_BroadcastNodes( Handle );

        // Get next index before we delete the node
        Index = Node.TypeIteratorNext( Local.m_BroadcastNodes, m_iType );

        // Delete the node
        Node.BCRemove  ( Local.m_BroadcastNodes, Node.m_Notifier->m_iChannel );
        Node.TypeRemove( Local.m_BroadcastNodes, m_iType );
        Local.m_BroadcastNodes.DeleteByHandle( Handle );
    }    

    // Remove node from all the link lists
    for( u16 Index = m_iChannel; Index != broadcast_node::Type_NULL ;   )
    {
        Handle.Set( Index );
        broadcast_node& Node = Local.m_BroadcastNodes( Handle );

        // Get next index before we delete the node
        Index = Node.BCIteratorNext( Local.m_BroadcastNodes, m_iChannel );

        // Delete the node
        Node.BCRemove  ( Local.m_BroadcastNodes, m_iChannel );
        Node.TypeRemove( Local.m_BroadcastNodes, Node.m_Receiver->m_iType );
        Local.m_BroadcastNodes.DeleteByHandle( Handle );
    }    

}

//=========================================================================

void xmsg_receiver::NotifySender( const xmsg_base& Msg )
{
    xhandle Handle;
    xscope_atomic( GetMessageLocal().m_CriticalSection );
    for( u16 Index = m_iType; Index != broadcast_node::Type_NULL ;   )
    {
        Handle.Set( Index );
        GetMessageLocal().m_BroadcastNodes( Handle ).m_Notifier->SendXMessage( Msg );
        Index = GetMessageLocal().m_BroadcastNodes( Handle ).TypeIteratorNext( GetMessageLocal().m_BroadcastNodes, m_iType );
    }
}

//=========================================================================

void xmsg_receiver::BroadcastMessage( const xmsg_base& Msg )
{
    xhandle Handle;
    xscope_atomic( GetMessageLocal().m_CriticalSection );
    message_local& Local = GetMessageLocal();
    
    for( u16 Index = m_iChannel; Index != broadcast_node::BC_NULL ;   )
    {
        Handle.Set( Index );
        Local.m_BroadcastNodes( Handle ).m_Receiver->SendXMessage( Msg );
        Index = Local.m_BroadcastNodes( Handle ).BCIteratorNext( Local.m_BroadcastNodes, m_iChannel );
    }
}

//=========================================================================

void xmsg_receiver::AddReceiver( xmsg_receiver& Receiver )
{
    xscope_atomic( GetMessageLocal().m_CriticalSection );
    message_local& Local = GetMessageLocal();

    //
    // Make sure that node doesn't already exits
    //
#ifdef X_DEBUG
    if( Local.m_BroadcastNodes.getCount() > 0 )
    {
        xhandle Handle;
        for( u16 Index = m_iChannel; Index != broadcast_node::BC_NULL ;   )
        {
            Handle.Set( Index );
            ASSERT( Local.m_BroadcastNodes( Handle ).m_Receiver != &Receiver );

            Index = Local.m_BroadcastNodes( Handle ).BCIteratorNext( Local.m_BroadcastNodes, m_iChannel );
        }
    }
#endif

    //
    // Add node into lists
    //
    xhandle hChannel;
    broadcast_node& Node = Local.m_BroadcastNodes.append( hChannel );
    ASSERT( hChannel.Get() < 0xffff );

    // Set basic info
    Node.m_Notifier   = this;
    Node.m_Receiver   = &Receiver; 

    // Append node into the link lists
    Node.BCAppend  ( Local.m_BroadcastNodes, m_iChannel );
    Node.TypeAppend( Local.m_BroadcastNodes, Receiver.m_iType );
}

//=========================================================================

void xmsg_receiver::RemoveReceiver( xmsg_receiver& Receiver )
{
    xscope_atomic( GetMessageLocal().m_CriticalSection );
    message_local& Local = GetMessageLocal();

    xhandle Handle;
    for( u16 Index = m_iChannel; Index != broadcast_node::BC_NULL ;   )
    {
        Handle.Set( Index );
        if( Local.m_BroadcastNodes( Handle ).m_Receiver == &Receiver )
        {
            Local.m_BroadcastNodes( Handle ).BCRemove  ( Local.m_BroadcastNodes, m_iChannel );
            Local.m_BroadcastNodes( Handle ).TypeRemove( Local.m_BroadcastNodes, Receiver.m_iType );
            Local.m_BroadcastNodes.DeleteByHandle( Handle );
            return;
        }

        Index = Local.m_BroadcastNodes( Handle ).BCIteratorNext( Local.m_BroadcastNodes, m_iChannel );
    }
}

//=========================================================================
//=========================================================================
//=========================================================================
// THREAD SAFE STUFF
//=========================================================================
//=========================================================================
//=========================================================================
#undef new
#undef delete 

void x_MsgInitInstance( void )
{
    //
    // Create the memory for this instance
    //
    {
        xbase_instance& Instance = x_GetXBaseInstance();
        Instance.m_pMessage     = new message_local;
        if( Instance.m_pMessage == NULL )
        {
            ASSERTS( 0, "Out of memory" );
        }
    }
}

void x_MsgKillInstance( void )
{
    //
    // Release the x_debug memory
    //
    xbase_instance& Instance = x_GetXBaseInstance();
    if(Instance.m_pMessage) 
    { 
        message_local* pMessage = (message_local*)Instance.m_pMessage; 
        delete pMessage;
        Instance.m_pMessage=NULL; 
    }

}
