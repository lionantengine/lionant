#include "../x_base.h"

//------------------------------------------------------------------------------
// VARS
//------------------------------------------------------------------------------
static xproperty_type* s_PropertyTypeTable[0xff] = {0};

const xproperty_type_decl<xprop_int>        g_PropInt;
const xproperty_type_decl<xprop_enum>       g_PropEnum;
const xproperty_type_decl<xprop_string>     g_PropString;
const xproperty_type_decl<xprop_filepath>   g_PropFilePath;
const xproperty_type_decl<xprop_guid>       g_PropGuid;
const xproperty_type_decl<xprop_vector3>    g_PropVector3;
const xproperty_type_decl<xprop_float>      g_PropFloat;
const xproperty_type_decl<xprop_bool>       g_PropBool;
const xproperty_type_decl<xprop_color>      g_PropColor;
const xproperty_type_decl<xprop_date>       g_PropDate;
const xproperty_type_decl<xprop_button>     g_PropButton;

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// MANAGER
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
struct reg_mgr_property
{
    xproperty*  m_pInterface;
    const char* m_pName;
};

static xsafe_array<reg_mgr_property,64>  s_PropertyMgr;
static s32                               s_PropertyMgrIndex=0;
    
//------------------------------------------------------------------------------

void x_PropertyMgrRegister( const char* pName, xproperty* pProperty )
{
    s_PropertyMgr[s_PropertyMgrIndex].m_pInterface = pProperty;
    s_PropertyMgr[s_PropertyMgrIndex].m_pName      = pName;
    s_PropertyMgrIndex++;
}

//------------------------------------------------------------------------------

s32 x_PropertyMgrGetCount( void )
{
    return s_PropertyMgrIndex;
}

//------------------------------------------------------------------------------

xproperty* x_PropertyMgrGetInterface( s32 i )
{
    return s_PropertyMgr[i].m_pInterface;
}

//------------------------------------------------------------------------------

const char* x_PropertyMgrGetName( s32 i )
{
    return s_PropertyMgr[i].m_pName;
}

//------------------------------------------------------------------------------

xproperty*  x_PropertyMgrGetInterface( const char* pName )
{
    for( s32 i=0; i<s_PropertyMgrIndex; i++ )
    {
        if( x_strcmp( s_PropertyMgr[i].m_pName, pName ) == 0 )
            return s_PropertyMgr[i].m_pInterface;
    }

    return NULL;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// TYPE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void xproperty_type::RegisterType( xproperty_type* Type ) 
{
    u8 UID = Type->GetUID();
    ASSERT( UID >= 0 );
    ASSERT( UID < 0xff );
    
    // If this hits then there is a duplicated uid
    ASSERT( s_PropertyTypeTable[ UID ] == NULL );
    s_PropertyTypeTable[ UID ] = Type;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// xproperty
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const xproperty_type* xproperty::getPropertyType ( const char* pPropertyName )
{
    // okay lets collect all the properties
    xproperty_enum Enum;
    onPropEnum( Enum );

    return Enum.GetPropertyType( pPropertyName );
}

//------------------------------------------------------------------------------

void xproperty::Save( const xstring& FileName, xtextfile::flags Flags )
{
    xtextfile TextFile;
    TextFile.OpenForWriting( FileName, Flags );
    Save( TextFile );
    TextFile.Close();
}

//------------------------------------------------------------------------------

xbool xproperty::Load( const xstring& FileName )
{
    xtextfile TextFile;
    TextFile.OpenForReading( FileName );
    xbool Ret = Load( TextFile );
    TextFile.Close();
    return Ret;
}

//------------------------------------------------------------------------------

xbool xproperty::onPropQuery( xproperty_query& Query ) const
{ 
    ASSERT( Query.IsUserGet() == TRUE ); 
    return (const_cast<xproperty*>(this))->onPropQuery( Query );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// QUERY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

xbool xproperty_query::Var( const char* pVar )
{
    s32               i;
    const char* const pName = &m_pName[m_iScope];

    // Go find the name
    for( i = 0; pName[i] == pVar[i]; i++ )
    {
        if( pName[i] == 0 ) return TRUE;
    }
    
    // handle extensions as end of string
    return pName[i] == '.' && pVar[i] == 0;
}

//------------------------------------------------------------------------------

xbool xproperty_query::Scope( const char* pScope )
{
    s32 i, j;

    // Go find the name
    for( j=0, i=m_iScope; pScope[j] != 0; i++, j++ )
    {
        if( m_pName[i] != pScope[j] ) 
            return FALSE;

        if( m_pName[i] == '[' )
        {
            ASSERT(pScope[j]=='[');
            ASSERT(pScope[j+1]==']');
            ASSERT(pScope[j+2]==0);
            
            i++;
            m_ArrayOffset=0;
            for( ; m_pName[i] != ']'; i++ )
            {
                ASSERT( m_pName[i] != 0 );
                ASSERT( m_pName[i] >= '0' );
                ASSERT( m_pName[i] <= '9' );
                m_ArrayOffset = m_ArrayOffset*10 + (m_pName[i] - '0');
            }

            // push the scope
            ASSERT( m_pName[i] == ']' );
            if( m_pName[i+1] == '\\' || m_pName[i+1] == '/')
            {
                m_iScope = i+2;
                ASSERT( m_pName[m_iScope] != 0 );
            }
            else
            {
                m_iScope = i+1;
                ASSERT( m_pName[m_iScope] == 0 );
            }
            return TRUE;
        }
    }

    // okay found the scope
    if( m_pName[i] == pScope[j] || m_pName[i] == '\\' || m_pName[i] == '/' )
    {
        // push the scope
        m_iScope = i;
        if( m_pName[i] == '\\' || m_pName[i] == '/' ) m_iScope++;
        return TRUE;
    }

    // some how we did not match
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xproperty_query::IsUserGet( void )
{
    return x_FlagIsOn( m_Flags, FLAGS_IS_GET);
}

//------------------------------------------------------------------------------

xproperty_type& xproperty_query::GetType( void )
{
    return *m_pType;
}

//------------------------------------------------------------------------------

s32 xproperty_query::GetArrayIndex( s32 MaxRange )
{
    ASSERT( m_ArrayOffset < MaxRange );
    ASSERT( m_ArrayOffset >= 0 );
    return m_ArrayOffset;
}

//------------------------------------------------------------------------------

xbool xproperty_query::Set( xproperty& Property, const char* pPropName, xproperty_type& TypeDecl, u32 Flags )
{
    m_iScope = 0;
    m_pType  = &TypeDecl;
    m_pName  = pPropName;
    m_Flags  = Flags;

    x_FlagOff( m_Flags, FLAGS_IS_GET );
    

    return Property.onPropQuery( *this );
}

//------------------------------------------------------------------------------

xbool xproperty_query::Get( const xproperty& Property, const char* pPropName, xproperty_type& TypeDecl )
{
    m_iScope = 0;
    m_pType  = &TypeDecl;
    m_pName  = pPropName;
    m_Flags  = FLAGS_IS_GET;

    return Property.onPropQuery( *this );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PROPERTY ENUM
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

xproperty_enum::xproperty_enum( void ) : 
    m_iRoot(-1),
    m_iStack(0)
{

}

//------------------------------------------------------------------------------

const xproperty_type* xproperty_enum::GetPropertyType( const char* pProperty ) const
{
    // Find our property index
    s32 iEntry = FindEntry( pProperty );
    if( iEntry == -1 ) return NULL;

    // and return the type
    const entry& Entry = GetEntry<xproperty_enum::entry>( iEntry );
    return Entry.m_pType; 
}

//------------------------------------------------------------------------------

s32 xproperty_enum::Add( s32 iScope )
{
    ASSERT( iScope >= -1 );

    s32     Count = m_lNodes.getCount();
    node&   Node  = m_lNodes.append();

    // Set scope/parent
    if( iScope != -1 ) 
    {
        // Some sanity check
        ASSERT( iScope < Count );
        Node.m_iScope = iScope;
    }

    // Set the scope
    Node.m_iScope = iScope;

    //
    // Insert our sevels in the last position of the link list
    // Because the order of properties is key and user define.
    // !! Note that the first node in the list is really the last
    //

    // do we have any node inserted yet?
    if( Node.m_iScope == -1 )
    {
        // Make sure that this is the last node
        if( m_iRoot == -1 )
        {
            m_iRoot                   = Count;        
            Node.m_iNext              = Count;
        }
        else
        {
            Node.m_iNext              = m_lNodes[m_iRoot].m_iNext;
            m_lNodes[m_iRoot].m_iNext = Count;
            m_iRoot                   = Count;        
        }
    }
    else
    {
        if( m_lNodes[iScope].m_iChild == -1 )
        {
            Node.m_iNext              = Count;
            m_lNodes[iScope].m_iChild = Count;
        }
        else
        {
            s32 iChild = m_lNodes[iScope].m_iChild;
            Node.m_iNext              = m_lNodes[iChild].m_iNext; // Points to the first node
            m_lNodes[iChild].m_iNext  = Count;                    // We are officially the last node now
            m_lNodes[iScope].m_iChild = Count;                    // Make the parent point to the last node again
        }
    }

    // Set the rest of the variables
    Node.m_iChild               = -1;

    return Count;
}

//------------------------------------------------------------------------------

s32 xproperty_enum::SetRoot( s32 iScope )
{
    ASSERT( iScope < m_lNodes.getCount() );
    ASSERT( iScope >= -1 );

    s32 OldRoot = m_iRoot;
        m_iRoot = iScope;
    return OldRoot;
}

//------------------------------------------------------------------------------

s32 xproperty_enum::GetRoot( void ) const
{
    return m_iRoot;
}

//------------------------------------------------------------------------------

s32 xproperty_enum::GetEntryCount( void ) const
{
    return m_lNodes.getCount();
}

//------------------------------------------------------------------------------

s32 xproperty_enum::GetEntryParent( s32 Index )
{
    return m_lNodes[ Index ].m_iScope;
}

//------------------------------------------------------------------------------

xproperty_type& xproperty_enum::GetEntryType( s32 Index )
{    
    entry& Entry = GetEntry<entry>( Index );
    return *((xproperty_type*)Entry.m_pType);
}

//------------------------------------------------------------------------------

void xproperty_enum::GetPropFullName( s32 Index, xstring& FullName ) const
{
    s32                 i;
    xsafe_array<s32,16> Path;
    s32                 Count=0;

    // First collect the path to the property
    for( i=Index; i!=-1; i = m_lNodes[i].m_iScope )
    {
        Path[Count++] = i;
    }

    // Now lets buid the string
    FullName.Clear();
    for( i=Count-1; i>=0; i-- )
    {
        const entry& Entry = GetEntry<entry>( Path[i] );
        FullName.AppendFormat( Entry.m_Name );
        if( i>0 ) FullName.AppendFormat( "/" );
    }
}

//------------------------------------------------------------------------------

s32 xproperty_enum::AddScope( const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    if( m_iStack == 0 ) 
    {
        return AddScope( -1, pPropertyName, pUserHelp );
    }

    return AddScope( m_ScopeStack[m_iStack-1], pPropertyName, pUserHelp );
}

//------------------------------------------------------------------------------

s32 xproperty_enum::AddScope( s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    return g_PropString.ScopeEnum( *this, iScope, pPropertyName, pUserHelp, Flags | PROP_FLAGS_READ_ONLY | PROP_FLAGS_FOLDER );
}

//------------------------------------------------------------------------------

s32 xproperty_enum::FindEntry( const char* pFullName ) const
{
    s32 iCurrOffset = 0;
    s32 iRoot       = GetRoot();
    s32 iNode       = iRoot;

    while( iRoot != -1 )
    {
        s32           i;
        const node&   Node  = m_lNodes[iNode];
        const entry&  Entry = m_Entries.Get<entry>( Node.m_iEntry );

        // compare the strings
        for( i=0; Entry.m_Name[i] && Entry.m_Name[i] == pFullName[iCurrOffset+i]; i++) 
        {
        }

        // Did we found the correct name?
        if( Entry.m_Name[i] == 0 )
        {
            // If this node is a folder lets go into it
            if( pFullName[iCurrOffset+i] == '\\' || pFullName[iCurrOffset+i] == '/' )
            {
                iCurrOffset = i+1;
                iNode       = Node.m_iChild;
                iRoot       = Node.m_iChild;
                continue;
            }
            // If is the last thing then we found the entry
            else if( pFullName[iCurrOffset+i] == 0 )
            {
                return iNode;                    
            }
            // If we found an extension then we assume we found the right node too
            else if( pFullName[iCurrOffset+i] == '.' )
            {
                return iNode;                    
            }
        }

        // Okay lets move to the next node
        iNode = Node.m_iNext;

        // are we done?
        if( iNode == iRoot ) break;
    }

    return -1;
}

//------------------------------------------------------------------------------

void xproperty_enum::PushScope( const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    if( m_iStack == 0 )
    {
        m_ScopeStack[ m_iStack ] = AddScope( pPropertyName, pUserHelp, Flags );
        m_iStack++;
    }
    else
    {
        m_ScopeStack[ m_iStack ] = AddScope( m_ScopeStack[ m_iStack-1 ], pPropertyName, pUserHelp, Flags );
        m_iStack++;
    }
}

//------------------------------------------------------------------------------

void xproperty_enum::PopScope( void )
{
    m_iStack--;
    ASSERT(m_iStack>=0);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PROPERTY
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void AddTypes( xtextfile& DataFile )
{
    g_PropInt.RegisterTextFileType      ( DataFile );
    g_PropEnum.RegisterTextFileType     ( DataFile );
    g_PropString.RegisterTextFileType   ( DataFile );
    g_PropFilePath.RegisterTextFileType ( DataFile );
    g_PropGuid.RegisterTextFileType     ( DataFile );
    g_PropVector3.RegisterTextFileType  ( DataFile );
    g_PropFloat.RegisterTextFileType    ( DataFile );
    g_PropBool.RegisterTextFileType     ( DataFile );
    g_PropColor.RegisterTextFileType    ( DataFile );
    g_PropDate.RegisterTextFileType     ( DataFile );
    g_PropButton.RegisterTextFileType   ( DataFile );
}

void xproperty::Save( xtextfile& DataFile, const xproperty_enum&  Enum )
{
    xstring         PropName;
    s32             i;
    s32             count=0;
    
    //
    // Setup all the user types
    //
    AddTypes( DataFile );
    
    //
    // Find which properties need to be saved
    //
    for( i=0; i<Enum.GetEntryCount(); i++ )
    {
        const xproperty_enum::entry& Entry = Enum.GetEntry<xproperty_enum::entry>( i );
        
        // Skip undesirable saves
        if( Entry.m_Flags &
           ( PROP_FLAGS_READ_ONLY | PROP_FLAGS_FOLDER ) &&
           !(Entry.m_Flags & PROP_FLAGS_FORCE_SAVE)) continue;
        
        count++;
    }
    
    char TypeString[2] = {0};
    DataFile.WriteRecord( "Properties", count );
    for( i=0; i<Enum.GetEntryCount(); i++ )
    {
        const xproperty_enum::entry& Entry = Enum.GetEntry<xproperty_enum::entry>( i );
        
        // Skip undesirable saves
        if( Entry.m_Flags &
           ( PROP_FLAGS_READ_ONLY | PROP_FLAGS_FOLDER ) &&
           !(Entry.m_Flags&PROP_FLAGS_FORCE_SAVE)) continue;
        
        // Get the type
        xproperty_type&   Type  = *((xproperty_type*)Entry.m_pType);
        
        TypeString[0] = Type.GetUID();
        Enum.GetPropFullName( i, PropName );
        DataFile.WriteField( "Type:<?>", Type.getType() );
        DataFile.WriteField( "Name:s", (const char*)PropName );
        Type.Serialize( *this, PropName, DataFile, TRUE );
        DataFile.WriteLine();
    }
}

//------------------------------------------------------------------------------

void xproperty::Save( xtextfile& DataFile )
{
    xproperty_enum    Enum;
    
    // Enumerate the properties that need saving
    onPropEnum( Enum );

    // Save all the properties in the enum
    Save( DataFile, Enum );
}

//------------------------------------------------------------------------------

xbool xproperty::Load( xtextfile& DataFile )
{
    if( !(DataFile.GetRecordName() == "Properties") )
        return FALSE;

    //
    // Add types to make sure we have their UIDS
    //
    AddTypes( DataFile );
    
    xstring         TypeUID;
    xstring         PropName;
    s32             i;

    for( i=0; i<DataFile.GetRecordCount(); i++ )
    {
        DataFile.ReadLine();

        s32 Index = DataFile.ReadField( "Type:<?>", &TypeUID );
        ASSERT( Index >= 0 );
        
        DataFile.ReadFieldXString( "Name:s", PropName );

        // Search for the right Type
		xproperty_type* pNext = s_PropertyTypeTable[ DataFile.getUserTypeUID( Index ) ];

        if( pNext == NULL )
            x_throw( "Invalid property type" );

        // Set the property
        pNext->Serialize( *this, PropName, DataFile, FALSE );
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// INT
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_int::GetUID( void ) const
{
    return 'I';
}

//------------------------------------------------------------------------------

const char* xprop_int::GetName( void ) const
{
    return "int";
}

//------------------------------------------------------------------------------

void xprop_int::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%d", m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_int::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    s32 Int;

    if( 1 != x_sscanf( pSourceData, "%d", &Int ) )
        return FALSE;

    if( Set( Property, pPropFullName, Int ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_int::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        s32 Int;
        if( Get( Property, pFullName, Int ) == FALSE ) 
            return FALSE;

        File.Write( Int );
    }
    else
    {
        s32 Int;
        File.Read( Int );
        if( Set( Property, pFullName, Int ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_int::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        s32 Int;
        if( Get( Property, pFullName, Int ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", Int );
    }
    else
    {
        s32 Int;

        File.ReadField( "Value:.INT", &Int );
        if( Set( Property, pFullName, Int ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_int::Get( const xproperty& Property, const char* pFullName, s32& Int, s32& Min, s32& Max )
{
    xproperty_query       Query;
    xprop_int             Var;

    // Set the int
    if( Query.Get( Property, pFullName, Var ) )
    {
        Min = Var.m_Min; 
        Max = Var.m_Max;
        Int = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_int::Get( const xproperty& Property, const char* pFullName, s32& Min, s32& Max )
{
    s32 Int;
    return Get( Property, pFullName, Int, Min, Max );
}

//------------------------------------------------------------------------------

xbool xprop_int::Get( const xproperty& Property, const char* pFullName, s32& Int )
{
    s32 Min, Max;
    return Get( Property, pFullName, Int, Min, Max );
}

//------------------------------------------------------------------------------

xbool xprop_int::Set( xproperty& Property, const char* pFullName, s32 Int, u32 Flags )
{
    xproperty_query       Query;
    xprop_int             Var;

    Var.m_Data = Int;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

s32 xprop_int::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, button_type ButtonType )
{
    s32 iNewScope;
    enum_int& Entry = E.Append<enum_int>( iNewScope, iScope );

    // Guessing....
    Entry.Set( pPropertyName, &g_PropInt, pUserHelp, Flags );
    Entry.m_ButtonType  = ButtonType;
    
    return iNewScope;
}

//------------------------------------------------------------------------------

xbool xprop_int::Query( xproperty_query& I, s32& Var, s32 Min, s32 Max )
{ 
    // cast it right
    xprop_int& Int = xprop_int::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) Int.m_Data = x_Max( Min, x_Min( Max, Var ));     
    else                Var        = x_Max( Min, x_Min( Max, Int.m_Data ));

    // Notify the ranges too
    Int.m_Min = Min; 
    Int.m_Max = Max; 

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

s32 xprop_int::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_int& Vec = xprop_int::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_int::SetUserValue( xproperty_query& I, const s32 Value )
{
    // cast it right
    xprop_int& Vec = xprop_int::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ENUM
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_enum::GetUID( void ) const
{
    return 'E';
}

//------------------------------------------------------------------------------

const char* xprop_enum::GetName( void ) const
{
    return "enum";
}

//------------------------------------------------------------------------------

void xprop_enum::SetString( xstring& Arguments ) const
{
    Arguments.Format( "\"%s\"", (const char*)m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_enum::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    char String[256];

    ASSERT( x_strlen(pSourceData) < 256 );

    if( 1 != x_sscanf( pSourceData, "%s", &String ) )
        return FALSE;

    if( Set( Property, pPropFullName, String ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_enum::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.Write( (const xstring&)S );
    }
    else
    {
        xstring S;
        File.Read( S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_enum::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", (const char*)S );
    }
    else
    {
        xstring S;
        File.ReadFieldXString( "Value:.ENUM", S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_enum::Get( const xproperty& Property, const char* pFullName, xstring& EnumName, const entry*& pEntry )
{
    xprop_enum          Var;
    xproperty_query     Query;

    Var.m_Data = EnumName;

    if( Query.Get( Property, pFullName, Var ) )
    {
        pEntry   = Var.m_pEntry;
        EnumName = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_enum::Set( xproperty& Property, const char* pFullName, const xstring& EnumName, u32 Flags )
{
    xprop_enum       Var;
    xproperty_query           Query;

    Var.m_Data = EnumName;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_enum::Set( xproperty& Property, const char* pFullName, const char* pEnumName, u32 Flags )
{
    xstring                 String;
    String.Copy( pEnumName );
    return Set( Property, pFullName, String, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_enum::Set( xproperty& Property, const char* pFullName, const entry& Entry, u32 Flags )
{
    return Set( Property, pFullName, Entry.m_EnumName, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_enum::Get( const xproperty& Property, const char* pFullName, xstring& EnumName )
{
    const entry* pEntry;
    return Get(Property, pFullName, EnumName, pEntry );
}

//------------------------------------------------------------------------------

xbool xprop_enum::Get( const xproperty& Property, const char* pFullName, const entry*& pEntry )
{
    xstring EnumName; 
    return Get(Property, pFullName, EnumName, pEntry );
}

//------------------------------------------------------------------------------

void xprop_enum::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    s32 iNewScope;
    enum_enum& Entry = E.Append<enum_enum>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropEnum, pUserHelp, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_enum::Query_S32( xproperty_query& I, s32& Var, const entry* pList )
{ 
    // cast it right
    xprop_enum& Enum = xprop_enum::SafeCast( I.GetType() ); 

    // Set a pointer to the entry list
    Enum.m_pEntry = pList;

    if( I.IsUserGet() )
    {
        for( s32 i=0; pList[i].m_EnumID != ENTRY_ENUM_END; i++ )
        {
            if( pList[i].m_EnumID == Var )
            {
                Enum.m_Data = pList[i].m_EnumName;
                return TRUE;
            }
        }
    }
    else
    {
        for( s32 i=0; pList[i].m_EnumID != ENTRY_ENUM_END; i++ )
        {
            if( Enum.m_Data == pList[i].m_EnumName )
            {
                Var = pList[i].m_EnumID;
                return TRUE;
            }
        }
    }

    // Something bad happen
    return FALSE;
}

//------------------------------------------------------------------------------

s32 xprop_enum::GetUserValue( xproperty_query& I, const entry* pList )
{
    ASSERT(pList);

    // cast it right
    xprop_enum& Vec = xprop_enum::SafeCast( I.GetType() ); 
    for( s32 i=0; pList[i].m_EnumID != ENTRY_ENUM_END; i++ )
    {
        if( Vec.m_Data == pList[i].m_EnumName )
        {
            return pList[i].m_EnumID;
        }
    }

    ASSERT(0);
    return -1;
}

//------------------------------------------------------------------------------

void xprop_enum::SetUserValue( xproperty_query& I, s32 Var, const entry* pList )
{
    // cast it right
    xprop_enum& Vec = xprop_enum::SafeCast( I.GetType() ); 

    for( s32 i=0; pList[i].m_EnumID != ENTRY_ENUM_END; i++ )
    {
        if( pList[i].m_EnumID == Var )
        {
            Vec.m_Data = pList[i].m_EnumName;
            break;
        }
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// STRING
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_string::GetUID( void ) const
{
    return 'S';
}

//------------------------------------------------------------------------------

const char* xprop_string::GetName( void ) const
{
    return "string";
}

//------------------------------------------------------------------------------

void xprop_string::SetString( xstring& Arguments ) const
{
    Arguments.Format( "\"%s\"", (const char*)m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_string::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    char String[256];

    ASSERT( x_strlen(pSourceData) < 256 );

    if( 1 != x_sscanf( pSourceData, "%s", &String ) )
        return FALSE;

    if( Set( Property, pPropFullName, String ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.Write( S );
    }
    else
    {
        xstring S;
        File.Read( S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", (const char*)S );
    }
    else
    {
        xstring S;
        File.ReadFieldXString( "Value:.STRING", S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Get( const xproperty& Property, const char* pFullName, xstring& StringVar )
{
    xprop_string     Var;
    xproperty_query           Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        StringVar = Var.m_Data;
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Get( const xproperty& Property, const char* pFullName, char* pVar, s32 SizeOfCharBuffer )
{
    xprop_string     Var;
    xproperty_query           Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        x_strcpy( pVar, SizeOfCharBuffer, Var.m_Data );
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Set( xproperty& Property, const char* pFullName, const xstring& VarString, u32 Flags )
{
    xprop_string    Var;
    xproperty_query          Query;

    Var.m_Data = VarString;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_string::Set( xproperty& Property, const char* pFullName, const char* pVar, u32 Flags )
{
    xprop_string    Var;
    xproperty_query          Query;

    Var.m_Data.Copy(pVar);

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

s32 xprop_string::ScopeEnum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, display_type Type )
{
    s32 iNewScope;
    enum_string& Entry = E.Append<enum_string>( iNewScope, iScope );

    Entry.m_Display         = Type;
    Entry.Set( pPropertyName, &g_PropString, pUserHelp, Flags );
    return iNewScope;
}

//------------------------------------------------------------------------------

void xprop_string::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, display_type Type )
{
    ScopeEnum(E, iScope, pPropertyName, pUserHelp, Flags, Type );
}

//------------------------------------------------------------------------------

xbool xprop_string::Query( xproperty_query& I, xstring& Var )
{ 
    // cast it right
    xprop_string& String = xprop_string::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() )
    {
        String.m_Data = Var;     
    }
    else
    {
        Var = String.m_Data;
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_string::Query( xproperty_query& I, char* pVar, s32 SizeOfCharBuffer )
{ 
    // cast it right
    xprop_string& String = xprop_string::SafeCast( I.GetType() ); 
    ASSERT(pVar);
    ASSERT(SizeOfCharBuffer>1);

    // Handle the property
    if( I.IsUserGet() )
    {
        String.m_Data.Copy(pVar);
    }
    else
    {
        x_strcpy( pVar, SizeOfCharBuffer, String.m_Data );     
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_string::QueryROnly( xproperty_query& I, const char* pVar )
{
    // cast it right
    xprop_string& String = xprop_string::SafeCast( I.GetType() ); 

    // Handle the property
    String.m_Data.Copy(pVar);

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xstring xprop_string::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_string& Vec = xprop_string::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_string::SetUserValue( xproperty_query& I, const xstring& Value )
{
    // cast it right
    xprop_string& Vec = xprop_string::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// FILEPATH
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_filepath::GetUID( void ) const
{
    return 'L';
}

//------------------------------------------------------------------------------

const char* xprop_filepath::GetName( void ) const
{
    return "filepath";
}

//------------------------------------------------------------------------------

void xprop_filepath::SetString( xstring& Arguments ) const
{
    Arguments.Format( "\"%s\"", (const char*)m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_filepath::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    char String[xfile::MAX_PATH];

    ASSERT( x_strlen(pSourceData) < sizeof(String) );

    if( 1 != x_sscanf( pSourceData, "%s", &String ) )
        return FALSE;

    if( Set( Property, pPropFullName, String ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.Write( S );
    }
    else
    {
        xstring S;
        File.Read( S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring S;
        if( Get( Property, pFullName, S ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", (const char*)S );
    }
    else
    {
        xstring S;
        File.ReadFieldXString( "Value:.FILEPATH", S );

        if( Set( Property, pFullName, S ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Get( const xproperty& Property, const char* pFullName, xstring& StringVar )
{
    xprop_filepath     Var;
    xproperty_query           Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        StringVar = Var.m_Data;
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Get( const xproperty& Property, const char* pFullName, char* pVar, s32 SizeOfCharBuffer )
{
    xprop_filepath     Var;
    xproperty_query           Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        x_strcpy( pVar, SizeOfCharBuffer, Var.m_Data );
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Set( xproperty& Property, const char* pFullName, const xstring& VarString, u32 Flags )
{
    xprop_filepath    Var;
    xproperty_query          Query;

    Var.m_Data = VarString;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Set( xproperty& Property, const char* pFullName, const char* pVar, u32 Flags )
{
    xprop_filepath    Var;
    xproperty_query          Query;

    Var.m_Data.Copy(pVar);

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

s32 xprop_filepath::ScopeEnum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, const char* pFileType, const char* pDefaultPath, display_type Type, u32 Flags )
{
    s32 iNewScope;
    enum_filepath& Entry = E.Append<enum_filepath>( iNewScope, iScope );

    Entry.m_Display         = Type;
    Entry.m_pFileType       = pFileType;
    Entry.m_pDefaultPath    = pDefaultPath;
    Entry.Set( pPropertyName, &g_PropFilePath, pUserHelp, Flags );
    return iNewScope;
}

//------------------------------------------------------------------------------

void xprop_filepath::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, const char* pFileType, const char* pDefaultPath, display_type Type, u32 Flags )
{
    ScopeEnum(E, iScope, pPropertyName, pUserHelp, pFileType, pDefaultPath, Type, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Query( xproperty_query& I, xstring& Var )
{ 
    // cast it right
    xprop_filepath& String = xprop_filepath::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() )
    {
        String.m_Data = Var;     
    }
    else
    {
        Var = String.m_Data;
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::Query( xproperty_query& I, char* pVar, s32 SizeOfCharBuffer )
{ 
    // cast it right
    xprop_filepath& String = xprop_filepath::SafeCast( I.GetType() ); 
    ASSERT(pVar);
    ASSERT(SizeOfCharBuffer>1);

    // Handle the property
    if( I.IsUserGet() )
    {
        String.m_Data.Copy(pVar);
    }
    else
    {
        x_strcpy( pVar, SizeOfCharBuffer, String.m_Data );     
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_filepath::QueryROnly( xproperty_query& I, const char* pVar )
{
    // cast it right
    xprop_filepath& String = xprop_filepath::SafeCast( I.GetType() ); 

    // Handle the property
    String.m_Data.Copy(pVar);

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xstring xprop_filepath::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_filepath& Vec = xprop_filepath::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_filepath::SetUserValue( xproperty_query& I, const xstring& Value )
{
    // cast it right
    xprop_filepath& Vec = xprop_filepath::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// GUID
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_guid::GetUID( void ) const
{
    return 'G';
}

//------------------------------------------------------------------------------

const char* xprop_guid::GetName( void ) const
{
    return "guid";
}

//------------------------------------------------------------------------------

void xprop_guid::SetString( xstring& Arguments ) const
{
    Arguments = m_Data.GetHexString();
}

//------------------------------------------------------------------------------

xbool xprop_guid::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    char String[256];

    ASSERT( x_strlen(pSourceData) < 256 );

    if( 1 != x_sscanf( pSourceData, "%s", &String ) )
        return FALSE;

    xguid Guid;

    Guid.SetFromHexString( String );

    if( Set( Property, pPropFullName, Guid ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_guid::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xguid   Guid;
        if( Get( Property, pFullName, Guid ) == FALSE ) 
            return FALSE;

        File.Write( Guid );
    }
    else
    {
        xguid   Guid;
        File.Read( Guid );

        if( Set( Property, pFullName, Guid ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_guid::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xguid   Guid;
        if( Get( Property, pFullName, Guid ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", (u64)Guid );
    }
    else
    {
        xguid   Guid;
        File.ReadField( "Value:.GUID", &Guid );

        if( Set( Property, pFullName, Guid ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_guid::Get( const xproperty& Property, const char* pFullName, xguid& Guid )
{
    xproperty_query       Query;
    xprop_guid   Var;

    if( Query.Get( Property, pFullName, Var ) )
    {
        Guid = Var.m_Data;
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_guid::Set( xproperty& Property, const char* pFullName, xguid Guid, u32 Flags )
{
    xproperty_query       Query;
    xprop_guid   Var;

    Var.m_Data = Guid;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

void xprop_guid::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    s32 iNewScope;
    enum_guid& Entry = E.Append<enum_guid>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropGuid, pUserHelp, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_guid::Query( xproperty_query& I, xguid& Var )
{ 
    // cast it right
    xprop_guid& Guid = xprop_guid::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) Guid.m_Data = Var;     
    else                Var         = Guid.m_Data;

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xguid xprop_guid::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_guid& Vec = xprop_guid::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_guid::SetUserValue( xproperty_query& I, const xguid Value )
{
    // cast it right
    xprop_guid& Vec = xprop_guid::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Vector3
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
xvector3d xprop_vector3::VECTOR_MIN(-F32_MAX, -F32_MAX, -F32_MAX);
xvector3d xprop_vector3::VECTOR_MAX(F32_MAX, F32_MAX, F32_MAX);

//------------------------------------------------------------------------------

u8 xprop_vector3::GetUID( void ) const
{
    return 'V';
}

//------------------------------------------------------------------------------

const char* xprop_vector3::GetName( void ) const
{
    return "vector3";
}

//------------------------------------------------------------------------------

void xprop_vector3::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%f %f %f", m_Data.m_X, m_Data.m_Y, m_Data.m_Z );
}

//------------------------------------------------------------------------------

xbool xprop_vector3::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    f32 X, Y, Z;

    if( 3 != x_sscanf( pSourceData, "%lf%lf%lf", &X, &Y, &Z ) )
        return FALSE;

    if( Set( Property, pPropFullName, xvector3d(X,Y,Z) ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xvector3d Vec;
        if( Get( Property, pFullName, Vec ) == FALSE ) 
            return FALSE;

        File.Write( Vec.m_X );
        File.Write( Vec.m_Y );
        File.Write( Vec.m_Z );
    }
    else
    {
        xvector3d Vec;
        File.Read( Vec.m_X );
        File.Read( Vec.m_Y );
        File.Read( Vec.m_Z );

        if( Set( Property, pFullName, Vec ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xvector3d Vec;
        if( Get( Property, pFullName, Vec ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", Vec.m_X, Vec.m_Y, Vec.m_Z );
    }
    else
    {
        xvector3d Vec;

        File.ReadField( "Value:.V3", &Vec.m_X, &Vec.m_Y, &Vec.m_Z );
        if( Set( Property, pFullName, Vec ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Get( const xproperty& Property, const char* pFullName, xvector3d& Vector, xvector3d& Min, xvector3d& Max )
{
    xprop_vector3    Var;
    xproperty_query           Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        Vector = Var.m_Data;
        Min    = Var.m_Min; 
        Max    = Var.m_Max;
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Get( const xproperty& Property, const char* pFullName, f32& Data, f32& Min, f32& Max )
{
    xprop_vector3    Var;
    xproperty_query           Query;
    s32                       Length = x_strlen(pFullName);

    // We need an extension in order to collect the individual components
    if( pFullName[Length-3] == '.' )
    {
        x_LogError( "xProperty", "An unsupported extension property type vector3 was used" );
    }

    if( Query.Get( Property, pFullName, Var ) )
    {
        switch( pFullName[Length-2] )
        {
        case 'X':
            Data = Var.m_Data.m_X;
            Min  = Var.m_Min.m_X;
            Max  = Var.m_Max.m_X;
            break;
        case 'Y':
            Data = Var.m_Data.m_Y;
            Min  = Var.m_Min.m_Y;
            Max  = Var.m_Max.m_Y;
            break;
        case 'Z':
            Data = Var.m_Data.m_Z;
            Min  = Var.m_Min.m_Z;
            Max  = Var.m_Max.m_Z;
            break;
        default :
            x_LogError( "xProperty", "A extension for property type vector3 was used that is not X,Y,Z" );
            return FALSE;
        }

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Get( const xproperty& Property, const char* pFullName, f32& Data )
{
    xprop_vector3    Var;
    xproperty_query           Query;
    s32                       Length = x_strlen(pFullName);

    // We need an extension in order to collect the individual components
    if( pFullName[Length-3] == '.' )
    {
        x_LogError( "xProperty", "An unsupported extension property type vector3 was used" );
    }

    if( Query.Get( Property, pFullName, Var ) )
    {
        if( pFullName[Length-2] == '.' )
        {
            switch( pFullName[Length-1] )
            {
            case 'X':
                Data = Var.m_Data.m_X;
                break;
            case 'Y':
                Data = Var.m_Data.m_Y;
                break;
            case 'Z':
                Data = Var.m_Data.m_Z;
                break;
            default :
                {
                    x_LogError( "xProperty", "A extension for property type vector3 was used that is not X,Y,Z" );
                    return FALSE;
                }
            }
        }

        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Set( xproperty& Property, const char* pFullName, const xvector3d& Vector, u32 Flags )
{
    xproperty_query           Query;
    xprop_vector3    Var;

    Var.m_Data = Vector;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Set( xproperty& Property, const char* pFullName, f32 Data, u32 Flags )
{
    xproperty_query           Query;
    xprop_vector3    Var;
    xvector3d                 Vector;
    s32                       Length = x_strlen(pFullName);

    // We need an extension in order to collect the individual components
    if( pFullName[Length-3] == '.' )
    {
        x_LogError( "xProperty", "An unsupported extension property type vector3 was used" );
    }

    // First get the full vector
    if( Get( Property, pFullName, Vector ) == FALSE )
    {
        return FALSE;
    }

    // Base on the extension modify the vector
    if( pFullName[Length-2] == '.' )
    {
        switch( pFullName[Length-1] )
        {
        case 'X':
            Vector.m_X = Data;
            break;
        case 'Y':
            Vector.m_Y = Data;
            break;
        case 'Z':
            Vector.m_Z = Data;
            break;
        default :
            {
                x_LogError( "xProperty", "A extension for property type vector3 was used that is not X,Y,Z" );
                return FALSE;
            }
        }
    }

    // Now we can do a normal set
    Var.m_Data = Vector;
    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Get( const xproperty& Property, const char* pFullName, xvector3d& Min, xvector3d& Max )
{
    xvector3d Vector;
    return Get( Property, pFullName, Vector, Min, Max );
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Get( const xproperty& Property, const char* pFullName, xvector3d& Vector )
{
    xvector3d Min, Max;
    return Get( Property, pFullName, Vector, Min, Max );
}

//------------------------------------------------------------------------------

void xprop_vector3::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, button_type ButtonType )
{
    s32 iNewScope;
    enum_vector3& Entry = E.Append<enum_vector3>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropVector3, pUserHelp, Flags );
    Entry.m_ButtonType  = ButtonType;
}

//------------------------------------------------------------------------------

xbool xprop_vector3::Query( xproperty_query& I, xvector3d& Var, xvector3d& Min, xvector3d& Max )
{ 
    // cast it right
    xprop_vector3& Vec = xprop_vector3::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) 
    {
        Vec.m_Data = Var;
        Vec.m_Data = (Vec.m_Data).GetMin( Max );
        Vec.m_Data = (Vec.m_Data).GetMax( Min );
    }
    else
    {
        Var = Vec.m_Data;
        Var = Var.GetMin( Max );
        Var = Var.GetMax( Min );
    }

    // Notify the ranges too
    Vec.m_Min = Min; 
    Vec.m_Max = Max; 

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xvector3d xprop_vector3::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_vector3& Vec = xprop_vector3::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_vector3::SetUserValue( xproperty_query& I, const xvector3d& Value )
{
    // cast it right
    xprop_vector3& Vec = xprop_vector3::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Float
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_float::GetUID( void ) const
{
    return 'F';
}

//------------------------------------------------------------------------------

const char* xprop_float::GetName( void ) const
{
    return "float";
}

//------------------------------------------------------------------------------

void xprop_float::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%f", m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_float::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    f32 X;

    if( 1 != x_sscanf( pSourceData, "%lf", &X ) )
        return FALSE;

    if( Set( Property, pPropFullName, X ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_float::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        f32 Float;
        if( Get( Property, pFullName, Float ) == FALSE ) 
            return FALSE;

        File.Write( Float );
    }
    else
    {
        f32 Float;
        File.Read( Float );
        if( Set( Property, pFullName, Float ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_float::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        f32 Float;
        if( Get( Property, pFullName, Float ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", Float );
    }
    else
    {
        f32 Float;

        File.ReadField( "Value:.FLOAT", &Float );
        if( Set( Property, pFullName, Float ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_float::Get( const xproperty& Property, const char* pFullName, f32& Float, f32& Min, f32& Max )
{
    xprop_float Var;
    xproperty_query      Query;

    if( Query.Get( Property, pFullName, Var ) )
    {
        Min   = Var.m_Min; 
        Max   = Var.m_Max;
        Float = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_float::Set( xproperty& Property, const char* pFullName, f32 Float, u32 Flags )
{
    xproperty_query       Query;
    xprop_float  Var;

    Var.m_Data = Float;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_float::Get( const xproperty& Property, const char* pFullName, f32& Min, f32& Max )
{
    f32 Float;
    return Get( Property, pFullName, Float, Min, Max );
}

//------------------------------------------------------------------------------

xbool xprop_float::Get( xproperty& Property, const char* pFullName, f32& Float )
{
    f32 Min, Max;
    return Get( Property, pFullName, Float, Min, Max );
}

//------------------------------------------------------------------------------

void xprop_float::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, button_type ButtonType )
{
    s32 iNewScope;
    enum_float& Entry = E.Append<enum_float>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropFloat, pUserHelp, Flags );
    Entry.m_ButtonType  = ButtonType;
}

//------------------------------------------------------------------------------

xbool xprop_float::Query( xproperty_query& I, f32& Var, f32 Min, f32 Max )
{ 
    // cast it right
    xprop_float& Float = xprop_float::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) Float.m_Data = x_Max( Min, x_Min( Max, Var ));     
    else                Var          = x_Max( Min, x_Min( Max, Float.m_Data ));

    // Notify the ranges too
    Float.m_Min = Min; 
    Float.m_Max = Max; 

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

f32 xprop_float::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_float& Vec = xprop_float::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_float::SetUserValue( xproperty_query& I, const f32 Value )
{
    // cast it right
    xprop_float& Vec = xprop_float::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Bool
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_bool::GetUID( void ) const
{
    return 'b';
}

//------------------------------------------------------------------------------

const char* xprop_bool::GetName( void ) const
{
    return "flags";
}

//------------------------------------------------------------------------------

void xprop_bool::SetString( xstring& Arguments ) const
{
    Arguments.Format( "0x%H", m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_bool::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    s32 X;

    if( 1 != x_sscanf( pSourceData, "%x", &X ) )
        return FALSE;

    if( Set( Property, pPropFullName, X ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        u32 Flags;
        if( Get( Property, pFullName, Flags ) == FALSE ) 
            return FALSE;

        File.Write( Flags );
    }
    else
    {
        u32 Flags;
        File.Read( Flags );
        if( Set( Property, pFullName, Flags ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        u32 Flags;
        if( Get( Property, pFullName, Flags ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", Flags );
    }
    else
    {
        u32 Flags;

        File.ReadField( "Value:.BOOL", &Flags );
        if( Set( Property, pFullName, Flags ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::GetFlags( xproperty& Property, const char* pFullName, u32& Flags )
{
    xprop_bool Var;
    xproperty_query      Query;

    Var.m_BitNumber = -1;

    if( Query.Get( Property, pFullName, Var ) )
    {
        Flags = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::Get( const xproperty& Property, const char* pFullName, xbool& Bool )
{
    xprop_bool Var;
    xproperty_query      Query;

    Var.m_BitNumber = -1;

    if( Query.Get( Property, pFullName, Var ) )
    {
        Bool = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::Set( xproperty& Property, const char* pFullName, u32 Flags, u32 SystemFlags  )
{
    xproperty_query       Query;
    xprop_bool  Var;

    Var.m_Data  = Flags;
    Var.m_BitNumber = -1;

    return Query.Set( Property, pFullName, Var, SystemFlags );
}

//------------------------------------------------------------------------------

xbool xprop_bool::Set( xproperty& Property, const char* pFullName, s32 BitNumber, xbool OnOff, u32 SystemFlags  )
{
    xproperty_query       Query;
    xprop_bool  Var;

    Var.m_Data      = OnOff;
    Var.m_BitNumber = BitNumber;

    return Query.Set( Property, pFullName, Var, SystemFlags );
}

//------------------------------------------------------------------------------

void xprop_bool::Enum( xproperty_enum&  E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags, button_type ButtonType, const char* pFlagDescritionString )
{
    s32 iNewScope;
    enum_bool& Entry = E.Append<enum_bool>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropBool, pUserHelp, Flags );
    Entry.m_ButtonType  = ButtonType;
    Entry.m_pFlagsDesc  = pFlagDescritionString;
}

/*
//------------------------------------------------------------------------------

xbool xprop_bool::Query( xproperty_query& I, xbool& Var )
{ 
    // cast it right
    xprop_bool& Flags = xprop_bool::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) Flags.m_Data = Var;     
    else                Var          = Flags.m_Data;

    // allow us to put it in an if statement
    return TRUE;
}
*/
//------------------------------------------------------------------------------

xbool xprop_bool::Query( xproperty_query& I, u32& Var, u32 Mask  )
{ 
    // cast it right
    xprop_bool& Flags = xprop_bool::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) 
    {
        Flags.m_Data = Var;     
    }
    else 
    {
        if( Flags.m_BitNumber <= -1 )
        {
            Var = Mask & Flags.m_Data;     
        }
        else if( Flags.m_Data )
        {
            ASSERT( (Mask>>Flags.m_BitNumber) );
            x_FlagOn( Var, 1<<Flags.m_BitNumber );
        }
        else
        {
            ASSERT( (Mask>>Flags.m_BitNumber) );
            x_FlagOff( Var, 1<<Flags.m_BitNumber );
        }
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_bool::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_bool& Vec = xprop_bool::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_bool::SetUserValue( xproperty_query& I, const xbool Value )
{
    // cast it right
    xprop_bool& Vec = xprop_bool::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------

u32 xprop_bool::GetUserValueFlags( xproperty_query& I )
{
    // cast it right
    xprop_bool& Vec = xprop_bool::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_bool::SetUserValueFlags( xproperty_query& I, const u32 Value )
{
    // cast it right
    xprop_bool& Vec = xprop_bool::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Color
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_color::GetUID( void ) const
{
    return 'C';
}

//------------------------------------------------------------------------------

const char* xprop_color::GetName( void ) const
{
    return "color";
}

//------------------------------------------------------------------------------

void xprop_color::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%d %d %d %d", m_Data.m_R, m_Data.m_G, m_Data.m_B, m_Data.m_A );
}

//------------------------------------------------------------------------------

xbool xprop_color::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    s32 R,G,B,A;

    if( 4 != x_sscanf( pSourceData, "%d%d%d%d", &R, &G, &B, &A ) )
        return FALSE;

    if( Set( Property, pPropFullName, xcolor( R,G,B,A ) ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_color::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xcolor C;
        if( Get( Property, pFullName, C ) == FALSE ) 
            return FALSE;

        File.Write( C.m_R );
        File.Write( C.m_G );
        File.Write( C.m_B );
        File.Write( C.m_A );
    }
    else
    {
        xcolor C;
        File.Read( C.m_R );
        File.Read( C.m_G );
        File.Read( C.m_B );
        File.Read( C.m_A );

        if( Set( Property, pFullName, C ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_color::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xcolor C;
        if( Get( Property, pFullName, C ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", C.m_R, C.m_G, C.m_B, C.m_A );
    }
    else
    {
        xcolor C;

        File.ReadField( "Value:.COLOR", &C.m_R, &C.m_G, &C.m_B, &C.m_A );
        if( Set( Property, pFullName, C ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_color::Get( const xproperty& Property, const char* pFullName, xcolor& Color )
{
    xprop_color      Var;
    xproperty_query           Query;


    if( Query.Get( Property, pFullName, Var ) )
    {
        Color = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_color::Set( xproperty& Property, const char* pFullName, xcolor Color, u32 Flags )
{
    xproperty_query           Query;
    xprop_color      Var;

    Var.m_Data = Color;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

void xprop_color::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    s32 iNewScope;
    enum_color& Entry = E.Append<enum_color>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropColor, pUserHelp, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_color::Query( xproperty_query& I, xcolor& Var )
{ 
    // cast it right
    xprop_color& Vec = xprop_color::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) 
    {
        Vec.m_Data = Var;
    }
    else
    {
        Var = Vec.m_Data;
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xcolor xprop_color::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_color& Vec = xprop_color::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_color::SetUserValue( xproperty_query& I, const xcolor Value )
{
    // cast it right
    xprop_color& Vec = xprop_color::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Date
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_date::GetUID( void ) const
{
    return 'D';
}

//------------------------------------------------------------------------------

const char* xprop_date::GetName( void ) const
{
    return "date";
}

//------------------------------------------------------------------------------

void xprop_date::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%H", m_Data.GetDate() );
}

//------------------------------------------------------------------------------

xbool xprop_date::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    s32 Date;

    if( 1 != x_sscanf( pSourceData, "%x", &Date ) )
        return FALSE;

    xdate TheDate;
    TheDate.SetDate( Date );

    if( Set( Property, pPropFullName, TheDate ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_date::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xdate Date;
        if( Get( Property, pFullName, Date ) == FALSE ) 
            return FALSE;

        File.Write( Date.GetDate() );
    }
    else
    {
        s32 D;
        File.Read( D );

        xdate Date;
        Date.SetDate( D );
        if( Set( Property, pFullName, Date ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_date::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xdate Date;
        if( Get( Property, pFullName, Date ) == FALSE ) 
            return FALSE;

        s32 DD, MM, YY;
        
        Date.GetDate( MM, DD, YY );
        File.WriteField( "Value:<Type>", MM, DD, YY );
    }
    else
    {
        s32 MM, DD, YY;

        File.ReadField( "Value:.DATE", &MM, &DD, &YY );

        xdate Date;
        
        Date.SetDate( MM, DD, YY);
        if( Set( Property, pFullName, Date ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_date::Get( const xproperty& Property, const char* pFullName, xdate& Date )
{
    xprop_date       Var;
    xproperty_query           Query;


    if( Query.Get( Property, pFullName, Var ) )
    {
        Date = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_date::Set( xproperty& Property, const char* pFullName, xdate& Date, u32 Flags )
{
    xproperty_query           Query;
    xprop_date      Var;

    Var.m_Data = Date;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

void xprop_date::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    s32 iNewScope;
    enum_date& Entry = E.Append<enum_date>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropDate, pUserHelp, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_date::Query( xproperty_query& I, xdate& Var )
{ 
    // cast it right
    xprop_date& Vec = xprop_date::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) 
    {
        Vec.m_Data = Var;
    }
    else
    {
        Var = Vec.m_Data;
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xdate xprop_date::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_date& Vec = xprop_date::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_date::SetUserValue( xproperty_query& I, const xdate Value )
{
    // cast it right
    xprop_date& Vec = xprop_date::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Button
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

u8 xprop_button::GetUID( void ) const
{
    return 'B';
}

//------------------------------------------------------------------------------

const char* xprop_button::GetName( void ) const
{
    return "button";
}

//------------------------------------------------------------------------------

void xprop_button::SetString( xstring& Arguments ) const
{
    Arguments.Format( "%s", (const char*)m_Data );
}

//------------------------------------------------------------------------------

xbool xprop_button::SetPropertyFromString( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const
{
    char String[256];

    ASSERT( x_strlen(pSourceData) < 256 );

    if( 1 != x_sscanf( pSourceData, "%s", &String ) )
        return FALSE;

    xstring Temp;
    Temp.Copy(String );

    if( Set( Property, pPropFullName, Temp ) == FALSE ) 
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_button::Serialize( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring String;
        if( Get( Property, pFullName, String ) == FALSE ) 
            return FALSE;

        File.Write( String );
    }
    else
    {
        xstring String;
        File.Read( String );

        if( Set( Property, pFullName, String ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_button::Serialize( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )
{
    if( bSave )
    {
        xstring String;
        if( Get( Property, pFullName, String ) == FALSE ) 
            return FALSE;

        File.WriteField( "Value:<Type>", (const char*)String );
    }
    else
    {
        xstring String;

        File.ReadFieldXString( "Value:.BUTTON", String );
        if( Set( Property, pFullName, String ) == FALSE ) 
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------

xbool xprop_button::Get( const xproperty& Property, const char* pFullName, xstring& Value )
{
    xprop_button       Var;
    xproperty_query           Query;


    if( Query.Get( Property, pFullName, Var ) )
    {
        Value = Var.m_Data;
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------

xbool xprop_button::Set( xproperty& Property, const char* pFullName, xstring& Value, u32 Flags )
{
    xproperty_query            Query;
    xprop_button      Var;

    Var.m_Data = Value;

    return Query.Set( Property, pFullName, Var, Flags );
}

//------------------------------------------------------------------------------

void xprop_button::Enum( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags )
{
    s32 iNewScope;
    enum_button& Entry = E.Append<enum_button>( iNewScope, iScope );

    Entry.Set( pPropertyName, &g_PropButton, pUserHelp, Flags );
}

//------------------------------------------------------------------------------

xbool xprop_button::Query( xproperty_query& I, xstring& Var )
{ 
    // cast it right
    xprop_button& Vec = xprop_button::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) 
    {
        Vec.m_Data = Var;
    }
    else
    {
        Var = Vec.m_Data;
    }

    // allow us to put it in an if statement
    return TRUE;
}

//------------------------------------------------------------------------------

xstring xprop_button::GetUserValue( xproperty_query& I )
{
    // cast it right
    xprop_button& Vec = xprop_button::SafeCast( I.GetType() ); 
    return Vec.m_Data;
}

//------------------------------------------------------------------------------

void xprop_button::SetUserValue( xproperty_query& I, const xstring& Value )
{
    // cast it right
    xprop_button& Vec = xprop_button::SafeCast( I.GetType() ); 
    Vec.m_Data = Value;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Dealing with game code
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#ifndef TARGET_MFC
CXTPPropertyGridItem* xprop_float::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_int::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_enum::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_string::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_filepath::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_guid::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_vector3::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_bool::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_color::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_date::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

CXTPPropertyGridItem* xprop_button::CreatePropertyControl( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const
{
    return NULL;
}

#endif