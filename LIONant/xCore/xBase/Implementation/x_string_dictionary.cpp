#include "../x_base.h"

#define MIN_HASH_TABLE_SIZE         2048
#define MIN_STRING_BUFFER_SIZE      (16*1024)

//==============================================================================

xstring_dictionary::xstring_dictionary()
{
    m_pBuffer       = NULL;
    m_pHashTable    = NULL;
    Reset();

}

//==============================================================================

xstring_dictionary::~xstring_dictionary()
{
    Reset();
}

//==============================================================================

void xstring_dictionary::Reset( void )
{
    if( m_pBuffer )
        x_free( m_pBuffer );

    if( m_pHashTable )
        x_free( m_pHashTable );

    m_Entries.DeleteAllNodes();

    m_pBuffer = NULL;
    m_pHashTable = NULL;
    m_BufferLength  = 0;
    m_NextOffset    = 0;
    m_HashTableSize = 0;
}

//==============================================================================

void xstring_dictionary::ResizeHashTable( s32 NewSize )
{
    NewSize = x_Max(NewSize,MIN_HASH_TABLE_SIZE);
    NewSize = x_Max(NewSize,m_Entries.getCount()*2);
    ASSERT( NewSize >= m_Entries.getCount() );

    m_HashTableSize = NewSize;
    if( m_pHashTable == NULL )
    {
        m_pHashTable    = (s32*)x_malloc( sizeof(s32), m_HashTableSize , 0 );
        x_memset( m_pHashTable, 0xff, m_HashTableSize * sizeof(s32) );
    }
    else
    {
        m_pHashTable    = (s32*)x_realloc( m_pHashTable, m_HashTableSize * sizeof(s32), 0 );
        x_memset( m_pHashTable, 0xff, m_HashTableSize * sizeof(s32) );
    }
    

    for( s32 i=0 ; i<m_Entries.getCount() ; i++ )
    {
        s32 Index = m_Entries[i].Hash % m_HashTableSize;

        while( m_pHashTable[Index] != -1 )
        {
            Index++;
            if( Index == m_HashTableSize ) Index = 0;
        }

        m_pHashTable[Index] = i;
    }
}

//==============================================================================

void xstring_dictionary::HashString( const char* pString, u32& Hash, s32& Length ) const
{
    const char* pStringStart = pString;
    Hash                     = 5381;
    Length                   = 0;

    // Process each character to generate the hash key
    while( *pString )
    {
        // These other hash functions are left here for completeness, they have all been tried and
        // are sorted in order of worst to best for performance (performance here usually means a
        // combination of simplicity to calculate and resulting distribution)
//        Hash = (Hash << 4) + *pString++;
//        Hash = (Hash << 4) ^ (Hash >> 28) + *pString++;
//        Hash = *pString++ + (Hash << 6) + (Hash << 16) - Hash;
        Hash = (Hash * 33) ^ *pString++;
    }

    // Calculate length of string
    Length = s32(pString - pStringStart);
}

//==============================================================================

s32 xstring_dictionary::Find( const char* pString, u32& Hash, s32& Length ) const
{
    // Hash the string for faster comparison, also get the length while we're at it
    HashString( pString, Hash, Length );

    // If the hash table has no entries then we won't find it!
    if( m_HashTableSize==0 )
        return -1;

    // Try to find existing match in xstring_dictionary
    s32 Index = Hash % m_HashTableSize;
    while( m_pHashTable[Index] != -1 )
    {
        if( (m_pHashTable[Index] != -1) &&
            (m_Entries[m_pHashTable[Index]].Hash == Hash) &&
            (x_strcmp( pString, &m_pBuffer[m_Entries[m_pHashTable[Index]].Offset] ) == 0) )
        {
            return m_pHashTable[Index];
        }

        Index++;
        if( Index==m_HashTableSize ) Index = 0;
    }

    // Return not found
    return -1;
}

//==============================================================================

s32 xstring_dictionary::Find( const char* pString ) const
{
    u32 Hash;
    s32 Length;

    // Call internal find function
    return( Find( pString, Hash, Length ) );
}

//==============================================================================

s32 xstring_dictionary::Add( const char* pString )
{
    s32 i;
    u32 Hash;
    s32 Length;

    // Try to find match
    i = Find( pString, Hash, Length );
    if( i != -1 )
        return i;

    // Reallocate the buffer if too small to add a new string
    if( m_pBuffer == NULL )
    {
        m_BufferLength = MIN_STRING_BUFFER_SIZE;
        m_pBuffer = (char*)x_malloc( sizeof(char), m_BufferLength, 0 );
    }
    else if( (m_BufferLength-m_NextOffset) < (Length+1) )
    {
        m_BufferLength = x_Max( (m_BufferLength*2), MIN_STRING_BUFFER_SIZE );
        m_pBuffer = (char*)x_realloc( m_pBuffer, m_BufferLength, 0 );
        ASSERT( m_pBuffer );
    }

    // Create a new xstring_dictionary_entry
    xstring_dictionary::entry& Entry = m_Entries.append();
    Entry.Hash   = Hash;
    Entry.Offset = m_NextOffset;

    // Add string into buffer
    x_strcpy( &m_pBuffer[m_NextOffset], 256, pString );
    m_pBuffer[m_NextOffset+Length] = 0;
    m_NextOffset += Length+1;

    // Add into hash table
    if( (m_HashTableSize/2) < m_Entries.getCount() )
        ResizeHashTable( m_HashTableSize*2 );

    s32 Index = Hash % m_HashTableSize;
    while( m_pHashTable[Index] != -1 )
    {
        Index++;
        if( Index == m_HashTableSize ) Index = 0;
    }
    m_pHashTable[Index] = m_Entries.getCount()-1;

    // Return index of string
    return m_Entries.getCount()-1;
}

//==============================================================================

const char* xstring_dictionary::getString( s32 iEntry ) const
{
    ASSERT( (iEntry >= 0) && (iEntry < m_Entries.getCount()) );
    return &m_pBuffer[m_Entries[iEntry].Offset];
}

//==============================================================================

s32 xstring_dictionary::getOffset( s32 iEntry ) const
{
    ASSERT( (iEntry >= 0) && (iEntry < m_Entries.getCount()) );
    return m_Entries[iEntry].Offset;
}

//==============================================================================

s32 xstring_dictionary::getCount( void ) const
{
    return m_Entries.getCount();
}

//==============================================================================

s32 xstring_dictionary::getSaveSize( void ) const
{
    return m_NextOffset;
}

//==============================================================================

s32 xstring_dictionary::Save( xfile& File ) const
{
    // Write the data buffer
    File.WriteRaw( m_pBuffer, 1, m_NextOffset );

    // Return number of bytes written
    return m_NextOffset;
}

//==============================================================================

xbool xstring_dictionary::Load( xfile& File, s32 nBytes )
{
    // Resize the data buffer
    m_pBuffer = (char*)x_realloc( m_pBuffer, nBytes, 0 );
    m_BufferLength = nBytes;
    m_NextOffset   = nBytes;
 
    // Read the data into buffer
    s32 BytesRead = File.ReadRaw( m_pBuffer, 1, nBytes );
    ASSERT( BytesRead == nBytes );
    (void)BytesRead;
 
    // Recreate all the xstring_dictionary entries
    m_Entries.DeleteAllNodes();
    const char* pString = m_pBuffer;
    while( (pString - m_pBuffer) < m_NextOffset )
    {
        s32     Length;
 
        // Add a new xstring_dictionary xstring_dictionary_entry
        xstring_dictionary::entry&  Entry = m_Entries.append();
 
        // Get the hash key and length of the string
        HashString( pString, Entry.Hash, Length );
 
        // Set the offset into the xstring_dictionary_entry
        Entry.Offset = s32(pString - m_pBuffer);
 
        // Advance to next string
        pString += Length+1;
    }
 
    // Rebuild the hash table
    ResizeHashTable( m_Entries.getCount()*2 );
 
    return TRUE;
}

//==============================================================================

s32 xstring_dictionary::Save( const char* pFileName  ) const
{
    ASSERT( pFileName );
    
    xfile File;
    
    if( FALSE == File.Open( pFileName, "wb" ) )
        return FALSE;
    
    return Save( File );
}

//==============================================================================

xbool xstring_dictionary::Load( const char* pFileName )
{    
    ASSERT( pFileName );
    
    xfile File;
    if( FALSE == File.Open( pFileName, "rb" ) )
        return FALSE;
   
    return Load( File, File.GetFileLength() );
}

//==============================================================================

void xstring_dictionary::Export( xbitstream& BitStream )
{
#if 0
    // Save the xstring_dictionary to a bitstream

    // write out data size (in bytes)
    BitStream.WriteU32( m_NextOffset );

    // Write the data buffer
    BitStream.WriteBits( m_pBuffer, ( m_NextOffset * 8 ) );
#endif
}

//==============================================================================

void xstring_dictionary::Import( xbitstream& BitStream )
{
#if 0
    // Load the xstring_dictionary from a bitstream

    // read data size
    u32 nBytes;
    BitStream.ReadU32( nBytes );

    // Resize the data buffer
    m_pBuffer = (char*)x_realloc( m_pBuffer, nBytes );
    m_BufferLength = nBytes;
    m_NextOffset   = nBytes;
 
    // Read the data into buffer
    BitStream.ReadBits( m_pBuffer, (nBytes*8) );
 
    // Recreate all the xstring_dictionary entries
    m_Entries.Clear();
    const char* pString = m_pBuffer;
    while( (pString - m_pBuffer) < m_NextOffset )
    {
        s32     Length;
 
        // Add a new xstring_dictionary xstring_dictionary_entry
        xstring_dictionary_entry&  Entry = m_Entries.Append();
 
        // Get the hash key and length of the string
        HashString( pString, Entry.Hash, Length );
 
        // Set the offset into the xstring_dictionary_entry
        Entry.Offset = (pString - m_pBuffer);
 
        // Advance to next string
        pString += Length+1;
    }
 
    // Rebuild the hash table
    ResizeHashTable( m_Entries.getCount()*2 );
#endif
}

//==============================================================================

void xstring_dictionary::Dump( void )
{
    x_DebugMsg( 0, "--------------------------\n" );
    x_DebugMsg( 0, " xstring_dictionary       \n" );
    x_DebugMsg( 0, "--------------------------\n" );

    // Dump the contents of the xstring_dictionary to the debug window
    for( s32 x=0; x<m_Entries.getCount(); x++ )
    {
        x_DebugMsg( 0, "xstring_dictionary string : %s\n", &m_pBuffer[m_Entries[x].Offset] );
    }
}

//==============================================================================

