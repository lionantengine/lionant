
//==============================================================================
// INCLUDES
//==============================================================================

#include <stdio.h>

#include "../x_target.h"
#ifdef TARGET_PC
    #include <windows.h>
    #include <tchar.h>
#endif

#include "../x_base.h"

//==============================================================================
// UNDEFINE A FEW THINGS HERE
//==============================================================================
#undef x_LogWarning
#undef x_LogError  
#undef x_LogMessage

//==============================================================================
//==============================================================================
//==============================================================================
// LOCAL STUFF
//==============================================================================
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
// Locals
//==============================================================================

enum
{
    XDB_FLAG_IGNORE             = X_BIT(1),
    XDB_FLAG_CONCADENATE        = X_BIT(2),
};

struct debug_local
{
    xscope_info                 m_ScopeInfo;
    xsafe_array<char, 1024>     m_ExceptionBuffer;
    s32                         m_iExceptionBuffer;
    s32                         m_LogLineNumber;
    const char*                 m_pLogFileName;
};

xdebug_level g_xDebugLevel = XDEBUG_LEVEL_FAST;
volatile s32 g_xDDBZ;

//==============================================================================
// Functions
//==============================================================================

static inline
debug_local& GetDebugLocal( void )
{
    xbase_instance& Instance = x_GetXBaseInstance();
    ASSERT( Instance.m_pDebug );
    return *((debug_local*)Instance.m_pDebug);
}
//DOM-IGNORE-END
//==============================================================================
//==============================================================================
//==============================================================================
// CUSTOM MESSAGE BOX
//==============================================================================
// This is so that I can add a few more buttons to the regular messagebox
// also I need to have a way to change the way the buttons display info.
//==============================================================================
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN

#ifdef TARGET_PC

//------------------------------------------------------------------------------

struct custom_msgbox
{
    s32                                 m_nButtons;
    xsafe_array<char*,7>                m_ButtonName;
};

enum buttons
{   
    BUTTON1 = IDYES,
    BUTTON2 = IDNO,
    BUTTON3 = IDIGNORE,
    BUTTON4 = IDRETRY,
    BUTTON5 = IDTRYAGAIN,
    BUTTON6 = IDABORT,
    BUTTON7 = IDCONTINUE,
};

//------------------------------------------------------------------------------

static WNDPROC          s_oldProc;
static HHOOK            s_hMsgBoxHook;
static custom_msgbox*   s_pMsgBox = NULL;

//------------------------------------------------------------------------------

static
LRESULT CALLBACK MsgBoxCBTProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	HWND hwnd;
	HWND hwndButton;

	if(nCode < 0)
		return CallNextHookEx(s_hMsgBoxHook, nCode, wParam, lParam);

    static buttons ButtonID[] =
    {
        BUTTON1,
        BUTTON2,
        BUTTON3,
        BUTTON4,
        BUTTON5,
        BUTTON6,
        BUTTON7,
    };

	switch(nCode)
	{
	case HCBT_ACTIVATE:

		// Get handle to the message box!
		hwnd = (HWND)wParam;

        ASSERT( s_pMsgBox );
        // handle general buttons 
        if( s_pMsgBox->m_nButtons == 1 )
        {
		    hwndButton = GetDlgItem(hwnd, IDOK );
            SetWindowText(hwndButton, s_pMsgBox->m_ButtonName[0] );
        }
        else
        {
		    hwndButton = GetDlgItem(hwnd, ButtonID[0] );
            SetWindowText(hwndButton, s_pMsgBox->m_ButtonName[0] );

		    hwndButton = GetDlgItem(hwnd, ButtonID[1] );
            SetWindowText(hwndButton, s_pMsgBox->m_ButtonName[1] );
        }

        // Lets see if we need to add more buttons
        if( s_pMsgBox->m_nButtons>2 && !GetDlgItem(hwnd,ButtonID[2]) )
        {
            // Get the very fist button W/H
            RECT Rect;
            GetWindowRect( hwndButton, &Rect );
            s32 W = (Rect.right - Rect.left);
            s32 H = (Rect.bottom - Rect.top);

            // Get the actual window W/H
            RECT WindowRect;
            GetWindowRect( hwnd, &WindowRect );
            s32 WinW = (WindowRect.right - WindowRect.left);
            s32 Offset = WinW - ((W+5)*s_pMsgBox->m_nButtons);
            if( Offset < 0 ) Offset = 0;

            // Okay lets find the true offset
            Offset = Offset/2;

            // Also get the position
            POINT Point;
            Point.x = Rect.left;
            Point.y = Rect.top;
            ScreenToClient( hwnd, &Point );

            // Lets move the general buttons there
    		hwndButton = GetDlgItem(hwnd, ButtonID[0] );
            MoveWindow( hwndButton, Offset, Point.y, W, H, TRUE ); 

    		hwndButton = GetDlgItem(hwnd, ButtonID[1] );
            MoveWindow( hwndButton, Offset+(W+5), Point.y, W, H, TRUE ); 

            // Get the font as well
		    HFONT sysFont=(HFONT)SendMessage(hwndButton,WM_GETFONT,0,0); 

            // Create additional buttons
            for( s32 i=2; i<s_pMsgBox->m_nButtons; i++ )
            {
                hwndButton = CreateWindow("BUTTON", s_pMsgBox->m_ButtonName[i], BS_PUSHBUTTON |
                            WS_CHILD | WS_VISIBLE, Offset + (W+5)*i,Point.y,W,H,
                            hwnd, (HMENU)ButtonID[i], //(u16) (i+1), 
                            (HINSTANCE)(u16)GetWindowLongPtr( hwnd, GWLP_HINSTANCE), NULL); 

                SendMessage(hwndButton,WM_SETFONT, (WPARAM)sysFont,1); 
            }

            // Fix problem when window is smaller than the space for the buttons
            if( WinW - ((W+5)*s_pMsgBox->m_nButtons) < 0 )
            {
                WindowRect.right -= (WinW - ((W+5)*s_pMsgBox->m_nButtons));
                MoveWindow( hwnd, WindowRect.top, WindowRect.left, WindowRect.right-WindowRect.left,WindowRect.bottom-WindowRect.top, TRUE );
            }
        }

		return 0;

	}

	return CallNextHookEx(s_hMsgBoxHook, nCode, wParam, lParam);
}


//------------------------------------------------------------------------------
// TODO: May be this should be thread safe?
//------------------------------------------------------------------------------
static
int CustomMessageBox2(HWND hwnd, TCHAR *szText, TCHAR *szCaption, UINT uType, custom_msgbox& MsgBoxInfo )
{
	int retval;

    // Set the info
    s_pMsgBox = &MsgBoxInfo;

	// Install a window hook, so we can intercept the message-box
	// creation, and customize it
	s_hMsgBoxHook = SetWindowsHookEx(
		WH_CBT, 
		MsgBoxCBTProc, 
		NULL, 
		GetCurrentThreadId()			// Only install for THIS thread!!!
		);

    // Hack around the fact that vista doesn't render the text right
    xstring String;
    static const char Spaces[]= "                                                                                 ";
    String.Format( "%s%s", szCaption, &Spaces[ sizeof(Spaces) - x_Max( 0, (s32)sizeof(Spaces) - x_strlen(szCaption) ) ] );

	// Display a standard message box
    retval = MessageBox(hwnd, szText, String, uType| ((MsgBoxInfo.m_nButtons>=2)?MB_YESNO:MB_OK) );

	// remove the window hook
	UnhookWindowsHookEx(s_hMsgBoxHook);

    // handle the single button case
    if( MsgBoxInfo.m_nButtons == 1 ) retval = BUTTON1;

    // Clear the global var
    s_pMsgBox = NULL;

    // done
	return retval;
}

//------------------------------------------------------------------------------

#endif
//DOM-IGNORE-END

//==============================================================================
//==============================================================================
//==============================================================================
// EXCEPTIONS
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================
// VARIABLES
//==============================================================================
//DOM-IGNORE-BEGIN
xbool   g_bSkipAllThrowDialogs = FALSE;
xbool   g_bExceptionBreak      = TRUE;
//DOM-IGNORE-END

//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================
static
void x_ExceptionInit( void )
{
    debug_local& Local = GetDebugLocal();
    Local.m_ExceptionBuffer[0]=0;
    Local.m_iExceptionBuffer=0;
}

//==============================================================================
static
void x_ExceptionKill( void )
{

}

//==============================================================================

const char* x_ExceptionMessage( void )
{
    //
    // First get the locals
    //
    debug_local& Local = GetDebugLocal();

    return Local.m_ExceptionBuffer;
}

//==============================================================================

xbool x_ExceptionHandler( const char* pFileName, s32 LineNum, const char* pMessage, u32& Flags )
{
    //
    // First get the locals
    //
    debug_local& Local = GetDebugLocal();

    //
    // Update state variables
    //
    if( x_FlagIsOn( Flags, XDB_FLAG_CONCADENATE ) == FALSE )
    {
        Local.m_ExceptionBuffer[0]=0;
        Local.m_iExceptionBuffer=0;
    }

    //
    // Do a Divider
    //
    if( x_FlagIsOn( Flags, XDB_FLAG_CONCADENATE ) )
    {
        static const char* pDivider = "========\n";
        s32 Length = x_strlen(pDivider);

        if( (Local.m_iExceptionBuffer + Length + 8 ) < Local.m_ExceptionBuffer.getCount() )
        {
            x_strcpy( &Local.m_ExceptionBuffer[ Local.m_iExceptionBuffer ], Local.m_ExceptionBuffer.getCount()-Local.m_iExceptionBuffer, pDivider );
            Local.m_iExceptionBuffer += Length;
        }
    }

    //
    // Copy Message
    //
    if( pMessage )
    {
        s32 Length = x_strlen( pMessage );
        if( (Local.m_iExceptionBuffer + Length + 8 ) < Local.m_ExceptionBuffer.getCount() )
        {
            Local.m_iExceptionBuffer += x_sprintf( &Local.m_ExceptionBuffer[ Local.m_iExceptionBuffer ], Local.m_ExceptionBuffer.getCount()-Local.m_iExceptionBuffer,"*  MSG : %s\n", pMessage ); 
        }
    }

    //
    // Copy the file name
    //
    if( pFileName )
    {
        s32 Length = x_strlen( pFileName );
        if( (Local.m_iExceptionBuffer + Length + 8 ) < Local.m_ExceptionBuffer.getCount() )
        {
            Local.m_iExceptionBuffer += x_sprintf( &Local.m_ExceptionBuffer[ Local.m_iExceptionBuffer ], Local.m_ExceptionBuffer.getCount()-Local.m_iExceptionBuffer,"*  FILE: %s\n", pFileName ); 
        }
    }

    //
    // Copy the line Number
    //
    if( (Local.m_iExceptionBuffer + 32) < Local.m_ExceptionBuffer.getCount() )
    {
        Local.m_iExceptionBuffer += x_sprintf( &Local.m_ExceptionBuffer[ Local.m_iExceptionBuffer ], Local.m_ExceptionBuffer.getCount()-Local.m_iExceptionBuffer,"*  LINE: %d\n", LineNum ); 
    }

    //
    // Make sure we terminated momentarly
    //
    Local.m_ExceptionBuffer[ Local.m_iExceptionBuffer + 1] = 0;

    //
    // Handle logic
    //
    if( g_bSkipAllThrowDialogs == FALSE && !x_FlagIsOn( Flags, XDB_FLAG_IGNORE ) && g_bExceptionBreak )
    {
    #ifdef TARGET_PC
        custom_msgbox   MsgBox;

#ifdef X_DEBUG
        MsgBox.m_nButtons = 4;
        MsgBox.m_ButtonName[0] = "Debug";
        MsgBox.m_ButtonName[1] = "Ignore";
        MsgBox.m_ButtonName[2] = "IgnoreAlways";
        MsgBox.m_ButtonName[3] = "DisableDialog";
#else
        MsgBox.m_nButtons = 3;
        MsgBox.m_ButtonName[0] = "OK";
        MsgBox.m_ButtonName[1] = "IgnoreAlways";
        MsgBox.m_ButtonName[2] = "DisableDialog";
#endif

        buttons Answer = (buttons)CustomMessageBox2( NULL, Local.m_ExceptionBuffer, "!!!EXCEPTION!!! To Ignore just close the dialog box.", 
                                        MB_ICONINFORMATION, MsgBox );

#ifndef X_DEBUG

        switch( Answer )
        {
        case BUTTON1: Answer = BUTTON2; break;
        case BUTTON2: Answer = BUTTON3; break;
        case BUTTON3: Answer = BUTTON4; break;
        };
#endif

        if( Answer == BUTTON1 ) return TRUE;
        if( Answer == BUTTON2 ) return FALSE;
        if( Answer == BUTTON3 )
        {
            x_FlagOn( Flags, XDB_FLAG_IGNORE ); 
            return FALSE;
        }
        if( Answer == BUTTON4 ) 
        { 
            g_bExceptionBreak = FALSE;
            return FALSE;
        }

        // User must have close the dialog
        return FALSE;
    #else
        
        return g_bExceptionBreak;
    #endif
    }

    return FALSE;
}

//==============================================================================

void x_ExceptionDisplay( void )
{
    debug_local& Local = GetDebugLocal();

    //
    // TODO: The exceptions should be printed in the oposite order.
    // Display the exception
    //
#ifdef TARGET_PC
    s32 Answer = MessageBox( NULL, Local.m_ExceptionBuffer, "EXCEPTION", MB_ICONINFORMATION|MB_OK );
#elif defined TARGET_IOS
    
#endif
    

    //
    // Reset the message buffer
    //
    Local.m_ExceptionBuffer[0]=0;
    Local.m_iExceptionBuffer=0;
}

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
// ASSERT
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
// variables
//==============================================================================
static
xbool BeforeXBaseHasInitedAssertHandler( 
    u32&           Flags,
    const char*    pFileName,
    s32            LineNumber,
    const char*    pExprString,
    const char*    pMessageString )
{
    return TRUE;
}

static xassert_fn* s_pFunction = BeforeXBaseHasInitedAssertHandler;

//==============================================================================
// Functions
//==============================================================================

//==============================================================================
#ifdef X_ASSERT
static
xbool DefaultAssertHandler( 
    u32&           Flags,
    const char*    pFileName,
    s32            LineNumber,
    const char*    pExprString,
    const char*    pMessageString )
{
    x_BeginAtomic();

    //
    // Handle Flags
    //
    if( x_FlagIsOn( Flags, XDB_FLAG_IGNORE ) ) return FALSE;

    //
    // Survive NULL entries
    //
    if( pFileName      == NULL ) pFileName      = "Unkown";
    if( pExprString    == NULL ) pExprString    = "Unkown";
    if( pMessageString == NULL ) pMessageString = "Unkown";

    //
    // Create the report to print
    //
    char Report[1024*10];
    s32 Index = x_sprintf( Report, sizeof(Report),  "*  EXPR: %s\n"
                                                    "*  MSG : %s\n" 
                                                    "*  FILE: %s\n"
                                                    "*  LINE: %d\n"
                                                    , pExprString, pMessageString, pFileName, LineNumber );
    //
    // Dump the scope info
    //
    xscope_info& Scope  = x_GetScopeInfo();
    s32          iScope = Index;
    if( Scope.m_CurScope > 0 )
    {
        Index += x_sprintf( &Report[Index], sizeof(Report)-Index, "\nSCOPE INFO:\n" );
        for( s32 i=0; i<Scope.m_CurScope; i++ )
        {
            Index += x_sprintf( &Report[Index], sizeof(Report)-Index, "%s(%d) : %s\n", Scope.m_Scope[i]->m_pFileName, Scope.m_Scope[i]->m_LineNumber, Scope.m_Scope[i]->m_pScopeName ); 
        }

        //
        // Dump log info with scope
        //
        x_LogPush( pFileName, LineNumber );
        x_LogError( "Assert", "EXPR:%s\nMSG:%s %s", pExprString, pMessageString, &Report[iScope] );
    }
    else
    {
        //
        // dump log with out scope report
        //
        x_LogPush( pFileName, LineNumber );
        x_LogError( "Assert", ">>>>> EXPR:%s MSG:%s ", pExprString, pMessageString );
    }

    //
    // Display menu to the user
    //
#ifdef TARGET_PC

    custom_msgbox   MsgBox;

    MsgBox.m_nButtons = 4;
    MsgBox.m_ButtonName[0] = "Debug";
    MsgBox.m_ButtonName[1] = "Ignore";
    MsgBox.m_ButtonName[2] = "IgnoreAlways";
    MsgBox.m_ButtonName[3] = "CallStack";

    buttons Answer = (buttons)CustomMessageBox2( NULL, Report, "!!!ASSERT!!!! - Do you wish to debug?", 
                                                 MB_ICONEXCLAMATION, MsgBox );
    
    if( Answer == BUTTON1 ) return TRUE;
    if( Answer == BUTTON2 ) return FALSE;
    if( Answer == BUTTON3 ) 
    { 
        x_FlagOn( Flags, XDB_FLAG_IGNORE ); 
        return FALSE;
    }
    if( Answer == BUTTON4 ) 
    { 
        custom_msgbox   MsgBox;

        MsgBox.m_nButtons = 2;
        MsgBox.m_ButtonName[0] = "Yes";
        MsgBox.m_ButtonName[1] = "No";

        char* pCallStack = x_DebugGetCallStack();
        s32 Q = CustomMessageBox2(NULL, pCallStack, "!!!ASSERT!!!! - Do you wish to debug?", MB_ICONEXCLAMATION, MsgBox );
        if( Q == IDYES )
        {
            return TRUE;
        }        
    }

    x_EndAtomic();
    return FALSE;
#else
    
    x_EndAtomic();
    return TRUE;
#endif
        
}
#endif
//DOM-IGNORE-END

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     PRIVATE XBASE FUNCTION. IGNORE IGNORE.
// Arguments:
//     Flags                - Reference to a local copy of flags for the assert.
//     pFileName            - File name in which the assert happen
//     LineNumber           - Line number where the assert happen
//     pExprString          - Expresion of the assert
//     pMessageString       - Additional string containing information about the 
//                            assert.
// Returns:
//     xbool - TRUE when the program should be holdted FALSE other wise
// Description:
//     This function is call when an assert happens.
// See Also:
//     x_SetAssertHandler
//------------------------------------------------------------------------------
xbool x_AssertHandler( 
    u32&           Flags,
    const char*    pFileName,
    s32            LineNumber,
    const char*    pExprString,
    const char*    pMessageString )
{
    static s32 Count=0;

    Count++;
    if( Count >= 10 ) 
    {
        // Hitting infinite recursion
        X_BREAK;
    }

    xbool Ret =  s_pFunction(   Flags,
                                pFileName,
                                LineNumber,
                                pExprString,
                                pMessageString );

    Count--;

    return Ret;
}

//==============================================================================

void x_SetAssertHandler( xassert_fn* pAssertHandler )
{
    if( pAssertHandler == NULL )
        return;

    s_pFunction = pAssertHandler;
}

//==============================================================================
//==============================================================================
//==============================================================================
// SCOPE FUNCTIONS
//==============================================================================
//==============================================================================
//==============================================================================

xscope_info& x_GetScopeInfo( void )
{
    debug_local& Local = GetDebugLocal();
    return Local.m_ScopeInfo;
}


//==============================================================================
//==============================================================================
//==============================================================================
// LOG FUNCTIONS
//==============================================================================
//==============================================================================
//==============================================================================

#if defined TARGET_IOS || defined TARGET_OSX
#include <sys/sysctl.h>
#include <unistd.h>

bool IsDebuggerPresent() {
    int mib[4];
    struct kinfo_proc info;
    size_t size;
    
    info.kp_proc.p_flag = 0;
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();
    
    size = sizeof(info);
    sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    
    return ((info.kp_proc.p_flag & P_TRACED) != 0);
}

void OutputDebugString(const char*  fmt, ...) {
    if( !IsDebuggerPresent() )
        return;
    
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}
#endif


//DOM-IGNORE-BEGIN
static s32 s_LogSequence=0;
//DOM-IGNORE-END
//------------------------------------------------------------------------------

void x_DebugMsg( const char* pFormatStr, ... )
{
    va_list   Args;
    va_start( Args, pFormatStr );

    //debug_local Local = GetDebugLocal();

    char Buffer[512*2];
    s32  Index = 0;
//    s32 Index = sprintf( Buffer, "---> SEQUENCE:%d TYPE:Message CHANNEL:%s LINE:%d SOURCE:%s\n    :", s_LogSequence++, "Debug", Local.m_LogLineNumber, Local.m_pLogFileName  );
    //s32 Index = sprintf( Buffer, "%s(%d) : SEQUENCE:%d TYPE:Message CHANNEL:%s \n", Local.m_pLogFileName, Local.m_LogLineNumber, s_LogSequence++, "Debug" );
    Index += x_vsprintf( &Buffer[Index], sizeof(Buffer), pFormatStr, Args );
    //Index += sprintf( &Buffer[Index], "\n" );

#ifdef TARGET_PC
    OutputDebugString(xfs("MSG[%f]: %s", x_GetTimeSec(), Buffer));
   // OutputDebugString(Buffer);
    //printf( Buffer );
   // fflush(stdout);
#elif defined TARGET_IOS || defined TARGET_OSX
    OutputDebugString(xfs("\e[0;31mMSG[%f]: %s", x_GetTimeSec(), Buffer));
#elif defined TARGET_ANDROID
    
#endif
    
/*
    va_list   Args;
    va_start( Args, pFormatStr );

    char Buffer[512];
    vsprintf( Buffer, pFormatStr, Args );

    OutputDebugString(Buffer);
*/
}

//------------------------------------------------------------------------------

void x_LogMessage( const char* pChannel, const char* pFormatStr, ... )
{
    va_list   Args;
    va_start( Args, pFormatStr );

    debug_local& Local = GetDebugLocal();

    char Buffer[512*2];
    s32 Index = x_sprintf( Buffer, sizeof(Buffer), "%s(%d) : SEQUENCE:%d TYPE:Message CHANNEL:%s \n", Local.m_pLogFileName, Local.m_LogLineNumber, s_LogSequence++, pChannel );
    Index += x_vsprintf( &Buffer[Index], sizeof(Buffer)-Index, pFormatStr, Args );
    Index += x_sprintf( &Buffer[Index], sizeof(Buffer)-Index, "\n" );

#ifdef TARGET_PC
    OutputDebugString(Buffer);
//    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_IOS || defined TARGET_OSX
    OutputDebugString(Buffer);
//    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_ANDROID
    
#endif

}

//------------------------------------------------------------------------------

void x_LogWarning( const char* pChannel, const char* pFormatStr, ... )
{
    va_list   Args;
    va_start( Args, pFormatStr );

    debug_local& Local = GetDebugLocal();

    char Buffer[512*2];
    s32 Index = x_sprintf( Buffer, sizeof(Buffer), "%s(%d) : SEQUENCE:%d TYPE:Warning CHANNEL:%s \n", Local.m_pLogFileName, Local.m_LogLineNumber, s_LogSequence++, pChannel );
    Index += x_vsprintf( &Buffer[Index], sizeof(Buffer) - Index, pFormatStr, Args );
    Index += x_sprintf( &Buffer[Index], sizeof(Buffer) - Index, "\n" );

#ifdef TARGET_PC
    OutputDebugString(Buffer);
    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_IOS || defined TARGET_OSX
    OutputDebugString(Buffer);
    //    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_ANDROID
    
#endif
}

//------------------------------------------------------------------------------

void x_LogError( const char* pChannel, const char* pFormatStr, ... )
{
    va_list   Args;
    va_start( Args, pFormatStr );

    debug_local& Local = GetDebugLocal();

    char Buffer[1024*10];
    s32 Index = x_sprintf( Buffer, sizeof(Buffer), "%s(%d) : SEQUENCE:%d TYPE:Error CHANNEL:%s \n", Local.m_pLogFileName, Local.m_LogLineNumber, s_LogSequence++, pChannel );
    Index += x_vsprintf( &Buffer[Index], sizeof(Buffer) - Index, pFormatStr, Args );
    Index += x_sprintf( &Buffer[Index], sizeof(Buffer) - Index, "\n" );

#ifdef TARGET_PC
    OutputDebugString(Buffer);
    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_IOS || defined TARGET_OSX
    OutputDebugString(Buffer);
    //    printf( Buffer );
    fflush(stdout);
#elif defined TARGET_ANDROID
    
#endif
}

//------------------------------------------------------------------------------

void x_LogFlush( void )
{

}

//------------------------------------------------------------------------------

void x_LogPush( const char* pFileName, s32 LineNum )
{
    debug_local& Local = GetDebugLocal();

    Local.m_LogLineNumber = LineNum;
    Local.m_pLogFileName  = pFileName;
}

//==============================================================================
//==============================================================================
//==============================================================================
// MAIN FILE: Initialization of the file
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================
#undef new
#undef delete

void x_DebugInstanceInit( void )
{
    //
    // Make sure that there is good AssertHandler inplace 
    //
#ifdef X_ASSERT
    x_SetAssertHandler( DefaultAssertHandler );
#endif

    //
    // Create the memory for this instance
    //
    {
        xbase_instance& Instance = x_GetXBaseInstance();
        Instance.m_pDebug        = new debug_local;
        if( Instance.m_pDebug == NULL )
        {
            ASSERTS( 0, "Out of memory" );
        }
    }

    //
    // Get the local structure
    //
    debug_local& Local = GetDebugLocal();

    //
    // Initialize the scope info
    //
    Local.m_ScopeInfo.m_CurScope = 0;

    //
    // Initialize exceptions
    //
    x_ExceptionInit();
}

//==============================================================================

void x_DebugInstanceKill( void )
{
    //
    // Initialize exceptions
    //
    x_ExceptionKill();

    //
    // Release the x_debug memory
    //
    xbase_instance& Instance = x_GetXBaseInstance();
    if(Instance.m_pDebug) 
    { 
        delete (debug_local*)Instance.m_pDebug; 
        Instance.m_pDebug=NULL; 
    }
}

//==============================================================================
//==============================================================================
//==============================================================================
// StackWalk
//==============================================================================
//==============================================================================
//==============================================================================
//DOM-IGNORE-BEGIN
#ifdef TARGET_PC

//------------------------------------------------------------------------------

#include "Windows/x_debug_pc_StackWalker.h"

//------------------------------------------------------------------------------

class MyStackWalker : public StackWalker
{
public:
                MyStackWalker   ( void ) : StackWalker() { Init(); }
    char*       GetBuffer       ( void ) { return m_pBuffer; }

protected:

    void Init( void )
    {
        m_options       = RetrieveLine; 
        m_BufferSize    = 1024;
        m_pBuffer       = (char*)x_malloc( 1, m_BufferSize, 0 );
        m_Index         = 0;
    }

    virtual void OnOutput(LPCSTR szText) 
    { 
        s32 LocalIndex = 0;

        // Concatenate the string
        while( m_pBuffer[m_Index] = szText[LocalIndex] ) 
        {
            m_Index++;
            LocalIndex++;
            if( m_Index >= m_BufferSize )
            {
                m_BufferSize *= 2;
                m_pBuffer = (char*)x_realloc( m_pBuffer, 1, m_BufferSize );
            }
        }

        // Output to the console as well
        StackWalker::OnOutput(szText); 
    }

    char*       m_pBuffer;
    s32         m_Index;
    s32         m_BufferSize;
};
//DOM-IGNORE-END

//------------------------------------------------------------------------------

char* x_DebugGetCallStack( void )
{
    MyStackWalker StackWalk; 

    StackWalk.ShowCallstack();
    return StackWalk.GetBuffer();
}
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
