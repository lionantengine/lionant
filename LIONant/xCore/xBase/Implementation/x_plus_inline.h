#pragma once		// Include this file only once

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Toggles a bit flag on or off depending of its previous status. 
// Arguments:
//     N - Is the atomic type to activate the flag. Note that the variable passed will changed its value.
//     F - Is the bit flags which identifies which which bits to toggle
// Returns:
//     void
// Description:
//     Toggles a bit flag on or off depending of its previous status. 
// See Also:
//     x_FlagOn x_FlagOff x_FlagsAreOn X_BIN X_BIT x_FlagIsOn
//------------------------------------------------------------------------------
template< class ta > inline 
void x_FlagToggle( ta& N, const u32 F )
{ 
    N = (ta)(N^F);
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Turns a bit on in an atomic integer type. 
// Arguments:
//     N - Is the atomic type to activate the flag. Note that the variable passed will changed its value.
//     F - Is the flag
// Returns:
//     void
// Description:
//     Turns a bit on in an atomic integer type. 
// See Also:
//     x_FlagOff x_FlagsAreOn X_BIN X_BIT x_FlagIsOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > inline 
void x_FlagOn( ta& N, const u32 F )
{ 
    N = (ta)(N|F); 
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Turns a bit off in an atomic integer type. 
// Arguments:
//     N - Is the atomic type to deactivate the flag. Note that the variable passed will changed its value.
//     F - Is the flag
// Returns:
//     void
// Description:
//     Turns a bit off in an atomic integer type. 
// See Also:
//     x_FlagOn x_FlagIsOn X_BIN X_BIT x_FlagsAreOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > inline 
void x_FlagOff( ta& N, const u32 F )
{ 
    N = (ta)(N & (~F)); 
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Test whether a set of flags is on at all or not.
// Arguments:
//     N - Is the atomic type that contains the current value.  
//     F - Is the set of flags to test.
// Returns:
//     TRUE if any of the flags in F turns to be on, FALSE other wise.
// Description:
//     Test whether in a set of flags one IS on or not.
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagsAreOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > inline 
xbool x_FlagIsOn( const ta  N, const u32 F )
{ 
    return !!(N&F); 
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Test whether a set of flags is on at all or not.
// Arguments:
//     N - Is the atomic type that contains the current value.  
//     F - Is the set of flags to test.
// Returns:
//     TRUE if any of the flags in F turns to be on, FALSE other wise.
// Description:
//     Test whether in a set of flags all ARE on or not.
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagIsOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > inline 
xbool x_FlagsAreOn( const ta  N, const u32 F )
{ 
    return (N&F)==F; 
}

//------------------------------------------------------------------------------

template< class ta > inline
ta x_Pow2RoundUp( ta x )
{
    if (x < 0)
        return 0;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    
    return x+1;
}

//------------------------------------------------------------------------------

template< class ta > inline
ta x_Pow2MinimumRequire( ta x )
{
    if( x == 0 )
        return 0;

    s32 r=1;
    if (x >> 16) { r += 16; x >>= 16; }
    if (x >>  8) { r +=  8; x >>=  8; }
    if (x >>  4) { r +=  4; x >>=  4; }
    if (x >>  2) { r +=  2; x >>=  2; }
    if (x - 1) ++r;

    return r;
}


//------------------------------------------------------------------------------
// x_EndianSwap functions
//------------------------------------------------------------------------------

inline u8  x_EndianSwap( const u8  A ){ return A; }

// <COMBINE x_EndianSwap>
inline s8  x_EndianSwap( const s8  A ){ return A; }

// <COMBINE x_EndianSwap>
inline u16 x_EndianSwap( const u16 A ){ return (A >>  8) | (A <<  8); }

// <COMBINE x_EndianSwap>
inline u32 x_EndianSwap( const u32 A ){ return (A >> 24) | (A << 24) | ((A & 0x00FF0000) >> 8) | ((A & 0x0000FF00) << 8); }

// <COMBINE x_EndianSwap>
inline u64 x_EndianSwap( const u64 A ){ return (A >> 56) | (A << 56) | ((A & 0x00FF000000000000ULL) >> 40) | ((A & 0x000000000000FF00ULL) << 40) | ((A & 0x0000FF0000000000ULL) >> 24) | ((A & 0x0000000000FF0000ULL) << 24) | ((A & 0x000000FF00000000ULL) >> 8) | ((A & 0x00000000FF000000ULL) << 8); }

// <COMBINE x_EndianSwap>
inline s16 x_EndianSwap( const s16 A ){ return (s16)x_EndianSwap( (u16)A ); }

// <COMBINE x_EndianSwap>
inline s32 x_EndianSwap( const s32 A ){ return (s32)x_EndianSwap( (u32)A ); }

// <COMBINE x_EndianSwap>
inline s64 x_EndianSwap( const s64 A ){ return (s64)x_EndianSwap( (u64)A ); }

// <COMBINE x_EndianSwap>
inline f32 x_EndianSwap( const f32 A ){ u32 a = x_EndianSwap( *((u32*)&A) ); return *(f32*)&a; }

// <COMBINE x_EndianSwap>
inline f64 x_EndianSwap( const f64 A ){ u64 a = x_EndianSwap( *((u64*)&A) ); return *(f64*)&a; }

//------------------------------------------------------------------------------
// x_Align functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

template< class T > inline 
T x_Align( T Addr, s32 AlignTo ) 
{ 
    return T( ( (((u64)(Addr)) + ((AlignTo)-1)) & (-(AlignTo)) )); 
}

//------------------------------------------------------------------------------

template< class T > inline 
T x_AlignLower( T Addr, s32 AlignTo ) 
{ 
    return T( ( (((u64)(Addr))) & (-(AlignTo)) )); 
}

//------------------------------------------------------------------------------

template< class T > inline 
xbool x_IsAlign( T Addr, s32 AlignTo ) 
{ 
    return (((u64)Addr) & (AlignTo-1)) == 0;
}

//------------------------------------------------------------------------------
// IsPow2 function
//------------------------------------------------------------------------------

template< class ta > inline 
xbool x_IsPow2( ta  N ) 
{ 
    return (((N)&((N)-1))==0); 
}
