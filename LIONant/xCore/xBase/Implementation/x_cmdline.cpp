//==============================================================================
// INCLUDES
//==============================================================================
#include <math.h>
#include <stdlib.h>
#include "../x_base.h"

//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================

xcmdline::xcmdline( void )
{
    m_bNeedHelp = FALSE;
}

//==============================================================================

xbool xcmdline::DoesUserNeedsHelp( void )
{
    return m_bNeedHelp;
}

//==============================================================================

s32  xcmdline::AddCmdSwitch( 
    const char* pName, 
    s32         MinArgCount, 
    s32         MaxArgCount, 
    s32         nMinTimes, 
    s32         nMaxTimes, 
    xbool       MustFallowOrder, 
    type        Type,
    xbool       bMainSwitch, 
    s32         iParentID )
{
    cmd_def& CmdDef         = m_CmdDef.append();

    CmdDef.m_Name.Copy( pName );
    CmdDef.m_crcName        = x_strCRC( pName );
    CmdDef.m_MinArgCount    = MinArgCount;      
    CmdDef.m_MaxArgCount    = MaxArgCount;      
    CmdDef.m_nMaxTimes      = nMaxTimes;           
    CmdDef.m_nMinTimes      = nMinTimes;           
    CmdDef.m_RefCount       = 0;         
    CmdDef.m_bFallowOrder   = MustFallowOrder;     
    CmdDef.m_Type           = Type;             
    CmdDef.m_bMainSwitch    = bMainSwitch; 
    CmdDef.m_iParentID      = iParentID;
  
    return m_CmdDef.getCount()-1;
}

//==============================================================================

void xcmdline::Parse( s32 argc, const char** argv )
{    
    xarray<xstring> Arguments;

    // User needs help
    if( argc == 1 ) 
    {
        s32 TotalMustHave = 0;
        for( s32 i=0; i<m_CmdDef.getCount(); i++ )
        {
            TotalMustHave += m_CmdDef[i].m_nMinTimes;
        }

        if( TotalMustHave > 0 )
        {
            ClearArguments();
            m_bNeedHelp = TRUE;
            return;
        }
    }

    // Put all the arguments into the list
    // skip argv 0 sinec it is the file name.
    for( s32 i=1; i<argc; i++ )
    {
        ASSERT(argv[i]);
        
        Arguments.append().Copy( argv[i] );
        if( Arguments[i-1][0] == '-' ) 
            Arguments[i-1][0] = 1;
    }

    // Parse these arguments
    Parse( Arguments );
}

//==============================================================================

void xcmdline::Parse( const char* pString )
{
    s32             iStart;
    s32             iEnd;
	s32	            iNext;
    s32             Length = x_strlen(pString);
    xstring         strCmd = xstring::BuildFromFormat(pString);
    xarray<xstring> Arguments;

    // process all the arguments
    iStart = 0;
    while( iStart < Length )
    {
        // Skip Whitespace
        while( (iStart < Length) && x_isspace( strCmd[iStart] ) )
        {
            iStart++;
        }

		// At end of string?
		if( iStart < Length )
		{
			// Find limits of string
			iEnd = iStart+1;
            xbool bStartArgument = FALSE;
			if( strCmd[iStart] == '"' )
            {
                bStartArgument = TRUE;
                if( iStart > 0 )
                {
                    if( strCmd[iStart-1]=='\\')
                        bStartArgument = FALSE;
                }
            }
            else if (strCmd[iStart] == '-' && !x_isdigit(strCmd[iStart+1]) && strCmd[iStart+1]!='.')
            {
                // Replace the switch identifier to ascii 1
                strCmd[iStart] = 1;
            }

            if(bStartArgument)
			{
				// iStart++; // old vesion removed the quotes new version will keep them
				while( iEnd<Length )
                {
                    if(strCmd[iEnd] == '"')
                    {
                        if(strCmd[iEnd-1]!='\\')
                        {
                            iEnd++;     // old version removed the quotes but the new version keeps it
                            break;
                        }
                    }
					iEnd++;
                }
				iNext = iEnd+1;
			}
			else
			{
				while( (iEnd<Length) && !x_isspace(strCmd[iEnd]) )
					iEnd++;
				iNext = iEnd;
			}

			// Add to argument array
			Arguments.append().Copy( &strCmd[ iStart ], (iEnd-iStart) );

			// Set start past end of string
			iStart = iNext;
		}
    }

    // Parse these arguments
    Parse( Arguments );
}

//==============================================================================

void xcmdline::ProcessResponseFile( xstring& PathName )
{
    xfile File;

    // Open the file
    if( File.Open( PathName, "rt" ) == FALSE )
        x_throw( "Error: Unable to open the response file [%s]", (const char*)PathName );

    //
    // Read the hold file in
    //
    s32 Length = File.GetFileLength();
    xptr<char>  Buff;
    Buff.Alloc( Length+1 );
    s32 L = File.ReadRaw( &Buff[0], 1, Length );
    File.Close();

    // Make sure to terminate everything
    Buff[L]=0;

    //
    // Now process the data
    //
    xptr_lock LockBuff( Buff );
    Parse( &Buff[0] );
}

//==============================================================================

void xcmdline::Parse( xarray<xstring>& Args )
{
    s32 i;
    s32 iMainSwitch=-1;
/*
    for( i=0; i<Args.getCount(); i++)
    {
        xstring&  curArg = Args[i];
        s32 iLength = curArg.GetLength();
        char* pReading = new char[iLength+1];
        char* pWriting = new char[iLength+1];
        char* pSource = pReading;
        char* pTarget = pWriting;
        x_strcpy(pReading, iLength+1, curArg);
        x_strcpy(pWriting, iLength+1, pReading);
        s32 iSlash = FindCharacterInString(pSource, '\\');
        while( iSlash>=0 )
        {
            s32 iOffset = 0;
            if(pSource+iSlash < pReading+iLength-1)
                if( pSource[iSlash+1]=='\"' )
                    iOffset = 1;

            if(iOffset)
                x_strcpy( pTarget+iSlash, x_strlen(pSource)-iSlash+1-iOffset, pSource+iSlash+iOffset );
            pSource = pSource+iSlash+1;
            pTarget = pTarget+iSlash+1-iOffset;
            if( pSource-pReading >= iLength )
                break;
            iSlash = FindCharacterInString(pSource, '\\');
        }

        curArg.Clear();
        curArg.Format(pWriting);
        delete[] pReading;
        delete[] pWriting;
    }
*/

    // Process Args
    for( i=0 ; i<Args.getCount(); i++ )
    {
        xstring&  a = Args[i];

        // Check for Help?
        if( (a == "?") || (a == "\1?") || (a == "\1HELP") )
        {
            ClearArguments();
            m_bNeedHelp = TRUE;
            return;
        }

        // Check for response file
        if( a[0] == '@' )
        {
            xstring FileName;
            FileName.Copy( &a[1] );
            ProcessResponseFile( FileName );
            continue;
        }

        // Check for a command
        // The switch identifier is replaced by ascii 1 at this time
        // if( a[0] == '-' && ( (!x_isdigit(a[1])) && (a[1] != '.')) )
        if( a[0] == 1 && ( (!x_isdigit(a[1])) && (a[1] != '.')) )
        {
            xbool   Found = FALSE;

            // Remove leading '-' and find crc
            u32 CRC = x_strCRC( &a[1] );

            // check for minimun rage of the previous switch
            if( m_Command.getCount() )
            {
                cmd_entry& Cmd = m_Command[ m_Command.getCount()-1 ];

                if( m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount > Cmd.m_ArgCount )
                {
                    ClearArguments();
                    x_throw( "Error: We found this switch [%s] had too few arguments we expected at least [%d].", (const char*)a, m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount );
                    return;
                }                
            }

            // Search for option and read value into option list
            for( s32 j=0 ; j<m_CmdDef.getCount(); j++ )
            {
                // Check if found.
                // The switch we are looking for must match the name and its parent id
                if( m_CmdDef[j].m_crcName == CRC && 
                   iMainSwitch == m_CmdDef[j].m_iParentID  )
                {
                    xstring OptionValue;

                    Found = TRUE;

                    // Add the cmd entry
                    cmd_entry& Cmd = m_Command.append();
                    Cmd.m_iCmdDef  = j;
                    Cmd.m_iArg     = m_Arguments.getCount();
                    Cmd.m_ArgCount = 0;

                    // if it is a main switch make sure that there are not other ones active
                    if( m_CmdDef[j].m_bMainSwitch )
                    {
                        // Set the variable to have the id of the main switch
                        iMainSwitch = j;
                        
                        for( s32 t=0; t<m_CmdDef.getCount(); t++ )
                        {
                            if( t == j ) continue;
                            if( m_CmdDef[t].m_RefCount <= 0 ) continue;
                            if( m_CmdDef[t].m_bMainSwitch == FALSE ) continue;

                            ClearArguments();
                            x_throw( "Error: We found two main switches [%s] and [%s]. You can only enter one of this type of switches.", (const char*)m_CmdDef[t].m_Name, (const char*)m_CmdDef[j].m_Name );
                            return;                            
                        }
                    }

                    // make sure that we don't have this reference too many times
                    m_CmdDef[j].m_RefCount++;
                    if( m_CmdDef[j].m_RefCount > m_CmdDef[j].m_nMaxTimes && m_CmdDef[j].m_nMaxTimes != -1 )
                    {
                        ClearArguments();
                        x_throw( "Error: We found this switch [%s] too many times in the commandline. We were specting to find it only [%d] times", (const char*)a, m_CmdDef[j].m_nMaxTimes );
                        return;
                    }

                    // make sure that fallows the proper order in the sequence
                    if( m_CmdDef[j].m_bFallowOrder )
                    {
                        for( s32 t=0; t<m_Command.getCount(); t++ )
                        {
                            if( j < m_Command[t].m_iCmdDef )
                            {
                                ClearArguments();
                                x_throw( "Error: This switch:[%s] is out of order check help for proper usage.", (const char*)a );
                                return;
                            }
                        }
                    }

                    break;
                }
                else
                {
                    //
                    // We are going to be nice and search to see if there is another switch
                    // that the user may have been talking about but it is the wrong parenting
                    //
                    if( m_CmdDef[j].m_crcName == CRC )
                    {
                        xbool bFoundSameName = FALSE;
                        for( s32 k=j+1 ; k<m_CmdDef.getCount(); k++ )
                        {
                            if( m_CmdDef[k].m_crcName == CRC && iMainSwitch == m_CmdDef[k].m_iParentID )
                            {
                                bFoundSameName = TRUE;
                            }
                        }
                        
                        if( bFoundSameName == FALSE )
                        {
                            // check whether it needs another switch to exits
                            if( m_CmdDef[j].m_iParentID != -1 )
                            {
                                s32 Index = m_CmdDef[j].m_iParentID;
                                if( m_CmdDef[ Index ].m_RefCount <= 0 )
                                {
                                    ClearArguments();
                                    x_throw( "Error: We found a switch:[%s] which can only be use with this other switch:[%s]", (const char*)m_CmdDef[j].m_Name, (const char*)m_CmdDef[Index].m_Name );
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            // Check if option was found
            if( !Found )
            {
                ClearArguments();
                x_throw( "Error: Unable to find a match for this switch [%s]", (const char*)a );
                return;
            }
        }
        else
        {
            // Add to argument list
            m_Arguments.append() = a;

            // make sure that we at least have one command going on
            if( m_Command.getCount() <= 0 )
            {
                // We forgive argument zero because it is the name of the exe
                if( m_Arguments.getCount() > 1 )
                {
                    ClearArguments();
                    x_throw( "Error: Arguments been pass without setting switches Arg:[%s]", (const char*)a );
                }
            }
            else
            {
                // Notify the cmd entry about its new arg
                s32 iCommand = m_Command.getCount()-1;
                m_Command[ iCommand ].m_ArgCount++;

                // Make sure that we have the righ number of maximun arguments
                cmd_def& Def = m_CmdDef[ m_Command[ iCommand ].m_iCmdDef ];
                if( m_Command[ iCommand ].m_ArgCount > Def.m_MaxArgCount && Def.m_MaxArgCount != -1 )
                {
                    ClearArguments();
                    x_throw( "Error: The comman has too many Arguments for the switch:[%s]", (const char*)Def.m_Name );
                }

                // Make sure that the type matches with the expected type
                if( Def.m_Type != TYPE_NONE )
                {
                    const char* pTypeString = NULL;
                    const char* pExpectType = NULL;
                    switch( Def.m_Type )
                    {
                    case TYPE_NONE:
                            ASSERT(0);
                            break;
                    case TYPE_INT:
                        {
                            if( x_isstrint( a ) == TRUE )
                                pTypeString = "INT";
                            pExpectType = "INT";
                            break;
                        }
                    case TYPE_FLOAT:
                        {
                            if( x_isstrfloat( a ) == TRUE )
                                pTypeString = "FLOAT";
                            pExpectType = "FLOAT";
                            break;
                        }
                    case TYPE_GUID:
                        {
                            if( x_isstrguid( a ) == TRUE )
                                pTypeString = "GUID";
                            pExpectType = "GUID";
                            break;
                        }
                    case TYPE_HEX:
                        {
                            if( x_isstrhex( a ) == TRUE )
                                pTypeString = "HEX";
                            pExpectType = "HEX";
                            break;
                        }
                    case TYPE_STRING:
                        {
                            pTypeString = "STRING";

                            // The new version removed quotes for string only
                            if( a[0] == '"' )
                            {
                                s32 l = a.GetLength();
                                for( s32 i=0;i<l; i++ )
                                {
                                    a[i] = a[i+1];
                                }
                                a[l-2] = 0;
                            }
                            break;
                        }
                    case TYPE_STRING_RETAIN_QUOTES:
                        {
                            pTypeString = "STRING_RETAIN_QUOTES";
                            break;
                        }
                    default:
                        {
                            ASSERT(0);
                        }
                    }

                    if( !pTypeString )
                    {
                        ClearArguments();
                        x_throw( "Error: expecting a [%s] but found something different for the argument of the switch:[%s]", pExpectType, (const char*)Def.m_Name );
                    }
                }            
            }
        }
    }

    // check for minimun rage of the last switch (how many arguments can it have)
    if( m_Command.getCount() )
    {
        cmd_entry& Cmd = m_Command[ m_Command.getCount()-1 ];

        if( m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount > Cmd.m_ArgCount )
        {
            ClearArguments();
            x_throw( "Error: We found this switch [%s] had too few arguments we expected at least [%d].", (const char*)m_CmdDef[ Cmd.m_iCmdDef ].m_Name, m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount );
            return;
        }                
    }

    // check whether the command line was specting for a switch but it never happen
    for( i=0; i<m_CmdDef.getCount(); i++ )
    {
        const cmd_def& CmdDef = m_CmdDef[i];
         
        if( CmdDef.m_iParentID != iMainSwitch && 
            CmdDef.m_iParentID != -1 )
            continue;
        
        if( CmdDef.m_RefCount < CmdDef.m_nMinTimes )
        {
            ClearArguments();
            x_throw( "Error: We were expecting this switch[%s] to happen [%d] times, but we found it [%d] times.", (const char*)CmdDef.m_Name, CmdDef.m_nMinTimes, CmdDef.m_RefCount );
            return;
        }
    }
}

//==============================================================================

s32 xcmdline::GetCommandCount( void ) const
{
    return m_Command.getCount();
}

//==============================================================================

s32 xcmdline::GetArgumentCount( void ) const
{
    return m_Arguments.getCount();
}

//==============================================================================

u32 xcmdline::GetCmdCRC( s32 Index ) const
{
    return m_CmdDef[ m_Command[Index].m_iCmdDef ].m_crcName;
}

//==============================================================================

s32 xcmdline::GetCmdArgumentOffset( s32 Index ) const
{
    return m_Command[Index].m_iArg;
}

//==============================================================================

s32 xcmdline::GetCmdArgumentCount ( s32 Index ) const
{
    return m_Command[Index].m_ArgCount;
}

//==============================================================================

const xstring& xcmdline::GetCmdName( s32 Index ) const
{
    return m_CmdDef[ m_Command[Index].m_iCmdDef ].m_Name;
}

//==============================================================================

const xstring& xcmdline::GetArgument( s32 Index ) const
{
    return m_Arguments[Index];
}

//==============================================================================

void xcmdline::ClearArguments( void )
{
    m_bNeedHelp = FALSE;
    m_Arguments.DeleteAllNodes();
    m_Command.DeleteAllNodes();

    for( s32 i=0; i<m_CmdDef.getCount(); i++ )
    {
        m_CmdDef[i].m_RefCount = 0;
    }
}
