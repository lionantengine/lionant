//===============================================================================
// XBASE 
//===============================================================================
inline 
xbase_instance::xbase_instance( void ) :
    m_pDebug(NULL),
    m_pString(NULL),
    m_pMessage(NULL)
{

}

//===============================================================================
// XBASE 
//===============================================================================

xbase_instance&     x_GetXBaseInstance            ( void );
void                x_ThreadInit                  ( void );
void                x_ThreadKill                  ( void );
void                x_InitXBaseForCurrentThread   ( void );
void                x_KillXBaseForCurrentThread   ( void );

//===============================================================================
// END
//===============================================================================
//DOM-IGNORE-END
