
//==============================================================================
// INCLUDES
//==============================================================================
#include <string.h>
#include "../x_base.h"
#include "lz4h/lz4.h"
#include "lz4h/lz4hc.h"


//DOM-IGNORE-BEGIN

//==============================================================================
// table for computation of CRC 32
//==============================================================================

static const u32 s_CrcTab32[256] =
{
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e,
    0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb,
    0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8,
    0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
    0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,
    0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
    0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb,
    0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074,
    0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5,
    0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f, 0x5edef90e,
    0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27,
    0x7d079eb1, 0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0,
    0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1,
    0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
	0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92,
    0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
    0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4,
    0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d,
    0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a,
    0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37,
    0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};
//DOM-IGNORE-END

//==============================================================================
// Types
//==============================================================================

struct platform_info
{
    const char*     m_pString;
    xplatform       m_Platform;
    s32             m_Endian;           // 1 big, 0 small
    xbool           m_bSwapEndian;
};

//--------------------------------------------------------------------------

#ifdef X_LITTLE_ENDIAN
    #define COMPILED_ENDIAN 0
#else
    #define COMPILED_ENDIAN 1
#endif

static const platform_info s_PlatformInfo[]=
{
    { "NULL",       X_PLATFORM_NULL,        0,  COMPILED_ENDIAN != 0   },
    { "PC",         X_PLATFORM_PC,          0,  COMPILED_ENDIAN != 0   },
    { "PS3",        X_PLATFORM_PS3,         1,  COMPILED_ENDIAN != 1   },
    { "360",        X_PLATFORM_360,         1,  COMPILED_ENDIAN != 1   },
    { "RVL",        X_PLATFORM_RVL,         1,  COMPILED_ENDIAN != 1   },
    { "IOS",        X_PLATFORM_IOS,         0,  COMPILED_ENDIAN != 0   },
    { "MARMALADE",  X_PLATFORM_MARMALADE,   0,  COMPILED_ENDIAN != 0   },
    { "OSX",        X_PLATFORM_OSX,         0,  COMPILED_ENDIAN != 0   },
    { "3DS",        X_PLATFORM_3DS,         0,  COMPILED_ENDIAN != 0   },
    { "ANDROID",    X_PLATFORM_ANDROID,     1,  COMPILED_ENDIAN != 1   }
};

#undef COMPILED_ENDIAN

//--------------------------------------------------------------------------

const char* x_PlatformString( xplatform Platform )
{
    ASSERT( Platform > X_PLATFORM_NULL );
    ASSERT( Platform < X_PLATFORM_COUNT );
    ASSERT( s_PlatformInfo[Platform].m_Platform == Platform );
    return s_PlatformInfo[Platform].m_pString;
}

//--------------------------------------------------------------------------

xbool x_PlatformSwapEndian( xplatform Platform )
{
    ASSERT( Platform > X_PLATFORM_NULL );
    ASSERT( Platform < X_PLATFORM_COUNT );
    ASSERT( s_PlatformInfo[Platform].m_Platform == Platform );
    return s_PlatformInfo[Platform].m_bSwapEndian;
}

//--------------------------------------------------------------------------

xbool x_PlatformEndian( xplatform Platform )
{
    ASSERT( Platform > X_PLATFORM_NULL );
    ASSERT( Platform < X_PLATFORM_COUNT );
    ASSERT( s_PlatformInfo[Platform].m_Platform == Platform );
    return s_PlatformInfo[Platform].m_Endian;
}

//------------------------------------------------------------------------------

u16 x_memCRC16( const void* pBuf, s32 Count, u16 crcSum )
{
    ASSERT( pBuf );
    ASSERT( Count > 0 );
    
	crcSum = ~crcSum;
    
    const u8* p = reinterpret_cast<const u8*>(pBuf);
    while (Count)
    {
        const u16 q = *p++ ^ (crcSum >> 8);
        crcSum <<= 8;
        
        u16 r = (q >> 4) ^ q;
        
        crcSum ^= r; r <<= 5;
        crcSum ^= r; r <<= 7;
        crcSum ^= r;
        
        Count--;
    }
    
    return static_cast<u16>(~crcSum);
}

//------------------------------------------------------------------------------

u32 x_memCRC32( const void* pBuf, s32 Count, u32 crcSum )
{
    ASSERT( pBuf );
    ASSERT( Count > 0 );
    
	const u8* const pBuffer = (const u8* )pBuf;
    for ( s32 i = 0; i < Count; i++ )
    {
		crcSum = ( (crcSum >> 8) & 0x00FFFFFF ) ^ s_CrcTab32[ (crcSum ^ pBuffer[i]) & 0xFF ];
	}
    
	return crcSum;
}

//------------------------------------------------------------------------------

void* x_memset( void* pBuf, s32 C, s32 Count )
{
    ASSERT( Count >= 0 );
    return memset( pBuf, C, Count );

}

//------------------------------------------------------------------------------

void* x_memmove( void* dest, const void* src, s32 count )
{
    return  memmove( dest, src, count );
}

//------------------------------------------------------------------------------

void* x_memcpy( void* dest, const void* src, s32 count )
{
    return memcpy( dest, src, count );
}

//------------------------------------------------------------------------------

s32 x_memcmp( const void* pBuf1, const void* pBuf2, s32 Count )
{
    xbyte* p1;
    xbyte* p2;
    xbyte* pEnd;

    ASSERT( pBuf1 );
    ASSERT( pBuf2 );
    ASSERT( Count >= 0 );

    p1   = (xbyte*)pBuf1;
    p2   = (xbyte*)pBuf2;
    pEnd = (xbyte*)pBuf1 + Count;

    //
    // Compare aligned data 32 bits at a time.
    //
    if( ( Count >= 4 ) && (x_IsAlign( p1, 4 ) && x_IsAlign( p2, 4 )) )
    {
        pEnd -= 4;
        while( p1 <= pEnd )
        {
            if( *((u32*)p1) != *((u32*)p2) )
                break;

            p1 += 4;
            p2 += 4;
        }
        pEnd += 4;
    }

    //
    // Compare remaining data one byte at a time.
    //
    while( p1 < pEnd )
    {
        if( *p1 != *p2 )
        {
            if( *p1 < *p2 )  return( -1 );
            else             return(  1 );
        }

        p1++;
        p2++;
    }

    return( 0 );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     x_strHash - perform a 32 bit Fowler/Noll/Vo hash on a buffer
// Arguments:
//	    pStr	 - string to hash
//      Range    - Range of the hash to be return
//	    hVal	 - previous hash value or nothing if first call
// Returns:
//	    32 bit hash as a static hash type
// Description:
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// See Also:
//     x_strHash
//------------------------------------------------------------------------------
u32 x_memHash( void* pBuf, s32 nBytes, u32 Range, u32 hVal )
{
    u8* bp = (u8*)pBuf;	            // start of buffer 
    u8* be = bp + nBytes;		    // beyond end of buffer 

    //
    // FNV-1 hash each octet in the buffer
    //
    while( bp < be ) 
    {
	    // multiply by the 32 bit FNV magic prime mod 2^32 
    	hVal *= 0x01000193;

	    // xor the bottom with the current octet 
	    hVal ^= (s32)*bp++;
    }

    // Do we need to compute a different range?
    if( Range != 0xffffffff )
    {
        const u32 RetryLevel = (0xffffffff/Range)*Range;
        while( hVal >= RetryLevel ) 
        {
            hVal = (hVal * ((u32)16777619UL)) + ((u32)2166136261UL);
        }
        hVal %= Range;
    }

    // return our new hash value 
    return hVal;
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pKey            -  Reference item to search for.
//     pBase           -  First item in the array.
//     NItems          -  Number of items in array.
//     ItemSize        -  Size of one item.
//     pCompare        -  Compare function.
// Returns:
//     The first item in the list which matches will be returned, NULL other wise.
// Summary:
//     Binary search of a sorted array.
// Description:
//     The input to the binary search functions must be a list sorted in ascending 
//     order.  The standard ANSI bsearch function does not allow for duplicates in 
//     the list.  The xBase versions handle duplicates.  The first item in the list 
//     which matches will be returned.
// See Also:
//     x_qsort x_bsearch x_compare_fn
//------------------------------------------------------------------------------
void* x_bsearch( const void*   pKey,
                 const void*   pBase,       
                 s32           NItems,      
                 s32           ItemSize,    
                 x_compare_fn* pCompare )
{
    void* pLocation;
    return( x_bsearch( pKey, pBase, NItems, ItemSize, pCompare, pLocation ) );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Arguments:
//     pKey            -  Reference item to search for.
//     pBase           -  First item in the array.
//     NItems          -  Number of items in array.
//     ItemSize        -  Size of one item.
//     pCompare        -  Compare function.
//     pLocation       -  Where item should be in list.
// Returns:
//     The first item in the list which matches will be returned, NULL other wise.
// Summary:
//     Binary search of a sorted array, with where item should be in the 
//     list in case of a miss
// Description:
//     The input to the binary search functions must be a list sorted in ascending 
//     order.  The standard ANSI bsearch function does not allow for duplicates in 
//     the list.  The xBase versions handle duplicates.  
// See Also:
//     x_qsort x_bsearch x_compare_fn
//------------------------------------------------------------------------------
void* x_bsearch( const void*    pKey,
                 const void*    pBase,       
                 s32            NItems,      
                 s32            ItemSize,    
                 x_compare_fn*  pCompare,
                 void*&         pLocation )
{
    xbyte* pLo;
    xbyte* pHi;
    xbyte* pMid;
    s32   Half1;
    s32   Half2;
    s32   Result;
    
    ASSERT( pKey     );
    ASSERT( pBase    );
    ASSERT( pCompare );
    ASSERT( ItemSize > 0 );

    if( NItems == 0 )
    {
        pLocation = (void*)pBase;
        return( NULL );
    }

    pLocation = NULL;

    pLo = (xbyte*)pBase;
    pHi = pLo + ((NItems-1) * ItemSize);

    while( pLo <= pHi )
    {
        //
        // If there are at least two items in the list, then we want to do the
        // binary search stuff.
        //
        // But if there is only one item, then just do a single compare and be
        // done with it.  The "normal" code path (for 2 or more items) would 
        // work, but it is far less efficient.
        //
        // A "no items in the list" case is impossible based on the nature of
        // the surrounding while loop.
        //

        if( NItems >= 2 )
        {
            Half1  = NItems / 2;
            Half2  = NItems - Half1 - 1;
            pMid   = pLo + (Half1 * ItemSize);

            Result = pCompare( pKey, pMid );

            if( Result < 0 )
            {
                // The mid item is too "high".  
                // Keep searching.  New range is [pLo, pMid-1].
                pHi    = pMid - ItemSize;
                NItems = Half1;
            }

            else if( Result > 0 )
            {
                // The mid item is too "low".  
                // Keep searching.  New range is [pMid+1, pHi].
                pLo    = pMid + ItemSize;
                NItems = Half2;
            }

            else // if( Result == 0 )
            {
                //
                // In a version which is not prepared for duplicates, this case
                // would simply "return( pMid )".
                //
                // Instead, we will continue to search since there may be items
                // just prior to pMid which are the same.  We want to return the
                // first match in the list, not the first match we find.
                //
                // Check the pMid-1 item.  If it doesn't match, then we are 
                // assuredly done.  If it does match, then we will act as if we 
                // had a "<0" return from before.
                //
                // Note that it is impossible for pLo and pMid to be the same at
                // this point.  (Or, more to the point, it is impossible for 
                // pMid-1 to go out of the valid range.)
                //

                Result = pCompare( pKey, pMid-ItemSize );

                ASSERT( Result >= 0 );

                if( Result > 0 )
                {
                    // No match in prior item.  So, pMid is it!
                    return( pMid );
                }
                else
                {
                    // Both pMid and pMid-1 match the desired item!
                    // Keep searching.  New range is [pLo, pMid-1].
                    pHi    = pMid - ItemSize;
                    NItems = Half1;
                }
            }
        }
        else
        {
            // If we are here, then NItems is 1.
            ASSERT( NItems == 1 );

            Result = pCompare( pKey, pLo );
            if( Result == 0 )
            {
                // We got it!
                return( pLo );
            }

            //
            // If we are here, then the desired item is not in the list.  The
            // only thing remaining is to decide where it would go if it were
            // to be added.  The results from the most recent compare tell us
            // all we need to know.
            //

            if( Result > 0 )
            {
                pLocation = pLo + ItemSize;
            }
            else // if( Result < 0 )
            {
                pLocation = pLo;
            }

            // That's it.  The pLocation has been set.  Return NULL to indicate
            // the search failed.
            return( NULL );
        }
    }

    // If we are here, then the desired item is NOT in the list.  And, if it 
    // were to be inserted, it would go at the end of the list.  So, set the
    // pLocation variable and return NULL.
    
    pLocation = pHi + ItemSize;
    return( NULL );
}

//==============================================================================
//  Quick Sort
//==============================================================================
//  
//  This implementation of the quick sort algorithm is, on average, 25% faster 
//  than most "system" versions.  The improvement ranges from a low of 10% to a 
//  high of 50%.
//  
//  Two functions are used to accomplish the sort: x_qsort and PseudoQuickSort.  
//  
//  The PseudoQuickSort function (abbreviated PQS) will only "mostly" sort a 
//  given array of items.  It is recursive, but it is guaranteed that the 
//  function will not recurse more than log base 2 of n times.
//  
//  The x_qsort function will first perform some set up.  It then optionally
//  invokes the PQS function.  Finally, it will perform an optimized insertion 
//  sort to finish off the sort process.
//  
//==============================================================================

//DOM-IGNORE-BEGIN
// Two thresholds are used to optimize the algorithm.  These values are 
// currently tuned for items having an average size of 48 bytes.

#define RECURSION_THRESH  4     //  Items needed to invoke PQS.
#define PARTITION_THRESH  6     //  Items needed to seek partition key.


//------------------------------------------------------------------------------
//
//  What the hell is a PseudoQuickSort?  Simple!  Its "sorta quick sort".
//
//  Find a partition element and put it in the first position of the list.  The 
//  partition value is the median value of the first, middle, and last items.  
//  (Using the median value of these three items rather than just using the 
//  first item is a big win.)
//
//  Then, the usual partitioning and swapping, followed by swapping the 
//  partition element into place.
//
//  Then, of the two portions of the list which remain on either side of the 
//  partition, sort the smaller portion recursively, and sort the larger 
//  portion via another iteration of the same code.
//
//  Do not bother "quick sorting" lists (or sub lists) which have fewer than
//  RECURSION_THRESH items.  All final sorting is handled with an insertion sort
//  which is executed by the caller (x_qsort).  This is another huge win.  This 
//  means that this function does not actually completely sort the input list.  
//  It mostly sorts it.  That is, each item will be within RECURSION_THRESH 
//  positions of its correct position.  Thus, an insert sort will be able to 
//  finish off the sort process without a serious performance hit.
//
//  All data swaps are done in-line, which trades some code space for better
//  performance.  There are only three swap points, anyway.
//
//------------------------------------------------------------------------------

static 
void PseudoQuickSort( xbyte*         pBase,          
                      xbyte*         pMax,           
                      s32            ItemSize,        
                      s32            RecursionThresh,
                      s32            PartitionThresh,
                      x_compare_fn*  pCompare )
{
    xbyte* i;
    xbyte* j;
    xbyte* jj;
    xbyte* mid;
    xbyte* tmp;
    xbyte  c;
    s32    ii;
    s64    lo;
    s64    hi;

    lo = (s32)(pMax - pBase);   // Total data to sort in bytes.

    // Deep breath now...
    do
    {
        //
        // At this point, lo is the number of bytes in the items in the current
        // partition.  We want to find the median value item of the first, 
        // middle, and last items.  This median item will become the middle 
        // item.  Set j to the "greater of first and middle".  If last is larger
        // than j, then j is the median.  Otherwise, compare the last item to 
        // "the lesser of the first and middle" and take the larger.  The code 
        // is biased to prefer the middle over the first in the event of a tie.
        //

        mid = i = pBase + ItemSize * ((u32)(lo / ItemSize) >> 1);

        if( lo >= PartitionThresh )
        {
            j = (pCompare( (jj = pBase), i ) > 0)  ?  jj : i;

            if( pCompare( j, (tmp = pMax - ItemSize) ) > 0 )
            {
                // Use lesser of first and middle.  (First loser.)
                j = (j == jj ? i : jj);
                if( pCompare( j, tmp ) < 0 )
                    j = tmp;
            }

            if( j != i )
            {
                // Swap!
                ii = ItemSize;
                do
                {
                    c    = *i;
                    *i++ = *j;
                    *j++ = c;
                } while( --ii );
            }
        }
                
        //
        // Semi-standard quicksort partitioning / swapping...
        //

        for( i = pBase, j = pMax - ItemSize;  ;  )
        {
            while( (i < mid) && (pCompare( i, mid ) <= 0) )
            {
                i += ItemSize;
            }

            while( j > mid )
            {
                if( pCompare( mid, j ) <= 0 )
                {
                    j -= ItemSize;
                    continue;
                }

                tmp = i + ItemSize;     // Value of i after swap.

                if( i == mid )
                {
                    // j <-> mid, new mid is j.
                    mid = jj = j;       
                }
                else
                {
                    // i <-> j
                    jj = j;
                    j -= ItemSize;
                }

                goto SWAP;
            }

            if( i == mid )
            {
                break;
            }
            else
            {
                jj  = mid;
                tmp = mid = i;
                j  -= ItemSize;
            }
SWAP:
            ii = ItemSize;
            do
            {
                c     = *i;
                *i++  = *jj;
                *jj++ = c;
            } while( --ii );

            i = tmp;
        }

        //
        // Consider the sizes of the two partitions.  Process the smaller
        // partition first via recursion.  Then process the larger partition by
        // iterating through the above code again.  (Update variables as needed
        // to make it work.)
        //
        // NOTE:  Do not bother sorting a given partition, either recursively or
        // by another iteration, if the size of the partition is less than 
        // RECURSION_THRESH items.
        //

        j = mid;
        i = mid + ItemSize;

        if( (lo = (j-pBase)) <= (hi = (pMax-i)) )
        {
            if( lo >= RecursionThresh )
                PseudoQuickSort( pBase, j, 
                                 ItemSize,        
                                 RecursionThresh,
                                 PartitionThresh,
                                 pCompare );
            pBase = i;
            lo    = hi;
        }
        else
        {
            if( hi >= RecursionThresh )
                PseudoQuickSort( i, pMax,
                                 ItemSize,        
                                 RecursionThresh,
                                 PartitionThresh,
                                 pCompare );
            pMax = j;
        }

    } while( lo >= RecursionThresh );
}

//DOM-IGNORE-END
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary: 
//     Optimized quick sort function.
// Arguments:
//     pBase        - Address of first item in array.
//     NItems       - Number of items in array.
//     ItemSize     - Size of one item.
//     pCompare     - Compare function.
// Returns:
//     void
// Description:
//     The qsort function implements a quick-sort algorithm to sort an array of 
//     num elements, each of width bytes. The argument base is a pointer to the base 
//     of the array to be sorted. qsort overwrites this array with the sorted elements. 
//     The argument pCompare is a pointer to a user-supplied routine that compares two 
//     array elements and returns a value specifying their relationship. qsort calls the 
//     compare routine one or more times during the sort, passing pointers to two array 
//     elements on each call: pCompare( (void *) elem1, (void *) elem2 );
//
//     The quick sort function is recursive.  It is guaranteed that it will not 
//     recurse more than "log base 2 of NItems".  Use of x_qsort in a critical 
//     program (such as a game) should be tested under the most extreme potential 
//     conditions to prevent stack overflows.
// 
//     This implementation of the quick sort algorithm is, on average, 25% faster 
//     than most "system" versions.  The improvement ranges from a low of 10% to a 
//     high of 50%.
// See Also:
//     x_compare_fn x_bsearch
//------------------------------------------------------------------------------
void x_qsort( const void*    apBase,     
              s32            NItems,    
              s32            ItemSize,  
              x_compare_fn*  pCompare )  
{
    xbyte  c;
    xbyte* i;
    xbyte* j;
    xbyte* lo;
    xbyte* hi;
    xbyte* min;
    xbyte* pMax;
    xbyte* pBase = (xbyte*)apBase;
    s32    RecursionThresh;          // Recursion threshold in bytes
    s32    PartitionThresh;          // Partition threshold in bytes

    ASSERT( pBase    );
    ASSERT( pCompare );
    ASSERT( NItems   >= 0 );
    ASSERT( ItemSize > 0 );

    // Easy out?
    if( NItems <= 1 )
        return;

    // Set up some useful values.
    RecursionThresh = ItemSize * RECURSION_THRESH;
    PartitionThresh = ItemSize * PARTITION_THRESH;
    pMax            = pBase + (NItems * ItemSize);

    //
    // Set the 'hi' value.
    // Also, if there are enough values, call the PseudoQuickSort function.
    //
    if( NItems >= RECURSION_THRESH )
    {
        PseudoQuickSort( pBase, 
                         pMax, 
                         ItemSize, 
                         RecursionThresh, 
                         PartitionThresh, 
                         pCompare );
        hi = pBase + RecursionThresh;
    }
    else
    {
        hi = pMax;
    }

    //
    // Find the smallest element in the first "MIN(RECURSION_THRESH,NItems)"
    // items.  At this point, the smallest element in the entire list is 
    // guaranteed to be present in this sublist.
    //
    for( j = lo = pBase; (lo += ItemSize) < hi;  )
    {
        if( pCompare( j, lo ) > 0 )
            j = lo;
    }

    // 
    // Now put the smallest item in the first position to prime the next 
    // loop.
    //
    if( j != pBase )
    {
        for( i = pBase, hi = pBase + ItemSize; i < hi;  )
        {
            c    = *j;
            *j++ = *i;
            *i++ = c;
        }
    }

    //
    // Smallest item is in place.  Now we run the following hyper-fast
    // insertion sort.  For each remaining element, min, from [1] to [n-1],
    // set hi to the index of the element AFTER which this one goes.  Then,
    // do the standard insertion sort shift on a byte at a time basis.
    //
    for( min = pBase; (hi = min += ItemSize) < pMax;  )
    {
        while( pCompare( hi -= ItemSize, min ) > 0 )
        {
            // No body in this loop.
        }        

        if( (hi += ItemSize) != min )
        {
            for( lo = min + ItemSize; --lo >= min;  )
            {
                c = *lo;
                for( i = j = lo; (j -= ItemSize) >= hi; i = j )
                {
                    *i = *j;
                }
                *i = c;
            }
        }
    }
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     The C++ ICE Encryption Class
// 
// Description:
//     The IceKey class is used for encrypting and decrypting 64-bit blocks of data 
//     with the ICE (Information Concealment Engine) encryption algorithm.
// 
//     The constructor creates a new IceKey object that can be used to encrypt and 
//     decrypt data. The level of encryption determines the size of the key, and hence 
//     its speed. Level 0 uses the Thin-ICE variant, which is an 8-round cipher taking 
//     an 8-byte key. This is the fastest option, and is generally considered to be at 
//     least as secure as DES, although it is not yet certain whether it is as secure as 
//     its key size.
//
//     For levels n greater than zero, a 16n-round cipher is used, taking 8n-byte keys. 
//     Although not as fast as level 0, these are very very secure.
// 
//     Before an IceKey can be used to encrypt data, its key schedule must be set with 
//     the set() member function. The length of the key required is determined by the level, 
//     as described above.
// 
//     The member functions encrypt() and decrypt() encrypt and decrypt respectively data 
//     in blocks of eight characters, using the specified key.
// 
//     Two functions keySize() and blockSize() are provided which return the key and block 
//     size respectively, measured in bytes. The key size is determined by the level, while 
//     the block size is always 8.
// 
//     The destructor zeroes out and frees up all memory associated with the key. 
// 
//     for more information : http://www.darkside.com.au/ice/
// 
//------------------------------------------------------------------------------
class xicekey 
{
public:
                    xicekey         ( s32 Level );
                   ~xicekey         ( void );

    void		    Set             ( const void* pKey );

    void		    Encrypt         ( void* pDest, const void* pSrc ) const;
    void		    Decrypt         ( void* pDest, const void* pSrc ) const;

    s32		        GetKeySize      ( void ) const;
    s32		        GetBlockSize    ( void ) const;

protected:

    struct icesubkey 
    {
        u32 m_Val[3];
    };

protected:

    void	        ScheduleBuild ( u16* k, int n, const s32* keyrot );
    u32             Ice_f         ( s32 p, const icesubkey* sk ) const;

protected:

    s32		        m_Size;
    s32		        m_Rounds;
    icesubkey*      m_pKeysched;
};

//------------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------------

// The S-boxes 
static u32	    s_ice_sbox[4][1024];
static xbool	s_ice_sboxes_initialised = FALSE;


// Modulo values for the S-boxes 
static const s32 s_ice_smod[4][4] = 
{
    {333, 313, 505, 369},
    {379, 375, 319, 391},
    {361, 445, 451, 397},
    {397, 425, 395, 505}
};

// XOR values for the S-boxes 
static const s32 s_ice_sxor[4][4] = 
{
    {0x83, 0x85, 0x9b, 0xcd},
    {0xcc, 0xa7, 0xad, 0x41},
    {0x4b, 0x2e, 0xd4, 0x33},
    {0xea, 0xcb, 0x2e, 0x04}
};

// Permutation values for the P-box 
static const u32	s_ice_pbox[32] = 
{
    0x00000001, 0x00000080, 0x00000400, 0x00002000,
    0x00080000, 0x00200000, 0x01000000, 0x40000000,
    0x00000008, 0x00000020, 0x00000100, 0x00004000,
    0x00010000, 0x00800000, 0x04000000, 0x20000000,
    0x00000004, 0x00000010, 0x00000200, 0x00008000,
    0x00020000, 0x00400000, 0x08000000, 0x10000000,
    0x00000002, 0x00000040, 0x00000800, 0x00001000,
    0x00040000, 0x00100000, 0x02000000, 0x80000000
};

// The key rotation schedule
static const s32    s_ice_keyrot[16] = 
{
    0, 1, 2, 3, 2, 1, 3, 0,
    1, 3, 2, 0, 3, 1, 0, 2
};

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// 8-bit Galois Field multiplication of a by b, modulo m.
// Just like arithmetic multiplication, except that additions and
// subtractions are replaced by XOR.
static 
u32 gf_mult ( u32 a, u32 b, u32	m ) 
{
    u32 res = 0;

    while( b ) 
    {
        if (b & 1)
            res ^= a;

        a <<= 1;
        b >>= 1;

        if (a >= 256)
            a ^= m;
    }

    return res;
}

//------------------------------------------------------------------------------
// Galois Field exponentiation.
// Raise the base to the power of 7, modulo m.
static 
u32 gf_exp7( u32 b, u32 m ) 
{
    u32	x;

    if (b == 0)
        return (0);

    x = gf_mult (b, b, m);
    x = gf_mult (b, x, m);
    x = gf_mult (x, x, m);
    return gf_mult (b, x, m);
}



//------------------------------------------------------------------------------
//Carry out the ICE 32-bit P-box permutation.
static 
u32 ice_perm32( u32 x ) 
{
    u32		    res  = 0;
    const u32*  pbox = s_ice_pbox;

    while (x) 
    {
        if (x & 1)
            res |= *pbox;
        pbox++;
        x >>= 1;
    }

    return res;
}


//------------------------------------------------------------------------------
// Initialize the ICE S-boxes.
// This only has to be done once.
static 
void ice_sboxes_init( void )
{
    s32 i;

    for( i=0; i<1024; i++ ) 
    {
        s32	col = (i >> 1) & 0xff;
        s32	row = (i & 0x1) | ((i & 0x200) >> 8);
        u32 x;

        x = gf_exp7 (col ^ s_ice_sxor[0][row], s_ice_smod[0][row]) << 24;
        s_ice_sbox[0][i] = ice_perm32 (x);

        x = gf_exp7 (col ^ s_ice_sxor[1][row], s_ice_smod[1][row]) << 16;
        s_ice_sbox[1][i] = ice_perm32 (x);

        x = gf_exp7 (col ^ s_ice_sxor[2][row], s_ice_smod[2][row]) << 8;
        s_ice_sbox[2][i] = ice_perm32 (x);

        x = gf_exp7 (col ^ s_ice_sxor[3][row], s_ice_smod[3][row]);
        s_ice_sbox[3][i] = ice_perm32 (x);
    }
}

//------------------------------------------------------------------------------
// Create a new ICE key.
xicekey::xicekey( s32 n )
{
    if( !s_ice_sboxes_initialised ) 
    {
        ice_sboxes_init ();
        s_ice_sboxes_initialised = TRUE;
    }

    if (n < 1) 
    {
        m_Size   = 1;
        m_Rounds = 8;
    } 
    else 
    {
        m_Size   = n;
        m_Rounds = n * 16;
    }

    m_pKeysched = (icesubkey*)x_malloc( sizeof(icesubkey), m_Rounds, 0 );
    ASSERT(m_pKeysched);
}

//------------------------------------------------------------------------------
// Destroy an ICE key.
xicekey::~xicekey( void )
{
    s32	i, j;

    for( i=0; i<m_Rounds; i++ )
    {
        for (j=0; j<3; j++)
        {
            m_pKeysched[i].m_Val[j] = 0;
        }
    }

    m_Rounds = m_Size = 0;

    x_free( m_pKeysched );
    m_pKeysched = NULL;
}


//------------------------------------------------------------------------------
// The single round ICE f function.
u32 xicekey::Ice_f( s32 p, const xicekey::icesubkey* sk ) const
{
    u32	tl, tr;		// Expanded 40-bit values 
    u32	al, ar;		// Salted expanded 40-bit values 

    // Left half expansion 
    tl = ((p >> 16) & 0x3ff) | (((p >> 14) | (p << 18)) & 0xffc00);

    // Right half expansion 
    tr = (p & 0x3ff) | ((p << 2) & 0xffc00);

    // Perform the salt permutation 
    // al = (tr & sk->m_Val[2]) | (tl & ~sk->m_Val[2]);
    // ar = (tl & sk->m_Val[2]) | (tr & ~sk->m_Val[2]);
    al = sk->m_Val[2] & (tl ^ tr);
    ar = al ^ tr;
    al ^= tl;

    al ^= sk->m_Val[0];		// XOR with the subkey 
    ar ^= sk->m_Val[1];

    // S-box lookup and permutation 
    return (s_ice_sbox[0][al >> 10] | s_ice_sbox[1][al & 0x3ff] | 
            s_ice_sbox[2][ar >> 10] | s_ice_sbox[3][ar & 0x3ff]);
}


//------------------------------------------------------------------------------
// Encrypt a block of 8 bytes of data with the given ICE key.
void xicekey::Encrypt( void* pDest, const void* pSrc ) const
{
    s32	i;
    u32	l, r;
    const xbyte* ptext = (const xbyte*)pSrc;
    xbyte*       ctext = (xbyte*)pDest;

    l = (((u32) ptext[0]) << 24)
        | (((u32) ptext[1]) << 16)
        | (((u32) ptext[2]) << 8) | ptext[3];
    r = (((u32) ptext[4]) << 24)
        | (((u32) ptext[5]) << 16)
        | (((u32) ptext[6]) << 8) | ptext[7];

    for (i = 0; i < m_Rounds; i += 2) 
    {
        l ^= Ice_f (r, &m_pKeysched[i]);
        r ^= Ice_f (l, &m_pKeysched[i + 1]);
    }

    for (i = 0; i < 4; i++) 
    {
        ctext[3 - i] = r & 0xff;
        ctext[7 - i] = l & 0xff;

        r >>= 8;
        l >>= 8;
    }
}


//------------------------------------------------------------------------------
//
// Decrypt a block of 8 bytes of data with the given ICE key.
//
void xicekey::Decrypt( void* pDest, const void* pSrc ) const
{
    const xbyte* ctext = (const xbyte*)pSrc;
    xbyte* ptext       = (xbyte*)pDest;

    s32 i;
    u32 l, r;

    l = (((u32) ctext[0]) << 24)
        | (((u32) ctext[1]) << 16)
        | (((u32) ctext[2]) << 8) | ctext[3];
    r = (((u32) ctext[4]) << 24)
        | (((u32) ctext[5]) << 16)
        | (((u32) ctext[6]) << 8) | ctext[7];

    for (i = m_Rounds - 1; i > 0; i -= 2) 
    {
        l ^= Ice_f (r, &m_pKeysched[i]);
        r ^= Ice_f (l, &m_pKeysched[i - 1]);
    }

    for (i = 0; i < 4; i++) 
    {
        ptext[3 - i] = r & 0xff;
        ptext[7 - i] = l & 0xff;

        r >>= 8;
        l >>= 8;
    }
}

//------------------------------------------------------------------------------
// Set 8 rounds [n, n+7] of the key schedule of an ICE key.
void xicekey::ScheduleBuild( u16* kb, s32 n, const s32* keyrot ) 
{
    s32 i;

    for (i=0; i<8; i++) 
    {
        s32	        j;
        s32	        kr = keyrot[i];
        icesubkey*  isk = &m_pKeysched[n + i];

        for (j=0; j<3; j++)
        {
            isk->m_Val[j] = 0;
        }

        for (j=0; j<15; j++) 
        {
            s32	 k;
            u32* curr_sk = &isk->m_Val[j % 3];

            for (k=0; k<4; k++) 
            {
                u16* curr_kb = &kb[(kr + k) & 3];
                s32	 bit = *curr_kb & 1;

                *curr_sk = (*curr_sk << 1) | bit;
                *curr_kb = (*curr_kb >> 1) | ((bit ^ 1) << 15);
            }
        }
    }
}


//------------------------------------------------------------------------------
// Set the key schedule of an ICE key.
void xicekey::Set( const void* pKey ) 
{
    s32 i;

    const xbyte* key = (const xbyte*)pKey;

    if( m_Rounds == 8 ) 
    {
        u16 kb[4];

        for (i=0; i<4; i++)
        {
            kb[3 - i] = (key[i*2] << 8) | key[i*2 + 1];
        }

        ScheduleBuild (kb, 0, s_ice_keyrot );
        return;
    }

    for( i=0; i<m_Size; i++ ) 
    {
        s32 j;
        u16	kb[4];

        for (j=0; j<4; j++)
        {
            kb[3 - j] = (key[i*8 + j*2] << 8) | key[i*8 + j*2 + 1];
        }

        ScheduleBuild (kb, i*8, s_ice_keyrot);
        ScheduleBuild (kb, m_Rounds - 8 - i*8, &s_ice_keyrot[8]);
    }
}


//------------------------------------------------------------------------------
// Return the key size, in bytes.
s32 xicekey::GetKeySize( void ) const
{
    return m_Size * 8;
}


//------------------------------------------------------------------------------
// Return the block size, in bytes.
s32 xicekey::GetBlockSize( void ) const
{
    return 8;
}


//------------------------------------------------------------------------------
s32 x_EncryptComputeKeySize( s32 Level )
{
    s32 Size;
    if (Level < 1) 
    {
        Size   = 1;
    } 
    else 
    {
        Size   = Level;
    }

    return Size * 8;
}

//------------------------------------------------------------------------------
s32 x_EncryptComputeDestDataSize( s32 SrcDataSize )
{
    return x_Align( SrcDataSize, 8 );
}

//------------------------------------------------------------------------------
void x_EncryptMem( void* pDest, s32 DestDataSize, s32 Level, const void* pKey, s32 KeySize, const void* pSrcData, s32 SrcDataSize )
{
    ASSERT( Level >= 0 );
    ASSERT( KeySize == x_EncryptComputeKeySize(Level) );
    ASSERT( DestDataSize >= 0 );
    ASSERT( DestDataSize >= SrcDataSize );
    ASSERT( DestDataSize == x_EncryptComputeDestDataSize(SrcDataSize) );
    ASSERT( pDest );
    ASSERT( pSrcData );

    xicekey     IceKey( Level );
    s32         i;
    xbyte*      pIn  = (xbyte*)pSrcData;
    xbyte*      pOut = (xbyte*)pDest;

    // Set the key to Encrypt with 
    IceKey.Set( pKey );

    // Encrypt 8 bytes at a time
    SrcDataSize-=8;
    for( i=0; i<SrcDataSize; i+=8 )
    {
        IceKey.Encrypt( &pOut[i], &pIn[i] );
    }
    SrcDataSize+=8;

    // Encrypt the last bytes
    u8 Pad[8];
    for( s32 j=0; j<8; j++ )
    {
        if( j+i < SrcDataSize )
        {
            Pad[j] = pIn[j+i];
        }
        else
        {
            Pad[j] = 0;
        }
    }
    IceKey.Encrypt( &pOut[i], Pad );
}

//------------------------------------------------------------------------------
void x_DencryptMem( void* pDest, s32 DestDataSize, s32 Level, const void* pKey, s32 KeySize, const void* pSrcData, s32 SrcDataSize )
{
    ASSERT( Level >= 0 );
    ASSERT( KeySize == x_EncryptComputeKeySize(Level) );
    ASSERT( DestDataSize >= 0 );
    ASSERT( SrcDataSize >= DestDataSize );
    ASSERT( x_IsAlign( SrcDataSize, 8 ) );
    ASSERT( pDest );
    ASSERT( pSrcData );

    xicekey     IceKey( Level );
    s32         i;
    xbyte*      pIn  = (xbyte*)pSrcData;
    xbyte*      pOut = (xbyte*)pDest;

    IceKey.Set( pKey );

    // decrypt 8 bytes at a time 
    SrcDataSize-=8;
    for( i=0; i<SrcDataSize; i+=8 )
    {
        IceKey.Decrypt( &pOut[i], &pIn[i] );
    }
    //SrcDataSize+=8;

    // decrypt any reminding bytes
    u8 Pad[8];
    IceKey.Decrypt( &Pad, &pIn[i] );
    for( s32 j=0; i<DestDataSize; j++, i++ )
    {
        pOut[i] = Pad[j];
    }
}

//------------------------------------------------------------------------------
void x_EncryptBuildKeyFromPassword( void* pKey, s32 KeySize, s32 Level, const char* pPassWord )
{
   s32  i = 0;

   ASSERT( KeySize == x_EncryptComputeKeySize(Level) );
   ASSERT( pPassWord );
   ASSERT( pKey );

   x_memset( pKey, 0, KeySize );

   xbyte* buf = (xbyte*)pKey;
   while( *pPassWord != '\0' ) 
   {
       xbyte	c = *pPassWord & 0x7f;
       int		idx = i / 8;
       int		bit = i & 7;

       if( idx >= KeySize )
           return;

       if (bit == 0) 
       {
           buf[idx] = (c << 1);
       } 
       else if (bit == 1) 
       {
           buf[idx] |= c;
       } 
       else 
       {
           buf[idx] |= (c >> (bit - 1));

           if( (idx+1) >= KeySize )
               return;
           buf[idx + 1] = (c << (9 - bit));
       }

       i += 7;
       pPassWord++;
   }
}

//------------------------------------------------------------------------------

void x_EncryptBuildRandomKey( void* pKey, s32 KeySize, s32 Level )
{
    ASSERT( KeySize == x_EncryptComputeKeySize(Level) );
    ASSERT( pKey );

    xbyte* buf = (xbyte*)pKey;
    for( s32 i=0; i<KeySize; i++ )
    {
        buf[i] = (xbyte)x_irand(0, 0xff);
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Compression rutines
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Decompressmem
//------------------------------------------------------------------------------
void x_DecompressMem ( void* pDestUncompress, s32 DestSize, const void* pSrcCompressBlock, s32 SrcSize )
{
    if( DestSize == SrcSize )
    {
        x_memcpy( pDestUncompress, pSrcCompressBlock, SrcSize );
    }
    else
    {
        s32 Total = LZ4_decompress_fast( (const char*)pSrcCompressBlock, (char*)pDestUncompress, DestSize );
        (void)Total;
        ASSERT( SrcSize == Total  );
    }
}


//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------
s32 x_CompressMem( xptr<xbyte>& DestCompress, s32 DestOffset, const void* pSrcUncompressBlock, s32 SrcSize )
{
    ASSERT( DestOffset >= 0 );
    ASSERT( pSrcUncompressBlock );
    ASSERT( SrcSize > 0 );

    s32 Length = LZ4_compressHC_limitedOutput( (const char*) pSrcUncompressBlock, (char*)&DestCompress[DestOffset], SrcSize, SrcSize );
    ASSERT( Length <= DestCompress.getCount() );

    // Let the user deal with the problem
    if( Length >= SrcSize || Length == 0 )
    {
        x_memcpy( &DestCompress[DestOffset], pSrcUncompressBlock, SrcSize );
        return SrcSize;
    }
    
    return Length;
}

//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------
s32 x_CompressMem( xptr<xbyte>& DestCompress, const void* pSrcUncompressBlock, s32 SrcSize )
{
    return x_CompressMem( DestCompress, 0, pSrcUncompressBlock, SrcSize );
}








/*
// OLD COMPRESSION FOR X_BASE IS DEPREDICATED... DUE TO LICENSE ISSUES AND PERFORMANCE
 
 #include "lzo/minilzo.h"

//------------------------------------------------------------------------------
// Decompressmem
//------------------------------------------------------------------------------
void x_DecompressMem ( void* pDestUncompress, s32 DestSize, const void* pSrcCompressBlock, s32 SrcSize )
{
    lzo_uint DestLength;
    lzo1x_decompress_safe( (const xbyte*)pSrcCompressBlock, SrcSize, (xbyte*)pDestUncompress, &DestLength, (lzo_uint*)NULL );
    ASSERT( DestSize == DestLength );
}

//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------
s32 x_CompressMem( xptr<xbyte>& DestCompress, const void* pSrcUncompressBlock, s32 SrcSize )
{
    xptr<lzo_align_t>   WrkMem;
    s32                 Length = SrcSize + SrcSize / 16 + 64 + 3; //lzo_uint

    // temporary work memory for the compression
    WrkMem.Alloc( ((LZO1X_1_MEM_COMPRESS) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) );

    if( DestCompress.getCount() < Length )
    {
        DestCompress.Destroy();
        DestCompress.Alloc( Length );
    }

    lzo_uint LOut;
    lzo1x_1_compress( (const xbyte*) pSrcUncompressBlock, SrcSize, (xbyte*)&DestCompress[0], &LOut, &WrkMem[0] );
    Length = (s32)LOut;

    return Length;
}

//------------------------------------------------------------------------------

s32 x_CompressMem( xptr<xbyte>& DestCompress, s32 Offset, const void* pSrcUncompressBlock, s32 SrcSize )
{
    ASSERT( Offset >= 0 );
    ASSERT( pSrcUncompressBlock );
    ASSERT( SrcSize > 0 );

    xptr<lzo_align_t>   WrkMem;
    s32                 Length = SrcSize + SrcSize / 16 + 64 + 3; //lzo_uint

    // temporary work memory for the compression
    WrkMem.Alloc( ((LZO1X_1_MEM_COMPRESS) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) );

    // Not sufficient memory for compression
    ASSERT( (Offset+DestCompress.getCount()) >= Length );

    lzo_uint LOut;
    lzo1x_1_compress( (const xbyte*) pSrcUncompressBlock, SrcSize, (xbyte*)&DestCompress[Offset], &LOut, &WrkMem[0] );
    Length = (s32)LOut;

    return Length;
}
 */
