//==============================================================================
// TODO:
// Like always happens one inplemented there is yet a better way to do it.
// Okay here is how version 2 needs to look for this:
// The only part that readlly needs to change is the reading part.
// For optimal performance in consoles you want to read blocks of a particular size
// the reasons why this is it is because we want to try to bipass their internal buffer
// because they will not buy us anything. This can be done by including the flag FILE_FLAG_NO_BUFFERING
// in the create file funtion. This should improve memory usage and memory copies. 
// Because of this we need to decaple the read buffers from the compress block size.
// That means the that read and the seek function may need to change but not allot.
// I dont want to implement this I want to collect more updates and do all at the same time.
//==============================================================================
//DOM-IGNORE-BEGIN

//==============================================================================
// INCLUDES
//==============================================================================
#include <stdio.h>
#if !defined(TARGET_3DS) && !defined(TARGET_PC)
    #include <unistd.h>
#endif
#include "../x_base.h"
#include "lz4h/lz4.h"
#include "lz4h/lz4hc.h"

//==============================================================================
// TYPES
//==============================================================================

struct read_block
{
    s32             m_VirtualOffset;
    s32             m_Size;             // lzo_uint
    s32             m_iBlock;
    xptr<xbyte>     m_Buffer;

    read_block( void )
    {
        m_VirtualOffset = 0;
        m_Size          = 0;
        m_iBlock        = 0;
    }
};

struct compress_file_header
{
    u32             m_Signature;                                // Signature of the file
    s32             m_UncompressFileSize;                       // Uncompress file size
    s32             m_BlockSize;                                // How big are blocks in the compress file
    s32             m_nBlocks;                                  // Number of blocks in the file. The last block may not be BlocksSize big.
};

struct xfile_compress
{
    // This file is the one that we really used in the operations. 
    xfile                       m_ActiveFile;                     

    // This member variables are used for writing. All writing happens in a temporary file
    // the ones the user closes the file then it compresses all the data at that point.
    // this allows the user to have free direct access to the data all the way to the end.
    // For appending files the file is actually decompress and put into the temporary file.
    // So user can still access old data as he pleases. 
    xbool                       m_bWriting;                     // File first get written without compression then at close file the file is compress
    xstring                     m_FileName;                     // File name for the temporary file
    void*                       m_pFile;                        // This variables are the back up of the original device. Basically the compress
    xfile_device_i*             m_pDevice;                      // device works like a fake device. So it "shadows" the original device with it own
                                                                // but it will still access the original device when it needs.    

    // These variables are for reading.
    // Reading happens in a 4 buffer and 3 parallel pipes. The 3 pipes are:
    // Pipe 1) DVD-to->DVDBuffer[x]                 : Asynchronous reading from the DVD to the buffer
    // Pipe 2) DVDBuffer[x+1]-to->UserBuffer[x+1]   : Uncompressing step (For now this will be in the same thread as the user access. This should be move into its own worker thread
    // Pipe 3) UserBuffer[x]-to->User               : Copying uncompress data to user step
    // In the future we can have an option to collapse Pipe2 & Pipe3 when the user simply
    // wants the hold file to be dump into a buffer
    s32                         m_iUser;                        // Index of buffer that user has access
    s32                         m_iDVD;                         // Index of buffer which the DVD reading to
    xsafe_array<read_block,2>   m_DVDBuffer;                    // When reading we double buffer our data
    xsafe_array<read_block,2>   m_UserBuffer;                   // When reading we double buffer our data
    s32                         m_iVirtualCursor;               // This is the cursor where the user is reading from
    compress_file_header        m_Header;                       // Header for the file we are reading
    xsafe_array<s32,256>        m_BlockSize;                    // The block size palette
};

//------------------------------------------------------------------------------

class xfile_compression_device : public xfile_device_i
{
public:
                    xfile_compression_device ( void ) : xfile_device_i("compression:")
                    { 
                    }

public:

    virtual void*               Open        ( const char* pFileName, u32 Mode ) { ASSERT(0); return FALSE; }
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual void                AsyncAbort  ( void* pFile );

public:

    xharray<xfile_compress>     m_File;
};


//------------------------------------------------------------------------------

void xfile_compression_device::Close( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    //
    // Do all file processing
    //
    {
        xfile_compress& CFile = m_File(Handle);
        xptr_lock            Ref( m_File );
        
        //
        // Compress file here.
        // The compressor will work in parallel with the file system for reading,
        // the writing can also work in parallel but it depends on the user to add
        // the async mode. We assume here that when two async commands are issue
        // if the first one didn't finish the second one will block until first one 
        // finish.
        //
        if( CFile.m_bWriting )
        {
            // Lets reopen our file with async reading
            s32 Length = CFile.m_ActiveFile.GetFileLength();
            CFile.m_ActiveFile.Close();

            if( CFile.m_ActiveFile.Open( CFile.m_FileName, "r@" ) == FALSE )
                x_throw( "Fail while writting a file [%s]", (const char*)CFile.m_FileName );
            
            // Write header
            CFile.m_Header.m_Signature          = 'LACF';
            CFile.m_Header.m_UncompressFileSize = Length;
            CFile.m_Header.m_BlockSize          = x_Max( 16*1024, x_Align(Length/256,16*1024) );
            CFile.m_Header.m_nBlocks            = Length/CFile.m_Header.m_BlockSize;
            CFile.m_Header.m_nBlocks           += CFile.m_Header.m_nBlocks*CFile.m_Header.m_BlockSize != Length?1:0;
            CFile.m_pDevice->Write( CFile.m_pFile, &CFile.m_Header, sizeof(CFile.m_Header));

            // Create the compress table for all the sizes that we will need
            xptr<s32>   SizeTable;
            s32         iSize=0;
            SizeTable.Alloc( CFile.m_Header.m_nBlocks );

            // We preallocate the space for the table
            CFile.m_pDevice->Write( CFile.m_pFile, &SizeTable[0], CFile.m_Header.m_nBlocks*sizeof(s32));

            // Allocate work memory
            xsafe_array<xptr<xbyte>,2>  Out;
            xsafe_array<xptr<xbyte>,2>  In;
            xsafe_array<s32,2>          InL;
            xsafe_array<s32,2>          OutL;
            s32                         t = 0;
            const s32                   OutLen = CFile.m_Header.m_BlockSize;

            Out[0].Alloc   ( OutLen );
            Out[1].Alloc   ( OutLen );
            In[0].Alloc    ( CFile.m_Header.m_BlockSize );
            In[1].Alloc    ( CFile.m_Header.m_BlockSize );
            
            // Read a block
            InL[t&1]  = (s32)x_Min( CFile.m_Header.m_BlockSize, Length );
            CFile.m_ActiveFile.ReadRaw( &In[t&1][0], 1, InL[t&1] ); 
            
            Length-= InL[t&1];

            while( Length > 0 )
            {
                
                CFile.m_ActiveFile.Synchronize( TRUE );
                
                // Read next block
                InL[1-(t&1)]  = x_Min( (s32)CFile.m_Header.m_BlockSize, Length );
                CFile.m_ActiveFile.ReadRaw( &In[1-(t&1)][0], 1, InL[1-(t&1)] ); 

                // Compress block
                OutL[t&1] = x_CompressMem( Out[t&1], 0, &In[t&1][0], InL[t&1] );

                // Write the block out
                SizeTable[iSize++] = OutL[t&1];
                CFile.m_pDevice->Write( CFile.m_pFile, &Out[t&1][0], OutL[t&1] );

                // Get ready for the next one
                t++;

                // Done reading this one too
                Length -= InL[t&1];
            }

            //
            // Output the last block
            //

            // Sync for the last block
            CFile.m_ActiveFile.Synchronize( TRUE );

            // Compress block
            OutL[t&1] = x_CompressMem( Out[t&1], 0, &In[t&1][0], InL[t&1] );
            
            // Write the block out
            SizeTable[iSize++] = OutL[t&1];
            CFile.m_pDevice->Write( CFile.m_pFile, &Out[t&1][0], OutL[t&1] );

            //
            // Lets write the table for this file
            //
            ASSERT( CFile.m_Header.m_nBlocks == iSize );
            CFile.m_pDevice->Seek( CFile.m_pFile, xfile_device_i::SKM_ORIGIN, sizeof(compress_file_header) );
            CFile.m_pDevice->Write( CFile.m_pFile, &SizeTable[0], CFile.m_Header.m_nBlocks*sizeof(s32));

            // Make sure that we are all sync for the writting as well
            CFile.m_pDevice->Synchronize( CFile.m_pFile, TRUE );

            // Close temp file
            CFile.m_ActiveFile.Close();

            //
            // Okay lets nuke the temporary file
            //
#ifdef TARGET_PC
            _unlink( CFile.m_FileName );
#else
            ASSERT(false && "Need to be solved here");
#endif

            //
            // Done
            //
            CFile.m_pDevice->Close( CFile.m_pFile );
        }
    }

    //
    // Clear our entry
    //
    m_File.DeleteByHandle( Handle );
}

//------------------------------------------------------------------------------

xbool xfile_compression_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.ReadRaw( pBuffer, 1, Count );
    }
    else
    {
        while( Count )
        {
            //
            // Try to copy as much data into the user buffer as we can
            //
            s32 Offset = CFile.m_iVirtualCursor - CFile.m_UserBuffer[ CFile.m_iUser&1 ].m_VirtualOffset;
            ASSERT( Offset >= 0 );

            if( Offset >= (s32)CFile.m_UserBuffer[ CFile.m_iUser&1 ].m_Size || Offset < 0 )
            {
                //
                // Okay start reading the next chuck
                //
                CFile.m_iDVD++;
                read_block& PrevDVD = CFile.m_DVDBuffer[(CFile.m_iDVD+1)&1];
                read_block& NextDVD = CFile.m_DVDBuffer[CFile.m_iDVD&1];

                // If we dont know which other block to read then lets read ahead
                if( NextDVD.m_Size == -1 )
                {
                    NextDVD.m_iBlock        = PrevDVD.m_iBlock + 1;

                    // Read more if we can if not just give up
                    if( NextDVD.m_iBlock < CFile.m_Header.m_nBlocks )
                    {
                        NextDVD.m_VirtualOffset = PrevDVD.m_VirtualOffset + CFile.m_Header.m_BlockSize;
                        NextDVD.m_Size          = CFile.m_BlockSize[ NextDVD.m_iBlock ];
                    }
                    else
                    {
                        // end of file
                        NextDVD.m_VirtualOffset = -1;
                        NextDVD.m_Size          = -1;
                    }

                    // Make the next block be unkown
                    PrevDVD.m_Size = -1;
                }

                // Handle the read part of the show
                if( NextDVD.m_Size != -1 )
                {
                    if( CFile.m_ActiveFile.ReadRaw( &NextDVD.m_Buffer[0], sizeof(xbyte), NextDVD.m_Size ) == FALSE )
                        return FALSE;
                }
                else
                {
                    // Make sure to sync with the previous read so that we can start decompressing the block
                    if( CFile.m_ActiveFile.Synchronize( TRUE ) < xfile::SYNC_COMPLETED )
                        return FALSE;

                }

                //
                // next stage of the pipe is to decompress the chunk
                //
                CFile.m_iUser++;
                read_block& NextUser = CFile.m_UserBuffer[CFile.m_iUser&1];

                // The next block that the user will have access will start at this virtual offset
                NextUser.m_VirtualOffset = PrevDVD.m_VirtualOffset;
                
                // Uncompress next block for the user
                {
                    // Are we at the last block?
                    const s32 iBlock = NextUser.m_VirtualOffset / CFile.m_Header.m_BlockSize;
                    if( iBlock == (CFile.m_Header.m_nBlocks-1) )
                    {
                        const s32 LastBlockSize = CFile.m_Header.m_UncompressFileSize - (iBlock*CFile.m_Header.m_BlockSize);
                        x_DecompressMem ( &NextUser.m_Buffer[0], LastBlockSize, &PrevDVD.m_Buffer[0], CFile.m_BlockSize[ PrevDVD.m_iBlock ] );
                        NextUser.m_Size = LastBlockSize;
                    }
                    else
                    {
                        x_DecompressMem ( &NextUser.m_Buffer[0], CFile.m_Header.m_BlockSize, &PrevDVD.m_Buffer[0], CFile.m_BlockSize[ PrevDVD.m_iBlock ] );
                        NextUser.m_Size = CFile.m_Header.m_BlockSize;
                    }
                }

                //
                // Recompute the offset
                //
                Offset = CFile.m_iVirtualCursor - CFile.m_UserBuffer[ CFile.m_iUser&1 ].m_VirtualOffset;
            }

            // Make sure that the buffer we just finish reading is what we expected
            read_block& NextUser = CFile.m_UserBuffer[CFile.m_iUser&1];
            ASSERT( Offset >= 0 );
            ASSERT( Offset <  (s32)NextUser.m_Size );

            //
            // Give the user as much data as we can from the current buffer
            //
            s32 nBytes = x_Min( Count, (s32)NextUser.m_Size - Offset );
            ASSERT( NextUser.m_Buffer[Offset] | 1 );
            ASSERT( NextUser.m_Buffer[Offset+nBytes-1] | 1 );
            ASSERT( nBytes <= Count );
            x_memcpy( pBuffer, &NextUser.m_Buffer[Offset], nBytes );

            // Move the buffer pointer
            pBuffer = &(((char*)pBuffer)[nBytes]);

            // Increment cursors
            CFile.m_iVirtualCursor += nBytes;
            Count -= nBytes;
        }
    }   

    return TRUE;
}

//------------------------------------------------------------------------------

void xfile_compression_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        CFile.m_ActiveFile.WriteRaw( pBuffer, 1, Count );
    }
    else
    {
        ASSERT(0);
    }        
}

//------------------------------------------------------------------------------

void xfile_compression_device::Seek( void* pFile, seek_mode Mode, s32 Pos )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        switch( Mode )
        {
        case SKM_ORIGIN:    CFile.m_ActiveFile.SeekOrigin ( Pos ); break;
        case SKM_CURENT:    CFile.m_ActiveFile.SeekCurrent( Pos ); break;
        case SKM_END:       CFile.m_ActiveFile.SeekEnd    ( Pos ); break;
        }        
    }
    else
    {
        switch( Mode )
        {
        case SKM_ORIGIN:    CFile.m_iVirtualCursor  = Pos; break;
        case SKM_CURENT:    CFile.m_iVirtualCursor += Pos; break;
        case SKM_END:       CFile.m_iVirtualCursor  = CFile.m_Header.m_UncompressFileSize - Pos; break;
        }        

        //
        // Okay the user is moving the cursor outside the current user buffer &&
        // The next buffer been read still not good. Then we will need to cancel the read and 
        // start reading a hold different buffer
        //
        if( CFile.m_iVirtualCursor <  CFile.m_UserBuffer[ CFile.m_iUser&1 ].m_VirtualOffset ||
            CFile.m_iVirtualCursor >= CFile.m_UserBuffer[ CFile.m_iUser&1 ].m_VirtualOffset + (s32)CFile.m_DVDBuffer[ CFile.m_iUser&1 ].m_Size )
        {
            if( CFile.m_DVDBuffer[ CFile.m_iDVD&1 ].m_Size == -1 ||
                CFile.m_iVirtualCursor <  CFile.m_DVDBuffer[ CFile.m_iDVD&1 ].m_VirtualOffset ||
                CFile.m_iVirtualCursor >= CFile.m_DVDBuffer[ CFile.m_iDVD&1 ].m_VirtualOffset + (s32)CFile.m_DVDBuffer[ CFile.m_iDVD&1 ].m_Size )
            {
                // Lets abourt what he is doing and lets start reading the right block
                CFile.m_ActiveFile.AsyncAbort();

                // We also cancel any future reads for now
                CFile.m_DVDBuffer[(CFile.m_iDVD+1)&1].m_Size =-1;

                // Set the next dvd buffer to read to be the one that we need
                read_block&     NextDVD = CFile.m_DVDBuffer[CFile.m_iDVD&1];
                NextDVD.m_iBlock        = CFile.m_iVirtualCursor/CFile.m_Header.m_BlockSize;
                NextDVD.m_VirtualOffset = NextDVD.m_iBlock * CFile.m_Header.m_BlockSize;
                NextDVD.m_Size          = CFile.m_BlockSize[ NextDVD.m_iBlock ];
            
                //
                // Now we need to compute the right offset to the compress file
                //
                s32 Offset = sizeof(CFile.m_Header) + CFile.m_Header.m_nBlocks*sizeof(s32);
                for( s32 i=0; i<NextDVD.m_iBlock; i++ )
                {
                    Offset += CFile.m_BlockSize[i];
                }

                // Seek to the right place in the file to start reading the right block
                CFile.m_ActiveFile.SeekOrigin( Offset );

                // Now start reading the block
                CFile.m_ActiveFile.ReadRaw( &NextDVD.m_Buffer[0], sizeof(xbyte), NextDVD.m_Size );
            }
        }
    }        
}

//------------------------------------------------------------------------------

s32 xfile_compression_device::Tell( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.Tell(); 
    }
    else
    {
        return CFile.m_iVirtualCursor;
    }     

    return EOF;
}

//------------------------------------------------------------------------------

void xfile_compression_device::AsyncAbort( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );

    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.AsyncAbort(); 
    }
    else
    {
        return CFile.m_ActiveFile.AsyncAbort(); 
    }        
}

//------------------------------------------------------------------------------

xfile::sync_state xfile_compression_device::Synchronize ( void* pFile, xbool bBlock )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );

    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.Synchronize( bBlock ); 
    }
    else
    {
        return CFile.m_ActiveFile.Synchronize( bBlock ); 
    }     

    return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void xfile_compression_device::Flush( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.Flush();
    }
    else
    {
        // ..
        ASSERT(0);
    }        
}

//------------------------------------------------------------------------------

s32 xfile_compression_device::Length( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.GetFileLength();
    }
    else
    {
        return CFile.m_Header.m_UncompressFileSize;
    }        

    return -1;
}

//------------------------------------------------------------------------------

xbool xfile_compression_device::IsEOF( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );

    xfile_compress& CFile = m_File(Handle);
    xptr_lock            Ref( m_File );
    
    //
    // Compress file here
    //
    if( CFile.m_bWriting )
    {
        return CFile.m_ActiveFile.IsEOF();
    }
    else
    {
        return CFile.m_iVirtualCursor >= CFile.m_Header.m_UncompressFileSize;
    } 

    return TRUE;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// OTHER FUNCTIONS
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static xfile_compression_device s_CompressionDevice;

xbool x_SetCompressDevice( xfile& File, xbool bCompressAndText )
{
    //
    // If we are reading we need to find out the block size
    //
    xhandle         H;
    xfile_compress& CFile     = s_CompressionDevice.m_File.append(H);
    xptr_lock       Ref(s_CompressionDevice.m_File);

    //
    // Are we going to have to write? If so we must use a temporary file
    //
    CFile.m_bWriting = x_FlagIsOn( File.m_Flags, xfile_device_i::ACC_WRITE );
    if( CFile.m_bWriting )
    {
        xstring Mode;
        
        //
        // Create temporary file
        //
#ifdef TARGET_PC
        CFile.m_FileName.Copy( _tempnam( NULL, "x_BaseStdio." ) ); 
#elif defined(TARGET_3DS)
        ASSERTS(false, "Not implemented yet!");
#else
        char Temp[256];
        mkstemp( Temp );
        CFile.m_FileName.Copy( Temp );
#endif

        // Handle the writting mode
        Mode.Format( "w" );

        // The line below is a mix blessing. If the user does want to do async writtings then the 
        // line below will be very helpfull. If the user doesnt want to do async writting then
        // the line below could slow the compression part of the show at the close of the file.
        // The core issue is that we want the user to always add the async flag because it is faster
        // but we may not want him to deal with syncronacing buffer, etc if he doesnt want to. 
        // Removing this line gives this behavier which it may be the most common case.
        // if( x_FlagIsOn( File.m_Flags, xfile_device_i::ACC_ASYNC ) ) Mode.AppendFormat( "@" );

        if( CFile.m_ActiveFile.Open( CFile.m_FileName, Mode ) == FALSE )
        {
            s_CompressionDevice.m_File.DeleteByHandle( H );
            return FALSE;
        }

        //
        // Are we doing an append file? If so lets copy what we already have over.
        // When appending we are going to decompress the hold file into a temporary file.
        // This is done for 3 reasons. 1 it allows the user to seek and read the file at will.
        // 2 the last block gets recompress as a more full block. 3 It makes the append identical 
        // to the writting for most part. This ofcourse has the performance penalty of having to
        // decompress the hold file and recompress it again. But I dont think appending is going
        // to happen offen to justify the code needed to do this efficiently. 
        //
        if( x_FlagIsOn( File.m_Flags, xfile_device_i::ACC_CREATE ) == FALSE )
        {
            //
            // First lets read the header
            //            
            File.SeekOrigin(0);
            File.ReadRaw( &CFile.m_Header, sizeof(CFile.m_Header), 1 );
            File.Synchronize(TRUE);

            File.ReadRaw( &CFile.m_BlockSize[0], sizeof(s32), CFile.m_Header.m_nBlocks );
            File.Synchronize(TRUE);
            //
            // Now we can read the rest of the file
            //
            xptr<xbyte>                 Out;
            xptr<xbyte>                 In;
            s32                         OutL;  // lzo_uint

            Out.Alloc   ( CFile.m_Header.m_BlockSize );
            In.Alloc    ( CFile.m_Header.m_BlockSize );

            //
            // Copy data to the temporary file
            //

            for( s32 i=0; i<CFile.m_Header.m_nBlocks; i++ )
            {
                File.ReadRaw( &In[0], 1, CFile.m_BlockSize[i] );
                File.Synchronize(TRUE);

                //
                // Decompress block
                //
                {
                    if( i == (CFile.m_Header.m_nBlocks-1) )
                    {
                        const s32 LastBlockSize = CFile.m_Header.m_UncompressFileSize - (i*CFile.m_Header.m_BlockSize);
                        x_DecompressMem ( &Out[0], LastBlockSize, &In[0], CFile.m_BlockSize[i] );
                        OutL = LastBlockSize;
                    }
                    else
                    {
                        x_DecompressMem ( &Out[0], CFile.m_Header.m_BlockSize, &In[0], CFile.m_BlockSize[i] );
                        OutL = CFile.m_Header.m_BlockSize;
                    }
                }
                
                // Write decompress data into the new file
                CFile.m_ActiveFile.WriteRaw( &Out[0], 1, OutL );
            }
        }

        //
        // Now lets seek to the begging since not matter what we need to overwrite the hold thing
        //
        File.SeekOrigin(0);

        //
        // lets backup the active device so that we can use it later
        //
        CFile.m_pDevice     = File.m_pDevice;
        CFile.m_pFile       = File.m_pFile;
    }
    else
    {
        s32                     Fail=0;

        //
        // Read the header
        // Reading the header like this could be a very bad thing as this is happening Sync
        // this ideally should happen Async. But we will wait untill more pieces are in place 
        // to see where this should go.
        //
        Fail |= !File.ReadRaw( &CFile.m_Header, sizeof(CFile.m_Header), 1 );
        Fail |= File.Synchronize( TRUE ) <= xfile::SYNC_INCOMPLETE;
        Fail |= CFile.m_Header.m_Signature != 'fCxV';
        if( Fail ) 
        {
            s_CompressionDevice.m_File.DeleteByHandle( H );
            return FALSE;
        }

        // Allocate the buffers
        CFile.m_DVDBuffer[0].m_Buffer.Alloc ( CFile.m_Header.m_BlockSize );
        CFile.m_DVDBuffer[0].m_Size  = -1;
        CFile.m_DVDBuffer[1].m_Buffer.Alloc ( CFile.m_Header.m_BlockSize );
        CFile.m_DVDBuffer[1].m_Size  = -1;
        CFile.m_UserBuffer[0].m_Buffer.Alloc( CFile.m_Header.m_BlockSize );
        CFile.m_UserBuffer[0].m_Size = -1;
        CFile.m_UserBuffer[1].m_Buffer.Alloc( CFile.m_Header.m_BlockSize );
        CFile.m_UserBuffer[1].m_Size = -1;
        CFile.m_iVirtualCursor = 0;
        CFile.m_iUser          = 0;
        CFile.m_iDVD           = 0;

        //
        // Lets do a big guess and assume that the user is going to want to start reading from the begginng
        //
        File.ReadRaw( &CFile.m_BlockSize[0], sizeof(s32), CFile.m_Header.m_nBlocks );
        File.Synchronize( TRUE );

        // Make sure to read the size of the next block as well
        CFile.m_DVDBuffer[0].m_VirtualOffset = 0;
        CFile.m_DVDBuffer[0].m_iBlock        = 0;
        CFile.m_DVDBuffer[0].m_Size          = x_Abs(CFile.m_BlockSize[CFile.m_DVDBuffer[0].m_iBlock]);
        File.ReadRaw( &CFile.m_DVDBuffer[0].m_Buffer[0], sizeof(xbyte), CFile.m_DVDBuffer[0].m_Size );

        //
        // Lets setup our fake file so that we can work there
        //
        CFile.m_ActiveFile.m_Flags   = File.m_Flags;
        CFile.m_ActiveFile.m_pDevice = File.m_pDevice;
        CFile.m_ActiveFile.m_pFile   = File.m_pFile;

        //
        // Okay we are going to enable text for the high level functions
        //
        if( bCompressAndText )
        {
            File.m_Flags |= xfile_device_i::ACC_TEXT;
        }
    }
 
    //
    // Okay now we must take control over the device
    //    
    File.m_pDevice      = &s_CompressionDevice;
    File.m_pFile        = (void*)(s64)(H.Get()+1);

    return TRUE;
}

//DOM-IGNORE-END
