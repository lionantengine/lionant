//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xirect::xirect( void )
{
}

//------------------------------------------------------------------------------
inline
xirect::xirect( const xirect& Rect ) :
    m_Left      ( Rect.m_Left ),
    m_Top       ( Rect.m_Top ),
    m_Right     ( Rect.m_Right ),
    m_Bottom    ( Rect.m_Bottom ) 
{

}

//------------------------------------------------------------------------------
inline
xirect::xirect( s32 Left, s32 Top, s32 Right, s32 Bottom )
{
    Set( Left, Top, Right, Bottom );
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Set( s32 Left, s32 Top, s32 Right, s32 Bottom )
{
    m_Left   = Left;
    m_Top    = Top;
    m_Right  = Right;
    m_Bottom = Bottom;
    return *this;
}

//------------------------------------------------------------------------------
inline
void xirect::Zero( void )
{
    m_Left      = 0;
    m_Top       = 0;
    m_Right     = 0;
    m_Bottom    = 0;
}

//------------------------------------------------------------------------------
inline
void xirect::setMax( void )
{
    m_Left      =  S32_MAX;
    m_Top       =  S32_MAX;
    m_Right     = -S32_MAX;
    m_Bottom    = -S32_MAX;
}

//------------------------------------------------------------------------------
inline
xbool xirect::Intersect( const xirect& Rect )
{
    return  ( m_Left   <= Rect.m_Right  ) &&
            ( m_Top    <= Rect.m_Bottom ) &&
            ( m_Right  >= Rect.m_Left   ) &&
            ( m_Bottom >= Rect.m_Top    );
}

//------------------------------------------------------------------------------
inline
xbool xirect::Intersect( xirect& R, const xirect& Rect )
{
    if( Intersect( Rect ) == FALSE )
        return( FALSE );

    R.m_Left    = x_Max( m_Left,   Rect.m_Left    );
    R.m_Top     = x_Max( m_Top,    Rect.m_Top     );
    R.m_Right   = x_Min( m_Right,  Rect.m_Right   );
    R.m_Bottom  = x_Min( m_Bottom, Rect.m_Bottom  );

    return TRUE ;
}

//------------------------------------------------------------------------------
inline
xbool xirect::PointInRect( s32 X, s32 Y ) const
{
    return ((X >= m_Left) && (X <= m_Right) && (Y >= m_Top) && (Y <= m_Bottom));
}

//------------------------------------------------------------------------------
inline
xirect& xirect::AddPoint( s32 X, s32 Y )
{
    m_Left   = x_Min( m_Left  , X );
    m_Top    = x_Min( m_Top   , Y );
    m_Right  = x_Max( m_Right , X );
    m_Bottom = x_Max( m_Bottom, Y );
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::AddRect( const xirect& Rect )
{
    m_Left   = x_Min( m_Left  , Rect.m_Left   );
    m_Top    = x_Min( m_Top   , Rect.m_Top    );
    m_Right  = x_Max( m_Right , Rect.m_Right  );
    m_Bottom = x_Max( m_Bottom, Rect.m_Bottom );
    return *this;
}

//------------------------------------------------------------------------------
inline
s32 xirect::GetWidth( void ) const
{
    return m_Right - m_Left;
}

//------------------------------------------------------------------------------
inline
s32 xirect::GetHeight( void ) const
{
    return m_Bottom - m_Top;
}

//------------------------------------------------------------------------------
inline
xvector2 xirect::GetSize( void ) const
{
    return xvector2( (f32)GetWidth(), (f32)GetHeight() );
}

//------------------------------------------------------------------------------
inline
xvector2 xirect::GetCenter( void ) const
{
    return xvector2( (f32)GetWidth()/2.0f, (f32)GetHeight()/2.0f );
}

//------------------------------------------------------------------------------
inline
xirect& xirect::SetWidth( s32 W )
{
    m_Right = m_Left + W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::SetHeight( s32 H )
{
    m_Bottom = m_Top + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::SetSize( s32 W, s32 H )
{
    m_Right  = m_Left + W;
    m_Bottom = m_Top  + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Translate( s32 X, s32 Y )
{
    m_Left   += X;
    m_Top    += X;
    m_Right  += Y;
    m_Bottom += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Inflate( s32 X, s32 Y )
{
    m_Left    -= X;
    m_Top     += X;
    m_Right   -= Y;
    m_Bottom  += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Deflate( s32 X, s32 Y )
{
    m_Left    += X;
    m_Top     -= X;
    m_Right   += Y;
    m_Bottom  -= Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xirect::InRange( s32 Min, s32 Max ) const
{
    return (m_Left   >= Min) && (m_Left   <= Max) &&
           (m_Top    >= Min) && (m_Top    <= Max) &&
           (m_Right  >= Min) && (m_Right  <= Max) && 
           (m_Bottom >= Min) && (m_Bottom <= Max);
}

//------------------------------------------------------------------------------
inline
xbool xirect::IsEmpty( void ) const
{
    return( (m_Left>=m_Right) || (m_Top>=m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xbool xirect::operator == ( const xirect& R ) const
{
    return( (m_Left   == R.m_Left  ) &&
            (m_Top    == R.m_Top   ) &&
            (m_Right  == R.m_Right ) &&
            (m_Bottom == R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xbool xirect::operator != ( const xirect& R ) const
{
    return( (m_Left   != R.m_Left  ) ||
            (m_Top    != R.m_Top   ) ||
            (m_Right  != R.m_Right ) ||
            (m_Bottom != R.m_Bottom) );
}
