#include "../../x_target.h"
#ifdef TARGET_PC
//==============================================================================
// INCLUDES
//==============================================================================
#include <windows.h>
#include "../../x_base.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//===============================================================================
// LOCKLESS FUNCTIONS
//===============================================================================
/********************************************************************************
#ifdef X_TARGET_32BIT

//-------------------------------------------------------------------------------
// CAS_acquire uses acquire semantics (usually for pops)
xbool x_CasAcquire( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal )
{
	xbool RetVal = InterlockedCompareExchange((LONG*)ptr, (LONG)NewVal, (LONG)OldVal) == (LONG)OldVal;
	X_LWSYNC();
	return RetVal;
}

//-------------------------------------------------------------------------------
// CAS_release uses release semantics (usually for pushes)
xbool x_CasRelease( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal)
{
	X_LWSYNC();
	return InterlockedCompareExchange((LONG*)ptr, (LONG)NewVal, (LONG)OldVal) == (LONG)OldVal;
}

//-------------------------------------------------------------------------------
// this only works for x86 chips. That's all we've got here, but if we're shipping we need to use the Vista version.
xbool x_Cas2( x_cas_base_node* volatile* ptr, x_cas_base_node* Old1, u32 Old2, x_cas_base_node* New1, u32 New2 )
{
	register bool f;
	_asm
	{
		    mov esi,ptr
			mov eax,old1
			mov edx,old2
			mov ebx,new1
			mov ecx,new2
			lock cmpxchg8b [esi]
			setz f
	}
	return f;
}

//-------------------------------------------------------------------------------
void x_Swap2( x_cas_base_node* volatile* ptr, x_cas_base_node* NewNode, u32 NewCount)
{
	LONGLONG Exchange = (LONGLONG)(NewNode) << 32 | NewCount;
	InterlockedExchange64(reinterpret_cast<LONGLONG volatile *>(ptr), Exchange);
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// 64BIT Version of the functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

#elif defined X_TARGET_64BIT

//-------------------------------------------------------------------------------
// CAS_acquire uses acquire semantics (usually for pops)
xbool x_CasAcquire( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal )
{
	xbool RetVal = InterlockedCompareExchange64((LONGLONG*)ptr, (LONGLONG)NewVal, (LONGLONG)OldVal) == (LONGLONG)OldVal;
	X_LWSYNC();
	return RetVal;
}

//-------------------------------------------------------------------------------
// CAS_release uses release semantics (usually for pushes)
xbool x_CasRelease( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal)
{
	X_LWSYNC();
	return InterlockedCompareExchange64((LONGLONG*)ptr, (LONGLONG)NewVal, (LONGLONG)OldVal) == (LONGLONG)OldVal;
}

//-------------------------------------------------------------------------------
xbool x_Cas2( x_cas_base_node* volatile* ptr, x_cas_base_node* Old1, u32 Old2, x_cas_base_node* New1, u32 New2 )
{
	LONGLONG Comparand = (LONGLONG)(Old1) << 32 | Old2;
	LONGLONG Exchange  = (LONGLONG)(New1) << 32 | New2;

	if( Comparand != *((LONGLONG*)ptr) ) 
    {
		return FALSE;
	}

	return InterlockedCompareExchange64( (LONGLONG volatile *) ptr, Exchange, Comparand) == Comparand;
}

//-------------------------------------------------------------------------------
void x_Swap2( x_cas_base_node* volatile* ptr, x_cas_base_node* NewNode, u32 NewCount)
{
	LONGLONG Exchange = (LONGLONG)(NewNode) << 32 | NewCount;
	InterlockedExchange64(reinterpret_cast<LONGLONG volatile *>(ptr), Exchange);
}

#endif
********************************************************************************/
//==============================================================================
// types
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This is the main thread class.
// Description:
//     As you can see it comes from the xthread. This allows to treat all the threads
//     the same way including the main thread. The Initialize function sets up some
//     basic variables that the xthread needs. Note that the xthread is never really
//     created since one we get to this point is already done. Note that the main thread
//     has a critical section inside of it. That is the global critical section. 
//     g_pThread is a variable which is unique per-thread although it does not looks like.
//     There is where we keep the pointer of the thread.
//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN
__declspec (thread) xthread* g_pThread = NULL;

class main_thread : public xthread
{
public:

    void Initialize( void )
    {   
        //
        // Set the basic info
        // 
        x_strcpy( m_Name, m_Name.getCount(), "MainThread" );
        m_ID        = FindThreadID();
        m_Handle    = FindHandle();

        //
        // Set the pointer for the thread
        //
        g_pThread   = this;
    }
};

class custom_thread : public xthread
{
public:

    void Initialize( void )
    {   
        //
        // Set the basic info
        // 
        x_strcpy( m_Name, m_Name.getCount(), "CustomThread" );
        m_ID        = FindThreadID();
        m_Handle    = FindHandle();

        //
        // Set the pointer for the thread
        //
        g_pThread   = this;
    }

    void Uninitialize( void )
    {
    }
};
//DOM-IGNORE-END

//==============================================================================
// Variables
//==============================================================================
//DOM-IGNORE-BEGIN
#if defined( TARGET_PC )

// This is PC specific. This will force x_Init to be called
// prior to the constructors so constructors should be able to
// do memory allocations WITH the allocator initialized.
#pragma warning(push)
#pragma warning( disable : 4073 )
#pragma  init_seg(lib)

// Make the constructor be the first ones to kick on
static main_thread s_MainThread;

// this will be the second constructors
struct pc_forcestartup
{
public:
     pc_forcestartup( void ) 
     { 
        x_BaseInit(); 
     }

    ~pc_forcestartup( void ) 
    { 
        x_BaseKill(); 
    }

} g_xForceStartup;


// back to the local name space
#pragma warning(pop)
#endif
//DOM-IGNORE-END
//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

#ifdef TARGET_PC
//==============================================================================
#undef new
#undef delete
xcritical_section::xcritical_section( void )
{
    m_Count            = 0;
    m_pCriticalSection = new CRITICAL_SECTION;
    ASSERT(m_pCriticalSection);

    InitializeCriticalSection( (CRITICAL_SECTION*)m_pCriticalSection );
}

//==============================================================================

xcritical_section::~xcritical_section( void )
{
    if( m_pCriticalSection )delete m_pCriticalSection;
}

//==============================================================================

void xcritical_section::BeginAtomic( void )
{
    ASSERT(m_pCriticalSection);
    EnterCriticalSection( (CRITICAL_SECTION*)m_pCriticalSection );
    m_Count++;
}

//==============================================================================

void xcritical_section::EndAtomic( void )
{
    m_Count--;
    ASSERT(m_pCriticalSection);
    LeaveCriticalSection( (CRITICAL_SECTION*)m_pCriticalSection );
}

//==============================================================================

xbool xcritical_section::IsAtomic( void ) const
{
    return !!m_Count;
}

//==============================================================================

xsemaphore::xsemaphore()
{
    mHandle = CreateEvent(NULL, TRUE, TRUE, NULL );//TEXT("xsemaphore"));
    ASSERT(NULL != mHandle);
    DWORD dw = GetLastError();
    ASSERT( dw !=  ERROR_ALREADY_EXISTS );
}

xsemaphore::~xsemaphore()
{
    if ( mHandle )
    {
        CloseHandle(mHandle);
        mHandle = NULL;
    }
}

void xsemaphore::WaitOne()
{
    ASSERT(NULL != mHandle);
    DWORD err = WaitForSingleObject(mHandle, INFINITE);
    ResetEvent( mHandle );
}

xbool xsemaphore::WaitOne(int millisecondsTimeOut)
{
    ASSERT(NULL != mHandle);
    if ( WAIT_OBJECT_0 == WaitForSingleObject(mHandle, millisecondsTimeOut) )
    {
        ResetEvent( mHandle );
        return true;
    }
    return false;
}

void xsemaphore::Signal( void )
{
    SetEvent( mHandle );
   // ResetEvent( mHandle );
}


/*
void xsemaphore::Release()
{
    ASSERT(NULL != mHandle);
    SetEvent(mHandle);
}
*/
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

xsignal::xsignal(void)
{
	m_Handle = CreateEvent(NULL, TRUE, TRUE, NULL);//TEXT("xsemaphore"));
	ASSERT(NULL != m_Handle);
	ASSERT(GetLastError() != ERROR_ALREADY_EXISTS);
}

//------------------------------------------------------------------------------
xsignal::~xsignal(void)
{
	if (m_Handle)
	{
		CloseHandle(m_Handle);
		m_Handle = NULL;
	}
}

//------------------------------------------------------------------------------

void xsignal::WaitOne( void )
{
	ASSERT(NULL != m_Handle);
	DWORD err = WaitForSingleObject(m_Handle, INFINITE);
	ResetEvent(m_Handle);
}

//------------------------------------------------------------------------------

void xsignal::Signal(void)
{
	SetEvent(m_Handle);
	ResetEvent(m_Handle);
}



//==============================================================================
//==============================================================================

//==============================================================================
xevent::xevent( void )
{
    mHandle = NULL;
}

//==============================================================================
void xevent::Init( void )
{
    mHandle = CreateEvent(NULL, TRUE, TRUE, NULL );//TEXT("xsemaphore"));
    ASSERT(NULL != mHandle);
    DWORD dw = GetLastError();
    ASSERT( dw !=  ERROR_ALREADY_EXISTS );
}

//==============================================================================
xevent::~xevent( void )
{
    if ( mHandle )
    {
        CloseHandle(mHandle);
        mHandle = NULL;
    }
}

//==============================================================================
void xevent::WaitOne( void )
{
    ASSERT(NULL != mHandle);
    DWORD err = WaitForSingleObject(mHandle, INFINITE);
    ResetEvent( mHandle );
}

//==============================================================================
xbool xevent::WaitOne(int millisecondsTimeOut)
{
    ASSERT(NULL != mHandle);
    if ( WAIT_OBJECT_0 == WaitForSingleObject(mHandle, millisecondsTimeOut) )
    {
        return true;
    }
    return false;
}

//==============================================================================
void xevent::Set()
{
    ASSERT(NULL != mHandle);
    SetEvent(mHandle);
}

//==============================================================================
void xevent::Reset()
{
    ASSERT(NULL != mHandle);
    ResetEvent(mHandle);
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER
#endif

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================


//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

xthread::xthread( void )
{
    m_ID            = 0;
    m_Handle        = 0;
    m_bAutoDelete   = FALSE;
    m_bInitialize   = FALSE;
}


//==============================================================================

xthread::~xthread( void )
{
}

//==============================================================================
#undef new
#undef delete
static
s32 ExecuteFirstCall( void* pParam )
{
    xthread* This = (xthread*)pParam;

    //
    // Keep a reference to this thread
    //
    g_pThread = This;

    //
    // for logic control lets call the user init
    //
    This->onInitInstance();

    //
    // Tell the program to run
    //
    This->onRun();

    //
    // Okay we must be done now
    //
    xbool bAutoDelete = This->m_bAutoDelete;
    This->onExitInstance();

    //
    // See whether we have to clean house
    //

    // TODO: We need to delete this memory somehow
    // deleting this here may nto be the correct thing to do...
    if( bAutoDelete ) 
        x_delete(This);

    return 0;
}
#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

//==============================================================================

void xthread::onInitInstance( void ) 
{
    x_BaseInitInstance();
}

//==============================================================================

void xthread::onExitInstance( void ) 
{
    x_BaseKillInstance();
}

//==============================================================================

void xthread::Create( 
    const char*                 pName,
    xbool                       bStartActive,
    xthread::priority           Priority, 
    s32                         StackSize, 
    xbool                       bAutoDelete )
{
    ASSERT( pName );
    ASSERT( StackSize > 256 );
    ASSERT( m_bInitialize == FALSE );

    //
    // Set the auto delete flag
    // and the name
    //
    m_bAutoDelete = bAutoDelete;
    ASSERT( x_strlen( pName ) < m_Name.getCount() );
    x_strcpy( m_Name, m_Name.getCount(), pName );

    //
    // Create a windows thread
    // TODO: This hold thing will have to be moved to other platforms
    //
    m_Handle = (void*)CreateThread(
                        NULL,
                        StackSize,
                        (LPTHREAD_START_ROUTINE)ExecuteFirstCall,
                        this,
                        bStartActive?0:CREATE_SUSPENDED,
                        (DWORD*)&m_ID );

    if( m_Handle == NULL ) 
    {
//TODO:        x_throw( "Unable to create xthread" );
    }

    //
    // We are initialize now
    //
    m_bInitialize   = TRUE;

    //
    // Set the priority of this thread
    //
    SetPriority( Priority );

//TODO:    LOG_INFO( "THREAD", "Thread Created:[%s] ID:%d\n", pName, m_ID );
}

//==============================================================================

xbool xthread::StillAlife( void ) const
{
	DWORD ec;

	// check to see if the thread has ended yet
	if( GetExitCodeThread( (HANDLE)m_Handle, &ec) == STILL_ACTIVE ) 
    {
		return FALSE;
	}
    
    return TRUE;
}

//==============================================================================

void xthread::WaitUntillDone( void ) const
{
	WaitForSingleObject( (HANDLE)m_Handle, INFINITE );
}


//==============================================================================

void xthread::Resume( void )
{
    ResumeThread( (HANDLE)m_Handle );
}

//==============================================================================

void xthread::Suspend( void )
{
    DWORD Count = SuspendThread( (HANDLE)m_Handle );
    ASSERT( 0xFFFFFFFF != Count );
}

//==============================================================================

void xthread::BeginAtomic( void )
{
    m_CriticalSection.BeginAtomic();
}

//==============================================================================

void xthread::EndAtomic( void )
{
    m_CriticalSection.EndAtomic();
}

//==============================================================================

xbool xthread::IsAtomic( void )
{
    return m_CriticalSection.IsAtomic();
}

//==============================================================================

void xthread::SetPriority( xthread::priority Priority )
{
    s32     PC_Priority;

    //
    // Lookup the priority
    //
    switch( Priority )
    {
    default: ASSERT(0); PC_Priority = THREAD_PRIORITY_NORMAL; break;
    case PRIORITY_TIME_CRITICAL:    PC_Priority = THREAD_PRIORITY_TIME_CRITICAL; break;
    case PRIORITY_HIGHEST:          PC_Priority = THREAD_PRIORITY_HIGHEST; break;
    case PRIORITY_ABOVE_NORMAL:     PC_Priority = THREAD_PRIORITY_ABOVE_NORMAL; break;
    case PRIORITY_NORMAL:           PC_Priority = THREAD_PRIORITY_NORMAL; break;
    case PRIORITY_BELOW_NORMAL:     PC_Priority = THREAD_PRIORITY_BELOW_NORMAL; break;
    case PRIORITY_LOWEST:           PC_Priority = THREAD_PRIORITY_LOWEST; break;
    case PRIORITY_IDLE:             PC_Priority = THREAD_PRIORITY_IDLE; break;
    }

    //
    // if we needed more resolution than these we can look into THREAD_BASE_PRIORITY
    // 
    SetThreadPriority( (HANDLE)m_Handle, PC_Priority);
}

//==============================================================================

void xthread::Sleep( s32 Milliseconds )
{
    ::Sleep( Milliseconds );
}

//==============================================================================
u64 xthread::FindThreadID( void )
{
    return GetCurrentThreadId();
}

//==============================================================================
void* xthread::FindHandle( void )
{
    return (void*)GetCurrentThread();
}

//==============================================================================
void xthread::Destroy( void )
{
    CloseHandle( (HANDLE)m_Handle );
}

//==============================================================================
u64 xthread::GetID( void ) const
{
    return m_ID;
}

//==============================================================================
// functions
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
void x_ThreadInit( void )
{
    //
    // Initializes the main thread
    //
    s_MainThread.Initialize();
}

//==============================================================================
void x_ThreadKill( void )
{
    //
    // TODO:
    // Go throw all the threads that may not have been terminated and
    // force them out of the system.
    //
}

//==============================================================================

void x_Sleep( s32 Milliseconds )
{
    Sleep( Milliseconds );
}

void x_Yield( void )
{
    Sleep(1);
}

//==============================================================================

xthread& x_GetCurrentThread ( void )
{
    // This doesn't work because we didn't 
    xthread* pThread = g_pThread;
    ASSERT(pThread);
    return *pThread;
}

//==============================================================================

xbase_instance& x_GetXBaseInstance( void )
{
    xthread& Thread = x_GetCurrentThread();
    return Thread.m_XBaseInstance;
}
//DOM-IGNORE-END

//==============================================================================

void x_BeginAtomic( void )
{
    s_MainThread.BeginAtomic();
}

//==============================================================================

void x_EndAtomic( void )
{
    s_MainThread.EndAtomic();
}

//==============================================================================

xbool x_IsAtomic( void )
{
    return s_MainThread.IsAtomic();
}

//==============================================================================

xthread& x_GetMainThread( void )
{
    return s_MainThread;
}

//==============================================================================
s32 x_GetCPUCoreCount( void )
{
    SYSTEM_INFO systemInfo;
    GetSystemInfo(&systemInfo);
    return systemInfo.dwNumberOfProcessors;
}

#undef new
#undef delete
void x_InitXBaseForCurrentThread( void )
{
    if ( NULL == g_pThread )
    {
        custom_thread* pThreadObject = new custom_thread();
        pThreadObject->Initialize();
        g_pThread = pThreadObject;
        x_BaseInitInstance();
    }
}

void x_KillXBaseForCurrentThread( void )
{
    if ( NULL != g_pThread )
    {
        custom_thread* pThreadObject = (custom_thread*)g_pThread;
        pThreadObject->Uninitialize();
        delete pThreadObject;
        g_pThread = NULL;
    }
}
#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

#endif