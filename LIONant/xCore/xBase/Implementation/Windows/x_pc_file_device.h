#ifndef X_PC_FILE_DEVICE_H
#define X_PC_FILE_DEVICE_H

//==============================================================================
// INCLUDES
//==============================================================================
#include "../../x_base.h"

class pc_device : public xfile_device_i
{
public:
    pc_device ( void ) : xfile_device_i("a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:s:t:u:v:w:x:y:z:"){}
    
protected:
    
    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock );
    virtual void                AsyncAbort  ( void* pFile );
    
    struct file : public x_qt_ptr
    {
        HANDLE      m_Handle;
        OVERLAPPED  m_Overlapped;
        u32         m_Flags;
    };
    
    x_qt_fixed_pool_static_mem<file, 128>   m_qtlFileHPool;
};

#endif // X_PC_FILE_DEVICE_H