#include "../../x_target.h"
#ifdef TARGET_PC
//==============================================================================
// INCLUDES
//==============================================================================
#include <windows.h>
#include "x_pc_file_device.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

static struct temp_path
{
    temp_path( void )
    {
        GetTempPath( 512, m_Path );
    }

    char m_Path[ 512 ];

} s_TempPath;

//------------------------------------------------------------------------------

void* pc_device::Open( const char* paFileName, u32 Flags )
{
	u32 FileMode    = 0;
	u32 Disposition = 0;
    u32 PCFlags     = 0;
    u32 AttrFlags   = 0;
    u32 ShareType   = FILE_SHARE_READ;
    
    ASSERT( paFileName != NULL );
    
    xstring FileName;
    if ( x_strncmp( paFileName, "temp:/", sizeof( "temp:/" )-1 ) == 0 )
    {
        FileName.Format( "%s%s", s_TempPath.m_Path, &paFileName[ 6 ] );
    }
    else
    {
        FileName.Format( paFileName );
    }

	FileMode    = GENERIC_WRITE|GENERIC_READ;
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
    {
        Disposition = CREATE_ALWAYS;
    }
    else
    {
        if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
        {
		    FileMode   &= ~GENERIC_WRITE;
        }
        
        Disposition = OPEN_EXISTING;
    }
    
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ) )
    {
		// FILE_FLAG_OVERLAPPED     -	This allows asynchronous I/O.
		// FILE_FLAG_NO_BUFFERING   -	No cached asynchronous I/O.
        AttrFlags = FILE_FLAG_OVERLAPPED;// | FILE_FLAG_NO_BUFFERING;
    }
    else
    {
        AttrFlags = FILE_ATTRIBUTE_NORMAL;
    }
    
	// open the file (or create a new one)
    HANDLE Handle = CreateFile( FileName, FileMode, ShareType, NULL, Disposition, AttrFlags, NULL );
	if( Handle == INVALID_HANDLE_VALUE ) 
    {
        return NULL;
	} 
    
    //
    // Okay we are in business
    //
    file& File = *(file*)m_qtlFileHPool.AllocByPtr();

    File.m_Handle = Handle;
    File.m_Flags  = Flags;
    x_memset( &File.m_Overlapped, 0, sizeof(File.m_Overlapped) );
    
	if( x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ) ) 
    {
		// Create an event to detect when an asynchronous operation is done.
		// Please see documentation for further information.
		//HANDLE const Event = CreateEvent( NULL, TRUE, FALSE, NULL );
		//ASSERT(Event != NULL);
        //File.m_Overlapped.hEvent = Event;
	}
    
	// done
    return (void*)&File;
}

//------------------------------------------------------------------------------
void pc_device::AsyncAbort( void* pFile )
{
    file&   File = *(file*)pFile;

    VERIFY( CancelIo( File.m_Handle ) );
}

//------------------------------------------------------------------------------

xfile::sync_state pc_device::Synchronize( void* pFile, xbool bBlock )
{
    file&   File = *(file*)pFile;

    if( HasOverlappedIoCompleted( &File.m_Overlapped ) )
        return xfile::SYNC_COMPLETED;
    
	// Get the current status.
	DWORD nBytesTransfer = 0;
	BOOL  Result         = GetOverlappedResult( File.m_Handle, &File.m_Overlapped, &nBytesTransfer, bBlock );
    
	// Has the asynchronous operation finished?
	if( Result == TRUE ) 
    {
		// Clear the event's signal since it isn't automatically reset.
        // VERIFY( ResetEvent( File.m_Overlapped.hEvent ) == TRUE );
        
		// The operation is complete.
		return xfile::SYNC_COMPLETED;
	} 
    
    //
    // Deal with errors
    //
    DWORD dwError = GetLastError();
    
    if( dwError == ERROR_HANDLE_EOF )
    {
        // we have reached the end of
        // the file during asynchronous
        // operation
        return xfile::SYNC_EOF;
    } 
    
    if( dwError == ERROR_IO_INCOMPLETE) 
    {
		return xfile::SYNC_INCOMPLETE;
	} 
    
    if( dwError == ERROR_OPERATION_ABORTED )
    {
        return xfile::SYNC_UNKNOWN_ERR;
    }
    
	// The result is FALSE and the error isn't ERROR_IO_INCOMPLETE, there's a real error!
	return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void pc_device::Close( void* pFile )
{
    //
    // Close the handle
    //
    {
        file&   File = *(file*)pFile;

        if( !CloseHandle( File.m_Handle ) )
        {
            LPVOID lpMsgBuf;
            FormatMessage( 
                          FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                          NULL,
                          GetLastError(),
                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                          (LPTSTR) &lpMsgBuf,
                          0,
                          NULL );
            MessageBox( NULL, (LPCSTR)lpMsgBuf, "GetLastError", MB_OK|MB_ICONINFORMATION );
            LocalFree( lpMsgBuf );
            ASSERT( 0 );
        }
    }
    
    //
    // Lets free our entry
    //
    m_qtlFileHPool.FreeByPtr( (x_qt_ptr*)pFile );
}

//------------------------------------------------------------------------------

xbool pc_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    file&   File = *(file* )pFile;

    DWORD nBytesRead;
    BOOL  bResult = ReadFile( File.m_Handle, pBuffer, Count, &nBytesRead, &File.m_Overlapped ); 
    
    // Set the file pointer (We assume we didnt make any errors)
    //if( File.m_Flags&xfile_device_i::ACC_ASYNC) 
    {
        File.m_Overlapped.Offset += Count;
    }
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                return FALSE;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                return TRUE;                    
            }
                
            default:
            {
                LPVOID lpMsgBuf;
                FormatMessage( 
                              FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                              NULL,
                              GetLastError(),
                              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                              (LPTSTR) &lpMsgBuf,
                              0,
                              NULL );
                MessageBox( NULL, (LPCSTR)lpMsgBuf, "GetLastError", MB_OK|MB_ICONINFORMATION );
                LocalFree( lpMsgBuf );
                ASSERT(0);
                break;
            }
        }
    }
    
    // Not problems
    return TRUE;
}

//------------------------------------------------------------------------------

void pc_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    file& File = *(file*)pFile;
    DWORD nBytesWritten;
    BOOL  bResult = WriteFile( File.m_Handle, pBuffer, Count, &nBytesWritten, &File.m_Overlapped ); 
    
    // Set the file pointer (We assume we didnt make any errors)
    //if( File.m_Flags&xfile_device_i::ACC_ASYNC) 
    {
        File.m_Overlapped.Offset += Count;
    }
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                ASSERT(0);
                return;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                return;                    
            }
                
            default:
            {
                LPVOID lpMsgBuf;
                FormatMessage( 
                              FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                              NULL,
                              GetLastError(),
                              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                              (LPTSTR) &lpMsgBuf,
                              0,
                              NULL );
                MessageBox( NULL, (LPCSTR)lpMsgBuf, "GetLastError", MB_OK|MB_ICONINFORMATION );
                LocalFree( lpMsgBuf );
                ASSERT(0);
                break;
            }
        }
    }
}

//------------------------------------------------------------------------------

void pc_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
    file&   File = *(file*)pFile;

    s32 HardwareMode;
    switch( aMode )
    {
        case SKM_ORIGIN: HardwareMode = FILE_BEGIN; break;
        case SKM_CURENT: HardwareMode = FILE_CURRENT; break;
        case SKM_END:    HardwareMode = FILE_END; break; 
        default: ASSERT(0);               break;
    }
    
    // We will make sure we are sync here
    // WARNING: Potencial time wasted here
    if( File.m_Flags&xfile_device_i::ACC_ASYNC )
    {
        VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
    }
    
	// Seek!
    LARGE_INTEGER Position;
    LARGE_INTEGER NewFilePointer;
    
    Position.LowPart  = Pos;
    Position.HighPart = 0;
    DWORD Result = SetFilePointerEx( File.m_Handle, Position, &NewFilePointer, HardwareMode );
    if( !Result )
    {
	    if( Result == INVALID_SET_FILE_POINTER ) 
        {
		    // Failed to seek.
		    ASSERT( 0 );
	    }
        
        ASSERT( 0 );
    }
    
    // Set the position for async files
    File.m_Overlapped.Offset     = NewFilePointer.LowPart;
    File.m_Overlapped.OffsetHigh = NewFilePointer.HighPart;
}

//------------------------------------------------------------------------------

s32 pc_device::Tell( void* pFile )
{
    file&   File = *(file*)pFile;

    LARGE_INTEGER Position;
    LARGE_INTEGER NewFilePointer;
    Position.LowPart  = 0;
    Position.HighPart = 0;
    
    if( FALSE == SetFilePointerEx( File.m_Handle, Position, &NewFilePointer, FILE_CURRENT ) )
    {
        LPVOID lpMsgBuf;
        FormatMessage( 
                      FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                      NULL,
                      GetLastError(),
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                      (LPTSTR) &lpMsgBuf,
                      0,
                      NULL );
        MessageBox( NULL, (LPCSTR)lpMsgBuf, "GetLastError", MB_OK|MB_ICONINFORMATION );
        LocalFree( lpMsgBuf );
        ASSERT(0);
    }
    
    return (s32)NewFilePointer.LowPart;
}

//------------------------------------------------------------------------------

void pc_device::Flush( void* pFile )
{
    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
}

//------------------------------------------------------------------------------

s32 pc_device::Length( void* pFile )
{
    s32 Length;
    s32 Cursor;
    
    Cursor = Tell( pFile ); Seek( pFile, SKM_END,    0 );
    Length = Tell( pFile ); Seek( pFile, SKM_ORIGIN, Cursor );
    
    return Length;
}

//------------------------------------------------------------------------------

xbool pc_device::IsEOF( void* pFile )
{
    file&   File = *(file* )pFile;

    DWORD nBytesTransfer;
    BOOL  bResult  = GetOverlappedResult( File.m_Handle, &File.m_Overlapped, &nBytesTransfer, FALSE );
    
    if (!bResult) 
    { 
        DWORD dwError = GetLastError();
        
        // deal with the error code 
        switch(dwError ) 
        { 
            case ERROR_HANDLE_EOF: 
            { 
                // we have reached the end of the file 
                // during the call to ReadFile 
                
                // code to handle that 
                return TRUE;
            } 
                
            case ERROR_IO_PENDING: 
            { 
                if( Synchronize( pFile, TRUE ) >= xfile::SYNC_COMPLETED )
                    return FALSE;
                
                return TRUE;                    
            }
                
            default:
                ASSERT( 0 );
                break;
        }
    }
    
    // Not sure about this yet
    return FALSE;
}

#endif