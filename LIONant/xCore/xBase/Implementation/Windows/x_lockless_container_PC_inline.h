#ifndef _X_LOCKLESS_CONTAINER_PC_INLINE_
#define _X_LOCKLESS_CONTAINER_PC_INLINE_


#include "../x_target.h"

/////////////////////////////////////////////////////////////////////////////////
// INLINE GLOBAL
/////////////////////////////////////////////////////////////////////////////////
inline xbool x_cas32( volatile u32*  pPointer,
                      u32   Old,
                      u32   New)
{
    ASSERT( pPointer );
    return xbool(_InterlockedCompareExchange( (volatile long *)pPointer, New, Old) == Old);
}

//-------------------------------------------------------------------------------

inline s32 x_casInc32( volatile s32* pPointer )
{
    ASSERT( pPointer );
    return _InterlockedIncrement( (volatile long *)pPointer );
}

//-------------------------------------------------------------------------------

inline s32 x_casDec32( volatile s32* pPointer )
{
    ASSERT( pPointer );
    return _InterlockedDecrement( (volatile long *)pPointer );
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( volatile u64*  pPointer,
                      u64   Old,
                      u64   New )
{
    ASSERT( pPointer );
	const u64 temp = _InterlockedCompareExchange64((volatile __int64 *)pPointer, New, Old);
	return temp == Old;
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( volatile x_qt_ptr&         Pointer,
                      const x_qt_ptr&   Old,
                      const x_qt_ptr&   New )
{
	const u64 newv = *(u64*)&New;
	const u64 oldv = *(u64*)&Old;
	const u64 temp = _InterlockedCompareExchange64((volatile __int64 *)(u64*)&Pointer, newv, oldv );
	return temp == oldv;
}

#endif //_X_LOCKLESS_CONTAINER_PC_INLINE_