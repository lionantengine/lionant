#include "../x_base.h"

//==============================================================================
// small_block FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

small_block::small_block( void )
{
    s32 i;

    //
    // Don't release pages back to the mm
    //
    m_bFreePages = TRUE;

    //
    // Not size specified yet
    //
    m_nSizes         = 0;

    //
    // Create a link list of page entries
    //
    m_iLastEmptyPage = -1;
    m_iNextFreePage  = 0;
    for( i=0; i<m_PagePool.getCount(); i++ )
    {
        m_PagePool[i].m_iNextPage = (s16)(i+1);
        m_PagePool[i].m_iSize     = -1;
        m_PagePool[i].m_pPage     = NULL;
    }
    m_PagePool[i-1].m_iNextPage = -1;
    m_PagePool[0].m_iPrevPage   = -1;

    //
    // Initialize the hash table
    //
    for( i=0; i<m_PageHash.getCount(); i++ )
    {
        m_PageHash[i] = -1;
    }
}

//------------------------------------------------------------------------------

small_block::~small_block( void )
{
    // The sequence on how to release this memory is not clear
    /*
    for( s32 i=0; i<m_PagePool.getCount(); i++ )
    {
        if(m_PagePool[i].m_pPage) 
        {
            if( m_pBlock &&
                m_PagePool[i].m_pPage >= (s16*)m_pBlock &&
                m_PagePool[i].m_pPage < (s16*)&m_pBlock[m_BlockSize] )
            {
                // This is part of the block
            }
            else
            {
                x_free( m_PagePool[i].m_pPage );
            }
        }
    }

    if( m_pBlock ) x_free( m_pBlock );
    */
}

//------------------------------------------------------------------------------

s32 small_block::FragmentedMemory( void ) const
{
    s32 GranTotal=0;
    s32 Free=0;
    for( s32 i=0; i<m_nSizes; i++ )
    {
        GranTotal += m_Sizes[i].m_TotalEntryCount;
        Free      += m_Sizes[i].m_FreeEntryCount;
    }
    
    ASSERT( GranTotal > 0 );

    return (Free*100)/GranTotal;
}


//------------------------------------------------------------------------------

s32 small_block::GetTotalMemory( void ) const
{
    s32 Total=0;

    // Collect all the pages from the 
    for( s32 i=0; i<m_PagePool.getCount(); i++ )
    {
        if(m_PagePool[i].m_pPage) 
        {
            Total += MAX_PAGE_SIZE;
        }
    }

    return Total;
}

//------------------------------------------------------------------------------

s32 small_block::GetNumberOfAllocations( void ) const
{
    s32 Total=0;
    for( s32 i=0; i<m_nSizes; i++ )
    {
        Total += (m_Sizes[i].m_TotalEntryCount - m_Sizes[i].m_FreeEntryCount);
    }
    return Total;

}

//------------------------------------------------------------------------------

s32 small_block::GetTotalUsed( void ) const
{
    s32 Total=0;
    for( s32 i=0; i<m_nSizes; i++ )
    {
        Total += (m_Sizes[i].m_TotalEntryCount - m_Sizes[i].m_FreeEntryCount)*m_Sizes[i].m_Size;
    }
    return Total;
}

//------------------------------------------------------------------------------

s32 small_block::GetTotalFree( void ) const
{
    s32 Total=0;
    s32 i;

    for( i=0; i<m_nSizes; i++ )
    {
        Total += m_Sizes[i].m_FreeEntryCount*m_Sizes[i].m_Size;
    }

    for( i=m_iLastEmptyPage; i!=-1; i = m_PagePool[i].m_iNextPage )
    {
        Total += MAX_PAGE_SIZE;
    }

    return Total;
}

//------------------------------------------------------------------------------

s32 small_block::ComputeHashEntry( const void* pData ) const
{
    //s32 Hash = (s32)(u64)x_AlignLower(pData, MAX_PAGE_SIZE);
    //return Hash%m_PageHash.getCount();

    u64 Data = (u64)pData;
    u32 Hash = u32(Data ^ (Data>>32))/MAX_PAGE_SIZE;
    return Hash%m_PageHash.getCount();

}

//------------------------------------------------------------------------------

s32 small_block::GetPageFromHash( const void* pPtr ) const
{
    // Find the right entry
    s32 i;

    s32 Handle = ComputeHashEntry( pPtr );

    //
    // The hash entry could be in only two places. The reason why this is is because
    // we are trying to make the algorithm work without having to align the allocations to
    // 8K. Which is a bit aggressive memory wise.
    //
    Handle--;
    if( Handle<0) Handle = m_PageHash.getCount()-1;

    for( s32 t=0; t<2; t++ )
    {
        for( i=m_PageHash[Handle]; i!=-1; i = m_PagePool[i].m_iNextHash )
        {
            if( pPtr >= m_PagePool[i].m_pPage && pPtr < &m_PagePool[i].m_pPage[MAX_PAGE_SIZE/2] )
            {
                return i;
            }
        }
        Handle++;
        Handle %= m_PageHash.getCount();
    }

    return -1;
}

//------------------------------------------------------------------------------

void small_block::DonateBlock( void* pBlock, s32 Count )
{
    ASSERT( x_IsAlign( Count, MAX_PAGE_SIZE ) );
    ASSERT( m_pBlock != NULL );

    // Set variables
    m_BlockSize   = Count;
    m_pBlock      = (xbyte*)pBlock;

    // Create pages
    const s32 nPages = m_BlockSize / MAX_PAGE_SIZE;
    for( s32 i=0; i<nPages; i++ )
    {
        // Allocate one new page
        s32 iPage       = m_iNextFreePage;
        m_iNextFreePage = m_PagePool[ m_iNextFreePage ].m_iNextPage;

        // Inset page into the last empty page pool
        m_PagePool[ iPage ].m_iNextPage = m_iLastEmptyPage;
        m_iLastEmptyPage                = iPage;

        // Initialize a few fields
        m_PagePool[ iPage ].m_iSize      = -1;
        m_PagePool[ iPage ].m_UsedMemory =  0;
        m_PagePool[ iPage ].m_pPage      = (s16*)&m_pBlock[i*MAX_PAGE_SIZE];

        // Insert the page into the hash
        s32                   Handle    = ComputeHashEntry( m_PagePool[ iPage ].m_pPage );
        m_PagePool[ iPage ].m_iNextHash = m_PageHash[Handle];
        m_PageHash[Handle]              = iPage;
    }
}

//------------------------------------------------------------------------------

void small_block::Initialize( const char* pSizeString, void* pBlock, s32 Count )
{
    xsafe_array<char,16>    Buffer;
    const char*             pStr =  pSizeString;

    //
    // User decided to donate memory
    //
    if( pBlock )
    {
        DonateBlock( pBlock, Count );
    }

    //
    // Okay handle sizes
    // 
    for( s32 i=0; 1 ; pStr++ )
    {
        Buffer[i] = *pStr;
        if( Buffer[i] >= '0' && Buffer[i] <= '9' )
        {
            i++;
        }
        else if( i>0 )
        {
            Buffer[i] = 0;
            m_Sizes[m_nSizes].m_Size = x_atoi32( &Buffer[0] );   
            m_Sizes[m_nSizes].m_TotalEntryCount = 0;   
            m_Sizes[m_nSizes].m_FreeEntryCount  = 0;   
            m_Sizes[m_nSizes].m_iNextFreePage   = -1;   
            m_Sizes[m_nSizes].m_iNextFullPage   = -1; 

            ASSERT( MAX_PAGE_SIZE >= m_Sizes[m_nSizes].m_Size );

            // Get ready for the next entry
            m_nSizes++;
            i=0;
        }

        if( *pStr == 0 )
        {
            break;
        }
    }
}

//------------------------------------------------------------------------------

void* small_block::AllocatePage( s32 iSize )
{
    sizes& SizeType = m_Sizes[iSize];

    //
    // Do we have pages in our reserves?
    //
    ASSERT( m_iNextFreePage != -1 );

    // Get a free page
    s16         iPage     = m_iLastEmptyPage != -1?m_iLastEmptyPage:m_iNextFreePage;
    page_entry& PageEntry = m_PagePool[ iPage ];

    //
    // Set this as the next free page for the type size
    //
    SizeType.m_FreeEntryCount   = MAX_PAGE_SIZE/SizeType.m_Size;
    SizeType.m_TotalEntryCount += SizeType.m_FreeEntryCount; 
    SizeType.m_iNextFreePage    = iPage;

    //
    // Allocate the memory for the page
    //
    if( m_iLastEmptyPage == -1 )
    {
        // allocate memory
        PageEntry.m_pPage = (s16*)x_malloc( MAX_PAGE_SIZE, 1, XMEM_FLAG_OKAY_TO_FAIL|XMEM_FLAG_ALIGN_16B );
        ASSERT( PageEntry.m_pPage );

        if( PageEntry.m_pPage == NULL ) 
            return NULL;

        // Set the next free page index
        m_iNextFreePage       = PageEntry.m_iNextPage;

        //
        // Insert the page into the hash
        //
        s32         Handle    = ComputeHashEntry( PageEntry.m_pPage );
        PageEntry.m_iNextHash = m_PageHash[Handle];
        m_PageHash[Handle]    = iPage;
    }
    else
    {
        // Move one over 
        m_iLastEmptyPage = m_PagePool[m_iLastEmptyPage].m_iNextPage;

        ASSERT( PageEntry.m_UsedMemory == 0 );

        // If we have the same exact size then there is nothing else to do
        if( PageEntry.m_iSize == iSize )
        {
            SizeType.m_iNextFreePage = iPage;

            // Set the next and previous pointers
            PageEntry.m_iNextPage       = -1;
            PageEntry.m_iPrevPage       = -1;

            return PageEntry.m_pPage;
        }
    }

    // Initialize the entries in the page
	s32 i;
    for( i=0; i<SizeType.m_FreeEntryCount; i++ )
    {
        s16 OffSet = i*(SizeType.m_Size/2);
        PageEntry.m_pPage[OffSet] = OffSet + (SizeType.m_Size/2);
    }    

    // Terminate the list
    PageEntry.m_pPage[(i-1)*(SizeType.m_Size/2)] = -1;

    //
    // Fill up the rest of the page info
    //
    PageEntry.m_UsedMemory      =  0;
    PageEntry.m_iNextFreeEntry  =  0;
    PageEntry.m_iSize           =  iSize;
    PageEntry.m_iNextPage       = -1;
    PageEntry.m_iPrevPage       = -1;

    return PageEntry.m_pPage;
}

//------------------------------------------------------------------------------

s32 small_block::GetTypeSizeFromUserSize( s32 UserSize ) const
{
    s32    first = 0;
    s32    last  = m_nSizes-1;

    if( m_Sizes[last].m_Size < UserSize ) 
        return -1;

    // Bind search for our size
    while( first < last ) 
    {
        s32 mid = (first + last) / 2;  
        if( UserSize > m_Sizes[mid].m_Size ) 
        {
            first = mid + 1;  
        }
        else if (UserSize <= m_Sizes[mid].m_Size ) 
        {
            if( UserSize > m_Sizes[mid-1].m_Size ) 
                return mid;

            last = mid - 1; 
        }
    }

    return first;    
}

//------------------------------------------------------------------------------

void* small_block::Malloc( s32 UserSize )
{
    //
    // Find which size does it belong
    //
    s32 i = GetTypeSizeFromUserSize( UserSize );
 
    // Did we find the right size?
    if( i == -1 ) return NULL;

    // Make sure that we have the perfect size
    ASSERT( UserSize <= m_Sizes[i].m_Size );
    ASSERT( i==0 || i!=0 && UserSize > m_Sizes[i-1].m_Size );

    //
    // Get a free node
    //
    sizes& SizeType = m_Sizes[i];
    if( SizeType.m_iNextFreePage == -1 )
    {
        ASSERT( SizeType.m_FreeEntryCount == 0 );
        if( AllocatePage( i ) == NULL ) 
            return NULL;

        ASSERT( m_PagePool[SizeType.m_iNextFreePage].m_iSize == i );
    }

    //
    // Get the page
    //
    s32         iPage = SizeType.m_iNextFreePage;
    page_entry& Page  = m_PagePool[ iPage ];
    ASSERT( Page.m_iSize == i );

    //
    // Get the entry
    //
    ASSERT( -1 != Page.m_iNextFreeEntry );
    s16*              pData = &Page.m_pPage[ Page.m_iNextFreeEntry ];
    Page.m_iNextFreeEntry   = Page.m_pPage [ Page.m_iNextFreeEntry ];

    ASSERT((MAX_PAGE_SIZE-Page.m_UsedMemory) >= SizeType.m_Size );

    // Take care of stats
    Page.m_UsedMemory    += SizeType.m_Size;
    SizeType.m_FreeEntryCount--;

    ASSERT( SizeType.m_FreeEntryCount >= 0 );
    ASSERT( (Page.m_UsedMemory%SizeType.m_Size) == 0 );

    //
    // Take care of the page itself
    //

    // Is this page full?
    if( (MAX_PAGE_SIZE-Page.m_UsedMemory) < SizeType.m_Size )
    {
        // Remove our page from the list
        SizeType.m_iNextFreePage = Page.m_iNextPage;
        if( Page.m_iNextPage != -1 ) m_PagePool[ Page.m_iNextPage ].m_iPrevPage = -1;

        // Set the page into the full page list
        Page.m_iNextPage         = SizeType.m_iNextFullPage;
        SizeType.m_iNextFullPage = iPage;
        if( Page.m_iNextPage != -1 )  m_PagePool[ Page.m_iNextPage ].m_iPrevPage = iPage;
        ASSERT(Page.m_iPrevPage == -1);

    }     
    else
    {
        ASSERT((MAX_PAGE_SIZE-Page.m_UsedMemory) >= SizeType.m_Size );
    }

    return pData;
}

//------------------------------------------------------------------------------

xbool small_block::Free( void* pPtr )
{
    //
    // Convert pointer into handle
    //
    s32         iPage     = GetPageFromHash( pPtr );

    if( iPage < 0) 
    {
        return FALSE;
    }

    page_entry& PageEntry = m_PagePool[ iPage ];
    sizes&      TypeSize  = m_Sizes[ PageEntry.m_iSize ];
    s16         iEntry    = ((s16)((xbyte*)pPtr - (xbyte*)PageEntry.m_pPage))/sizeof(s16);

    ASSERT( iEntry >= 0 );
    ASSERT( iEntry < (MAX_PAGE_SIZE/2) );
    ASSERT( PageEntry.m_pPage[iEntry] != 0x3f3f );

    //
    // Get the entry back into the empty list
    //
#ifdef X_DEBUG
    x_memset( pPtr, '?', TypeSize.m_Size );
#endif
    *((s16*)pPtr) = PageEntry.m_iNextFreeEntry;
    ASSERT( PageEntry.m_pPage[iEntry] == PageEntry.m_iNextFreeEntry);
    PageEntry.m_iNextFreeEntry = iEntry;

    // fix stats
    PageEntry.m_UsedMemory -= TypeSize.m_Size;
    TypeSize.m_FreeEntryCount++;

    //
    // Lets sort this page properly
    //
    if( PageEntry.m_UsedMemory == 0 )
    {
        // Remove page from the current list list
        if( PageEntry.m_iPrevPage != -1 ) m_PagePool[ PageEntry.m_iPrevPage ].m_iNextPage = PageEntry.m_iNextPage;
        if( PageEntry.m_iNextPage != -1 ) m_PagePool[ PageEntry.m_iNextPage ].m_iPrevPage = PageEntry.m_iPrevPage;
        if( TypeSize.m_iNextFreePage == iPage ) TypeSize.m_iNextFreePage = PageEntry.m_iNextPage;
        if( TypeSize.m_iNextFullPage == iPage ) TypeSize.m_iNextFullPage = PageEntry.m_iNextPage;

        // Update stats
        TypeSize.m_FreeEntryCount  -= (MAX_PAGE_SIZE/TypeSize.m_Size);
        TypeSize.m_TotalEntryCount -= (MAX_PAGE_SIZE/TypeSize.m_Size);

        // Now we add the page into the generic page list
        if( (m_bFreePages == FALSE) || ((PageEntry.m_pPage >= (s16*)m_pBlock) && (PageEntry.m_pPage < (s16*)&m_pBlock[m_BlockSize])) )
        {
            PageEntry.m_iNextPage     = m_iLastEmptyPage;
            m_iLastEmptyPage          = iPage;
        }
        else
        {
            s32 iHash = ComputeHashEntry( PageEntry.m_pPage );
            if( m_PageHash[iHash] == iPage )
            {
                m_PageHash[iHash] = PageEntry.m_iNextHash;
            }
            else
            {
				s32 i;
                for( i=m_PageHash[iHash]; m_PagePool[i].m_iNextHash != iPage; i = m_PagePool[i].m_iNextHash );
                m_PagePool[i].m_iNextHash = PageEntry.m_iNextHash;
            }

            //
            // Free memory
            //
            x_free( PageEntry.m_pPage );
            PageEntry.m_pPage = NULL;

            //
            // Put free page back in the empty list
            //
            PageEntry.m_iNextPage = m_iNextFreePage;
            m_iNextFreePage       = iPage;
        }
    }
    // Did it used to be full?
    else if( *((s16*)pPtr) == -1 )
    {
        // Remove page from the current list list
        if( PageEntry.m_iPrevPage != -1 ) m_PagePool[ PageEntry.m_iPrevPage ].m_iNextPage = PageEntry.m_iNextPage;
        if( PageEntry.m_iNextPage != -1 ) m_PagePool[ PageEntry.m_iNextPage ].m_iPrevPage = PageEntry.m_iPrevPage;
        if( TypeSize.m_iNextFullPage == iPage ) TypeSize.m_iNextFullPage = PageEntry.m_iNextPage;

        // Now lets add it into the free list
        PageEntry.m_iPrevPage = -1;
        PageEntry.m_iNextPage = TypeSize.m_iNextFreePage;
        if( PageEntry.m_iNextPage != -1 ) m_PagePool[ PageEntry.m_iNextPage ].m_iPrevPage = iPage;
        TypeSize.m_iNextFreePage = iPage;
    }
    else if( PageEntry.m_iNextPage != -1 && PageEntry.m_UsedMemory < m_PagePool[ PageEntry.m_iNextPage ].m_UsedMemory )
    {
        // Remove page from the current list list
        if( PageEntry.m_iPrevPage != -1 ) m_PagePool[ PageEntry.m_iPrevPage ].m_iNextPage = PageEntry.m_iNextPage;
        if( PageEntry.m_iNextPage != -1 ) m_PagePool[ PageEntry.m_iNextPage ].m_iPrevPage = PageEntry.m_iPrevPage;

        // Find the right spot to insert our page
        s16 iNext = PageEntry.m_iNextPage;
        while( m_PagePool[iNext].m_iNextPage != -1 )
        {
            s16 iNextNext = m_PagePool[iNext].m_iNextPage;
            if( PageEntry.m_UsedMemory >= m_PagePool[iNextNext].m_UsedMemory ) break;
            iNext = iNextNext;
        }

        // Set the new next free page
        if( TypeSize.m_iNextFreePage == iPage ) TypeSize.m_iNextFreePage = PageEntry.m_iNextPage;

        // insert it after the inext
        PageEntry.m_iNextPage           = m_PagePool[ iNext ].m_iNextPage;
        m_PagePool[ iNext ].m_iNextPage = iPage;
        
        PageEntry.m_iPrevPage           = iNext;
        if( PageEntry.m_iNextPage != -1 ) m_PagePool[PageEntry.m_iNextPage].m_iPrevPage = iPage;
    }    

    ASSERT((MAX_PAGE_SIZE-PageEntry.m_UsedMemory) >= TypeSize.m_Size );

    return TRUE;
}

//------------------------------------------------------------------------------

xbool small_block::SanityCheck( void ) const
{
    s32 i;
    
    for( i=0; i<m_nSizes; i++ )
    {
        s32    j;
        const sizes& TypeSize = m_Sizes[i];

        ASSERT( TypeSize.m_FreeEntryCount >= 0 );
        ASSERT( TypeSize.m_FreeEntryCount <= TypeSize.m_TotalEntryCount );
        ASSERT( TypeSize.m_TotalEntryCount >= 0 );

        //
        // Check all the free pages in the list
        //
        s32 FreeCount=0;
        s32 FullCount=0;
        for( j=TypeSize.m_iNextFreePage; j!=-1; j = m_PagePool[j].m_iNextPage )
        {
            const page_entry& Page = m_PagePool[j]; 

            // Very first entry must have a prev set to null 
            ASSERT( m_PagePool[TypeSize.m_iNextFreePage].m_iPrevPage == - 1);

            // Make sure that pointers of pages look good
            ASSERT( Page.m_iNextPage == -1 || Page.m_iNextPage != -1 && m_PagePool[Page.m_iNextPage].m_iPrevPage == j );
            ASSERT( Page.m_iPrevPage == -1 || Page.m_iPrevPage != -1 && m_PagePool[Page.m_iPrevPage].m_iNextPage == j );

            // the TypeSizes must match
            ASSERT( Page.m_iSize == i );

            // Better have a valid pointer
            ASSERT( Page.m_pPage != NULL );

            // Make sure that we are in sorting order
            ASSERT( Page.m_iPrevPage == -1 || Page.m_iPrevPage != -1 && Page.m_UsedMemory <= m_PagePool[Page.m_iPrevPage].m_UsedMemory  );

            // All the memory needs to be a multiple of the size
            ASSERT( Page.m_UsedMemory < MAX_PAGE_SIZE );
            ASSERT( (Page.m_UsedMemory%TypeSize.m_Size)==0 );

            // Make sure that we can find the node in the hash
            s32 Hash = ComputeHashEntry( Page.m_pPage );
            s32 t;
            ASSERT( GetPageFromHash( Page.m_pPage ) == j );

            for( t=m_PageHash[ Hash ]; t!=-1; t=m_PagePool[t].m_iNextHash )
            {
                if( t == j ) break;
            }
            ASSERT( t!= -1 );

            // Make sure that its entries reach an end
            s32 Count=0;
            s32 Total = Page.m_UsedMemory;
            ASSERT( Total <= MAX_PAGE_SIZE );
            for( s32 j=Page.m_iNextFreeEntry; j!=-1; j = Page.m_pPage[j] )
            {
                ASSERT( j != Page.m_pPage[j] );
                ASSERT( j < MAX_PAGE_SIZE/2 );
                ASSERT( j >= 0 );
                ASSERT( Total <= MAX_PAGE_SIZE );
                Total += TypeSize.m_Size;

                Count++;

                // Update the count of free pages
                FreeCount++;

                ASSERT( FreeCount <= TypeSize.m_FreeEntryCount );
            }

            FullCount += ((MAX_PAGE_SIZE/TypeSize.m_Size)-Count);
        }

        // Make sure the free count matches
        ASSERT( FreeCount == TypeSize.m_FreeEntryCount );

        //
        // Check all the full pages in the list
        //
        for( j=TypeSize.m_iNextFullPage; j!=-1; j = m_PagePool[j].m_iNextPage )
        {
            const page_entry& Page = m_PagePool[j]; 

            // update the full count
            FullCount += MAX_PAGE_SIZE/TypeSize.m_Size;

            // Very first entry must have a prev set to null 
            ASSERT( m_PagePool[TypeSize.m_iNextFullPage].m_iPrevPage == - 1);

            // Make sure that pointers of pages look good
            ASSERT( Page.m_iNextPage == -1 || Page.m_iNextPage != -1 && m_PagePool[Page.m_iNextPage].m_iPrevPage == j );
            ASSERT( Page.m_iPrevPage == -1 || Page.m_iPrevPage != -1 && m_PagePool[Page.m_iPrevPage].m_iNextPage == j );

            // the TypeSizes must match
            ASSERT( Page.m_iSize == i );

            // Better have a valid pointer
            ASSERT( Page.m_pPage != NULL );

            // All the memory needs to be a multiple of the size
            ASSERT( Page.m_UsedMemory <= MAX_PAGE_SIZE );
            ASSERT( (Page.m_UsedMemory%TypeSize.m_Size)==0 );
            ASSERT( (Page.m_UsedMemory+TypeSize.m_Size) >= MAX_PAGE_SIZE );

            // Make sure that we can find the node in the hash
            s32 Hash = ComputeHashEntry( Page.m_pPage );
            s32 t;
            ASSERT( GetPageFromHash( Page.m_pPage ) == j );

            for( t=m_PageHash[ Hash ]; t!=-1; t=m_PagePool[t].m_iNextHash )
            {
                if( t == j ) break;
            }
            ASSERT( t!= -1 );
        }

        // Make sure the free count matches
        ASSERT( FreeCount+FullCount == TypeSize.m_TotalEntryCount );

    }

    //
    // Check all the pages in the empty page list
    //
    for( i=m_iLastEmptyPage; i!=-1; i = m_PagePool[i].m_iNextPage )
    {
        const page_entry& Page = m_PagePool[i]; 

        // Better have a valid pointer
        ASSERT( Page.m_pPage != NULL );

        // All the memory needs to be a multiple of the size
        ASSERT( Page.m_UsedMemory == 0 );

        // Make sure that we can find the node in the hash
        s32 Hash = ComputeHashEntry( Page.m_pPage );
        s32 t;
        for( t=m_PageHash[ Hash ]; t!=-1; t=m_PagePool[t].m_iNextHash )
        {
            if( t == i ) break;
        }
        ASSERT( t!= -1 );
    }

    //
    // Check all the pages in the free page pool
    //
    for( i=m_iNextFreePage; i!=-1; i = m_PagePool[i].m_iNextPage )
    {
        //const page_entry& Page = m_PagePool[i]; 
        ASSERT( m_PagePool[i].m_iSize == -1 );
    }

    return TRUE;
}
