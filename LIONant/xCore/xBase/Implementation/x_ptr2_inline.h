
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::ChangeBehavior( u32 Flags ) const
{
#ifdef X_DEBUG
    Freeze();

    do
    {
        const debug     LocalDebug      = m_DebugFlags;
        debug           NewDebug        = LocalDebug;
        
        NewDebug.m_Flags        = Flags | FLAGS_FROZEN;

        if ( x_cas64( (u64*)&m_DebugFlags.m_Raw, LocalDebug.m_Raw, NewDebug.m_Raw ) )
            break;

    } while( 1 );

    Unfreeze();
#endif
}

//---------------------------------------------------------------------------------------

inline
xbool xptr2_debug_lock::isMutable( xbool bAssert ) const
{
#ifdef X_DEBUG
    const u32       Flags       = m_DebugFlags.m_Flags;
    const debug     DebugFlags  = m_DebugFlags;

    if ( x_FlagIsOn( Flags, FLAGS_READ_ONLY ) )
    {
        if ( bAssert )
        {
            ASSERT( x_FlagIsOn( Flags, FLAGS_READ_ONLY ) == FALSE );
        }
        else
        {
            return FALSE;
        }
    }

    if ( x_FlagIsOn( Flags, FLAGS_FROZEN ) == TRUE )
    {
        if ( bAssert )
        {
            ASSERT( x_FlagIsOn( Flags, FLAGS_FROZEN ) == FALSE );
        }
        else
        {
            return FALSE;
        }
    }

    if ( x_FlagIsOn( Flags, FLAGS_QT_MUTABLE | FLAGS_QT_READABLE ) )
    {
    }
    else if ( DebugFlags.m_ThreadID != 0 )
    {
        u64 ThisThreadID = xthread::FindThreadID();
            ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
        u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );

        if ( bAssert )
        {
            ASSERT( Compact == DebugFlags.m_ThreadID );
        }
        else
        {
            if(Compact != DebugFlags.m_ThreadID)
                return FALSE;
        }
   }

    if ( x_FlagIsOn( Flags, FLAGS_SINGLE_WRITTER ) )
    {
        if( DebugFlags.m_nWritters > 0 )
        {
            if ( bAssert )
            {
                ASSERT( DebugFlags.m_nWritters == 0 );
            }
            else
            {
                return FALSE;
            }
        }
    }
#endif
    return TRUE;
}

//---------------------------------------------------------------------------------------
inline
xbool xptr2_debug_lock::isReadable( xbool bAssert ) const
{
#ifdef X_DEBUG
    const u32   Flags       = m_DebugFlags.m_Flags;
    const debug DebugFlags  = m_DebugFlags;

    if ( x_FlagIsOn( Flags, FLAGS_FROZEN ) == TRUE )
    {
        if(bAssert)
        {
            ASSERT( x_FlagIsOn( Flags, FLAGS_FROZEN ) );
        }
        else
        {
            return FALSE;
        }
    }
    
    if ( x_FlagIsOn( Flags, FLAGS_SINGLE_WRITTER ) )
    {
        if ( DebugFlags.m_nWritters > 0 )
        {
            if ( bAssert )
            {
                ASSERT( DebugFlags.m_nWritters == 0 );
            }
            else return FALSE;
        }
    }

    if ( x_FlagIsOn( Flags, FLAGS_QT_READABLE ) == FALSE )
    {
        if ( DebugFlags.m_ThreadID != 0 )
        {
            u64 ThisThreadID = xthread::FindThreadID();
                ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
            u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );

            if ( bAssert )
                ASSERT( Compact == DebugFlags.m_ThreadID );
            else
                if(Compact != DebugFlags.m_ThreadID) 
                    return FALSE;
       }
    }

    if ( x_FlagIsOn( Flags, FLAGS_SINGLE_READER ) )
    {
        if ( DebugFlags.m_nReaders > 0 )
        {
            if ( bAssert )
            {
                ASSERT( DebugFlags.m_nReaders == 0 );
            }
            else return FALSE;
        }
    }
#endif
    return TRUE;
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::RefMutate( xbool bInc ) const
{
#ifdef X_DEBUG
    do
    {
        const debug LocalDebugFlags = m_DebugFlags;
        const u32   Flags           = LocalDebugFlags.m_Flags;
        s32         nWritters       = LocalDebugFlags.m_nWritters;
        const s32   nReaders        = LocalDebugFlags.m_nReaders;
        u16         ThreadID        = LocalDebugFlags.m_ThreadID;

        //
        // Check situation before updating
        //
        if ( x_FlagIsOn( Flags, FLAGS_SINGLE_WRITTER)  )
        {
            if(bInc) ASSERT( nWritters == 0 );
            else     ASSERT( nWritters == 1 );
            ASSERT( nReaders  == 0 );
        }

        if ( x_FlagIsOn( Flags, FLAGS_QT_MUTABLE ) == FALSE ) 
        {
            u64 ThisThreadID = xthread::FindThreadID();
                ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
            u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );
            
            if( ThreadID == 0 )
                ThreadID = Compact;
            else
                ASSERT( ThreadID == Compact );
        }

        //
        // Get ready to update
        //
        if(bInc) nWritters ++;
        else     nWritters --;

        ASSERT( nWritters <=0xff );
        ASSERT( nWritters >=0    );

        debug NewDebugFlags   = LocalDebugFlags;
        NewDebugFlags.m_nWritters = nWritters;
        
        if( nWritters | nReaders ) NewDebugFlags.m_ThreadID  = ThreadID;
        else NewDebugFlags.m_ThreadID  = 0;

        if ( x_cas64( (u64*)&m_DebugFlags.m_Raw, LocalDebugFlags.m_Raw, NewDebugFlags.m_Raw ) )
            break;

    } while( 1 );
#endif
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::RefReader( xbool bInc ) const
{
#ifdef X_DEBUG
    do
    {
        const debug LocalDebugFlags = m_DebugFlags;
        const u32   Flags           = LocalDebugFlags.m_Flags;
        const s32   nWritters       = LocalDebugFlags.m_nWritters;
        s32         nReaders        = LocalDebugFlags.m_nReaders;
        u16         ThreadID        = LocalDebugFlags.m_ThreadID;

        //
        // Check situation before updating
        //
        if ( x_FlagIsOn( Flags, FLAGS_SINGLE_READER) )
        {
            if(bInc) ASSERT( nReaders == 0 );
            else     ASSERT( nReaders == 1 );
        }

        if ( x_FlagIsOn( Flags, FLAGS_SINGLE_WRITTER)  )
        {
            ASSERT( nWritters  == 0 );
        }

        if ( x_FlagIsOn( Flags, FLAGS_QT_READABLE ) == FALSE ) 
        {
            u64 ThisThreadID = xthread::FindThreadID();
                ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
            u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );
            
            if( ThreadID == 0 )
                ThreadID = Compact;
            else
                ASSERT( ThreadID == Compact );

            ThreadID = Compact;
        }

        //
        // Get ready to update
        //
        if(bInc) nReaders ++;
        else     nReaders --;

        ASSERT( nReaders <=0xff );
        ASSERT( nReaders >=0    );

        debug NewDebugFlags         = LocalDebugFlags;
        NewDebugFlags.m_nReaders    = nReaders;

        if( nWritters | nReaders ) NewDebugFlags.m_ThreadID  = ThreadID;
        else NewDebugFlags.m_ThreadID  = 0;

        if ( x_cas64( const_cast<u64*>(&m_DebugFlags.m_Raw), LocalDebugFlags.m_Raw, NewDebugFlags.m_Raw ) )
            break;

    } while( 1 );
#endif
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::getEntryReadOnly( void ) const
{   
#ifdef X_DEBUG
    const debug  LocalDebugFlags = m_DebugFlags;
    if ( ( LocalDebugFlags.m_Flags & FLAGS_QT_READABLE ) == 0 )
    {
        if ( LocalDebugFlags.m_ThreadID != 0 )
        {
            u64 ThisThreadID = xthread::FindThreadID();
                ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
            u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );

            ASSERT( Compact == LocalDebugFlags.m_ThreadID );
       }
    }
#endif
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::getEntry( void ) 
{
#ifdef X_DEBUG
    const debug  LocalDebugFlags = m_DebugFlags;
    if ( ( LocalDebugFlags.m_Flags & FLAGS_QT_MUTABLE ) == 0 )
        if ( ( LocalDebugFlags.m_Flags & FLAGS_QT_READABLE ) == 0 )
        {
            if ( LocalDebugFlags.m_ThreadID != 0 )
            {
                u64 ThisThreadID = xthread::FindThreadID();
                    ThisThreadID = (ThisThreadID>>32)^ThisThreadID;
                u16 Compact      = u16( ((ThisThreadID >> 16) ^ ThisThreadID) );

                // We are not allowed to access this variable from a different thread
                ASSERT( Compact == LocalDebugFlags.m_ThreadID );
            }
        }
#endif
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::Freeze( void ) const
{
#ifdef X_DEBUG
    do
    {
        const debug     LocalDebug      = m_DebugFlags;

        // At this point not readables or writables allowed
        ASSERT( LocalDebug.m_nWritters <= 1);
        ASSERT( LocalDebug.m_nReaders == 0);

        debug NewDebug     = LocalDebug;
        NewDebug.m_Flags  |= FLAGS_FROZEN;

        if ( x_cas64( (u64*)&m_DebugFlags.m_Raw, LocalDebug.m_Raw, NewDebug.m_Raw ) )
            break;

    } while (1);
#endif
}

//---------------------------------------------------------------------------------------
inline
void xptr2_debug_lock::Unfreeze( void ) const
{
#ifdef X_DEBUG
    do
    {
        const debug     LocalDebug      = m_DebugFlags;
        
        // At this point not readables or writables allowed
        ASSERT( LocalDebug.m_nWritters <= 1);
        ASSERT( LocalDebug.m_nReaders == 0);
        ASSERT( (LocalDebug.m_Flags & FLAGS_FROZEN) == FLAGS_FROZEN );

        debug NewDebug    = LocalDebug;
        NewDebug.m_Flags &= ~FLAGS_FROZEN;

        if ( x_cas64( (u64*)&m_DebugFlags.m_Raw, LocalDebug.m_Raw, NewDebug.m_Raw ) )
            break;

    } while (1);
#endif
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2_interface<T,D>::Copy( s32 DestinationOffset, const this_type& SourceRef, s32 SourceOffset, s32 ElementCount )
{
    ( ( this_type* )this )->Copy( DestinationOffset, SourceRef, SourceOffset, ElementCount  );
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2_interface<T,D>::setMemory( s32 Value )
{
    ( ( xptr2<T>* )this )->setMemory( Value );
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::ChangeBehavior( u32 Flags ) const
{
    m_Debug.ChangeBehavior( Flags );
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
s32 xptr2<T,D>::getCount( void ) const
{
    return m_Capacity;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Alloc( s32 Count )
{
    if( m_pData != NULL )
        Destroy( );

    m_Debug.Freeze();

    // Allocate the memory first
    s32     AllocSize   = sizeof(T)*Count;
    T*      pData       = (T*)x_malloc( sizeof(u8), AllocSize, xmem_aligment(T) );

    // Finally set the new pointer
    m_pData     = pData;
    m_Capacity  = Count;

    m_Debug.Unfreeze();
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::New( s32 Count )
{
    Alloc( Count );

    // Now we need to construct the right number of items
    for( s32 i = 0; i < Count; i++ )
    {
        x_Construct(&m_pData[i]);
    }

    // mark that it was newed
    m_ReleaseFlags |= RELEASE_FLAGS_NEW;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Destroy( void )
{
    if( m_pData == NULL )
        return;

    // Lock down
    m_Debug.Freeze();

    // If it was newed then destructors must be call
    if ( x_FlagIsOn(m_ReleaseFlags, RELEASE_FLAGS_NEW ) )
        for( s32 i=0; i<m_Capacity; i++ )
        {
            ((destructor*)&m_pData[i])->~destructor();
        }

    // Free the pointer
    x_free( m_pData );
    m_pData    = NULL;
    m_Capacity = 0;

    // Unlock it
    m_Debug.Unfreeze();
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Resize( s32 NewCount  )
{
    if( m_pData == NULL )
    {
        Alloc( NewCount );
        return;
    }

    m_Debug.Freeze();
    
    if( ( m_ReleaseFlags & RELEASE_FLAGS_NEW ) == 0 )
    {
        s32     AllocSize   = sizeof(T)*NewCount;
        T*      pData = (T*)     x_realloc( m_pData, sizeof(u8), AllocSize );

        // Fix a few bits of information
        m_Capacity       = NewCount;
        m_pData          = pData;
    }
    else
    {
        ASSERT( 0 ); // No implemented yet
    }

    m_Debug.Unfreeze();
}

//---------------------------------------------------------------------------------------

template< typename T, typename D > inline
void xptr2<T,D>::opMutableMap( s32 iStart, x_function<void(T& Entry, s32 iEntry, xbool& About )> Function )
{
    mutable_ref( rlEntry, *this );
    xbool bAbort = FALSE;
    for( s32 i=iStart; i<getCount(); i++)
    {
        Function( rlEntry[i], i, bAbort );
        if(bAbort) break;
    }
}

//---------------------------------------------------------------------------------------

template< typename T, typename D > inline
void xptr2<T,D>::opConstMap( s32 iStart, x_function<void(const T& Entry, s32 iEntry, xbool& About )> Function ) const
{
    const_ref( rlEntry, *this );
    xbool bAbort = FALSE;
    for( s32 i=iStart; i<getCount(); i++)
    {
        Function( rlEntry[i], i, bAbort );
        if(bAbort) break;
    }
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
const typename xptr2<T,D>::smart_lock& xptr2<T,D>::getSmartLock( void ) const
{
    return m_Debug;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
typename xptr2<T,D>::smart_lock& xptr2<T,D>::getSmartLock( void )
{
    return m_Debug;
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
const T& xptr2<T,D>::getEntryReadOnly( s32 Index ) const
{   
    ASSERT(isValid());
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Capacity );

    m_Debug.getEntryReadOnly();

    return m_pData[Index];
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
T& xptr2<T,D>::getEntry( s32 Index ) 
{
    ASSERT(isValid());
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Capacity );

    m_Debug.getEntry();

    return m_pData[Index];
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Freeze( void )
{
    m_Debug.Freeze();
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Unfreeze( void )
{
    m_Debug.Unfreeze();
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::Copy( s32 DestinationOffset, const this_type& SourceRef, s32 SourceOffset, s32 ElementCount )
{
    ASSERT(isValid());

    // Count must be positive
    ASSERT(ElementCount>=0);
    if( ElementCount == 0 ) return;

    // Make sure we are in the right ranges
    ASSERT( &getEntry(DestinationOffset+ElementCount-1) );
    ASSERT( &SourceRef.getEntryReadOnly(SourceOffset+ElementCount-1) );

    x_memmove( &getEntry(DestinationOffset), &SourceRef.getEntryReadOnly(SourceOffset), ElementCount*sizeof(T) ); 
}

//---------------------------------------------------------------------------------------
template< typename T, typename D > inline
void xptr2<T,D>::setMemory( s32 Value )
{
    ASSERT(isValid());
    x_memset( &getEntry(0), Value, sizeof(T)*m_Capacity );
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
inline
void xsemaphore_lock::ChangeBehavior( u32 Flags ) const
{
    do
    {
        const semaphore LocalReality = *(semaphore*)&m_Semapore;
        semaphore       NewReality   = LocalReality;
        
        NewReality.m_Flags = Flags;
        NewReality.m_Counter++;

        if ( x_cas64( (u64*)&m_Semapore.m_Raw, LocalReality.m_Raw, NewReality.m_Raw ) )
            break;

    } while( 1 );
}

//---------------------------------------------------------------------------------------
inline
void xsemaphore_lock::RefReader( xbool bInc ) const
{
    do
    {
        const semaphore LocalReality = *(semaphore*)&m_Semapore;

        if( bInc )
        {
            ASSERT( x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_QT_READABLE ) );

            const s32 MaxReaders = x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_SINGLE_READER )?1:0xff;            
            if( LocalReality.m_nWritters > 0 || LocalReality.m_nReaders >= MaxReaders )
            {
                // Do jobs while we wait
                g_Scheduler.ProcessWhileWait( TRUE, [this, &MaxReaders]() -> xbool
                {
                    return m_Semapore.m_nWritters > 0 || m_Semapore.m_nReaders >= MaxReaders;
                } );

                // Ok lets try again
                continue;            
            }
        }

        ASSERT( LocalReality.m_nWritters == 0 );
        ASSERT( LocalReality.m_nReaders  < 0xff );

        semaphore NewReality   = LocalReality;
            
        NewReality.m_Counter++;
        if( bInc ) NewReality.m_nReaders++;
        else       NewReality.m_nReaders--;

        if ( x_cas64( (u64*)&m_Semapore.m_Raw, LocalReality.m_Raw, NewReality.m_Raw ) )
            break;

    } while( 1 );
}

//---------------------------------------------------------------------------------------
inline
void xsemaphore_lock::RefMutate( xbool bInc ) const
{
    do
    {
        const semaphore LocalReality = *(semaphore*)&m_Semapore;

        if( bInc )
        {
            ASSERT( x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_QT_MUTABLE ) ); 
            ASSERT( FALSE == x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_READ_ONLY ) ); 

            const s32 MaxWritters = x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_SINGLE_WRITTER )?1:0xff;
            
            if( LocalReality.m_nReaders > 0 || LocalReality.m_nWritters >= MaxWritters )
            {
                // Do jobs while we wait
                g_Scheduler.ProcessWhileWait( TRUE, [this, &MaxWritters]() -> xbool
                {
                    return m_Semapore.m_nReaders > 0 || m_Semapore.m_nWritters >= MaxWritters;
                } );

                // Ok lets try again
                continue;            
            }
        }

        ASSERT( LocalReality.m_nWritters < x_FlagIsOn( LocalReality.m_Flags, xptr2_debug_lock::FLAGS_SINGLE_WRITTER )?1:0xff );
        ASSERT( LocalReality.m_nReaders  == 0 );

        semaphore NewReality   = LocalReality;
            
        NewReality.m_Counter++;
        if( bInc ) NewReality.m_nWritters++;
        else       NewReality.m_nWritters--;

        if ( x_cas64( (u64*)&m_Semapore.m_Raw, LocalReality.m_Raw, NewReality.m_Raw ) )
            break;

    } while( 1 );
}
