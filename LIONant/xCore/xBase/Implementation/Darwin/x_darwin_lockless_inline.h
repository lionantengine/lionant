
inline xbool x_cas32( volatile u32*  pPointer,
                      u32            Old,
                      u32            New)
{
    ASSERT( pPointer );
    return xbool(OSAtomicCompareAndSwapIntBarrier( (int)Old, (int)New, (volatile int*)pPointer));
}

//-------------------------------------------------------------------------------

inline s32 x_casInc32( volatile s32* pPointer )
{
    ASSERT( pPointer );
    return OSAtomicIncrement32Barrier( pPointer );
}

//-------------------------------------------------------------------------------

inline s32 x_casDec32( volatile s32* pPointer )
{
    ASSERT( pPointer );
    return OSAtomicDecrement32Barrier( pPointer );
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( volatile u64*  pPointer,
                      u64            LocalReality,
                      u64            New )
{
    ASSERT( pPointer );
    return xbool(OSAtomicCompareAndSwap64Barrier( (int64_t)LocalReality, (int64_t)New, (volatile int64_t*)pPointer ));
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( volatile x_qt_ptr& Pointer,
                      const x_qt_ptr&    Local,
                      const x_qt_ptr&    New )
{
    return x_cas64( (u64*)&Pointer, *(u64*)&Local, *(u64*)&New );
}

///////////////////////////////////////////////////////////////////////////////////
// END
///////////////////////////////////////////////////////////////////////////////////
