#if 0
#ifdef TARGET_IOS
//==============================================================================
// INCLUDES
//==============================================================================
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "../../x_target.h"
#include "x_ios_file_device.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

char ios_device::sDefaultPath[256] = {0};

//------------------------------------------------------------------------------
void ios_device::sInitialize(void)
{
    confstr(_CS_DARWIN_USER_TEMP_DIR, sDefaultPath, 256);
    x_strcpy(sDefaultPath, 256, "/var/mobile/Applications/2EF61C1F-06DB-40B1-BE97-7913BFB383CB/Documents/");
}

//------------------------------------------------------------------------------
void* ios_device::Open( const char* pFileName, u32 Flags )
{
    char    finalName[256] = {0};
    s32     flag = O_RDWR;
    mode_t  mode = 0;
    
	ASSERT( pFileName != NULL );
    
    if ( '/' != pFileName[0] )
    {
        x_strcpy(finalName, 256, sDefaultPath);
        x_strcat(finalName, 256, pFileName);
    }
    else
    {
        x_strcpy(finalName, 256, pFileName);
    }
    
	flag    = O_RDWR;
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
    {
        flag |= (O_CREAT | O_TRUNC);
        mode = S_IRWXU;
    }
    else
    {
        if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
        {
		    flag   = O_RDONLY;
        }
    }
    
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ) )
    {
        flag |= O_NONBLOCK;
    }
    
    s32 fileDescriptor = open(finalName, flag, mode);
    if ( -1 == fileDescriptor )
    {
        return NULL;
    }
    
    //
    // Okay we are in business
    //
    xhandle hFile;
    file&   File = m_lFiles.append( hFile );
    xptr_lock    Ref( m_lFiles );
    
    x_memset(&File.mIOControlBlock, 0, sizeof(File.mIOControlBlock));
    File.mIOControlBlock.aio_fildes = fileDescriptor;
    File.mOffset = 0;
    File.mFlag  = Flags;
    
    return (void*)(u64)(hFile.m_Handle+1);
}

//------------------------------------------------------------------------------
void ios_device::AsyncAbort( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    VERIFY( aio_cancel( File.mIOControlBlock.aio_fildes, &File.mIOControlBlock ) );
}

//------------------------------------------------------------------------------

xfile::sync_state ios_device::Synchronize( void* pFile, xbool bBlock )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    while(true)
    {
        s32 errorCode = aio_error(&File.mIOControlBlock);
        if ( EINPROGRESS == errorCode )
        {
            if ( !bBlock )
            {
                return xfile::SYNC_INCOMPLETE;
            }
            else
            {
                x_Yield();
            }
        }
        else
        {
            if( 0 == errorCode )
            {
                aio_return(&File.mIOControlBlock);
                return xfile::SYNC_COMPLETED;
            }
            else
            {
                break;
            }
        }
    }
    
	// The result is FALSE and the error isn't ERROR_IO_INCOMPLETE, there's a real error!
	return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void ios_device::Close( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    
    //
    // Close the handle
    //
    {
        xptr_lock    Ref( m_lFiles );
        file&   File = m_lFiles( hFile );
        
        if( !close( File.mIOControlBlock.aio_fildes ) )
        {
            ASSERT( 0 );
        }
    }
    
    //
    // Lets free our entry
    //
    m_lFiles.DeleteByHandle( hFile );
}

//------------------------------------------------------------------------------

xbool ios_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 errorCode = 0;
    
    // Set the file pointer (We assume we didnt make any errors)
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mIOControlBlock.aio_buf = pBuffer;
        File.mIOControlBlock.aio_nbytes = Count;
        errorCode = aio_read(&File.mIOControlBlock);
    }
    else
    {
        errorCode = read(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    
    if (-1 == errorCode) 
    {
        return FALSE;
    }
    
    // Not problems
    return TRUE;
}

//------------------------------------------------------------------------------

void ios_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 errorCode = 0;
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mIOControlBlock.aio_buf = (volatile void*)pBuffer;
        File.mIOControlBlock.aio_nbytes = Count;
        errorCode = aio_write(&File.mIOControlBlock);
    }
    else
    {
        errorCode = write(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    
    if ( -1 == errorCode) 
    {
        ASSERT(0);
    }
}

//------------------------------------------------------------------------------

void ios_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 HardwareMode;
    switch( aMode )
    {
        case SKM_ORIGIN: HardwareMode = SEEK_SET; break;
        case SKM_CURENT: HardwareMode = SEEK_CUR; break;
        case SKM_END:    HardwareMode = SEEK_END; break; 
        default: ASSERT(0);               break;
    }
    
    // We will make sure we are sync here
    // WARNING: Potencial time wasted here
    if( File.mFlag & xfile_device_i::ACC_ASYNC )
    {
        VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
    }
    
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mOffset = (s32)lseek(File.mIOControlBlock.aio_fildes, Pos, HardwareMode);
    }
    else
    {
        lseek(File.mIOControlBlock.aio_fildes, Pos, HardwareMode);
    }
}

//------------------------------------------------------------------------------

s32 ios_device::Tell( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    return (s32)lseek(File.mIOControlBlock.aio_fildes, 0, SEEK_CUR);
}

//------------------------------------------------------------------------------

void ios_device::Flush( void* pFile )
{
    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
}

//------------------------------------------------------------------------------

s32 ios_device::Length( void* pFile )
{
    s32 Length;
    s32 Cursor;
    
    Cursor = Tell( pFile ); Seek( pFile, SKM_END,    0 );
    Length = Tell( pFile ); Seek( pFile, SKM_ORIGIN, Cursor );
    
    return Length;
}

//------------------------------------------------------------------------------

xbool ios_device::IsEOF( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        s32 errorCode = aio_error(&File.mIOControlBlock);
        if ( EINPROGRESS == errorCode )
        {
            return TRUE;
        }
        else
        {
            if( 0 != errorCode )
            {
                return TRUE;
            }
        }
    }
    else
    {
        s32 Length;
        s32 Cursor;
        
        Cursor = Tell( pFile ); Seek( pFile, SKM_END,    0 );
        Length = Tell( pFile ); Seek( pFile, SKM_ORIGIN, Cursor );

        if ( Cursor >= Length )
        {
            return TRUE;
        }
    }
    return FALSE;
}
#endif
#endif