#pragma once		// Include this file only once
#ifndef X_FILE_DEVICE_H
#define X_FILE_DEVICE_H

#if defined TARGET_IOS || defined TARGET_OSX
//==============================================================================
// INCLUDES
//==============================================================================
#include <aio.h>
#include "../../x_Base.h"
#include "x_stdio.h"

class ios_device : public xfile_device_i
{
public:
    ios_device ( void ) : xfile_device_i("a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:s:t:u:v:w:x:y:z:"){}
    
    static void                 sInitialize ( void );
    
protected:
    
    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock );
    virtual void                AsyncAbort  ( void* pFile );
    
    struct file
    {
        aiocb       mIOControlBlock;
        s32         mFlag;
        s32         mOffset;
    };
    
    xharray<file>   m_lFiles;
};

#endif // TARGET_IOS

#endif // X_FILE_DEVICE_H