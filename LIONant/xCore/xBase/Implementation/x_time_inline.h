
//------------------------------------------------------------------------------
inline
xtimer::xtimer( void ) :
m_StartTime( 0     ),
m_TotalTime( 0     ),
m_bRunning ( FALSE ),
m_nSamples ( 0     ) {}

//------------------------------------------------------------------------------
inline
void xtimer::Start( void )
{
    if( m_bRunning ) return;

    m_StartTime = x_GetTime();
    m_bRunning  = TRUE;        
    m_nSamples++;
}

//------------------------------------------------------------------------------
inline
void xtimer::Reset( void )
{
    m_bRunning   = FALSE;
    m_StartTime  = 0;
    m_TotalTime  = 0;
    m_nSamples   = 0;
}

//------------------------------------------------------------------------------
inline
xtick xtimer::Stop( void )
{
    if( m_bRunning )
    {
        m_TotalTime += x_GetTime() - m_StartTime;
        m_bRunning   = FALSE;
    }

    return m_TotalTime;
}

//------------------------------------------------------------------------------
inline
f64 xtimer::StopMs( void )
{
    return x_TicksToMs( Stop() );
}

//------------------------------------------------------------------------------
inline
f64 xtimer::StopSec( void )
{
    return x_TicksToSec( Stop() );
}

//------------------------------------------------------------------------------
inline
xtick xtimer::Read( void ) const
{
    if( m_bRunning ) 
        return m_TotalTime + (x_GetTime() - m_StartTime);

    return m_TotalTime;
}

//------------------------------------------------------------------------------
inline
f64 xtimer::ReadMs( void ) const
{
    const f64 Time = x_TicksToMs( Read() );
    return Time;
}

//------------------------------------------------------------------------------

inline
f64 xtimer::ReadSec( void ) const
{
    const f64 Time = x_TicksToSec( Read() );
    return Time;
}

//------------------------------------------------------------------------------
inline
xtick xtimer::Trip( void )
{
    xtick Ticks;

    if( m_bRunning )
    {
        xtick Now = x_GetTime();

        Ticks = m_TotalTime + (Now - m_StartTime);

        m_TotalTime = 0;
        m_StartTime = Now;

        m_nSamples++;
    }
    else
    {
        Ticks = 0;
    }

    return Ticks;
}

//------------------------------------------------------------------------------
inline
f64 xtimer::TripMs( void )
{
    return x_TicksToMs( Trip() );
}

//------------------------------------------------------------------------------
inline
f64 xtimer::TripSec( void )
{
    return x_TicksToSec( Trip() );
}

//------------------------------------------------------------------------------
inline
s32 xtimer::GetSampleCount( void ) const
{
    return m_nSamples;
}

//------------------------------------------------------------------------------
inline
f64 xtimer::GetAverageMs( void ) const
{
    if( m_nSamples <= 0 ) 
        return 0 ;

    return ReadMs() / m_nSamples;
}

//------------------------------------------------------------------------------
inline
xbool xtimer::IsRunning( void ) const
{
    return m_bRunning; 
} 

//------------------------------------------------------------------------------
inline
f64 x_TicksToMs( xtick Ticks )
{
    const f64 Time = ((f64)Ticks)/x_GetTicksPerMs();
    return Time;
}

//------------------------------------------------------------------------------
inline
f64 x_TicksToSec( xtick Ticks )
{
    const f64 Time = ((f64)Ticks)/x_GetTicksPerSecond();
    return Time;
}

//------------------------------------------------------------------------------
inline
f64 x_GetTimeSec( void )
{
    return x_TicksToSec( x_GetTime() );
}

//==============================================================================
// PRIVATE FUNCTIONS
//==============================================================================

void    x_TimeInit          ( void );
void    x_TimeKill          ( void );    


