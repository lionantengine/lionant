//DOM-IGNORE-BEGIN
#pragma once		// Include this file only once

//==============================================================================
//==============================================================================
//==============================================================================
// PRIVATE EXCEPTIONS
//==============================================================================
//==============================================================================
//==============================================================================

#undef x_try                    
#undef x_catch_display          
#undef x_display_exception      
#undef x_append_exception_msg
#undef x_catch_begin            
#undef x_catch_end              
#undef x_throw               
#undef x_append_throw  
#undef x_catch_append       
#undef x_catch_end_ret          

#ifdef X_EXCEPTIONS
//     #define x_try                     try{ ((void)0)
//     #define x_catch_display           } catch( E_PARAM ) { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ BREAK }; } ((void)0)
//     #define x_display_exception                          { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ BREAK }; x_ExceptionDisplay(); }((void)0)
//     #define x_append_exception_msg(S)                    { static u32 exceptions_Flags=X_BIT(2); if( x_ExceptionHandler( __FILE__, __LINE__, S,    exceptions_Flags)){ BREAK };  }((void)0)
//     #define x_catch_begin             } catch( E_PARAM ) { ((void)0)
//     #define x_catch_end               } ((void)0)
//     #define x_throw(S)                do{ static u32 exceptions_Flags=0; if( x_ExceptionHandler( __FILE__, __LINE__, S, exceptions_Flags )){ BREAK }; throw(0); } while(0)
//     #define x_append_throw(S)         x_append_exception_msg(S); throw(0)
//     #define x_catch_append(S)         x_catch_begin; x_append_throw(S); x_catch_end
//     #define x_catch_end_ret           x_append_throw(NULL); x_catch_end   
#define x_try                           try{ ((void)0)
#define x_catch_display                 } catch( E_PARAM ) { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ X_BREAK }; } ((void)0)
#define x_display_exception                          { static u32 exceptions_Flags=0;        if( x_ExceptionHandler( __FILE__, __LINE__, NULL, exceptions_Flags)){ BREAK }; x_ExceptionDisplay(); }((void)0)
#define x_append_exception_msg(S)                    { static u32 exceptions_Flags=X_BIT(2); if( x_ExceptionHandler( __FILE__, __LINE__, S,    exceptions_Flags)){ BREAK };  }((void)0)
#define x_catch_begin                   } catch( E_PARAM ) { ((void)0)
#define x_catch_end                     } ((void)0)
#define x_throw(S, ...)                 do{ static u32 exceptions_Flags=0; if( x_ExceptionHandler( __FILE__, __LINE__, (const char*)xfs(S, ##__VA_ARGS__ ), exceptions_Flags )){ X_BREAK }; throw(0); } while(0)
#define x_append_throw(S)               x_append_exception_msg(S); throw(0)
#define x_catch_append(S)               x_catch_begin; x_append_throw(S); x_catch_end
#define x_catch_end_ret                 x_append_throw(NULL); x_catch_end   

#else
    #define x_try                     if( 1 ) {
    #define x_catch_display           }
    #define x_display_exception     
    #define x_append_exception_msg(S) ASSERT(0 && S);
    #define x_catch_begin             } if( 0 ) {
    #define x_catch_end               }
    #define x_throw( ... )            ASSERTS(0, (const char*)xfs( __VA_ARGS__ ) )
    #define x_append_throw( S )       ASSERTS(0,S)

    #define x_catch_append(S)         x_catch_begin; x_append_throw(S); x_catch_end
    #define x_catch_end_ret           x_append_throw(NULL); x_catch_end
#endif

#define x_catch_end_display x_display_exception; x_catch_end
//==============================================================================
//==============================================================================
//==============================================================================
// PRIVATE ASSERT
//==============================================================================
//==============================================================================
//==============================================================================

template<int N>
struct print_sizeof_as_warning
{
    char operator()( ) { return N + 0xff; } //deliberately causing overflow
};


//==============================================================================
// PRIVATE SCOPE
//==============================================================================

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The scope node is statically allocated in each place where the macro is
//     used.
//------------------------------------------------------------------------------
struct xscope_node
{
    inline xscope_node( const char* pFileName, const char* pScopeName, s32 LineNumer ) :
            m_pFileName(pFileName), m_pScopeName(pScopeName), m_LineNumber(LineNumer) {}

    const char* const   m_pFileName;
    const char* const   m_pScopeName;
    const s32           m_LineNumber;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     There is one of this per thread.
//------------------------------------------------------------------------------
struct xscope_info
{
    xsafe_array<const xscope_node*,32> m_Scope;
    s32                                m_CurScope;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Function needed to get the scope_info for the right thread
//------------------------------------------------------------------------------
xscope_info& x_GetScopeInfo( void );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is the class that pushes and pops the scope info into the local thread
//------------------------------------------------------------------------------
struct xdebug_scope
{
    inline xdebug_scope( const xscope_node& Node ) 
    {         
        xscope_info& Info = x_GetScopeInfo();
        Info.m_Scope[Info.m_CurScope] = &Node;
        Info.m_CurScope++;
    }

   inline ~xdebug_scope( void )		         
   { 
        xscope_info& Info = x_GetScopeInfo();
        Info.m_CurScope--; 
   }
};

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
// SAFE ARRAY
//==============================================================================
//==============================================================================
//==============================================================================

/*
//------------------------------------------------------------------------------

template< class T, s32 Count > inline
T& xsafe_array<T,Count>::operator[]( s32 Index )
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}

//------------------------------------------------------------------------------

template< class T, s32 Count > inline
T& xsafe_array<T,Count>::operator[]( u32 Index )
{
    ASSERT(Index<(u32)Count);
    return m_Data[Index];
}

//------------------------------------------------------------------------------

template< class T, s32 Count > inline
T& xsafe_array<T,Count>::operator[]( u16 Index )
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}

//------------------------------------------------------------------------------

template< class T, s32 Count > inline
T& xsafe_array<T,Count>::operator[]( s16 Index )
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}

//------------------------------------------------------------------------------

template< class T, s32 Count > inline
T& xsafe_array<T,Count>::operator[]( u8 Index )
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}
*/


//------------------------------------------------------------------------------

template< class T, s32 Count >  template< class I > inline
T& xsafe_array<T,Count>::operator[]( I Index )
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}

//------------------------------------------------------------------------------

template< class T, s32 Count >  template< class I > inline
const T& xsafe_array<T,Count>::operator[]( I Index ) const
{
    ASSERT(Index>=0);
    ASSERT(Index<Count);
    return m_Data[Index];
}


//------------------------------------------------------------------------------

template< class T, s32 Count > inline
void xsafe_array<T,Count>::Copy( s32 DestIndexOffset, const T* pSrc, s32 aCount )
{
    ASSERT( aCount >= 0 );
    ASSERT( DestIndexOffset >= 0 );
    ASSERT( DestIndexOffset < Count );
    ASSERT( (DestIndexOffset + aCount) <= Count );
    x_memcpy( &m_Data[DestIndexOffset], pSrc, aCount*sizeof(T) );
}

//------------------------------------------------------------------------------

template< class T, s32 Count > inline
void xsafe_array<T,Count>::SetMemory( s32 C )
{
    x_memset( m_Data, C, Count*sizeof(T) );
}


//==============================================================================
//==============================================================================
//==============================================================================
// LOG
//==============================================================================
//==============================================================================
//==============================================================================
#ifdef X_DEBUG
    void x_LogPush( const char* pFileName, s32 LineNum );
    #define x_LogWarning    x_LogPush( __FILE__, __LINE__ ), x_LogWarning
    #define x_LogError      x_LogPush( __FILE__, __LINE__ ), x_LogError
    #define x_LogMessage    x_LogPush( __FILE__, __LINE__ ), x_LogMessage
#else
    inline void             x_DebugNull ( ... ){ }
#endif

//==============================================================================
// XDEBUG LEVEL
//==============================================================================

extern xdebug_level g_xDebugLevel;

//------------------------------------------------------------------------------

inline xdebug_level x_GetDebugLevel( void )
{
    if( g_xDebugLevel >= XDEBUG_LEVEL_FORCE_FAST )
        return (xdebug_level)(g_xDebugLevel - XDEBUG_LEVEL_FORCE_FAST);

    return g_xDebugLevel;
}


//------------------------------------------------------------------------------

inline void x_SetDebugLevel( xdebug_level Level )
{
    g_xDebugLevel = Level;
}

//------------------------------------------------------------------------------

inline xdebug_level x_ChooseDebugLevel( s32 LocalLevel )
{
    if( LocalLevel >= XDEBUG_LEVEL_FORCE_FAST )
        return (xdebug_level)(LocalLevel - XDEBUG_LEVEL_FORCE_FAST);

    if( g_xDebugLevel >= XDEBUG_LEVEL_FORCE_FAST )
        return (xdebug_level)(g_xDebugLevel - XDEBUG_LEVEL_FORCE_FAST);

    return g_xDebugLevel > LocalLevel ? g_xDebugLevel : (xdebug_level)LocalLevel;
}


//==============================================================================
// INITIALIZE PER INSTANCE
//==============================================================================

void x_DebugInstanceInit( void );
void x_DebugInstanceKill( void );

//==============================================================================
// END
//==============================================================================
//DOM-IGNORE-END
