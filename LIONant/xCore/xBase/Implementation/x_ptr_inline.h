//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Xptr_ref Functions
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template< class T > inline
T& xptr_ref<T>::operator[]( s32 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_pPtr[Index];
}

//------------------------------------------------------------------------------
template< class T > inline
T& xptr_ref<T>::operator[]( u16 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_pPtr[Index];
}

//------------------------------------------------------------------------------
template< class T > inline
T& xptr_ref<T>::operator[]( s16 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_pPtr[Index];
}

//------------------------------------------------------------------------------
template< class T > inline
T& xptr_ref<T>::operator[]( u8 Index )
{
    ASSERT( Index >= 0 );
    ASSERT( Index < m_Count );
    return m_pPtr[Index];
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Xptr Functions
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

template< class T > inline
T* xptr<T>::getPtr( void ) const
{
    ASSERT( m_pPtr );
    return m_pPtr;
}

//------------------------------------------------------------------------------

template< class T > inline
xptr<T>::xptr( void )
{
    m_pPtr  = NULL;
    m_Count = 0;
    m_Flags = 0;
}

//------------------------------------------------------------------------------

template< class T > inline
xptr<T>::xptr( T* pPtr, s32 Count, u32 Flags )
{
    m_pPtr  = pPtr;
    m_Count = Count;
    m_Flags = Flags;
}

//------------------------------------------------------------------------------

template< class T > inline
typename xptr<T>::x_smart_xptr& xptr<T>::getSmartPointer( void ) const
{
    ASSERT(m_pPtr);
    ASSERT(!m_bStaticMemory);
    s32 FinalOffset = x_Align( s32(sizeof(T)*getCount()), 4 );
    x_smart_xptr& SmartPtr = *((x_smart_xptr*)&((u8*)m_pPtr)[FinalOffset]);
    return SmartPtr;
}

//------------------------------------------------------------------------------

template< class T > template< class K, s32 Count >
xptr<T>::xptr( const xsafe_array<K, Count>& Ptr )
{
    m_Flags         = 0;
    m_Count         = Ptr.getCount();
    ASSERT( m_Count < COUNT_MASK );
    
    m_pPtr  = &Ptr[0];
    m_bStaticMemory = TRUE;
    m_bIsReadOny    = TRUE;
}

//------------------------------------------------------------------------------

template< class T > template< class K, s32 Count >
xptr<T>::xptr( xsafe_array<K, Count>& Ptr )
{
    m_Count = Ptr.getCount();
    ASSERT( m_Count < COUNT_MASK );
    m_pPtr  = &Ptr[0];
    m_bStaticMemory = TRUE;
}

//------------------------------------------------------------------------------

template< class T > inline
xptr<T>::~xptr( void )
{
    if( m_pPtr ) Destroy();
}

//------------------------------------------------------------------------------

template< class T > inline
xbool xptr<T>::isValid( void ) const
{ 
    return m_pPtr != NULL;
}

//------------------------------------------------------------------------------
template< class T > inline
s32 xptr<T>::getXPtrCount( void ) const
{
    return getSmartPointer().m_RefCount.get();
}

//------------------------------------------------------------------------------
template< class T > inline
s32 xptr<T>::getXRefCount( void ) const
{
    if( m_pPtr == NULL ) return 0;
#ifdef X_DEBUG
    return getSmartPointer().m_nAccesses.get();
#else
    return 0;
#endif
}

//------------------------------------------------------------------------------

template< class T > noinline
void xptr<T>::Alloc( s32 Count, xmem_flags Flags )
{
    if( m_pPtr ) Destroy();

    s32 FinalOffset = x_Align( s32(sizeof(T)*Count), 4 );
    s32 FinalSize   = FinalOffset + sizeof(x_smart_xptr);
    
    m_pPtr = (T*)x_malloc( sizeof(u8), FinalSize, xmem_UpdateAligtmentForA(Flags, xmem_aligment(T)) );
    ASSERT( m_pPtr );
 
    m_Flags     = 0;
    m_Count     = Count;
    m_bWasAlloc = TRUE;
    
    x_smart_xptr& SmartPtr = getSmartPointer();
    SmartPtr.m_RefCount.setup(1);
#ifdef X_DEBUG
    SmartPtr.m_nAccesses.setup(0);
#endif
}

//------------------------------------------------------------------------------

template< class T > noinline
void xptr<T>::New( s32 Count, xmem_flags Flags )
{
    // Allocate the memory first
    Alloc( Count, Flags );

    // Now we need to construct the right number of items
    const s32 RealCount = getCount();
    for( s32 i = 0; i < RealCount; i++ )
    {
        x_Construct(&m_pPtr[i]);
    }
    
    // Now we can say is not longer allocated
    m_bWasAlloc = FALSE;
}

//------------------------------------------------------------------------------

template< class T > noinline
void xptr<T>::Destroy( void )
{
    if( isValid() == FALSE ) return;
    if( m_bStaticMemory ) return;
    ASSERT( !m_bIsReadOny );
    
    // Get the smart pointer
    x_smart_xptr& SmartPointer = getSmartPointer();
    
#ifdef X_DEBUG
    ASSERT( SmartPointer.m_nAccesses.get() == 0 );
#endif
    
    //
    // Deal with Ref counting if we have to
    //
    const s32 val = SmartPointer.m_RefCount.Dec();
    
    // Tomas: seems like we are ignoring this??
    if( val > 0 )
    {
        // Lets clear this pointer
        m_pPtr  = NULL;
        m_Count = 0;
        m_Flags = 0;

        return;
    }
    
    //
    // See if we need to call destructors
    //
    if( !m_bWasAlloc )
    {
        const s32 Count = getCount();
        for( s32 i=0; i<Count; i++ )
        {
            ((destructor*)&m_pPtr[i])->~destructor();
        }
    }

    // Free up the memory
    x_free( m_pPtr );

    // Lets clear this pointer
    m_pPtr      = NULL;
    m_Count     = 0;
    m_Flags     = 0;
}

//------------------------------------------------------------------------------

template< class T > inline
const xptr<T>&	xptr<T>::operator = ( const xptr<T>& Ptr )
{
    // We are are doing the same pointer then just leave return
    if( Ptr.m_pPtr == m_pPtr )
    {
        m_Count = Ptr.m_Count;
        m_Flags = Ptr.m_Flags;

        return *this;
    }

    // Clear exiting
    if( m_pPtr ) Destroy();

    // Handle new
    m_pPtr  = Ptr.m_pPtr;
    m_Count = Ptr.m_Count;
    m_Flags = Ptr.m_Flags;

    // Deal with Ref counting if we have to
    if( !m_bStaticMemory )
    {
        getSmartPointer().m_RefCount.Inc();
    }
    
    return *this;
}

//------------------------------------------------------------------------------

template< class T > inline
xptr<T>::xptr( xptr<T>& Ptr )
{
    m_pPtr      = NULL;
    m_Count     = 0;
    m_Flags     = 0;

    *this   = Ptr;
}

//------------------------------------------------------------------------------

template< class T > inline
xptr<T>::xptr( const xptr<T>& Ptr )
{
    m_pPtr      = NULL;
    m_Count     = 0;
    m_Flags     = 0;

    *this   = Ptr;

    m_bIsReadOny = TRUE;
}

//------------------------------------------------------------------------------

template< class T > inline
void xptr<T>::Resize( s32 NewCount )
{
    ASSERT( m_bStaticMemory == FALSE );
    ASSERT( m_bIsReadOny == FALSE );
    ASSERT( m_bIsUnresizable == FALSE );

    // Do we have anything allocated yet?
    if( m_Count == 0 )
    {
        Alloc( NewCount );
        return;
    }
    
    // Get the smart pointer
    x_smart_xptr& SmartPointer = getSmartPointer();
    
    // Someone has a reference to this memory. I can't grow it if someone has a reference.
#ifdef X_DEBUG
    ASSERT( SmartPointer.m_RefCount.get() == 1 );
    ASSERT( SmartPointer.m_nAccesses.get() == 0 );
#endif

    if( m_pPtr == NULL || m_bWasAlloc )
    {
        s32          FinalOffset = x_Align( s32(sizeof(T)*NewCount), 2 );
        s32          FinalSize   = FinalOffset + sizeof(x_smart_xptr);
        x_smart_xptr SmartBackup = SmartPointer;
        
        m_pPtr = (T*)x_realloc( m_pPtr, sizeof(u8), FinalSize );
        ASSERT( m_pPtr);
        
        // Set the new data
        m_Flags       = 0;
        
        m_Count = NewCount;
        getSmartPointer() = SmartBackup;
        m_bWasAlloc = TRUE;
    }
    else
    {
        s32 OldCount = m_Count;

        //
        // Need to delete some nodes
        //
        if ( NewCount < OldCount )
        {
            for ( s32 i = NewCount; i < OldCount; i++ )
            {
                x_Destruct( &( *this )[i] );
            }
        }

        s32          FinalOffset = x_Align( s32(sizeof(T)*NewCount), 2 );
        s32          FinalSize   = FinalOffset + sizeof(x_smart_xptr);
        x_smart_xptr SmartBackup = SmartPointer;
        
        m_pPtr = (T*)x_realloc( m_pPtr, sizeof(u8), FinalSize );
        ASSERT( m_pPtr);
        
        // Set the new data
        m_Flags       = 0;
        
        m_Count = NewCount;
        getSmartPointer() = SmartBackup;
        m_bWasAlloc = FALSE;

        //
        // Construct any new nodes
        //
        if ( NewCount > OldCount )
        {
            for ( s32 i = OldCount; i < NewCount; i++ )
            {
                x_Construct( &( *this )[i] );
            }
        }
    }
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xptr<T>::getCount( void ) const
{
    s32 Count = m_Count;
    return Count;
}

//------------------------------------------------------------------------------

template< class T > inline
s32 xptr<T>::getByteCount( void ) const
{
    return getCount()*sizeof(T);
}

//------------------------------------------------------------------------------

template< class T > inline
void xptr<T>::StartAccess( void ) const
{
#ifdef X_DEBUG
    getSmartPointer().m_nAccesses.Inc();
#endif
}

//------------------------------------------------------------------------------

template< class T > inline
void xptr<T>::EndAccess( void ) const
{
#ifdef X_DEBUG
    getSmartPointer().m_nAccesses.Dec();
#endif
}

//------------------------------------------------------------------------------

template< class T > 
template< class J > inline
T& xptr<T>::operator []( J Index )
{
    ASSERT( s32(Index) < getCount()  );
    ASSERT( m_bIsReadOny == FALSE );

    return getPtr()[Index];
}

//------------------------------------------------------------------------------

template< class T > 
template< class J > inline
const T& xptr<T>::operator []( J Index ) const
{
    ASSERT( s32(Index) < getCount() );

    return getPtr()[Index];
}

//------------------------------------------------------------------------------

template< class T > inline
const T* xptr<T>::operator ->( void ) const
{
    return &(*this)[0];
}

//------------------------------------------------------------------------------

template< class T > inline
T* xptr<T>::operator ->( void )
{
    ASSERT( m_bIsReadOny == FALSE );
    return &(*this)[0];
}

//------------------------------------------------------------------------------
template<class T> inline
void xptr<T>::Copy( s32 DestinationOffset, const xptr<T>& SourceRef, s32 SourceOffset, s32 ElementCount )
{
    // Count must be positive
    ASSERT(ElementCount>=0);
    ASSERT( m_bIsReadOny == FALSE );

    if( ElementCount == 0 ) return;

    // Make sure we are in the right ranges
    ASSERT( &(*this)[DestinationOffset+ElementCount-1] );
    ASSERT( &SourceRef[SourceOffset+ElementCount-1] );

    x_memmove( &(*this)[DestinationOffset], &SourceRef[SourceOffset], ElementCount*sizeof(T) ); 
}

//------------------------------------------------------------------------------
template<class T> inline
void xptr<T>::Copy( const xptr<T>& SourceRef )
{
    Destroy();

    const s32 Count = SourceRef.getCount();
    if( Count <= 0 )
        return;

    // Allocate the memory first
    New( Count );
    for( s32 i=0; i<Count; i++ )
        (*this)[i] = SourceRef[i];
}

//------------------------------------------------------------------------------
template<class T> inline
void xptr<T>::SetMemory( s32 Value )
{
    ASSERT( m_bIsReadOny == FALSE );
    x_memset( (*this), Value, sizeof(T)*getCount() );
}

//------------------------------------------------------------------------------
template<class T> inline
xptr<T>::operator const T* ( void ) const
{
    return &(*this)[0];
}

//------------------------------------------------------------------------------
template<class T> inline
xptr<T>::operator T* ( void ) 
{
    ASSERT( m_bIsReadOny == FALSE );
    return &(*this)[0];
}
