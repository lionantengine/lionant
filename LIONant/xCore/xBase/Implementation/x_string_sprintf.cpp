#include<math.h>
#include "../x_base.h"

#define x_modf modf

//==============================================================================
// DEFINES
//==============================================================================

// --- FLAGS FOR THE VSPRINTF ---

// NUMERIC VALUES
#define LONGDBL			0x01        // long double; unimplemented
#define LONGINT			0x02        // long integer
#define QUADINT			0x04        // quad integer
#define SHORTINT		0x08        // short integer
#define NUMBERMASK		0x0F

// OTHER FLAGS
#define ALT				0x10        // alternate form
#define HEXPREFIX		0x20        // add 0x or 0X prefix
#define LADJUST			0x40        // left adjustment
#define ZEROPAD			0x80        // zero (as opposed to blank) pad

// CONSTANTS
#define SPF_LONG_MAX    0x7FFFFFFF
#define WORKSIZE		128         // space for %c, %[diouxX], %[eEfgG]
#define DEFPREC         6           // number of precision for the real numbers


//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
// vsprintf_pow
//------------------------------------------------------------------------------
static
f64 vsprintf_pow( f64 x, s32 p )
{
    f64 r;

    if (p == 0) return 1.0;
    if (x == 0.0) return 0.0;

    if (p < 0)
    {
        p = -p;
        x = 1.0 / x;
    }

    r = 1.0;
    for(;;)
    {
        if (p & 1) r *= x;
        if ((p >>= 1) == 0) return r;
        x *= x;
    }
}

//------------------------------------------------------------------------------
// Check to see if it is infinite
//------------------------------------------------------------------------------
static
s32 IsINF( f64 x )
{
    return x != x;
}

//------------------------------------------------------------------------------
// Check to see if it is NOT a number
//------------------------------------------------------------------------------
static
s32 IsNAN( f64 x )
{
    return x != x;
}

//------------------------------------------------------------------------------
// fmtbase - String where the output is going to go
//          (Make sure that the string is atleast 24 bytes long)
// fpnum   - number which is going to be converted
// cvt     - what type of conversion needs
// width   - total width of the output number
// prec    - how many digits of precision
//------------------------------------------------------------------------------
static
s32 dtoa( char* fmtbase, f64 fpnum, char cvt, s32 width, s32 prec )
{
    static const f64 PowTable[]= {1,10,10e1,10e2,10e3,10e4,10e5,10e6,10e7,10e8,10e9,10e10,10e11,10e12,10e13,10e14,10e15,10e16,10e17,10e18,10e19,10e20,10e21,10e22,10e23 };
    char    fwork[WORKSIZE];
    char*   fw;

    // for integer part
    char    iwork[WORKSIZE];
    char*   iworkend;
    char*   iw;

    // for exponent part
    char    ework[16];
    char*   eworkend;
    char*   ew;

    // other variables
    s32     is_neg;
    f64     powprec;
    f64     rounder;
    s32     f_fmt;
    s32     iwidth;
    s32     fwidth;
    s32     ewidth;

    // arrange everything in returned string variables
    char*   fmt;
    s32     i;
    s32     showdot;
    s32     fmtwidth;
    s32     pad;

    ASSERT(fmtbase);
    //ASSERT(width > 0);
    ASSERT(prec >= 0);

    // initialize some variables
    fw        = fwork;

    // set up the integer part
    iworkend  = &iwork[sizeof(iwork) - 1];
    iw        = iworkend;
    *iw       = 0;

    // setup the exponent part
    eworkend  = &ework[sizeof(ework) - 1];
    ew        = eworkend;
    *ew       = 0;

    if (IsINF(fpnum))
    {
        char* inffmt  = fmtbase;
        char* inffmtp = inffmt;

        if (fpnum < 0) *inffmtp++ = '-';
        x_strcpy(inffmtp, 4, "Inf");
        return (s32)(inffmt - fmtbase);
    }

    if (IsNAN(fpnum))
    {
        char* nanfmt = fmtbase;
        x_strcpy(nanfmt, 4, "NaN");
        return (s32)(nanfmt - fmtbase);
    }

    // grab sign & make non-negative
    is_neg = fpnum < 0;
    if (is_neg) fpnum = -fpnum;

    // precision matters

    // can't have more prec than supported
    if (prec > WORKSIZE - 2) prec = WORKSIZE - 2;

    if (prec == 6) powprec = 1.0e6;
    else
    {
        if( prec > 23 )
        {
            powprec = vsprintf_pow( 10.0, prec );
        }
        else
        {
            powprec = PowTable[prec];
        }
    }

    rounder = 0.5 / powprec;

    f_fmt = cvt == 'f' ||
          ((cvt == 'g') && (fpnum == 0.0 || (fpnum >= 1e-4 && fpnum < powprec)));

    iwidth = 0;
    fwidth = 0;
    ewidth = 0;

    if (f_fmt)  // fixed format
    {
        f64 ipart;
        f64 fpart = x_modf(fpnum, &ipart);

        // convert fractional part
        if (fpart >= rounder || cvt != 'g')
        {
            f64  ifpart;
            s32     i;
            f64  ffpart;

            fpart += rounder;
            if (fpart >= 1.0)
            {
                ipart += 1.0;
                fpart -= 1.0;
            }

            ffpart = fpart;

            xbool bAnySignificantInteger=FALSE;
            for (i = 0; i < prec; ++i)
            {
                ffpart = x_modf(ffpart * 10.0, &ifpart);
                *fw++ = (char)('0' + (s32)(ifpart));

                // This was added to keep prec number of significan digits
                // When the user is looking for higher precision we will do this
                if( prec > DEFPREC )
                {
                    if( bAnySignificantInteger==FALSE && ifpart == '0' )  prec++;
                    else bAnySignificantInteger = TRUE;
                }

                ++fwidth;
            }

            if (cvt == 'g')  // inhibit trailing zeroes if g-fmt
            {
                char* p;
                for ( p = fw - 1; p >= fwork && *p == '0'; --p)
                {
                    *p = 0;
                    --fwidth;
                }
            }
        }

        // convert integer part
        if (ipart == 0.0)
        {
            // Removed this because I want to always see the zero I dont like things
            // that look like ".324234" I like "0.324234" to make sure we dont give
            // up precision I incremented the space for the number.
            prec++;
            //if (cvt != 'g' || fwidth < prec || fwidth < width)
            {
                *--iw = '0'; ++iwidth;
            }
        }
        else
            if (ipart <= (f64)(SPF_LONG_MAX)) // a useful speedup
            {
                s32 li = (s32)(ipart);
                while (li != 0)
                {
                    *--iw = (char)('0' + (li % 10));
                    li = li / 10;
                    ++iwidth;
                    ASSERT( iwidth < WORKSIZE );
                }
            }
            else // the slow way
            {
                while (ipart > 0.5)
                {
                    f64 ff = x_modf(ipart / 10.0, &ipart);
                    ff = (ff + 0.05) * 10.0;
                    *--iw = (char)('0' + (s32)(ff));
                    ++iwidth;
                    ASSERT( iwidth < WORKSIZE );
                }
            }

        // g-fmt: kill part of frac if prec/width exceeded
        if (cvt == 'g')
        {
            s32 m = prec;
            s32 adj;

            if (m < width) m = width;

            adj = iwidth + fwidth - m;
            if (adj > fwidth) adj = fwidth;

            if (adj > 0)
            {
                char* f;
                for (f = &fwork[fwidth-1]; f >= fwork && adj > 0; --adj, --f)
                {
                    char ch = *f;

                    --fwidth;
                    *f = 0;

                    if (ch > '5') // properly round: unavoidable propagation
                    {
                        char* p;
                        s32 carry = 1;

                        for ( p = f - 1; p >= fwork && carry; --p)
                        {
                            ++*p;

                            if (*p > '9') *p = '0';
                            else carry = 0;
                        }

                        if (carry)
                        {
                            for (p = iworkend - 1; p >= iw && carry; --p)
                            {
                                ++*p;
                                if (*p > '9') *p = '0';
                                else carry = 0;
                            }

                            if (carry)
                            {
                                *--iw = '1';
                                ++iwidth;
                                --adj;
                                ASSERT( iwidth < WORKSIZE );
                            }
                        }
                    }
                }

                // kill any additinal trailing zeros
                {
                    char* p;
                    for ( p = f; p >= fwork && *p == '0'; --p)
                    {
                        *p = 0;
                        --fwidth;
                    }
                }
            }
        }
    }
    else  // e-fmt
    {
        f64     almost_one;
        f64     ipart;
        f64     fpart;
        f64     ffpart;
        f64     ifpart;
        s32     i;
        char    eneg;
        s32     exp;

        // normalize
        exp = 0;

        while (fpnum >= 10.0)
        {
            fpnum *= 0.1;
            ++exp;
        }

        almost_one = 1.0 - rounder;

        while (fpnum > 0.0 && fpnum < almost_one)
        {
            fpnum *= 10.0;
            --exp;
        }

        fpart = x_modf(fpnum, &ipart);

        if (cvt == 'g')     // used up one digit for int part...
        {
            --prec;
            powprec /= 10.0;
            rounder = 0.5 / powprec;
        }

        // convert fractional part -- almost same as above
        if (fpart >= rounder || cvt != 'g')
        {
            fpart += rounder;

            if (fpart >= 1.0)
            {
                fpart -= 1.0;
                ipart += 1.0;

                if (ipart >= 10.0)
                {
                    ++exp;
                    ipart /= 10.0;
                    fpart /= 10.0;
                }
            }

            ffpart = fpart;

            for (i = 0; i < prec; ++i)
            {
                ffpart = x_modf(ffpart * 10.0, &ifpart);
                *fw++ = (char)('0' + (s32)(ifpart));
                ++fwidth;
                ASSERT( fwidth < WORKSIZE );
            }

            if (cvt == 'g')  // inhibit trailing zeroes if g-fmt
            {
                char* p;

                for ( p = fw - 1; p >= fwork && *p == '0'; --p)
                {
                    *p = 0;
                    --fwidth;
                    ASSERT( fwidth >= 0 );
                }
            }
        }


        // convert exponent
        eneg = exp < 0;
        if (eneg) exp = - exp;

        while (exp > 0)
        {
            *--ew = (char)('0' + (exp % 10));
            exp /= 10;
            ++ewidth;
            ASSERT( ewidth < 16 );
        }

        while (ewidth <= 2)  // ensure at least 2 zeroes
        {
            *--ew = '0';
            ++ewidth;
            ASSERT( ewidth < 16 );
        }

        *--ew = eneg ? '-' : '+';
        *--ew = 'e';

        ewidth += 2;

        // convert the one-digit integer part
        *--iw = (char)('0' + (s32)(ipart));
        ++iwidth;

    }

    // arrange everything in returned string
    showdot = cvt != 'g' || fwidth > 0;
    fmtwidth = is_neg + iwidth + showdot + fwidth + ewidth;
    pad = width - fmtwidth;

    if (pad < 0) pad = 0;

    //fmtbase = (char *)malloc(fmtwidth + pad + 1); // NOW IS PAST AS A PARAMETER
    fmt = fmtbase;

    for (i = 0; i < pad; ++i) *fmt++ = ' ';

    if (is_neg) *fmt++ = '-';

    for (i = 0; i < iwidth; ++i) *fmt++ = *iw++;

    if (showdot)
    {
        *fmt++ = '.';
        fw = fwork;
        for (i = 0; i < fwidth; ++i) *fmt++ = *fw++;
    }

    for (i = 0; i < ewidth; ++i) *fmt++ = *ew++;

    *fmt = 0;


    return (s32)(fmt - fmtbase);
}

//------------------------------------------------------------------------------
// Convert an unsigned long to ASCII for printf purposes, returning
// a pointer to the first character of the string representation.
// Octal numbers can be forced to have a leading zero; hex numbers
// use the given digits.
//------------------------------------------------------------------------------
// val      - Numeric value to be converted
// endp     - String to be fill in fromt the end
// base     - base of the number (10, 8, 16)
// octzero  - flag to add to the oct numbers the 0 in front
// xdigs    - Hexadecimal string array of number 0,1,2,3,4,5,9,A,or a, ..etc
//------------------------------------------------------------------------------
#define to_digit(c) ((c) - '0')
#define is_digit(c) ((u32)to_digit(c) <= 9)
#define to_char(n)  ((char)((n) + '0'))

static
char* ULtoA( u32 val, char* endp, s32 base, xbool octzero, const char* xdigs )
{
    char *cp = endp;
    s32   sval;

    ASSERT(endp);
//    ASSERT(xdigs);

    // Handle the three cases separately, in the hope of getting
    // better/faster code.

    switch (base)
    {
    case 10:
            if (val < 10)
            {   // many numbers are 1 digit
                *--cp = to_char(val);
                return (cp);
            }


            // On many machines, unsigned arithmetic is harder than
            // signed arithmetic, so we do at most one unsigned mod and
            // divide; this is sufficient to reduce the range of
            // the incoming value to where signed arithmetic works.

            if (val > SPF_LONG_MAX)
            {
                *--cp = to_char(val % 10);
                sval = (s32)(val / 10);
            }
            else
            {
                sval = (s32)val;
            }

            do
            {
                *--cp = to_char(sval % 10);
                sval /= 10;
            } while (sval != 0);

            break;

    case 8:
            do
            {
                *--cp = to_char(val & 7);
                val >>= 3;
            } while (val);

            if (octzero && *cp != '0') *--cp = '0';

            break;

    case 16:
            do
            {
                *--cp = xdigs[val & 15];
                val >>= 4;
            } while (val);

            break;

    default:            /* oops */
        return NULL;
    }

    return (cp);
}

//------------------------------------------------------------------------------
// Same as above but for s64
//------------------------------------------------------------------------------
static
char* UQtoA( u64 val, char* endp, s32 base, xbool octzero, const char* xdigs )
{
    char *cp = endp;
    s64   sval;

    ASSERT(endp);
//    ASSERT(xdigs);

    // Handle the three cases separately, in the hope of getting
    // better/faster code.

    switch (base)
    {
    case 10:
            if (val < 10)
            {   // many numbers are 1 digit
                *--cp = to_char(val);
                return (cp);
            }


            // On many machines, unsigned arithmetic is harder than
            // signed arithmetic, so we do at most one unsigned mod and
            // divide; this is sufficient to reduce the range of
            // the incoming value to where signed arithmetic works.

            if (val > ((~(u64)0)>>1) )
            {
                *--cp = to_char(val % 10);
                sval = (s64)(val / 10);
            }
            else
            {
                sval = (s64)val;
            }

            do
            {
                *--cp = to_char(sval % 10);
                sval /= 10;
            } while (sval != 0);

            break;

    case 8:
            do
            {
                *--cp = to_char(val & 7);
                val >>= 3;
            } while (val);

            if (octzero && *cp != '0') *--cp = '0';

            break;

    case 16:
            do
            {
                *--cp = xdigs[val & 15];
                val >>= 4;
            } while (val);

            break;

    default:            /* oops */
        return NULL;
    }

    return (cp);
}

//------------------------------------------------------------------------------
// WriteToBuffer
//------------------------------------------------------------------------------
static
void WriteToBuffer(char** TempBufferPtr, s32 MaxChars, const char* String, s32 Size )
{
    s32 i;

    ASSERT(String);
    ASSERT(Size>=0);

    for (i=0; i < Size; i++)
    {
        **TempBufferPtr = String[i];
        (*TempBufferPtr)++;
    }
}

//------------------------------------------------------------------------------
// Choose PADSIZE to trade efficiency vs. size.  If larger printf
// fields occur frequently, increase PADSIZE and make the initialisers
// below longer.
//------------------------------------------------------------------------------
#define PADSIZE  16     // pad chunk size

static
void PadBuffer( char** TempBufferPtr, s32 MaxChars, s32 HowMany, char With )
{
    s32 i;
    char* Type;
    static char Blanks[PADSIZE] =
     {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '};
    static char Zeroes[PADSIZE] =
     {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};

    // Find what type we need to path with
    Type = (With == ' ') ? Blanks : Zeroes;

    if ( HowMany > 0 )
    {
        for (i = HowMany; i > PADSIZE; i -= PADSIZE)
        {
            WriteToBuffer( TempBufferPtr, MaxChars, Type, PADSIZE );
        }

        WriteToBuffer( TempBufferPtr, MaxChars, Type, i );
    }
}

//------------------------------------------------------------------------------
// To extend shorts properly, we need both signed and unsigned
// argument extraction methods.
//------------------------------------------------------------------------------
s32 GetSignARG( xva_list* ap, s32 flags )
{
    ASSERT(ap);
    ASSERT(*ap);

    switch (flags & NUMBERMASK)
    {
    case QUADINT:   ASSERT(0); // This case is handle inline
    case LONGDBL:   ASSERT(0); //return x_va_arg(ap, long);
    case LONGINT:   return x_va_arg(*ap, s32);
    case SHORTINT:  return x_va_arg(*ap, s32);
    }

    return x_va_arg(*ap, s32);
}

//------------------------------------------------------------------------------
// To extend shorts properly, we need both signed and unsigned
// argument extraction methods.
//------------------------------------------------------------------------------
u32 GetUnSignARG( xva_list& ap, s32 flags )
{
    ASSERT(ap);
    //ASSERT(*ap);

    switch (flags & NUMBERMASK)
    {
    case QUADINT:   ASSERT(0); // this case is handle inline    
    case LONGDBL:   ASSERT(0); //return x_va_arg(ap, long);
    case LONGINT:   return x_va_arg(ap, u32);
    case SHORTINT:  return x_va_arg(ap, u32);
    }

    return x_va_arg(ap, u32);
}
//DOM-IGNORE-END

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Write a string formatted output into a buffer.
// Arguments:
//	    pBuffer	            - Storage location for output. 
//      MaxChars            - Maximum number of characters to store 
//	    pFormatStr	        - String containing the formating specification. 
//      Args                - Pointer to list of arguments OR the actual arguments in case of x_sprintf
// Returns:
//	    return the number of characters written, not including the terminating null character.
// Description:
//      These functions formats and stores a series of characters and values in buffer. 
//      Each argument (if any) is converted and output according to the corresponding format 
//      specification in format. The format consists of ordinary characters and has the same 
//      form and function as the format argument for x_printf. A null character is appended after 
//      the last character written. If copying occurs between strings that overlap, the behavior 
//      is undefined.
//
//<P><B>Type Field Characters</B>
//<TABLE>
//      Character   Output format
//      =========   --------------------------------------------------------
//      %%	        a percent sign
//      %c	        a character with the given number
//      %s	        a string
//      %d	        a signed integer, in decimal
//      %u	        an unsigned integer, in decimal
//      %o	        an unsigned integer, in octal
//      %x	        an unsigned integer, in hexadecimal (lower case)
//      %e	        a floating-point number, in scientific notation
//      %f	        a floating-point number, in fixed decimal notation
//      %g	        a floating-point number, in %e or %f notation
//      %X	        like %x, but using upper-case letters
//      %E	        like %e, but using an upper-case "E"
//      %G	        like %g, but with an upper-case "E" (if applicable)
//      %p	        a pointer (outputs the pointer value's address in hexadecimal)
//      %n	        special: *stores* the number of characters output so far into the next variable in the parameter list
//</TABLE>
//    
//   <B>Size flags</B>
//<TABLE>
//     Character    Output
//     =========    ------------------------------------------------------------
//         h        interpret integer as s16 or u16
//         l        interpret integer as s32 or u32
//        q, L      interpret integer as s64 or u64
//         L        interpret floating point as higher precision adds a few extra floating point numbers
//</TABLE>
//
// Examples:
//<CODE>
//      x_printf( "%Lf %Lf %Lf", Vec.X, Vec.Y, Vec.Z );  // Note that we use the higher precision floating point representation
//      x_printf( "%Ld" (s64)123 );                      // This is a way to print an s64 bit number
//      x_printf( "%d" 123 );                            // Nothing special here printing an 32bit integer
//      x_printf( "%f" 123.0 );                          // Nothing special here printing a floating point number
//      x_printf( "%+010.4f", 123.456 );                 // The printf works like you will expect
//</CODE>
//
// See Also:
//     x_printf x_printfxy
//------------------------------------------------------------------------------
s32 x_vsprintf( char* pBuffer, s32 MaxChars, const char* pFormatStr, xva_list Args )
{
    char*           fmt;            // format string */
    char            ch;             // character from fmt */
    s32             n;              // handy integer (short term usage)
    char*           cp;             // handy char pointer (short term usage)
    s32             flags;          // flags as above
    s32             ret;            // return value accumulator
    s32             width;          // width from format (%8d), or 0
    s32             prec;           // precision from format (%.3d), or -1
    char            sign;           // sign prefix (' ', '+', '-', or \0)
    f64             _double;        // double precision arguments %[eEfgG]
    u32             ulval = 0;      // integer arguments %[diouxX]
    u64             uqval = 0;      // %q integers 
    s32             base;           // base for [diouxX] conversion
    s32             dprec;          // a copy of prec if [diouxX], 0 otherwise
    s32             realsz;         // field size expanded by dprec, sign, etc
    s32             size;           // size of converted field or string
    const char*     xdigs = NULL;   // digits for [xX] conversion
    char            buf[WORKSIZE];  // space for %c, %[diouxX], %[eEfgG]
    char            ox[2];          // space for 0x hex-prefix

    ASSERT(pFormatStr);
    ASSERT(Args );

    // Initialize variables
    fmt = (char *)pFormatStr;
    ret = 0;

    // Scan the format for conversions (`%' character).
    for (;;)
    {
        // Find the first "interesting symbol"
        for (cp = fmt; (ch = *fmt) != '\0' && ch != '%'; fmt++)
        {
            // Empty
        }

        // Write all the caracters before the "interesting symbol"
        n = (s32)(fmt - cp);
        if( n != 0 )
        {
            WriteToBuffer(&pBuffer, MaxChars, cp, n);
            ret += n;
        }

        // are we done?
        if (ch == '\0') goto done;

        // skip over '%'
        fmt++;

        // Get ready for formating the info
        flags = 0;
        dprec = 0;
        width = 0;
        prec = -1;
        sign = '\0';

rflag:
        ch = *fmt++;

reswitch:
        switch (ch)
        {
            case ' ':

                // ``If the space and + flags both appear, the space
                // flag will be ignored.''
                //  -- ANSI X3J11
                if (!sign) sign = ' ';
                goto rflag;

            case '#':
                flags |= ALT;
                goto rflag;

            case '*':
                 // ``A negative field width argument is taken as a
                 // - flag followed by a positive field width.''
                 // -- ANSI X3J11
                 // They don't exclude field widths read from args.

                if ( (width = x_va_arg(Args, s32)) >= 0 ) goto rflag;

                width = -width;

                /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////
            case '-':
                flags |= LADJUST;
                goto rflag;

            case '+':
                sign = '+';
                goto rflag;

            case '.':
                if ((ch = *fmt++) == '*')
                {
                    n = x_va_arg(Args, s32);
                    prec = n < 0 ? -1 : n;
                    goto rflag;
                }

                n = 0;

                while ( is_digit(ch) )
                {
                    n = 10 * n + to_digit(ch);
                    ch = *fmt++;
                }

                prec = n < 0 ? -1 : n;
                goto reswitch;


            case '0':
                 // ``Note that 0 is taken as a flag, not as the
                 // beginning of a field width.''
                 // -- ANSI X3J11
                flags |= ZEROPAD;
                goto rflag;

            case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
                n = 0;
                do
                {
                    n = 10 * n + to_digit(ch);
                    ch = *fmt++;
                } while (is_digit(ch));

                width = n;
                goto reswitch;

            case 'L':
                flags |= LONGDBL;

                /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////
            case 'q':
                flags |= QUADINT;
                goto rflag;

            case 'h':
                flags |= SHORTINT;
                goto rflag;

            case 'l':
                flags |= LONGINT;
                goto rflag;

            case 'c':
                *(cp = buf) = (char)x_va_arg(Args, s32);
                size = 1;
                sign = '\0';
                break;

            case 'D':
                flags |= LONGINT;
                /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////

            case 'd':
            case 'i':

                if (flags & QUADINT)
                {
                    s64 Temp = x_va_arg( Args, s64 );

                    if( Temp < 0 )
                    {
                        Temp = -Temp;
                        sign = '-';
                    }
                    
                    uqval = (u64)Temp;
                }
                else
                {
                    s32 Temp = x_va_arg(Args, s32); //GetSignARG( Args, flags );
                    if( Temp < 0 )
                    {
                        Temp = -Temp;
                        sign = '-';
                    }
                    
                    ulval = (u32)(Temp);
                }

                base = 10;
                goto number;

            case 'g':
            case 'G':
                    if (prec == 0) prec = 1;

            case 'e':
            case 'E':
            case 'f':
                /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////

                if (sign == '+') 
                {
                    WriteToBuffer(&pBuffer, MaxChars, &sign, 1);
                    width--;
                    ret++;
                }

                if (prec == -1) prec = DEFPREC;

                if (flags & LONGDBL) 
                {
                    // add additional precision when we say long double
                    prec += 4;
                    _double = (f64)x_va_arg(Args, f64); 
                }
                else
                {
                    _double = (f64)x_va_arg(Args, f64);
                }

                if (flags & LADJUST) width = -width;

                // right-adjusting zero padding
                size = dtoa(buf, _double, ch, width, prec);

                // check whether we have to pad or not
                if (flags & ZEROPAD)
                {
                    s32 i;
                    for (i=0; buf[i] == ' '; i++)
                    {
                        buf[i] = '0';
                    }
                }

                WriteToBuffer(&pBuffer, MaxChars, buf, size);

                if (flags & LADJUST) PadBuffer(&pBuffer, MaxChars, -width - size, ' ');

                // finally, adjust ret
                ret += width > size ? width : size;

                continue;

                    ///////////////////// FLOATING_POINT //////////////////////
            case 'n':

                if (flags & QUADINT) *x_va_arg(Args, s64 *) = ret;   
                else
                if (flags & LONGINT) *x_va_arg(Args, s32 *) = ret;
                else
                if (flags & SHORTINT) *x_va_arg(Args, s16 *) = (s16)ret;
                else
                    *x_va_arg(Args, s32 *) = ret;

                // no output
                continue;

            //////////////////////////////// THIS IS NOT ANSI STANDARD
            //case 'O':
            //    flags |= LONGINT;
            //    /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////

            case 'o':
                if (flags & QUADINT) uqval = (u64)x_va_arg(Args, s64); 
                else
                    ulval = x_va_arg(Args, u32); //GetUnSignARG( (xva_list*)&Args, flags );

                base = 8;
                goto nosign;

            case 'p':
                 // "The argument shall be a pointer to void.  The
                 // value of the pointer is converted to a sequence
                 // of printable characters, in an implementation-
                 // defined manner." 
                 // -- ANSI X3J11
#if defined X_TARGET_64BIT
                uqval = (u64)x_va_arg(Args, void *);
                base  = 16;
                xdigs = "0123456789abcdef";
                flags |= QUADINT;           // | HEXPREFIX; Not prefixes
                ch    = 'x';
                if (prec < 0) prec = 8;     // make sure that the precision is at 8

#else
                ulval = (u32)x_va_arg(Args, void *);
                base  = 16;
                xdigs = "0123456789abcdef";
                flags = (flags & ~QUADINT); // | HEXPREFIX; Not prefixes
                ch    = 'x';
                if (prec < 0) prec = 8;     // make sure that the precision is at 8
#endif
                goto nosign;

            case 's':

                cp = x_va_arg(Args, char*);
                ASSERT( cp != NULL );
                if (prec >= 0)
                {
                    // can't use strlen; can only look for the
                    // NULL in the first `prec' characters, and
                    // strlen() will go further.
                    char *p = (char*)x_memchr(cp, 0, (s32)prec);

                    if (p != NULL)
                    {
                        size = (s32)(p - cp);
                        if (size > prec) size = prec;
                    }
                    else
                        size = prec;
                }
                else
                    size = x_strlen(cp);

                sign = '\0';
                break;

            case 'U':
                flags |= LONGINT;
                /////////>>>>>>>>>>>>>>>>>> FALLTHROUGH <<<<<<<<<<<<<<//////////

            case 'u':
                if (flags & QUADINT) uqval = (u64)x_va_arg(Args, u64); 
                else
                    ulval = x_va_arg(Args, u32); //GetUnSignARG( (xva_list*)&Args, flags );

                base = 10;
                goto nosign;

            case 'X': xdigs = "0123456789ABCDEF"; goto hex;
            case 'x': xdigs = "0123456789abcdef"; hex:


                if (flags & QUADINT) uqval = x_va_arg(Args, u64); 
                else
                    ulval = x_va_arg(Args, u32); //GetUnSignARG( (xva_list*)&Args, flags );

                base = 16;

                // leading 0x/X only if non-zero
                if (flags & ALT && (flags & QUADINT ? uqval != 0 : ulval != 0))
                    flags |= HEXPREFIX;

                // unsigned conversions
    nosign:     sign = '\0';

                // ``... diouXx conversions ... if a precision is
                // specified, the 0 flag will be ignored.''
                //  -- ANSI X3J11

    number:     if ((dprec = prec) >= 0) flags &= ~ZEROPAD;
                
                // ``The result of converting a zero value with an
                // explicit precision of zero is no characters.''
                // -- ANSI X3J11
                cp = buf + WORKSIZE;

                if (flags & QUADINT)
                {
                    if (uqval != 0 || prec != 0)
                    {
                        cp = UQtoA(uqval, cp, base, flags & ALT, xdigs);
                    }
                }
                else
                {
                    if (ulval != 0 || prec != 0)
                    {
                        cp = ULtoA(ulval, cp, base,flags & ALT, xdigs);
                    }
                }

                size = (s32)(buf + WORKSIZE - cp);
                break;

            default:    // "%?" prints ?, unless ? is NUL
                if (ch == '\0') goto done;

                // pretend it was %c with argument ch
                cp = buf;
                *cp = ch;
                size = 1;
                sign = '\0';

                break;
            }

            // All reasonable formats wind up here.  At this point, `cp'
            // points to a string which (if not flags&LADJUST) should be
            // padded out to `width' places.  If flags&ZEROPAD, it should
            // first be prefixed by any sign or other prefix; otherwise,
            // it should be blank padded before the prefix is emitted.
            // After any left-hand padding and prefixing, emit zeroes
            // required by a decimal [diouxX] precision, then print the
            // string proper, then emit zeroes required by any leftover
            // floating precision; finally, if LADJUST, pad with blanks.


            // Compute actual size, so we know how much to pad.
            // size excludes decimal prec; realsz includes it.
            realsz = dprec > size ? dprec : size;
            if (sign) realsz++;
            else
                if (flags & HEXPREFIX) realsz += 2;

            // right-adjusting blank padding
            if ((flags & (LADJUST|ZEROPAD)) == 0) PadBuffer(&pBuffer, MaxChars, width - realsz, ' ');

            // prefix
            if (sign)
            {
                WriteToBuffer(&pBuffer, MaxChars, &sign, 1);
            }
            else
                if (flags & HEXPREFIX)
                {
                    ox[0] = '0';
                    ox[1] = ch;
                    WriteToBuffer(&pBuffer, MaxChars, ox, 2);
                }

            // right-adjusting zero padding
            if ((flags & (LADJUST|ZEROPAD)) == ZEROPAD)
                PadBuffer(&pBuffer, MaxChars, width - realsz, '0');

            // leading zeroes from decimal precision
            PadBuffer(&pBuffer, MaxChars, dprec - size, '0');

            // writte the integer number
            WriteToBuffer(&pBuffer, MaxChars, cp, size);

            // left-adjusting padding (always blank)
            if (flags & LADJUST) PadBuffer(&pBuffer, MaxChars, width - realsz, ' ');

            // finally, adjust ret
            ret += width > realsz ? width : realsz;
    }

done:
    WriteToBuffer(&pBuffer, MaxChars, "\0", 1);

    return (ret);
}

//------------------------------------------------------------------------------
// <COMBINE x_vsprintf >
s32 x_sprintf( char* pBuffer, s32 MaxChars, const char* pFormatStr, ... )
{
    xva_list   Args;
    x_va_start( Args, pFormatStr );
    s32 StrLen = x_vsprintf( pBuffer, MaxChars, pFormatStr, Args );
    return StrLen ;
}
