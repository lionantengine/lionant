
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This is the NULL constructor of the color structure. It doesn't do anything.
//      packed version of the color structure.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( void )
{
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This constructor builds by passing a single U32 which represents a 
//      packed version of the color structure.
// Arguments:
//      K       - This is the color packed into a 32 bit variable. Note 
//                 that this may have issues in different endian machines. The Rule is that
//                 the color is packed little endian.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( const u32 K )
{ 
    *this = K; 
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This constuctor builds the color by passing each component in a u8 forms.
// Arguments:
//      r   - is the Red component of the color.
//      g   - is the Green component of the color.
//      b   - is the Blue component of the color.
//      a   - is the Alpha component of the color.
//------------------------------------------------------------------------------
inline
xcolor::xcolor( u8 r, u8 g, u8 b, u8 a ) : 
    m_R(r), 
    m_G(g), 
    m_B(b),
    m_A(a) {}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This constuctor builds the color by passing a vector3 which represents the
//      floating point values of RGB.
// Arguments:
//      C   - C represents a vector which contains values ranges from 0 to 1, 
//             C.X: is the Red component of the color.
//             C.Y: is the Green component of the color.
//             C.Z: is the Blue component of the color.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( const xvector3d& C )
{
    SetFromRGBA( C.m_X, C.m_Y, C.m_Z, 1 );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This constuctor builds the color by passing a vector4 which represents the
//      floating point values of RGBA.
// <param name="C"> 
//      C   - C represents a vector which contains values ranges from 0 to 1, 
//             C.X: is the Red component of the color.
//             C.Y: is the Green component of the color.
//             C.Z: is the Blue component of the color.
//             C.W: is the Alpha component of the color.
//------------------------------------------------------------------------------
inline 
xcolor::xcolor( const xvector4& C )
{
    SetFromRGBA( C.m_X, C.m_Y, C.m_Z, C.m_W );
}

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//      This function sets the values directly by passing the u8 components 
//      of the color.
// Arguments:
//      r   - is the Red component of the color.
//      g   - is the Green component of the color.
//      b   - is the Blue component of the color.
//      a   - is the Alpha component of the color.
//------------------------------------------------------------------------------
inline 
void xcolor::Set( u8 r, u8 g, u8 b, u8 a )
{
    m_A = a;
    m_R = r;
    m_G = g;
    m_B = b;
}

//------------------------------------------------------------------------------
inline 
void xcolor::Set( const u32 K )
{
    *this = K; 
}

//------------------------------------------------------------------------------
inline 
xbool xcolor::operator == ( xcolor C ) const
{
    return m_Color == C.m_Color;
}

//------------------------------------------------------------------------------
inline 
xbool xcolor::operator != ( xcolor C ) const
{
    return m_Color != C.m_Color;
}

//------------------------------------------------------------------------------
inline 
u8& xcolor::operator[]( int Index )
{
    ASSERT( Index >=  0 );
    ASSERT( Index <=  3 );
    return ((u8*)this)[ Index ];
}

//------------------------------------------------------------------------------
inline 
xcolor::operator u32()
{
    return *((u32*)this);
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator = ( u32 C )
{
    m_A = (u8)((C>>24)&0xff);
    m_R = (u8)((C>>16)&0xff);
    m_G = (u8)((C>>8 )&0xff);
    m_B = (u8)((C>>0 )&0xff);

    return *this;
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator += ( xcolor C )
{
    m_A = x_Min( 255, s32(m_A) + C.m_A );
    m_R = x_Min( 255, s32(m_R) + C.m_R );
    m_G = x_Min( 255, s32(m_G) + C.m_G );
    m_B = x_Min( 255, s32(m_B) + C.m_B );

    return *this;
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator -= ( xcolor C )
{
    m_A = x_Max( 0, s32(m_A) - C.m_A );
    m_R = x_Max( 0, s32(m_R) - C.m_R );
    m_G = x_Max( 0, s32(m_G) - C.m_G );
    m_B = x_Max( 0, s32(m_B) - C.m_B );

    return *this;
}

//------------------------------------------------------------------------------
inline 
const xcolor& xcolor::operator *= ( xcolor C )
{
    m_A = x_Max( 0, x_Min( 255, s32(m_A) * C.m_A ) );
    m_R = x_Max( 0, x_Min( 255, s32(m_R) * C.m_R ) );
    m_G = x_Max( 0, x_Min( 255, s32(m_G) * C.m_G ) );
    m_B = x_Max( 0, x_Min( 255, s32(m_B) * C.m_B ) );

    return *this;
}

//------------------------------------------------------------------------------
//"The YIQ system is the colour primary system adopted by NTSC for colour
//television  broadcasting. The YIQ color solid is formed by a linear
//transformation of the RGB cude. Its purpose is to exploit certain
//characteristics of the human visual system to maximize the use of a fixed
//bandwidth" (Funds... op cit).
//------------------------------------------------------------------------------
inline 
void xcolor::BuildYIQ( f32& Y, f32& I, f32& Q ) const
{
    f32 r,g,b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    Y = r*0.299f + g*0.587f + b*0.114f;
    I = r*0.596f - g*0.274f - b*0.322f;
    Q = r*0.212f - g*0.523f + b*0.311f;
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromYIQ( f32 Y, f32 I, f32 Q )
{
    f32 r,g,b;

    r = Y*1 + I*0.956f + Q*0.621f;
    g = Y*1 - I*0.272f - Q*0.647f;
    b = Y*1 - I*1.105f + Q*1.702f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}

//------------------------------------------------------------------------------
// YUV is like YIQ, except that it is the PAL/European standard. It's only trivial
// to many poeple in Northen America (i.e. not only the STATES), but since the
// USENET messages are read all over the world...
//------------------------------------------------------------------------------
inline 
void xcolor::BuildYUV( f32& Y, f32& U, f32& V ) const
{
    f32 r,g,b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    Y =  r*0.299f + g*0.587f + b*0.114f;
    U = -r*0.147f - g*0.289f + b*0.437f;
    V =  r*0.615f - g*0.515f - b*0.100f;
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromYUV( f32 Y, f32 U, f32 V )
{
    f32 r,g,b;

    r = Y*1 + U*0.000f + V*1.140f;
    g = Y*1 - U*0.394f - V*0.581f;
    b = Y*1 + U*2.028f + V*0.000f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}

//------------------------------------------------------------------------------
inline 
void xcolor::BuildCIE( f32& C, f32& I, f32& E ) const
{
    f32 r, g, b;

    r = m_R*(1/255.0f);
    g = m_G*(1/255.0f);
    b = m_B*(1/255.0f);

    C = r*0.6067f + g*0.1736f + b*0.2001f;
    I = r*0.2988f + g*0.5868f + b*0.1143f;
    E = r*0.0000f + g*0.0661f + b*1.1149f;
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromCIE( f32 C, f32 I, f32 E )
{
    f32 r, g, b;

    r =  C*1.9107f - I*0.5326f - E*0.2883f;
    g = -C*0.9843f + I*1.9984f - E*0.0283f;
    b =  C*0.0583f - I*0.1185f + E*0.8986f;

    r *= 255.0f;
    g *= 255.0f;
    b *= 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}

//------------------------------------------------------------------------------
// color to a parametric rgb values
//------------------------------------------------------------------------------
inline 
void xcolor::BuildRGB( f32& aR, f32& aG, f32& aB  ) const
{
    aR = m_R*(1/255.0f);   
    aG = m_G*(1/255.0f);   
    aB = m_B*(1/255.0f);   
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromRGB( f32 aR, f32 aG, f32 aB )
{
    f32 r,g,b;

    r = aR * 255.0f;
    g = aG * 255.0f;
    b = aB * 255.0f;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}

//------------------------------------------------------------------------------
inline 
void xcolor::BuildRGBA( f32& aR, f32& aG, f32& aB, f32& aA  ) const
{
    aR = m_R*(1/255.0f);   
    aG = m_G*(1/255.0f);   
    aB = m_B*(1/255.0f);   
    aA = m_A*(1/255.0f);   
}

//------------------------------------------------------------------------------
inline 
xvector4 xcolor::BuildRGBA( void ) const
{
    return xvector4( m_R*(1/255.0f), m_G*(1/255.0f), m_B*(1/255.0f), m_A*(1/255.0f) );   
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::BuildRGB( void ) const
{
    return xvector3( m_R*(1/255.0f), m_G*(1/255.0f), m_B*(1/255.0f) );
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromRGB( xvector3d& Vector )
{
    SetFromRGB( Vector.m_X, Vector.m_Y, Vector.m_Z );
}


//------------------------------------------------------------------------------
inline 
void xcolor::SetFromRGBA( f32 aR, f32 aG, f32 aB, f32 aA )
{
    f32 r,g,b,a;

    r = aR * 255.0f;
    g = aG * 255.0f;
    b = aB * 255.0f;
    a = aA * 255.0f;    

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = (u8)x_Min( 255.0f, a );
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromRGBA( const xvector4& C )
{
    SetFromRGBA( C.m_X, C.m_Y, C.m_Z, C.m_W );
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromNormal( const xvector3d& Normal )
{
    m_R = (u8)x_Min( 255.0f, ( ( Normal.m_X + 1.0f ) * 127.0f ) + 0.5f );
    m_G = (u8)x_Min( 255.0f, ( ( Normal.m_Y + 1.0f ) * 127.0f ) + 0.5f );
    m_B = (u8)x_Min( 255.0f, ( ( Normal.m_Z + 1.0f ) * 127.0f ) + 0.5f );
    m_A = 255;
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::BuildNormal( void ) const
{
    return xvector3(   (((f32)m_R)-127.0f ) * (1/127.0f),
                       (((f32)m_G)-127.0f ) * (1/127.0f),
                       (((f32)m_B)-127.0f ) * (1/127.0f)     );

}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromLight( const xvector3d& LightDir )
{
    SetFromNormal( -LightDir );
}

//------------------------------------------------------------------------------
inline 
xvector3 xcolor::BuildLight( void ) const
{
    return -BuildNormal();
}

//------------------------------------------------------------------------------
// color to Cyan, Magenta and Yellow
//------------------------------------------------------------------------------
inline 
void xcolor::BuildCMY( f32& C, f32& M, f32& Y ) const
{
    C = 1 - m_R*(1/255.0f);   // ASSUME C
    M = 1 - m_G*(1/255.0f);   // ASSUME M
    Y = 1 - m_B*(1/255.0f);   // ASSUME Y
}

//------------------------------------------------------------------------------
inline 
void xcolor::SetFromCMY( f32 C, f32 M, f32 Y )
{
    f32 r, g, b;

    r = (1-C)*255;
    g = (1-M)*255;
    b = (1-Y)*255;

    m_R = (u8)x_Min( 255.0f, r );
    m_G = (u8)x_Min( 255.0f, g );
    m_B = (u8)x_Min( 255.0f, b );
    m_A = 255;
}

//------------------------------------------------------------------------------
inline 
xcolor::format xcolor::FindClosestFormat( u32 FormatMask, format Match )
{    
    for( const xcolor::format* pMatch = &g_Match[ Match ].m_Format[0]; *pMatch != FMT_END; pMatch++ )
    {
        if( FormatMask & (1<<*pMatch) ) return *pMatch;
    }

    return FMT_NULL;
}

//------------------------------------------------------------------------------
inline 
void xcolor::BuildColorFromData( u32 Data, format DataFormat )
{
    const fmt_desc& Fmt = g_FormatDesc[ DataFormat ];
    ASSERT( Fmt.m_Format == DataFormat );
    
    m_R = (u8)((Fmt.m_RShift<0) ? ((Data & Fmt.m_RMask) << (-Fmt.m_RShift)) : ((Data & Fmt.m_RMask) >> (Fmt.m_RShift)));
    m_G = (u8)((Fmt.m_GShift<0) ? ((Data & Fmt.m_GMask) << (-Fmt.m_GShift)) : ((Data & Fmt.m_GMask) >> (Fmt.m_GShift)));
    m_B = (u8)((Fmt.m_BShift<0) ? ((Data & Fmt.m_BMask) << (-Fmt.m_BShift)) : ((Data & Fmt.m_BMask) >> (Fmt.m_BShift)));

    // force m_A to 255 if the src format doesn't have alpha
    if( Fmt.m_AMask == 0 ) m_A = 255;
    else                   m_A = (u8)((Fmt.m_AShift<0) ? ((Data & Fmt.m_AMask) << (-Fmt.m_AShift)) : ((Data & Fmt.m_AMask) >> (Fmt.m_AShift)));
} 

//------------------------------------------------------------------------------
inline 
u32 xcolor::BuildDataFromColor( format DataFormat ) const
{
    u32             Data;
    const fmt_desc& Fmt = g_FormatDesc[ DataFormat ];
    ASSERT( Fmt.m_Format == DataFormat );
    
    Data  = ~( Fmt.m_AMask | Fmt.m_RMask | Fmt.m_GMask | Fmt.m_BMask );
    Data |= (Fmt.m_AShift<0) ? ((((u32)m_A) >> (-Fmt.m_AShift)) & Fmt.m_AMask ) : ((((u32)m_A) << Fmt.m_AShift) & Fmt.m_AMask );
    Data |= (Fmt.m_RShift<0) ? ((((u32)m_R) >> (-Fmt.m_RShift)) & Fmt.m_RMask ) : ((((u32)m_R) << Fmt.m_RShift) & Fmt.m_RMask );
    Data |= (Fmt.m_GShift<0) ? ((((u32)m_G) >> (-Fmt.m_GShift)) & Fmt.m_GMask ) : ((((u32)m_G) << Fmt.m_GShift) & Fmt.m_GMask );
    Data |= (Fmt.m_BShift<0) ? ((((u32)m_B) >> (-Fmt.m_BShift)) & Fmt.m_BMask ) : ((((u32)m_B) << Fmt.m_BShift) & Fmt.m_BMask );
        
    return Data;
}

//------------------------------------------------------------------------------
inline 
xcolor::format xcolor::FindFormat( u32 AMask, u32 RMask, u32 GMask, u32 BMask  )
{
    for( int i=FMT_NULL; i<FMT_END; i++ )
    {
        if( AMask != g_FormatDesc[i].m_AMask ) continue;
        if( RMask != g_FormatDesc[i].m_RMask ) continue;
        if( GMask != g_FormatDesc[i].m_GMask ) continue;
        if( BMask != g_FormatDesc[i].m_BMask ) continue;

        return g_FormatDesc[i].m_Format;
    }

    return FMT_NULL;
}


//------------------------------------------------------------------------------
inline
xcolor& xcolor::BlendColors( const xcolor& Src1, const xcolor& Src2, f32 t )
{
    const xvector4 S1 = Src1.BuildRGBA();
    xvector4 newColor = S1 + t * ( Src2.BuildRGBA() - S1 );
    SetFromRGBA( newColor );
    return *this;
}


//------------------------------------------------------------------------------
inline
xcolor xcolor::ComputeNewAlpha( f32 Alpha ) const
{
    xcolor Ret( *this );
    
    Ret.m_A = x_Max( 0,x_Min(255, s32(Alpha*m_A)));
    
    return Ret;
}
