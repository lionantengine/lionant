#ifdef TARGET_ANDROID
//==============================================================================
// INCLUDES
//==============================================================================
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "../../x_target.h"
#include "x_android_file_device.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

char android_device::sDefaultPath[256] = {0};

//------------------------------------------------------------------------------
void android_device::sInitialize(void)
{
    x_strcpy(sDefaultPath, 256, "/sdcard/");
}

//------------------------------------------------------------------------------
void* android_device::Open( const char* pFileName, u32 Flags )
{
    char    finalName[256] = {0};
    s32     flag = O_RDWR;
    mode_t  mode = 0;
    
	ASSERT( pFileName != NULL );
    
    if ( '/' != pFileName[0] )
    {
        x_strcpy(finalName, 256, sDefaultPath);
        x_strcat(finalName, 256, pFileName);
    }
    else
    {
        x_strcpy(finalName, 256, pFileName);
    }
    
	flag    = O_RDWR;
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
    {
        flag |= (O_CREAT | O_TRUNC);
        mode = S_IRWXU;
    }
    else
    {
        if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
        {
		    flag   = O_RDONLY;
        }
    }
    
    if( x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ) )
    {
        x_printf("filename[%s], is in ACC_ASYNC", finalName);
        flag |= O_NONBLOCK;
    }
    
    s32 fileDescriptor = open(finalName, flag, mode);
    x_printf("filename[%s], fileDescriptor[%d]", finalName, fileDescriptor);
    if ( -1 == fileDescriptor )
    {
        return NULL;
    }
    
    //
    // Okay we are in business
    //
    xhandle hFile;
    file&   File = m_lFiles.append( hFile );
    xptr_lock    Ref( m_lFiles );
    
    x_memset(&File.mIOControlBlock, 0, sizeof(File.mIOControlBlock));
    /* Allocate a data buffer for the aiocb request */

    /* Initialize the necessary fields in the aiocb */
    File.mIOControlBlock.aio_fildes = fileDescriptor;
    File.mIOControlBlock.aio_offset = 0;
    File.mOffset = 0;
    File.mFlag  = Flags;
    
    /* Link the AIO request with a thread callback */
    File.mIOControlBlock.aio_sigevent.sigev_notify = SIGEV_THREAD;
    File.mIOControlBlock.aio_sigevent.sigev_notify_function = aio_completion_handler;
    File.mIOControlBlock.aio_sigevent.sigev_notify_attributes = NULL;
    File.mIOControlBlock.aio_sigevent.sigev_value.sival_ptr = &File.mIOControlBlock;


    return (void*)(u64)(hFile.m_Handle+1);
}

//------------------------------------------------------------------------------
void android_device::AsyncAbort( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    VERIFY( aio_cancel( File.mIOControlBlock.aio_fildes, &File.mIOControlBlock ) );
}

//------------------------------------------------------------------------------

xfile::sync_state android_device::Synchronize( void* pFile, xbool bBlock )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    while(true)
    {
        s32 errorCode = aio_error(&File.mIOControlBlock);
        //s32 errorCode = errno;
        if ( EINPROGRESS == errorCode )
        {
            if ( !bBlock )
            {
                return xfile::SYNC_INCOMPLETE;
            }
            else
            {
                x_Yield();
            }
        }
        else
        {
            if( 0 == errorCode )
            {
                aio_return(&File.mIOControlBlock);
                return xfile::SYNC_COMPLETED;
            }
            else
            {
                break;
            }
        }
    }
    
	// The result is FALSE and the error isn't ERROR_IO_INCOMPLETE, there's a real error!
	return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void android_device::Close( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    
    //
    // Close the handle
    //
    {
        xptr_lock    Ref( m_lFiles );
        file&   File = m_lFiles( hFile );
        x_printf("close!!!!!!!!, [%d]", File.mIOControlBlock.aio_fildes);
        if( !close( File.mIOControlBlock.aio_fildes ) )
        {
            ASSERT( 0 );
        }
    }
    
    //
    // Lets free our entry
    //
    m_lFiles.DeleteByHandle( hFile );
}

//------------------------------------------------------------------------------

xbool android_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 errorCode = 0;
    
    // Set the file pointer (We assume we didnt make any errors)
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mIOControlBlock.aio_buf = pBuffer;
        File.mIOControlBlock.aio_nbytes = Count;
        errorCode = aio_read(&File.mIOControlBlock);
        //x_printf("errorCode is [%d] [%d][%s][%d]", errorCode, File.mIOControlBlock.aio_fildes, File.mIOControlBlock.aio_buf, File.mIOControlBlock.aio_nbytes);
        //errorCode = read(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    else
    {
        errorCode = read(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    
    if (-1 == errorCode) 
    {
        return FALSE;
    }
    
    // Not problems
    return TRUE;
}

//------------------------------------------------------------------------------

void android_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 errorCode = 0;
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mIOControlBlock.aio_buf = (volatile void*)pBuffer;
        File.mIOControlBlock.aio_nbytes = Count;
        errorCode = aio_write(&File.mIOControlBlock);
        //errorCode = write(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    else
    {
        errorCode = write(File.mIOControlBlock.aio_fildes, pBuffer, Count);
    }
    
    if ( -1 == errorCode) 
    {
        ASSERT(0);
    }
}

//------------------------------------------------------------------------------

void android_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s32 HardwareMode;
    switch( aMode )
    {
        case SKM_ORIGIN: HardwareMode = SEEK_SET; break;
        case SKM_CURENT: HardwareMode = SEEK_CUR; break;
        case SKM_END:    HardwareMode = SEEK_END; break; 
        default: ASSERT(0);               break;
    }
    
    // We will make sure we are sync here
    // WARNING: Potencial time wasted here
    if( File.mFlag & xfile_device_i::ACC_ASYNC )
    {
        VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
    }
    
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        File.mOffset = (s32)lseek(File.mIOControlBlock.aio_fildes, Pos, HardwareMode);
    }
    else
    {
        lseek(File.mIOControlBlock.aio_fildes, Pos, HardwareMode);
    }
}

//------------------------------------------------------------------------------

s32 android_device::Tell( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    return (s32)lseek(File.mIOControlBlock.aio_fildes, 0, SEEK_CUR);
}

//------------------------------------------------------------------------------

void android_device::Flush( void* pFile )
{
    // We will make sure we are sync here. 
    // I dont know what else to do there is not a way to flush anything the in the API
    // WARNING: Potencial time wasted here
    VERIFY( xfile::SYNC_COMPLETED == Synchronize( pFile, TRUE ) );
}

//------------------------------------------------------------------------------

s32 android_device::Length( void* pFile )
{
    s32 Length;
    s32 Cursor;
    
    Cursor = Tell( pFile ); Seek( pFile, SKM_END,    0 );
    Length = Tell( pFile ); Seek( pFile, SKM_ORIGIN, Cursor );
    
    return Length;
}

//------------------------------------------------------------------------------

xbool android_device::IsEOF( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    if( x_FlagIsOn( File.mFlag, xfile_device_i::ACC_ASYNC ) )
    {
        s32 errorCode = aio_error(&File.mIOControlBlock);
        //s32 errorCode = errno;
        if ( EINPROGRESS == errorCode )
        {
            return TRUE;
        }
        else
        {
            if( 0 != errorCode )
            {
                return TRUE;
            }
        }
    }
    else
    {
        s32 Length;
        s32 Cursor;
        
        Cursor = Tell( pFile ); 
        Seek( pFile, SKM_END,    0 );
        Length = Tell( pFile ); 
        Seek( pFile, SKM_ORIGIN, Cursor );
//        
        if ( Cursor >= Length )
        {
            return TRUE;
        }
    }
    return FALSE;
}

void aio_completion_handler( sigval_t sigval )
{
    struct aiocb *req;
    req = (struct aiocb *)sigval.sival_ptr;

    /* Did the request complete? */
    if ( aio_error( req ) == 0)
    {
        LOGD("aio_completion_handler Request completed successfully!");
        /* Request completed successfully, get the return status */
        int a = aio_return( req );
        char tmp[512] = {0};
        sprintf(tmp, "successfully[%d]", a);
        LOGD(tmp);
    }
    else
        LOGD("aio_completion_handler Request incompleted!");
    return;
}

#endif