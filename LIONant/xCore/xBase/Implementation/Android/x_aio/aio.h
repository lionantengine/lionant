/*
 *  Modified from glibc by Eric
 */

/*
 * ISO/IEC 9945-1:1996 6.7: Asynchronous Input and Output
 */
#ifdef TARGET_ANDROID

#ifndef _AIO_H
#define _AIO_H	1

#include <features.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include "../../../x_base.h"

/* Asynchronous I/O control block.  */
struct aiocb
{
    int aio_fildes;		            /* File desriptor.  */
    int aio_lio_opcode;		        /* Operation to be performed.  */
    int aio_reqprio;		        /* Request priority offset.  */
    volatile void *aio_buf;	        /* Location of buffer.  */
    size_t aio_nbytes;		        /* Length of transfer.  */
    struct sigevent aio_sigevent;	/* Signal number and value.  */

    /* Internal members.  */
    struct aiocb *__next_prio;
    int __abs_prio;
    int __policy;
    int __error_code;
    ssize_t __return_value;

    off_t aio_offset;		        /* File offset.  */
    char __pad[sizeof (off64_t) - sizeof (off_t)];
    char unused[32];
};

/* To customize the implementation one can use the following struct.
   This implementation follows the one in Irix.  */
struct aioinit
{
    int aio_threads;		/* Maximal number of threads.  */
    int aio_num;		    /* Number of expected simultanious requests. */
    int aio_locks;		    /* Not used.  */
    int aio_usedba;		    /* Not used.  */
    int aio_debug;		    /* Not used.  */
    int aio_numusers;		/* Not used.  */
    int aio_idle_time;		/* Number of seconds before idle thread terminates.  */
    int aio_reserved;
};


/* Return values of cancelation function.  */
enum
{
    AIO_CANCELED,
    #define AIO_CANCELED AIO_CANCELED
    AIO_NOTCANCELED,
    #define AIO_NOTCANCELED AIO_NOTCANCELED
    AIO_ALLDONE
    #define AIO_ALLDONE AIO_ALLDONE
};


/* Operation codes for `aio_lio_opcode'.  */
enum
{
    LIO_READ,
    #define LIO_READ LIO_READ
    LIO_WRITE,
    #define LIO_WRITE LIO_WRITE
    LIO_NOP
    #define LIO_NOP LIO_NOP
};


/* Synchronization options for `lio_listio' function.  */
enum
{
    LIO_WAIT,
    #define LIO_WAIT LIO_WAIT
    LIO_NOWAIT
    #define LIO_NOWAIT LIO_NOWAIT
};


/* Allow user to specify optimization.  */
extern void aio_init (__const struct aioinit *__init);
/* Enqueue read request for given number of bytes and the given priority.  */
extern int aio_read (struct aiocb *__aiocbp);
/* Enqueue write request for given number of bytes and the given priority.  */
extern int aio_write (struct aiocb *__aiocbp);

/* Initiate list of I/O requests.  */
//extern int lio_listio (int __mode,
//		       struct aiocb *__const __list[__restrict_arr],
//		       int __nent, struct sigevent *__restrict __sig);

/* Retrieve error status associated with AIOCBP.  */
extern int aio_error (__const struct aiocb *__aiocbp);
/* Return status associated with AIOCBP.  */
extern ssize_t aio_return (struct aiocb *__aiocbp);

/* Try to cancel asynchronous I/O requests outstanding against file
   descriptor FILDES.  */
extern int aio_cancel (int __fildes, struct aiocb *__aiocbp);

#endif /* aio.h */

#endif
