#ifndef	_X_LOCKLESS_CONTAINER_ANDROID_INLINE_
#define	_X_LOCKLESS_CONTAINER_ANDROID_INLINE_

#include "../../x_target.h"

inline xbool x_cas32( u32*  pPointer,
                     u32   Old,
                     u32   New)
{
    ASSERT( pPointer );
    return xbool(__atomic_cmpxchg( (int)Old, (int)New, (volatile int*)pPointer));
}

//-------------------------------------------------------------------------------

inline s32 x_casInc32( s32* pPointer )
{
    ASSERT( pPointer );
    int32_t oldValue;
    do {
        oldValue = *pPointer;
    } while (__atomic_cmpxchg(oldValue, oldValue+1, pPointer));
    return oldValue;
}

//-------------------------------------------------------------------------------

inline s32 x_casDec32( s32* pPointer )
{
    ASSERT( pPointer );
    int32_t oldValue;
    do {
        oldValue = *pPointer;
    } while (__atomic_cmpxchg(oldValue, oldValue-1, pPointer));
    return oldValue;
}

//-------------------------------------------------------------------------------

inline s32 android_quasiatomic_cmpxchg_64(int64_t oldvalue, int64_t newvalue,
    volatile int64_t* addr) {
        int result;
        if (*addr == oldvalue) {
            *addr  = newvalue;
            result = 1;
        } else {
            result = 0;
        }
        return result;
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( u64*  pPointer, 
                     u32   OldPtr, 
                     u32   OldGuard, 
                     u32   NewPtr, 
                     u32   NewGuard)
{
    ASSERT( pPointer );
    u64 Comparand = (u64)(OldGuard) << 32 | OldPtr;
    u64 Exchange  = (u64)(NewGuard) << 32 | NewPtr;
    
    if( Comparand != *((u64*)pPointer) ) 
    {
        return FALSE;
    }

    return xbool(android_quasiatomic_cmpxchg_64( (int64_t)Comparand, (int64_t)Exchange, (volatile int64_t*)pPointer ));
}

inline xbool x_cas64( u64*            pPointer, 
                     const void*     pOldPtr, 
                     u32             OldGuard, 
                     const void*     pNewPtr, 
                     u32             NewGuard )
{
    return x_cas64( pPointer, (u32)pOldPtr, OldGuard, (u32)pNewPtr, NewGuard );
}

//-------------------------------------------------------------------------------
//				QT FLAGS
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOn( u32 Flags, x_qt_flags& LocalReality )
{
    u64 Reality     = LocalReality.m_qtFlags;
    u64 Counter     = (Reality + 0x100000000ULL)&0xffffffff00000000ULL;
    u64 NewFlags    = (Reality | Flags)&0xffffffff;
        
    NewFlags        = Counter|NewFlags;

    return xbool(android_quasiatomic_cmpxchg_64( (int64_t)Reality, (int64_t)NewFlags, (volatile int64_t*)&m_qtFlags ));
    //return xbool(_InterlockedCompareExchange64( (volatile __int64 *)&m_qtFlags, NewFlags, Reality ) == Reality);
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOff( u32 Flags, x_qt_flags& LocalReality )
{
    Flags = ~Flags;
    u64 Reality     = LocalReality.m_qtFlags;
    u64 Counter     = (Reality + 0x100000000ULL)&0xffffffff00000000ULL;
    u64 NewFlags    = Reality & u64(Flags);
        
    NewFlags        = Counter|NewFlags;

    return xbool(android_quasiatomic_cmpxchg_64( (int64_t)Reality, (int64_t)NewFlags, (volatile int64_t*)&m_qtFlags ));
}
#endif  ///end _X_LOCKLESS_CONTAINER_ANDROID_INLINE_