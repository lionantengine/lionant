//==============================================================================
// PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! 
//==============================================================================
//DOM-IGNORE-BEGIN
//------------------------------------------------------------------------------
// USER Does not need to worry about xstring_xchar. This is just an implementation detail.
//------------------------------------------------------------------------------
class xstring_xchar  { xstring_xchar ( void ) = default; const char  m_Data[256]; };
class xwstring_xchar { xwstring_xchar( void ) = default; const wchar m_Data[256]; };

//------------------------------------------------------------------------------
// DONT USE THIS CLASS DIRECTLY USE THE TYPEDEFS
//------------------------------------------------------------------------------
template< s32 Pow >
class xstring_fixed_size
{
public:
                    xstring_fixed_size      ( void )       { Buffer[0] = (Pow-2)|(char)(xstring::FLAGS_NON_SYSTEM); Buffer[1] = 0; }
    xstring         operator ->             ( void ) const { return ((xstring_xchar*)&Buffer[1]); }
                    operator const char*    ( void ) const { return &Buffer[1]; }
                    operator char*          ( void )       { return &Buffer[1]; }
    const char*     operator ()             ( void ) const { return &Buffer[1]; }

protected:    

    char   Buffer[1<<Pow];
};

//------------------------------------------------------------------------------

inline xstring xstring::BuildFromFormat( const char* pFormat, ... )

{
    xstring Output;
    xva_list  Args;
    x_va_start( Args, pFormat );
    Output.AppendFormatV( pFormat, Args );
    x_va_end(Args);
    return Output;
}

//------------------------------------------------------------------------------

inline 
xbool xstring::operator == ( const char* pStr ) const
{ 
    return x_strcmp( pStr, (*this) )==0; 
}

//------------------------------------------------------------------------------

inline 
xbool operator == ( const char* pStr, const xstring& Str )
{
    return Str == pStr;
}

//------------------------------------------------------------------------------
// Initialize the module
//------------------------------------------------------------------------------
void x_StringInstanceInit( void );
void x_StringInstanceKill( void );
void x_StringInit        ( void );
void x_StringKill        ( void );
//==============================================================================
// END
//==============================================================================
//DOM-IGNORE-END
