#include "../x_base.h"


//////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////

xbitmap::~xbitmap( void )
{
    if( m_pData && x_FlagIsOn( m_Flags, FLAGS_OWN_MEMORY ) )
        x_free( m_pData );
}

//-------------------------------------------------------------------------------

xbitmap::xbitmap( void )
{
    m_pData     = NULL;             // +4 pointer to the data
    m_DataSize  = 0;                // +4 total data size in bytes
    m_FrameSize = 0;                // +4 Size of one frame of data.(one bitmap)
    m_Height    = 0;                // +4 height in pixels
    m_Width     = 0;                // +4 width in pixels
    m_Flags     = 0;                // +1 miscellaneous flags
    m_nMips     = 0;                // +1 Number of mips
    m_Format    = FORMAT_NULL;      // +1 type of bitmap format.
    m_nFrames   = 0;                // +1 Number of bitmaps in here (example cube maps == 6)
}

//-------------------------------------------------------------------------------

void xbitmap::Kill( void )
{
    // Do we have anything to do?
    if( m_pData == NULL )
        return;
    
    if( m_pData && x_FlagIsOn( m_Flags, FLAGS_OWN_MEMORY ) )
        x_free( m_pData );

    m_pData     = NULL;             // +4 pointer to the data
    m_DataSize  = 0;                // +4 total data size in bytes
    m_FrameSize = 0;                // +4 Size of one frame of data.(one bitmap)
    m_Height    = 0;                // +4 height in pixels
    m_Width     = 0;                // +4 width in pixels
    m_Flags     = 0;                // +1 miscellaneous flags
    m_nMips     = 0;                // +1 Number of mips
    m_Format    = FORMAT_NULL;      // +1 type of bitmap format.
    m_nFrames   = 0;                // +1 Number of bitmaps in here (example cube maps == 6)
}

//-------------------------------------------------------------------------------

xbool xbitmap::Load( const char* pFileName )
{
    Kill();
  
    xfile File;
    
    if( File.Open( pFileName, "rb" ) == FALSE )
        return FALSE;
    
    //
    // Read the signature
    //
    {
        u32 Signature;
        File.Read( Signature );
        
        if( Signature != u32('XBMP') )
            return FALSE;
    }
    
    File.Read( (u8&)m_Format );
    File.Read( m_DataSize );
    File.Read( m_FrameSize );
    File.Read( m_Height );
    File.Read( m_Width );
    File.Read( m_Flags );
    File.Read( m_nMips );
    File.Read( m_nFrames );
    
    //
    // Read the big data
    //
    m_pData = (mip*)x_malloc( sizeof(xbyte), m_DataSize, 0 );
    File.ReadRaw( m_pData, m_DataSize, 1 );
    
    return TRUE;
}

//-------------------------------------------------------------------------------

xbool xbitmap::Save( const char* pFileName ) const
{
    xfile File;
    
    if( File.Open( pFileName, "wb" ) == FALSE )
        return FALSE;
    
    File.Write( u32('XBMP') );
    File.Write( (u8&)m_Format );
    File.Write( m_DataSize );
    File.Write( m_FrameSize );
    File.Write( m_Height );
    File.Write( m_Width );
    File.Write( m_Flags );
    File.Write( m_nMips );
    File.Write( m_nFrames );
    File.WriteRaw( m_pData, m_DataSize, 1 );
    
    return TRUE;
}

//-------------------------------------------------------------------------------

void xbitmap::SerializeIO( xserialfile& SerialFile ) const
{
    SerialFile.Serialize( m_RawData, m_DataSize );
    SerialFile.Serialize( m_DataSize );
    SerialFile.Serialize( m_FrameSize );
    SerialFile.Serialize( m_Height );
    SerialFile.Serialize( m_Width );
    SerialFile.Serialize( m_Flags );
    SerialFile.Serialize( m_nMips );
    SerialFile.SerializeEnum( m_Format );
    SerialFile.Serialize( m_nFrames );
}

//-------------------------------------------------------------------------------

void xbitmap::setup(
    s32             Width                   ,
    s32             Height                  ,
    xbitmap::format BitmapFormat            ,
    s32             DataSize                ,
    s32             FrameSize               ,
    void*           pData                   ,
    xbool           bFreeMemoryOnDestruction,
    s32             nMips                   ,
    s32             nFrames               )
{
    ASSERT( pData );
    ASSERT( FrameSize   > 0 );
    ASSERT( FrameSize   < DataSize );   // In fact this should be equal to: DataSize - ((nMips*sizeof(s32)) * nFrames) which means it should be removed
    ASSERT( DataSize    > 4 );
    ASSERT( nMips       > 0 );
    ASSERT( nFrames     > 0 );
    ASSERT( Width       > 0 );
    ASSERT( Height      > 0 );
    ASSERT( BitmapFormat > FORMAT_NULL );
    ASSERT( BitmapFormat < FORMAT_TOTAL );

    Kill();

    m_pData         = (xbitmap::mip *)pData;
    m_DataSize      = DataSize;
    
    ASSERT( FrameSize == DataSize - ((nMips*sizeof(s32)) * nFrames) );
    
    m_FrameSize     = FrameSize;
    m_Height        = Height;   
    m_Width         = Width;
    
    m_Flags        |= bFreeMemoryOnDestruction?FLAGS_OWN_MEMORY:0;
    
    m_nMips         = nMips;    
    m_Format        = BitmapFormat;   
    m_nFrames       = nFrames;  
}

//-------------------------------------------------------------------------------

void xbitmap::setupFromColor( s32             Width                           ,
                              s32             Height                          ,
                              void*           pData                           ,
                              xbool           bFreeMemoryOnDestruction        )
{
    setup( Width,
           Height,
           FORMAT_XCOLOR,
           sizeof(xcolor)*( 1 + Width*Height ),
           sizeof(xcolor)*Width*Height,
           pData,
           bFreeMemoryOnDestruction,
           1,
           1 );
}


//-------------------------------------------------------------------------------

xbool xbitmap::hasAlphaChannel( void ) const
{
    static xsafe_array<xbool,FORMAT_TOTAL> bSupportAlpha;
    static xbool bInit=FALSE;

    if( bInit == FALSE )
    {
        bInit = TRUE;
        
        bSupportAlpha[ FORMAT_A4B4G4R4          ] = 1;
        bSupportAlpha[ FORMAT_R8G8B8            ] = 0;
        bSupportAlpha[ FORMAT_R8G8B8U8          ] = 0;
        bSupportAlpha[ FORMAT_R8G8B8A8          ] = 1;
        bSupportAlpha[ FORMAT_XCOLOR            ] = 1;
        bSupportAlpha[ FORMAT_A8R8G8B8          ] = 1;
        bSupportAlpha[ FORMAT_U8R8G8B8          ] = 0;
        
	    bSupportAlpha[ FORMAT_PAL4              ] = 1;
	    bSupportAlpha[ FORMAT_PAL8              ] = 1;
        
	    bSupportAlpha[ FORMAT_DXT1_RGBA              ] = 1;
	    bSupportAlpha[ FORMAT_DXT3_RGBA              ] = 1;
	    bSupportAlpha[ FORMAT_DXT5_RGBA              ] = 1;
        
        bSupportAlpha[ FORMAT_R8                ] = 0;
        bSupportAlpha[ FORMAT_D32               ] = 0;
        bSupportAlpha[ FORMAT_R32               ] = 0;
        bSupportAlpha[ FORMAT_R8G8              ] = 0;
        bSupportAlpha[ FORMAT_R16G16B16A16      ] = 1;
        bSupportAlpha[ FORMAT_R16G16B16A16_f    ] = 1;
        bSupportAlpha[ FORMAT_R10G10B10A2       ] = 1;
        bSupportAlpha[ FORMAT_D24S8             ] = 0;
        bSupportAlpha[ FORMAT_R11G11B10         ] = 0;
        
	    bSupportAlpha[ FORMAT_16DU16DV          ] = 0;
	    bSupportAlpha[ FORMAT_16_DOT3_COMPRESSED] = 0;          // ??? DXT1? or ATI version?
	    bSupportAlpha[ FORMAT_A8                ] = 1;
        
	    // Xenon formats here
	    bSupportAlpha[ FORMAT_360_DXN           ] = 0;	                    // normal map compression ... 8bpp
	    bSupportAlpha[ FORMAT_360_DXT3A         ] = 1;	                // dxt3a
	    bSupportAlpha[ FORMAT_360_DXT5A         ] = 1;	                // dxt5a
	    bSupportAlpha[ FORMAT_360_CTX1          ] = 0;	                // normal map compression ... 4bpp
        
        bSupportAlpha[ FORMAT_ETC1              ] = 1;                        // Nintendo compression modes
        bSupportAlpha[ FORMAT_ETC2              ] = 1;
        
        bSupportAlpha[ FORMAT_PVR1_2RGB         ] = 0;                   // PVR compression modes
        bSupportAlpha[ FORMAT_PVR1_2RGBA        ] = 1;
        bSupportAlpha[ FORMAT_PVR1_4RGB         ] = 0;
        bSupportAlpha[ FORMAT_PVR1_4RGBA        ] = 1;
        
        bSupportAlpha[ FORMAT_PVR2_2RGBA        ] = 1;
        bSupportAlpha[ FORMAT_PVR2_4RGBA        ] = 1;
    }

    return bSupportAlpha[m_Format];
}

//-------------------------------------------------------------------------------

void xbitmap::ComputeHasAlphaInfo ( void )
{
    //
    // Do the easy case first
    //
    if( hasAlphaChannel() == FALSE )
    {
        setHasAlphaInfo( FALSE );
        return;
    }
    
    
    //
    // Check all the pixels to see if they have information other than the default
    //
    
    // We can handle anything with the compress formats and such
    ASSERT( m_Format < FORMAT_XCOLOR_END );
    
    const xcolor::fmt_desc& Dest   = xcolor::getFormatDesc( (xcolor::format)m_Format );
    const xcolor::format    Format = xcolor::format(m_Format);
    if( Dest.m_TB == 16 )
    {
        u32     X=0;
        xcolor  C1;
        u16*    pData = (u16*)getMip(0);
        for( s32 y=0; y<m_Height; y++ )
        for( s32 x=0; x<m_Width; x++ )
        {
            u16 Data = pData[ x + y*m_Width ];
            C1.BuildColorFromData( Data, Format );
            
            X |= (C1.m_A == 0xff);
            X |= (C1.m_A == 0x00)<<1;
            if( X == 1 || X == 2 )
                continue;
            
            setHasAlphaInfo( TRUE );
            return;
        }
    }
    else
    {
        u32     X=0;
        xcolor  C1;
        u32*    pData = (u32*)getMip(0);
        for( s32 y=0; y<m_Height; y++ )
        for( s32 x=0; x<m_Width; x++ )
        {
            u32 Data = pData[ x + y*m_Width ];
            C1.BuildColorFromData( Data, Format );
            
            X |= (C1.m_A == 0xff);
            X |= (C1.m_A == 0x00)<<1;
            if( X == 1 || X == 2 )
                continue;
            
            setHasAlphaInfo( TRUE );
            return;
        }
    }
}

//-------------------------------------------------------------------------------

void xbitmap::setDefaultTexture( void )
{
    static xbitmap      Bitmap;
    
    Kill();
    
    //
    // Create a simple texture
    //
    if( Bitmap.getHeight() == 0 )
    {
        xcolor C1( 128, 128, 128, 250 );
        xcolor C2( 200, 200, 200, 255 );
        
        xcolor CT[]={ xcolor( 0,  70, 70, 0 ),
                      xcolor( 0,  70,  0, 0 ),
                      xcolor( 70, 70,  0, 0 ) };
        s32    nCheckers    = 8;
        s32    Size         = 128;
        s32    CheckerSize  = 128/nCheckers;
        
        Bitmap.CreateBitmap( Size, Size );
        
        xcolor* pData = (xcolor*)Bitmap.getMip( 0 );
        
        // Create basic checker pattern
        for( s32 y = 0; y < Size; y++ )
        {
            for( s32 x = 0; x < Size; x++)
            {
                // Create the checker pattern
                pData[ x+ Size*y ] = ((y&CheckerSize)==CheckerSize)^((x&CheckerSize)==CheckerSize)?C1:C2;
            }
        }
        
        // Draw a simple arrows at the top left pointing up...
        for( s32 k=0; k<3; k++ )
        {
            s32 yy = 1;
            for( s32 y=1; y<(CheckerSize-1); ++y )
            {
                for( s32 x = yy; x < (CheckerSize-1)-yy; x++)
                {
                    if( k&1) pData[ k*CheckerSize + x + Size*(CheckerSize-y - 1) ] += CT[k];
                    else     pData[ k*CheckerSize + x + Size*(CheckerSize-y - 1) ] -= CT[k];
                }
                
                if (y&1) yy++;
            }
        }
    }
    
    //
    // Assign it to xbitmap
    //
    *this = Bitmap;
    
    // Make sure it wont release the data
    x_FlagOff( m_Flags, FLAGS_OWN_MEMORY );
}

//-------------------------------------------------------------------------------

const xbitmap& xbitmap::getDefaultBitmap( void )
{
    static xbitmap  Bitmap;
    
    if( Bitmap.getHeight() == 0 )
    {
        Bitmap.setDefaultTexture();
    }
    
    return Bitmap;
}

//-------------------------------------------------------------------------------

void xbitmap::CreateBitmap( s32 Width, s32 Height )
{
    ASSERT( Width  >= 1 );
    ASSERT( Height >= 1 );
    
    // Allocate the necesary data
    s32* pPtr = (s32*)x_malloc( sizeof(s32), 1 + Width * Height, 0 );
    
    // Initialize the offset table
    *pPtr = 0;
    
    setupFromColor( Width, Height, pPtr, TRUE );
}

//-------------------------------------------------------------------------------

xbool xbitmap::SaveTGA( const xstring FileName ) const
{
    xbyte   Header[18];
    
    // The format of this picture must be in color format
    ASSERT( m_Format == FORMAT_XCOLOR || m_Format == FORMAT_R8G8B8U8 );
    
    // Build the header information.
    x_memset( Header, 0, 18 );
    Header[ 2] = 2;     // Image type.
    Header[12] = (getWidth()  >> 0) & 0xFF;
    Header[13] = (getWidth()  >> 8) & 0xFF;
    Header[14] = (getHeight() >> 0) & 0xFF;
    Header[15] = (getHeight() >> 8) & 0xFF;
    Header[16] = 32;    // Bit depth.
    Header[17] = 32;    // NOT flipped vertically.
    
    // Open the file.
    xfile File;
    
    if( !File.Open( FileName, "wb" ) )
    {
        return FALSE;
    }
    
    // Write out the data.
    File.WriteRaw( Header, 1, 18 );
    
    //
    // Convert to what tga expects as a color
    //
    const xcolor* pColor = (xcolor*)getMip(0);
    const s32     Size   = getWidth() * getHeight();
    
    for( s32 i=0; i<Size; ++i )
    {
        xcolor Color = pColor[i];
        x_Swap( Color.m_R, Color.m_B );
        File.WriteRaw( &Color, 4, 1 );
    }
    
    return TRUE;
}

//-------------------------------------------------------------------------------

void xbitmap::CreateFromMips( const xbitmap* pMipList, s32 Count )
{
    //
    // Compute the total size
    //
    s32 TotalSize=0;
    for( s32 i=0; i<Count; i++ )
    {
        const xbitmap& Mip = pMipList[i];
        
        TotalSize += Mip.m_DataSize;
        
        ASSERT( Mip.m_nMips   == 1 );
        ASSERT( Mip.m_nFrames == 1 );
    }
    
    //
    // Allocate data
    //
    xbyte* pBaseData    = (xbyte*)x_malloc( 1, TotalSize, 0 );
    s32*   pOffsetTable = (s32*)pBaseData;
    xbyte* pData        = (xbyte*)&pOffsetTable[Count];
    
    //
    // Copy the actual data
    //
    {
        s32 TotalOffset=0;
        for( s32 i=0; i<Count; i++ )
        {
            const xbitmap& Mip         = pMipList[i];
            s32&           Offset      = pOffsetTable[i];
            const s32      MipDataSize = Mip.m_DataSize - sizeof(s32);
            
            Offset = TotalOffset;
            
            // Copy Data
            x_memcpy( &pData[ Offset ], &Mip.m_pData[1], MipDataSize );
            
            // Get ready for the next entry
            TotalOffset += MipDataSize;
        }
    }
    
    //
    // OK we are ready to setup the bitmap
    //
    const xbitmap& Mip         = pMipList[0];
    setup(
          Mip.m_Width,
          Mip.m_Height,
          (xbitmap::format)Mip.m_Format,
          TotalSize,
          TotalSize - sizeof(s32)*Count,
          pBaseData,
          TRUE,
          Count,
          1 );
}

