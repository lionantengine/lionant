//==============================================================================
// INCLUDES
//==============================================================================
#include "../../x_target.h"
#include "x_file_device.h"
#include "s3e.h"
#include "s3eDebug.h"


//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

void* mmlade_device::Open( const char* pFileName, u32 Flags )
{
	char* mode = NULL;

	if( pFileName != NULL )
    {
        mode = "a+";

        if( x_FlagIsOn( Flags, xfile_device_i::ACC_CREATE ) )
        {
            mode = "a+";
        }
        else
        {
            if( x_FlagIsOn( Flags, xfile_device_i::ACC_WRITE ) == FALSE )
            {
                mode = "r";
            }
        }

        // open the file (or create a new one)
        s3eFile* file = s3eFileOpen (pFileName,mode);
        if( file == NULL) 
        {
            s3eFileError  error = s3eFileGetError();
            switch (error)
            {
            case S3E_FILE_ERR_INVALID_PATH:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"file name could not be resolved onto a device-specific path\n");
                break;
            case S3E_FILE_ERR_NOT_FOUND:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"file could not be found\n");
                break;
            case S3E_FILE_ERR_ACCESS:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"file permission was denied on the file\n");
                break;
            case S3E_FILE_ERR_DEVICE:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"underlying file device failed\n");
                break;
            case S3E_FILE_ERR_TOO_MANY:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"maxmimum number of file handles has been reached\n");
                break;
            }

            return NULL;
        } 

        //
        // Okay we are in business
        //
        xhandle hFile;
        mmlade_device::file&   File = m_lFiles.append( hFile );
        xptr_lock    Ref( m_lFiles );

        File.m_Handle = file;
        File.m_Flags  = Flags;
        File.mOffset  = 0;


        if( x_FlagIsOn( Flags, xfile_device_i::ACC_ASYNC ) ) 
        {
            //didn't support async
        }

        // done
        return (void*)(u64)(hFile.m_Handle+1);
    }

    return NULL;	
}

//------------------------------------------------------------------------------
void mmlade_device::AsyncAbort( void* pFile )
{
    //didn't support aysnc right now
}

//------------------------------------------------------------------------------

xfile::sync_state mmlade_device::Synchronize( void* pFile, xbool bBlock )
{
    //didn't support async right now
    return xfile::SYNC_UNKNOWN_ERR;
}

//------------------------------------------------------------------------------

void mmlade_device::Close( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    
    //
    // Close the handle
    //
    {
        xptr_lock    Ref( m_lFiles );
        mmlade_device::file&   File = m_lFiles( hFile );
        
        if( S3E_FILE_ERR_NONE != s3eFileClose( File.m_Handle ) )
        {
            s3eFileError error = s3eFileGetError();

            switch(error)
            {
            case S3E_FILE_ERR_PARAM:
                s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP," file is not a valid file handle\n");
                break;
            default:
                break;
            }         
        }
    }
    
    //
    // Lets free our entry
    //
    m_lFiles.DeleteByHandle( hFile );
}

//------------------------------------------------------------------------------

xbool mmlade_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock               Ref( m_lFiles );
    mmlade_device::file&   File = m_lFiles( hFile );
    
    u32 sizeReaded = s3eFileRead( pBuffer, sizeof(s32), Count, File.m_Handle);

    if (sizeReaded == 0) 
    { 
        s3eFileError error = s3eFileGetError();

        switch(error)
        {
        case S3E_FILE_ERR_PARAM:
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"buffer is null or file is invalid\n");
            break;
        case S3E_FILE_ERR_DATA:
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"underlying file device failded to read in data\n");
            break;
        case S3E_FILE_ERR_EOF:
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"end of the file was reached before all the data could be read in\n");
            break;

        default:
            break;
        }
        
        return FALSE;
    }
    
    // Not problems
    return TRUE;
}

//------------------------------------------------------------------------------

void mmlade_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    if (!s3eFileWrite( pBuffer, sizeof(s32), Count,File.m_Handle)) 
    { 

        s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"File Writing Error\n");
    }
    return;
}

//------------------------------------------------------------------------------
void mmlade_device::Seek( void* pFile, seek_mode aMode, s32 Pos )
{
     xhandle hFile;
     hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    s3eFileSeekOrigin HardwareMode;
    switch( aMode )
    {
        case SKM_ORIGIN: HardwareMode = S3E_FILESEEK_SET; break;
        case SKM_CURENT: HardwareMode = S3E_FILESEEK_CUR; break;
        case SKM_END:    HardwareMode = S3E_FILESEEK_END; break; 
        default: 
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"unknown file seek mode\n");
            break;
    } 
        
	// Seek!
    if( !s3eFileSeek( File.m_Handle, Pos, HardwareMode ))
    {
        s3eFileError error = s3eFileGetError();
        switch(error)
        {
        case S3E_FILE_ERR_PARAM:
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"file is invalid, origin is invalid, or an attempt is made to seek before the beginning of the file\n");
            break;
        case S3E_FILE_ERR_DEVICE:
            s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP," underlying file device failed to seek\n");
            break;
        }
        
    }    

    return;
}

//------------------------------------------------------------------------------

s32 mmlade_device::Tell( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
   
    s32 pos = s3eFileTell( File.m_Handle);
    if( -1 == pos )
    {
        s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"Can't seek the file!\n");
    }
    
    return pos;
}

//------------------------------------------------------------------------------

void mmlade_device::Flush( void* pFile )
{
    //didn't support async
}

//------------------------------------------------------------------------------

s32 mmlade_device::Length( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    mmlade_device::file&   File = m_lFiles( hFile );

    return s3eFileGetSize( File.m_Handle );
}

//------------------------------------------------------------------------------

xbool mmlade_device::IsEOF( void* pFile )
{
    xhandle hFile;
    hFile.Set( (s32)(u64)( pFile ) -1 );
    xptr_lock    Ref( m_lFiles );
    file&   File = m_lFiles( hFile );
    
    if (!s3eFileEOF( File.m_Handle)) 
    { 

        s3eDebugAssertShow(S3E_MESSAGE_CONTINUE_STOP,"File get EOF error!\n");
    }
    
    // Not sure about this yet
    return FALSE;
}
