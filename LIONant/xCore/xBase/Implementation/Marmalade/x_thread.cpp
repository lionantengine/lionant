//==============================================================================
// INCLUDES
//==============================================================================
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>
#include "../../x_target.h"
#include "../../x_base.h"
#include "../x_types_inline.h"
#include "s3eDevice.h"

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//===============================================================================
// LOCKLESS FUNCTIONS
//===============================================================================
/********************************************************************************
#ifdef X_TARGET_32BIT

//-------------------------------------------------------------------------------
// CAS_acquire uses acquire semantics (usually for pops)
xbool x_CasAcquire( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal )
{
	xbool RetVal = InterlockedCompareExchange((LONG*)ptr, (LONG)NewVal, (LONG)OldVal) == (LONG)OldVal;
	X_LWSYNC();
	return RetVal;
}

//-------------------------------------------------------------------------------
// CAS_release uses release semantics (usually for pushes)
xbool x_CasRelease( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal)
{
	X_LWSYNC();
	return InterlockedCompareExchange((LONG*)ptr, (LONG)NewVal, (LONG)OldVal) == (LONG)OldVal;
}

//-------------------------------------------------------------------------------
// this only works for x86 chips. That's all we've got here, but if we're shipping we need to use the Vista version.
xbool x_Cas2( x_cas_base_node* volatile* ptr, x_cas_base_node* Old1, u32 Old2, x_cas_base_node* New1, u32 New2 )
{
	register bool f;
	_asm
	{
		    mov esi,ptr
			mov eax,old1
			mov edx,old2
			mov ebx,new1
			mov ecx,new2
			lock cmpxchg8b [esi]
			setz f
	}
	return f;
}

//-------------------------------------------------------------------------------
void x_Swap2( x_cas_base_node* volatile* ptr, x_cas_base_node* NewNode, u32 NewCount)
{
	LONGLONG Exchange = (LONGLONG)(NewNode) << 32 | NewCount;
	InterlockedExchange64(reinterpret_cast<LONGLONG volatile *>(ptr), Exchange);
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
// 64BIT Version of the functions
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

#elif defined X_TARGET_64BIT

//-------------------------------------------------------------------------------
// CAS_acquire uses acquire semantics (usually for pops)
xbool x_CasAcquire( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal )
{
	xbool RetVal = InterlockedCompareExchange64((LONGLONG*)ptr, (LONGLONG)NewVal, (LONGLONG)OldVal) == (LONGLONG)OldVal;
	X_LWSYNC();
	return RetVal;
}

//-------------------------------------------------------------------------------
// CAS_release uses release semantics (usually for pushes)
xbool x_CasRelease( x_cas_base_node* volatile* ptr, x_cas_base_node* OldVal, x_cas_base_node* NewVal)
{
	X_LWSYNC();
	return InterlockedCompareExchange64((LONGLONG*)ptr, (LONGLONG)NewVal, (LONGLONG)OldVal) == (LONGLONG)OldVal;
}

//-------------------------------------------------------------------------------
xbool x_Cas2( x_cas_base_node* volatile* ptr, x_cas_base_node* Old1, u32 Old2, x_cas_base_node* New1, u32 New2 )
{
	LONGLONG Comparand = (LONGLONG)(Old1) << 32 | Old2;
	LONGLONG Exchange  = (LONGLONG)(New1) << 32 | New2;

	if( Comparand != *((LONGLONG*)ptr) ) 
    {
		return FALSE;
	}

	return InterlockedCompareExchange64( (LONGLONG volatile *) ptr, Exchange, Comparand) == Comparand;
}

//-------------------------------------------------------------------------------
void x_Swap2( x_cas_base_node* volatile* ptr, x_cas_base_node* NewNode, u32 NewCount)
{
	LONGLONG Exchange = (LONGLONG)(NewNode) << 32 | NewCount;
	InterlockedExchange64(reinterpret_cast<LONGLONG volatile *>(ptr), Exchange);
}

#endif
********************************************************************************/
//==============================================================================
// types
//==============================================================================

struct xthread_data
{
    pthread_t           mThread;
    xsemaphore          mSemaphore;
    xbool               mIsFinished;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This is the main thread class.
// Description:
//     As you can see it comes from the xthread. This allows to treat all the threads
//     the same way including the main thread. The Initialize function sets up some
//     basic variables that the xthread needs. Note that the xthread is never really
//     created since one we get to this point is already done. Note that the main thread
//     has a critical section inside of it. That is the global critical section. 
//     g_pThread is a variable which is unique per-thread although it does not looks like.
//     There is where we keep the pointer of the thread.
//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN

static pthread_key_t    gCurrentThreadKey = -1;
static pthread_key_t    gThreadId = -1;
static int              gNextThreadId = 0;

class main_thread : public xthread
{
public:

    void Initialize( void )
    {   
        //
        // Set the basic info
        // 
        x_strcpy( m_Name, m_Name.getCount(), "MainThread" );
        m_ID        = FindThreadID();
        m_Handle = new xthread_data();
        xthread_data*   threadData = (xthread_data*)m_Handle;
        threadData->mIsFinished = false;

        //
        // Set the pointer for the thread
        //
        pthread_setspecific(gCurrentThreadKey, (const void*)this);
        pthread_setspecific(gThreadId, (const void*)gNextThreadId++);
    }
    
    void Uninitialize()
    {
        delete ((xthread_data*)m_Handle);
    }
};
//DOM-IGNORE-END

//==============================================================================
// Variables
//==============================================================================
//DOM-IGNORE-BEGIN
#if defined( TARGET_PC )

// This is PC specific. This will force x_Init to be called
// prior to the constructors so constructors should be able to
// do memory allocations WITH the allocator initialized.
#pragma warning(push)
#pragma warning( disable : 4073 )
#pragma  init_seg(lib)

// Make the constructor be the first ones to kick on
static main_thread s_MainThread;

// this will be the second constructors
struct pc_forcestartup
{
public:
     pc_forcestartup( void ) 
     { 
        x_BaseInit(); 
     }

    ~pc_forcestartup( void ) 
    { 
        x_BaseKill(); 
    }

} g_xForceStartup;


// back to the local name space
#pragma warning(pop)
#endif
//DOM-IGNORE-END
//==============================================================================
// FUNCTIONS
//==============================================================================

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
#if defined TARGET_IOS || TARGET_MARMALADE

static main_thread s_MainThread;
//==============================================================================
#undef new
#undef delete
xcritical_section::xcritical_section( void )
{
    m_Count            = 0;
    m_pCriticalSection = new pthread_mutex_t();
    ASSERT(m_pCriticalSection);

    pthread_mutex_init((pthread_mutex_t *)m_pCriticalSection, NULL);
}

//==============================================================================

xcritical_section::~xcritical_section( void )
{
    if( m_pCriticalSection )
    {
        delete (pthread_mutex_t*)m_pCriticalSection;
    }
}

//==============================================================================

void xcritical_section::BeginAtomic( void )
{
    ASSERT(m_pCriticalSection);
    pthread_mutex_lock( (pthread_mutex_t*)m_pCriticalSection );
    m_Count++;
}

//==============================================================================

void xcritical_section::EndAtomic( void )
{
    m_Count--;
    ASSERT(m_pCriticalSection);
    pthread_mutex_unlock( (pthread_mutex_t*)m_pCriticalSection );
}

//==============================================================================

xbool xcritical_section::IsAtomic( void ) const
{
    return !!m_Count;
}

//==============================================================================
struct xsemaphore_data
{
    pthread_mutex_t     mMutex;
    s32                 mCount;
};

xsemaphore::xsemaphore()
{
    xsemaphore_data*    data = new xsemaphore_data();
    mHandle = data;
    ASSERT(NULL != mHandle);
    data->mCount = 0;
    pthread_mutex_init(&data->mMutex, NULL);
}

xsemaphore::~xsemaphore()
{
    if ( mHandle )
    {
        xsemaphore_data*    data = (xsemaphore_data*)mHandle;
        pthread_mutex_destroy(&data->mMutex);
        delete data;
        mHandle = NULL;
    }
}

void xsemaphore::WaitOne()
{
    ASSERT(NULL != mHandle);
    xsemaphore_data*    data = (xsemaphore_data*)mHandle;
    while(true)
    {
        bool isSignaled = false;
        pthread_mutex_lock(&data->mMutex);
        if ( data->mCount )
        {
            data->mCount = 0;
            isSignaled = true;
        }
        pthread_mutex_unlock(&data->mMutex);
        if ( isSignaled )
        {
            return;
        }
    }
}

xbool xsemaphore::WaitOne(int millisecondsTimeOut)
{
    ASSERT(NULL != mHandle);
    xsemaphore_data*    data = (xsemaphore_data*)mHandle;
    int now = gettimeofday(NULL, NULL);
    bool isSignaled = false;
    do
    {
        if ( 0 == pthread_mutex_trylock(&data->mMutex) )
        {
            if ( data->mCount )
            {
                data->mCount = 0;
                isSignaled = true;
            }
            pthread_mutex_unlock(&data->mMutex);
        }
        if ( isSignaled )
        {
            break;
        }
    } while ((gettimeofday(NULL, NULL) - now) < millisecondsTimeOut);
    return isSignaled;
}

void xsemaphore::Release()
{
    ASSERT(NULL != mHandle);
    xsemaphore_data*    data = (xsemaphore_data*)mHandle;
    pthread_mutex_lock(&data->mMutex);
    data->mCount = 1;
    pthread_mutex_unlock(&data->mMutex);
}

#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER
#endif // TARGET_IOS

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

//==============================================================================

xscope_csection::xscope_csection( xcritical_section& CriticalSection ) :
    m_RefCount( 0 ),
    m_CriticalSection( CriticalSection ) 
{
    BeginAtomic();
}

//==============================================================================

xscope_csection::~xscope_csection( void )
{
    ASSERT( m_RefCount >= 0 );
    for( s32 i=0; i<m_RefCount; i++ )
    {
        m_CriticalSection.EndAtomic();
    }
}

//==============================================================================

void xscope_csection::BeginAtomic( void )
{
    m_CriticalSection.BeginAtomic();
    m_RefCount++;
}

//==============================================================================

void xscope_csection::EndAtomic( void )
{
    ASSERT( m_RefCount > 0 );
    m_CriticalSection.EndAtomic();
    m_RefCount--;
}

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

xthread::xthread( void )
{
    m_ID            = 0;
    m_Handle        = 0;
    m_bAutoDelete   = FALSE;
    m_bInitialize   = FALSE;
}


//==============================================================================

xthread::~xthread( void )
{
    if ( m_Handle )
    {
        x_delete((xthread_data*)m_Handle);
        m_Handle    = 0;
    }
}

//==============================================================================
#undef new
#undef delete
s32 ExecuteFirstCall( void* pParam )
{
    xthread* This = (xthread*)pParam;

    //
    // Keep a reference to this thread
    //
    pthread_setspecific(gCurrentThreadKey, (const void *)This);
    pthread_setspecific(gThreadId, (const void*)gNextThreadId++);

    //
    // for logic control lets call the user init
    //
    This->onInitInstance();

    //
    // Tell the program to run
    //
    This->onRun();

    //
    // Okay we must be done now
    //
    xbool bAutoDelete = This->m_bAutoDelete;
    This->onExitInstance();

    //
    // See whether we have to clean house
    //
    if( This ) x_delete(This);

    return 0;
}
#define new                     X_NEW_HANDLER
#define delete                  X_NEW_HANDLER

//==============================================================================

void xthread::onInitInstance( void ) 
{
    x_BaseInitInstance();
}

//==============================================================================

void xthread::onExitInstance( void ) 
{
    x_BaseKillInstance();
    
    xthread_data*   threadData = (xthread_data*)m_Handle;
    
    ASSERT(NULL != threadData);
    
    threadData->mSemaphore.Release();
}

typedef void* (*ThreadExecutionCallBack)(void*);

//==============================================================================

void xthread::Create( 
    const char*                 pName,
    xbool                       bStartActive,
    xthread::priority           Priority, 
    s32                         StackSize, 
    xbool                       bAutoDelete )
{
    ASSERT( pName );
    ASSERT( StackSize > 256 );
    ASSERT( m_bInitialize == FALSE );

    //
    // Set the auto delete flag
    // and the name
    //
    m_bAutoDelete = bAutoDelete;
    ASSERT( x_strlen( pName ) < m_Name.getCount() );
    x_strcpy( m_Name, m_Name.getCount(), pName );

    m_Handle = x_new(xthread_data, 1, 0);
    xthread_data*   threadData = (xthread_data*)m_Handle;
    threadData->mIsFinished = false;
    
    //
    // Create a windows thread
    // TODO: This hold thing will have to be moved to other platforms
    //
    pthread_attr_t threadAttr;
    
    pthread_attr_init(&threadAttr);
    pthread_attr_setstacksize(&threadAttr, StackSize);
    
    sched_param param;
    //
    // Lookup the priority
    //
    // Maramalade don't support priority setting, sched_get_priority_min is not supported
    // so we just using hard code first.
    s32 threadPriority = 1;
    switch( Priority )
    {
        case PRIORITY_TIME_CRITICAL:    param.sched_priority = threadPriority; break;
        case PRIORITY_HIGHEST:          param.sched_priority = threadPriority; break;
        case PRIORITY_ABOVE_NORMAL:     param.sched_priority = threadPriority; break;
        case PRIORITY_NORMAL:           param.sched_priority = threadPriority; break;
        case PRIORITY_BELOW_NORMAL:     param.sched_priority = threadPriority; break;
        case PRIORITY_LOWEST:           param.sched_priority = threadPriority; break;
        case PRIORITY_IDLE:             param.sched_priority = threadPriority; break;
    }
    
    pthread_attr_setschedparam(&threadAttr, &param);
    
    pthread_create(&threadData->mThread, &threadAttr, (ThreadExecutionCallBack)ExecuteFirstCall, (void*)this);

    //
    // We are initialize now
    //
    m_bInitialize   = TRUE;

    //
    // Set the priority of this thread
    //
    SetPriority( Priority );

//TODO:    LOG_INFO( "THREAD", "Thread Created:[%s] ID:%d\n", pName, m_ID );
}

//==============================================================================

xbool xthread::StillAlife( void ) const
{
    if ( ((xthread_data*)m_Handle)->mSemaphore.WaitOne(1) )
    {
        return FALSE;
    }
    return TRUE;
}

//==============================================================================

void xthread::WaitUntillDone( void ) const
{
    ASSERT(NULL != m_Handle);
    xthread_data*   threadData = (xthread_data*)m_Handle;
    threadData->mSemaphore.WaitOne();
}


//==============================================================================

void xthread::Resume( void )
{
    ASSERT(!"Resume is not supported on IOS");
}

//==============================================================================

void xthread::Suspend( void )
{
    ASSERT(!"Suspend is not supported on IOS");
}

//==============================================================================

void xthread::BeginAtomic( void )
{
    m_CriticalSection.BeginAtomic();
}

//==============================================================================

void xthread::EndAtomic( void )
{
    m_CriticalSection.EndAtomic();
}

//==============================================================================

xbool xthread::IsAtomic( void )
{
    return m_CriticalSection.IsAtomic();
}

//==============================================================================

void xthread::SetPriority( xthread::priority Priority )
{
    ASSERT(!"SetPriority is not supported on IOS");
}

//==============================================================================

void xthread::Sleep( s32 Milliseconds )
{
    x_Sleep( Milliseconds );
}

//==============================================================================
s32 xthread::FindThreadID( void )
{
    return (s32)pthread_getspecific(gThreadId);
}

//==============================================================================
void* xthread::FindHandle( void )
{
    xthread* currentThread = (xthread*)pthread_getspecific(gCurrentThreadKey);
    if ( currentThread )
    {
        return currentThread->m_Handle;
    }
    else
    {
        return NULL;
    }
}

//==============================================================================
void xthread::Destroy( void )
{
    pthread_cancel(((xthread_data*)m_Handle)->mThread);
}

//==============================================================================
s32 xthread::GetID( void ) const
{
    return m_ID;
}


//==============================================================================
// functions
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
void x_ThreadInit( void )
{
    pthread_key_create(&gCurrentThreadKey, NULL);
    pthread_key_create(&gThreadId, NULL);
    //
    // Initializes the main thread
    //
    s_MainThread.Initialize();
}

//==============================================================================
void x_ThreadKill( void )
{
    //
    // TODO:
    // Go throw all the threads that may not have been terminated and
    // force them out of the system.
    //
    pthread_key_delete(gThreadId);
    pthread_key_delete(gCurrentThreadKey);
}

//==============================================================================

void x_Sleep( s32 Milliseconds )
{
    sleep( Milliseconds );
}

//==============================================================================

void x_Yield( void )
{
    ::sched_yield();
}

//==============================================================================

xthread& x_GetCurrentThread ( void )
{
    // This doesn't work because we didn't 
    xthread* pThread = (xthread*)pthread_getspecific(gCurrentThreadKey);
    ASSERT(pThread);
    return *pThread;
}

//==============================================================================

xbase_instance& x_GetXBaseInstance( void )
{
    xthread& Thread = x_GetCurrentThread();
    return Thread.m_XBaseInstance;
}
//DOM-IGNORE-END

//==============================================================================

void x_BeginAtomic( void )
{
    s_MainThread.BeginAtomic();
}

//==============================================================================

void x_EndAtomic( void )
{
    s_MainThread.EndAtomic();
}

//==============================================================================

xbool x_IsAtomic( void )
{
    return s_MainThread.IsAtomic();
}

//==============================================================================

xthread& x_GetMainThread( void )
{
    return s_MainThread;
}

s32 x_GetCPUCoreCount( void )
{
    return s3eDeviceGetInt(S3E_DEVICE_NUM_CPU_CORES);;
}