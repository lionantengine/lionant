#ifndef	_X_LOCKLESS_CONTAINER_MARMALADE_INLINE_
#define	_X_LOCKLESS_CONTAINER_MARMALADE_INLINE_

#include "../x_target.h"

inline xbool x_cas32( u32*  pPointer,
                     u32   Old,
                     u32   New)
{
    ASSERT( pPointer );
    return FALSE;
}

//-------------------------------------------------------------------------------

inline s32 x_casInc32( s32* pPointer )
{
    ASSERT( pPointer );
    return FALSE;
}

//-------------------------------------------------------------------------------

inline s32 x_casDec32( s32* pPointer )
{
    ASSERT( pPointer );
    return FALSE;
}

//-------------------------------------------------------------------------------

inline xbool x_cas64( u64*  pPointer, 
                     u32   OldPtr, 
                     u32   OldGuard, 
                     u32   NewPtr, 
                     u32   NewGuard)
{
    return FALSE;
}

inline xbool x_cas64( u64*            pPointer, 
                     const void*     pOldPtr, 
                     u32             OldGuard, 
                     const void*     pNewPtr, 
                     u32             NewGuard )
{
    return FALSE;
}

//-------------------------------------------------------------------------------
//				QT FLAGS
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOn( u32 Flags, x_qt_flags& LocalReality )
{
    
}

//-------------------------------------------------------------------------------
inline
xbool x_qt_flags::qtSetFlagsOff( u32 Flags, x_qt_flags& LocalReality )
{

}
#endif  ///end _X_LOCKLESS_CONTAINER_MARMALADE_INLINE_