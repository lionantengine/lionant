
//===============================================================================
// DEFINE THE X_GUID MACRO
//===============================================================================
#undef X_GUID
#define __X_GUID_HELPER(C,TOTAL)                                            \
    (                                                                           \
        (10+26) * u64(TOTAL) + (( ((C) >= '0') && ((C) <= '9') )? (C) - '0' :   \
        ( ((C) >= 'a') && ((C) <= 'z') )? (C) - 'a' + 10:                       \
        ( ((C) >= 'A') && ((C) <= 'Z') )? (C) - 'A' + 10:                       \
        0)                                                                      \
    )

#if _MSC_VER < 1900
    #define X_GUID( STR )                                   \
    u64(                                                    \
    __X_GUID_HELPER( ( #STR )[13],                          \
    __X_GUID_HELPER( ( #STR )[12],                          \
    __X_GUID_HELPER( ( #STR )[11],                          \
    __X_GUID_HELPER( ( #STR )[10],                          \
    __X_GUID_HELPER( ( #STR )[8],                           \
    __X_GUID_HELPER( ( #STR )[7],                           \
    __X_GUID_HELPER( ( #STR )[6],                           \
    __X_GUID_HELPER( ( #STR )[5],                           \
    __X_GUID_HELPER( ( #STR )[3],                           \
    __X_GUID_HELPER( ( #STR )[2],                           \
    __X_GUID_HELPER( ( #STR )[1],                           \
    __X_GUID_HELPER( ( #STR )[0], 0 ))))))))))))            \
    )

#else

    inline constexpr
    u64 __X_GUID( const char* STR )
    {
        return u64( 
        __X_GUID_HELPER( STR[ 13 ], \
        __X_GUID_HELPER( STR[ 12 ], \
        __X_GUID_HELPER( STR[ 11 ], \
        __X_GUID_HELPER( STR[ 10 ], \
        __X_GUID_HELPER( STR[ 8 ], \
        __X_GUID_HELPER( STR[ 7 ], \
        __X_GUID_HELPER( STR[ 6 ], \
        __X_GUID_HELPER( STR[ 5 ], \
        __X_GUID_HELPER( STR[ 3 ], \
        __X_GUID_HELPER( STR[ 2 ], \
        __X_GUID_HELPER( STR[ 1 ], \
        __X_GUID_HELPER( STR[ 0 ], 0 ) ) ) ) ) ) ) ) ) ) ) )            
        );
    }                                   

    #define X_GUID( STR )  __X_GUID(#STR)
#endif
//===============================================================================
// FUNCTIONS
//===============================================================================

//-------------------------------------------------------------------------------

inline 
xguid::xguid( void ) : 
    m_Guid(0) {}

//-------------------------------------------------------------------------------

inline 
xguid::xguid( u64 NewGuid ) : 
    m_Guid( NewGuid ) {}


//-------------------------------------------------------------------------------

inline
void xguid::setNull( void )                
{ 
    m_Guid = 0; 
}

//-------------------------------------------------------------------------------

inline
xbool xguid::IsNull( void ) const          
{ 
    return m_Guid==0; 
}   

//-------------------------------------------------------------------------------

inline
xguid::operator u64( void ) const
{
    return m_Guid; 
}

//-------------------------------------------------------------------------------

inline
u32 xguid::GetLow( void )
{ 
    return (u32)((m_Guid>> 0)&0xFFFFFFFF); 
}

//-------------------------------------------------------------------------------

inline
u32 xguid::GetHigh( void )
{
    return (u32)((m_Guid>>32)&0xFFFFFFFF); 
}

//-------------------------------------------------------------------------------

inline
xstring xguid::GetHexString( void ) const
{ 
    xstring String;
    String.Format( "%08X:%08X", (u32)((m_Guid>>32)&0xFFFFFFFF), (u32)((m_Guid>>0)&0xFFFFFFFF) ); 
    return String;
}


//===============================================================================
//===============================================================================
//===============================================================================
// xghash
//===============================================================================
//===============================================================================
//===============================================================================

//-------------------------------------------------------------------------------
template< class T > inline
xghash<T>::xghash( void )
{
    m_Count      = 0;
    m_iEmptyList = -1;
}

//-------------------------------------------------------------------------------
template< class T > inline
s32 xghash<T>::GetHashIndex( xguid Guid ) const
{
    const s32 Count = m_lHash.getCount();
    const s32 H     = (s32)(Guid.m_Guid % Count);
    return H;
}

//-------------------------------------------------------------------------------
template< class T >
void xghash<T>::GrowListBy( s32 nGuids )
{
    s32 i;

    ASSERT( nGuids > 0 );  
    
    // Do we need to grow or our we okay?
    s32 NewNodeCount = nGuids + m_Count;
    if( NewNodeCount < m_lNode.getCount() )
        return;

    // make the new count multiple of 1000 and choose the bigger side
    NewNodeCount = (NewNodeCount/1000)*1000;
    if( (nGuids + m_Count) > NewNodeCount ) NewNodeCount += 1000;

    // compute the size of the table for the hash entries
    s32 NewHashCount = ((NewNodeCount/2)*3) / 1000;
    if( NewHashCount > 256 ) NewHashCount = 256;
    NewHashCount = xguid::s_HashTableSize[ NewHashCount-1 ];

    //
    // realloc the nodes to the new count
    //
    s32 OldNodeCount = m_lNode.getCount();
    //m_lNode.Realloc( NewNodeCount );
    m_lNode.Alloc( NewNodeCount );

    //
    // Clear new nodes and add to free list
    //
    for( i = OldNodeCount; i < NewNodeCount; i++ )
    {
        m_lNode[i].m_Guid.setNull();
        m_lNode[i].m_Data   = 0;
        m_lNode[i].m_iNext  = m_iEmptyList;

        m_iEmptyList = i;
    }

    //
    // Allocate the new hash
    //
    //m_lHash.Free();
    m_lHash.Alloc( NewHashCount );

    // Clear hash table entries
    for( i=0; i<NewHashCount; i++ )
        m_lHash[i] = -1;

    //
    // Reinsert all the nodes again
    //

    // Insert current nodes into new hash table
    for( i=0; i<OldNodeCount; i++ )
    {
        // Check if node is in use
        if( m_lNode[i].m_Guid.IsNull() == FALSE )
        {
            // Compute hash entry index
            s32 Index = GetHashIndex( m_lNode[i].m_Guid );

            m_lNode[i].m_iNext = m_lHash[ Index ];
            m_lHash[ Index ]   = i;
        }
    }
}

//-------------------------------------------------------------------------------

template< class T > inline
s32 xghash<T>::FindNode( xguid Guid ) const
{
    if( m_lHash.getCount() <= 0 ) return -1;

    s32 Index = GetHashIndex( Guid );
    for( s32 iNode = m_lHash[ Index ]; iNode != -1; iNode = m_lNode[iNode].m_iNext )
    {
        if( m_lNode[iNode].m_Guid == Guid )
            return iNode;
    }

    return -1;
}

//-------------------------------------------------------------------------------

template< class T > inline
xbool xghash<T>::IsAlloc( xguid Guid ) const
{
    return FindNode( Guid ) != -1;
}

//-------------------------------------------------------------------------------

template< class T > inline
s32 xghash<T>::AllocNode( xguid Guid )
{
    ASSERT( FindNode( Guid ) == -1 );

    // Make sure that we have nodes
    if( m_iEmptyList == -1 )
        GrowListBy( x_Max( 1000, m_lNode.getCount()/2 ) );

    // take the node away from the empty list
    s32   iNode  = m_iEmptyList;
    node& Node   = m_lNode[m_iEmptyList];
    m_iEmptyList = m_lNode[m_iEmptyList].m_iNext;

    // Now lets insert it in the hash list
    s32 Index      = GetHashIndex( Guid );
    Node.m_iNext   = m_lHash[Index];
    m_lHash[Index] = iNode;

    // Add the count
    m_Count++;

    return iNode;
}

//-------------------------------------------------------------------------------

template< class T > inline
void xghash<T>::FreeNode( xguid Guid )
{
    // First lets find it
    s32 Index = GetHashIndex( Guid );

    // Get the first node
    s32 iNode = m_lHash[ Index ];

    // make sure that we have nodes in this hash entry
    // This means that the guid was not allocated
    ASSERT( iNode != -1 );

    // Is the first node in the hash?
    if( m_lNode[ iNode ].m_Guid == Guid )
    {
        m_lHash[ Index ] = m_lNode[ iNode ].m_iNext;
    }
    else
    {
        s32 iLastNode = iNode;
        for( iNode = m_lNode[iNode].m_iNext; iNode != -1; iNode = m_lNode[iNode].m_iNext )
        {
            if( m_lNode[iNode].m_Guid == Guid )
                break;
        }

        // This means that the guid was not allocated
        ASSERT( iNode != -1 );

        // Remove node from the chain
        m_lNode[ iLastNode ].m_iNext = m_lNode[ iNode ].m_iNext;
    }

    // OKay insert the node in the empty list
    m_lNode[ iNode ].m_iNext = m_iEmptyList;
    m_iEmptyList             = iNode;

    // Set the guid value to null (to indicate that we are not valid)
    m_lNode[ iNode ].m_Guid.setNull();

    // One less node
    m_Count--;
}

//-------------------------------------------------------------------------------

template< class T > inline
void xghash<T>::Add( xguid Guid, T Data )
{
    s32 iNode = AllocNode( Guid );
    m_lNode[iNode].m_Guid = Guid;
    m_lNode[iNode].m_Data = Data;
}

//-------------------------------------------------------------------------------

template< class T > inline
void xghash<T>::Delete( xguid Guid )
{
    FreeNode( Guid );
}

//-------------------------------------------------------------------------------

template< class T > inline
void xghash<T>::SetValue( xguid Guid, T Data )
{
    // Make sure that we have the node
    s32 iNode = FindNode( Guid );
    ASSERT( iNode >= 0 );

    // Set the data
    m_lNode[iNode].m_Data = Data;
}

//-------------------------------------------------------------------------------

template< class T > inline
T xghash<T>::GetValue( xguid Guid ) const
{
    // Make sure that we have the node
    s32 iNode = FindNode( Guid );
    ASSERT( iNode >= 0 );

    // Set the data
    return m_lNode[iNode].m_Data;
}

//-------------------------------------------------------------------------------

template< class T > inline
T xghash<T>::GetValueOrNull( xguid Guid ) const
{
    // Make sure that we have the node
    s32 iNode = FindNode( Guid );
    if( iNode < 0 ) return NULL;
    
    // return th actual node
    return m_lNode[iNode].m_Data;
}

