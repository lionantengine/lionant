//==============================================================================
// INCLUDES
//==============================================================================
//DOM-IGNORE-BEGIN
#include "../x_base.h"

//==============================================================================
// Functions
//==============================================================================
void x_FileSystemAddRamDevice( void );

//==============================================================================
//  MEMORY FILE CLASS
//==============================================================================
//
//  xmemblock
//      xmemblock is the class where the data is stored.
//
//  xmemfile 
//      xmemfile is a class that contains the interface to access the memory files.
//      currently, it is implemented as an array of xmemblock's of the size
//      xmemblock::block::SIZE.
//
//==============================================================================
class xmemblock
{
public:
    typedef xsafe_array<u8,1024> block;

    xmemblock( void )
    {
        m_pData = x_new( block, 1, 0 );
        ASSERT( m_pData );
    }

    ~xmemblock( void )
    {
        x_delete( m_pData );
    }

    void Set( s32 index, u8 data )
    {   
        (*m_pData)[index] = data;
    }

    u8 Get( s32 index )
    {
        return (*m_pData)[index];
    }

protected:

    block*  m_pData;
};

//------------------------------------------------------------------------------

class xmemfile
{
public:
            xmemfile        ( void );
           ~xmemfile        ( void );
    xbool   Read            (       void* pBuffer, s32 Count );  
    void    Write           ( const void* pBuffer, s32 Count );
    void    SeekOrigin      ( s32 Offset );
    void    SeekCurrent     ( s32 Offset );
    void    SeekEnd         ( s32 Offset );
    s32     Tell            ( void );
    xbool   IsEOF           ( void );
    s32     Getc            ( void );
    void    Putc            ( s32 C );           
    s32     GetFileLength   ( void );
    s32     GetNumBlocks    ( void );

protected:

    xarray<xmemblock>   m_Block;
    xbool               m_bCompressed;
    s32                 m_SeekPosition;
    s32                 m_EOF;    
};

//------------------------------------------------------------------------------

class xfile_ram_device : public xfile_device_i
{
public:
                    xfile_ram_device ( void ) : xfile_device_i("ram:"){}

protected:

    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock ){return xfile::SYNC_COMPLETED; }
    virtual void                AsyncAbort  ( void* pFile ){};
};

//------------------------------------------------------------------------------

void* xfile_ram_device::Open( const char* pFileName, u32 Flags )
{
    // Nothing to do with the file name
    (void)pFileName;
    (void)Flags;

    //
    // Allocate the structure
    //
    xmemfile* pMemFile = x_new( xmemfile, 1, 0 );

    return pMemFile;
}

//------------------------------------------------------------------------------

void xfile_ram_device::Close( void* pFile )
{
    ASSERT(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;
    x_delete( pMemFile );
}

//------------------------------------------------------------------------------

xbool xfile_ram_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    ASSERT(pFile);
    ASSERT(pBuffer);
    xmemfile* pMemFile = (xmemfile*)pFile;
    return pMemFile->Read( pBuffer, Count );
}

//------------------------------------------------------------------------------

void xfile_ram_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    ASSERT(pFile);
    ASSERT(pBuffer);
    xmemfile* pMemFile = (xmemfile*)pFile;
    pMemFile->Write( pBuffer, Count );
}

//------------------------------------------------------------------------------

void xfile_ram_device::Seek( void* pFile, seek_mode Mode, s32 Pos )
{
    ASSERT(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;

    switch( Mode )
    {
    case xfile_device_i::SKM_ORIGIN: pMemFile->SeekOrigin( Pos ); break;
    case xfile_device_i::SKM_CURENT: pMemFile->SeekCurrent( Pos ); break;
    case xfile_device_i::SKM_END:    pMemFile->SeekEnd( Pos ); break;
    default: ASSERT(0); break;
    }
}

//------------------------------------------------------------------------------

s32 xfile_ram_device::Tell( void* pFile )
{
    ASSERT(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;
    return pMemFile->Tell();
}

//------------------------------------------------------------------------------

void xfile_ram_device::Flush( void* pFile )
{
    ASSERT(pFile);
    (void)pFile;
}

//------------------------------------------------------------------------------

s32 xfile_ram_device::Length( void* pFile )
{
    ASSERT(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;
    return pMemFile->GetFileLength();
}

//------------------------------------------------------------------------------

xbool xfile_ram_device::IsEOF( void* pFile )
{
    ASSERT(pFile);
    xmemfile* pMemFile = (xmemfile*)pFile;
    return pMemFile->IsEOF();
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// XMEMFILE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

xmemfile::xmemfile( void )
{
    m_bCompressed   = FALSE;
    m_SeekPosition  = 0;
    m_EOF           = 0;
}

//------------------------------------------------------------------------------

xmemfile::~xmemfile( void )
{
}

//------------------------------------------------------------------------------

xbool xmemfile::Read( void* pBuffer, s32 Count )
{
    if( m_SeekPosition >= m_EOF )
        return FALSE;

    s32 currentBlockIndex  = m_SeekPosition / xmemblock::block::SIZE;
    s32 currentBlockOffset = m_SeekPosition % xmemblock::block::SIZE;
    s32 bufferOffset = 0;

    ASSERT( currentBlockIndex >= 0 && currentBlockIndex < m_Block.getCount() );
    
    while( bufferOffset < Count )
    {
        // Copy data from blocks.
        ((u8*)pBuffer)[bufferOffset] = m_Block[currentBlockIndex].Get(currentBlockOffset);
        currentBlockOffset++;
        bufferOffset++;
        m_SeekPosition++;
        if (currentBlockOffset >= xmemblock::block::SIZE)
        {
            // Since we've completed the current block, increment to next block.
            currentBlockIndex++;
            currentBlockOffset = 0;
            //ASSERT(currentBlockIndex < m_Block.getCount());
            if (currentBlockIndex >= m_Block.getCount()) 
                return TRUE;
        }
    }
    return TRUE;
}

//------------------------------------------------------------------------------

void xmemfile::Write( const void* pBuffer, s32 Count )
{
    // Check current position and size of data being added.
    s32 NewDataPosition = m_SeekPosition + Count;
    if( m_EOF < NewDataPosition ) m_EOF = NewDataPosition;

    // If we need to allocate more memory for the blocks, then do so.
    if (m_EOF >= m_Block.getCount() * xmemblock::block::SIZE)
    {
        s32 NumBlocksRequired = m_EOF/xmemblock::block::SIZE + 1;

        m_Block.appendList( NumBlocksRequired - m_Block.getCount() );
    }

    s32 currentBlockIndex  = m_SeekPosition / xmemblock::block::SIZE;
    s32 currentBlockOffset = m_SeekPosition % xmemblock::block::SIZE;
    s32 bufferOffset = 0;

    ASSERT( currentBlockIndex >= 0 && currentBlockIndex < m_Block.getCount() );
    
    while (bufferOffset < Count)
    {
        // Copy data into blocks.
        m_Block[currentBlockIndex].Set(currentBlockOffset, ((u8*)pBuffer)[bufferOffset]);

        bufferOffset++;
        currentBlockOffset++;
        m_SeekPosition++;

        if (currentBlockOffset >= xmemblock::block::SIZE)
        {
            // Since we've completed the current block, increment to next block.
            currentBlockIndex++;
            currentBlockOffset = 0;
            ASSERT(currentBlockIndex < m_Block.getCount());
        }
    }
}

//------------------------------------------------------------------------------

void xmemfile::SeekOrigin( s32 Offset )
{
    ASSERT( Offset >= 0 );
    m_SeekPosition = Offset;
}

//------------------------------------------------------------------------------

void xmemfile::SeekEnd( s32 Offset )
{
    ASSERT( Offset >= 0 );
    m_SeekPosition = m_EOF - Offset;
}

//------------------------------------------------------------------------------

void xmemfile::SeekCurrent( s32 Offset )
{
    m_SeekPosition += Offset;
}

//------------------------------------------------------------------------------

s32 xmemfile::Tell( void )
{
    // Tell does not necessarily return a valid position, 
    // because Seek allows user to seek outside of file bounds.
    return( m_SeekPosition );
}

//------------------------------------------------------------------------------

xbool xmemfile::IsEOF( void )
{
    if ( m_SeekPosition > m_EOF )
        return TRUE;

    return FALSE;
}

//------------------------------------------------------------------------------

s32 xmemfile::Getc( void )
{
    if ( m_SeekPosition > m_EOF )
        return( xfile::XEOF );

    s32 currentBlockIndex  = m_SeekPosition / xmemblock::block::SIZE;
    s32 currentBlockOffset = m_SeekPosition % xmemblock::block::SIZE;
    
    m_SeekPosition++;
    return((s32)m_Block[currentBlockIndex].Get(currentBlockOffset));
}

//------------------------------------------------------------------------------

void xmemfile::Putc( s32 C )           
{
    if ( m_SeekPosition > m_EOF )
        m_EOF = m_SeekPosition;

    // If we need to allocate more memory for the blocks, then do so.
    if (m_EOF >= m_Block.getCount() * xmemblock::block::SIZE)
    {
        // Make sure we don't have this strange degenerate case.
        ASSERT( xmemblock::block::SIZE > 1 );

        // Add one block, since it is just a char being added, 
        // it should never add more than one block.
        if ( m_Block.getCount() == 0 )
        {
            m_Block.append();
        }
    }

    s32 currentBlockIndex  = m_SeekPosition / xmemblock::block::SIZE;
    s32 currentBlockOffset = m_SeekPosition % xmemblock::block::SIZE;
    
    m_SeekPosition++;
    m_Block[currentBlockIndex].Set(currentBlockOffset, (u8)C );
}

//------------------------------------------------------------------------------

s32 xmemfile::GetFileLength( void )
{
    return( m_EOF );
}

//------------------------------------------------------------------------------

s32 xmemfile::GetNumBlocks( void )
{
    return( m_Block.getCount() );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// OTHER FUNCTIONS
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static xfile_ram_device s_RamDevice;

void x_FileSystemAddRamDevice( void )
{
    x_FileSystemAddDevice( s_RamDevice );
}

//==============================================================================
// END
//==============================================================================
//DOM-IGNORE-END
