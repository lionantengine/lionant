//
//  x_random_inline.h
//  xBase
//
//  Created by Tomas Arce on 8/23/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef X_RANDOM_INLINE_H
#define X_RANDOM_INLINE_H

//--------------------------------------------------------------------------
inline
xrandom_small::xrandom_small( void )
{
    // These values are not magical, just the default values Marsaglia used.
    // Any pair of unsigned integers should be fine.
    m_W = 521288629;
    m_Z = 362436069;
}
    
//--------------------------------------------------------------------------
// The random generator seed
inline
void xrandom_small::setSeed64( u64 u )
{
    m_W = u32(u);
    m_Z = u32(u>>32);
    
    // The seed can not be zero in the lower bits
    ASSERT( m_W );
    
    // This one can not be zero neither but we will fix it for the user
    if( m_Z == 0 ) m_Z = 362436069;
}
    
//--------------------------------------------------------------------------
inline
void xrandom_small::setSeed32( u32 u )
{
    m_W = u32(u);
    m_Z = 362436069;
    
    ASSERT( m_W );
}
    
//--------------------------------------------------------------------------
// get the full seed.
inline
u64 xrandom_small::getSeed( void ) const
{
    u64 Seed = (u64(m_W)<<0) |
    (u64(m_Z)<<32);
    
    return Seed;
}
    
//--------------------------------------------------------------------------
// Produce a uniform random sample from the open interval (0, 1).
// The method will not return either end point.
inline
f64 xrandom_small::RandF64( void )
{
    // 0 <= u < 2^32
    u32 u = Rand32();
    // The magic number below is 1/(2^32 + 2).
    // The result is strictly between 0 and 1.
    return (u + 1.0) * 2.328306435454494e-10;
}

//--------------------------------------------------------------------------
// Produce a uniform random sample from the open interval (0, 1).
// The method will not return either end point.
inline
f32 xrandom_small::RandF32( void )
{
    return f32(RandF64());
}

//------------------------------------------------------------------------------
inline
f32 xrandom_small::RandF32( f32 Start, f32 End )
{
    return Start+RandF32()*(End-Start);
}

//------------------------------------------------------------------------------
inline
s32 xrandom_small::Rand32( s32 Start, s32 End )
{
    const s32 L = End-Start;
    return Start+(Rand32()%L);
}

//------------------------------------------------------------------------------
inline
f64 xrandom_small::RandF64( f64 Start, f64 End )
{
    return Start+RandF64()*(End-Start);
}

//--------------------------------------------------------------------------
// This is the heart of the generator.
// It uses George Marsaglia's MWC algorithm to produce an unsigned integer.
// See http://www.bobwheeler.com/statistics/Password/MarsagliaPost.txt
inline
u32 xrandom_small::Rand32( void )
{
    m_Z = 36969 * (m_Z & 65535) + (m_Z >> 16);
    m_W = 18000 * (m_W & 65535) + (m_W >> 16);
    return (m_Z << 16) + m_W;
}

//--------------------------------------------------------------------------
// Get normal (Gaussian) random sample with mean 0 and standard deviation 1
inline
f64 xrandom_small::Normal( void )
{
    // Use Box-Muller algorithm
    f64 u1 = RandF64();
    f64 u2 = RandF64();
    f64 r = x_SqrtF64( -2.0f*x_LogF64(u1) );
    f64 theta = 2.0 * PI*u2;
    return r*x_SinF64(theta);
}

//--------------------------------------------------------------------------
// Get normal (Gaussian) random sample with specified mean and standard deviation
inline
f64 xrandom_small::Normal( f64 mean, f64 standardDeviation)
{
    // Shape must be positive. Received {0}.
    ASSERT(standardDeviation > 0.0 );
    return mean + standardDeviation * Normal();
}

//--------------------------------------------------------------------------
// Get exponential random sample with mean 1
inline
f64 xrandom_small::Exponential()
{
    return x_LogF64( RandF64() );
}

//--------------------------------------------------------------------------
// Get exponential random sample with specified mean
inline
f64 xrandom_small::Exponential( f64 mean )
{
    // Mean must be positive. Received {0}.
    ASSERT(mean > 0.0);
    return mean * Exponential();
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::Gamma( f64 shape, f64 scale)
{
    // Implementation based on "A Simple Method for Generating Gamma Variables"
    // by George Marsaglia and Wai Wan Tsang.  ACM Transactions on Mathematical Software
    // Vol 26, No 3, September 2000, pages 363-372.
    f64 d, c, x, xsquared, v, u;
    
    if (shape >= 1.0)
    {
        d = shape - 1.0/3.0;
        c = 1.0/x_SqrtF64(9.0*d);
        for (;;)
        {
            do
            {
                x = Normal();
                v = 1.0 + c*x;
            }
            while (v <= 0.0);
            v = v*v*v;
            u = RandF64();
            xsquared = x*x;
            if (u < 1.0 -.0331*xsquared*xsquared || x_LogF64(u) < 0.5*xsquared + d*(1.0 - v + x_LogF64(v)))
                return scale*d*v;
        }
    }
    else
    {
        // Shape must be positive. Received {0}
        ASSERT( shape > 0.0 );
        
        f64 g = Gamma(shape+1.0, 1.0);
        f64 w = RandF64();
        return scale*g*x_PowF64(w, 1.0/shape);
    }
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::ChiSquare( f64 degreesOfFreedom )
{
    // A chi squared distribution with n degrees of freedom
    // is a gamma distribution with shape n/2 and scale 2.
    return Gamma(0.5 * degreesOfFreedom, 2.0);
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::InverseGamma( f64 shape, f64 scale )
{
    // If X is gamma(shape, scale) then
    // 1/Y is inverse gamma(shape, 1/scale)
    return 1.0 / Gamma(shape, 1.0 / scale);
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::Weibull( f64 shape, f64 scale)
{
    // Shape and scale parameters must be positive. Recieved shape {0} and scale{1}.
    ASSERT( shape > 0 && scale > 0 );
    
    return scale * x_PowF64(-x_LogF64(RandF64()), 1.0 / shape);
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::Cauchy( f64 median, f64 scale )
{
    // Scale must be positive. Received {0}
    ASSERT( scale > 0 );
    
    f64 p = RandF64();
    
    // Apply inverse of the Cauchy distribution function to a uniform
    return median + scale*x_TanF64( PI*(p - 0.5));
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::StudentT( f64 degreesOfFreedom )
{
    // Degrees of freedom must be positive. Received {0}.
    ASSERT( degreesOfFreedom > 0 );
    
    // See Seminumerical Algorithms by Knuth
    f64 y1 = Normal();
    f64 y2 = ChiSquare(degreesOfFreedom);
    return y1 / x_SqrtF64(y2 / degreesOfFreedom);
}

//--------------------------------------------------------------------------
// The Laplace distribution is also known as the double exponential distribution.
inline
f64 xrandom_small::Laplace( f64 mean, f64 scale)
{
    f64 u = RandF64();
    return (u < 0.5) ?
    mean + scale*x_LogF64(2.0*u) :
    mean - scale*x_LogF64(2*(1-u));
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::LogNormal( f64 mu, f64 sigma )
{
    return x_ExpF64(Normal(mu, sigma));
}

//--------------------------------------------------------------------------
inline
f64 xrandom_small::Beta( f64 a, f64 b)
{
    // Beta parameters must be positive. Received {0} and {1}.
    ASSERT( a > 0 && b > 0 );
    
    // There are more efficient methods for generating beta samples.
    // However such methods are a little more efficient and much more complicated.
    // For an explanation of why the following method works, see
    // http://www.johndcook.com/distribution_chart.html#gamma_beta
    
    f64 u = Gamma(a, 1.0);
    f64 v = Gamma(b, 1.0);
    return u / (u + v);
}

//////////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////////
#endif
