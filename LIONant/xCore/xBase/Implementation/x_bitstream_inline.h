
//------------------------------------------------------------------------------
template< class T > inline
s32 xbitstream::SerializeOutRLE( const T* pData, s32 nSamples )
{
    s32     nBitsPerSample;
    s32     nBitsPerCount;
    s32     MaxRunLength;
    s32     TotalBitsWritten = 0;
    T       MaxSample=pData[0];
    T       MinSample=pData[0];

    //
    // Compute number of bits per sample
    //
    {
        for( s32 i=0; i<nSamples; i++ )
        {
            MaxSample = x_Max(MaxSample,s32(pData[i]));
            MinSample = x_Min(MinSample,s32(pData[i]));
        }
        nBitsPerSample = x_Pow2MinimumRequire( MaxSample - MinSample );
    }
    ASSERT( nBitsPerSample <= sizeof(T)*8 );

    //
    // Compute number of bits per count and max run length
    //
    {
        s32 BestCountBits  = 0;
        s32 BestBitsNeeded = S32_MAX;
        for( s32 i=3; i<8; i++ )
        {
            s32 MaxRunLength = (1<<i)-1;
            s32 nRuns        = 0;
            s32 RunStartI    = 0;
            s32 I            = 0;
            while( I < nSamples )
            {
                // Start a new run if we need to
                if( (I==0) || 
                    (pData[I] != pData[RunStartI]) ||
                    ((I-RunStartI+1) > MaxRunLength) )
                {
                    nRuns++;
                    RunStartI = I;
                }

                I++;
            }

            // Compute number of bits needed. Remember to add on bits for
            // terminator.
            s32 nBitsNeeded = ((nRuns+1) * i) + (nBitsPerSample*nSamples);
            if( nBitsNeeded < BestBitsNeeded )
            {
                BestBitsNeeded = nBitsNeeded;
                BestCountBits  = i;
            }
        }

        MaxRunLength = (1<<BestCountBits)-1;
        nBitsPerCount = BestCountBits;
    }

    //
    // Pack bitcounts into bitstream
    //
    SerializeOut( nBitsPerCount,  3, 8  );   
    SerializeOut( u32(MinSample), sizeof(T)*8 );
    SerializeOut( u32(MaxSample), sizeof(T)*8 );
    
    TotalBitsWritten += x_Pow2MinimumRequire( 8-3 );
    TotalBitsWritten += sizeof(T)*8*2;

    //
    // Pack samples into bitstream
    //
    {
        s32 I = 0;
        //s32 TotalBitsWritten = BS.GetCursor();

        while( I < nSamples )
        {
            // Start a new run
            s32 RunStartI = I;
            s32 RunLength = 0;

            // Determine run length
            while( 1 )
            {
                if( I == nSamples ) break;
                if( pData[I] != pData[RunStartI] ) break;
                if( (I-RunStartI+1) > MaxRunLength) break;
                I++;
                RunLength++;
            }
            ASSERT( RunLength <= MaxRunLength );
            
            // Pack Count
            SerializeOut( RunLength, nBitsPerCount );
            SerializeOut( pData[RunStartI], MinSample, MaxSample );
            TotalBitsWritten += ( nBitsPerCount + nBitsPerSample);
        }

        // Add terminator
        SerializeOut( 0, nBitsPerCount );
        TotalBitsWritten += nBitsPerCount;
    }

    return TotalBitsWritten;
}

//------------------------------------------------------------------------------
template< class T > inline
void xbitstream::SerializeInRLE( T* pData, s32 Count )
{
    u32 nBitsPerCount;
    T   Min, Max;
    u32 C;
    T V;

    //
    // Unpack bitcounts from bitstream
    //
    SerializeIn( nBitsPerCount,  3, 8  );   
    SerializeIn( Min, sizeof(T)*8 );
    SerializeIn( Max, sizeof(T)*8 );
    
    //
    // Loop until all samples are decompressed
    //
    s32 nSamples = 0;
    while( 1 )
    {
        // Read Count
        SerializeIn( C, nBitsPerCount );

        // Increment total samples
        nSamples += C;

        // If we hit terminator break out
        if( C==0 ) break;

        // Read Value and duplicate
        SerializeIn( V, Min, Max );
        while( C-- ) *pData++ = V;
    }

    ASSERT( nSamples == Count );
}
