
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Declare all the different types of properties 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN

//------------------------------------------------------------------------------

template< class U >
struct xproperty_type_decl : public U
{
    x_rtti_class1( xproperty_type_decl, U )          
    xproperty_type_decl( void ) { U::RegisterType( this ); }
};
//DOM-IGNORE-END

//------------------------------------------------------------------------------

// <ALIAS xproperty_type_int>
extern const xproperty_type_decl<xprop_int>         g_PropInt;

// <ALIAS xprop_enum>
extern const xproperty_type_decl<xprop_enum>        g_PropEnum;

// <ALIAS xprop_string>
extern const xproperty_type_decl<xprop_string>      g_PropString;

// <ALIAS xprop_filepath>
extern const xproperty_type_decl<xprop_filepath>    g_PropFilePath;

// <ALIAS xprop_guid>
extern const xproperty_type_decl<xprop_guid>        g_PropGuid;

// <ALIAS xprop_vector3>
extern const xproperty_type_decl<xprop_vector3>     g_PropVector3;

// <ALIAS xprop_float>
extern const xproperty_type_decl<xprop_float>       g_PropFloat;

// <ALIAS xprop_bool>
extern const xproperty_type_decl<xprop_bool>        g_PropBool;

// <ALIAS xprop_color>
extern const xproperty_type_decl<xprop_color>       g_PropColor;

// <ALIAS xprop_date>
extern const xproperty_type_decl<xprop_date>        g_PropDate;

// <ALIAS xprop_date>
extern const xproperty_type_decl<xprop_button>      g_PropButton;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PROPERTY ENUM
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

template<class T> inline
T& xproperty_enum::Append( s32& iNewScope, s32 iScope )
{
    // Allocate the node
    iNewScope = Add( iScope );

    // Set the entry index
    m_lNodes[iNewScope].m_iEntry = m_Entries.GetEndIndex();

    // officially allocate the entry
    return m_Entries.append<T>();
}

//------------------------------------------------------------------------------

template<class T> inline
T& xproperty_enum::GetEntry( s32 Index )
{
    return m_Entries.Get<T>( m_lNodes[Index].m_iEntry );
}

//------------------------------------------------------------------------------

template<class T> inline
const T& xproperty_enum::GetEntry( s32 Index ) const
{
    return m_Entries.Get<T>( m_lNodes[Index].m_iEntry );
}

//------------------------------------------------------------------------------
inline
void xproperty_enum::entry::Set( const char* pName, const xproperty_type* pType, const char* pHelp, u32 Flags )
{
    x_strcpy( &m_Name[0], m_Name.getCount(), pName );
    m_pHelp = pHelp;
    m_Flags = Flags;
    m_pType = pType;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PROPERTY TYPE ENUM
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template<class T> inline
xbool xprop_enum::Query( xproperty_query& I, T& Var, const entry* pList )
{
    s32   Q;
    xbool A;
    if( I.IsUserGet() )
    {
        ASSERT( Var >= 0 );
        ASSERT( Var < 10000 );
        Q=Var;
        A = Query_S32( I, Q, pList );
    }
    else
    {
        A = Query_S32( I, Q, pList );
        if(A) Var = T(Q);
    }
    return A;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PROPERTY BOOL
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#undef xprop_bool_QueryBitfield
#define xprop_bool_QueryBitfield(Q,STRING,VAR)                      \
    if( Q.Var( STRING ) )                                           \
    {                                                               \
        xprop_bool& Flags = xprop_bool::SafeCast( Q.GetType() );    \
                                                                    \
        if( Q.IsUserGet() ) Flags.m_Data = VAR;                     \
        else                VAR          = Flags.m_Data;            \
                                                                    \
        return TRUE;                                                \
    }                                                               \

//------------------------------------------------------------------------------

template<class T> inline
xbool xprop_bool::Query( xproperty_query& I, T& Var )
{
    // cast it right
    xprop_bool& Flags = xprop_bool::SafeCast( I.GetType() ); 

    // Handle the property
    if( I.IsUserGet() ) Flags.m_Data = Var;     
    else                Var          = Flags.m_Data;

    // allow us to put it in an if statement
    return TRUE;
}
