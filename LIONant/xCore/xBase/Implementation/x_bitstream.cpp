//==============================================================================
// This is an implementation of a "range coder".  Whereas the usual
// implementation of an arithmetic coder outputs 1 bit at a time, the
// range coder presented here outputs a byte at a time.  This translates
// into some potential CPU time savings, which can be important in a 
// server environment where you're encoding messages for many clients.
// Also, it makes for slightly simpler code, which I like.
//==============================================================================
// INCLUDES
//==============================================================================
#include "../x_base.h"


//==============================================================================
// FLOATING POINT ENCODER
//==============================================================================
//DOM-IGNORE-BEGIN
//==============================================================================
// LOCALS
//==============================================================================
static const s32 MANTISSA_BITS_32 = 23;
static const u32 EXPONENT_BITS_32 = 8;
static const u32 MANTISSA_MASK_32 = 0x007fffff;
static const u32 EXPONENT_MASK_32 = 0x7f800000;
//static const u32 SIGN_MASK_32     = 0x80000000;
static const s32 EXPONENT_BIAS_32 = 127;
static const s32 SIGN_SHIFT_32    = 31;
static const char EMPTY_CHAR = '\0';

//==============================================================================
// TYPES
//==============================================================================

struct xfloat_encoder  
{
                xfloat_encoder  ( s32 nExponentBits, s32 nMantissaBits );
    s32         GetOutputSize   ( void ) const ;
    u32         Encode          ( f32 F, xbool Rounding ) const;
    f32         Decode          ( u32 Encoded ) const ;

    const s32     m_ExponentBits;
    const s32     m_MantissaBits;
    const s32     m_SignMask;
    const s32     m_MantissaMask;
    const s32     m_ExponentMask;
    const s32     m_ExponentBias;
    const s32     m_SignShift;
    const s32     m_ExponentMin;
    const s32     m_ExponentMax;
};

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

inline 
static u32 U32Ref( f32 F ) 
{
    return *(u32*)&F;
}

//------------------------------------------------------------------------------


xfloat_encoder::xfloat_encoder( s32 nExponentBits, s32 nMantissaBits ) :
    m_ExponentBits( nExponentBits ),
    m_MantissaBits( nMantissaBits ),
    m_ExponentBias( (1 << (nExponentBits - 1)) - 1 ),
    m_SignShift   ( nExponentBits + nMantissaBits ),
    m_SignMask    ( 1 << (nExponentBits + nMantissaBits) ),
    m_ExponentMask( ((1 << nExponentBits) - 1) << nMantissaBits ),
    m_MantissaMask( (1 << nMantissaBits) - 1 ),
    m_ExponentMax ( (1 << (nExponentBits - 1)) - 1),
    m_ExponentMin ( -((1 << (nExponentBits - 1)) - 1) - 1 )
{
    (void)EXPONENT_BITS_32;
    ASSERT( m_ExponentBits <= EXPONENT_BITS_32 );
    ASSERT( m_MantissaBits <= MANTISSA_BITS_32 );
    ASSERT( m_ExponentBits > 0 );
    ASSERT( m_MantissaBits > 0 );
}

//------------------------------------------------------------------------------

s32 xfloat_encoder::GetOutputSize( void ) const
{
    return m_ExponentBits + m_MantissaBits + 1;
}

//------------------------------------------------------------------------------

u32 xfloat_encoder::Encode( f32 f, xbool bRounding ) const
{
    if (f == 0.0f) return 0;     // IEEE 0 is a special case.
    u32 src = U32Ref(f);

    s32 mantissa_shift = (MANTISSA_BITS_32 - m_MantissaBits);

    // Mask out the mantissa, exponent, and sign fields.

    u32 mantissa = (src & MANTISSA_MASK_32);
    s32 exponent = (src & EXPONENT_MASK_32) >> MANTISSA_BITS_32;
    u32 sign     = (src >> SIGN_SHIFT_32);

    // Subtract the IEEE-754 number's exponent bias, then add our own.

    exponent -= EXPONENT_BIAS_32;

    // Round the mantissa, and bump up the exponent if necessary.
    if( bRounding ) 
    {
        s32 rounding_constant = 1 << (mantissa_shift - 1);
        s32 test_bit = 1 << MANTISSA_BITS_32;
        mantissa += rounding_constant;
        if (mantissa & test_bit) 
        {
            mantissa = 0;
            exponent++;  // XXX exponent overflow
        }
    }

    // Shift the mantissa to the right, killing the extra precision.
    mantissa >>= mantissa_shift;

    // Deal with the exponent.
    if (exponent < m_ExponentMin) 
    {
        if (exponent < m_ExponentMin - 1) return 0;
        exponent = m_ExponentMin;
    }

    if (exponent > m_ExponentMax) exponent = m_ExponentMax;

    exponent = (exponent - m_ExponentMin);

    // Put the pieces back together.
    u32 result = (sign << m_SignShift) | (exponent << m_MantissaBits) | (mantissa);

    return result;
}

//------------------------------------------------------------------------------

f32 xfloat_encoder::Decode( u32 src ) const
{
    if (src == 0) return 0.0f;

    // Mask out the mantissa, exponent, and sign fields.

    u32 mantissa = (src & m_MantissaMask);
    s32 exponent = (src & m_ExponentMask) >> m_MantissaBits;
    u32 sign     = (src >> m_SignShift);

    // Subtract our exponent bias, then add IEEE-754's.
    exponent += m_ExponentMin;
    exponent += EXPONENT_BIAS_32;

    // Adjust the mantissa.
    mantissa <<= (MANTISSA_BITS_32 - m_MantissaBits);

    // Assemble the pieces.
    u32 result = (sign << SIGN_SHIFT_32) | (exponent << MANTISSA_BITS_32) | (mantissa);

    return *(f32*)&result;
}


//==============================================================================
// LOCALS
//==============================================================================

// NUM_HIGH_BITS is how many bits we shift out at a time (1 byte).
static const s32 NUM_HIGH_BITS = 8;

// NUM_LOW_BITS is the machine word size, minus 1, minus the number of
// high bits.  The -1 happens because we need to represent numbers from
// 0 to 2**n, not just 0 to 2**n-1.  So the high bit is almost entirely
// unused, but not quite.  We could actually work around this, by making
// the code more complicated; in return we'd get 1 extra bit of precision
// in the math.  But since this is sample code, I have elected not to
// do that.
static const u64 NUM_LOW_BITS = 63 - NUM_HIGH_BITS; //31 - NUM_HIGH_BITS;

// If the range of the coding interval is less than RANGE_TOO_LOW, it's
// time to shift a byte into the output.  This gives us back some precision.
static const u64 RANGE_TOO_LOW = (((u64)1) << NUM_LOW_BITS);

// We start out with the maximum range, since that means maximal math precision.
static const u64 RANGE_INITIAL = ((u64)1) << (NUM_LOW_BITS + NUM_HIGH_BITS);
static const u64 OVERFLOW_MASK = ((u64)1) << (NUM_LOW_BITS + NUM_HIGH_BITS);

static const u64 MAX_BIT_BEFORE_SPLIT = NUM_LOW_BITS;
static const u64 BEFORE_SPLIT_MASK    = ((u64(1)<<MAX_BIT_BEFORE_SPLIT)-1);

//DOM-IGNORE-END
//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

xbitstream::xbitstream( void ) 
{
    setMode( TRUE );
}

//------------------------------------------------------------------------------

xbitstream::~xbitstream( void )
{

}

//------------------------------------------------------------------------------

void xbitstream::setMode( xbool IsPacking ) 
{
    m_bPack  = IsPacking;
    m_Low    = 0;
    m_Range  = m_bPack?RANGE_INITIAL : u64(1) << (NUM_LOW_BITS + NUM_HIGH_BITS);
    m_Code   = 0;
    m_Cursor = 0;
    m_nBytes = 0;
    m_xBuffer.Destroy();
}

//------------------------------------------------------------------------------

void xbitstream::Serialize( char* pString, s32 MaxChars )  
{  
    ASSERT( pString );
    ASSERT( MaxChars >= 2 );

    if( m_bPack )   
    { 
        s32 i;
        for( i=0; pString[i]; i++ )
        { 
            ASSERT( i < (MaxChars-1) );
            Pack32( (u8)pString[i], 0, 0xff );
        } 
        Pack32( (u8)pString[i], 0, 0xff ); 
        ASSERT( i < MaxChars );
    }
    else
    {
        for( s32 i=0; (pString[i] = (char)Unpack32( 0, 0xff )) != EMPTY_CHAR; i++ ) 
        {
            ASSERT( i < (MaxChars-1) );
        }
    } 
}

//------------------------------------------------------------------------------
// has_anything_been_packed() checks to see if we are still satisfying
// the initial conditions set by reset().  If so, nobody ever put anything
// into this coder.  (I use this in networking situations, where I run
// through a bunch of code that might pack data or might not; then at the
// end I check whether there's actually anything to send).
//------------------------------------------------------------------------------
xbool xbitstream::HasAnythingBeenPacked() 
{
    if( m_Range  != RANGE_INITIAL ) return TRUE;
    if( m_Low    != 0 ) return TRUE;
    if( m_nBytes != 0 ) return TRUE;
    if( m_Cursor != 0 ) return TRUE;    

    return FALSE;
}

//------------------------------------------------------------------------------
// renormalize() is the routine that maintains math precision.  It
// checks to see whether 'range' is too low; if so, it shifts some
// data into the output, then scales up the coding interval.
//------------------------------------------------------------------------------
void xbitstream::PackRenormalize( void ) 
{
    ASSERT(m_Range > 0);

    while( m_Range <= RANGE_TOO_LOW ) 
    {
        ASSERT((m_Range >> NUM_LOW_BITS) <= 1);
        ASSERT(m_Range > 0);

        u32 Output = m_Low >> NUM_LOW_BITS;
        PackOutputByte( Output );

        m_Range <<= 8;
        m_Low   <<= 8;
        m_Low    &= (OVERFLOW_MASK - 1);
    }
}

//------------------------------------------------------------------------------

void xbitstream::setupInitialBufferSize( s32 ByteCount )
{
    m_xBuffer.Alloc( ByteCount );
}

//------------------------------------------------------------------------------
// output_byte() puts a byte into the output buffer.  Normally
// you'd expect the parameter to be between 0 and 255; however,
// we allow it to have an extra overflow bit on top, so that it
// can be as big as 511.  If it's got an overflow bit, we perform
// a carry into earlier data.  Putting the overflow checking
// here simplifies the rest of the code.
//------------------------------------------------------------------------------
void xbitstream::PackOutputByte( u32 Value ) 
{
    ASSERT(Value < 512);

    if( Value >= 256 ) 
    {
        PackCarry();
        Value -= 256;
    }

    ASSERT(Value < 256);

    //
    // Grow then we have to
    //
    if( m_Cursor >= m_xBuffer.getCount() )
    {
        if( m_nBytes == 0 )
        {
            m_xBuffer.Alloc( 1024 );
        }
        else
        {
            m_xBuffer.Resize( m_xBuffer.getCount() + m_xBuffer.getCount()/2 );
        }
    }

    m_xBuffer[m_Cursor] = Value;
    m_Cursor++;

    // Set this for consistency
    m_nBytes = m_Cursor;
}

//------------------------------------------------------------------------------
// When it's time to carry, it means we need to add 1 to the
// most recent byte we had output.  Usually that is all we
// need to do; but if that byte is already 255, the carry
// will make it 0, and create another carry bit, which
// we need to propagate one byte earlier in the output. 
// You might imagine this continuing for many output bytes,
// but such a circumstance would be extremely rare.
//
// This carry stuff is necessary because to maintain precision
// we need to output results whenever the range becomes small;
// but if the range happens to straddle 0.5, then we don't
// know whether the most significant bit will end up being
// 1 or 0.  But we need to shift it out anyway.  The carry
// mechanism is just a way of fixing that when we resolve
// which side of 0.5 we are on.
//------------------------------------------------------------------------------
void xbitstream::PackCarry( void ) 
{
    s32 Cursor = m_Cursor;

    while( --Cursor >= 0 ) 
    {
        u32 Value = m_xBuffer[Cursor];

        if( Value == 255 ) 
        {
            m_xBuffer[Cursor] = 0;
        } 
        else 
        {
            m_xBuffer[Cursor] = Value + 1;
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Play nice with all integer numbers
//------------------------------------------------------------------------------
void xbitstream::PackFlags( u32 Flags, s32 MaxBits )
{
    if( MaxBits == 0 )
        return;

//    ASSERT( (Flags >> (u32)MaxBits) == 0  );
    ASSERT( MaxBits> 0 );
    ASSERT( MaxBits < 33 );

    u64 FlagsMask = (u64(1)<<MaxBits)-1;
    SysPack( Flags&FlagsMask, FlagsMask );
}

//------------------------------------------------------------------------------
// Play nice with all integer numbers
//------------------------------------------------------------------------------
void xbitstream::PackFlags( u64 Flags, s32 MaxBits )
{
    if( MaxBits == 0 )
        return;

    if( MaxBits <= MAX_BIT_BEFORE_SPLIT )
    {
        u64 FlagsMask = (u64(1)<<MaxBits)-1;
        SysPack( Flags&FlagsMask, FlagsMask );    
    }
    else
    {
        ASSERT( MaxBits <= 64 );
        SysPack( Flags&BEFORE_SPLIT_MASK, BEFORE_SPLIT_MASK ); 
        SysPack( Flags>>MAX_BIT_BEFORE_SPLIT, (1<<(MaxBits-MAX_BIT_BEFORE_SPLIT))-1 ); 
    }

}


//------------------------------------------------------------------------------

void xbitstream::PackFloat( f32 Value, f32 Min, f32 Max, s32 nBits )
{
    ASSERT( Max > Min );
    ASSERT( nBits > 0 );

    Value -= Min;
    f32 Range = Max-Min;
    s32 Scalar = X_BIT
        (nBits)-1;
    u32 N = (u32)(((Value/Range) * (f32)Scalar) + 0.5f);

    SysPack( N, Scalar+1 );
}

//------------------------------------------------------------------------------

void xbitstream::PackFloat( f32 Value, xbool bRound, s32 nExponentBits, s32 nMantissaBits )
{
    xfloat_encoder Packer( nExponentBits, nMantissaBits );

    // Compress the number
    u32 Packed = Packer.Encode( Value, bRound );
    s32 Limit  = 1<<(Packer.GetOutputSize()+1);
    SysPack( Packed, Limit );
}

//------------------------------------------------------------------------------
// Play nice with all integer numbers
//------------------------------------------------------------------------------
void xbitstream::Pack32( s32 Value, s32 Min, s32 Max )
{
    ASSERT( Max > Min );
    ASSERT( Value <= Max );
    ASSERT( Value >= Min );

    Value -= Min;
    Max   -= Min;

    SysPack( Value, Max );
}

//------------------------------------------------------------------------------
// Play nice with all integer numbers
//------------------------------------------------------------------------------
void xbitstream::Pack64( s64 Value, s64 Min, s64 Max )
{
    ASSERT( Max > 0 );
    ASSERT( Max > Min );
    ASSERT( Value <= Max );
    ASSERT( Value >= Min );

    Value -= Min;
    Max   -= Min;

    if( Max <= BEFORE_SPLIT_MASK )
        SysPack( Value, Max );
    else
    {
        SysPack( Value&BEFORE_SPLIT_MASK, BEFORE_SPLIT_MASK );
        SysPack( Value >> MAX_BIT_BEFORE_SPLIT, Max >> MAX_BIT_BEFORE_SPLIT );
    }
        
}

//------------------------------------------------------------------------------
// When it's time to put a new value into the encoder, we call pack().
// This just does the math explained in the article.
//------------------------------------------------------------------------------
void xbitstream::SysPack( u64 Value, s64 Limit ) 
{
    // Allow for values to be equal to their limits
    Limit++;

    ASSERT( Value >= 0 );
    ASSERT( Value <= u64(Limit) );
  //  ASSERT( Limit <= S32_MAX );


    PackRenormalize();

    ASSERT( m_Range / Limit ); 
    m_Range /= Limit;
    m_Low   += Value * m_Range;

    // Don't overflow out of machine word!
    ASSERT( m_Low >= (m_Low - Value * m_Range) );  
}

//------------------------------------------------------------------------------
// This seems tricky, but it's correct... we want to not output
// more information than we need, which means we need to output
// enough bits to yield a number n, where low <= n <= high.
// The most economical way to do this is output the MSB's of
// (low + range - 1) until we output a bit that differs from 'low'.
//
// But since we're outputting whole bytes at a time here, we want
// to detect a byte that differs from 'low'.  This is the same as
// saying, output up to and including the first byte for which
// (range - 1) is nonzero.  The renormalize() call ensures that that
// byte becomes the top byte.  Then we just output that top byte.
// Unless of course 'low' is 0, in which case we get something
// >= low just by doing nothing.
//
// Khalid Sayood's book "Introduction to Data Compression, 2nd Edition" 
// gives  an expression for computing how many bits you need ideally
// (section 4.4.1).
// But it's hard to implement this expression in a robust way,
// and it also doesn't account for the inherent leakage due to
// fixed precision math.  Thus our method here.
//------------------------------------------------------------------------------
void xbitstream::PackFlush( void ) 
{
    PackRenormalize();

    if( m_Low == 0 ) return;

    u64 High = m_Low + m_Range - 1;

    PackOutputByte( High >> NUM_LOW_BITS );
    ASSERT((High >> NUM_LOW_BITS) > (m_Low >> NUM_LOW_BITS));
}

//------------------------------------------------------------------------------
// get_result() is just a wrapper to give us our buffer.
//------------------------------------------------------------------------------
void xbitstream::getResult( xptr<xbyte>& xBuffer, s32& Length ) 
{
    PackFlush();

    xBuffer = m_xBuffer;
    Length  = m_Cursor;
}

//------------------------------------------------------------------------------
// Much like the Encoder's version of renormalize, we are just trying
// to duplicate the action of the encoder here.
//------------------------------------------------------------------------------
void xbitstream::UnpackRenormalize( void ) 
{
    while( m_Range <= RANGE_TOO_LOW ) 
    {
        ASSERT((m_Range >> NUM_LOW_BITS) <= 1);
        ASSERT(m_Range > 0);

        m_Range <<= 8;
        m_Low   <<= 8;
        m_Code  <<= 8;
        m_Low    &= (OVERFLOW_MASK - 1);
        m_Code   &= (OVERFLOW_MASK - 1);

        if( m_Code < m_Low ) 
        {
            // Riddle me this.
            m_Code |= OVERFLOW_MASK;  
        }

        m_Code |= UnpackNextByte();
    }
}

//------------------------------------------------------------------------------
// input_next_byte() pulls the next byte out of the buffer and
// hands it to us.  This code looks more complicated than it
// ought to be; the reason is that we're shifting in 8 bits
// at a time, but those 8 bits don't land exactly on a byte
// boundary.  They're shifted by a bit, as a consequence of the
// fact that our 'range' is only 31 bits.  We might shrink
// 'range' down to 24 bits for the decoder here, but that would
// cause the math to come out differently than on the encoder,
// which ruins the situation.
//
// The modification mentioned earlier that allows 'range' to be
// 32 bits in both the encoder and the decoder would solve this
// problem.  So I'll think about doing that for next month's version.
//------------------------------------------------------------------------------
u32 xbitstream::UnpackNextByte( void ) 
{
    u32 FirstPart, SecondPart;

    if((m_Cursor == 0) || (m_Cursor > m_nBytes)) 
    {
        FirstPart = 0;
    } 
    else 
    {
        ASSERT(m_Cursor - 1 < m_nBytes) ;
        FirstPart = m_xBuffer[m_Cursor - 1] & 1;
    }

    if (m_Cursor >= m_nBytes) 
    {
        SecondPart = 0;
    } 
    else 
    {
        SecondPart = m_xBuffer[m_Cursor] & ~1;
    }

    // @Robustness: This assertion is loose; should figure out exactly
    // what it ought to be.
   // ASSERT( m_Cursor <= m_nBytes + (8) );

    m_Cursor++;
    return (FirstPart << 7) + (SecondPart >> 1);
}

//------------------------------------------------------------------------------

f32 xbitstream::UnpackFloat( f32 Min, f32 Max, s32 nBits )
{
    ASSERT( nBits > 0 );
    ASSERT( Min < Max );

    f32 Range = Max-Min;
    s32 Scalar = X_BIT(nBits)-1;

    // Get the compress data
    u32 Data = u32(SysUnpack( Scalar+1 ));

    f32 Value = (((f32)Data)/Scalar)*Range + Min;
    ASSERT( (Value>=Min) && (Value<=Max) );

    return Value;
}

//------------------------------------------------------------------------------

f32 xbitstream::UnpackFloat( s32 nExponentBits, s32 nMantissaBits )
{
    xfloat_encoder Packer( nExponentBits, nMantissaBits );

    // Get the compress data
    u32 Data = u32(SysUnpack( u64(1)<<(Packer.GetOutputSize()+1) ));

    // decompress the number and return
    return Packer.Decode( Data );
}

//------------------------------------------------------------------------------

s32 xbitstream::Unpack32( s32 Min, s32 Max )
{
    ASSERT( Max > Min );
    ASSERT( Max <= S32_MAX );
    ASSERT( Min >= S32_MIN );

    s32 Count = s32(SysUnpack( (u32)(Max - Min) ));

    Count = Count + Min;
    ASSERT( Count >= Min );
    ASSERT( Count <= Max );

    return Count;
}

//------------------------------------------------------------------------------

s64 xbitstream::Unpack64( s64 Min, s64 Max )
{
    ASSERT( Max > Min );

    const s64 TotalRange = Max - Min;

    if( TotalRange <= BEFORE_SPLIT_MASK )
    {
        s64 Count = SysUnpack( TotalRange );

        Count = Count - Min;
        ASSERT( Count >= Min );
        ASSERT( Count <= Max );
    
        return Count;
    }

    u64 Value1 = Unpack64( 0, BEFORE_SPLIT_MASK );
    u64 Value2 = Unpack64( 0, Max >> MAX_BIT_BEFORE_SPLIT );

    return Value1 | (Value2<<MAX_BIT_BEFORE_SPLIT);
}

//------------------------------------------------------------------------------

u32 xbitstream::UnpackFlags32( s32 MaxBits )
{
    if( MaxBits == 0 )
        return 0;

    ASSERT( MaxBits> 0 );
    ASSERT( MaxBits <= 32 );

    u64 Result;
    Result  = SysUnpack( (u64(1)<<MaxBits)-1 );

    return u32(Result);
}

//------------------------------------------------------------------------------

u64 xbitstream::UnpackFlags64( s32 MaxBits )
{
    if( MaxBits == 0 )
        return 0;

    if( MaxBits <= MAX_BIT_BEFORE_SPLIT )
        return SysUnpack( (u64(1)<<MaxBits)-1 );

    u64 UnpackLow   = SysUnpack( BEFORE_SPLIT_MASK );
    u64 UnpackHeigh = SysUnpack( (1<<(MaxBits-MAX_BIT_BEFORE_SPLIT))-1 )<<MAX_BIT_BEFORE_SPLIT;
    return UnpackLow | UnpackHeigh; 
}

//------------------------------------------------------------------------------
// unpack() is just the entry point that gives us back a value.
//------------------------------------------------------------------------------
u64 xbitstream::SysUnpack( u64 Limit ) 
{
    // Allow values to include the limit as well
    Limit++;

    ASSERT( Limit > 0 );
  //  ASSERT( Limit <= S32_MAX );

    UnpackRenormalize();

    m_Range /= Limit;

    u64 Value = (m_Code - m_Low) / m_Range;
    ASSERT(Value <= Limit);

    m_Low += Value * m_Range;

    return Value;
}

//------------------------------------------------------------------------------
// set_data() is called with the data to decode, when we want to
// start decoding.  We copy the data into an internal buffer, and
// then we also shift the first 4 bytes of that buffer into the
// variable 'code' which represents the part of the number we
// are actively decoding.
//------------------------------------------------------------------------------
void xbitstream::setPackData( const xptr<xbyte>& xBuffer, s32 Length ) 
{
    ASSERT( m_bPack == FALSE );

    m_xBuffer = xBuffer;
    m_nBytes  = Length;

    s32 i;
    for( i = 0; i < 8; i++ ) 
    {
        m_Code <<= 8;
        m_Code |= UnpackNextByte();
    }
}

//------------------------------------------------------------------------------
// might_there_be_any_data_left() is another function I use to
// help me out when dealing with garbage packets; I wouldn't
// worry about it too much!
//------------------------------------------------------------------------------
xbool xbitstream::HasAnyUnpackDataLeft( void ) 
{
    if( m_Cursor < m_nBytes + 4) return TRUE;
    return FALSE;
}

//------------------------------------------------------------------------------

void xbitstream::PackQuaternion( const xquaternion& Q )
{
	f32     a = 0;
    f32     b = 0;
    f32     c = 0;

    const f32 w = Q.m_W;
    const f32 x = Q.m_X;
	const f32 y = Q.m_Y;
	const f32 z = Q.m_Z;

	#ifdef DEBUG
	const f32 epsilon = 0.0001f;
	const f32 length_squared = w*w + x*x + y*y + z*z;
	ASSERT( length_squared >= 1.0f - epsilon && length_squared <= 1.0f + epsilon );
	#endif

	const f32 abs_w = x_Abs( w );
	const f32 abs_x = x_Abs( x );
	const f32 abs_y = x_Abs( y );
	const f32 abs_z = x_Abs( z );

	u32 Largest = 0;
	f32 LargestValue = abs_x;

	if ( abs_y > LargestValue )
	{
		Largest = 1;
		LargestValue = abs_y;
	}

	if ( abs_z > LargestValue )
	{
		Largest = 2;
		LargestValue = abs_z;
	}

	if ( abs_w > LargestValue )
	{
		Largest = 3;
	}

	switch ( Largest )
	{
		case 0:
			if ( x >= 0 )
			{
				a = y;
				b = z;
				c = w;
			}
			else
			{
				a = -y;
				b = -z;
				c = -w;
			}
			break;

		case 1:
			if ( y >= 0 )
			{
				a = x;
				b = z;
				c = w;
			}
			else
			{
				a = -x;
				b = -z;
				c = -w;
			}
			break;

		case 2:
			if ( z >= 0 )
			{
				a = x;
				b = y;
				c = w;
			}
			else
			{
				a = -x;
				b = -y;
				c = -w;
			}
			break;

		case 3:
			if ( w >= 0 )
			{
				a = x;
				b = y;
				c = z;
			}
			else
			{
				a = -x;
				b = -y;
				c = -z;
			}
			break;

		default:
			ASSERT( false );
	}

	const f32 minimum = - 1.0f / 1.414214f;		// note: 1.0f / sqrt(2)
	const f32 maximum = + 1.0f / 1.414214f;

	const f32 Normal_a = ( a - minimum ) / ( maximum - minimum ); 
	const f32 Normal_b = ( b - minimum ) / ( maximum - minimum );
	const f32 Normal_c = ( c - minimum ) / ( maximum - minimum );

	u32 integer_a = (u32)x_Floor( Normal_a * 1024.0f + 0.5f );
	u32 integer_b = (u32)x_Floor( Normal_b * 1024.0f + 0.5f );
	u32 integer_c = (u32)x_Floor( Normal_c * 1024.0f + 0.5f );

	u32 CompressedOrientation = ( Largest << 30 ) | ( integer_a << 20 ) | ( integer_b << 10 ) | integer_c;

    PackFlags( CompressedOrientation, 32 );
}

//------------------------------------------------------------------------------

void xbitstream::UnpackQuaternion( xquaternion& Q )
{
    const u32 CompressedOrientation = UnpackFlags32( 32 );
	const u32 Largest   = CompressedOrientation >> 30;
	const u32 integer_a = ( CompressedOrientation >> 20 ) & ( (1<<10) - 1 );
	const u32 integer_b = ( CompressedOrientation >> 10 ) & ( (1<<10) - 1 );
	const u32 integer_c = ( CompressedOrientation >>  0 ) & ( (1<<10) - 1 );

	const f32 minimum = - 1.0f / 1.414214f;		// note: 1.0f / sqrt(2)
	const f32 maximum = + 1.0f / 1.414214f;

	const f32 a = integer_a / 1024.0f * ( maximum - minimum ) + minimum;
	const f32 b = integer_b / 1024.0f * ( maximum - minimum ) + minimum;
	const f32 c = integer_c / 1024.0f * ( maximum - minimum ) + minimum;

	switch( Largest )
	{
		case 0:
		{
			// (?) y z w
			Q.m_Y = a;
			Q.m_Z = b;
			Q.m_W = c;
			Q.m_Z = x_Sqrt( 1 - Q.m_Y*Q.m_Y - Q.m_Z*Q.m_Z - Q.m_W*Q.m_W );
		}
		break;

		case 1:
		{
			// x (?) z w
			Q.m_X = a;
			Q.m_Z = b;
			Q.m_W = c;
			Q.m_Y = x_Sqrt( 1 - Q.m_X*Q.m_X - Q.m_Z*Q.m_Z - Q.m_W*Q.m_W ); 
		}
		break;

		case 2:
		{
			// x y (?) w
			Q.m_X = a;
			Q.m_Y = b;
			Q.m_W = c;
			Q.m_Z = x_Sqrt( 1 - Q.m_X*Q.m_X - Q.m_Y*Q.m_Y - Q.m_W*Q.m_W ); 
		}
		break;

		case 3:
		{
			// x y z (?)
			Q.m_X = a;
			Q.m_Y = b;
			Q.m_Z = c;
			Q.m_W = x_Sqrt( 1 - Q.m_X*Q.m_X - Q.m_Y*Q.m_Y - Q.m_Z*Q.m_Z ); 
		}
		break;

		default:
		{
			ASSERT( 0 );
			Q.m_W = 1.0f;
			Q.m_X = 0.0f;
			Q.m_Y = 0.0f;
			Q.m_Z = 0.0f;
		}
	}

	Q.Normalize();
}
