//===============================================================================
// INCLUDES
//===============================================================================
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "../x_target.h"
#ifdef TARGET_PC
    #include <process.h>
#elif defined TARGET_IOS || TARGET_ANDROID

#endif

#include "../x_Base.h"

#pragma warning(disable : 4996)  // warning C4996: 'getenv' was declared deprecated

//===============================================================================
// DEFINES
//===============================================================================

#define BITS_USERID     20 // 5 4bit characters
#define BITS_SEQUENCE   10 // 2^10 = 1024 objects used within 1 second
#define BITS_PROCESSID   6 // 2^6  = 64 unique processes generating guids
#define BITS_SECONDS    28 // 2^28 = 268,435,456 = 8.51 years starting from Jan 01 2006

//===============================================================================
// VARS
//===============================================================================

// Note that all the sizes are prime numbers
// Note that the size of the hash table are computed so that there is an average
// of one entry per node. So a 1:1 ratio.
s32 xguid::s_HashTableSize[] = 
{
      1009,     2003,     3001,     4001,     5003,     6007,     7001,     8009,
      9001,    10007,    11003,    12007,    13001,    14009,    15013,    16001,
     17011,    18013,    19001,    20011,    21001,    22003,    23003,    24001,
     25013,    26003,    27011,    28001,    29009,    30011,    31013,    32003,
     33013,    34019,    35023,    36007,    37003,    38011,    39019,    40009,
     41011,    42013,    43003,    44017,    45007,    46021,    47017,    48017,
     49003,    50021,    51001,    52009,    53003,    54001,    55001,    56003,
     57037,    58013,    59009,    60013,    61001,    62003,    63029,    64007,
     65003,    66029,    67003,    68023,    69001,    70001,    71011,    72019,
     73009,    74017,    75011,    76001,    77003,    78007,    79031,    80021,
     81001,    82003,    83003,    84011,    85009,    86011,    87011,    88001,
     89003,    90001,    91009,    92003,    93001,    94007,    95003,    96001,
     97001,    98009,    99013,   100003,   101009,   102001,   103001,   104003,
    105019,   106013,   107021,   108007,   109001,   110017,   111029,   112019,
    113011,   114001,   115001,   116009,   117017,   118033,   119027,   120011,
    121001,   122011,   123001,   124001,   125003,   126001,   127031,   128021,
    129001,   130003,   131009,   132001,   133013,   134033,   135007,   136013,
    137029,   138007,   139021,   140009,   141023,   142007,   143053,   144013,
    145007,   146009,   147011,   148013,   149011,   150001,   151007,   152003,
    153001,   154001,   155003,   156007,   157007,   158003,   159013,   160001,
    161009,   162007,   163003,   164011,   165001,   166013,   167009,   168013,
    169003,   170003,   171007,   172001,   173021,   174007,   175003,   176017,
    177007,   178001,   179021,   180001,   181001,   182009,   183023,   184003,
    185021,   186007,   187003,   188011,   189011,   190027,   191021,   192007,
    193003,   194003,   195023,   196003,   197003,   198013,   199021,   200003,
    201007,   202001,   203011,   204007,   205019,   206009,   207013,   208001,
    209021,   210011,   211007,   212029,   213019,   214003,   215051,   216023,
    217001,   218003,   219001,   220009,   221021,   222007,   223007,   224011,
    225023,   226001,   227011,   228013,   229003,   230003,   231001,   232003,
    233021,   234007,   235003,   236017,   237011,   238001,   239017,   240007,
    241013,   242009,   243011,   244003,   245023,   246011,   247001,   248021
};

static const u8 s_CharTable[] = 
//'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P' 'Q' 'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z'
{  0,  5,  9, 10,  1, 11,  7,  8,  2,  7, 12, 13,  6,  6,  3, 14,  3, 14, 15, 15,  4,  5,  4,  8,  2,  9 };

// WARNING: Only insert names at the very end of the list or it will cause all the names already out there
//          to be bad. Note that the end of the list is express via a NULL.
static const char* s_pUserNames[] = 
{
    "John Doe",
    "tomas.arce",
    "pepe.romirez",
    "sandin.raliperz",
    NULL
};

static x_qt_counter s_Sequence;
static x_qt_counter s_LastSecond;

//===============================================================================
// FUNCTIONS
//===============================================================================

//-------------------------------------------------------------------------------

static u32 GetGuidSeconds( void )
{
    time_t Seconds;
    ::time(&Seconds);

    // Subtract Jan 1st 2014 from Jan 1st 1970
    Seconds -= (365 * (2014 - 1970) * 24 * 60 * 60 );

    // If this ever happens then this means that we must
    // reset the time above from 2004 to what ever year this is.
    ASSERT( (Seconds & ((1<<BITS_SECONDS)-1)) == Seconds );

    return (u32)Seconds;
}

//-------------------------------------------------------------------------------

static u64 BuildUserNameID( void )
{
    static xbool s_bInit            = FALSE;
    static u64   s_UserNameID       = 0;

    // Initialize the name id part of the show
    if( s_bInit == FALSE )
    {
        char UserName[256]; 
        s32  i;

        // Okay we should consider ourselfs initialize at this point
        s_bInit = TRUE;

        // try to get the user name from the system
        if( getenv("USERNAME") )
        {
            x_strcpy( UserName, 256, getenv("USERNAME") );

            // Try to find the user name from a known list of users
            for( i=0; s_pUserNames[i]; i++ )
            {
                if( x_stricmp( UserName, s_pUserNames[i] ) == 0 )
                {
                    // enter err code and add the name index = 12bits
                    s_UserNameID = 0x11 | (i<<12);
                    break;
                }
            }

            // If we didn't find the user then create a unique id for him
            if( s_UserNameID == 0 )
            {
                // Create a unique name for this unkown user
                for( i=0; i<5 && UserName[i]; i++ )
                {
                    s32 C = x_toupper( UserName[i] );

                    // if the letter is out of bounds then make 'E' the default one
                    if( C < 'A' || C > 'Z' )
                    {
                        s_UserNameID = (s_UserNameID<<4) | s_CharTable[ 4 ];
                    }
                    else
                    {
                        s_UserNameID = (s_UserNameID<<4) | s_CharTable[ C - 'A' ];
                    }
                }
                
                // just add some more unique stuff
                if( i < 5 )
                {
                    s_UserNameID = (s_UserNameID<<4) | x_strlen( UserName );
                }
            }
        }
        else
        {
            // if there is not way to get the user name from the system then make him john doe
            s_UserNameID = 0x11;
        }

        // This sequence (AAAA?) is not accepted as a valid user id so make him john doe
        // The reson is that we use this sequence to determine whether this guid was made 
        // in the editor or ingame.
        if( (s_UserNameID&0xFF) == 0 )
        {
            s_UserNameID = 0x11;
        }

        s_UserNameID = s_UserNameID & ((1<<BITS_USERID)-1);
    }

    return s_UserNameID;
}

//-------------------------------------------------------------------------------

static u64 BuildProcessID( void )
{
    u64 ProcessID = xthread::FindThreadID();
        ProcessID = ProcessID ^ (ProcessID >> BITS_PROCESSID);
        ProcessID = ProcessID & ((1<<BITS_PROCESSID)-1);
    return ProcessID;
}

//-------------------------------------------------------------------------------

static u64 BuildSequenceID( void )
{   
    s32 Local = s_Sequence.Inc();

    // if there are more than 1024 entries in a second wait
    if( Local > 1023 )
    {
        // Wait untill we reach the next second
        while( s_LastSecond.get() == GetGuidSeconds() );

        do
        {
            s32 ClockLocal = s_LastSecond.get();
            s32 ClockNew   = GetGuidSeconds();

            // Keep everyone looping so that we can have a chance to zero
            if( s_Sequence.get() < 1023 )
            {
                // before we exit we take our unique sequence
                Local = s_LastSecond.Inc();  
                break;
            }

            // Someone may have reset the clock but not zero out the sequence
            if( ClockLocal == ClockNew )
                continue;

            // If I can set the second I can reset the requence
            if( s_LastSecond.set( ClockLocal, ClockNew ) )
            {
                // Make sure someone reset this to zero
                s_Sequence.Zero();
                Local = 0;
                break;
            }

        } while(1);
    }

    return Local;
}

//-------------------------------------------------------------------------------

static u64 BuildTimeID( void )
{   
    u32 Seconds      = GetGuidSeconds();
    s32 LocalSeconds = s_LastSecond.get();
     
    if( LocalSeconds != Seconds )
    {
        s_Sequence.Zero();
        do
        {
            s32 Local    = s_LastSecond.get();
            u32 New      = GetGuidSeconds();
            
            if( Local == New )
            {
                Seconds = New;
                break;
            }
        
            if( s_LastSecond.set( Local, New ) )
            {
                Seconds = New;
                break;
            }

        } while(1);
    }

    Seconds = Seconds & ((1<<BITS_SECONDS)-1);
    return Seconds;
}

//-------------------------------------------------------------------------------
 
static u64 GenerateNewGuid( void )
{
    u64 UserID     = BuildUserNameID();
    u64 ProcessID  = BuildProcessID();
    u64 TimeID     = BuildTimeID();
    u64 SequenceID = BuildSequenceID();
    u64 Final      = 0;

    // Make sure this is done in the correct order (last entries first)
    Final = (Final<<0)              | TimeID;
    Final = (Final<<BITS_PROCESSID) | ProcessID;
    Final = (Final<<BITS_SEQUENCE)  | SequenceID;
    Final = (Final<<BITS_USERID)    | UserID;

    // Debug dump
    if( 0 )
    {
        xguid Guid( Final );
        u64 dFinal      = Final;
//         u64 dUserID     = dFinal&((1<<BITS_USERID)-1);    dFinal = (dFinal >> BITS_USERID);
//         u64 dSequenceID = dFinal&((1<<BITS_SEQUENCE)-1);  dFinal = (dFinal >> BITS_SEQUENCE);
//         u64 dProcessID  = dFinal&((1<<BITS_PROCESSID)-1); dFinal = (dFinal >> BITS_PROCESSID);
//         u64 dTimeID     = dFinal&((1<<BITS_SECONDS)-1);   dFinal = (dFinal >> BITS_SECONDS);

        ASSERT( (dFinal&((1<<BITS_USERID)-1))      == UserID     );  dFinal = (dFinal >> BITS_USERID);
        ASSERT( (dFinal&((1<<BITS_SEQUENCE)-1))    == SequenceID     );  dFinal = (dFinal >> BITS_SEQUENCE);
        ASSERT( (dFinal&((1<<BITS_PROCESSID)-1))   == ProcessID  );  dFinal = (dFinal >> BITS_PROCESSID);
        ASSERT( (dFinal&((1<<BITS_SECONDS)-1))     == TimeID );  dFinal = (dFinal >> BITS_SECONDS);

        xstring String;
        String = Guid.GetHexString(  );
        x_LogMessage( "xguid::debug:", "UserID:%d ProcessID:%d TimeID:%d SequenceID:%d GUID:%s", 
                       (s32)UserID, (s32)ProcessID, (s32)TimeID, (s32)SequenceID, (const char*)String ); 
    }

    return Final;
}

//-------------------------------------------------------------------------------

void xguid::ResetValue( void )
{ 
    m_Guid = GenerateNewGuid(); 
}

//-------------------------------------------------------------------------------

void xguid::SetFromHexString( const char* pStrGuid )
{
    ASSERT( pStrGuid );
    ASSERT( pStrGuid[0] );

    m_Guid = 0;
    for( s32 i=0; pStrGuid[i]; i++ )
    {
        if( pStrGuid[i] == ':')
            continue;

        u32 d = 0;
        if( pStrGuid[i] > '9' ) d = (pStrGuid[i] - 'A') + 10;
        else                    d = (pStrGuid[i] - '0');

        ASSERT( d >= 0x0 );
        ASSERT( d <= 0xf );

        m_Guid = (m_Guid << 4) | (d & 0xF);
    }
}

//-------------------------------------------------------------------------------

xstring xguid::GetAlphaString( void ) const
{
    xsafe_array<char,16>    Buffer;
    s32                     Len = x_dtoa( m_Guid, Buffer, 32, 10+26 );
    s32                     NewLen = 4*3 + 2;

    for( ; NewLen >= 0; --NewLen, Len-- )
    {
        if( Len < 0 ) Buffer[NewLen] = '0';
        else          Buffer[NewLen] = Buffer[Len];
        
        if( (NewLen%5) ==  0 )
        {
            if(NewLen) Buffer[--NewLen] = '_';
        }
    }

    xstring String;
    String.Copy( Buffer ); 
    return String;
}

//-------------------------------------------------------------------------------

void xguid::SetFromAlphaString ( const char* pStrGuid )
{
    m_Guid = x_atod64( pStrGuid, 10+26 );
}
