// x_va_list.cpp - x base variable argument
#include "../x_base.h"


const x_va	x_va::sEmpty;


x_va::x_va(const xstring& inStr)
:mType(TYPE_PCTCHAR)
{
    ASSERT(0);
	//*(const char**)mArg = inStr.c_str();
}

x_va::x_va(const xstring_tmp& inStr)
:mType(TYPE_PCTCHAR)
{
    ASSERT(0);
	//*(const char**)mArg = inStr.c_str();
}

s8			x_va::convertToInt8() const
{
	u32 i = convertToUInt32();
	return (s8)i;
}

u8			x_va::convertToUInt8() const
{
	u32 i = convertToUInt32();
	return (u8)i;
}

s16			x_va::convertToInt16() const
{
	u32 i = convertToUInt32();
	return (s16)i;
}

u16			x_va::convertToUInt16() const
{
	u32 i = convertToUInt32();
	return (u16)i;
}

s32			x_va::convertToInt32() const
{
	u32 i = convertToUInt32();
	return (s32)i;
}

u32			x_va::convertToUInt32() const
{
	u32 i = 0;
	switch (mType)
	{
		case TYPE_BOOLEAN:
		case TYPE_UINT:
		case TYPE_INT:		{ i = (u32)(*(u32*)mArg); } break;
		case TYPE_UINT32:
		case TYPE_INT32:	{ i = (u32)(*(u32*)mArg); } break;

		case TYPE_UINT8:
		case TYPE_INT8:		{ i = (u32)(*(u8*)mArg);  } break;
		case TYPE_UINT16:
		case TYPE_INT16:	{ i = (u32)(*(u16*)mArg); } break;
		case TYPE_UINT64:
		case TYPE_INT64:	{ i = (u32)(*(u64*)mArg); } break;
		case TYPE_FLOAT32:	{ i = (u32)(*(f32*)mArg); } break;
		case TYPE_FLOAT64:	{ i = (u32)(*(f64*)mArg); } break;
		case TYPE_PCTCHAR:
		default:			break; // Fall through
	};

	return i;
}

s64			x_va::convertToInt64() const
{
	u64 i = convertToUInt64();
	return (s64)i;
}

u64			x_va::convertToUInt64() const
{
	u64 i = 0;
	switch (mType)
	{
		case TYPE_BOOLEAN:
		case TYPE_UINT:
		case TYPE_INT:		{ i = (u64)(*(u32*)mArg);  } break;
		case TYPE_UINT32:
		case TYPE_INT32:	{ i = (u64)(*(u32*)mArg);  } break;

		case TYPE_UINT8:
		case TYPE_INT8:		{ i = (u64)(*(u8*)mArg);   } break;
		case TYPE_UINT16:
		case TYPE_INT16:	{ i = (u64)(*(u16*)mArg);  } break;
		case TYPE_UINT64:
		case TYPE_INT64:	{ i = (u64)(*(u64*)mArg);  } break;
		case TYPE_FLOAT32:	{ i = (u64)(*(f32*)mArg);  } break;
		case TYPE_FLOAT64:	{ i = (u64)(*(f64*)mArg);  } break;
		case TYPE_PCTCHAR:
		default:			break; // Fall through
	};

	return i;
}

f32			x_va::convertToFloat() const
{
	f32 i = 0.0f;
	switch (mType)
	{
		case TYPE_BOOLEAN:
		case TYPE_UINT:
		case TYPE_INT:		{ i = (f32)(*(u32*)mArg); } break;
		case TYPE_UINT32:
		case TYPE_INT32:	{ i = (f32)(*(u32*)mArg); } break;

		case TYPE_UINT8:
		case TYPE_INT8:		{ i = (f32)(*(u8*)mArg);  } break;
		case TYPE_UINT16:
		case TYPE_INT16:	{ i = (f32)(*(u16*)mArg); } break;
		case TYPE_UINT64:
		case TYPE_INT64:	{ i = (f32)(*(u64*)mArg); } break;

		case TYPE_FLOAT32:	{ i = (f32)(*(f32*)mArg); } break;
		case TYPE_FLOAT64:	{ i = (f32)(*(f64*)mArg); } break;
		case TYPE_PCTCHAR:
		default:			break; // Fall through
	};

	return i;
}

f64			x_va::convertToDouble() const
{
	f64 i = 0.0;
	switch (mType)
	{
		case TYPE_BOOLEAN:
		case TYPE_UINT:
		case TYPE_INT:		{ i = (f64)(*(u32*)mArg); } break;
		case TYPE_UINT32:
		case TYPE_INT32:	{ i = (f64)(*(u32*)mArg); } break;

		case TYPE_UINT8:
		case TYPE_INT8:		{ i = (f64)(*(u8*)mArg);  } break;
		case TYPE_UINT16:
		case TYPE_INT16:	{ i = (f64)(*(u16*)mArg); } break;
		case TYPE_UINT64:
		case TYPE_INT64:	{ i = (f64)(*(u64*)mArg); } break;

		case TYPE_FLOAT32:	{ i = (f64)(*(f32*)mArg); } break;
		case TYPE_FLOAT64:	{ i = (f64)(*(f64*)mArg); } break;

		case TYPE_PCTCHAR:
		default:			break; // Fall through
	};

	return i;
}

bool			x_va::convertToBool() const
{
	u32 i = convertToUInt32();
	return i != 0;
}

const char*			x_va::convertToCharPointer() const
{
	switch (mType)
	{
		case TYPE_PCTCHAR:
			{
				const char* p = *(const char**)mArg; 
				return p;
			}

		default:			break; // Fall through
	};

	return "?";
}

x_va_r	x_va_r::sEmpty;

x_va_r&					x_va_r::operator =(const xstring& str)
{
	switch (mType)
	{
	case TYPE_BOOLEAN:	*((xbool*)mRef) = str.IsEmpty() ? TRUE : FALSE; break;
	case TYPE_UINT:		*((u32*)mRef) = 0; break;
	case TYPE_INT:		*((s32*)mRef) = 0; break;
	case TYPE_UINT32:	*((u32*)mRef) = 0; break;
	case TYPE_INT32:	*((s32*)mRef) = 0; break;
	case TYPE_UINT8:	*((u8*)mRef) = 0; break;
	case TYPE_INT8:		*((s8*)mRef) = 0; break;
	case TYPE_UINT16:	*((u16*)mRef) = 0; break;
	case TYPE_INT16:	*((s16*)mRef) = 0; break;
	case TYPE_UINT64:	*((u64*)mRef) = 0; break;
	case TYPE_INT64:	*((s64*)mRef) = 0; break;
	case TYPE_FLOAT32:	*((f32*)mRef) = 0.0f; break;
	case TYPE_FLOAT64:	*((f64*)mRef) = 0.0; break;
	case TYPE_XSTRING:  *((xstring*)mRef) = str; break;
	default:			break;
	};

	return *this;
}


x_va_r&					x_va_r::operator=(s8 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = rhs; break;
		case TYPE_INT:		*((s32*)mRef) = rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = rhs; break;
		case TYPE_UINT8:	*((u8*)mRef) = rhs; break;
		case TYPE_INT8:		*((s8*)mRef) = rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(u8 rhs)
{
	return operator=((s8)rhs);
}

x_va_r&					x_va_r::operator=(s16 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = rhs; break;
		case TYPE_INT:		*((s32*)mRef) = rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = rhs; break;
		case TYPE_UINT8:	*((u8*)mRef) = (u8)rhs; break;
		case TYPE_INT8:		*((s8*)mRef) = (s8)rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(u16 rhs)
{
	return operator=((s16)rhs);
}

x_va_r&					x_va_r::operator=(s32 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = rhs; break;
		case TYPE_INT:		*((s32*)mRef) = rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = rhs; break;
		case TYPE_UINT8:	*((u8*)mRef) = (u8)rhs; break;
		case TYPE_INT8:		*((s8*)mRef) = (s8)rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = (u16)rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = (s16)rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(u32 rhs)
{
	return operator=((s32)rhs);
}

x_va_r&					x_va_r::operator=(s64 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT:		*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT8:	*((u8*)mRef) = (u8)rhs; break;
		case TYPE_INT8:		*((s8*)mRef) = (s8)rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = (u16)rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = (s16)rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(u64 rhs)
{
	return operator=((s64)rhs);
}

x_va_r&					x_va_r::operator=(f32 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT:		*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT8:	*(( u8*)mRef) = ( u8)rhs; break;
		case TYPE_INT8:		*(( s8*)mRef) = ( s8)rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = (u16)rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = (s16)rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = (u64)rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = (s64)rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(f64 rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = rhs!=0 ? TRUE : FALSE; break;
		case TYPE_UINT:		*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT:		*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT32:	*((u32*)mRef) = (u32)rhs; break;
		case TYPE_INT32:	*((s32*)mRef) = (s32)rhs; break;
		case TYPE_UINT8:	*(( u8*)mRef) = ( u8)rhs; break;
		case TYPE_INT8:		*(( s8*)mRef) = ( s8)rhs; break;
		case TYPE_UINT16:	*((u16*)mRef) = (u16)rhs; break;
		case TYPE_INT16:	*((s16*)mRef) = (s16)rhs; break;
		case TYPE_UINT64:	*((u64*)mRef) = (u64)rhs; break;
		case TYPE_INT64:	*((s64*)mRef) = (s64)rhs; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = (f32)rhs; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = (f64)rhs; break;
		default:			break; // Fall through
	};

	return *this;
}

x_va_r&					x_va_r::operator=(bool rhs)
{
	switch (mType)
	{
		case TYPE_BOOLEAN:	*((xbool*)mRef) = xbool(rhs); break;
		case TYPE_UINT:		*((u32*)mRef) = rhs ? 1 : 0; break;
		case TYPE_INT:		*((s32*)mRef) = rhs ? 1 : 0; break;
		case TYPE_UINT32:	*((u32*)mRef) = rhs ? 1 : 0; break;
		case TYPE_INT32:	*((s32*)mRef) = rhs ? 1 : 0; break;
		case TYPE_UINT8:	*((u8*)mRef) = rhs ? 1 : 0; break;
		case TYPE_INT8:		*((s8*)mRef) = rhs ? 1 : 0; break;
		case TYPE_UINT16:	*((u16*)mRef) = rhs ? 1 : 0; break;
		case TYPE_INT16:	*((s16*)mRef) = rhs ? 1 : 0; break;
		case TYPE_UINT64:	*((u64*)mRef) = rhs ? 1 : 0; break;
		case TYPE_INT64:	*((s64*)mRef) = rhs ? 1 : 0; break;
		case TYPE_FLOAT32:	*((f32*)mRef) = rhs ? 1.0f : 0.0f; break;
		case TYPE_FLOAT64:	*((f64*)mRef) = rhs ? 1.0 : 0.0; break;
		default:			break; // Fall through
	};

	return *this;
}
