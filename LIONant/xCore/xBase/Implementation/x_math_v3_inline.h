//==============================================================================
// FUNCTIONS
//==============================================================================


//------------------------------------------------------------------------------
inline 
xvector3::xvector3( void )
{
	ASSERT( x_IsAlign(this,16) );
}

//------------------------------------------------------------------------------
inline 
xvector3::xvector3( f32 X, f32 Y, f32 Z )
{
    ASSERT( x_IsAlign(this,16) );
    Set( X, Y, Z );
}

//------------------------------------------------------------------------------
inline 
xvector3::xvector3( xradian Pitch, xradian Yaw )
{
    ASSERT( x_IsAlign(this,16) );
    xvector3d::Set( Pitch, Yaw );
}

//------------------------------------------------------------------------------
inline 
xvector3::xvector3( const xvector3d& V ) 
{
    ASSERT( x_IsAlign(this,16) );
    Set( V.m_X, V.m_Y, V.m_Z );
}


//------------------------------------------------------------------------------
inline 
xvector3::xvector3( const f32 n ) 
{
    ASSERT( x_IsAlign(this,16) );
    Set( n );
}

//------------------------------------------------------------------------------
inline
f32x4& xvector3::GetH( void ) const
{
    ASSERT( x_IsAlign(this,16) );
    return *((f32x4*)this);
}

//------------------------------------------------------------------------------
inline 
xbool xvector3::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y) && x_isValid(m_Z);
}

//------------------------------------------------------------------------------
inline 
xvector3::xvector3( const f32x4& Register ) 
{     
    ASSERT( x_IsAlign(this,16) );
    *((f32x4*)this) = Register;
    //ASSERT( isValid() );
}

//------------------------------------------------------------------------------
inline
f32 xvector3::operator ()( const s32 i ) const
{
    ASSERT( i >= 0 && i <= 3 );
    return ((f32*)&m_X)[i];
}

//------------------------------------------------------------------------------
inline
f32& xvector3::operator ()( const s32 i )
{
    ASSERT( i >= 0 && i <= 3 );
    return ((f32*)&m_X)[i];
}

//------------------------------------------------------------------------------
inline 
void xvector3::Zero( void )
{
#ifdef TARGET_PC
    GetH() = *(f32x4*)&_mm_setzero_ps();
#elif defined(TARGET_360)
    WZYX = _XMVectorZero();
#else
    m_X = m_Y = m_Z = 0;
#endif
    m_W = 1;
}

//------------------------------------------------------------------------------
inline
void xvector3::Identity( void )
{
    f32x4& WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_setzero_ps();
#elif defined(TARGET_360)
    WZYX = _XMVectorZero();
#else
    WZYX.x = 0;
    WZYX.y = 0;
    WZYX.z = 0;
    WZYX.w = 1;
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Set( f32 X, f32 Y, f32 Z )
{
    f32x4& WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_set_ps( 1, Z, Y, X );
#elif defined(TARGET_360)
    WZYX =  XMVectorSet( X, Y, Z, 1 );
#else
    WZYX.x = X;
    WZYX.y = Y;
    WZYX.z = Z;
    WZYX.w = 1;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Set( const f32 n )
{
    f32x4& WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_set1_ps( n );
    m_W = 1;
#elif defined(TARGET_360)
    WZYX =  XMVectorSet( n, n, n, 1 );
#else
    WZYX.x = WZYX.y = WZYX.z = n;
    WZYX.w = 1;
#endif

    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Set( const f32x4& Register )
{
    *((f32x4*)this) = Register;
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector3::GetLength( void ) const
{
    const f32x4& WZYX = *((const f32x4*)this);
#ifdef SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( WZYX, WZYX );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));
          a = _mm_sqrt_ss(a);

    return ((f32*)&a)[0];

#elif defined(TARGET_360)
	f32x4 res = XMVector3Length( WZYX );
	return res.x;

#else
    return x_Sqrt( WZYX.x*WZYX.x + WZYX.y*WZYX.y + WZYX.z*m_Z);
#endif
}

//------------------------------------------------------------------------------
inline
f32 xvector3::GetLengthSquared( void ) const
{
    const f32x4& WZYX = *((const f32x4*)this);
#ifdef SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( WZYX, WZYX );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));

    return ((f32*)&a)[0];
#elif defined(TARGET_360)
	f32x4 res = XMVector3LengthSq( WZYX );
	return res.x;
#else
    return  WZYX.x*WZYX.x + WZYX.y*WZYX.y + WZYX.z*WZYX.z;
#endif
}

//------------------------------------------------------------------------------
inline
f32 xvector3::GetDistanceSquare( const xvector3& V ) const
{
    return ((*this) - V).GetLengthSquared();
}

//------------------------------------------------------------------------------
inline
f32 xvector3::GetDistance( const xvector3& V ) const
{
    return ((*this) - V).GetLength();
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Normalize( void )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    // get len
    f32x4 a = _mm_mul_ps( WZYX, WZYX );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));
          a = _mm_rsqrt_ss(a);

    const f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    WZYX = _mm_mul_ps( WZYX, oneDivLen );
    
#elif defined(TARGET_360)
	WZYX = XMVector3Normalize( WZYX );
#else
    f32 Div = 1.0f/x_Sqrt(WZYX.x*WZYX.x + WZYX.y*WZYX.y + WZYX.z*WZYX.z);
    WZYX.x *= Div;
    WZYX.y *= Div;
    WZYX.z *= Div;
#endif

    ASSERT( isValid() );

    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::NormalizeSafe( void )
{
    f32x4&      WZYX = *((f32x4*)this);
    const f32   Tof  = 0.00001f;
#ifdef SSE2_SUPPORT
    // get len
    f32x4 a = _mm_mul_ps( WZYX, WZYX );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));

    if( ((f32*)&a)[0] < Tof )
    {
        Set( 1, 0, 0 );
        return *this;
    }

                  a = _mm_rsqrt_ss(a);
    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    WZYX = _mm_mul_ps( WZYX, oneDivLen );
#elif defined(TARGET_360)
	// boils down to 1 instruction, so not much waste, really
	f32x4 len_sq = XMVector3LengthSq( WZYX );		

	if( len_sq.x < Tof ) 
    {		
        Set( 1, 0, 0 );
        return *this;
    }
	WZYX = XMVector3Normalize( WZYX );
#else
    f32 Div = WZYX.x*WZYX.x + WZYX.y*WZYX.y + WZYX.z*WZYX.z;
	if( Div< Tof ) 
    {		
        Set( 1, 0, 0 );
        return *this;
    }
    Div = x_InvSqrt( Div );
    WZYX.x *= Div;
    WZYX.y *= Div;
    WZYX.z *= Div;
#endif

    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator += ( const xvector3& V )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_add_ps( WZYX, V.GetH() );
#elif defined(TARGET_360)
    WZYX = XMVectorAdd( WZYX, V.GetH() );
#else
    WZYX.x += V.m_X;
    WZYX.y += V.m_Y;
    WZYX.z += V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator -= ( const xvector3& V )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_sub_ps( WZYX, V.GetH() );
#elif defined(TARGET_360)
	WZYX = XMVectorSub( WZYX, V.GetH() );
#else
    WZYX.x -= V.m_X;
    WZYX.y -= V.m_Y;
    WZYX.z -= V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator *= ( const xvector3& V )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_mul_ps( WZYX, V.GetH() );
#elif defined(TARGET_360)
	WZYX = XMVectorMul( WZYX, V.GetH() );
#else
    WZYX.x *= V.m_X;
    WZYX.y *= V.m_Y;
    WZYX.z *= V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator *= ( f32 Scalar )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_mul_ps( WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
	WZYX = XMVectorScale( WZYX, Scalar);
#else
    WZYX.x *= Scalar;
    WZYX.y *= Scalar;
    WZYX.z *= Scalar;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator /= ( f32 Div )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_div_ps( WZYX, _mm_set1_ps( Div ) );
#elif defined(TARGET_360)
	WZYX = XMVectorScale( WZYX, 1.0f/Div);
#else
    f32 Scalar = 1.0f/Div;
    WZYX.x *= Scalar;
    WZYX.y *= Scalar;
    WZYX.z *= Scalar;
#endif
    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3& V, f32 Div )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
#ifdef SSE2_SUPPORT
    f32x4 res = _mm_div_ps( WZYX, _mm_set1_ps( Div ) );
#elif defined(TARGET_360)
	f32x4 res = XMVectorScale( WZYX, 1.0f/Div);
#else
    f32 Scalar = 1.0f/Div;
    xvector3 res( WZYX.x * Scalar, WZYX.y * Scalar, WZYX.z * Scalar );
#endif
    return res;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V, f32 Scalar )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
#ifdef SSE2_SUPPORT
    f32x4 res = _mm_mul_ps( WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
	f32x4 res =  XMVectorScale( WZYX, Scalar);
#else
    xvector3 res( WZYX.x * Scalar, WZYX.y * Scalar, WZYX.z * Scalar );
#endif
    return res;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( f32 Scale, const xvector3& V )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
#ifdef SSE2_SUPPORT
    f32x4 res = _mm_mul_ps( WZYX, _mm_set1_ps( Scale ) );
#elif defined(TARGET_360)
	f32x4 res =  XMVectorScale( WZYX, Scale);
#else
    xvector3 res( WZYX.x * Scale, WZYX.y * Scale, WZYX.z * Scale );
#endif
    return res;
}


//------------------------------------------------------------------------------
inline
xvector3 operator + ( f32 Scalar, const xvector3& V )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
    
#ifdef SSE2_SUPPORT
    f32x4 res =  _mm_add_ps( WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorAdd( WZYX, Scalar);
#else
    xvector3 res( WZYX.x + Scalar, WZYX.y + Scalar, WZYX.z + Scalar );
#endif
    
    return res;
}

//------------------------------------------------------------------------------
inline
xvector3 operator + ( const xvector3& V, f32 Scalar )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
    
#ifdef SSE2_SUPPORT
    f32x4 res =  _mm_add_ps( WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorAdd( WZYX, Scalar);
#else
    xvector3 res( WZYX.x + Scalar, WZYX.y + Scalar, WZYX.z + Scalar );
#endif
    
    return res;
}

//------------------------------------------------------------------------------
inline
xvector3 operator - ( f32 Scalar, const xvector3& V )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
    
#ifdef SSE2_SUPPORT
    f32x4 res =  _mm_sub_ps( _mm_set1_ps( Scalar ), WZYX );
#elif defined(TARGET_360)
    return XMVectorAdd( Scalar, WZYX );
#else
    xvector3 res( Scalar - WZYX.x, Scalar - WZYX.y, Scalar - WZYX.z );
#endif
    
    return res;
}

//------------------------------------------------------------------------------
inline
xvector3 operator - ( const xvector3& V, f32 Scalar )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
    
#ifdef SSE2_SUPPORT
    f32x4 res =  _mm_sub_ps( WZYX, _mm_set1_ps( Scalar ) );
#elif defined(TARGET_360)
    return XMVectorAdd( WZYX, Scalar);
#else
    xvector3 res( WZYX.x - Scalar, WZYX.y - Scalar, WZYX.z - Scalar );
#endif
    
    return res;
}

//------------------------------------------------------------------------------
inline 
xbool xvector3::operator == ( const xvector3& V ) const
{
         if( x_Abs( V.m_X - m_X) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Y - m_Y) > XFLT_TOL) return FALSE;
    else if( x_Abs( V.m_Z - m_Z) > XFLT_TOL) return FALSE;
    return TRUE;
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::GetMin( const xvector3& V ) const
{
    const f32x4&      WZYX = *((const f32x4*)this);
#ifdef SSE2_SUPPORT
    return _mm_min_ps( WZYX, V.GetH() );
#elif defined(TARGET_360)
    return XMVectorMin( WZYX, V.GetH() )
#else
    return xvector3( x_Min(V.m_X, WZYX.x), x_Min( V.m_Y,WZYX.y), x_Min( V.m_Z,WZYX.z) );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::GetMax( const xvector3& V ) const
{
    const f32x4&      WZYX = *((const f32x4*)this);
#ifdef SSE2_SUPPORT
    return _mm_max_ps( WZYX, V.GetH() );
#elif defined(TARGET_360)
    return XMVectorMax( WZYX, V.GetH() )
#else
    return xvector3( x_Max(V.m_X, WZYX.x), x_Max( V.m_Y,WZYX.y), x_Max( V.m_Z,WZYX.z) );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3& V0, const xvector3& V1 )
{
    const f32x4&      V0_WZYX = *((const f32x4*)&V0);
    const f32x4&      V1_WZYX = *((const f32x4*)&V1);
#ifdef SSE2_SUPPORT
    return _mm_add_ps( V0_WZYX, V1_WZYX);
#elif defined(TARGET_360)
    return XMVectorAdd( V0_WZYX, V1_WZYX)
#else
    return xvector3( V0_WZYX.x + V1_WZYX.x, 
                     V0_WZYX.y + V1_WZYX.y, 
                     V0_WZYX.z + V1_WZYX.z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3d&  V0, const xvector3& V1 )
{
    return V1 + xvector3( V0 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 + xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3d&  V0, const xvector3& V1 )
{
    return xvector3( V0 ) - V1 ;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 - xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3d&  V0, const xvector3& V1 )
{
    return V1 * xvector3( V0 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 * xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3d&  V0, const xvector3& V1 )
{
    return xvector3( V0 ) / V1 ;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 / xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V0, const xvector3& V1 )
{
    const f32x4&      V0_WZYX = *((const f32x4*)&V0);
    const f32x4&      V1_WZYX = *((const f32x4*)&V1);
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( V0_WZYX, V1_WZYX);
#elif defined(TARGET_360)
    return XMVectorSub( V0_WZYX, V1_WZYX)
#else
    return xvector3( V0_WZYX.x - V1_WZYX.x, 
                     V0_WZYX.y - V1_WZYX.y, 
                     V0_WZYX.z - V1_WZYX.z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V0, const xvector3& V1 )
{
    const f32x4&      V0_WZYX = *((const f32x4*)&V0);
    const f32x4&      V1_WZYX = *((const f32x4*)&V1);
#ifdef SSE2_SUPPORT
    return _mm_mul_ps( V0_WZYX, V1_WZYX);
#elif defined(TARGET_360)
    return XMVectorMul( V0_WZYX, V1_WZYX)
#else
    return xvector3( V0_WZYX.x * V1_WZYX.x, 
                     V0_WZYX.y * V1_WZYX.y, 
                     V0_WZYX.z * V1_WZYX.z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3& V0, const xvector3& V1 )
{
    const f32x4&      V0_WZYX = *((const f32x4*)&V0);
    const f32x4&      V1_WZYX = *((const f32x4*)&V1);
#ifdef SSE2_SUPPORT
    return _mm_div_ps( V0_WZYX, V1_WZYX);
#elif defined(TARGET_360)
    return XMVectorMul( V0_WZYX, XMVectorReciprocal( V1_WZYX) );
#else
    return xvector3( V0_WZYX.x / V1_WZYX.x, 
                     V0_WZYX.y / V1_WZYX.y, 
                     V0_WZYX.z / V1_WZYX.z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V )
{
    const f32x4&      WZYX = *((const f32x4*)&V);
#ifdef SSE2_SUPPORT
    return _mm_sub_ps( _mm_setzero_ps(), WZYX );
#elif defined(TARGET_360)
    return XMVectorNegate( WZYX )
#else
    return xvector3( - WZYX.x, 
                     - WZYX.y, 
                     - WZYX.z );
#endif
}

//------------------------------------------------------------------------------
inline 
f32 xvector3::Dot( const xvector3& V ) const
{
#ifdef TARGET_PC_32BIT
    f32 Dest;    
    __asm
    {
        mov         ebx, this
        mov         eax, V
        movaps      xmm0, dword ptr [ebx]
        mulps       xmm0, dword ptr [eax]      
        movaps      xmm1, xmm0                          
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                          
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)    
        addss       xmm0, xmm1                          
        movss       xmmword ptr [Dest], xmm0
    }

    return Dest;
#elif defined SSE2_SUPPORT

    const f32x4&      V1_WZYX = *((f32x4*)this);
    const f32x4&      V2_WZYX = *((const f32x4*)&V);

    const f32x4       A = _mm_mul_ps( V1_WZYX, V2_WZYX );
    const f32x4       B = _mm_add_ss(_mm_shuffle_ps( A, A, _MM_SHUFFLE(0,0,0,0)), _mm_add_ss(_mm_shuffle_ps(A, A, _MM_SHUFFLE(1,1,1,1)), _mm_shuffle_ps(A, A, _MM_SHUFFLE(2,2,2,2))));

    return ((f32*)&B)[0];

#elif defined TARGET_360
    const f32x4&      WZYX = *((const f32x4*)this);
    return XMVector3Dot( WZYX, V.GetH() );
#else
    return m_X * V.m_X + 
           m_Y * V.m_Y +
           m_Z * V.m_Z;
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::Cross( const xvector3& V ) const
{
#ifdef TARGET_PC_32BIT
    // a = v0.y | v0.z | v0.x | xxx
    // b = v1.z | v1.x | v1.y | xxx
    // c = v0.z | v0.x | v0.y | xxx
    // d = v1.y | v1.z | v1.x | xxx
    f32x4 Dest;
    __asm
    {
        mov         ecx, V                     
        mov         edx, this

        movaps      xmm0, dword ptr [edx]
        movaps      xmm2, xmm0
        shufps      xmm0, xmm0, _MM_SHUFFLE(3,0,2,1)    // xmm0: W, X, Z, Y
        shufps      xmm2, xmm2, _MM_SHUFFLE(3,1,0,2)    // xmm2: W, Y, X, Z

        movaps      xmm1, dword ptr [ecx]
        movaps      xmm3, xmm1
        shufps      xmm1, xmm1, _MM_SHUFFLE(3,1,0,2)    // xmm1: W, Y, X, Z
        shufps      xmm3, xmm3, _MM_SHUFFLE(3,0,2,1)    // xmm3: W, X, Z, Y

        mulps       xmm0, xmm1
        mulps       xmm2, xmm3
        subps       xmm0, xmm2

        movaps      xmmword ptr [Dest], xmm0
    }
    return Dest;
#elif defined SSE2_SUPPORT

    static const int X = 0;
    static const int Y = 1;
    static const int Z = 2;
    static const int W = 3;

    const f32x4&      V1_WZYX = *((f32x4*)this);
    const f32x4&      V2_WZYX = *((const f32x4*)&V);

    const f32x4 A = _mm_mul_ps( _mm_shuffle_ps( V1_WZYX, V1_WZYX, _MM_SHUFFLE(W, X, Z, Y)), _mm_shuffle_ps( V2_WZYX, V2_WZYX, _MM_SHUFFLE(W, Y, X, Z)) );
    const f32x4 B = _mm_mul_ps( _mm_shuffle_ps( V1_WZYX, V1_WZYX, _MM_SHUFFLE(W, Y, X, Z)), _mm_shuffle_ps( V2_WZYX, V2_WZYX, _MM_SHUFFLE(W, X, Z, Y)) );

    return xvector3(_mm_sub_ps(A, B));

#elif defined TARGET_360
    const f32x4&      WZYX = *((const f32x4*)this);
    return XMVector3Cross( WZYX, V.GetH() );    
#else
    return xvector3( m_Y * V.m_Z - m_Z * V.m_Y,
                     m_Z * V.m_X - m_X * V.m_Z,
                     m_X * V.m_Y - m_Y * V.m_X );   
#endif
}

//------------------------------------------------------------------------------
inline
xvector3& xvector3::Abs( void )
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    WZYX = _mm_max_ps( WZYX, _mm_sub_ps( _mm_setzero_ps(), WZYX ) );
#elif defined(TARGET_360)
    WZYX = XMVectorAbs( WZYX );
#else
    WZYX.x = x_Abs(WZYX.x);
    WZYX.y = x_Abs(WZYX.y);
    WZYX.z = x_Abs(WZYX.z);
#endif
    ASSERT( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline
xbool xvector3::InRange( const f32 Min, const f32 Max ) const
{
    f32x4&      WZYX = *((f32x4*)this);
#ifdef SSE2_SUPPORT
    f32x4 t1 = _mm_add_ps( _mm_cmplt_ps( WZYX, _mm_set1_ps(Min) ), _mm_cmpgt_ps( WZYX, _mm_set1_ps(Max) ) );    
    return ((f32*)&t1)[0] == 0 && ((f32*)&t1)[1] == 0 && ((f32*)&t1)[2] == 0;
#elif defined(TARGET_360)
    return XMVector3GreaterOrEqual( WZYX, Min.GetH() ) &&  XMVector3LessOrEqual( WZYX, Max.GetH() );
#else
    return x_InRange( WZYX.x, Min, Max ) && 
           x_InRange( WZYX.y, Min, Max ) &&  
           x_InRange( WZYX.z, Min, Max );
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::GetOneOver( void ) const
{
    const f32x4&      WZYX = *((const f32x4*)this);
#ifdef SSE2_SUPPORT
    return _mm_rcp_ps( WZYX );
#elif defined(TARGET_360)
    return XMVectorReciprocal( WZYX );
#else
    return xvector3(1.0f/WZYX.x, 1.0f/WZYX.y, 1.0f/WZYX.z);
#endif
}

//------------------------------------------------------------------------------
inline
f32* xvector3::operator()( void )
{
    return &m_X;
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::Blend( const f32 T, const xvector3& End ) const
{
    return (*this) + T * ( End - (*this) );
}
