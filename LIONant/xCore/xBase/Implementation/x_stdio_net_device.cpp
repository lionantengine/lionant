//==============================================================================
// INCLUDES
//==============================================================================
//
// TODO:
//      * There is not reasons why the seeks,tell,length, etc should go throw the 
//        network all that should be taken care of locally.
//      * This implementation of the net file server/client is really bad.
//        But it is sufficient for now, when the xbase matures a bit more we need
//        to do another pass on this one.
//
//==============================================================================
//DOM-IGNORE-BEGIN
#if 0
#include "../x_base.h"


//==============================================================================
// Functions
//==============================================================================

enum fsp_header
{
    PACKET_REQUEST_BEGING,
    PACKET_OPEN_REQUEST,
    PACKET_CLOSE_REQUEST,
    PACKET_SEEK_REQUEST,
    PACKET_TELL_REQUEST,
    PACKET_READ_REQUEST,
    PACKET_WRITE_REQUEST,
    PACKET_FLUSH_REQUEST,
    PACKET_LENGTH_REQUEST,
    PACKET_ISEOF_REQUEST,
    PACKET_REQUEST_END,

    PACKET_ANSWERS_BEGING,
    PACKET_OPEN_ANSWER,
    PACKET_SEEK_ANSWER,
    PACKET_TELL_ANSWER,
    PACKET_READ_ANSWER,
    PACKET_WRITE_ANSWER,
    PACKET_LENGTH_ANSWER,
    PACKET_ISEOF_ANSWER,
    PACKET_ANSWERS_END,

    ENUM_END = 0xffffffff
};

//------------------------------------------------------------------------------

class file_server : public xthread
{
public:

                        file_server         ( void );
    void                SetNetAddress       ( const xnet_address& NetAddress ) { m_ServerIP = NetAddress; }
    void                IAmServer           ( void ) { m_bIAmServer = TRUE; }
    xbool               IsInited            ( void ) { return m_bInit && (m_bConnectedToServer || m_bIAmServer); }
    xhandle             Open                ( const char* pFileName, u32 Flags );
    void                Close               ( xhandle hFile );
    xbool               Read                ( xhandle hFile , void* pBuffer, s32 Count );
    void                Write               ( xhandle hFile , const void* pBuffer, s32 Count );
    void                Seek                ( xhandle hFile , xfile_device_i::seek_mode Mode, s32 Pos );
    s32                 Tell                ( xhandle hFile );
    void                Flush               ( xhandle hFile );
    s32                 Length              ( xhandle hFile );
    xbool               IsEOF               ( xhandle hFile );
    void                Kill                ( void );

protected:

    struct waiting
    {
        fsp_header  m_Header;           // Header that we are waiting for
        xthread*    m_pThread;          // Thread waiting for the header
        xhandle     m_hFile;            // Handle to the file we are talking about
    };

    struct incoming
    {
        xhandle     m_hFile;            // File handle that we are talking about
        xbitstream  m_BitStream;        // Bitstream coming from the server
    };

protected:

    virtual void        OnRun               ( void );
    void                WaitOnAnswer        ( s32 Header, xhandle hFile );

protected:
    
    xbool               m_bIAmServer;
    xcritical_section   m_CriticalSection;  // Only one thread at a time please
    xbool               m_bInit;            // Are we initialize   
    xbool               m_bConnectedToServer;
    xnet_connection     m_Net;              // Network Layer
    xharray<xfile>      m_File;             // List of files that we are reserving to the client
    xnet_address        m_ServerIP;         // File Server IP
    xhandle             m_ServerID;         // File Server ID
    xbool               m_bSuspended;

    xcritical_section   m_CSectionData;     // Carefull this data below is share across threads. Use this critical section to be safe
    waiting             m_Waiting;          // Thread waiting for answer
    incoming            m_Incoming;         // Incoming data with answers
};

//------------------------------------------------------------------------------

class xfile_net_device : public xfile_device_i
{
public:
                    xfile_net_device ( void ) : xfile_device_i("net:"){m_bInit=FALSE;}

public:

    virtual void*               Open        ( const char* pFileName, u32 Flags );
    virtual void                Close       ( void* pFile );
    virtual xbool               Read        ( void* pFile, void* pBuffer, s32 Count );
    virtual void                Write       ( void* pFile, const void* pBuffer, s32 Count );
    virtual void                Seek        ( void* pFile, xfile_device_i::seek_mode Mode, s32 Pos );
    virtual s32                 Tell        ( void* pFile );
    virtual void                Flush       ( void* pFile );
    virtual s32                 Length      ( void* pFile );
    virtual xbool               IsEOF       ( void* pFile );
    virtual xfile::sync_state   Synchronize ( void* pFile, xbool bBlock ){return xfile::SYNC_COMPLETED; }
    virtual void                AsyncAbort  ( void* pFile ){};
    virtual void                Init        ( const void* pData );
    virtual void                Kill        ( void );

protected:

    void            Init      ( void );


protected:

    file_server         m_FileServer;
    xbool               m_bInit;
};

//------------------------------------------------------------------------------

void xfile_net_device::Init( const void* pData )
{
    const xnet_address* pAddress = (const xnet_address*)pData;
    ASSERT(pAddress);

    // Set the server IP
    m_FileServer.SetNetAddress( *pAddress );
}

//------------------------------------------------------------------------------

void xfile_net_device::Init( void )
{
    //
    // Create a thread for the file server
    //
    m_FileServer.Create( "FileServer", TRUE );
    m_bInit = TRUE;
    
    // Syncronice with the file server
    while( m_FileServer.IsInited() == FALSE )
    {        
        x_Sleep( 5 );
    }
}

//------------------------------------------------------------------------------

void* xfile_net_device::Open( const char* pFileName, u32 Flags )
{
    xbool bVailOut = FALSE;

    //
    // Tell the file server that we are the actual server
    //
    if( x_stricmp( &pFileName[4], "I am the net file server" ) == 0 )
    {
        m_FileServer.IAmServer();
        bVailOut = TRUE;
    }

    //
    // make sure that the system is initialize
    // 
    if( m_bInit == FALSE )
    {
        Init();
    }

    // Get me out of here
    if( bVailOut )
    {
        return NULL;
    }

    xhandle Handle;
    Handle = m_FileServer.Open( pFileName, Flags );
    if( Handle.IsNull() ) return NULL;

    return (void*)(u64)(Handle.m_Handle+1);
}

//------------------------------------------------------------------------------

void xfile_net_device::Close( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    m_FileServer.Close( Handle );        
}

//------------------------------------------------------------------------------

xbool xfile_net_device::Read( void* pFile, void* pBuffer, s32 Count )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    return m_FileServer.Read( Handle, pBuffer, Count );        
}

//------------------------------------------------------------------------------

void xfile_net_device::Write( void* pFile, const void* pBuffer, s32 Count )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    m_FileServer.Write( Handle, pBuffer, Count );        
}

//------------------------------------------------------------------------------

void xfile_net_device::Seek( void* pFile, seek_mode Mode, s32 Pos )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    m_FileServer.Seek( Handle, Mode, Pos );        
}

//------------------------------------------------------------------------------

s32 xfile_net_device::Tell( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    return m_FileServer.Tell( Handle );        
}

//------------------------------------------------------------------------------

void xfile_net_device::Flush( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    m_FileServer.Flush( Handle );        
}

//------------------------------------------------------------------------------

s32 xfile_net_device::Length( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    return m_FileServer.Length( Handle );        
}

//------------------------------------------------------------------------------
xbool xfile_net_device::IsEOF( void* pFile )
{
    const s64 FileID = (s64)pFile;
    xhandle Handle;
    Handle.Set( (s32)FileID-1 );
    return m_FileServer.IsEOF( Handle );        
}

//------------------------------------------------------------------------------

void xfile_net_device::Kill( void )
{
    m_FileServer.Kill();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// FILE SERVER
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

file_server::file_server( void )
{
    m_bInit = FALSE;    
    m_bConnectedToServer = FALSE;
    m_bIAmServer         = FALSE;
    m_ServerIP;         
    m_ServerID.setNull();
}

//------------------------------------------------------------------------------

void file_server::Kill( void )
{
    if( m_bInit )
    {
        m_bInit = FALSE;  
        m_Net.Close();
    }
}

//------------------------------------------------------------------------------

void file_server::OnRun( void )
{
    //
    // Open the connection at some specific port
    //
    m_Net.Open( m_ServerIP.GetPort() );

    // Connect to the server
    //if( !m_bIAmServer )
    {
        m_Net.ConnectToClient( m_ServerIP, m_ServerID );
    }

    // Tell the world we are ready
    m_bInit = TRUE;

    //
    // Lets loop
    //
    while( m_bInit )
    {
        fsp_header Header;

        // update the network connection
        m_Net.Update();

        // Check for new messages
        while( XNET_HEADER_INVALID != (Header = (fsp_header)m_Net.PullNextHeader( FALSE )) )
        {
            xhandle ClientID = m_Net.ReceicedClient();

            //
            // Is the user requesting something?
            //
            if( Header < PACKET_REQUEST_END )
            {
                switch( Header )
                {
                case PACKET_OPEN_REQUEST:
                    {
                        xbitstream              BitStream;
                        u32                     AccFlags;
                        xsafe_array<char,256>   String;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( AccFlags, 0, xfile_device_i::ACC_MASK );
                        BitStream.Serialize( &String[0], String.getCount() );
                        
                        // Add a new file handle
                        xhandle Handle;
                        xfile&  File     =  m_File.append( Handle );

                        // Make the AccFlags into a string 
                        s32                     i=0;
                        xsafe_array<char,32>   Mode;
                        if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_CREATE   ) ) Mode[i++] = 'w'; else Mode[i++] = 'r';
                        if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_WRITE    ) ) Mode[i++] = '+';
                        if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_ASYNC    ) ) Mode[i++] = 'a';
                        if( x_FlagIsOn( AccFlags, xfile_device_i::ACC_COMPRESS ) ) Mode[i++] = 'c';
                        Mode[i++] = 0;

                        xbool   bSuccess = File.Open( &String[0], &Mode[0] );

                        // If file fail to open then remove clear
                        if( bSuccess == FALSE )
                        {
                            m_File.DeleteByHandle( Handle );
                            Handle.setNull();
                        }

                        // Return the handle
                        BitStream.SetMode( TRUE );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        m_Net.SendToClient( ClientID, PACKET_OPEN_ANSWER, BitStream, TRUE ); 

                        break;
                    }
                case PACKET_CLOSE_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );

                        // Exececute command
                        m_File(Handle).Close();
                        m_File.DeleteByHandle( Handle );

                        break;
                    };
                case PACKET_SEEK_REQUEST:
                    {
                        xbitstream      BitStream;
                        s32             Mode;
                        s32             Pos;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( Mode, 0, 3 );
                        BitStream.Serialize( Pos, 32 );

                        // Exececute command
                        switch( Mode )
                        {
                        case xfile_device_i::SKM_ORIGIN:    m_File(Handle).SeekOrigin( Pos );   break;
                        case xfile_device_i::SKM_CURENT:    m_File(Handle).SeekCurrent( Pos );  break;
                        case xfile_device_i::SKM_END:       m_File(Handle).SeekEnd( Pos );      break;
                        default:    ASSERT(0); break;
                        }
                        
                        break;
                    };
                case PACKET_TELL_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );

                        // Exececute command
                        s32 Pos = m_File(Handle).Tell();

                        // Send packet with the position
                        BitStream.SetMode( TRUE );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( Pos, 32 );              
                        m_Net.SendToClient( ClientID, PACKET_TELL_ANSWER, BitStream, TRUE );

                        break;
                    };
                case PACKET_FLUSH_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );

                        // Exececute command
                        m_File(Handle).Flush();

                        break;
                    }
                case PACKET_LENGTH_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );

                        // Exececute command
                        s32 Size = m_File(Handle).GetFileLength();

                        // Send packet with the position
                        BitStream.SetMode( TRUE );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( Size, 32 );              
                        m_Net.SendToClient( ClientID, PACKET_LENGTH_ANSWER, BitStream, TRUE );

                        break;
                    };
                case PACKET_ISEOF_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );

                        // Exececute command
                        xbool bEOF = m_File(Handle).IsEOF();

                        // Send packet with the position
                        BitStream.SetMode( TRUE );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( bEOF, 0, 1 );              
                        m_Net.SendToClient( ClientID, PACKET_ISEOF_ANSWER, BitStream, TRUE );

                        break;
                    }
                case PACKET_READ_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;
                        s32             Count;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( Count, 32 );

                        // Exececute command
                        xptr<xbyte> Data;
                        Data.Alloc( Count );

                        xbool bSuccess = m_File(Handle).ReadRaw( &Data[0], 1, Count );
                        if( bSuccess == FALSE )
                        {
                            s32 Zero = 0;
                            BitStream.SetMode( TRUE );
                            BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                            BitStream.Serialize( Zero, 32 );              
                            BitStream.Serialize( Zero, 32 );

                            m_Net.SendToClient( ClientID, PACKET_READ_ANSWER, BitStream, TRUE );
                            m_Net.FlushAll();
                            break;
                        }

                        // Send all the packets for this request

                        // Send actual data
                        s32 PrevOffset = 0;
                        s32 NextOffset = x_Min( Count, 800 );
                        
                        do
                        {
                            s32 i;

                            BitStream.SetMode( TRUE );
                            BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                            BitStream.Serialize( PrevOffset, 32 );              
                            BitStream.Serialize( NextOffset, 32 );              

                            // Copy data raw
                            for( i=PrevOffset; i<(NextOffset-2); i+=2 )
                            {
                                u16 Data16 = *((u16*)&Data[i]);
                                BitStream.Serialize( Data16, 0, 0xffff );
                            }

                            // copy last two/one bytes a byte at a time
                            for( ; i<NextOffset; i++ )
                            {
                                u8 Data8 = *((u8*)&Data[i]);
                                BitStream.Serialize( Data8, 0, 0xff );
                            }

                            m_Net.SendToClient( ClientID, PACKET_READ_ANSWER, BitStream, TRUE );

                            // Make sure that we are kicking off all the data
                            m_Net.FlushAll();

                            x_DebugMsg( "Sending data: %d %d %d \n", PrevOffset, NextOffset, NextOffset-PrevOffset );

                            PrevOffset = NextOffset;
                            NextOffset = x_Min( Count, NextOffset+800 );

                        } while( PrevOffset != NextOffset );

                        break;
                    }
                case PACKET_WRITE_REQUEST:
                    {
                        xbitstream      BitStream;
                        xhandle         Handle;
                        s32             Count;

                        // Read from network
                        m_Net.ReceivedPacket( Header, BitStream );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        BitStream.Serialize( Count, 32 );
                        
                        // Write to file
                        for( s32 i=0; i<Count; i++ )
                        {
                            u8 Data;
                            BitStream.Serialize( Data, 0, 0xff );
                            m_File(Handle).WriteRaw( &Data, 1, 1 ); 
                        }

                        // Send an answer to make sure we sync up
                        BitStream.SetMode( TRUE );
                        BitStream.Serialize( Handle.m_Handle, 0, 65000 );
                        m_Net.SendToClient( ClientID, PACKET_WRITE_ANSWER, BitStream, TRUE );

                        break;
                    }
                }
            }
            else if( Header < PACKET_ANSWERS_END )
            {
                m_CSectionData.BeginAtomic();

                // Set the data in the bit stream
                m_Net.ReceivedPacket( Header, m_Incoming.m_BitStream );

                // Get the file handle
                m_Incoming.m_BitStream.Serialize( m_Incoming.m_hFile.m_Handle, 0, 65000 );

                // We may have beat the client to the punch
                // lets wait untill this is true
                if( m_Waiting.m_Header != Header )
                {
                    m_CSectionData.EndAtomic();

                    while( m_Waiting.m_Header != Header )
                    {
x_DebugMsg( "Sleeping waiting for a header to match\n" );
                        x_Sleep(5);
                    }

                    m_CSectionData.BeginAtomic();
                }

                // Reactivate the thread
                // and let it take the data it needs
                m_Waiting.m_pThread->Resume();

                // Hack for now
                m_bSuspended = FALSE;

                m_CSectionData.EndAtomic();
            }
            else
            {
                if( Header == XNET_HEADER_ACK )
                {
                    xnet_ack_receipt Ack;
                    m_Net.ReceivedPacket( Header, &Ack, sizeof(Ack) );
                    
                    if( Ack.m_AckHeader == XNET_HEADER_HANDSHAKE )
                    {
                        m_bConnectedToServer = TRUE;
                    }
                }
                else if ( XNET_HEADER_DISCONNECT == Header )
                {
                    m_bConnectedToServer = FALSE;
                    m_Net.SkipSystemPacket( Header );
                }
                else
                {
                    m_Net.SkipSystemPacket( Header );
                }
            }
        }
    }
}

//------------------------------------------------------------------------------

void file_server::WaitOnAnswer( s32 Header, xhandle hFile )
{
    //
    // Submit the entry
    //
    {
        // Protection on
        m_CSectionData.BeginAtomic();

        // Fill up the entry
        m_Waiting.m_pThread   = &x_GetCurrentThread();
        m_Waiting.m_hFile     = hFile;
        m_Waiting.m_Header    = (fsp_header)Header;

        // Protection off
        m_CSectionData.EndAtomic();
    }

    //
    // Okay lets suspend the thread here
    //
    m_bSuspended = TRUE;
    while( m_bSuspended )
    {
        x_Sleep(5);
    }
    //x_GetCurrentThread().Suspend();
}

//------------------------------------------------------------------------------

xhandle file_server::Open( const char* pFileName, u32 Flags )
{
    xscope_csection CriticalSection( m_CriticalSection );

    s32                     i;

    //
    // First lets skip the "net:" should be 4 characters.
    // After that the user may have enter a number of '\' before preceding with 
    // with the actual path which should be an absolute path so something along 
    // these lines will be valid: "net:\\c:\test\test.txt"
    //
    for( i=4; (pFileName[i]=='\\' || pFileName[i]=='/') && pFileName[i]; i++ );
    
    //
    // Lets send this data over
    // 
    xbitstream BitStream;
    BitStream.SetMode( TRUE );
    BitStream.Serialize( Flags, 0, xfile_device_i::ACC_MASK );
    BitStream.Serialize( (char*)&pFileName[i], 255 );

    m_Net.SendToClient( m_ServerID, PACKET_OPEN_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();

    //
    // Lets wait for the answer
    //
    xhandle hFile;
    hFile.setNull();

    // Lets wait untill I get my answer
    WaitOnAnswer( PACKET_OPEN_ANSWER, hFile );

    // Protection
    xscope_csection Protect(m_CSectionData);

    // Get data 
    hFile = m_Incoming.m_hFile;

    // This is the file handle
    return hFile;
}

//------------------------------------------------------------------------------

void file_server::Close( xhandle hFile )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );

    m_Net.SendToClient( m_ServerID, PACKET_CLOSE_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();
}

//------------------------------------------------------------------------------

xbool file_server::Read( xhandle hFile , void* pBuffer, s32 Count )
{
    xscope_csection CriticalSection( m_CriticalSection );


    s32 PrevOffset = 0;
    s32 NextOffset = x_Min( Count, 800 );
    
    do
    {
        s32 i;

        s32 CurCount = NextOffset-PrevOffset;

        //
        // Send a request for data
        //
        {
            xbitstream BitStream;
            BitStream.Serialize( hFile.m_Handle, 0, 65000 );
            BitStream.Serialize( CurCount, 32 );


            m_Net.SendToClient( m_ServerID, PACKET_READ_REQUEST, BitStream, TRUE ); 
            m_Net.FlushAll();
        }

        // Lets wait untill I get my answer
        WaitOnAnswer( PACKET_READ_ANSWER, hFile );

        //
        // Read data
        //
        {
            // Protection
            xscope_csection Protect(m_CSectionData);

            // Get data 
            s32 ReadPrevOffset;
            s32 ReadNextOffset;
            m_Incoming.m_BitStream.Serialize( ReadPrevOffset, 32 );              
            m_Incoming.m_BitStream.Serialize( ReadNextOffset, 32 );              

            // Report if we had an error in the other side
            if( ReadPrevOffset == ReadNextOffset && ReadNextOffset == 0 )
                return FALSE;

            // Set the right ranges
            ReadPrevOffset += PrevOffset;
            ReadNextOffset += PrevOffset;

            // Copy data in u16 increments except for the last two
            for( i=ReadPrevOffset; i<(ReadNextOffset-2); i+=2 )
            {
                u16& Data16 = *((u16*)&(((xbyte*)pBuffer)[i]));
                m_Incoming.m_BitStream.Serialize( Data16, 0, 0xffff );
            }

            // Copy the remaining bytes
            for( ;i<ReadNextOffset; i++ )
            {
                u8& Data8 = *((u8*)&((xbyte*)pBuffer)[i]);
                m_Incoming.m_BitStream.Serialize( Data8, 0, 0xff );
            }

            // Increment the total
            s32 Total = (ReadNextOffset-ReadPrevOffset);

            x_DebugMsg( "Read: %d %d \n", Total, NextOffset );
        }

        //
        // Get ready for the next block
        //
        PrevOffset = NextOffset;
        NextOffset = x_Min( Count, NextOffset+800 );

    } while( PrevOffset != NextOffset );

/*
    //
    // Send Packet
    //
    {
        xbitstream BitStream;
        BitStream.Serialize( hFile.m_Handle, 0, 65000 );
        BitStream.Serialize( Count, 32 );

        m_Net.SendToClient( m_ServerID, PACKET_READ_REQUEST, BitStream, TRUE ); 
        m_Net.FlushAll();
    }

    //
    // Wait for incoming data
    //
    s32 Total=0;
    do 
    {
        s32 i;

        // Lets wait untill I get my answer
        WaitOnAnswer( PACKET_READ_ANSWER, hFile );

        // Protection
        xscope_csection Protect(m_CSectionData);

        // Get data 
        s32 PrevOffset;
        s32 NextOffset;
        m_Incoming.m_BitStream.Serialize( PrevOffset, 32 );              
        m_Incoming.m_BitStream.Serialize( NextOffset, 32 );              

        // Report if we had an error in the other side
        if( PrevOffset == NextOffset && NextOffset == 0 )
            return FALSE;

        // Copy data in u16 increments except for the last two
        for( i=PrevOffset; i<(NextOffset-2); i+=2 )
        {
            u16& Data16 = *((u16*)&(((xbyte*)pBuffer)[i]));
            m_Incoming.m_BitStream.Serialize( Data16, 0, 0xffff );
        }

        // Copy the remaining bytes
        for( ;i<NextOffset; i++ )
        {
            u8& Data8 = *((u8*)&((xbyte*)pBuffer)[i]);
            m_Incoming.m_BitStream.Serialize( Data8, 0, 0xff );
        }

        // Increment the total
        Total += (NextOffset-PrevOffset);

        x_DebugMsg( "Read: %d \n", Total );
    } while( Total != Count );
*/

    return TRUE;
}

//------------------------------------------------------------------------------

void file_server::Write( xhandle hFile , const void* pBuffer, s32 Count )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    s32 i = 0;
    do                
    {
        xbitstream BitStream;

        s32 CurCount = x_Min( 800, Count-i );
        BitStream.Serialize( hFile.m_Handle, 0, 65000 );
        BitStream.Serialize( CurCount, 32 );

        // Serialize in the data
        for( s32 j=0; j<CurCount; j++ )
        {
            u8 Data = ((xbyte*)pBuffer)[i+j];
            BitStream.Serialize( Data, 0, 0xff );
        }

        // Send packet
        m_Net.SendToClient( m_ServerID, PACKET_WRITE_REQUEST, BitStream, TRUE ); 
        m_Net.FlushAll();

        // Lets wait untill I get my answer
        WaitOnAnswer( PACKET_WRITE_ANSWER, hFile );

        // Send another packet with data
        i += CurCount;

    } while( i < Count );
}

//------------------------------------------------------------------------------

void file_server::Seek( xhandle hFile , xfile_device_i::seek_mode Mode, s32 Pos )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );
    BitStream.Serialize( Mode, 0, 3 );
    BitStream.Serialize( Pos, 32 );

    m_Net.SendToClient( m_ServerID, PACKET_SEEK_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();

    // Lets wait until I get my answer
    WaitOnAnswer( PACKET_SEEK_ANSWER, hFile );
}

//------------------------------------------------------------------------------

s32 file_server::Tell( xhandle hFile )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );

    m_Net.SendToClient( m_ServerID, PACKET_TELL_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();

    //
    // Lets wait until I get my answer
    //
    WaitOnAnswer( PACKET_TELL_ANSWER, hFile );

    // Protection
    xscope_csection Protect(m_CSectionData);

    s32 Pos;
    m_Incoming.m_BitStream.Serialize( Pos, 32 );              

    return Pos;
}

//------------------------------------------------------------------------------

void file_server::Flush( xhandle hFile )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );

    m_Net.SendToClient( m_ServerID, PACKET_FLUSH_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();
}

//------------------------------------------------------------------------------

s32 file_server::Length( xhandle hFile )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );

    m_Net.SendToClient( m_ServerID, PACKET_LENGTH_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();

    //
    // Lets wait untill I get my answer
    //
    WaitOnAnswer( PACKET_LENGTH_ANSWER, hFile );

    // Protection
    xscope_csection Protect(m_CSectionData);

    s32 Length;
    m_Incoming.m_BitStream.Serialize( Length, 32 ); 

    return Length;
}

//------------------------------------------------------------------------------

xbool file_server::IsEOF( xhandle hFile )
{
    xscope_csection CriticalSection( m_CriticalSection );

    //
    // Send Packet
    //
    xbitstream BitStream;
    BitStream.Serialize( hFile.m_Handle, 0, 65000 );

    m_Net.SendToClient( m_ServerID, PACKET_ISEOF_REQUEST, BitStream, TRUE ); 
    m_Net.FlushAll();

    //
    // Lets wait untill I get my answer
    //
    WaitOnAnswer( PACKET_ISEOF_ANSWER, hFile );

    // Protection
    xscope_csection Protect(m_CSectionData);

    xbool bEOF;
    m_Incoming.m_BitStream.Serialize( bEOF, 0, 1 );  

    return bEOF;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS (Self registration)
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static xfile_net_device s_NetDevice;
void x_FileSystemAddNetDevice( void )
{
    x_FileSystemAddDevice( s_NetDevice );
}

//==============================================================================
// END
//==============================================================================
//DOM-IGNORE-END
#endif