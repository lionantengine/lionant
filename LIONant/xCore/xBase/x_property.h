//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

/*
 
Pending Propousal:
Change the property query so that it can become smarter by remembering the last scoped used.
 So properties can go from something like this:
 
 [ Properties : ? ]
 { Type:<?>  Name:s											                            Value:<Type> }
 //--------  ------------------------------------------------------------------	        ------------
 .STRING  "Key\Main\Global\MaterialShader"							                    "FirstMaterial--5m5s_3eho_t8gv.material"
 .INT     "Key\Main\Global\UniformVals\Count"                                           4
 .STRING  "Key\Main\Global\UniformVals\Val[0]\UID"	    	                            "DoShadows.BOOL"
 .BOOL    "Key\Main\Global\UniformVals\Val[0]\Bool"                                     0
 .STRING  "Key\Main\Global\UniformVals\Val[1]\UID"                                      "Specular.V1"
 .FLOAT   "Key\Main\Global\UniformVals\Val[1]\Float"                                    5.0
 .STRING  "Key\Main\Global\UniformVals\Val[2]\UID"                                      "Texture01.TEXTURE"
 .STRING  "Key\Main\Global\UniformVals\Val[2]\Resource"                                 "GameData/General/CompilerKeys/--5m5s_3eho_t8gv.texture"
 .STRING  "Key\Main\Global\UniformVals\Val[3]\UID"                                      "UVAnim01.SYSTEM_UV_ANIM"
 .BOOL    "Key\Main\Global\UniformVals\Val[3]\Resource"                                 "GameData/General/Animations/UVAnim001.rawgeom"
 .ENUM    "Key\Main\Global\UniformVals\Val[3]\bLoopType"                                PINGPONG
 .FLOAT   "Key\Main\Global\UniformVals\Val[3]\Speed"                                    1.0
 
 to something like this:
 
 [ Properties : ? ]
 { Type:<?>  Name:s                                                      Value:<Type> }
 //--------  ---------------------------------------------------         ------------
 .STRING  "Key\Main\Global\MaterialShader"                            "GameData/General/CompilerKeys/FirstMaterial--5m5s_3eho_t8gv.material"
 .INT     ".\UniformVals\Count"                                        4
 .STRING  ".\Val[0]\UID"                                               "DoShadows.BOOL"
 .BOOL    ".\Bool"                                                     0
 .STRING  ".1\Val[1]\UID"                                              "Specular.V1"
 .FLOAT   ".\Float"                                                    5.0
 .STRING  ".1\Val[2]\UID"                                              "Texture01.TEXTURE"
 .STRING  ".\Resource"                                                 "GameData/General/CompilerKeys/--5m5s_3eho_t8gv.texture"
 .STRING  ".1\Val[3]\UID"                                              "UVAnim01.SYSTEM_UV_ANIM"
 .BOOL    ".\Resource"                                                 "GameData/General/Animations/UVAnim001.rawgeom"
 .ENUM    ".\bLoopType"                                                PINGPONG
 .FLOAT   ".\Speed"                                                    1.0
 
 .0\ || .\<- use the last top scope
 .1\ <- same as before but with a 1 cd .. commad (so one scope less) 
 .3\ <- 3 cd.. etc
 
 Ideally the property query will remeber which scopes fail vs succed and so next time it
 needs to set the property it does not test strings just a u64 bit trail base on the scope tested.
 This will give a performance boost to the loading as well as make the files smaller.
 Note that must remember the array indexes as well.
 
*/



//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN

class xproperty;
class xproperty_enum;
class xproperty_query;
class xproperty_type;

//------------------------------------------------------------------------------

#define xproperty_friends               \
           class xproperty;             \
    friend class xprop_button;          \
    friend class xprop_date;            \
    friend class xprop_string;          \
    friend class xprop_filepath;        \
    friend class xprop_guid;            \
    friend class xprop_vector2;         \
    friend class xprop_vector3;         \
    friend class xprop_vector4;         \
    friend class xprop_float;           \
    friend class xprop_enum;            \
    friend class xprop_int;             \
    friend class xprop_bool;            \
    friend class xprop_color;           \
    friend class xproperty_type;        \
    friend class xproperty_query;       \
    friend class xproperty_enum;        \
    friend class mfc_BasePropertyGrid

//DOM-IGNORE-END

//------------------------------------------------------------------------------

enum xproperty_flags
{
    PROP_FLAGS_READ_ONLY        =  X_BIT(0),
    PROP_FLAGS_FOLDER           =  X_BIT(1),
    PROP_FLAGS_FORCE_SAVE       =  X_BIT(2),
    PROP_FLAGS_REFRESH_ONCHANGE =  X_BIT(3),
    PROP_FLAGS_NOT_VISIBLE      =  X_BIT(4)
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is the interface for the properties. When ever you want to have a set
//     of properties in your object simply in-heritage this interface. The the two
//     main functions to overwrite are the onPropQuery and onPropEnum. The first
//     function is what enables the system to set or get properties from the object.
//     The second function is that it tells the system what properties the object has.
//
//<P>  The specifics of dealing with a type of property is describe in each of the classes
//     that defines their types. This was done like that so it will be more clear and
//     flexible to the user. Here is a list of this classes:
//<TABLE>
//           Class                    Variable        Description
//           -----------------------  --------------  ---------------------------------------------------
//           xprop_string    g_PropString    Deals with const char* string as well as xstrings
//           xprop_filepath  g_PropFilePath  Deals with paths and file paths
//           xprop_guid      g_PropGuid      Deals with xguid types
//           xprop_vector3   g_PropVector3   Vector3 which uses xvector3d
//           xprop_float     g_PropFloat     Deals with f32 types
//           xprop_enum      g_PropEnum      Deals with enumeration which are done via strings
//           xprop_int       g_PropInt       Deals with s32 types
//           xprop_date      g_PropDate      Deals with dates and such
//           xprop_bool      g_PropBool      Deals with xbool as well as with single bit flags
//           xprop_color     g_PropColor     Deals with xcolor
//</TABLE>
//     
//     onPropQuery has a Query object as a parameter. This query objects(xproperty_query) contains the 
//     information about the specific query. As part of its functionality it deals with
//     the virtual tree that describes the property. Together with the g_Prop... is what
//     make the hold query system possible. Note that at the end of the function you return 
//     either a TRUE to tell the system that you were able to handle the property or a FALSE
//     telling the system that you weren't able to do it.
//     
//<CODE>
//    virtual xbool onPropQuery( xproperty_query& Q )
//    {
//        if( Q.Scope( "Editor" ) )
//        {
//            if( Q.Var( "X" ) && g_PropInt.Query( Q, m_X, 0, 1000 ) )
//                return TRUE;
//
//            if( Q.Var( "Float" ) && g_PropFloat.Query( Q, m_Float, 0, 1000.0f ) )
//                return TRUE;
//
//            if( Q.Var( "Bool" ) && g_PropBool.Query( Q, m_Bool ) )
//                return TRUE;
//
//            return FALSE;
//        }
//
//        return FALSE;
//    }
//</CODE>
//
//     onPropEnum this function is call to gather each of the properties. In this parameter there
//     is an object(xproperty_enum) that collects the property information. This object also deals with 
//     the virtual directory and it knows about different scopes and such. This object 
//     together with the g_Prop... it is what enables to enumerate all the properties.
//
//<CODE>
//    virtual void onPropEnum( xproperty_enum& E )
//    {
//        s32 iScope = E.AddScope( "Editor", "Some Description for the user." );
//
//        g_PropInt.Enum  ( E, iScope, "X",     "Some Description for the user.", 0, xprop_int::BUTTON_SLIDER ); 
//        g_PropFloat.Enum( E, iScope, "Float", "Some Description for the user." );
//        g_PropBool.Enum ( E, iScope, "Bool",  "Some Description for the user." );
//    }
//</CODE>
//
// See Also:
//     xproperty_query xproperty_enum xprop_string xprop_guid xprop_vector3
//     xprop_float xprop_enum xprop_int xprop_bool xprop_color 
//
// TODO:
//     * Add the save/load for non-xtexfiles as well.
//
//------------------------------------------------------------------------------
class xproperty
{
public:

    x_rtti_base( xproperty )

    virtual xbool                   onPropQuery     ( xproperty_query& Query ) const;
    virtual xbool                   onPropQuery     ( xproperty_query& Query )=0;
    virtual void                    onPropEnum      ( xproperty_enum&  Enum  ) const =0;
            const xproperty_type*   getPropertyType ( const char* pPropertyName );
            void                    Save            ( xtextfile& DataFile, const xproperty_enum&  Enum );
            void                    Save            ( xtextfile& DataFile  );
            xbool                   Load            ( xtextfile& DataFile  );
            void                    Save            ( const xstring& FileName, xtextfile::flags Flags = xtextfile::FLAGS_DEFAULTS );
            xbool                   Load            ( const xstring& FileName );
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This is the interface is what allows queries to take place in the property system.
//     This object is pass as part of the xproperty::onPropQuery. Ones inside this function
//     the user can deal with the particular query in multiple ways.
//
//     1. Simple which means that it is not going to do anything special with the query.
//<CODE>
//    virtual xbool onPropQuery( xproperty_query& Q )
//    {
//        if( Q.Scope( "Editor" ) )
//        {
//            if( Q.Var( "X" ) && g_PropInt.Query( Q, m_X, 0, 1000 ) )
//                return TRUE;
//
//            return FALSE;
//        }
//
//        return FALSE;
//    }
//</CODE>
//
//     2. Hierarchical which means that you can allow parent objects to deal with properties queries.
//<CODE>
//    virtual xbool onPropQuery( xproperty_query& Q )
//    {
//        // Here I can overwrite my parent properties if I want. Such lets over-right the health.
//        if( Q.Scope( "ClassParent" ) )
//        {
//            if( Q.Var( "Health" ) && g_PropInt.Query( Q, m_Health, 0, 1000 ) )
//                return TRUE;
//        }
//
//        // Let the parent object deal with queries if it can find them
//        if( class_parent::onPropQuery( Q ) )
//            return TRUE;
//        
//        // Now I deal with my own queries such the parent count not deal with it
//        if( Q.Scope( "Editor" ) )
//        {
//            if( Q.Var( "X" ) && g_PropInt.Query( Q, m_X, 0, 1000 ) )
//                return TRUE;
//
//            return FALSE;
//        }
//
//        return FALSE;
//    }
//</CODE>
//
//     3. Embedded objects. Which means that we can deal with other classes that we may contain as member variables.
//<CODE>
//    virtual xbool onPropQuery( xproperty_query& Q )
//    {
//        // Note that we can overwrite properties of member variables as well
//        if( Q.Scope( "Class1" ) )
//        {
//            if( Q.Var( "ID" ) && g_PropInt.Query( Q, m_MyID, 0, 1000 ) )
//                return TRUE;
//        }
//
//        // Deal with the actual member variable
//        if( m_Class1.onPropQuery( Q ) )
//           return TRUE;
//
//        // Now we can deal with our own
//        if( Q.Scope( "Editor" ) )
//        {
//            if( Q.Var( "X" ) && g_PropInt.Query( Q, m_X, 0, 1000 ) )
//                return TRUE;
//            return FALSE;
//        }
//
//        return FALSE;
//    }
//</CODE>
//       
//     There are two ways to deal with a particular property the direct way such:
//<CODE>
//            if( Q.Var( "ID" ) && g_PropInt.Query( Q, m_MyID, 0, 1000 ) )
//                return TRUE;
//</CODE>
//
//     Or the indirect way such:
//<CODE>
//            if( Q.Var( "ID" ) )
//            {
//                if( Q.IsUserGet() )
//                {
//                    // The user is trying to get a property from us
//                    // Lets get our proper type. Note as it has RTTI protection on it.
//                    xprop_int& Int = xprop_int::SafeCast( Q.GetType() );
//                    m_MyID = Int.GetValue();
//                    ...   
//                }
//                else
//                {
//                    // The user is trying to set a property    
//                    // lets ignore him
//                    return FALSE;
//                }
//                return TRUE;
//            }
//</CODE>
//
//      Finally a property can also be part of an array. Such you may have an array of integer properties.
//      This is how we will deal with that.
//<CODE>
//            if( Q.Var( "ID" ) && g_PropInt.Query( Q, m_MyID[Q.GetArrayIndex( m_MyID.getCount() )], 0, 1000 ) )
//                return TRUE;
//</CODE>
//
//     GetArrayIndex always returns the index of the last array loaded by a Scope. Such in this example:
//     "Something[12]\\Editor\\ID[4]"
//     if ( Query.Scope( "Something[]" ) ) {
//        s32 iSomeThing = Query.GetArrayIndex();       // This case should be 12
//        if ( Query.Scope( "Editor" ) ) {
//           s32 iWhatisNow = Query.GetArrayIndex();    // Still should be 12 as not new indexes have been added
//           if ( Query.Scope( "Editor[]" ) ) {
//              s32 iID = Query.GetArrayIndex();        // This should be 4
//     }  }  }
//
//     The 0 represent the order of index such if you have a property that looks like this:
//     "Editor\ID[4]" it will mean that it is the 0 index in the list. But property can have arrays inside arrays such:
//     "Something[12]\\Editor\\ID[4]" In this case if we wanted the ArrayIndex of ID we will want 
//     #1 one (We always start from zero is C++).
//
//     Properties also have the ability of dealing with older version of previous properties, 
//     and many other type of queries. But we will leave that for the advance course. 
//
// See Also:
//     xproperty xproperty_enum xprop_string xprop_guid xprop_vector3
//     xprop_float xprop_enum xprop_int xprop_bool xprop_color
//
// TODO:
//      * Add the GetValue / SetValue to all property types.
//      * Todo I need to add the popscope
//
//------------------------------------------------------------------------------
class xproperty_query
{
public:
    
    enum flags
    {
        FLAGS_IS_GET          = (1<<0),
        FLAGS_REAL_TIME_QUERY = (1<<1)
    };

public:

    xbool               Var             ( const char* pVar );
    xbool               Scope           ( const char* pScope );
    xbool               IsUserGet       ( void );
    xproperty_type&     GetType         ( void );
    s32                 GetArrayIndex   ( s32 MaxRange = 10000 );
    xbool               IsRealTime      ( void ){ return x_FlagIsOn( m_Flags, FLAGS_REAL_TIME_QUERY); }
    const char*         GetPropertyName ( void ){ return m_pName; }

protected:

    xbool               Get             ( const xproperty& Property, const char* pPropName, xproperty_type& TypeDecl );
    xbool               Set             ( xproperty& Property, const char* pPropName, xproperty_type& TypeDecl, u32 Flags=0 );

protected:

    s32                 m_iScope;           // Index of the current place in the scope
    xproperty_type*     m_pType;            // Pointer to the property
    const char*         m_pName;            // property name
    s32                 m_ArrayOffset;
    u32                 m_Flags;

protected:

    friend xproperty_friends;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This object is use to collect all the properties that an object may have.
//     The object get pass as part of the onPropEnum function from the xproperty interface.
//     To enumerate the properties we need to deal with scopes. Scopes is just a way of 
//     saying subdirectory just like a folder in windows. Properties get organize using
//     this folders thought they don't really exits any where they are virtual.
//
//<P>  A typical example of enumerating properties will look like this:
//<CODE>
//    virtual void onPropEnum( xproperty_enum& E )
//    {
//        s32 iScope = E.AddScope( "Editor", "Some Description for the user." );
//
//        g_PropInt.Enum  ( E, iScope, "X",     "Some Description for the user.", 0, xprop_int::BUTTON_SLIDER ); 
//        g_PropFloat.Enum( E, iScope, "Float", "Some Description for the user." );
//        g_PropBool.Enum ( E, iScope, "Bool",  "Some Description for the user." );
//    }
//</CODE>
//
//    As you can see each property type gives its own enumeration function. By convention all the 
//    functions are name the same. But some of the functions may have more or less parameters depending 
//    of the options that the property may have. In the previous example you can see how the Int
//    property seem to have a way to display the property as a slider. There are many types of options
//    that some properties can have to display themselves in the editor. 
//
//<P> There are other kind of flags that a property can have. This are call xproperty_flags these
//    flags give certain behaviors to properties such the PROP_FLAGS_READ_ONLY. Which makes the 
//    property as constant and not one can set it. You can OR together several flags to fully describe
//    the property behavior. 
//
//<P> Scopes can also have their own properties it doesn't need to be just strings. 
//    For instance you may want to have a scope that is an integer which give you how many
//    array entries are there. You can do that by having a property which is an integer. 
//    You will do that like this:
//
//<CODE>
//       g_PropInt.ScopeEnum( E, iScope, "MyScope", "Some Description for the user.", PROP_FLAGS_READ_ONLY | PROP_FLAGS_FOLDER );
//</CODE>
//
//    Properties can also be arrays. To make an array is simple you just need to enumerate every property individually.
//    For instance creating an array of integers will look like this:
//<CODE>
//    virtual void onPropEnum( xproperty_enum& E )
//    {
//        s32 iScope = E.AddScope( "Editor", "Some Description for the user." );
//
//        for( s32 i=0; i<20; i++ )
//        {
//            s32 iArrayScope = E.AddScope( iScope, xfs("MyArray[%d]",i), "Some Description for the user." );
//            g_PropFloat.Enum( E, iArrayScope, "Float", "Some Description for the user." );
//            g_PropBool.Enum ( E, iArrayScope, "Bool",  "Some Description for the user." );
//        }
//    }
//</CODE>
//
//    This will create set of properties that will look like this:
//<TABLE>
//    Output Properties
//    ------------------------------------------------------------
//    "Editor\\MyArray[0]\\Float"
//    "Editor\\MyArray[0]\\Bool"
//    "Editor\\MyArray[1]\\Float"
//    "Editor\\MyArray[1]\\Bool"
//</TABLE>
//
// See Also:
//     xproperty xproperty_query xprop_string xprop_guid xprop_vector3
//     xprop_float xprop_enum xprop_int xprop_bool xprop_color
//
// TODO: 
//      * Add integer scopes to support arrays better
//      * Add additional flags
//------------------------------------------------------------------------------
class xproperty_enum
{
public:
                            xproperty_enum      ( void );
    s32                     SetRoot             ( s32 iScope = -1 );
    s32                     GetRoot             ( void ) const;
    s32                     GetEntryCount       ( void ) const;
    s32                     GetEntryParent      ( s32 Index );
    xproperty_type&         GetEntryType        ( s32 Index );
    const xproperty_type*   GetPropertyType     ( const char* pProperty ) const;
    void                    GetPropFullName     ( s32 Index, xstring& FullName ) const;
    s32                     FindEntry           ( const char* pFullName ) const;
    s32                     AddScope            ( s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    s32                     AddScope            ( const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    void                    PushScope           ( const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    void                    PopScope            ( void );

protected:

    struct entry : public xcmd_array::cmd_entry
    {
        x_rtti_class1( entry, xcmd_array::cmd_entry )
        void Set( const char* pName, const xproperty_type* pType, const char* pHelp, u32 Flags );
        xsafe_array<char,64>    m_Name;         // xproperty Name
        const xproperty_type*   m_pType;        // xproperty type
        const char*             m_pHelp;        // Pointer to the help for the xproperty in question
        u32                     m_Flags;        // Flags for the property
    };

protected:

    template<class T> T&        Append  ( s32& iNewScope, s32 iScope = -1 );
    template<class T> T&        GetEntry( s32 Index );
    template<class T> const T&  GetEntry( s32 Index ) const;

protected:

    struct node 
    {
        s32                     m_iScope;       // xproperty parent
        s32                     m_iNext;        // Next node in same level. Circular link list.
        s32                     m_iChild;       // This is use for scopes. points to the last node
        s32                     m_iEntry;       // Contains the info about the property such type/name/etc
    };

protected:

    s32             Add             ( s32 iScope );

protected:

    xcmd_array          m_Entries;                  // Type defined entries
    xarray<node>        m_lNodes;                   // Node which describe the hierarchy
    s32                 m_iRoot;                    // Node root
    s32                 m_iStack;                   // Index to the scope Push/Pop stack head
    xsafe_array<s32,16> m_ScopeStack;               // Stack for the Push/Pop scopes  

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
};

//------------------------------------------------------------------------------

class CXTPPropertyGridItem;
class xproperty_type
{
    x_rtti_base( xproperty_type )

public:

    virtual void                    SetString               ( xstring& Arguments ) const =0;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const =0;
    virtual const char*             GetName                 ( void ) const=0;

protected:

    virtual u8                      GetUID                  ( void ) const=0;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile& File, xbool bSave )=0;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave )=0;
    static  void                    RegisterType            ( xproperty_type* Type );
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const = 0;
    virtual const char*             getType                 ( void ) const = 0;

protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const=0;

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
};

//------------------------------------------------------------------------------

class xprop_int : public xproperty_type
{
    x_rtti_class1( xprop_int, xproperty_type )

public:

    enum button_type
    {
        BUTTON_PLAIN,
        BUTTON_SLIDER,
        BUTTON_PADDING  = 0xffffffff
    };

public:

    static xbool   Get                      ( const xproperty& Property, const char* pFullName, s32& Int, s32& Min, s32& Max );
    static xbool   Get                      ( const xproperty& Property, const char* pFullName, s32& Min, s32& Max );
    static xbool   Get                      ( const xproperty& Property, const char* pFullName, s32& Int );
    static xbool   Set                      ( xproperty& Property, const char* pFullName, s32 Int, u32 Flags=0 );
    static s32     Enum                     ( xproperty_enum&  E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, button_type ButtonType = BUTTON_PLAIN );
    static xbool   Query                    ( xproperty_query& I, s32& Var, s32 Min, s32 Max );
    static s32     GetUserValue             ( xproperty_query& I );
    static void    SetUserValue             ( xproperty_query& I, const s32 Value );

protected:

    struct enum_int : public xproperty_enum::entry
    {
        x_rtti_class1( enum_int, xproperty_enum::entry )

        button_type m_ButtonType;
    };

protected:

    virtual u8                      GetUID                  ( void ) const;
    virtual const char*             GetName                 ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "d", ".INT", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".INT"; }

protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    s32     m_Min, m_Max;
    s32     m_Data;

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_enum : public xproperty_type
{
    x_rtti_class1( xprop_enum, xproperty_type )

public:

    enum
    {
        ENTRY_ENUM_END      = -1,
        ENTRY_DISPLAY_END   = -2
    };

    struct entry
    {
        entry       ( void ) = default;
        entry       (s32 EnumID, const xstring& String) : m_EnumID(EnumID), m_EnumName(String) {}
        s32         m_EnumID        = ENTRY_ENUM_END;
        xstring     m_EnumName      = X_STR("");
    };

public:

    static xbool             Get          ( const xproperty& Property, const char* pFullName, xstring& EnumName, const entry*& pEntry );
    static xbool             Set          ( xproperty& Property, const char* pFullName, const xstring& EnumName, u32 Flags=0 );
    static xbool             Set          ( xproperty& Property, const char* pFullName, const char* pEnumName, u32 Flags=0 );
    static xbool             Set          ( xproperty& Property, const char* pFullName, const entry& Entry, u32 Flags=0 );
    static xbool             Get          ( const xproperty& Property, const char* pFullName, xstring& EnumName );
    static xbool             Get          ( const xproperty& Property, const char* pFullName, const entry*& pEntry );
    static void              Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
template<class T> static xbool Query      ( xproperty_query& I, T& Var, const entry* pList );
    static s32               GetUserValue ( xproperty_query& I, const entry* pList );
    static void              SetUserValue ( xproperty_query& I, s32 Var, const entry* pList );

protected:

    struct enum_enum : public xproperty_enum::entry
    {
        x_rtti_class1( enum_enum, xproperty_enum::entry )
    };

protected:

    static xbool                    Query_S32               ( xproperty_query& I, s32& Var, const entry* pList );
    virtual u8                      GetUID                  ( void ) const;
    virtual const char*             GetName                 ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const; 
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "e", ".ENUM", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".ENUM"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xstring         m_Data;
    const entry*    m_pEntry;

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_string : public xproperty_type
{
    x_rtti_class1( xprop_string, xproperty_type )

public:

    enum display_type
    {
        TYPE_PLAIN          = 0,
        TYPE_PASSWORD       = 1,

        TYPE_PADDING        = 0xffffffff
    };

public:

    static xbool    Get          ( const xproperty& Property, const char* pFullName, xstring& Var );
    static xbool    Get          ( const xproperty& Property, const char* pFullName, char* pVar, s32 SizeOfCharBuffer );
    static xbool    Set          ( xproperty& Property, const char* pFullName, const xstring& Var, u32 Flags=0 );
    static xbool    Set          ( xproperty& Property, const char* pFullName, const char* pVar, u32 Flags=0 );
    static void     Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, display_type Type=TYPE_PLAIN );
    static xbool    Query        ( xproperty_query& I, xstring& Var );
    static xbool    Query        ( xproperty_query& I, char* pVar, s32 SizeOfCharBuffer );
    static xbool    QueryROnly   ( xproperty_query& I, const char* pVar );
    static s32      ScopeEnum    ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, display_type Type=TYPE_PLAIN );
    static xstring  GetUserValue ( xproperty_query& I );
    static void     SetUserValue ( xproperty_query& I, const xstring& Value );
    static void     SetUserValue ( xproperty_query& I, const char* pValue );

protected:

    struct enum_string : public xproperty_enum::entry
    {
        x_rtti_class1( enum_string, xproperty_enum::entry )
        display_type    m_Display;
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "s", ".STRING", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".STRING"; }

protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xstring    m_Data;

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_filepath : public xproperty_type
{
    x_rtti_class1( xprop_filepath, xproperty_type )

public:

    enum display_type
    {
        TYPE_FILE           = 2,
        TYPE_PATH           = 3,

        TYPE_PADDING        = 0xffffffff
    };

public:

    static xbool    Get          ( const xproperty& Property, const char* pFullName, xstring& Var );
    static xbool    Get          ( const xproperty& Property, const char* pFullName, char* pVar, s32 SizeOfCharBuffer );
    static xbool    Set          ( xproperty& Property, const char* pFullName, const xstring& Var, u32 Flags=0 );
    static xbool    Set          ( xproperty& Property, const char* pFullName, const char* pVar, u32 Flags=0 );
    static void     Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, const char* pFileType, const char* pDefaultPath, display_type Type=TYPE_FILE, u32 Flags = 0 );
    static xbool    Query        ( xproperty_query& I, xstring& Var );
    static xbool    Query        ( xproperty_query& I, char* pVar, s32 SizeOfCharBuffer );
    static xbool    QueryROnly   ( xproperty_query& I, const char* pVar );
    static s32      ScopeEnum    ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, const char* pFileType, const char* pDefaultPath, display_type Type=TYPE_FILE, u32 Flags = 0 );
    static xstring  GetUserValue ( xproperty_query& I );
    static void     SetUserValue ( xproperty_query& I, const xstring& Value );
    static void     SetUserValue ( xproperty_query& I, const char* pValue );

protected:

    struct enum_filepath : public xproperty_enum::entry
    {
        x_rtti_class1( enum_filepath, xproperty_enum::entry )

        display_type    m_Display;
        const char*     m_pFileType;
        const char*     m_pDefaultPath;
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "s", getType(), GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".FILEPATH"; }

protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xstring    m_Data;

protected:

    friend xproperty_friends;
    friend class mfc_BasePropertyGrid;
    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_guid : public xproperty_type
{
    x_rtti_class1( xprop_guid, xproperty_type )

public:

    static xbool    Get             ( const xproperty& Property, const char* pFullName, xguid& Guid );
    static xbool    Set             ( xproperty& Property, const char* pFullName, xguid  Guid, u32 Flags=0 );
    static void     Enum            ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    static xbool    Query           ( xproperty_query& I, xguid& Var );
    static xguid    GetUserValue    ( xproperty_query& I );
    static void     SetUserValue    ( xproperty_query& I, const xguid Value );

protected:

    struct enum_guid : public xproperty_enum::entry
    {
        x_rtti_class1( enum_guid, xproperty_enum::entry )
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "g", ".GUID", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".GUID"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xguid   m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_vector3 : public xproperty_type
{
    x_rtti_class1( xprop_vector3, xproperty_type )

public:

    enum button_type
    {
        BUTTON_PLAIN,
        BUTTON_SLIDER,
        BUTTON_PADDING  = 0xffffffff
    };

public:
    static xvector3d VECTOR_MIN;
    static xvector3d VECTOR_MAX;

    static xbool     Get          ( const xproperty& Property, const char* pFullName, xvector3d& Vector, xvector3d& Min, xvector3d& Max );
    static xbool     Get          ( const xproperty& Property, const char* pFullName, f32& Data, f32& Min, f32& Max );
    static xbool     Get          ( const xproperty& Property, const char* pFullName, f32& Data );
    static xbool     Set          ( xproperty& Property, const char* pFullName, f32 Data, u32 Flags=0 );
    static xbool     Set          ( xproperty& Property, const char* pFullName, const xvector3d& Vector, u32 Flags=0 );
    static xbool     Get          ( const xproperty& Property, const char* pFullName, xvector3d& Min, xvector3d& Max );
    static xbool     Get          ( const xproperty& Property, const char* pFullName, xvector3d& Vector );
    static void      Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, button_type ButtonType = BUTTON_PLAIN );
    static xbool     Query        ( xproperty_query& I, xvector3d& Var, xvector3d& Min=VECTOR_MIN, xvector3d& Max=VECTOR_MAX );
    static xvector3d GetUserValue ( xproperty_query& I );
    static void      SetUserValue ( xproperty_query& I, const xvector3d& Value );

protected:

    struct enum_vector3 : public xproperty_enum::entry
    {
        x_rtti_class1( enum_vector3, xproperty_enum::entry )
        button_type    m_ButtonType;
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "fff", ".V3", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".V3"; }

protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xvector3d   m_Max;
    xvector3d   m_Min;
    xvector3d   m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );    
};

//------------------------------------------------------------------------------

class xprop_float : public xproperty_type
{
    x_rtti_class1( xprop_float, xproperty_type )

public:

    enum button_type
    {
        BUTTON_PLAIN,
        BUTTON_SLIDER,

        BUTTON_PADDING  = 0xffffffff
    };

public:

    static xbool   Get          ( const xproperty& Property, const char* pFullName, f32& Float, f32& Min, f32& Max );
    static xbool   Set          ( xproperty& Property, const char* pFullName, f32 Float, u32 Flags=0 );
    static xbool   Get          ( const xproperty& Property, const char* pFullName, f32& Min, f32& Max );
    static xbool   Get          ( xproperty& Property, const char* pFullName, f32& Float );
    static void    Enum         ( xproperty_enum&  E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, button_type ButtonType = BUTTON_PLAIN );
    static xbool   Query        ( xproperty_query& I, f32& Var, f32 Min, f32 Max );
    static f32     GetUserValue ( xproperty_query& I );
    static void    SetUserValue ( xproperty_query& I, const f32 Value );

protected:

    struct enum_float : public xproperty_enum::entry
    {
        x_rtti_class1( enum_float, xproperty_enum::entry )
        button_type    m_ButtonType;
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "f", ".FLOAT", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".FLOAT"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    f32     m_Min, m_Max;
    f32     m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );    
};

//------------------------------------------------------------------------------

class xprop_bool : public xproperty_type
{
    x_rtti_class1( xprop_bool, xproperty_type )

public:

    enum button_type
    {
        BUTTON_BOOL,
        BUTTON_FLAGS,

        BUTTON_PADDING  = 0xffffffff
    };

public:

    static xbool   GetFlags     ( xproperty& Property, const char* pFullName, u32& Flags );
    static xbool   Set          ( xproperty& Property, const char* pFullName, u32 Flags, u32 SystemFlags=0 );
    static xbool   Get          ( const xproperty& Property, const char* pFullName, xbool& Bool );
    static xbool   Set          ( xproperty& Property, const char* pFullName, s32 BitNumber, xbool OnOff, u32 SystemFlags=0 );
    static void    Enum         ( xproperty_enum&  E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0, button_type ButtonType = BUTTON_BOOL, const char* pFlagDescritionString = NULL );
    //static xbool   Query        ( xproperty_query& I, xbool& Var );
    static xbool   Query        ( xproperty_query& I, u32& Var, u32 Mask=0xffffffff );
    
    #define        xprop_bool_QueryBitfield(Q,STRING,I,VAR)

    template<class T>
    static xbool   Query        ( xproperty_query& I, T& Var );

    static xbool   GetUserValue      ( xproperty_query& I );
    static void    SetUserValue      ( xproperty_query& I, const xbool Value );
    static u32     GetUserValueFlags ( xproperty_query& I );
    static void    SetUserValueFlags ( xproperty_query& I, const u32 Value );

protected:

    struct enum_bool : public xproperty_enum::entry
    {
        x_rtti_class1( enum_bool, xproperty_enum::entry )

        button_type     m_ButtonType;
        const char*     m_pFlagsDesc;
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "h", ".BOOL", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".BOOL"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

public:         u32     m_Data;         // Made public because of the macro... However do not access it directly
protected:      s32     m_BitNumber;    // 0...n, -1 == all of them
    
protected:

    friend void AddTypes( xtextfile& DataFile );    
};

//------------------------------------------------------------------------------

class xprop_color : public xproperty_type
{
    x_rtti_class1( xprop_color, xproperty_type )

public:

    static xbool   Get          ( const xproperty& Property, const char* pFullName, xcolor& Color );
    static xbool   Set          ( xproperty& Property, const char* pFullName, xcolor Color, u32 Flags=0 );
    static void    Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    static xbool   Query        ( xproperty_query& I, xcolor& Var );
    static xcolor  GetUserValue ( xproperty_query& I );
    static void    SetUserValue ( xproperty_query& I, const xcolor Value );

protected:

    struct enum_color : public xproperty_enum::entry
    {
        x_rtti_class1( enum_color, xproperty_enum::entry )
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "cccc", ".COLOR", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".COLOR"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xcolor  m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );    
};

//------------------------------------------------------------------------------

class xprop_date : public xproperty_type
{
    x_rtti_class1( xprop_date, xproperty_type )

public:

    static xbool   Get          ( const xproperty& Property, const char* pFullName, xdate& Date );
    static xbool   Set          ( xproperty& Property, const char* pFullName, xdate& Date, u32 Flags=0 );
    static void    Enum         ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    static xbool   Query        ( xproperty_query& I, xdate& Date );
    static xdate   GetUserValue ( xproperty_query& I );
    static void    SetUserValue ( xproperty_query& I, const xdate Value );

protected:

    struct enum_date : public xproperty_enum::entry
    {
        x_rtti_class1( enum_date, xproperty_enum::entry )
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "ddd", ".DATE", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".DATE"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xdate   m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );
};

//------------------------------------------------------------------------------

class xprop_button : public xproperty_type
{
    x_rtti_class1( xprop_button, xproperty_type )

public:

    static xbool   Get              ( const xproperty& Property, const char* pFullName, xstring& Value );
    static xbool   Set              ( xproperty& Property, const char* pFullName, xstring& Value, u32 Flags=0 );
    static void    Enum             ( xproperty_enum& E, s32 iScope, const char* pPropertyName, const char* pUserHelp, u32 Flags = 0 );
    static xbool   Query            ( xproperty_query& I, xstring& Date );
    static xstring GetUserValue     ( xproperty_query& I );
    static void    SetUserValue     ( xproperty_query& I, const xstring& Value );

protected:

    struct enum_button : public xproperty_enum::entry
    {
        x_rtti_class1( enum_button, xproperty_enum::entry )
    };

protected:

    virtual const char*             GetName                 ( void ) const;
    virtual u8                      GetUID                  ( void ) const;
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xfile&     File, xbool bSave );
    virtual xbool                   Serialize               ( xproperty& Property, const char* pFullName, xtextfile& File, xbool bSave );
    virtual void                    SetString               ( xstring& Arguments ) const;
    virtual xbool                   SetPropertyFromString   ( xproperty& Property, const char* pPropFullName, const char* pSourceData ) const;
    virtual void                    RegisterTextFileType    ( xtextfile& TextFile ) const override { TextFile.AddUserType( "s", ".BUTTON", GetUID() ); }
    virtual const char*             getType                 ( void ) const override { return ".BUTTON"; }
    
protected:

    virtual CXTPPropertyGridItem*   CreatePropertyControl   ( CXTPPropertyGridItem& Parent, xproperty& Property, s32 iProp, s32 iItem, const xproperty_enum& Enum ) const;

protected:

    xstring   m_Data;
    
protected:

    friend void AddTypes( xtextfile& DataFile );    
};

//------------------------------------------------------------------------------

void        x_PropertyMgrRegister       ( const char* pName, xproperty* pProperty );
s32         x_PropertyMgrGetCount       ( void );
xproperty*  x_PropertyMgrGetInterface   ( s32 i );
xproperty*  x_PropertyMgrGetInterface   ( const char* pName );
const char* x_PropertyMgrGetName        ( s32 i );
