//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is useful when ever you want to have an array of unknown number
//     of elements, or a dynamic amount of them. The class will construct only
//     elements that are been used even thought its capacity may reflect other wise.
//     So full usage of void constructors and destructors is supported.
//
//<P>  Note that when deleting an element of the array the other elements store
//     higher than the index will be moved down to close the gap. This is useful
//     for sorted elements. Although there is an obvious performance hit. If you
//     need faster deletes you may want to look at the xharray.
//
//<P>  When deleting elements from the xarray the system does not provide any
//     additional protection for its users. It is upto the user to know
//     that an index may become invalid or that a reference of an element
//     has been destroy.
//
//<P>  The xarray class tries to minimize the number of dynamic memory operations
//     performed.  However, it relies on information provided by the SetCapacity
//     function.
//
//<P>  An xarray can be prevented from automatically adjusting its capacity by
//     "locking" it with SetGrowAmount( 0 ).  The xarray will behave normally as
//     long as no alterations to the capacity are required. The xarray can be
//     unlocked at any time by changing the grow amount again.
//
//<P>  The xarray uses the xptr which provides unprecedented security and flexibility
//     to the user and the system. All memory allocated via the xarray can be moved
//     by the system in order to keep memory fragmentation down. In addition the
//     xarray will be able to catch the user from making the typical bug of
//     dereferencing an array element while appending another.
//
//<P>  It should be clear to the user that having a pointer to any of the elements in
//     the array will be at least doubly bad. First the array could grow which could
//     case the memory location to change. Second the system may decided to move
//     the memory for defragmentation proposes and again nuke the user. Having
//     pointers as a genral rule is a bad idea. The array allows the user to have
//     indices which are always valid. Not only that but the user may choose to
//     store their indices as u16 there by cutting the memory usage in comparison to
//     a pointer which could be up to 8 bytes.
//
//<P>  To access an element of the array you must first create a reference with xptr_lock
//     then you can access any of the elements in the xarray via the [] operator function.
//
// Example:
//<CODE>
//      void Test( void )
//      {
//          s32             Index;
//          xarray<entity>  lEntity;
//          entity&         Entity = lEntity.append( Index );
//
//          // Create an xptr_lock for safety
//          xptr_lock LockArray( lEntity );
//
//          // You can access your elements like this
//          Entity.m_pNext = NULL;
//          // or like this
//          lEntity[Index].m_pNext = NULL;
//      }
//</CODE>
// See also:
//     xptr_lock xptr xharray xsafe_array xblock_array xcmd_array
//==============================================================================
template <class T>
class xarray
{
public:
    
    typedef T type;
    
public:
    xarray                  ( s32 StartCapacity, xmem_flags xMemFlags = XMEM_FLAGS_DEFAULT );
    xarray                  ( const xarray<T>& Array ) { Copy(Array); }
    xarray                  ( xmem_flags xMemFlags = XMEM_FLAGS_DEFAULT );
    ~xarray                  ( void );
    
    xiter< xarray<T>, T > begin                ( void )     { return xiter< xarray<T>, T >(0,*this); }
    xiter< xarray<T>, T > end                  ( void )     { return xiter< xarray<T>, T >(m_Count,*this); }
    
    xiter< const xarray<T>, const T > begin     ( void ) const    { return xiter< const xarray<T>, const T >(0,*this); }
    xiter< const xarray<T>, const T > end       ( void ) const    { return xiter< const xarray<T>, const T >(m_Count,*this); }
    
    
    const T&            operator []             ( s32 Index ) const;
    T&                  operator []             ( s32 Index );
    
                        operator const T*       ( void ) const;
                        operator T*             ( void );
    
    void        Copy                    ( const xarray<T>& Array );
    
    void        DeleteWithCollapse      ( s32 Index, s32 Count=1 );
    void        DeleteWithSwap          ( s32 Index, s32 Count=1 );
    void        DeleteAllNodes          ( void );
    void        Kill                    ( void );
    
    T&          Insert                  ( s32 Index );
    T&          append                  ( void );
    T&          append                  ( s32& Index );
    void        appendList              ( s32 Count );
    
    s32         getCount                ( void ) const;
    void        SetGrowAmount           ( s32 Amt );
    void        Grow                    ( s32 NewItems );
    s32         getCapacity             ( void ) const;
    
    void        Pack                    ( void );
    
    template <class KEY>
    xbool       BinarySearch            ( const KEY Key, s32& Index ) const;
    
    template <class KEY>
    xbool       BinarySearch            ( const KEY Key ) const;
    
    xbool       Search                  ( const T& Element, s32& Index ) const;
    xbool       Search                  ( const T& Element) const;
    
    void        Sort                    ( void );
    void        SortMaxtoMin            ( void );
    
protected:
    
    void        Quicksort               ( s32 lower, s32 upper );
    void        QuicksortMaxToMin       ( s32 lower, s32 upper );
    void        StartAccess             ( void );
    void        EndAccess               ( void );
    
protected:
    
    s32         m_Count;
    xptr<T>     m_xData;
    s32         m_GrowAmount;
    xmem_flags  m_MemoryFlags;
    
    friend class xptr_lock;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The common use of this class is usually for editor, and other system pieces
//     that they need to retain an always valid handle to a node. It is also dynamic
//     in growth and it has 8 bytes of over head per entry.
//
//<P>  Advantages of using this class over the xarray is that it uses handles,
//     vesus indices. That means what when you delete or add new nodes old
//     handles are still valid. The second advantage is that when deleting elements
//     creates no memory movedment there by speeding up the operation, but also
//     creating a random index order to the nodes.
//
//<P>  Note that most of the warnings explain in the xarray are also valid here.
//     As a special note is the pointer issue.
//
//<P>  As special issue to this class accessing the entries is done throw the class
//     it self rather than throw the xptr_lock_r? The reason for this is to make sure
//     that there is not confusion between the Handle access and the Index access.
//
//<P>  You can access elements in this class sequencially similar to the xarray.
//     That means you can create a for-loop and access each node in sequence.
//
// Example:
//<CODE>
//      void Test( void )
//      {
//          xhandle             Handle
//          xharray<entity>     lEntity;
//          entity&             Entity = lEntity.Add( Handle );
//
//
//          // Create an xptr_lock to begin access your elements
//          xptr_lock LockList( lEntity );
//
//          // You can access your elements like this
//          lEntity( Handle ).m_pNext = NULL;
//          // or like this
//          Entity.m_pNext = NULL;
//      }
//</CODE>
// See also:
//     xptr_lock xptr xarray xsafe_array xblock_array xcmd_array
//------------------------------------------------------------------------------
template< class T >
class xharray
{
public:
    
    typedef T type;
    
public:
                    xharray                 ( xmem_flags Flags = XMEM_FLAGS_DEFAULT );
                    ~xharray                ( void );
    inline s32      getCount                ( void ) const;
    
    operator const xptr<T>& ( void ) const;
    operator xptr<T>&       ( void );
    
    operator const T*       ( void ) const;
    operator T*             ( void );
    
    s32             GetCapacity             ( void ) const;
    void            Kill                    ( void );
    T&              append                  ( xhandle& Handle );
    T&              append                  ( void );
    
    xiter< xharray<T>, T > begin                ( void )     { return xiter< xharray<T>, T >(0,*this); }
    xiter< xharray<T>, T > end                  ( void )     { return xiter< xharray<T>, T >(m_nNodes,*this); }
    
    xiter< const xharray<T>, const T > begin                ( void ) const    { return xiter< const xharray<T>, const T >(0,*this); }
    xiter< const xharray<T>, const T > end                  ( void ) const    { return xiter< const xharray<T>, const T >(m_nNodes,*this); }
    
    inline T&       operator[]              ( s32       Index    );
    inline const T& operator[]              ( s32       Index    ) const;
    inline T&       operator()              ( xhandle   hHandle  );
    inline const T& operator()              ( xhandle   hHandle  ) const;
    
    void            DeleteAllNodes          ( xbool bReorder = FALSE );
    void            DeleteByIndex           ( s32       Index    );
    void            DeleteByHandle          ( xhandle   hHandle  );
    xhandle         GetHandleByIndex        ( s32       Index    ) const;
    s32             GetIndexByHandle        ( xhandle   hHandle  ) const;
    void            GrowListBy              ( s32       nNodes   );
    void            SwapIndex               ( s32 IndexA, s32 IndexB );
    
protected:
    
    struct map
    {
        xhandle     m_IndexToHandle;
        s32         m_HandleToIndex;
    };
    
protected:
    
    void        StartAccess             ( void );
    void        EndAccess               ( void );
    
protected:
    
    s32             m_nNodes;
    xptr<map>       m_xMap;
    xptr<T>         m_xData;
    xmem_flags      m_MemoryFlags;
    
    friend class xptr_lock;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     xpage_array is similar to a xharray except it guarantees that the memory will always be valid.
//     in other words it wont realloc memory stead it allocates pages and the entries are manage there.
//     this avoids reallocations and makes the memory safer. 
// See also:
//     xptr_lock xptr xarray xsafe_array xblock_array xcmd_array xharray
//------------------------------------------------------------------------------
template<class T>
class xparray
{
public:

    s32 getCount( void ) const;

    template< class I > T&              operator[]          ( I index ); 
    template< class I > const T&        operator[]          ( I index ) const;
    T&                                  operator()          ( xhandle   hHandle  );
    const T&                            operator()          ( xhandle   hHandle  ) const;

    s32                                 HandleToIndex       ( xhandle Handle ) const;
    xhandle                             IndexToHandle       ( s32 Index ) const;

    void                                SanityCheck         ( void ) const;
    T&                                  append              ( void );
    T&                                  append              ( xhandle& Handle );
    void                                Delete              ( s32 index );
    void                                Delete              ( xhandle Handle ); 

    xiter< xparray<T>, T >              begin               ( void )     { return xiter< xparray<T>, T >(0,*this); }
    xiter< xparray<T>, T >              end                 ( void )     { return xiter< xparray<T>, T >(m_Count,*this); }
    
    xiter< const xparray<T>, const T >  begin               ( void ) const    { return xiter< const xharray<T>, const T >(0,*this); }
    xiter< const xparray<T>, const T >  end                 ( void ) const    { return xiter< const xharray<T>, const T >(m_Count,*this); }

protected:

    enum constants:u32
    {
        ENTRIES_PER_PAGE_POW2   = 9,
        ENTRIES_PER_PAGE        = (1<<ENTRIES_PER_PAGE_POW2),
        HANDLE_PAGE_SHIFT       = ENTRIES_PER_PAGE_POW2,
        HANDLE_PAGE_MASK        = ((1<<HANDLE_PAGE_SHIFT)-1)<<HANDLE_PAGE_SHIFT,
        HANDLE_ENTRY_MASK       = ~HANDLE_PAGE_MASK,
    };

    struct page
    {
        u16                                 m_iFreeHead;
        u16                                 m_nAlloced;
        u16                                 m_iNextFreePage;
        xsafe_array<T,ENTRIES_PER_PAGE>     m_Entry;
    };

    struct iterator_entry
    {
        xhandle                             m_hEntry;               // Handle to the actual entry
        u32                                 m_iHandle;              // Given an [hEntry] we give back the iterator index
    };

protected:

    void    AllocPage( void );

protected:

    xptr<page>              m_lPages;
    xptr<iterator_entry>    m_Iterator;
    u32                     m_Count             = 0;                // Total Allocated Entries
    u16                     m_iFreePage         = 0xffff;           // Link List of empty pages
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     A simple bit array. Indices and sizes always in bits.
//------------------------------------------------------------------------------
class xbitarray
{
public:
    xbitarray               ( void );
    ~xbitarray               ( void );
    void            Init                    ( s32 Size );
    void            SetAllBits              ( xbool BitValue );
    xbool           Get                     ( s32 Index ) const;
    void            Set                     ( s32 Index, xbool Value );
    
private:
    
    xptr<xbyte>     m_Buffer;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This block array is design for things that can't have memory movement.
//     Stead what it does it is to allocate pages of arrays of the item in question,
//     which is more gentle with memory than a large allocation. Because of this limitation
//     you should be carefull when to use this class. Other similar classes to this may
//     become avariable. Classes where it will allow to move memory.
//
//<P>  Note that in the template the 'S' is the count of items in each page. You should
//     try to valance the page size with this field. Note that the more pages you have
//     the worse the performance because it needs to seach when deleting an entry.
//
// Note:
//     <B>This class is very likely to go away at some point</B>
//
// Example:
//<CODE>
//      void Test( void )
//      {
//          xblock_array<entity, 128> lEntity;
//
//          entity* pEntry = lEntity.append();
//      }
//</CODE>
// See also:
//     xptr xharray xarray xsafe_array xblock_array xcmd_array
//------------------------------------------------------------------------------
template<class T, s32 S>
class xblock_array
{
public:
            xblock_array    ( void );
           ~xblock_array    ( void );
    void    Kill            ( void );
    T*      append          ( void );
    void    Delete          ( T* pNode );
    
protected:
    
    struct node
    {
        u16     m_iNext;
        T       m_Node;
        inline ~node() {}
    };
    
    struct block
    {
        block*              m_pNext;        // Next block
        block*              m_pEmpty;       // Next empty block
        u16                 m_iUsed;        // Link list of used nodes
        u16                 m_iEmpty;       // Link list of free nodes
        xsafe_array<node,S> m_Node;         // array of nodes
    };
    
protected:
    
    void Grow( void );
    
protected:
    
    block*  m_pHead;
    block*  m_pEmpty;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     A cmd array is list of structures which have different sizes. This type of
//     array it is use in things like a list of commands for a system, where
//     each command could have a diferent set of arguments.
//
//<p>  This class uses the xbase xrtti for type safety. You can also you the xptr_lock for safety.
//
// Example:
//<CODE>
//
//      struct my_command1 : public xcmd_array::cmd_entry
//      {
//          x_rtti_class1( my_command1, xcmd_array::cmd_entry );
//          f32 X, Y, Z;
//      };
//
//      struct my_command2 : public xcmd_array::cmd_entry
//      {
//          x_rtti_class1( my_command2, xcmd_array::cmd_entry );
//          s32 Size;
//      };
//
//      void Test( void )
//      {
//          xcmd_array CmdList;
//
//          // Write some commands
//          my_command1& Cmd1 = CmdList.Append<my_command1>();
//          Cmd1.X = 2.0f;
//          Cmd1.Y = Cmd1.Z = 3.0f;
//
//          my_command2& Cmd2 = CmdList.Append<my_command2>();
//          Cmd2.Size = 100;
//
//          // Read some commands
//          for( s32 Index=0; Index < CmdList.GetEndIndex(); Index = CmdList.GetNextIndex( Index ) )
//          {
//              const xcmd_array::cmd_entry& Entry = CmdList.GetEntry( Index );
//
//              if( Entry.IsKindOf( my_command1::GetRTTI() ) )
//              {
//                  const my_command1& Cmd1 = my_command1::SafeCast( Entry );
//                  ASSERT( Cmd1.X == 2.0f );
//                  ASSERT( Cmd1.Y == Cmd1.Z );
//              }
//              else if( Entry.IsKindOf( my_command2::GetRTTI() ) )
//              {
//                  const my_command2& Cmd2 = my_command2::SafeCast( Entry );
//                  ASSERT( Cmd2.Size == 100 );
//              }
//              else
//              {
//                  ASSERTS( 0, "UNKNOWN COMMAND" );
//              }
//          }
//      }
//</CODE>
// See also:
//     xptr_lock xptr xharray xsafe_array xblock_array xarray
//------------------------------------------------------------------------------
class xcmd_array
{
public:
    
    class cmd_entry
    {
        x_rtti_base( cmd_entry )
        
    public:
        inline s32  GetNextCmd( void ) const { return m_iNextCmd; }
        
    private:
        inline void SetNextCmd( s32 Next )   { m_iNextCmd = Next; }
        
    private:
        s32     m_iNextCmd;
        friend class xcmd_array;
    };
    
public:
    xcmd_array      ( void );
    ~xcmd_array      ( void );
    
    void                SetGrowAmount   ( s32 nBytes );
    void                Grow            ( s32 nBytes );
    inline void         Kill            ( void );
    inline void         Reset           ( void );
    s32                 GetEndIndex     ( void );
    s32                 GetNextIndex    ( s32 Index );
    
    template< class T >
    T&                  append          ( void );
    
    cmd_entry&          GetEntry        ( s32 Index );
    const cmd_entry&    GetEntry        ( s32 Index ) const;
    
    template< class T >
    T&                  Get             ( s32 Index );
    
    template< class T >
    const T&            Get             ( s32 Index ) const;
    
protected:
    
    void            StartAccess     ( void );
    void            EndAccess       ( void );
    
protected:
    
    s32                     m_GrowAmount;
    s32                     m_iCurrent;
    s32                     m_iPrevious;
    xptr<u8>                m_Buffer;
};

