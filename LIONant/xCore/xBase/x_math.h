//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//@@Math
//
//<P> Here are the supported math functions for all the platforms. While some of
//    the signatures of this functions may look like ANSI, they are not double 
//    precision. They are all (f32) float.
//
//<P> All math functions are optimize for each of the platforms. 
//
//<P> WARNING: The PC vesion is optimize for sse. Make sure your computer have it.
//
// TODO:
//
//  * For the PC all the sse optimizations are not taken into consideration the
//     stalls from instruction to instruction. For now I left it like this to 
//     keep it clear.
//
//  * All the inline functions should be order such the if a function a needs 
//     function b. To make sure that b comes before a. This is because some
//     compilers only inline if it comes before.
//
//  * Reminder: Convention for Getxxxx is that it will not modify the class. Putxxx
//     and not prefix functions are always put and modifying the class. Except for 
//     a few exception as noted. Added also the concept that almost all Set-ers should 
//     also return themselves
//
//  * Include all the C++ code for the assembly functions as well. This can be use
//     to test the assembly and/or for reference in the future.
//
//  * Add matrix43 for software support and lower memory overhead
//
//  * Do a pass and see where the vector3 are needed vs the vector3d. Such there is never
//     a reason to return a vector3d always return a vector3. It should never be a reason why
//     to use a vector3d as a temporary variable always use vector3
//
//  * Reminder: Rules for Get/Set/Setup/no prefix. Get will get the value of the class without
//     changing the class it self. Set will overwrite some value in the class. Setup will overwrite
//     the hold class. When there is nothing is an implicid set. But unline the set it usually means
//     that there is an operation that goes along with it.
//
//  * The math library is optimize for sse. The pentium 4 now have sse3 which they added a
//     bunch of new instructions that could help out the performance of the library. 
//
//  * Do a pass on isValid && ASSERTS for all the classes.
// 
//==============================================================================

//==============================================================================
//  GENERAL MATH FUNCTIONS
//==============================================================================
//
// Functions:
//
//  f32   x_Sqrt    ( f32 x )           - Square root.
//  f32   x_Floor   ( f32 x )           - Floor.
//  f32   x_ceil    ( f32 x )           - Ceiling.
//  f32   x_Log     ( f32 x )           - Log base e.  Natural log.
//  f32   x_Log2    ( f32 x )           - Log base 2.
//  f32   x_Log10   ( f32 x )           - Log base 10.
//  f32   x_Exp     ( f32 x )           - Raise e to x power.
//  f32   x_Pow     ( f32 x )           - Raise a number to a power.
//  f32   x_FMod    ( f32 x, f32 y )    - True modulus between two numbers.
//  f32   x_ModF    ( f32 x, f32* y )   - Break number into whole (integer) and fractional parts.
//  T     x_Sqr     ( T x )             - Square.
//  f32   x_InvSqrt ( f32 x )           - One over square root.
//  f32   x_LPR     ( f32 x, f32 y )    - Least Positive Residue.  Non-negative modulus value.
//  T     x_Abs     ( T x, T y )        - Absolute value of any signed numeric type.
//  f32   x_Round   ( f32 x, f32 y )    - Round a number to nearest multiple of another number.
//  xbool x_IsValid ( f32 x )           - Returns TRUE if value is valid (not INF or NAN).
//  T     x_Min     ( f32 x, f32 y )    - Returns what ever number is the minimun. (f32, s32, s8, etc)
//  T     x_Max     ( f32 x, f32 y )    - Returns what ever number is the maximun. (f32, s32, s8, etc)
//  xbool x_Sign    ( f32 x )           - Returns the sign of a number (-1,0,1)  (f32, s32, s8, etc)
//
// Additional functions:
//
//  T   x_InRange( T x, T min, T max )  - Returns a boolean indicating whether a number is in rage of two others.
//  T   x_Range  ( T x, T min, T max )  - Returns a number which will be in the range of two to other.
//
//==============================================================================

//==============================================================================
//  TRIGONOMETRIC MATH FUNCTIONS
//==============================================================================
//
// Types:
//
//  xradian - is a typed defined to describe xradian values.
//
// Functions:
//  
//  f32      x_Sin   ( xradian Angle   )  - Sine.
//  f32      x_Cos   ( xradian Angle   )  - Cosine.
//  f32      x_Tan   ( xradian Angle   )  - Tangent.
//  xradian  x_ASin  ( f32     Sine    )  - Arc sine.
//  xradian  x_ACos  ( f32     Cosine  )  - Arc cosine.
//  xradian  x_ATan  ( f32     Tangent )  - Arc tangent.
//  xradian  x_ATan2 ( f32 y, f32 x    )  - Standard "atan2" arc tangent where y can equal 0.
//                
// Additional functions:
//
//  void     x_SinCos       ( xradian Angle, f32& sin, f32& cos ) - Sine and cosine in one function call.
//  xradian  x_ModAngle     ( xradian Angle )                     - Provide equivalent angle in [    0, 360 ) degrees.
//  xradian  x_ModAngle2    ( xradian Angle )                     - Provide equivalent angle in [ -180, 180 ) degrees.
//  xradian  x_MinAngleDiff ( xradian Angle1, xradian Angle2 )    - Provide smallest angle between two given angles.
//
// There are some macros also defined
//
//  PI                      - It is the constance value of PI. (in floating point)
//  f32 DEG_TO_RAD( T x )   - Converts degrees to xradians
//  f32 RAD_TO_DEG( T x )   - Converts a number from xradiants to degrees.
//  f32 XRADIAN( T x )      - Works the same way than DEG_TO_RAD
//
//==============================================================================

//==============================================================================
// TYPES
//==============================================================================
typedef f32 xradian;                // f32 radian type 
typedef f32 xdegree;                // f32 degree type 
struct      xradian3;               // Software base 2 floats no alignment
struct      xvector2;               // Software base 2 floats no alignment
struct      xvector3d;              // Software base 3 floats no alignment
struct      xvector3;               // Hardware accelerated 4 floats and 16 bytes align
union       xvector4;               // Hardware accelerated with 16 bytes align
union       xmatrix4;               // Hardware base matrix Row major with 16 bytes align compatible with d3d
union       xquaternion;            // Hardware accelerated and 16 bytes align
struct      xplane;                 // Software base 4 floats no alignment 
struct      xbbox;                  // Software base 6 floats no alignment 
struct      xsphere;                // Software base 4 floats no alignment
struct      xirect;                 // Software base 6 integers no alignment 

//==============================================================================
// CONSTANTS
//==============================================================================

#ifdef PI
#undef PI
#endif

#ifndef TARGET_MARMALADE
#ifndef TARGET_ANDROID

    #ifdef M_E
        #undef M_E
    #endif

    #define M_E                 (2.7182818284590452354f)   // e 
#endif // TARGET_ANDROID
#endif // TARGET_MARMALADE

#define PI                  (3.14159265358979323846f)  // pi
#define PI2                 (6.28318530717958647693f)  // pi * 2
#define LOG2E               (1.4426950408889634074f)   // log 2e 
#define LOG10E              (0.43429448190325182765f)  // log 10e 
#define LN2                 (0.69314718055994530942f)  // log e2 
#define LN10                (2.30258509299404568402f)  // log e10 
#define PI_OVER2            (1.57079632679489661923f)  // pi/2 
#define PI_OVER4            (0.78539816339744830962f)  // pi/4 
#define SQRT2               (1.41421356237309504880f)  // sqrt(2)

#define DEG_TO_RAD(A)       ((A) * 0.017453292519943295769f)   // (A*PI)/180
#define RAD_TO_DEG(A)       ((A) * 57.29577951308232087685f)   // (A*180)/PI
#define X_RADIAN(A)         DEG_TO_RAD(A)

#define XFLT_TOL            0.001f

//==============================================================================
// BASIC FUNTIONS
//==============================================================================
xradian                     x_ModAngle              ( xradian Angle );
xradian                     x_ModAngle2             ( xradian Angle );
xradian                     x_MinAngleDiff          ( xradian Angle1, xradian Angle2 );
xradian                     x_LerpAngle             ( f32 t, xradian Angle1, xradian Angle2 );
void                        x_SinCos                ( xradian Angle, f32& S, f32& C );
f32                         x_Sin                   ( xradian x );
f32                         x_Cos                   ( xradian x );        
f64                         x_SinF64                ( f64 x );
f64                         x_CosF64                ( f64 x );
f32                         x_ASin                  ( f32 x );
f32                         x_ACos                  ( f32 x );
f32                         x_Tan                   ( xradian x );
f64                         x_TanF64                ( f64 x );
f32                         x_ATan2                 ( f32 x, f32 y );    
f32                         x_ATan                  ( f32 x );    

template< class T > T       x_Sqr                   ( T a );
f32                         x_Sqrt                  ( f32 x );	
f64                         x_SqrtF64               ( f64 x );
f32                         x_InvSqrt               ( f32 x );
xbool                       x_SolvedQuadraticRoots  ( f32& Root1, f32& Root2, f32 a, f32 b, f32 c );
f32                         x_Exp                   ( f32 f );
f64                         x_ExpF64                ( f64 f );
f32                         x_Pow                   ( f32 a, f32 b);
f64                         x_PowF64                ( f64 a, f64 b );
f32                         x_FMod                  ( f32 x, f32 y );
f32                         x_ModF                  ( f32 x, f32 y );
f32                         x_Log                   ( f32 A );
f64                         x_LogF64                ( f64 A );
f32                         x_Log2                  ( f32 A ); 
f32                         x_Log10                 ( f32 A);
f32                         x_LPR                   ( f32 a, f32 b );
template< class T > T       x_Lerp                  ( f32 t, T a, T b );
template< typename T1, typename T2 > auto x_Min     ( T1 a, T2 b ) -> decltype( a + b );
template< typename T1, typename T2 > auto x_Max     ( T1 a, T2 b ) -> decltype( a + b );
template< class T > s32     x_GetPowerOfTwo         ( T a );

xbool                       x_FEqual                ( f32 f0, f32 f1, f32 tol = XFLT_TOL );
xbool                       x_FLess                 ( f32 f0, f32 f1, f32 tol = XFLT_TOL );
xbool                       x_FGreater              ( f32 f0, f32 f1, f32 tol = XFLT_TOL ); 
f32                         x_FSel                  ( f32 A, f32 B, f32 C );
f32                         x_I2F                   ( f32 i );
s32                         x_F2I                   ( f32 f );
xbool                       x_isValid               ( f32 a );
s32                         x_LRound                ( f32 x );
f32                         x_Round                 ( f32 a, f32 b );
f32                         x_Ceil                  ( f32 x );
f32                         x_Floor                 ( f32 x );
template< class T > xbool   x_Sign                  ( T a );
template< class T > xbool   x_InRange               ( T X, T Min, T Max );
template< class T > T       x_Range                 ( T X, T Min, T Max );
template< class T > T       x_Abs                   ( T a );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 3D rotation using the classical Pitch, Yaw and Roll.
//     It is very useful when trying to minimize the memory footprint needed to 
//     represent a rotation or if you just want to deal with only angles.
//
//<P>  The class is not hardware accelerated and works only with floats. There
//     is not need for special aligments except for the atomic types.
// See Also:
//     xradian xquaternion xmatrix4
//------------------------------------------------------------------------------
struct xradian3
{
                            xradian3                ( void ); 
                            xradian3                ( const xradian3& R ); 
                            xradian3                ( xradian Pitch, xradian Yaw, xradian Roll );
                            xradian3                ( const xquaternion& Q );
                            xradian3                ( const xmatrix4& M );

    void                    Zero                    ( void );
    inline xradian3&        Set                     ( xradian Pitch, xradian Yaw, xradian Roll );

    xradian3&               ModAngle                ( void );
    xradian3&               ModAngle2               ( void );
    xradian3                GetMinAngleDiff         ( const xradian3& R ) const;

    f32                     Difference              ( const xradian3& R ) const;
    xbool                   InRange                 ( xradian Min, xradian Max ) const;
    xbool                   isValid                 ( void ) const;

    xbool                   operator ==             ( const xradian3& R ) const;
    const   xradian3&       operator +=             ( const xradian3& R );
    const   xradian3&       operator -=             ( const xradian3& R );
    inline const xradian3&  operator *=             ( f32 Scalar );
    const   xradian3&       operator /=             ( f32 Scalar );

    friend  xradian3        operator +              ( const xradian3& R1, const xradian3& R2 );
    friend  xradian3        operator -              ( const xradian3& R1, const xradian3& R2 );
    friend  xradian3        operator -              ( const xradian3& R );
    friend  xradian3        operator /              ( const xradian3& R,        f32      S  );
    friend  xradian3        operator *              ( const xradian3& R,        f32      S  );
    friend  xradian3        operator *              (        f32      S,  const xradian3& R  );

public:

    xradian     m_Pitch, m_Yaw, m_Roll;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 2D vector. 
//
//<P>  The class is not hardware accelerated and works only with floats. There
//     is not need for special aligments except for the atomic types.
// See Also:
//     xvector3 xvector4
//------------------------------------------------------------------------------
struct xvector2
{
                            xvector2                ( void ) = default;
                            xvector2                ( const xvector2& V );
    inline                  xvector2                ( f32 X, f32 Y );
                            xvector2                ( f32 Float );

    // Exceptions to the Get/Set rule
    inline f32              Dot                     ( const xvector2& V ) const;
    xvector2                Lerp                    ( f32 t, const xvector2& V1 ) const;

    // Normal functions
    inline xvector2&        Set                     ( f32 X, f32 Y );
    void                    Zero                    ( void );
    f32                     GetLength               ( void ) const;
    f32                     GetLengthSquared        ( void ) const;
    xradian                 GetAngle                ( void ) const;
    xradian                 GetAngleBetween         ( const xvector2& V ) const;
    f32                     GetDistance             ( const xvector2& V ) const;
    xbool                   InRange                 ( f32 Min, f32 Max ) const;
    xbool                   isValid                 ( void ) const;                                            
    xvector2&               Rotate                  ( xradian Angle );        
    xvector2&               Normalize               ( void );
    xvector2&               NormalizeSafe           ( void );

    f32                     GetWhichSideOfLine          ( const xvector2 &V0, const xvector2& V1 );
    xvector2                GetClosestPointInLine       ( const xvector2 &V0, const xvector2 &V1 );
    xvector2                GetClosestPointInLineSegment( const xvector2 &V0, const xvector2 &V1 );

    inline xbool            operator ==             ( const xvector2& V ) const;
    const   xvector2&       operator +=             ( const xvector2& V );
    const   xvector2&       operator -=             ( const xvector2& V );
    const   xvector2&       operator *=             ( const xvector2& V );
    const   xvector2&       operator *=             ( f32 Scalar );
    const   xvector2&       operator /=             ( f32 Scalar );  

    friend  xvector2        operator +              ( const xvector2& V1, const xvector2& V2 );
    friend  xvector2        operator -              ( const xvector2& V1 );
    friend  xvector2        operator -              ( const xvector2& V1, const xvector2& V2 );
    friend  xvector2        operator /              ( const xvector2& V,        f32      S  );
    friend  xvector2        operator *              ( const xvector2& V,        f32      S  );
    friend  xvector2        operator *              ( const xvector2& V1, const xvector2& V2 );
    friend  xvector2        operator *              (       f32      S,  const xvector2& V  );

public:

    f32 m_X, m_Y;                                                      
};                                          

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 3D vector. 
//
//<P>  xvector3d is not a hardware accelerated class and works only with floats. There
//     is not need for special aligments thought except for the atomic types. it also
//     doesn;t waste memoty like its speedy child xvector3.
//
//<P>  xvector3 is meant for speed not memory considerations. For that it contains 
//     four elements stead of the 3 that are actually needed. The class has special
//     aligment requirements (16byte) Which allow to have an extra bust for speed.
//     The class uses any hardware feature it can find to speed it self up.
//
//<P>  When a user should choolse one class versus the other? There are a few simple
//     rules that can be fallow. If you are returning a vector3 always return a xvector3
//     because it is compatible with xvector3d and if the user uses other xvector3 will
//     get a speed boost. If you create temporary vectors also use xvector3 as they can
//     give you a boost in speed. The only reason to use xvector3 is as a parameter 
//     variable in a function which you dont know whether the user is going to be using
//     xvector3 or xvector3d. This ofcourse will slow down the compotation so only do
//     that if you are not doing too much math. Remember xvector3 can be constructed from
//     a xvector3d. Finally if memory is a very sensitive issue because you have allot 
//     of vectors then use xvector3d as it saves 1 float per vector. 
//
// Example:
//<CODE>
//      xvector3 DoSomething( const xvector3d& V )
//      {
//          xvector3 G;
//          G = V * 0.5f;
//          G = G.GetMax( V );
//          return G.Cross( V );
//      }
//</CODE>
//      
// See Also:
//     xvector3 xvector3d xvector2 xvector4
//------------------------------------------------------------------------------
struct xvector3d
{
                            xvector3d               ( void );                     
                            xvector3d               ( f32 X, f32 Y, f32 Z ); 
                            xvector3d               ( xradian Pitch, xradian Yaw );
                            xvector3d               ( const f32x4& Register );
                            xvector3d               ( const f32 n );

    // Exceptions to the Get/Set rule
    xvector3                Lerp                    ( f32 t, const xvector3d& V ) const;
    f32                     Dot                     ( const xvector3d& V ) const;
    xvector3                Cross                   ( const xvector3d& V ) const;

    // Normal functions
    inline xvector3d&       Set                     ( xradian Pitch, xradian Yaw );
    inline xvector3d&       Set                     ( f32 X, f32 Y, f32 Z );
    xvector3d&              Set                     ( const f32x4& Register );        
    inline xvector3d&       Set                     ( const f32 n );
                            
    void                    Identity                ( void );
    void                    Zero                    ( void );
    xbool                   isValid                 ( void ) const;
    xbool                   InRange                 ( const f32 Min, const f32 Max ) const;
    xbool                   IsRightHanded           ( const xvector3d& P1, const xvector3d& P2 ) const;
                            
    xvector3d&              RotateX                 ( xradian Rx );           
    xvector3d&              RotateY                 ( xradian Ry );           
    xvector3d&              RotateZ                 ( xradian Rz ); 
    xvector3d&              Rotate                  ( const xradian3& R );
                            
    xradian                 GetPitch                ( void ) const;
    xradian                 GetYaw                  ( void ) const;
    void                    GetPitchYaw             ( xradian& Pitch, xradian& Yaw ) const;
    xradian                 GetAngleBetween         ( const xvector3d& V ) const;
    void                    GetRotationTowards      ( const xvector3d& DestV,
                                                            xvector3d& RotAxis, 
                                                            xradian&   RotAngle ) const;

    f32                     GetX                    ( void ) const;
    f32                     GetY                    ( void ) const;
    f32                     GetZ                    ( void ) const;
    xvector3                GetOneOver              ( void ) const;
    f32                     GetLength               ( void ) const;
    f32                     GetLengthSquared        ( void ) const;
    f32                     GetDistanceSquare       ( const xvector3d& V ) const;
    f32                     GetDistance             ( const xvector3d& V ) const;
    xvector3                GetMax                  ( const xvector3d& V ) const;
    xvector3                GetMin                  ( const xvector3d& V ) const;
    xvector3d&              Abs                     ( void );
    xvector3d&              Normalize               ( void );
    xvector3d&              NormalizeSafe           ( void );
    xvector3                GetReflection           ( const xvector3d& V ) const;
    xvector3d&              GridSnap                ( f32 GridX, f32 GridY, f32 GridZ );
                            
    xvector3                GetVectorToLineSegment      ( const xvector3d& Start, const xvector3d& End ) const;
    xvector3                GetClosestPointInLineSegment( const xvector3d& Start, const xvector3d& End ) const;
    f32                     GetSquareDistToLineSeg      ( const xvector3d& Start, const xvector3d& End ) const;
    f32		                GetClosestPointToRectangle  ( const xvector3d& P0,   // Origin from the edges. 
                                                          const xvector3d& E0, 
                                                          const xvector3d& E1,	
                                                                xvector3d& OutClosestPoint ) const;

    f32*                    operator ()             ( void );
    f32&                    operator ()             ( const s32 i );
    f32                     operator ()             ( const s32 i ) const;
    xbool                   operator ==             ( const xvector3d& V ) const;
    const xvector3d&        operator +=             ( const xvector3d& V );
    const xvector3d&        operator -=             ( const xvector3d& V );
    const xvector3d&        operator *=             ( const xvector3d& V );
    const xvector3d&        operator /=             ( f32 Div );
    const xvector3d&        operator *=             ( f32 Scalar );
                                    
    friend xvector3         operator /              ( const xvector3d& V, f32 Div );
    friend xvector3         operator *              ( f32 Scale, const xvector3d& V );
    friend xvector3         operator *              ( const xvector3d& V, f32 Scale );
    friend inline xvector3  operator -              ( const xvector3d& V );
    friend xvector3         operator *              ( const xvector3d& V0, const xvector3d& V1 );
    friend xvector3         operator -              ( const xvector3d& V0, const xvector3d& V1 );
    friend xvector3         operator +              ( const xvector3d& V0, const xvector3d& V1 );
    friend xvector3         operator /              ( const xvector3d& V0, const xvector3d& V1 );

    friend union xvector4;
    friend union xmatrix4;
    friend union axis;

public:

    f32 m_X, m_Y, m_Z;
};

//==============================================================================
// VECTOR3
//==============================================================================

X_MS_ALIGNMENT(16) 
// <COPY xvector3d>
struct xvector3 : public xvector3d
{
                            xvector3                ( void );                     
    inline                  xvector3                ( const xvector3d&  V );                     
    inline                  xvector3                ( f32 X, f32 Y, f32 Z ); 
                            xvector3                ( xradian Pitch, xradian Yaw );
                            xvector3                ( const f32x4& Register );
                            xvector3                ( const f32 n );

    // Exceptions to the Get/Set rule
    xvector3                Lerp                    ( f32 t, const xvector3& V ) const;
    inline f32              Dot                     ( const xvector3& V ) const;
    xvector3                Cross                   ( const xvector3& V ) const;

    // Normal functions
    inline xvector3&        Set                     ( f32 X, f32 Y, f32 Z );
    xvector3&               Set                     ( const f32x4& Register );        
    inline xvector3&        Set                     ( const f32 n );

    void                    Identity                ( void );
    void                    Zero                    ( void );
    xbool                   isValid                 ( void ) const;
    xbool                   InRange                 ( const f32 Min, const f32 Max ) const;

    f32x4&                  GetH                    ( void ) const;
    xvector3                GetOneOver              ( void ) const;
    f32                     GetLength               ( void ) const;
    inline f32              GetLengthSquared        ( void ) const;
    f32                     GetDistanceSquare       ( const xvector3& V ) const;
    f32                     GetDistance             ( const xvector3& V ) const;
    xvector3                GetMax                  ( const xvector3& V ) const;
    xvector3                GetMin                  ( const xvector3& V ) const;
    xvector3&               Abs                     ( void );
    xvector3&               Normalize               ( void );
    xvector3&               NormalizeSafe           ( void );
    xvector3                Blend                   ( const f32 T, const xvector3& End ) const;
                            
    f32*                    operator ()             ( void );
    f32&                    operator ()             ( const s32 i );
    f32                     operator ()             ( const s32 i ) const;
    xbool                   operator ==             ( const xvector3& V ) const;
    const xvector3&         operator +=             ( const xvector3& V );
    inline const xvector3&  operator -=             ( const xvector3& V );
    const xvector3&         operator *=             ( const xvector3& V );
    const xvector3&         operator /=             ( f32 Div );
    const xvector3&         operator *=             ( f32 Scalar );
                                    
    friend inline xvector3  operator /              ( const xvector3& V, f32 Div );
    friend inline xvector3  operator *              ( f32 Scale, const xvector3& V );
    friend inline xvector3  operator *              ( const xvector3& V, f32 Scale );
    friend inline xvector3  operator +              ( f32 Scale, const xvector3& V );
    friend inline xvector3  operator +              ( const xvector3& V, f32 Scale );
    friend inline xvector3  operator -              ( f32 Scale, const xvector3& V );
    friend inline xvector3  operator -              ( const xvector3& V, f32 Scale );
    
    friend inline xvector3  operator -              ( const xvector3& V );
    friend inline xvector3  operator *              ( const xvector3& V0, const xvector3& V1 );
    friend inline xvector3  operator -              ( const xvector3& V0, const xvector3& V1 );
    friend inline xvector3  operator +              ( const xvector3& V0, const xvector3& V1 );
    friend inline xvector3  operator /              ( const xvector3& V0, const xvector3& V1 );

    friend inline xvector3  operator +              ( const xvector3d& V0, const xvector3&  V1 );
    friend inline xvector3  operator +              ( const xvector3&  V0, const xvector3d& V1 );
    friend inline xvector3  operator -              ( const xvector3d& V0, const xvector3&  V1 );
    friend inline xvector3  operator -              ( const xvector3&  V0, const xvector3d& V1 );
    friend inline xvector3  operator *              ( const xvector3d& V0, const xvector3&  V1 );
    friend inline xvector3  operator *              ( const xvector3&  V0, const xvector3d& V1 );
    friend inline xvector3  operator /              ( const xvector3d& V0, const xvector3&  V1 );
    friend inline xvector3  operator /              ( const xvector3&  V0, const xvector3d& V1 );

    friend union xvector4;
    friend union xmatrix4;
    friend union axis;
public:

    f32     m_W;
    
} X_SN_ALIGNMENT(16);

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 4D vector. 
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xvector2
//------------------------------------------------------------------------------
X_MS_ALIGNMENT(16) 
union xvector4
{

    inline                  xvector4                ( void );
    inline                  xvector4                ( f32 x );
    inline                  xvector4                ( const xvector3d& V );
    inline                  xvector4                ( const xvector4& V );    
    inline                  xvector4                ( f32 X, f32 Y, f32 Z, f32 W=0 ); 
    inline                  xvector4                ( const f32x4& Register );
    inline                  xvector4                ( const xvector3d& V, f32 W=0 );    

    xvector4&               Set                     ( f32 X, f32 Y, f32 Z, f32 W=0 );

    void                    Zero                    ( void );    
    void                    Identity                ( void );
    f32                     GetLength               ( void ) const; 
    f32                     GetLengthSquared        ( void ) const; 
    f32                     Dot                     ( const xvector4& V ) const;
    xbool                   isValid                 ( void ) const;

    const xvector4&         operator +=             ( const xvector4& V );
    const xvector4&         operator -=             ( const xvector4& V );
    const xvector4&         operator *=             ( const xvector4& V );
    const xvector4&         operator *=             ( f32 Scalar );
    const xvector4&         operator /=             ( f32 Scalar );

    xvector4&               Normalize               ( void );
    xvector4&               NormalizeSafe            ( void );
    xvector4&               Abs                     ( void );
    xbool                   InRange                 ( const f32 Min, const f32 Max ) const;
    xvector4                GetMax                  ( const xvector4& V ) const;
    xvector4                GetMin                  ( const xvector4& V ) const;
    xvector4&               Homogeneous             ( void );
    xvector4                OneOver                 ( void ) const;
    xbool                   operator ==             ( const xvector4& V ) const;

    friend inline xvector4  operator +              ( const xvector4& V1, const xvector4& V2 );
    friend inline xvector4  operator -              ( const xvector4& V1, const xvector4& V2 );
    friend inline xvector4  operator *              ( const xvector4& V1, const xvector4& V2 );
    friend inline xvector4  operator -              ( const xvector4& V );
    friend inline xvector4  operator /              ( const xvector4& V,        f32      S  );
    friend inline xvector4  operator *              ( const xvector4& V,        f32      S  );
    friend inline xvector4  operator *              (        f32      S,  const xvector4& V  );
    friend inline xvector4  operator +              ( const xvector4& V,        f32      S  );
    friend inline xvector4  operator +              (        f32      S,  const xvector4& V  );
    friend inline xvector4  operator -              ( const xvector4& V,        f32      S  );
    friend inline xvector4  operator -              (        f32      S,  const xvector4& V  );

public:

    f32x4       m_WZYX;

    struct
    {
        f32     m_X;
        f32     m_Y;
        f32     m_Z;
        f32     m_W;
    };
} X_SN_ALIGNMENT(16);

//==============================================================================
// MATRIX 4
//==============================================================================

X_MS_ALIGNMENT(16) 
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a Column Major 4x4 matrix.
//     With vectors treated as column matrices.
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xvector2
//------------------------------------------------------------------------------
union xmatrix4
{
                            xmatrix4                ( void );
#ifdef X_DEBUG_PLUS
                            xmatrix4                ( const xmatrix4& M );
#endif
    
                            xmatrix4                ( const xradian3& R );
                            xmatrix4                ( const xquaternion& Q );
                            xmatrix4                ( const f32x4& C1, const f32x4& C2, 
                                                      const f32x4& C3, const f32x4& C4 );
									 //Note, because +Z is backwards in a conventional RH
									 //system, the third vector here is the "back" vector
									 //of your local frame.
                            xmatrix4                ( const xvector3& Right, const xvector3& Up, 
                                                      const xvector3& Back, const xvector3& Translation );
                            xmatrix4                ( f32 m11, f32 m12, f32 m13, f32 m14,
                                                      f32 m21, f32 m22, f32 m23, f32 m24,
                                                      f32 m31, f32 m32, f32 m33, f32 m34,
                                                      f32 m41, f32 m42, f32 m43, f32 m44 );
                            xmatrix4                ( const xvector3d&   Scale,
                                                      const xradian3&    Rotation,
                                                      const xvector3d&   Translation );

                            xmatrix4                ( const xvector3&    Scale,
                                                      const xquaternion& Rotation,
                                                      const xvector3&    Translation );

    void                    Identity                ( void );
    void                    Zero                    ( void );
    xbool                   isIdentity              ( void ) const;
    xbool                   isValid                 ( void ) const;
    void                    SanityCheck             ( void ) const;
    inline xmatrix4&        setup                   ( f32 m11, f32 m12, f32 m13, f32 m14,
                                                      f32 m21, f32 m22, f32 m23, f32 m24,
                                                      f32 m31, f32 m32, f32 m33, f32 m34,
                                                      f32 m41, f32 m42, f32 m43, f32 m44 );
    inline xmatrix4&        setTranslation          ( const xvector3& Translation );
    inline xmatrix4&        setRotation             ( const xradian3& R );
    inline xmatrix4&        setRotation             ( const xquaternion& Q );
    xmatrix4&               setScale                ( f32 Scale );
    xmatrix4&               setScale                ( const xvector3d& Scale );
                                                 
    xmatrix4&               Rotate                  ( const xradian3& R );
    xmatrix4&               RotateX                 ( xradian Rx );
    xmatrix4&               RotateY                 ( xradian Ry );
    xmatrix4&               RotateZ                 ( xradian Rz );
    xmatrix4&               PreRotateX              ( xradian Rx );
    xmatrix4&               PreRotateY              ( xradian Ry );
    xmatrix4&               PreRotateZ              ( xradian Rz );
    xmatrix4&               PreRotate               ( const xquaternion& Rotation );
    xmatrix4&               PreRotate               ( const xradian3& Rotation );
    xradian3                getRotationR3           ( void ) const;
    xmatrix4&               ClearRotation           ( void );
                                                 
    xmatrix4&               Translate               ( const xvector3& Translation );
    xmatrix4&               PreTranslate            ( const xvector3& Translation );
    inline xvector3         getTranslation          ( void ) const;
    xmatrix4&               ClearTranslation        ( void );
                                                 
    xmatrix4&               Scale                   ( const xvector3d& Scale );
    xmatrix4&               Scale                   ( f32 Scale );
    xmatrix4&               PreScale                ( f32 Scale );
    inline xmatrix4&        PreScale                ( const xvector3& Scale );
    xvector3                getScale                ( void ) const;
    inline xmatrix4&        ClearScale              ( void );

    xmatrix4&               FullInvert              ( void );
    xmatrix4&               InvertSRT               ( void );
    xmatrix4&               InvertRT                ( void );

    xvector3                RotateVector            ( const xvector3& V ) const;
    xvector3                InvRotateVector         ( const xvector3& V ) const;
    inline xvector3         getBack                 ( void ) const;
    inline xvector3         getUp                   ( void ) const;
    inline xvector3         getRight                ( void ) const;

    xmatrix4&               Transpose               ( void );
    f32                     Determinant             ( void ) const;
    xmatrix4&               Orthogonalize           ( void );

    xvector3                Transform3D             ( const xvector3& V ) const;
    xvector4                Transform3D             ( const xvector4& V ) const;

    xmatrix4&               LookAt                  ( const xvector3& From, const xvector3& To, const xvector3& Up );
    xmatrix4&               Billboard               ( const xvector3& From, const xvector3& To, const xvector3& Up );
	xmatrix4&               OrthographicProjection  ( f32 Width, f32 Height, f32 Near, f32 Far );
    xmatrix4&               OrthographicProjection  ( f32 Left, f32 Right, f32 Bottom, f32 Top, f32 Near, f32 Far );
	xmatrix4&               PerspectiveProjection   ( f32 Left, f32 Right, f32 Bottom, f32 Top, f32 Near, f32 Far);

    xmatrix4&               setup                   ( const xvector3d& Axis, xradian Angle );
    xmatrix4&               setup                   ( const xvector3& From, const xvector3& To, xradian Angle );
    xmatrix4&               setup                   ( const xradian3& Rotation );
    inline xmatrix4&        setup                   ( const xquaternion& Q );
    xmatrix4&               setup                   ( const xvector3&    Scale,
                                                      const xradian3&    Rotation,
                                                      const xvector3&    Translation );
    xmatrix4&               setup                   ( const xvector3&    Scale,
                                                      const xquaternion& Rotation,
                                                      const xvector3&    Translation );
    xvector3                getEulerZYZ             ( void ) const;
    xmatrix4&               setupFromZYZ            ( const xvector3d& ZYZ );

    xmatrix4                getAdjoint              ( void ) const;

    f32                     operator ()				( const s32 i, const s32 j ) const;
    f32&                    operator ()				( const s32 i, const s32 j );
    const xmatrix4&         operator +=             ( const xmatrix4& M );
    const xmatrix4&         operator -=             ( const xmatrix4& M );
    const xmatrix4&         operator *=             ( const xmatrix4& M );
    friend  xvector4        operator *              ( const xmatrix4& M, const xvector4& V );
    friend  xvector3        operator *              ( const xmatrix4& M, const xvector3d& V );
    friend  xvector3        operator *              ( const xmatrix4& M, const xvector3& V );
    friend  xvector2        operator *              ( const xmatrix4& M, const xvector2& V );
    friend  xmatrix4        operator *              ( const xmatrix4& M1, const xmatrix4& M2 );
    friend  xmatrix4        operator +              ( const xmatrix4& M1, const xmatrix4& M2 );
    friend  xmatrix4        operator -              ( const xmatrix4& M1, const xmatrix4& M2 );
    //
    // Missing functions
    //
    /*
    xvector3d         Transform           ( const xvector3d& V ) const;
    void            Transform           (       xvector3d* pDest, 
                                                const xvector3d* pSource, 
                                                s32      NVerts ) const;
    void            Transform           (       xvector4* pDest, 
                                                const xvector4* pSource, 
                                                s32      NVerts ) const;
    */

protected:
	
	f32		acof(s32 r0, s32 r1, s32 r2, s32 c0, s32 c1, s32 c2) const;

    struct
    {
        f32x4   m_Column1;
        f32x4   m_Column2;
        f32x4   m_Column3;
        f32x4   m_Column4;
    };

	 //because the memory layout is column-major
	 //the first index here is the column, not the row, and vice versa.
	 //the (,) and Set operators transpose this for you automatically.
    f32     m_Cell[4][4];

    friend union xquaternion;
    friend void m4_Multiply( xmatrix4& dst, const xmatrix4& src2, const xmatrix4& src1 );
} X_SN_ALIGNMENT(16);

X_MS_ALIGNMENT(16)
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a quaternion. A quaternion is a rotation described as
//     a 4D vector. Where the vector loosely represents an Axis and an Angle.
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xradian3 xradian xmatrix4
//------------------------------------------------------------------------------
union xquaternion
{
                            xquaternion             ( void );    
                            xquaternion             ( const xquaternion& Q );
                   explicit xquaternion             ( f32 X, f32 Y, f32 Z, f32 W );
                            xquaternion             ( const f32x4& Register );
                   explicit xquaternion             ( const xradian3&    R );
                   explicit xquaternion             ( const xvector3& Axis, xradian Angle );
                   explicit xquaternion             ( const xmatrix4&    M );

    xbool                   isValid                 ( void ) const;
    void                    Identity                ( void );
    void                    Zero                    ( void );

    xquaternion&            setup                   ( f32 X, f32 Y, f32 Z, f32 W );
    xquaternion&            setup                   ( const xradian3& R );
    xquaternion&            setup                   ( const xvector3& Axis, xradian Angle );
    xquaternion&            setup                   ( const xvector3& StartDir, const xvector3& EndDir );
    xquaternion&            setup                   ( const xmatrix4& M );

    xquaternion&            setRotationX            ( xradian Rx );
    xquaternion&            setRotationY            ( xradian Ry );
    xquaternion&            setRotationZ            ( xradian Rz );

    f32                     Dot                     ( const xquaternion& Q ) const;
    xquaternion&            Normalize               ( void );
    xquaternion&            NormalizeSafe           ( void );

    xquaternion&            Conjugate               ( void );
    xquaternion&            Invert                  ( void );
    f32                     Length                  ( void ) const;
    f32                     LengthSquared           ( void ) const;
    xvector3                getAxis                 ( void ) const;
    xradian                 getAngle                ( void ) const;
    xradian3                getRotation             ( void ) const;
    xvector3                getLookDirection        ( void ) const;
    xquaternion             getDeltaRotation        ( const xquaternion& B ) const { return xquaternion(*this).Invert() * B; }

    xquaternion&            RotateX                 ( xradian Rx );
    xquaternion&            RotateY                 ( xradian Ry );
    xquaternion&            RotateZ                 ( xradian Rz );
    xquaternion&            PreRotateX              ( xradian Rx );
    xquaternion&            PreRotateY              ( xradian Ry );
    xquaternion&            PreRotateZ              ( xradian Rz );

    xquaternion             BlendFast               ( f32 T, const xquaternion& End ) const;
    xquaternion             BlendAccurate           ( f32 T, const xquaternion& End ) const;

    xquaternion&            Ln                      ( void );
    xquaternion&            Exp                     ( void );

    const xquaternion&      operator =              ( const xquaternion& Q );
    xbool                   operator ==             ( const xquaternion& Q ) const;
    
    const xquaternion&      operator /=             ( f32 Scalar );
    const xquaternion&      operator *=             ( f32 Scalar );
    const xquaternion&      operator *=             ( const xquaternion& Q );
    const xquaternion&      operator -=             ( const xquaternion& Q );
    const xquaternion&      operator +=             ( const xquaternion& Q );

    friend  xquaternion     operator *              ( const xquaternion& Q1, const xquaternion& Q2 );
    friend  xquaternion     operator *              ( f32 Scalar, const xquaternion& Q );
    friend  xquaternion     operator *              ( const xquaternion& Q, f32 Scalar );
    friend  xvector3        operator *              ( const xquaternion& A, const xvector3d& V );
    friend  xquaternion     operator -              ( const xquaternion& Q );
    friend  xquaternion     operator -              ( const xquaternion& Q1, const xquaternion& Q2 );
    friend  xquaternion     operator +              ( const xquaternion& Q1, const xquaternion& Q2 );

public:

    f32x4       m_WZYX;

    struct 
    {
        f32     m_X, m_Y, m_Z, m_W;
    };
} X_SN_ALIGNMENT(16);

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
// See Also:
//     xvector3 xvector3d xmatrix4 xquaternion
//------------------------------------------------------------------------------
struct xtransform
{
    void Blend( const xtransform& From, const f32 T, const xtransform& To )
    {
        m_Scale         = From.m_Scale + T*( To.m_Scale - From.m_Scale );
        m_Rotation      = From.m_Rotation.BlendAccurate( T, To.m_Rotation );
        m_Translation   = From.m_Translation + T*( To.m_Translation - From.m_Translation );
    }

    void Blend( const f32 T, const xtransform& To )
    {
        m_Scale         += T*( To.m_Scale - m_Scale );
        m_Rotation       = m_Rotation.BlendAccurate( T, To.m_Rotation );
        m_Translation   += T*( To.m_Translation - m_Translation );
    }

    void Identity( void )
    {
        m_Translation.Zero();
        m_Scale.Set( 1.0f, 1.0f, 1.0f );
        m_Rotation.Identity();
    }

    void getMatrix( xmatrix4& Matrix )
    {
        Matrix.setup( m_Scale, m_Rotation, m_Translation );
    }

    xvector3        m_Scale;
    xquaternion     m_Rotation;
    xvector3        m_Translation;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a plane. The class is describe by a normal that should
//     remain normalize and D which is the offset of the plane. The plane equation
//     A + B + C + D = 0 equates to m_Normal.m_X + m_Normal.m_Y + m_Normal.m_Z + m_D = 0
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 
//------------------------------------------------------------------------------
struct xplane
{
                            xplane                  ( void );
                            xplane                  ( const xvector3d& P1, 
                                                      const xvector3d& P2,
                                                      const xvector3d& P3 );
                            xplane                  ( const xvector3d& Normal, f32 Distance );
                            xplane                  ( f32 A, f32 B, f32 C, f32 D );
                            xplane                  ( const xvector3d& Normal, const xvector3d& Point );                

    xbool                   isValid                 ( void ) const;
    f32                     Dot                     ( const xvector3d& P ) const;
    f32                     GetDistance             ( const xvector3d& P ) const;
    s32                     GetWhichSide            ( const xvector3d& P ) const;
    void                    ComputeD                ( const xvector3d& P );

    inline void             Setup                   ( const xvector3d& P1, 
                                                      const xvector3d& P2,
                                                      const xvector3d& P3 );
    inline void             Setup                   ( const xvector3d& Normal, f32 Distance );
    inline void             Setup                   ( const xvector3d& Normal, const xvector3d& Point );
    void                    Setup                   ( f32 A, f32 B, f32 C, f32 D );
    xbool                   IntersectLine           ( f32& T, 
                                                      const xvector3d& Start, 
                                                      const xvector3d& Direction ) const;
    xbool                   IntersectLineSegment    ( f32&           t,
                                                      const xvector3d& P0, 
                                                      const xvector3d& P1 ) const;

    void                    GetComponents           ( const xvector3d& V,
                                                            xvector3d& Parallel,
                                                            xvector3d& Perpendicular ) const; 
    void                    GetOrthovectors         ( xvector3d& AxisA,
                                                      xvector3d& AxisB ) const;
    xvector3                GetReflection           ( const xvector3d& V ) const;

    xbool                   ClipNGon                ( xvector3d* pDst, s32& NDstVerts, 
                                                      const xvector3d* pSrc, s32  NSrcVerts ) const;
    xbool                   ClipNGon                ( xvector3* pDst, s32& NDstVerts, 
                                                      const xvector3* pSrc, s32  NSrcVerts ) const;

    xplane&                 operator -              ( void );
    friend xplane           operator *              ( const xmatrix4& M, const xplane& xplane );

public:

    xvector3d   m_Normal;
    f32         m_D;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents three Dimensional Axis Aligned Bounding Box. 
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 
//------------------------------------------------------------------------------
struct xbbox
{
                            xbbox                   ( void );
                            xbbox                   ( const xvector3d& P1 );
                            xbbox                   ( const xvector3d& P1, const xvector3d& P2 );
                            xbbox                   ( const xvector3d* pVerts, const s32 nVerts );
                            xbbox                   ( const xvector3d& Center, const f32 Radius );

    inline xbbox&           Set                     ( const xvector3d& Center, const f32 Radius );
    inline xbbox&           Set                     ( const xvector3d& P1, const xvector3d& P2 ); 

    void                    Zero                    ( void );
    inline void             Identity                ( void );
    xbool                   isValid                 ( void ) const;

    xvector3                GetSize                 ( void ) const;
    xvector3                GetCenter               ( void ) const;
    f32                     GetRadius               ( void ) const;
    f32                     GetRadiusSquared        ( void ) const;
    f32                     GetSurfaceArea          ( void ) const;

    xbool                   InRange                 ( f32 Min, f32 Max ) const;
    xbbox&                  Inflate                 ( const xvector3d& S );
    xbbox&                  Deflate                 ( const xvector3d& S );
    xbbox&                  Transform               ( const xmatrix4& M );

    inline xbool            Intersect               ( const xvector3d&  Point ) const;
    xbool                   Intersect               ( const xbbox&      BBox  ) const;
    xbool                   Intersect               ( const xplane&     Plane ) const;
    xbool                   Intersect               ( f32&              t,
                                                      const xvector3d&  P0, 
                                                      const xvector3d&  P1 ) const;
    xbool                   Intersect               ( const xvector3d* pVert,
                                                      s32              nVerts ) const;
    xbool                   IntersectTriangleBBox   ( const xvector3d& P0,
                                                      const xvector3d& P1,
                                                      const xvector3d& P2 )  const;

    f32                     GetClosestVertex        ( xvector3d& ClosestPoint, const xvector3d& Point ) const;

    void                    GetVerts                ( xvector3d* pVerts, s32 nVerts ) const;
    xbool                   Contains                ( const xbbox&     BBox  ) const;
    xbbox&                  AddVerts                ( const xvector3d* pVerts, s32 NVerts );
    xbbox&                  AddVerts                ( const xvector3* pVerts, s32 NVerts );
    xbbox&                  Translate               ( const xvector3d& Delta );

    inline const xbbox&     operator +=             ( const xbbox&     BBox  );
    inline const xbbox&     operator +=             ( const xvector3d& Point );

    friend  xbbox           operator +              ( const xbbox&    xbbox1, const xbbox&    xbbox2 );
    friend  xbbox           operator +              ( const xbbox&    xbbox,  const xvector3d& Point );
    friend  xbbox           operator +              ( const xvector3d& Point, const xbbox&    xbbox  );

public:

    xvector3d m_Min;
    xvector3d m_Max;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a three Dimensional Sphere. 
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 xbbox
//------------------------------------------------------------------------------
struct xsphere
{
                            xsphere                 ( void );
                            xsphere                 ( const xvector3d& Pos, const f32 R );
                            xsphere                 ( const xbbox& BBox );

    void                    Zero                    ( void );

    void                    Set                     ( const xvector3d& Pos, const f32 R );
    xbbox                   GetBBox                 ( void ) const;

    xbool                   TestIntersect           ( const xvector3d& P0, const xvector3d& P1 ) const;
    s32                     TestIntersection        ( const xplane& Plane ) const;

    s32                     Intersect               ( f32& t0, f32& t1, const xvector3d& P0, const xvector3d& P1 ) const;
    xbool                   Intersect               ( f32& t0, const xvector3d& P0, const xvector3d& P1 ) const;
    xbool                   Intersect               ( const xbbox& BBox ) const;
    xbool                   Intersect               ( const xsphere& xsphere ) const;
    xbool                   Intersect               ( f32&             t0,        
                                                      f32&             t1,        
                                                      const xvector3d& Vel1,       
                                                      const xsphere&   Sph2,      
                                                      const xvector3d& Vel2 );
public:

    xvector3d   m_Pos;
    f32         m_R;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 2D floating point base rectangle. 
//------------------------------------------------------------------------------
struct xrect
{
                            xrect                   ( void );
                            xrect                   ( const xrect& Rect );
                            xrect                   ( f32 l, f32 t, f32 r, f32 b );
    
    void                    setMax                  ( void );
    void                    Zero                    ( void );
    inline xrect&           Set                     ( f32 l, f32 t, f32 r, f32 b );
    xrect&                  setup                   ( f32 X, f32 Y, f32 Size );

    xbool                   Intersect               ( const xrect& Rect );
    xbool                   Intersect               ( xrect& R, const xrect& Rect );

    xbool                   PointInRect             ( const xvector2& Pos ) const;
    xbool                   PointInRect             ( f32 X, f32 Y ) const;
    xrect&                  AddPoint                ( f32 X, f32 Y );
    xrect&                  AddRect                 ( const xrect& Rect  );

    f32                     GetWidth                ( void ) const;
    f32                     GetHeight               ( void ) const;
    xvector2                GetSize                 ( void ) const;
    xvector2                GetCenter               ( void ) const;

    xrect&                  SetWidth                ( f32 W );
    xrect&                  SetHeight               ( f32 H );
    xrect&                  SetSize                 ( f32 W, f32 H );

    xrect&                  Translate               ( f32 X, f32 Y );
    xrect&                  Inflate                 ( f32 X, f32 Y );
    xrect&                  Deflate                 ( f32 X, f32 Y );

    f32                     Difference              ( const xrect& R ) const;
    xbool                   InRange                 ( f32 Min, f32 Max ) const;
    xbool                   IsEmpty                 ( void ) const;

    xrect                   Interpolate             ( f32 T, const xrect& Rect ) const;
    
    xbool                   operator ==             ( const xrect& R ) const;
    xbool                   operator !=             ( const xrect& R ) const;

public:

    f32 m_Left,  m_Top;
    f32 m_Right, m_Bottom;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 2D floating point base rectangle.
//------------------------------------------------------------------------------
struct xrectwh
{
public:
    
    xvector2    m_Pos;
    f32         m_W, m_H;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class represents a 2D integer base rectangle. 
//------------------------------------------------------------------------------
struct xirect
{
                            xirect                  ( void );
                            xirect                  ( const xirect& Rect );
                            xirect                  ( s32 l, s32 t, s32 r, s32 b );

    void                    setMax                  ( void );
    void                    Zero                    ( void );
    inline xirect&          Set                     ( s32 l, s32 t, s32 r, s32 b );

    xbool                   Intersect               ( const xirect& Rect );
    xbool                   Intersect               ( xirect& R, const xirect& Rect );

    xbool                   PointInRect             ( s32 X, s32 Y ) const;
    xirect&                 AddPoint                ( s32 X, s32 Y );
    xirect&                 AddRect                 ( const xirect& Rect  );

    s32                     GetWidth                ( void ) const;
    s32                     GetHeight               ( void ) const;
    xvector2                GetSize                 ( void ) const;
    xvector2                GetCenter               ( void ) const;

    xirect&                 SetWidth                ( s32 W );
    xirect&                 SetHeight               ( s32 H );
    xirect&                 SetSize                 ( s32 W, s32 H );

    xirect&                 Translate               ( s32 X, s32 Y );
    xirect&                 Inflate                 ( s32 X, s32 Y );
    xirect&                 Deflate                 ( s32 X, s32 Y );

    f32                     Difference              ( const xirect& R ) const;
    xbool                   InRange                 ( s32 Min, s32 Max ) const;
    xbool                   IsEmpty                 ( void ) const;

    xbool                   operator ==             ( const xirect& R ) const;
    xbool                   operator !=             ( const xirect& R ) const;

public:

    s32 m_Left,  m_Top;
    s32 m_Right, m_Bottom;
};
