//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
// Author:
//     Tomas Arce
// Summary:
//     There are a few categories of functionality that aren't covered in the other
//     files and which are not large enough to justify a new file.  So, all those
//     bits have been gathered into this file.  And thus, x_plus was created.
// Description:
//  * Categories of functionality:
//    - Integral value alignment.
//    - Bit manipulation
//    - Endian swapping for 16 and 32 bit values.
//    - Quick sort and binary search.
//    - memory manipulation functions
//    - compression
//    - encryption 
//------------------------------------------------------------------------------

xbool        x_PlatformSwapEndian   ( xplatform Platform );
const char*  x_PlatformString       ( xplatform Platform );
xbool        x_PlatformEndian       ( xplatform Platform );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     convert from constant binary to u32 Example: X_BIN( 111 ) == 7
// Description:
//     Is use to convert from a constant binary number to a regular
//     decimal number. Example: X_BIN( 111 ) == 7. X_BIN can do a a max of 
//     16bits worth of binary number ( 1111111111111111 ). So it does the  
//     first 16bits of a dword.
//------------------------------------------------------------------------------
//DOM-IGNORE-BEGIN
#define _XBIN( A, L ) ( (u32)(((((u64)0##A)>>(3*L)) & 1)<<L) )
//DOM-IGNORE-END
#define X_BIN( N ) ((u32)( _XBIN( N,  0 ) | _XBIN( N,  1 ) | _XBIN( N,  2 ) | _XBIN( N,  3 ) | \
                           _XBIN( N,  4 ) | _XBIN( N,  5 ) | _XBIN( N,  6 ) | _XBIN( N,  7 ) | \
                           _XBIN( N,  8 ) | _XBIN( N,  9 ) | _XBIN( N, 10 ) | _XBIN( N, 11 ) | \
                           _XBIN( N, 12 ) | _XBIN( N, 13 ) | _XBIN( N, 14 ) | _XBIN( N, 15 ) ))

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     convert from constant binary to u32
// Description:
//     Works just as X_BIN but it does the upper 16bits of a dword.
// See Also:
//     X_BIN
//------------------------------------------------------------------------------
#define X_BINH( N ) ( X_BIN( N ) << 16 )

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This function is design to convert a number to an align power of 2.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
// Description:
//     The result in the next number greater than or equal to "n" 
//     which is a multiple of "a".  For example, x_Align(57,16) is 64.
// See Also:
//     x_IsAlign x_AlignLower xalingof xmem_aligment
//------------------------------------------------------------------------------
template< class T > T x_Align( T Addr, s32 AlignTo );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This function is design to convert a number to an align power of 2.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
// Description:
//     The result in the next number less than or equal to "n" 
//     which is a multiple of "a".  For example, x_AlignLower(57,16) is 48.
// See Also:
//     x_Align x_IsAlign xalingof xmem_aligment
//------------------------------------------------------------------------------
template< class T > T x_AlignLower( T Addr, s32 AlignTo );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro checks whether a number is align to a power of 2.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
// Description:
//     This macro result in a TRUE/FALSE which indicates whether a number/address/offset
//     is align to a certain power of two provided by the user.
// See Also:
//     x_Align x_AlignLower xalingof xmem_aligment
//------------------------------------------------------------------------------
template< class T > xbool x_IsAlign( T Addr, s32 AlignTo );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     returns the aligment of the type
// Arguments:
//     the type
// Description:
//     returns the aligment of the type
// See Also:
//     x_Align x_AlignLower xmem_aligment
//------------------------------------------------------------------------------
#ifdef X_TARGET_PC
    #if (_MSC_VER < 1900)
        template<class T> s32 _x_alignof( void ) { struct one { T A; u8 b; }; return ( sizeof( one ) - sizeof( T ) ); }
        #define xalingof(a) _x_alignof<a>()
    #else
        #define xalingof(a) alignof(a)
    #endif
#else
    #define xalingof(a) alignof(a)
#endif

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro is used to get the memory location of a data member.
// Arguments:
//     CLASS    - Is the actual type that the member belongs to.
//     MEMBER   - Is the name of the member variable that you want to know its location.
// Description:
//     The macro is used to find the memory location of a member variable.
//------------------------------------------------------------------------------
#define X_MEMBER_OFFSET(CLASS,MEMBER)  ((s64)&(((CLASS*)0)->MEMBER))

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro is used to turn a bit on.
// Arguments:
//     A - Is the bit number that you want to turn on.
// Description:
//     This macro is used to turn a bit on. careful not to pass unfriendly
//     bit manipulation types such f32
// See Also:
//     x_FlagOn x_FlagOff x_FlagIsOn
//------------------------------------------------------------------------------
#define X_BIT(A) ((1)<<(A))

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Function swaps the endian of a given atomic type. There is one function per type.
// Arguments:
//     A - Where A is a number to be endian swapped. 
// Returns:
//     Is the same as the source except with the endian swapped.
// Description:
//     The LITTLE_ENDIAN_xx and BIG_ENDIAN_xx macros convert the Endian of 16 or 32
//     bit values between the system "native" Endian to the specified Endian.  Note
//     that these macros work for both reading and writing.  Or, said another way, 
//     these macros toggle when the system native Endian does NOT match the macro 
//     name, and do nothing when the system native does match the macro name.
// See Also:
//     X_LITTLE_ENDIAN X_BIG_ENDIAN X_INTEL_ENDIAN
//------------------------------------------------------------------------------
u8  x_EndianSwap( const u8  A );

#ifdef X_LITTLE_ENDIAN
    #define x_NetworkEndian(X)      x_EndianSwap( X )
#else
    #define x_NetworkEndian(X)		(X)
#endif

//==============================================================================
// x_FlagToggle -   Toggles a bit flag on or off depending of its previous status
// x_FlagOn     -   Turns a bit on in an atomic integer type. 
// x_FlagOff    -   Turns a bit off in an atomic integer type. 
// x_FlagIsOn   -   Test whether a bit is on or off.
//==============================================================================

template< class ta > inline void    x_FlagToggle(       ta& N, const u32 F );
template< class ta > inline void    x_FlagOn    (       ta& N, const u32 F );
template< class ta > inline void    x_FlagOff   (       ta& N, const u32 F );
template< class ta > inline xbool   x_FlagIsOn  ( const ta  N, const u32 F );
template< class ta > inline xbool   x_FlagsAreOn( const ta  N, const u32 F );

//------------------------------------------------------------------------------
// Summary:
//     It is a function used to test whether an integer number is a power of two.
// Author:
//     Tomas Arce
// Arguments:
//     N    - Test if a number is a power of two.
// Returns:
//     TRUE if it is a power of two, FALSE if it is not.
// Description:
//     It is a function used to test whether an integer number is a power of two.
//------------------------------------------------------------------------------
template< class ta > xbool x_IsPow2( ta  N );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Rounds an integer number to the next power of two.
// Arguments:
//     x - the number in question
// Returns:
//     The next power of two.
// Description:
//     Rounds an integer number to the next power of two.
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagIsOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > ta x_Pow2RoundUp( ta x );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Returns the minimum pow2 needed to represent the number
// Arguments:
//     x - the number in question
// Returns:
//     The next power of two.
// Description:
//     Returns the minimum pow2 needed to represent the number
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagIsOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class ta > ta x_Pow2MinimumRequire( ta x );

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Define a "standard compare function" type.
// Description:
//     Functions which match this signature take the addresses of two items
//     (provided as void pointers) and return a value indicating the relationship
//     between the two items.  The 
// Arguments:
//     pItem1 - Is a void pointer and is up to the function to make sense of it.
//     pItem2 - Is a void pointer and is up to the function to make sense of it.
// Returns:
//     Returns a integer which its value range is from negative to positive.
//     The signes of the return value has his meaning:
//     <TABLE>
//     Value Return   Description
//     ============   -------------------------
//      < 0           Indicates   Item1 < Item2
//      = 0           Indicates   Item1 = Item2
//      > 0           Indicates   Item1 > Item2
//     </TABLE>
//  Functions x_strcmp and x_stricmp fit the compare function pattern and can be
//  used as compare functions.
//
//  This type definition is used by the quick sort and binary search functions. 
// See Also:
//     x_qsort x_bsearch
//------------------------------------------------------------------------------
typedef s32 x_compare_fn( const void* pItem1, const void* pItem2 );

//==============================================================================
//  FUNCTIONS
//==============================================================================

//==============================================================================
//  
//  x_qsort     - Optimized quick sort function.
//
//  x_bsearch   - Binary search function.  Return value is address of desired
//                item in the list, or NULL if item was not found.
//  
//  x_bsearch   - Has an extra parameter.  Like x_bsearch(), but, if not found, 
//                uses the extra parameter to report where the item would go if 
//                it were added to the list.
//  
//  The quick sort function is recursive.  It is guaranteed that it will not 
//  recurse more than "log base 2 of NItems".  Use of x_qsort in a critical 
//  program (such as a game) should be tested under the most extreme potential 
//  conditions to prevent stack overflows.
//  
//  The input to the binary search functions must be a list sorted in ascending 
//  order.  The standard ANSI bsearch function does not allow for duplicates in 
//  the list.  The x_files versions handle duplicates.  The first item which 
//  matches will be returned.
//
//==============================================================================
                                  
void x_qsort    ( const void*     pBase,          // Address of first item in array.
                  s32             NItems,         // Number of items in array.
                  s32             ItemSize,       // Size of one item.
                  x_compare_fn*   pCompare );     // Compare function.

void* x_bsearch ( const void*     pKey,           // Reference item to search for.
                  const void*     pBase,          // First item in the array.
                  s32             NItems,         // Number of items in array.
                  s32             ItemSize,       // Size of one item.
                  x_compare_fn*   pCompare );     // Compare function.
                  
void* x_bsearch ( const void*     pKey,           // Reference item to search for.
                  const void*     pBase,          // First item in the array.
                  s32             NItems,         // Number of items in array.
                  s32             ItemSize,       // Size of one item.
                  x_compare_fn*   pCompare,       // Compare function.
                  void*&          pLocation );    // Where item should be in list.


//==============================================================================
// memory manipulation functions
//==============================================================================
void*   x_memset    ( void* pBuf, s32 C, s32 Count );
void*   x_memmove   ( void* dest, const void* src, s32 count );
void*   x_memcpy    ( void* dest, const void* src, s32 count );
u32     x_memCRC32  ( const void* pBuf, s32 Count, u32 CRC = 0x00000000 );
u16     x_memCRC16  ( const void* pBuf, s32 Count, u16 CRC = 0x0000 );
s32     x_memcmp    ( const void* pBuf1, const void* pBuf2, s32 Count );
u32     x_memHash   ( void* pBuf, s32 nBytes, u32 Range = 0xffffffff, u32 hVal = 0x811c9dc5 );

template< class T > inline void x_Swap( T& a, T& b ) { const T c(a); a=b; b=c; }

//==============================================================================
// Compression routines
//==============================================================================
template< class T > class xptr;

s32     x_CompressMem   ( xptr<xbyte>& DestCompress, const void* pSrcUncompressBlock, s32 SrcSize );
s32     x_CompressMem   ( xptr<xbyte>& DestCompress, s32 DestOffset, const void* pSrcUncompressBlock, s32 SrcSize );
void    x_DecompressMem ( void* pDestUncompress, s32 DestSize, const void* pSrcCompressBlock, s32 SrcSize );

//==============================================================================
//
// The encryption functions is used for encrypting and decrypting 64-bit blocks of data 
// with the ICE (Information Concealment Engine) encryption algorithm.
//
// To encrypt and decrypt you always need a level. The level of encryption 
// determines the size of the key, and hence its speed. Level 0 uses the Thin-ICE variant, 
// which is an 8-round cipher taking an 8-byte key. This is the fastest option, and is 
// generally considered to be at least as secure as DES, although it is not yet certain 
// whether it is as secure as its key size. 
//
// For levels n greater than zero, a 16n-round cipher is used, taking 8n-byte keys. 
// Although not as fast as level 0, these are very very secure.
//
// Example:
//<CODE>
//      void main( void )
//      {
//          const char* pSomething = "What were you doing Sunday, February 17, 2002 around 2PM? Well, some of us went down to"
//                                   "Tower Records in Sherman Oaks and saw Something Corporate perform live at an in store for"
//                                   "the EP, 'Audioboxer.' I recorded this footage when I was with MCA Records but unfortunately,"
//                                   "I never was able to get it up on the SoCo site because I lost the tape. Well, I found it. Four"
//                                   "years later. So, here it is, enjoy.";
//          s32 Level           = 1;
//          s32 InLength        = x_strlen( pSomething );
//          s32 KeySize         = x_EncryptComputeKeySize( Level );           
//          s32 EncryptDestSize = x_EncryptComputeDestDataSize(InLength);
//
//          xptr<xbyte> Key;
//          xptr<xbyte> Dest;
//          xptr<xbyte> Out;
//
//          Key.Alloc( KeySize );
//          Dest.Alloc( EncryptDestSize );
//          Out.Alloc( InLength );
//
//          // compute a key for the encryption
//          x_EncryptBuildKeyFromPassword( &Key[0], KeySize, Level, "My Password" );
//
//          // encrypt
//          x_EncryptMem( &Dest[0], EncryptDestSize, Level, &Key[0], KeySize, pSomething, InLength );
//
//          // dencrypt
//          x_DencryptMem( &Out[0], InLength, Level, &Key[0], KeySize, &Dest[0], EncryptDestSize );
//
//          ASSERT( x_memcmp( pSomething, &Out[0], InLength ) == 0 );
//       }
//</CODE>
//==============================================================================

s32     x_EncryptComputeKeySize         ( s32 Level );
s32     x_EncryptComputeDestDataSize    ( s32 SrcDataSize );
void    x_EncryptBuildRandomKey         ( void* pKey, s32 KeySize, s32 Level );
void    x_EncryptBuildKeyFromPassword   ( void* pKey, s32 KeySize, s32 Level, const char* pPassWord );
void    x_EncryptMem                    ( void* pDest, s32 DestDataSize, s32 Level, const void* pKey, s32 KeySize, const void* pSrcData, s32 SrcDataSize );
void    x_DencryptMem                   ( void* pDest, s32 DestDataSize, s32 Level, const void* pKey, s32 KeySize, const void* pSrcData, s32 SrcDataSize );
