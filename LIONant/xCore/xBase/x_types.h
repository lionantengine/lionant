//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
// Author:
//     Tomas Arce
// Summary:
//     This file assists with cross platform development by providing a standard
//     set of type definitions.
// Description:
//     This file contains all basic types needed to create a homogeneous cross 
//     platform environment. Only key base types and definitions are included here. 
//------------------------------------------------------------------------------
#ifdef TARGET_PC
    #include <xmmintrin.h>
#endif

#ifdef TARGET_ANDROID
    #include <sys/types.h>
#endif

#ifdef TARGET_360
    #include <xtl.h>
#endif

#ifdef TARGET_3DS
    #include <nn/types.h>
#endif

//==============================================================================
//  HANDLE BOOL 
//==============================================================================
//DOM-IGNORE-BEGIN

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

#define FALSE           0
#define TRUE            1

// Deal with NULL pointer
#ifdef NULL
    #undef NULL
#endif
#define NULL nullptr

//==============================================================================
//  MAX TYPES
//==============================================================================

#define U8_MAX                           255
#define U8_MIN                             0
                                
#define U16_MAX                        65535
#define U16_MIN                            0
                               
#define U32_MAX             ((u32)4294967295)
#define U32_MIN                            0

#define U64_MAX       u64(0xffffffffffffffff)
#define U64_MIN                            0

#define S8_MAX                           127
#define S8_MIN                          -128
                            
#define S16_MAX                        32767
#define S16_MIN                       -32768
                            
#define S32_MAX             ((s32)2147483647)
#define S32_MIN            -((s32)2147483647)

#define S64_MAX          9223372036854775807
#define S64_MIN         -9223372036854775808

#define F32_MIN             1.175494351e-38f    // Smallest POSITIVE f32 value.
#define F32_MAX             3.402823466e+38f

#define F64_MIN      2.2250738585072014e-308    // Smallest POSITIVE f64 value.
#define F64_MAX      1.7976931348623158e+308

//==============================================================================

#ifdef TARGET_PC
    typedef unsigned char           u8;
    typedef unsigned short          u16;
    typedef unsigned int            u32;
    typedef unsigned __int64        u64;
    typedef   signed char           s8;
    typedef   signed short          s16;
    typedef   signed int            s32;
    typedef   signed __int64        s64;
    typedef          float          f32;
    typedef          double         f64;
    typedef size_t                  xalloc_size;
    typedef wchar_t                 wchar;

    #define thread_local            __declspec(thread)
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_360
    typedef unsigned char           u8;
    typedef unsigned short          u16;
    typedef unsigned int            u32;
    typedef unsigned __int64        u64;
    typedef   signed char           s8;
    typedef   signed short          s16;
    typedef   signed int            s32;
    typedef   signed __int64        s64;
    typedef          float          f32;
    typedef          double         f64;
    typedef u32                     xalloc_size;

    #define thread_local            __declspec(thread)
    typedef wchar_t                 wchar;
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_PS3
	typedef unsigned char           u8;
	typedef unsigned short          u16;
	typedef unsigned int            u32;
	typedef unsigned long long      u64;
	typedef   signed char           s8;
	typedef   signed short          s16;
	typedef   signed int            s32;
	typedef   signed long long      s64;
	typedef          float          f32;
	typedef          double         f64;
	typedef u32                     xalloc_size;

    #define thread_local            __thread
    typedef char16_t                wchar;
#endif

//------------------------------------------------------------------------------

#ifdef TARGET_IOS
    typedef unsigned char           u8;
    typedef unsigned short          u16;
    typedef unsigned int            u32;
    typedef unsigned long long      u64;
    typedef   signed char           s8;
    typedef   signed short          s16;
    typedef   signed int            s32;
    typedef   signed long long      s64;
    typedef          float          f32;
    typedef          double         f64;
    typedef long unsigned int       xalloc_size;

    #define thread_local            __thread
    typedef char16_t                wchar;
#endif

#ifdef TARGET_ANDROID
    typedef unsigned char           u8;
    typedef unsigned short          u16;
    typedef unsigned int            u32;
    typedef unsigned long long      u64;
    typedef   signed char           s8;
    typedef   signed short          s16;
    typedef   signed int            s32;
    typedef   signed long long      s64;
    typedef          float          f32;
    typedef          double         f64;
    typedef size_t                  xalloc_size;

    #define thread_local            __thread
    typedef char16_t                wchar;
#endif



#ifdef TARGET_MARMALADE
    typedef unsigned char           u8;
    typedef unsigned short          u16;
    typedef unsigned int            u32;
    typedef unsigned long long int	u64;
    typedef   signed char           s8;
    typedef   signed short          s16;
    typedef   signed int            s32;
    typedef   signed long long      s64;
    typedef          float          f32;
    typedef          double         f64;
    typedef size_t                  xalloc_size;
    typedef char16_t                wchar;
#endif



#ifdef TARGET_OSX

    #ifdef X_TARGET_64BIT
        typedef unsigned char       u8;
        typedef unsigned short      u16;
        typedef unsigned int        u32;
        typedef unsigned long       u64;
        typedef   signed char       s8;
        typedef   signed short      s16;
        typedef   signed int        s32;
        typedef   signed long       s64;
        typedef          float      f32;
        typedef          double     f64;
        typedef __SIZE_TYPE__       xalloc_size;
    #else
        typedef unsigned char       u8;
        typedef unsigned short      u16;
        typedef unsigned int        u32;
        typedef unsigned long long  u64;
        typedef   signed char       s8;
        typedef   signed short      s16;
        typedef   signed int        s32;
        typedef   signed long long  s64;
        typedef          float      f32;
        typedef          double     f64;
        typedef __SIZE_TYPE__       xalloc_size;
    #endif

    #define thread_local            __thread
    typedef char16_t                wchar;

#endif

#ifdef TARGET_3DS
        typedef size_t              xalloc_size;
#endif

// Deal with vector math
#ifdef SSE2_SUPPORT
    typedef          __m128         f32x4;
#elif defined TARGET_360
    typedef          XMVECTOR       f32x4;
#else
    struct                          f32x4
    {
        f32 x, y, z, w;
    };
#endif

//------------------------------------------------------------------------------
// DERIVED TYPES
//------------------------------------------------------------------------------

#ifdef X_TARGET_64BIT
    typedef             u64         xuptr;
#else
    typedef             u32         xuptr;
#endif

typedef u8                          xbyte;
typedef u32                         xbool;


//DOM-IGNORE-END
//------------------------------------------------------------------------------

#include <stdarg.h>

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//
//------------------------------------------------------------------------------
#define x_va_start(list,prev)   va_start(list,prev)
#define x_va_end(list)          va_end(list)
#define x_va_arg(list,mode)     va_arg(list,mode)

typedef va_list xva_list;

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
// C++ 11 Standard Iterator for xBase structures 
//------------------------------------------------------------------------------
template< class T, class B >
struct xiter
{
                    xiter           ( s32 i, T& Ref ) : m_i(i), m_Ref(Ref){}
    bool            operator!=      ( const xiter& other ) const    { return m_i != other.m_i; }
    const xiter&    operator++      ( void )                        { ++m_i; return *this; }
 
    const B&        operator*       ( void ) const  { return m_Ref[m_i]; }
    B&              operator*       ( void )        { return m_Ref[m_i]; }

    s32   m_i;
    T&    m_Ref;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
// C++ 11 Standard Iterator for xBase structures 
// This is version 2 of the xbase standard iterator meant to work with version 2
// of containers and xptr.
//------------------------------------------------------------------------------
template< class T >
struct xiter2
{
    typedef typename T::entry_type entry_type; 

                            xiter2          ( T& Ref ) : m_Ref(Ref){}
                            xiter2          ( s32 i, T& Ref ) : m_Ref(Ref){ m_Ref.getMutableIterator() = i; }
    bool                    operator!=      ( const xiter2& End ) const   { return m_Ref.getIterator() != End.m_Ref.getCount(); }
    const xiter2&           operator++      ( void )                      { ++m_Ref.getMutableIterator(); return *this; }
    const entry_type&       operator*       ( void ) const                { return m_Ref[m_Ref.getIterator()]; }
    entry_type&             operator*       ( void )                      { return m_Ref[m_Ref.getIterator()]; }

    T&    m_Ref;
};


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
// simple replacement for std::function
//------------------------------------------------------------------------------
template< typename F > class x_function;
template< typename R, typename ... A>
class x_function< R(A...) >
{
public:
            
    x_function( void ) = default;
            
    template<typename T>
    x_function( const T& functor )
    {
        // If this assert kicks in then our storage object may be too small
        // perhaps make it into an array
#ifdef X_ASSERT
        static_assert( sizeof(m_Storage) >= sizeof(T), "Stoge Size for x_function is too small, increase m_Storage if needed" );
#endif 
        // Contruct the struct.
        x_Construct( ((p_intern<T>*)&m_Storage), functor );
                
        // Set the helper
        m_FuncHelper = &p_intern<T>::Invoke;
    }
            
    R operator()( A... args )
    {
        return m_FuncHelper( &m_Storage, args... );
    }
            
private:
            
    template<typename T>
    struct p_intern
    {
        const T  m_pRealFunctor;
                
        p_intern( const T& functor ) : m_pRealFunctor( functor ) { }
                
        static R Invoke( void* Ptr, A... Args )
        {
            const p_intern<T>* Self = reinterpret_cast<p_intern<T>*>( Ptr );
            return Self->m_pRealFunctor( Args... );
        }
    };
            
    typedef  R(* func_helper)(void*,  A...);
            
    xuptr           m_Storage[8];       // Keep the data used to call the function
    func_helper     m_FuncHelper;       // Pointer to the Invoke function ones all the type magic has happen
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     If you never check your arrays for out of bounds situations you should use this class.
//     This class checks every time the user access its contents. It is pretty transparent 
//     and provides some additional features which makes the class very convenient and clean.
//     GetCount will get the size of the array and while it is a function the optimizer will
//     convert it into a constant. Other utility functions such Copy and SetMem are created
//     as well to provided a bullet proof chunk of data. 
// Example:
//<CODE>
//     void main( void )
//      {
//          xsafe_array<s32,10>  m_List;
//
//          // No need for asserting we are always safe
//          for( s32 i=0; i<m_List.getCount(); i++ )
//          {
//              m_List[i] = x_rand();
//          }
//      }
//</CODE>
// See also:
//     xarray xharray xblock_array xptr_ref
//------------------------------------------------------------------------------
template<class T> class xptr;

template< class T, s32 Count >
struct xsafe_array
{
public:
        enum
        {
            SIZE = Count
        };
    
    typedef T type;
    typedef T entry_type; //xsafe_array<T,Count> entry_type;
    
public:

    inline                    xsafe_array             ( void ) = default;
    inline                    xsafe_array             ( s32 C )			{ SetMemory(C);   }
    inline	  s32             getCount                ( void ) const	{return Count; }
    inline			  		  operator T*             ( void )			{return m_Data; }
    inline		              operator const T*       ( void ) const	{return m_Data; }

    template<class I>
    inline    T&              operator[]              ( I Index );

    template<class I>
    inline const T&           operator[]              ( I Index ) const;

    xiter< xsafe_array<T,Count>, T > begin                 ( void )     { return xiter< xsafe_array<T,Count>, T >(0,*this); }
    xiter< xsafe_array<T,Count>, T > end                   ( void )     { return xiter< xsafe_array<T,Count>, T >(Count,*this); }

    xiter< const xsafe_array<T,Count>, const T > begin                 ( void ) const    { return xiter< const xsafe_array<T,Count>, const T >(0,*this); }
    xiter< const xsafe_array<T,Count>, const T > end                   ( void ) const    { return xiter< const xsafe_array<T,Count>, const T >(Count,*this); }
    
    template< class K >
    inline    void            Copy                    ( s32 DestIndexOffset, const xptr<K>& SrcRef, s32 OffsetIndexOfSource = 0, s32 Length = -1 ) { Copy( DestIndexOffset, &SrcRef[OffsetIndexOfSource+Length], (Length == -1)?SrcRef.getCount():Length ); }
    
    inline    void            Copy                    ( s32 DestIndexOffset, const T* pSrc, s32 size );
    inline    void            SetMemory               ( s32 C );

protected:

    T    m_Data[ Count ];
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is used to define a handle which is 32bits wide. Handles have the property
//     that can be pass to function as a unique type. This makes function type checking much 
//     safer. The only thing which is standard in a handle is its NULL value which is xhandle::HNULL
//     while it has a member variable and it is public it is not recommended to access it if
//     you don't have to.
// See Also:
//     X_HANDLE_TYPE
//------------------------------------------------------------------------------
struct xhandle
{
    u32 m_Handle;

    enum flags:u32
    {
        HNULL = 0xffffffff
    };

//    inline explicit       xhandle   ( u32 Value )           { m_Handle = Value;            }
    inline                xhandle   ( void ) = default;
    inline explicit       xhandle   ( flags Value )         { m_Handle = Value;            }
    inline xbool          isValid   ( void ) const          { return m_Handle != HNULL;    }    
    inline xbool          IsNull    ( void ) const          { return m_Handle == HNULL;    }    
    inline void           setNull   ( void )                { m_Handle = HNULL;            }  
    inline u32            Get       ( void ) const          { return m_Handle;             } 
    inline void           Set       ( u32 V )               { m_Handle=V;                  } 
    inline bool operator  ==        ( xhandle H ) const     { return H.m_Handle==m_Handle; }
    inline bool operator  !=        ( xhandle H ) const     { return H.m_Handle!=m_Handle; }
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Macro used to create units in c++
// Arguments:
//------------------------------------------------------------------------------

#define x_units( ATOMIC_TYPE, NAME , ... ) struct NAME { NAME()=default; explicit NAME(ATOMIC_TYPE x) : m_Value(x){} __VA_ARGS__ ATOMIC_TYPE m_Value; } 
//#define x_units_from(CONVERT_TYPE, NAME, VARS ) NAME ( const CONVERT_TYPE& C ) : m_Value( C.VARS ) {} 


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     Macro used to create units in c++
// Arguments:
//------------------------------------------------------------------------------

#define x_const(A) ( *(const decltype(A)*) &A )

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro it is used to create a unique handle type.
// Arguments:
//     TYPE_NAME  - Name of the type
// Description:
//     When creating classes that uses handles it is convenient to create unique types for
//     user safety reasons. This macro allows to create a unique handle type with the same 
//     functionality as the xhandle. 
// See Also:
//     xhandle
//------------------------------------------------------------------------------
#define X_HANDLE_TYPE( TYPE_NAME )                                                                  \
struct TYPE_NAME : public xhandle                                                                   \
{                                                                                                   \
    inline                  TYPE_NAME               ( void )                    {}                                  \
    inline explicit         TYPE_NAME               ( u32 Value )               { m_Handle = Value;            }    \
    inline xbool            operator  ==            ( xhandle H ) const         { return H.m_Handle==m_Handle; }    \
    inline xbool            operator  !=            ( xhandle H ) const         { return H.m_Handle!=m_Handle; }    \
};
//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro is used to defined link list functions for your container. 
// Arguments:
//     PREFIX       - This is a prefix for the function name. Usually use so you can have multiple link lists
//     TYPE         - This is the type of the container that you are putting this macro on.
//     NEXT         - Name of the member variable to be use as the next pointer
//     PREV         - Name of the member variable to be use as the prev pointer
// Description:
//     Link List helper functions. This macro defines basic functions inside a
//     container that are used to manipulate a circular link list. There is a list of 
//     functions that it defines:
//<CODE>
//     inline void          PREFIX##Insert          ( TYPE*& pHead )              
//     inline void          PREFIX##InsertBefore    ( TYPE*& pHead, TYPE* pNode )
//     inline void          PREFIX##InsertAfter     ( TYPE* pNode )          
//     inline void          PREFIX##Append          ( TYPE*& pHead )              
//     inline static void   PREFIX##Merge           ( TYPE* pHeadA, TYPE* pHeadB )
//     inline TYPE*         PREFIX##IteratorNext    ( const TYPE* const pHead ) const 
//     inline TYPE*         PREFIX##IteratorPrev    ( const TYPE* const pHead ) const 
//     inline TYPE*         PREFIX##Remove          ( TYPE*& pHead )             
//</CODE>
//
//<TABLE>
//     Functions                              Description
//     -----------------------------------    ---------------------------------------------------------
//     Insert / InsertBefore / InsertAfter    All these just inserts a node in the list
//     Append                                 Also inserts a node in the list but at the end
//     IteratorNext / InteratorPrev           There functions are use to get the next element in a loop
//     Remove                                 Removes a node from the list. Note that it returns the next usefull node.
//     Merge                                  Mergest two link lists togetter. Note that the final list is return as part of pHeadA
//</TABLE>
//
// Example:
//<CODE>
//      struct paco 
//      {
//          X_LL_CIRCULAR_FUNCTIONS(LLC, paco, m_pNext,m_pPrev);
//          paco*   m_pNext;
//          paco*   m_pPrev;
//      };
//</CODE>
//
//     After the container has been defined with the functions then you can call
//     them up like this:
//
//<CODE>
//      void main( void );
//      {
//          xsafe_array<paco,100> Buffer;
//          paco*                 pHead = NULL;
//
//          // Insert nodes
//          for( s32 i=0; i<Buffer.getCount(); i++ )
//          {
//              s32 Count = 0;
//              Buffer[i].LLCInsert( pHead ); 
//
//              // Check the link list forward
//              for( paco* pTemp = pHead; pTemp ; pTemp = pTemp->LLCIteratorNext( pHead ) )
//              {
//                  Count++;
//              }
//              // check the link list backwards
//              for( paco* pTemp = pHead; pTemp ; pTemp = pTemp->LLCIteratorPrev( pHead ) )
//              {
//                  Count--;
//              }
//              ASSERT( Count == 0 );
//          }
//      }
//</CODE>
//
//     If you want to errase some node in a safe way try doing it this way
//
//<CODE>
//      void ErraseRandom( paco* pHead )
//      {
//          paco* pNext;
//          paco* pTemp; 
//          for( pTemp = pHead; pTemp; pTemp = pNext )
//          {
//              if( x_rand()&1 )
//              {
//                  pNext = pTemp->LLCRemove( pHead );
//                  x_free( pTemp );
//              }
//              else
//              {
//                  pNext = pTemp->LLCIteratorNext( pHead );
//              }
//          }
//      }
//</CODE>
// See Also:
//     X_LL_CIRCULAR_ARRAY_FUNCTIONS
//------------------------------------------------------------------------------
#define X_LL_CIRCULAR_FUNCTIONS(PREFIX,TYPE,NEXT,PREV) 

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Summary:
//     This macro is used to defined link list functions for your container. 
// Arguments:
//     PREFIX       - This is a prefix for the function name. Usually use so you can have multiple link lists
//     NODETYPE     - This is the type of the node, which usually is the structure containing next and previous.
//     INDEXTYPE    - This is the type of the index to the nodes.
//     NEXT         - Name of the member variable to be use as the next pointer
//     PREV         - Name of the member variable to be use as the prev pointer
// Description:
//     Link List helper functions. This macro defines basic functions inside a
//     container that are used to manipulate a circular index link list. There is a list of 
//     functions that it defines. Check X_LL_CIRCULAR_FUNCTIONS for farther reference.
//     Note that you always have to pass the array which contains the nodes. We also 
//     define the NULL as NODETYPE::PREFIX##_NULL which should always be safer than 
//     just comparing it against (-1 or ~0) which is the default value for NULL. 
//
//<CODE>
//     inline void          PREFIX##Insert          ( NODETYPE* pArray, INDEXTYPE& iHead )              
//     inline void          PREFIX##InsertBefore    ( NODETYPE* pArray, INDEXTYPE& iHead, INDEXTYPE iNode )
//     inline void          PREFIX##InsertAfter     ( NODETYPE* pArray, INDEXTYPE iNode )          
//     inline void          PREFIX##Append          ( NODETYPE* pArray, INDEXTYPE& iHead )              
//     inline static void   PREFIX##Merge           ( NODETYPE* pArray, INDEXTYPE iHeadA, INDEXTYPE iHeadB )
//     inline INDEXTYPE     PREFIX##IteratorNext    ( NODETYPE* pArray, const INDEXTYPE iHead ) const 
//     inline INDEXTYPE     PREFIX##IteratorPrev    ( NODETYPE* pArray, const INDEXTYPE iHead ) const 
//     inline INDEXTYPE     PREFIX##Remove          ( NODETYPE* pArray, INDEXTYPE& iHead )             
//</CODE>
// See Also:
//     X_LL_CIRCULAR_FUNCTIONS
//------------------------------------------------------------------------------
#define X_LL_CIRCULAR_ARRAY_FUNCTIONS(PREFIX,NODETYPE,INDEXTYPE,NEXT,PREV)


//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     Simple to use, first create a global variable of type xfactory_base*
//     You dont need to worry to define 'extern' for it, just make sure it is well named.
//     This is going to be the variable you will need to give to every definition of XFACTORY
//     XFACTORY should be use inside the CPP file and inside of it declares a variale.
//     Example:
//      First create the factory and give it a good global var name:
//      XFACTORY_VARBASE( g_MyFactory );
//
//      In another CPP file where your instance have functions:
//      XFACTORY_INSTANCE( my_type, g_MyFactory );
//
//      In your instance class definition:
//      XFACTORY_CLASS   ( my_type );
//
//      To create an instance just use: pMyInstance = xfactory::CreateType( StringType, g_MyFactory );
//
//      To destroy a particular Instance do:
//          const xfactory& Factory = pMyInstance->getFactory();
//          Factory.DestroyInstance( pMyInstance );
//
//------------------------------------------------------------------------------
struct xfactory
{
                             xfactory        ( const xfactory* pFactory, const char* pName ) : m_pNext(pFactory), m_pTypeName( pName ){}
    static const xfactory*   FindFactory     ( const char* pType, const xfactory* pVarBase )
    {
        for( const xfactory* pNext = pVarBase; pNext; pNext = pNext->m_pNext )
        {
            s32 i=0;
            for( ; pType[i] && pType[i] == pNext->m_pTypeName[i]; ++i ){}
            if( pType[i] == pNext->m_pTypeName[i] ) return pNext;
        }
        return NULL;
    }
    
    static void*            CreateFromType  ( const char* pType, const xfactory* pVarBase )
    {
        const xfactory* pFactory =  FindFactory( pType, pVarBase );
        if( pFactory ) return pFactory->CreateInstance();
        return NULL;
    }
    
    virtual void*           CreateInstance  ( void ) const          = 0;
    virtual void            DestroyInstance ( void* pPtr ) const    = 0;
    const char* const       m_pTypeName;
    const xfactory* const   m_pNext;
};

#define XFACTORY_CLASS( TYPE )                                              \
    virtual const xfactory&     getVFactory     ( void ) const ;            \
    virtual const char*         getVFactoryName ( void ) const ;            \
    static  const xfactory&     getFactory      ( void );                   \
    static  const char*         getFactoryName  ( void );


#define XFACTORY_VARBASE( FACTORY_VAR_BASE ) xfactory* FACTORY_VAR_BASE = NULL;
#define XFACTORY_INSTANCE( TYPE, FACTORY_VAR_BASE )                         \
extern xfactory* FACTORY_VAR_BASE;                                          \
static struct xfactory_##TYPE : public xfactory                             \
{                                                                           \
    xfactory_##TYPE( void ) : xfactory( FACTORY_VAR_BASE, #TYPE )           \
    { FACTORY_VAR_BASE = this; }                                            \
    virtual void*   CreateInstance  ( void ) const                          \
    { TYPE* pVar = x_new( TYPE, 1, XMEM_FLAG_ALIGN_16B ); return pVar; }    \
    virtual void    DestroyInstance ( void* pPtr ) const                    \
    { TYPE* pType = (TYPE*)pPtr; if( pType ) x_delete( pType ); }           \
} s_Factory_##TYPE;                                                         \
const xfactory& TYPE::getFactory( void )  { return s_Factory_##TYPE; }      \
const char* TYPE::getFactoryName( void )  { return #TYPE; }                 \
const xfactory& TYPE::getVFactory( void ) const { return s_Factory_##TYPE;} \
const char* TYPE::getVFactoryName( void ) const { return #TYPE; }



//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     The rtti it is used to give Run Time Type Information to any structure/class hirarchy 
//     that you want. The type information main priority is to keep casting always safe. 
//     The most common case of casting is when you have a base object trying to be cast
//     to one of its children. But the RTTI can be use for more than just casting. It also
//     can be use to switch between the different types.
//
//<P>  The usage is very straign foward and it should be clear in the example provided.     
// Example:
//<CODE>
// struct base
// {
//     x_rtti_base( base );
// };
//
// struct pepe : public base
// {
//     x_rtti_class1( pepe, base )
//     void Normal( void ){}
// };
//
// struct base2
// {
//     x_rtti_base( base2 );
// };
//
// struct jaime : public base2
// {
//     x_rtti_class1( jaime, base2 )
//     void Normal( void ){}
// };
//
// struct super_pepe : public pepe, jaime
// {
//     x_rtti_class2( super_pepe, pepe, jaime )
//     void Super( void ){}
// };
// 
// void main( void )
// {
//     jaime      JT;
//     super_pepe SP;           // The original class
//     base&      B = SP;       // Some generic base reference
//     base2&     T = SP;       // SP has multiple bases
//     pepe       PP;           // A real pepe
//
//     // We want to safely cast
//     pepe&   P = pepe::SafeCast( B );
//     ASSERT( P.isKindOf( base2::GetRTTI() ) );            // Because of multiple inheritage this works... scarry
//     ASSERT( PP.isKindOf( base2::GetRTTI() ) == FALSE );  // Here it knows better
//     jaime&  J = jaime::SafeCast( T );
//
//     // cast the base class right
//     if( B.isKindOf( super_pepe::GetRTTI() ) )
//     {
//         super_pepe& SP = super_pepe::SafeCast( B );
//         SP.Super();
//     }
//     else if( B.isKindOf( pepe::GetRTTI() ) )
//     {
//         pepe& P = pepe::SafeCast( B );
//         P.Normal();
//     }
// }
//</CODE>
//==============================================================================
class xrtti
{
public:
                xrtti               ( const char* pTypeName );
                xrtti               ( const char* pTypeName, const xrtti& RTTI1 );
                xrtti               ( const char* pTypeName, const xrtti& RTTI1, const xrtti& RTTI2 );
                xrtti               ( const char* pTypeName, const xrtti& RTTI1, const xrtti& RTTI2, const xrtti& RTTI3 );
    xbool       isKindOf            ( const xrtti& RTTI ) const;
    xbool       isKindOf            ( const char* pTypeName ) const;
    const char* getName             ( void ) const { return m_pType; }

//private:

    const char*  const  m_pType;     // String Name for the type
    const xrtti* const  m_pNext1;    // Next type
    const xrtti* const  m_pNext2;    // Next type
    const xrtti* const  m_pNext3;    // Next type
};

//==============================================================================
//==============================================================================
//==============================================================================
// RTTI IMPLEMENTATION
//==============================================================================
//==============================================================================
//==============================================================================
// More in x_types_inline.h

//-------------------------------------------------------------------------------

//DOM-IGNORE-BEGIN
#define PRIVATE_RTTI_FUNCTION_SET( TYPE )                             \
    virtual inline const xrtti& getObjectRTTI( void ) const           \
    {                                                                 \
        return TYPE::getRTTI();                                       \
    }                                                                 \
    inline xbool isKindOf( const char* pTypeString ) const            \
    {                                                                 \
        return getObjectRTTI().isKindOf( pTypeString );               \
    }                                                                 \
    inline xbool isKindOf( const xrtti& RTTI ) const                  \
    {                                                                 \
        return getObjectRTTI().isKindOf( RTTI );                      \
    }                                                                 \
    template<class T>                                                 \
    static inline TYPE& SafeCast( T& Object )                         \
    {                                                                 \
        ASSERT( Object.isKindOf(TYPE::getRTTI()) );                   \
        return *((TYPE*)&Object);                                     \
    }                                                                 \
    template<class T>                                                 \
    static inline const TYPE& SafeCast( const T& Object )             \
    {                                                                 \
        ASSERT( Object.isKindOf(TYPE::getRTTI()) );                   \
        return *((const TYPE*)&Object);                               \
    }

#undef x_rtti_base
#undef x_rtti_class1
#undef x_rtti_class2
#undef x_rtti_class3
//DOM-IGNORE-END

// <COMBINE xrtti>
#define x_rtti_class1( CLSS_NAME, CLSS_PARENT1 )                                                                       \
public:                                                                                                                \
    static inline const xrtti& getRTTI( void )                                                                         \
    {                                                                                                                  \
        static xrtti s_RTTI( #CLSS_NAME, CLSS_PARENT1::getRTTI() );                                                    \
        return s_RTTI;                                                                                                 \
    }                                                                                                                  \
    virtual void* SafeCastInterface( const xrtti& RTTI ) const                                                         \
    {                                                                                                                  \
        const xrtti& Mine = CLSS_NAME::getObjectRTTI();                                                                \
        if( &RTTI == &Mine ) return (CLSS_NAME*)this;                                                                  \
        if( Mine.m_pNext1  ) { void* pPtr = CLSS_PARENT1::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        return NULL;                                                                                                   \
    }                                                                                                                  \
    PRIVATE_RTTI_FUNCTION_SET(CLSS_NAME)                        

// <COMBINE xrtti>
#define x_rtti_class2( CLSS_NAME, CLSS_PARENT1, CLSS_PARENT2 )                                                         \
public:                                                                                                                \
    static inline const xrtti& getRTTI( void )                                                                         \
    {                                                                                                                  \
        static xrtti s_RTTI( #CLSS_NAME, CLSS_PARENT1::getRTTI(), CLSS_PARENT2::getRTTI() );                           \
        return s_RTTI;                                                                                                 \
    }                                                                                                                  \
    virtual void* SafeCastInterface( const xrtti& RTTI ) const                                                         \
    {                                                                                                                  \
        const xrtti& Mine = CLSS_NAME::getObjectRTTI();                                                                \
        if( &RTTI == &Mine ) return (CLSS_NAME*)this;                                                                  \
        if( Mine.m_pNext1  ) { void* pPtr = CLSS_PARENT1::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        if( Mine.m_pNext2  ) { void* pPtr = CLSS_PARENT2::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        return NULL;                                                                                                   \
    }                                                                                                                  \
    PRIVATE_RTTI_FUNCTION_SET(CLSS_NAME)                        

// <COMBINE xrtti>
#define x_rtti_class3( CLSS_NAME, CLSS_PARENT1, CLSS_PARENT2, CLSS_PARENT3 )                                           \
public:                                                                                                                \
    static inline const xrtti& getRTTI( void )                                                                         \
    {                                                                                                                  \
        static xrtti s_RTTI( #CLSS_NAME, CLSS_PARENT1::getRTTI(), CLSS_PARENT2::getRTTI(), CLSS_PARENT3::getRTTI() );  \
        return s_RTTI;                                                                                                 \
    }                                                                                                                  \
    virtual void* SafeCastInterface( const xrtti& RTTI ) const                                                         \
    {                                                                                                                  \
        const xrtti& Mine = CLSS_NAME::getObjectRTTI();                                                                \
        if( &RTTI == &Mine ) return (CLSS_NAME*)this;                                                                  \
        if( Mine.m_pNext1  ) { void* pPtr = CLSS_PARENT1::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        if( Mine.m_pNext2  ) { void* pPtr = CLSS_PARENT2::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        if( Mine.m_pNext3  ) { void* pPtr = CLSS_PARENT3::SafeCastInterface( RTTI );  if( pPtr ) return pPtr; }        \
        return NULL;                                                                                                   \
    }                                                                                                                  \
    PRIVATE_RTTI_FUNCTION_SET(CLSS_NAME)                        

// <COMBINE xrtti>
#define x_rtti_base( CLSS_NAME )                                                \
public:                                                                         \
    static inline const xrtti& getRTTI( void )                                  \
    {                                                                           \
        static xrtti s_RTTI( #CLSS_NAME );                                      \
        return s_RTTI;                                                          \
    }                                                                           \
    virtual void* SafeCastInterface( const xrtti& RTTI ) const                  \
    {                                                                           \
        if( &RTTI == &CLSS_NAME::getObjectRTTI() ) return (CLSS_NAME*)this;     \
        return NULL;                                                            \
    }                                                                           \
    PRIVATE_RTTI_FUNCTION_SET(CLSS_NAME)                              
