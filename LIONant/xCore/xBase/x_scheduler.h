//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

struct base_worker;

/////////////////////////////////////////////////////////////////////////////////
// BASE TRIGGER
/////////////////////////////////////////////////////////////////////////////////

class x_base_trigger : public x_qt_ptr
{
    x_rtti_base(x_base_trigger)

public:
                            x_base_trigger      ( void );
    virtual                ~x_base_trigger      ( void );

            void            AndThenRuns         ( x_base_trigger& Trigger ) { appendDependency( Trigger, TRUE  ); }
            void            OrThenRuns          ( x_base_trigger& Trigger ) { appendDependency( Trigger, FALSE ); }
            void            AndWaitFor          ( x_base_trigger& Trigger ) { Trigger.appendDependency( *this, TRUE ); }
            void            OrWaitFor           ( x_base_trigger& Trigger ) { Trigger.appendDependency( *this, FALSE ); }
            void            setupAutoDelete     ( void ) { m_bDelete    = TRUE; }
            void            setupLocalSync      ( void ) { m_bLocalSync = TRUE; }

protected:

    struct entry : public x_qt_ptr
    {
        u32                 m_bAnd:1,
                            m_bDelete:1;
        x_base_trigger*     m_pPtr;
    };

protected:

    virtual void            doBeforeTrigger     ( void ) {}
    virtual entry&          appendEntry         ( void );
    virtual void            vReset              ( void );
            void            appendDependency    ( x_base_trigger& Trigger, xbool bAnd );

//protected:
public:

    x_qt_popless_queue      m_NotifyQueue;              // List of jobs+triggers to notify
    x_qt_counter            m_RefCount;                 // Indicates when the trigger can be deleted
    x_qt_counter            m_RefAndCount;              // Indicates when the trigger can be trigger
    xbool volatile*         m_pSignal;                  // This allows the local syncronization
    u32                     m_bQuantumWorld:1,          // We are in the quantum world?
                            m_bTiggered:1,              // have we been trigger already?
                            m_bDelete:1,                // after I am done should I be deleted?
                            m_bKillWorker:1,            // For Jobs... should I will the worker?
                            m_bJob:1,                   // am I a job?
                            m_bLight:1,                 // 
                            m_bLocalSync:1,             // 
                            m_bAffinityMainThread:1,
                            m_bAffinityNotMainThread:1;
protected:
    friend class x_scheduler;
};

/////////////////////////////////////////////////////////////////////////////////
// BASE JOB
/////////////////////////////////////////////////////////////////////////////////

class x_base_job : public x_base_trigger
{
    x_rtti_class1(x_base_job, x_base_trigger)

public:

    enum priority
    {
        PRIORITY_BELOW_NORMAL,
        PRIORITY_NORMAL,
        PRIORITY_ABOVE_NORMAL,
        PRIORITY_COUNT,

        PRIORITY_PADDING    = 0xffffffff
    };
    
    enum affinity
    {
        AFFINITY_NORMAL             = 0,
        AFFINITY_MAIN_THREAD        = 1,
        AFFINITY_NOT_MAIN_THREAD    = 2
    };
    

public:
                        x_base_job              ( void );
            void        setupPriority           ( priority Priority ) { m_JobPriority           = Priority; }
            void        setupAffinity           ( affinity Affinity )
            {
                switch( Affinity )
                {
                    case AFFINITY_MAIN_THREAD:     m_bAffinityMainThread = 1;    break;
                    case AFFINITY_NOT_MAIN_THREAD: m_bAffinityNotMainThread = 1; break;
                    default: m_bAffinityMainThread = m_bAffinityNotMainThread = 0;
                }
            }
protected:

    virtual void        onRun                   ( void ) {};
    virtual void        vReset                  ( void );

protected:

    priority              m_JobPriority;       // This is one of the priorities from the 
                                               // enumeration. This priorities are used to set the thread 
                                               // priority and to insert the job into the right queue.
protected:
    friend struct normal_worker_thread;
    friend class x_scheduler;
};

/////////////////////////////////////////////////////////////////////////////////
// LIGHT JOB
/////////////////////////////////////////////////////////////////////////////////

class x_light_job : public x_base_job
{
    x_rtti_class1(x_light_job, x_base_job)

public:
                        x_light_job             ( void );
protected:

    virtual void        onRun                   ( void ) = 0;
};

/////////////////////////////////////////////////////////////////////////////////
// LIGHT TRIGGER
/////////////////////////////////////////////////////////////////////////////////

class x_light_trigger : public x_base_trigger
{
    x_rtti_class1(x_light_trigger, x_base_trigger)

public:
                        x_light_trigger         ( void );
    virtual            ~x_light_trigger         ( void );
    void                LocalSync               ( xbool bLightJob = TRUE );
    void                ManualReset             ( void );

protected:

    volatile xbool      m_LocalSignal;
};

/////////////////////////////////////////////////////////////////////////////////
// SIMPLE JOB
/////////////////////////////////////////////////////////////////////////////////
template< s32 ENTRY_COUNT, class U=x_base_job >
class x_simple_job : public U
{
    x_rtti_class1(x_simple_job, U )

public:
                                        x_simple_job    ( void ) { m_nEntries=0; }

protected:

    virtual x_base_trigger::entry&      appendEntry     ( void ) { return m_lEntries[m_nEntries++]; }
    virtual void                        vReset          ( void ) { m_nEntries=0; U::vReset(); }
    
protected:

    s32                                             m_nEntries;
    xsafe_array<x_base_trigger::entry,ENTRY_COUNT>        m_lEntries;
};

/////////////////////////////////////////////////////////////////////////////////
// INLINE LIGHT JOB
//-------------------------------------------------------------------------------
// Usage Example:
//
//     xsafe_array<x_inline_light_jobs,5>  Jobs;
//     x_light_trigger                     Trigger;
//
//     // Set all the jobs with the right lamda function & Link with trigger
//     for( s32 i=0; i<Jobs.getCount(); i++ )
//        Jobs[Count++].setup( Trigger, [&]()
//        {
//           DoSomeWork();
//        });
//
//     // Tell scheguler to start working
//     for( s32 i=0; i<Jobs.getCount(); i++ )
//        g_Scheduler.StartLightJobChain( Jobs[i] );
//
//     // Wait untill everything is done
//     Trigger.LocalSync();
//
/////////////////////////////////////////////////////////////////////////////////

struct x_inline_light_jobs : public x_simple_job<1,x_light_job>
{
    void            setup   ( x_light_trigger& Trigger, x_function<void(void)> func ) { m_Function=func; AndThenRuns ( Trigger ); }
    virtual void    onRun   ( void )                                                  { m_Function(); }
    x_function<void(void)>   m_Function;
};

/////////////////////////////////////////////////////////////////////////////////
// SIMPLE TRIGGER
/////////////////////////////////////////////////////////////////////////////////
template< s32 ENTRY_COUNT=10, class U=x_base_trigger >
class x_simple_trigger : public U
{
    x_rtti_class1(x_simple_trigger, U)

public:
                            x_simple_trigger    ( void ) { m_nEntries=0; }

protected:

    virtual x_base_trigger::entry&      appendEntry         ( void ) { return m_lEntries[m_nEntries++]; }
    virtual void                        vReset              ( void ) { m_nEntries=0; U::vReset(); }
    
protected:

    s32                                             m_nEntries;
    xsafe_array<x_base_trigger::entry,ENTRY_COUNT>        m_lEntries;
};

/////////////////////////////////////////////////////////////////////////////////
// SCHEDULER
/////////////////////////////////////////////////////////////////////////////////
class x_scheduler
{
public:

    void                    Init                        ( s32 WorkerCount=-1 );
    void                    StartJobChain               ( x_base_job& Job );
    void                    StartLightJobChain          ( x_light_job& Job );
    void                    StartTriggerChain           ( x_base_trigger& Tri );
    void                    LocalSyncToTrigger          ( x_base_trigger& Tri );

    void                    MainThreadRunJobs           ( xbool bDoMainTheadOnly = FALSE, xbool bAndLightJobOnly = FALSE, xbool bBlocking = TRUE );
    void                    ProcessWhileWait            ( f32 WaitTimeMilliseconds = 0, xbool bOnlyLighJobs = TRUE );
    void                    ProcessWhileWait            ( xbool bOnlyLighJobs, x_function<xbool(void)> Function );

protected:

    void                    SubmitJob                   ( x_base_job& Job );     
    void                    SubmitTrigger               ( x_base_trigger& Trigger );
    x_base_job*             getJob                      ( xbool bLightOnly, xbool bIsMainThread );
    x_base_job*             getMainThreadJob            ( void );
    void                    appendWorker                ( void );
    void                    ReleaseDependencies         ( x_qt_popless_queue& NotifyQueue );
    void                    ResolveDependency           ( x_base_trigger& Node, xbool bAnd );

protected:

    x_qt_fober_queue        m_JobQueueNotMain           [ x_base_job::PRIORITY_COUNT ][2];
    x_qt_fober_queue        m_JobQueueNormal            [ x_base_job::PRIORITY_COUNT ][2];
    x_qt_fober_queue        m_JobQueueUIonMainThread    [ x_base_job::PRIORITY_COUNT ][2];
    
    base_worker*            m_pWorker;
    s32                     m_WorkerCount;

protected:
    friend struct normal_worker_thread;
    friend class x_base_trigger;
    friend class x_base_job;
};

extern x_scheduler g_Scheduler;

/////////////////////////////////////////////////////////////////////////////////
// INLINE LIGHT JOB LOOP
//-------------------------------------------------------------------------------
// Usage Example:
//
//     // Declares block and it says it can run up to 8 jobs at the sametime
//     x_inline_light_jobs_block<8>       JobBlock;
//
//     // Set all the jobs, note that they can be run at any time ones in the queue
//     for( s32 i=0; i<SomeCounter; i++ )
//        JobBlock.AddJob( [&]()
//        {
//           DoSomeWork();
//        });
//
//     // Finish running any remaining Jobs as well as reset the block
//     JobBlock.FinishJobs();
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TARGET_3DS

template< s32 ENTRY_COUNT >
struct x_inline_light_jobs_block
{
    s32                                     m_JobCount{0};
    xsafe_array<x_inline_light_jobs,ENTRY_COUNT>  m_Jobs;
    x_light_trigger                         m_Trigger;
    
    void SubmitJob( x_function<void(void)> func )
    {
        m_Jobs[m_JobCount++].setup(m_Trigger,func);
        
        // If we have reach our memory limit then lets flush the jobs and reset
        if( m_JobCount == ENTRY_COUNT )
        {
            FinishJobs();
        }
    }
    
    void FinishJobs( void )
    {
        // If we have nothing to do just bail
        if ( m_JobCount == 0 )
            return;

        for ( s32 k = 0; k<m_JobCount; k++ )
            g_Scheduler.StartLightJobChain( m_Jobs[ k ] );

        // Wait for all to sync
        m_Trigger.LocalSync();
        
        // Reset just in case the user wants to add more jobs
        m_Trigger.ManualReset();
        m_JobCount=0;
    }
};
#endif

