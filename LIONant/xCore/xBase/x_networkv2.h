//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
/////////////////////////////////////////////////////////////////////////////////
//
// MAKE SURE ANY DATA YOU PUT IN PACKETS USES x_NetworkEndian() SINCE DIFFERENT
// ENDIAN MACHINES COULD RECIVE THE DATA.
//
//  Paket
//               +----------------+
//  Base Header  | u2  protocol   | [00]-base | [01]-unreliable | [10]-reliable | [11]-inorder
//  Unrealiable  | u30 crcseq     | crcseq = crc^sequence || sequence = crcseq^crc
//               +----------------+
//  Relable      | u32 ack_bits   | bits used to acknowleage the packets that we have recived
//               +----------------+
//  InOrder      | u16 order_seq  | Real sequence which the order is based on
//               +----------------+
//               |                |
//               | u8 user_data[] | The actual user data
//               |                |
//               +----------------+
//
// TODO: 1. Everything in the header could be converted to 16 bits or less except for the ack_bits.
//       2. [00]-base could also be replace with the meaning of mix packet where
//          a packet could contain a mix of the 3 where each section will have a lenth u16 additional field
//       4. The CRC should be change to a security hash since UDP/IP already supports the CRC
//       5. RemoteSequences... needs a bit more thinking to see if we really need them...
//       6. Handling of client disconnects is not done 
//       7. We need to add a hardbit for the reliable protocol... since it continuesly needs to
//          ack packets. (Hardbits could be identify by having the user data size == 0 )
//       7. Packet encryption could be added
//       8. Always more testing will be great.
/////////////////////////////////////////////////////////////////////////////////

void x_NetV2EnableLagSimulator( s32 PercentagePackageLost, s32 PercentageDuplicated, s32 PercentageHackedData, s32 PercentageLagNoise, s32 PercentageLagDistance );
void x_NetV2DisableLagSimulator( void );

/////////////////////////////////////////////////////////////////////////////////
// net address contains the address of a connection IP and the port number.
/////////////////////////////////////////////////////////////////////////////////
class xnet_address
{
public:

                        xnet_address        ( void );
                        xnet_address        ( u32 IP, s32 Port )        { m_Port = Port; m_IP = IP;     }
                        xnet_address        ( const char* pAddrStr )    { SetStrAddress ( pAddrStr );   }
        void            Clear               ( void );
        xbool           isValid             ( void ) const;

        void            Setup               ( u32 IP, s32 Port ); 
        void            SetupLocalIP        ( void );
        void            SetIP               ( u32 IP );
        void            SetPortNumber       ( s32 Port );
        const char*     SetStrIP            ( const char* pIPStr   );
        const char*     SetStrAddress       ( const char* pAddrStr );

        u32             GetIP               ( void ) const;
        s32             GetPort             ( void ) const;
        xstring         GetStrIP            ( void ) const;
        xstring         GetStrAddress       ( void ) const;

        xbool           operator ==         ( const xnet_address& A ) const;
        xbool           operator !=         ( const xnet_address& A ) const;

private:    

        u32             m_IP;
        s32             m_Port;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_socket
{
public:
                        xnet_socket             ( void );
                       ~xnet_socket             ( void ){}

    static  u32         getLocalIP              ( void );

            xbool       OpenUDPSocket           ( s32 Port );
            xbool       OpenTCPSocket           ( s32 Port );
            xbool       AcceptTCPConnection     ( xnet_socket& NewSocket ) const;
            xbool       ConnectTCPServer        ( xnet_address Address );
            xbool       ReadFromSocket          ( void* pData, s32 MaxLength, s32& Length, xnet_address& FromAddr, xbool bBlock );
            xbool       SendPacket              ( void* pData, s32 Size, xnet_address Address );
            s32         AddRef                  ( void )        { return ++m_RefCount; }
            s32         DecRef                  ( void )        { return --m_RefCount; }
            s32         getPort                 ( void )        { return m_Port;       }
            xbool       isTCP                   ( void ) const  { return m_bTCP;       }
            u32         getLowLevelSocket       ( void ) const  { return m_Socket;     }
protected:

    xbool   m_bTCP;
    u32     m_Socket;
    s32     m_Port;
    s32     m_RefCount;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_mesh;
class xnet_base_protocol
{
protected:
                                xnet_base_protocol      ( void );
    virtual xbool               HandleRecivePacket      ( xhandle hLocalSocket, void* pData, s32& Size )=0;
            xbool               setupConnection         ( xnet_address RemoteAddress, xnet_udp_mesh& Mesh );
    static  u32                 setPacketHeader         ( s32& LocalSequence, const void* pData, s32 Size, u8 Protocol );
            xbool               getInfoPacketHeader     ( s32  RemoteSequence, const void* pData, s32 Size, s32& Sequence );  
            s32                 getProtocol             ( const void* pData );
            xbool               WriteToSocket           ( xhandle hLocalSocket, void* pData, s32 Size );

            void                AddClientInPendingList  ( void );
   virtual  xbool               getPendingPacketData    ( void* pUserData, s32& Size )=0;

   virtual  void                vResetClient            ( void );

protected:

    xnet_udp_mesh*      m_pMesh;
    xnet_address        m_ConnectionAddress;
    xnet_base_protocol* m_pPendingNext;         // Link list of clients which has packets pending

    xtimer              m_TimeSinceLastRecived;            // Timer for heart-beats and such
    u32                 m_BytesSent;
    u32                 m_PacketsSent;
    u32                 m_BytesReceived;
    u32                 m_PackagesReceived;

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_nonreliable_protocol : public xnet_base_protocol
{
public:
            xbool   SendNonreliablePacket       ( xhandle hLocalSocket, const void* pData, s32 Length );
    static  xbool   SendBroadcast               ( s32 DestinationPort, xnet_socket& LocalSocket, const void* pData, s32 Length );

protected:

                    xnet_nonreliable_protocol   ( void );
            xbool   OnRecivePacket              ( void* pData, s32& Size, s32 Sequence );
   virtual  void    vResetClient                ( void );

protected:

    s32     m_NonRealibleLocalSequence;                     
    s32     m_NonRealibleRemoteSequence;

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_reliable_protocol : public xnet_nonreliable_protocol
{
public:
            xbool   SendReliablePacket      ( xhandle hLocalSocket, const void* pData, s32 Length );

protected:

    struct pending_packet
    {
        s32                         m_Protocol;
        s32                         m_Sequence;
        s32                         m_Size;
        pending_packet*             m_pNext;
        pending_packet*             m_pPrev;
        xsafe_array<xbyte,1024>     m_Data;
    };

protected:
                    xnet_reliable_protocol  ( void );
            xbool   OnRecivePacket          ( xhandle hLocalClient, void* pData, s32& Size, s32 Sequence );
            void    HandleAckknowleage      ( xhandle hLocalClient, s32 RemoteLocalSequence, u32 AckBits );
            xbool   SendReliablePacket      ( xhandle hLocalClient, const void* pData, s32 Length, s32 Protocol );
   virtual  void    vResetClient            ( void );
            xbool   Update                  ( xhandle hLocalSocket );

protected:

    s32                                 m_TotalInQueue;
    u32                                 m_RecivedPacketsMask;
    xsafe_array<pending_packet*,128>    m_PendingQueue;             // Waiting to be confirm
    pending_packet*                     m_pReliablePoolFreeList;    // Free packet list
    xptr<pending_packet>                m_PendingPacketPool;        // The actual allocation of packets
    s32                                 m_RealibleSequence;
    s32                                 m_nResentPackets;
    xbool                               m_bForcePing;
    xtimer                              m_Timer;            // Timer for heart-beats and such

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_inorder_protocol : public xnet_reliable_protocol
{
public:
            xbool   SendInOrderPacket       ( xhandle hLocalSocket, const void* pData, s32 Length );

protected:
                    xnet_inorder_protocol   ( void );
            xbool   OnRecivePacket          ( xhandle hLocalClient, void* pData, s32& Size, s32 Sequence );
   virtual  xbool   getPendingPacketData    ( void* pUserData, s32& Size );
   virtual  void    vResetClient            ( void );

protected:

    pending_packet*     m_pPendingInOrder;          // Queue of pending packets 
    pending_packet*     m_pPendingInOrderLast;      // Points to the last node in the queue
    s32                 m_InOrderLocalSequence;
    s32                 m_InOrderRemoteSequence;

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_client : public xnet_inorder_protocol
{
public:
        
        enum type
        {
            TYPE_UNKOWN,
            TYPE_CERTIFIED,
            TYPE_LOCAL
        };

public:
                                xnet_udp_client         ( void ) { m_ClientType = TYPE_UNKOWN; } 
            const xnet_address& getAddress              ( void ) const { return m_ConnectionAddress; } 
            type                getClientType           ( void ) const { return m_ClientType; }
            void                CertifyClient           ( void );
            void                SetLocalClient          ( void ) { m_ClientType = TYPE_LOCAL; }

protected:

            void                setupClient             ( type Type )  { m_ClientType = Type; }
    virtual xbool               HandleRecivePacket      ( xhandle hLocalClient, void* pData, s32& Size );
   virtual  void                vResetClient            ( void );

protected:

    type    m_ClientType;

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_mesh
{
public:

                                xnet_udp_mesh           ( void );
            xhandle             OpenSocket              ( s32 Port );
            xbool               getSocketPackets        ( xhandle hLocalSocket, void* pUserBuffer, s32& UserSize, xhandle& hClient, xbool bBlock = FALSE );
            xnet_socket&        getSocket               ( xhandle hSocket ) { return m_SocketList(hSocket); }
            xhandle             CreateClient            ( xnet_address Address );
            void                DeleteClient            ( xhandle hClient );
            s32                 getClientCount          ( void ) const      { return m_lClient.getCount(); }
            xnet_udp_client&    getClient               ( s32 Index )       { return m_lClient[Index]; }
            xnet_udp_client&    getClient               ( xhandle hClient ) { return m_lClient(hClient); }
            xhandle             findClient              ( const xnet_address& FromAddr );

protected:

            xhandle             FindOrCreateSocket      ( s32 Port );
            xbool               WriteToSocket           ( xhandle hLocalSocket, void* pData, s32 Size, xnet_address Address );

protected:

    xharray<xnet_udp_client>    m_lClient;
    xharray<xnet_socket>        m_SocketList;
    xnet_base_protocol*         m_pPendingPacketClientList;

protected:

    friend class xnet_base_protocol; 
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_tcp_client 
{
public:
        
        enum type
        {
            TYPE_UNKOWN,
            TYPE_CERTIFIED
        };

public:
                                xnet_tcp_client         ( void ) { m_ClientType = TYPE_UNKOWN; } 
            xbool               OpenSocket              ( s32 Port );
            xbool               ConnectToServer         ( xnet_address Address );
            xbool               Send                    ( const void* pData, s32 Length );
            xbool               Recive                  ( void* pUserBuffer, s32& UserSize, xbool bBlock = FALSE );
            type                getClientType           ( void ) const { return m_ClientType; }
            void                CertifyClient           ( void )       { m_ClientType = TYPE_CERTIFIED; }
            xnet_socket&        getSocket               ( void )       { return m_Socket; }

protected:

    type            m_ClientType;
    xnet_socket     m_Socket;

protected:

    friend class xnet_tcp_server;
};

//////////////////////////////////////////////////////////////////////////////////

class xnet_tcp_server
{
public:
                                xnet_tcp_server         ( void );
            xbool               OpenSocket              ( s32 Port );
            xbool               AcceptConnections       ( void );
            xnet_socket&        getSocket               ( void ) { return m_Socket; }
//            xhandle             CreateClient            ( xnet_address Address );
//            void                DeleteClient            ( xhandle hClient );
            s32                 getClientCount          ( void ) const      { return m_lClient.getCount(); }
            xnet_tcp_client&    getClient               ( s32 Index )       { return *m_lClient[Index]; }
            xnet_tcp_client&    getClient               ( xhandle hClient ) { return *m_lClient(hClient); }
//            xhandle             findClient              ( const xnet_address& FromAddr );

protected:

    xnet_socket                 m_Socket;
    xharray<xnet_tcp_client*>   m_lClient;
};

//////////////////////////////////////////////////////////////////////////////////
// PRIVATE!!  PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!! PRIVATE!!
//////////////////////////////////////////////////////////////////////////////////
void x_NetInit( void );
void x_NetKill( void );
