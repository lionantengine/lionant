//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class xptr2_debug_lock
{
public:

    // Use "xptr2_flags" to get access to the flags from outside the class
    // default or 0 is: MULTI_READER | MULTI_WRITTER | NOT_QT_ACCESS
    enum public_flags:u8
    {
        FLAGS_READ_ONLY             = X_BIT(1),
        FLAGS_QT_READABLE           = X_BIT(2),
        FLAGS_QT_MUTABLE            = X_BIT(3),
        FLAGS_SINGLE_WRITTER        = X_BIT(4),
        FLAGS_SINGLE_READER         = X_BIT(5),
        FLAGS_MULTI_READER          = 0,
        FLAGS_MULTI_WRITTER         = 0,
        FLAGS_NOT_QT_ACCESS         = 0
    };

public:

    xbool           isMutable               ( xbool bAssert ) const;
    xbool           isReadable              ( xbool bAssert ) const;
    void            RefReader               ( xbool bInc ) const;
    void            RefMutate               ( xbool bInc ) const;
    void            Freeze                  ( void ) const;
    void            Unfreeze                ( void ) const;
    void            ChangeBehavior          ( u32 Flags ) const;
    void            getEntryReadOnly        ( void ) const;
    void            getEntry                ( void );

protected:

    enum flags_private:u32
    {
        FLAGS_FROZEN            = X_BIT(0),
    };

    union debug
    {
        u64             m_Raw               = 0;

        struct 
        {
            u16         m_UniqueID;
            u16         m_ThreadID;
            u8          m_Flags;
            u8          m_nWritters;
            u8          m_nReaders;
            u8          m_Pad[1];
        };
    };

protected:

#ifdef X_DEBUG
    debug      m_DebugFlags;               // Flags
#endif   
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class xsemaphore_lock
{
public:

    enum
    {
        FLAGS_XREADERS_1WRITTER       = 0  
                                        | xptr2_debug_lock::FLAGS_QT_READABLE      
                                        | xptr2_debug_lock::FLAGS_MULTI_READER      
                                        | xptr2_debug_lock::FLAGS_SINGLE_WRITTER   
                                        | xptr2_debug_lock::FLAGS_QT_MUTABLE,

        FLAGS_XREADERS_XWRITTERS      = 0  
                                        | xptr2_debug_lock::FLAGS_QT_READABLE      
                                        | xptr2_debug_lock::FLAGS_MULTI_READER      
                                        | xptr2_debug_lock::FLAGS_QT_MUTABLE,

        FLAGS_XREADERS_0WRITTERS      = 0  
                                        | xptr2_debug_lock::FLAGS_QT_READABLE      
                                        | xptr2_debug_lock::FLAGS_MULTI_READER      
                                        | xptr2_debug_lock::FLAGS_READ_ONLY,

        FLAGS_0READERS_1WRITTER       = 0  
                                        | xptr2_debug_lock::FLAGS_SINGLE_WRITTER      
                                        | xptr2_debug_lock::FLAGS_NOT_QT_ACCESS,
    };

    void    RefReader               ( xbool bInc  ) const;
    void    RefMutate               ( xbool bInc  ) const;
    void    ChangeBehavior          ( u32   Flags ) const;
    xbool   isMutable               ( xbool bAssert ) const     {ASSERT(!(m_Semapore.m_Flags&FREEZ)); return TRUE;}
    xbool   isReadable              ( xbool bAssert ) const     {ASSERT(!(m_Semapore.m_Flags&FREEZ)); return TRUE;}
    void    Freeze                  ( void ) const              { ASSERT(!(m_Semapore.m_Flags&FREEZ)); ChangeBehavior( m_Semapore.m_Flags | FREEZ ); }
    void    Unfreeze                ( void ) const              { ASSERT( m_Semapore.m_Flags&FREEZ); ChangeBehavior( m_Semapore.m_Flags & ~FREEZ ); }
    void    getEntryReadOnly        ( void ) const              {}
    void    getEntry                ( void )                    {}
    void    BeginAtomic             ( void )                    { RefMutate(TRUE);   }
    void    EndAtomic               ( void )                    { RefMutate(FALSE);  }
    void    BeginAtomic             ( void ) const              { RefReader(TRUE);   }
    void    EndAtomic               ( void ) const              { RefReader(FALSE);  }
    s32     getWritterCount         ( void ) const              { return m_Semapore.m_nWritters; }
    s32     getReaderCount          ( void ) const              { return m_Semapore.m_nReaders; }

protected:

    enum
    {
        FREEZ = X_BIT(7)
    };

    union semaphore
    {
        u64             m_Raw               = 0 | (FLAGS_XREADERS_1WRITTER<<16) ;

        struct 
        {
            u16         m_Counter;
            u8          m_Flags;
            u8          m_nReaders;
            u8          m_nWritters;
            u8          m_Pad[3];
        };
    };

protected:

    volatile semaphore   m_Semapore;
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class xptr2_smaphore_lock : public xptr2_debug_lock
{
public:
            xptr2_smaphore_lock     ( void )              { xptr2_debug_lock::ChangeBehavior( xsemaphore_lock::FLAGS_XREADERS_XWRITTERS ); }
    void    RefReader               ( xbool bInc  ) const {m_Semaphore.RefReader(bInc); xptr2_debug_lock::RefReader(bInc); }
    void    RefMutate               ( xbool bInc  ) const {m_Semaphore.RefMutate(bInc); xptr2_debug_lock::RefMutate(bInc); }
    void    ChangeBehavior          ( u32   Flags ) const {xptr2_debug_lock::ChangeBehavior(Flags); m_Semaphore.ChangeBehavior(Flags); }

protected:

    xsemaphore_lock     m_Semaphore;
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template< typename T, typename D = xptr2_debug_lock >
class xvar
{
public:

    using   this_type           = xvar<T,D>;
    using   smart_lock          = D;
    using   public_interface    = T;
    using   entry_type          = T;

public:

    void                            ChangeBehavior          ( u32 Flags ) const { m_Debug.ChangeBehavior(Flags); }

protected:

    const smart_lock&               getSmartLock            ( void ) const      { return m_Debug; }
    smart_lock&                     getSmartLock            ( void )            { return m_Debug; }
    const entry_type&               getEntryReadOnly        ( s32 Index ) const { return m_Var[Index]; }
    entry_type&                     getEntry                ( s32 Index )       { return m_Var[Index]; }
    public_interface*               getPublicInterface      ( void )            { m_Debug.getEntry(); return &m_Var; }
    const public_interface* const   getPublicInterface      ( void ) const      { m_Debug.getEntryReadOnly(); return &m_Var; }

protected:
    
    T           m_Var;
    smart_lock  m_Debug;

protected:
    template<class> friend class const_xref;
    template<class> friend class mutable_xref;
};

typedef xptr2_debug_lock  xvar_flags;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template< typename T, typename D = xptr2_debug_lock >
class xlvar
{
public:

    using   this_type           = xvar<T,D>;
    using   smart_lock          = D;
    using   public_interface    = T;
    using   entry_type          = typename T::entry_type;

public:

    void                            ChangeBehavior          ( u32 Flags ) const { m_Debug.ChangeBehavior(Flags); }
    s32                             getCount                ( void )            { return m_Var.getCount(); }

protected:

    const smart_lock&               getSmartLock            ( void ) const      { return m_Debug; }
    smart_lock&                     getSmartLock            ( void )            { return m_Debug; }
    const entry_type&               getEntryReadOnly        ( s32 Index ) const { return m_Var[Index]; }
    entry_type&                     getEntry                ( s32 Index )       { return m_Var[Index]; }
    public_interface*               getPublicInterface      ( void )            { m_Debug.getEntry(); return &m_Var; }
    const public_interface* const   getPublicInterface      ( void ) const      { m_Debug.getEntryReadOnly(); return &m_Var; }

protected:
    
    T           m_Var;
    smart_lock  m_Debug;

protected:
    template<class> friend class const_xref;
    template<class> friend class mutable_xref;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     xptr_ref is a very safe way to pass a reference to a block of memory. 
//     Because it will check for the bounce when accessing to it. This allows things
//     like xsafe_array and xptr to pass them selfs to functions.
//
// Example:
//<CODE>
//      void TestFunction( xptr_ref<s32> Test )
//      {
//          for( s32 i=0; i<Test.GetCount(); i++ )
//          {
//              Test[i] = i;
//          }
//      }
//
//      void main( void )
//      {
//          xsafe_array<s32,32>  Array;
//          xptr<s32>            Ptr;
//          Ptr.Alloc( 32 );
//          
//          TestFunction( Array );
//          TestFunction( Ptr );
//      }
//</CODE>
// See Also:
//     xptr_lock xarray xharray xptr xsafe_array
//------------------------------------------------------------------------------
template< typename T, typename D > class xptr2;

//---------------------------------------------------------------------------------------

template< typename T, typename D >
class xptr2_interface
{
public:

    using   this_type           = xptr2<T,D>;
    using   smart_lock          = D;
    using   entry_type          = T;
    using   public_interface    = xptr2_interface< entry_type, smart_lock >;

public:

    void            Copy                    ( s32 DestinationOffset, const this_type& SourceRef, s32 SourceOffset, s32 ElementCount );
    void            setMemory               ( s32 Value );
    s32             getEntrySize            ( void ) const                  { return sizeof(T); }

protected:
                    xptr2_interface         ( void ){}
};

//---------------------------------------------------------------------------------------

template< typename T, typename D = xptr2_debug_lock >
class xptr2 : protected xptr2_interface<T,D>
{
public:

    using   this_type           = xptr2<T,D>;
    using   smart_lock          = D;
    using   entry_type          = T;
    using   public_interface    = xptr2_interface< entry_type, smart_lock >;

public:
                    xptr2               ( void ) {}
    xbool           isValid             ( void ) const                  { return m_pData != NULL; }
    void            ChangeBehavior      ( u32 Flags ) const;
    s32             getCount            ( void ) const;
    void            Destroy             ( void );
    void            Alloc               ( s32 Count );
    void            New                 ( s32 Count );
    void            Resize              ( s32 Count );
//    void            operator <<         ( xbool x )                     { if(x)Freez(); else Unfreez(); }
    s32             getEntrySize        ( void ) const                  { return sizeof(T); }
    void            opMutableMap        ( s32 iStart, x_function<void(T& Entry, s32 iEntry, xbool& About )> Function );
    void            opConstMap          ( s32 iStart, x_function<void(const T& Entry, s32 iEntry, xbool& About )> Function ) const;

protected:

    enum info_memory_aligment
    {
        ALIGNMENT_BYTE   = 8,
        ALIGNMENT_OFFSET = ( ( (((u64)(sizeof(T))) + ((ALIGNMENT_BYTE)-1)) & (-(ALIGNMENT_BYTE)) )) - sizeof(T)
    };

    enum release_flags:u8
    {
        RELEASE_FLAGS_NEW       = X_BIT(0)
    };

    struct destructor				// Handy class make sure that T have a destructor 		
    {  
        inline ~destructor(){} 
        entry_type          m_Item; 
    };     

protected:

    public_interface*               getPublicInterface   ( void )        { return this; }
    const public_interface* const   getPublicInterface   ( void ) const  { return this; }

    void                Copy                    ( s32 DestinationOffset, const this_type& SourceRef, s32 SourceOffset, s32 ElementCount );
    void                setMemory               ( s32 Value );
    const T&            getEntryReadOnly        ( s32 Index ) const;
    T&                  getEntry                ( s32 Index ); 
    const void*         getElementPtr           ( void ) const;
    void*               getElementPtr           ( void );
    T*                  getEntryPtr             ( void )                        { ASSERT(isValid()); return m_pData->m_pEntry; }
    const T*            getEntryPtr             ( void ) const                  { ASSERT(isValid()); return m_pData->m_pEntry; }
    void                Unfreeze                ( void );
    void                Freeze                  ( void );

    const smart_lock&   getSmartLock             ( void ) const;
    smart_lock&         getSmartLock             ( void );

    // Prevent people from copying it?
    // or add references?
    const public_interface& operator =      ( const T& X ) const{ return *this; }

protected:

    smart_lock          m_Debug;
    entry_type*         m_pData         = NULL;
    s32                 m_Capacity      = 0;
    u32                 m_ReleaseFlags  = 0;

protected:

    template< typename > friend class const_xref;
    template< typename > friend class mutable_xref;
    template< typename, typename > friend class xarray2_interface;
    template< typename, typename > friend class xptr2_interface;
    template< typename, typename > friend class xvar;
};

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

typedef xptr2_debug_lock   xptr2_flags;

#define const_ref(VAR,XCONTAINER)  const const_xref< typename x_unref<decltype(XCONTAINER)>::type > VAR( XCONTAINER );
#define mutable_ref(VAR,XCONTAINER)    mutable_xref< typename x_unref<decltype(XCONTAINER)>::type > VAR( XCONTAINER );

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

template<class T>
class const_xref
{
public:

    using   smart_lock          = typename T::smart_lock;
    using   public_interface    = typename T::public_interface;
    using   entry_type          = const typename T::entry_type;

public:

    xiter2< const const_xref<T> >   begin               ( void ) const              { return xiter2< const const_xref<T> >(0,*this); }
    xiter2< const const_xref<T> >   end                 ( void ) const              { return xiter2< const const_xref<T> >(*this); }
    xiter2< const_xref<T> >         begin               ( void )                    { return xiter2< const_xref<T> >(0,*this); }
    xiter2< const_xref<T> >         end                 ( void )                    { return xiter2< const_xref<T> >(*this); }
    explicit                        const_xref          ( const T& X ) : m_Ptr(X)   { ASSERT( X.getSmartLock().isReadable( TRUE ) ); const_cast<T&>(X).getSmartLock().RefReader( TRUE ); m_bAttached = TRUE; }
                                   ~const_xref          ( void )                    { if(m_bAttached) Release();  }
    entry_type&                     operator[]          ( s32 Index ) const         { return m_Ptr.getEntryReadOnly( Index ); }
    const public_interface*         operator->          ( void ) const              { return m_Ptr.getPublicInterface();   }
    s32                             getCount            ( void ) const              { return m_Ptr.getCount(); }
    s32&                            getMutableIterator  ( void ) const              { return m_Iterator;                    }
    s32                             getIterator         ( void ) const              { return m_Iterator;                    }
    void                            endLoop             ( void ) const              { m_Iterator = getCount()-1; }
    xbool                           isIteratorAtEnd     ( void ) const              { return  m_Iterator == m_Ptr.getCount(); }
    const public_interface&         operator*           ()       const              { return *m_Ptr.getPublicInterface();   }
    void                            Release             ( void ) const              { ASSERT(m_bAttached); const_cast<T&>(m_Ptr).getSmartLock().RefReader( FALSE ); m_bAttached = FALSE; }
    void                            ReleaseAndChangeBehavior( u32 Flags ) const  { Release(); m_Ptr.ChangeBehavior( Flags ); }
    void                            ReleaseAndQTReadOnly( void ) const              { ReleaseAndChangeBehavior(xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY); }
    s32                             getEntrySize        ( void ) const              { return sizeof(entry_type); } 
                                    operator const entry_type&   () const           { return *m_Ptr.getPublicInterface(); }

protected:

                                    const_xref      ( void )                    {}
    const_xref&                     operator =      ( const const_xref<T>& X )  { return *this; }
                                    const_xref      ( const const_xref<T>& X )  {}
                                    const_xref      ( const const_xref<T>&& X ) {}

protected:

    mutable xbool   m_bAttached;
    const T&        m_Ptr;
    mutable s32     m_Iterator;
};

//---------------------------------------------------------------------------------------

template<class T>
class mutable_xref
{
public:

    using   smart_lock          = typename T::smart_lock;
    using   public_interface    = typename T::public_interface;
    using   entry_type          = typename T::entry_type;

public:

    explicit                    mutable_xref        ( T& X ) : m_Ptr(X) { ASSERT( X.getSmartLock().isMutable(TRUE) ); X.getSmartLock().RefMutate ( TRUE ); m_bAttached = TRUE; }
                               ~mutable_xref        ( void )            { if(m_bAttached) Release();  }
    void                        operator <<         ( xbool x )         { ASSERT(m_bAttached != x); m_Ptr.RefMutate ( !x ); m_bAttached = x; }
    xiter2< mutable_xref<T> >   begin               ( void )            { return xiter2< mutable_xref<T> >(0,*this); }
    xiter2< mutable_xref<T> >   end                 ( void )            { return xiter2< mutable_xref<T> >(*this); }   
    xiter2< mutable_xref<T> >   begin               ( void ) const      { return xiter2< mutable_xref<T> >(0,*this); }
    xiter2< mutable_xref<T> >   end                 ( void ) const      { return xiter2< mutable_xref<T> >(*this); }
    entry_type&                 operator[]          ( s32 Index )       { return m_Ptr.getEntry( Index );       }
    public_interface*           operator->          ( void )            { return m_Ptr.getPublicInterface();    }
    const public_interface* const operator->        ( void ) const      { return m_Ptr.getPublicInterface();    }
    s32                         getCount            ( void ) const      { return m_Ptr.getCount();              }
    s32&                        getMutableIterator  ( void ) const      { return m_Iterator;                    }
    s32                         getIterator         ( void ) const      { return m_Iterator;                    }
    void                        endLoop             ( void ) const      { m_Iterator = getCount()-1; }
    xbool                       isIteratorAtEnd     ( void ) const      { return m_Iterator == m_Ptr.getCount(); }
    public_interface&           operator*           ()                  { return *m_Ptr.getPublicInterface();   }
    const public_interface&     operator*           ()       const      { return *m_Ptr.getPublicInterface();   }
    void                        Release             ( void ) const      { ASSERT(m_bAttached); m_Ptr.getSmartLock().RefMutate( FALSE ); m_bAttached = FALSE; }
    void                        ReleaseAndChangeBehavior( u32 Flags ) const  { Release(); m_Ptr.ChangeBehavior( Flags ); }
    void                        ReleaseAndQTReadOnly( void ) const              { ReleaseAndChangeBehavior(xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY); }
    s32                         getEntrySize        ( void ) const      { return sizeof(entry_type); }
                                operator entry_type&         ()         { return *m_Ptr.getPublicInterface(); }
                                operator const entry_type&   () const   { return *m_Ptr.getPublicInterface(); }

protected:

                                mutable_xref    ( void )                        {}
    const mutable_xref&         operator =      ( const T& X ) const            { return *this; }
                                mutable_xref    ( const mutable_xref<T>& X )    {}
                                mutable_xref    ( const mutable_xref<T>&& X )   {}

protected:

    T&              m_Ptr;
    mutable s32     m_Iterator;
    mutable xbool   m_bAttached;
};

