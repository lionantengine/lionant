//==============================================================================
// (C) LIONANT - To use this library please contact us at LIONant. License in x_base.cpp
//==============================================================================
//------------------------------------------------------------------------------
// Global function. BE careful when you used them. Make sure you don't need to 
// use the class stead.
//------------------------------------------------------------------------------

void    x_srand     ( s32 Seed );
s32     x_rand      ( void );               
s32     x_irand     ( s32 Min, s32 Max );   
f32     x_frand     ( f32 Min=0, f32 Max=1 );   

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
//     John D. Cook
//     George Marsaglia
//
// Description:
//     xrandom_small is a simple random number generator based on
//     George Marsaglia's MWC (multiply with carry) generator.
//     Although it is very simple, it passes Marsaglia's DIEHARD
//     series of random number generator tests.
//
//------------------------------------------------------------------------------

class xrandom_small
{
public:
                xrandom_small       ( void );
    
    void        setSeed64           ( u64 u );
    void        setSeed32           ( u32 u );
    u64         getSeed             ( void ) const;
    
    
    u32         Rand32              ( void );
    s32         Rand32              ( s32 Start, s32 End );

    f64         RandF64             ( void );
    f32         RandF32             ( void );
    f32         RandF32             ( f32 Start, f32 End );
    f64         RandF64             ( f64 Start, f64 End );
    
    f64         Normal              ( void );
    f64         Normal              ( f64 mean, f64 standardDeviation );
    f64         Exponential         ( void );
    f64         Exponential         ( f64 mean );
    f64         Gamma               ( f64 shape, f64 scale );
    f64         ChiSquare           ( f64 degreesOfFreedom );
    f64         InverseGamma        ( f64 shape, f64 scale );
    f64         Weibull             ( f64 shape, f64 scale );
    f64         Cauchy              ( f64 median, f64 scale );
    f64         StudentT            ( f64 degreesOfFreedom );
    f64         Laplace             ( f64 mean, f64 scale );
    f64         LogNormal           ( f64 mu, f64 sigma );
    f64         Beta                ( f64 a, f64 b );
    
protected:
    
    u32         m_W;
    u32         m_Z;
};

//------------------------------------------------------------------------------
// Author:
//     Tomas Arce
// Description:
//     This class is design to create radom numbers. It is important to have your
//     own instance of this class when you want to create your own radom numbers
//     base on a known seeded sequence. If you used the global functions such x_rand
//     you may get unsequence random numbers which may lead to have issues while 
//     trying to recreate situation in the game such when debugging for instance.
//
//<P>  Most offten you will just need the Rand32/FastRand32 or RandF32. Some of
//     other random function such the Gamma and Chi, etc. Are for different types
//     of distributions that may be use in things like tools.
//
//<P>  Also note that the class has an over head of about 2K. So dont over use it.
//
// Example:
// <CODE>
//      void main( void )
//      {
//          xrandom_large MyRndGenerator;
//
//          x_printf( " A number between 0 and 1 %f\n", MyRndGenerator.RandF32() );
//          x_printf( " A number between 0 and 100 %d\n", , MyRndGenerator.Rand32(0,100) );
//      }
// </CODE>
//------------------------------------------------------------------------------
class xrandom_large
{
public:
                xrandom_large   ( void );
                xrandom_large   ( s32 Seed );
    void        Seed            ( s32 Seed );

    s32         Rand32          ( void );
    s32         FastRand32      ( void );
    s32         Rand32          ( s32 Start, s32 End );

    f64         RandF64         ( void );
    f32         RandF32         ( void );
    f32         RandF32         ( f32 Start, f32 End );
    f64         RandF64         ( f64 Start, f64 End );

    void        SetupVector2    ( xvector2& V );
    void        SetupVector3    ( xvector3d& V );

    f64         DiskF64         ( f64& xp, f64& yp );
    f32         DiskF32         ( f32& xp, f32& yp );
    f64         NormalF64       ( f64 Sigma, f64 Mean );
    f64         Exponential     ( f64 mu );
    f64         Gamma           ( f64 a );
    f64         Beta            ( f64 a, f64 b );
    f64         Chi             ( f64 nu );
    f64         FDist           ( f64 nu1, f64 nu2 );
    f64         TDist           ( f64 nu );
    s32         Geometric       ( f64 p );
    s32         Binomial        ( f64 p, s32 trials );
    s32         Poisson         ( f64 mu );
    
protected:

    enum
    {
        R	    = 48,
        LOGR	=  6,
        MAX_V   = 571
    };

protected:
    
    s32                         m_Feed;   
    s32                         m_Tap;    
    s32                         m_Borrow;
    s32                         m_Y;
    xsafe_array<s32,R>          m_Vec;
    xsafe_array<s32,MAX_V>      m_V;
};

