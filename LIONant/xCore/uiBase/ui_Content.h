//
//  ui_Content.h
//  uiBase
//
//  Created by Tomas Arce on 2/22/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

///////////////////////////////////
// CONTENT BASE
///////////////////////////////////
struct uicontent_base : public xproperty
{
    struct guid : public xguid {};
    x_rtti_class1( uicontent_base, xproperty );

                    uicontent_base  ( void ) {}
    virtual void    onRender        ( const uistyle_content::guid gElement, const uistyle_base::render& Render )=0;
            xbool   onPropQuery     ( xproperty_query& Query ) override {return FALSE;}
            void    onPropEnum      ( xproperty_enum&  Enum  ) const override {}
            guid    getGuid         ( void ) const { return m_Guid; }

    guid            m_Guid;
    uimanager*      m_pManager;
};

///////////////////////////////////
// CONTENT SYSTEM TEXT
///////////////////////////////////

struct uicontent_text : public uicontent_base
{
    x_rtti_class1( uicontent_text, uicontent_base );
    XFACTORY_CLASS( uicontent_text )
    
                    uicontent_text   ( void ) {}
    void            setString               ( const xstring& String )  { m_String = String; }
    const xstring&  getString               ( void ) const             { return m_String; }   
    void            onRender                ( const uistyle_content::guid gElement, const uistyle_base::render& Render ) override;
    
    xstring         m_String;
};

///////////////////////////////////
// CONTENT SYSTEM TEXT
///////////////////////////////////

struct uicontent_sprite : public uicontent_base
{
    x_rtti_class1( uicontent_sprite, uicontent_base );
    XFACTORY_CLASS( uicontent_sprite )

    void            onRender            ( const uistyle_content::guid gElement, const uistyle_base::render& Render ) override;

    eng_sprite    m_Sprite;
};

