//
//  ui_Regions.h
//
//  Created by Tomas Arce on 1/20/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

///////////////////////////////////
// REGION -
///////////////////////////////////
class uiregion :
    public xmsg_receiver,
    public xproperty
{
public:
    
    x_rtti_class1( uiregion, xproperty );
    
    struct guid : public xguid {};
    
    struct on_startup : public xmsg_base
    {
        X_MSG_UA( on_startup, xmsg_base );            // Declaration of the unique identifier
        
        on_startup( uiregion& Region ) : m_Region(Region) {}
        
        uiregion& m_Region;
    };
    
public:
                    uiregion( void );
    virtual        ~uiregion( void ){}
    
            void    setStyleSheet   ( uistyle_sheet::guid gStyle )  { m_gStyle = gStyle; }
            void    setRect         ( const uirect& Rect )          { m_Region = Rect;   }
            guid    getGuid         ( void ) const                  { return m_Guid;     }
    
protected:
    
    virtual void    onRender        ( uistyle_base::render& Info );
    virtual void    onMouseInput    ( void ) {};
    virtual void    onKeyboardInput ( void ) {};
            xbool   onPropQuery     ( xproperty_query& Query ) override { return TRUE; }
            void    onPropEnum      ( xproperty_enum&  Enum  ) const override {}
    virtual xbool   onInput         ( const xrect& ParentRect, const xvector2& Pos );
    virtual void    onLogic         ( f32 DeltaTime ) {}
    
protected:
    
    guid                    m_Guid;         // Guid for the region
    uirect                  m_Region;       // Area of the region
    uistyle_sheet::guid     m_gStyle;       // Style used to render the region
    uimanager*              m_pManager;     // Manager that his object belongs
    
protected:
    
    friend class uimanager;
    friend class uicontainer_area;
};

///////////////////////////////////
// CONTAINER -
///////////////////////////////////

class uicontainer_base : public uiregion
{
protected:
    x_rtti_class1( uicontainer_base, uiregion );
    
    virtual void    AddRegion       ( uiregion& Region )=0;    
            xbool   onPropQuery     ( xproperty_query& Query ) override { return TRUE; }
            void    onPropEnum      ( xproperty_enum&  Enum  ) const override {}
    
protected:
    
    friend class uimanager;
};

///////////////////////////////////
// CONTAINER - freeform version of the container
///////////////////////////////////

class uicontainer_freeform : public uicontainer_base
{
protected:
    
    struct region
    {
        uiregion*   m_pRegion;      // Region set as a delta from the hotspot
        xvector2    m_Hotpoint;     // Absolute position where the hotpoint is relative to
    };
    
protected:

    virtual xrect   onRender        ( const xrect& Rect, f32 Alpha, f32 DeltaTime );

protected:
    
    xarray<region>      m_Regions;
};

///////////////////////////////////
// CONTAINER - Area base container
///////////////////////////////////

class uicontainer_area : public uicontainer_base
{
public:
     x_rtti_class1( uicontainer_area, uicontainer_base );
    XFACTORY_CLASS( uicontainer_area )
    
            void    AddRegion           ( uiregion& Region ) override;
            void    onRender            ( uistyle_base::render& Info ) override;
            xbool   onInput             ( const xrect& ParentRect, const xvector2& Pos ) override;
            void    RenderTopContainer  ( const xrect& ScreenRes );
    
protected:
    
    xarray<uiregion*>   m_Regions;
    
protected:
    
    friend class uimanager;
};

