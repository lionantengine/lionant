//
//  ui_Controls.h
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

///////////////////////////////////
// UI GEOMETRY TRANSITION
///////////////////////////////////

class uicontrol_style_transition_base : public uistyle_base
{
public:
    x_rtti_class1( uicontrol_style_transition_base, uistyle_base );
    
    virtual void onTransition( uistyle_transform::key_frame& Final, const uistyle_transform::key_frame& From, const f32 T, const uistyle_transform::key_frame& To ) const =0;
};

///////////////////////////////////
// UI BOING TRANSITION
///////////////////////////////////

class uicontrol_style_transition_boing : public uicontrol_style_transition_base
{
public:
    x_rtti_class1( uicontrol_style_transition_boing, uicontrol_style_transition_base );
    XFACTORY_CLASS( uicontrol_style_transition_boing )
    
    void onTransition( uistyle_transform::key_frame& Final, const uistyle_transform::key_frame& From, const f32 T, const uistyle_transform::key_frame& To ) const override
    {
        Final = To;
        
        f32 FinalT = x_Sin( x_Pow(T+1,-1.3f)*30.0f )* 0.06f*(T-1);
        Final.m_Scale += Final.m_Scale * FinalT;
    }
};

///////////////////////////////////
// UI STYLE SHEET
///////////////////////////////////

class uicontrol_style_sheet : public uistyle_sheet
{
public:
    x_rtti_class1( uicontrol_style_sheet, uistyle_sheet );
    XFACTORY_CLASS( uicontrol_base_style )
  
            uicontrol_style_sheet   ( void ) : m_TimeToTransition(0) { m_gGeometryTransition.setNull();      }
    void    setTimeToTransition     ( f32 TimeToArrible )            { m_TimeToTransition = TimeToArrible;   }
    f32     getTimeToTransition     ( void ) const                   { return m_TimeToTransition;            }
    void    setGeometryTransition   ( uistyle_base::guid gGT )       { m_gGeometryTransition = gGT;          }
    guid    getGeometryTransition   ( void ) const                   { return m_gGeometryTransition;         }
    
protected:
    
    f32             m_TimeToTransition;             // Time that takes to transition to this style
    guid            m_gGeometryTransition;          // Geometric effect to transition
};


///////////////////////////////////
// BASE
///////////////////////////////////

class uicontrol_base_style : public uistyle_base
{
public:
    //X_MESSAGE_MAP_DECL
    
    x_rtti_class1( uicontrol_base_style, uistyle_base );
    XFACTORY_CLASS( uicontrol_base_style )
    
    enum base_state
    {
        STATE_DISABLE,          // Should not recive input and such
        STATE_ACTIVE,           // When is working normally
        STATE_HOVER,            // When the mouse go over in top
        STATE_SELECTED,         // When the ui has been selected by the user
        STATE_MAX_COUNT
    };
    
    void setStyle( base_state State, uistyle_sheet::guid gStyle ) { m_gStyleSheet[State] = gStyle; }
    uicontrol_base_style( void )
    {
        for( s32 i=0;i<m_gStyleSheet.getCount();i++)
            m_gStyleSheet[i].setNull();
        
        m_gGeometryTransition.setNull();
    }
    
    void    setGeometryTransition   ( uistyle_base::guid gGT )       { m_gGeometryTransition = gGT;          }
    guid    getGeometryTransition   ( void ) const                   { return m_gGeometryTransition;         }
    
protected:
    
    xsafe_array<uicontrol_style_sheet::guid,STATE_MAX_COUNT>    m_gStyleSheet;
    f32                                                         m_TransitionTime;
    uicontrol_style_sheet::guid                                 m_gGeometryTransition;
    
protected:
    
    friend class uicontro_base;
};


///////////////////////////////////
// UI CONTROL BASE
///////////////////////////////////

class uicontro_base : public uiregion
{
public:
    
    x_rtti_class1( uicontro_base, uiregion );
    
                    uicontro_base               ( void );
            void    setState                    ( uicontrol_base_style::base_state State );
            void    TriggerControlTransition    ( void );


    
protected:

    struct uicontrol_data_ref
    {
        uicontrol_data_ref(
            const f32                       T0,
            const f32                       T1,
                  uistyle_base::render&     RenderInfo,
            const uistyle_sheet* const      pCurStyleSheet,
            const uistyle_sheet* const      pOldStyleSheet) :
            m_T0                            ( T0 ),
            m_T1                            ( T1 ),
            m_RenderInfo                    ( RenderInfo ),
            m_pCurStyleSheet                ( pCurStyleSheet ),
            m_pOldStyleSheet                ( pOldStyleSheet ){}

        const f32                       m_T0;
        const f32                       m_T1;
              uistyle_base::render&     m_RenderInfo;
        const uistyle_sheet* const      m_pCurStyleSheet;
        const uistyle_sheet* const      m_pOldStyleSheet;
    };
    
protected:
    
            void    onRender                    ( uistyle_base::render& Info ) final;
    virtual void    onHandleStates              ( uistyle_base::render& Info );
    virtual void    onHover                     ( void ) {}
    virtual void    onActive                    ( void ) {}
    virtual void    onDisable                   ( void ) {}
    virtual void    onSelected                  ( void ) {}
    virtual void    onRender                    ( uicontrol_data_ref& Ref )=0;
            xbool   onInput                     ( const xrect& ParentRect, const xvector2& Pos ) override;
    
            xbool   IsMouseInRegion             ( uistyle_base::render& Info ) const;
            void    ComputeTheMatrixAndTime     ( uistyle_base::render& Info ) ;
            xbool   CanChangeStates             ( void ) const;
    
protected:
    
    uistyle_sheet::guid                 m_gRenderElement;
    s32                                 m_CurState;
    s32                                 m_OldState;
    f32                                 m_CurStateTransitionTime;
    f32                                 m_CurControlTransitionTime;
    
};

///////////////////////////////////
// STATIC CONTENT
///////////////////////////////////

class uicontrol_static : public uicontro_base
{
public:
    
    x_rtti_class1( uicontrol_static, uicontro_base );
    XFACTORY_CLASS( uicontrol_static )
    
                    uicontrol_static    ( void );
    
protected:
    
    void   onRender             ( uicontrol_data_ref& Ref ) override;
    
protected:
    
    uicontent_base::guid            m_gContent;
};

///////////////////////////////////
// BUTTON
///////////////////////////////////

class uicontrol_button : public uicontro_base
{
public:
    
    x_rtti_class1( uicontrol_button, uicontro_base );
    XFACTORY_CLASS( uicontrol_button )
    
    enum action
    {
        ACTION_ONRELEASE,
        ACTION_ONPRESS,
    };
    
    enum type
    {
        TYPE_PUSHBUTTON,
        TYPE_RADIOBUTTON,
    };
    
                    uicontrol_button            ( void );
            void    setContent                  ( uicontent_base::guid Guid ) { m_gContent = Guid; }

protected:

            void    onRender                    ( uicontrol_data_ref& Ref ) override;
            void    onHandleStates              ( uistyle_base::render& Info ) override;
    virtual void    onClick                     ( void );
    
protected:
   
    uicontent_base::guid        m_gContent;
    action                      m_Action;
    type                        m_Type;
    xbool                       m_bPressed;
};


