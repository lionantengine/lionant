//
//  ui_base.h
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "eng_Base.h"
#include "ui_Style.h"
#include "ui_Content.h"
#include "ui_Regions.h"
#include "ui_controls.h"

///////////////////////////////////
// MANAGER
///////////////////////////////////
class uimanager : public eng_renderjob
{
public:
    
    X_HANDLE_TYPE( hlistener );
    
            template< class T >
            T&                      CreateRegion        ( void )                        { return T::SafeCast( CreateRegion( T::getFactory() ) ); }
            template< class T >
            T&                      CreateRegion        ( uicontainer_base& Container ) { return T::SafeCast( CreateRegion( T::getFactory(), Container ) ); }
            uiregion&               CreateRegion        ( const char* pTypeName   );
            uiregion&               CreateRegion        ( const xfactory& Factory )     { return CreateRegion( Factory, m_TopContainer ); }
            uiregion&               CreateRegion        ( const xfactory& Factory, uicontainer_base& Container );
    
            template< class T >
            T&                      CreateStyle         ( void )                        { return T::SafeCast( CreateStyle( T::getFactory() ) ); }
            uistyle_base&           CreateStyle         ( const char* pTypeName   );
            uistyle_base&           CreateStyle         ( const xfactory& Factory );
    
            template< class T >
            T&                      CreateContent       ( void )                        { return T::SafeCast( CreateContent( T::getFactory() ) ); }
            uicontent_base&         CreateContent       ( const char* pTypeName   );
            uicontent_base&         CreateContent       ( const xfactory& Factory );
    
            void                    RegisterListener    ( xmsg_receiver& Listener, uiregion::guid gRegion );
            void                    Render              ( f32 DeltaTime );
            void                    Input               ( xvector2& Pos, xbool Is, xbool Was );
    
            template< class T >
            T&                      getStyle            ( uistyle_base::guid gStyle )   { return T::SafeCast( *m_lStylesHash.GetValue( gStyle ) ); }

            template< class T >
            T&                      getRegion           ( uiregion::guid gRegion )      { return T::SafeCast( *m_lRegionHash.GetValue( gRegion ) ); }

            template< class T >
            T&                      getContent          ( uicontent_base::guid gContent )     { return T::SafeCast( *m_lContentHash.GetValue( gContent ) ); }
    
            f32                     getDeltaTime        ( void ) const                  { return m_DeltaTime; }
            const xvector2&         getMousePos         ( void ) const                  { return m_MousePos;  }
            const xbool             getIsMouseLButton   ( void ) const                  { return m_MouseIsLeftButton;  }
            const xbool             getWasMouseLButton  ( void ) const                  { return m_MouseWasLeftButton; }
            const uiregion*         getMouseLock        ( void ) const                  { return m_pMouseLock; }
            void                    setMouseLock        ( const uiregion* pRegion )     { m_pMouseLock = pRegion; }
    
protected:
    
            uiregion&               CreateRegion        ( const xfactory& Factory, uiregion::guid gRegion, uicontainer_base& Container );
            uistyle_base&           CreateStyle         ( const xfactory& Factory, uistyle_base::guid gStyle );
            uicontent_base&         CreateContent       ( const xfactory& Factory, uicontent_base::guid gContent );
    
            void                    onRunRenderJob     ( void ) const override;
    
protected:
    
    uicontainer_area        m_TopContainer;
    xghash<uiregion*>       m_lRegionHash;
    xghash<uistyle_base*>   m_lStylesHash;
    xghash<uicontent_base*> m_lContentHash;
    f32                     m_DeltaTime;
    eng_draw*               m_pDraw;
    xvector2                m_MousePos;
    xbool                   m_MouseIsLeftButton;
    xbool                   m_MouseWasLeftButton;
    const uiregion*         m_pMouseLock;
    
protected:
    
    friend class  uistyle_sheet;
    friend struct uistyle_background;
    friend struct uistyle_boder;
};


