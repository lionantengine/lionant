//
//  ui_Controls.cpp
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "ui_base.h"

XFACTORY_INSTANCE( uicontrol_style_transition_boing,    g_StyleTypes  )
XFACTORY_INSTANCE( uicontrol_base_style,                g_StyleTypes  )
XFACTORY_INSTANCE( uicontrol_style_sheet,               g_StyleTypes  )
XFACTORY_INSTANCE( uicontrol_static,                    g_RegionTypes )
XFACTORY_INSTANCE( uicontrol_button,                    g_RegionTypes )

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

//X_BEGIN_MESSAGE_MAP( uicontro_base, uiregion )                               // Message declaration table
//X_END_MESSAGE_MAP

//-------------------------------------------------------------------------------

uicontro_base::uicontro_base( void )
{
    m_CurState = m_OldState = uicontrol_base_style::STATE_DISABLE;
    m_CurStateTransitionTime = 1;
    m_CurControlTransitionTime = 1;
}

//-------------------------------------------------------------------------------

void uicontro_base::setState( uicontrol_base_style::base_state State )
{
    if( m_CurState == State )
        return;
    
    m_OldState = m_CurState;
    m_CurState = State;
    m_CurStateTransitionTime = (1 - m_CurStateTransitionTime);
}

//-------------------------------------------------------------------------------

void uicontro_base::TriggerControlTransition( void )
{
    if( m_CurControlTransitionTime >= 0.5 ) m_CurControlTransitionTime=0;
}

//-------------------------------------------------------------------------------

xbool uicontro_base::IsMouseInRegion( uistyle_base::render& Info ) const
{
    const xvector2 Pos = m_pManager->getMousePos();

    // Build all the 4 points
    xvector2 V[4];
    
    V[0].Set( Info.m_RenderArea.m_Left,  Info.m_RenderArea.m_Top );
    V[1].Set( Info.m_RenderArea.m_Right, Info.m_RenderArea.m_Top );
    V[2].Set( Info.m_RenderArea.m_Right, Info.m_RenderArea.m_Bottom );
    V[3].Set( Info.m_RenderArea.m_Left,  Info.m_RenderArea.m_Bottom );
    
    // Transform all the points
    for( s32 i=0; i<4; i++ )
    {
        V[i] = Info.m_Matrix * V[i];
    }
    
    // Do a bsp check on the primitive
    for( s32 i=0; i<4; i++ )
    {
        const xvector2 Normal( V[ 3&(i+1) ] - V[i] );
        const xvector2 Vec( Pos - V[i] );
        
        if( Normal.Dot( Vec ) < 0 ) return FALSE;
    }

    return TRUE;
}

//-------------------------------------------------------------------------------

xbool uicontro_base::CanChangeStates( void ) const
{
  //  if( m_CurStateTransitionTime != 1 )
  //      return FALSE;
    
    // If there is nothing to do the vail
    if( m_CurState == uicontrol_base_style::STATE_DISABLE )
        return FALSE;
    
    // Make sure that the mouse is free
    if( m_pManager->getMouseLock() )
        if( m_pManager->getMouseLock() != this )
            return FALSE;
    
    return TRUE;
}

//-------------------------------------------------------------------------------

void uicontro_base::onHandleStates( uistyle_base::render& Info )
{
    //
    // If someone else has the mouse lock then there is nothing to do.
    //
    if( CanChangeStates() == FALSE )
        return;
    
    //
    // Mouse in the area
    //
    if( IsMouseInRegion( Info ) )
    {
        if( m_CurState == uicontrol_base_style::STATE_ACTIVE )
        {
            setState( uicontrol_base_style::STATE_HOVER );
        }
    }
    // Mouse is not in the area
    else if( m_CurState == uicontrol_base_style::STATE_HOVER )
    {
        setState( uicontrol_base_style::STATE_ACTIVE );
    }
}

//-------------------------------------------------------------------------------

void uicontro_base::ComputeTheMatrixAndTime( uistyle_base::render& Info )
{
    if( m_CurControlTransitionTime < 1 )
    {
        m_CurControlTransitionTime += 0.04f;
        
        const uicontrol_base_style&             BaseStyle       = m_pManager->getStyle<uicontrol_base_style>( m_gStyle );        
        const uicontrol_style_transition_base&  TransformStyle  = m_pManager->getStyle<uicontrol_style_transition_base>( BaseStyle.getGeometryTransition() );
        uistyle_transform::key_frame            KeyFrame[3];

        KeyFrame[0].Identity();
        KeyFrame[1] = KeyFrame[0];
        
        TransformStyle.onTransition( KeyFrame[2], KeyFrame[0], m_CurControlTransitionTime, KeyFrame[1] );
        xmatrix4 M( KeyFrame[2].m_Scale, KeyFrame[2].m_Rotation, KeyFrame[2].m_Translation );
        
        // Concadenate it the exisiting matrix
        Info.m_Matrix = Info.m_Matrix * M;
    }
    
    //
    // If we are in the middle of a transition deal with geometry transition
    //
    if( m_CurStateTransitionTime < 1 )
    {
        const uicontrol_base_style&     BaseStyle = m_pManager->getStyle<const uicontrol_base_style>( m_gStyle );
        const uicontrol_style_sheet&    NewStyle  = m_pManager->getStyle<const uicontrol_style_sheet>( BaseStyle.m_gStyleSheet[ m_CurState ] );
        const uicontrol_style_sheet&    OldStyle  = m_pManager->getStyle<const uicontrol_style_sheet>( BaseStyle.m_gStyleSheet[ m_OldState ] );
        const f32                       DeltaTime = m_pManager->getDeltaTime();
        const f32                       TransTime = x_Max( NewStyle.getTimeToTransition()*0.002f, 0.00000001f);
        
        m_CurStateTransitionTime = (x_Pow( m_CurStateTransitionTime* TransTime, 0.999f )  + DeltaTime)/TransTime;
        m_CurStateTransitionTime = x_Min( 1, m_CurStateTransitionTime );
        
        //
        // Handle Geometry Transitions
        //
        {
            uistyle_transform::key_frame KeyFrame[3];
            
            // Get the transform of the old state
            if( OldStyle.getTransform().isValid() )
            {
                const uistyle_transform& TransformStyle = m_pManager->getStyle<uistyle_transform>( OldStyle.getTransform() );
                KeyFrame[0] = TransformStyle.m_KeyFrame;
            }
            else
            {
                KeyFrame[0].Identity();
            }
            
            // Get the transform of the new state
            if( NewStyle.getTransform().isValid() )
            {
                const uistyle_transform& TransformStyle = m_pManager->getStyle<uistyle_transform>( NewStyle.getTransform() );
                KeyFrame[1] = TransformStyle.m_KeyFrame;
            }
            else
            {
                KeyFrame[1].Identity();
            }
            
            // Decide how we are going to transition between both.
            if( NewStyle.getGeometryTransition().isValid() )
            {
                const uicontrol_style_transition_base& TransformStyle = m_pManager->getStyle<uicontrol_style_transition_base>( NewStyle.getGeometryTransition() );
                
                TransformStyle.onTransition( KeyFrame[2], KeyFrame[0], m_CurStateTransitionTime, KeyFrame[1] );
            }
            else
            {
                KeyFrame[2] = KeyFrame[0].Blend( m_CurStateTransitionTime, KeyFrame[1] );
            }
            
            // Compute the final local matrix (Make sure it is in the local space of the object)
            xmatrix4 M( KeyFrame[2].m_Scale, KeyFrame[2].m_Rotation, KeyFrame[2].m_Translation );
            
            // Concadenate it the exisiting matrix
            Info.m_Matrix = Info.m_Matrix * M;
            
            // Get the render area center
            const xvector2 V2 = Info.m_RenderArea.GetCenter();
            const xvector3 V3( V2.m_X, V2.m_Y, 0 );
            
            Info.m_Matrix.PreTranslate( -V3 );
            Info.m_Matrix.Translate( V3 );
            Info.m_Matrix = Info.m_pParent->m_Matrix * Info.m_Matrix;
            
        }
    }
    else
    {
        if( m_gStyle.isValid() )
        {
            const uicontrol_base_style& BaseStyle   = m_pManager->getStyle<const uicontrol_base_style>( m_gStyle );
            const uistyle_sheet&        NewStyle    = m_pManager->getStyle<const uistyle_sheet>( BaseStyle.m_gStyleSheet[ m_CurState ] );
            
            NewStyle.ComputeRenderMatrix( Info );
        }
        else
        {
            // Get the render area center
            const xvector2 V2 = Info.m_RenderArea.GetCenter();
            const xvector3 V3( V2.m_X, V2.m_Y, 0 );
            
            Info.m_Matrix.PreTranslate( -V3 );
            Info.m_Matrix.Translate( V3 );
            Info.m_Matrix = Info.m_pParent->m_Matrix * Info.m_Matrix;
        }
    }
}

//-------------------------------------------------------------------------------

void uicontro_base::onRender( uistyle_base::render& Info )
{
    Info.m_RenderArea = m_Region.getRect( Info.m_pParent->m_RenderArea );

    //
    // Compute the Matrix and Transition Time
    //
    ComputeTheMatrixAndTime( Info );
    
    //
    // Now that we have the final matrix lets compute the states
    //
    onHandleStates( Info );
    
    //
    // Nothing special to draw?
    //
    if( !m_gStyle.isValid() )
    {
        uicontrol_data_ref Data( 0,
                                 1,
                                 Info,
                                 NULL,
                                 NULL);
        onRender( Data );
    }
    //
    // Deal with transitions
    //
    else if( m_CurStateTransitionTime < 1 )
    {
        const uicontrol_base_style&     BaseStyle = m_pManager->getStyle<const uicontrol_base_style>( m_gStyle );
        const uicontrol_style_sheet&    NewStyle  = m_pManager->getStyle<const uicontrol_style_sheet>( BaseStyle.m_gStyleSheet[ m_CurState ] );
        const uicontrol_style_sheet&    OldStyle  = m_pManager->getStyle<const uicontrol_style_sheet>( BaseStyle.m_gStyleSheet[ m_OldState ] );
        
        const f32   T0          ( Info.m_Alpha * (1-m_CurStateTransitionTime) );
        const f32   T1          ( Info.m_Alpha * m_CurStateTransitionTime );
        
        //
        // Render the style sheets for both times
        //

        // Render first style
        Info.m_Alpha = T0;
        const xrect Rect1( OldStyle.onRender( Info, FALSE ) );
        
        // Render the second style
        Info.m_Alpha = T1;
        const xrect Rect2( NewStyle.onRender( Info, FALSE ) );
        
        //
        // Compute the final render area
        //
        Info.m_RenderArea = Rect1.Interpolate( m_CurStateTransitionTime, Rect2 );
        

        uicontrol_data_ref Data( T0,
                                 T1,
                                 Info,
                                 &NewStyle,
                                 &OldStyle );
        onRender( Data );
    }
    //
    // Normal case
    //
    else
    {
        const uicontrol_base_style& BaseStyle   = m_pManager->getStyle<const uicontrol_base_style>( m_gStyle );
        const uistyle_sheet&        NewStyle    = m_pManager->getStyle<const uistyle_sheet>( BaseStyle.m_gStyleSheet[ m_CurState ] );
        
        // Compute final render area
        Info.m_RenderArea = NewStyle.onRender( Info, FALSE );
        
        m_CurStateTransitionTime = 1;
        
        uicontrol_data_ref Data( 0,
                                 Info.m_Alpha,
                                 Info,
                                 &NewStyle,
                                 NULL );
        onRender( Data );
    }
}

//-------------------------------------------------------------------------------

xbool uicontro_base::onInput( const xrect& ParentRect, const xvector2& Pos )
{
    
    return FALSE;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

uicontrol_static::uicontrol_static( void )
{
    m_gContent.setNull();
}

//-------------------------------------------------------------------------------

void uicontrol_static::onRender( uicontrol_data_ref& Ref )
{
    //
    // Make sure we have something to display
    //
    if( m_gContent.IsNull() )
        return;
    
    //
    // Render the content
    //
    uicontent_base& Content = m_pManager->getContent<uicontent_base>( m_gContent );
    
    if( Ref.m_T0 )
    {
        Ref.m_RenderInfo.m_Alpha = Ref.m_T0;
        Content.onRender( Ref.m_pOldStyleSheet->getElement(), Ref.m_RenderInfo );
    }
    
    if( Ref.m_T1 )
    {
        Ref.m_RenderInfo.m_Alpha = Ref.m_T1;
        Content.onRender( Ref.m_pCurStyleSheet->getElement(), Ref.m_RenderInfo );
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------

uicontrol_button::uicontrol_button( void )
{
    m_gContent.setNull();
    m_Action   = ACTION_ONRELEASE;
    m_Type     = TYPE_PUSHBUTTON;
    m_bPressed = FALSE;
}

//-------------------------------------------------------------------------------

void uicontrol_button::onClick( void )
{
    
}

//-------------------------------------------------------------------------------

void uicontrol_button::onRender( uicontrol_data_ref& Ref )
{
    //
    // If we have nothing to render bail
    //
    if( m_gContent.IsNull() )
        return;
    
    //
    // Render the content
    //
    uicontent_base& Content = m_pManager->getContent<uicontent_base>( m_gContent );

    if( Ref.m_T0 )
    {
        Ref.m_RenderInfo.m_Alpha = Ref.m_T0;
        Content.onRender( Ref.m_pOldStyleSheet->getElement(), Ref.m_RenderInfo );
    }
    
    if( Ref.m_T1 )
    {
        Ref.m_RenderInfo.m_Alpha = Ref.m_T1;
        Content.onRender( Ref.m_pCurStyleSheet->getElement(), Ref.m_RenderInfo );
    }
    
}

//-------------------------------------------------------------------------------

void uicontrol_button::onHandleStates( uistyle_base::render& Info )
{
    //
    // If someone else has the mouse lock then there is nothing to do.
    //
    if( CanChangeStates() == FALSE )
        return;
    
    // Deal with the base functionality
    if( IsMouseInRegion( Info ) )
    {
        if( m_pManager->getIsMouseLButton() )
        {
            if( m_Action != ACTION_ONPRESS ) m_pManager->setMouseLock( this );
            
            if( m_Action == ACTION_ONPRESS )
            {
                if( m_CurState == uicontrol_base_style::STATE_HOVER )
                {
                    if( m_Type == TYPE_RADIOBUTTON )
                        m_bPressed = !m_bPressed;
                    TriggerControlTransition();
                    onClick();
                }
            }
            
            if( m_Type == TYPE_RADIOBUTTON )
            {
                if( m_bPressed ) setState(uicontrol_base_style::STATE_ACTIVE);
                else             setState(uicontrol_base_style::STATE_SELECTED);
            }
            else
            {
                if(m_CurState == uicontrol_base_style::STATE_HOVER) TriggerControlTransition();
                setState(uicontrol_base_style::STATE_SELECTED);
            }
        }
        else
        {
            if( m_Type == TYPE_RADIOBUTTON )
            {
                if( m_Action == ACTION_ONRELEASE &&
                    ( (m_CurState == uicontrol_base_style::STATE_SELECTED && !m_bPressed ) ||
                      (m_CurState == uicontrol_base_style::STATE_ACTIVE   &&  m_bPressed ) )  )
                {
                    TriggerControlTransition();
                    onClick();
                    m_bPressed = !m_bPressed;
                }
            }
            else
            {
                if( m_CurState == uicontrol_base_style::STATE_SELECTED )
                {
                    TriggerControlTransition();
                    onClick();
                }
                setState(uicontrol_base_style::STATE_HOVER);
                m_pManager->setMouseLock( NULL );
            }
        }
    }
    else
    {
        // Release the lock as soon as the user releases the mouse
        if( m_pManager->getIsMouseLButton() == FALSE )
        {
            m_pManager->setMouseLock( NULL );
        }
        
        if( m_Type == TYPE_RADIOBUTTON && m_bPressed )
        {
            setState(uicontrol_base_style::STATE_SELECTED );
        }
        else
        {
            setState(uicontrol_base_style::STATE_ACTIVE );
        }
    }
}


