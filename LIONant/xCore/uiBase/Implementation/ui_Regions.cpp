//
//  ui_Regions.cpp
//
//  Created by Tomas Arce on 1/20/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "ui_base.h"

XFACTORY_INSTANCE( uicontainer_area,       g_RegionTypes  )


//-------------------------------------------------------------------------------

uiregion::uiregion( void )
{
    m_Region.setup( uirect::TYPE_ABSOLUTE, xrect( 0, 0, 100, 100 ) );
    
    m_Guid.setNull();
    m_gStyle.setNull();
}

//-------------------------------------------------------------------------------

void uiregion::onRender( uistyle_base::render& Info )
{
    if( Info.m_pParent )
    {
        Info.m_RenderArea = m_Region.getRect( Info.m_pParent->m_RenderArea );
    }
    Info.m_Alpha = 1;
    
    //
    // Render the style sheet first
    //
    if( m_gStyle.isValid() )
    {
        uistyle_sheet& StyleSheet = m_pManager->getStyle<uistyle_sheet>( m_gStyle );
        Info.m_RenderArea = StyleSheet.onRender( Info );
    }
}

//-------------------------------------------------------------------------------

xbool uiregion::onInput( const xrect& ParentRect, const xvector2& Pos )
{
    xrect Region( m_Region.getRect( ParentRect ) );
    return Region.PointInRect( Pos );
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void uicontainer_area::AddRegion( uiregion& Region )
{
    m_Regions.append() = &Region;
}

//-------------------------------------------------------------------------------

void uicontainer_area::onRender( uistyle_base::render& Info  )
{
    // Give a chance to render my self
    uiregion::onRender( Info );
    
    // Now can render all the children
    for( s32 i=0; i<m_Regions.getCount(); i++ )
    {
        uistyle_base::render RegionInfo( &Info );
        m_Regions[i]->onRender( RegionInfo );
    }
}

//-------------------------------------------------------------------------------

xbool uicontainer_area::onInput( const xrect& ParentRect, const xvector2& Pos )
{
    xrect Region( m_Region.getRect( ParentRect ) );
    
    if( FALSE == Region.PointInRect( Pos ) )
        return FALSE;
    
    // Now can render all the children
    for( s32 i=0; i<m_Regions.getCount(); i++ )
    {
        if( TRUE == m_Regions[i]->onInput( Region, Pos ) )
            return TRUE;
    }
    
    return FALSE;
}

//-------------------------------------------------------------------------------

void uicontainer_area::RenderTopContainer( const xrect& ScreenRes  )
{
    xmatrix4                Matrix;
    Matrix.Identity();
    
    uistyle_base::render RenderInfo( NULL );
    
    RenderInfo.m_RenderArea = ScreenRes;
    
    onRender( RenderInfo );
}

