//
//  ui_base.cpp
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "ui_base.h"

XFACTORY_VARBASE( g_StyleTypes )
XFACTORY_VARBASE( g_RegionTypes )
XFACTORY_VARBASE( g_ContentTypes )


//-------------------------------------------------------------------------------

uiregion& uimanager::CreateRegion( const char* pTypeName )
{
    const xfactory* pFactory = xfactory::FindFactory( pTypeName, g_RegionTypes );
    ASSERT( pFactory );
    
    return CreateRegion( *pFactory );
}

//-------------------------------------------------------------------------------

uiregion& uimanager::CreateRegion( const xfactory& Factory, uicontainer_base& Container )
{
    uiregion::guid      Guid;
    Guid.ResetValue();
    
    return CreateRegion( Factory, Guid, Container );
}

//-------------------------------------------------------------------------------

uiregion& uimanager::CreateRegion( const xfactory& Factory, uiregion::guid gRegion, uicontainer_base& Container )
{
    ASSERT( m_lRegionHash.IsAlloc( gRegion ) == FALSE );
    
    uiregion& Region = uiregion::SafeCast( *((uiregion*)Factory.CreateInstance()) );
    Region.m_Guid = gRegion;
    Region.m_pManager = this;
    
    m_lRegionHash.Add( gRegion, &Region );
    Container.AddRegion( Region );
    
    return Region;
}

//-------------------------------------------------------------------------------

uistyle_base& uimanager::CreateStyle( const char* pTypeName )
{
    const xfactory* pFactory = xfactory::FindFactory( pTypeName, g_RegionTypes );
    ASSERT( pFactory );
    
    return CreateStyle( *pFactory );
}

//-------------------------------------------------------------------------------

uistyle_base& uimanager::CreateStyle( const xfactory& Factory )
{
    uistyle_base::guid      Guid;
    Guid.ResetValue();
    
    return CreateStyle( Factory, Guid );
}

//-------------------------------------------------------------------------------

uistyle_base& uimanager::CreateStyle( const xfactory& Factory, uistyle_base::guid gStyle )
{
    ASSERT( m_lStylesHash.IsAlloc( gStyle ) == FALSE );
    
    uistyle_base& StyleSheet = uistyle_base::SafeCast( *((uistyle_base*)Factory.CreateInstance()) );
    StyleSheet.m_Guid = gStyle;
    StyleSheet.m_pManager = this;
    
    m_lStylesHash.Add( gStyle, &StyleSheet );
    
    return StyleSheet;
}

//-------------------------------------------------------------------------------

uicontent_base& uimanager::CreateContent( const char* pTypeName )
{
    const xfactory* pFactory = xfactory::FindFactory( pTypeName, g_RegionTypes );
    ASSERT( pFactory );
    
    return CreateContent( *pFactory );
}

//-------------------------------------------------------------------------------

uicontent_base& uimanager::CreateContent( const xfactory& Factory )
{
    uicontent_base::guid      Guid;
    Guid.ResetValue();
    
    return CreateContent( Factory, Guid );
}

//-------------------------------------------------------------------------------

uicontent_base& uimanager::CreateContent( const xfactory& Factory, uicontent_base::guid gContent )
{
//    ASSERT( m_lContentHash.IsAlloc( gStyle ) == FALSE );
    
    uicontent_base& Content = uicontent_base::SafeCast( *((uicontent_base*)Factory.CreateInstance()) );
    Content.m_Guid = gContent;
    Content.m_pManager = this;
    
    m_lContentHash.Add( gContent, &Content );
    
    return Content;
}


//-------------------------------------------------------------------------------
/*
void uimanager::RegisterListener( xmsg_receiver& Listener, uiregion::guid gRegion )
{
    hlistener Handle;
    
    //
    // Make sure it is there
    //
    if( !m_lRegionHash.IsAlloc( gRegion ) )
    {
        CreateRegion( gRegion );
    }
    
    //
    // Get the actual region
    //
    uiregion&  Region = *m_lRegionHash.GetValue( gRegion );
    Region.AddReceiver( Listener );
}
*/

//-------------------------------------------------------------------------------

void uimanager::Render( f32 DeltaTime )
{
    m_DeltaTime = DeltaTime;
    
    eng_context& Context = eng_GetCurrentContext();
    
    Context.qtAddRenderJob( *this );
}

//-------------------------------------------------------------------------------
// Job for rendering
//-------------------------------------------------------------------------------
void uimanager::onRunRenderJob( void ) const
{
    uimanager*      pThis       = const_cast<uimanager*>(this);
    eng_context&    Context     = eng_GetCurrentContext();
    s32             W, H;
    
    Context.GetScreenResolution( W, H );
    
    // Get draw for all the subsequent calls
    pThis->m_pDraw  = &Context.getDraw();
    
    xrect ScreenRes( 0, 0, f32(W), f32(H) );
    
    xbool LockIt = FALSE;
    if( getMouseLock() == NULL && getIsMouseLButton() )
        LockIt = TRUE;
    else if( getMouseLock() == &m_TopContainer && getIsMouseLButton() == FALSE )
    {
        pThis->setMouseLock( NULL );
    }
    
    pThis->m_TopContainer.RenderTopContainer( ScreenRes );
    
    if( LockIt && getMouseLock() == NULL )
    {
        pThis->setMouseLock( &m_TopContainer );
    }
}

//-------------------------------------------------------------------------------

void  uimanager::Input( xvector2& Pos, xbool Is, xbool Was  )
{
    eng_context& Context = eng_GetCurrentContext();
    s32          W, H;
    
    Context.GetScreenResolution( W, H );
    
    // Get draw for all the subsecuent calls
    m_pDraw  = &Context.getDraw();
    
    xrect ScreenRes( 0,0, f32(W), f32(H) );
    
    m_MousePos     = Pos;
    m_MousePos.m_Y = H - Pos.m_Y;
    
    m_MouseIsLeftButton = Is;
    m_MouseWasLeftButton = Was;
    
    m_TopContainer.onInput( ScreenRes, m_MousePos );
}