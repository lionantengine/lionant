//
//  ui_Content.cpp
//  uiBase
//
//  Created by Tomas Arce on 2/22/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "ui_base.h"

XFACTORY_INSTANCE( uicontent_text,      g_ContentTypes )
XFACTORY_INSTANCE( uicontent_sprite,    g_ContentTypes )

static uistyle_content  s_DefaultContent;

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void eng_DrawString( const xmatrix4& L2W, const char *s, const f32 x, const f32 y, const xcolor Color );

void uicontent_text::onRender(const uistyle_content::guid gElement, const uistyle_base::render& Render )
{
    uistyle_content* pContent;
    
    if( gElement.isValid() )
    {
        pContent = &m_pManager->getStyle<uistyle_content>( gElement );
    }
    else
    {
        pContent = &s_DefaultContent;
    }
    
    xbool bDrawn = FALSE;
    if( pContent->m_gContentStyle.isValid() )
    {
        uistyle_font& Font = m_pManager->getStyle<uistyle_font>( pContent->m_gContentStyle );
        if( Font.m_Font.Resolve() )
        {
            Font.m_Font.DrawText( 
                eng_GetCurrentContext().getDraw(), 
                Render.m_Matrix, 
                xvector2( Render.m_RenderArea.m_Left, 
                          Render.m_RenderArea.m_Top ), 
                pContent->m_ContentScale,
                m_String, 
                pContent->m_Color.ComputeNewAlpha( Render.m_Alpha ) );
        
            bDrawn = TRUE;
        }
    }

    if( bDrawn == FALSE )
    {
        // TODO: Need to compute the final string position base on the content alighment
        eng_DrawString( Render.m_Matrix, m_String, Render.m_RenderArea.m_Left, Render.m_RenderArea.m_Top, pContent->m_Color.ComputeNewAlpha( Render.m_Alpha ) );
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void uicontent_sprite::onRender( const uistyle_content::guid gElement, const uistyle_base::render& Render )
{
    uistyle_content* pContent;

    if ( gElement.isValid() )
    {
        pContent = &m_pManager->getStyle<uistyle_content>( gElement );
    }
    else
    {
        pContent = &s_DefaultContent;
    }

    if ( m_Sprite.Resolve() )
    {
        m_Sprite.DrawSprite( 
            eng_GetCurrentContext().getDraw(),
            xvector2( Render.m_RenderArea.m_Left,
                        Render.m_RenderArea.m_Top ),
            pContent->m_ContentScale
        );
    }
}