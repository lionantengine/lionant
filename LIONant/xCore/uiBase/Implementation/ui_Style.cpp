//
//  ui_Style.cpp
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "ui_base.h"

XFACTORY_INSTANCE( uistyle_sheet,       g_StyleTypes )
XFACTORY_INSTANCE( uistyle_background,  g_StyleTypes )
XFACTORY_INSTANCE( uistyle_boder,       g_StyleTypes )
XFACTORY_INSTANCE( uistyle_content,     g_StyleTypes )
XFACTORY_INSTANCE( uistyle_transform,   g_StyleTypes )
XFACTORY_INSTANCE( uistyle_font,        g_StyleTypes )
XFACTORY_INSTANCE( uistyle_sprite,      g_StyleTypes )

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------

void uistyle_background::Render( const render& Render )
{
    if( m_Texture.isValid() )
    {
        
    }
    else
    {
        m_pManager->m_pDraw->Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_RASTER_SOLID | ENG_DRAW_MODE_BLEND_ALPHA );
        m_pManager->m_pDraw->SetL2W( Render.m_Matrix );
        m_pManager->m_pDraw->DrawShadedRect( Render.m_RenderArea,
                                             m_Color[0].ComputeNewAlpha(Render.m_Alpha),
                                             m_Color[1].ComputeNewAlpha(Render.m_Alpha),
                                             m_Color[2].ComputeNewAlpha(Render.m_Alpha),
                                             m_Color[3].ComputeNewAlpha(Render.m_Alpha) );
        m_pManager->m_pDraw->End();
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

 X_BEGIN_MESSAGE_MAP( uistyle_sheet, xmsg_receiver )                               // Message declaration table
 X_END_MESSAGE_MAP

//-------------------------------------------------------------------------------

uistyle_sheet::uistyle_sheet( void )
{
    m_gMarginStyle.setNull();
    m_gFrameStyle.setNull();
    m_gBackground.setNull();
    m_gElementStyle.setNull();
    m_gTransformStyle.setNull();
    
    m_Margin.Zero   ( uirect::TYPE_ABS_DELTA );
    m_Frame.Zero    ( uirect::TYPE_ABS_DELTA );
    m_Padding.Zero  ( uirect::TYPE_ABS_DELTA );
}

//-------------------------------------------------------------------------------

void uistyle_sheet::ComputeRenderMatrix( render& Render ) const
{
    const xvector2 V2 = Render.m_RenderArea.GetCenter();
    const xvector3 V3(V2.m_X, V2.m_Y, 0 );
    
    if( m_gTransformStyle.isValid() )
    {
        uistyle_transform& TransformStyle = m_pManager->getStyle<uistyle_transform>( m_gTransformStyle );
        
        xmatrix4 Transform( TransformStyle.m_KeyFrame.m_Scale, TransformStyle.m_KeyFrame.m_Rotation, TransformStyle.m_KeyFrame.m_Translation );
        
        Transform.PreTranslate( -V3 );
        
        Render.m_Matrix = Render.m_Matrix * Transform;
    }
    else
    {
        // Assume that Matrix has some useful operations and it is to be done in the local space of the area
        Render.m_Matrix.PreTranslate( -V3 );
    }
    
    // Put the area back
    Render.m_Matrix.Translate( V3 );
    
    // If it has a parent concadenate with it
    if( Render.m_pParent )
    {
        Render.m_Matrix = Render.m_pParent->m_Matrix * Render.m_Matrix;
    }
}

//-------------------------------------------------------------------------------

xrect uistyle_sheet::onRender( render& Render, const xbool bUseTransform ) const
{
    const xrect Frame   ( m_Frame.getRect  ( Render.m_RenderArea, FALSE ) );
    const xrect Padding ( m_Padding.getRect( Render.m_RenderArea ) );
    
    //
    // Deal with transformation from the style
    //
    if( bUseTransform )
    {
        ComputeRenderMatrix( Render );
    }
    
    //
    // Render the frame if we have a style for it
    //
    if( m_gFrameStyle.isValid() )
    {
        uistyle_boder& FrameStyle = m_pManager->getStyle<uistyle_boder>( m_gFrameStyle );
        
        FrameStyle.Render( Render, Frame );
    }
    
    //
    // Render Margin style
    //
    if( m_gMarginStyle.isValid() )
    {
        const xrect Margin( m_Margin.getRect ( Frame, FALSE ) );
        
        uistyle_boder& MarginStyle = m_pManager->getStyle<uistyle_boder>( m_gMarginStyle );
        
        MarginStyle.Render( Render, Frame, Margin );
    }
    
    //
    // Render the Background
    //
    if( m_gBackground.isValid() )
    {
        uistyle_background& Background = m_pManager->getStyle<uistyle_background>( m_gBackground );
        
        Background.Render( Render );
    }

    return Padding;
}

//-------------------------------------------------------------------------------

const uistyle_sheet& uistyle_sheet::operator = ( const uistyle_sheet& Style )
{
    m_FieldFlags    = Style.m_FieldFlags;
    m_gParent       = Style.m_gParent;
   
    m_Margin        = Style.m_Margin;                       // From Region to Margin (Delta)
    m_Frame         = Style.m_Frame;                        // From Region to Frame  (Delta)
    m_Padding       = Style.m_Padding;                      // From Frame to Padding (Delta)
   
   
    m_gMarginStyle      = Style.m_gMarginStyle;                 // Margin style
    m_gFrameStyle       = Style.m_gFrameStyle;                  // Frame style
    m_gBackground       = Style.m_gBackground;                  // Background style for the region
    m_gElementStyle     = Style.m_gElementStyle;                // Element to be render inside the Padding
    m_gTransformStyle   = Style.m_gTransformStyle;

    return *this;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

void uistyle_boder::Render( const render& Render, const xrect& From, const xrect& To ) const
{
    if( 0 && m_bUseSprite )
    {
       // m_Sprite.Render( )
    }
    else
    {
        xcolor C[4];
        
        for( s32 i=0; i<4; i++ )
        {
            C[i] = m_Color[i].ComputeNewAlpha( Render.m_Alpha );
        }
        
        m_pManager->m_pDraw->Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_RASTER_SOLID | ENG_DRAW_MODE_BLEND_ALPHA );
        m_pManager->m_pDraw->SetL2W( Render.m_Matrix );
        
        xrect Region1( From.m_Left, From.m_Top, From.m_Right, To.m_Top );
        m_pManager->m_pDraw->DrawShadedRect( Region1, C[0], C[1], C[2], C[3] );
        
        xrect Region2( From.m_Left, To.m_Top, To.m_Left, To.m_Bottom );
        m_pManager->m_pDraw->DrawShadedRect( Region2, C[0], C[1], C[2], C[3] );

        xrect Region3( From.m_Left, To.m_Bottom, From.m_Right, From.m_Bottom );
        m_pManager->m_pDraw->DrawShadedRect( Region3, C[0], C[1], C[2], C[3] );

        xrect Region4( To.m_Right, To.m_Top, From.m_Right, To.m_Bottom );
        m_pManager->m_pDraw->DrawShadedRect( Region4, C[0], C[1], C[2], C[3] );
        
        m_pManager->m_pDraw->End();
        
    }
}