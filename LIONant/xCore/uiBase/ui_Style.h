//
//  ui_Style.h
//
//  Created by Tomas Arce on 1/19/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

class uimanager;

///////////////////////////////////
// RECTANGLE
///////////////////////////////////
struct uirect
{
    enum type
    {
        TYPE_GLOBAL_ABSOLUTE,
        TYPE_ABSOLUTE,
        TYPE_PERCENT,
        TYPE_ABS_DELTA,
        TYPE_PECENT_DELTA
    };
    
    // This must match with xrect order
    enum direction
    {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    };
    
    uirect( void ){}
    uirect( type Type, const xrect& Rect )
    {
        setup( Type, Rect );
    }

    void Zero( type Type = TYPE_ABSOLUTE )
    {
        setupEdge( LEFT,   Type, 0 );
        setupEdge( TOP,    Type, 0 );
        setupEdge( RIGHT,  Type, 0 );
        setupEdge( BOTTOM, Type, 0 );
    }
    
    void setup( type Type, const xrect& Rect )
    {
        setupEdge( LEFT,   Type, Rect.m_Left                );
        setupEdge( TOP,    Type, Rect.m_Top                 );
        setupEdge( RIGHT,  Type, Rect.m_Right - Rect.m_Left );
        setupEdge( BOTTOM, Type, Rect.m_Bottom - Rect.m_Top );
    }
    
    void setup( type Type, const xrectwh& Rect )
    {
        setupEdge( LEFT,   Type, Rect.m_Pos.m_X     );
        setupEdge( TOP,    Type, Rect.m_Pos.m_Y     );
        setupEdge( RIGHT,  Type, Rect.m_W           );
        setupEdge( BOTTOM, Type, Rect.m_H           );
    }
    
    void setupEdge( direction Dir, type Type, f32 Value )
    {
        m_EdgeType[ Dir ] = Type;
        m_Edge[ Dir ]     = Value;
    }
    
    /*
    void            setup               ( type Type, const xrect& Rect )
    {
        setupEdge( LEFT,   Type, Rect.m_Left      );
        setupEdge( TOP,    Type, Rect.m_Top       );
        setupEdge( WIDTH,  Type, Rect.GetWidth()  );
        setupEdge( HEIGHT, Type, Rect.GetHeight() );
    }
    
    void            setupEdge           ( direction Dir, type Type, f32 Value ) { m_EdgeType[ Dir ] = Type; m_Edge[ Dir ] = Value; }
    f32             getEdgeValue        ( direction Dir ) const                 { return m_Edge[ Dir ]; }
    type            getEdgeType         ( direction Dir ) const                 { return m_EdgeType[ Dir ]; }
    
    
    f32             ConvertEdgeValueTo  ( direction Dir, type Type, const xrect& Rect )
    {
        static const f32    Sign[] = { 1, 1 -1, -1 };
        const f32           WH[]   = { Rect.GetWidth(), Rect.GetHeight() };
        const f32*  const   pIn    = (const f32*)&Rect;
        const f32           eVal   = m_Edge[ Dir ];
        const s32           e      = Dir&1;
        
        // First convert to ABS value
        f32 AbsValue;
        switch( m_Type[Dir] )
        {
            case TYPE_ABSOLUTE:         AbsValue = eVal; break;
            case TYPE_GLOBAL_ABSOLUTE:  AbsValue = eVal - pIn[e]; break;
            case TYPE_PERCENT:          AbsValue = WH[e] * eVal; break;
            case TYPE_ABS_DELTA:        AbsValue = pIn[Dir] + Sign[Dir] * eVal - pIn[e]; break;
            case TYPE_PECENT_DELTA:     AbsValue = pIn[Dir] + Sign[Dir] * WH[e] * eVal - pIn[e]; break;
            default: ASSERT(0);
        }
        
        // From ABS value to the destination
        f32 Value;
        switch( Type )
        {
            case TYPE_ABSOLUTE:         Value = AbsValue; break;
            case TYPE_GLOBAL_ABSOLUTE:  Value = AbsValue + pIn[e]; break;
            case TYPE_PERCENT:          Value = ( AbsValue ) / WH[e]; break;
            case TYPE_ABS_DELTA:        Value = AbsValue - Sign[Dir] * eVal; break;
            case TYPE_PECENT_DELTA:     Value = ( AbsValue - Sign[Dir] * eVal ) / WH[e]; break;
            default: ASSERT(0);
        }
        
        return Value;
    }
    
    void            ChangeEdgeType      ( direction Dir, type Type, const xrect& Rect )
    {
        // Update to the new type
        m_Type[ Dir ] = Type;
        m_Edge[ Dir ] = ConvertEdgeValueTo( Dir, Type, Rect );
    }
    
    xrect getRectMargin( const xrect& Rect ) const
    {
        xrect               Out;
        static const f32    Sign[] = { -1, -1, 1, 1 };
        const f32           WH[]   = { Rect.GetWidth(), Rect.GetHeight() };
        const f32*  const   pIn    = (const f32*)&Rect;
        f32* const          pOut   = (f32*)&Out;
        
        for( s32 i=0; i<4; i++ )
        {
            const s32 e = i&1;
            
            switch( m_Type[i] )
            {
                case TYPE_ABSOLUTE:         pOut[i] = m_Edge[i] + pIn[e]; break;
                case TYPE_GLOBAL_ABSOLUTE:  pOut[i] = m_Edge[i]; break;
                case TYPE_PERCENT:          pOut[i] = Sign[i] * WH[e] * m_Edge[i] + pIn[e]; break;
                case TYPE_ABS_DELTA:        pOut[i] = Sign[i] * m_Edge[i] + pIn[i]; break;
                case TYPE_PECENT_DELTA:     pOut[i] = Sign[i] * WH[e] * m_Edge[i] + pIn[i]; break;
                default: ASSERT(0);
            }
        }
        
        return Out;
    }
     */
   
    xrect getRect( const xrect& Rect, const xbool Inner = TRUE ) const
    {
        xrect               Out;
        static const f32    SignTable[] = { -1, -1, 1, 1, -1, -1 };
        const f32* const    Sign   = &SignTable[ Inner*2];
        const f32           InWH[] = { Rect.GetWidth(), Rect.GetHeight() };
        const f32*  const   pIn    = (const f32*)&Rect;
        f32* const          pOut   = (f32*)&Out;
        const f32           Edge[] = { m_Edge[0], m_Edge[1], m_Edge[0] + m_Edge[2], m_Edge[1] + m_Edge[3] };

        for( s32 i=0; i<4; i++ )
        {
            const s32 e = i&1;
            
            switch( m_EdgeType[i] )
            {
                case TYPE_ABSOLUTE:         pOut[i] = Edge[i] + pIn[e]; break;
                case TYPE_GLOBAL_ABSOLUTE:  pOut[i] = Edge[i]; break;
                case TYPE_PERCENT:          pOut[i] = InWH[e] * Edge[i] + pIn[e]; break;
                case TYPE_ABS_DELTA:        pOut[i] = pIn[i] + Sign[i] * Edge[i]; break;
                case TYPE_PECENT_DELTA:     pOut[i] = pIn[i] + Sign[i] * InWH[e] * Edge[i]; break;
                default: ASSERT(0);
            }
        }
        
        return Out;
    }

protected:
    
    type        m_EdgeType[4];      // LEFT EDGE, TOP EDGE, RIGHT EDGE, BOTTOM EDGE
    f32         m_Edge[4];          // Data is store as X,Y,W,H
};

///////////////////////////////////
// BASE CLASS FOR ALL STYLES
///////////////////////////////////
struct uistyle_base : public xproperty
{
    struct guid : public xguid { guid(void)=default; guid(u64 x) : xguid(x){} };
    x_rtti_class1( uistyle_base, xproperty );
    
    struct render
    {
        render( const render* const pParent ) : m_pParent( pParent )
        {
            m_Matrix.Identity();
            if( m_pParent )
            {
                m_Alpha  = m_pParent->m_Alpha;
            }
            else
            {
                m_Alpha = 1.0;
            }
        }
        
        xrect                   m_RenderArea;
        mutable xmatrix4        m_Matrix;
        f32                     m_Alpha;
        const render* const     m_pParent;
    };
    
                                    uistyle_base    ( void ){}
            xbool                   onPropQuery     ( xproperty_query& Query ) override         { return FALSE; }
            void                    onPropEnum      ( xproperty_enum&  Enum  ) const override   { }  
            guid                    getGuid         ( void ) const { return m_Guid; }
    
    xstring                 m_StyleName;
    guid                    m_Guid;
    s32                     m_RefCount;
    uimanager*              m_pManager;
};

///////////////////////////////////
// BORDER STYLE - Texture/Color This is used for the margin and the frame
///////////////////////////////////
struct uistyle_boder : public uistyle_base
{
    x_rtti_class1( uistyle_boder, uistyle_base );
    XFACTORY_CLASS( uistyle_boder )
    
    void    Render( const render& StyleRender, const xrect& From ) const { Render( StyleRender, From, StyleRender.m_RenderArea ); }
    void    Render( const render& StyleRender, const xrect& From, const xrect& To ) const;
    void    setColor( xcolor C ) { m_Color[0] = m_Color[1] = m_Color[2] = m_Color[3] = C; }
    
    xbool                   m_bUseSprite;
    xcolor                  m_Color[4];
    eng_sprite              m_Sprite[4];
};

///////////////////////////////////
// BACKGROUND STYLE - Texture/Color
///////////////////////////////////
struct uistyle_background : public uistyle_base
{
    x_rtti_class1( uistyle_background, uistyle_base );
    XFACTORY_CLASS( uistyle_background )
    
    void setColor( xcolor C ){ m_Color[0] = m_Color[1] = m_Color[2] = m_Color[3] = C; }
    
    void Render( const render& Render );
    
    xcolor                  m_Color[4];
    eng_texture             m_Texture;
};

///////////////////////////////////
// ELEMENT STYLE - Includes things like aligment and basic color
///////////////////////////////////
struct uistyle_content : public uistyle_base
{
    x_rtti_class1( uistyle_content, uistyle_base );
    XFACTORY_CLASS( uistyle_content )
    
    enum haligment
    {
        HALIGMENT_CENTER,
        HALIGMENT_LEFT,
        HALIGMENT_RIGHT
    };
    
    enum valigment
    {
        VALIGMENT_CENTER,
        VALIGMENT_TOP,
        VALIGMENT_BOTTOM
    };
    
    xcolor                  m_Color         = 0xffffffff;
    haligment               m_HAligment     = HALIGMENT_LEFT;
    valigment               m_VAligment     = VALIGMENT_TOP;
    uistyle_base::guid      m_gContentStyle = 0;                   // Element to be render inside the Padding
    xvector2                m_ContentScale  = xvector2(1.0f);
};

///////////////////////////////////
// ELEMENT STYLE TRANSFORM
///////////////////////////////////
struct uistyle_transform : public uistyle_base
{
    x_rtti_class1( uistyle_transform, uistyle_base );
    XFACTORY_CLASS( uistyle_transform )

    struct key_frame
    {
        void        Identity( void ) { m_Rotation.Identity(); m_Scale.Set(1,1,1), m_Translation.Zero(); }
        key_frame   Blend   ( const f32 T, const key_frame& End ) const
        {
            key_frame Key;
            Key.m_Rotation      = m_Rotation.BlendAccurate( T, End.m_Rotation );
            Key.m_Translation   = m_Translation.Blend( T, End.m_Translation );
            Key.m_Scale         = m_Scale.Blend( T, End.m_Scale );
            
            return Key;
        }
        
        xquaternion m_Rotation;
        xvector3    m_Translation;
        xvector3    m_Scale;
    };
    
    key_frame   m_KeyFrame;
};

///////////////////////////////////
// ELEMENT STYLE TRANSFORM
///////////////////////////////////
struct uistyle_font : public uistyle_base
{
    x_rtti_class1( uistyle_font, uistyle_base );
    XFACTORY_CLASS( uistyle_font )

    eng_font    m_Font;
};

///////////////////////////////////
// ELEMENT STYLE TRANSFORM
///////////////////////////////////
struct uistyle_sprite : public uistyle_base
{
    x_rtti_class1( uistyle_sprite, uistyle_base );
    XFACTORY_CLASS( uistyle_sprite )

    
};

///////////////////////////////////
// STYLE SHEET
///////////////////////////////////
//                 Margin
//   +----------------------------
//   |             Frame
//   |  +-------------------------
//   |  |          Region
//   |  |  +----------------------
//   |  |  |       Padding
//   |  |  |  +-------------------
//   |  |  |  |    Content
//   |  |  |  |
//   |  |  |  +-------------------
//   |  |  +----------------------
//   |  +-------------------------
//   +----------------------------
//
// Margin  - Clears an area around the region. The margin does not have a background color, it is completely transparent
// Region  - The actual window position. This is where everything starts drawing from.
// Frame   - (From Region to Frame) A frame that goes around the padding and content. The frame is inherited from the color property of the box
// Padding - Clears an area around the content. The padding is affected by the background color of the box
// Content - The content of the box, where text and images appear
//////////////////////////////////
class uistyle_sheet :
    public uistyle_base,
    public xmsg_receiver

{
public:
    x_rtti_class1( uistyle_sheet, uistyle_base );
    XFACTORY_CLASS( uistyle_sheet )
    X_MESSAGE_MAP_DECL

                                    uistyle_sheet       ( void );
            void                    setBackground       ( uistyle_base::guid gBackground   )    { m_gBackground     = gBackground;   }
            void                    setFrame            ( uistyle_base::guid gBorderStyle  )    { m_gFrameStyle     = gBorderStyle;  }
            void                    setMargin           ( uistyle_base::guid gBorderStyle  )    { m_gMarginStyle    = gBorderStyle;  }
            void                    setElement          ( uistyle_base::guid gElementStyle )    { m_gElementStyle   = gElementStyle; }
            void                    setTransform        ( uistyle_base::guid gTransformStyle )  { m_gTransformStyle = gTransformStyle; }
            uistyle_base::guid      getElement          ( void ) const                          { return m_gElementStyle; }
            void                    setName             ( xstring& SheetName   );
            void                    setMargin           ( const xrect& Margin  )                { m_Margin.setup ( uirect::TYPE_ABS_DELTA, Margin  ); }
            void                    setFrame            ( const xrect& Frame   )                { m_Frame.setup  ( uirect::TYPE_ABS_DELTA, Frame   ); }
            void                    setPadding          ( const xrect& Padding )                { m_Padding.setup( uirect::TYPE_ABS_DELTA, Padding ); }
            const xirect&           getMargin           ( void ) const;
            uistyle_base::guid      getTransform        ( void ) const                          { return m_gTransformStyle; }
            xrect                   onRender            ( render& Render, const xbool bUseTransform = TRUE ) const;
    
            void                    ComputeRenderMatrix ( render& Render ) const;
            xbool                   onPropQuery         ( xproperty_query& Query ) override     { return FALSE; }
            void                    onPropEnum          ( xproperty_enum&  Enum  ) const override { }
            const uistyle_sheet&    operator =          ( const uistyle_sheet& Style );
    
protected:
    
    enum style_property_bits : u32
    {
        OVERWRITE_MARGIN            = X_BIT( 0 ),
        OVERWRITE_FRAME             = X_BIT( 1 ),
        OVERWRITE_PADDING           = X_BIT( 2 ),
        OVERWRITE_MARGIN_STYLE      = X_BIT( 3 ),
        OVERWRITE_FRAME_STYLE       = X_BIT( 4 ),
        OVERWRITE_BACKGROUND_STYLE  = X_BIT( 5 ),
        OVERWRITE_ELEMENT_STYLE     = X_BIT( 6 ),
    };
    
    u32                     m_FieldFlags;
    guid                    m_gParent;
    
    // Different aligments in pixels relative to its corresponding parent (ex: Padding -> Border )
    uirect                  m_Margin;                       // From Region to Margin (Delta)
    uirect                  m_Frame;                        // From Region to Frame  (Delta)
    uirect                  m_Padding;                      // From Frame to Padding (Delta)
    
    // Style of the frame
    uistyle_base::guid      m_gMarginStyle;                 // Margin style
    uistyle_base::guid      m_gFrameStyle;                  // Frame style
    uistyle_base::guid      m_gBackground;                  // Background style for the region
    uistyle_base::guid      m_gElementStyle;                // Element to be render inside the Padding
    uistyle_base::guid      m_gTransformStyle;              // Element to be render inside the Padding
};

