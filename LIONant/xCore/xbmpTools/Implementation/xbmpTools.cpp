#include "Filters/xBRZ/xbrz.h"
#include "xbmp_Tools.h"

xbool png_Load( xbitmap& Bitmap, const char* pFileName );
xbool tga_Load( xbitmap& Bitmap, const char* pFileName );
xbool psd_Load( xbitmap& Bitmap, const char* pFileName );
xbool bmp_Load( xbitmap& Bitmap, const char* pFileName );

//-------------------------------------------------------------------------------

xbool xbmp_Load( xbitmap& Bitmap, const char* pFileName )
{
    if( x_stristr( pFileName, ".png" ) )
    {
        return png_Load( Bitmap, pFileName );
    }
    else if( x_stristr( pFileName, ".tga" ) )
    {
        return tga_Load( Bitmap, pFileName );
    }
    else if( x_stristr( pFileName, ".psd" ) )
    {
        return psd_Load( Bitmap, pFileName );
    }
    else if( x_stristr( pFileName, ".bmp" ) )
    {
        return bmp_Load( Bitmap, pFileName );
    }

    return FALSE;
}

//-------------------------------------------------------------------------------

void xbmp_FilterBRZ( xbitmap& Dest, const xbitmap& Src, s32 Scale, xbool bIgnoreAlpha )
{
    ASSERT( Scale > 1 );
    ASSERT( Scale < 6 );
    
    // Allocate memory for destionation
    Dest.CreateBitmap( Src.getWidth()*Scale, Src.getHeight()*Scale );
    
    if( bIgnoreAlpha )
    {
        //
        // Run the filter
        //
        xbrz::scale(
                    Scale,
                    (const uint32_t*)Src.getMip(0),
                    (uint32_t*)Dest.getMip(0),
                    Src.getWidth(),
                    Src.getHeight() );
    }
    else
    {
        //
        // Premultiply the alpha with the source to clean the image
        // and separate the alpha into a different bitmap
        //
        xbitmap TempSrcRGB;
        xbitmap TempSrcAlpha;
        xbitmap TempDestAlpha;
        
        TempSrcRGB.CreateBitmap     ( Src.getWidth(), Src.getHeight() );
        TempSrcAlpha.CreateBitmap   ( Src.getWidth(), Src.getHeight() );
        TempDestAlpha.CreateBitmap  ( Src.getWidth()*Scale, Src.getHeight()*Scale );
        
        {
            const s32       Count       = Src.getWidth()*Src.getHeight();
            const xcolor*   pSrc        = (xcolor*)Src.getMip(0);
            xcolor*         pDestRGB    = (xcolor*)TempSrcRGB.getMip(0);
            xcolor*         pDestAlpha  = (xcolor*)TempSrcAlpha.getMip(0);
            
            for( s32 i=0; i<Count; i++ )
            {
                pDestRGB[i].m_R = (pSrc[i].m_R * (s32)pSrc[i].m_A)/255;
                pDestRGB[i].m_G = (pSrc[i].m_G * (s32)pSrc[i].m_A)/255;
                pDestRGB[i].m_B = (pSrc[i].m_B * (s32)pSrc[i].m_A)/255;
                pDestRGB[i].m_A = pSrc[i].m_A;
                
                pDestAlpha[i].m_R = pSrc[i].m_A;
                pDestAlpha[i].m_G = 0;
                pDestAlpha[i].m_B = 0;
                pDestAlpha[i].m_A = pSrc[i].m_A;
            }
        }
        
        xbrz::ScalerCfg Config;
        
        //
        // Now run the filter with the RGB component
        //
        xbrz::scale(
                    Scale,
                    (const uint32_t*)TempSrcRGB.getMip(0),
                    (uint32_t*)Dest.getMip(0),
                    TempSrcRGB.getWidth(),
                    TempSrcRGB.getHeight(),
                    Config );
    
        //
        // Now the alpha component
        //
        xbrz::scale(
                    Scale,
                    (const uint32_t*)TempSrcAlpha.getMip(0),
                    (uint32_t*)TempDestAlpha.getMip(0),
                    TempSrcRGB.getWidth(),
                    TempSrcRGB.getHeight(),
                    Config );
        
        //
        // Now merge the alpha into the final bitmap
        //
        {
            const s32       Count = Src.getWidth()*Src.getHeight()*Scale*Scale;
            
            xcolor*         pDestRGB    = (xcolor*)Dest.getMip(0);
            xcolor*         pDestAlpha  = (xcolor*)TempDestAlpha.getMip(0);
            
            for( s32 i=0; i<Count; i++ )
            {
                pDestRGB[i].m_A = pDestAlpha[i].m_R;
            }
        }
    }
}

//-------------------------------------------------------------------------------
// Reference: http://lodev.org/cgtutor/filtering.html

static
void GenericFilter(
    xbitmap&            Dest,
    const xbitmap&      Src,
    const f64* const    Filter,
    const s32           filterWidth,
    const s32           filterHeight,
    const f32           Bias,
    const f64           Factor )
{
    const s32       Width       = Src.getWidth();
    const s32       Height      = Src.getHeight();
    const xcolor*   pSrcImage   = (const xcolor*)Src.getMip(0);
    xcolor*         pDestImage  = (xcolor*)x_malloc( sizeof(xcolor), Width*Height, 0 );
    
    // Apply the filter
    for( s32 x = 0; x < Width;  x++ )
    for( s32 y = 0; y < Height; y++ )
    {
        f64 red     = 0.0;
        f64 green   = 0.0;
        f64 blue    = 0.0;
            
        // Multiply every value of the filter with corresponding image pixel
        for( s32 filterX = 0; filterX < filterWidth;  filterX++)
        for( s32 filterY = 0; filterY < filterHeight; filterY++)
        {
            const s32 imageX  = (x - filterWidth  / 2 + filterX + Width ) % Width;
            const s32 imageY  = (y - filterHeight / 2 + filterY + Height) % Height;
            const s32 iPixel  = imageX  + Width       * imageY;
            const s32 iFilter = filterX + filterWidth * filterY;
            
            red   += pSrcImage[iPixel].m_R * Filter[iFilter];
            green += pSrcImage[iPixel].m_G * Filter[iFilter];
            blue  += pSrcImage[iPixel].m_B * Filter[iFilter];
        }
        
        //
        // Copy the result
        //
        const s32 iPixel = x + y * Width;
        
        pDestImage[iPixel].m_R = x_Range( s32(Factor * red   + Bias), 0, 255 );
        pDestImage[iPixel].m_G = x_Range( s32(Factor * green + Bias), 0, 255 );
        pDestImage[iPixel].m_B = x_Range( s32(Factor * blue  + Bias), 0, 255 );
        pDestImage[iPixel].m_A = pSrcImage[iPixel].m_A;
    }
    
    //
    // Set the final bitmap
    //
    Dest.setupFromColor( Width, Height, pDestImage );
}

//-------------------------------------------------------------------------------

void xbmp_FilterSharpen( xbitmap& Dest, const xbitmap& Src )
{
    static const f64 pFilterMatrix[] =
    {
        -1, -1, -1, -1, -1,
        -1,  2,  2,  2, -1,
        -1,  2, 20,  2, -1,
        -1,  2,  2,  2, -1,
        -1, -1, -1, -1, -1,
    };
    
    f64 Factor = 0;
    for( s32 i=0; i<5*5; i++ ) Factor += pFilterMatrix[i];
    
    Factor = 1.0 / Factor;
    static const f32 Bias = 0.0;
    
    GenericFilter( Dest, Src,
                   pFilterMatrix, 5, 5,
                   Bias, Factor );
}

