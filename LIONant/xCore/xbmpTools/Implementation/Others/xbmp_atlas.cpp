//
//  xbmp_atlas.cpp
//  endBaseFontExample
//
//  Created by Tomas Arce on 8/26/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
//
#include "../../xbmp_Tools.h"

//-------------------------------------------------------------------------------------------------
static
s32 Area( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB )
{
    s32 b1 = (*pA)->getArea();
    s32 a1 = (*pB)->getArea();
    if( a1 < b1 ) return -1;
    return a1>b1;
}

//-------------------------------------------------------------------------------------------------
static
s32 Perimeter( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB )
{
    s32 b1 = (*pA)->getPerimeter();
    s32 a1 = (*pB)->getPerimeter();
    if( a1 < b1 ) return -1;
    return a1>b1;
}

//-------------------------------------------------------------------------------------------------
static
s32 MaxSide( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB )
{
    s32 b1 = x_Max( (*pA)->m_W, (*pA)->m_H );
    s32 a1 = x_Max( (*pB)->m_W, (*pB)->m_H );
    if( a1 < b1 ) return -1;
    return a1>b1;
}

//-------------------------------------------------------------------------------------------------
static
s32 MaxWidth( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB )
{
	s32 b1 = (*pA)->m_W;
    s32 a1 = (*pB)->m_W;
    if( a1 < b1 ) return -1;
    return a1>b1;
}

//-------------------------------------------------------------------------------------------------
static
s32 MaxHeight( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB )
{
	s32 b1 = (*pA)->m_H;
    s32 a1 = (*pB)->m_H;
    if( a1 < b1 ) return -1;
    return a1>b1;
}

// just add another comparing function name to cmpf to perform another packing attempt
// more functions == slower but probably more efficient cases covered and hence less area wasted
s32 (*s_CompareFunctions[])( const xbmp_atlast::rect_xywhf** pA, const xbmp_atlast::rect_xywhf** pB ) =
{
	Area,
	Perimeter,
	MaxSide,
	MaxWidth,
	MaxHeight
};

// if you find the algorithm running too slow you may double this factor to increase speed but also decrease efficiency
// 1 == most efficient, slowest
// efficiency may be still satisfying at 64 or even 256 with nice speedup

const s32 s_DiscardStep = 16;

// For every sorting function, algorithm will perform packing attempts beginning with a bin with width
// and height equal to max_side, and decreasing its dimensions if it finds out that rectangles did
// actually fit, increasing otherwise. Although, it's doing that in sort of binary search manner, so
// for every comparing function it will perform at most log2(max_side) packing attempts looking for
// the smallest possible bin size. discard_step means that if the algorithm will break of the searching
// loop if the rectangles fit but "it may be possible to fit them in a bin smaller by discard_step"
//
//  may be pretty slow in debug mode anyway (std::vector and stuff like that in debug mode is always slow)
//
// the algorithm was based on http://www.blackpawn.com/texts/lightmaps/default.html
// the algorithm reuses the node tree so it doesn't reallocate them between searching attempts
//
//
// please let me know about bugs at  <----
// unknownunreleased@gmail.com           |
// |
// |
// ps. I'm 16 so take this --------------- more than seriously, though I made some realtime tests
// with packing several hundreds of rectangles every frame, no crashes, no memory leaks, good results
// Thank you.


struct node
{
	struct pnode
    {
		node*   m_pNode;
		xbool   m_Fill;
        
		pnode() : m_Fill(false), m_pNode( NULL ) {}
        
		void setup( s32 L, s32 T, s32 R, s32 B )
        {
			if( m_pNode )
            {
				m_pNode->m_RC  = xbmp_atlast::rect_ltrb( L, T, R, B );
				m_pNode->m_pID = NULL;
            }
			else
            {
                m_pNode = x_new( node, 1, 0 );
                m_pNode->setup( xbmp_atlast::rect_ltrb( L, T, R, B ) );
			}
            
			m_Fill = TRUE;
		}
	};
    
	pnode                       m_C[2];
	xbmp_atlast::rect_ltrb      m_RC;
	xbmp_atlast::rect_xywhf*    m_pID;
    
	~node()
    {
		if( m_C[0].m_pNode ) x_delete( m_C[0].m_pNode );
		if( m_C[1].m_pNode ) x_delete( m_C[1].m_pNode );
	}
    
    node( void )
    {
        setup( xbmp_atlast::rect_ltrb(0,0,0,0) );
    }
    
	void setup( const xbmp_atlast::rect_ltrb& RC )
    {
        m_pID = NULL;
        m_RC  = RC;
    }
    
	void Reset( const xbmp_atlast::rect_wh& R )
    {
		m_pID = NULL;
		m_RC  = xbmp_atlast::rect_ltrb( 0, 0, R.m_W, R.m_H );
		DelCheck();
	}
    
	void DelCheck( void )
    {
		if( m_C[0].m_pNode ) { m_C[0].m_Fill = FALSE; m_C[0].m_pNode->DelCheck(); }
		if( m_C[1].m_pNode ) { m_C[1].m_Fill = FALSE; m_C[1].m_pNode->DelCheck(); }
	}
    
	node* Insert( xbmp_atlast::rect_xywhf& ImageRect )
    {
		if( m_C[0].m_pNode && m_C[0].m_Fill )
        {
			node* pNewNode = m_C[0].m_pNode->Insert( ImageRect );
			if( pNewNode ) return pNewNode;
            
            pNewNode = m_C[1].m_pNode->Insert( ImageRect );
			return pNewNode;
		}
        
		if( m_pID ) return NULL;
        
		s32 f = ImageRect.Fits( xbmp_atlast::rect_xywh(m_RC) );
        
		switch( f )
        {
            case 0: return 0;
            case 1: ImageRect.m_bFlipped = FALSE; break;
            case 2: ImageRect.m_bFlipped = TRUE;  break;
            case 3: m_pID = &ImageRect; ImageRect.m_bFlipped = FALSE; return this;
            case 4: m_pID = &ImageRect; ImageRect.m_bFlipped = TRUE;  return this;
		}
        
		s32 iW = (ImageRect.m_bFlipped ? ImageRect.m_H : ImageRect.m_W);
        s32 iH = (ImageRect.m_bFlipped ? ImageRect.m_W : ImageRect.m_H);
        
		if( ( m_RC.getWidth() - iW) >  ( m_RC.getHeight() - iH) )
        {
			m_C[0].setup( m_RC.m_Left,      m_RC.m_Top,     m_RC.m_Left + iW,   m_RC.m_Bottom );
			m_C[1].setup( m_RC.m_Left + iW, m_RC.m_Top,     m_RC.m_Right,       m_RC.m_Bottom );
		}
		else
        {
			m_C[0].setup( m_RC.m_Left,      m_RC.m_Top,      m_RC.m_Right,      m_RC.m_Top + iH   );
			m_C[1].setup( m_RC.m_Left,      m_RC.m_Top + iH, m_RC.m_Right,      m_RC.m_Bottom     );
		}
        
		return m_C[0].m_pNode->Insert( ImageRect );
	}
};

//-------------------------------------------------------------------------------------------------
static
xbmp_atlast::rect_wh TheRect2D( xbmp_atlast::rect_xywhf* const*    pV,
                                xbmp_atlast::pack_mode             Mode,
                                s32                                n,
                                s32                                MaxS,
                                xarray<xbmp_atlast::rect_xywhf*>&  Succ,
                                xarray<xbmp_atlast::rect_xywhf*>&  UnSucc )
{
	node                        Root;
	const s32                   nFuncs = 5;
	xbmp_atlast::rect_xywhf**   Order[ nFuncs ];
    const s32                   Multiple = (Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_4)?4:8;
    
	for( s32 f = 0; f < nFuncs; ++f )
    {
		Order[f] = x_new( xbmp_atlast::rect_xywhf*, n, 0 );
		x_memcpy( Order[f], pV, sizeof(xbmp_atlast::rect_xywhf*) * n);
		x_qsort( (void*)Order[f], n, sizeof(xbmp_atlast::rect_xywhf*), (x_compare_fn*)s_CompareFunctions[f] );
	}
    
	xbmp_atlast::rect_wh    MinBin      = xbmp_atlast::rect_wh( MaxS, MaxS );
	s32                     MinFunc     = -1;
    s32                     BestFunc    = 0;
    s32                     BestArea    = 0;
    s32                     TheArea     = 0;
	xbool                   bFail       = false;
    s32                     Step, Fit;
    
	for( s32 f = 0; f < nFuncs; ++f )
    {
		pV      = Order[f];
		Step    = MinBin.m_W / 2;
		Root.Reset( MinBin );
        
		while(1)
        {
			if( Root.m_RC.getWidth() > MinBin.m_W )
            {
				if( MinFunc > -1 )
                    break;
				TheArea = 0;
                
				Root.Reset( MinBin );
                
				for( s32 i = 0; i < n; ++i )
                {
					if( Root.Insert( *pV[i]) )
						TheArea += pV[i]->getArea();
                }
                
				bFail = TRUE;
				break;
			}
            
			Fit = -1;
            
			for( s32 i = 0; i < n; ++i )
            {
				if( !Root.Insert( *pV[i]) )
                {
					Fit = 1;
					break;
				}
            }
            
            if( Fit == -1 && Step <= s_DiscardStep )
                break;
            
            s32 OldW = Root.m_RC.getWidth();
            s32 OldH = Root.m_RC.getHeight();
            s32 NewW = OldW  + Fit*Step;
            s32 NewH = OldH  + Fit*Step;
            
            if( Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_4 ||
                Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_8 )
            {
                NewW = x_Align( NewW, Multiple );
                NewH = x_Align( NewH, Multiple );
                
                ASSERT( x_Align( OldW, Multiple) == OldW );
                ASSERT( x_Align( OldH, Multiple) == OldH );
            }
            else if( Mode == xbmp_atlast::PACKMODE_POWER_OF_TWO ||
                     Mode == xbmp_atlast::PACKMODE_POWER_OF_TWO_SQUARE )
            {
                NewW = x_Pow2RoundUp( NewW );
                NewH = x_Pow2RoundUp( NewH );
                
                ASSERT( x_Pow2RoundUp( OldW ) == OldW );
                ASSERT( x_Pow2RoundUp( OldH ) == OldH );
            }

            
            //
            // Try to choose the search in the right dimension
            //
            if( Mode != xbmp_atlast::PACKMODE_ANYSIZE )
            {
                if( Fit == 1 )
                {
                    if( OldW < OldH ) NewH = OldH;
                    else              NewW = OldW;
                }
                else
                {
                    if( OldW < OldH ) NewW = OldW;
                    else              NewH = OldH;
                }
            }
            
            //
            // For square sizes we need to pick the best dimension for all
            //
            if( Mode == xbmp_atlast::PACKMODE_POWER_OF_TWO_SQUARE )
            {
                if( Fit == 1 )
                {
                    NewH = NewW = x_Max( NewW, NewH );
                }
                else
                {
                    NewH = NewW = x_Min( NewW, NewH );
                }
            }

            //
            // Ok now we can change the size
            //
            Root.Reset( xbmp_atlast::rect_wh( NewW, NewH ) );

            //
            // Keep searching
            //
            Step /= 2;
            if( !Step )
                Step = 1;
		}
        
		if( !bFail && ( MinBin.getArea() >= Root.m_RC.getArea()) )
        {
			MinBin  = xbmp_atlast::rect_wh( Root.m_RC );
			MinFunc = f;
		}
		else if( bFail && ( TheArea > BestArea) )
        {
			BestArea = TheArea;
			BestFunc = f;
		}
        
		bFail = false;
	}
    
	pV = Order[ MinFunc == -1 ? BestFunc : MinFunc ];
    
	s32     ClipX = 0, ClipY = 0;
	node*   pRet;
    
	Root.Reset( MinBin );
    
	for( s32 i = 0; i < n; ++i )
    {
        pRet = Root.Insert( *pV[i]);
		if( pRet )
        {
			pV[i]->m_X = pRet->m_RC.m_Left;
			pV[i]->m_Y = pRet->m_RC.m_Top;
            
			if( pV[i]->m_bFlipped )
            {
				pV[i]->m_bFlipped = FALSE;
				pV[i]->Flip();
			}
            
			ClipX = x_Max( ClipX, pRet->m_RC.m_Right );
			ClipY = x_Max( ClipY, pRet->m_RC.m_Bottom );
            
			Succ.append() = pV[i];
		}
		else
        {
			UnSucc.append() = pV[i];
            
			pV[i]->m_bFlipped = FALSE;
		}
	}
    
	for( s32 f = 0; f < nFuncs; ++f )
    {
		x_delete( Order[f] );
    }
    
    // Make sure that we do return a power of two texture
    if( Mode == xbmp_atlast::PACKMODE_POWER_OF_TWO ||
        Mode == xbmp_atlast::PACKMODE_POWER_OF_TWO_SQUARE )
    {
        return xbmp_atlast::rect_wh( x_Pow2RoundUp(ClipX), x_Pow2RoundUp(ClipY) );
    }
    else if( Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_4 ||
             Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_8 )
    {
        const s32 Multiple = (Mode == xbmp_atlast::PACKMODE_MULTIPLE_OF_4)?4:8;
        return xbmp_atlast::rect_wh( x_Align(ClipX, Multiple), x_Align(ClipY,Multiple) );
    }
    
	return xbmp_atlast::rect_wh( (ClipX), (ClipY) );
}

//-------------------------------------------------------------------------------------------------

xbool xbmp_atlast::Pack( rect_xywhf* const* pV, s32 nRects, s32 MaxSize, xarray<bin>& Bins, pack_mode Mode )
{
	rect_wh TheRect( MaxSize, MaxSize );
    
    // make sure that all the rectangles fit in the given MaxSize
	for( s32 i = 0; i < nRects; ++i )
    {
		if( !pV[i]->Fits(TheRect) )
            return FALSE;
    }
    
    // Create a double buffer array of pointers
	xarray<rect_xywhf*>     Vec[2];
    xarray<rect_xywhf*>*    pVec[2] = { &Vec[0], &Vec[1] };
	
    // Set one pointer per rectangle past by the user
    Vec[0].appendList( nRects );
    
    // Initialize the firt array to zero
	x_memcpy( &Vec[0][0], pV, sizeof(rect_xywhf*) * nRects );
    
	while(1)
    {
		bin& NewBin = Bins.append();
        
		NewBin.m_Size = TheRect2D( &((*pVec[0])[0]), Mode, pVec[0]->getCount(), MaxSize, NewBin.m_Rects, *pVec[1] );
        
		pVec[0]->DeleteAllNodes();
        
		if( pVec[1]->getCount() == 0 ) break;
        
		x_Swap( pVec[0], pVec[1] );
	}
    
	return TRUE;
}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_wh::rect_wh(const rect_ltrb& rr) : m_W(rr.getWidth()), m_H(rr.getHeight()) {}
xbmp_atlast::rect_wh::rect_wh(const rect_xywh& rr) : m_W(rr.getWidth()), m_H(rr.getHeight()) {}
xbmp_atlast::rect_wh::rect_wh(s32 w, s32 h) : m_W(w), m_H(h) {}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_wh::Fits( const rect_wh& r ) const
{
	if( m_W == r.m_W && m_H == r.m_H ) return 3;
	if( m_H == r.m_W && m_W == r.m_H ) return 4;
	if( m_W <= r.m_W && m_H <= r.m_H ) return 1;
	if( m_H <= r.m_W && m_W <= r.m_H ) return 2;
	return 0;
}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_ltrb::rect_ltrb( void ) : m_Left(0), m_Top(0), m_Right(0), m_Bottom(0) {}
xbmp_atlast::rect_ltrb::rect_ltrb( s32 l, s32 t, s32 r, s32 b) : m_Left(l), m_Top(t), m_Right(r), m_Bottom(b) {}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_ltrb::getWidth( void ) const
{
	return m_Right - m_Left;
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_ltrb::getHeight( void ) const
{
	return m_Bottom - m_Top;
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_ltrb::getArea( void ) const
{
	return getWidth() * getHeight();
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_ltrb::getPerimeter( void ) const
{
	return 2*getWidth() + 2*getHeight();
}

//-------------------------------------------------------------------------------------------------

void xbmp_atlast::rect_ltrb::setWidth( s32 Width )
{
	m_Right = m_Left + Width;
}

//-------------------------------------------------------------------------------------------------

void xbmp_atlast::rect_ltrb::setHeight( s32 Height )
{
	m_Bottom = m_Top + Height;
}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_xywh::rect_xywh( void ) : m_X(0), m_Y(0) {}
xbmp_atlast::rect_xywh::rect_xywh( const rect_ltrb& rc ) :
    m_X( rc.m_Left ), m_Y( rc.m_Top )
{
    m_H = rc.m_Bottom - rc.m_Top;
    m_W = rc.m_Right  - rc.m_Left;
}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_xywh::rect_xywh( s32 x, s32 y, s32 w, s32 h ) : m_X(x), m_Y(y), rect_wh( w, h ) {}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_xywh::operator xbmp_atlast::rect_ltrb( void )
{
	rect_ltrb rr( m_X, m_Y, 0, 0 );
	rr.setWidth ( m_W );
    rr.setHeight( m_H );
	return rr;
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_xywh::getRight( void ) const
{
	return m_X + m_W;
};

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_xywh::getBottom( void ) const
{
	return m_Y + m_H;
}

//-------------------------------------------------------------------------------------------------

void xbmp_atlast::rect_xywh::setWidth( s32 Right )
{
	m_W = Right - m_X;
}

//-------------------------------------------------------------------------------------------------

void xbmp_atlast::rect_xywh::setHeight( s32 Bottom )
{
	m_H = Bottom - m_Y;
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_wh::getArea( void ) const
{
	return m_W * m_H;
}

//-------------------------------------------------------------------------------------------------

s32 xbmp_atlast::rect_wh::getPerimeter( void ) const
{
	return 2 * m_W + 2 * m_H;
}

//-------------------------------------------------------------------------------------------------

xbmp_atlast::rect_xywhf::rect_xywhf( const rect_ltrb& rr ) : rect_xywh(rr), m_bFlipped( FALSE ) {}
xbmp_atlast::rect_xywhf::rect_xywhf( s32 x, s32 y, s32 width, s32 height ) : rect_xywh( x, y, width, height ), m_bFlipped( FALSE ) {}
xbmp_atlast::rect_xywhf::rect_xywhf() : m_bFlipped( FALSE ) {}

//-------------------------------------------------------------------------------------------------

void xbmp_atlast::rect_xywhf::Flip( void )
{
	m_bFlipped = !m_bFlipped;
	x_Swap( m_W, m_H );
}

