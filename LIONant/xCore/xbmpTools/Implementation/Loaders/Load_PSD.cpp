#include "x_base.h"

#ifdef EXIT_FAILURE 
#undef EXIT_FAILURE
#endif

//==============================================================================
//  TYPES
//==============================================================================

// 
//  This structure does not match the layout of the header data in a TGA file.
//  It simply has fields for all of the data members needed for this simple
//  loader.
//

struct psd_info
{                                                           
    s32     Signature;      // 8BPS
    s16     Version;        // Version
    s16     nChannels;      // Number of Channels (3=RGB) (4=RGBA)
    s32     Height;         // Number of Image Rows
    s32     Width;          // Number of Image Columns
    s16     Depth;          // Number of Bits per Channel
    s16     Mode;           // Image Mode (0=Bitmap)(1=Grayscale)(2=Indexed)(3=RGB)(4=CYMK)(7=Multichannel)
};

//==============================================================================
//  FUNCTIONS
//==============================================================================

static
xbool psd_ReadData( xbyte*      pData, 
                    xfile&      File, 
                    psd_info&   Info,
                    s32         NPixels, 
                    s32         BytesPerPixel )
{
    xbyte    Buffer[2];
    xbyte*   pPlane = NULL;
    xbyte*   pRowTable = NULL;
    s32      iPlane;
    s16      CompressionType;
    s32      iPixel;
    s32      iRow;
    s32      CompressedBytes;
    s32      iByte;
    s32      Count;
    xbyte    Value;

    // Read Compression Type
    if( File.ReadRaw( Buffer, 1, 2 ) == FALSE )
        goto EXIT_FAILURE;
    CompressionType = ((s32)Buffer[0] <<  8) +
                      ((s32)Buffer[1] <<  0);

    // Allocate Plane Buffer
    pPlane = (xbyte*)x_malloc( 1, NPixels, 0 );
    ASSERT( pPlane );
    if( !pPlane ) goto EXIT_FAILURE;

    // Uncompressed?
    if( CompressionType == 0 )
    {
        // Loop through the planes
        for( iPlane=0 ; iPlane<Info.nChannels ; iPlane++ )
        {
            s32 iWritePlane = iPlane;
            if( iWritePlane > BytesPerPixel-1 ) iWritePlane = BytesPerPixel-1;

            // Read Plane
            if( File.ReadRaw( pPlane, 1, NPixels ) )
                goto EXIT_FAILURE;

            // Move Plane to Image Data
            for( iPixel=0 ; iPixel<NPixels ; iPixel++ )
            {
                pData[iWritePlane + (iPixel*BytesPerPixel)] = pPlane[iPixel];
            }
        }
    }
    // RLE?
    else if( CompressionType == 1 )
    {
        // Allocate Row Table
        pRowTable = (xbyte*)x_malloc( 1, Info.nChannels*Info.Height*2, 0 );
        ASSERT( pRowTable );
        if( pRowTable == NULL )
            goto EXIT_FAILURE;

        // Load RowTable
        if( File.ReadRaw( pRowTable, 2, Info.nChannels*Info.Height ) == FALSE )
            goto EXIT_FAILURE;

        // Loop through the planes
        for( iPlane=0 ; iPlane<Info.nChannels ; iPlane++ )
        {
            s32 iWritePlane = iPlane;
            if( iWritePlane > BytesPerPixel-1 ) iWritePlane = BytesPerPixel-1;

            // Loop through the rows
            for( iRow=0 ; iRow<Info.Height ; iRow++ )
            {
                // Load a row
                CompressedBytes = (pRowTable[(iPlane*Info.Height+iRow)*2  ] << 8) +
                                  (pRowTable[(iPlane*Info.Height+iRow)*2+1] << 0);
              
                if( File.ReadRaw( pPlane, 1, CompressedBytes ) == FALSE )
                    goto EXIT_FAILURE;

                // Decompress Row
                iPixel = 0;
                iByte = 0;
                while( (iPixel < Info.Width) && (iByte < CompressedBytes) )
                {
                    s8 code = (s8)pPlane[iByte++];

                    // Is it a repeat?
                    if( code < 0 )
                    {
                        Count = -(s32)code + 1;
                        Value = pPlane[iByte++];
                        while( Count-- > 0 )
                        {
                            pData[iWritePlane + (iPixel*BytesPerPixel) + (iRow*Info.Width*BytesPerPixel)] = Value;
                            iPixel++;
                        }
                    }
                    // Must be a literal then
                    else
                    {
                        Count = (s32)code + 1;
                        while( Count-- > 0 )
                        {
                            Value = pPlane[iByte++];
                            pData[iWritePlane + (iPixel*BytesPerPixel) + (iRow*Info.Width*BytesPerPixel)] = Value;
                            iPixel++;
                        }
                    }
                }

                // Confirm that we decoded the right number of bytes
                ASSERT( iByte  == CompressedBytes );
                ASSERT( iPixel == Info.Width );
            }
        }
    }
    else
        goto EXIT_FAILURE;


    // Free Buffers
    if( pPlane )    x_free( pPlane );
    if( pRowTable ) x_free( pRowTable );

    // Success!
    return( TRUE );

    // Failure!
    EXIT_FAILURE:
    if( pPlane    ) x_free( pPlane );
    if( pRowTable ) x_free( pRowTable );
    return( FALSE );
}

//==============================================================================

xbool psd_Load( xbitmap& Bitmap, const char* pFileName )
{
    psd_info    Info;
    xbyte       Buffer[26];
    xfile       File;
    xbyte*      pData = NULL;
    xbyte*      pClut = NULL;
    xbyte*      pClutTmp = NULL;
    s32         ClutSize;
    s32         NPixels;
    s32         BytesPerPixel = 1;
    s32         DataSize;
    s32         ImageResourceSize;
    s32         LayerAndMaskSize;

    xbitmap::format Format = xbitmap::FORMAT_NULL;

    ASSERT( pFileName );

    // Attempt to open the specified file for read.
    if( File.Open( pFileName, "rb" ) == FALSE )
        goto EXIT_FAILURE;

    // Read the header data into the buffer.
    if( File.ReadRaw( &Buffer, 1, 26 ) == FALSE )
        goto EXIT_FAILURE;

    // Pick out the values we need from the header data in the buffer.
    // The file is in PC Little Endian.  Make sure to read it safely.
    Info.Signature      = ((s32)Buffer[ 0] << 24) +
                          ((s32)Buffer[ 1] << 16) +
                          ((s32)Buffer[ 2] <<  8) +
                          ((s32)Buffer[ 3] <<  0);
    Info.Version        = ((s32)Buffer[ 4] <<  8) +
                          ((s32)Buffer[ 5] <<  0);
    Info.nChannels      = ((s32)Buffer[12] <<  8) +
                          ((s32)Buffer[13] <<  0);
    Info.Height         = ((s32)Buffer[14] << 24) +
                          ((s32)Buffer[15] << 16) +
                          ((s32)Buffer[16] <<  8) +
                          ((s32)Buffer[17] <<  0);
    Info.Width          = ((s32)Buffer[18] << 24) +
                          ((s32)Buffer[19] << 16) +
                          ((s32)Buffer[20] <<  8) +
                          ((s32)Buffer[21] <<  0);
    Info.Depth          = ((s32)Buffer[22] <<  8) +
                          ((s32)Buffer[23] <<  0);
    Info.Mode           = ((s32)Buffer[24] <<  8) +
                          ((s32)Buffer[25] <<  0);

    // Fail on bad signature or version
    if( (Info.Signature != 0x38425053) || (Info.Version != 1) )
        goto EXIT_FAILURE;

    // Fail on unsupported formats.
    if( (Info.Mode != 2) && (Info.Mode != 3) )
        goto EXIT_FAILURE;

    // Only allow 1 channel on indexed images
    if( (Info.Mode == 2) && (Info.nChannels != 1) )
        goto EXIT_FAILURE;

    // Read CLUT size
    if( File.ReadRaw( &Buffer, 1, 4 ) == FALSE )
        goto EXIT_FAILURE;
    ClutSize = ((s32)Buffer[ 0] << 24) +
               ((s32)Buffer[ 1] << 16) +
               ((s32)Buffer[ 2] <<  8) +
               ((s32)Buffer[ 3] <<  0);

    // Read Clut for an indexed color mode, skip otherwise
    if( Info.Mode == 2 )
    {
        // Allocate and Load Clut
        pClut = (xbyte*)x_malloc( 1, ClutSize, 0 );
        ASSERT( pClut );
        if( pClut == NULL )
            goto EXIT_FAILURE;
        pClutTmp = (xbyte*)x_malloc( 1, ClutSize, 0 );
        ASSERT( pClutTmp );
        if( pClutTmp == NULL )
            goto EXIT_FAILURE;

        if( File.ReadRaw(pClutTmp, 1, ClutSize) == FALSE )
            goto EXIT_FAILURE;

        for( s32 i=0 ; i<256 ; i++ )
        {
            pClut[3*i+0] = pClutTmp[  0+i];
            pClut[3*i+1] = pClutTmp[256+i];
            pClut[3*i+2] = pClutTmp[512+i];
        }

        x_free( pClutTmp );
        pClutTmp = NULL;
    }
    else
    {
        // Skip Clut
        File.SeekCurrent( ClutSize );
    }

    // Skip Image Resource Section
    if( File.ReadRaw( &Buffer, 1, 4 ) == FALSE )
        goto EXIT_FAILURE;
    ImageResourceSize = ((s32)Buffer[ 0] << 24) +
                        ((s32)Buffer[ 1] << 16) +
                        ((s32)Buffer[ 2] <<  8) +
                        ((s32)Buffer[ 3] <<  0);
    File.SeekCurrent( ImageResourceSize );

    // Skip Layer and Mask Section
    if( File.ReadRaw( &Buffer, 1, 4 ) == FALSE )
        goto EXIT_FAILURE;
    LayerAndMaskSize = ((s32)Buffer[ 0] << 24) +
                       ((s32)Buffer[ 1] << 16) +
                       ((s32)Buffer[ 2] <<  8) +
                       ((s32)Buffer[ 3] <<  0);
    File.SeekCurrent( LayerAndMaskSize );

    // Determine number of bytes per pixel
    switch( Info.Mode )
    {
    case 2:
        // We don't accept palettice bitmaps
        //Format = xbitmap::FMT_P8_RGB_888;
        BytesPerPixel = 1;
        goto EXIT_FAILURE;
        break;
    case 3:
        if( Info.nChannels == 3 )
        {
            //Format = xbitmap::FMT_24_RGB_888;
            BytesPerPixel = 3;
        }
        else
        {
            //Format = xbitmap::FMT_32_ABGR_8888;
            BytesPerPixel = 4;
        }
        break;
    }

    // Allocate pixel data.
    NPixels  = Info.Width * Info.Height;
    DataSize = NPixels * BytesPerPixel;
    pData    = (xbyte*)x_malloc( 1, DataSize, 0 );
    ASSERT( pData );

    // Load the pixel data.
    if( !psd_ReadData( pData,
                       File,
                       Info,
                       NPixels,
                       BytesPerPixel ) )
    {
        goto EXIT_FAILURE;
    }

    // Done reading from file.
    File.Close();

    // Set up the xbitmap.
    {
        xbyte* pFinalData = NULL;

        if( BytesPerPixel == 3)
        {
            DataSize += NPixels;
            pFinalData = (xbyte*)x_malloc( 1, 4 + DataSize, 0 );
            pFinalData[0]=0;
            pFinalData[1]=0;
            pFinalData[2]=0;
            pFinalData[3]=0;

            for( s32 i=0; i<NPixels; i++ )
            {
                pFinalData[4 + i*4+0] = pData[i*3+2];
                pFinalData[4 + i*4+1] = pData[i*3+1];
                pFinalData[4 + i*4+2] = pData[i*3+0];
                pFinalData[4 + i*4+3] = 0xff;
            }

            Format = xbitmap::FORMAT_U8R8G8B8;
        }
        else
        {
            pFinalData = (xbyte*)x_malloc( 1, 4 + DataSize, 0 );
            pFinalData[0]=0;
            pFinalData[1]=0;
            pFinalData[2]=0;
            pFinalData[3]=0;

            for( s32 i=0; i<NPixels; i++ )
            {
                pFinalData[4 + i*4+0] = pData[i*4+2];
                pFinalData[4 + i*4+1] = pData[i*4+1];
                pFinalData[4 + i*4+2] = pData[i*4+0];
                pFinalData[4 + i*4+3] = pData[i*4+3];
            }

            Format = xbitmap::FORMAT_A8R8G8B8;
        }

        //
        // Setup the bitmap
        //
        ASSERT( Format != xbitmap::FORMAT_NULL );

        Bitmap.setup( Info.Width,
                      Info.Height,             
                      Format, 
                      DataSize+4,
                      DataSize,
                      pFinalData,
                      TRUE,
                      1,
                      1 );
    }

    if( pData )     x_free  ( pData );
    if( pClut )     x_free  ( pClut );

    // Success!
    return( TRUE );

    // Failure!
    EXIT_FAILURE:
    if( pData )     x_free  ( pData );
    if( pClut )     x_free  ( pClut );
    if( pClutTmp )  x_free  ( pClutTmp );
    return( FALSE );
}

//==============================================================================
/*
xbool psd_Info( const char* pFileName, info& BitmapInfo )
{
    psd_info    Info;
    byte        Buffer[26];
	s32			BytesRead;
    X_FILE*     pFile = NULL;

    xbitmap::format Format = xbitmap::FMT_NULL;

    ASSERT( pFileName );

    // Attempt to open the specified file for read.
    pFile = x_fopen( pFileName, "rb" );
    if( !pFile )
        goto EXIT_FAILURE;

    // Read the header data into the buffer.
    BytesRead = x_fread( &Buffer, 1, 26, pFile );
    if( BytesRead != 26 )
        goto EXIT_FAILURE;

    // Pick out the values we need from the header data in the buffer.
    // The file is in PC Little Endian.  Make sure to read it safely.
    Info.Signature      = ((s32)Buffer[ 0] << 24) +
                          ((s32)Buffer[ 1] << 16) +
                          ((s32)Buffer[ 2] <<  8) +
                          ((s32)Buffer[ 3] <<  0);
    Info.Version        = ((s32)Buffer[ 4] <<  8) +
                          ((s32)Buffer[ 5] <<  0);
    Info.nChannels      = ((s32)Buffer[12] <<  8) +
                          ((s32)Buffer[13] <<  0);
    Info.Height         = ((s32)Buffer[14] << 24) +
                          ((s32)Buffer[15] << 16) +
                          ((s32)Buffer[16] <<  8) +
                          ((s32)Buffer[17] <<  0);
    Info.Width          = ((s32)Buffer[18] << 24) +
                          ((s32)Buffer[19] << 16) +
                          ((s32)Buffer[20] <<  8) +
                          ((s32)Buffer[21] <<  0);
    Info.Depth          = ((s32)Buffer[22] <<  8) +
                          ((s32)Buffer[23] <<  0);
    Info.Mode           = ((s32)Buffer[24] <<  8) +
                          ((s32)Buffer[25] <<  0);

    // Fail on bad signature or version
    if( (Info.Signature != 0x38425053) || (Info.Version != 1) )
        goto EXIT_FAILURE;

    // Fail on unsupported formats.
    if( (Info.Mode != 2) && (Info.Mode != 3) )
        goto EXIT_FAILURE;

    // Only allow 1 channel on indexed images
    if( (Info.Mode == 2) && (Info.nChannels != 1) )
        goto EXIT_FAILURE;

    // Determine number of bytes per pixel
    switch( Info.Mode )
    {
    case 2:
        Format = xbitmap::FMT_P8_RGB_888;
        break;
    case 3:
        if( Info.nChannels == 3 )
        {
            Format = xbitmap::FMT_24_RGB_888;
        }
        else
        {
            Format = xbitmap::FMT_32_ABGR_8888;
        }
        break;
    }

    // Done reading from file.
    x_fclose( pFile );

	// Fill in the xbitmap::info struct
	BitmapInfo.W      = Info.Width;
	BitmapInfo.H      = Info.Height;
	BitmapInfo.nMips  = 0;
	BitmapInfo.Format = Format;

    // Success!
    return( TRUE );

    // Failure!
    EXIT_FAILURE:
    if( pFile )     x_fclose( pFile );
    return( FALSE );
}

//==============================================================================
*/