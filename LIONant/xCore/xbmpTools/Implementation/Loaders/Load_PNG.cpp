#include "png.h"
#include "x_base.h"
#include <stdio.h>

//------------------------------------------------------------------------------------------------
static 
xbool loadPngImage( const char* pName, s32& outWidth, s32& outHeight, xbool& outHasAlpha, s32& DataSize, xbyte*& pOutData ) 
{
    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    int color_type;
    FILE *fp;

    if ( NULL == (fp = fopen( pName, "rb"))  )
        return false;

    /* Create and initialize the png_struct
     * with the desired error handler
     * functions.  If you want to use the
     * default stderr and longjump method,
     * you can supply NULL for the last
     * three parameters.  We also supply the
     * the compiler header file version, so
     * that we know if the application
     * was compiled with a compatible version
     * of the library.  REQUIRED
     */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
            NULL, NULL, NULL);

    if (png_ptr == NULL) {
        fclose(fp);
        return false;
    }

    /* Allocate/initialize the memory
     * for image information.  REQUIRED. */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return false;
    }

    /* Set error handling if you are
     * using the setjmp/longjmp method
     * (this is the normal method of
     * doing things with libpng).
     * REQUIRED unless you  set up
     * your own error handlers in
     * the png_create_read_struct()
     * earlier.
     */
    if (setjmp(png_jmpbuf(png_ptr))) {
        /* Free all of the memory associated
         * with the png_ptr and info_ptr */
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL );
        fclose(fp);
        /* If we get here, we had a
         * problem reading the file */
        return false;
    }

    /* Set up the output control if
     * you are using standard C streams */
    png_init_io(png_ptr, fp);

    /* If we have already
     * read some of the signature */
    png_set_sig_bytes(png_ptr, sig_read);

    /*
     * If you have enough memory to read
     * in the entire image at once, and
     * you need to specify only
     * transforms that can be controlled
     * with one of the PNG_TRANSFORM_*
     * bits (this presently excludes
     * dithering, filling, setting
     * background, and doing gamma
     * adjustment), then you can read the
     * entire image (including pixels)
     * into the info structure with this
     * call
     *
     * PNG_TRANSFORM_STRIP_16 |
     * PNG_TRANSFORM_PACKING  forces 8 bit
     * PNG_TRANSFORM_EXPAND forces to
     *  expand a palette into RGB
     */
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL );

    s32         bit_depth;
    png_uint_32 w;
    png_uint_32 h;
    png_get_IHDR(png_ptr, info_ptr, &w, &h, &bit_depth,
      &color_type, NULL, NULL, NULL);

    outWidth = w;
    outHeight = h;


    //outWidth = info_ptr->width;
    //outHeight = info_ptr->height;
    switch (color_type) {
        case PNG_COLOR_TYPE_RGBA:
            outHasAlpha = true;
            break;
        case PNG_COLOR_TYPE_RGB:
            outHasAlpha = false;
            break;
        default:
            x_printf( "ERROR: Color Type %d not dupported", color_type );
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            fclose(fp);
            return false;
    }

    unsigned int row_bytes = (unsigned int)png_get_rowbytes(png_ptr, info_ptr);
    ASSERT( (s32)row_bytes <= 4 * outWidth );

    DataSize = 4 + 4 * outWidth * outHeight;
    pOutData = (xbyte *)x_malloc( sizeof(xbyte), DataSize, 0 );

    // this is the mip offset
    *((u32*)pOutData)=0;

    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);

    // note that png is ordered top to
    // bottom, but OpenGL expect it bottom to top
    // so the order or swapped
    if( outHasAlpha )
    {
        for (int i = 0; i < outHeight; i++) 
        {
            u32* pDest = (u32*)(&pOutData[4 + row_bytes * i]);
            u32* pSrc  = (u32*)row_pointers[i];

            // Make it into ARGB
            for( int x=0; x<outWidth; x++ )
            {
                *pDest = (((*pSrc>>16)&0xff)<<16) |
                         (((*pSrc>> 8)&0xff)<< 8) |
                         (((*pSrc>> 0)&0xff)<< 0) |
                         (((*pSrc>>24)&0xff)<<24);

                pDest++;
                pSrc++;
            }
        }
    }
    else
    {
        for (int i = 0; i < outHeight; i++) 
        {
            u32* pDest = (u32*)(&pOutData[ 4 + 4*outWidth * i]);
            u32* pSrc  = (u32*)(((u8*)row_pointers[i]));

            // Make it into ARGB
            for( int x=0; x<outWidth; x++ )
            {
                *pDest = (((*pSrc>>16)&0xff)<<16) |
                         (((*pSrc>> 8)&0xff)<< 8) |
                         (((*pSrc>> 0)&0xff)<< 0) |
                         0xff000000;
                
                pDest++;
                pSrc = (u32*)(((u8*)pSrc)+3);
            }
        }
    }


    /* Clean up after the read,
     * and free any memory allocated */
    png_destroy_read_struct( &png_ptr, &info_ptr, NULL );

    /* Close the file */
    fclose(fp);

    /* That's it */
    return true;
}


//------------------------------------------------------------------------------------------------

xbool png_Load( xbitmap& Bitmap, const char* pFileName )
{
    s32         Width, Height;
    xbool       bHasAlpha;
    xbyte*      pData;
    s32         DataSize;

    //
    // Load the picture
    //
    if( FALSE == loadPngImage( pFileName, Width, Height, bHasAlpha, DataSize, pData ) )
        return FALSE;

    //
    // Create the bitmap
    //            ,
    if( bHasAlpha )
    {
        Bitmap.setup( Width                   ,
                      Height                  ,             
                      xbitmap::FORMAT_XCOLOR  ,
                      DataSize                ,
                      DataSize-4              ,
                      pData                   ,
                      TRUE                    ,
                      1                       ,
                      1                       );
        
        Bitmap.setHasAlphaInfo(true);
    }
    else
    {
        Bitmap.setup( Width                   ,
                      Height                  ,             
                      xbitmap::FORMAT_XCOLOR  ,
                      DataSize                ,
                      DataSize-4              ,
                      pData                   ,
                      TRUE                    ,
                      1                       ,
                      1                       );
        
        Bitmap.setHasAlphaInfo(false);
    }
    
    return TRUE;
}
