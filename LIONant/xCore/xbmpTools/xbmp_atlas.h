//
//  xbmp_atlas.h
//  endBaseFontExample
//
//  Created by Tomas Arce on 8/26/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
// of your interest:
//
// rect_xywhf - structure representing your rectangle object
// bin - structure representing your bin object
// bool rect2D(rect_xywhf* const * v, int n, int max_side, std::vector<bin>& bins) - actual packing function
//
// v        - pointer to array of pointers to your rectangle (" ** " design is for the
//            sake of sorting speed and const means that the pointers will point to the same rectangles)
// n        - pointers' count
// max_side - maximum bins' side - algorithm works with square bins (in the end it may trim them
//            to rectangular form). for the algorithm to finish faster, pass a reasonable value
//            (unreasonable would be passing 1 000 000 000 for packing 4 50x50 rectangles).
// bins     - vector to which the function will push_back() created bins, each of them containing vector
//            to pointers of rectangles from "v" belonging to that particular bin.
//            Every bin also keeps information about its width and height of course, none of the
//            dimensions is bigger than max_side.
//
// returns true on success, false if one of the rectangles' dimension was bigger than max_side
//
// You want to pass your vector of rectangles representing your textures/glyph objects with
// GL_MAX_TEXTURE_SIZE as max_side, then for each bin iterate through its rectangles, typecast each
// one to your own structure and then memcpy its pixel contents (rotated by 90 degrees if "flipped"
// rect_xywhf's member is true) to the array representing your texture atlas to the place specified
// by the rectangle, then in the end upload it with glTexImage2D.
//
// Algorithm doesn't create any new rectangles.
// You just pass an array of pointers and rectangles' x/y/w/h are modified in place, with just vector
// of pointers for every new bin to let you know which ones belong to the particular bin.
// Modifying w/h means that the dimensions can be actually swapped for the sake of fitting, the flag
// "flipped" will be set to true if such a thing occurs.
//
// For description how to tune the algorithm and how it actually works see the .cpp file.
//
class xbmp_atlast
{
public:

    struct rect_ltrb;
    struct rect_xywh;

    struct rect_wh
    {
                    // 0 - no, 1 - yes, 2 - flipped, 3 - perfectly, 4 perfectly flipped
                    rect_wh         ( const rect_ltrb& );
                    rect_wh         ( const rect_xywh& );
                    rect_wh         ( s32 W = 0, s32 H = 0 );
        s32         Fits            ( const rect_wh& bigger ) const;
        s32         getArea         ( void ) const;
        s32         getPerimeter    ( void ) const;
        
        s32         m_W;
        s32         m_H;
        s32         m_Area;
        s32         m_Perimeter;
    };

    // rectangle implementing left/top/right/bottom behaviour

    struct rect_ltrb
    {
                    rect_ltrb           ( void );
                    rect_ltrb           ( s32 left, s32 top, s32 right, s32 bottom );
        
        s32         m_Left;
        s32         m_Top;
        s32         m_Right;
        s32         m_Bottom;
        
        s32         getWidth            ( void ) const;
        s32         getHeight           ( void ) const;
        s32         getArea             ( void ) const;
        s32         getPerimeter        ( void ) const;
        void        setWidth            ( s32 Width );
        void        setHeight           ( s32 Height );
    };

    struct rect_xywh : public rect_wh
    {
                    rect_xywh           ( void );
                    rect_xywh           ( const rect_ltrb& );
                    rect_xywh           ( s32 x, s32 y, s32 width, s32 height );
        s32         getWidth            ( void ) const { return m_W; }
        s32         getHeight           ( void ) const { return m_H; }
        s32         getRight            ( void ) const;
        s32         getBottom           ( void ) const;
        void        setWidth            ( s32 Right );
        void        setHeight           ( s32 Bottom );    
                    operator rect_ltrb  ( void );
        
        s32         m_X;
        s32         m_Y;
    };

    struct rect_xywhf : public rect_xywh
    {
                    rect_xywhf          ( const rect_ltrb& );
                    rect_xywhf          ( s32 X, s32 Y, s32 Width, s32 Height );
                    rect_xywhf          ( void );
        void        Flip                ( void );
        
        xbool       m_bFlipped;
        rect_xywhf* m_pNextFree;
    };

    struct bin
    {
        rect_wh                 m_Size;
        xarray<rect_xywhf*>     m_Rects;
    };
    
    enum pack_mode
    {
        PACKMODE_ANYSIZE,
        PACKMODE_MULTIPLE_OF_4,
        PACKMODE_MULTIPLE_OF_8,
        PACKMODE_POWER_OF_TWO,
        PACKMODE_POWER_OF_TWO_SQUARE,
        PACKMODE_COUNT
    };

public:
    
    xbool Pack( rect_xywhf* const* pV, s32 nRects, s32 MaxSide, xarray<bin>& bins, pack_mode Mode = PACKMODE_ANYSIZE );
};

