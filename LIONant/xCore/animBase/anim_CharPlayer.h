

class anim_char_player //: public 
{
public:
    ENG_ANIM_GROUP_LOCAL_TYPES

    xbool   BeginAdvanceTime( f32 DeltaTime );
    xbool   WhileAdvanceTime( event& Event )  {return FALSE;}
    
    
    xmatrix4*                           getMatrices                 ( void );
    skeleton*                           Resolve                     ( void );
    void                                setup                       ( const skeleton_ref& SkeletonRef, const xvector3& Pos )  { m_SkeletonRef = SkeletonRef; m_AccTranslations = Pos; }
    void                                AddMixer                    ( anim_track_mixer& Mixer, xbool bEntry )   { if(bEntry) m_iCurrTrackMixer = m_nMixers;  m_lTrackMixer[m_nMixers++] = &Mixer; }
    const xvector3&                     getRootTranslation          ( void ) const                              { return m_AccTranslations; }
    const xradian3&                     getRootRotation             ( void ) const                              { return m_AccRotations; }
    const xvector3&                     getRootDetailTranslation    ( void ) const                              { return m_RootKey.m_Translation; }
    const xradian3                      getRootDetailRotation       ( void ) const                              { return m_RootKey.m_Rotation.getRotation(); }

protected:

    eng_skeleton_rsc::ref                       m_SkeletonRef;
    xsafe_array<anim_track_mixer*,16>           m_lTrackMixer;
    xtransform                                  m_WorldTransform;
    s32                                         m_iCurrTrackMixer       = 0;
    s32                                         m_nMixers               = 0;

    eng_scrach_buffer_cache<xmatrix4>           m_MatrixCache;

    xradian3                                    m_AccRotations;
    xvector3                                    m_AccTranslations;
    xtransform                                  m_RootKey;

    xbool                                       m_bResolved             = FALSE;
};