
#include "anim_Base.h"

//---------------------------------------------------------------------------

anim_char_player::skeleton* anim_char_player::Resolve( void ) 
{ 
    skeleton* pSkeleton = m_lTrackMixer[ 0 ]->Resolve();

    if( m_bResolved )
        return pSkeleton;
     
    if( pSkeleton )
        m_bResolved = TRUE;
    
    return pSkeleton;
}

//---------------------------------------------------------------------------

xbool anim_char_player::BeginAdvanceTime( f32 DeltaTime )
{
    if ( DeltaTime == 0 )
        return FALSE;

    //
    // Backup the last frame root information 
    //
    const auto LastFrameRootInfo = m_RootKey; 

    //
    // Advance the mixer
    //
    auto& Mixer = *m_lTrackMixer[ m_iCurrTrackMixer ]; 
   
    Mixer.BeginAdvanceTime( DeltaTime );
   
    const anim_root_info& RootInfo  = Mixer.getRootData( );

    //
    // Set the new frame scale 
    //
    m_RootKey.m_Scale = RootInfo.m_RootKey.m_Scale;

    //
    // Rotate the new translations in the space of the current accumulator
    //
    const xvector3 AccDeltaTranslation = xquaternion( m_AccRotations ) * RootInfo.m_AccDeltaTranslation;
    const xvector3 AbsDeltaTranslation = xquaternion( m_AccRotations ) * RootInfo.m_AbsDeltaTranslation;
  
    //
    // Add the new translation delta 
    // Compute final translations for this frame
    //
    m_AccTranslations          += AccDeltaTranslation; 
    m_RootKey.m_Translation     = m_AccTranslations + AbsDeltaTranslation;

    //
    // Accumulate any delta rotation requested by the system
    // Set the new frame rotation
    //
    m_AccRotations             += RootInfo.m_AccDeltaRotation;
    m_RootKey.m_Rotation        = xquaternion( m_AccRotations ) * RootInfo.m_RootKey.m_Rotation;

    return FALSE;
}

//---------------------------------------------------------------------------

xmatrix4* anim_char_player::getMatrices( void )
{
    ASSERT( m_SkeletonRef.isValid() );
    ASSERT( m_nMixers > 0 );

    auto&       Skeleton    = *m_SkeletonRef.getAsset();
    auto*       pKeys       = m_lTrackMixer[ m_iCurrTrackMixer ]->getKeys();
//    auto&       Context     = eng_GetCurrentContext();
    auto        nBones      = Skeleton.m_nBones;
//    auto&       ScrachMem   = Context.getScrachMem();
    xmatrix4*   pMatrix     = m_MatrixCache.NewAlloc( nBones );

    //
    // Set the root matrix
    //
    m_RootKey.getMatrix( pMatrix[ 0 ] );

    //
    // Create the matrices
    //
    for ( s32 i = 1; i < nBones; i++ )
    {
        auto& Key = pKeys[ i ];

        pMatrix[ i ].setup( Key.m_Scale, Key.m_Rotation, Key.m_Translation );

        // Concatenate with parent
        if ( Skeleton.m_pBone.m_Ptr[ i ].m_iParent != -1 )
        {
            ASSERT( Skeleton.m_pBone.m_Ptr[ i ].m_iParent < i );
            pMatrix[ i ] = pMatrix[ Skeleton.m_pBone.m_Ptr[ i ].m_iParent ] * pMatrix[ i ];
        }
    }

    //
    // Apply bind matrices
    //
    for ( s32 i = 0; i < nBones; i++ )
    {
        auto& Bone = Skeleton.m_pBone.m_Ptr[ i ];
        auto& Matrix = pMatrix[ i ];

        if ( 0 )
        {
            Matrix.setTranslation( Matrix * Bone.m_BindMatrixInv.getTranslation() );
        }
        else
        {
            Matrix = Matrix * xmatrix4( Bone.m_BindMatrixInv );
        }
    }

    return pMatrix;
}

