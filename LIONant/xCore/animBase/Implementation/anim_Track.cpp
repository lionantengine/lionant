#include "anim_Base.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// anim_track_controller::scope_eval
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

anim_track_scope_eval::anim_track_scope_eval( anim_track_mixer& TrackController, eng_scrach_memory& ScrachMem )
    : 
    m_TrackMixer(TrackController),
    m_rlMutablelTrack( TrackController.m_lTrack ),
    m_EvaluateStack( ScrachMem )
{
    // allocate array of xtransform** [m_lTrack.getsize()] and initialize to null
    m_pEvaluationCache = m_EvaluateStack.Push<const xtransform*>( m_TrackMixer.m_lTrack.getCount() );
    x_memset( m_pEvaluationCache, 0, sizeof(*m_pEvaluationCache) * m_TrackMixer.m_lTrack.getCount() );
}

//---------------------------------------------------------------------------

const xtransform* anim_track_scope_eval::EvaluateTrack   ( s32 iTrack )
{
    if( NULL == m_pEvaluationCache[iTrack] ) 
        m_pEvaluationCache[iTrack] = m_rlMutablelTrack[iTrack]->getKeys( *this );

    return  m_pEvaluationCache[iTrack];
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// eng_anim_track
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void eng_anim_track_playaback::setAnimation( const eng_anim_group_rsc::ref& AnimgroupRef, ianim iAnim, f32 BlendTime, f32 PlayBackRate )
{
    ASSERT( iAnim.m_Value != ianim::INVALID );
    ASSERT( iAnim.m_Value >= 0 );

    // Make sure that it is already loaded
    auto*   pNewAnimGroup  = AnimgroupRef.getAsset();
    ASSERT( pNewAnimGroup );
    auto&   NewAnimInfo    = pNewAnimGroup->m_pAnimInfo.m_Ptr[ iAnim.m_Value ];

    // Are we playing the same animation?
    if( m_iAnim.m_Value == iAnim.m_Value && pNewAnimGroup == m_AnimGroupRef.getAsset() )
	{
        const xbool     bLoop       = x_FlagIsOn( NewAnimInfo.m_Flags, animinfo::FLAGS_LOOP );
        
        m_Rate = PlayBackRate;
        if( m_bReachedEnd )
        {
            ASSERT( bLoop == FALSE );

            if( m_Rate < 0 ) m_FrameTime = f32(NewAnimInfo.m_AnimKeys.m_nFrames-1);
            else             m_FrameTime = 0;
            
            m_bReachedEnd = FALSE;
        }

        return; 
    }

    //
    // Updated the root flags
    //
    if( x_FlagIsOn( m_RootInfo.m_Flags, animinfo::FLAGS_ACCUM_ROTATIONS ) ) 
        m_FlushAccRotation = m_RootInfo.m_Flags;
    else if( x_FlagIsOn( NewAnimInfo.m_Flags, animinfo::FLAGS_ACCUM_ROTATIONS ) ) 
        m_FlushAccRotation = NewAnimInfo.m_Flags;
    else 
        m_FlushAccRotation = 0;


    m_RootInfo.m_Flags = NewAnimInfo.m_Flags;

    //
    // Reset playback values
    //  
    auto LastAnim       = m_iAnim;
    m_AnimGroupRef      = AnimgroupRef;
    m_iAnim             = iAnim;
    m_bReachedEnd       = FALSE;

    m_Rate          = PlayBackRate;
    if( m_Rate < 0 ) m_FrameTime = f32(NewAnimInfo.m_AnimKeys.m_nFrames-1);
    else             m_FrameTime = 0;
    
    m_LastFrameTime = 0;
    m_LastCycle     = 0;
    m_Cycle         = 0;
    if( LastAnim.m_Value != ianim::INVALID )
    {
        BlendTime = x_Abs( BlendTime * PlayBackRate );
        if( BlendTime < 0.001f )
        {
            m_BlendT        = 0;
            m_BlendRate     = 0;
        }
        else
        {
            m_BlendRate     = 1.f/BlendTime;
            m_BlendT        = 1.0f;
        }
    }

    if( FALSE == m_Cache.isValid() )
    {
        m_BlendT = 0;
    }
}

//---------------------------------------------------------------------------

xbool eng_anim_track_playaback::BeginAdvanceTime( f32 DeltaTime )
{
    //
    // Make sure that everything is ok
    // 
    if( m_iAnim.m_Value == ianim::INVALID )
        return TRUE; 

    if( m_bReachedEnd )
        return TRUE;

    auto* pAnimGroup = m_AnimGroupRef.getAsset();
    if( pAnimGroup == NULL )
        return TRUE;

    auto& AnimGroup  = *pAnimGroup;

    //
    // Remember previous frame and cycle
    //
    m_LastFrameTime = m_FrameTime;
    m_LastCycle     = m_Cycle;

    //
    // Count down blend time
    //
    if( m_BlendT > 0 ) 
    {
//        const u32 NewID = eng_GetCurrentContext().GetCurrentFrameNumber();
        
        m_BlendT    -= DeltaTime * m_BlendRate;
        if( m_BlendT <= 0 || FALSE == m_Cache.isValid() )
        {
            m_BlendT    = 0;
            m_BlendRate = 0;
        }
    }

    //
    // Advance frame 
    //
    auto&           AnimInfo    = AnimGroup.m_pAnimInfo.m_Ptr[ m_iAnim.m_Value ];
    f32             nFrames     = DeltaTime * AnimInfo.m_FPS * m_Rate;
    const xbool     bLoop       = x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_LOOP );
    xbool           bCrossOver  = FALSE;
    const xbool     bFirstFrame = ( m_LastFrameTime == 0 && m_Cycle == 0 );

    m_FrameTime += nFrames;

    // Update which cycle we are in and modulate the frame
    while( m_FrameTime >= (AnimInfo.m_AnimKeys.m_nFrames) )
    {
        m_FrameTime -= AnimInfo.m_AnimKeys.m_nFrames;
        m_Cycle++;
        bCrossOver = TRUE;
    }
    ASSERT( m_FrameTime < AnimInfo.m_AnimKeys.m_nFrames );

    // handle going backwards in time
    while( m_FrameTime < 0 )
    {
        m_FrameTime += AnimInfo.m_AnimKeys.m_nFrames;
        m_Cycle++;
        bCrossOver = TRUE;
    }
    ASSERT( m_FrameTime >= 0 );
    ASSERT( m_FrameTime < AnimInfo.m_AnimKeys.m_nFrames );

    if( bCrossOver && x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_PING_PONG ) )
    {
        m_FrameTime = AnimInfo.m_AnimKeys.m_nFrames - m_FrameTime;
        m_Rate = -m_Rate;
    }
    // If the anim doesn't loop and we are past the loop boundary then peg
    else if( !bLoop && bCrossOver ) 
    {
        if( m_Rate > 0 )  m_FrameTime = AnimInfo.m_AnimKeys.m_nFrames - 0.001f;
        else              m_FrameTime = 0;

        m_Cycle          = 0;
        m_bReachedEnd    = TRUE;
    }

    //
    // Do loop blending... I am not sure about if the length should be a parameter or not
    //
    if( bCrossOver ) 
    {
        if( x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_LOOP_INTERPOLATION ) )
        {
            static const f32 BlendTime = 0.9f;
            m_BlendRate = 1.f / BlendTime;
            m_BlendT    = 1.0f;
        }
        else
        {
            m_BlendRate = 0;
            m_BlendT    = 0;
        }    
    }

    //
    // compute deltas
    //
    m_OldRootKey = m_RootInfo.m_RootKey;

    // get new root
    g_AnimGroupSystem.getInterpolatedRoot( m_RootInfo.m_RootKey, AnimGroup, m_iAnim, m_FrameTime, m_Rate );

    auto backup = m_RootInfo.m_RootKey;

    // We recompute the last frame root in the new space loop (This is for discontinuous animations)
    xquaternion DeltaRot;
    u32         FlushAccRotation = 0;


    if( bCrossOver )
    {
        if ( x_FlagIsOn( m_RootInfo.m_Flags, animinfo::FLAGS_ACCUM_ROTATIONS ) )
        {
            // Determine the delta yaw of the previous animation so far
            DeltaRot = m_RootInfo.m_BaseRootKey.m_Rotation.getDeltaRotation( m_OldRootKey.m_Rotation );
            FlushAccRotation = m_RootInfo.m_Flags;
        }

        // Get the old root in the same animation loop space as the new root
        const s32 LastTime = s32( m_LastFrameTime );
        const f32 T = 1 - ( m_LastFrameTime - LastTime );
        
        g_AnimGroupSystem.getInterpolatedRoot( m_OldRootKey, AnimGroup, m_iAnim, T, -m_Rate );
    }

    // Blend when switching animations
    if ( m_BlendT > 0 && m_Cache.isValid() )
    {
        ASSERT( m_Cache.isValid() );
        const auto& LastCachedFrame = m_Cache.getCache()[ 0 ];
        
        m_RootInfo.m_RootKey.m_Scale   += m_BlendT*( LastCachedFrame.m_Scale - m_RootInfo.m_RootKey.m_Scale );
        m_RootInfo.m_RootKey.m_Rotation = m_RootInfo.m_RootKey.m_Rotation.BlendAccurate( m_BlendT, LastCachedFrame.m_Rotation );

        // The first frame of an animation always increments the delta yaw
        // TODO: This is not accurate since we don't always want this but we will wait until becomes more
        //       clear in the pipeline what the user wants
        if ( ( bFirstFrame || bCrossOver ) && x_FlagIsOn( m_FlushAccRotation, animinfo::FLAGS_ACCUM_ROTATIONS ) )
        {
            auto Randian3Rots = m_RootInfo.m_RootKey.m_Rotation.getRotation();

            Randian3Rots.m_Pitch =0;
            Randian3Rots.m_Roll = 0;
            xquaternion YawInvRot( Randian3Rots );
            YawInvRot.Invert();

            // Determine the delta yaw of the previous animation so far
            DeltaRot = m_RootInfo.m_BaseRootKey.m_Rotation.getDeltaRotation( m_RootInfo.m_RootKey.m_Rotation );

            x_printf( "--%f--\n", RAD_TO_DEG(DeltaRot.getRotation().m_Yaw) );
            
            // Remove the all the yaw from current frame as we are going to added in the accumulator
            m_RootInfo.m_RootKey.m_Rotation = YawInvRot * m_RootInfo.m_RootKey.m_Rotation;
            m_RootInfo.m_BaseRootKey = m_RootInfo.m_RootKey;

            FlushAccRotation |= animinfo::FLAGS_ACCUM_ROTATION_YAW;
        }
    }
    else if ( bFirstFrame )
    {
        {
            DeltaRot = m_RootInfo.m_RootKey.m_Rotation.getDeltaRotation( m_OldRootKey.m_Rotation );
            FlushAccRotation |= animinfo::FLAGS_ACCUM_ROTATION_YAW;
        }
        m_RootInfo.m_BaseRootKey = m_RootInfo.m_RootKey;
    }

    //
    // Flush rotation accumulation
    //
    if( FlushAccRotation )
    {
        m_RootInfo.m_AccDeltaRotation = DeltaRot.getRotation();
        if ( !x_FlagIsOn( FlushAccRotation, animinfo::FLAGS_ACCUM_ROTATION_ROLL ) )
            m_RootInfo.m_AccDeltaRotation.m_Roll  = 0;

        if ( !x_FlagIsOn( FlushAccRotation, animinfo::FLAGS_ACCUM_ROTATION_PITCH ) )
            m_RootInfo.m_AccDeltaRotation.m_Pitch = 0;
        
        if ( !x_FlagIsOn( FlushAccRotation, animinfo::FLAGS_ACCUM_ROTATION_YAW ) )
            m_RootInfo.m_AccDeltaRotation.m_Yaw   = 0;

        m_FlushAccRotation = m_RootInfo.m_Flags;
    }
    else
    {
        m_RootInfo.m_AccDeltaRotation.Set(0,0,0);
    }

    //
    // Deal with translation deltas
    //
    if( !bFirstFrame && x_FlagIsOn( AnimInfo.m_Flags,   animinfo::FLAGS_ACCUM_TRANSLATIONS ) )
    {
        m_RootInfo.m_AccDeltaTranslation = m_RootInfo.m_RootKey.m_Translation - m_OldRootKey.m_Translation;
    }

    //
    // Overwrite deltas with abs deltas for any fields that are needed
    //
    m_RootInfo.m_AbsDeltaTranslation.Zero();

    if ( !x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_ACCUM_TRANSLATION_X ) )
    {
        m_RootInfo.m_AbsDeltaTranslation.m_X = m_RootInfo.m_RootKey.m_Translation.m_X - m_RootInfo.m_BaseRootKey.m_Translation.m_X;
        m_RootInfo.m_AccDeltaTranslation.m_X = 0;
    }

    if ( !x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_ACCUM_TRANSLATION_Y ) )
    {
        m_RootInfo.m_AbsDeltaTranslation.m_Y = m_RootInfo.m_RootKey.m_Translation.m_Y - m_RootInfo.m_BaseRootKey.m_Translation.m_Y;
        m_RootInfo.m_AccDeltaTranslation.m_Y = 0;
    }

    if ( !x_FlagIsOn( AnimInfo.m_Flags, animinfo::FLAGS_ACCUM_TRANSLATION_Z ) )
    {
        m_RootInfo.m_AbsDeltaTranslation.m_Z = m_RootInfo.m_RootKey.m_Translation.m_Z - m_RootInfo.m_BaseRootKey.m_Translation.m_Z;
        m_RootInfo.m_AccDeltaTranslation.m_Z = 0;
    }

    return m_bReachedEnd;
}

//---------------------------------------------------------------------------

const anim_root_info&   eng_anim_track_playaback::getRootData( void ) const
{
    return m_RootInfo;
}

//---------------------------------------------------------------------------

xbool eng_anim_track_playaback::WhileAdvanceTime( event& Event )
{
    return FALSE;
}

//---------------------------------------------------------------------------

const xtransform* eng_anim_track_playaback::getKeys( anim_track_scope_eval& Eval )
{
    auto&       Context     = eng_GetCurrentContext();

    // Check if we have the news cached
    if( m_Cache.isCachedThisFrame() )
        return m_Cache.getCache();

    //
    // Compute a new set of keys
    //
    auto*       pAnimGroup  = m_AnimGroupRef.getAsset();
    ASSERT( pAnimGroup );
    auto&       AnimInfo    = pAnimGroup->m_pAnimInfo.m_Ptr[ m_iAnim.m_Value ];
    auto        nBones      = AnimInfo.m_AnimKeys.m_nBones;
    auto&       ScrachMem   = Context.getScrachMem();
    xtransform* pNewKeys    = ScrachMem.BufferAlloc<xtransform>( nBones );
    
    g_AnimGroupSystem.cpKeyFrames( *pAnimGroup, m_iAnim, m_FrameTime, m_Rate, [ this, &pNewKeys, &pAnimGroup ]( 
        keyframe*                       pF0, 
        keyframe*                       pF1, 
        f32                             T, 
        s32                             nBones,
        xbool                           bLooping )
    {
        const xtransform* pCachedTransform = NULL;
        if ( m_BlendT > 0 )
        {
            ASSERT( m_Cache.isValid() );
            pCachedTransform = m_Cache.getCache();
        }

        pNewKeys[0] = m_RootInfo.m_RootKey;
        for( s32 i=1; i<nBones; i++ )
        {
            auto& NewKey = pNewKeys[i];

            //
            // Get current Key
            //
            NewKey.Blend( pF0[i], T, pF1[i] );

            //
            // See if we have to also blend with previous keys
            //
            if( pCachedTransform )
            {
                auto& LastFrame = pCachedTransform[ i ];

                NewKey.Blend( m_BlendT, LastFrame );
            }
        }
    } );

    //
    // Update the key cache information
    //
    m_Cache.setup( pNewKeys, Context.GetCurrentFrameNumber() );

    return pNewKeys;
}

