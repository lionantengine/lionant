 #include "anim_Base.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// anim_track_controller
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

const xtransform* anim_track_mixer::getKeys( void )
{
    auto&                               Context   = eng_GetCurrentContext();
    auto&                               ScrachMem = Context.getScrachMem();
    anim_track_scope_eval               Eval( *this, ScrachMem );

    Eval.EvaluateTrack( m_iRootTrack );

    //
    // Cache results
    //
    // if( ) the memory of the root is in the stack then we will have to allocate a buffer size
    //       and copy the keys if not we can cache the results with our cache

    return Eval.m_pEvaluationCache[ m_iRootTrack ];
}

//---------------------------------------------------------------------------

void anim_track_mixer::AddTrack( eng_anim_track_base& Track, xbool bIsRoot )
{
    ASSERT( m_SkeletonRef.isValid() );
    mutable_ref( rlTrack, m_lTrack );
    if ( bIsRoot ) rlTrack->append( m_iRootTrack ) = &Track;
    else          rlTrack->append() = &Track;
}

//---------------------------------------------------------------------------

xmatrix4* anim_track_mixer::getMatrices( xbool bGetRoot )
{
    ASSERT(0);
    return NULL;

    /*
    ASSERT( m_SkeletonRef.isValid() );
    ASSERT( m_lTrack.getCount() > 0 );

    auto&       Skeleton    = *m_SkeletonRef.getAsset();
    auto*       pKeys       = getKeys();
    auto&       Context     = eng_GetCurrentContext();
    auto        nBones      = Skeleton.m_nBones;
    auto&       ScrachMem   = Context.getScrachMem();
    xmatrix4*   pMatrix     = m_MatrixCache.NewAlloc( nBones );

    //
    // Create the matrices
    //
    s32 iStart;
    if( bGetRoot )  iStart = 1;
    else            iStart = 0;
        
    for ( s32 i = iStart; i < nBones; i++ )
    {
        auto& Key = pKeys[ i ];

        pMatrix[ i ].setup( Key.m_Scale, Key.m_Rotation, Key.m_Translation );

        // Concatenate with parent
        if ( Skeleton.m_pBone.m_Ptr[ i ].m_iParent != -1 )
        {
            ASSERT( Skeleton.m_pBone.m_Ptr[ i ].m_iParent < i );
            pMatrix[ i ] = pMatrix[ Skeleton.m_pBone.m_Ptr[ i ].m_iParent ] * pMatrix[ i ];
        }
    }

    // Apply bind matrices
    for ( s32 i = iStart; i < nBones; i++ )
    {
        auto& Bone = Skeleton.m_pBone.m_Ptr[ i ];
        auto& Matrix = pMatrix[ i ];

        if ( 0 )
        {
            Matrix.setTranslation( Matrix * Bone.m_BindMatrixInv.getTranslation() );
        }
        else
        {
            Matrix = Matrix * xmatrix4(Bone.m_BindMatrixInv);
        }
    }

    return pMatrix;
    */
}

//---------------------------------------------------------------------------

xbool anim_track_mixer::BeginAdvanceTime( f32 DeltaTime )
{
    mutable_ref( rlTrack, m_lTrack );
    for ( auto& pTrack : rlTrack )
    {
        return pTrack->BeginAdvanceTime( DeltaTime );
        
        // TODO what should happen with this?
        //m_LastRootKey = m_NewRootKey;
        //pTrack->getRootData( m_NewRootKey,m_DeltaTranslation, m_DeltaRotation );
    }

    ASSERT(0);
    return rlTrack[ 0 ]->BeginAdvanceTime( DeltaTime );
}

//---------------------------------------------------------------------------

const anim_root_info& anim_track_mixer::getRootData( void ) const
{
    const_ref( rlTrack, m_lTrack );
    for ( auto& pTrack : rlTrack )
    {
        return pTrack->getRootData();
    }

    ASSERT( 0 );
    return rlTrack[0]->getRootData();
}
