//
//  eng_Base.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

//===============================================================================
// TRACK BASE
//===============================================================================

struct  anim_track_scope_eval;
class   anim_track_mixer;
class   eng_anim_track_base;

//===============================================================================
// anim_track_scope_eval
//===============================================================================

struct anim_track_scope_eval
{
    anim_track_scope_eval( void ) = delete;
    anim_track_scope_eval( anim_track_mixer& TrackController, eng_scrach_memory& ScrachMem );
    const xtransform*   EvaluateTrack( s32 iTrack );

    anim_track_mixer&                               m_TrackMixer;
    mutable_xref<xarray2<eng_anim_track_base*>>     m_rlMutablelTrack;
    eng_scrach_memory::xscope_stack                 m_EvaluateStack;
    const xtransform**                              m_pEvaluationCache;
};

//===============================================================================
// anim_root_info
//===============================================================================

struct anim_root_info
{
         anim_root_info( void ){ m_Flags = 0; Identity(); }

    void Identity( void )
    {
        m_RootKey.Identity();
        m_AccDeltaRotation.Zero();
        m_AccDeltaTranslation.Zero();
        m_AbsDeltaTranslation.Zero();
    }

    xtransform          m_RootKey;
    xtransform          m_BaseRootKey;
    xvector3            m_AccDeltaTranslation;
    xvector3            m_AbsDeltaTranslation;
    xradian3            m_AccDeltaRotation;
    u32                 m_Flags;
};

//===============================================================================
// eng_anim_track_base
//===============================================================================

class eng_anim_track_base
{
public:
    ENG_ANIM_GROUP_LOCAL_TYPES


public:
    virtual const xtransform*       getKeys                   ( anim_track_scope_eval& Eval ) = 0; 
    virtual xbool                   BeginAdvanceTime          ( f32 DeltaTime ) {return FALSE;}
    virtual xbool                   WhileAdvanceTime          ( event& Event )  {return FALSE;}
    virtual const anim_root_info&   getRootData               ( void ) const = 0;

    // Must be a way to return the per bone weight used for mixing
};

//===============================================================================
// PLAYBACK
//===============================================================================

class eng_anim_track_playaback : public eng_anim_track_base
{
public:
    ENG_ANIM_GROUP_LOCAL_TYPES

    void                            setAnimation            ( ianim iAnim, f32 BlendTime, f32 PlayBackRate = 1 ) { setAnimation( m_AnimGroupRef, iAnim, BlendTime, PlayBackRate ); }
    void                            setAnimation            ( const eng_anim_group_rsc::ref& AnimgroupRef, ianim iAnim, f32 BlendTime, f32 PlayBackRate=1 );
    void                            setup                   ( const eng_anim_group_rsc::ref& AnimgroupRef ) { m_AnimGroupRef = AnimgroupRef; }
    ianim                           getAnimation            ( void ) const { return m_iAnim; }
    const eng_anim_group_rsc::ref&  getAnimGroupRef         ( void ) const { return m_AnimGroupRef; }
    void                            setFrame                ( f32 FrameTime ) { m_FrameTime = FrameTime; }
    ianim                           getAnimationIndex       ( ianimhash Hash )         
    {
        // First make sure if we have it
        auto* pAnimGroup = m_AnimGroupRef.getAsset();
        if ( pAnimGroup == NULL )
            return ianim(0);

        auto& AnimGroup = *pAnimGroup;
        ianim iAnimation( -1 );

        eng_anim_group_rsc::hash_entry Key( Hash );
        
        // Set the next animation
        const eng_anim_group_rsc::hash_entry* pEntry = ( const eng_anim_group_rsc::hash_entry* )x_bsearch(
            &Key,
            AnimGroup.m_pHashTable.m_Ptr,
            AnimGroup.m_nTotalAnims,
            sizeof( *AnimGroup.m_pHashTable.m_Ptr ),
            eng_anim_group_rsc::hash_entry::CompareFunction );

        ASSERT( pEntry );
        return ianim( pEntry->m_iAnim );
    }

  //  ianim                           getAnimation            ( void ) const        { return m_iAnim; }
    virtual  xbool                  BeginAdvanceTime        ( f32 DeltaTime ) override;
    virtual  xbool                  WhileAdvanceTime        ( event& Event ) override;
    virtual const xtransform*       getKeys                 ( anim_track_scope_eval& Eval ) override;
    virtual const anim_root_info&   getRootData             ( void ) const override;

protected:

    eng_anim_group_rsc::ref                 m_AnimGroupRef;
    f32                                     m_BlendRate             =   0;
    f32                                     m_BlendT                =   0;
    f32                                     m_Rate                  =   1;
    f32                                     m_FrameTime             =   0;
    f32                                     m_LastFrameTime         =   0;
    s32                                     m_Cycle                 =   0;
    s32                                     m_LastCycle             =   0;
    u32                                     m_FlushAccRotation      =   0;
    ianim                                   m_iAnim                 =   ianim( ianim::INVALID );
    eng_scrach_buffer_cache<xtransform>     m_Cache;
    xbool                                   m_bReachedEnd:1;

    xtransform                              m_OldRootKey;
    anim_root_info                          m_RootInfo;
};

//===============================================================================
// 1D ADDITIVE
//===============================================================================

class eng_anim_track_1d_addtive : public eng_anim_track_base
{
protected:

        s32                     m_iAnimTrackFromBase;
        s32                     m_iAnimTrackToAdd;
        f32                     m_AdditiveWeight;
};

//===============================================================================
// 1D BLEND
//===============================================================================

class eng_anim_track_1d_blend : public eng_anim_track_base
{
protected:

        s32                     m_iAnimTrackFromBase;
        s32                     m_iAnimTrackToBlend;
        f32                     m_T;
};

//===============================================================================
// 2D BLEND
//===============================================================================

class eng_anim_track_2d_blend : public eng_anim_track_base
{
public:
    

protected:

    struct entry
    {
        s32                     m_iTrack;
        f32                     m_Weight;
        xvector2                m_PositionInMesh;
    };

protected:

    xarray2<entry>              m_lAnimTrack;
};

