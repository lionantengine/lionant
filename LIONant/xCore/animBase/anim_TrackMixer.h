//
//  eng_Base.h
//  engBase
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class eng_anim_track_base;

class anim_track_mixer : public eng_anim_track_base
{
public:
    ENG_ANIM_GROUP_LOCAL_TYPES

public:

    virtual xbool                       BeginAdvanceTime    ( f32 DeltaTime ) override;
    virtual const xtransform*           getKeys             ( anim_track_scope_eval& Eval ) override { ASSERT(0); return NULL; }
    virtual const anim_root_info&       getRootData         ( void ) const override;

    const xtransform*                   getKeys             ( void );
    xmatrix4*                           getMatrices         ( xbool bGetRootMatrix );
    skeleton*                           Resolve             ( void ) { return m_SkeletonRef.getAsset(); }
    void                                setup               ( const skeleton_ref& SkeletonRef ) { m_SkeletonRef = SkeletonRef; }
    void                                AddTrack            ( eng_anim_track_base& Track, xbool bIsRoot );

protected:
   
    eng_skeleton_rsc::ref                   m_SkeletonRef;                  // This is the skeleton which this track controller uses
    s32                                     m_iRootTrack            = -1;   // Which track is root
    xarray2<eng_anim_track_base*>           m_lTrack;

    /*
    xtransform                              m_LastRootKey;
    xtransform                              m_NewRootKey;
    xquaternion                             m_DeltaRotation;
    xvector3                                m_DeltaTranslation;
    */

protected: 

    friend struct anim_track_scope_eval;

};
