#ifndef GB_REPLAY_MGR_H
#define GB_REPLAY_MGR_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// REPLAY_MANAGER
/////////////////////////////////////////////////////////////////////////////////

class gb_replay_mgr
{
system_public:

                gb_replay_mgr               ( void );
    void        Init                        ( void );
    void        RecordFrame                 ( void );
    void        PlayBack                    ( void );
    xbool       isRecording                 ( void ) const { return m_State == STATE_RECORDING; }
    xbool       isPlayingBack               ( void ) const { return m_State == STATE_PLAYBACK;  }
    void        setupRecording              ( void );
    void        Stop                        ( void );
    void        setupPlayback               ( xbool bLoop );
    void        setupRecordNetworkPlayer    ( xhandle hPlayer ) { m_hNetworkPlayer = hPlayer; }

protected:

    struct frame
    {
		xvector3d	m_Origin;			// Origin for the compression of the positions
        u8          m_iDeltaTime;       // Delta Time for this frame in 1/60fps
        u16         m_nComponents;      // Number of entities saved
    };

    enum state
    {
        STATE_OFF,
        STATE_RECORDING,
        STATE_STOP_RECORDING,
        STATE_PLAYBACK,
        STATE_STOP_PLAYBACK
    };

protected:

    s32                 m_iPlaybackFrame;   // Current Frame of the playback
    f64                 m_TotalTimeRecorded;
    state               m_State;            // Current state of the system
    xbitstream          m_BitStream;        // Information about the frames been recorded
    xarray<frame>       m_Frames;           // Information about each of the frames.
    xarray<s32>         m_iPackTypes;       // Which types have pack data
    xptr<xbyte>         m_PlaybackBuffer;   
    s32                 m_Length;
    xhandle             m_hNetworkPlayer;
    xbool               m_bLoop;
};

extern gb_replay_mgr    g_ReplayMgr;

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif