#ifndef GB_CLOUD_H
#define GB_CLOUD_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// PRE-DEFINES
//////////////////////////////////////////////////////////////////////////////////
class gb_sync_point;
class gb_cloud_state;
struct gb_mgr_base_msg;

//////////////////////////////////////////////////////////////////////////////////
// BASE MANAGER
//////////////////////////////////////////////////////////////////////////////////
class gb_base_mgr : public x_simple_job<1>, public xproperty
{
public:
    x_rtti_class1(gb_base_mgr, x_simple_job<1>);

    typedef void (gb_base_mgr::*callback_fn)( const gb_mgr_base_msg& Msg );

                                        gb_base_mgr             ( void );
                                        gb_base_mgr             ( const char* pName );
    virtual                            ~gb_base_mgr             ( void );
    static void                         InitAll                 ( void );
    static void                         KillAll                 ( void );
            gb_sync_point&              getSyncPoint            ( void ) const { return *m_pSyncPoint; }
            xbool                       hasUpdatedThisFrame     ( void ) const;

protected:

    virtual xbool                       onPropQuery             ( xproperty_query& Query ) override;
    virtual void                        onPropEnum              ( xproperty_enum&  Enum  ) const override;

    virtual void                        onClearUp               ( void ) {}
    virtual void                        onDispatchMessages      ( void );
    virtual void                        onUpdate                ( void ) {}

    virtual void                        vReset                  ( void );
    virtual void                        vInit                   ( void );
    virtual void                        vKill                   ( void );
    virtual void                        sendMessage             ( const gb_mgr_base_msg& Message );

system_protected:

    virtual void                        onRun                   ( void );
    virtual void                        MessageHandler          ( gb_mgr_base_msg& Msg );

system_protected:

    x_qt_fober_queue            m_MessageQueue;
    gb_base_mgr*                m_pLocalNext;           // Next Manager (Link list of managers in side a sync point)
    gb_sync_point*              m_pSyncPoint;           // A reference back to the sync point
    const char*                 m_pName;                // Name of the manager used for debugging
    u32                         m_ProcessTick;          // Allows us to know which process tick we have done
    gb_base_mgr*                m_pGMLNext;             // Global Manager List Next
    static gb_base_mgr*         s_pGMLManagerList;

protected:

    friend class gb_sync_point;
    friend class gb_state_mgr;
    friend class gb_game_mgr;
    friend class gb_cloud_state;
};

//////////////////////////////////////////////////////////////////////////////////
// MANAGER BASE MESSAGE
//////////////////////////////////////////////////////////////////////////////////

struct gb_mgr_base_msg : public x_qt_ptr
{
    x_rtti_base(gb_mgr_base_msg); 

    union type_union
    {
        void*   m_pPointer;
        s32     m_S32[2];
        u32     m_U32[2];
        u64     m_U64;
        u16     m_U16[4];
        u8      m_U8[4];
    };

    gb_base_mgr::callback_fn        m_MessageFunc;                          // Function which a certain message is a associated with
    xsafe_array<type_union,2>       m_Arg;
};

//////////////////////////////////////////////////////////////////////////////////
// SYNC-POINT
//////////////////////////////////////////////////////////////////////////////////
class gb_sync_point : public x_simple_trigger<8>
{
public:
    x_rtti_class1( gb_sync_point, x_simple_trigger<8> )

                                        gb_sync_point           ( void ) = default;
    virtual                            ~gb_sync_point           ( void );

    void                                setup                   ( const char* pName, gb_cloud_state& State, const x_va_list& Dependencies );
    void                                setup                   ( const char* pName, gb_cloud_state& State, const x_va_list& Dependencies, const x_va_list& Managers );

    xbool                               hasTimeAdvance          ( void ) const;
    s32                                 getTxRead               ( s32 Tx );
    s32                                 getT1Write              ( void );
    s32                                 getLatestReadTx         ( void );
    s32                                 getTickCount            ( void );
    s32                                 getFrameTick            ( void ) const { return m_FrameTick; }

system_protected:

    virtual void                        vReset                  ( void );
    void                                setFrameTick            ( u32 Tick )    { m_FrameTick = Tick; m_TickCount = m_FrameTick-1; }
    void                                setup                   ( const char* pName, gb_cloud_state& State, const x_va_list* pDependencies, const x_va_list* pManagers ); 
    virtual void                        doBeforeTrigger         ( void );

protected:

    u32                             m_TickCount         = 0;        // This can be thought of the T0 tick. But within a frame because it
                                                                    //  gets incremented before updating is T0 after updated is T1.
    u32                             m_FrameTick         = 0;        // This is the time that we are going to become ones we go from T0 to T1. 
                                                                    //  You can also think of this as the T1 timer for a particular frame.

    gb_base_mgr*                    m_pManager          = NULL;     // Simple Link List of all the managers we are incharge off
    s32                             m_nDependencies     = 0;
    xsafe_array<gb_sync_point*,10>  m_lDependency;                  // List of sync points that we are dependent on
    gb_sync_point*                  m_pExecNext         = NULL;     // Link list of sync points inside a state
    const char*                     m_pName             = NULL;     // Name of the sync point. Use for debbuging

protected:

    friend class gb_cloud_state;
    friend class gb_state_mgr;
};

//////////////////////////////////////////////////////////////////////////////////
// CLOUD STATE
//////////////////////////////////////////////////////////////////////////////////
class gb_cloud_state
{
public:
                                        gb_cloud_state              ( void );
    virtual                            ~gb_cloud_state              ( void );
    void                                setupBeging                 ( const char* pName );
    void                                setupEnd                    ( void );
    gb_cloud_state*                     findState                   ( const char* pName ) const;
    gb_sync_point&                      getEndSyncP                 ( void ) { return m_EndSyncP; }
    gb_sync_point&                      getStartSyncP               ( void ) { return m_StartSyncP; }

protected:

    s32                                 findLongestPath             ( gb_sync_point& SyncPoint, s32 c ) const;
    void                                BuildProcessingPath         ( void );
    virtual void                        onBeginFrame                ( void ){}
    virtual void                        onEndFrame                  ( void ){}
    
protected:

    struct post_end_sync_point : public gb_sync_point
    {
        virtual void  vReset ( void );
        xbool               m_bLoop;
        gb_cloud_state*     m_pState;
    };

protected:

            gb_sync_point*              m_pSyncPExecPath    = NULL;     // The start of the execution path in the graph
            gb_cloud_state*             m_pStateNext        = NULL;     // Next for the Link lists of states
    static  gb_cloud_state*             m_pFirstState;                  // Head of the link list of states
            const char*                 m_pStateName        = NULL;     // Name of the state

system_public:

            gb_sync_point               m_StartSyncP;                   // The first sync point of the graph
            gb_sync_point               m_EndSyncP;                     // The sync point compleates the frame
            post_end_sync_point         m_PostEndSyncP;                 // The last sync point of the graph. For any system mgr that needs to do work past the end of the frame.
            s32                         m_nManagers         = 0;        // Number of managers in the state

protected:

    friend class gb_sync_point;
    friend struct graph_building_job;
    friend class gb_game_mgr;
    friend class gb_state_mgr;
};

//////////////////////////////////////////////////////////////////////////////////
// INCLUDE INLINES
//////////////////////////////////////////////////////////////////////////////////
#ifndef GB_CLOUD_INLINE_H
#include "Implementation/gb_cloud_inline.h"
#endif

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif
