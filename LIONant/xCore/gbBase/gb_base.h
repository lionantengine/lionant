#ifndef GB_BASE_H
#define GB_BASE_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifdef USE_PRAGMA_ONCE 
#pragma once 
#endif

//////////////////////////////////////////////////////////////////////////////////
// X FILES
//////////////////////////////////////////////////////////////////////////////////
#include "eng_base.h"

// VC2015 does not deal with __declspec
#if _MSC_VER >= 1900
    #define __declspec(noinline)
    #include <stdio.h>
    #undef __declspec
#else
    #include <stdio.h>
#endif

#define system_protected    protected
#define system_public       public

extern s32 g_order;

class gb_game_world;

//////////////////////////////////////////////////////////////////////////////////
// GAME LIB
//////////////////////////////////////////////////////////////////////////////////
#include "Implementation/gb_component_inline.h"
#include "gb_cloud.h"
#include "gb_state_mgr.h"
#include "gb_component.h"
#include "gb_msg.h"
#include "gb_component_type_mgr.h"
#include "gb_component_mgr.h"
#include "gb_entity.h"
#include "gb_blueprint.h"
#include "gb_entity_mgr.h"
#include "gb_game_mgr.h"
#include "gb_replay_mgr.h"
#include "gb_network_mgr.h"
#include "gb_input_mgr.h"
#include "gb_game_world.h"

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif