#ifndef GB_COMPONENT_TYPE_MANAGER_H
#define GB_COMPONENT_TYPE_MANAGER_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE MANAGER
//////////////////////////////////////////////////////////////////////////////////

class gb_component_type_mgr
{
public:
    
                                gb_component_type_mgr   ( void );
    gb_component_type*          findComponentType       ( u32 Guid );
    gb_component_type*          findComponentType       ( const xrtti& RTTI );
    gb_component_type*          findComponentType       ( const char* pName );
    xhandle                     getNetworkID            ( xhandle hComponent );

system_public:

    struct network_info
    {
        xhandle             m_hNetworkID;
        xsafe_array<f32,16> m_Priority;             // All the priorities for all the network players
    };

protected:

    struct type_node
    {
        gb_component_type*                  m_pType;
        s32                                 m_nActive;                  // Number of active components
        xbool                               m_bNetwork:1;
        xharray<gb_component*>              m_lComponent;               // List of components of this type
        xptr<network_info>                  m_lNetworkInfo;             // Used when this component is a server component

        type_node( void )
        {
            m_pType     = NULL;
            m_nActive   = 0;
        }
    };

system_public:
	
    void                        vInit                   ( void );

    void                        ActivateComponent       ( gb_component& Component );
    void                        DeactivateComponent     ( gb_component& Component );

    void                        RegisterComponent       ( gb_component& Component );
    void                        UnregisterComponent     ( gb_component& Component );

    gb_component&               GetComponentFromHandle    ( xhandle hComponent );
    gb_component&               GetComponentFromNetworkID ( xhandle hNetworkID ); 

    network_info&               getNetworkInfo          ( xhandle hComponent );

    gb_component_type&          GetType                 ( s32 iType ) { return *m_lComponentTypes[iType].m_pType;      }
    xharray<gb_component*>&     GetComponentList        ( s32 iType ) { return m_lComponentTypes[iType].m_lComponent; }
    s32                         GetActiveCount          ( s32 iType ) { return m_lComponentTypes[iType].m_nActive;    }

    s32                         getMessageGroupCount    ( void )      { return m_MessageGroupCount; }
    void                        getAllActiveTypes       ( const gb_base_mgr& BaseMgr, xarray<s32>& TypeList )  const; 
    void                        getAllNetworkTypes      ( xarray<s32>& TypeList )  const;

    xhandle                     getHandleFromNetworkID  ( s32 NetworkID );
    void                        SetServerNetworkID      ( gb_component& Component, xhandle NetworkID );

protected:

    s32                         m_MessageGroupCount;        // Total Count of message groups
    xptr<type_node>             m_lComponentTypes;          // array of all the types of components
    xharray<xhandle>            m_NetworkIDToClient;        // Mapping from the network id to the client    

protected:
    friend s32 CompareTypes( const void* pA, const void* pB );
};

extern gb_component_type_mgr g_ComponentTypeMgr;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif