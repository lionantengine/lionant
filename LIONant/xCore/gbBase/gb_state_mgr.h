#ifndef STATE_MANAGER_H
#define STATE_MANAGER_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// State Manager
//////////////////////////////////////////////////////////////////////////////////
class gb_state_mgr  : public gb_base_mgr
{
public:

                                GB_MANAGER_RTTI( gb_state_mgr, gb_base_mgr );

                                gb_state_mgr        ( const char* pName );                                
                               ~gb_state_mgr        ( void ){}

    void                        setActiveState      ( gb_cloud_state& State );
    s32                         getTickCountT1      ( void ) { return m_pActiveState->getStartSyncP().getTickCount(); }
    void                        TransitionToState   ( gb_cloud_state& State ) { m_pNextActiveState = &State; }

protected:

    virtual void                onUpdate            ( void );
            void                SetupDependencyGraph( void );                

system_public:

    void                        ExecuteFrame        ( void );
    void                        ExecuteNextFrame    ( void );

system_public:

    gb_cloud_state*             m_pActiveState      = NULL;     // What is the state we are trying to run
    gb_cloud_state*             m_pNextActiveState  = NULL;     // What is the state we are trying to run
    
};

extern gb_state_mgr g_StateMgr;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif