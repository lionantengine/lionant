#ifndef GB_ENTITY_H
#define GB_ENTITY_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "gb_component.h"
#include "gb_entity_mgr.h"

/////////////////////////////////////////////////////////////////////////////////
// DECLAREATION
/////////////////////////////////////////////////////////////////////////////////
class gb_blueprint;
class gb_entity_blueprint;
class gb_entity;

/////////////////////////////////////////////////////////////////////////////////
// INTERFACE TRANSFORM
/////////////////////////////////////////////////////////////////////////////////

class i_transform
{
public:
    x_rtti_base(i_transform);

    virtual void        setupTransform      ( const xtransform& Transform )=0;
    virtual xtransform  getTransform        ( s32 Tx ) const = 0;
};

/////////////////////////////////////////////////////////////////////////////////
// ENTITY
/////////////////////////////////////////////////////////////////////////////////
class gb_entity : public gb_component
{
public:
    GB_COMPONENT_RTTI1( gb_entity, gb_component );

public:
                                        gb_entity               ( void );
                                       ~gb_entity               ( void );

    virtual void                        init                    ( void );

            void                        sendMessage             ( const gb_entity& Sender, f32 DeltaTime, gb_component_type& Type, gb_msg& Message );
            void                        addComponent            ( gb_component& Component );
            void                        delComponent            ( gb_component& Component );
            xguid                       getGuid                 ( void ) const { return m_Guid; }
            void                        setGuid                 ( xguid Guid ) { m_Guid = Guid; }                

            void                        saveEntity              ( xtextfile& File, s32 iEntity=0 );
    static  gb_entity&                  loadEntity              ( xtextfile& File );

    static  gb_entity&                  createFromType          ( xguid gEntity, const gb_component_type& Type, xguid gPresets=xguid(0) );
    
    //----------------------------------------------------------------------------
    // Messages - Handlers
protected:

	virtual void                        onUpdate                ( const gb_msg& msg );

    //----------------------------------------------------------------------------
    // System - System related functions 
protected:

            u32                         getEntityBlueprintGUID  ( void ) const;
            void                        resolveDependencies     ( void );    
            void                        initComponentTable      ( void );

    virtual void                        vUpdate                 ( void );

system_public:
            
            void                        enableServer            ( void );
            void                        enableClient            ( void );

protected:

    xguid                               m_Guid;                     // The unique id for this entity
    gb_entity_blueprint*                m_pEntityBluePrint;         // Which blue print did this entity came from. 
                                                                    // If this value is not NULL it means that this entity has been compiled.
protected:

    static class type : public gb_component_type
    {
    public:
                                    x_rtti_class1       ( gb_entity::type, gb_component_type);
                                    type                ( u32 Flags ) : gb_component_type( Flags ){}                          \
        virtual const char*         getName             ( void ) const { return "EntityTYPE"; }
                void*               getVoidedManager    ( void ) const { return &g_EntityMgr; }

    protected:

        virtual gb_component*               NewComponent            ( void ) const override                     { ASSERT(0); return NULL; }
        virtual void                        DeleteComponent         ( gb_component* pComponent ) const override { ASSERT(0); }
        virtual gb_component::presets*      NewPresets              ( void ) const  override                    { ASSERT(0); return NULL; }

    } s_Type;

protected:
    friend class gb_blueprint;
    friend class gb_entity_type;
    friend class gb_entity_blueprint;
};

/////////////////////////////////////////////////////////////////////////////////
// SIMPLE ENTITY
/////////////////////////////////////////////////////////////////////////////////

class gb_entity_simple : public gb_entity
{
    GB_COMPONENT_RTTI1( gb_entity_simple, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

protected:

    virtual void                    vSetup                  ( void ) {}
    virtual void                    vResolveDependencies    ( void ) {}
    virtual void                    onUpdate                ( const gb_msg& msg ) {}
};

/////////////////////////////////////////////////////////////////////////////////
// ENTITY MESSAGES
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

class entity_update_msg : public gb_msg
{
public:
    GB_MSG_UID(entity_update_msg);
    f32 m_TimeDelta;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif