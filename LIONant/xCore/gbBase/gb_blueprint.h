#ifndef GB_BLUEPRINT_H
#define GB_BLUEPRINT_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// FOWARD DECLARATIONS
//////////////////////////////////////////////////////////////////////////////////
struct gb_msg_map;
class  gb_entity;
class  gb_entity_type;
class  gb_blueprint;

//////////////////////////////////////////////////////////////////////////////////
// ENTITY BLUE PRINT
//////////////////////////////////////////////////////////////////////////////////
class gb_entity_blueprint
{
public:
                                        gb_entity_blueprint         ( void );
                                       ~gb_entity_blueprint         ( void );

    s32                                 decRefCount                 ( void )                { return --m_RefCount; }
    s32                                 getRefCount                 ( void )                { return m_RefCount;   }

protected:

    void                                initEntity                  ( gb_entity& Entity );
    void                                compileMsgTable             ( gb_entity& Entity );

protected:

    u32                                 m_Guid;                 // Unique ID for this blueprint entity 
    s32                                 m_RefCount;             // Reference count to know how many entities are point at this blue print
    s32                                 m_nComponents;          // Number of componets for this entity type

protected:
    friend class gb_blueprint;
    friend class gb_blueprint_manager;
    friend class gb_entity;
};

//////////////////////////////////////////////////////////////////////////////////
// BLUE PRINT
//////////////////////////////////////////////////////////////////////////////////
class gb_blueprint
{
public:
    x_rtti_base(gb_blueprint);

                                        gb_blueprint            ( void );
                                       ~gb_blueprint            ( void );
protected:

    void                                createEntities          ( xarray<gb_entity*>& Entities, const xtransform& Transform );

    void                                setFilename             ( const char* pFilename ) { m_Filename.Copy( pFilename ); }
    void                                appendEntity            ( gb_entity& Entity );
    void                                Compile                 ( void );
    void                                saveBlueprint           ( xtextfile& TextFile );

protected:

    xguid                               m_Guid;
    xstring                             m_Filename;
    xarray<gb_entity*>                  m_MasterEntities; 
	xbool								m_bFileInMemory;

protected:
    friend class gb_blueprint_manager;
};

//////////////////////////////////////////////////////////////////////////////////
// BLUE PRINT MANAGER
//////////////////////////////////////////////////////////////////////////////////
class gb_blueprint_manager
{
public:

    void                    createEntities              ( xarray<gb_entity*>& Entities, xguid gBluePrint, const xtransform& Transform );
    xguid                   getBlueprintByFilename      ( const char* pFileName );
    void                    loadBlueprint               ( const char* pFileName );
    gb_component::presets*  findPresets                 ( xguid Guid );

    //----------------------------------------------------------------------------
    // Editor functions
public:

    xguid                   createBlueprint             ( const char* pFileName );
    gb_component::presets&  createNewPresets            ( gb_component_type& CompType );
    void                    appendEntity                ( xguid gBluePrint, gb_entity& Entity );
    void                    compileBlueprint            ( xguid gBluePrint );
    void                    deleteBlueprint             ( xguid gBluePrint );
    void                    appendPresets               ( gb_component::presets& Presets );
    void                    saveBlueprints              ( const char* pFolder = "" );
    void                    savePresets                 ( const char* pFolder = "" );
    void                    loadPresets                 ( const char* pFolder = "" );

public:

    void                    init                        ( void ) {}
    void                    kill                        ( void );

protected:

    gb_blueprint*           findBlueprint               ( const char* pFileName );
    gb_blueprint*           findBlueprint               ( xguid Guid );
    gb_entity_blueprint*    findEntityBlueprint         ( u32 Guid );
    void                    setMessageTable             ( u32 gEntityBP, gb_entity& Entity );

protected:

    xarray<gb_blueprint>            m_Blueprints;
    xarray<gb_entity_blueprint>     m_EntityBlue;
    xarray<gb_component::presets*>  m_lPresets;

protected:
    friend class gb_entity;
    friend class gb_blueprint;
};

extern gb_blueprint_manager g_BlueprintMgr;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif 