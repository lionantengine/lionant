#ifndef INPUT_MGR_H
#define INPUT_MGR_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// gb_input_mgr
/////////////////////////////////////////////////////////////////////////////////

class gb_input_mgr : public gb_default_component_mgr
{
public:
                        GB_MANAGER_RTTI(gb_input_mgr, gb_default_component_mgr);

                        gb_input_mgr            ( const char* pName );
            void        setupAddVirtualDevice   ( eng_input_vdevice& VirtualDevice ) { m_lVirtualDevice[m_nVirtualDevices++] = &VirtualDevice; }
    const eng_input_vdevice&    VDevice         ( s32 iDevice ) const                { ASSERT(iDevice<m_nVirtualDevices); return *m_lVirtualDevice[iDevice]; }
    
protected:
    
    virtual void        onUpdate                ( void );
    
protected:

    xsafe_array<eng_input_vdevice*,4>    m_lVirtualDevice;
    s32                                  m_nVirtualDevices;
};

/////////////////////////////////////////////////////////////////////////////////
// GLOBALS 
/////////////////////////////////////////////////////////////////////////////////
extern gb_input_mgr g_InputMgr;

/////////////////////////////////////////////////////////////////////////////////
// gb_input_component
/////////////////////////////////////////////////////////////////////////////////

class gb_input_component : public gb_component
{
public:
    GB_COMPONENT_RTTI1( gb_input_component, gb_component );
    GB_COMPONENT_TYPEBASE( g_InputMgr, XMEM_FLAG_ALIGN_8B )

public:
                        gb_input_component      ( void );
    eng_input_vdevice&  setupGetVDevice         ( void ) { return m_VirtualDevice; }
    
protected:

    virtual void        vResolveDependencies    ( void ){}
    virtual void        vSetup                  ( void ){}
    virtual void        vUpdate                 ( void );
    virtual void        vWritePacketData        ( xbitstream& BitStream, const xvector3d& Origin );
    virtual void        vReadPacketData         ( xbitstream& BitStream, const xvector3d& Origin );

protected:

    eng_input_vdevice       m_VirtualDevice;
    
	friend class input_mgr;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif