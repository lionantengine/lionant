#ifndef GB_NET_CONNECTION_MGR_H
#define GB_NET_CONNECTION_MGR_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// NETWORK_MANAGER
/////////////////////////////////////////////////////////////////////////////////
/*
class gb_net_connection_mgr : public gb_base_mgr
{
public:
                               GB_MANAGER_RTTI( gb_net_connection_mgr, gb_base_mgr );
public:
    
    enum connection_state
    {
        CONNECTION_UNCONNECTED_NULL,
        CONNECTION_UNCONNECTED_FAIL,
        CONNECTION_TO_ROOT,
        CONNECTION_ROOT_NEGOCIATING,
        CONNECTION_ROOT_GETTING_PEERS,
        CONNECTING_TO_PEERS,
        CONNECTING_FINISH
    };

public:

	        void			    UpdateData			    ( gb_component& Component, f32 Weight, xvector3& Position, f32 Energy );
            void                UpdateOrigin            ( xhandle hNetworkPlayer, const xvector3& Pos ) { m_lNetworkPlayers(hNetworkPlayer).m_Origin = Pos; }
            void                Connect                 ( xnet_address ServerAdr );
//            connection_state    getConnectionStatus     ( void );
            xbool               PullPacket              ( s32& Header, s32& Size );

system_public:

								gb_net_connection_mgr   ( const char* pName );
    virtual void				vInit                   ( void );
    virtual void                onUpdate                ( void );

protected:

	xharray<network_player>		m_lNetworkPlayers;
    xarray<s32>                 m_lNetworkComponetTypes;    // List of network component types
    xnet_udp_mesh               m_NetworkMesh;              // Network 
    connection_state            m_ConnectionState;
    xhandle                     m_hSocket;

	friend class gb_replay_mgr;
};

extern gb_network_mgr    g_NetworkMgr;
*/

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif