#ifndef GB_COMPONENT_MANAGER_H
#define GB_COMPONENT_MANAGER_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// BASE COMPONENT MANAGER
//////////////////////////////////////////////////////////////////////////////////
class gb_base_component_mgr : public gb_base_mgr
{
public:
                        GB_MANAGER_RTTI( gb_base_component_mgr, gb_base_mgr )

                        gb_base_component_mgr   ( const char* pName ) : gb_base_mgr( pName ) {}

            void        linearAppendComponent   ( gb_component& Component );
            void        linearDeleteComponent   ( gb_component& Component );
            void        msgAppendComponent      ( gb_component& Component );
            void        msgDeleteComponent      ( gb_component& Component );
            void        msgActivateComponent    ( gb_component& Component );
            void        msgDeactivateComponent  ( gb_component& Component );
            void        msgSetServerNetworkID   ( gb_component& Component, xhandle hNetworkID );
            s32         getComponentCount       ( void ) const;

protected:

    struct delete_node
    {
        u64             m_iFrame;
        gb_component*   m_pComponent;
    };

protected:
    
            void        onDispatchMessages      ( void );
    virtual void        onUpdate                ( void )=0;
    virtual void        onClearUp               ( void );
    virtual void        onAppendComponent       ( const gb_mgr_base_msg& Msg );
    virtual void        onDeleteComponent       ( const gb_mgr_base_msg& Msg );
    virtual void        onActivateComponent     ( const gb_mgr_base_msg& Msg );
    virtual void        onDeactivateComponent   ( const gb_mgr_base_msg& Msg );
    virtual void        onSetServerNetworkID    ( const gb_mgr_base_msg& Msg );
    virtual void        vInit                   ( void );

protected:

    xarray<s32>                     m_lActiveType;
    xarray<delete_node>             m_lDelete;              // List of components that has been deleted
//    x_qt_fober_queue                m_DeleteMsgQueue;       // We have a separate delete msg queue to make sure 
                                                            // we give the lowest priority to this messages.
                                                            // this way appended messages should always happen first
                                                            // the idea is to try to avoid deleting something that is
                                                            // has not been appended yet.
};

//////////////////////////////////////////////////////////////////////////////////
// JOBLIST COMPONENT MANAGER
//////////////////////////////////////////////////////////////////////////////////
class gb_joblist_component_mgr : public gb_base_component_mgr
{
public:

    struct component_list
    {
        xharray<gb_component*>* m_plComponent;
        s32                     m_iStart;
        s32                     m_Count;
    };

    struct simple_job : public x_simple_job<1>
    {
        void            onRun       ( void );
        xarray<component_list>   m_lCompList;
    };

public:
                        GB_MANAGER_RTTI( gb_joblist_component_mgr, gb_base_component_mgr );
                        gb_joblist_component_mgr ( const char* pName );

protected:
    
    enum
    {
        MAX_COMPONENTS_PER_JOB = 30
    };

protected:

    virtual void        onAppendComponent       ( const gb_mgr_base_msg& Msg );
    virtual void        onDeleteComponent       ( const gb_mgr_base_msg& Msg );
    virtual void        onActivateComponent     ( const gb_mgr_base_msg& Msg );
    virtual void        onDeactivateComponent   ( const gb_mgr_base_msg& Msg );

    template< class T > void BuidlJobLists( xarray<T>& lJob );

protected:

    xbool          m_bDirty;
};

//////////////////////////////////////////////////////////////////////////////////
// DEFAULT COMPONENT MANAGER
//////////////////////////////////////////////////////////////////////////////////
class gb_default_component_mgr : public gb_joblist_component_mgr
{
public:
                        gb_default_component_mgr( const char* pMgrName );

protected:

    virtual void        onUpdate                ( void );

protected:

    xarray<gb_joblist_component_mgr::simple_job> m_lJob;
};

//////////////////////////////////////////////////////////////////////////////////
// INLINE
//////////////////////////////////////////////////////////////////////////////////
#ifndef GB_COMPONENT_MANAGER_INLINE_H
#include "Implementation/gb_component_mgr_inline.h"
#endif

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif