#ifndef GB_NETWORK_MGR_H
#define GB_NETWORK_MGR_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// NETWORK_MANAGER
/////////////////////////////////////////////////////////////////////////////////

class gb_network_mgr : public gb_base_mgr
{
public:
                               GB_MANAGER_RTTI( gb_network_mgr, gb_base_mgr );
public:
    
    enum connection_state
    {
        CONNECTION_UNCONNECTED_NULL,
        CONNECTION_UNCONNECTED_FAIL,
        CONNECTION_TO_ROOT,
        CONNECTION_ROOT_NEGOCIATING,
        CONNECTION_ROOT_GETTING_PEERS,
        CONNECTING_TO_PEERS,
        CONNECTING_FINISH
    };

    enum mode_flags
    {
        MODE_CLIENT = X_BIT(1),      
        MODE_SERVER = X_BIT(2),      
        MODE_RECORD = X_BIT(3),
    };

public:

	        void			    UpdateData			    ( gb_component& Component, f32 Weight, xvector3& Position, f32 Energy );
            void                UpdateOrigin            ( xhandle hNetworkPlayer, const xvector3& Pos ) { m_lNetworkPlayers(hNetworkPlayer).m_Origin = Pos; }
            void                Connect                 ( xnet_address ServerAdr );
//            connection_state    getConnectionStatus     ( void );
            xbool               PullPacket              ( s32& Header, s32& Size );
            void                InitNetwork             ( xbool isServer, s32 Port );

system_public:

								gb_network_mgr          ( const char* pName );
    virtual void				vInit                   ( void );
    virtual void                onUpdate                ( void );
            xvector3d           getOrigin               ( xhandle hNetworkPlayer ) const { return m_lNetworkPlayers(hNetworkPlayer).m_Origin; }
            void                ConvertAllToClients     ( void );
            xhandle             RegisterNetworkPlayer   ( void );
            void                PreUpdate               ( void );

protected:

	struct network_player
	{
    	xvector3d	        m_Origin;                   // Location of the player
    	xarray<xhandle>	    m_lPriority;	            // Global priorities to which data needs to be sent
	    xhandle             m_hNetworkClient;           // ID of the network player

        network_player( void )
        {
            m_Origin.Set( 0, 0, 0 );
            m_hNetworkClient.SetNull();
        }
    };

protected:

	xharray<network_player>		m_lNetworkPlayers;
    xarray<s32>                 m_lNetworkComponetTypes;    // List of network component types
    xnet_udp_mesh               m_NetworkMesh;              // Network 
    connection_state            m_ConnectionState;
    xhandle                     m_hSocket;
    u32                         m_Mode;

	friend class gb_replay_mgr;
};

extern gb_network_mgr    g_NetworkMgr;

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif