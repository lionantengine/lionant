#ifndef GB_COMPONENT_H
#define GB_COMPONENT_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// PRE-DEFINES
//////////////////////////////////////////////////////////////////////////////////
struct gb_msg;
struct gb_msg_map_full;
class gb_entity;
class gb_component_type;

//////////////////////////////////////////////////////////////////////////////////
// MACROS
//================================================================================
// GB_COMPONENT_RTTIx			- This macro creates the RTTI information for all 
//								  your components.
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// COMPONENT
//////////////////////////////////////////////////////////////////////////////////
class gb_component : public xproperty
{
public:

    typedef void (gb_component::*callback_fn)( const gb_msg& Msg );

    struct presets : public xproperty
    {
        u32         m_Type;
        xguid       m_Guid;
        s32         m_RefCount;

                        presets         ( void ){ m_RefCount=0; }
        virtual        ~presets         ( void ){}
        virtual void    vLoad           ( xtextfile& File );
        virtual void    vSave           ( xtextfile& File );
        virtual xbool   onPropQuery     ( xproperty_query& Query ) override;
        virtual void    onPropEnum      ( xproperty_enum&  Enum  ) const override;
                s32     incRefCount     ( void ) { return ++m_RefCount; }
                s32     decRefCount     ( void ) { return --m_RefCount; }
    };

    struct context : public x_qt_fober_queue
    {
                        context         ( void ) {}
        virtual        ~context         ( void ) {}
        virtual void    vTransferData   ( const context& Src ){};
    };

public:
                                        x_rtti_base(gb_component); 


                                        gb_component            ( void );
    virtual                            ~gb_component            ( void );    

            presets&                    setupNewPresets         ( void );
            const presets&              getPresets              ( void ) const { return *m_pPresets; }                                                                      \
	virtual gb_component_type&			getType					( void ) const = 0;
            gb_entity&                  getEntity               ( void ) { return *(gb_entity*)m_pComponents[0]; }
            gb_component&               getComponent            ( const gb_component_type& Type );
            void*                       findInterface           ( const xrtti& RTTI ) const;
            xbool                       isDeleted               ( void ) const { return m_qtAttrFlags.isFlagOn(QT_ATTR_DELETED); }
            xbool                       isActive                ( void ) const { return m_qtAttrFlags.isFlagOn(QT_ATTR_ACTIVE);  }
            xbool                       isClient                ( void ) const { return m_qtAttrFlags.isFlagOn(QT_ATTR_CLIENT);  }
    virtual xbool                       hasPresets              ( void ) const { return FALSE; }

    virtual void                        msgActivate             ( void );
    virtual void                        msgDeactivate           ( void );
            
protected:

            context&                    getSetupContext         ( void )            { return *m_pT[0]; }
            const context&              getReadContextTx        ( s32 Tx ) const;
    
            context&                    getWriteContextT1       ( void );
            const context&              getReadContextLatest    ( void ) const;
    
            context&                    getRawContext           ( s32 Tx )          { return *m_pT[Tx]; }   
            const context&              getRawContext           ( s32 Tx ) const    { return *m_pT[Tx]; }   

            void                        msgRegisterEventCallback  ( xstring& EventName, gb_component* pComponent, gb_component::callback_fn MessageFunc );
            void                        msgUnregisterEventCallback( xstring& EventName, gb_component* pComponent );

    virtual void                        onRegisterEventCallback   ( gb_msg& Message ) { ASSERT(0); }
    virtual void                        onUnregisterEventCallback ( gb_msg& Message ) { ASSERT(0); }

system_public:

    virtual void                        vSaveComponent          ( xtextfile& File );
    virtual void                        vLoadComponent          ( xtextfile& File );
    virtual xbool                       onPropQuery             ( xproperty_query& Query ) override;
    virtual void                        onPropEnum              ( xproperty_enum&  Enum  ) const override;
    virtual void                        vResolveDependencies    ( void )=0;
    virtual void                        vWritePacketData        ( xbitstream& BitStream, const xvector3d& Origin ){ ASSERT(0); }
    virtual void                        vReadPacketData         ( xbitstream& BitStream, const xvector3d& Origin ){ ASSERT(0); }

            void                        sendMessage             ( const gb_msg& Message );

    virtual gb_component&               getServer               ( void ) { ASSERT(0); return *(gb_component*)NULL; }
    virtual void                        SwitchToServer          ( void ) { ASSERT(0); }
    virtual void                        SwitchToClient          ( void ) { ASSERT(0); }

system_public:

    typedef gb_component_type type;
    enum message_group:u32              { MSG_GROUP_MAX = 0 };
    enum qt_attr_flags:u32
    {
        QT_ATTR_DELETED          = X_BIT(0),   // Whether this component is to be assume to be deleted
        QT_ATTR_IN_WORLD         = X_BIT(1),   // Whether this component is in the world
        QT_ATTR_ACTIVE           = X_BIT(2),   // Whether the component is active or not
        QT_ATTR_CLIENT           = X_BIT(3)    // This attribute is only for entities and is to determine whether they are running in server or client mode
    };

    typedef gb_component                __baseclass;    
    virtual void                        BuildContextBuffers     ( void )                    { ASSERT(0); }
    virtual void                        DestroyContextBuffers   ( void )                    { ASSERT(0); }
            void                        SetupContexTransferC02C1( void )                    { m_pT[1]->vTransferData( *m_pT[0] ); }

    virtual void                        MessageHandler2         ( gb_msg& msg );

    virtual void                        vSetup                  ( void )=0;
    virtual void                        vUpdate                 ( void );
    virtual void                        vInWorld                ( void ) {}

    virtual void                        DestroyPresetBuffer     ( void ) { ASSERT(0); }
    virtual void                        BuildPresetBuffer       ( void ) { ASSERT(0); }

            xhandle                     getHandle               ( void ) const { return m_Handle; }

    //--------------------------------------------------------------------------------
    // Read only variables + SYS (Variables that only the object can change in the onUpdate function.)
system_protected:

    x_qt_flags                          m_qtAttrFlags;              // Quantum Attributes for the components
    presets*                            m_pPresets;                 // Pointer to the present structure
    gb_component**                      m_pComponents;              // This is the table where all the pointers for the components are hold.
                                                                    // Note that the pointer is actually offset by 1 entry because the real zero entry
                                                                    // is where the pointer for the gb_main_msg_table_entry is. This allows any component to 
                                                                    // Directly access both the entity which should always be at index [0] and the message
                                                                    // table that it should actually be in index[-1]
                                                                    // ** Note that the Entity is incharge to manage this pointer.
    context*                            m_pT[2];                    // This is the Public data which people cam access (read only)
    xhandle                             m_Handle;                   // Used to quickly identify where in the list is the component

    friend class gb_entity;
    friend class gb_entity_mgr;
    friend class gb_blueprint;
    friend class gb_component_type;
    friend class gb_component_type_mgr;
};

//////////////////////////////////////////////////////////////////////////////////
// BASE COMPONENT TYPE
//////////////////////////////////////////////////////////////////////////////////
// This class represents a type of components.
// He in charge of creating all the components that belong to this type and hold 
// this memory.
// I get an ID to specialize this type (e.g. tank physics) of a general type (e.g. physics)
//////////////////////////////////////////////////////////////////////////////////
class gb_component_type
{
public:
    
    enum
    {
        FLAGS_DEFAULT             = 0,
        FLAGS_HAS_PACKED_DATA     = X_BIT(0), // It has data to be saved for replay or network
        FLAGS_SERVER			  = X_BIT(1), // It reads the data for replay or network
        FLAGS_CLIENT			  = X_BIT(2), // It reads the data for replay or network
        FLAGS_REALTIME_DYNAMIC    = X_BIT(3),
        FLAGS_50MS_DYNAMIC        = X_BIT(4),
        FLAGS_200MS_DYNAMIC       = X_BIT(5),
        FLAGS_DYNAMIC_MASK        = FLAGS_REALTIME_DYNAMIC | FLAGS_50MS_DYNAMIC | FLAGS_200MS_DYNAMIC,
		FLAGS_NETWORK			  = FLAGS_SERVER | FLAGS_CLIENT,
		FLAGS_ALL				  = 0xffffffff 
    };

public:
    x_rtti_base(gb_component_type);

                                        gb_component_type		( u32 Flags );
    virtual                            ~gb_component_type		( void );
    virtual u32                         getGUID                 ( void ) const { return m_Guid; }
            s32                         getMessageGroupID       ( void ) const  { return m_MessageGroupID; }
            
            s32                         getTypeIndex            ( void ) const { return m_iType; }
            u32                         getFlags                ( void ) const { return m_Flags; }
    static  gb_component_type*          getFirstType            ( void ) { return m_pFirst; }
            gb_component_type*          getNextType             ( void ) const { return m_pNext;  }

            gb_base_mgr&                getManager              ( void ) const { return gb_base_mgr::SafeCast( *(gb_base_mgr*)getVoidedManager() ); } 
    virtual gb_sync_point&              getSyncPoint            ( void ) const { return getManager().getSyncPoint(); }

            gb_component*               createComponent         ( xguid gPreset ) const;
            void                        destroyComponent        ( gb_component& Component);
            xbool                       hasPresets              ( void ) { return FALSE; }
    
    virtual gb_component_type&          getClientType           ( void ) { ASSERT(0); return *((gb_component_type*)(NULL)); }

protected:

    virtual const char*                 getName                 ( void ) const = 0;
    virtual gb_component*               NewComponent            ( void ) const = 0;
    virtual void                        DeleteComponent         ( gb_component* pComponent) const = 0;
    virtual void*                       getVoidedManager        ( void ) const = 0;
    virtual gb_component::presets*      NewPresets              ( void ) const = 0;
    
system_protected:

            void                        setupPresets            ( gb_component* pComp, xguid gPreset ) const;
            gb_component*               createComponentFromLoad ( xguid gPreset );

system_protected:

    u32                             m_Flags;                    // Flags for the type
    u32                             m_Guid;                     // A unique interger identifier which is derive from the string name
    s32                             m_MessageGroupID;           // Message group allows to reserve a slot in the message map table for a component type.
                                                                // * If you derive from this component type then the message group id will be the same across the hirarchy
                                                                //   this means that ony component of the hirarchy can be added together in the same entity.
                                                                // * This also explains why you can not have the same exact component in the entity more than ones.
                                                                // Note that numbers need to be sequencial so they need to be auto generated for safety and compackness. 
    gb_component_type*              m_pNext;                    // Next pointer in the list
    s32                             m_iType;                    // Index for the global table of types
    static gb_component_type*       m_pFirst;                   // First type in the list

protected:

    friend class gb_entity_blueprint;
    friend class gb_game_mgr;
    friend class gb_entity;
    friend class gb_component;
    friend class gb_blueprint;
    friend class gb_blueprint_manager;
    friend class gb_component_type_mgr;
    friend s32 CompareTypes( const void* pA, const void* pB );
};

//////////////////////////////////////////////////////////////////////////////////
// INCLUDE INLINES
//////////////////////////////////////////////////////////////////////////////////
#ifndef GB_COMPONENT_INLINE_H
#include "Implementation\gb_component_inline.h"
#endif

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif  