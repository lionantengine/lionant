//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Base Component
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

gb_component::gb_component( void ) : 
    m_pComponents(NULL)
{
    m_pT[0] = m_pT[1] = NULL;
    m_pPresets = NULL;
    m_qtAttrFlags.setFlags( QT_ATTR_ACTIVE );
    m_Handle.SetNull();
}

//-------------------------------------------------------------------------------

gb_component::~gb_component( void )
{
}

//-------------------------------------------------------------------------------

void gb_component::MessageHandler2( gb_msg& Msg )
{
	(this->*Msg.m_MessageFunc)(Msg);
}

//-------------------------------------------------------------------------------

gb_component& gb_component::getComponent( const gb_component_type& Type )
{
	s32 i=0;
	for( ; m_pComponents[i]; i++ )
	{
		if( &m_pComponents[i]->getType() == &Type )
			break;
	}

	ASSERT(m_pComponents[i]);
	return *m_pComponents[i];
}

//-------------------------------------------------------------------------------

void* gb_component::findInterface( const xrtti& RTTI ) const
{
    // If you have this interface why are you asking the system to get it for you?
//    ASSERT( getObjectRTTI().isKindOf( RTTI ) == FALSE );

    // Search throw all the components in the list
    if( m_pComponents )
    {
        for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
        {
            void* pPtr = (*pCom)->SafeCastInterface( RTTI );
            if( pPtr )
                return pPtr;
        }
    }

    return NULL;
}

//-------------------------------------------------------------------------------

void gb_component::msgActivate( void )
{
    if( isActive() ) return;
    gb_base_mgr& Mgr = getType().getManager();

    gb_base_component_mgr& ComMgr = gb_base_component_mgr::SafeCast( Mgr );
    ComMgr.msgActivateComponent( *this );
}

//-------------------------------------------------------------------------------

void gb_component::msgDeactivate( void )
{
    if( isActive() == FALSE ) return;
    gb_base_mgr& Mgr = getType().getManager();

    gb_base_component_mgr& ComMgr = gb_base_component_mgr::SafeCast( Mgr );
    ComMgr.msgDeactivateComponent( *this );
}

//-------------------------------------------------------------------------------

void gb_component::sendMessage( const gb_msg& Msg )
{
    //
    // If delta time is less than the delta time we are trying to increase this frame by 
    // then the messages should get executed this frame. However if the delta time is larger 
    // than the message should execute the following frame.
    if( Msg.m_DeltaTime <= g_GameMgr.getDeltaTime() )
    {
        // All messages sent to this component for T0 must happen before the sync point is trigger
        // If this does not happen changes are that you have not setup your graph right.
        // The reasons why we must inforce this is that we need to set a limit on the end of procesing
        // messages from the interval T0T1. The limit here is the syncpoint been trigger... if fact it
        // is a little bit worse since it is the actual manager begging of the execution.
        ASSERT( x_FlagIsOn( Msg.m_Flags, gb_msg::FLAGS_ONUPDATE_MESSAGE) || !getType().getManager().hasUpdatedThisFrame() );

        // This are the messages been set to the T0T1 interval
        context& Write = *((context*)(&getReadContextTx(0)));
        
        gb_msg& unMsg = const_cast<gb_msg&>( Msg );
        Write.push( &unMsg );
    }
    else
    {
        // These are the messages been set to T1Tx interval
        context& Write = getWriteContextT1();
        
        gb_msg& unMsg = const_cast<gb_msg&>( Msg );
        Write.push( &unMsg );
    }
}

//-------------------------------------------------------------------------------

void gb_component::vUpdate( void )
{
    // Get Component Type
    const gb_component_type& CompType = getType();

    // Get context
    s32      T0             = CompType.getSyncPoint().getTxRead(0);
    context& ReadContext    = *(context*)&getRawContext(T0);
    context& WriteContext   = *(context*)&getRawContext(1-T0);

    // We switched the buffer, but we haven't transfered the data yet.
    WriteContext.vTransferData( ReadContext );

    //
    // Handle normal messages first
    //
    const gb_msg* pMsg;
    while( (pMsg = (gb_msg*)ReadContext.pop()) )
    {
        // Handle the message
        if (pMsg)
        {
            //MessageHandler( pMessageMap[iMsg], *pMsg );
			MessageHandler2( *(gb_msg*)pMsg );

            // Should we errase it?
            pMsg->m_RefCount--;
            if( pMsg->m_RefCount <= 0 )
            {
                if( !x_FlagIsOn( pMsg->m_Flags, gb_msg::FLAGS_DO_NOT_DELETE) )
                {
                    x_delete(pMsg);
                }
            }
        }
    }
}

//-------------------------------------------------------------------------------

const gb_component::context& gb_component::getReadContextTx( s32 Tx ) const
{
    const s32 d = getType().getSyncPoint().getTxRead(Tx);
    return getRawContext( d );
}

//-------------------------------------------------------------------------------

gb_component::context& gb_component::getWriteContextT1( void )
{
    const s32 d = getType().getSyncPoint().getT1Write();
    return getRawContext( d );
}

//-------------------------------------------------------------------------------

const gb_component::context& gb_component::getReadContextLatest( void ) const
{
    const s32 d = getType().getSyncPoint().getLatestReadTx();
    return getRawContext( d );
}

//------------------------------------------------------------------------------

void gb_component::vSaveComponent( xtextfile& File )
{
    u32 Flags=0;
    File.WriteField("Flag:h", Flags);
}

//------------------------------------------------------------------------------

void gb_component::vLoadComponent( xtextfile& File )
{
    u32 Flags;
    File.ReadField("Flag:h", &Flags);
}

//------------------------------------------------------------------------------

xbool gb_component::onPropQuery( xproperty_query& Query )
{
    if( Query.Scope( "Base") )
    {
        u32 Flags=0;
        if( Query.Var( "Flags" ) && g_PropBool.Query( Query, Flags ) )
            return TRUE;
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------

void gb_component::onPropEnum( xproperty_enum&  Enum  ) const
{
    const s32 iScope = Enum.AddScope( "Base", "Base properties for the presets");
    
    g_PropBool.Enum( Enum, iScope, "Flags", "????????");
}


//------------------------------------------------------------------------------

gb_component::presets& gb_component::setupNewPresets( void )
{
    presets& Presets = g_BlueprintMgr.createNewPresets( getType() );
    m_pPresets = &Presets;
    return Presets;
}

//------------------------------------------------------------------------------

void gb_component::msgRegisterEventCallback( xstring& EventName, gb_component* pComponent, gb_component::callback_fn MessageFunc )
{
    gm_msg_register_event* pMsg = x_new( gm_msg_register_event, 1, XMEM_FLAG_ALIGN_16B );
    pMsg->m_CallBack   = MessageFunc;
    pMsg->m_EventName  = EventName;
    pMsg->m_pComponent = pComponent;

    pMsg->setupSystemVars( gb_component::callback_fn(&gb_component::onRegisterEventCallback), 0 );

    sendMessage( *pMsg );
}

//------------------------------------------------------------------------------

void gb_component::msgUnregisterEventCallback( xstring& EventName, gb_component* pComponent )
{
    gm_msg_register_event* pMsg = x_new( gm_msg_register_event, 1, XMEM_FLAG_ALIGN_16B );
    pMsg->m_CallBack   = NULL;
    pMsg->m_EventName  = EventName;
    pMsg->m_pComponent = pComponent;

    pMsg->setupSystemVars( gb_component::callback_fn(&gb_component::onUnregisterEventCallback), 0 );

    sendMessage( *pMsg );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// COMPONENT PRESETS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

void gb_component::presets::vLoad( xtextfile& File )
{
    File.ReadField("Guid:g", &m_Guid );
}

//------------------------------------------------------------------------------

void gb_component::presets::vSave( xtextfile& File )
{
    File.WriteField("Guid:g", m_Guid.m_Guid );
}

//------------------------------------------------------------------------------

xbool gb_component::presets::onPropQuery( xproperty_query& Query )
{
    if( Query.Scope( "Base") )
    {
        if( Query.Var( "Guid" ) && g_PropGuid.Query( Query, m_Guid ) )
            return TRUE;
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------

void gb_component::presets::onPropEnum( xproperty_enum&  Enum  ) const
{
    const s32 iScope = Enum.AddScope( "Base", "Base properties for the presets");

    g_PropGuid.Enum( Enum, iScope, "Guid", "This is the GUID for this preset");
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

gb_component_type*  gb_component_type::m_pFirst             = NULL;

//------------------------------------------------------------------------------

gb_component_type::gb_component_type( u32 Flags )
{ 
    //
    // Link our type in the chain
    //
    m_Flags             = Flags;
    m_MessageGroupID    = -1;
    m_pNext             = m_pFirst;
    m_pFirst            = this;
}

//------------------------------------------------------------------------------

gb_component_type::~gb_component_type( void )
{
}


//------------------------------------------------------------------------------

void gb_component_type::setupPresets( gb_component* pComp, xguid gPreset ) const
{
    if( pComp->hasPresets() )
    {
        gb_component::presets* pPresets;
        if( !gPreset.IsNull() )
        {
            pPresets = g_BlueprintMgr.findPresets( gPreset );
            ASSERT(pPresets);
        }
        else
        {
            // Try to load the default presets
            gPreset.m_Guid = getGUID();
            pPresets = g_BlueprintMgr.findPresets( gPreset );
            if( pPresets == NULL )
            {   
                // Time to create the default presets
                pPresets = NewPresets();

                // Initialize some of the key variables
                pPresets->m_Type        = getGUID();
                pPresets->m_Guid        = gPreset;
                pPresets->m_RefCount    = 0;
                g_BlueprintMgr.appendPresets( *pPresets );
            }
        }

        // Set the presets into the component
        pComp->m_pPresets = pPresets;
        pComp->m_pPresets->incRefCount();
    }
    else
    {
        pComp->m_pPresets = NULL;
    }
}

//------------------------------------------------------------------------------

gb_component* gb_component_type::createComponent( xguid gPreset ) const
{
    gb_component* pComp = NewComponent();

    //
    // We always need to create the context buffers
    //
    pComp->BuildContextBuffers();

    //
    // Setup the presets
    //
    setupPresets( pComp, gPreset );

    //
    // Call vSetup because we are not loading the object and so we must have some defaults
    //
    pComp->vSetup();

    return pComp;
}

//------------------------------------------------------------------------------

gb_component* gb_component_type::createComponentFromLoad( xguid gPreset )
{
    gb_component* pComp = NewComponent();

    // We always need to create the context buffers
    pComp->BuildContextBuffers();

    //
    // Setup the presets
    //
    setupPresets( pComp, gPreset );

    //
    // Not need to call: pComp->vSetup(); Seems all the parameters will be loaded 
    //

    return pComp;
}

//------------------------------------------------------------------------------

void gb_component_type::destroyComponent( gb_component& Component)
{
    // Okay make sure he looses all hid context buffers
    Component.DestroyContextBuffers();

    // Count down the presets
    if( Component.m_pPresets ) Component.m_pPresets->decRefCount();

    // Now we should be able to release his memory
    DeleteComponent( &Component );
}

