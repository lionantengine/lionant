//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////

gb_game_mgr g_GameMgr("GameMgr");

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// GAME MANAGER
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
gb_game_mgr::gb_game_mgr( const char* pName ) 
    : gb_base_mgr( pName )
    , m_IsRunning(FALSE)
    , m_IsShutdown(FALSE)
    , m_IsDirty(FALSE)
    , m_iFrameNumber( 0 )
{
}

//-------------------------------------------------------------------------------

void gb_game_mgr::vInit( void )
{
    //
    // Initialize the base system
    //
    gb_base_mgr::vInit();

    //
    // Make sure that the scheguler has the right number of workers
    // 
    g_Scheduler.Init();
}


//------------------------------------------------------------------------------- 

void gb_game_mgr::ComputeDeltaTime( void )
{
    //
    // Get the time
    //
    m_LastFrameTime    = m_CurrentFrameTime;
    m_CurrentFrameTime = x_GetTimeSec();

    f32 DeltaTime = f32(m_CurrentFrameTime - m_LastFrameTime);
    
    //
    // Align to 1/60 second
    //
    f32 Count  = DeltaTime / (1/60.0f);
    s32 iCount = s32(Count);

    if( iCount == 0 ) 
    {
        m_CurrentDeltaTime = DeltaTime;
    }
    else
    {
        // If we run too slow peg time to 15fps
        if( DeltaTime > 5/60.0f )
        {

            m_CurrentDeltaTime = 5/60.0f;
        }
        else
        {
            m_CurrentDeltaTime = (iCount/60.0f);
        }

        f32 LeftOver = DeltaTime - m_CurrentDeltaTime;
        m_CurrentFrameTime -= LeftOver;
    }

    // Keep a count of which frame we are computing
    m_iFrameNumber += iCount;
}

//-------------------------------------------------------------------------------

void gb_game_mgr::onStart( void )
{
    ASSERT( m_IsRunning == FALSE );

    //
    // We are going to run so get ready
    //
    m_IsRunning = TRUE;

    //
    // Start the next frame as a 1/60fps
    //
    m_CurrentFrameTime = x_GetTimeSec() - 1/60.0f;

    //
    // Ok lets start running
    // Now we can send this job into the quantum world
    //
    g_StateMgr.ExecuteFrame();
}

//-------------------------------------------------------------------------------
// This is the job
void gb_game_mgr::onUpdate( void )
{
   // static int J=0;
   // printf("[gm:%d]",J++);
   // ASSERT( g_order == 5 ); g_order = 6;

    //
    // Give the last chance to all the manager to handler their messages for this frame
    // so that we can start the new frame fresh and upto date
    // TODO: This could be done in parallel.
    //
    for( gb_base_mgr* pNext = s_pGMLManagerList; pNext ; pNext = pNext->m_pGMLNext )
    {
        // As long as the manager is not ine the beging sync point of in the end sync point
        // we will ask them to run their message update function
        if( pNext->m_pSyncPoint != m_pSyncPoint &&              // End Sync Point
            pNext->m_pSyncPoint != g_StateMgr.m_pSyncPoint &&   // Start Sync Point
            pNext->m_pSyncPoint != g_NetworkMgr.m_pSyncPoint )  // Post-end Sync Point 
        {
            pNext->onDispatchMessages();
        }
    }

    //
    // If we are not running then just stop completly
    //
    if( m_IsRunning == FALSE )
        return;

    //
    // Execute next loop other wise when we finish this one
    //
    g_StateMgr.ExecuteNextFrame();
}

//-------------------------------------------------------------------------------
void gb_game_mgr::onStop( void )
{
    m_IsRunning = FALSE;
}

//-------------------------------------------------------------------------------

void gb_game_mgr::msgStop( void )
{
    if( m_IsRunning )
    {
        gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, 0 );
        ASSERT(pMsg);
        pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_game_mgr::onStop);
        sendMessage( *pMsg );
    }
}

//-------------------------------------------------------------------------------

void gb_game_mgr::msgStart( void )
{
    // If it is running the we must consider this as a message
    if( m_IsRunning )
    {
        gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, 0 );
        ASSERT(pMsg);
        pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_game_mgr::onStart);
        sendMessage( *pMsg );
    }
    else
    {
        // If we are not running then there is not really message pump
        // so we call it directly.
        onStart();
    }
}

