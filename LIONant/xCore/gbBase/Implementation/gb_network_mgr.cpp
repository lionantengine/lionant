#include "gb_base.h"
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////

gb_network_mgr g_NetworkMgr("NetworkMgr");

//////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------

gb_network_mgr::gb_network_mgr( const char* pName ) :
    gb_base_mgr( pName )
{
    m_hSocket.SetNull();
}

//--------------------------------------------------------------------------------

void gb_network_mgr::vInit( void )
{
    //
    // Initialize the base stuff
    //
    gb_base_mgr::vInit();

    //
    // get all the components types which are network
    //
    g_ComponentTypeMgr.getAllNetworkTypes( m_lNetworkComponetTypes );
}

//--------------------------------------------------------------------------------

void gb_network_mgr::InitNetwork( xbool isServer, s32 Port )
{
    //
    // Create a socket to talk to the outside world
    //
    m_hSocket = m_NetworkMesh.OpenSocket( Port );
}

//--------------------------------------------------------------------------------

xhandle gb_network_mgr::RegisterNetworkPlayer( void )
{
    xhandle hNetworkPlayer;
    m_lNetworkPlayers.append(hNetworkPlayer);
    return hNetworkPlayer;
}

//--------------------------------------------------------------------------------

void gb_network_mgr::UpdateData( gb_component& Component, f32 Weight, xvector3& Position, f32 Energy )
{
    gb_component_type_mgr::network_info& NetworkInfo = g_ComponentTypeMgr.getNetworkInfo( Component.getHandle() ); 
    for( s32 p=0; p<m_lNetworkPlayers.getCount(); p++ )
    {
        network_player& Pl          = m_lNetworkPlayers[p];
        f32&            Priority    = NetworkInfo.m_Priority[p];
        
        // Make the distance be relevant to the priorities
        xvector3 V1     = Position - Pl.m_Origin;
        f32 DistanceSqr = 1.0f/x_Max( 1.0f, V1.Dot( V1 ) );
        
        // The fact that we call this function we will give some priority
        Priority += (DistanceSqr + Energy) * Weight; 
    }
}

//--------------------------------------------------------------------------------
static
s32 CompareObj( const void* pA, const void* pB )
{
    xhandle hA = *(xhandle*)pA;
    xhandle hB = *(xhandle*)pB;

    gb_component_type_mgr::network_info& NIA = g_ComponentTypeMgr.getNetworkInfo( hA ); 
    gb_component_type_mgr::network_info& NIB = g_ComponentTypeMgr.getNetworkInfo( hB ); 
    
    if( NIB.m_Priority[0] < NIA.m_Priority[0] ) return -1;
    return NIB.m_Priority[0] > NIA.m_Priority[0];
}

//--------------------------------------------------------------------------------

void gb_network_mgr::onUpdate( void )
{
   // ASSERT( g_order == 6 ); g_order = 7;

    //
    // Add all the active objects into the priority list
    //
    for( s32 p=0; p<m_lNetworkPlayers.getCount(); p++ )
    {
        network_player& Pl = m_lNetworkPlayers[p];

        //
        // Collect all the instances that are active
        //
        Pl.m_lPriority.DeleteAllNodes();
        for( s32 i=0; i<m_lNetworkComponetTypes.getCount(); i++ )
        {
            s32                     iType      = m_lNetworkComponetTypes[i];
            xharray<gb_component*>& lComponent = g_ComponentTypeMgr.GetComponentList( iType );
            s32                     nActive    = g_ComponentTypeMgr.GetActiveCount( iType );
            
            for( s32 j=0; j<nActive; j++ )
            {
                gb_component& Comp = *lComponent[j];
                Pl.m_lPriority.append() = Comp.getHandle();

                // Reduce the priority base on time
                //gb_component_type_mgr::network_info& NIA = g_ComponentTypeMgr.getNetworkInfo( Comp.getHandle() );
                //NIA.m_Priority[p] += DeltaTime;
            }
        }

        //
        // Sort the list here
        //
        if( Pl.m_lPriority.getCount() > 0 )
        {
            x_qsort( &Pl.m_lPriority[0], Pl.m_lPriority.getCount(), sizeof(xhandle), CompareObj );
        }
    }

   
    //
    // Ok now we should send the data to all our players
    //
    if( 1 )
    {
        for( s32 p=0; p<m_lNetworkPlayers.getCount(); p++ )
        {
            network_player&         Pl              = m_lNetworkPlayers[p];
	        const xarray<xhandle>&  PriorityList    = Pl.m_lPriority;
            xbitstream              BitStream;
            s32                     nComponentsPacked = 0;

            // Set the bit stream to be packing
            BitStream.setMode( TRUE );

	        for( s32 i=0; i<PriorityList.getCount(); i++ )
	        {
                xhandle         hComponent = PriorityList[i];
		        gb_component&   Component  = g_ComponentTypeMgr.GetComponentFromHandle( hComponent );
        		
		        // This component should be a server since servers are the only things
		        // that should be sending information from the network
		        ASSERT( x_FlagIsOn( Component.getType().getFlags(), gb_component_type::FLAGS_SERVER ) );

                // Lets pack him
                BitStream.PackFlags( g_ComponentTypeMgr.getNetworkID( hComponent ).m_Handle, 32 );
                Component.vWritePacketData( BitStream, Pl.m_Origin );

                // set the priority to zero
                gb_component_type_mgr::network_info& NIA = g_ComponentTypeMgr.getNetworkInfo( hComponent );
                NIA.m_Priority[ m_lNetworkPlayers.GetHandleByIndex(p).m_Handle ] = x_frand();

                // Okay we count this guy
                nComponentsPacked++;
                if( nComponentsPacked > 12 ) break;
	        }

            //
            // We send the network data here
            //

            // There can be entities that are ment to be sent over the network
            // without specting to recive any data. This is the case of the Player 
            if( Pl.m_hNetworkClient.isValid() )
            {
                xptr<xbyte> Buffer;
                s32         Length;         
                BitStream.getResult( Buffer, Length );

                // 
                // Create final packet
                //
                xptr<xbyte> Packet;
                const s32   HeaderSize = sizeof(s32)*5;   
                Packet.Alloc( Length + HeaderSize );

                // Build the packet Header
                *((u32*)&Packet[4*0]) = 1001; 
                *((u32*)&Packet[4*1]) = nComponentsPacked; 
                *((u32*)&Packet[4*2]) = *((u32*)&Pl.m_Origin.m_X);
                *((u32*)&Packet[4*3]) = *((u32*)&Pl.m_Origin.m_Y);
                *((u32*)&Packet[4*4]) = *((u32*)&Pl.m_Origin.m_Z);

                // Copy the data needed to be sent to the client
                Packet.Copy( HeaderSize, Buffer, 0, Length );

                // Send the packet to the client
                m_NetworkMesh.getClient( Pl.m_hNetworkClient ).SendNonreliablePacket( m_hSocket, &Packet[0], Packet.getCount() );
            }
        }
    }
    else
    {
        if( g_ReplayMgr.isRecording() )
        {
            g_ReplayMgr.RecordFrame();
        }
    }
}

//--------------------------------------------------------------------------------

void gb_network_mgr::ConvertAllToClients( void )
{

}

//--------------------------------------------------------------------------------

void gb_network_mgr::PreUpdate( void )
{
    //
    // Update the network
    // 
 //   m_Connection.Update();

    //
    // Have the replay manager update all its stuff
    //
    if( 0 )
    {
        xhandle         hClient;
        s32             AllocSize   = 1024;
        xptr<xbyte>     xPacket;

        // Alloc the packet
        xPacket.Alloc( AllocSize );

        //
        // Recive information
        //
        for( s32 Size = AllocSize; m_NetworkMesh.getSocketPackets( m_hSocket, &xPacket[0], Size, hClient ); Size = AllocSize )
        {
            xnet_udp_client& Client = m_NetworkMesh.getClient( hClient );

            // Handle disconnections
            if( Size == -1 )
            {
                printf( "Client[%s] Has timeout and disconnected\n", (const char*)Client.getAddress().GetStrAddress() );
                m_NetworkMesh.DeleteClient( hClient );
                continue;
            }

            // Handle new clients connecting to the system
            if( Client.getClientType() == xnet_udp_client::TYPE_UNKOWN )
            {
                //
                // TODO: Add a callback to the game world to see if we should add this guys as 
                //       a new player in our game
                //
                printf( "Client[%s] Has been added to the system\n", (const char*)Client.getAddress().GetStrAddress() );
                Client.CertifyClient();
                m_lNetworkPlayers[0].m_hNetworkClient = hClient;
                continue;
            }

            //
            // TODO: add the network callback here
            //
        

            // We only deal with a particular type of packet
            if( *((u32*)&xPacket[4*0]) != 1001 ) continue;

            // We got a good packet lets decoded
            xvector3d        Origin;
            s32              nComponents;
            const s32        HeaderSize = sizeof(s32)*5;   

            // Get how many components we need to update
            nComponents = *((u32*)&xPacket[4*1]);

            // Copy the 3 components of the origion
            Origin = *((xvector3d*)&xPacket[4*2]);

            // Ok now lets decode all the stream
            xhandle     hNetworkID;
            xbitstream  Bitstream;
            xptr<xbyte> BitData;

            BitData.Alloc(Size-HeaderSize);
            BitData.Copy(0,xPacket,HeaderSize,Size-HeaderSize);

            Bitstream.setMode(FALSE);
            Bitstream.setPackData( BitData, Size-HeaderSize );

            for( s32 i=0; i<nComponents; i++ )
            {
                hNetworkID.m_Handle = Bitstream.UnpackFlags32( 32 );
                gb_component& Comp  = g_ComponentTypeMgr.GetComponentFromNetworkID( hNetworkID );
		        Comp.vReadPacketData( Bitstream, Origin );
            }
        }
    }
    else
    {
        g_ReplayMgr.PlayBack();
    }


    //
    // Pull packets
    //
    /*
    xnet_connection&    Connection = m_lNetworkPlayers[i].m_Connection;
    s32                 Header;

    // TODO: Must skip local player

    while( XNET_HEADER_INVALID != (Header = Connection.PullNextHeader()) )
    {
        static char PacketData[1024];
        s32         Size = Connection.ReceiveSize();

        ASSERT( Size < sizeof(PacketData) );
        
        // Get packet data
        Connection.ReceivedPacket( Header, &PacketData, Size );

        if( Header == XNET_HEADER_HANDSHAKE )
        {
            // check whether is the correct handshake
            // Add new client to the list
        }
        else if( Header == XNET_HEADER_ACK )
        {
            xnet_ack_receipt& Receipt = *(xnet_ack_receipt*)&PacketData;

            if( Receipt.m_AckHeader == XNET_HEADER_HANDSHAKE )
            {
                // OK we are ready to start working
            }
            else
            {
                // Something else?
            }
        }
        else if( Header == PACKET_REQUEST_CLIENT_LIST )
        {
            // Send update list
        }
        else if( Header == PACKET_ADDCLIENT_LIST )
        {
            // Update the client list
        }
        else if( Header == PACKET_DATA )
        {
            // Deal with all the incoming data here
        }
    }

    //
    // We are done processing the network so let it flush all packets out
    //
    Connection.FlushAll();
    */
}

//--------------------------------------------------------------------------------

void gb_network_mgr::Connect( xnet_address ServerAdr )
{
    char    Hello[] ="Hello World!!!";

    // Create a client
    xhandle hClientID = m_NetworkMesh.CreateClient( ServerAdr );

    // We are going to certify the server
    m_NetworkMesh.getClient( hClientID ).CertifyClient();

    // With this the server should open the door for us. 
    m_NetworkMesh.getClient( hClientID ).SendReliablePacket( m_hSocket, Hello, x_strlen(Hello)+1 );
}

//--------------------------------------------------------------------------------
/*
connection_state gb_network_mgr::getConnectionStatus( void )
{
    return m_State;
}
*/