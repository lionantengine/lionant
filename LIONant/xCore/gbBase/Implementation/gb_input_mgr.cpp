#include "gb_base.h"

#ifdef TARGET_PC
#include "windows.h"
#endif

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// GLOBALS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

gb_input_mgr g_InputMgr("InputMgr");


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// INPUT MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

gb_input_mgr::gb_input_mgr( const char* pName ) :
    gb_default_component_mgr( pName )
{
    // We let the main thread deal always with the input because most
    // systems such osx, windows, android, etc likes that
    setupAffinity( x_base_job::AFFINITY_MAIN_THREAD );
}

//-------------------------------------------------------------------------------

void gb_input_mgr::onUpdate( void )
{
    eng_context& DisplayContext = eng_GetCurrentContext();
    if( !DisplayContext.HandleEvents() )
    {
        //TODO: We should tell the system that we want to quit
    }

    //
    // Update the virtual devices
    //
    for( s32 i=0; i<m_nVirtualDevices; i++ )
    {
        eng_input_vdevice* pDevice = m_lVirtualDevice[i];
        if( pDevice ) pDevice->Update();
    }
    
    gb_default_component_mgr::onUpdate();
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// INPUT COMPONENT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE(gb_input_component, 
    gb_component_type::FLAGS_REALTIME_DYNAMIC |
    gb_component_type::FLAGS_HAS_PACKED_DATA )

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// INPUT DEVICE MAPPING
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------

gb_input_component::gb_input_component( void )
{

}

//-------------------------------------------------------------------------------

void gb_input_component::vUpdate( void )
{
 //   eng_context& DisplayContext = eng_GetCurrentContext();
//    if( !DisplayContext.HandleEvents() )
    {
        //TODO: We should tell the system that we want to quit
    }
    
    /*
    //
    // Update all the logical structures
    //
    if( getEntity().isClient() == FALSE )
    {
        if( 1 ) //focus )
        {
            for( s32 j=0; j<m_lPhysical[0].getCount(); j++)
            {
                physical& Physical = m_lPhysical[0][j];
                
                if( Physical.m_GadgetID > INPUT_KBD__DIGITAL &&
                    Physical.m_GadgetID < INPUT_KBD__END )
                {
                    logical& Logical = m_lLogical( Physical.m_hLogical );

                    Logical.m_IsValue  = (f32)eng_InputIsKeyPress ( Physical.m_GadgetID - INPUT_KBD__BEGIN );
                    Logical.m_WasValue = (f32)eng_InputWasKeyPress( Physical.m_GadgetID - INPUT_KBD__BEGIN );

                }
                else if( Physical.m_GadgetID > INPUT_MOUSE__BEGIN &&
                         Physical.m_GadgetID < INPUT_MOUSE__END )
                {
                    // Not support for now
                    ASSERT(0);
                }
            }
        }
        else
        {
            for( s32 j=0; j<m_lLogical.getCount(); j++)
            {
                logical& Logical = m_lLogical[j];
                Logical.m_IsValue      = 0;
                Logical.m_WasValue     = 0;
            }
        }
    }
     */

    //
    // Handle any of our messages
    //
    gb_component::vUpdate();
}

//-------------------------------------------------------------------------------

void gb_input_component::vWritePacketData( xbitstream& BitStream, const xvector3d& Origin )
{
    /*
    for( s32 j=0; j<m_lLogical.getCount(); j++)
    {
            logical& Logical = m_lLogical[j];
            BitStream.PackFloat( Logical.m_IsValue, 0, 1 );
            BitStream.PackFloat( Logical.m_WasValue, 0, 1 );
    }
     */
}

//-------------------------------------------------------------------------------

void gb_input_component::vReadPacketData( xbitstream& BitStream, const xvector3d& Origin )
{
    /*
    for( s32 j=0; j<m_lLogical.getCount(); j++)
    {
            logical& Logical = m_lLogical[j];
            Logical.m_IsValue  = BitStream.UnpackFloat( 0, 1, 15 );
            Logical.m_WasValue = BitStream.UnpackFloat( 0, 1, 15 );
    }
     */
}
