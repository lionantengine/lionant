//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////

gb_component_type_mgr g_ComponentTypeMgr;

//////////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE MANAGER
//////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------

gb_component_type_mgr::gb_component_type_mgr( void )
{
    m_MessageGroupCount = 0;
}

//--------------------------------------------------------------------------------

s32 CompareTypes( const void* pA, const void* pB )
{
    const gb_component_type& A = *(((const gb_component_type_mgr::type_node*)pA)->m_pType);
    const gb_component_type& B = *(((const gb_component_type_mgr::type_node*)pB)->m_pType);

    // base on been dynamic
    const xbool bADynamic = A.m_Flags&gb_component_type::FLAGS_DYNAMIC_MASK;
    const xbool bBDynamic = B.m_Flags&gb_component_type::FLAGS_DYNAMIC_MASK;

    if( bADynamic < bBDynamic ) return -1;
    if( bADynamic > bBDynamic ) return 1;

    // Base on the mgr
    if( &A.getManager() < &B.getManager() ) return -1;
    if( &A.getManager() > &B.getManager() ) return  1;

    // base on message id
    if( A.m_MessageGroupID < B.m_MessageGroupID ) return -1;
    return A.m_MessageGroupID > B.m_MessageGroupID;
}


//--------------------------------------------------------------------------------

void gb_component_type_mgr::vInit( void )
{
    //
    // Compare and assign guids and ids
    //
    for( gb_component_type* pTop = gb_component_type::getFirstType(); pTop ; pTop = pTop->m_pNext )
    {
        //
        // Compare with other types
        //
        for( gb_component_type* pBottom = pTop->m_pNext; pBottom; pBottom = pBottom->m_pNext )
        {
            // If we share the same hirarchy we must share the same MessageGroupID
            if( pTop->getObjectRTTI().isKindOf( pBottom->getObjectRTTI() ) ||
                pBottom->getObjectRTTI().isKindOf( pTop->getObjectRTTI() ))
            {
                if( pTop->m_MessageGroupID == -1 )
                {
                    if( pBottom->m_MessageGroupID == -1 )
                    {
                        pTop->m_MessageGroupID = m_MessageGroupCount++;
                    }
                    else
                    {
                    
                        pTop->m_MessageGroupID = pBottom->m_MessageGroupID;
                    }
                }

                pBottom->m_MessageGroupID = pTop->m_MessageGroupID;
            }
        }

        //
        // Give a unqiue messageGroupdID if we don't already have one
        //
        if( pTop->m_MessageGroupID == -1 )
            pTop->m_MessageGroupID = m_MessageGroupCount++;
    } 

    //
    // Setup the GUID for everyone
    //
    s32 TotalTypes = 0;
    for( gb_component_type* pTop = gb_component_type::getFirstType(); pTop ; pTop = pTop->m_pNext )
    {
        // Count the types
        TotalTypes++;

        pTop->m_Guid = x_strHash(pTop->getName());

        for( gb_component_type* pBottom = gb_component_type::getFirstType(); pBottom != pTop; pBottom = pBottom->m_pNext )
        {
            ASSERT( pTop->m_Guid != pBottom->m_Guid );
        }
    }

    //
    // Insert all the types into the array
    //
    m_lComponentTypes.New( TotalTypes );
    for( gb_component_type* pTop = gb_component_type::getFirstType(); pTop ; pTop = pTop->m_pNext )
    {
        m_lComponentTypes[--TotalTypes].m_pType = pTop;            
    }

    //
    // Sort all the types
    //
    x_qsort( (void*)&m_lComponentTypes[0], m_lComponentTypes.getCount(), sizeof(type_node), CompareTypes );

    //
    // Assign all the type ids
    //
    for( s32 i=0; i<m_lComponentTypes.getCount(); i++ )
    {
        m_lComponentTypes[i].m_pType->m_iType = i;
    }

    //
    // Set some of the flags
    //
    for( s32 i=0; i<m_lComponentTypes.getCount(); i++ )
    {
        const gb_component_type& CompType = *m_lComponentTypes[i].m_pType;

        // Check whether this guy is a network guy...
        if( x_FlagsAreOn( CompType.getFlags(), gb_component_type::FLAGS_SERVER|gb_component_type::FLAGS_HAS_PACKED_DATA ) )
        {
            m_lComponentTypes[i].m_bNetwork = TRUE;
        }
        else
        {
            m_lComponentTypes[i].m_bNetwork = FALSE;
        }
    }
}

//--------------------------------------------------------------------------------

xhandle gb_component_type_mgr::getNetworkID( xhandle hComponent )
{
    s32             iType           = hComponent.m_Handle >> 24;
    s32             iHandle         = hComponent.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];

    return TypeNode.m_lNetworkInfo[iHandle].m_hNetworkID;
}

//--------------------------------------------------------------------------------

void gb_component_type_mgr::ActivateComponent( gb_component& Component )
{
    ASSERT( Component.m_Handle.isValid() );

    // If it is already active there is nothing to do
    if( Component.isActive() )
    {
#ifdef X_DEBUG
        xhandle         hComp;
        s32             iType           = Component.m_Handle.m_Handle >> 24;
                        hComp.m_Handle  = Component.m_Handle.m_Handle & 0x00ffffff;
        type_node&      TypeNode        = m_lComponentTypes[iType];
        s32             iComp           = TypeNode.m_lComponent.GetIndexByHandle(hComp);
        ASSERT(iComp<TypeNode.m_nActive);
#endif
        return;
    }

    xhandle         hComp;
    s32             iType           = Component.m_Handle.m_Handle >> 24;
                    hComp.m_Handle  = Component.m_Handle.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];
    s32             iComp           = TypeNode.m_lComponent.GetIndexByHandle(hComp);
    
    // Make sure that everything is clear
    ASSERT( TypeNode.m_nActive < TypeNode.m_lComponent.getCount() );

    // Put the node into the active side
    ASSERT( Component.isActive() == FALSE );
    TypeNode.m_lComponent.SwapIndex( iComp, TypeNode.m_nActive );
    TypeNode.m_nActive++;

    // Set the component active
    Component.m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_ACTIVE );
}

//--------------------------------------------------------------------------------

void gb_component_type_mgr::DeactivateComponent( gb_component& Component )
{
    ASSERT( Component.m_Handle.isValid() );

    // If it is already deactivated there is nothing to do
    if( !Component.isActive() )
    {
#ifdef X_DEBUG
        xhandle         hComp;
        s32             iType           = Component.m_Handle.m_Handle >> 24;
                        hComp.m_Handle  = Component.m_Handle.m_Handle & 0x00ffffff;
        type_node&      TypeNode        = m_lComponentTypes[iType];
        s32             iComp           = TypeNode.m_lComponent.GetIndexByHandle(hComp);
        ASSERT(iComp>=TypeNode.m_nActive);
#endif
        return;
    }

    xhandle         hComp;
    s32             iType           = Component.m_Handle.m_Handle >> 24;
                    hComp.m_Handle  = Component.m_Handle.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];
    s32             iComp           = TypeNode.m_lComponent.GetIndexByHandle(hComp);
    
    // Put the node into the active side
    ASSERT( Component.isActive() );
    TypeNode.m_nActive--;
    TypeNode.m_lComponent.SwapIndex( iComp, TypeNode.m_nActive );

    // Set the component deactive
    Component.m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_ACTIVE );
}

//--------------------------------------------------------------------------------

void gb_component_type_mgr::RegisterComponent( gb_component& Component )
{
    ASSERT( Component.m_Handle.IsNull() );
    s32             iType    = Component.getType().m_iType;
    type_node&      TypeNode = m_lComponentTypes[iType];
    xhandle         Handle;

    // Add the node into the list
    TypeNode.m_lComponent.append(Handle) = &Component;

    //
    // Register the server if it is one
    //
    if( TypeNode.m_bNetwork )
    {
        // Match the size of the harray
        if( TypeNode.m_lNetworkInfo.isValid() == FALSE )
        {
            TypeNode.m_lNetworkInfo.Alloc( TypeNode.m_lComponent.GetCapacity() );
        }
        else if( TypeNode.m_lNetworkInfo.getCount() < TypeNode.m_lComponent.GetCapacity() )
        {
            TypeNode.m_lNetworkInfo.Resize( TypeNode.m_lComponent.GetCapacity() );
        }

        network_info& Network = TypeNode.m_lNetworkInfo[Handle.m_Handle];

        // Reset the priorities for all the players
        for( s32 j=0; j<Network.m_Priority.getCount(); j++ ) 
            Network.m_Priority[j]  = 0;

        Network.m_hNetworkID.SetNull();
    }

    //
    // Deal with the initial server/client state
    //
    const u32 TypeFlags = Component.getType().getFlags();
    if( x_FlagIsOn( TypeFlags, gb_component_type::FLAGS_NETWORK ) )
    {
        gb_component& Entity = Component.getEntity();

        //
        // Determine whether this component should be active base on the 
        // server/client mode
        //
        if( x_FlagIsOn( TypeFlags, gb_component_type::FLAGS_CLIENT) == 
            Entity.m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_CLIENT ) ||
            x_FlagIsOn( TypeFlags, gb_component_type::FLAGS_SERVER ) != 
            Entity.m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_CLIENT ) )
        {
            // if the entity and the component agree in the state there is nothing to do.
            // whether the component is active or not we will leave it alone
        }
        else
        {
            // If the entity thinks that it is something different than the component
            // then the component will be deactvated.
            Component.m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_ACTIVE );

            /*
            if( x_FlagIsOn( TypeFlags, gb_component_type::FLAGS_CLIENT) )
            {
                // Make sure that the component is not also its own client
                if( &Component.getServer() != &Component )
                    Component.m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_ACTIVE );
            }
            else
            {
                Component.m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_ACTIVE );
            }
            */
        }
    }

    //
    // Make it active if is mark as active
    //
    if( Component.isActive() )
    {
        s32 iComp = TypeNode.m_lComponent.GetIndexByHandle( Handle );
        TypeNode.m_lComponent.SwapIndex( iComp, TypeNode.m_nActive );
        TypeNode.m_nActive++;

        // Make sure that the component is in a valid range
        ASSERT( TypeNode.m_lComponent.GetIndexByHandle(Handle) < TypeNode.m_nActive );
    }
    else
    {
        // Make sure that the component is in a valid range
        ASSERT( TypeNode.m_lComponent.GetIndexByHandle(Handle) >= TypeNode.m_nActive );
    }

    //
    // Set the new handle into the component
    //
    Handle.m_Handle |= (iType<<24);
    Component.m_Handle = Handle;

    //
    // Get the network id for this component
    //
    if( x_FlagIsOn( TypeFlags, gb_component_type::FLAGS_CLIENT) )
    {
        gb_component&       Server = Component.getServer();
        gb_component_type&  ServerType = Server.getType();

        if( x_FlagsAreOn( ServerType.getFlags(), gb_component_type::FLAGS_HAS_PACKED_DATA|gb_component_type::FLAGS_SERVER ) )
        {
            // Set the network ID for the client
            xhandle hNetworkID;
            m_NetworkIDToClient.append( hNetworkID ) = Component.m_Handle;

            // Set the network ID for the server. Two ways here the fast way if by chance
            // the server has register before the client, or the slow way if the client
            // was the first to get register.
            if( Server.m_Handle.isValid() )
            {
                xhandle         hComp;
                s32             iType           = Server.m_Handle.m_Handle >> 24;
                                hComp.m_Handle  = Server.m_Handle.m_Handle & 0x00ffffff;
                type_node&      TypeNode        = m_lComponentTypes[iType];

                TypeNode.m_lNetworkInfo[hComp.m_Handle].m_hNetworkID = hNetworkID;
            }
            else
            {
                // Let the server know which is the network id for the server
                gb_base_component_mgr&  CmpTypeMgr  = gb_base_component_mgr::SafeCast( Server.getType().getManager() );
            
                CmpTypeMgr.msgSetServerNetworkID( Server, hNetworkID );
            }
        }
    }

    // Notify the component that it is officially in the world
    Component.vInWorld();
}

//--------------------------------------------------------------------------------

void gb_component_type_mgr::UnregisterComponent( gb_component& Component )
{
    ASSERT( Component.m_Handle.isValid() );

    xhandle         hComp;
    s32             iType           = Component.m_Handle.m_Handle >> 24;
                    hComp.m_Handle  = Component.m_Handle.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];

    // First remove it from the active list if it is there
    if( Component.isActive() )
    {
        ASSERT( TypeNode.m_lComponent.GetIndexByHandle(hComp) < TypeNode.m_nActive );
        s32 iComp = TypeNode.m_lComponent.GetIndexByHandle( hComp );
        TypeNode.m_nActive--;
        TypeNode.m_lComponent.SwapIndex( iComp, TypeNode.m_nActive );
    }

    // Unregister node
    TypeNode.m_lComponent.DeleteByHandle(hComp);

    // Set the handle of the node to null
    Component.m_Handle.SetNull();
}

//--------------------------------------------------------------------------------

gb_component_type_mgr::network_info& gb_component_type_mgr::getNetworkInfo( xhandle hComponent )
{
    ASSERT( hComponent.isValid() );

    xhandle         hComp;
    s32             iType           = hComponent.m_Handle >> 24;
                    hComp.m_Handle  = hComponent.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];

    ASSERT( TypeNode.m_bNetwork );

    return  TypeNode.m_lNetworkInfo[hComp.m_Handle];
}

//--------------------------------------------------------------------------------

void gb_component_type_mgr::SetServerNetworkID( gb_component& Component, xhandle hNetworkID )
{
    getNetworkInfo( Component.m_Handle ).m_hNetworkID = hNetworkID;
}

//--------------------------------------------------------------------------------

gb_component& gb_component_type_mgr::GetComponentFromHandle( xhandle hComponent )
{
    xhandle         hComp;
    s32             iType           = hComponent.m_Handle >> 24;
                    hComp.m_Handle  = hComponent.m_Handle & 0x00ffffff;
    type_node&      TypeNode        = m_lComponentTypes[iType];

    return *TypeNode.m_lComponent(hComp);
}

//--------------------------------------------------------------------------------

gb_component& gb_component_type_mgr::GetComponentFromNetworkID( xhandle hNetworkID )
{
    return GetComponentFromHandle( m_NetworkIDToClient(hNetworkID) );
}

//------------------------------------------------------------------------------

gb_component_type* gb_component_type_mgr::findComponentType( const char* pName )
{
    for( gb_component_type* pNext = gb_component_type::getFirstType(); pNext ; pNext = pNext->m_pNext )
    {   
        if( x_strcmp( pNext->getName(), pName) == 0  )
           return pNext;
    }

    return NULL;
}

//------------------------------------------------------------------------------

gb_component_type* gb_component_type_mgr::findComponentType( u32 Guid )
{
    for( gb_component_type* pNext = gb_component_type::getFirstType(); pNext ; pNext = pNext->m_pNext )
    {   
       if( pNext->m_Guid == Guid )
           return pNext;
    }

    return NULL;
}

//------------------------------------------------------------------------------

gb_component_type* gb_component_type_mgr::findComponentType( const xrtti& RTTI )
{
    for( gb_component_type* pNext = gb_component_type::getFirstType(); pNext ; pNext = pNext->m_pNext )
    {   
       if( &pNext->getObjectRTTI() == &RTTI )
           return pNext;
    }

    return NULL;
}

//------------------------------------------------------------------------------

void gb_component_type_mgr::getAllActiveTypes( const gb_base_mgr& BaseMgr, xarray<s32>& TypeList ) const
{
    for( s32 i=0; i<m_lComponentTypes.getCount(); i++ )
    {
        const type_node& Node = m_lComponentTypes[i];

        // Make sure that the manager is the same
        if( &Node.m_pType->getManager() != &BaseMgr )
            continue;

        // Type must be wanting for us to call update
        if( !(Node.m_pType->m_Flags & gb_component_type::FLAGS_DYNAMIC_MASK) )
            continue;

        // Add the index of the type
        TypeList.append() = Node.m_pType->m_iType;
    }
}

//------------------------------------------------------------------------------

void gb_component_type_mgr::getAllNetworkTypes( xarray<s32>& TypeList ) const
{
    for( s32 i=0; i<m_lComponentTypes.getCount(); i++ )
    {
        const type_node& Node = m_lComponentTypes[i];

        if( Node.m_bNetwork )
        {
            // Add the index of the type
            TypeList.append() = Node.m_pType->m_iType;
        }
    }
}
