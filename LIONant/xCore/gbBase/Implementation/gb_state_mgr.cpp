#include "gb_base.h"
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////

gb_state_mgr g_StateMgr("StateMgr");

//////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------

gb_state_mgr::gb_state_mgr( const char* pName ) :
    gb_base_mgr( pName )
    , m_pActiveState( NULL )
{
}

//-------------------------------------------------------------------------------
// This job build the actual state grouph from the dependency graph in the sync points.
// This bit is a bit confussing becase it looks like there is many connections been made.
// Also because there is some hidden details which makes it non intuitive at a first glance.
// This is how the graph should look like ones this function finish execution:
//
// SyncP - Sync Point. Note that he is a trigger as well.
// Mgr   - A manager inside a sync point. Note that he is a job as well.
// Cjobs - Component jobs. This are the jobs created by the manager but they also need to be added to 
//         the local SyncPoint's trigger. Note that this function does not deal with these.
//
//   +-----+                            
//   |State|           +-----+                            +-----+  
//   |SyncP|---+       |SyncP|------------------+         |SyncP|----------------->
//(T)#######   |    (T)####### <---------+      |      (T)####### <---------+
//             |          |              |      |            |              |
//             |     (J)#####----------->|      |       (J)#####----------->|  
//             +------> |Mgr|..+-----+   |      +--------> |Mgr|..+-----+   |
//             |        +---+  |Cjobs|-->|      |          +---+  |Cjobs|-->|
//             |          |    ^^^^^^^   |      |            |    ^^^^^^^   |
//             |     (J)#####----------->|      |       (J)#####----------->|  
//             +------> |Mgr|..+-----+   |      +--------> |Mgr|..+-----+   |
//                      +---+  |Cjobs|-->|                 +---+  |Cjobs|-->|
//                             ^^^^^^^                            ^^^^^^^            
// Sync Points - They are incharge of the local time passing. They are also incharge of triggering
//               the next step of manager jobs as the graph shows.
// Managers    - They deal with 3 things:
//                 1. Creating the components jobs. (Jobs that will update components)
//                 2. Adding the components jobs to the local Sync Point as shown in the graph.
//                    this allows the sync point to only trigger next sync point ones all the 
//                    components are updated.
//                 3. Adding jobs to the scheguler. This is a bit confusing but in a way the manager acts
//                    like a trigger as well. The timming on when they get added to the sceguler is very
//                    important they need to be added after they have been added to the local sync point
//                    trigger (step 2).  If not they may get run before the sync point knows about them.
//
// NOTE: It should be clear that the connections between the component jobs to the actual local Sync Point
//       trigger is the responsability of the manager and not of this job.
// NOTE: There is a tricky bit which is the fact that the managers jobs are in the triggers sync point.
//       The fact that they are there forces the system to not be able to advance before they are done.
//       This is very important because this fact is what allows these jobs to add the components jobs 
//       to the sync point triggers other wise it will be impossible. 
//-------------------------------------------------------------------------------
void gb_state_mgr::SetupDependencyGraph( void )
{
    // TODO: Because managers are incharge of their instances tick counts should be in the managers not in the triggers.
    //       This will solve the problem for instance of changeing the state of the game and the ticks been wrong.
    //       Ultimatelly it is the component's manager to know that the time is for their intances since they represent a "type"
    //       Also managers could be excluded in certain states and their local time should reflect that.
    //
    
    
    //
    // Get the tick for this frame
    //
    u32 FrameTick = m_pActiveState->getStartSyncP().getTickCount();

    // Update the tick count to the next frame
    FrameTick++;

    //
    // Add all the graph dependencies
    //
    
    // First make sure that the start triggers don't have work to do seems there is nothing
    // that will trigger them.
    for( gb_sync_point* pNext = m_pSyncPoint; pNext; pNext = pNext->m_pExecNext )
    {
        // Set the frmar tick for all the sync points
        pNext->setFrameTick( FrameTick );

        // Add the dependency managers to our trigger
        for( s32 i=0; i<pNext->m_nDependencies; i++ )
        {
            gb_sync_point& Depend = *pNext->m_lDependency[i];

            // Add each of the managers in our dependency to our trigger
            for( gb_base_mgr* pMgr = Depend.m_pManager; pMgr; pMgr = pMgr->m_pLocalNext )
            {                
                pNext->AndThenRuns( *pMgr );
            }
        }

        // Until we do not finish our local managers the trigger can not move on.
        // This allows local syncronization with in the trigger itself.
        for( gb_base_mgr* pMgr = pNext->m_pManager; pMgr; pMgr = pMgr->m_pLocalNext )
        {
            // Make the ync point wait for our manager
            pNext->AndWaitFor( *pMgr );
        }
    }
}

//-------------------------------------------------------------------------------

void gb_state_mgr::onUpdate( void )
{
   // printf("[sm]");
    ASSERT( g_order == 0 ); g_order = 1;
}

//-------------------------------------------------------------------------------

void gb_state_mgr::ExecuteFrame( void )
{
    ASSERT( m_pActiveState );

    //
    // First set the dependency graph for the execution
    //
    SetupDependencyGraph();

    //
    // Compute the delta time for this frame
    //
    g_GameMgr.ComputeDeltaTime();

    //
    // preUpdate the network manager
    //
    g_NetworkMgr.PreUpdate();

    //
    // Notify the begging of the gram
    //
    m_pActiveState->onBeginFrame();
    
    //
    // Now we can start all our managers
    //
    g_order = 0;
    for( gb_base_mgr* pNext = m_pSyncPoint->m_pManager; pNext; pNext = pNext->m_pLocalNext )
    {
        g_Scheduler.StartJobChain( *pNext );
    }
}

//-------------------------------------------------------------------------------

void gb_state_mgr::ExecuteNextFrame( void )
{
    m_pActiveState->m_PostEndSyncP.m_bLoop = TRUE;
}

//-------------------------------------------------------------------------------

void gb_state_mgr::setActiveState( gb_cloud_state& State )
{
    m_pActiveState = &State;
}

