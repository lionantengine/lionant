#ifndef CLOUD_INLINE_H
#define CLOUD_INLINE_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// BASE MANAGER
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
inline
xbool gb_base_mgr::hasUpdatedThisFrame( void ) const 
{
    u32 FrameTick = m_pSyncPoint->getFrameTick();
    return m_ProcessTick == FrameTick; 
}

//-------------------------------------------------------------------------------

inline
void gb_base_mgr::MessageHandler( gb_mgr_base_msg& Msg ) 
{ 
    (this->*Msg.m_MessageFunc)(Msg); 
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// SYNC-POINT
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
inline 
xbool gb_sync_point::hasTimeAdvance( void ) const  
{ 
    return m_TickCount == m_FrameTick;
}

//-------------------------------------------------------------------------------
inline 
s32 gb_sync_point::getTxRead( s32 Tx )      
{ 
    if(Tx)
    { 
        ASSERT(hasTimeAdvance()); 
        return m_FrameTick&1; 
    } 
    
    return 1-(m_FrameTick&1); 
}

//-------------------------------------------------------------------------------
inline 
s32 gb_sync_point::getT1Write( void )
{ 
    // We do not allow writting was the manager has updated
    ASSERT(!hasTimeAdvance()); 
    return m_FrameTick&1; 
} 

//-------------------------------------------------------------------------------
inline 
s32 gb_sync_point::getLatestReadTx( void )        
{ 
    return m_TickCount&1;
}

//-------------------------------------------------------------------------------
inline 
s32 gb_sync_point::getTickCount( void )
{ 
    return m_TickCount;
}

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif