//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

/////////////////////////////////////////////////////////////////////////////////
// VARIABLES
/////////////////////////////////////////////////////////////////////////////////

gb_blueprint_manager g_BlueprintMgr;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ENTITY BLUEPRINT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------

gb_entity_blueprint::gb_entity_blueprint( void )
{
    m_Guid          = 0;
    m_RefCount      = 0;    
}

//-------------------------------------------------------------------------------

gb_entity_blueprint::~gb_entity_blueprint( void )
{
    ASSERT(m_RefCount==0);
}

//-------------------------------------------------------------------------------

void gb_entity_blueprint::compileMsgTable( gb_entity& Entity )
{
    //
    // First identify how much memory we are going to need for the message table
    //
    s32 nComponents = 0;

    for( gb_component** pComponent = Entity.m_pComponents; *pComponent; pComponent++ )
    {
        // Collect the number of components as well
        nComponents++;
    }

    //
    // Backup the messageTable
    //
    m_RefCount    = 0;
    m_nComponents = nComponents;
}

//-------------------------------------------------------------------------------

void gb_entity_blueprint::initEntity( gb_entity& Entity )
{
    //
    // Compile entity
    //

    // Resolve all dependencies
    Entity.resolveDependencies();

    // compile map table
    if( m_Guid == 0 )
    {
        compileMsgTable( Entity );

        //
        // Set the GUID
        //
        m_Guid = Entity.getEntityBlueprintGUID();
    }

    //
    // Set the entity a pointer to this class to handle reference counting and such
    //
    Entity.m_pEntityBluePrint = this;

    //
    // Increase the ref count
    //
    m_RefCount++;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BLUEPRINT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

gb_blueprint::gb_blueprint( void )
{
    m_Guid.ResetValue();
	m_bFileInMemory = FALSE;
}

//------------------------------------------------------------------------------

gb_blueprint::~gb_blueprint( void )
{
    // TODO: Clean up
}

//------------------------------------------------------------------------------

void gb_blueprint::appendEntity( gb_entity& Entity )
{
    // Append the entity in the list
    m_MasterEntities.append() = &Entity;
}

//------------------------------------------------------------------------------

void gb_blueprint::saveBlueprint( xtextfile& File )
{
    xarray<gb_entity_blueprint*>    Entities;
    s32                             nBluePrintEntries = m_MasterEntities.getCount();

    //
    // Save base information about the blue print
    //
    File.WriteRecord("Blueprint");
    File.WriteField("FileName:s",           (const char*)m_Filename );
    File.WriteField("GUID:g",               m_Guid.m_Guid );
    File.WriteField("nEntityBluePrints:d",  nBluePrintEntries );
    File.WriteLine();

    //
    // Save all the entity blue prints
    //
    for( s32 i = 0; i<nBluePrintEntries; i++ )
    {
        m_MasterEntities[i]->saveEntity( File, i );
    }
}

//------------------------------------------------------------------------------

void gb_blueprint::createEntities( xarray<gb_entity*>& Entities, const xtransform& Transform )
{
    xtextfile File;

    //
    // Open the file for writting
    //
	if( m_bFileInMemory == FALSE )
	{
		x_FileSystemCacheFile( m_Filename );
		m_Filename.Format( "cache:%s", (const char*)m_Filename );
		m_bFileInMemory = TRUE;
	}

	File.OpenForReading( m_Filename );

    //
    // Read our key record
    //
    if( !File.ReadRecord() )
    {
        x_throw( "Unable to read blue print key record");
    }

    if( File.GetRecordName() != "Blueprint" )
    {
        x_throw( "Not Blueprint header found in this file" );
    }

    //
    // Read the key data
    //
    s32         nBluePrintEntries;
    File.ReadLine();
    //TODO: also we may rethink the way we deal with this part because reloading this everytime
    //      a new instance is created is unnecesary.
    File.ReadField( "GUID:g", &m_Guid );
    File.ReadField( "nEntityBluePrints:d",  &nBluePrintEntries );

    //
    // Read the entities
    //
    for( s32 i=0; i<nBluePrintEntries; i++ )
    {
        // Load entity from the file
        gb_entity& Entity = gb_entity::loadEntity( File );

        // Set a new guid to the entity
        xguid       Guid;
        Guid.ResetValue();

        Entity.setGuid( Guid );

        // Try to get the transform interface
        void* pInterface = Entity.findInterface( i_transform::getRTTI() );
        if( pInterface )
        {
            i_transform& itTransform = i_transform::SafeCast( *(i_transform*) pInterface );
            itTransform.setupTransform(Transform);
        }

        // Set the entity into our list
        Entities.append() = &Entity;
    }

    //
    // Done with the file
    //
    File.Close();
}

//------------------------------------------------------------------------------

void gb_blueprint::Compile( void )
{
    //
    // Make sure everything is compiled
    //
    for( s32 i=0; i<m_MasterEntities.getCount(); i++ )
    {
        //
        // First make sure that everything has a components table
        // Some entities without additional components may not create components table
        // to save time at the loading of entities.
        //
        if( m_MasterEntities[i]->m_pComponents == NULL )
            m_MasterEntities[i]->initComponentTable();    


        u32 Guid = m_MasterEntities[i]->getEntityBlueprintGUID();

        // Try to find the entity blueprint
        gb_entity_blueprint* pEntityBlue = g_BlueprintMgr.findEntityBlueprint( Guid );

        // if we can not find it then compile a new one
        if( pEntityBlue == NULL )
        {
            pEntityBlue = &g_BlueprintMgr.m_EntityBlue.append();
        }

        // Setup both the entity blue print and the entity
        pEntityBlue->initEntity( *m_MasterEntities[i] );
    }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// gb_blueprint_manager
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

gb_blueprint* gb_blueprint_manager::findBlueprint( const char* pFileName )
{
    for( s32 i=0; i<m_Blueprints.getCount(); i++ )
    {
        if( x_strcmp( m_Blueprints[i].m_Filename, pFileName ) == 0 )
            return &m_Blueprints[i];
    }

    return NULL;
}

//------------------------------------------------------------------------------

gb_blueprint* gb_blueprint_manager::findBlueprint( xguid Guid )
{
    for( s32 i=0; i<m_Blueprints.getCount(); i++ )
    {
        if( Guid == m_Blueprints[i].m_Guid )
            return &m_Blueprints[i];
    }

    return NULL;
}

//------------------------------------------------------------------------------

gb_entity_blueprint* gb_blueprint_manager::findEntityBlueprint( u32 Guid )
{
    for( s32 i=0; i<m_EntityBlue.getCount(); i++ )
    {
        if( m_EntityBlue[i].m_Guid == Guid )
            return &m_EntityBlue[i];
    }

    return NULL;
}

//------------------------------------------------------------------------------

xguid gb_blueprint_manager::getBlueprintByFilename( const char* pFilename )
{
    xguid Guid;
    Guid.SetNull();
    gb_blueprint* pBlueprint = findBlueprint( pFilename );
    if( pBlueprint == NULL ) return Guid;
    return pBlueprint->m_Guid;
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::deleteBlueprint( xguid gBluePrint )
{
    s32 i;

    // Find our blue print
    for( i=0; i<m_Blueprints.getCount(); i++ )
    {
        if( gBluePrint == m_Blueprints[i].m_Guid )
            break;
    }

    // nothing to do
    if( i == m_Blueprints.getCount() ) return ;

    // Other wise...
    m_Blueprints.DeleteWithSwap(i);
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::createEntities( xarray<gb_entity*>& Entities, xguid gBluePrint, const xtransform& Transform )
{
    //
    // First lets find our blueprint
    //
    gb_blueprint* pBlueprint = findBlueprint( gBluePrint );
    if( !pBlueprint )
    {
        x_throw("Unable to find a blueprint");
		return;
    }

    //
    // Create the entities
    //
    pBlueprint->createEntities( Entities, Transform );
}

//------------------------------------------------------------------------------

xguid gb_blueprint_manager::createBlueprint( const char* pFileName )
{
    //
    // Make sure that we do not have it already
    //
    if( findBlueprint( pFileName ) )
    {
        ASSERT(0);
     //   x_throw( xfs("We already have a blue print named [%s]", pFileName) );
    }

    //
    // Create the actual blue print
    //
    gb_blueprint& BluePrint = m_Blueprints.append();

    // TODO: This needs to be deeped copied
    BluePrint.setFilename(pFileName);
    
    return BluePrint.m_Guid;
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::loadBlueprint( const char* Filename )
{


}

//------------------------------------------------------------------------------

void gb_blueprint_manager::saveBlueprints( const char* pFolder )
{
    for( s32 i=0; i<m_Blueprints.getCount(); i++ )
    {
        xtextfile File;

        // open for writting in text mode
        File.OpenForWriting( xstring::BuildFromFormat( "%s%s", pFolder, (const char*)m_Blueprints[i].m_Filename) );

        // Save the blue print
        m_Blueprints[i].saveBlueprint( File );

        // Done
        File.Close();
    }
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::appendEntity( xguid gBluePrint, gb_entity& Entity )
{
    //
    // Make sure that we do not have it already
    //
    gb_blueprint* pBlueprint = findBlueprint( gBluePrint );
    if( !pBlueprint )
    {
        ASSERT(0);
     //   x_throw( "We could not find the blue print with the guid" );
    }

    //
    // Add the entity in our blue print
    pBlueprint->appendEntity( Entity );
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::compileBlueprint( xguid gBluePrint )
{
    //
    // Make sure that we do not have it already
    //
    gb_blueprint* pBlueprint = findBlueprint( gBluePrint );
    if( !pBlueprint )
    {
        ASSERT(0);
     //   x_throw( "We could not find the blue print with the guid" );
    }

    //
    // Compile the blue print
    //
    pBlueprint->Compile();
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::setMessageTable( u32 gEntityBP, gb_entity& Entity )
{
    //
    // Find the entity blue print if we dont find it then make a new one
    //
    gb_entity_blueprint* pEntityBP = findEntityBlueprint( gEntityBP );
    if( pEntityBP == NULL )
    {
        pEntityBP = &m_EntityBlue.append();
    }

    //
    // Initialize the entity and the entity blue print if it needs so
    // 
    pEntityBP->initEntity( Entity );

    // Make sure everything turn out time be as spected
    ASSERT( pEntityBP->m_Guid == gEntityBP );
}

//------------------------------------------------------------------------------

gb_component::presets* gb_blueprint_manager::findPresets( xguid Guid )
{
    for( s32 i=0; i<m_lPresets.getCount(); i++ )
    {
        if( m_lPresets[i]->m_Guid == Guid )
            return m_lPresets[i];
    }
    return NULL;
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::appendPresets( gb_component::presets& Presets )
{
    ASSERT( NULL == gb_blueprint_manager::findPresets( Presets.m_Guid ) );
    
    Presets.incRefCount();
    m_lPresets.append() = &Presets;
}

//------------------------------------------------------------------------------

gb_component::presets& gb_blueprint_manager::createNewPresets( gb_component_type& CompType )
{
    gb_component::presets& Presets = *CompType.NewPresets();

    // Initialize some of the key variables
    Presets.m_Type = CompType.getGUID();
    Presets.m_Guid.ResetValue();
    Presets.m_RefCount = 1;
    
    // append them
    appendPresets( Presets );

    return Presets;
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::savePresets( const char* pFolder )
{
    xtextfile PresetTextFile;

    PresetTextFile.OpenForWriting( xstring::BuildFromFormat("%sPresets.txt", pFolder) );

    for( s32 i=0; i<m_lPresets.getCount(); i++ )
    {
        gb_component::presets& Preset = *m_lPresets[i];

        // If this is the default presets then we do not save them.
        if( Preset.m_Guid == Preset.m_Type )
            continue;

        PresetTextFile.WriteRecord("Presets");
        PresetTextFile.WriteField( "gType:h",  Preset.m_Type );
        
        Preset.vSave( PresetTextFile );        
        
        PresetTextFile.WriteLine();
    }

    PresetTextFile.Close();
}

//------------------------------------------------------------------------------

void gb_blueprint_manager::loadPresets( const char* pFolder )
{
    xtextfile PresetTextFile;

    PresetTextFile.OpenForReading( xstring::BuildFromFormat("%sPresets.txt", pFolder) );

    // Read all the presets in the file
    while( PresetTextFile.TryReadRecord() )
    {
        PresetTextFile.ReadLine();
        
        // Read the type of the preset
        u32 TypeGuid;
        PresetTextFile.ReadField( "gType:h",  &TypeGuid );

        // Create a new instance of the preset
        gb_component_type* pType = g_ComponentTypeMgr.findComponentType( TypeGuid );
        ASSERT(pType);

        // Load all the data
        gb_component::presets* pPresets = pType->NewPresets();
        pPresets->vLoad( PresetTextFile );

        // Added into the blue print manager
        g_BlueprintMgr.appendPresets( *pPresets ); 
    }

    PresetTextFile.Close();
}
