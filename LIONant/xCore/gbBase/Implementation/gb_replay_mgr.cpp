#include "gb_base.h"
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

gb_replay_mgr g_ReplayMgr;

/////////////////////////////////////////////////////////////////////////////////
// REPLAY_MANAGER
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

gb_replay_mgr::gb_replay_mgr( void )
{
    m_State = STATE_OFF;
}

//-------------------------------------------------------------------------------

void gb_replay_mgr::setupRecording( void )
{
    m_State = STATE_RECORDING;
    m_Frames.Grow( 60*60*60 );
    m_BitStream.setMode( TRUE );
    m_BitStream.setupInitialBufferSize( 60*60*60 );
    m_TotalTimeRecorded = 0;
}

//-------------------------------------------------------------------------------

void gb_replay_mgr::Stop( void )
{
    if( m_State == STATE_RECORDING )
    {
        ASSERT( m_State == STATE_RECORDING );
        m_State = STATE_STOP_RECORDING;

        // Get the data out
        m_BitStream.getResult( m_PlaybackBuffer, m_Length );

        // TODO: Okay now we should be able to destruct the bit stream...
    }
    else
    {
        ASSERT( isPlayingBack() );
        m_State = STATE_STOP_PLAYBACK;
    }
}

//-------------------------------------------------------------------------------

void gb_replay_mgr::setupPlayback( xbool bLoop )
{
    m_State = STATE_PLAYBACK;
    m_bLoop = bLoop;

    //
    // Read the playback file
    //
    s32   nFrames;
    xfile File;
    File.Open( "Recording.bin", "rb" );
    File.Read( &nFrames, 1 );
    File.Read( &m_Length, 1 );

    // Read the frame headers
    m_Frames.Grow( nFrames + 1 );
    m_Frames.appendList( nFrames );

    File.ReadRaw( &m_Frames[0], sizeof(frame), nFrames );

    // Read the playback data        
    m_PlaybackBuffer.Alloc( m_Length + 10 );

    File.ReadRaw( &m_PlaybackBuffer[0], 1, m_Length);
    File.Close();
    printf("Done loading the recording\n");

    //
    // Get ready for the playback
    //
    m_iPlaybackFrame = 0;
    m_BitStream.setMode( FALSE );
    m_BitStream.setPackData( m_PlaybackBuffer, m_Length );

    //
    // Go throw all the components that are in server mode and convert them into ghosts/clients
    //

}

//-------------------------------------------------------------------------------

void gb_replay_mgr::Init( void )
{
    // Go throw the types and identify which ones need packing
    for( gb_component_type* pType = gb_component_type::getFirstType(); pType; pType = pType->getNextType() )
    {
        if( !x_FlagIsOn( pType->getFlags(), gb_component_type::FLAGS_HAS_PACKED_DATA ) ) 
            continue;

        m_iPackTypes.append() = pType->getTypeIndex();
    }
}

//-------------------------------------------------------------------------------

void gb_replay_mgr::RecordFrame( void )
{
    // Make sure that we are recording first
    if( m_State != STATE_RECORDING )
        return;

    frame& Frame = m_Frames.append();
    Frame.m_iDeltaTime  = s32(g_GameMgr.getDeltaTime()/(1/60.0f));
    Frame.m_nComponents = 0;
    Frame.m_Origin      = g_NetworkMgr.getOrigin( m_hNetworkPlayer );

    // Count the total time we have recorded so far
    m_TotalTimeRecorded += g_GameMgr.getDeltaTime();

	//
	// Record of the server based components
	//
    gb_network_mgr::network_player&     Pl              = g_NetworkMgr.m_lNetworkPlayers(m_hNetworkPlayer);
	const xarray<xhandle>&              PriorityList    = Pl.m_lPriority;
	for( s32 i=0; i<PriorityList.getCount(); i++ )
	{
        xhandle         hComponent = PriorityList[i];
		gb_component&   Component  = g_ComponentTypeMgr.GetComponentFromHandle( hComponent );
		
		// This component should be a server since servers are the only things
		// that should be sending information from the network
		ASSERT( x_FlagIsOn( Component.getType().getFlags(), gb_component_type::FLAGS_SERVER ) );

        // Lets pack him
        m_BitStream.PackFlags( g_ComponentTypeMgr.getNetworkID( hComponent ).m_Handle, 32 );
		Component.vWritePacketData( m_BitStream, Frame.m_Origin );

        // Okay we count this guy
        Frame.m_nComponents++;

        // set the priority to zero
        gb_component_type_mgr::network_info& NIA = g_ComponentTypeMgr.getNetworkInfo( hComponent );
        NIA.m_Priority[m_hNetworkPlayer.m_Handle] = x_frand();

        // Lets collect the top 8 most important componets
        if( Frame.m_nComponents > 4 ) break;
	}

    //
    // Okay we will shot down recording after a while
    //
    if( m_TotalTimeRecorded >= 60 )
    {
        Stop();
        s32   nFrames = m_Frames.getCount();
        xfile File;
        File.Open( "Recording.bin", "wb" );
        File.Write( &nFrames, 1 );
        File.Write( &m_Length, 1 );
        File.WriteRaw( &m_Frames[0], sizeof(frame), m_Frames.getCount() );
        File.WriteRaw( &m_PlaybackBuffer[0], 1, m_Length);
        File.Close();
        printf("Done recording\n");
    }
}

//-------------------------------------------------------------------------------

void gb_replay_mgr::PlayBack( void )
{
    // Make sure that we are recording first
    if( m_State != STATE_PLAYBACK )
        return;

    static int a=0;
    if( a == 0 )
    {
        a = 1;
        return;
    }

    //
    // Read back all the states
    //
    frame& Frame = m_Frames[m_iPlaybackFrame];
    xhandle hNetworkID;
    for( s32 i=0; i<Frame.m_nComponents; i++ )
    {
        hNetworkID.m_Handle = m_BitStream.UnpackFlags32( 32 );
        gb_component& Comp  = g_ComponentTypeMgr.GetComponentFromNetworkID( hNetworkID );
		Comp.vReadPacketData( m_BitStream, Frame.m_Origin );
    }

    //
    // Make sure that we match the original frame rate
    //
    s32 iDeltaTime = s32(g_GameMgr.getDeltaTime()/(1/60.0f));
    if( m_Frames[m_iPlaybackFrame].m_iDeltaTime > iDeltaTime )
    {
        x_Sleep( (m_Frames[m_iPlaybackFrame].m_iDeltaTime-iDeltaTime)*16 );
    }

    //
    // Get ready for next frame
    //
    m_iPlaybackFrame++;
    if( m_iPlaybackFrame >= m_Frames.getCount() )
    { 
        if( m_bLoop )
        {
            m_iPlaybackFrame = 0;
            m_BitStream.setMode( FALSE );
            m_BitStream.setPackData( m_PlaybackBuffer, m_Length );
        }
        else
        {
            Stop();
        }
    }
}
