//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ENTITY
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

GB_CLASS_TYPE( gb_entity, gb_component_type::FLAGS_REALTIME_DYNAMIC )
GB_CLASS_TYPE( gb_entity_simple, gb_component_type::FLAGS_REALTIME_DYNAMIC)

//-------------------------------------------------------------------------------

gb_entity::gb_entity( void )
{
    m_pComponents = NULL;
    initComponentTable();
}

//-------------------------------------------------------------------------------

gb_entity::~gb_entity( void )
{
    //
    // Free up all the components
    //
    /*
    if( m_pComponents )
    {
        //
        // If we have components to delete lets do so now
        // TODO: There is an issue here with multicore... can not delete thing if we are in the middle of something
        for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
        {
            if( *pCom != this )
                (*pCom)->getType().destroyComponent(*pCom);
        }
        
        //
        // If we come from a blue print then we can not delete the component table
        // other wise we are responsable
        //
        if( m_pComponents[-1] == NULL )
        {
            //
            // Free the memory
            //
            x_free( m_pComponents );
        }

        // Done with our variable
        m_pComponents=NULL;
    }
    */
}

//-------------------------------------------------------------------------------

void gb_entity::onUpdate( const gb_msg& msg )
{
    // If you hit this chances are you 
    ASSERT(0);

//    printf( "((Entity[%X]))", this );

    /// if it does nothing it should not be here, because if not it will be called.
}

//------------------------------------------------------------------------------

u32 gb_entity::getEntityBlueprintGUID( void ) const
{
    u32 CRC = 0;
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        u32 Guid = (*pCom)->getType().getGUID();
        CRC = x_memCRC32( &Guid, sizeof(u32), CRC );
    }
    return CRC;
}

//------------------------------------------------------------------------------

void gb_entity::resolveDependencies( void )
{
    ASSERT(m_pComponents);

    // Search throw all the components in the list
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        (*pCom)->vResolveDependencies();
    }
}

//------------------------------------------------------------------------------

void gb_entity::initComponentTable( void )
{
    // Seems like it already been initialized
    if( m_pComponents )
        return;

    // Create a block of memory
    m_pComponents = (gb_component**)x_malloc( sizeof(gb_component*), 3, XMEM_FLAGS_DEFAULT );
    ASSERT(m_pComponents);

    // Set the basic information
    m_pComponents[0] = NULL;        // Message callbacks
    m_pComponents[1] = this;        // The Entity component
    m_pComponents[2] = NULL;        // Null terminator

    // Base is always one element foward
    m_pComponents = &m_pComponents[1];
}

//------------------------------------------------------------------------------

void gb_entity::addComponent( gb_component& Component )
{
    //
    // Make sure that at least the basic stuff is there
    //
    if( m_pComponents == NULL )
    {
        initComponentTable();
    }

    // Search throw all the components in the list
    s32 Count = 0;
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        Count++;

        if( Component.getType().getMessageGroupID() == (*pCom)->getType().getMessageGroupID() )
        {
            x_throw("Two components can not have the same message group id" );
        }
    }

    // Ok add component to the end
    m_pComponents = (gb_component**)x_realloc( &m_pComponents[-1], sizeof(gb_component*), Count+2+1 );
    ASSERT(m_pComponents);

    // Skip the message map
    m_pComponents = &m_pComponents[1];

    // 
    // Do an insert sort for this component
    // It is important to note that the entity is always at zero location
    //
    gb_component* pTemp = &Component;
    for( s32 i = 1; i<(Count-1); i++ )
    {
        if( pTemp == &Component )
        {
            // Insert by smallest guid
            if( m_pComponents[i]->getType().getGUID() >= Component.getType().getGUID() )
            {
                // They can not be equal or else something very wrong is going on
                ASSERT( m_pComponents[i]->getType().getGUID() != Component.getType().getGUID() );

                // Swap
                x_Swap( pTemp, m_pComponents[i] );
            }
        }
        else
        {   // Keep swaping until end
            x_Swap( pTemp, m_pComponents[i] );
        }
    }
    
    // Last swap
    m_pComponents[Count] = pTemp;

    // Make sure it finish with the NULL
    m_pComponents[Count+ 1] = NULL;

    //
    // Set the component's component pointer to all components
    //
    for( gb_component** pNext = m_pComponents; *pNext; pNext++ )
    {
        (*pNext)->m_pComponents = m_pComponents;
    }
}

//-------------------------------------------------------------------------------
// There are only 1 reason why to delete a component:
// 1 - Because you are creating a new entity and try are just trying different components in the editor.
void gb_entity::delComponent( gb_component& Component )
{
    // We dont have anything to delete
    ASSERT(m_pComponents);

    // We should not have been compiled yet.
    ASSERT(m_pComponents[-1]==NULL);

    //
    // make sure that the user did not make a mistake
    //
    s32 LocalID = 0;
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        if( &Component == *pCom )             
            break;

        LocalID++;
    }

    ASSERT( LocalID > 0 );

    //
    // Ok now lets remove the component.
    // Note that at this point the dependencies should be resolved again.
    //
    for( gb_component** pCom = &m_pComponents[LocalID]; *pCom; pCom++ )
    {
        pCom[0] = pCom[1];
    }
}

//------------------------------------------------------------------------------

void gb_entity::init( void )
{
}

//------------------------------------------------------------------------------

void gb_entity::saveEntity( xtextfile& File, s32 iEntity )
{
    //
    // Write the key general information for all entities
    //
    File.WriteRecord("Entity");
    File.WriteField("Index:d",          iEntity );
    File.WriteField("gEntityBP:h",      m_pEntityBluePrint->m_Guid );
    File.WriteField("nComponents:d",    m_pEntityBluePrint->m_nComponents  );
    File.WriteLine();

    //
    // Now we can write all the component information
    //
    s32 Count=0;
    xguid ZeroGuid;
    ZeroGuid.SetNull();
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        File.WriteRecord( "Component" );
        File.WriteField("Index:d", Count++ );
        File.WriteField("CType:h", (*pCom)->getType().getGUID() );
        
        if( (*pCom)->m_pPresets ) File.WriteField("gPreset:g", (*pCom)->m_pPresets->m_Guid.m_Guid );
        else                      File.WriteField("gPreset:g", ZeroGuid.m_Guid );

        File.WriteField("Name:s",  (*pCom)->getType().getName() );

        // Now save the rest of the component
        (*pCom)->vSaveComponent( File );
        File.WriteLine();
    }
}

//------------------------------------------------------------------------------

gb_entity& gb_entity::createFromType( xguid gEntity, const gb_component_type& Type, xguid gPresets )
{
    gb_entity& Entity = gb_entity::SafeCast( *Type.createComponent(gPresets) );
    
    Entity.setGuid( gEntity );
    
    return Entity;
}

//------------------------------------------------------------------------------

gb_entity& gb_entity::loadEntity( xtextfile& File )
{
    //
    // Try to read the first header
    //
    if( !File.ReadRecord() )
    {
        x_throw( "Unable to read entity. Not a single header found to read");
    }

    if( File.GetRecordName() != "Entity" )
    {
        x_throw( "Not Entity header found in this file" );
    }

    //
    // Read the main header 
    //
    File.ReadLine();

    u32 gEntityBP;
    File.ReadField("gEntityBP:h", &gEntityBP );

    s32 nComponents;
    File.ReadField("nComponents:d", &nComponents );

    //
    // Allocate memory for the component Table
    // 
    gb_component** pTable = (gb_component**)x_malloc( sizeof(gb_component*), nComponents+2, XMEM_FLAGS_DEFAULT );
    ASSERT(pTable);

    // Clear the memory
    x_memset( pTable, 0, sizeof(gb_component*)*(nComponents+2) );

    //
    // Okay lets read the components
    //
    for( s32 i=0; i<nComponents; i++ )
    {
        //
        // Read the header first
        //
        if( !File.ReadRecord() )
        {
            x_throw( "Unable to read entity. No [Component] header found");
        }

        if( File.GetRecordName() != "Component" )
        {
            x_throw( "Header found but not a [Component] Header" );
        }

        //
        // Now read the component info
        //
        File.ReadLine();
        u32 ComponentType; 
        File.ReadField("CType:h", &ComponentType );

        xguid PresetGuid;
        File.ReadField("gPreset:g", &PresetGuid );

        // Now lets find the type
        gb_component_type* pEntityType = g_ComponentTypeMgr.findComponentType( ComponentType );
        if( pEntityType == NULL )
        {
            x_throw( "Unable to find component type" );
        }

        // Set the component
        gb_component* pComponent = pEntityType->createComponentFromLoad( PresetGuid );

        // Create the component for this entity
        pTable[1+i] = pComponent;

        // Set the component table
        pComponent->m_pComponents = &pTable[1];

        // Now as the component to load any information it may need
        pComponent->vLoadComponent( File );
    }

    //
    // Make sure we set the message map to this entity
    //
    gb_entity& Entity = gb_entity::SafeCast( *pTable[1] );
    g_BlueprintMgr.setMessageTable( gEntityBP, Entity );

    return Entity;
}

//------------------------------------------------------------------------------

void gb_entity::vUpdate( void )
{
    //
    // Now we send the official onUpdate for the entities
    //
    entity_update_msg Msg;
	Msg.setupSystemVars( gb_component::callback_fn(&gb_entity::onUpdate), gb_msg::FLAGS_ONUPDATE_MESSAGE | gb_msg::FLAGS_DO_NOT_DELETE );
    sendMessage( *this, g_GameMgr.getDeltaTime(), getType(), Msg );

    //
    // Update all messages
    //
    gb_component::vUpdate();
}

//------------------------------------------------------------------------------

void gb_entity::sendMessage( const gb_entity& Sender, f32 DeltaTime, gb_component_type& Type, gb_msg& Message )
{
    //
    // Fill the rest of the message
    //
    Message.m_FromGuid  = Sender.getGuid();
    Message.m_DeltaTime = DeltaTime;
    Message.m_ToGuid    = getGuid();    

    //
    // Send it to the right component
    //
	s32 iComponent;
	for( iComponent = 0; m_pComponents[iComponent]; iComponent++ )
	{
		if( &m_pComponents[iComponent]->getType() == &Type )
			break;
	}

    // Check whether we have that type of component
    // if so just send the message
    if( m_pComponents[iComponent] )
    {
        m_pComponents[iComponent]->sendMessage( Message );
    }
}

//------------------------------------------------------------------------------

void gb_entity::enableServer( void )
{
    // Force the Entity to be mark as a server
    m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_CLIENT );

    //
    // Send Messages to all the components
    //
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        gb_component_type& Type = (*pCom)->getType();
        if( x_FlagIsOn( Type.getFlags(), gb_component_type::FLAGS_NETWORK ) )
        {
            if( x_FlagIsOn( Type.getFlags(), gb_component_type::FLAGS_CLIENT ) )
            {
                (*pCom)->msgDeactivate();
            }
            else
            {
                (*pCom)->msgActivate();
            }
        }
    }
}

//------------------------------------------------------------------------------

void gb_entity::enableClient( void )
{
    // Force the Entity to be mark as a server
    m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_CLIENT );

    //
    // Send Messages to all the components
    //
    for( gb_component** pCom = m_pComponents; *pCom; pCom++ )
    {
        gb_component_type& Type = (*pCom)->getType();
        if( x_FlagIsOn( Type.getFlags(), gb_component_type::FLAGS_NETWORK ) )
        {
            if( x_FlagIsOn( Type.getFlags(), gb_component_type::FLAGS_CLIENT ) )
            {
                (*pCom)->msgActivate();
            }
            else
            {
                (*pCom)->msgDeactivate();
            }
        }
    }
}
