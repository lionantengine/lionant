#ifndef GB_COMPONENT_MANAGER_INLINE_H
#define GB_COMPONENT_MANAGER_INLINE_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// JOBLIST COMPONENT MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

template< class T > inline
void gb_joblist_component_mgr::BuidlJobLists( xarray<T>& lJob )
{
    if( !m_bDirty ) return;

    s32                 TotalInJob            = MAX_COMPONENTS_PER_JOB;
    simple_job*         pJob                  = NULL;   

    // Reset our jobs
    lJob.DeleteAllNodes();

    // Start processing
    for( s32 i=0; i<m_lActiveType.getCount(); i++ )
    {
        s32                 iType           = m_lActiveType[i];
        s32                 TypeCount       = g_ComponentTypeMgr.GetActiveCount(iType);
        s32                 TypeTaken       = 0;
        component_list*     pComponentList  = NULL;
        s32                 iLastType       = -1;

        // If we do not have anything active in this list then there is nothing to do for this type
        if( TypeCount == 0 ) continue;

        // Create a new job when we have the maximun components to run
        while( TypeTaken < TypeCount )
        {
            if( TotalInJob >= MAX_COMPONENTS_PER_JOB )
            {
                TotalInJob            = 0;
                pJob                  = &lJob.append(); 
                iLastType             = -1;
                ASSERT( iLastType != iType );
            }

            if( iLastType != iType )
            {
                iLastType             = iType;
                pComponentList        = &pJob->m_lCompList.append();
            }

            ASSERT(pComponentList);
            pComponentList->m_plComponent = &g_ComponentTypeMgr.GetComponentList(iType);
            pComponentList->m_iStart      = TypeTaken;
            pComponentList->m_Count       = x_Min( (s32)MAX_COMPONENTS_PER_JOB, TypeCount - TypeTaken ); 

            TotalInJob += pComponentList->m_Count;
            TypeTaken  += pComponentList->m_Count;
        }
    }

    // Done with the update
    m_bDirty = FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif