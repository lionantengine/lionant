//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

//===============================================================================
// GLOBAL
//===============================================================================
gb_entity_mgr g_EntityMgr("EntityMgr");

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ENTITY TYPE MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------- 
// TODO: ***HACK*** This is not thread safe
gb_entity* gb_entity_mgr::getEntity( xguid gEntity )
{
    return m_GuidLookup.GetValueOrNull( gEntity );
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::msgRemoveEntity( xguid gEntity )
{
    gb_entity* pEntity = getEntity( gEntity );
    ASSERT(pEntity);
    msgRemoveEntity( *pEntity );
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::msgRemoveEntity( gb_entity& Entity )
{
    ASSERT(Entity.m_pComponents);
    ASSERT(Entity.m_pComponents[0]);

    //
    // If this entity is mark to be deleted and is already deleted then ignore the message
    //
    while(1)
    {
        x_qt_flags LocalRealityFlags = Entity.m_qtAttrFlags;
        X_LWSYNC;
        if( LocalRealityFlags.isFlagOn( gb_component::QT_ATTR_DELETED ) )
            return;

        X_LWSYNC;
        // Lets set the flag to be deleted
        if( Entity.m_qtAttrFlags.qtSetFlagsOn( gb_component::QT_ATTR_DELETED, LocalRealityFlags ) )
            break;
    }

    // Okay now we own the reality of deleting an entity... hurray!

    //
    // Add the components to each of their managers
    //
    for( gb_component** pNext = Entity.m_pComponents; *pNext; pNext++ )
    {
        gb_base_component_mgr&   Mgr     = gb_base_component_mgr::SafeCast( (*pNext)->getType().getManager() );

        // This should be set on in theory
        ASSERT( (*pNext)->m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ));

        // Make sure every component knows is getting deleted
        (*pNext)->m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_DELETED );

        // Mark the component as not been part of the world now
        (*pNext)->m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_IN_WORLD );

        // Add the component to the actual manager
        Mgr.msgDeleteComponent( **pNext );
    }
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::linearDeleteEntity( xguid gEntity )
{
    gb_entity* pEntity = getEntity( gEntity );
    ASSERT(pEntity);
    linearDeleteEntity( *pEntity );
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::linearDeleteEntity( gb_entity& Entity )
{
    // Find how many components we have
    s32 nCounponents=0;
    for( gb_component** pNext = Entity.m_pComponents; *pNext; pNext++ )
    {
        nCounponents++;
    }

    // Then delete them backwards since the [0] is the entity itself and owns the table
    for( s32 i=nCounponents-1; i>=0; i-- )
    {
        gb_component&            Comp = *Entity.m_pComponents[i];
        gb_base_component_mgr&   Mgr  = gb_base_component_mgr::SafeCast( Comp.getType().getManager() );

        // This should be set on in theory
        ASSERT( Comp.m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ));

        // Make sure every component knows is getting deleted
        Comp.m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_DELETED );

        // Mark the component as not been part of the world now
        Comp.m_qtAttrFlags.qtForceSetFlagsOff( gb_component::QT_ATTR_IN_WORLD );

        // Add the component to the actual manager
        Mgr.linearDeleteComponent( Comp );
    }
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::onCleanUp( void )
{
    //
    // Remove the guid entry from the table
    //
    for( s32 i=0; i<m_lDelete.getCount(); i++ )
    {
        gb_entity& Entity = gb_entity::SafeCast( *(gb_entity*)m_lDelete[i].m_pComponent );
        m_GuidLookup.Delete( Entity.getGuid() );
    }

    // Clean up officially 
    gb_joblist_component_mgr::onClearUp();
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::onDeleteComponent( const gb_mgr_base_msg& Msg )
{
    //gb_entity& Entity = gb_entity::SafeCast( *(gb_entity*)Msg.m_Arg[0].m_pPointer );

    //
    // Now let my parent handle the rest of the dirty business
    //
    gb_joblist_component_mgr::onDeleteComponent( Msg );
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::onAppendComponent( const gb_mgr_base_msg& Msg )
{
    gb_entity& Entity = gb_entity::SafeCast( *(gb_entity*)Msg.m_Arg[0].m_pPointer );

    //
    // Remove the guid entry from the table
    //
    m_GuidLookup.Add( Entity.getGuid(), &Entity );

    //
    // Now let my parent handle the rest of the dirty business
    //
    gb_joblist_component_mgr::onAppendComponent( Msg );
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::msgAppendEntity( gb_entity& Entity )
{
    ASSERT(Entity.m_pComponents);
    ASSERT(Entity.m_pComponents[0]);

    //
    // If this entity is mark to be added in the world if the flag is set
    // we will assume another thread beat us at doing so.
    //
    while(1)
    {
        x_qt_flags LocalRealityFlags = Entity.m_qtAttrFlags;
        if( LocalRealityFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ) )
            return;

        // Lets set the flag to be put in the world
        if( Entity.m_qtAttrFlags.qtSetFlagsOn( gb_component::QT_ATTR_IN_WORLD, LocalRealityFlags ) )
            break;
    }

    // Okay now we own the reality of appending the entity... hurray!

    //
    // Determine whether we are is playback mode. If so we must force the
    // entity to run as a client rather than a server.
    //
    if( g_ReplayMgr.isPlayingBack() )
    {
        Entity.m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_CLIENT );
    }

    //
    // Add the components to each of their managers
    //
    for( gb_component** pNext = Entity.m_pComponents; *pNext; pNext++ )
    {
        gb_base_component_mgr&   Mgr     = gb_base_component_mgr::SafeCast( (*pNext)->getType().getManager() );

        // Mark the component as been part of the world now
        // Note that this will happen doring the frame so some parts of the
        // entity will get updated this frame other parts will be next frame.
        (*pNext)->m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_IN_WORLD );

        // Make sure that both buffers are set
        (*pNext)->SetupContexTransferC02C1();

        // Add the component to the actual manager
        Mgr.msgAppendComponent( **pNext );
    }
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::linearAppendEntity( gb_entity& Entity )
{
    // Make sure we are not in some strange mode...
    ASSERT( g_ReplayMgr.isPlayingBack() == FALSE );

    //
    // Add the components to each of their managers
    //
    for( gb_component** pNext = Entity.m_pComponents; *pNext; pNext++ )
    {
        gb_base_component_mgr&   Mgr     = gb_base_component_mgr::SafeCast( (*pNext)->getType().getManager() );

        // Mark the component as been part of the world now
        // Note that this will happen doring the frame so some parts of the
        // entity will get updated this frame other parts will be next frame.
        (*pNext)->m_qtAttrFlags.qtForceSetFlagsOn( gb_component::QT_ATTR_IN_WORLD );

        // Make sure that both buffers are set
        (*pNext)->SetupContexTransferC02C1();

        // Add the component to the actual manager
        Mgr.linearAppendComponent( **pNext );
    }
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::msgCreateEntity( xguid gBluePrint, const xtransform& Transform )
{
    xarray<gb_entity*> Entities;
    
    // First create the entities
    g_BlueprintMgr.createEntities( Entities, gBluePrint, Transform );

    // Then we must assign guids and set them into the world
    for( s32 i=0; i<Entities.getCount(); i++)
    {
        gb_entity&  Entity = *Entities[i];

        // Here we skip any setup for the entity

        // Set the components of the entity into each manager
        msgAppendEntity(Entity);
    }
}

//------------------------------------------------------------------------------- 

void gb_entity_mgr::onUpdate( void )
{
    // ASSERT( g_order == 3 ); g_order = 4;

    // Try to build new jobs if we have to
    BuidlJobLists(m_lJobs);

    // Now we can issue them
    for( s32 i=0; i<m_lJobs.getCount(); i++ )
    {
        gb_joblist_component_mgr::simple_job& Job = m_lJobs[i];
        
        // First inser the trigger since we can not touch the job after is in the quatum world
        m_pSyncPoint->AndWaitFor( Job );

        // Inser the job into the quantum world
        g_Scheduler.StartJobChain( Job );
    }
}

