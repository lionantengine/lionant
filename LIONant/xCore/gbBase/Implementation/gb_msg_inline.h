#ifndef GB_MSG_INLINE_H
#define GB_MSG_INLINE_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// FOWARD DECLARATIONS
//////////////////////////////////////////////////////////////////////////////////
class gb_component;

//////////////////////////////////////////////////////////////////////////////////
// MESSAGE SYSTEM MACROS
//////////////////////////////////////////////////////////////////////////////////

#define GM_MESSAGE_MAP_DECL(classname,parentclass)                                                                  \
public:                                                                                                             \
    typedef classname                   __thisclass;                                                                \
    typedef parentclass                 __parentclass;                                                              \
    typedef parentclass::__baseclass    __baseclass;                                                                \

//////////////////////////////////////////////////////////////////////////////////
// MESSAGE MACROS
//////////////////////////////////////////////////////////////////////////////////
#undef GB_MSG_UID

#define GB_MSG_UID(theclass)                                   \
    x_rtti_class1(theclass, gb_msg)                                             


//////////////////////////////////////////////////////////////////////////////////
// MESSAGE MAP MACROS
//////////////////////////////////////////////////////////////////////////////////
#undef GB_BEGIN_MSG_MAP
#undef GB_MSG_MAP_RANGE
#undef GB_MSG_MAP_ENTRY
#undef GB_END_MSG_MAP

//-------------------------------------------------------------------------------

#define GB_CLASS_TYPE(theclass,typeflags)                                                \
    theclass::type theclass::s_Type(typeflags);                                             

//-------------------------------------------------------------------------------
// They are actually defined in gb_component_inline because of a dependency with the base component
//-------------------------------------------------------------------------------
struct gb_msg_map_full;
struct gb_msg_map;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif