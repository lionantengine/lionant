//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"


gb_base_mgr* gb_base_mgr::s_pGMLManagerList = NULL;

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// MANAGER BASE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
void gb_base_mgr::InitAll( void )
{
    //
    // Initialize all the types
    //
    g_ComponentTypeMgr.vInit();

    //
    // Initialize the playback system
    //
    g_ReplayMgr.Init();

    //
    // Go throw the list of all managers and initialize them
    //
    for( gb_base_mgr* pNext = s_pGMLManagerList; pNext; pNext = pNext->m_pGMLNext )
    {
        pNext->vInit();
    }
}

//-------------------------------------------------------------------------------

void gb_base_mgr::KillAll( void )
{
}

//-------------------------------------------------------------------------------
gb_base_mgr::gb_base_mgr( const char* pName ) : 
    m_pName( pName ),
    m_pLocalNext( NULL ),
    m_pSyncPoint( NULL )
{
    //
    // Link list all the managers
    //
    m_pGMLNext        = s_pGMLManagerList;
    s_pGMLManagerList = this;
    x_PropertyMgrRegister( pName, this );
}

//-------------------------------------------------------------------------------
gb_base_mgr::~gb_base_mgr( void )
{

}

//-------------------------------------------------------------------------------

void gb_base_mgr::vInit( void )
{
}

//-------------------------------------------------------------------------------

void gb_base_mgr::vKill( void )
{

}

//-------------------------------------------------------------------------------

void gb_base_mgr::vReset( void )
{ 
    x_simple_job<1>::vReset(); 
    //printf("%s : manager\n", m_pName );
}

//-------------------------------------------------------------------------------

void gb_base_mgr::sendMessage( const gb_mgr_base_msg& Message )
{
    // Remove the constantness since the pointers in the base will be change.
    // but this is a detail on the container and should not affect the end user
    gb_mgr_base_msg& Ptr = const_cast<gb_mgr_base_msg&>( Message );
    
    // Push it into the queue
    m_MessageQueue.push( &Ptr );
}


//-------------------------------------------------------------------------------

void gb_base_mgr::onDispatchMessages( void )
{
    //
    // Handle Messages
    //
    gb_mgr_base_msg* pMsg;
    while( NULL != (pMsg = (gb_mgr_base_msg*)m_MessageQueue.pop()) )
    {
        MessageHandler(*pMsg);
    }
}

//-------------------------------------------------------------------------------
// This function is the actual job part of the class. Its mission is to deliver
// the messages that they were acumulated during the execution.
// and to call the actual update function
void gb_base_mgr::onRun( void )
{
    //
    // Let the system know that we have moved on
    //
    m_ProcessTick = m_pSyncPoint->getFrameTick();

    //
    // Handle the delete of components and other things
    // 
    onClearUp();

    //
    // Handle all the messages
    //
    onDispatchMessages();

    //
    // Call the official update function
    //
    onUpdate();
}

//-------------------------------------------------------------------------------

void gb_base_mgr::onPropEnum( xproperty_enum&  E  ) const
{
    s32 iScope = E.AddScope( "Base", "Base properties for the game managers." );

    g_PropString.Enum( E, iScope, "ManagerName", 
        "This is the name of the manager. Each manger has a unique name that identifies it.", 
        PROP_FLAGS_READ_ONLY ); 
}

//-------------------------------------------------------------------------------

xbool gb_base_mgr::onPropQuery( xproperty_query& Q )
{
    if( Q.Scope( "Base" ) )
    {
        if( Q.Var( "ManagerName" ) && g_PropString.QueryROnly( Q, m_pName ) )
            return TRUE;
    }

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SYNC POINT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
void gb_sync_point::vReset( void )
{
    x_simple_trigger<8>::vReset(); 
    //printf("%s : syncpoint\n", m_pName );
}

//-------------------------------------------------------------------------------

void gb_sync_point::setup( const char* pName, gb_cloud_state& State, const x_va_list& Dependencies )
{
    setup( pName, State, &Dependencies, NULL );
}

//-------------------------------------------------------------------------------

void gb_sync_point::setup( const char* pName, gb_cloud_state& State, const x_va_list& Dependencies, const x_va_list& Managers )
{
    setup( pName, State, &Dependencies, &Managers );
}

//-------------------------------------------------------------------------------

void gb_sync_point::setup( const char* pName, gb_cloud_state& State, const x_va_list* pDependencies, const x_va_list* pManagers )
{
    m_pName         = pName;

    //
    // Add this node into the state link list
    //
    m_pExecNext             = State.m_pSyncPExecPath;
    State.m_pSyncPExecPath  = this;

    //
    // Add the dependencies
    //
    if( pDependencies )
    {
        for( s32 i=0; i<pDependencies->length(); i++ )
        {
            // make sure that it looks like a pointer
            ASSERT( (*pDependencies)[i].isXUPtr() );

            // get the value 
            gb_sync_point& SyncPoint = *( (gb_sync_point*) ((xuptr)(*pDependencies)[i]) );

            // make sure that is is the right type of object
            ASSERT( SyncPoint.isKindOf( gb_sync_point::getRTTI() ) );

            // Added to our link list of managers
            m_lDependency[m_nDependencies++] = &SyncPoint;
        }
    }

    //
    // Update the managers
    //
    m_pManager = NULL;
    if( pManagers )
    {
        for( s32 i=0; i<pManagers->length(); i++ )
        {
            // make sure that it looks like a pointer
            ASSERT( (*pManagers)[i].isXUPtr() );

            // get the value 
            gb_base_mgr& Manager = *( (gb_base_mgr*) ((xuptr)(*pManagers)[i]) );

            // make sure that is is the right type of object
            ASSERT( Manager.isKindOf( gb_base_mgr::getRTTI() ) );

            // Added to our link list of managers
            Manager.m_pLocalNext = m_pManager;
            m_pManager = &Manager;

            // Add the sync point pointer
            Manager.m_pSyncPoint = this;
        }

        // Add to the total manager count
        State.m_nManagers += pManagers->length();
    }

    // We are also going to count the sync points
    State.m_nManagers ++;
}

//-------------------------------------------------------------------------------

gb_sync_point::~gb_sync_point( void )
{
}

//-------------------------------------------------------------------------------

void gb_sync_point::doBeforeTrigger( void )
{
    // Updating out T0 to be our new T1
    m_TickCount = m_FrameTick;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// CLOUD STATE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

gb_cloud_state* gb_cloud_state::m_pFirstState = NULL;

//-------------------------------------------------------------------------------

void gb_cloud_state::post_end_sync_point::vReset( void )
{
    g_order = 888;

    //
    // Make sure we clean our end point
    //
    gb_sync_point::vReset();

    //
    // Now we can see if we want to loop again
    //
    if( m_bLoop )
    {
        // Make sure to set the state of this before heading into the quantum world
        // This just says the the default looping state is to just loop ones
        m_bLoop = FALSE;

        // Note this puts the hold system back in the quantum world
        g_StateMgr.ExecuteFrame();
    }    
}

//-------------------------------------------------------------------------------

gb_cloud_state::gb_cloud_state( void ) 
{
    m_pFirstState = this;

    m_PostEndSyncP.m_pState = this;
    m_PostEndSyncP.m_bLoop  = FALSE;
}

//-------------------------------------------------------------------------------

gb_cloud_state::~gb_cloud_state( void )
{
}

//-------------------------------------------------------------------------------
// * This function will rearrange the next pointers base on a execution path which
//   allows the dependencies to have information of triggers if this is needed.
// * The path itself is a form of prioritazion for the jobs. Since at the end there is 
//   a job queue and jobs are taken out of the order that they are push in.
//   Farther optimization could be done for the path such identifind which nodes have
//   larger dependencies trees and prioritize those over shorter branches. 
struct temp_node
{
    gb_sync_point*                  m_pSyncPoint;       // Which sync point we are talking about
    s32                             m_Level;            // How far is from the start node
    s32                             m_nDependencies;    // Number of dependencies
    xsafe_array<gb_sync_point*,10>  m_lDependency;      // List of sync points that we are dependent on
};

static 
s32 CompareLevels( const void* pA, const void* pB )
{
    const temp_node& A = *((const temp_node*)pA);
    const temp_node& B = *((const temp_node*)pB);
    if( A.m_Level < B.m_Level ) return -1;
    return A.m_Level > B.m_Level;
}

s32 gb_cloud_state::findLongestPath( gb_sync_point& SyncPoint, s32 c ) const
{
    if( &SyncPoint == &m_StartSyncP )
        return c;

    s32 Longest = c;
    for( s32 i=0; i<SyncPoint.m_nDependencies; i++ )
    {
        s32 L = findLongestPath( *SyncPoint.m_lDependency[i], c+1 );
        if( L > Longest )
            Longest = L;
    }

    return Longest;
}

void gb_cloud_state::BuildProcessingPath( void )
{
    //
    // First count all the nodes that we are going to have for this state
    //
    s32 Count = 0;                                                                                  
    for( gb_sync_point* pNext = m_pSyncPExecPath; pNext; pNext = pNext->m_pExecNext )
    {
        Count++;
    }

    //
    // Okay lets create a buffer of the right size for this
    //
    xptr<temp_node> lTempNode;
    lTempNode.Alloc( Count );

    //
    // Lets fill all the information for each node
    //
    Count = 0;
    for( gb_sync_point* pNext = m_pSyncPExecPath; pNext; pNext = pNext->m_pExecNext, Count++ )
    {
        lTempNode[Count].m_pSyncPoint       = pNext;
        lTempNode[Count].m_Level            = -1;
        lTempNode[Count].m_nDependencies    = -1;
    }

    //---------------------------------------------------------------------------
    // Create execution path
    //---------------------------------------------------------------------------

    //
    // Now lets start walking its dependency tree and count to the start node
    //
    for( s32 Index = 0; Index < Count; Index++ )
    {
        temp_node& TempNode = lTempNode[Index];

        // Check which is the longest path to the start node.
        // Because this will be the place where it will need to sync with other dependencies
        s32 Level = findLongestPath( *TempNode.m_pSyncPoint, 0 );

        // Deal with some special cases 
        if( TempNode.m_pSyncPoint == &m_EndSyncP )
        {
            Level = 10000000;
        }
        else if( TempNode.m_pSyncPoint == &m_PostEndSyncP )
        {
            Level = 20000000;
        }
        else if( Level == 0 )
        {
            // If we get a level zero it better be the start node. If not it is a node that has not
            // dependencies which is impossible because at a minimun it should the the start dependency
            ASSERT( TempNode.m_pSyncPoint == &m_StartSyncP );
        }
          
        TempNode.m_Level = Level;
    }

    // Sort the list by the level
    x_qsort((void*)&lTempNode[0], Count, sizeof(temp_node), CompareLevels);

    //
    // Now we should be able to setup the Next Pointer if the right way
    //
    m_pSyncPExecPath = lTempNode[0].m_pSyncPoint;
    for( s32 i = 0; i < Count-1; i++ )
    {
        lTempNode[i].m_pSyncPoint->m_pExecNext = lTempNode[i+1].m_pSyncPoint;
    }

    // Add also the post end node as part of the execution (The last node of course)
    lTempNode[Count-1].m_pSyncPoint->m_pExecNext    = NULL;
    ASSERT( lTempNode[Count-1].m_pSyncPoint == &m_PostEndSyncP );

    //---------------------------------------------------------------------------
    // Revert all the dependencies
    //---------------------------------------------------------------------------

    //
    // First back up the dependencies for all the nodes
    // and Zero out all the dependencies in the real nodes
    //
    for( s32 Index = 0; Index < Count; Index++ )
    {
        temp_node& TempNode = lTempNode[Index];

        TempNode.m_nDependencies = TempNode.m_pSyncPoint->m_nDependencies;
        for( s32 i=0; i<TempNode.m_pSyncPoint->m_nDependencies; i++ )
        {
            TempNode.m_lDependency[i] = TempNode.m_pSyncPoint->m_lDependency[i];
        }

        // Zero out the dependencies in the actual nodes
        TempNode.m_pSyncPoint->m_nDependencies = 0;
    }

    //
    // Start adding dependencies to nodes
    //
    for( s32 Index = 0; Index < Count; Index++ )
    {
        temp_node& TempNode = lTempNode[Index];

        for( s32 i=0; i<TempNode.m_nDependencies; i++ )
        {
            gb_sync_point*  pSynD = TempNode.m_lDependency[i];
            s32             iDep  = pSynD->m_nDependencies;

            pSynD->m_lDependency[iDep] = TempNode.m_pSyncPoint;
            pSynD->m_nDependencies++;
        }
    }

    //
    // Add the end into the references. All nodes without a reference must be the end of the graph.
    // So we connect all those nodes to the end node.
    //
    s32 CountEnds = 0;
    for( s32 Index = 0; Index < Count; Index++ )
    {
        gb_sync_point*  pSynD    = lTempNode[Index].m_pSyncPoint;
        
        // skip the end last system sync point
        if( pSynD == &m_PostEndSyncP )
            continue;

        if( pSynD->m_nDependencies == 0 )
        {
            pSynD->m_lDependency[ pSynD->m_nDependencies++ ] = &m_EndSyncP;
            CountEnds++;
        }
    }

    // It must be at least one end in the graph to be correct
    ASSERT( CountEnds > 0 );
}

//-------------------------------------------------------------------------------

void gb_cloud_state::setupBeging( const char* pName )   
{ 
    //
    // Setup the name of the state
    //
    m_pStateName = pName;

    //
    // First register the state it self in the graph
    // 
    x_va_list va_list((xuptr)&g_StateMgr);
    m_StartSyncP.setup( "Start", *this, NULL, &va_list ); 
}

//-------------------------------------------------------------------------------

void gb_cloud_state::setupEnd( void )
{
    //
    // Fill some key information for the end sync point
    //
    x_va_list va_list1((xuptr)&g_GameMgr);
    x_va_list va_list2((xuptr)&m_EndSyncP);
    x_va_list va_list3((xuptr)&g_NetworkMgr);
    m_EndSyncP.setup    ( "End",        *this,  NULL,        &va_list1 );
    m_PostEndSyncP.setup( "PostEnd",    *this,  &va_list2,   &va_list3 );

    //
    // Build the processing path for this state
    //
    BuildProcessingPath();
}

//-------------------------------------------------------------------------------

gb_cloud_state* gb_cloud_state::findState( const char* pName ) const
{
    for( gb_cloud_state* pNext = m_pFirstState; pNext; pNext = pNext->m_pStateNext )
    {
        if( 0 == x_strcmp( m_pStateName, pNext->m_pStateName ) )
            return pNext;
    }

    return NULL;
}

