#ifndef GB_COMPONENT_INLINE_H
#define GB_COMPONENT_INLINE_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// RTTI OBJECT
////////////////////////////////////////////////////////////////////////////////// 
#undef GB_COMPONENT_RTTI
#undef GB_COMPONENT_RTTI_NO_CONTEXT

//-------------------------------------------------------------------------------

#define _GB_BUILD_CONTEXT(theclass, parentclass)                                                                                                     \
public:                                                                                                                                              \
    theclass::context&               getSetupContext      ( void )            { return *(theclass::context*)m_pT[0]; }                               \
    const theclass::context&         getReadContextLatest ( void )   const    { return *((theclass::context*)&parentclass::getReadContextLatest()); }\
    const theclass::context&         getReadContextTx     ( s32 Tx ) const    { return *((theclass::context*)&parentclass::getReadContextTx(Tx)); }  \
    theclass::context&               getWriteContextT1    ( void )            { return *((theclass::context*)&parentclass::getWriteContextT1()); }   \
    theclass::context&               getRawContext        ( s32 Tx )          { return *(theclass::context*)m_pT[Tx]; }                              \
    const theclass::context&         getRawContext        ( s32 Tx ) const    { return *(theclass::context*)m_pT[Tx]; }                              \
    virtual void                     DestroyContextBuffers( void ) { if(m_pT[0]) { x_delete( (theclass::context*)m_pT[0] ); m_pT[0]=m_pT[1]=NULL; }} \
    virtual void                     BuildContextBuffers  ( void )                                                                                   \
    {                                                                                                                                                \
        theclass::context* pT = x_new(theclass::context, 2, XMEM_FLAG_ALIGN_16B);                                                                    \
        m_pT[0] = &pT[0]; m_pT[1] = &pT[1];                                                                                                          \
    }																																				

//-------------------------------------------------------------------------------

#define _GB_COMPONENT_RTTI_1( theclass, parentclass )                                                               \
public:                                                                                                             \
    typedef theclass __theclass;                                                                                    \
    typedef parentclass __theparentclass;                                                                           \
    x_rtti_class1( theclass, parentclass );                                                                         \
    GM_MESSAGE_MAP_DECL( theclass, parentclass )																	\
	virtual void MessageHandler2( gb_msg& Msg ) { (this->*Msg.m_MessageFunc)(Msg);}													

#define _GB_COMPONENT_RTTI_2( theclass, parentclass, interface2 )                                                   \
public:                                                                                                             \
    typedef theclass __theclass;                                                                                    \
    typedef parentclass __theparentclass;                                                                           \
    x_rtti_class2( theclass, parentclass, interface2 );                                                             \
    GM_MESSAGE_MAP_DECL( theclass, parentclass )                                                                    \
	virtual void MessageHandler2( gb_msg& Msg ) { (this->*Msg.m_MessageFunc)(Msg);}													

#define _GB_COMPONENT_RTTI_3( theclass, parentclass, interface2, interface3 )                                       \
public:                                                                                                             \
    typedef theclass __theclass;                                                                                    \
    typedef parentclass __theparentclass;                                                                           \
    x_rtti_class3( theclass, parentclass, interface2, interface3 );                                                 \
    GM_MESSAGE_MAP_DECL( theclass, parentclass )                                                                    \
	virtual void MessageHandler2( gb_msg& Msg ) { (this->*Msg.m_MessageFunc)(Msg);}													


#define _GB_COMPONENT_CONTEXT( theclass, parentclass )                                                              \
public:                                                                                                             \
    struct context;                                                                                                 \
    _GB_BUILD_CONTEXT( theclass, parentclass )                                                                       

#define _GB_COMPONENT_NO_CONTEXT( theclass, parentclass )                                                           \
public:                                                                                                             \
    typedef parentclass::context context;                                                                           \
    _GB_BUILD_CONTEXT( theclass, parentclass )                                                                       

//-------------------------------------------------------------------------------

#define _GB_COMPONENT_PRESETS( theclass, parentclass )             \
public:                                                            \
    struct presets;                                                \
    theclass::presets&               setupNewPresets    ( void ) { return *(theclass::presets*)&gb_component::setupNewPresets(); }                                                                             \
    const theclass::presets&         getPresets         ( void ) const { return *(theclass::presets*)m_pPresets; }                                                                       \
    virtual void                     DestroyPresetBuffer( void ) { if(m_pPresets) { if(m_pPresets->decRefCount()==0) x_delete( (theclass::context*)m_pPresets ); m_pPresets=NULL; }}     \
    virtual xbool                    hasPresets         ( void ) const                  { return TRUE; }                                                                                 

#define _GB_COMPONENT_NO_PRESETS( theclass, parentclass )                                                           \
public:                                                                                                             \
    typedef parentclass::presets presets;

//-------------------------------------------------------------------------------

#define GB_COMPONENT_RTTI1_CONTEXT_PRESETS(theclass, parentclass )                                                  \
    _GB_COMPONENT_RTTI_1(  theclass, parentclass )                                                                  \
    _GB_COMPONENT_PRESETS( theclass, parentclass )                                                                  \
    _GB_COMPONENT_CONTEXT( theclass, parentclass )                                                                  

#define GB_COMPONENT_RTTI1_CONTEXT(theclass, parentclass )                                                          \
    _GB_COMPONENT_RTTI_1(  theclass, parentclass )                                                                  \
    _GB_COMPONENT_NO_PRESETS( theclass, parentclass )                                                               \
    _GB_COMPONENT_CONTEXT( theclass, parentclass )                                                                   

#define GB_COMPONENT_RTTI1_PRESETS(theclass, parentclass )                                                          \
    _GB_COMPONENT_RTTI_1(  theclass, parentclass )                                                                  \
    _GB_COMPONENT_PRESETS( theclass, parentclass )                                                                  \
    _GB_COMPONENT_NO_CONTEXT( theclass, parentclass )                                                               

#define GB_COMPONENT_RTTI1(theclass, parentclass )                                                                  \
    _GB_COMPONENT_RTTI_1(  theclass, parentclass )                                                                  \
    _GB_COMPONENT_NO_PRESETS( theclass, parentclass )                                                               \
    _GB_COMPONENT_NO_CONTEXT( theclass, parentclass )                                                               \


#define GB_COMPONENT_RTTI2_CONTEXT_PRESETS(theclass, parentclass, interface2 )                                      \
    _GB_COMPONENT_RTTI_2(  theclass, parentclass, interface2 )                                                      \
    _GB_COMPONENT_PRESETS( theclass, parentclass )                                                                  \
    _GB_COMPONENT_CONTEXT( theclass, parentclass )                                                                  \


#define GB_COMPONENT_RTTI2_CONTEXT(theclass, parentclass, interface2 )                                              \
    _GB_COMPONENT_RTTI_2(  theclass, parentclass, interface2 )                                                      \
    _GB_COMPONENT_NO_PRESETS( theclass, parentclass )                                                               \
    _GB_COMPONENT_CONTEXT( theclass, parentclass )                                                                  

#define GB_COMPONENT_RTTI2_PRESETS(theclass, parentclass, interface2 )                                              \
    _GB_COMPONENT_RTTI_2(  theclass, parentclass, interface2 )                                                      \
    _GB_COMPONENT_PRESETS( theclass, parentclass )                                                                  \
    _GB_COMPONENT_NO_CONTEXT( theclass, parentclass )                                                               

#define GB_COMPONENT_RTTI2(theclass, parentclass, interface2 )                                                      \
    _GB_COMPONENT_RTTI_2(  theclass, parentclass, interface2 )                                                      \
    _GB_COMPONENT_NO_PRESETS( theclass, parentclass )                                                               \
    _GB_COMPONENT_NO_CONTEXT( theclass, parentclass )                                                               


#define _GM_BUILD_TYPE( parentclass, themanager, ALLOC_INSTANCE_FLAGS )                                             \
static class type : public parentclass                                                                              \
{                                                                                                                   \
public:                                                                                                             \
                                x_rtti_class1       ( type, parentclass );                                          \
                                type                ( u32 Flags ) : parentclass( Flags ){}                          \
    virtual const char*         getName             ( void ) const { return __theclass::getRTTI().getName(); }      \
    virtual void*               getVoidedManager    ( void ) const { return &themanager; }                          \
                                                                                                                    \
protected:                                                                                                          \
                                                                                                                    \
    virtual gb_component*           NewComponent        ( void ) const override { return x_new(__theclass, 1, ALLOC_INSTANCE_FLAGS ); }           \
    virtual void                    DeleteComponent     ( gb_component* pComponent ) const override { x_delete( &__theclass::SafeCast( *pComponent) ); }            \
    virtual gb_component::presets*  NewPresets          ( void ) const override { return x_new(__theclass::presets, 1, ALLOC_INSTANCE_FLAGS); }    \
} s_Type;


#define _GM_BUILD_TYPE2( parentclass, ALLOC_INSTANCE_FLAGS )                                                        \
static class type : public parentclass                                                                              \
{                                                                                                                   \
public:                                                                                                             \
                                x_rtti_class1       ( type, parentclass );                                          \
                                type                ( u32 Flags ) : parentclass( Flags ){}                          \
    virtual const char*         getName             ( void ) const { return __theclass::getRTTI().getName(); }      \
                                                                                                                    \
protected:                                                                                                          \
                                                                                                                    \
    virtual gb_component*       NewComponent        ( void ) const override { return x_new(__theclass, 1, ALLOC_INSTANCE_FLAGS ); }    \
    virtual void                DeleteComponent     ( gb_component* pComponent ) const override { x_delete( &__theclass::SafeCast( *pComponent) ); }    \
    virtual gb_component::presets*  NewPresets      ( void ) const override { return x_new(__theclass::presets, 1, ALLOC_INSTANCE_FLAGS); }    \
} s_Type;


#define GB_COMPONENT_TYPENEW( component_type, entity_mgr, ALLOC_INSTANCE_FLAGS )    \
    _GM_BUILD_TYPE( component_type, entity_mgr, ALLOC_INSTANCE_FLAGS )              \
    gb_component_type& getType( void ) const { return __theclass::s_Type; }

#define GB_COMPONENT_TYPEBASE( entity_mgr, ALLOC_INSTANCE_FLAGS )           \
    _GM_BUILD_TYPE( gb_component::type, entity_mgr, ALLOC_INSTANCE_FLAGS )  \
    gb_component_type& getType( void ) const { return __theclass::s_Type; }

#define GB_COMPONENT_TYPE(ALLOC_INSTANCE_FLAGS)                             \
    _GM_BUILD_TYPE2( __parentclass::type, ALLOC_INSTANCE_FLAGS )            \
    gb_component_type& getType( void ) const { return __theclass::s_Type; }

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

#define GB_MANAGER_RTTI(theclass, parentclass)                                                              \
public:                                                                                                     \
    x_rtti_class1( theclass, parentclass );                                                                 \
protected:                                                                                                  \
    virtual void MessageHandler( gb_mgr_base_msg& Msg ) { (this->*Msg.m_MessageFunc)(Msg); }                \
public:                                                                                                     


//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif

