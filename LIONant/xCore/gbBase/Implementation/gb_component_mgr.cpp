//////////////////////////////////////////////////////////////////////////
//                                                                      //
// LionAnt Engine, Copyright (C) LiontAnt                               //
// All rights reserved.  Email: info@lionant.com   Web: www.lionant     //
//                                                                      //
// This library is free software; you can redistribute it and/or        //
// modify it under the terms of EITHER:                                 //
//   (1) The GNU Lesser General Public License as published by the Free //
//       Software Foundation; either version 2.1 of the License, or (at //
//       your option) any later version. The text of the GNU Lesser     //
//       General Public License is included with this library in the    //
//       file LICENSE.TXT.                                              //
//   (2) The BSD-style license that is included with this library in    //
//       the file LICENSE-BSD.TXT.                                      //
//                                                                      //
// This library is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE COMPONENT MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void gb_base_component_mgr::vInit( void )
{
    //
    // Initialize the base stuff
    //
    gb_base_mgr::vInit();

    //
    // Collect all the components types which need update
    //
    g_ComponentTypeMgr.getAllActiveTypes( *this, m_lActiveType );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::linearAppendComponent( gb_component& Component )
{
    gb_mgr_base_msg Msg;
    Msg.m_Arg[0].m_pPointer = &Component;

    onAppendComponent( Msg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::msgAppendComponent( gb_component& Component )
{
    gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pMsg);
    pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_base_component_mgr::onAppendComponent);
    pMsg->m_Arg[0].m_pPointer = &Component;
    sendMessage( *pMsg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::linearDeleteComponent( gb_component& Component )
{
    g_ComponentTypeMgr.UnregisterComponent( Component );

    gb_component_type&  Type = Component.getType();

    Type.destroyComponent( Component );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::msgDeleteComponent( gb_component& Component )
{
    // Prepare all the message stuff
    gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pMsg);
    pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_base_component_mgr::onDeleteComponent);
    pMsg->m_Arg[0].m_pPointer   = &Component;
    pMsg->m_Arg[1].m_U64        = g_GameMgr.getFrameNumber();

    // Now we can send the message
    sendMessage( *pMsg );
//    m_DeleteMsgQueue.push( pMsg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::msgActivateComponent( gb_component& Component )
{
    // Prepare all the message stuff
    gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pMsg);
    pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_base_component_mgr::onActivateComponent);
    pMsg->m_Arg[0].m_pPointer   = &Component;
    pMsg->m_Arg[1].m_U64        = g_GameMgr.getFrameNumber();

    // Now we can send the message
    sendMessage( *pMsg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::msgDeactivateComponent( gb_component& Component )
{
    // Prepare all the message stuff
    gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pMsg);
    pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_base_component_mgr::onDeactivateComponent);
    pMsg->m_Arg[0].m_pPointer   = &Component;
    pMsg->m_Arg[1].m_U64        = g_GameMgr.getFrameNumber();

    // Now we can send the message
    sendMessage( *pMsg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::msgSetServerNetworkID( gb_component& Component, xhandle hNetworkID )
{
    // Prepare all the message stuff
    gb_mgr_base_msg* pMsg = x_new( gb_mgr_base_msg, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pMsg);
    pMsg->m_MessageFunc = gb_base_mgr::callback_fn(&gb_base_component_mgr::onSetServerNetworkID);
    pMsg->m_Arg[0].m_pPointer   = &Component;
    pMsg->m_Arg[1].m_U32[0]     = (u32)hNetworkID.m_Handle;

    // Now we can send the message
    sendMessage( *pMsg );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onDispatchMessages( void )
{
    //
    // Let it handle all normal messages
    //
    gb_base_mgr::onDispatchMessages();

    /*
    //
    // Handle all the delete Messages
    //
    gb_mgr_base_msg* pMsg;
    while( pMsg = (gb_mgr_base_msg*)m_DeleteMsgQueue.pop() )
    {
        MessageHandler(*pMsg);
    }
    */
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onClearUp( void )
{
    //
    // Lets release all the pending components that need to be delete
    //
    for( s32 i=0; i<m_lDelete.getCount(); i++ )
    {
        gb_component&       Comp = *m_lDelete[i].m_pComponent;
        gb_component_type&  Type = Comp.getType();

        Type.destroyComponent( Comp );
    }

    // Okay now we can clear out list
    m_lDelete.DeleteAllNodes();
}

//-------------------------------------------------------------------------------

s32 gb_base_component_mgr::getComponentCount( void ) const
{
    s32 Total = 0;
    for( s32 i=0; i<m_lActiveType.getCount(); i++ )
    {
        s32 iType = m_lActiveType[i];
        Total += g_ComponentTypeMgr.GetComponentList(iType).getCount();
    }

    return Total;
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onAppendComponent( const gb_mgr_base_msg& Msg )
{
    gb_component* pComponent = (gb_component*)Msg.m_Arg[0].m_pPointer;
    ASSERT(pComponent);

    // Register the component with the manager
    g_ComponentTypeMgr.RegisterComponent( *pComponent );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onDeleteComponent( const gb_mgr_base_msg& Msg )
{
    gb_component* pComponent = (gb_component*)Msg.m_Arg[0].m_pPointer;
    ASSERT(pComponent);

    // first unregister the component
    g_ComponentTypeMgr.UnregisterComponent( *pComponent );

    // then tell the 
    delete_node& DelNode = m_lDelete.append();
    DelNode.m_pComponent = pComponent;
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onActivateComponent( const gb_mgr_base_msg& Msg )
{
    gb_component* pComponent = (gb_component*)Msg.m_Arg[0].m_pPointer;
    ASSERT(pComponent);

    g_ComponentTypeMgr.ActivateComponent( *pComponent );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onDeactivateComponent( const gb_mgr_base_msg& Msg )
{
    gb_component* pComponent = (gb_component*)Msg.m_Arg[0].m_pPointer;
    ASSERT(pComponent);

    g_ComponentTypeMgr.DeactivateComponent( *pComponent );
}

//-------------------------------------------------------------------------------

void gb_base_component_mgr::onSetServerNetworkID( const gb_mgr_base_msg& Msg )
{
    gb_component* pComponent = (gb_component*)Msg.m_Arg[0].m_pPointer;
    ASSERT(pComponent);
    xhandle hNetworkID;
    hNetworkID.m_Handle = Msg.m_Arg[1].m_U32[0];
    g_ComponentTypeMgr.SetServerNetworkID( *pComponent, hNetworkID );
}

/////////////////////////////////////////////////////////////////////////////////
// JOB FROM THE JOBLIST COMPONENT MANAGER
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void gb_joblist_component_mgr::simple_job::onRun( void )
{
    for( s32 i=0; i<m_lCompList.getCount(); i++ )
    {
        component_list& CompList = m_lCompList[i];

        for( s32 j=0; j<CompList.m_Count; j++ )
        {
            gb_component& Comp = *(*CompList.m_plComponent)[CompList.m_iStart + j ];
            Comp.vUpdate();
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// JOBLIST COMPONENT MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

gb_joblist_component_mgr::gb_joblist_component_mgr( const char* pName ) : 
    gb_base_component_mgr( pName ) 
{
    m_bDirty = TRUE;
}

//-------------------------------------------------------------------------------

void gb_joblist_component_mgr::onAppendComponent( const gb_mgr_base_msg& Msg )
{
    gb_base_component_mgr::onAppendComponent( Msg );
    m_bDirty = TRUE;
}

//-------------------------------------------------------------------------------

void gb_joblist_component_mgr::onDeleteComponent( const gb_mgr_base_msg& Msg )
{
    gb_base_component_mgr::onDeleteComponent( Msg );
    m_bDirty = TRUE;
}

//-------------------------------------------------------------------------------

void gb_joblist_component_mgr::onActivateComponent( const gb_mgr_base_msg& Msg )
{
    gb_base_component_mgr::onActivateComponent( Msg );
    m_bDirty = TRUE;
}

//-------------------------------------------------------------------------------

void gb_joblist_component_mgr::onDeactivateComponent( const gb_mgr_base_msg& Msg )
{
    gb_base_component_mgr::onDeactivateComponent( Msg );
    m_bDirty = TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// DEFAULT COMPONENT MANAGER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
    
gb_default_component_mgr::gb_default_component_mgr( const char* pMgrName ) : 
    gb_joblist_component_mgr( pMgrName ), m_lJob( XMEM_FLAG_ALIGN_8B )
{ 
}

//-------------------------------------------------------------------------------

void gb_default_component_mgr::onUpdate( void )
{
    // See whether we need to build a new batch of jobs
    BuidlJobLists( m_lJob );

    ///// TODO: I think this is a bug since we should insert all jobs first then start them in the manager
    ////        because if not this could trigger the trigger faster than it should
    // Update the jobs with the latest info and issue them
    for( s32 i=0; i<m_lJob.getCount(); i++ )
    {
        gb_joblist_component_mgr::simple_job& Job = m_lJob[i];
        
        // First inser the trigger since we can not touch the job after is in the quatum world
        m_pSyncPoint->AndWaitFor( Job );

        // Inser the job into the quantum world
        g_Scheduler.StartJobChain( Job );
    }
}
