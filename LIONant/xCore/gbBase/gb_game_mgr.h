#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// Game Manager
//////////////////////////////////////////////////////////////////////////////////
class gb_game_mgr  : public gb_base_mgr
{
public:
        GB_MANAGER_RTTI( gb_game_mgr, gb_base_mgr );

                                gb_game_mgr         ( const char* pName );                                
                               ~gb_game_mgr         ( void ){}

    void                        msgStart            ( void );
    void                        msgStop             ( void );
    f32                         getDeltaTime        ( void ) const { return m_CurrentDeltaTime; }
    u64                         getFrameNumber      ( void ) const { return m_iFrameNumber;     }

protected:

    void                        vInit               ( void );
    virtual void                onUpdate            ( void );
    virtual void                onStart             ( void );
    virtual void                onStop              ( void );

system_public:

            void                ComputeDeltaTime    ( void );


protected:

    xbool                       m_IsRunning;
    xbool                       m_IsShutdown;
    xbool                       m_IsDirty;
    xbool                       m_Inited;

    f64                         m_CurrentFrameTime;     // Current Game Time
    f64                         m_LastFrameTime;        // Last Frame Time    
    f32                         m_CurrentDeltaTime;     // Current delta time
    u64                         m_iFrameNumber;         // How many 1/60 of a seconds has passed
};

extern gb_game_mgr g_GameMgr;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif