#ifndef GB_ENTITY_MGR_H
#define GB_ENTITY_MGR_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "gb_Entity.h"

/////////////////////////////////////////////////////////////////////////////////
// ENTITY MANAGER
/////////////////////////////////////////////////////////////////////////////////
class gb_entity_mgr : public gb_joblist_component_mgr
{
public:

                                    GB_MANAGER_RTTI( gb_entity_mgr, gb_joblist_component_mgr );

                                    gb_entity_mgr       ( const char* pName ) : gb_joblist_component_mgr( pName ), m_lJobs(XMEM_FLAG_ALIGN_8B) {}
    virtual                        ~gb_entity_mgr       ( void ) {}

    void                            linearAppendEntity  ( gb_entity& Entity );
    void                            linearDeleteEntity  ( gb_entity& Entity );
    void                            linearDeleteEntity  ( xguid Guid );
    void                            msgCreateEntity     ( xguid gBluePrint, const xtransform& Transform );
    void                            msgAppendEntity     ( gb_entity& Entity );
    void                            msgRemoveEntity     ( gb_entity& Entity );
    void                            msgRemoveEntity     ( xguid gEntity );
    gb_entity*                      getEntity           ( xguid gEntity );

system_protected:

    virtual void                    onUpdate            ( void );
    virtual void                    onDeleteComponent   ( const gb_mgr_base_msg& Msg );
    virtual void                    onAppendComponent   ( const gb_mgr_base_msg& Msg );
    virtual void                    onCleanUp           ( void );

protected:

    xghash<gb_entity*>                              m_GuidLookup;
    xarray<gb_joblist_component_mgr::simple_job>    m_lJobs;
};

extern gb_entity_mgr g_EntityMgr;

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif