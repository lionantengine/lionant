#ifndef GB_MSG_H
#define GB_MSG_H
//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//                                                                      //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// KEY CONCEPTS:
//--------------------------------------------------------------------------------
// MessageGroup: Every message type belongs to a group. A message group is really design to solve
//               some key problems; first It allows at compile time to give each message type a unique
//               contigious id. This in turns allows for very quick access to its handler, not need to search.
//               The Second problem that it solves is that each group may or may not be use in an Entity.
//               When not messages from a group are used it helps to save memory and to quickly answer 
//               the question on whether we have a handler for those messages.
//               You should group messages base on related functionality. For instance if you have a rendering
//               component you probably want to group all the rendeing messages inside the same group.
//               The final (compiled) Entity message map looks something like this:
//
//               mg - MessageGroup  | nul - NULL (empty) | cb - CallBack inside a component | i - Index to the component table
//               Entity->+---+---+   +---+---+---+---+---+---+
//                       | i |mg |-->|nul|cb |cb |nul|nul|cb |
//                       +---+---+   +---+---+---+---+---+---+
//                       | i |nul|-->
//                       +---+---+   +---+---+---+
//                       | i |mg |-->|nul|nul|cb |
//                       +---+---+   +---+---+---+---+---+
//                       | i |mg |-->|nul|cb |cb |nul|cb |
//                       +---+---+   +---+---+---+---+---+
//                       | i |nul|-->
//                       +---+---+
//
//              This should also explain why you can only have one component type per Entity.
//              because as you see if two had two component types that had the same message group
//              Their entries will overwrite eachother.
//              To handle a message is very fast. A message has two virtual functions that will answer:
//                  a - Which message group it belongs
//                  b - Which entry inside that group it belongs
//              so first check its message group and see whether it has messages if so index to its entry and see if it null.
//              if not get (i) from the messagegroup table and get the instace of the component from the component table.
//              Ones that you can call the callback. This basically give a O(1) performance for messasge handling with some memory wasted ofcourse... 
//
//////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// FOWARD DECLARATIONS
//////////////////////////////////////////////////////////////////////////////////
class  gb_world;
                                  
//////////////////////////////////////////////////////////////////////////////////
// gb_msg
//////////////////////////////////////////////////////////////////////////////////

struct gb_msg : public x_qt_ptr
{
                        x_rtti_base(gb_msg);
    enum
    {
        FLAGS_DO_NOT_DELETE         = (1<<0),   // If you do not want to delete the message you can set this flag
        FLAGS_ONUPDATE_MESSAGE      = (1<<1),   // These type of messages are the only ones that can be 
                                                // send at T0T1 when the manager is updating
        FLAGS_REFERENCE             = (1<<0),   // Means that this message is actually a reference to another message
                                                // so in the union should resolve for m_pMessage
    };
    
    inline				gb_msg		                ( void ) : m_RefCount(1), m_Flags(0) {}
    virtual			   ~gb_msg                      ( void ) {}
    void                setupSystemVars             ( gb_component::callback_fn Func, u16 Flags=0 ) { m_MessageFunc = Func; m_Flags = Flags; }
    void                setupSystemVars             ( gb_msg* pMsg, u16 Flags=0 )                   { m_pMessage = pMsg; m_Flags = Flags; }

    // System vars
    union
    {
	    gb_component::callback_fn	m_MessageFunc;              // Which function should it be call from the component
        gb_msg*                     m_pMessage;                 // Pointer to the actual message
    };

    u16							    m_Flags;
    mutable u16					    m_RefCount;                 // Reference counting for the message

    // Fill by the entity
    xguid						    m_FromGuid;                 // Who is sending this message
    xguid						    m_ToGuid;                   // Who is reciving this message
    mutable f32					    m_DeltaTime;                // What deltaTime this message was created
};

//////////////////////////////////////////////////////////////////////////////////

struct gm_msg_register_event : public gb_msg
{
    x_rtti_class1( gm_msg_register_event, gb_msg );

    xstring                     m_EventName;
    gb_component::callback_fn	m_CallBack;
    gb_component*               m_pComponent;
};

//////////////////////////////////////////////////////////////////////////////////

struct event_notify_callbacks
{
        X_LL_CIRCULAR_FUNCTIONS(List, event_notify_callbacks, m_pNext, m_pPrev);

	    gb_component::callback_fn	    m_MessageFunc;              // Which function should it be call from the component
        gb_component*                   m_pComponent;               // A pointer to the component

        event_notify_callbacks*         m_pNext;
        event_notify_callbacks*         m_pPrev;
};

//////////////////////////////////////////////////////////////////////////////////
// INLINE
//////////////////////////////////////////////////////////////////////////////////
#ifndef GB_MSG_INLINE_H
#include "Implementation/gb_msg_inline.h"
#endif

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif
