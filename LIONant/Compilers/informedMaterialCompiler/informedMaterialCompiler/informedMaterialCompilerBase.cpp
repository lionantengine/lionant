//
//  fontBaseCompiler.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "informedMaterialCompilerBase.h"
#include "materialCompilerKey.h"
#include "textureCompilerKey.h"
#include "shaderCompilerKey.h"

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::CompilationUniqueness( void )
{
    m_bSingleCompilation = TRUE;
    
    //
    // Lets go throught the key platforms
    //
    const_ref( rEntry, m_Key.m_lEntry );
    const_ref( rMain, m_Key.m_Main );
    for ( const informed_material_compiler_key::entry_ppflist& UniformVal : rEntry )
        for ( s32 j = 0; j<m_Target.getCount( ); j++ )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( !m_Target[ j ].m_bValid )
                continue;

            if ( x_FlagIsOn( UniformVal[ j ].m_OverrideBits, informed_material_compiler_key::entry_params::SINGLE_COMPILATION_FLAGS ) )
            {
                m_bSingleCompilation = FALSE;
                return;
            }

            if ( x_FlagIsOn( rMain[j].m_OverrideBits, informed_material_compiler_key::main_params::SINGLE_COMPILATION_FLAGS ) )
            {
                m_bSingleCompilation = FALSE;
                return;
            }
        }

}

//-------------------------------------------------------------------------------------------------

u64 informed_material_compiler_base::AddNewTexture( const xstring& SrcTexture, const xplatform Platform ) const
{
    informed_material_compiler_base& This = const_cast<informed_material_compiler_base&>(*this);
    xscope_atomic( This.m_CriticalSection );

    //
    // Search to see if we have our texture
    //
    for ( new_texture& Texture : This.m_lNewTexture )
    {
        if ( 0 == x_stricmp( SrcTexture, Texture.m_SrcPath ) )
        {
            Texture.m_Platform |= (1<<Platform);
            return Texture.m_RscGuid.m_UID;
        }
    }

    //
    // Must create a new one
    //
    new_texture& Texture = This.m_lNewTexture.append();

    // make sure to add our platform    
    Texture.m_Platform = (1<<Platform);

    // copy the source path
    Texture.m_SrcPath.Copy( SrcTexture );
    Texture.m_SrcPath.MakeLower();

    // Try to compute a guid that will be predictable in the future
    // and some what relatable to the current one
    u32 Hash  = x_strHash( Texture.m_SrcPath );
        Hash  = (Hash&0xffff) ^ (Hash>>16);
    Texture.m_RscGuid.m_UID  = m_RscGuid.m_Guid ^ u64(Hash);
    Texture.m_RscGuid.setType( eng_texture_rsc::UID );

    // Create file name of the dependent resource
    Texture.m_NewKeyFile.Format( "%s/%s-0x%X--%s.texture.txt",
        (const char*)&m_ExternalCompilerKeysPath[m_ProjectPathLength+1],
        (const char*)m_RscFileName,
        Hash,
        (const char*)Texture.m_RscGuid.getXGuid().GetAlphaString() );

    // make sure that the guid is unique
    for ( const new_texture& OldTexture : m_lNewTexture )
    {
        if ( &OldTexture != &Texture )
            if ( Texture.m_RscGuid.m_UID == OldTexture.m_RscGuid.m_UID && 
                 &Texture != &OldTexture )
            {
                ASSERT(0);
            }
    }

    // Add this texture as an external key
    AddExternalDependencyKey(  Texture.m_NewKeyFile );

    return Texture.m_RscGuid.m_UID;
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::LoadShader ( 
    shader_compiler_key&                        Shader, 
    const xstring&                              ShaderKey, 
    const xplatform                             iPlatform ) const
{
    xstring             KeyPath;

    KeyPath.Format( "%s/%s.txt", 
        (const char*)m_ProjectPath, 
        (const char*)ShaderKey );

    Shader.LoadDefaults( m_ProjectPath );
    Shader.Load( KeyPath );
}

//-------------------------------------------------------------------------------------------------

struct val_info
{
    const informed_material_compiler_key::uniform_val*  m_pKeyVal           = NULL;
    s32                                                 m_iShader           = -1;
    s32                                                 m_iRegister         = -1;
    s32                                                 m_iVal              = -1;
    u64                                                 m_gTexture          =  0;
    eng_shader_program::uniform_type                    m_CallBackType      =  eng_shader_program::UNIFORM_TYPE_NULL;
};


static informed_material_compiler_base* s_pThis;
struct key_passthrough : public shader_compiler_key
{
    key_passthrough( void ) : shader_compiler_key( *s_pThis ){}
};

struct new_technique
{
    xstring                             m_Name;
    s32                                 m_TechniqueMaterialIndex;
    s32                                 m_TechniqueImformedIndex;
    xsafe_array<key_passthrough, 3>     m_Shaders;
    xarray<val_info>                    m_UniformVal;
};

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::CompileKeyPlatform(
    const material_compiler_key&                        MaterialCompilerKey, 
    compilation_data&                                   CompilationData, 
    const xplatform                                     iPlatform ) const
{
    x_inline_light_jobs_block<8>                        Block;
    xarray<new_technique>                               lNewTechnique;
    xarray<const val_info*>                             lCompactList;
    xsafe_array<
        informed_material_compiler_key::uniform_val, 
        eng_shader_program::UNIFORM_TYPE_ENUM_COUNT> SystemKeyVals;

    const_ref( rEntry, m_Key.m_lEntry );

    //
    // Do a quick sanity check in our values
    //

    //
    // Identify which techniques we are going to be using
    //
    {

        for ( const xsafe_array<informed_material_compiler_key::entry_params, X_PLATFORM_COUNT>& PlatformList : rEntry )
        {
            const informed_material_compiler_key::entry_params& InformedEntry = PlatformList[iPlatform];

            new_technique& NewTechnique = lNewTechnique.append();

            NewTechnique.m_Name                   = InformedEntry.m_TechniqueName;
            NewTechnique.m_TechniqueImformedIndex = rEntry.getIterator();

            // all material entries have exactly the same non system uniforms
            NewTechnique.m_TechniqueMaterialIndex = 0; 
        }
    }


    //
    // Create fake values for the system types
    //
    Block.SubmitJob( [&]()
    {
        for ( informed_material_compiler_key::uniform_val& SysKeyVal : SystemKeyVals )
        {
            const s32 Index = s32( &SysKeyVal - &SystemKeyVals[0] );
            SysKeyVal.m_Type = eng_shader_program::uniform_type( Index );
        }
    });

    //
    // We will have to do this for each technique
    //

    // Now we can load them all at ones
    const_ref( rEntryMatCompilerKey, MaterialCompilerKey.m_lEntry);
    for ( new_technique& NewTechnique : lNewTechnique )
    for ( auto& ShaderKey : NewTechnique.m_Shaders )
    {
        const s32       iShader    = s32( &ShaderKey - &NewTechnique.m_Shaders[0] ); 
        const xstring&  ShaderName = rEntryMatCompilerKey[ NewTechnique.m_TechniqueMaterialIndex ][ iPlatform ].m_ShaderKey[iShader];

        if ( ShaderName.IsEmpty() == FALSE )
            Block.SubmitJob( [&]()
            {
                // Load the shader key file
                LoadShader( ShaderKey, ShaderName, iPlatform  );
            });
    }

    Block.FinishJobs();

    //
    // Now for each technique we must have all the relevant values of its uniforms
    //
    for ( new_technique& NewTechnique : lNewTechnique )
    {
        const informed_material_compiler_key::entry_params&         MainParams = rEntry[ NewTechnique.m_TechniqueImformedIndex ][ iPlatform ];
        xarray<val_info>&                                           UniformVal = NewTechnique.m_UniformVal;

        Block.SubmitJob( [&]()
        {
            //
            // Make sure that we have the right data
            //
            for ( const shader_compiler_key& ShaderKey : NewTechnique.m_Shaders )
            {
                const_ref( rShaderKey, ShaderKey.m_Main );
                for ( const shader_compiler_key::uniform& ShaderUniform : rShaderKey[ iPlatform ].m_lUniformDef )
                {
                    xbool bFound = FALSE;
                    for ( const informed_material_compiler_key::uniform_val& InformedVal : MainParams.m_lVal )
                    {
                        if ( InformedVal.m_UID == ShaderUniform.m_UID )
                        {
                            bFound = TRUE;
                            break;
                        }
                    }

                    if ( bFound )
                    {
                        // Make sure that it is an informed value
                        if ( ShaderUniform.m_Type == eng_shader_program::UNIFORM_TYPE_NULL ||
                             ShaderUniform.m_Type >= eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                        {
                             x_throw(  "ERROR: Uniform [%s] with UID [%d] is flag as getting data from the SYSTEM_ but I found a Informed Uniform Value for it\n"
                                          "       Technique [%s] Platform [%s]\n",
                             (const char*)ShaderUniform.m_Name,
                             ShaderUniform.m_UID,
                             (const char*)NewTechnique.m_Name,
                             iPlatform == 0 ? "ALL" : x_PlatformString(iPlatform)
                              );
                        }
                    }
                    else
                    {
                        // If it is an expected value this is a problem...
                        if ( ShaderUniform.m_Type == eng_shader_program::UNIFORM_TYPE_NULL ||
                             ShaderUniform.m_Type < eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                        {
                             x_throw(  "ERROR: Uniform [%s] with UID [%d] has not Informed Uniform Data for it, place make sure it has one.\n"
                                          "       Technique [%s] Platform [%s]\n",
                             (const char*)ShaderUniform.m_Name,
                             ShaderUniform.m_UID, 
                             (const char*)NewTechnique.m_Name,
                             iPlatform == 0 ? "ALL" : x_PlatformString(iPlatform)
                              );
                        }
                    }
                }
            }

            //
            // Make sure we only keep uniforms that are going to be use 
            // First is the Fragment Shader Offset this allows textures to be register first
            // Then the vertex shader offset, and finally the geometry shader
            //
            xsafe_array<s32,3> RegisterOffsets;
            {
                const_ref( rNewTechniqueShader1, NewTechnique.m_Shaders[ 1 ].m_Main );
                RegisterOffsets[ 0 ] = rNewTechniqueShader1[ iPlatform ].m_lUniformDef.getCount();
                RegisterOffsets[ 1 ] = 0;

                // If we have a geometry shader then create its offset too
                if(NewTechnique.m_Shaders.getCount() == 3 )
                { 
                    const_ref( rNewTechniqueShader0, NewTechnique.m_Shaders[ 0 ].m_Main );
                    RegisterOffsets[ 2 ] = RegisterOffsets[ 0 ] + rNewTechniqueShader0[ iPlatform ].m_lUniformDef.getCount();
                }
            }

            for ( const informed_material_compiler_key::uniform_val& InformedVal : MainParams.m_lVal )
            {
                s32                                 nFound          = 0;
                s32                                 iShader         = -1;
                s32                                 iLocalRegister  = -1;
                eng_shader_program::uniform_type    CallBackType    = eng_shader_program::UNIFORM_TYPE_NULL;
                for ( const auto& ShaderKey : NewTechnique.m_Shaders )
                {
                    if ( nFound > 1 )
                    {
                         x_throw(  "ERROR: Uniform with UID [%d] was found in multiple shader.Technique [%s] Platform [%s]\n",
                         InformedVal.m_UID, 
                         (const char*)NewTechnique.m_Name,
                         iPlatform == 0 ? "ALL" : x_PlatformString(iPlatform)
                          );
                    }

                    const_ref( rShaderKeyMain, ShaderKey.m_Main );
                    for ( const shader_compiler_key::uniform& ShaderUniform : rShaderKeyMain[ iPlatform ].m_lUniformDef )
                    {
                        if ( InformedVal.m_UID == ShaderUniform.m_UID )
                        {
                            nFound++;
                            iShader         = s32( &ShaderKey - &NewTechnique.m_Shaders[0] );
                            iLocalRegister  = s32( &ShaderUniform - &rShaderKeyMain[ iPlatform ].m_lUniformDef[0] );
                            CallBackType    = ShaderUniform.m_Type;
                            break;
                        }
                    }
                }

                if ( nFound == 0 )
                {
                    x_printf( "WARNING: I found a uniform value UID:[%d] unfound in any of the shader in the material. It will be removed. Technique[%s], Platform[%s]",
                     InformedVal.m_UID,
                     (const char*)NewTechnique.m_Name,
                      iPlatform == 0 ? "ALL" : x_PlatformString(iPlatform) );
                }
                else
                {
                    val_info& ValInfo = UniformVal.append();

                    ValInfo.m_pKeyVal        = &InformedVal;
                    ValInfo.m_iShader        = iShader;
                    ValInfo.m_iRegister      = RegisterOffsets[ iShader ] + iLocalRegister;
                    ValInfo.m_CallBackType   = CallBackType;
                }
            }

            //
            // Add the system types as values as well... even though the values will be generated at runtime
            //
            /*
            for ( const shader_compiler_key& ShaderKey : NewTechnique.m_Shaders )
            for ( const shader_compiler_key::uniform& ShaderUniform : ShaderKey.m_Main[ iPlatform ].m_lUniformDef )
            {
                if ( ShaderUniform.m_Type <= eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                    continue;

                val_info& ValInfo           = UniformVal.append();
                const s32 iLocalRegister    = s32( &ShaderUniform - &ShaderKey.m_Main[ iPlatform ].m_lUniformDef[0] );

                ValInfo.m_pKeyVal           = &SystemKeyVals[ ShaderUniform.m_Type ];
                ValInfo.m_iShader           = s32( &ShaderKey - &NewTechnique.m_Shaders[0] );
                ValInfo.m_iRegister         = RegisterOffsets[ ValInfo.m_iShader ] + iLocalRegister;
                ValInfo.m_CallBackType      = eng_shader_program::UNIFORM_TYPE_NULL;
            }
            */
        } );          
    } 

    Block.FinishJobs();

    //
    // Compactify the value list
    //
    for ( new_technique& NewTechnique : lNewTechnique )
    for( val_info& ValInfo : NewTechnique.m_UniformVal )
    {
        s32 Index = -1;
        for ( const val_info*& pCompactValue : lCompactList )
        {
            const s32 iValue = s32( &pCompactValue - &lCompactList[0] );

            if ( pCompactValue->m_pKeyVal->m_CallbackGuid == ValInfo.m_pKeyVal->m_CallbackGuid )
            {
                if ( pCompactValue->m_pKeyVal->m_CallbackGuid == 0 )
                {
                    if ( pCompactValue->m_pKeyVal->m_Type == ValInfo.m_pKeyVal->m_Type )
                    {
                        switch ( pCompactValue->m_pKeyVal->m_Type )
                        {
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA:
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGB:
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL:
                            if ( 0 == x_stricmp( pCompactValue->m_pKeyVal->m_TextureRSC, ValInfo.m_pKeyVal->m_TextureRSC ) )
                            { 
                                Index = iValue;
                            }
                            break;
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL:
                            if ( pCompactValue->m_pKeyVal->m_Bool == ValInfo.m_pKeyVal->m_Bool )
                            { 
                                Index = iValue;
                            }
                            break;
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT:
                            if ( pCompactValue->m_pKeyVal->m_Float == ValInfo.m_pKeyVal->m_Float )
                            { 
                                Index = iValue;
                            }
                            break;
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_V2:
                            if ( pCompactValue->m_pKeyVal->m_V2 == ValInfo.m_pKeyVal->m_V2 )
                            { 
                                Index = iValue;
                            }
                            break;
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_V3:
                            if ( pCompactValue->m_pKeyVal->m_V3 == ValInfo.m_pKeyVal->m_V3 )
                            { 
                                Index = iValue;
                            }
                            break;
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_V4:
                            if ( pCompactValue->m_pKeyVal->m_V4 == ValInfo.m_pKeyVal->m_V4 )
                            { 
                                Index = iValue;
                            }
                            break;
                        default:
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_M3:
                        case eng_shader_program::UNIFORM_TYPE_INFORMED_M4:
                        ASSERT(0);
                        break;
                        };

                        // break out of for loop
                        if ( Index == iValue )
                            break;
                    }
                }
                else
                {
                    Index = iValue;
                    break;
                }
            }
        }

        //
        // Did we find the value
        //
        if ( Index == -1 )
        {
            ValInfo.m_iVal = lCompactList.getCount();
            lCompactList.append() = &ValInfo;
        }
        else
        {
            ValInfo.m_iVal = Index;
        }

    }

    //
    // Create the final values from the compacted list 
    //
    Block.SubmitJob( [&]()
    {
        CompilationData.m_lValue.appendList( lCompactList.getCount() );

        for ( const val_info*& pCompactValue : lCompactList )
        {
            const s32                                   iCompactVal = s32( &pCompactValue - &lCompactList[0] );
            eng_informed_material_rsc::uniform_value&   FVal        =  CompilationData.m_lValue[ iCompactVal ];
            
            // we always copy the type
            FVal.m_Type          = pCompactValue->m_pKeyVal->m_Type;
            FVal.m_xCallbackGUID = 0;

            // Copy the remaining values
            if ( pCompactValue->m_pKeyVal->m_CallbackGuid )
            {
                FVal.m_xCallbackGUID = pCompactValue->m_pKeyVal->m_CallbackGuid;
                FVal.m_Type          = pCompactValue->m_CallBackType;
                ASSERT( FVal.m_xCallbackGUID < 0 );
            }
            else switch ( pCompactValue->m_pKeyVal->m_Type )
            {
            case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA:
            case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGB:
            case eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL:
                    
                    // Try to get the guid of the reousrce
                    FVal.m_Guid = getGuidFromFilePath( pCompactValue->m_pKeyVal->m_TextureRSC );

                    // if it does not have one then we have to assume the user wants us to add this texture to the build
                    if ( FVal.m_Guid == 0 ) 
                        FVal.m_Guid = AddNewTexture( pCompactValue->m_pKeyVal->m_TextureRSC, iPlatform );
                    else
                    {
                        AddExternalDependencyKey( xstring::BuildFromFormat("%s.txt",(const char*)pCompactValue->m_pKeyVal->m_TextureRSC) );
                    }
                        

                    ASSERT(FVal.m_Guid);
                    break;
            case eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL:
                    FVal.m_Bool = pCompactValue->m_pKeyVal->m_Bool;
                    break;
            case eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT:
                    FVal.m_F[0] = pCompactValue->m_pKeyVal->m_Float;
                    break;
            case eng_shader_program::UNIFORM_TYPE_INFORMED_V2:
                    FVal.m_F[0] = pCompactValue->m_pKeyVal->m_V2.m_X;
                    FVal.m_F[1] = pCompactValue->m_pKeyVal->m_V2.m_Y;
                    break;
            case eng_shader_program::UNIFORM_TYPE_INFORMED_V3:
                    FVal.m_F[0] = pCompactValue->m_pKeyVal->m_V3.m_X;
                    FVal.m_F[1] = pCompactValue->m_pKeyVal->m_V3.m_Y;
                    FVal.m_F[2] = pCompactValue->m_pKeyVal->m_V3.m_Z;
                    break;
            case eng_shader_program::UNIFORM_TYPE_INFORMED_V4:
                    FVal.m_F[0] = pCompactValue->m_pKeyVal->m_V4[0];
                    FVal.m_F[1] = pCompactValue->m_pKeyVal->m_V4[1];
                    FVal.m_F[2] = pCompactValue->m_pKeyVal->m_V4[2];
                    FVal.m_F[3] = pCompactValue->m_pKeyVal->m_V4[3];
                    break;
            default:
            case eng_shader_program::UNIFORM_TYPE_INFORMED_M3:
            case eng_shader_program::UNIFORM_TYPE_INFORMED_M4:
            ASSERT(0);
            break;
            };
        }
    } );

    //
    // Create the technique and cmds
    //
    Block.SubmitJob( [&]()
    {
        //
        // build all the techniques & allocate the list of cmds
        //
        {
            CompilationData.m_InformedRSC.m_nCmds = 0;
    
            CompilationData.m_lTechnique.appendList( lNewTechnique.getCount() );

            for ( const new_technique& NewTechnique : lNewTechnique )
            {
                const s32                               iTechnique = s32( &NewTechnique - &lNewTechnique[0] );
                eng_informed_material_rsc::technique&   FTechnique = CompilationData.m_lTechnique[ iTechnique ];
                
                FTechnique.m_nCmds      = NewTechnique.m_UniformVal.getCount();
                FTechnique.m_iCmd       = CompilationData.m_InformedRSC.m_nCmds;

                CompilationData.m_InformedRSC.m_nCmds += FTechnique.m_nCmds;
            }

            // Create the list for all the commands
            CompilationData.m_Cmds.appendList( CompilationData.m_InformedRSC.m_nCmds );
        }

        //
        // Build cmds
        //
        for ( const new_technique& NewTechnique : lNewTechnique )
        {
            const s32                                       iTechnique = s32( &NewTechnique - &lNewTechnique[0] );
            const eng_informed_material_rsc::technique&     FTechnique = CompilationData.m_lTechnique[ iTechnique ];

            for ( const val_info& ValInfo : NewTechnique.m_UniformVal )
            {
                const s32                           iValInfo = s32( &ValInfo - &NewTechnique.m_UniformVal[0] );
                eng_informed_material_rsc::cmds&    FCmd     = CompilationData.m_Cmds[ iValInfo + FTechnique.m_iCmd ];

                FCmd.m_iShader          = ValInfo.m_iShader;
                FCmd.m_iRegister        = ValInfo.m_iRegister;
                FCmd.m_iValue           = ValInfo.m_iVal;
                ASSERT(FCmd.m_iValue>=0);
                ASSERT(FCmd.m_iValue< CompilationData.m_Cmds.getCount() );
            }
        }
    });

    Block.FinishJobs();

    //
    // Build the final structure
    //
    CompilationData.m_InformedRSC.m_nTechniques         = CompilationData.m_lTechnique.getCount();
    CompilationData.m_InformedRSC.m_nCmds               = CompilationData.m_Cmds.getCount();
    CompilationData.m_InformedRSC.m_nUniformValues      = CompilationData.m_lValue.getCount();
    CompilationData.m_InformedRSC.m_pCmd.m_Ptr          = &CompilationData.m_Cmds[0]; 
    CompilationData.m_InformedRSC.m_pUniformValue.m_Ptr = &CompilationData.m_lValue[0]; 
    CompilationData.m_InformedRSC.m_pTechnique.m_Ptr    = &CompilationData.m_lTechnique[0];

    const_ref( rMain, m_Key.m_Main );
    CompilationData.m_InformedRSC.m_rMaterial           = getGuidFromFilePath( rMain[iPlatform].m_MaterialRsc );
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::ExportResource( const compilation_data& CompilationData, const platform& Target ) const
{
    //
    // Ok Ready to save everything then
    //
    xserialfile     SerialFile;
    xstring         FinalRscPath;
    
    FinalRscPath = getFinalResourceName( Target );
    
    SerialFile.Save( (const char*)FinalRscPath, CompilationData.m_InformedRSC, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
    
    // for debugging
    if( (1) )
    {
        xserialfile             test;
        eng_informed_material_rsc::informed_rsc* pMyInformedMaterial;
        test.Load( (const char*)FinalRscPath, pMyInformedMaterial );
        
        ASSERT( pMyInformedMaterial );
        x_delete( pMyInformedMaterial );
    }
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::DealWithNewTextures( void )
{
    x_inline_light_jobs_block<8> Block;

    for ( new_texture& Texture : m_lNewTexture )
    {
        Block.SubmitJob( [this, &Texture ]()
        {
            xbool               bReExportKeyFile = TRUE;
            xstring             FinalPath;

            FinalPath.Format( "%s/%s", 
                (const char*)m_ProjectPath, 
                (const char*)Texture.m_NewKeyFile );

            // If we already have this file created lets make sure that we have to overwrite it
            // because if we can avoid it then we will save compilation time
            // I AM REMOVING THIS OPTIMIZATION SINCE I CREATED A MORE GENERIC ONE.
            // This is a bit inacurate but more forgiving... so lets wait and see if we need something
            /*
            if ( x_io::FileExists( FinalPath ) )
            {
                texture_compiler_key TextureCompilerKey;

                TextureCompilerKey.Load( FinalPath );

                for ( const platform& Target : m_Target )
                {
                    // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
                    if ( Target.m_bValid == FALSE )
                        continue;

                    if ( 0 == x_stricmp( TextureCompilerKey.getSrcFileName( Target.m_Platform ), Texture.m_SrcPath ) )
                    {
                        bReExportKeyFile = FALSE;
                        break;
                    }
                }
            }
            */

            if ( bReExportKeyFile )
            {
                texture_compiler_key    TextureKey(*this);
        
                //
                // Setup the texture compiler key
                //
                TextureKey.LoadDefaults( m_ProjectPath );
 
                // First lets set some Global PArameters
                // this is a shortcut in reality we should search which platforms this texture really must be build for
                g_PropFilePath.Set( TextureKey, "Key/Main/Global/FileName",  Texture.m_SrcPath );
                g_PropBool.Set  ( TextureKey, "Key/Main/Global/bCreateMips", TRUE  );
                g_PropEnum.Set  ( TextureKey, "Key/Main/Global/U-Tiling",    TextureKey.getTileEnum  ( texture_compiler_key::TILEMODE_TILE      ) );
                g_PropEnum.Set  ( TextureKey, "Key/Main/Global/V-Tiling",    TextureKey.getTileEnum  ( texture_compiler_key::TILEMODE_TILE      ) );
                g_PropEnum.Set  ( TextureKey, "Key/Main/Global/FilterMig",   TextureKey.getFilterEnum( eng_texture::FILTER_MINMAG_LINEAR ) );
                g_PropEnum.Set  ( TextureKey, "Key/Main/Global/FilterMag",   TextureKey.getFilterEnum( eng_texture::FILTER_MINMAG_LINEAR ) );
                g_PropFloat.Set  ( TextureKey, "Key/Main/Global/CompressionLevel", 0 );
                
                
                //
                // save the dependency file
                //
                TextureKey.Save( FinalPath );
            }
        });
    }

    Block.FinishJobs();
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_base::onCompile( void )
{
    //
    // Determine Uniqueness of the compilation
    //
    CompilationUniqueness();

    const_ref( rMain, m_Key.m_Main );
    if ( rMain[0].m_MaterialRsc.IsEmpty() )
        x_throw( "ERROR: No MaterialResource property set." );

    //
    // We only need to load the material compiler key ones for any platform
    //
    material_compiler_key   MaterialCompilerKey(*this);
    xstring                 MaterialCompilerKeyPathName;

    MaterialCompilerKeyPathName.Format( "%s/%s.txt",
        (const char*)m_ProjectPath,
        (const char*)rMain[0].m_MaterialRsc );

    MaterialCompilerKey.LoadDefaults( m_ProjectPath );
    MaterialCompilerKey.Load( MaterialCompilerKeyPathName );

    // Lets add this dependency into our collection 

    AddExternalDependencyKey( RemoveProjectPath( MaterialCompilerKeyPathName ) );

    //
    // Single compilation vs multiple
    //
    if( m_bSingleCompilation )
    {
        x_inline_light_jobs_block<8> Block;

        //
        // Prepare the data structures
        //
        CompileKeyPlatform( MaterialCompilerKey, m_lCompilationData[0], xplatform(0) );

        //
        // Deal with new texture that we may have to create
        //
        Block.SubmitJob( [this]()
        {
            DealWithNewTextures();
        } );

        //
        // Export Files
        //
        for( const platform& Target : m_Target )
        {
            if( Target.m_bValid == FALSE )
                continue;
            
            // export resources
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[0], Target);
            });
        }
        
        Block.FinishJobs();
    }
    else
    {
        x_inline_light_jobs_block<8> Block;
        
        //
        // Process for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;
            
            // Compile
            Block.SubmitJob( [this, &Target, &MaterialCompilerKey]()
            {
                CompileKeyPlatform( MaterialCompilerKey, m_lCompilationData[Target.m_Platform], Target.m_Platform );
            });
        }

        Block.FinishJobs();

        //
        // Deal with new texture that we may have to create
        //
        Block.SubmitJob( [this]()
        {
            DealWithNewTextures();
        } );

        //
        // Export for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;

            // Export
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[ Target.m_Platform ], Target);
            });
        }
        
        Block.FinishJobs();
    }
}

