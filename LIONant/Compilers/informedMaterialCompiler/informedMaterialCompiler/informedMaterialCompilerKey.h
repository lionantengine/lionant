//
//  informedMaterialCompilerKeyObject.h
//  informedMaterialCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef INFORMED_MATERIAL_COMPILER_KEY_OBJECT_H
#define INFORMED_MATERIAL_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"
#include "eng_base.h"

class informed_material_compiler_key : public compiler_key_object
{
public:
    
    typedef eng_shader_program::uniform_type uniform_type;

    struct uniform_val
    {
        s32                         m_UID               = -1;
        uniform_type                m_Type              = uniform_type::UNIFORM_TYPE_NULL;
        s32                         m_CallbackGuid      =  0;
        
        // Possible values depending of the type
        xstring                     m_TextureRSC        = X_STR("");
        xstring                     m_RawGeom           = X_STR("");
        f32                         m_Float             = 0;
        xvector2                    m_V2;
        xvector3d                   m_V3;
        xsafe_array<f32,4>          m_V4;
        xbool                       m_Bool              = FALSE;
    };

    struct main_params : public main_params_base_link<main_params>
    {
        enum param_flags
        {
            OVERRIDE_MATERIAl_RSC       =    X_BIT( 0 ),
            OVERRIDE_DEFAULT_INFORMED   =    X_BIT( 1 ),

            SINGLE_COMPILATION_FLAGS    =   0
                                            | OVERRIDE_MATERIAl_RSC
                                            | OVERRIDE_DEFAULT_INFORMED
        };

        virtual void    onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool   onPropQuery         ( xproperty_query& Query )  override final;
        void            onUpdateFromSrc     ( const main_params& Src, const u64 Masks );

        xstring     m_MaterialRsc               = X_STR("");
        xstring     m_DefaultInform             = X_STR("");
    };

    struct entry_params : public entry_params_base_link<entry_params>
    {
        enum param_flags
        {
            OVERRIDE_CHANGE_VALUES     = X_BIT( 0 ),
            SINGLE_COMPILATION_FLAGS   = OVERRIDE_CHANGE_VALUES
        };

        virtual void    onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool   onPropQuery         ( xproperty_query& Query )  override final;
        void            onUpdateFromSrc     ( const entry_params& Src, const u64 Masks );

        xstring                                     m_TechniqueName     = X_STR("");
        xarray<uniform_val>                         m_lVal;
    };

public:
    
                                informed_material_compiler_key  ( const compiler_base& CompilerBase ) : compiler_key_object(CompilerBase){}
    virtual const char*         getCompilerName                 ( void ) const override final { return "informedMaterialCompiler"; }
    virtual const char*         getCompiledExt                  ( void ) const override final { return "informed";         }
    
protected:
    
    virtual void                getAdditionalDependencies       ( xarray2<dependency>& List ) const override final;
    virtual xbool               onUniquePropQuery               ( xproperty_query& Query ) override final;
    
    KEY_STANDARD_STUFF
        
protected:
    
    friend class informed_material_compiler_base;
    friend class mesh_compiler_base;
};

#endif
