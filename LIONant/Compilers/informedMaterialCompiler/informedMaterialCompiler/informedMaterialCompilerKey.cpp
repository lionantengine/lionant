//
//  materialCompilerKeyObject.cpp
//  materialCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "informedMaterialCompilerKey.h"

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void informed_material_compiler_key::entry_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    g_PropString.Enum( Enum, iScope, "TechniqueName", "The name of the technique that we are talking about" );

    for( const uniform_val& UniformVal : m_lVal )
    {
    //    const s32 Index         = s32( &UniformVal - &m_lVal[0] );
        const s32 iUIDScope     = Enum.AddScope( iScope, xfs( "UniformUID[%d]", UniformVal.m_UID ), "Uniform UID is in the index of the array" );

        if ( UniformVal.m_CallbackGuid )
        {
            g_PropInt.Enum( Enum, iUIDScope, "CallbackGUID", "The GUID for the callback to resolve the value for this uniform." );
        }
        else switch ( UniformVal.m_Type )
        {
            case uniform_type::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA:
            case uniform_type::UNIFORM_TYPE_INFORMED_TEXTURE_RGB:
            case uniform_type::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL: 
                g_PropFilePath.Enum( Enum, iUIDScope, "TextureRSC", "Name of the texture we are refereing to.", "*.tga,*.png,*.bmp", "" );
            break;
            case uniform_type::UNIFORM_TYPE_INFORMED_BOOL:
                g_PropBool.Enum( Enum, iUIDScope, "Bool", "Value of a bool uniform variable." ); 
            break;
            case uniform_type::UNIFORM_TYPE_INFORMED_FLOAT: 
                g_PropFloat.Enum( Enum, iUIDScope, "V1", "Value of a bool uniform variable." ); 
            break;
            case uniform_type::UNIFORM_TYPE_INFORMED_V2: 
            break;
            case uniform_type::UNIFORM_TYPE_INFORMED_V3: 
                g_PropVector3.Enum( Enum, iUIDScope, "V3", "Value of a bool uniform variable." ); 
            break;
            default:
                ASSERT(0);
            break;
            /*
            case uniform_type::UNIFORM_TYPE_SYSTEM_UV_ANIM:
                g_PropString.Enum( Enum, iAScope, "RawGeom", "Name of the rawgeom containing the UV animation." ); 
                g_PropEnum.Enum( Enum, iAScope, "LoopType", "Type of the looping animation, PINGPONG, LOOP, ONES." );
                g_PropFloat.Enum( Enum, iAScope, "Speed", "Speed of the animation playback." );
            break;
            case uniform_type::UNIFORM_TYPE_SYSTEM_LIGHTDIR: 
            break;
            */
        }
    }
}

//-------------------------------------------------------------------------------------------------

xbool informed_material_compiler_key::entry_params::onPropQuery( xproperty_query& Query )
{
    if ( Query.Var( "TechniqueName" ) && g_PropString.Query( Query, m_TechniqueName ) )
    {
        if ( FALSE == Query.IsUserGet() )  x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
        return TRUE;
    }

    if ( Query.Scope( "UniformUID[]" ) )
    {
        const s32 iUID   = Query.GetArrayIndex( 1000 );

        //
        // Deal with known vals
        //
        do
        {
            for( uniform_val& UniformVal : m_lVal )
            {
                if ( UniformVal.m_UID == iUID )
                {
                    if ( Query.Var( "CallbackGUID" ) && g_PropInt.Query( Query, UniformVal.m_CallbackGuid, S32_MIN, S32_MAX ) )
                    {
                        if ( FALSE == Query.IsUserGet() ) 
                        {
                            x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
                            UniformVal.m_Type = uniform_type::UNIFORM_TYPE_NULL;
                        }
                        return TRUE;
                    }

                    if ( Query.Var( "TextureRSC" ) && g_PropFilePath.Query( Query, UniformVal.m_TextureRSC ) )
                    {
                        if ( FALSE == Query.IsUserGet() ) 
                        {
                            x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
                            UniformVal.m_Type = uniform_type::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA;
                        }
                        return TRUE;
                    }

                    if ( Query.Var( "Bool" ) && g_PropBool.Query( Query, UniformVal.m_Bool ) )
                    {
                        if ( FALSE == Query.IsUserGet() )
                        {
                            x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
                            UniformVal.m_Type = uniform_type::UNIFORM_TYPE_INFORMED_BOOL;
                        }
                        return TRUE;
                    }

                    if ( Query.Var( "FLOAT" ) && g_PropFloat.Query( Query, UniformVal.m_Float, -F32_MAX, F32_MAX ) )
                    {
                        if ( FALSE == Query.IsUserGet() ) 
                        {
                            x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
                            UniformVal.m_Type = uniform_type::UNIFORM_TYPE_INFORMED_FLOAT;
                        }
                        return TRUE;
                    }

                    if ( Query.Var( "V3" ) && g_PropVector3.Query( Query, UniformVal.m_V3 ) )
                    {
                        if ( FALSE == Query.IsUserGet() ) 
                        {
                            x_FlagOn( m_OverrideBits, OVERRIDE_CHANGE_VALUES );
                            UniformVal.m_Type = uniform_type::UNIFORM_TYPE_INFORMED_V3;
                        }
                        return TRUE;
                    }

                    ASSERT(0);
                    break;
                }
            }

            //
            // we did not find it so we must add the new value
            // If we are writting new properties then we must create it first
            //
            if ( Query.IsUserGet() )
                break;
            
            uniform_val& NewUniformVal = m_lVal.append();
            NewUniformVal.m_UID  = iUID;

            // Next loop it will set the rest of the values.

        } while (1);
    }

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_key::entry_params::onUpdateFromSrc( const entry_params& Src, const u64 Masks )
{
    if ( x_FlagIsOn( Masks, OVERRIDE_CHANGE_VALUES ) )
        return;

    m_TechniqueName = Src.m_TechniqueName;
    m_lVal.Copy( Src.m_lVal );
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

void informed_material_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "MaterialShader",           OVERRIDE_MATERIAl_RSC,      "File name of the source material to be use.", "*.material.txt" )
    KEY_PROP_ENUM_FILE( "DefaultInformedKeyFile",   OVERRIDE_DEFAULT_INFORMED,   "Uses this file as the default informed", "*.informed.txt" )
}

//-------------------------------------------------------------------------------------------------

xbool informed_material_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "MaterialShader",           OVERRIDE_MATERIAl_RSC,              g_PropFilePath.Query( Query, m_MaterialRsc )                )     
    KEY_PROP_QUERY( "DefaultInformedKeyFile",   OVERRIDE_DEFAULT_INFORMED,          g_PropFilePath.Query( Query, m_DefaultInform )              )     

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void informed_material_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_MaterialRsc,         OVERRIDE_MATERIAl_RSC,             .Copy )     
    KEY_PROP_UPDATE( m_DefaultInform,       OVERRIDE_DEFAULT_INFORMED,         .Copy )     
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------

xbool informed_material_compiler_key::onUniquePropQuery( xproperty_query& Query )
{
    //
    // We need to load the shader file when it is the right time for it
    //
    if( compiler_key_object::onUniquePropQuery( Query ) )
    {
        if ( FALSE == Query.IsUserGet( ) && Query.Var( "DefaultInformedKeyFile" )  )
        {
            for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
            {
                const char* pScopeName = "Global";
                if(i) pScopeName = x_PlatformString( xplatform(i) );

                if( x_stristr( Query.GetPropertyName( ), pScopeName ) )
                {
                    mutable_ref( rMain, m_Main );
                    auto& Entry = rMain[i];

                    if( Entry.m_DefaultInform.IsEmpty() == FALSE )
                    {
                        xstring FullPath;

                        FullPath.Format( "%s/%s", 
                            (const char*)m_CompilerBase.getProjectPath(),
                            (const char*)Entry.m_DefaultInform );

                        if( Load( FullPath ) == FALSE )
                            x_throw( "ERROR: Fail to load the default informed file [%s]",
                                (const char*)FullPath  );

                    }

                    // done
                    break;
                }
            }
        }
        return TRUE;
    }

    return FALSE;
}

//---------------------------------------------------------------------------------------

void informed_material_compiler_key::getAdditionalDependencies( xarray2<dependency>& List ) const
{
    //
    // Stead of adding more dependencies we are going to filter out all the textures
    // basically filter everything except the material and the potencial informed default file
    // the reasons why texture gets filter out is because they dont influence the way we build
    // the only things that influence us are the material and the informed defaullt.
    //
    mutable_ref( rList, List );
    for ( s32 i = 0; i < List.getCount(); i++ )
    {
        dependency& Entry = rList[i];

        if( Entry.m_FilePath.FindI( ".material.txt" ) != -1 )
        {
            // We always build if the material builds
            Entry.m_bForceKeyBuild = TRUE;
            continue;
        }

        if( Entry.m_FilePath.FindI( ".informed.txt" ) != -1 )
            continue;

        rList->DeleteWithCollapse(i);
        i--;
    }
}


