//
//  fontBaseCompiler.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef INFORMED_MATERIAL_COMPILER_BASE_H
#define INFORMED_MATERIAL_COMPILER_BASE_H

#include "informedMaterialCompilerKey.h"
#include "eng_base.h"

class material_compiler_key;
class shader_compiler_key;
class informed_material_compiler_base : public compiler_base
{
public:
    
                                        informed_material_compiler_base( void ) : m_Key( *this ){};
                                       ~informed_material_compiler_base( void ){}

    virtual void                        onCompile           ( void );
    virtual compiler_key_object&        getKeyObject        ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject        ( void ) const { return m_Key;             }
    
protected:
    
    struct new_texture
    {
        xstring                                                 m_SrcPath;
        xstring                                                 m_NewKeyFile;
        eng_resource_guid                                       m_RscGuid;
        u32                                                     m_Platform          = 0;
    };

    struct compilation_data
    {
        xarray<eng_informed_material_rsc::technique>            m_lTechnique;
        xarray<eng_informed_material_rsc::cmds>                 m_Cmds;
        xarray<eng_informed_material_rsc::uniform_value>        m_lValue;
        eng_informed_material_rsc::informed_rsc                 m_InformedRSC;
    };
    
protected:

    void                                CompilationUniqueness   ( void );
    void                                CompileKeyPlatform      ( const material_compiler_key&                       MaterialCompilerKey,
                                                                  compilation_data&                                  CompilationData, 
                                                                  const xplatform                                    Platform ) const;
    void                                ExportResource          ( const compilation_data& CompilationData, const platform& Target ) const;
    u64                                 AddNewTexture           ( const xstring& SrcTexture, const xplatform Platform ) const;
    void                                DealWithNewTextures     ( void );
    void                                LoadShader              ( shader_compiler_key& Shader, const xstring& ShaderKey, const xplatform iPlatform ) const;

protected:
    
    informed_material_compiler_key                              m_Key;
    xbool                                                       m_bSingleCompilation;
    xsafe_array<compilation_data, X_PLATFORM_COUNT>             m_lCompilationData;
    xarray<new_texture>                                         m_lNewTexture;
};

#endif
