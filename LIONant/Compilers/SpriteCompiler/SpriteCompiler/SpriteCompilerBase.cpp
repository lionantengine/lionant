//
//  main.cpp
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/16/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <stdlib.h>
#include "xbmp_Tools.h"
#include "SpriteCompilerBase.h"
#include "eng_Base.h"
#include "TextureCompilerKey.h"


#ifdef AddJob                       // Microsoft do you even know what you are doing!?
#undef AddJob
#endif


typedef eng_sprite_rsc::info    eng_sprite_info;
typedef eng_sprite_rsc::group   eng_sprite_set;

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

void compile_sprite_atlas::LoadSprites( xsafe_array<sprite_list, X_PLATFORM_COUNT>& SelfSprites ) const
{
    //
    // Allocate memory require for all the platforms we are going to compile for
    //
    const_ref( rSpriteList, m_KeyObject.m_lEntry );
    for( s32 p=0; p<X_PLATFORM_COUNT; p++ )
    {
        if( ((m_PlatformMask>>p)&1) == 0 )
            continue;
        
        SelfSprites[p].m_SpriteList.appendList( rSpriteList.getCount() );
    }
    
    //
    // Load all the asset sprites
    //
    x_inline_light_jobs_block<1>  JobBlock;

    for( s32 i=0; i<rSpriteList.getCount(); i++) JobBlock.SubmitJob( [i,this,&SelfSprites]()
    {
        xarray<xstring> NameList;
        
        const_ref( rSpriteList, m_KeyObject.m_lEntry );
        for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
        {
            // We are not working on this platform then skip it
            if( ((m_PlatformMask>>p)&1) == 0 )
                continue;
            
            xstring                                         SpritePath;
            const compile_sprite_atlas_key::entry_params&   KeySprite   = rSpriteList[i][p];
            xarray<sprite>&                                 SpriteList  = SelfSprites[p].m_SpriteList;
            sprite&                                         Sprite      = SpriteList[i];
            
            // May sure that we have a reference back to the key file index
            Sprite.m_KeyIndex = i;
            
            // Clear the name list
            NameList.DeleteAllNodes();
            
            // Here is a shortcut. If the user set properties for all the platforms rather than specific ones then
            // we can copy from the all platform
            if ( x_FlagIsOn( rSpriteList[i][0].m_OverrideBits, compile_sprite_atlas_key::entry_params::CHANGE_FILENAME ) &&
                 x_FlagIsOn( KeySprite.m_OverrideBits, compile_sprite_atlas_key::entry_params::CHANGE_FILENAME ) )
            {
                ASSERT( Sprite.m_Core.getCount() > 0 );
                
                // Copy everything from the platform "All Platforms" (0)
                for( s32 c=0; c<Sprite.m_Core.getCount(); c++ )
                {
                    Sprite.m_Core[c].m_Bitmap.Copy( SelfSprites[0].m_SpriteList[i].m_Core[c].m_Bitmap );
                }
                
                //
                // update key information
                //
                if( Sprite.m_bAnimated )
                {
                    SelfSprites[p].m_TotalAnimatedSprites.Inc();
                    Sprite.m_bAnimated = TRUE;
                }
                else
                {
                    Sprite.m_bAnimated = FALSE;
                }
            }
            else
            {
                //
                // Are we dealing with an animated sprite?
                // If so we must determine how many frames it has
                //
                if( KeySprite.m_SrcFileName.FindI("000" ) != -1 )
                {
                    xstring Formated;
                    
                    // Count how many animated sprites we got
                    Sprite.m_bAnimated = TRUE;
                    SelfSprites[p].m_TotalAnimatedSprites.Inc();
                    
                    // format the path for the files
                    Formated.Format( "%s/%s", (const char*)m_ProjectPath, (const char*)KeySprite.m_SrcFileName );
                    
                    const s32 iCountOffset = Formated.FindI("000" );
                    ASSERT( iCountOffset > 0 );
                    
                    xstring Number[2];
                    Number[1].Format("000");
                    for( s32 nEntries=0; 1 ;nEntries++ )
                    {
                        Number[nEntries&1].Format("%03d",nEntries);
                        
                        Formated.SearchAndReplace( Number[1-(nEntries&1)], Number[nEntries&1] );
                        
                        if( FALSE == x_io::FileExists( Formated ) )
                            break;
                        
                        // Save the name for later use
                        NameList.append().Copy( Formated );
                    }
                }
                else
                {
                    Sprite.m_bAnimated = FALSE;
                    NameList.append().Format( "%s/%s", (const char*)m_ProjectPath, (const char*)KeySprite.m_SrcFileName );
                }

                // Allocated all the require
                Sprite.m_Core.appendList( NameList.getCount() );
                
                // We are going to add the total sprites added
                SelfSprites[p].m_TotalUniqueSprites.Add( Sprite.m_Core.getCount() );
                
                //
                // Get ready to process all the cores for this sprite
                //
                for( sprite_core& Core : Sprite.m_Core )
                {
                    const s32 Index = s32( &Core - &Sprite.m_Core[0] );
                    
                    // Now try to load the sprite
                    if( FALSE == xbmp_Load( Core.m_Bitmap, NameList[Index] ) )
                    {
                        x_throw("ERROR: Opening file [%s] \n", (const char*) KeySprite.m_SrcFileName);
                    }
                    
                    //
                    // Nothing to do if we have more than one entry
                    //
                    if( Index > 1 )
                        continue;
                    
                    //
                    // Compute the string Hash
                    //
                    char SpriteFileName[xfile::MAX_FNAME];
                    
                    // Get the file name (without the extension)
                    x_splitpath( KeySprite.m_SrcFileName, NULL, NULL, SpriteFileName, NULL );
                    
                    // Compute the final hash of the file name
                    // TODO: We may want to have a property that is the actual name of the sprite rather than using the source file name
                    Sprite.m_StringHash = (u16)x_strHash( (const char*) SpriteFileName, 0xffff );
                    
                    // make sure it is unique
                    for( s32 u=0; u<i-1; u++ )
                    {
                        const sprite& OtherSprite = SpriteList[u];
                        
                        if( OtherSprite.m_StringHash == Sprite.m_StringHash )
                        {
                            if( rSpriteList[u][p].m_SrcFileName == KeySprite.m_SrcFileName )
                            {
                                x_throw("ERROR: I found two file names which are the same %s", (const char*)KeySprite.m_SrcFileName );
                            }
                            
                            // else...
                            x_throw("ERROR: Bad luck I found a string that hashes in the same way as another.\n"
                                         "Please change one of this file names\n"
                                         "[ %s ] or [ %s ]\n", 
                                         (const char*)rSpriteList[u][p].m_SrcFileName, 
                                         (const char*)KeySprite.m_SrcFileName );
                        }
                    }
                }
            }
        }
    }); JobBlock.FinishJobs();
        
    //
    // Compute base information for the sprites
    //
    for( s32 i=0; i<rSpriteList.getCount(); i++) JobBlock.SubmitJob( [i,this,&SelfSprites]()
    {
        const_ref( rSpriteList, m_KeyObject.m_lEntry );
        for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
        {
            // We are not working on this platform then skip it
            if( ((m_PlatformMask>>p)&1) == 0 )
                continue;
      
            xarray<sprite>&                                 SpriteList  = SelfSprites[p].m_SpriteList;
            sprite&                                         Sprite      = SpriteList[i];
            const compile_sprite_atlas_key::entry_params&   SpriteDesc  = rSpriteList[i][p];
            
            //
            // Go though all the cores... usually there is just one but for animated sprites there are more than one
            //
            for( s32 c=0; c<Sprite.m_Core.getCount(); c++ )
            {
                sprite_core& Core = Sprite.m_Core[c];
                
                //
                // First deal with any transformations that we need to do
                //
                if( SpriteDesc.m_Scale.m_X != 1 || SpriteDesc.m_Scale.m_Y != 1  )
                {
                    if( SpriteDesc.m_ScaleFilter == compile_sprite_atlas_key::SCALEFILTER_BRZ )
                    {
                        xbitmap Temp;
                        
                        // Sanity check to scales
                        if(SpriteDesc.m_Scale.m_X < 1 || SpriteDesc.m_Scale.m_Y < 1 )
                        {
                            x_throw("ERROR: to use the BRZ filter you need to scale larger than 1. Current Scales [%f,%f]", SpriteDesc.m_Scale.m_X, SpriteDesc.m_Scale.m_Y );
                        }
                        
                        // Pick the best resolution for it
                        s32 Scale = (s32)x_Min( 5, x_Ceil( x_Max( SpriteDesc.m_Scale.m_X, SpriteDesc.m_Scale.m_Y ) ));
                        
                        // Lets scale using the BRZ filter first
                        xbmp_FilterBRZ( Temp, Core.m_Bitmap, Scale );
                        
                        f32 FinalScaleX = SpriteDesc.m_Scale.m_X/Scale;
                        f32 FinalScaleY = SpriteDesc.m_Scale.m_Y/Scale;
                        
                        Temp.CreateResizedBitmap( Core.m_Bitmap,
                                                  s32( Temp.getWidth()  * FinalScaleX ),
                                                  s32( Temp.getHeight() * FinalScaleY ) );
                        
                        xbmp_FilterSharpen( Core.m_Bitmap, Core.m_Bitmap );
                    }
                    else
                    {
                        Core.m_Bitmap.CreateResizedBitmap( Core.m_Bitmap,
                            s32( Core.m_Bitmap.getWidth( )  * SpriteDesc.m_Scale.m_X ),
                            s32( Core.m_Bitmap.getHeight( ) * SpriteDesc.m_Scale.m_Y ) );
                    }
                }
                
                //
                // Set the original Width and Height just in case we compact things later
                //
                const s32 OriginalW = Core.m_Bitmap.getWidth();
                const s32 OriginalH = Core.m_Bitmap.getHeight();
                
                //
                // Set the initial positions of the vertices
                //
                Core.m_Vertex[0].m_Pos.m_X = 0;
                Core.m_Vertex[0].m_Pos.m_Y = 0;
                Core.m_Vertex[1].m_Pos.m_X = (f32)Core.m_Bitmap.getWidth();
                Core.m_Vertex[1].m_Pos.m_Y = 0;
                Core.m_Vertex[2].m_Pos.m_X = (f32)Core.m_Bitmap.getWidth( );
                Core.m_Vertex[2].m_Pos.m_Y = (f32)Core.m_Bitmap.getHeight( );
                Core.m_Vertex[3].m_Pos.m_X = 0;
                Core.m_Vertex[3].m_Pos.m_Y = (f32)Core.m_Bitmap.getHeight( );
                
                //
                // Compact as much as possible the sprite (If the user want us to)
                //
                if( SpriteDesc.m_bCompactSprite )
                    CompactSprite( Core );
                
                //
                // Compute the final hotpoint
                //
                {
                    const f32 XOffset = m_KeyObject.ComputeHotPoint( SpriteDesc.m_HotPointXMode, SpriteDesc.m_HotPointXOffset, OriginalW );
                    const f32 YOffset = m_KeyObject.ComputeHotPoint( SpriteDesc.m_HotPointYMode, SpriteDesc.m_HotPointYOffset, OriginalH );
                    
                    for( s32 i=0; i<4; i++ )
                    {
                        Core.m_Vertex[i].m_Pos.m_X -= XOffset;
                        Core.m_Vertex[i].m_Pos.m_Y += YOffset;
                    }
                }
            }
            
            //
            // Lets mark this sprite as ready to be computed
            //
            Sprite.m_bComputed = TRUE;
        }
        
    }); JobBlock.FinishJobs();
}



//---------------------------------------------------------------------------------------

void compile_sprite_atlas::CompactSprite( sprite_core& SpriteCore ) const
{
    const s32     BW      = SpriteCore.m_Bitmap.getWidth();
    const s32     BH      = SpriteCore.m_Bitmap.getHeight();
    const xcolor* pData   = (xcolor*)SpriteCore.m_Bitmap.getMip(0);
    s32     t,b,l,r;
    
    //
    // From Top
    //
    for( t=0; t<BH; t++ )
    {
        xbool bFoundSomething=FALSE;
        for( s32 x=0; x<BW; x++ )
        {
            // Any value in the alpha will trigger the stop condition
            if( pData[ t*BW + x ].m_A )
            {
                bFoundSomething = TRUE;
                break;
            }
        }
        if( bFoundSomething ) break;
    }
    
    //
    // From Bottom
    //
    for( b=BH-1; b>= 0; b-- )
    {
        xbool bFoundSomething=FALSE;
        for( s32 x=0; x<BW; x++ )
        {
            // Any value in the alpha will trigger the stop condition
            if( pData[ b*BW + x ].m_A )
            {
                bFoundSomething = TRUE;
                break;
            }
        }
        if( bFoundSomething ) break;
    }
    
    //
    // From Left
    //
    for( l=0; l<BW; l++ )
    {
        xbool bFoundSomething=FALSE;
        for( s32 y=t; y<=b; y++ )
        {
            // Any value in the alpha will trigger the stop condition
            if( pData[ y*BW + l ].m_A )
            {
                bFoundSomething = TRUE;
                break;
            }
        }
        if( bFoundSomething ) break;
    }
    
    //
    // From Right
    //
    for( r=BW-1; r>=0; r-- )
    {
        xbool bFoundSomething=FALSE;
        for( s32 y=t; y<=b; y++ )
        {
            // Any value in the alpha will trigger the stop condition
            if( pData[ y*BW + r ].m_A )
            {
                bFoundSomething = TRUE;
                break;
            }
        }
        if( bFoundSomething ) break;
    }
    
    //
    // Clip Bitmap Base on our findings
    //
    
    // If we dont have anything to clip bail out
    if( l==0 && t==0 && r==BW-1 && b==BH-1 )
        return;
    
    // Edge case where it found actually no pixels
    if( r < 0 )
    {
        SpriteCore.m_Bitmap.CreateBitmap(1, 1);
        *((u32*)SpriteCore.m_Bitmap.getMip(0)) = 0;
        
        SpriteCore.m_Vertex[0].m_Pos.m_X = 0;
        SpriteCore.m_Vertex[0].m_Pos.m_Y = 0;
        SpriteCore.m_Vertex[1].m_Pos.m_X = 0;
        SpriteCore.m_Vertex[1].m_Pos.m_Y = 0;
        SpriteCore.m_Vertex[2].m_Pos.m_X = 0;
        SpriteCore.m_Vertex[2].m_Pos.m_Y = 0;
        SpriteCore.m_Vertex[3].m_Pos.m_X = 0;
        SpriteCore.m_Vertex[3].m_Pos.m_Y = 0;

        return;
    }
        
    //
    // make into a new bitmap
    //
    
    xbitmap NewBitmap;
    const s32     NW = r-l+1;
    const s32     NH = b-t+1;
    
    NewBitmap.CreateBitmap( NW, NH );
    xcolor* pNewData = (xcolor*)NewBitmap.getMip(0);
    
    for(s32 y=t, y1=0; y1<NH; y++, y1++ )
    for(s32 x=l, x1=0; x1<NW; x++, x1++ )
    {
        pNewData[ x1 + y1*NW ] = pData[ x + y*BW ];
    }
    
    //
    // Replace the old with the new
    //
    SpriteCore.m_Bitmap.Copy( NewBitmap );
    
    
    //
    // Handle the vertices
    //

    // This crazy formula is to compensate for the OPENGL YAxis Flip
    {
        s32 d = t;
        t = BH - b;
        b = BH - d;
    }
    
    // set the final coordinates
    SpriteCore.m_Vertex[ 0 ].m_Pos.m_X = (f32)l;
    SpriteCore.m_Vertex[ 0 ].m_Pos.m_Y = (f32)t;
    SpriteCore.m_Vertex[ 1 ].m_Pos.m_X = (f32)r;
    SpriteCore.m_Vertex[ 1 ].m_Pos.m_Y = (f32)t;
    SpriteCore.m_Vertex[ 2 ].m_Pos.m_X = (f32)r;
    SpriteCore.m_Vertex[ 2 ].m_Pos.m_Y = (f32)b;
    SpriteCore.m_Vertex[ 3 ].m_Pos.m_X = (f32)l;
    SpriteCore.m_Vertex[ 3 ].m_Pos.m_Y = (f32)b;
}

//---------------------------------------------------------------------------------------

void compile_sprite_atlas::GeneratePlatformSpecificAtlas(
    const platform&                         Target,
    const xptr<xbmp_atlast::rect_xywhf*>&   RectanglePtr,
    const xptr<xbmp_atlast::rect_xywhf>&    Rectangles,
    const xptr<mapping>                     Mapping ) const
{
    const sprite_list&      SpritePlatform   = m_Sprites[ Target.m_Platform ];
    const xarray<sprite>&   SpriteList       = m_Sprites[ Target.m_Platform ].m_SpriteList;
    
    //
    // Ask the atlas to pack them
    //
    xarray<xbmp_atlast::bin>    Bins;
    const_ref( rMain, m_KeyObject.m_Main );
    {
        xbmp_atlast                 AtlasPacker;
        xbmp_atlast::pack_mode      Mode = xbmp_atlast::PACKMODE_ANYSIZE;
        
        //
        // Are we going to use texture compression if so then pick the right sizes
        //
        if( rMain[Target.m_Platform].m_AtlastCompression > 0 )
        {
            // 16bits textures like to be multiple of some number (Here assuming 4)
            // PC fails if this is not true. So I am forcing this out.

            if( Target.m_Platform == X_PLATFORM_IOS ||
                Target.m_Platform == X_PLATFORM_ANDROID )
            {
                if ( rMain[ Target.m_Platform ].m_AtlastCompression > 0.3 )
                    Mode = xbmp_atlast::PACKMODE_POWER_OF_TWO_SQUARE;
                else 
                    Mode = xbmp_atlast::PACKMODE_MULTIPLE_OF_4;
            }
            else if( Target.m_Platform == X_PLATFORM_OSX ||
                     Target.m_Platform == X_PLATFORM_PC )
            {
                if ( rMain[ Target.m_Platform ].m_AtlastCompression > 0.3 )
                    Mode = xbmp_atlast::PACKMODE_MULTIPLE_OF_8;
                else
                    Mode = xbmp_atlast::PACKMODE_MULTIPLE_OF_4;
            }
        }
        
        s32 MaxResolutionSquare;
        if( Target.m_Platform == X_PLATFORM_IOS ||
            Target.m_Platform == X_PLATFORM_ANDROID )
        {
            MaxResolutionSquare = 1024;
        }
        else
        {
            MaxResolutionSquare = 2048;
        }
        
        AtlasPacker.Pack( &RectanglePtr[0],
                         RectanglePtr.getCount(),
                         MaxResolutionSquare,
                         Bins,
                         Mode );

        if( Bins.getCount() != 1 )
        {
            x_throw("ERROR: I was not able to pack all those textures into a single atlas" );
        }
    }
    
    //
    // Create the atlas texture
    //
    xbmp_atlast::bin&   Bin = Bins[0];
    xbitmap             AtlasTexture;

    AtlasTexture.CreateBitmap( Bin.m_Size.m_W, Bin.m_Size.m_H );

    // Clear the memory
    x_memset( (void*)AtlasTexture.getMip(0), 0, AtlasTexture.getFrameSize() );
    
    //
    // Copy the bitmaps into the right place in the
    //
    xptr<xsafe_array<vertex,4>>  FinalSpritesVerts;
    
    {
        u32*        pAtlas  = (u32*)AtlasTexture.getMip(0);
        const s32   AW      = AtlasTexture.getWidth();
        const s32   AH      = AtlasTexture.getHeight();
        
        // Create our own list of sprites (each platform may be different)
        FinalSpritesVerts.Alloc( SpritePlatform.m_TotalUniqueSprites.get() );
        
        for( s32 i=0; i<Bin.m_Rects.getCount(); i++ )
        {
            const xbmp_atlast::rect_xywhf&  Rect        = *Bin.m_Rects[i];
            const s32                       Index       = (s32)(&Rect - &Rectangles[0]);
            xsafe_array<vertex,4>&          FinalVer    = FinalSpritesVerts[Index];
            const mapping&                  Map         = Mapping[Index];
            const sprite_core&              Core        = SpriteList[Map.m_iSprite].m_Core[Map.m_iCore];
            const xbitmap&                  Bitmap      = Core.m_Bitmap;
            const u32*                      pBitmap     = (const u32*)Bitmap.getMip(0);
            
            // Copy the sprite vertices into the final destination
            FinalVer = Core.m_Vertex;
            
            //
            // Copy the bitmap into the atlast and compute the vertices
            //
            if( Rect.m_bFlipped )
            {
                // Need to write the code for the flip case....
                const s32 BW = Bitmap.getWidth();
                const s32 BH = Bitmap.getHeight();
                
                ASSERT( BW == Rect.getHeight() );
                ASSERT( BH == Rect.getWidth() );
                
                for( s32 y=0; y<BW; y++ )
                for( s32 x=0; x<BH; x++ )
                {
                    pAtlas[ (Rect.m_X + x) + (Rect.m_Y + y)*AW ] = pBitmap[ (BH-x-1) * BW + y  ];
                }
                
                FinalVer[0].m_UV.m_X  = (Rect.m_X + 0) / (f32)AW;
                FinalVer[0].m_UV.m_Y  = (Rect.m_Y + 0) / (f32)AH;
                
                FinalVer[1].m_UV.m_X  = (Rect.m_X +  0) / (f32)AW;
                FinalVer[1].m_UV.m_Y  = (Rect.m_Y + BW) / (f32)AH;
                
                FinalVer[2].m_UV.m_X  = (Rect.m_X + BH) / (f32)AW;
                FinalVer[2].m_UV.m_Y  = (Rect.m_Y + BW) / (f32)AH;
                
                FinalVer[3].m_UV.m_X  = (Rect.m_X + BH) / (f32)AW;
                FinalVer[3].m_UV.m_Y  = (Rect.m_Y +  0) / (f32)AH;
            }
            else
            {
                const s32 BW = Bitmap.getWidth();
                const s32 BH = Bitmap.getHeight();
                
                ASSERT( BH == Rect.getHeight() );
                ASSERT( BW == Rect.getWidth() );
                
                for( s32 y=0; y<BH; y++ )
                for( s32 x=0; x<BW; x++ )
                {
                    pAtlas[ (Rect.m_X + x) + (Rect.m_Y + y)*AW ] = pBitmap[ x + y * BW ];
                }
                
                FinalVer[0].m_UV.m_X  = (Rect.m_X +  0) / (f32)AW;
                FinalVer[0].m_UV.m_Y  = (Rect.m_Y + BH) / (f32)AH;
                
                FinalVer[1].m_UV.m_X  = (Rect.m_X + BW) / (f32)AW;
                FinalVer[1].m_UV.m_Y  = (Rect.m_Y + BH) / (f32)AH;
                
                FinalVer[2].m_UV.m_X  = (Rect.m_X + BW) / (f32)AW;
                FinalVer[2].m_UV.m_Y  = (Rect.m_Y +  0) / (f32)AH;
                
                FinalVer[3].m_UV.m_X  = (Rect.m_X +  0) / (f32)AW;
                FinalVer[3].m_UV.m_Y  = (Rect.m_Y +  0) / (f32)AH;
            }
        }
    }
    
    //
    // Now we want to organized the vertex in the following manner.
    // [Non-Animated Sprites+animated sprite headers] + [Animated Sprite 1] + [Animated Sprite 2] + ...
    // For any animated sprite we also want to have a single entry inside the Non-Aminated sprite
    // because we are going to use that memory to store information about the animation
    //
    xptr<eng_sprite_rsc::info>  FinalOrderSpritesVerts;

    // Now we can allocate all our vertex
    FinalOrderSpritesVerts.Alloc( SpritePlatform.m_TotalUniqueSprites.get() + SpritePlatform.m_TotalAnimatedSprites.get() );
    
    // Lets first add all the standard sprites
    s32 Index = 0;
    s32 iAllocAnimatedFrames=0;
    const_ref( rSpriteList, m_KeyObject.m_lEntry );
    for( const sprite& Sprite : SpriteList )
    {
//        const s32                                       i           = s32( &Sprite - &SpriteList[0] );
        eng_sprite_rsc::info&                           FOVert      = FinalOrderSpritesVerts[Index++];
        xsafe_array<vertex,4>&                          VFinal      = FinalSpritesVerts[ Sprite.m_GlobaIndex ];
        const compile_sprite_atlas_key::entry_params&   SpriteDesc  = rSpriteList[ Sprite.m_KeyIndex ][ Target.m_Platform ];
                                                                                           
        if( Sprite.m_bAnimated )
        {
            FOVert.m_Animated.m_SpecialMask = 0xffff;
            FOVert.m_Animated.m_nFrames     = Sprite.m_Core.getCount();
            FOVert.m_Animated.m_FPS         = SpriteDesc.m_FPS;
            FOVert.m_Animated.m_PlayBack    = SpriteDesc.m_PlayBackMode;
            FOVert.m_Animated.m_iBase       = iAllocAnimatedFrames + SpriteList.getCount();
            FOVert.m_Animated.m_nEvents     = 0;
            FOVert.m_Animated.m_Padding     = 0;
            
            // Make the newly allocated verts
            iAllocAnimatedFrames += Sprite.m_Core.getCount();
            
            //
            // Copy all the verts into the right place
            //
            for( s32 c=0; c<Sprite.m_Core.getCount(); c++ )
            {
                // Add all the verts into our pool
                const xsafe_array<vertex,4>&    CVFinal = FinalSpritesVerts[ Sprite.m_GlobaIndex + c ];
                eng_sprite_rsc::info&           CFOVert = FinalOrderSpritesVerts[ FOVert.m_Animated.m_iBase + c ];
                
                for( s32 v=0; v<VFinal.getCount(); v++ )
                {
                    CFOVert.m_Vertex[ v ].m_Pos.m_X = CVFinal[ v ].m_Pos.m_X;
                    CFOVert.m_Vertex[ v ].m_Pos.m_Y = CVFinal[ v ].m_Pos.m_Y;
                    CFOVert.m_Vertex[ v ].m_UV.m_X  = CVFinal[ v ].m_UV.m_X;
                    CFOVert.m_Vertex[ v ].m_UV.m_Y  = CVFinal[ v ].m_UV.m_Y;
                }
            }
        }
        else
        {
            ASSERT( Sprite.m_Core.getCount() == 1 );
            
            for( s32 v=0; v<VFinal.getCount(); v++ )
            {
                FOVert.m_Vertex[ v ].m_Pos.m_X = VFinal[ v ].m_Pos.m_X;
                FOVert.m_Vertex[ v ].m_Pos.m_Y = VFinal[ v ].m_Pos.m_Y;
                FOVert.m_Vertex[ v ].m_UV.m_X = VFinal[ v ].m_UV.m_X;
                FOVert.m_Vertex[ v ].m_UV.m_Y = VFinal[ v ].m_UV.m_Y;
            }
        }
    }
    
    // Make sure we did everything right
    ASSERT( iAllocAnimatedFrames + SpriteList.getCount() - SpritePlatform.m_TotalAnimatedSprites.get() == SpritePlatform.m_TotalUniqueSprites.get() );
    
    //
    // Ok Create our SpriteSet
    //
    eng_sprite_set SpriteSet;

    //
    // Compile the hash into a table
    // Also fill in all the data for SpriteSet
    //
    xptr<u16>   HashMemory;
    {
        
        //
        // Create the final sprite info
        //
        SpriteSet.m_nCores               = SpritePlatform.m_TotalUniqueSprites.get() + SpritePlatform.m_TotalAnimatedSprites.get();
        SpriteSet.m_nSprites             = SpriteList.getCount();
        
        // Allocate memory for it
        HashMemory.Alloc( SpriteSet.m_nSprites );
        
        // Set memory to the pointers
        SpriteSet.m_SpriteInfo.m_Ptr     = &FinalOrderSpritesVerts[0];
        SpriteSet.m_SpriteNameHash.m_Ptr = &HashMemory[0];
        
        // Copy all the hash data
        for( s32 i=0; i<SpriteSet.m_nSprites; i++ )
        {
            const sprite&    Sprite  = SpriteList[i];
            HashMemory[i] = Sprite.m_StringHash;
        }
    }

    //
    // Time to save everything
    //
    {
        //
        // Safe the file info
        //
        xserialfile SerialFile;
        xstring     InfoAtlastFile;
        
        InfoAtlastFile = getFinalResourceName( Target );
        
        SerialFile.Save( (const char*)InfoAtlastFile, SpriteSet, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
        
        // for debugging
        if( 1 )
        {
            xserialfile test;
            eng_sprite_set* pSet;
            test.Load( (const char*)InfoAtlastFile, pSet );
            
          //  eng_sprite_rsc::info* pInfo = &pSet->m_SpriteInfo.m_Ptr[6];
           // eng_sprite_rsc::info* pSetInfo = &SpriteSet.m_SpriteInfo.m_Ptr[6];
            
            ASSERT( pSet );
            x_delete( pSet );
        }
    }

    //
    // Save the atlas texture tga format
    //
    xstring TextureAtlasFullPath;
 
    TextureAtlasFullPath.Format( "%s/%s[%s]--%s.tga",
                                (const char*)m_IntermediateDataPath,
                                (const char*)m_RscFileName,
                                x_PlatformString( Target.m_Platform ),
                                (const char*)m_RscGuid.GetAlphaString()
                                );

    //
    // Clear out the alpha if requested by the user
    // TODO: This should not be a parameter for each of the sprites should only just be as one global parameter
    //
    const compile_sprite_atlas_key::main_params&    MainDesc = rMain[Target.m_Platform];

    if( MainDesc.m_bDisableAlpha )
    {
        const s32 Count  = AtlasTexture.getFrameSize()/4;
        xcolor*   pColor = (xcolor*)AtlasTexture.getMip(0);
        for( s32 i=0; i<Count; i++ )
        {
            pColor[i].m_A = 0xff;
        }
    }
    
    //
    // Save file
    //
    if( AtlasTexture.SaveTGA( TextureAtlasFullPath ) == FALSE )
    {
        x_throw("ERROR: Fail to create the destination file %s", (const char*) &TextureAtlasFullPath[ m_ProjectPath.GetLength()+1 ] );
    }
}


//---------------------------------------------------------------------------------------

void compile_sprite_atlas::onCompile( void )
{
    m_KeyObject.m_lEntry.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    m_KeyObject.m_Main.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );

    //
    // Load all the sprites and the short them by their hash
    //
    LoadSprites( m_Sprites );
    
    //
    // Pre-compile the sprite atlas
    //
    x_inline_light_jobs_block<8>  JobBlock;
    for( s32 p=0; p<X_PLATFORM_COUNT; p++ )
        if( (m_PlatformMask>>p)&1 )
            JobBlock.SubmitJob( [this, p]()
    {
        xarray<sprite>& SpriteList = m_Sprites[p].m_SpriteList;
        const s32       TotalCores = m_Sprites[p].m_TotalUniqueSprites.get();
        
        //
        // First lets go though the list and delete any empty entry we may have
        //
        for( s32 i=0; i<SpriteList.getCount(); i++ )
        {
            if( SpriteList[i].m_bComputed == FALSE )
            {
                SpriteList.DeleteWithSwap(i);
                i--;
            }
        }
        
        if( SpriteList.getCount() == 0 )
            x_throw("ERROR: We have not sprites to compile. You must have at least one." );

        //
        // Now we can short our list by the Hash Name so later we can binsearch for our names
        //
        x_qsort( &SpriteList[0],
                  SpriteList.getCount(),
                  sizeof(sprite),
                 []( const void* pItem1, const void* pItem2 )-> s32
        {
            const sprite* pS1 = (const sprite*)pItem1;
            const sprite* pS2 = (const sprite*)pItem2;
            
            if( pS1->m_StringHash < pS2->m_StringHash ) return -1;
            return ( pS1->m_StringHash > pS2->m_StringHash );
        });
        
        // Verify that the shorting is working
        ASSERT( SpriteList[0].m_StringHash <= SpriteList[SpriteList.getCount()-1].m_StringHash );
        
        
        //
        // create all the rects to be packed
        //
        xptr<xbmp_atlast::rect_xywhf>   Rectangles;
        xptr<xbmp_atlast::rect_xywhf*>  RectanglePtr;
        xptr<mapping>                   Mapping;
        
        RectanglePtr.New( TotalCores );
        Rectangles.New  ( TotalCores );
        Mapping.New     ( TotalCores );
        
        s32 iTotalSprites = 0;
        for( sprite& Sprite : SpriteList )
        {
            // Set the global index and later we will use it for other stuff
            Sprite.m_GlobaIndex = iTotalSprites;
            
            // Now go thought all our cores
            for( sprite_core& Core : Sprite.m_Core )
            {
                const s32 W = Core.m_Bitmap.getWidth();
                const s32 H = Core.m_Bitmap.getHeight();
                
                Rectangles  [ iTotalSprites ] = xbmp_atlast::rect_xywhf( 0, 0, W, H );
                RectanglePtr[ iTotalSprites ] = &Rectangles[ iTotalSprites ];
                
                Mapping     [ iTotalSprites ].m_iSprite = s32( &Sprite - &SpriteList[0] );
                Mapping     [ iTotalSprites ].m_iCore   = s32( &Core - &Sprite.m_Core[0] );
                
                iTotalSprites++;
            }
        }
        ASSERT( iTotalSprites == TotalCores );
        
        //
        // Ok we are ready to pack all of our sprites
        //
        GeneratePlatformSpecificAtlas( m_Target[p], RectanglePtr, Rectangles, Mapping );
    
    
    } ); JobBlock.FinishJobs();
    
    //
    // Done
    //
    // = m_ExternalKeys.append();
    
    //
    // Deal with External Keys dependency list
    //
    texture_compiler_key    TextureKey( *this );
    xstring                 TextureKeyPath;
    
    eng_resource_guid       Guid;
    Guid.setup( m_RscGuid, eng_texture_rsc::UID );
    
    if( m_RscFileName.IsEmpty() )
    {
        TextureKeyPath.Format( "%s/%s.%s.txt",
                              (const char*)m_ExternalCompilerKeysPath,
                              (const char*)Guid.getAlphaString(),
                              (const char*)TextureKey.getCompiledExt() );
    }
    else
    {
        TextureKeyPath.Format( "%s/%s--%s.%s.txt",
                              (const char*)m_ExternalCompilerKeysPath,
                              (const char*)m_RscFileName,
                              (const char*)Guid.getAlphaString(),
                              (const char*)TextureKey.getCompiledExt() );
    }
        
        
    //
    // Setup the texture compiler key
    //
    TextureKey.LoadDefaults( m_ProjectPath );
 
    // First lets set some Global PArameters
    const_ref( rMain, m_KeyObject.m_Main );
    g_PropBool.Set  ( TextureKey, "Key/Main/Global/bCreateMips", FALSE  );
    g_PropEnum.Set  ( TextureKey, "Key/Main/Global/U-Tiling",    TextureKey.getTileEnum  ( texture_compiler_key::TILEMODE_CLAMP      ) );
    g_PropEnum.Set  ( TextureKey, "Key/Main/Global/V-Tiling",    TextureKey.getTileEnum  ( texture_compiler_key::TILEMODE_CLAMP      ) );
    g_PropEnum.Set  ( TextureKey, "Key/Main/Global/FilterMig",   TextureKey.getFilterEnum( rMain[0].m_FilterMig ) );
    g_PropEnum.Set  ( TextureKey, "Key/Main/Global/FilterMag",   TextureKey.getFilterEnum( rMain[0].m_FilterMag ) );
    
    //g_PropFloat.Set ( TextureKey, "Global/CompressionLevel",  0 );
    
    // Now we have to set the name of the texture for each of the platforms
    // Weather we compile for them or not
    s32 LengthProject = m_ProjectPath.GetLength()+1;
    for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
    {
        xstring TextureAtlasFullPath;
        const char* pPlatformString = x_PlatformString( xplatform(p) );
        
        TextureAtlasFullPath.Format( "%s/%s[%s]--%s.tga",
                                    (const char*)m_IntermediateDataPath,
                                    (const char*)m_RscFileName,
                                    (const char*)pPlatformString,
                                    (const char*)m_RscGuid.GetAlphaString()
                                    );
        
        g_PropFilePath.Set( TextureKey, xfs("Key/Main/%s/FileName", pPlatformString ), &TextureAtlasFullPath[LengthProject] );
        g_PropFloat.Set( TextureKey, xfs("Key/Main/%s/CompressionLevel", pPlatformString ), rMain[p].m_AtlastCompression );
    }

    //
    // Append the dependency into our list and save the dependency file
    //
    AddExternalDependencyKey( xstring::BuildFromFormat( &TextureKeyPath[LengthProject] ) );
    TextureKey.Save( TextureKeyPath );

    //
    // change the key object back to default 
    //
    m_KeyObject.m_lEntry.ChangeBehavior( 0 );
    //m_KeyObject.m_Main.ChangeBehavior( 0 );
}
