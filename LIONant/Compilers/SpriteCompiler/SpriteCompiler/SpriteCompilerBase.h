//
//  SpriteCompiler.h
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef SPRITE_COMPILER_H
#define SPRITE_COMPILER_H

#include "compilerBase.h"
#include "SpriteCompilerKey.h"
#include "xbmp_Tools.h"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

class compile_sprite_atlas : public compiler_base
{
public:
    
    compile_sprite_atlas( void ) : m_KeyObject( *this ) {}
    
protected:
    
    struct vertex
    {
        xvector2 m_Pos;
        xvector2 m_UV;
    };

    struct sprite_core
    {
        xbitmap                     m_Bitmap;
        xsafe_array<vertex,4>       m_Vertex;
    };
    
    struct sprite
    {
        xbool operator < ( const sprite& A ) const { return m_StringHash < A.m_StringHash; }
        
        xbool                       m_bAnimated;
        s32                         m_GlobaIndex;           // This is the temporary global index of the vertex
        s32                         m_KeyIndex;             // This is the original sprite index and a map back to the key index entry
        xarray<sprite_core>         m_Core;
        u16                         m_StringHash;
        xbool                       m_bComputed{FALSE};
    };
    
    struct sprite_list
    {
        xarray<sprite>              m_SpriteList;
        x_qt_counter                m_TotalUniqueSprites;       // Including frames from animated sprites
        x_qt_counter                m_TotalAnimatedSprites;     // Number of animated sprites
    };
    
    struct external_textures_list
    {
        xarray<xstring>             m_FileNameList;
    };

    struct mapping
    {
        s32     m_iSprite;
        s32     m_iCore;
    };
    
protected:
   
    virtual void                            onCompile                       ( void ) override ;
    void                                    LoadSprites                     ( xsafe_array<sprite_list, X_PLATFORM_COUNT>& SelfSprites ) const;
    void                                    CompactSprite                   ( sprite_core& Sprite ) const;
    virtual compiler_key_object&            getKeyObject                    ( void ) override { return m_KeyObject; }
    virtual const compiler_key_object&      getKeyObject                    ( void ) const override { return m_KeyObject; }
    void                                    GeneratePlatformSpecificAtlas   ( const platform&                   Target,
                                                                              const xptr<xbmp_atlast::rect_xywhf*>&   RectanglePtr,
                                                                              const xptr<xbmp_atlast::rect_xywhf>&    Rectangles,
                                                                              const xptr<mapping>                     Mapping ) const;
    
protected:
    
    xsafe_array<sprite_list, X_PLATFORM_COUNT>              m_Sprites;
    compile_sprite_atlas_key                                m_KeyObject;
};

#endif
