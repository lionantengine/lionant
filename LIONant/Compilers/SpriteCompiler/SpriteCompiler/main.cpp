//
//  main.cpp
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/16/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "SpriteCompilerBase.h"

// -BUILDTYPE RELEASE -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/ProjectAlpha/Data" -TARGET OSX IOS -INPUT "GameData/CompilerKeys/test--1ui1_jzcw_tukx.iatlas"
// -OVERWRITE_DEFAULTS -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/ProjectAlpha/Data"
//---------------------------------------------------------------------------------------

int main(int argc, const char * argv[])
{
    compile_sprite_atlas    Compiler;
    
    g_bExceptionBreak = FALSE;
    
    //
    // Parse the command line
    //
    if( Compiler.Parse( argc, argv ) )
    {
        return Compiler.Compile();
    }
    
    return 1;
}

