//
//  SpriteCompilerKey.h
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/4/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef SPRITE_COMPILER_KEY_H
#define SPRITE_COMPILER_KEY_H

#include "eng_base.h"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

class compile_sprite_atlas_key : public compiler_key_object
{
public:
    
    enum scale_filer
    {
        SCALEFILTER_NORMAL,
        SCALEFILTER_BRZ,
        SCALEFILTER_COUNT
    };

    enum hot_point
    {
        HOTPOINT_ZERO_RELATIVE,
        HOTPOINT_MIDDLE_RELATIVE,
        HOTPOINT_ONE_RELATIVE,
        HOTPOINT_COUNT
    };
    
    enum sprite_anim_playback
    {
        PLAYBACK_ONES,
        PLAYBACK_ONES_RESET,
        PLAYBACK_LOOP,
        PLAYBACK_PINGPONG,
        PLAYBACK_COUNT
    };
   
    struct main_params : public main_params_base_link<main_params>
    {
        enum overwrites
        {
            CHANGE_MAIN_ATLAS_COMPRESSION   = X_BIT(0),
            CHANGE_MAIN_DISABLE_ALPHA       = X_BIT(1),
            CHANGE_MAIN_FILTER_MIG          = X_BIT(2),
            CHANGE_MAIN_FILTER_MAG          = X_BIT(3),
            CHANGE_MAIN_COMPUTE_MIPS        = X_BIT(4),
        };

        virtual void    onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool   onPropQuery                 ( xproperty_query& Query ) override final;
                void    onUpdateFromSrc             ( const main_params& Src, const u64 Masks );
        
        static const xprop_enum::entry      m_FilterEnum[ eng_texture::FILTER_MINMAG_COUNT ];

        f32                                 m_AtlastCompression     = 0.5;
        xbool                               m_bDisableAlpha         = FALSE;
        eng_texture::filter_mode            m_FilterMig             = eng_texture::FILTER_MINMAG_NEAREST;
        eng_texture::filter_mode            m_FilterMag             = eng_texture::FILTER_MINMAG_NEAREST;
        xbool                               m_bComputeMips          = FALSE;
    };

    struct entry_params : public entry_params_base_link<entry_params>
    {
        enum overwrites
        {
            CHANGE_FILENAME                 = X_BIT(0),
            CHANGE_NFRAMES                  = X_BIT(1),
            CHANGE_XSCALE                   = X_BIT(2),
            CHANGE_YSCALE                   = X_BIT(3),
            CHANGE_SCALE_FILTER             = X_BIT(4),
            CHANGE_COMPACT_SPRITE           = X_BIT(5),
            CHANGE_HOTPOINT_XMODE           = X_BIT(6),
            CHANGE_HOTPOINT_YMODE           = X_BIT(7),
            CHANGE_HOTPOINT_XOFFSET         = X_BIT(8),
            CHANGE_HOTPOINT_YOFFSET         = X_BIT(9),
            CHANGE_PLAYBACK_MODE            = X_BIT(10),
            CHANGE_FPS                      = X_BIT(11),
        };

        virtual void    onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool   onPropQuery         ( xproperty_query& Query ) override final;
        void            onUpdateFromSrc     ( const entry_params& Src, const u64 Masks );
        
        static const xprop_enum::entry m_HotpointEnum[ HOTPOINT_COUNT ];
        static const xprop_enum::entry m_PlayBackEnum[ PLAYBACK_COUNT ];
        static const xprop_enum::entry m_ScaleFilterEnum[ SCALEFILTER_COUNT ];
        
        xstring                 m_SrcFileName       = X_STR("");            // Asset File Name
        s32                     m_nFrames           = 1;                    // Number of frames when the file name has hinted animation "000"
        xvector2                m_Scale             = {1,1};                // Scaling of the asset before going into the atlas
        scale_filer             m_ScaleFilter       = SCALEFILTER_NORMAL;   // Type of filter that it will use to scale sprites
        xbool                   m_bCompactSprite    = TRUE;                 // Try to pack it as tright as possible using the Alpha for guidence
        
        hot_point               m_HotPointXMode     = HOTPOINT_ZERO_RELATIVE;
        hot_point               m_HotPointYMode     = HOTPOINT_ZERO_RELATIVE;
        f32                     m_HotPointXOffset   = 0;
        f32                     m_HotPointYOffset   = 0;
        
        sprite_anim_playback    m_PlayBackMode      = PLAYBACK_LOOP;
        s32                     m_FPS               = 10;           // Playback rate in frames per second
    }; 

public:

                                compile_sprite_atlas_key    ( const compiler_base& Base ) : compiler_key_object( Base ){}
    f32                         ComputeHotPoint             ( hot_point HotpointMode, f32 Offset, s32 DimensionSize ) const;
    const xprop_enum::entry&    getHotpointEnum             ( hot_point HotPoint ) const            { return entry_params::m_HotpointEnum[HotPoint]; }
    virtual const char*         getCompiledExt              ( void ) const override                 { return "iatlas";          }
    virtual const char*         getCompilerName             ( void ) const override                 { return "SpriteCompiler";  }
    const xprop_enum::entry&    getFilterMode               ( eng_texture::filter_mode Mode ) const { return main_params::m_FilterEnum[ Mode]; }
    
    KEY_STANDARD_STUFF

protected:
    
    friend class compile_sprite_atlas;
};


#endif
