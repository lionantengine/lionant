//
//  SpriteCompilerKey.cpp
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/4/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "SpriteCompilerBase.h"

const xprop_enum::entry compile_sprite_atlas_key::main_params::m_FilterEnum[ eng_texture::FILTER_MINMAG_COUNT ] =
{
    { eng_texture::FILTER_MINMAG_NEAREST,                       X_STR( "NEAREST" ) },
    { eng_texture::FILTER_MINMAG_LINEAR,                        X_STR( "LINEAR" ) },
    { eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_NEAREST,        X_STR( "NEAREST_NEAREST" ) },
    { eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_NEAREST,         X_STR( "LINEAR_NEAREST" ) },
    { eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_LINEAR,         X_STR( "NEAREST_LINEAR" ) },
    { eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_LINEAR,          X_STR( "LINEAR_LINEAR" ) }
};

const xprop_enum::entry compile_sprite_atlas_key::entry_params::m_HotpointEnum[ compile_sprite_atlas_key::HOTPOINT_COUNT ]
{
    { HOTPOINT_ZERO_RELATIVE,           X_STR( "ZERO" ) },
    { HOTPOINT_MIDDLE_RELATIVE,         X_STR( "MIDDLE" ) },
    { HOTPOINT_ONE_RELATIVE,            X_STR( "ONE" ) }
};

const xprop_enum::entry compile_sprite_atlas_key::entry_params::m_PlayBackEnum[ compile_sprite_atlas_key::PLAYBACK_COUNT ]
{
    { PLAYBACK_ONES,            X_STR( "ONES" ) },
    { PLAYBACK_ONES_RESET,      X_STR( "ONES_RESET" ) },
    { PLAYBACK_LOOP,            X_STR( "LOOP" ) },
    { PLAYBACK_PINGPONG,        X_STR( "PINGPONG" ) }
};

const xprop_enum::entry compile_sprite_atlas_key::entry_params::m_ScaleFilterEnum[ compile_sprite_atlas_key::SCALEFILTER_COUNT ]
{
    { SCALEFILTER_NORMAL,       X_STR( "NORMAL" ) },
    { SCALEFILTER_BRZ,          X_STR( "BRZ" ) }
};

//---------------------------------------------------------------------------------------

void compile_sprite_atlas_key::entry_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "FileName",     CHANGE_FILENAME,                                "File name for the sprite that needs compiling into the atlas.", "*.tga,*.xbmp,*.png,*.bmp" )
    KEY_PROP_ENUM( "ScaleX",            CHANGE_XSCALE,                  g_PropFloat,    "X Scales the sprite before putting it in the atlast." )
    KEY_PROP_ENUM( "ScaleY",            CHANGE_YSCALE,                  g_PropFloat,    "Y Scales the sprite before putting it in the atlast." )
    KEY_PROP_ENUM( "ScaleFilter",       CHANGE_SCALE_FILTER,            g_PropEnum,     "Type of filter used to Scale the image NORMAL or BRZ." )
    KEY_PROP_ENUM( "bCompactSprite",    CHANGE_COMPACT_SPRITE,          g_PropBool,     "Try to pack it as tright as possible using the Alpha for guidence." )
    KEY_PROP_ENUM( "HotpointXMode",     CHANGE_HOTPOINT_XMODE,          g_PropEnum,     "Hotpoint relative X coordinate. Zero means left, One means right. " )
    KEY_PROP_ENUM( "HotpointXOffset",   CHANGE_HOTPOINT_XOFFSET,        g_PropFloat,    "Hotpoint X coordinate relative to its XMode." )
    KEY_PROP_ENUM( "HotpointYMode",     CHANGE_HOTPOINT_YMODE,          g_PropEnum,     "Hotpoint relative Y coordinate. Zero means bottom, One means Top. " )
    KEY_PROP_ENUM( "HotpointYOffset",   CHANGE_HOTPOINT_YOFFSET,        g_PropFloat,    "Hotpoint Y coordinate relative to its YMode." )
  
    //
    // Animations properties
    //  
    {
        s32 iParent = iScope;
        s32 iScope  =  Enum.AddScope( iParent, "Animation", "This properties are specific for animation");
        KEY_PROP_ENUM( "PlayBackMode",      CHANGE_PLAYBACK_MODE,           g_PropEnum,     "How you want the sprite animation to be played, ONES, ONES_RESET, LOOP or PINGPONG" )
        KEY_PROP_ENUM( "FPS",               CHANGE_FPS,                     g_PropInt,      "Playback rate in frames per second (slowest is 1 frame per second)" )
    }
}

//---------------------------------------------------------------------------------------

xbool compile_sprite_atlas_key::entry_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "FileName",          CHANGE_FILENAME,               g_PropFilePath.Query( Query, m_SrcFileName )                    )                
    KEY_PROP_QUERY( "ScaleX",            CHANGE_XSCALE,                 g_PropFloat.Query( Query, m_Scale.m_X, 0.001f, 10)              )   
    KEY_PROP_QUERY( "ScaleY",            CHANGE_YSCALE,                 g_PropFloat.Query( Query, m_Scale.m_Y, 0.001f, 10)              )    
    KEY_PROP_QUERY( "ScaleFilter",       CHANGE_SCALE_FILTER,           g_PropEnum.Query( Query, m_ScaleFilter, m_ScaleFilterEnum )     ) 
    KEY_PROP_QUERY( "bCompactSprite",    CHANGE_COMPACT_SPRITE,         g_PropBool.Query( Query, m_bCompactSprite)                      )     
    KEY_PROP_QUERY( "HotpointXMode",     CHANGE_HOTPOINT_XMODE,         g_PropEnum.Query( Query, m_HotPointXMode, m_HotpointEnum )      )     
    KEY_PROP_QUERY( "HotpointXOffset",   CHANGE_HOTPOINT_XOFFSET,       g_PropFloat.Query( Query, m_HotPointXOffset, -1024, 1024)         )
    KEY_PROP_QUERY( "HotpointYMode",     CHANGE_HOTPOINT_YMODE,         g_PropEnum.Query( Query, m_HotPointYMode, m_HotpointEnum )      )
    KEY_PROP_QUERY( "HotpointYOffset",   CHANGE_HOTPOINT_YOFFSET,       g_PropFloat.Query( Query, m_HotPointYOffset, -1024, 1024)         )

    if( Query.Scope( "Animation" ))
    {
        KEY_PROP_QUERY( "PlayBackMode", CHANGE_PLAYBACK_MODE,           g_PropEnum.Query( Query, m_PlayBackMode, m_PlayBackEnum )       )
        KEY_PROP_QUERY( "FPS",          CHANGE_FPS,                     g_PropInt.Query( Query, m_FPS, 1, 60 )                          )
    }

    return FALSE;
}

//---------------------------------------------------------------------------------------

void compile_sprite_atlas_key::entry_params::onUpdateFromSrc( const entry_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_SrcFileName,          CHANGE_FILENAME,            .Copy  )                
    KEY_PROP_UPDATE( m_Scale.m_X,            CHANGE_XSCALE,              =      )   
    KEY_PROP_UPDATE( m_Scale.m_Y,            CHANGE_YSCALE,              =      )    
    KEY_PROP_UPDATE( m_ScaleFilter,         CHANGE_SCALE_FILTER,         =      ) 
    KEY_PROP_UPDATE( m_bCompactSprite,      CHANGE_COMPACT_SPRITE,       =      )     
    KEY_PROP_UPDATE( m_HotPointXMode,       CHANGE_HOTPOINT_XMODE,       =      )     
    KEY_PROP_UPDATE( m_HotPointXOffset,     CHANGE_HOTPOINT_XOFFSET,     =      )
    KEY_PROP_UPDATE( m_HotPointYMode,       CHANGE_HOTPOINT_YMODE,       =      )
    KEY_PROP_UPDATE( m_HotPointYOffset,     CHANGE_HOTPOINT_YOFFSET,     =      )
    KEY_PROP_UPDATE( m_PlayBackMode,        CHANGE_PLAYBACK_MODE,        =      )
    KEY_PROP_UPDATE( m_FPS,                 CHANGE_FPS,                  =      )
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------

void compile_sprite_atlas_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM( "AtlasCompression",      CHANGE_MAIN_ATLAS_COMPRESSION,          g_PropFloat,    "This is the parameter that will be pass to the texture compiler for compression." )
    KEY_PROP_ENUM( "bDisableAlpha",         CHANGE_MAIN_DISABLE_ALPHA,              g_PropBool,     "Ignore what alpha the texture has" )
    KEY_PROP_ENUM( "FilterMig",             CHANGE_MAIN_FILTER_MIG,                 g_PropEnum,     "Scale Down filter mode for the atlas" )
    KEY_PROP_ENUM( "FilterMag",             CHANGE_MAIN_FILTER_MAG,                 g_PropEnum,     "Scale Up filter mode for the atlas" )
    KEY_PROP_ENUM( "bComputeMips",          CHANGE_MAIN_COMPUTE_MIPS,               g_PropBool,     "Will compute the mips for the atlas if requested" )
}

//---------------------------------------------------------------------------------------

xbool compile_sprite_atlas_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "AtlasCompression",      CHANGE_MAIN_ATLAS_COMPRESSION,          g_PropFloat.Query( Query, m_AtlastCompression, 0,1 )     )
    KEY_PROP_QUERY( "bDisableAlpha",         CHANGE_MAIN_DISABLE_ALPHA,              g_PropBool.Query( Query, m_bDisableAlpha)                )
    KEY_PROP_QUERY( "FilterMig",             CHANGE_MAIN_FILTER_MIG,                 g_PropEnum.Query( Query, m_FilterMig, m_FilterEnum )     )
    KEY_PROP_QUERY( "FilterMag",             CHANGE_MAIN_FILTER_MAG,                 g_PropEnum.Query( Query, m_FilterMag, m_FilterEnum )     )
    KEY_PROP_QUERY( "bComputeMips",          CHANGE_MAIN_COMPUTE_MIPS,               g_PropBool.Query( Query, m_bComputeMips )                )              

    return FALSE;
}


//---------------------------------------------------------------------------------------

void compile_sprite_atlas_key::main_params::onUpdateFromSrc ( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_AtlastCompression,       CHANGE_MAIN_ATLAS_COMPRESSION,   =    )
    KEY_PROP_UPDATE( m_bDisableAlpha,           CHANGE_MAIN_DISABLE_ALPHA,       =    )
    KEY_PROP_UPDATE( m_FilterMig,               CHANGE_MAIN_FILTER_MIG,          =    )
    KEY_PROP_UPDATE( m_FilterMag,               CHANGE_MAIN_FILTER_MAG,          =    )
    KEY_PROP_UPDATE( m_bComputeMips,            CHANGE_MAIN_COMPUTE_MIPS,        =    )              
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

f32 compile_sprite_atlas_key::ComputeHotPoint( hot_point HotpointMode, f32 Offset, s32 DimensionSize ) const
{
    switch( HotpointMode )
    {
        case HOTPOINT_ZERO_RELATIVE:    return Offset;
        case HOTPOINT_MIDDLE_RELATIVE:  return DimensionSize/2.0f + Offset;
        case HOTPOINT_ONE_RELATIVE:     return DimensionSize - Offset;
        default: ASSERT(0);
    }
    
    ASSERT(0);
    return 0;
}

//---------------------------------------------------------------------------------------
/*
void compile_sprite_atlas_key::getAssetDependencies( xarray<dependency>& List )
{
    for( s32 i=0; i<m_SpriteList.getCount(); i++ )
    {
        // If the user over wrote the All the Platforms then it is easy just get it from there
        if( x_FlagIsOn( m_SpriteList[i].m_Platform[0].m_OverWrittenMask, CHANGE_FILENAME ) )
        {
            dependency&         Dep = List.append();
            
            // Copy the dependency out
            Dep.m_FilePath = m_SpriteList[i].m_Platform[0].m_SrcFileName;
            
            // All the platforms is the same input (at least for right now)
            Dep.m_Plaforms = ~0;
            continue;
        }
        
        // If not we are going to have to add each dependency base on if it exist in the given platform
        s32 iBase = List.getCount();
        for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
        {
            const entry_params&  Desc = m_SpriteList[i].m_Platform[p];
            
            if( x_FlagIsOn( Desc.m_OverWrittenMask, CHANGE_FILENAME ) == FALSE )
                continue;
            
            xbool bFound = FALSE;
            for( s32 k=iBase; k<List.getCount(); k++ )
            {
                dependency&     Dep = List[k];
                
                if( Dep.m_FilePath == Desc.m_SrcFileName )
                {
                    bFound = TRUE;
                    Dep.m_Plaforms |= p;
                    break;
                }
            }
            
            if( bFound == FALSE )
            {
                dependency&         Dep = List.append();
                
                // Copy the dependency out
                Dep.m_FilePath = Desc.m_SrcFileName;
                
                // All the platforms is the same input (at least for right now)
                Dep.m_Plaforms = p;
            }
        }
    }
}
*/