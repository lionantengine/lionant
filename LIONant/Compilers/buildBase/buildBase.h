//
//  buildBase.h
//  buildBase
//
//  Created by Tomas Arce on 9/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef BUILD_BASE_H
#define BUILD_BASE_H

class build_base
{
public:

    enum
    {
        MAX_NUMBER_OF_KEY_FILES   =  (1<<10),
        MAX_NUMBER_OF_ASSET_FILES = MAX_NUMBER_OF_KEY_FILES * 10
    };
    
    enum buidtype:s32
    {
        BUILDTYPE_NULL,
        BUILDTYPE_RELEASE,
        BUILDTYPE_DEBUG,
        BUILDTYPE_DEPENDENCY,
        BUILDTYPE_NEW_GUID,
        BUILDTYPE_MAX
    };
    
public:

    xbool           Parse                           ( s32 argc, const char* argv[] );
    void            StartBuild                      ( void );

protected:
    
    struct file_info
    {
        xstring                 m_FileName;                         // File Name including the extension
        xstring                 m_FileRelativePath;                 // Relative path for this src file
        x_io::file_details      m_FileDetails;                      // Details for the file such size, creation time, etc.
        u32                     m_WhichPlatforms    = 0;            // This is the list of platforms that we need to compile for
    };
    
    struct target
    {
                                target( void )          { m_bValid=FALSE; }
        xbool                   m_bValid;
        xplatform               m_Target;
        xstring                 m_ReleasePath;
        xstring                 m_DebugPath;
    };

    struct asset_dependency : x_qt_ptr
    {
        file_info               m_FileInfo;
    };

    x_units( u32, iasset_hash );

    struct force_buids
    {
        xarray2<xguid>                              m_lForceBuild;                      // List of keys that if they build we also need to build
    };

    struct key_file2 : x_simple_job<1,x_base_job>
    {
        using parent = x_simple_job<1,x_base_job>;

        enum queue_state:u32
        {
            QUEUE_STATE_PENDING,
            QUEUE_STATE_IN_COMPILATION_QUEUE,
            QUEUE_STATE_DONE_WAS_BUILT,
            QUEUE_STATE_DONE_NO_BUILT,
            QUEUE_STATE_FORCE_REBUILD
        };

        enum final_state:u32
        {
            FINAL_STATE_PENDING,
            FINAL_STATE_NOT_NEED_COMPILATION,
            FINAL_STATE_WAS_BUILT,
        };

        volatile queue_state                        m_qtQueueState          = QUEUE_STATE_PENDING;
        final_state                                 m_FinalResult           = FINAL_STATE_PENDING;
        xguid                                       m_Guid                  = 0;
        u32                                         m_CompilePlatforms      = 0;        // What platforms this key file is going to compiled for
        file_info                                   m_KeyFile;                          // Information about the key file
        xstring                                     m_KeyFileNameNoGuid;                //
        s32                                         m_CompilerID            = -1;
        file_info                                   m_AssetDepenFile;                   // Information about the .depend file 
        xarray2<iasset_hash>                        m_lAssetDependencies;               // List list of regular source assets that we need to check to determine if we need to build
        xarray2<xguid>                              m_lReferencialsKeys;                // Resources/keys which we refer to
        xsafe_array<file_info, X_PLATFORM_COUNT>    m_lReleaseFile;                     // Information about our release file
        build_base*                                 m_pBuildBase            = NULL;
        s32                                         m_iBuildStage;

        virtual void onRun  ( void ) { m_pBuildBase->StartCompiling2( *this ); }
        virtual void vReset ( void ) 
        { 
            parent::vReset();

            do
            {
                queue_state Local = m_qtQueueState;

                if( Local == QUEUE_STATE_FORCE_REBUILD )
                {
                    if( m_FinalResult == FINAL_STATE_WAS_BUILT )
                    {
                        x_cas32( (u32*)&m_qtQueueState, Local, QUEUE_STATE_IN_COMPILATION_QUEUE );
                        continue;
                    }
                    else
                    {
                        if( x_cas32( (u32*)&m_qtQueueState, Local, QUEUE_STATE_IN_COMPILATION_QUEUE ) )
                        {
                            m_pBuildBase->m_WaitingToCompile.Inc();
                            if( m_iBuildStage!=-1) m_pBuildBase->m_StageTriggers[ m_iBuildStage ].m_Count.Inc();
                            g_Scheduler.StartJobChain( *this );
                            break;
                        }
                    }
                }

                ASSERT( Local == QUEUE_STATE_IN_COMPILATION_QUEUE );

                queue_state New;                
                if( m_FinalResult == FINAL_STATE_NOT_NEED_COMPILATION ) New = QUEUE_STATE_DONE_NO_BUILT;
                else if( m_FinalResult == FINAL_STATE_WAS_BUILT )       New = QUEUE_STATE_DONE_WAS_BUILT;
                else { ASSERT(0); }

                if( x_cas32( (u32*)&m_qtQueueState, Local, New ) )
                {
                    if( New == QUEUE_STATE_DONE_WAS_BUILT )
                        m_pBuildBase->DealWithForceBuild( *this );

                    break;
                }

            } while(1);
            
            //
            // Done with this resource
            // 
            if( m_iBuildStage >= 0 )
                m_pBuildBase->m_StageTriggers[ m_iBuildStage ].m_Count.Dec();

            m_pBuildBase->m_WaitingToCompile.Dec();
        }
    };

    struct compiler
    {
        x_io::file_details      m_FileDetails;              // File Details of the compiler
        xstring                 m_CompilerPath;             // Full path of the compiler ready to get executed
        xstring                 m_CompilerName;             // Just the basic file name
        xstring                 m_CompilerExt;              // Resource Extension that this compiler handles
        s32                     m_ExtLength         =  0;   // The length of the extension. (An optimization for string compares)
        s32                     m_UID               = -1;   // Unique ID of the compiler
        s32                     m_BuildStage        =  0;   // Builds in stage order. -1 means it can be compile at any stage.
    };

    struct stage
    {
        x_qt_fober_queue    m_Queue;
        x_qt_counter        m_Count;
    };

    struct config
    {
        s32                     m_nFreeCPUs             = 0;
        xbool                   m_bUseDebugCompilers    = FALSE;
    };

protected:
    
    void            GetDirectKeyFiles               ( void );
    void            CheckDirectoryIntegrity         ( void );
    void            WhichKeyFilesToBuild            ( void );
    void            LoadConfiguration               ( void );
    s32             IdentifyCompilerFromExt         ( const char* pExt, xbool bExtIsEnd  );
    void            CollectAllKeyFiles              ( void );
    void            StartCompiling                  ( void );
    void            SaveLinkFiles                   ( void );
    xbool           CompileKeyFile                  ( xstring& FullPath, xstring& Platforms, s32 CompiletID );
    void            HandleNewGuidBuild              ( void );
    xbool           exec                            ( const char* pRscName, const char* cmd, s32& nErrors, s32& nWarnings ) const;
    

    xguid           SetupKeyFileInfo2               ( const char* pFilePath, s32 CompilerID, xbool bPushToBuild  );
    void            CollectBuildInformation2        ( key_file2& KeyFile );
    void            CollectAssetDependencies2       ( key_file2& KeyFile );
    void            ReadRscDependencyFile2          ( key_file2& KeyFile );
    void            CheckWithReleaseFiles2          ( key_file2& KeyFile );
    void            StartCompiling2                 ( key_file2& KeyFile );
    void            CompileKeyFile2                 ( key_file2& KeyFile, xstring& Platforms );
    void            SubmitKeyfileForCompilation     ( key_file2& KeyFile );
    void            DealWithForceBuild              ( key_file2& KeyFile );
 
protected:

    config                                  m_Config;
    u32                                     m_PlatformMask          = 0;
    xarray<compiler>                        m_CompilerList;
    xsafe_array< target, X_PLATFORM_COUNT>  m_Target;
    xstring                                 m_ProjectPath;
    s32                                     m_ProjectPathLength     = 0;
    xstring                                 m_DependencyPath;
    xstring                                 m_ExternalDependencyKeyPath;
    xstring                                 m_IntermediateDataPath;
    xstring                                 m_ImportedFilesPath;
    xstring                                 m_RscBuildDependencyPath;
    xstring                                 m_LogPath;
    buidtype                                m_BuildType             = BUILDTYPE_NULL;
    xstring                                 m_CompilationPath;;
    xstring                                 m_CompilersPath;
    xstring                                 m_CompilerKeysPath;
    xstring                                 m_CompilerKeysDefaultPath;
    xstring                                 m_CmdLineArgName;
    s32                                     m_CmdLineUID            = -1;
    xtimer                                  m_Timer;
    mutable x_qt_counter                    m_TotalBuiltKeys;

    // Asset dependency
    x_qt_counter                            m_WaitingToCompile;
    x_qt_hash<force_buids, u64>             m_ForceBuilds;
    x_qt_hash<asset_dependency, u32>        m_AssetDependencyHashTable;
    x_qt_hash<key_file2, u64>               m_KeyFileHashTable;
    xsafe_array<stage,5>                    m_StageTriggers;
    x_qt_counter                            m_Stage;
};

#endif
