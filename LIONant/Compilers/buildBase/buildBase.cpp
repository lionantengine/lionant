//
//  buildBase.cpp
//  buildBase
//
//  Created by Tomas Arce on 9/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include<sstream>
#include<iostream>

#include "eng_Base.h"
#include "buildBase.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// UTILITY FUNCTIONS
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
static inline 
void CleanFilePath( xstring& String, const char* pFilepath )
{
    String.Copy( pFilepath );
    String.MakeLower();
    String.SearchAndReplace("\\","/");
}

//---------------------------------------------------------------------------
static inline 
xguid getGuidFromFilePath( const char* pFilePath )
{
    const char* pStr = x_strstr( pFilePath, "--" );
    if( pStr == NULL )
        x_throw( "ERROR: Unable to find a GUID in this file name path[%s]", pFilePath );

    xguid Guid;
    Guid.SetFromAlphaString( &pStr[2] );

    if( !(Guid&1) )
        x_throw("ERROR: The guid from this file does not appear to be a resource guid, missing 1 bit", pFilePath );

    return Guid;
}

//---------------------------------------------------------------------------
static inline 
s32 getResouceNameFromFilePath( xstring& FileName, const char* pFilePath )
{
    s32         iWhereTheNameStarted    = -1;
    s32         Length                  = x_strlen( pFilePath );
    s32         iTxtEntension           = 0;
    const char* pName                   = NULL;
    
    for( s32 i=Length; i >= 0; --i )
    {
        if( pFilePath[i] == '/' )
        {
            iWhereTheNameStarted = i + 1;
            iTxtEntension = Length - iWhereTheNameStarted - 4; 
            pName = &pFilePath[ iWhereTheNameStarted ];
            break;
        }
    }

    // Set the file name
    FileName.Copy( pName );
    FileName[iTxtEntension] = 0;

    return iWhereTheNameStarted;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void build_base::CheckDirectoryIntegrity( void )
{
    //
    // Make sure that the release directory is there fo
    //
    m_CompilationPath.Format( "%s/Compilation", (const char*)m_ProjectPath );
    if( FALSE == x_io::PathExists( m_CompilationPath ) )
    {
        x_throw( "ERROR: We don't have the compilation path setup. You need to create that directory to confirm you know what you are doing.");
    }
    
    //
    // Check the path for the compilers
    //
    m_CompilersPath.Format( "%s/Bin/Compilers", (const char*)m_ProjectPath );
    if( FALSE == x_io::PathExists( m_CompilersPath ) )
    {
        x_throw( "ERROR: We can not find where the compilers are.");
    }
    
    //
    // Check for the key file path
    //
    m_CompilerKeysPath.Format( "%s/GameData/CompilerKeys", (const char*)m_ProjectPath );
    if( FALSE == x_io::PathExists( m_CompilerKeysPath ) )
    {
        x_throw( "ERROR: We can not find where the compiler keys are.");
    }
    
    //
    // Create the dependency directory
    //
    m_DependencyPath.Format( "%s/Dependency", (const char*)m_CompilationPath );
    x_io::MakeDir( m_DependencyPath, 1 );

    //
    // Create the internal structure for the dependency directory
    //
    m_ExternalDependencyKeyPath.Format( "%s/ExternalCompilerKeys", (const char*)m_DependencyPath );
    x_io::MakeDir( m_ExternalDependencyKeyPath, 1 );

    m_IntermediateDataPath.Format( "%s/IntermediateData", (const char*)m_DependencyPath );
    x_io::MakeDir( m_IntermediateDataPath, 1 );

    m_ImportedFilesPath.Format( "%s/ImportedFiles", (const char*)m_DependencyPath );
    x_io::MakeDir( m_ImportedFilesPath, 1 );

    m_RscBuildDependencyPath.Format( "%s/RscBuildDepends", (const char*)m_DependencyPath );
    x_io::MakeDir( m_RscBuildDependencyPath, 1 );
    
    //
    // Create the log directory
    //
    m_LogPath.Format( "%s/Logs/Release", (const char*)m_DependencyPath );
    x_io::MakeDir( m_LogPath, 2 );
    m_LogPath.Format( "%s/Logs/Debug", (const char*)m_DependencyPath );
    x_io::MakeDir( m_LogPath, 2 );

    m_LogPath.Format( "%s/Logs/%s", (const char*)m_DependencyPath, ( m_BuildType == BUILDTYPE_RELEASE )?"Release":"Debug" );

    //
    // Make sure that all the targets we are going to build for have their directories setup
    //
    for( s32 i=0; i<m_Target.getCount(); i++ )
    {
        target& Target = m_Target[i];
        
        if( Target.m_bValid == FALSE )
            continue;
        
        //
        // First make sure that all the paths are there
        //
        
        const char* pPlatform = x_PlatformString( Target.m_Target );
        
        Target.m_DebugPath.Format    ( "%s/%s/Debug",     (const char*)m_CompilationPath, pPlatform );
        Target.m_ReleasePath.Format  ( "%s/%s/Release",   (const char*)m_CompilationPath, pPlatform );
        
        x_io::MakeDir( Target.m_DebugPath,      2 );
        x_io::MakeDir( Target.m_ReleasePath,    2 );
    }
}

//---------------------------------------------------------------------------

s32 build_base::IdentifyCompilerFromExt( const char* paExt, xbool bExtIsEnd )
{
    if( paExt == NULL )
        return -1;
    
    for( s32 i=0; i<m_CompilerList.getCount(); i++ )
    {
        const compiler&   Compiler = m_CompilerList[i];
        const char*       pExt     = paExt;
        
        // This is a trick to deal with the .txt extension
        if( bExtIsEnd ) pExt -= Compiler.m_ExtLength;

        for( s32 k=Compiler.m_ExtLength; k>=0; --k )
        {
            if( pExt[k] != Compiler.m_CompilerExt[k] )
            {
                break;
            }
            
            // It past all the tests
            if( k == 0 )
                return i;
        }
    }

    return -1;
}

//---------------------------------------------------------------------------

xguid build_base::SetupKeyFileInfo2( const char* pFilePath, s32 CompilerID, xbool bPushToBuild )
{
    //GetGuidFromFilePath( pFilePath );

    xstring FilePath;
    xstring Filename;
    
    CleanFilePath( FilePath, pFilePath );

    const s32   iNameStarted    = getResouceNameFromFilePath( Filename, FilePath );
    xguid       Guid            ( getGuidFromFilePath( Filename ) );

    //
    // Make sure that not other key exits with this GUID
    //
    if( m_KeyFileHashTable.isEntryInHash( Guid.m_Guid ) )
    {
        const auto&         KeyFileHashTable    = m_KeyFileHashTable;
        xstring             OldKeyFileName;

        KeyFileHashTable.cpGetEntry( Guid.m_Guid,[&](const key_file2& Entry )
        {
            OldKeyFileName      = Entry.m_KeyFile.m_FileName;
        });

        // we already have this entry
        if( x_stricmp( OldKeyFileName, Filename ) == 0 )
            return Guid;

        x_throw("ERROR: We have a duplicated GUID [%s], From Resource [%s] and [%s]",
            (const char*) Guid.GetAlphaString(),
            (const char*) OldKeyFileName,
            (const char*) FilePath );
    }

    //
    // Create an entry for this key
    //
    m_KeyFileHashTable.cpAddEntry( Guid.m_Guid,[&]( key_file2& KeyFile )
    {
        // Set the compiler ID
        // Make sure tha the guid type matches with the compiler type
        if ( m_CompilerList[ CompilerID ].m_UID != u8( Guid.m_Guid >> ( 64 - 7 ) ) )
        {
            x_throw( "ERROR: Key File [%s] Has a Guid [%s] which does not match the file extension",
                            (const char*)pFilePath,
                            (const char*)Guid.GetAlphaString() );
        }
        
        KeyFile.m_CompilerID    = CompilerID;
        KeyFile.m_Guid          = Guid;
        KeyFile.m_pBuildBase    = this;
        KeyFile.m_iBuildStage   = m_CompilerList[ CompilerID ].m_BuildStage;

        // TODO: In the future key files can exclude assets for certain build types
        //       Right now we include all of them.
        KeyFile.m_KeyFile.m_WhichPlatforms = ~0;
    
        // Here we will set which platform we care to compile for this asset.
        // this currently does not matter waiting for...
        KeyFile.m_KeyFile.m_WhichPlatforms &= m_PlatformMask;
    
        // Get additional details.
        x_io::FileDetails( pFilePath, KeyFile.m_KeyFile.m_FileDetails );
    
        // Copy the filename without the guid
        KeyFile.m_KeyFileNameNoGuid.Copy( Filename );
        s32 iLength = KeyFile.m_KeyFileNameNoGuid.FindI("--");
        ASSERT(iLength);
        KeyFile.m_KeyFileNameNoGuid[iLength] = 0;

        //
        // Set the key file info
        //
        KeyFile.m_KeyFile.m_FileName = Filename;

        //
        // Set the relative path for this source file
        //
        KeyFile.m_KeyFile.m_FileRelativePath.Copy( &FilePath[ m_ProjectPathLength + 1 ] );
        KeyFile.m_KeyFile.m_FileRelativePath[ iNameStarted - 2 - m_ProjectPathLength ] = 0;

        //
        // Push this newly created file into the build queue
        //
        if( bPushToBuild )
        {
            SubmitKeyfileForCompilation( KeyFile );
        }
    });

    return Guid;
}

//---------------------------------------------------------------------------

void build_base::SubmitKeyfileForCompilation( key_file2& KeyFile )
{
    
    //
    // Check which stage this file should be compile at
    //
    const s32               iStage      = m_CompilerList[ KeyFile.m_CompilerID ].m_BuildStage ;
    x_base_job::priority    Priority    = x_base_job::PRIORITY_NORMAL;

    if( m_CompilerList[ KeyFile.m_CompilerID ].m_BuildStage < 0 )
    {
        // For jobs that can be build anytime less keep then as low priority
        Priority    = x_base_job::PRIORITY_BELOW_NORMAL;
    }
    // We don't fill stage 0 since this stage can be process right away  
    else if( m_CompilerList[ KeyFile.m_CompilerID ].m_BuildStage <= m_Stage.get() )
    {
        // Try to finish jobs lacking behind stages as soon as possible
        Priority = x_base_job::PRIORITY_ABOVE_NORMAL;
    }
    else
    {
        do
        {
            key_file2::queue_state Local = KeyFile.m_qtQueueState;

            if ( Local == key_file2::QUEUE_STATE_PENDING )
            {
                if ( x_cas32( (u32*)&KeyFile.m_qtQueueState, Local, key_file2::QUEUE_STATE_IN_COMPILATION_QUEUE ) )
                {
                    m_StageTriggers[ iStage ].m_Count.Inc();
                    m_StageTriggers[iStage].m_Queue.push( &KeyFile );
                    return;
                }
            }
            else break;

        } while ( 1 );
    }
    
    do
    {
        key_file2::queue_state Local = KeyFile.m_qtQueueState;

        if ( Local == key_file2::QUEUE_STATE_PENDING )
        {
            if ( x_cas32( (u32*)&KeyFile.m_qtQueueState, Local, key_file2::QUEUE_STATE_IN_COMPILATION_QUEUE ) )
            {
                m_WaitingToCompile.Inc();
                if ( KeyFile.m_iBuildStage != -1 ) 
                    m_StageTriggers[ KeyFile.m_iBuildStage ].m_Count.Inc();
                KeyFile.setupPriority( Priority );
                g_Scheduler.StartJobChain( KeyFile );
                break;
            }
        }
        else break;

    } while ( 1 );
}

//---------------------------------------------------------------------------

void build_base::CollectAllKeyFiles( void )
{
    x_inline_light_jobs_block<8>    BlockJob;
    xhandle                         Handle;

    Handle = x_io::DirOpenPath( m_CompilerKeysPath );
    if( Handle.IsNull() )
    {
        x_throw( "ERROR: Unable to retive the key files require for compilation [%s]", (const char*)m_CompilerKeysPath );
    }
    
    x_io::file_info     DirFileInfo;
    s32                 CompilerID;
    while( x_io::DirGetNext( DirFileInfo, Handle ) )
    {
        // We dont care about no dirs
        if( DirFileInfo.m_bDir )
            continue;
        
        // Is the Extension pointing at anything here?
        
        // Filter files that dont have the right extension
        if( DirFileInfo.m_pExtension == NULL )
            continue;
        
        // Can we find a compiler for this key file?
        CompilerID = IdentifyCompilerFromExt( DirFileInfo.m_pExtension - 2, TRUE );
        if( CompilerID == -1 )
            continue;
        
        //
        // Create one entry the this key file
        //
        xstring FilePath;
        FilePath.Copy( DirFileInfo.m_Path );
        BlockJob.SubmitJob( [this, FilePath, CompilerID]()
        {
            SetupKeyFileInfo2( FilePath, CompilerID, TRUE );
        });
    }

    BlockJob.FinishJobs();
    
    x_io::DirClose( Handle );
}

//---------------------------------------------------------------------------

void build_base::ReadRscDependencyFile2( key_file2& KeyFile )
{
    // For sure we need to compile everything if our dependency file is missing
    // Because it means that nothing ever got compiled before since the dependency file always gets
    // Build not matter which platform.
    if( x_io::FileDetails( KeyFile.m_AssetDepenFile.m_FileName, KeyFile.m_AssetDepenFile.m_FileDetails ) == FALSE )
    {
        KeyFile.m_CompilePlatforms = KeyFile.m_KeyFile.m_WhichPlatforms;
        return;
    }
    
    //
    // Read the asset dependency file
    //
    xtextfile                       File;
    x_inline_light_jobs_block<8>    BlockJobs;

    File.OpenForReading( KeyFile.m_AssetDepenFile.m_FileName );
    if( File.ReadRecord() == FALSE )
        x_throw( "ERROR: Unable to located the record header in a dependency file [%s]", (const char*)KeyFile.m_AssetDepenFile.m_FileName );
    
    do
    {
        if( File.GetRecordName() == X_STR( "Assets") )
        {
            mutable_ref( rlAssetDependency, KeyFile.m_lAssetDependencies );
            x_qt_counter AssetCounter;

            //
            // Read all the source assets dependencies
            //
            KeyFile.m_lAssetDependencies.ChangeBehavior( xptr2_flags::FLAGS_QT_MUTABLE );
            rlAssetDependency->appendList( File.GetRecordCount() );
            for( iasset_hash& AssertHash : rlAssetDependency )
            {
                u32         WhichPlatforms  = 0;
                xstring     FileName        = X_STR("");
                xbool       bForceBuid      = FALSE;

                File.ReadLine();
                File.ReadFieldXString   ( "AssetPath:s",        FileName );
                File.ReadField          ( "Platform:h",         &WhichPlatforms );
                File.ReadField          ( "ForceKeyBuild:h",    &bForceBuid );

                // If this dependency is not relevant to the build then skip it
                if( !(WhichPlatforms & m_PlatformMask) )
                    continue;

                // Build the hash for the Asset base on the FileName
                xstring AssetCleanPath;
                CleanFilePath( AssetCleanPath, FileName );
                AssetCleanPath.Format( "%s/%s", (const char*)m_ProjectPath, (const char*)AssetCleanPath );
                AssertHash.m_Value = x_strHash( AssetCleanPath );

                // Make sure that this is the first time we see this asset
                if( m_AssetDependencyHashTable.isEntryInHash( AssertHash.m_Value ) )
                {
                    const auto& AD      = m_AssetDependencyHashTable;

                    AD.cpGetEntry( AssertHash.m_Value, [ &AssetCleanPath ]( const asset_dependency& Entry )
                    {
                        xbool bSame = (0 == x_strcmp( Entry.m_FileInfo.m_FileName, AssetCleanPath ));

                        // The hash matches but their paths don't... collesion in the hash?
                        ASSERT( bSame );
                    });

                    continue;
                }
                
                //
                // Do the rest in a job
                //
                BlockJobs.SubmitJob( [
                    this, 
                    AssertHash, 
                    WhichPlatforms, 
                    &rlAssetDependency, 
                    FileName, 
                    &AssetCounter, 
                    bForceBuid, 
                    AssetCleanPath, 
                    &KeyFile ]()
                {
                    if( bForceBuid )
                    {
                        xguid ReferenceGuid;
                        ReferenceGuid = getGuidFromFilePath( AssetCleanPath );
                        m_ForceBuilds.cpFindOrAddPopEntry( ReferenceGuid.m_Guid, [&]( force_buids& Entry )
                        {
                            mutable_ref( rlForceBuild, Entry.m_lForceBuild );
                            rlForceBuild->append() = KeyFile.m_Guid;

                            const auto& KeyFileHashTable = m_KeyFileHashTable;
                            if( KeyFileHashTable.isEntryInHash( KeyFile.m_Guid ) )
                            KeyFileHashTable.cpGetEntry( KeyFile.m_Guid.m_Guid, [&KeyFile]( const key_file2& KeyEntry )
                            {
                                if( KeyEntry.m_qtQueueState == key_file2::QUEUE_STATE_DONE_WAS_BUILT )
                                    KeyFile.m_CompilePlatforms = KeyEntry.m_CompilePlatforms; 
                            });
                        });
                    }
                    // Add the new entry and collect its info
                    else m_AssetDependencyHashTable.cpFindOrAddPopEntry( AssertHash.m_Value, [&]( asset_dependency& Entry )
                    {
                        // Check if it was there already
                        if( FALSE == Entry.m_FileInfo.m_FileName.IsEmpty() )
                        {
                            if( x_strcmp( Entry.m_FileInfo.m_FileName, AssetCleanPath ) == 0 )
                                return;
                            
                            // Hash Collision?
                            ASSERT(0);
                        }
                         
                        // Set the entry first
                        Entry.m_FileInfo.m_FileName         = AssetCleanPath;
                        Entry.m_FileInfo.m_WhichPlatforms   = WhichPlatforms;

                        // collect file details
                        if( x_io::FileDetails( Entry.m_FileInfo.m_FileName, Entry.m_FileInfo.m_FileDetails ) == FALSE )
                        {
                            x_throw(  "ERROR: One of the source assets is missing [%s] so I can not compile this resource [%s]",
                                         (const char*) &Entry.m_FileInfo.m_FileName[m_ProjectPath.GetLength()+1],
                                         (const char*) KeyFile.m_KeyFile.m_FileName );
                        }

                        // Add asset into the key 
                        rlAssetDependency[ AssetCounter.Inc() - 1 ] = AssertHash;
                    } );
                });
            }

            BlockJobs.FinishJobs();

            // Make sure we dont have blank assets in our list
            if( AssetCounter.get() < rlAssetDependency.getCount() )
                rlAssetDependency->DeleteWithCollapse( AssetCounter.get(), rlAssetDependency.getCount() - AssetCounter.get() );
            
            KeyFile.m_lAssetDependencies.ChangeBehavior( 0 );
        }
        else if( File.GetRecordName() == X_STR( "ExternalKeys") )
        {
            mutable_ref( rlReferencialsKeys, KeyFile.m_lReferencialsKeys );

            rlReferencialsKeys->Grow( File.GetRecordCount() );
            for( s32 i=0; i<File.GetRecordCount(); i++ )
            {
                File.ReadLine();
                
                // Read the relative path
                xstring FileName;
                File.ReadFieldXString( "KeyFilePath:s", FileName );

                // Make the full path plus txt extension
                xstring FullPath;
                CleanFilePath( FullPath, 
                                xstring::BuildFromFormat( "%s/%s.txt",
                                    (const char*) m_ProjectPath,
                                    (const char*) FileName ) );
                               
                // Can we find a compiler for this key file?
                s32 CompilerID = IdentifyCompilerFromExt( &FullPath[ FullPath.GetLength() -5 ], TRUE );
                if( CompilerID == -1 )
                {
                    x_throw( "ERROR: Unable to find the right compiler for an External Key Resource File\n[%s]", (const char*)FileName );
                }

                //
                // Filter out dependencies that are part of the compiler key paths
                //
                if ( x_stristr( FileName, &m_CompilerKeysPath[ m_ProjectPathLength + 1 ] ) )
                    continue;

                //
                // OK let setup the key file information now
                //
                rlReferencialsKeys->append() = SetupKeyFileInfo2( (const char*)FullPath, CompilerID, FALSE );
            }
        }
        else
        {
            x_throw( "ERROR: Unkown header [%s] found on dependency file [%s]",
                        (const char*)File.GetRecordName(),
                        (const char*)KeyFile.m_AssetDepenFile.m_FileName );
        }
        
    } while( File.ReadRecord() );
}

//---------------------------------------------------------------------------

void build_base::CollectAssetDependencies2( key_file2& KeyFile )
{
    xstring DependencyFile;
    
    DependencyFile.Format( "%s/%s.%s.depens",
                          (const char*)m_RscBuildDependencyPath,
                          (const char*)KeyFile.m_Guid.GetAlphaString(),
                          (const char*)m_CompilerList[KeyFile.m_CompilerID].m_CompilerExt );
    
    KeyFile.m_AssetDepenFile.m_FileName = DependencyFile;
    
    // Read its dependency file
    ReadRscDependencyFile2( KeyFile );
}

//---------------------------------------------------------------------------

void build_base::CheckWithReleaseFiles2( key_file2& KeyFile )
{
    //
    // For every key file check which target release files are there
    //
    for( target& Target : m_Target )
    {
        // Make sure that we are dealing with this platform
        if( Target.m_bValid == FALSE )
            continue;
        
        // If we already have mark this key file to be build for this platform then there
        // is nothing to do here.
        if( (KeyFile.m_CompilePlatforms >> Target.m_Target)&1 )
            continue;
        
        //
        // Start filling up some data
        //
        file_info& ReleaseFile = KeyFile.m_lReleaseFile[ Target.m_Target ];
        
        if( m_BuildType == BUILDTYPE_DEBUG )
        {
            if( KeyFile.m_KeyFileNameNoGuid.IsEmpty() )
            {
                ReleaseFile.m_FileName.Format( "%s/%s.%s",
                                              (const char*)Target.m_DebugPath,
                                              (const char*)KeyFile.m_Guid.GetAlphaString(),
                                              (const char*)m_CompilerList[KeyFile.m_CompilerID].m_CompilerExt );
            }
            else
            {
                ReleaseFile.m_FileName.Format( "%s/%s--%s.%s",
                                              (const char*)Target.m_DebugPath,
                                              (const char*)KeyFile.m_KeyFileNameNoGuid,
                                              (const char*)KeyFile.m_Guid.GetAlphaString(),
                                              (const char*)m_CompilerList[KeyFile.m_CompilerID].m_CompilerExt );
            }
        }
        else
        {
            ReleaseFile.m_FileName.Format( "%s/%s",
                                          (const char*)Target.m_ReleasePath,
                                          (const char*)KeyFile.m_Guid.GetAlphaString() );
            
        }
        
        // If we dont find it then we will have to build for this platform for sure
        if( x_io::FileDetails( ReleaseFile.m_FileName, ReleaseFile.m_FileDetails ) == FALSE )
        {
            KeyFile.m_CompilePlatforms |= 1 << Target.m_Target;
            continue;
        }
        
        //
        // Do time stamp checks
        //
        
        // Check with the src assets first
        const_ref( rlAssetDependencies, KeyFile.m_lAssetDependencies );
        const auto& AssetDependencyHashTable = m_AssetDependencyHashTable;

        for( const iasset_hash& AssetHash : rlAssetDependencies ) 
            AssetDependencyHashTable.cpGetEntry( AssetHash.m_Value, 
                [ &ReleaseFile, &KeyFile, &Target, &rlAssetDependencies ]( const asset_dependency& Entry )
        {
            if( ReleaseFile.m_FileDetails.m_MSTimeModified < Entry.m_FileInfo.m_FileDetails.m_MSTimeModified )
            {
                KeyFile.m_CompilePlatforms  |= 1 << Target.m_Target;
                rlAssetDependencies.endLoop();
            }
        } );
        
        if( (KeyFile.m_CompilePlatforms >> Target.m_Target)&1 )
            continue;
        
        // Check with the key file itself
        if( ReleaseFile.m_FileDetails.m_MSTimeModified < KeyFile.m_KeyFile.m_FileDetails.m_MSTimeModified )
        {
            KeyFile.m_CompilePlatforms  |= 1 << Target.m_Target;
            continue;
        }
        
        // Check with the compiler
        if( ReleaseFile.m_FileDetails.m_MSTimeModified < m_CompilerList[KeyFile.m_CompilerID].m_FileDetails.m_MSTimeModified )
        {
            KeyFile.m_CompilePlatforms  |= 1 << Target.m_Target;
            continue;
        }
    }
}

//---------------------------------------------------------------------------

void build_base::CollectBuildInformation2( key_file2& KeyFile )
{
    //
    // Collect the asset dependencies for all the key files
    //
    CollectAssetDependencies2( KeyFile );
    
    //
    // Collect all the release files
    //
    CheckWithReleaseFiles2( KeyFile );
}

//---------------------------------------------------------------------------

void build_base::WhichKeyFilesToBuild( void )
{
    //
    // Collect all the key files
    //
    CollectAllKeyFiles();
}

//---------------------------------------------------------------------------

void build_base::LoadConfiguration( void )
{
    xstring     ConfigPath;
    xtextfile   File;
    
    ConfigPath.Format( "%s/Bin/Configurations/RscBuildConfig.txt", (const char*)m_ProjectPath );
    
    File.OpenForReading( ConfigPath );
    
    if( File.ReadRecord() == FALSE )
        x_throw( "ERROR: Loading the Rsc Build Config file. I found that was not header" );
    
    do
    {
        if( x_strcmp( File.GetRecordName(), "System" ) == 0 )
        {
            File.ReadLine();
            File.ReadField( "CPUFree:d", &m_Config.m_nFreeCPUs );
            File.ReadField( "UseDebugCompilers:d", &m_Config.m_bUseDebugCompilers );
            
            if( m_Config.m_nFreeCPUs < 0 || m_Config.m_nFreeCPUs > 20 ) 
                x_throw("ERROR: You enter [%d] for numbr of free cpus it must be from [0-nCores]", m_Config.m_nFreeCPUs );
        }
        else if( x_strcmp( File.GetRecordName(), "Compilers" ) == 0 )
        {
            //
            // Ok lets get ready to start reading compilers
            //
            m_CompilerList.appendList( File.GetRecordCount() );
            for( compiler& Compiler : m_CompilerList )
            {
                File.ReadLine();
                File.ReadFieldXString ( "CompilerName:s", Compiler.m_CompilerName );
                File.ReadFieldXString ( "CompilerExtension:s", Compiler.m_CompilerExt );
                File.ReadField        ( "ResourceTypeUID:d", &Compiler.m_UID );
                File.ReadField        ( "PipelineStage:d", &Compiler.m_BuildStage );
        
                // Compute the length of the extension
                Compiler.m_ExtLength = Compiler.m_CompilerExt.GetLength() - 1;
        
                // Set the path for this compiler
        #ifdef TARGET_PC
                const char* pExt = ".exe";
        #else
                const char* pExt = "";
        #endif
                Compiler.m_CompilerPath.Format( "%s/%s%s", (const char*)m_CompilersPath, (const char*)Compiler.m_CompilerName, pExt );
        
                // Check if in fact exists
                if( x_io::FileDetails( Compiler.m_CompilerPath, Compiler.m_FileDetails ) == FALSE )
                {
                    x_throw(  "ERROR: The compiler [%s] is missing from the compiler directories", (const char*) Compiler.m_CompilerPath );
                }
            }
        }
        else
        {
            x_printf("WARNING: Found a header [%s] in the configuration file that I don't how to read", (const char*)File.GetRecordName() );
            File.ReadNextRecord();
        }

    } while( File.ReadRecord() );

    // Done with config file
    File.Close();
}

//---------------------------------------------------------------------------

void build_base::SaveLinkFiles( void )
{
    // This is only saved in case of debug
    if( m_BuildType != BUILDTYPE_DEBUG )
        return;
    
    // Nothing actually built
    if( m_TotalBuiltKeys.get() == 0 )
        return;

    //
    // Save file link
    //
    xtextfile File;
    xstring   LinkFilePath;
        
    for( target& Target : m_Target )
    {
        // Make sure that we are dealing with this plaform
        if( Target.m_bValid == FALSE )
            continue;

        LinkFilePath.Format( "%s/GuidFileName.Link", (const char*)Target.m_DebugPath );
        File.OpenForWriting( LinkFilePath, xtextfile::FLAGS_BINARY );
        
        File.WriteRecord( "GuidFileName", m_KeyFileHashTable.getCount() );

        const auto& KeyFileHashTable = m_KeyFileHashTable; 
        KeyFileHashTable.cpIterateGetEntry( [&File]( const key_file2& Entry )
        {
            File.WriteField ( "Guid:g", Entry.m_Guid );
            File.WriteField ( "Name:s", (const char*)Entry.m_KeyFileNameNoGuid );
            File.WriteLine  ();
        });

        File.Close();
    }
}

//---------------------------------------------------------------------------

void ProcessString( const char* Ptr, xfile& LogFile, xsafe_array<s32, 3>& Messages )
{
    //
    // Search for key words
    //
    s32 i;
    s32 l = 0;
    s32 m = 0;
    for ( i = 0; Ptr[ i ] && i <511; i++ )
    {
        static const char* KeyWords[ ] =
        {
            "ERROR:",
            "WARNING:",
            "INFO"
        };

        if ( l == 0 )
        {
            for ( m = 0; m<3; m++ )
            if ( Ptr[ i ] == KeyWords[ m ][ 0 ] )
                break;

            if ( m < 3 ) l++;
        }
        else if ( Ptr[ i ] == KeyWords[ m ][ l ] )
        {
            l++;
            if ( KeyWords[ m ][ l ] == 0 )
            {
                Messages[ m ]++;
                l = 0;
            }
        }
        else
        {
            l = 0;
        }
    }

    //
    // Let the user read the info
    //
    x_printf( "%s", Ptr );

    // Write output to the log
    LogFile.Write( Ptr, x_strlen( Ptr ) );
}

x_qt_counter s_ActiveProcesses;

//---------------------------------------------------------------------------
#ifdef TARGET_PC
xbool build_base::exec( const char* pRscName, const char* cmd, s32& nErrors, s32& nWarnings ) const
{
    xsafe_array<s32, 3> Messages{ 0 };

    m_TotalBuiltKeys.Inc();
    s_ActiveProcesses.Inc();
    x_printf( "PROCESSES: %d\n", 1 + s_ActiveProcesses.get() );

    nErrors = 0;
    nWarnings = 0;

    xfile File;

    xstring LogFilePath;

    LogFilePath.Format( "%s/%s.rscbuilder.log",
        (const char*)m_LogPath,
        pRscName );

    if ( File.Open( LogFilePath, "wt" ) == FALSE )
        x_throw(  "ERROR: Unable to save the log file [%s]",
        (const char*)LogFilePath );

    // Write the command line to the log file
    File.Write( xstring::BuildFromFormat( "Logfile for [%s]\n%s\n", pRscName, cmd ) );

    //
    // First create the pipes
    //
    HANDLE                  hReadPipe, hWritePipe;
    SECURITY_ATTRIBUTES     sa;

    sa.nLength = sizeof( sa );
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = NULL;

    if ( !CreatePipe( &hReadPipe, &hWritePipe, &sa, 0 ) )
    {
        return -1;
    }

    //
    // Now get ready to lanch the process
    //
    STARTUPINFO         siStartupInfo;
    PROCESS_INFORMATION piProcessInfo;

    memset(&siStartupInfo, 0, sizeof(siStartupInfo));
    memset(&piProcessInfo, 0, sizeof(piProcessInfo));

    siStartupInfo.cb            = sizeof(siStartupInfo);
    siStartupInfo.dwFlags = 0;// STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
    siStartupInfo.hStdInput     = INVALID_HANDLE_VALUE;
    siStartupInfo.hStdOutput    = hWritePipe;
    siStartupInfo.hStdError     = hWritePipe;
    siStartupInfo.wShowWindow   = SW_HIDE;

    DWORD dwExitCode;

    if(!CreateProcess(
        NULL,                           // Application name
        (LPSTR)cmd,				    	    // Application arguments
        0,
        0,
        FALSE,
        CREATE_DEFAULT_ERROR_MODE,
        0,
        0,                              // Working directory
        &siStartupInfo,
        &piProcessInfo))
    {
        s_ActiveProcesses.Dec();
        return FALSE;
    }

    //
    // Collect information as it goes
    //
    xbool bAbort = FALSE;
    DWORD dwRead = 0;
    char  buffer[ 512 ];
    while ( bAbort == FALSE )
    {
        dwRead = 0;
        if ( !PeekNamedPipe( hReadPipe, NULL, 0, NULL, &dwRead, NULL ) )
            break;

        if ( dwRead )
        {                 // yes we do, so read it and print out to the edit ctrl
            if ( !ReadFile( hReadPipe, &buffer, sizeof(buffer)-1, &dwRead, NULL ) )
                break;

            buffer[ dwRead ] = 0;
            ProcessString( buffer, File, Messages );
        }
        else
        {
            // no we don't have anything in the buffer
            // maybe the program exited
            if ( WaitForSingleObject( piProcessInfo.hProcess, 0 ) == WAIT_OBJECT_0 )
                break;        // so we should exit either

            
            // In the future the user may be able to cancel the compilation
            //Sleep( 2 );
            //if ( g_RescDescMGR.IsStopBuild( ) )
            //    bAbort = TRUE;
        }

        // continue otherwise
    }

    //
    // Clean up
    //

   // WaitForSingleObject(piProcessInfo.hProcess, INFINITE);
    
    // Close the process
    CloseHandle( piProcessInfo.hProcess );
    CloseHandle( piProcessInfo.hThread );

    // Close the pipes
    CloseHandle( hWritePipe );
    CloseHandle( hReadPipe );

    GetExitCodeProcess( piProcessInfo.hProcess, &dwExitCode );

    //
    // Done with this compiles
    //
    nErrors     = Messages[0];
    nWarnings   = Messages[1];

    s_ActiveProcesses.Dec();

    return TRUE;
}

#else

xbool build_base::exec( const char* pRscName, const char* cmd, s32& nErrors, s32& nWarnings ) const
{
    xsafe_array<s32,3> Messages{0};
    
    m_TotalBuiltKeys.Inc();
    s_ActiveProcesses.Inc();

    nErrors     = 0;
    nWarnings   = 0;

    char Ptr[512];
    
    xfile File;
    
    xstring LogFilePath;
    
    LogFilePath.Format( "%s/%s.rscbuilder.log",
                       (const char*)m_LogPath,
                       pRscName );
    
    if( File.Open( LogFilePath, "wt") == FALSE )
        x_throw( "ERROR: Unable to save the log file [%s]",
                     (const char*) LogFilePath );
    
    // Write the command line to the log file
    File.Write( xstring::BuildFromFormat( "Logfile for [%s]\n%s\n", pRscName, cmd) );
    
    //
    // Run compiler and pipe its output
    //
    FILE* pPipe = popen(cmd, "r");
    if( NULL == pPipe )
    {
        s_ActiveProcesses.Dec();
        return FALSE;
    }
    
    
    //
    // Start reading from the pipe
    //
    while( !feof(pPipe) )
    {
        // Read the next line of information
        if( NULL == fgets( Ptr, 512, pPipe ) )
            break;

        ProcessString( Ptr, File, Messages );
    }
    
    //
    // Done with this compiles
    //
    pclose( pPipe );
    
    nErrors     = Messages[0];
    nWarnings   = Messages[1];
    
    s_ActiveProcesses.Dec();
    return TRUE;
}
#endif



//---------------------------------------------------------------------------

xbool build_base::CompileKeyFile( xstring& FullPath, xstring& Platforms, s32 CompiletID )
{
    xstring CmdLine;
    
    const compiler& Compiler = m_CompilerList[ CompiletID ];
    
    const char* pBuildType;
    switch(m_BuildType)
    {
        case BUILDTYPE_RELEASE:     pBuildType = "RELEASE"; break;
        case BUILDTYPE_DEBUG:       pBuildType = "DEBUG"; break;
        case BUILDTYPE_DEPENDENCY:  pBuildType = "DEPENDENCY"; break;
        default: pBuildType = "?"; ASSERT( 0 );
    }
    
    CmdLine.Format( "\"%s\" -BUILDTYPE %s -PROJECT \"%s\" -TARGET%s -INPUT \"%s\"",
                   (const char*)Compiler.m_CompilerPath,
                   pBuildType,
                   (const char*)m_ProjectPath,
                   (const char*)Platforms,
                   (const char*)FullPath );
    
    // Find out where it was the last directory
    s32 ilast=0;
    for( s32 i=0; FullPath[i]; i++ )
    {
        if( FullPath[i] == '/' ) ilast = i+1;
    }
    
    s32 nErrors;
    s32 nWarnings;
    if( FALSE == exec( &FullPath[ilast], ( const char*) CmdLine, nErrors, nWarnings ) )
    {
        x_throw( "ERROR: Unale to run a compiler with this command line:\n%s\n", (const char*)CmdLine );
    }
    
    // If we have errors lets ignore it and keep going with the next file
    if( nErrors )
        return FALSE;
    
    return TRUE;
}

//---------------------------------------------------------------------------

void build_base::CompileKeyFile2( key_file2& KeyFile, xstring& Platforms )
{
    //
    // Lets compile first this file
    //
    xstring KeyFilePath;
    KeyFilePath.Format( "%s/%s",
                       (const char*)KeyFile.m_KeyFile.m_FileRelativePath,
                       (const char*)KeyFile.m_KeyFile.m_FileName );
    
    if( FALSE == CompileKeyFile( KeyFilePath, Platforms, KeyFile.m_CompilerID ) )
        return;
    
    //
    // Make sure that we have upto date information about its dependency file
    //
    {
        mutable_ref( rlAssetDependencies, KeyFile.m_lAssetDependencies );
        mutable_ref( rlReferencialsKeys,  KeyFile.m_lReferencialsKeys  );

        // Make sure we start this fresh
        rlAssetDependencies->DeleteAllEntries();
        rlReferencialsKeys->DeleteAllEntries();
    }
    
    // Make sure that we have upto date information about its dependency file
    ReadRscDependencyFile2( KeyFile );
}
 
//---------------------------------------------------------------------------

void build_base::DealWithForceBuild( key_file2& KeyFile )
{
    m_ForceBuilds.cpGetOrFailEntry( KeyFile.m_Guid.m_Guid, [this, &KeyFile]( force_buids& Entry )
    {
        const_ref( rlForceBuild, Entry.m_lForceBuild );
        for( const xguid& ForceBuild : rlForceBuild )
        {
            m_KeyFileHashTable.cpGetOrFailEntry( ForceBuild.m_Guid, [this, &KeyFile]( key_file2& EntryKey )
            {
                EntryKey.m_CompilePlatforms |= KeyFile.m_CompilePlatforms;

                do
                {
                    key_file2::queue_state Local = EntryKey.m_qtQueueState;
                    
                    // If it is not in the queue processing
                    if( Local == key_file2::QUEUE_STATE_DONE_WAS_BUILT ||
                        Local == key_file2::QUEUE_STATE_DONE_NO_BUILT  ||
                        Local == key_file2::QUEUE_STATE_PENDING )
                    {
                        if( Local != key_file2::QUEUE_STATE_DONE_WAS_BUILT )
                        {
                            if( x_cas32( (u32*)&EntryKey.m_qtQueueState, Local, key_file2::QUEUE_STATE_IN_COMPILATION_QUEUE ) )
                            {
                                m_WaitingToCompile.Inc();
                                if( KeyFile.m_iBuildStage!=-1) m_StageTriggers[ KeyFile.m_iBuildStage ].m_Count.Inc();
                                g_Scheduler.StartJobChain( EntryKey );
                                break;
                            }
                            continue;
                        }
                        break;
                    }

                    // If still in the queue processing processing...
                    if( Local == key_file2::QUEUE_STATE_FORCE_REBUILD )
                        break;

                    if( x_cas32( (u32*)&EntryKey.m_qtQueueState, Local, key_file2::QUEUE_STATE_FORCE_REBUILD ) )
                        break;
                
                } while(1);
            });
        }
    });
}

//---------------------------------------------------------------------------

void build_base::StartCompiling2( key_file2& KeyFile )
{
    //
    // Check compilation flags
    //
    if( KeyFile.m_FinalResult == key_file2::FINAL_STATE_PENDING )
        CollectBuildInformation2( KeyFile );

    //
    // We have nothing to compile here
    //
    if( KeyFile.m_CompilePlatforms )
    {
        //
        // Build string of platforms for the compiler
        //
        xstring Platforms;
        Platforms.Format( "" );
        for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
        {
            // Skip invalid platforms
            if( m_Target[i].m_bValid == FALSE )
                continue;
            
            // Do we have marked this platform to be compiled?
            if( ((KeyFile.m_CompilePlatforms >> i ) &1) == 0 )
                continue;
            
            // OK lets add another platform for our key file
            Platforms.AppendFormat( " %s", x_PlatformString( m_Target[i].m_Target ) );
        }
        
        //
        // Ok we should be ready to compile this key file
        //
        CompileKeyFile2( KeyFile, Platforms );

        //
        // Update its state
        //
        KeyFile.m_FinalResult = key_file2::FINAL_STATE_WAS_BUILT;
    }
    else
    {
        KeyFile.m_FinalResult = key_file2::FINAL_STATE_NOT_NEED_COMPILATION;
    }

    //
    // Handle referential keys
    //
    {
        const_ref( rlReferencialsKeys, KeyFile.m_lReferencialsKeys );

        // If nothing to compile check its referential dependencies need to be build
        for( const xguid& Guid : rlReferencialsKeys )
        {
            m_KeyFileHashTable.cpGetEntry( Guid.m_Guid, [this]( key_file2& EntryKey )
            {
                if( EntryKey.m_qtQueueState == key_file2::QUEUE_STATE_PENDING )
                    SubmitKeyfileForCompilation( EntryKey );
            });
        }
    }
}

//---------------------------------------------------------------------------

void build_base::StartCompiling( void )
{
    //
    // First deal with the stages
    //
    m_Stage.Zero();
    do
    {
        for( s32 i=0; i<m_StageTriggers.getCount(); i++ )
        {
            // start working
            do
            {
                // release all the jobs for this stage
                for( s32 j=0; j<=i; j++ )
                    while ( key_file2* pJob = (key_file2*)m_StageTriggers[j].m_Queue.pop() )
                    {
                        m_WaitingToCompile.Inc();
                        ASSERT( pJob->m_qtQueueState == key_file2::QUEUE_STATE_IN_COMPILATION_QUEUE );
                        g_Scheduler.StartJobChain( *pJob );
                    }

                g_Scheduler.ProcessWhileWait();

            } while ( m_StageTriggers[i].m_Count.get() > 0 );

            // Done processing this stage
            m_Stage.Inc();
        }

    } while ( m_WaitingToCompile.get() > 0 );
}

//---------------------------------------------------------------------------

void build_base::HandleNewGuidBuild( void )
{
    //
    // Does the user want us to ask him?
    //
    if( m_CmdLineUID ==-1 && m_CmdLineArgName.IsEmpty() )
    {
        //
        // Keep asking him until he gets it
        //
        xbool bValid = FALSE;
        
        do
        {
            x_printf( "Type the numeric option you want\n");
            x_printf("   0 )  Exits the program  \n" );
            for( const compiler& Compiler : m_CompilerList )
            {
                x_printf("   %3d )  %s  (%s) \n",
                         Compiler.m_UID,
                         (const char*)Compiler.m_CompilerExt,
                         (const char*)Compiler.m_CompilerName);
            }
            
            // Get Number from user
            std::cin >> m_CmdLineUID;
            
            // If the user requested us to quit then do so
            if( m_CmdLineUID == 0 )
                return;
            
            for( const compiler& Compiler : m_CompilerList  )
            {
                if( m_CmdLineUID == Compiler.m_UID )
                {
                    bValid = TRUE;
                    break;
                }
            }
            
        } while( bValid == FALSE );
    }
    else if( m_CmdLineUID ==-1 )
    {
        ASSERT( m_CmdLineArgName.IsEmpty() == FALSE );
        
        //
        // Seach for the right UID
        //
        xbool bValid = FALSE;
        
        for( const compiler& Compiler : m_CompilerList  )
        {
            if( x_stristr( m_CmdLineArgName, Compiler.m_CompilerExt ) == 0 )
            {
                m_CmdLineUID = Compiler.m_UID;
                bValid = TRUE;
                break;
            }
        }
        
        if( bValid == FALSE )
        {
            x_throw( "ERROR: You have enter an invalid type name [%s]. Not compiler extension have such name.",
                         (const char*)m_CmdLineArgName );
        }
    }
    
    //
    // Verify that the UID is actually valid
    //
    xbool bValid = FALSE;
    for( const compiler& Compiler : m_CompilerList  )
    {
        if( m_CmdLineUID == Compiler.m_UID )
        {
            bValid = TRUE;
            
            // Finally create the UID
            eng_resource_guid X;
            
            X.Create( m_CmdLineUID );

            // Give it to the user
            x_printf( "New Guid >>> %s -> %s: %s\n",
                     (const char*)Compiler.m_CompilerName,
                     (const char*)Compiler.m_CompilerExt,
                     (const char*)X.getAlphaString() );
            break;
        }
    }
    
    if( bValid == FALSE )
    {
        x_throw( "ERROR: You have enter an invalid type UID [%d]. Not compiler extension have such UID.", m_CmdLineUID );
    }
}

//---------------------------------------------------------------------------

void build_base::StartBuild( void )
{
    //
    // First lets make sure that the environment is properly setup
    //
    CheckDirectoryIntegrity();

    //
    // Load the build configuration file
    //
    LoadConfiguration();

    //
    // Multi-core up and running
    //
    // -1 is the main thread as will be a worker
    g_Scheduler.Init( x_Max( 1, x_GetCPUCoreCount() - m_Config.m_nFreeCPUs -1 ) );

    m_ForceBuilds.Initialize( MAX_NUMBER_OF_KEY_FILES );
    m_KeyFileHashTable.Initialize( MAX_NUMBER_OF_KEY_FILES );
    m_AssetDependencyHashTable.Initialize( MAX_NUMBER_OF_ASSET_FILES );

    //
    // Let the user know what is going on
    //
    x_printf( "Version[%s - %s]\n", __TIME__, __DATE__ );
    x_printf( "INFO:=================== START BUILDING PROCESS ===================\n" );
    m_Timer.Start();
    
    //
    // Handle main arguments
    //
    if( m_BuildType == BUILDTYPE_NEW_GUID )
    {
        HandleNewGuidBuild();
    }
    else
    {
        //
        // lets find out which files we need to build
        //
        WhichKeyFilesToBuild();
        
        //
        // OK we are ready to start compiling
        //
        StartCompiling();
        
        //
        // Save the link file
        //
        SaveLinkFiles();
    }
    
    //
    // Done
    //
    x_printf( "INFO:==================== END BUILDING PROCESS ====================\n" );

    f64 SecondsLarge    = m_Timer.TripSec();
    u64 TotalSeconds    = (u64)SecondsLarge;
    f32 Seconds         = f32((TotalSeconds%60) + SecondsLarge - TotalSeconds);
    s32 Minutes         = s32( (TotalSeconds/60)%60 );
    s32 Hours           = s32( (TotalSeconds/(60*60))%60 );
     
    x_printf( "INFO: Total Time Taken: %d::%d::%f \n", Hours, Minutes, Seconds );
}

//---------------------------------------------------------------------------

xbool build_base::Parse( s32 argc, const char* argv[] )
{
    xcmdline CmdLine;
    
    //
    // Create the switches and their rules
    //
    s32 iNewGuid   = CmdLine.AddCmdSwitch( "NEWGUID",    0,  0, 0, 1, TRUE, xcmdline::TYPE_STRING, TRUE );
    CmdLine.AddCmdSwitch( "PROJECT", 1,  1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, iNewGuid );
    CmdLine.AddCmdSwitch( "NAME",    1,  1, 0, 1, FALSE, xcmdline::TYPE_STRING, FALSE, iNewGuid );
    CmdLine.AddCmdSwitch( "UID",     1,  1, 0, 1, FALSE, xcmdline::TYPE_INT,    FALSE, iNewGuid );

    s32 iBuildType = CmdLine.AddCmdSwitch( "BUILDTYPE",    1,  1, 0, 1, TRUE, xcmdline::TYPE_STRING, TRUE );
    CmdLine.AddCmdSwitch( "PROJECT",    1,  1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, iBuildType );
    CmdLine.AddCmdSwitch( "TARGET",     1, -1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, iBuildType );
    
    //
    // Start parsing the arguments
    //
    CmdLine.Parse( argc, argv );
    
    //
    // Deal with the help message
    //
    if( CmdLine.DoesUserNeedsHelp() )
    {
        x_printf("---------------------------------------------------------------\n"
                 "LIONant - Build system.                                        \n"
                 "RscBuild (Resource Build) [ %s - %s ]                          \n"
                 "    Given an specific directory structure this program will go \n"
                 "    and build any resources from key files.                    \n"
                 "    This program can also generate new guids for the user.     \n"
                 "                                                               \n"
                 "Switches:                                                      \n"
                 "   -NEWGUID                                                    \n"
                 "         -PROJECT        <NetworkFriendlyPathOfProject>        \n"
                 "         -NAME           <rsc extension>                       \n"
                 "         -UID            <unique id of the resource type>      \n"
                 "   -BUILDTYPE            <RELEASE | DEBUG | DEPENDENCY>        \n"
                 "         -PROJECT        <NetworkFriendlyPathOfProject>        \n"
                 "         -TARGET         <PC OSX IOS ANDROID PS3 TOOLS>        \n"
                 "---------------------------------------------------------------\n", __TIME__, __DATE__ );
        
        return FALSE;
    }
    
    //
    // Go through the parameters of the command line
    //
    for( s32 i=0; i<CmdLine.GetCommandCount(); i++ )
    {
        xstring CmdName = CmdLine.GetCmdName(i);
        if( x_stricmp( CmdName, "TARGET" ) == 0 )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            s32 Count  = CmdLine.GetCmdArgumentCount(i);
            
            for( s32 i=0; i<Count; i++ )
            {
                const char* pString = CmdLine.GetArgument( Offset + i );
                xbool       bFound  = FALSE;
                
                // Go through all the platforms and pick up the right one
                for( xplatform p = xplatform(X_PLATFORM_NULL+1); p < X_PLATFORM_COUNT; p = xplatform(p + 1) )
                {
                    if( x_stricmp( pString, x_PlatformString( p ) ) == 0 )
                    {
                        target& NewTarget = m_Target[p];
                        
                        if( NewTarget.m_bValid )
                        {
                            x_throw( "ERROR: The same platform [%s] was enter multiple times", pString);
                        }
                        
                        NewTarget.m_Target      = p;
                        NewTarget.m_bValid      = TRUE;
                        
                        // Lets also set up the mask
                        m_PlatformMask |= (1<<p);
                        
                        // Mark it as we found it
                        bFound = TRUE;
                        break;
                    }
                }
                
                if(  bFound == FALSE )
                {
                    x_throw( "ERROR: Platform not supported (%s)\n", pString );
                }
            }
        }
        else if( x_stricmp( CmdName, "PROJECT" ) == 0 )
        {
            m_ProjectPath       = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
            m_ProjectPathLength = m_ProjectPath.GetLength();
        }
        else if( x_stricmp( CmdName, "BUILDTYPE" ) == 0 )
        {
            xstring BuildType;
            
            BuildType = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
            
            BuildType.MakeUpper();
            
            if( BuildType == X_STR("RELEASE" ) )
            {
                m_BuildType = BUILDTYPE_RELEASE;
            }
            else if( BuildType == X_STR("DEBUG" ) )
            {
                m_BuildType = BUILDTYPE_DEBUG;
            }
            else if( BuildType == X_STR("DEPENDENCY" ) )
            {
                m_BuildType = BUILDTYPE_DEPENDENCY;
            }
            else
            {
                x_throw( "ERROR: Build Type not supported (%s).", (const char*)BuildType );
            }
        }
        else if( x_stricmp( CmdName, "NEWGUID" ) == 0 )
        {
            m_BuildType = BUILDTYPE_NEW_GUID;
        }
        else if( x_stricmp( CmdName, "NAME" ) == 0 )
        {
            m_CmdLineArgName = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
        }
        else if( x_stricmp( CmdName, "UID" ) == 0 )
        {
            m_CmdLineUID = x_atoi32( CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) ) );
            if( m_CmdLineUID <= 0 || m_CmdLineUID >= 127 )
            {
                x_throw( "ERROR: You have enter a UID with an invalid range [%d] It must be from 1 to 127.", m_CmdLineUID );
            }
        }
        else
        {
            // We must have forgotten a switch because we should not be able to reach this point
            ASSERT( 0 );
        }
    }
    
    return TRUE;
}