//
//  TextureCompilerBase.h
//  TextureCompiler
//
//  Created by Tomas Arce on 9/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef TEXTURE_COMPILER_BASE_H
#define TEXTURE_COMPILER_BASE_H

#include "eng_base.h"
#include "TextureCompilerKey.h"

class texture_compiler : public compiler_base
{
public:
                                texture_compiler    ( void ) : m_Key( *this ) {}
    
protected:
    
    void                        HandleIOS           ( void ) const;
    void                        HandleOSXAndPC      ( const xplatform Platform ) const;
    void                        FillInfoStructure   ( const platform& Target, eng_texture_rsc::info& Info ) const;
    
protected:
    
    virtual void                        onCompile           ( void );
    virtual compiler_key_object&        getKeyObject        ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject        ( void ) const { return m_Key;             }
    
    void                                PrintFormatInfo     ( const xbitmap& Bitmap, xplatform Platform ) const;
    
protected:
    
    texture_compiler_key                        m_Key;
    xarray<xbitmap>                             m_lSrcBitmap;
    xarray2<compiler_key_object::dependency>    m_lSrcFileNames;
};


#endif
