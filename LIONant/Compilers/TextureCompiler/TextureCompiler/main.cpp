//
//  main.cpp
//  TextureCompiler
//
//  Created by Tomas Arce on 9/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

//
//  main.cpp
//  SpriteCompiler
//
//  Created by Tomas Arce on 9/16/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "CompilerBase.h"
#include "TextureCompilerBase.h"

// -BUILDTYPE DEBUG -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/ProjectAlpha/Data" -TARGET OSX IOS -INPUT "Compilation/Dependency/2xx2_4is5_ay35.texture"
// -OVERWRITE_DEFAULTS -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/ProjectAlpha/Data"
// Compilation/Dependency/2xx2_4is5_ay35.texture
// -BUILDTYPE DEBUG -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/LIONant/xReference/Data" -TARGET OSX IOS -INPUT "GameData/CompilerKeys/UV Grid Texture--2xx2_4is5_ay37.texture"

//---------------------------------------------------------------------------------------

int main(int argc, const char * argv[])
{
    texture_compiler        Compiler;
    
    g_bExceptionBreak = FALSE;
    
    if( Compiler.Parse( argc, argv ) )
    {
        return Compiler.Compile();
    }
    
    return 1;
}

