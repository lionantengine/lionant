//
//  TextureCompilerKey.cpp
//  TextureCompiler
//
//  Created by Tomas Arce on 9/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "compilerBase.h"
#include "TextureCompilerKey.h"

const xprop_enum::entry texture_compiler_key::main_params::m_ColorTypeEnum[ texture_compiler_key::COLOR_TYPE_COUNT ] =
{
    { COLOR_TYPE_COLOR,     X_STR( "COLOR" ) },
    { COLOR_TYPE_NORMALMAP, X_STR( "NORMALMAP" ) }
};

const xprop_enum::entry texture_compiler_key::main_params::m_AlphaEnum[ texture_compiler_key::ALPHA_COUNT ] =
{
    { ALPHA_DISABLE,        X_STR( "DISABLE" ) },
    { ALPHA_USE_SRC,        X_STR( "USE_SRC" ) },
    { ALPHA_FORCE_ONE_BIT,  X_STR( "FORCE_ONE_BIT" ) }
};

const xprop_enum::entry texture_compiler_key::main_params::m_TileEnum[ texture_compiler_key::TILEMODE_COUNT ]
{
    { TILEMODE_CLAMP,   X_STR( "CLAMP" ) },
    { TILEMODE_TILE,    X_STR( "TILE" ) },
    { TILEMODE_MIRROR,  X_STR( "MIRROR" ) }
};

const xprop_enum::entry texture_compiler_key::main_params::m_FilterEnum[ eng_texture::FILTER_MINMAG_COUNT ]
{
    { eng_texture::FILTER_MINMAG_NEAREST,                   X_STR( "NEAREST" ) },
    { eng_texture::FILTER_MINMAG_LINEAR,                    X_STR( "LINEAR" ) },
    { eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_NEAREST,    X_STR( "NEAREST_NEAREST" ) },
    { eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_NEAREST,     X_STR( "LINEAR_NEAREST" ) },
    { eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_LINEAR,     X_STR( "NEAREST_LINEAR" ) },
    { eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_LINEAR,      X_STR( "LINEAR_LINEAR" ) }
};

const xprop_enum::entry texture_compiler_key::main_params::m_SimpleFilterEnum[ FILTER_MODE_COUNT ]
{
    { FILTER_MODE_NULL,             X_STR( "NULL" ) },
    { FILTER_MODE_NEAREST,          X_STR( "NEAREST" ) },
    { FILTER_MODE_LINEAR,           X_STR( "LINEAR" ) },
    { FILTER_MODE_BILINEAR,         X_STR( "BILINEAR" ) },
    { FILTER_MODE_TRILINEAR,        X_STR( "TRILINEAR" ) }
};

const xprop_enum::entry texture_compiler_key::main_params::m_ColorSpaceEnum[ COLOR_SPACE_COUNT ]
{
    { COLOR_SPACE_LINEAR,           X_STR( "LINEAR" ) },
    { COLOR_SPACE_GAMMA,            X_STR( "GAMMA" ) }
};

//---------------------------------------------------------------------------------------

void texture_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM( "ColorType",         OVERWRITE_COLOR_TYPE,           g_PropEnum,     "This is the type of data that is in the source image. Different types of compression may apply base on this." )
    KEY_PROP_ENUM_FILE( "FileName",     OVERWRITE_FILENAME,                             "File name of the source image that needs compiling. Note that if the file name has the following sub strings:\n  _PPP will be consider a cube map and the rest of the chain will be expected. \n  _000 it will be consides an animation and the consequent source files will be compiled as well.", "*.tga,*.pbg,*.bmp" )
    KEY_PROP_ENUM( "CompressionLevel",  OVERWRITE_COMPRESSION_LEVEL,    g_PropFloat,    "It is the compression level vs quality slider. If 0 then no compression if 1 then Max compression." )
    KEY_PROP_ENUM( "AlphaMode",         OVERWRITE_ALPHA_MODE,           g_PropEnum,     "The termines what the compiler should do with the alpha, possible values are:\n  DISABLE - Means that the compiler will ignore the alpha.\n  USE_SRC - The alpha will be based on the src image. (If it has any).\n  FORCE_ONE_BIT - The alpha will be force tp 1 bit of information." )
    KEY_PROP_ENUM( "nFrames",           OVERWRITE_N_FRAMES,             g_PropInt,      "Tells how many frames the source texture has. Note that source filename must be formatted proeprly." )
    KEY_PROP_ENUM( "ScaleX",            OVERWRITE_X_SCALE,              g_PropFloat,    "X Scales the src image before putting it compiles it." )
    KEY_PROP_ENUM( "ScaleY",            OVERWRITE_Y_SCALE,              g_PropFloat,    "Y Scales the src image before putting it compiles it." )
    KEY_PROP_ENUM( "bCreateMips",       OVERWRITE_CREATE_MIPS,          g_PropBool,     "Tells the compiler to generate the full chain of mips." )
    KEY_PROP_ENUM( "U-Tiling",          OVERWRITE_U_TILE_MODE,          g_PropEnum,     "U tiling mode for the texture possible values are:\n   CLAMP.\n   TILE.\n   MIRROR." )
    KEY_PROP_ENUM( "V-Tiling",          OVERWRITE_V_TILE_MODE,          g_PropEnum,     "V tiling mode for the texture possible values are:\n   CLAMP.\n   TILE.\n   MIRROR." )
    KEY_PROP_ENUM( "W-Tiling",          OVERWRITE_W_TILE_MODE,          g_PropEnum,     "W tiling mode for the texture possible values are:\n   CLAMP.\n   TILE.\n   MIRROR." )
    KEY_PROP_ENUM( "FilterMig",         OVERWRITE_FILTER_MIG,           g_PropEnum,     "Which filter the texture will used when scale down. possible values are:\n   NEAREST.\n   LINEAR.\n   BILINEAR.\n   NEAREST_NEAREST.\n   LINEAR_NEAREST.\n   NEAREST_LINEAR.\n   LINEAR_LINEAR.\n" )
    KEY_PROP_ENUM( "FilterMag",         OVERWRITE_FILTER_MAG,           g_PropEnum,     "Which filter the texture will used when scale up. possible values are:\n   NEAREST.\n   LINEAR.\n   BILINEAR.\n   NEAREST_NEAREST.\n   LINEAR_NEAREST.\n   NEAREST_LINEAR.\n   LINEAR_LINEAR.\n" )
    KEY_PROP_ENUM( "TextureFilter",     OVERWRITE_TEXTURE_FILTER,       g_PropEnum,     "Type of texture filter (this will over ride the Filter Mig/Mag):\n   NEAREST.\n   LINEAR.\n   BILINEAR.\n  TRILINEAR.\n" )
    KEY_PROP_ENUM( "ColorSpace",        OVERWRITE_COLOR_SPACE,          g_PropEnum,     "Tells if the input texture should be treated as it has Gamma or Linear. For linear space textures they will be left alone for gamma color space conversions will have to be made.\n" )
}

//---------------------------------------------------------------------------------------

xbool texture_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "ColorType",         OVERWRITE_COLOR_TYPE,           g_PropEnum.Query( Query, m_ColorType, m_ColorTypeEnum )     )     
    KEY_PROP_QUERY( "FileName",          OVERWRITE_FILENAME,             g_PropFilePath.Query( Query, m_SrcFileName )                )     
    KEY_PROP_QUERY( "CompressionLevel",  OVERWRITE_COMPRESSION_LEVEL,    g_PropFloat.Query( Query, m_CompressionLevel, 0, 1)         )    
    KEY_PROP_QUERY( "AlphaMode",         OVERWRITE_ALPHA_MODE,           g_PropEnum.Query( Query, m_AlphaMode, m_AlphaEnum )         )     
    KEY_PROP_QUERY( "nFrames",           OVERWRITE_N_FRAMES,             g_PropInt.Query( Query, m_nFrames, -1, 100 )                )     
    KEY_PROP_QUERY( "ScaleX",            OVERWRITE_X_SCALE,              g_PropFloat.Query( Query, m_Scale.m_X, 0.001f, 10)          )    
    KEY_PROP_QUERY( "ScaleY",            OVERWRITE_Y_SCALE,              g_PropFloat.Query( Query, m_Scale.m_Y, 0.001f, 10)          )    
    KEY_PROP_QUERY( "bCreateMips",       OVERWRITE_CREATE_MIPS,          g_PropBool.Query( Query, m_bCreateMips )                    )     
    KEY_PROP_QUERY( "U-Tiling",          OVERWRITE_U_TILE_MODE,          g_PropEnum.Query( Query, m_UTilingMode, m_TileEnum )        )     
    KEY_PROP_QUERY( "V-Tiling",          OVERWRITE_V_TILE_MODE,          g_PropEnum.Query( Query, m_VTilingMode, m_TileEnum )        )     
    KEY_PROP_QUERY( "W-Tiling",          OVERWRITE_W_TILE_MODE,          g_PropEnum.Query( Query, m_WTilingMode, m_TileEnum )        )     
    KEY_PROP_QUERY( "FilterMig",         OVERWRITE_FILTER_MIG,           g_PropEnum.Query( Query, m_FilterMig, m_FilterEnum )        )     
    KEY_PROP_QUERY( "FilterMag",         OVERWRITE_FILTER_MAG,           g_PropEnum.Query( Query, m_FilterMag, m_FilterEnum )        )     
    KEY_PROP_QUERY( "TextureFilter",     OVERWRITE_TEXTURE_FILTER,       g_PropEnum.Query( Query, m_TextureFilter, m_SimpleFilterEnum ) )
    KEY_PROP_QUERY( "ColorSpace",        OVERWRITE_COLOR_SPACE,          g_PropEnum.Query( Query, m_ColorSpace, m_ColorSpaceEnum ) )
    
    return FALSE;
}

//---------------------------------------------------------------------------------------

void texture_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_ColorType,           OVERWRITE_COLOR_TYPE,           =     )     
    KEY_PROP_UPDATE( m_SrcFileName,         OVERWRITE_FILENAME,             .Copy )     
    KEY_PROP_UPDATE( m_CompressionLevel,    OVERWRITE_COMPRESSION_LEVEL,    =     )    
    KEY_PROP_UPDATE( m_AlphaMode,           OVERWRITE_ALPHA_MODE,           =     )     
    KEY_PROP_UPDATE( m_nFrames,             OVERWRITE_N_FRAMES,             =     )     
    KEY_PROP_UPDATE( m_Scale.m_X,           OVERWRITE_X_SCALE,              =     )    
    KEY_PROP_UPDATE( m_Scale.m_Y,           OVERWRITE_Y_SCALE,              =     )    
    KEY_PROP_UPDATE( m_bCreateMips,         OVERWRITE_CREATE_MIPS,          =     )     
    KEY_PROP_UPDATE( m_UTilingMode,         OVERWRITE_U_TILE_MODE,          =     )     
    KEY_PROP_UPDATE( m_VTilingMode,         OVERWRITE_V_TILE_MODE,          =     )     
    KEY_PROP_UPDATE( m_WTilingMode,         OVERWRITE_W_TILE_MODE,          =     )     
    KEY_PROP_UPDATE( m_FilterMig,           OVERWRITE_FILTER_MIG,           =     )     
    KEY_PROP_UPDATE( m_FilterMag,           OVERWRITE_FILTER_MAG,           =     )     
    KEY_PROP_UPDATE( m_TextureFilter,       OVERWRITE_TEXTURE_FILTER,       =     )     
    KEY_PROP_UPDATE( m_ColorSpace,          OVERWRITE_COLOR_SPACE,          =     )     
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

void texture_compiler_key::getAdditionalDependencies( xarray2<dependency>& List ) const
{
    /*
    //
    // Collect file names
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        const main_params& Desc = m_Users[i];
        
        if( !x_FlagIsOn( Desc.m_OverRideBits, OVERWRITE_FILENAME ) )
            continue;
        
        xbool bFound = FALSE;
        for( s32 j=0; j<List.getCount(); j++ )
        {
            dependency& Dep = List[j];
            
            if( Dep.m_FilePath == Desc.m_SrcFileName )
            {
                bFound = TRUE;
                Dep.m_Plaforms |= (1<<i);
                break;
            }
        }
        
        if( bFound == FALSE )
        {
            dependency& Dep = List.append();
            Dep.m_FilePath = Desc.m_SrcFileName;
            Dep.m_Plaforms = (1<<i);
            if( i == 0 )
            {
                Dep.m_Plaforms = ~0;
                break;
            }
        }
    }
    
    //
    // Deal with cube-maps
    //
    for( s32 i=0; i<List.getCount(); i++ )
    {
        const dependency&   DepBase = List[i];
        s32                 Index   = DepBase.m_FilePath.FindI( "_PPP" );
        
        if( Index != -1 )
        {
            static const char* Names[]=
            {
                "_PPP",
                "_PPN",
                "_PNP",
                "_PNN",
                "_NPP",
                "_NPN",
                "_NNP",
                "_NNN",
                NULL
            };
            
            // Copy all the names as dependencies
            for( s32 i=1; Names[i]; i++ )
            {
                dependency& Dep = List.append();
                
                Dep.m_FilePath.Format( "%s", (const char*)DepBase.m_FilePath );
                
                for( s32 j=0; (Dep.m_FilePath[ Index + j] = Names[i][j]); j++ );
                Dep.m_Plaforms = DepBase.m_Plaforms;
            }
        }
    }
    */

    //
    // Deal with animations
    //
    /*
    for( s32 i=0; i<List.getCount(); i++ )
    {
        const dependency&   DepBase = List[i];
        s32                 Index = DepBase.m_FilePath.FindI( "_000" );
        
        if( Index != -1 )
        {
            s32 MaxFrames=0;
            
            // Find the max frames from all the platforms
            for( s32 p=0; i<X_PLATFORM_COUNT; i++ )
            {
                if( (DepBase.m_Plaforms>>p)&1)
                    MaxFrames = x_Max( MaxFrames, m_Users[p].m_nFrames );
            }
            
            ASSERT( MaxFrames > 1 );
            ASSERT( MaxFrames < 1000 );
            
            // Copy all the names as dependencies
            for( s32 i=1; MaxFrames; i++ )
            {
                xstring     Number;
                dependency& Dep = List.append();
                
                Dep.m_FilePath.Format( "%s", (const char*)DepBase.m_FilePath );
                Number.Format( "_%03d", i );
                
                for( s32 j=0; (Dep.m_FilePath[ Index + j] = Number[j]); j++ );
                
                for( s32 p=0; p<X_PLATFORM_COUNT; p++ )
                {
                    if( (DepBase.m_Plaforms>>p)&1)
                    {
                        if( i < m_Users[p].m_nFrames)
                            Dep.m_Plaforms |= p;
                    }
                }
            }
        }
    }
     */
}

