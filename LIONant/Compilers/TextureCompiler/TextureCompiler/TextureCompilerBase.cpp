//
//  TextureCompilerBase.cpp
//  TextureCompiler
//
//  Created by Tomas Arce on 9/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

// System includes
//#include <thread>       // This is need it to get the std:function

#include "PVRTexture.h"
#include "PVRTextureUtilities.h"
using namespace pvrtexture;
#include "squish.h"

//xBase includes
#include "x_base.h"
#include "xbmp_Tools.h"
#include "CompilerBase.h"
#include "TextureCompilerBase.h"

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
static
eng_texture::wrapping_mode TilingMapping( texture_compiler_key::tiling_mode TilingMode )
{
    switch( TilingMode )
    {
        case texture_compiler_key::TILEMODE_CLAMP:  return eng_texture::WRAP_MODE_CLAMP;
        case texture_compiler_key::TILEMODE_MIRROR: return eng_texture::WRAP_MODE_MIRROR;
        case texture_compiler_key::TILEMODE_TILE:   return eng_texture::WRAP_MODE_TILE;
        default: ASSERT(0);
    }
   
    return eng_texture::WRAP_MODE_IGNORE;
}

//-------------------------------------------------------------------------------------------------

void texture_compiler::FillInfoStructure( const platform& Target, eng_texture_rsc::info& Info ) const
{
    //
    // Set the filtering
    // ref: http://gregs-blog.com/2008/01/17/opengl-texture-filter-parameters-explained/
    //
    // Filter Combination (MAG_FILTER/MIN_FILTER)	Bilinear Filtering (Near)	Bilinear Filtering (Far)	Mipmapping
    // GL_NEAREST / GL_NEAREST_MIPMAP_NEAREST       Off                         Off                         Standard
    // GL_NEAREST / GL_LINEAR_MIPMAP_NEAREST        Off                         On                          Standard
    // GL_NEAREST / GL_NEAREST_MIPMAP_LINEAR        Off                         Off                         Use trilinear filtering
    // GL_NEAREST / GL_LINEAR_MIPMAP_LINEAR         Off                         On                          Use trilinear filtering
    // GL_NEAREST / GL_NEAREST                      Off                         Off                         None
    // GL_NEAREST / GL_LINEAR                       Off                         On                          None
    // GL_LINEAR / GL_NEAREST_MIPMAP_NEAREST        On                          Off                         Standard
    // GL_LINEAR / GL_LINEAR_MIPMAP_NEAREST         On                          On                          Standard
    // GL_LINEAR / GL_NEAREST_MIPMAP_LINEAR         On                          Off                         Use trilinear filtering
    // GL_LINEAR / GL_LINEAR_MIPMAP_LINEAR          On                          On                          Use trilinear filtering
    // GL_LINEAR / GL_NEAREST                       On                          Off                         None
    // GL_LINEAR / GL_LINEAR                        On                          On                          None
    //
    
    const_ref( rMain, m_Key.m_Main );
    const texture_compiler_key::main_params& Params = rMain[ Target.m_Platform ];
    
    if( !Params.m_bCreateMips )
    {
        if( Params.m_FilterMig != eng_texture::FILTER_MINMAG_NEAREST &&
            Params.m_FilterMig != eng_texture::FILTER_MINMAG_LINEAR )
        {
            x_throw( "ERROR: The parameter FilterMig has the following setting[%s] only LINEAR and NEAREST as allowed when not mipmaping",
                         (const char*)m_Key.getFilterEnum(Info.m_Mig).m_EnumName );
        }
    }

    Info.m_Mig = Params.m_FilterMig;
    Info.m_Mag = Params.m_FilterMag;

   /*
    switch( Params.m_FilterMode )
    {
        default:ASSERT(0);
        case texture_compiler_key::FILTER_MODE_NEAREST:
        {
            if( Params.m_bCreateMips )
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_NEAREST;
                Info.m_Mag = eng_texture::FILTER_MINMAG_NEAREST;
            }
            else
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST;
                Info.m_Mag = eng_texture::FILTER_MINMAG_NEAREST;
            }
            break;
        }
        case texture_compiler_key::FILTER_MODE_LINEAR:
        {
            if( Params.m_bCreateMips )
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST_MIPMAP_NEAREST;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            else
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_LINEAR;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            break;
        }
        case texture_compiler_key::FILTER_MODE_BILINEAR:
        {
            if( Params.m_bCreateMips )
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_NEAREST;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            else
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_LINEAR;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            break;
        }
        case texture_compiler_key::FILTER_MODE_TRILINEAR:
        {
            if( Params.m_bCreateMips )
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_LINEAR;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            else
            {
                Info.m_Mig = eng_texture::FILTER_MINMAG_LINEAR;
                Info.m_Mag = eng_texture::FILTER_MINMAG_LINEAR;
            }
            break;
        }
        case texture_compiler_key::FILTER_MODE_ANISOTROPIC_LOW:
        {
            Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST;
            Info.m_Mag = eng_texture::FILTER_MINMAG_NEAREST;
            break;
        }
        case texture_compiler_key::FILTER_MODE_ANISOTROPIC_MID:
        {
            Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST;
            Info.m_Mag = eng_texture::FILTER_MINMAG_NEAREST;
            break;
        }
        case texture_compiler_key::FILTER_MODE_ANISOTROPIC_HIGH:
        {
            Info.m_Mig = eng_texture::FILTER_MINMAG_NEAREST;
            Info.m_Mag = eng_texture::FILTER_MINMAG_NEAREST;
            break;
        }
    }
    */
    
    //
    // Tiling Mode
    //
    Info.m_uWrapping = TilingMapping( Params.m_UTilingMode );
    Info.m_vWrapping = TilingMapping( Params.m_UTilingMode );
    Info.m_wWrapping = TilingMapping( Params.m_UTilingMode );
}

//-------------------------------------------------------------------------------------------------

void texture_compiler::HandleIOS( void ) const
{
    const xplatform                             Platform = X_PLATFORM_IOS;
    xarray<xbitmap>                             IOSBitmaps;


    const_ref( rMain, m_Key.m_Main );
    const texture_compiler_key::main_params&    Params = rMain[ Platform ];
    
    //
    // Start compiling
    //
    const_ref( rSrcFileNames, m_lSrcFileNames );
    for( s32 i=0; i<m_lSrcBitmap.getCount(); i++ )
    {
        const compiler_key_object::dependency& Dep =  rSrcFileNames[i];
        
        if( FALSE == x_FlagIsOn( Dep.m_Plaforms, 1<<Platform) )
            continue;
        
        // We get the first bitmap
        xbitmap&            OutputBitmap    = IOSBitmaps.append();
        
        //
        // Codify the alpha
        //
        u32 Alpha;
        
        const xbitmap&  T1Bitmap          = m_lSrcBitmap[i];
        switch( Params.m_AlphaMode )
        {
            case texture_compiler_key::ALPHA_FORCE_ONE_BIT: Alpha = 1; break;
            case texture_compiler_key::ALPHA_DISABLE:       Alpha = 0; break;
            case texture_compiler_key::ALPHA_USE_SRC:       Alpha = T1Bitmap.hasAlphaInfo()*0xff; break;
            default:ASSERT(0);
        }
        
        //
        // Do required transformations
        //
        xbitmap T2Bitmap;
        xbool   bResized=FALSE;
        if( Params.m_Scale.m_X != 1 || Params.m_Scale.m_Y != 1 )
        {
            T1Bitmap.CreateResizedBitmap( T2Bitmap, s32(T1Bitmap.getWidth() * Params.m_Scale.m_X), s32(T1Bitmap.getHeight() * Params.m_Scale.m_Y) );
            bResized = TRUE;
        }

        //
        // Set the official bitmap
        //
        const xbitmap& Bitmap = bResized ? T2Bitmap: T1Bitmap;

        //
        // Validate a few crazy requirements from ios
        //
        if( Bitmap.isPowerOfTwo() == FALSE )
        {
            if( Params.m_UTilingMode != texture_compiler_key::TILEMODE_CLAMP ||
                Params.m_VTilingMode != texture_compiler_key::TILEMODE_CLAMP )
                x_throw( "ERROR: IOS does not support any other mode except clamp for non power of 2 textures [%d,%d]", Bitmap.getWidth(), Bitmap.getHeight() );
        }
        
        //
        // Encode the bitmap into the PVR lib
        //
        CPVRTextureHeader   CTextureHeader  ( PVRStandard8PixelType.PixelTypeID, Bitmap.getHeight(), Bitmap.getWidth() );
        CPVRTexture         CTexture        ( CTextureHeader.getFileHeader(), Bitmap.getMip(0) );
        
        //
        // Handle mips
        //
        if( Params.m_bCreateMips )
        {
            if( false == GenerateMIPMaps( CTexture, eResizeCubic ) )
                x_throw( "ERROR: IOS -> Fail to Compute mips for [%s]", (const char*)rSrcFileNames[i].m_FilePath );
        }
        
        //
        // Let the user know what is going on with non-square textures
        //
        f32 CompressionLevel = Params.m_CompressionLevel;
        
        if( !Bitmap.isSquare() && Params.m_CompressionLevel >= 0.3 )
        {
            x_printf( "WARNING: Texture is not square, so we won't be able to compress it.");
            
            // change the quality level since we can not compress the texture
            CompressionLevel = 0.2f;
        }
        
        //
        // Determine the format base on the quality vs compression slider
        //
        xbitmap::format XBmpFormat( xbitmap::FORMAT_XCOLOR );
        if( CompressionLevel > 0.1f )
        {
            PixelType Format;
            
            // Determine the level of compression
            if( CompressionLevel < 0.3 )
            {
                Format = ePVRTPF_NumCompressedPFs;
                switch( Alpha )
                {
                    case 0:  Format     = PixelType('r','g','b',0,5,6,5,0);
                             XBmpFormat = xbitmap::FORMAT_B5G6R5;
                             break;
                    case 1:  Format     = PixelType('r','g','b','a',5,5,5,1);
                             XBmpFormat = xbitmap::FORMAT_A1B5G5R5;
                             break;
                    default: Format     = PixelType('r','g','b','a',4,4,4,4);
                             XBmpFormat = xbitmap::FORMAT_A4B4G4R4;
                             break;
                }
                
                if( Bitmap.getWidth()&1 || Bitmap.getHeight()&1 )
                {
                    x_printf( "WARNING: IOS may have issues with odd sized textures, even seems fine [%d,%d]", Bitmap.getWidth(), Bitmap.getHeight() );
                }

            }
            else
            {
                // Make sure that we care taken care of basic rules
                if( Bitmap.isPowerOfTwo() == FALSE )
                {
                    x_throw( "ERROR: IOS -> Texture is not power of 2 and the user require us to compress it [%s]", (const char*)rSrcFileNames[i].m_FilePath );
                }
    
                if( CompressionLevel < 0.7 )
                {
                    switch( Alpha )
                    {
                        case 0:  Format     = ePVRTPF_PVRTCI_4bpp_RGB;
                                 XBmpFormat = xbitmap::FORMAT_PVR1_4RGB;
                                 break;
                        default: Format     = ePVRTPF_PVRTCI_4bpp_RGBA;
                                 XBmpFormat = xbitmap::FORMAT_PVR1_4RGBA;
                                 break;
                    }
                }
                else
                {
                    
                    switch( Alpha )
                    {
                        case 0:  Format     = ePVRTPF_PVRTCI_2bpp_RGB;
                                 XBmpFormat = xbitmap::FORMAT_PVR1_2RGB;
                                 break;
                        default: Format     = ePVRTPF_PVRTCI_2bpp_RGBA;
                                 XBmpFormat = xbitmap::FORMAT_PVR1_2RGBA;
                                 break;
                    }
                }
            }
            
            //
            // Compress texture
            //
            ECompressorQuality Quality;
            
            if( getBuildType() == BUILDTYPE_RELEASE ) Quality = ePVRTCHigh;
            else                                      Quality = ePVRTCFast;
            
            if( FALSE == Transcode(
                                      CTexture,
                                      Format,
                                      ePVRTVarTypeUnsignedByteNorm,
                                      Params.m_ColorSpace == texture_compiler_key::COLOR_SPACE_LINEAR?ePVRTCSpacelRGB:ePVRTCSpacesRGB,
                                      Quality,                         // Quality level
                                      false ) )
            {
                x_throw( "ERROR: IOS -> Fail to Compress texture [%s]", (const char*)rSrcFileNames[i].m_FilePath );
            }
        }
        
        //
        // Save debug information
        //
        if( m_BuildType == BUILDTYPE_DEBUG )
        {
            CPVRTexture         Temp( CTexture );
            Transcode(
                       Temp,
                       PVRStandard8PixelType.PixelTypeID,
                       ePVRTVarTypeUnsignedByteNorm,
                       ePVRTCSpacelRGB,
                       ePVRTCHigh,                         // Quality level
                       false );
            
            xstring FileN;
            FileN.Format( "%s/%s--%s_debug_ios.tga",
                         (const char*)m_LogPath,
                         (const char*)m_RscFileName,
                         (const char*)m_RscGuid.GetAlphaString() );

            // Save the raw format
            CTexture.saveFile( (const char*)FileN );
            
            // Save the actual tga
            {
                xbitmap             Bitmap;
                PVRTextureHeaderV3  Header( Temp.getFileHeader() );
                
                s32     DataSize = Header.u32Height*Header.u32Width;
                s32*    pData    = (s32*)x_malloc( sizeof(xcolor), DataSize+1, 0);
                pData[0] = 0;
                
                x_memcpy( &pData[1], Temp.getDataPtr( 0 ), sizeof(xcolor)*DataSize );
                
                Bitmap.setupFromColor( Header.u32Width, Header.u32Height, pData );
                Bitmap.SaveTGA( FileN );
            }
        }
        
        
        //
        // Re encode into a xbitmap
        //
        {
            PVRTextureHeaderV3 Header( CTexture.getFileHeader() );
            
            // The last mip is problematic...
            const s32     nMips          = Header.u32MIPMapCount;
            const s32     MipOffsetTable = sizeof(s32)*nMips;
            const s32     TotalSize      = MipOffsetTable + CTexture.getDataSize();
            const void*   pBaseData      = CTexture.getDataPtr( 0 );
            
            void*         pBmpDataBase   = x_malloc( 1, TotalSize, 0 );
            s32*          pIndex         = (s32*)pBmpDataBase;
            xbyte*        pMipBase       = (xbyte*)&pIndex[nMips];
            
            // Copy all the mip data to our buffer
            x_memcpy( pMipBase, pBaseData, CTexture.getDataSize() );
            
            //
            // Now compute all the mips's offsets
            //
            for( s32 i=0; i<nMips; i++ )
            {
                void* pData   = CTexture.getDataPtr( i );
                s32   Offset  = s32( (xbyte*)pData - (xbyte*)pBaseData );
                pIndex[i] = Offset;
            }
            
            //
            // Build the final bitmap
            //
            OutputBitmap.setup(
                              Header.u32Width,
                              Header.u32Height,
                              XBmpFormat,
                              TotalSize,
                              TotalSize - MipOffsetTable,
                              pBmpDataBase,
                              TRUE,
                              nMips,
                              1 );
            OutputBitmap.setColorSpace( Params.m_ColorType != texture_compiler_key::COLOR_SPACE_LINEAR );

            //
            // Print some info for the user
            //
            PrintFormatInfo( OutputBitmap, Platform );
        }
    }

    //
    // Check to see if in deed we end up compiling anything
    //
    if( IOSBitmaps.getCount() == 0 )
    {
        x_printf("WARNING: IOS was excluded for resource %s ", (const char*)m_RscFileName );
        return;
    }
    
    //
    // Create the resource structure and export it
    //
    
    // For right now we just deal with one
    ASSERT( IOSBitmaps.getCount() == 1 );
    
    const platform&         Target = m_Target[ Platform ];
    eng_texture_rsc::info   Info;
    xserialfile             File;
    xstring                 FilePath;
    
    FillInfoStructure( Target, Info );
    
    FilePath = getFinalResourceName( Target );
    
    Info.m_Bitmap = IOSBitmaps[0];
    
    // We dont want to free this memory by this instance neither here nor in the engine
    Info.m_Bitmap.setOwnMemory( FALSE );
    
    File.Save( FilePath, Info );
}

//-------------------------------------------------------------------------------------------------

void texture_compiler::PrintFormatInfo( const xbitmap& Bitmap, xplatform Platform ) const
{
    const char* pFormat = "???";
    
    switch ( Bitmap.getFormat() )
    {
        case xbitmap::FORMAT_R8G8B8U8:      pFormat= "R8G8B8U8";    break;
        case xbitmap::FORMAT_R8G8B8A8:      pFormat= "R8G8B8A8";    break;
        case xbitmap::FORMAT_B5G6R5:        pFormat= "R5G6B5";      break;
        case xbitmap::FORMAT_A1B5G5R5:      pFormat= "R5G5B5A1";    break;
        case xbitmap::FORMAT_A4B4G4R4:      pFormat= "R4G4B4A4";    break;
        case xbitmap::FORMAT_PVR1_4RGB:     pFormat= "PVR1_4RGB";   break;
        case xbitmap::FORMAT_PVR1_4RGBA:    pFormat= "PVR1_4RGBA";  break;
        case xbitmap::FORMAT_PVR1_2RGB:     pFormat= "PVR1_2RGB";   break;
        case xbitmap::FORMAT_PVR1_2RGBA:    pFormat= "PVR1_2RGBA";  break;
        case xbitmap::FORMAT_DXT1_RGBA:     pFormat= "DXT1";        break;
        case xbitmap::FORMAT_DXT3_RGBA:     pFormat= "DXT3";        break;
        case xbitmap::FORMAT_DXT5_RGBA:     pFormat= "DXT5";        break;
            
            // Please add the format in the list
        default: ASSERT( 0 );
    }
    
    const char* pColorSpace = Bitmap.isGamma()?"LINEAR":"GAMMA";
    x_printf("INFO: %s Compiled as: %s [%s]",
             x_PlatformString( Platform ),
             pFormat,
             pColorSpace );
}

//-------------------------------------------------------------------------------------------------

void texture_compiler::HandleOSXAndPC( const xplatform Platform ) const
{
    xarray<xbitmap>                             OSXBitmap;

    const_ref( rMain, m_Key.m_Main );
    const texture_compiler_key::main_params&    Params = rMain[ Platform ];
    
    //
    // Start compiling
    //
    const_ref( rSrcFileNames, m_lSrcFileNames );
    for( s32 i=0; i<m_lSrcBitmap.getCount(); i++ )
    {
        const compiler_key_object::dependency& Dep =  rSrcFileNames[i];
        
        if( FALSE == x_FlagIsOn( Dep.m_Plaforms, 1<<Platform) )
           continue;
        
        // We get the first bitmap
        xarray<xbitmap> MipMaps;
        xbitmap&        OutputBitmap    =  OSXBitmap.append();
        
        //
        // Codify the alpha
        //
        u32 Alpha;
        {
            const xbitmap&  Bitmap = m_lSrcBitmap[i];
            
            switch( Params.m_AlphaMode )
            {
                case texture_compiler_key::ALPHA_FORCE_ONE_BIT: Alpha = 1; break;
                case texture_compiler_key::ALPHA_DISABLE:       Alpha = 0; break;
                case texture_compiler_key::ALPHA_USE_SRC:       Alpha = Bitmap.hasAlphaInfo()*0xff; break;
                default:ASSERT(0);
            }
            
            //
            // Handle bitmap scaling and mipmaps
            //
            xbitmap&        TopMip = MipMaps.append();
            
            if( Params.m_Scale.m_X != 1 || Params.m_Scale.m_Y != 1 )
            {
                Bitmap.CreateResizedBitmap( TopMip, s32(Bitmap.getWidth() * Params.m_Scale.m_X), s32(Bitmap.getHeight() * Params.m_Scale.m_Y) );
            }
            else
            {
                TopMip.Copy( Bitmap );
            }

            //
            // Handle mips
            //
            if( Params.m_bCreateMips )
            {
                // We already got the top mip so lets figure out the rest
                s32 nMips = TopMip.getFullMipChainCount()-1;
                
                if( nMips > 0 )
                {
                    MipMaps.appendList( nMips );
                    
                    s32 W = TopMip.getWidth();
                    s32 H = TopMip.getHeight();
                    for( s32 i=0; i<nMips; i++ )
                    {
                        xbitmap& Mip = MipMaps[1+i];
                        
                        W >>= 1;
                        H >>= 1;
                        
                        MipMaps[i].CreateResizedBitmap( Mip, W, H );
                    }
                }
            }
        }
        
        //
        // Determine the format base on the quality vs compression slider
        //
        xbitmap&        TopMip = MipMaps[0];
        xbitmap::format XBmpFormat( xbitmap::FORMAT_XCOLOR );
        
        if( Params.m_CompressionLevel > 0.1 )
        {
            u32 SquishFlags=0;
            
            // Determine the level of compression
            if( Params.m_CompressionLevel < 0.3 )
            {
                switch( Alpha )
                {
                    case 0:
                        XBmpFormat = xbitmap::FORMAT_B5G6R5;
                        break;
                    case 1:
                        XBmpFormat = xbitmap::FORMAT_A1B5G5R5;
                        break;
                    default:
                        XBmpFormat = xbitmap::FORMAT_A4B4G4R4;
                        break;
                }
            }
            else
            {
                // Choose compression base on alpha
                switch( Alpha )
                {
                    case 0:
                    case 1:
                        SquishFlags = squish::kDxt1;
                        XBmpFormat = xbitmap::FORMAT_DXT1_RGBA;
                        break;
                    default:
                        SquishFlags = squish::kDxt5;
                        XBmpFormat = xbitmap::FORMAT_DXT5_RGBA;
                        break;
                }
                
                // Make sure that we care taken care of basic rules
                if( XBmpFormat == xbitmap::FORMAT_DXT1_RGBA)
                {
                    if( (TopMip.getWidth() | TopMip.getHeight())&7 )
                    {
                        x_printf("WARNING: OSX -> Texture is not multiple of 8 W:%d H:%d [%s] ", TopMip.getWidth(), TopMip.getHeight(), (const char*)rSrcFileNames[i].m_FilePath );
                    }
                }
                else
                {
                    if( (TopMip.getWidth() | TopMip.getHeight())&3 )
                    {
                        x_printf("WARNING: OSX -> Texture is not multiple of 4 W:%d H:%d [%s] ", TopMip.getWidth(), TopMip.getHeight(), (const char*)rSrcFileNames[i].m_FilePath );
                    }
                }
            }
        
            //
            // Compress Mips
            //
            if( XBmpFormat < xbitmap::FORMAT_XCOLOR_END )
            {
                x_inline_light_jobs_block<16> JobBlock;

                for( xbitmap& Mip : MipMaps ) JobBlock.SubmitJob( [&Mip,this,XBmpFormat]()
                {
                    const s32       L        = Mip.getWidth() * Mip.getHeight();
                    const xcolor*   pSrcData = (xcolor*)Mip.getMip(0);
                    u16*            pDstData = (u16*)Mip.getMip(0);
                    
                    for ( s32 j = 0; j < L; j++ )
                    {
                        pDstData[ j ] = (u16)pSrcData[ j ].BuildDataFromColor( ( xcolor::format )XBmpFormat );

                        // Sanity check for color conversion
                        if ( 0 )
                        {
                            xcolor test = pDstData[ j ];
                            xcolor A;
                            A.BuildColorFromData( test.m_Color, xcolor::FMT_XCOLOR );
                            ASSERT( A == test );
                        }
                    }
                    
                    // Tell the xbitmap to not free the memory
                    Mip.setOwnMemory( FALSE );
                    Mip.setup( 
                        Mip.getWidth( ), 
                        Mip.getHeight( ), 
                        XBmpFormat, 
                        L * 2 + 4, 
                        L * 2, 
                        pDstData - 2, 
                        TRUE, 
                        1, 
                        1 );
                } );
                
                JobBlock.FinishJobs();
                
                if( (1) )
                {
                    xbitmap Debug;
                    
                    Debug.CreateBitmap( MipMaps[0].getWidth(), MipMaps[0].getHeight() );
                    {
                        const s32       L        = MipMaps[0].getWidth() * MipMaps[0].getHeight();
                        const u16*      pSrcData = (u16*)MipMaps[0].getMip(0);
                        xcolor*         pDstData = (xcolor*)Debug.getMip(0);
                        
                        for( s32 j=0; j<L; j++ )
                        {
                            pDstData[j].BuildColorFromData( pSrcData[j], (xcolor::format)XBmpFormat );
                        }
                    }

                    xstring FileN;
                    FileN.Format( "%s/%s--%s_debug_%s.tga",
                        (const char*)m_LogPath,
                        (const char*)m_RscFileName,
                        (const char*)m_RscGuid.GetAlphaString( ),
                        x_PlatformString( Platform ) );
                    Debug.SaveTGA( FileN );
                }
                
            }
            else
            {
                //
                // Lets think multi-core
                //
                x_inline_light_jobs_block<16> JobBlock;
                
                if( getBuildType() == BUILDTYPE_DEBUG ) SquishFlags |= squish::kColourRangeFit;
                else                                    SquishFlags |= squish::kColourIterativeClusterFit;
                
                for( xbitmap& Mip : MipMaps ) JobBlock.SubmitJob( [&Mip,this,XBmpFormat,SquishFlags]()
                {
                    const s32 CompressedSize = squish::GetStorageRequirements( Mip.getWidth(), Mip.getHeight(), SquishFlags );
                    const s32 TotalSize      = sizeof(s32) + CompressedSize;
                    xbyte*    pDstData       = (xbyte*)x_malloc( 1, TotalSize, 0 );
                    
                    // Compress
                    squish::CompressImage( (xbyte*)Mip.getMip(0), Mip.getWidth(), Mip.getHeight(), &pDstData[4], SquishFlags );
                    
                    // Set the index to the texture
                    ((u32*)pDstData)[0] = 0;
                    
                    // Build the new xbitmap
                    Mip.setup( Mip.getWidth(),
                               Mip.getHeight(),
                               XBmpFormat,
                               TotalSize,
                               CompressedSize,
                               pDstData,
                               TRUE,
                               1,
                               1 );
                } );
                
                //
                // Tell the system to run them all
                //
                JobBlock.FinishJobs();
            }
        }

        //
        // Finally combine the hold thing into a single texture
        //
        OutputBitmap.CreateFromMips( &MipMaps[0], MipMaps.getCount() );
        
        //
        // Print some info for the user
        //
        PrintFormatInfo( MipMaps[0], Platform );
    }
    
    //
    // Check to see if in deed we end up compiling anything
    //
    if( OSXBitmap.getCount() == 0 )
    {
        x_printf("WARNING: OSX was excluded for resource %s ", (const char*)m_RscFileName );
        return;
    }
    
    //
    // Create the resource structure and export it
    //
    
    // For right now we just deal with one
    ASSERT( OSXBitmap.getCount() == 1 );
    
    const platform&         Target = m_Target[ Platform ];
    eng_texture_rsc::info   Info;
    xserialfile             File;
    xstring                 FilePath;
    
    FillInfoStructure( Target, Info );
    
    FilePath = getFinalResourceName( Target );
    
    Info.m_Bitmap = OSXBitmap[0];
    
    // We dont want to free this memory by this instance neither here nor in the engine
    Info.m_Bitmap.setOwnMemory( FALSE );
    
    File.Save( FilePath, Info );
}

//-------------------------------------------------------------------------------------------------

void texture_compiler::onCompile( void )
{
    // Make the key quantum happy
    m_Key.m_Main.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    
    //
    // Load all the source pictures
    //
    {
        m_Key.getAssetDependencies( m_lSrcFileNames );
        
        //
        // Filter out all irrelevant bitmaps
        //
        {
            mutable_ref( rSrcFileNames, m_lSrcFileNames );
            for( s32 i=0; i<m_lSrcFileNames.getCount(); i++ )
            {
                const compiler_key_object::dependency& Dep = rSrcFileNames[i];
                
                // Make sure that this resource file is need it for the platforms we need to compile for
                if( 0 == (Dep.m_Plaforms & m_PlatformMask) )
                {
                    rSrcFileNames->DeleteWithCollapse(i);
                    i--;
                }
            }
            
            rSrcFileNames.ReleaseAndQTReadOnly();
        }
        
        //
        // Now we can preallocate all of them
        //
        m_lSrcBitmap.appendList( m_lSrcFileNames.getCount() );
        
        //
        // Now we are ready to load
        //
        const_ref( rSrcFileNames, m_lSrcFileNames );
        for( s32 i=0; i<m_lSrcFileNames.getCount(); i++ )
        {
            const compiler_key_object::dependency&  Dep     = rSrcFileNames[i];
            xbitmap&                                Bitmap  = m_lSrcBitmap[i];
            xstring                                 FilePath;
            
            FilePath.Format( "%s/%s", (const char*)m_ProjectPath, (const char*)Dep.m_FilePath );
            
            if( FALSE == xbmp_Load( Bitmap, FilePath ) )
                x_throw( "ERROR: Unable to find the source file [%s]", (const char*)FilePath );
            
            if( xbitmap::FORMAT_XCOLOR   != Bitmap.getFormat() &&
                xbitmap::FORMAT_R8G8B8U8 != Bitmap.getFormat() )
                x_throw( "ERROR: We found an asset bitmap with a non-standard RGBA format [%s]", (const char*)FilePath );
            
            
            //
            // Make sure that we have all the information that we require for the alpha
            //
            Bitmap.ComputeHasAlphaInfo();
        }
    }

    //
    // Ok start Building for the right platforms
    //
    x_inline_light_jobs_block<16> JobBlock;
    
    for( platform& Plaform : m_Target )
    {
        if( !Plaform.m_bValid )
            continue;
        
        switch( Plaform.m_Platform )
        {
            case X_PLATFORM_IOS : JobBlock.SubmitJob( [this](){ HandleIOS();} ); break;
            case X_PLATFORM_PC:   JobBlock.SubmitJob( [this]( ){ HandleOSXAndPC( X_PLATFORM_PC  ); } ); break;
            case X_PLATFORM_OSX:  JobBlock.SubmitJob( [this]( ){ HandleOSXAndPC( X_PLATFORM_OSX ); } ); break;
            default: x_throw( "ERROR: We can not compile for [%s]", x_PlatformString( Plaform.m_Platform ) );
        }
    }
    
    // OK ready to start processing all platform
    JobBlock.FinishJobs();
}


