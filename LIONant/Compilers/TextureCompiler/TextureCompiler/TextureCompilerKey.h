//
//  TextureCompilerKey.h
//  TextureCompiler
//
//  Created by Tomas Arce on 9/14/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef TEXTURE_COMPILER_KEY_H
#define TEXTURE_COMPILER_KEY_H

#include "eng_base.h"

class texture_compiler_key : public compiler_key_object
{
public:
    
    enum color_type
    {
        COLOR_TYPE_COLOR,
        COLOR_TYPE_NORMALMAP,
        COLOR_TYPE_COUNT
    };
    
    enum tiling_mode
    {
        TILEMODE_CLAMP,
        TILEMODE_TILE,
        TILEMODE_MIRROR,
        TILEMODE_COUNT
    };
    
    enum alpha_mode
    {
        ALPHA_DISABLE,
        ALPHA_USE_SRC,
        ALPHA_FORCE_ONE_BIT,
        ALPHA_COUNT
    };

    enum texture_filter
    {
        FILTER_MODE_NULL,
        FILTER_MODE_NEAREST,
        FILTER_MODE_LINEAR,
        FILTER_MODE_BILINEAR,
        FILTER_MODE_TRILINEAR ,
        FILTER_MODE_COUNT
    };

    enum color_space
    {
        COLOR_SPACE_LINEAR,
        COLOR_SPACE_GAMMA,
        COLOR_SPACE_COUNT
    };
    
    struct entry_params : public entry_params_base {};
    struct main_params : public main_params_base_link<main_params>
    {
        enum param_flags
        {
            OVERWRITE_COLOR_TYPE        = X_BIT(0),
            OVERWRITE_COMPRESSION_LEVEL = X_BIT(1),
            OVERWRITE_ALPHA_MODE        = X_BIT(2),
            OVERWRITE_N_FRAMES          = X_BIT(3),
            OVERWRITE_X_SCALE           = X_BIT(4),
            OVERWRITE_Y_SCALE           = X_BIT(5),
            OVERWRITE_CREATE_MIPS       = X_BIT(6),
            OVERWRITE_U_TILE_MODE       = X_BIT(7),
            OVERWRITE_V_TILE_MODE       = X_BIT(8),
            OVERWRITE_W_TILE_MODE       = X_BIT(9),
            OVERWRITE_FILENAME          = X_BIT(10),
            OVERWRITE_FILTER_MIG        = X_BIT(11),
            OVERWRITE_FILTER_MAG        = X_BIT(12),
            OVERWRITE_TEXTURE_FILTER    = X_BIT(13),
            OVERWRITE_COLOR_SPACE       = X_BIT(14),
        };


        virtual void            onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final ;
        virtual xbool           onPropQuery         ( xproperty_query& Query ) override final;
                void            onUpdateFromSrc     ( const main_params& Src, const u64 Masks );

        using enum_entry = const xprop_enum::entry; 

        static enum_entry           m_ColorTypeEnum     [ COLOR_TYPE_COUNT ];
        static enum_entry           m_AlphaEnum         [ ALPHA_COUNT ];
        static enum_entry           m_TileEnum          [ TILEMODE_COUNT ];
        static enum_entry           m_FilterEnum        [ eng_texture::FILTER_MINMAG_COUNT ];
        static enum_entry           m_SimpleFilterEnum  [ FILTER_MODE_COUNT ];
        static enum_entry           m_ColorSpaceEnum    [ COLOR_SPACE_COUNT ];


        color_type                  m_ColorType         = COLOR_TYPE_COLOR;     // Is this a normal map? This is a compression hint
        xstring                     m_SrcFileName       = X_STR("");            // Rules about the source file names
                                                                                //  * If it is a CubeMap the file names should be,SRC_"XYZ" such,
                                                                                //    P = Positive and N = Negative
                                                                                //    SRC_PPP SRC_PPN SRC_PNP SRC_PNN SRC_NPP SRC_NPN SRC_NNP SRC_NNN
                                                                                //    The source file should be the one with all the Ps
                                                                                //  * If an Animation then SRC_000 should be the source file.
        f32                         m_CompressionLevel  = 0.5;                  // [0-1] Where 0 is max quality and 1 is Maximun compression. 0.5 is the valance.
        alpha_mode                  m_AlphaMode         = ALPHA_USE_SRC;        // Basically over rides the fact that the source texture may have alpha
        s32                         m_nFrames           = 1;                    // Tells if the source texture that it is an animation and its frame count
        xvector2                    m_Scale             = {1,1};                // Rescales the bitmap to be a different dimension
        xbool                       m_bCreateMips       = TRUE;                 // Tells the system if it should create mip maps
        tiling_mode                 m_UTilingMode       = TILEMODE_CLAMP;
        tiling_mode                 m_VTilingMode       = TILEMODE_CLAMP;
        tiling_mode                 m_WTilingMode       = TILEMODE_CLAMP;
        eng_texture::filter_mode    m_FilterMig         = eng_texture::FILTER_MINMAG_LINEAR;
        eng_texture::filter_mode    m_FilterMag         = eng_texture::FILTER_MINMAG_LINEAR;
        texture_filter              m_TextureFilter     = FILTER_MODE_NULL;
        color_space                 m_ColorSpace        = COLOR_SPACE_LINEAR;
    };
    
public:
    
                                texture_compiler_key        ( const compiler_base& Base ) : compiler_key_object( Base ) {}
    const xprop_enum::entry&    getTileEnum                 ( tiling_mode Mode ) const                  { return main_params::m_TileEnum[Mode];   }
    const xprop_enum::entry&    getFilterEnum               ( eng_texture::filter_mode  Mode ) const    { return main_params::m_FilterEnum[Mode]; }

    virtual const char*         getCompilerName             ( void ) const          { return "TextureCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const          { return "texture";         }

protected:
    
    KEY_STANDARD_STUFF

    virtual void                getAdditionalDependencies   ( xarray2<dependency>& List ) const override;

protected:
    
    friend class texture_compiler;
};

#endif
