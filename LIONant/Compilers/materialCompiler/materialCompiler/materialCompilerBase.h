//
//  fontBaseCompiler.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef MATERIAL_COMPILER_BASE_H
#define MATERIAL_COMPILER_BASE_H

#include "materialCompilerKey.h"
#include "eng_base.h"
#include "shaderCompilerKey.h"

extern compiler_base* g_pCompilerBase;

class material_compiler_base : public compiler_base
{
public:
    
                                        material_compiler_base  ( void ) : m_Key( *this ) {}
                                       ~material_compiler_base  ( void ){}
    virtual void                        onCompile               ( void );
    virtual compiler_key_object&        getKeyObject            ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject            ( void ) const { return m_Key;             }
    

protected:

    struct shader_key
    {
        shader_key( void ) : m_ShaderKey( *g_pCompilerBase ) {}
        xstring                                     m_Path;
        shader_compiler_key                         m_ShaderKey;
    };
    
    struct compilation_data
    {
        xarray2<shader_key>                         m_lShaderKey;
        xptr2<xsafe_array<s32,3>>                   m_lEntryRedirect;
        xarray2<eng_material_rsc::system_cmds>      m_lCmd;
        eng_material_rsc::material_rsc              m_MaterialRSC;
        xarray2<eng_material_rsc::technique>        m_lTechnique;
    };
    
protected:

    void                                CompilationUniqueness   ( void );
    void                                CompileKeyPlatform      ( compilation_data& CompilationData, const xplatform iPlatform ) const;
    void                                ExportResource          ( const compilation_data& CompilationData, const platform& Target ) const;
    void                                CompileShaders          ( const xplatform iPlatform ) const;

protected:
    
    material_compiler_key                           m_Key;
    xbool                                           m_bSingleCompilation;
    xsafe_array<compilation_data, X_PLATFORM_COUNT> m_lCompilationData;
};


#endif
