//
//  main.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "compilerBase.h"
#include "materialCompilerBase.h"

// -BUILDTYPE DEBUG -PROJECT "C:/RationWorls/LIONant/xReference/Data" -TARGET PC -INPUT "GameData/CompilerKeys/DiffuseBump--6fmo_ycwi_4pi7.material"
// -OVERWRITE_DEFAULTS -PROJECT "C:/RationWorls/LIONant/xReference/Data"

int main(int argc, const char * argv[])
{
    material_compiler_base Compiler;
    
    if( Compiler.Parse( argc, argv ) )
    {
         Compiler.Compile();
    }
    
    return 0;
}
