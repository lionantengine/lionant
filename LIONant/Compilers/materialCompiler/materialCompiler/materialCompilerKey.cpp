//
//  materialCompilerKeyObject.cpp
//  materialCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "materialCompilerKey.h"

const xprop_enum::entry material_compiler_key::m_pTechniqueTypeEnum[ ] =
{
    { tech_type::TECH_TYPE_RIGID,               X_STR( "RIGID" )                },
    { tech_type::TECH_TYPE_MATRIX_SKIN,         X_STR( "MATRIX_SKIN" )          },
    { tech_type::TECH_TYPE_DQUATERNION_SKIN,    X_STR( "DQUATERNION_SKIN" )     },
};

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void material_compiler_key::entry_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM(      "TechniqueType",        OVERRIDE_TECHNIQUE_TYPE,    g_PropEnum,    "This descrives the category of the material usually given by the vertex shader" )  
    KEY_PROP_ENUM(      "nSkinWeights",         OVERRIDE_SKIN_WEIGHTS,      g_PropInt,     "From 0 to 4 This descrives the category of the material usually given by the vertex shader" )  
    KEY_PROP_ENUM_FILE( "VertexShaderKey",      OVERRIDE_VERTEX_SHADER,                    "Resource Name for the Vertex Shader", "*.shader.txt" )  
    KEY_PROP_ENUM_FILE( "FragmentShaderKey",    OVERRIDE_FRAGMENT_SHADER,                  "Resource Name for the Fragment Shader", "*.shader.txt" )  
    KEY_PROP_ENUM_FILE( "GeometryShaderKey",    OVERRIDE_GEOMETRY_SHADER,                  "Resource Name for the Geometry Shader", "*.shader.txt" )  
}

//-------------------------------------------------------------------------------------------------

xbool material_compiler_key::entry_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "TechniqueType",        OVERRIDE_TECHNIQUE_TYPE,    g_PropEnum.Query( Query, m_TechniqueType, m_pTechniqueTypeEnum ) )
    KEY_PROP_QUERY( "nSkinWeights",         OVERRIDE_SKIN_WEIGHTS,      g_PropInt.Query( Query, m_nWeights, 0, eng_material_rsc::WEIGHTS_ENUM_COUNT ) )
    KEY_PROP_QUERY( "VertexShaderKey",      OVERRIDE_VERTEX_SHADER,     g_PropFilePath.Query( Query, m_ShaderKey[0] ) )
    KEY_PROP_QUERY( "FragmentShaderKey",    OVERRIDE_FRAGMENT_SHADER,   g_PropFilePath.Query( Query, m_ShaderKey[1] ) )
    KEY_PROP_QUERY( "GeometryShaderKey",    OVERRIDE_GEOMETRY_SHADER,   g_PropFilePath.Query( Query, m_ShaderKey[2] ) )

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void material_compiler_key::entry_params::onUpdateFromSrc( const entry_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_TechniqueType,   OVERRIDE_TECHNIQUE_TYPE,    =       )
    KEY_PROP_UPDATE( m_nWeights,        OVERRIDE_SKIN_WEIGHTS,      =       )
    KEY_PROP_UPDATE( m_ShaderKey[0],    OVERRIDE_VERTEX_SHADER,     .Copy   )
    KEY_PROP_UPDATE( m_ShaderKey[1],    OVERRIDE_FRAGMENT_SHADER,   .Copy   )
    KEY_PROP_UPDATE( m_ShaderKey[2],    OVERRIDE_GEOMETRY_SHADER,   .Copy   )
}

//-------------------------------------------------------------------------------------------------

void material_compiler_key::getAdditionalDependencies( xarray2<dependency>& List ) const
{
    // Make sure that all the entries have this set to truth
    mutable_ref( rlList, List );
    for( auto& Dep : rlList )
    {
         Dep.m_bForceKeyBuild = TRUE;
    }
}