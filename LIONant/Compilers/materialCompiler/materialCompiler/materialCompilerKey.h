//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef MATERIAL_COMPILER_KEY_OBJECT_H
#define MATERIAL_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"
#include "eng_base.h"

class material_compiler_key : public compiler_key_object
{
public:
        
    using   tech_type   = eng_material_rsc::technique_type ;

    struct main_params : public main_params_base {};
    struct entry_params : public entry_params_base_link<entry_params>
    {
        enum param_flags
        {
            OVERRIDE_TECHNIQUE_TYPE             = X_BIT( 0 ),
            OVERRIDE_SKIN_WEIGHTS               = X_BIT( 1 ),
            OVERRIDE_VERTEX_SHADER              = X_BIT( 2 ),
            OVERRIDE_FRAGMENT_SHADER            = X_BIT( 3 ),
            OVERRIDE_GEOMETRY_SHADER            = X_BIT( 4 ),

            SINGLE_COMPILATION_FLAGS            = 0
                                                  | OVERRIDE_TECHNIQUE_TYPE  
                                                  | OVERRIDE_VERTEX_SHADER   
                                                  | OVERRIDE_FRAGMENT_SHADER 
                                                  | OVERRIDE_GEOMETRY_SHADER
                                                  | OVERRIDE_SKIN_WEIGHTS
        };

                entry_params    ( void ) { for( auto& ShaderKey : m_ShaderKey ) ShaderKey =X_STR(""); }
        void    onPropEnum      ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const;
        xbool   onPropQuery     ( xproperty_query& Query );
        void    onUpdateFromSrc ( const entry_params& Src, const u64 Masks );

        tech_type                           m_TechniqueType     = tech_type::TECH_TYPE_RIGID;
        s32                                 m_nWeights          = 0;
        xsafe_array<xstring,3>              m_ShaderKey;
    };

public:
    
                                material_compiler_key       ( compiler_base& Base ) : compiler_key_object( Base ) {}
    virtual const char*         getCompilerName             ( void ) const { return "MaterialCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const { return "material";         }
    virtual void                getAdditionalDependencies   ( xarray2<dependency>& List ) const;
        
public:

    static const xprop_enum::entry m_pTechniqueTypeEnum[];

protected:

    KEY_STANDARD_STUFF    
    
protected:
    
    friend class material_compiler_base;
    friend class informed_material_compiler_base;
};

#endif
