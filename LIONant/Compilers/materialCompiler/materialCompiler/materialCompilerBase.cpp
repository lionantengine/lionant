//
//  fontBaseCompiler.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "materialCompilerBase.h"


#pragma comment(lib, "OpenGL32.lib")


//-------------------------------------------------------------------------------------------------

void material_compiler_base::CompilationUniqueness( void )
{
    m_bSingleCompilation = TRUE;
    
    //
    // Lets go throught the key platforms
    //
    for ( s32 j = 0; j<m_Target.getCount( ); j++ )
    {
        // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
        if ( !m_Target[ j ].m_bValid )
            continue;

        const_ref( rKeyEntry, m_Key.m_lEntry );
        for ( s32 e = 0; e < m_Key.m_lEntry.getCount(); e++ )
        {
            const material_compiler_key::entry_params& Params = rKeyEntry[e][ j ];

            if ( x_FlagIsOn( Params.m_OverrideBits, material_compiler_key::entry_params::SINGLE_COMPILATION_FLAGS ) )
            {
                m_bSingleCompilation = FALSE;
                return;
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------

void material_compiler_base::CompileKeyPlatform( compilation_data& CompilationData, const xplatform iPlatform ) const
{
    x_inline_light_jobs_block<8> PipeA;
    x_inline_light_jobs_block<8> PipeB;
    
    //
    // Check if we can do anything
    //
    const s32 nTechniques = m_Key.m_lEntry.getCount(); 
    if ( nTechniques <= 0 )
        x_throw( "ERROR: Unable to compile anything since there is not data" );

    //
    // Set all the entries to the technique equal to -1
    //
    x_memset( &CompilationData.m_MaterialRSC.m_TechType, 0xff, sizeof(CompilationData.m_MaterialRSC.m_TechType) );

    //
    // Allocate all the redirects at front
    //
    CompilationData.m_lEntryRedirect.New( m_Key.m_lEntry.getCount() );

    //
    // Create a unique list for all the shaders 
    //
    PipeA.SubmitJob( [&]()
    {
        mutable_ref( rShaderKey, CompilationData.m_lShaderKey );
        mutable_ref( rRedirect, CompilationData.m_lEntryRedirect );
        const_ref( rKeyEntry, m_Key.m_lEntry );
        for( s32 e = 0; e<nTechniques; e++ )
        {
            const material_compiler_key::entry_params&  Entry = rKeyEntry[ e ][ iPlatform ];

            rRedirect[ e ][0] = -1;
            rRedirect[ e ][1] = -1;
            rRedirect[ e ][2] = -1;

            for ( const xstring& ShaderName : Entry.m_ShaderKey )
            {
                const s32 iShader = s32( &ShaderName - &Entry.m_ShaderKey[0] );

                if ( ShaderName.IsEmpty() )
                {
                    if( iShader < 2 )
                    {
                        x_throw("ERROR: We could not find a %s shader for technique [%d] With Weights [%d]",
                            iShader?"VERTEX":"FRAGMENT",
                            e,
                            Entry.m_nWeights );
                    }
                    else
                    {
                        continue;
                    }
                }

                //
                // If we dont find it in our list the add it
                // 
                for ( shader_key& Key : rShaderKey )
                {
                    if( Key.m_Path == ShaderName )
                        break;
                }

                if ( rShaderKey.isIteratorAtEnd() )
                    rShaderKey->append( rRedirect[ e ][iShader] ).m_Path = ShaderName;
                else 
                    rRedirect[ e ][ iShader ] = rShaderKey.getIterator();
            }
        }
    
        CompilationData.m_lEntryRedirect.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
    } );

    //
    // Collect all the shader guids
    // Set the technique types
    // add dependencies
    //
    PipeB.SubmitJob( [&]()
    {
        mutable_ref( rTechnique, CompilationData.m_lTechnique );
        const_ref( rKeyEntry, m_Key.m_lEntry );
        for( s32 e = 0; e<nTechniques; e++ )
        {
            const material_compiler_key::entry_params&  Entry       = rKeyEntry[ e ][ iPlatform ];
            eng_material_rsc::technique&                Technique   = rTechnique->append();

            auto FindGuid = []( const xstring& String, const char* pErroString ) -> u64
            {
                if ( String.IsEmpty() )
                    return 0;

                const char* pStr = x_strstr( String, "--" );
                if ( pStr == NULL )
                    x_throw( "ERROR: Unable to locate the guid for the %s shader [%s]", 
                    pErroString,
                    (const char*)String );

                char Buffer[16];
                x_strncpy( Buffer, &pStr[2], 14, 16 );

                xguid Guid;
                Guid.SetFromAlphaString( Buffer );

                if ( 0==(Guid.m_Guid&1) )
                    x_throw( "ERROR: %s shader guid fail the even number test [%s]",
                        pErroString,
                        (const char*)String );

                return Guid.m_Guid;
            };
         
            //
            // Find all the guids
            //        
            Technique.m_gShader[0] = FindGuid( Entry.m_ShaderKey[0], "vertex" );
            Technique.m_gShader[1] = FindGuid( Entry.m_ShaderKey[1], "freagment" );
            Technique.m_gShader[2] = FindGuid( Entry.m_ShaderKey[2], "geometry" );

            //
            // Add external dependencies
            // 
            if ( FALSE == Entry.m_ShaderKey[0].IsEmpty() ) AddExternalDependencyKey( Entry.m_ShaderKey[0] );
            if ( FALSE == Entry.m_ShaderKey[1].IsEmpty() ) AddExternalDependencyKey( Entry.m_ShaderKey[1] );
            if ( FALSE == Entry.m_ShaderKey[2].IsEmpty() ) AddExternalDependencyKey( Entry.m_ShaderKey[2] );

            if ( Technique.m_gShader[0] == Technique.m_gShader[1] ||
                 Technique.m_gShader[1] == Technique.m_gShader[2] ||
                 Technique.m_gShader[0] == Technique.m_gShader[2] )
                 x_throw( "ERROR: Shaders can not have the same GUID" );

            //
            // Set the technique types
            // 
            CompilationData.m_MaterialRSC.m_TechType[ Entry.m_TechniqueType ][ Entry.m_nWeights ].m_iTechnique = e;
        }
    });


    //
    // Load all the shaders
    //
    PipeA.FinishJobs();
    {
        mutable_ref( rShaderKey, CompilationData.m_lShaderKey );
        for ( auto& ShaderKey : rShaderKey ) PipeA.SubmitJob( [ this, &ShaderKey ]()
        {
            xstring             KeyPath;

            KeyPath.Format( "%s/%s.txt", 
                (const char*)m_ProjectPath, 
                (const char*)ShaderKey.m_Path );

            ShaderKey.m_ShaderKey.LoadDefaults( m_ProjectPath );
            ShaderKey.m_ShaderKey.Load( KeyPath );
            
        } );
    }

    PipeA.FinishJobs();
    {
        CompilationData.m_lShaderKey.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
        const_ref( rShaderKey, CompilationData.m_lShaderKey );
        for ( auto& ShaderKey : rShaderKey ) 
        {
            ShaderKey.m_ShaderKey.m_lEntry.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
            ShaderKey.m_ShaderKey.m_Main.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
        }
    }

    //
    // Compile all the system commands
    //
    PipeA.SubmitJob( [&]()
    {
        const_ref( rRedirect,   CompilationData.m_lEntryRedirect );
        const_ref( rShaderKey,  CompilationData.m_lShaderKey );
        const_ref( rKeyEntry,   m_Key.m_lEntry)
        for ( auto& Redirect : rRedirect )
        {
            const auto& Entry       = rKeyEntry[ rRedirect.getIterator() ][ iPlatform ];
            auto&       TechType    = CompilationData.m_MaterialRSC.m_TechType[ Entry.m_TechniqueType ][ Entry.m_nWeights ];

            // Set the begging of this technique commands
            TechType.m_iTechnique = rRedirect.getIterator();
            TechType.m_iSystemCmd = CompilationData.m_lCmd.getCount();

            // Compute the offsets (this will be used to computer the shader program registers)
            xsafe_array<s32,3> Offset;
            {
                auto& VShaderKey = rShaderKey[ Redirect[0] ].m_ShaderKey;
                auto& FShaderKey = rShaderKey[ Redirect[1] ].m_ShaderKey;

                const_ref( rFShaderKey, FShaderKey.m_Main );
                const_ref( rVShaderKey, VShaderKey.m_Main );

                Offset[1] = 0;
                Offset[0] = rFShaderKey[ iPlatform ].m_lUniformDef.getCount();
                Offset[2] = Offset[0] + rVShaderKey[ iPlatform ].m_lUniformDef.getCount();
            }

            // Fill the commands
            mutable_ref( rCmd, CompilationData.m_lCmd );
            for ( s32 i = 0; i < Offset.getCount(); i++ )
            {
                if ( Redirect[i] < 0 )
                    continue;

                auto& ShaderKey = rShaderKey[ Redirect[i] ].m_ShaderKey;

                const_ref( rShaderkeyMain, ShaderKey.m_Main );
                for ( auto& Uniform : rShaderkeyMain[ iPlatform ].m_lUniformDef )
                {
                    const s32 Index = s32( &Uniform - &rShaderkeyMain[ iPlatform ].m_lUniformDef[0] );
                
                    if( Uniform.m_Type < eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                        continue;

                    auto& Cmd = rCmd->append();
                    Cmd.m_iRegister = Offset[i] + Index;
                    Cmd.m_Type      = Uniform.m_Type;
                }

                // Set the total number of commands created
                TechType.m_nSystemCmds = CompilationData.m_lCmd.getCount() - TechType.m_iSystemCmd;
            }
        }
    });

    //
    // Make sure that all uniforms for all the techniques have the same non-system uniforms
    //
    PipeA.SubmitJob( [&]()
    {
        const_ref( rRedirect, CompilationData.m_lEntryRedirect );
        const_ref( rShaderKeys, CompilationData.m_lShaderKey );
        const_ref( rKeyEntry, m_Key.m_lEntry );
        for ( s32 e0 = 0; e0 < nTechniques; e0++ )
        {
//            const material_compiler_key::entry_params&  Entry       = rKeyEntry[ e0 ][ iPlatform ];
            const shader_compiler_key* const pShader0[] = 
            { 
                (rRedirect[e0][0]<0) ? NULL : &rShaderKeys[ rRedirect[e0][0] ].m_ShaderKey, 
                (rRedirect[e0][1]<0) ? NULL : &rShaderKeys[ rRedirect[e0][1] ].m_ShaderKey, 
                (rRedirect[e0][2]<0) ? NULL : &rShaderKeys[ rRedirect[e0][2] ].m_ShaderKey 
            };

            for ( s32 i = 0; i < 3; i++ )
            {
                if ( pShader0[ i ] == NULL )
                    continue;

                const_ref( rShader0Main, pShader0[ i ]->m_Main );
                const shader_compiler_key::main_params&     Shader0Main  = rShader0Main[ iPlatform ];

                for ( s32 e1 = e0+1; e1 < nTechniques; e1++ )
                {
                    const shader_compiler_key* const pShader1[] = 
                    { 
                        (rRedirect[e1][0]<0) ? NULL : &rShaderKeys[ rRedirect[e1][0] ].m_ShaderKey, 
                        (rRedirect[e1][1]<0) ? NULL : &rShaderKeys[ rRedirect[e1][1] ].m_ShaderKey, 
                        (rRedirect[e1][2]<0) ? NULL : &rShaderKeys[ rRedirect[e1][2] ].m_ShaderKey 
                    };
                    const_ref( rShader1Main, pShader1[ i ]->m_Main );
                    const shader_compiler_key::main_params&     Shader1Main  = rShader1Main[ iPlatform ];

                    //
                    // Make sure that all the shader0 uniforms are in shader1 uniform
                    //
                    for ( const shader_compiler_key::uniform& Uniform0 : Shader0Main.m_lUniformDef )
                    {
                        if ( Uniform0.m_Type >= eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                            continue;

                        xbool bFounded = FALSE; 
                        for ( const shader_compiler_key::uniform& Uniform1 : Shader1Main.m_lUniformDef )
                        {
                            if ( Uniform0.m_Name == Uniform1.m_Name  &&
                                 Uniform0.m_Type == Uniform1.m_Type  &&
                                 Uniform0.m_UID  == Uniform1.m_UID )
                            {
                                bFounded = TRUE;
                                break;
                            }
                        }

                        if ( bFounded == FALSE )
                        {
                            const material_compiler_key::entry_params& Params0 =  rKeyEntry[e0][iPlatform];
                            const material_compiler_key::entry_params& Params1 =  rKeyEntry[e1][iPlatform];
                            x_throw( "ERROR: Unable to find [%s] uniform from technique [%s] in technique [%s]. All techniques have to have the same non-system uniforms",
                                (const char*) Uniform0.m_Name,
                                (const char*) m_Key.m_pTechniqueTypeEnum[ Params0.m_TechniqueType ].m_EnumName,
                                (const char*) m_Key.m_pTechniqueTypeEnum[ Params1.m_TechniqueType ].m_EnumName );
                        }
                    }

                    //
                    // Make sure that all the shader1 uniforms are in shader0 uniform
                    //
                    for ( const shader_compiler_key::uniform& Uniform1 : Shader0Main.m_lUniformDef )
                    {
                        if ( Uniform1.m_Type >= eng_shader_program::UNIFORM_TYPE_SYSTEM_BASE )
                            continue;

                        xbool bFounded = FALSE; 
                        for ( const shader_compiler_key::uniform& Uniform0 : Shader1Main.m_lUniformDef )
                        {
                            if ( Uniform0.m_Name == Uniform1.m_Name  &&
                                 Uniform0.m_Type == Uniform1.m_Type  &&
                                 Uniform0.m_UID  == Uniform1.m_UID )
                            {
                                bFounded = TRUE;
                                break;
                            }
                        }

                        if ( bFounded == FALSE )
                        {
                            const material_compiler_key::entry_params& Params0 =  rKeyEntry[e0][iPlatform];
                            const material_compiler_key::entry_params& Params1 =  rKeyEntry[e1][iPlatform];
                            x_throw( "ERROR: Unable to find [%s] uniform from technique [%s] in technique [%s]. All techniques have to have the same non-system uniforms",
                                (const char*) Uniform1.m_Name,
                                (const char*) m_Key.m_pTechniqueTypeEnum[ Params1.m_TechniqueType ].m_EnumName,
                                (const char*) m_Key.m_pTechniqueTypeEnum[ Params0.m_TechniqueType ].m_EnumName );
                        }
                    }
                }
            }
        }
    } );

    //
    // Lets make sure that the unique ID for each of the uniforms dont collide between shaders
    //
    PipeA.SubmitJob( [&]()
    {
        const_ref( rRedirect, CompilationData.m_lEntryRedirect );
        const_ref( rShaderKeys, CompilationData.m_lShaderKey );
        const_ref( rKeyEntry, m_Key.m_lEntry );
        for ( s32 e = 0; e < nTechniques; e++ ) 
        {
            const material_compiler_key::entry_params&  Entry       = rKeyEntry[ e ][ iPlatform ];
            xarray2<eng_vertex_desc::attribute_link>    lAttrLinks;
            const shader_compiler_key* const pShader[] = 
            { 
                (rRedirect[e][0]<0) ? NULL : &rShaderKeys[ rRedirect[e][0] ].m_ShaderKey, 
                (rRedirect[e][1]<0) ? NULL : &rShaderKeys[ rRedirect[e][1] ].m_ShaderKey, 
                (rRedirect[e][2]<0) ? NULL : &rShaderKeys[ rRedirect[e][2] ].m_ShaderKey 
            };

            const_ref( rShader0Main, pShader[0]->m_Main );
            const_ref( rShader1Main, pShader[1]->m_Main );

            //
            // Lets make sure that the unique ID for each of the uniforms dont collide between shaders
            //
            for ( const shader_compiler_key::uniform& Uniform1 : rShader0Main[ iPlatform ].m_lUniformDef )
            for ( const shader_compiler_key::uniform& Uniform2 : rShader1Main[ iPlatform ].m_lUniformDef )
            {
                if ( Uniform1.m_UID == Uniform2.m_UID )
                {
                    ASSERT( m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumID == Entry.m_TechniqueType );
                    x_throw( "ERROR: Found a collision of unique IDs between the vertex shader and the pixel shader. TechniqueType[%s], ID[%d]",
                    (const char*)m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumName,
                    Uniform1.m_UID );
                }
            }

            if( pShader[2] )
            {
                const_ref( rShader2Main, pShader[2]->m_Main );

                for ( const shader_compiler_key::uniform& Uniform1 : rShader0Main[ iPlatform ].m_lUniformDef )
                for ( const shader_compiler_key::uniform& Uniform2 : rShader2Main[ iPlatform ].m_lUniformDef )
                {
                    if ( Uniform1.m_UID == Uniform2.m_UID )
                    {   
                        ASSERT( m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumID == Entry.m_TechniqueType );
                        x_throw( "ERROR: Found a collision of unique IDs between the vertex shader and the geometry shader. Technique[%s], ID[%d]",
                        (const char*)m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumName,
                        Uniform1.m_UID );
                    }
                }

                for ( const shader_compiler_key::uniform& Uniform1 : rShader1Main[ iPlatform ].m_lUniformDef )
                for ( const shader_compiler_key::uniform& Uniform2 : rShader2Main[ iPlatform ].m_lUniformDef )
                {
                    if ( Uniform1.m_UID == Uniform2.m_UID )
                    {
                        ASSERT( m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumID == Entry.m_TechniqueType );
                        x_throw( "ERROR: Found a collision of unique IDs between the fragment shader and the geometry shader. Technique[%s], ID[%d]",
                        (const char*)m_Key.m_pTechniqueTypeEnum[ Entry.m_TechniqueType ].m_EnumName,
                        Uniform1.m_UID );
                    }
                }
            }
        }
    });


    //
    // Link pending pointers
    //
    PipeA.FinishJobs();
    PipeB.FinishJobs();
    {
        mutable_ref( rTechnique, CompilationData.m_lTechnique );
        mutable_ref( rCmd, CompilationData.m_lCmd );
        CompilationData.m_MaterialRSC.m_nTechniques         = CompilationData.m_lTechnique.getCount();
        CompilationData.m_MaterialRSC.m_pTechnique.m_Ptr    = &rTechnique[0];
        CompilationData.m_MaterialRSC.m_nSystemCmds         = CompilationData.m_lCmd.getCount();
        CompilationData.m_MaterialRSC.m_pSystemCmd.m_Ptr    = &rCmd[0];
    }

    CompilationData.m_lTechnique.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
    CompilationData.m_lCmd.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
}


//-------------------------------------------------------------------------------------------------

void material_compiler_base::ExportResource( const compilation_data& CompilationData, const platform& Target ) const
{
    //
    // Ok Ready to save everything then
    //
    xserialfile     SerialFile;
    xstring         FinalRscPath;
    
    FinalRscPath = getFinalResourceName( Target );
    
    SerialFile.Save( (const char*)FinalRscPath, CompilationData.m_MaterialRSC, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
    
    // for debugging
    if( (1) )
    {
        xserialfile                     test;
        eng_material_rsc::material_rsc* pMyMaterial;
        test.Load( (const char*)FinalRscPath, pMyMaterial );
        
        ASSERT( pMyMaterial );
        x_delete( pMyMaterial );
    }
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
#ifdef TARGET_PC

//-------------------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
  switch (uMsg){
  case WM_DESTROY:
    PostQuitMessage(0);
    break;
  default:
    break;
  }
  return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

//-------------------------------------------------------------------------------------------------
static HDC     s_hDC = NULL;
static HGLRC   s_hRC = NULL;
static HWND    s_hWnd = NULL;

xbool InitOpenGL( void )
{
    HINSTANCE hInstance;

    hInstance = GetModuleHandle(NULL);  // Grab An Instance For Our Window

    const char* class_name = "DUMMY_CLASS";
 
    WNDCLASSEX wx;
    ZeroMemory(&wx, sizeof(WNDCLASSEX));
    wx.cbSize           = sizeof(WNDCLASSEX);
    wx.style            = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wx.lpfnWndProc      = WindowProc;
    wx.hInstance        = hInstance;
    wx.hCursor          = LoadCursor(nullptr, IDC_ARROW);
    wx.lpszClassName    = class_name;
    if( !RegisterClassEx(&wx) ) 
        return FALSE;

    s_hWnd = CreateWindowEx( 0, class_name, "dummy_name", 0, 0, 0, 0, 0, NULL, NULL, hInstance, NULL );
    if ( s_hWnd ==0  ) 
        return FALSE;

    if (!(s_hDC = GetDC(s_hWnd))) {
        return FALSE;
    }
    
    // pixel format
    PIXELFORMATDESCRIPTOR pfd;
    x_memset(&pfd, 0, sizeof(pfd));
	pfd.nSize = sizeof( pfd );
	pfd.nVersion        = 1;
	pfd.dwFlags         = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType      = PFD_TYPE_RGBA;
	pfd.cColorBits      = 24;
	pfd.cDepthBits      = 16;
	pfd.iLayerType      = PFD_MAIN_PLANE;

    const s32 pf  = ChoosePixelFormat(s_hDC, &pfd);
    const s32 res = SetPixelFormat(s_hDC, pf, &pfd);

    if ( pf == 0 || res == 0 )
        return FALSE;

   // DescribePixelFormat(s_hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

    if (!(s_hRC = wglCreateContext(s_hDC))) {
        return FALSE;
    }

    if(!wglMakeCurrent(s_hDC,s_hRC)) { 
        return FALSE;
    }

    glewInit();

    return TRUE;
}

//-------------------------------------------------------------------------------------------------

void KillOpenGL( void )
{
    wglMakeCurrent(NULL, NULL);         // make our context not current 
    wglDeleteContext(s_hRC);            // delete the rendering context 
    ReleaseDC(s_hWnd, s_hDC);           // release handle to DC 
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#else

//-------------------------------------------------------------------------------------------------

xbool InitOpenGL( void )
{
    static const int SCREEN_WIDTH   = 1024;
    static const int SCREEN_HEIGHT  = 768;
    
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init();
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);

    return TRUE;
}

//-------------------------------------------------------------------------------------------------

void KillOpenGL( void )
{
    eng_Kill();
}

#endif


//-------------------------------------------------------------------------------------------------

void material_compiler_base::CompileShaders( const xplatform iPlatform ) const
{

    //
    // Compile shaders to make sure everything is ok
    //
    const compilation_data& CompilationData = m_lCompilationData[ iPlatform ];
    {
        const_ref( rRedirect, CompilationData.m_lEntryRedirect );
        const_ref( rShaderKeys, CompilationData.m_lShaderKey );
        const_ref( rKeyEntry, m_Key.m_lEntry );
        for ( s32 e = 0; e < m_Key.m_lEntry.getCount(); e++ ) 
        {
//            const material_compiler_key::entry_params&  Entry       = rKeyEntry[ e ][ iPlatform ];
            xarray2<eng_vertex_desc::attribute_link>    lAttrLinks;
            const shader_compiler_key* const pShader[] = 
            { 
                (rRedirect[e][0]<0) ? NULL : &rShaderKeys[ rRedirect[e][0] ].m_ShaderKey, 
                (rRedirect[e][1]<0) ? NULL : &rShaderKeys[ rRedirect[e][1] ].m_ShaderKey, 
                (rRedirect[e][2]<0) ? NULL : &rShaderKeys[ rRedirect[e][2] ].m_ShaderKey 
            };

            //
            // Add all the attribute links
            //
            {
                mutable_ref( rAttrLinks, lAttrLinks );
                const_ref( rShader0Main, pShader[0]->m_Main );
                for ( const shader_compiler_key::vertex_attribute& SrcAttr : rShader0Main[iPlatform].m_lAttribute )
                {
                    eng_vertex_desc::attribute_link& DstAttr = rAttrLinks->append();
                        
                    DstAttr.m_pVarName.m_Ptr = &SrcAttr.m_Name[0];
                    DstAttr.m_UsageType      = SrcAttr.m_UsageType;
                }
                lAttrLinks.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
            }

            //
            // Build the proper shader
            //
            {
                eng_vshader                                 VShader;
                eng_gshader                                 GShader;
                eng_fshader                                 FShader;
                eng_shader_program                          Program;

                const_ref( rKeyVShaderMain, pShader[ 0 ]->m_Main );
                const_ref( rKeyFShaderMain, pShader[ 1 ]->m_Main );

                auto&                                       KeyVShader = rKeyVShaderMain[ iPlatform ];
                auto&                                       KeyFShader = rKeyFShaderMain[ iPlatform ];

                const_ref( rAttrLinks, lAttrLinks );
                VShader.LoadFromMemory(
                    &KeyVShader.m_ShaderFile[ 0 ],
                    KeyVShader.m_ShaderFile.GetLength(),
                    &rAttrLinks[ 0 ],
                    rAttrLinks.getCount(),
                    FALSE );

                FShader.LoadFromMemory(
                    &KeyFShader.m_ShaderFile[ 0 ],
                    KeyFShader.m_ShaderFile.GetLength() );

                if ( pShader[ 2 ] )
                {
                    const_ref( rKeyGShaderMain, pShader[ 2 ]->m_Main );
                    auto& KeyGShader = rKeyGShaderMain[ iPlatform ];

                    GShader.LoadFromMemory( &KeyGShader.m_ShaderFile[ 0 ],
                                            KeyGShader.m_ShaderFile.GetLength() );

                    Program.LinkShaders( VShader, GShader, FShader );
                }
                else
                {
                    Program.LinkShaders( VShader, FShader );
                }

                Program.Activate();

                //
                // Link all the system uniforms to test
                //
                {
                    const char* pShaderTypeName[ ] = { "Vertex", "Fragment", "Geometry" };
                    for ( s32 k = 0; k < 3; k++ )
                    {
                        if( pShader[ k ] == NULL )
                            continue;
                        
                        const_ref( rShaderMain, pShader[ k ]->m_Main );
                        auto& KeyShader = rShaderMain[ iPlatform ];
                        for ( s32 i = 0; i < KeyShader.m_lUniformDef.getCount(); i++ )
                        {
                            auto& Uniform = KeyShader.m_lUniformDef[i];

                            if ( Uniform.m_Type >= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA &&
                                 Uniform.m_Type <= eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA )
                            {
                                if( glGetUniformLocationARB( Program.getHandle(), Uniform.m_Name ) < 0 )
                                    x_throw( "ERROR: Fail to link Uniform Texture [%s] in %s Shader, Technique [%d]", 
                                        (const char*)Uniform.m_Name,
                                        pShaderTypeName[k],
                                        e );
                            }
                            else
                            {
                                if( glGetUniformLocationARB( Program.getHandle(), Uniform.m_Name ) < 0 )
                                    x_throw( "ERROR: Fail to link Uniform [%s] in %s shader, Technique [%d]", 
                                        (const char*)Uniform.m_Name,
                                        pShaderTypeName[k],
                                        e );
                            }
                        }
                    }
                }
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------
compiler_base* g_pCompilerBase = NULL;
void material_compiler_base::onCompile( void )
{
    //
    // Set global bar
    //
    g_pCompilerBase = this;
    
    //
    // Init OpenGL
    //
    if( InitOpenGL() == FALSE )
        x_throw( "ERROR: Failt to initialize OPENGL" );

    //
    // Allow the compiler to read only 
    //
    m_Key.m_lEntry.ChangeBehavior   ( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    m_Key.m_Main.ChangeBehavior     ( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );

    //
    // Determine Uniqueness of the compilation
    //
    CompilationUniqueness();

    //
    // Single compilation vs multiple
    //
    if( m_bSingleCompilation )
    {
        //
        // Prepare the data structures
        //
        CompileKeyPlatform( m_lCompilationData[0], xplatform(0) );

        //
        // Test the shaders
        //
        CompileShaders( xplatform(0) );

        //
        // Export Files
        //
        x_inline_light_jobs_block<8> Block;
        
        for( const platform& Target : m_Target )
        {
            if( Target.m_bValid == FALSE )
                continue;
            
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[0], Target);
            });
        }
        
        Block.FinishJobs();
    }
    else
    {
        x_inline_light_jobs_block<8> Block;
        
        //
        // Process for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;
            
            // Compile
            Block.SubmitJob( [this, &Target]()
            {
                CompileKeyPlatform( m_lCompilationData[Target.m_Platform], Target.m_Platform );
            });
        }
        
        Block.FinishJobs();

        //
        // Process for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;

            //
            // Test the shaders (this must run in the main thread)
            //
            CompileShaders( Target.m_Platform );

            //
            // Export
            //
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[ Target.m_Platform ], Target);
            });
        }
        Block.FinishJobs();
    }

    KillOpenGL();
}

