//
//  main.cpp
//  MeshCompiler
//
//  Created by Tomas Arce on 10/30/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "compilerBase.h"
#include "skeletonCompilerBase.h"

// -OVERWRITE_DEFAULTS -PROJECT "C:\RationWorls\LIONant\xReference\Data"
// -BUILDTYPE DEBUG -PROJECT "C:\RationWorls\LIONant\xReference\Data" -TARGET PC -INPUT "GameData\CompilerKeys\BaseMale--5dt3_y1j5_zvof.animation"

int main( int argc, const char * argv[ ] )
{
    skeleton_compiler_base Compiler;

    if ( Compiler.Parse( argc, argv ) )
    {
        Compiler.Compile( );
    }

    return 0;
}

