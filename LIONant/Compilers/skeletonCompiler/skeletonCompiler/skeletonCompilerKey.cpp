//
//  fontCompilerKeyObject.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "skeletonCompilerKey.h"


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void skeleton_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "FilePath",     OVERRIDE_FILEPATH,                      "Name of the file containg the skeleton. rawanim or a fbx file.", "*.fbx,*.rawanim" )
    KEY_PROP_ENUM     ( "bKeepBind",    OVERRIDE_KEEP_BINDING,  g_PropBool,     "Will bake the bind pouse into the keyframes which means less computations at runtime" )
    KEY_PROP_ENUM     ( "RootBoneName", OVERRIDE_ROOT_BONE,     g_PropString,   "It will make this bone the root node which means that any parent bones information will be merge into this bone and then deleted." )
}

//-------------------------------------------------------------------------------------------------

xbool skeleton_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "FilePath",     OVERRIDE_FILEPATH,              g_PropFilePath.Query( Query, m_FilePath ) )              
    KEY_PROP_QUERY( "bKeepBind",    OVERRIDE_KEEP_BINDING,          g_PropBool.Query( Query, m_bKeepBind ) )              
    KEY_PROP_QUERY( "RootBoneName", OVERRIDE_ROOT_BONE,             g_PropString.Query( Query, m_RootBone ) )

    return FALSE;
}        

//-------------------------------------------------------------------------------------------------

void skeleton_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_FilePath,            OVERRIDE_FILEPATH,                 .Copy   )
    KEY_PROP_UPDATE( m_bKeepBind,           OVERRIDE_KEEP_BINDING,             =       )
    KEY_PROP_UPDATE( m_RootBone,            OVERRIDE_ROOT_BONE,                .Copy   )
}

//-------------------------------------------------------------------------------------------------

xstring skeleton_compiler_key::main_params::getFileNameOnly( void ) const
{
    if( m_FilePath.IsEmpty() )
        return m_FilePath;
        
    char Filename[xfile::MAX_FNAME];
    x_splitpath( m_FilePath, NULL, NULL, Filename, NULL );
    
    return xstring::BuildFromFormat(Filename); 
}

//-------------------------------------------------------------------------------------------------

xstring skeleton_compiler_key::main_params::getImportedSkeletonFileName( void ) const
{
    return xstring::BuildFromFormat( "%s-Skeleton.rawanim",
                                     (const char*)getFileNameOnly() );
}

//-------------------------------------------------------------------------------------------------
xstring skeleton_compiler_key::main_params::getImportedMaxDistanceFileName( void ) const
{
    return xstring::BuildFromFormat( "%s-Distance.txt",
        (const char*)getFileNameOnly() );
}
