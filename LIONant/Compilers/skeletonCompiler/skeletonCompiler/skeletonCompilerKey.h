//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef SKELETON_COMPILER_KEY_OBJECT_H
#define SKELETON_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"

class skeleton_compiler_key : public compiler_key_object
{
public:

    struct main_params : public main_params_base_link<main_params>
    {
        enum override_main_flags
        {
            OVERRIDE_FILEPATH               =   X_BIT(0),
            OVERRIDE_KEEP_BINDING           =   X_BIT(1),
            OVERRIDE_ROOT_BONE              =   X_BIT(2),
            OVERRIDE_IMPORTED_MAX_DTB       =   X_BIT(3),
            OVERRIDE_IMPORTED_SKELETON      =   X_BIT(4),

            SINGLE_COMPILATION_FLAGS        =   0
                                                | OVERRIDE_KEEP_BINDING  
        };

        virtual void        onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const  override final;
        virtual xbool       onPropQuery                 ( xproperty_query& Query )  override final;
                void        onUpdateFromSrc             ( const main_params& Src, const u64 Masks );

        xstring     m_FilePath                  = X_STR("");        // Path to the source data
        xbool       m_bKeepBind                 = FALSE;            // FALSE to bake the binding pose inside the frames
        xstring     m_RootBone                  = X_STR("");

        xstring     getFileNameOnly             ( void ) const;
        xstring     getImportedSkeletonFileName     ( void ) const;
        xstring     getImportedMaxDistanceFileName  ( void ) const;
    };
    
    struct entry_params : public entry_params_base  {};

public:                 
                                skeleton_compiler_key       ( const compiler_base& Base ) : compiler_key_object( Base ) {}
            
protected:
    
    virtual const char*         getCompilerName             ( void ) const { return "SkeletonCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const { return "skeleton";         }

protected:

    KEY_STANDARD_STUFF

protected:
    
    friend class skeleton_compiler_base;
    friend class animation_compiler_base;
    friend class mesh_compiler_base;
};


#endif
