//
//  meshBaseCompiler.h
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef SKELETON_COMPILER_BASE_H
#define SKELETON_COMPILER_BASE_H

#include "skeletonCompilerKey.h"
#include "eng_base.h"
#include "RawGeom.h"
#include "eng_Geom.h"

class skeleton_compiler_base : public compiler_base
{
public:
    
                                        skeleton_compiler_base  ( void ) : m_Key( *this ) {}
    virtual void                        onCompile               ( void );
    virtual compiler_key_object&        getKeyObject            ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject            ( void ) const { return m_Key;             }

protected:

    struct compiler_data
    {
        xvar<eng_skeleton_rsc::skeleton_rsc>        m_varSkeletonRSC;
        xptr2<eng_skeleton_rsc::bone>               m_lBone;
        xptr2<eng_skeleton_rsc::hash_entry>         m_lHash;

        xvar<rawanim>                               m_varSkeleton;
        xvar<rawgeom>                               m_varSkeletonGeom;
    };


protected:

    void    CompileSpecificPlatform ( compiler_data& CompilerData, xplatform iPlatform ) const;
    void    LoadSkeleton            ( rawgeom& RawGeom, rawanim& RawAnim, const xstring& FilePath ) const;  
    void    LoadAnim                ( rawanim& RawAnim, const xstring& FilePath ) const;  
    void    ExportResource          ( const eng_skeleton_rsc::skeleton_rsc& GeomRsc ) const;

protected:

    skeleton_compiler_key                              m_Key;

};

#endif
