//
//  meshBaseCompiler.cpp
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "FBX2RawGeom.h"
#include "skeletonCompilerBase.h"

//-------------------------------------------------------------------------------------------------

void skeleton_compiler_base::LoadSkeleton( rawgeom& RawGeom, rawanim& RawAnim, const xstring& FilePath ) const
{
    xstring Temp;

    // Remove any path that has to do with the project
    Temp.Copy( FilePath );
    Temp = RemoveProjectPath( Temp );

    // choose base on type
    if( x_stristr( Temp, ".rawanim" ) )
    {
        RawAnim.Load( xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );
    }
    else if( x_stristr( Temp, ".fbx" ) )
    {
        fbx_to_rawmesh FBXToGeom;
        FBXToGeom.Convert2RawGeom( RawGeom, RawAnim, 
            xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );               
    }
    else
    {
        x_throw( "ERROR: Unkown extension for skeleton file [%s]",
            (const char*) FilePath );
    }
}

//-------------------------------------------------------------------------------------------------

void skeleton_compiler_base::LoadAnim( rawanim& RawAnim, const xstring& FilePath ) const
{
    xstring Temp;

    // Remove any path that has to do with the project
    Temp.Copy( FilePath );
    Temp = RemoveProjectPath( Temp );

    // choose base on type
    if( x_stristr( Temp, ".rawanim" ) )
    {
        RawAnim.Load( xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );
    }
    else if( x_stristr( Temp, ".fbx" ) )
    {
        fbx_to_rawmesh  FBXToGeom;
        FBXToGeom.Convert2RawGeom( RawAnim, 
            xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );               
    }
    else
    {
        x_throw("ERROR: Unkown extension for animation file [%s]",
            (const char*) FilePath );
    }
}  

//-------------------------------------------------------------------------------------------------

struct shorted_rawanims
{
    rawanim                                         m_RawAnim;
    const skeleton_compiler_key::entry_params*     m_pEntry;
};

void skeleton_compiler_base::CompileSpecificPlatform( compiler_data& CompilerData, xplatform iPlatform ) const
{
    x_inline_light_jobs_block<8>    BlockJobs;
    xbool                           bKeepBind;
    char                            SkeletonFileName[xfile::MAX_FNAME];

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // PRAPARE DATA 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Set the bKeepBind Local var
    // Load the skeleton
    //
    {
        const_ref( rKeyMainPlatform, m_Key.m_Main );
        auto& KeyMain = rKeyMainPlatform[ iPlatform ];
        
        // Check the Bind
        bKeepBind = KeyMain.m_bKeepBind;

        // Store the skeleton name
        x_splitpath( KeyMain.m_FilePath, NULL,NULL, SkeletonFileName, NULL );

        mutable_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );
        mutable_ref( rSkeleton,     CompilerData.m_varSkeleton );

        if( KeyMain.m_FilePath.IsEmpty() )
            x_throw( "ERROR: Please enter a filepath for the skeleton" );
        LoadSkeleton( rSkeletonGeom, rSkeleton, KeyMain.m_FilePath );

        // Clean up the skeleton
        rSkeleton->CleanUp();
  

        // If user wants to change the root do that
        if( FALSE == KeyMain.m_RootBone.IsEmpty() )
        {
             s32 Index = rSkeleton->GetBoneIDFromName( KeyMain.m_RootBone );
             if( Index == -1 )
                x_throw("ERROR: Unable to find RootBoneName[%s]", 
                    (const char*)KeyMain.m_RootBone );

             rSkeleton->SetNewRoot( Index );
        }

        //
        // Save Skeleton to our Imported directory
        //
//        BlockJobs.SubmitJob( [this, &CompilerData, iPlatform]()
        {
            const_ref( rKeyMainPlatform, m_Key.m_Main );
            auto& KeyMain = rKeyMainPlatform[ iPlatform ];

            SaveImportedFile( KeyMain.getImportedSkeletonFileName(), [&]( const xstring& FilePath )
            {
                const_ref( rSkeleton, CompilerData.m_varSkeleton );
                rSkeleton->Save( FilePath );
            } );
        }// );

        rSkeleton->BakeBindingIntoFrames( TRUE, TRUE, FALSE );
        rSkeleton.ReleaseAndQTReadOnly();
    };

    BlockJobs.FinishJobs();
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // COLLECT BONES AND FIXED WEIGHTS
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Clean the Skeleton vertices
    //
    BlockJobs.SubmitJob( [ this, &CompilerData ]()
    {
        const_ref( rSkeleton,       CompilerData.m_varSkeleton );
        mutable_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );

        rSkeletonGeom->CleanWeights( 4, 0.001f );
        rSkeletonGeom->ApplyNewSkeleton( rSkeleton );
        //rSkeletonGeom->ComputeBoneInfo();
        rSkeletonGeom.ReleaseAndChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    });    

    //
    // Build the Hash entries
    //
    BlockJobs.SubmitJob( [&CompilerData]()
    {
        const_ref( rSkeleton, CompilerData.m_varSkeleton );

        CompilerData.m_lHash.New( rSkeleton->m_Bone.getCount() );
        mutable_ref( rlHash, CompilerData.m_lHash );

        for ( auto& Bone : rSkeleton->m_Bone )
        {
            const s32   Index = s32( &Bone - &rSkeleton->m_Bone[0] );
            xstring     NameLower;

            NameLower.Copy( Bone.m_Name );
            NameLower.MakeLower();

            auto& Hash = rlHash[ Index ];
            Hash.m_iHash = eng_skeleton_rsc::ihash( x_strHash( NameLower ) );
            Hash.m_iBone = Index;

            // Search for hash collisions
            for ( s32 i = Index - 1; i != -1; --i )
            {
                auto& OtherHash = rlHash[ i ];
                if ( OtherHash.m_iHash.m_Value == Hash.m_iHash.m_Value )
                {
                    x_throw( "ERROR: Sorry there was a collision in the name bone hash name [%s] and [%s] both hash to the same [%h]",
                             (const char*)Bone.m_Name,
                             (const char*)rSkeleton->m_Bone[ OtherHash.m_iHash.m_Value ].m_Name,
                             Hash.m_iHash.m_Value );
                }
            }
        }

        // Make sure that they are shorted
        x_qsort(
            &rlHash[ 0 ],
            rlHash.getCount(),
            sizeof( eng_skeleton_rsc::hash_entry ),
            eng_skeleton_rsc::hash_entry::CompareFunction );

        // Set the data into the skeleton
        mutable_ref( rvarSkeletonRsc, CompilerData.m_varSkeletonRSC );
        rvarSkeletonRsc->m_pHashTable.m_Ptr = &rlHash[ 0 ];

        // Done with the hash
        rlHash.ReleaseAndQTReadOnly();
    } );

    //
    // Collect the bones
    //
    BlockJobs.SubmitJob( [this, &CompilerData, bKeepBind ]()
    {
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        CompilerData.m_lBone.New( rSkeleton->m_Bone.getCount() );
        
        rawanim NewSkeleton;
        NewSkeleton = rSkeleton;
        NewSkeleton.BakeBindingIntoFrames( TRUE, TRUE, FALSE );

        mutable_ref( rAnimBone, CompilerData.m_lBone );
        for( auto& RawBone : NewSkeleton.m_Bone )
        {
            const s32 Index     = s32( &RawBone - &NewSkeleton.m_Bone[0] );
            auto&     AnimBone  = rAnimBone[ Index ];
            
            AnimBone.m_iBone            = Index;
            AnimBone.m_iParent          = RawBone.m_iParent;
            AnimBone.m_nChildren        = RawBone.m_nChildren;
            AnimBone.m_BindMatrixInv    = RawBone.m_BindMatrixInv;
            AnimBone.m_BindTranslation  = RawBone.m_BindTranslation; //NewSkeleton.m_KeyFrame[Index].m_Translation;

           // AnimBone.m_BindMatrixInv.setTranslation( AnimBone.m_BindMatrixInv.getTranslation() * RawBone.m_BindScale ); 
                         
            x_strncpy( &AnimBone.m_Name[0], &RawBone.m_Name[0], sizeof(AnimBone.m_Name)-1, sizeof(AnimBone.m_Name) );
        }
    } );


    BlockJobs.FinishJobs();
    CompilerData.m_lBone.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // FIND MAX DISTANCES 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Save Log file
    //
    BlockJobs.SubmitJob( [&]()
    {
        const char* pPlatformName = "ALLPLATFORMS";
        if ( iPlatform ) pPlatformName = x_PlatformString( iPlatform );

        // same log file
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        rSkeleton->Save( xfs( "%s/%s_%s--%s_Skeleton.rawanim",
            (const char*)m_LogPath,
            SkeletonFileName,
            pPlatformName,
            (const char*)m_RscGuid.GetAlphaString() ) );
    } );

    //
    // Compute the MAX distance of vertices (including children) to each bone 
    //
    xptr2<f32>  lMaxDistanceToBone;

    BlockJobs.SubmitJob( [this, &CompilerData, &lMaxDistanceToBone, iPlatform ]()
    {
        const_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        lMaxDistanceToBone.Alloc( rSkeleton->m_Bone.getCount() );

        const rawgeom& SkeletonGeom = rSkeletonGeom;
         
        // Loop through bones from child to parent
        mutable_ref( rMaxDistance, lMaxDistanceToBone );
        for( s32 i = lMaxDistanceToBone.getCount() - 1; i >= 0 ; --i )
        {
            auto& MaxDistance = rMaxDistance[i];
            MaxDistance =  0;

            // Initialize distance to distance of verts from bone
            for( s32 j=0; j<SkeletonGeom.m_Vertex.getCount(); j++ )
                for( s32 k=0; k<SkeletonGeom.m_Vertex[j].m_nWeights; k++ )
                    if( SkeletonGeom.m_Vertex[j].m_Weight[k].m_iBone == i )
                    {
                        f32 BoneToVertDist = (SkeletonGeom.m_Vertex[j].m_Position - SkeletonGeom.m_Bone[i].m_Position).GetLength();
                        MaxDistance = x_Max( MaxDistance, BoneToVertDist );
                    }

            // Loop through children and see if their distance is greater
            for( s32 j = i+1; j<lMaxDistanceToBone.getCount(); j++ )
                if( SkeletonGeom.m_Bone[j].m_iParent == i )
                {
                    MaxDistance = x_Max( MaxDistance,  rMaxDistance[j] + SkeletonGeom.m_Bone[i].m_Position.GetLength() );
                }

            // Move to next bone higher in hierarchy
            i--;
        }

        for( s32 i=0; i<lMaxDistanceToBone.getCount(); i++ )
            x_DebugMsg("Max Distance Bone:%3d[%s] %f\n",
            i, 
            (const char*)rSkeleton->m_Bone[i].m_Name, 
            rMaxDistance[i] );

        //
        // Write the computed distance to the imported directory
        //
        {
            const_ref( rKeyMainPlatform, m_Key.m_Main );
            auto& KeyMain = rKeyMainPlatform[ iPlatform ];

            SaveImportedFile( KeyMain.getImportedMaxDistanceFileName(), [&]( const xstring& FilePath )
            {
                xtextfile Texfile;
                Texfile.OpenForWriting( FilePath );
                Texfile.WriteRecord("Distances", lMaxDistanceToBone.getCount() );
                
                for( f32 d : rMaxDistance )
                {
                    Texfile.WriteField( "iBone:d", rMaxDistance.getIterator() );
                    Texfile.WriteField( "Distance:f", d );
                    Texfile.WriteLine();
                }                
            } );
            
        }

        rMaxDistance.ReleaseAndQTReadOnly();
    } );

    BlockJobs.FinishJobs();

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // WRAP UP 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    const_ref( rBone, CompilerData.m_lBone );
    mutable_ref( rvarSkeletonRSC, CompilerData.m_varSkeletonRSC );
    rvarSkeletonRSC->m_pBone.m_Ptr        = const_cast<decltype(rvarSkeletonRSC->m_pBone.m_Ptr)>(&rBone[0]);
    rvarSkeletonRSC->m_nBones             = CompilerData.m_lBone.getCount();
    rvarSkeletonRSC->m_Flags              = 0;
}

//-------------------------------------------------------------------------------------------------

void skeleton_compiler_base::ExportResource( const eng_skeleton_rsc::skeleton_rsc& GeomRsc ) const
{
    //
    // Go though all the targets
    //
    for( const platform& Target : m_Target )
    {
        if( Target.m_bValid == FALSE )
            continue;

        //
        // Ok Ready to save everything then
        //
        xserialfile     SerialFile;
        xstring         FinalRscPath;
        
        FinalRscPath = getFinalResourceName( Target );
        
        SerialFile.Save( (const char*)FinalRscPath, GeomRsc, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
        
        // for debugging
        if( (1) )
        {
            xserialfile   test;
            eng_skeleton_rsc::skeleton_rsc* pGeom;
            test.Load( (const char*)FinalRscPath, pGeom );
            
           // ASSERT( eng_resource_guid::getType( pGeom->m_pInformed.m_Ptr[0].m_Guid ) == eng_informed_material_rsc::UID );

            ASSERT( pGeom );
            x_delete( pGeom );
        }
    }
}

//-------------------------------------------------------------------------------------------------

void skeleton_compiler_base::onCompile( void )
{
    xsafe_array<compiler_data, X_PLATFORM_COUNT>    CompileData;

    CompileSpecificPlatform( CompileData[0], xplatform(0));
    
    const_ref( rvarSkeleton, CompileData[0].m_varSkeletonRSC );
    ExportResource( rvarSkeleton );
}

