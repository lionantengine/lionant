//
//  compilerBase.h
//  compilerBase
//
//  Created by Tomas Arce on 9/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef COMPILER_BASE_H
#define COMPILER_BASE_H

#include "CompilerKeyObject.h"

class compiler_base
{
public:
    
    using imported_file_fn = x_function< void( const xstring& FilePath ) >;

    enum build_type
    {
        BUILDTYPE_NULL,
        BUILDTYPE_RELEASE,                      // Compiles the release version of the data
        BUILDTYPE_DEBUG,                        // Compile the debug version of the data for the game
                                                // (iteration speed and information over quality and compactness)
        BUILDTYPE_DEPENDENCY,                   // Creates only the external resource dependency file
        BUILDTYPE_SAVE_KEY_DEFAULTS             // Saves the defaults based on the constructor settings
    };
        
    struct platform
    {
                            platform(void);
        
        xplatform           m_Platform;         // Platform that we need to compile for
        
        xbool               m_bValid;           // If we need to build for this platform
        xstring             m_OutputReleaseDir; // This is where the compiler need to drop all the compiled data
        xstring             m_OutputDebugDir;   // This is where the compiler need to drop all the compiled data
        xstring             m_DependencyDir;    // Store here: Keys, Intermediate files, and resource dependencies
    };
        
public:
    
    xbool                       Compile                     ( void );
    xbool                       Parse                       ( int argc, const char* argv[] );
    const xstring&              getProjectPath              ( void ) const  { return m_ProjectPath;         }
    const xstring&              getInputPath                ( void ) const  { return m_InputPath;           }
    s32                         getPlatformCount            ( void ) const  { return m_Target.getCount();   }
    const platform&             getPlatform                 ( s32 i ) const { return m_Target[i];           }
    xstring                     getFinalResourceName        ( const platform& Target ) const;
    build_type                  getBuildType                ( void ) const  { return m_BuildType;           }
    xstring                     RemoveProjectPath           ( const xstring& String ) const;
    static u64                  getGuidFromFilePath         ( const xstring& RSCFilePath );
    void                        SaveImportedFile            ( const char* pFileName, imported_file_fn CallBack ) const;
    xstring                     getImportedPathForKeyFile   ( const xstring& KeyFileName ) const;
     
    // External keys are key compiler objects that are created dynamically by compilers.
    // For instance this could be the case of the Sprite Atlas. This resource compiler will generated a few files for its output.
    // One of them is a temporary texture that requires farther compilation by the texture compiler.
    // The key object file require to compile this texture will be an External Dependency.
    void                        AddExternalDependencyKey    ( const xstring& FilePath ) const;

protected:
    
    virtual void                        onCompile           ( void ) = 0;
    virtual compiler_key_object&        getKeyObject        ( void ) = 0;
    virtual const compiler_key_object&  getKeyObject        ( void ) const = 0;
    
protected:
    
    virtual void                 SaveKeyDefaults            ( void );
    virtual void                 LoadKeyDefaults            ( void );
    virtual void                 LoadKeyFile                ( void );
    virtual void                 SaveAssetDependencyList    ( void );
            xbool                InternalParse              ( s32 argc, const char *argv[] );
            void                 HandleExceptionMessage     ( const char* pMsg ) const;
    
protected:
    
    build_type                  m_BuildType;
    xtimer                      m_Timmer;
    xstring                     m_ProjectPath;              // Where is the project located
    s32                         m_ProjectPathLength;        // Length of the file path
    xstring                     m_GameDataPath;             // Path for all the game data references
    xstring                     m_InputPath;                // Where is the path of the key file
    xstring                     m_RscFileName;              // No extension and not Guid
    xguid                       m_RscGuid;                  // Guid base on the file name
    xstring                     m_KeyFilePath;              // Path for the input key file
    xstring                     m_ExternalCompilerKeysPath; // All generated key files go here
    xstring                     m_IntermediateDataPath;     // All generated intermediate data files go here
    xstring                     m_RscBuildDependsPath;      // Internal directory for all the depends files
    xstring                     m_LogPath;                  // This is the path where all the logs get saved
    xstring                     m_ImportedPath;             // The path for all imported files 
    xstring                     m_RscImportedPath;          // The path for imported files for this resource 
    mutable xarray2<xstring>    m_lExternalDepKeys;         // Array of external dependencies

    // Array with all the platform structures
    u32                                      m_PlatformMask;
    xsafe_array<platform, X_PLATFORM_COUNT>  m_Target;

    mutable xcritical_section   m_CriticalSection;      // Critical section used for external dependencies

protected:

    friend class compiler_key_object;
};

#endif
