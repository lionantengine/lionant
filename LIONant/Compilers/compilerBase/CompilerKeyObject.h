//
//  CompilerKeyObject.h
//  compilerBase
//
//  Created by Tomas Arce on 9/4/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef COMPILER_KEY_OBJECT_H
#define COMPILER_KEY_OBJECT_H

//===========================================================================================
// TODO:
//    One thing that is missing is to fully support a per platform configurations. This is not implemented now
//    to avoid complexity. However I have thought about it and my first impressions on how that should work is
//    like this:
//
//    The default strucutres should have x instances where x is for each of the platform plus one.
//    The NULL plaform entry will be known as the ALL entry. This entry will be the default of the other entries.
//    so a default of defaults. This means that other defaults will have to override it to custumize their selfs.
//    This is a good thing because it will keep the configurations easy to modify for each of the platforms.
//
//    The Instance structures should work base of their respective defaults and then they will just overwride them.
//    Anyways just some thoughts that I did not want to lose them.
//
//
//        1st Update this           onCascadeDefaultProperties
//     |---------------------------------------------------------------------------|
//
//                                                                 2nd This   onCascadeUniqueProperties
//                                                               |----------------------------------------------|
//
//                             +---> Platfrom A Defaults   ----> Platform A Unique <---+
//                            /                                                         \
//      Default All Platforms -----> Platform B Defaults   ----> Platform B Unique <----- Unique ALL Platforms
//                            \                                                         /
//                             +---> Platfrom C Defaults   ----> Platform C Unique <---+
//
// Conclusion the priority is:
//      Default All Platforms --> Platform X Defaults --> Unique All Platforms --> Plaform X Unique (Highest)
//
//===========================================================================================
class compiler_base;

#define KEY_PROP_QUERY( NAME, ENUM, PROP_TYPE )                                     \
    if ( Query.Var( NAME ) && PROP_TYPE )                                           \
    {                                                                               \
        if ( FALSE == Query.IsUserGet( ) ) x_FlagOn( m_OverrideBits, ENUM );        \
        return TRUE;                                                                \
    }

//===========================================================================================

#define KEY_PROP_UPDATE( VAR, ENUM, COPYOP )                                        \
        if ( !x_FlagIsOn( Masks, ENUM ) )                                           \
            VAR COPYOP ( Src.VAR );                 

//===========================================================================================

#define KEY_PROP_ENUM( NAME, ENUM, PROTYPE, DESC )                                  \
    if ( x_FlagIsOn( Mask, ENUM ) )                                                 \
        PROTYPE.Enum( Enum, iScope, NAME,  DESC );  

#define KEY_PROP_ENUM_FILE( NAME, ENUM, DESC, FILETYPE )                            \
    if ( x_FlagIsOn( Mask, ENUM ) )                                                 \
        g_PropFilePath.Enum( Enum, iScope, NAME,  DESC, FILETYPE, "" );  

//===========================================================================================
#define KEY_STANDARD_STUFF    \
    using   main_ppflist                = xsafe_array<main_params,   X_PLATFORM_COUNT>; \
    using   entry_ppflist               = xsafe_array<entry_params,  X_PLATFORM_COUNT>; \
    virtual void                opMainParam                 ( xplatform iPlatform, const_func_main_params             Function ) const override final   { const_ref( rEntry, m_Main);               Function( rEntry[ iPlatform ] );   } \
    virtual void                opMainParamDefault          ( xplatform iPlatform, const_func_main_params             Function ) const override final   { const_ref( rEntry, m_DefaultMain );       Function( rEntry[ iPlatform ] );   } \
    virtual void                opEntryParamDefault         ( xplatform iPlatform, const_func_entry_default_params    Function ) const override final   { const_ref( rEntry, m_DefaultEntry);       Function( rEntry[ iPlatform ] );   } \
    virtual xbool               opEntryParam                ( xplatform iPlatform, const_func_entry_params            Function ) const override final   { const_ref( rEntry, m_lEntry); xbool bSuccess = FALSE; for( auto& r : rEntry )     { Function( r[ iPlatform ], rEntry.getIterator(), bSuccess ); if(bSuccess) break; } return bSuccess; }  \
    virtual void                opMainParam                 ( xplatform iPlatform, func_main_params             Function ) override final               { mutable_ref( rEntry, m_Main);               Function( rEntry[ iPlatform ] );   } \
    virtual void                opMainParamDefault          ( xplatform iPlatform, func_main_params             Function ) override final               { mutable_ref( rEntry, m_DefaultMain );       Function( rEntry[ iPlatform ] );   } \
    virtual void                opEntryParamDefault         ( xplatform iPlatform, func_entry_default_params    Function ) override final               { mutable_ref( rEntry, m_DefaultEntry);       Function( rEntry[ iPlatform ] );   } \
    virtual xbool               opEntryParam                ( xplatform iPlatform, func_entry_params            Function ) override final               { mutable_ref( rEntry, m_lEntry); xbool bSuccess = FALSE; for( auto& r : rEntry )   { Function( r[ iPlatform ], rEntry.getIterator(), bSuccess ); if(bSuccess) break; } return bSuccess; }  \
    virtual s32                 getEntryParamCount          ( void ) const      { return m_lEntry.getCount(); }  \
    virtual void                setEntryParamCount          ( s32 newCount )    { if(newCount > m_lEntry.getCount()) { mutable_ref( r, m_lEntry); r->appendList(newCount - m_lEntry.getCount() ); } }  \
    xlvar<main_ppflist>         m_DefaultMain;     \
    xlvar<main_ppflist>         m_Main;             \
    xlvar<entry_ppflist>        m_DefaultEntry;      \
    xarray2<entry_ppflist>      m_lEntry;             

//===========================================================================================

class compiler_key_object : public xproperty
{
public:
    
    struct dependency
    {
        xstring     m_FilePath         = X_STR("");    // File name including the relative path to it from the Project Path
        u32         m_Plaforms         = 0;            // Bit Mask of which platforms needs this asset for compilation
        xbool       m_bForceKeyBuild   = FALSE;        // Force key build tell the build system that the source file is a key file
                                                       // and whenever that key file has to build we need to build as well.
                                                       // This usually happens when the source data of another resources actually affects
                                                       // this resource structure. 
    };

    struct main_params_base
    {
        virtual void        onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const    {}
        virtual xbool       onPropQuery                 ( xproperty_query& Query )                              { return FALSE; }
        virtual xbool       onPropQuery                 ( xproperty_query& Query ) const final                  { ASSERT(Query.IsUserGet()); return (const_cast<main_params_base*>(this))->onPropQuery(Query); }
        virtual void        onUpdateFromSrc             ( const main_params_base& Src, const u64 Masks )        {}
        u32                 m_OverrideBits = 0;
    };

    template< typename T >
    struct main_params_base_link : public main_params_base
    {
        virtual void        onUpdateFromSrc             ( const main_params_base& Src, const u64 Masks ) override final { ((T*)this)->onUpdateFromSrc( *((T*)&Src), Masks ); }
    };

    struct entry_params_base
    {
        virtual void        onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const    {}
        virtual xbool       onPropQuery                 ( xproperty_query& Query )                              { return FALSE; }
        virtual xbool       onPropQuery                 ( xproperty_query& Query ) const final                  { ASSERT(Query.IsUserGet()); return (const_cast<entry_params_base*>(this))->onPropQuery(Query); }
        virtual void        onUpdateFromSrc             ( const entry_params_base& Src, const u64 Masks )       {}
        u32                 m_OverrideBits = 0;
    };

    template< typename T >
    struct entry_params_base_link : public entry_params_base
    {
        virtual void        onUpdateFromSrc             ( const entry_params_base& Src, const u64 Masks ) override final { ((T*)this)->onUpdateFromSrc( *((T*)&Src), Masks ); }
    };
    
public:

                            compiler_key_object         ( const compiler_base& CompilerBase ) : m_CompilerBase( CompilerBase ) {}
    virtual void            SaveDefaults                ( const xstring& ProjectPath );
    virtual void            LoadDefaults                ( const xstring& ProjectPath );
    virtual xbool           Load                        ( xtextfile& DataFile  );
    virtual xbool           Load                        ( const xstring& FileName );
    virtual void            Save                        ( const xstring& FileName );

protected:

    using   func_main_params                    = x_function<void(main_params_base& Entry)>;
    using   func_entry_default_params           = x_function<void(entry_params_base& Entry)>;
    using   func_entry_params                   = x_function<void(entry_params_base& Entry, s32 iEntry, xbool& bSuccess)>; 
    using   const_func_main_params              = x_function<void(const main_params_base& Entry)>;
    using   const_func_entry_default_params     = x_function<void(const entry_params_base& Entry)>;
    using   const_func_entry_params             = x_function<void(const entry_params_base& Entry, s32 iEntry, xbool& bSuccess)>; 


protected:

                                compiler_key_object         ( void ) = delete;
    virtual void                opMainParam                 ( xplatform iPlatform, func_main_params             func ) = 0;
    virtual void                opMainParamDefault          ( xplatform iPlatform, func_main_params             func ) = 0;
    virtual void                opEntryParamDefault         ( xplatform iPlatform, func_entry_default_params    func ) = 0;
    virtual xbool               opEntryParam                ( xplatform iPlatform, func_entry_params            func ) = 0;
    virtual void                opMainParam                 ( xplatform iPlatform, const_func_main_params             func ) const = 0;
    virtual void                opMainParamDefault          ( xplatform iPlatform, const_func_main_params             func ) const = 0;
    virtual void                opEntryParamDefault         ( xplatform iPlatform, const_func_entry_default_params    func ) const = 0;
    virtual xbool               opEntryParam                ( xplatform iPlatform, const_func_entry_params            func ) const = 0;

    virtual s32                 getEntryParamCount          ( void ) const = 0;
    virtual void                setEntryParamCount          ( s32 newCount ) = 0;

    virtual const char*         getCompilerName             ( void ) const = 0;
    virtual const char*         getCompiledExt              ( void ) const = 0;

    // If there are dependencies that are not expres in the key file then the user will have to add them manually
    // You can also use it to filter the existing list
    virtual void                getAdditionalDependencies   ( xarray2<dependency>& List ) const {}

protected:

    // Regular property enumeration stuff
    virtual xbool           onPropQuery                 ( xproperty_query& Query ) override final;
    virtual void            onPropEnum                  ( xproperty_enum&  Enum  ) const final;
    
    // This function shoudl enumerate only properties that are different from the
    // default values.
    virtual xbool           onDefaultPropQuery          ( xproperty_query& Query ) final;
    virtual void            onDefaultEnumeration        ( xproperty_enum&  Enum, s32 iScope ) final;
    virtual xbool           onUniquePropQuery           ( xproperty_query& Query ) ;
    virtual void            onUniqueEnumeration         ( xproperty_enum& Enum, s32 iScope, u64 UniqueNessMask ) const final;
    virtual void            onCascadeProperties         ( void ) final;
    
    // THIS IS NOT DONE BY THE USER IS AUTOMATIC
    // Get all the asset dependenices for this key object
    virtual void            getAssetDependencies        ( xarray2<dependency>& List ) const final;
    
    // THIS IS NOT DONE HERE ANY MORE
    // Internal dependencies are other resources which the compiler system knows about.
    // Such it could be the case of a Material dependent of a Texture.
    // We assume both cases we created a key file in the compiling system.
    virtual void            getInternalDependencies     ( xarray2<xstring>& List ) final {};
    
protected:
    
    xbool                   m_bExternalResource:1;          // Whether the user created this key file other wise was created by a compiler
    const compiler_base&    m_CompilerBase;

protected:
    
    friend class compiler_base;
};

#endif