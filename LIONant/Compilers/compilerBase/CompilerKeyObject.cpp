//
//  CompilerKeyObject.cpp
//  compilerBase
//
//  Created by Tomas Arce on 9/4/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "CompilerBase.h"

//---------------------------------------------------------------------------------------

xbool compiler_key_object::Load( xtextfile& DataFile )
{
    if( xproperty::Load( DataFile ) )
    {
        onCascadeProperties();
        return TRUE;
    }
    
    return FALSE;
}

//---------------------------------------------------------------------------------------

xbool compiler_key_object::Load( const xstring& FileName )
{
    xtextfile TextFile;
    
    TextFile.OpenForReading( FileName );
    if( TextFile.ReadRecord() )
    {
        Load( TextFile );
    }
    else
    {
        x_throw( "ERROR: Unable to read any records for the key file [%s]", (const char*)FileName );
    }
    
    TextFile.Close();

    // Make sure that everything is updated
    onCascadeProperties();

    return TRUE;
}

//---------------------------------------------------------------------------------------

void compiler_key_object::SaveDefaults( const xstring& ProjectPath )
{
    //
    // Set the defaults overrideBits to 1
    //
    opMainParamDefault( xplatform(0), [this]( main_params_base& Entry )
    {
        Entry.m_OverrideBits = ~0;
    });
    
    opEntryParamDefault( xplatform(0), [this]( entry_params_base& Entry )
    {
        Entry.m_OverrideBits = ~0;
    });

    //
    // Save the defaults
    //
    xtextfile TextFile;
    xstring   KeyDefaultPath;
    
    KeyDefaultPath.Format( "%s/Bin/Configurations/%s.txt",
                          (const char*)ProjectPath,
                          getCompilerName() );

    TextFile.OpenForWriting( KeyDefaultPath );
    
    xproperty_enum  Enum;
    
    s32 iScope = Enum.AddScope( "Default", "These are the default properties" );
    onDefaultEnumeration( Enum, iScope );
    
    xproperty::Save( TextFile, Enum );
    
    
    TextFile.Close();
}

//---------------------------------------------------------------------------------------

void compiler_key_object::LoadDefaults( const xstring& ProjectPath )
{
    //
    // Set the defaults overrideBits to 1
    //
    opMainParamDefault( xplatform(0), [this]( main_params_base& Entry )
    {
        Entry.m_OverrideBits = ~0;
    });
    
    opEntryParamDefault( xplatform(0), [this]( entry_params_base& Entry )
    {
        Entry.m_OverrideBits = ~0;
    });

    //
    // Load the defaults
    //
    xtextfile   TextFile;
    xstring     KeyDefaultPath;
    
    KeyDefaultPath.Format( "%s/Bin/Configurations/%s.txt",
                          (const char*)ProjectPath,
                          getCompilerName() );
    
    TextFile.OpenForReading( KeyDefaultPath );
    
    Load( TextFile );
    
    TextFile.Close();
    
    //
    // Now we can tell the key class to update its user data
    //
    onCascadeProperties();
}

//---------------------------------------------------------------------------------------

void compiler_key_object::onPropEnum( xproperty_enum& Enum ) const
{
    s32 iScope = Enum.AddScope( "Key", "These are the key entry properties for each resource instance" );
    onUniqueEnumeration( Enum, iScope, 0 );
}

//---------------------------------------------------------------------------------------

xbool compiler_key_object::onPropQuery( xproperty_query& Query )
{
    if( Query.Scope( "Default" ) )
    {
        if( onDefaultPropQuery( Query ) )
            return TRUE;
    }
    else if( Query.Scope( "Key" ) )
    {
        if( onUniquePropQuery( Query ))
            return TRUE;
    }

    x_printf("WARNING: Unable to set property [%s]", Query.GetPropertyName() );
    return FALSE;
}

//---------------------------------------------------------------------------------------

void compiler_key_object::getAssetDependencies( xarray2<dependency>& lList ) const
{
    //
    // First enumerate all the properties
    //
    xproperty_enum  Enum;
    onPropEnum( Enum );

    //
    // Now we will go through the properties and identify FILEPATHs
    // and collect all of those
    //
    for ( s32 i = 0; i < Enum.GetEntryCount(); i++ )
    {
        mutable_ref( rList, lList );
        xproperty_type& Type = Enum.GetEntryType(i);

        if ( Type.isKindOf( xprop_filepath::getRTTI() ) == FALSE )
            continue;

        xstring PropFullName;
        xstring File;
        
        Enum.GetPropFullName( i, PropFullName );

        g_PropFilePath.Get( *this, PropFullName, File );

        if ( File.IsEmpty() )
            continue;

        //
        // Determine if we are dealing with a resource key
        //
        if ( File.Find( "CompilerKeys" ) != -1 )
        {
            File = xstring::BuildFromFormat( "%s.txt", (const char*)File );
        }

        //
        // try to find it already in the list
        //
        s32 iDepend = -1;
        for ( dependency& Dependency : rList )
        {
            if ( x_stricmp( Dependency.m_FilePath, File ) == 0 )
            {
                iDepend = s32( &Dependency - &rList[0] );
                break;
            }
        }

        // Must be a new entry
        if ( iDepend == -1 )
        {
            dependency& Dependency = rList->append( iDepend );
            Dependency.m_FilePath = File;
            Dependency.m_Plaforms = 0;

            // TODO: This is for key files that depend of other key files dependencies
            //       in other words if the key file needs to be build for whatever reason 
            //       we should also build.
            //       Key question is... is this true for all dependency key files
            //       I think it is but not sure, if so then this can be switch on automatically base
            //       on if this dependency is a key file... farther more at that point this should
            //       be done in the rscbuilder.
            Dependency.m_bForceKeyBuild = FALSE;
        }

        //
        // Get the dependency
        //
        dependency& Dependency = rList[ iDepend ];

        //
        // set the right platforms for it
        //
        if ( x_stristr( PropFullName, "Global" ) )
        {
            if ( Dependency.m_Plaforms == 0 )
                Dependency.m_Plaforms = ~0;
            else
            {
                // If we find this entry again let make sure it had everything turn on
                ASSERT(Dependency.m_Plaforms == ~0);
            }
        }
        else
        {
            for ( s32 p=1; p<X_PLATFORM_COUNT; p++ )
            if ( x_stristr( PropFullName, x_PlatformString( xplatform( p ) ) ) )
            {
                if ( Dependency.m_Plaforms == ~0 )
                {
                    // This should already include this platform
                }
                else
                {
                    Dependency.m_Plaforms |= (1<<p);
                }
                break;
            }
        }
    }

    //
    // Get additional dependencies from the user
    //
    getAdditionalDependencies( lList );
}

//---------------------------------------------------------------------------------------

void compiler_key_object::Save( const xstring& FileName )
{
    x_inline_light_jobs_block<8> JobBlock;

    //
    // Make sure that everything is updated properly
    //
    onCascadeProperties();

    //
    // If it exits we are going to try not to write to if it is identical since if we write to it
    // it will force the build system to build this resource.
    //
    if ( x_io::FileExists( FileName ) )
    {
        xptr2<char> lBufferNew;
        xptr2<char> lBufferOld;

        //
        // Write the new file in memory
        //        
        JobBlock.SubmitJob( [this, &lBufferNew, &FileName ]()
        {
            xfile       File;
            xtextfile   TempTextFile;
            
            if( FALSE == File.Open( "ram:temp", "wt" ) )
                x_throw( "ERROR: Internal error unable to create a ram file while saving [%s]",
                (const char*)FileName  );

            TempTextFile.WriteToFile( File );
            xproperty::Save( TempTextFile );
        
            lBufferNew.Alloc( File.GetFileLength() );
            {
                mutable_ref( rBuffer, lBufferNew );

                File.SeekOrigin(0);
                File.ReadRaw( &rBuffer[0], 1, lBufferNew.getCount() );
            }

            lBufferNew.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
        });

        //
        // Read the existing file
        //
        JobBlock.SubmitJob( [this, &lBufferOld, &FileName]()
        {
            xfile       File;

            // if it fails could be because the file was empty
            if( FALSE == File.Open( FileName, "rt" ) )
                return;

            lBufferOld.Alloc( File.GetFileLength() );
            {
                mutable_ref( rBuffer, lBufferOld );
                File.ReadRaw( &rBuffer[0], 1, lBufferOld.getCount() );
            }

            lBufferOld.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
        });

        //
        // make sure that is all finish
        //
        JobBlock.FinishJobs();
        
        //
        // Now we can compare the files... if they are identical then do nothing
        //
        if ( lBufferNew.getCount() == lBufferOld.getCount() )
        {
            const_ref( rBufferNew, lBufferNew );
            const_ref( rBufferOld, lBufferOld );

            // If they are identical the just vail
            if( 0 == x_memcmp( &rBufferNew[0], &rBufferOld[0], rBufferOld.getCount() - 4 ) )
                return;
        }

        //
        // if not identical then overwrite the existing file
        //
        {
            xfile File;

            if( FALSE == File.Open( FileName, "wb" ) )
                x_throw( "ERROR: Unable to open [%s] key file while trying to write",
                (const char*)FileName );

            const_ref( rBuffer, lBufferNew );
            File.WriteRaw( &rBuffer[0], 1, rBuffer.getCount() );
        }
    }
    else
    {
        xproperty::Save( FileName );
    }
}

//---------------------------------------------------------------------------------------

xbool compiler_key_object::onDefaultPropQuery( xproperty_query& Query )
{
    xbool bSuccess = FALSE;

    // 
    // Set main default properies
    //
    if( Query.Scope( "Main" ) )
    {
        for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
        {
            const char* pScopeName = "Global";
            if ( i ) pScopeName = x_PlatformString( xplatform( i ) );

            if ( Query.Scope( pScopeName ) )
            {
                opMainParamDefault( xplatform(i), [i,&Query, &bSuccess]( main_params_base& Entry )
                {
                    bSuccess = Entry.onPropQuery( Query );
                });

                // Check to see if we are done
                if( bSuccess )
                    return TRUE;
            }
        }
    }

    // 
    // Set entry default properies
    //
    if( Query.Scope( "Entry" ) )
    {
        for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
        {
            const char* pScopeName = "Global";
            if ( i ) pScopeName = x_PlatformString( xplatform( i ) );

            if ( Query.Scope( pScopeName ) )
            {
                opEntryParamDefault( xplatform(i), [i,&Query, &bSuccess]( entry_params_base& Entry )
                {
                    bSuccess = Entry.onPropQuery( Query );
                });

                // Check to see if we are done
                if( bSuccess )
                    return TRUE;
            }
        }
    }

    return TRUE;
}

//---------------------------------------------------------------------------------------

void compiler_key_object::onDefaultEnumeration( xproperty_enum& Enum, s32 iScope )
{
    //
    // Read the main enumeration
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opMainParamDefault( xplatform(i), [iScope, &Enum, i]( main_params_base& Entry )
        {
            // If we have not information then there is nothing to do
            if( Entry.m_OverrideBits == 0 )
                return;

            // Set the entry scope
            s32 iEntryScope = Enum.AddScope( iScope, "Main", "This scope is to indicate that the enumeration has to do with the main entry" );

            // Set the platform scope
            const char* pScopeName = "Global";
            if(i) pScopeName = x_PlatformString( xplatform(i) );        
            s32 iPlatformScope = Enum.AddScope( iEntryScope, pScopeName, "This is the base scope for each of the platforms" );

            // Do actual enumeration            
            Entry.onPropEnum( Enum, iPlatformScope, ~0 );
        });
    }
    
    //
    // Read the entry enumeration
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opEntryParamDefault( xplatform(i), [&Enum, iScope, i]( entry_params_base& Entry )
        {
            // If we have not information then there is nothing to do
            if( Entry.m_OverrideBits == 0 )
                return;
        
            // Set the entry scope
            s32 iEntryScope = Enum.AddScope( iScope, "Entry", "This scope is to indicate that the enumeration has to do with each entry" );

            // Set the platform scope
            const char* pScopeName = "Global";
            if(i) pScopeName = x_PlatformString( xplatform(i) );        
            s32 iPlatformScope = Enum.AddScope( iEntryScope, pScopeName, "This is the base scope for each of the platforms" );

            // Do actual enumeration
            Entry.onPropEnum( Enum, iPlatformScope, ~0 );
        });
    }
}

//---------------------------------------------------------------------------------------

xbool compiler_key_object::onUniquePropQuery( xproperty_query& Query )
{
    xbool bSuccess = FALSE;

    // 
    // Set main properies
    //
    if( Query.Scope( "Main" ) )
    {
        for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
        {
            const char* pScopeName = "Global";
            if ( i ) pScopeName = x_PlatformString( xplatform( i ) );

            if ( Query.Scope( pScopeName ) )
            {
                if( Query.IsUserGet() )
                {
                    ((const compiler_key_object*)this)->opMainParam( xplatform(i), [&Query, &bSuccess]( const main_params_base& Entry )
                    {
                        bSuccess = Entry.onPropQuery( Query );
                    });
                }
                else
                {
                    opMainParam( xplatform(i), [&Query, &bSuccess]( main_params_base& Entry )
                    {
                        bSuccess = Entry.onPropQuery( Query );
                    });
                }

                // Check to see if we are done
                if( bSuccess )
                    return TRUE;
            }
        }
    }

    // 
    // Set entry properies
    //
    if( Query.Scope( "Entries" ) )
    {
        if ( Query.Var( "Count" ) )
        {
            const s32   oldCount = getEntryParamCount();
            s32         newCount = oldCount;

            g_PropInt.Query( Query, newCount, 0, 1000 );

            if ( FALSE == Query.IsUserGet( ) && newCount != oldCount ) 
                setEntryParamCount( newCount );

            ASSERT( newCount == getEntryParamCount() );
            return TRUE;
        }

        if ( Query.Scope( "Entry[]" ) )
        {
            const s32   Index       = Query.GetArrayIndex();

            // Check every entry for every platform
            for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
            {
                const char* pScopeName  = "Global";
                if ( i ) pScopeName = x_PlatformString( xplatform( i ) );

                if ( Query.Scope( pScopeName ) )
                {
                    if( Query.IsUserGet() )
                    {
                        bSuccess = ((const compiler_key_object*)this)->opEntryParam( xplatform(i), [&Query,Index]( const entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
                        {
                            if( iEntry != Index )
                                return;

                            bSuccess = Entry.onPropQuery( Query );
                        });
                    }
                    else
                    {
                        bSuccess = opEntryParam( xplatform(i), [&Query,Index]( entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
                        {
                            if( iEntry != Index )
                                return;

                            bSuccess = Entry.onPropQuery( Query );
                        });
                    }

                    // Check to see if we are done
                    if( bSuccess )
                        return TRUE;
                }
            }
        }
    }

    return FALSE;
}

//---------------------------------------------------------------------------------------

void compiler_key_object::onUniqueEnumeration( xproperty_enum& Enum, s32 iScope, u64 UniqueNessMask ) const 
{
    //
    // Read the main enumeration
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opMainParam( xplatform(i), [UniqueNessMask, iScope, &Enum, i]( const main_params_base& Entry )
        {
            // If we have not information then there is nothing to do
            if( Entry.m_OverrideBits == 0 )
                return;

            // Set the entry scope
            s32 iEntryScope = Enum.AddScope( iScope, "Main", "This scope is to indicate that the enumeration has to do with the main entry" );

            // Platform        
            const char* pScopeName = "Global";
            if(i) pScopeName = x_PlatformString( xplatform(i) );
            s32 iPlatformScope = Enum.AddScope( iEntryScope, pScopeName, "This is the base scope for each of the platforms" );

            // Enumerate
            Entry.onPropEnum( Enum, iPlatformScope, Entry.m_OverrideBits | UniqueNessMask );
        });
    }

    //
    // Read the entry enumeration
    //
    const s32 iEntryArrayScope = Enum.AddScope( iScope, "Entries", "This is the particular entry scope" );
    if( getEntryParamCount() > 0 ) g_PropInt.Enum( Enum, iEntryArrayScope, "Count",  "How many entries do we have in the list" ); 
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opEntryParam( xplatform(i), [UniqueNessMask,&Enum,iEntryArrayScope,i]( const entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
        {
            // If we have not information then there is nothing to do
            if( Entry.m_OverrideBits == 0 )
                return;

            // Add entry scope
            const s32 iEntryScope = Enum.AddScope( iEntryArrayScope, xfs("Entry[%d]", iEntry), "This is the particular entry scope" );     


            // Add platform scope
            const char* pScopeName = "Global";
            if(i) pScopeName = x_PlatformString( xplatform(i) );
            const s32 iPlatformScope = Enum.AddScope( iEntryScope, pScopeName, "This is the base scope for each of the platforms" );

            // enumerate
            Entry.onPropEnum( Enum, iPlatformScope, Entry.m_OverrideBits | UniqueNessMask );
        });
    }
}

//---------------------------------------------------------------------------------------

void compiler_key_object::onCascadeProperties( void )
{
    //
    // main_params - First we update from the Default All to the Platform specific
    //
    opMainParamDefault( xplatform(0), [this]( main_params_base& Entry )
    {
        const main_params_base& Src  = Entry;

        for( s32 i=1; i<X_PLATFORM_COUNT; i++ )
        {
            opMainParamDefault( xplatform(i), [this,&Src]( main_params_base& Entry )
            {
                Entry.onUpdateFromSrc( Src, Entry.m_OverrideBits );
            });
        }
    });
            
    //
    // main_params - Then we update from each of the default properties into the unique
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opMainParamDefault( xplatform(i), [this,i]( main_params_base& Entry )
        {
            const main_params_base& Src  = Entry;
            opMainParam( xplatform(i), [&Src]( main_params_base& Entry )
            {
                Entry.onUpdateFromSrc( Src, Entry.m_OverrideBits );
            });
        });   
    }
    
    //
    // main_params - Finally from the Unique All Plaforms to the specific platforms
    //
    opMainParam( xplatform(0), [this]( main_params_base& Entry )
    {
        const main_params_base& Src  = Entry;
        for( s32 i=1; i<X_PLATFORM_COUNT; i++ )
        {
            opMainParam( xplatform(i), [&Src]( main_params_base& Entry )
            {
                // u - user unique per platform
                // s - user global
                //
                // u s TruthTable       a:(u ^ s)  b:( ~u )  c:(a & b)   d:~c
                // 0 0 -> no copy 1  |       0         1          0       1
                // 0 1 -> copy    0  |       1         1          1       0
                // 1 0 -> no copy 1  |       1         0          0       1
                // 1 1 -> no copy 1  |       0         0          0       1
                Entry.onUpdateFromSrc( Src, ~((Entry.m_OverrideBits ^ Src.m_OverrideBits) & (~Entry.m_OverrideBits)) );
            });
        }
    });   

    //
    // entry_params_base - First we update from the Default All to the Platform specific
    //
    opEntryParamDefault( xplatform(0), [this]( entry_params_base& Entry )
    {
        const entry_params_base& Src  = Entry;

        for( s32 i=1; i<X_PLATFORM_COUNT; i++ )
        {
            opEntryParamDefault( xplatform(i), [this,&Src]( entry_params_base& Entry )
            {
                Entry.onUpdateFromSrc( Src, Entry.m_OverrideBits );
            });
        }
    });

    //
    // entry_params - Then we update from each of the default properties into the unique
    //
    for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
    {
        opEntryParamDefault( xplatform(i), [this,i]( entry_params_base& Entry )
        {
            const entry_params_base& Src  = Entry;
            opEntryParam( xplatform(i), [&Src]( entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
            {
                Entry.onUpdateFromSrc( Src, Entry.m_OverrideBits );
            });
        });   
    }

    //
    // entry_params - Finally from the Unique All Plaforms to the specific platforms
    //
    opEntryParam( xplatform(0), [this]( entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
    {
        const entry_params_base& Src        = Entry;
        const s32                iCurrEntry = iEntry;
        
        for( s32 i=1; i<X_PLATFORM_COUNT; i++ )
        {
            opEntryParam( xplatform(i), [&Src,iCurrEntry]( entry_params_base& Entry, s32 iEntry, xbool& bSuccess )
            {
                if(iCurrEntry != iEntry)
                    return;
                // u - user unique per platform
                // s - user global
                //
                // u s TruthTable       a:(u ^ s)  b:( ~u )  c:(a & b)   d:~c
                // 0 0 -> no copy 1  |       0         1          0       1
                // 0 1 -> copy    0  |       1         1          1       0
                // 1 0 -> no copy 1  |       1         0          0       1
                // 1 1 -> no copy 1  |       0         0          0       1
                Entry.onUpdateFromSrc( Src, ~((Entry.m_OverrideBits ^ Src.m_OverrideBits) & (~Entry.m_OverrideBits)) );
                bSuccess = TRUE;
            });
        }
    });   
}
