//
//  compilerBase.cpp
//  compilerBase
//
//  Created by Tomas Arce on 9/3/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include <stdio.h>

#include "x_base.h"
#include "compilerBase.h"

static const char*                          g_pCompilerName = NULL;
static xsafe_array<char,xfile::MAX_FNAME>   g_RscFileName{0};

//--------------------------------------------------------------------------

compiler_base::platform::platform( void )
{
    m_Platform      = X_PLATFORM_NULL;
    m_bValid        = FALSE;
}

//---------------------------------------------------------------------------------------
static
void PrintF( const char* pString )
{
    if( g_RscFileName[0] )
    {
        // Call the system printf.
        printf( "%s: %s: %s\n",
               g_pCompilerName,
               (const char*)g_RscFileName,
               pString );
    }
    else
    {
        // Call the system printf.
        printf( "%s: %s\n", g_pCompilerName, pString );
    }
    
    fflush(stdout);
}

//--------------------------------------------------------------------------

xbool compiler_base::InternalParse( s32 argc, const char *argv[] )
{
    xcmdline    CmdLine;
    
    //
    // Have a nice printf
    //
    g_pCompilerName = getKeyObject().getCompilerName();
    x_SetFunctionPrintF( PrintF );

    //
    // Create the switches and their rules
    //
    {
        // Setup all the command switches
        const s32 idOverWriteDefaults = CmdLine.AddCmdSwitch( "OVERWRITE_DEFAULTS",  0,  0, 0, 1, TRUE, xcmdline::TYPE_STRING, TRUE );
        CmdLine.AddCmdSwitch( "PROJECT",    1,  1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, idOverWriteDefaults );
        
        const s32 idBuildType = CmdLine.AddCmdSwitch( "BUILDTYPE",  1,  1, 0, 1, TRUE, xcmdline::TYPE_STRING, TRUE );
        CmdLine.AddCmdSwitch( "PROJECT",    1,  1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, idBuildType );
        CmdLine.AddCmdSwitch( "TARGET",     1, -1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, idBuildType );
        CmdLine.AddCmdSwitch( "INPUT",      1,  1, 1, 1, FALSE, xcmdline::TYPE_STRING, FALSE, idBuildType );
    }
    
    //
    // Start parsing the arguments
    //
    CmdLine.Parse( argc, argv );
    
    //
    // Deal with the help message
    //
    if( argc < 2 || CmdLine.DoesUserNeedsHelp() )
    {
        x_printf( "\n"
                  "------------------------------------------------------------\n"
                  "LIONant - Compiler system.                                  \n"
                  "%s [ %s - %s ]                                              \n"
                  "Switches:                                                   \n"
                  "     -OVERWRITE_DEFAULTS <>                                 \n"
                  "         -PROJECT        <NetworkFriendlyPathOfProject>     \n"
                  "                                                            \n"
                  "     -BUILDTYPE          <RELEASE | DEBUG | DEPENDENCY>     \n"
                  "         -PROJECT        <NetworkFriendlyPathOfProject>     \n"
                  "         -TARGET         <PC OSX IOS ANDROID PS3 TOOLS>     \n"
                  "         -INPUT          <RelativeToProjectPath.%s>.txt     \n"
                  "------------------------------------------------------------\n",
                 g_pCompilerName,
                 __TIME__, __DATE__,
                 getKeyObject().getCompiledExt() );
        
        return FALSE;
    }
    
    //
    // Go through the parameters of the command line
    //
    xstring InputName;
    m_PlatformMask = 0;
    for( s32 i=0; i<CmdLine.GetCommandCount(); i++ )
    {
        xstring CmdName = CmdLine.GetCmdName(i);
        if( x_stricmp( CmdName, "TARGET" ) == 0 )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            s32 Count  = CmdLine.GetCmdArgumentCount(i);
            
            for( s32 i=0; i<Count; i++ )
            {
                const char* pString = CmdLine.GetArgument( Offset + i );
                xbool       bFound  = FALSE;
                
                // Go through all the platforms and pick up the right one
                for( xplatform p=xplatform(X_PLATFORM_NULL+1); p < X_PLATFORM_COUNT; p = xplatform((s32)p + 1) )
                {
                    if( x_stricmp( pString, x_PlatformString( p ) ) == 0 )
                    {
                        platform& NewTarget   = m_Target[p];
                        
                        if( NewTarget.m_bValid )
                        {
                            x_throw( "ERROR: The same platform [%s] was enter multiple times", pString );
                        }
                        
                        NewTarget.m_Platform  = p;
                        NewTarget.m_bValid    = TRUE;
                        
                        bFound = TRUE;
                        
                        // Create the plaform Mask
                        m_PlatformMask |= (1<<p);
                        break;
                    }
                }
                
               if(  bFound == FALSE )
               {
                   x_throw( "ERROR: Platform not supported (%s)\n", pString );
               }
            }
        }
        else if( x_stricmp( CmdName, "PROJECT" ) == 0 )
        {
            m_ProjectPath = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
            m_ProjectPath.SearchAndReplace( "\\", "/" );
        }
        else if( x_stricmp( CmdName, "INPUT" ) == 0 )
        {
            InputName = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
            InputName.SearchAndReplace( "\\", "/" );

            // Verify full extension
            xstring FullExtension;
            FullExtension.Format( ".%s", getKeyObject().getCompiledExt() );
            
            const s32 iExtension = InputName.FindI( FullExtension );
            if( iExtension == -1 )
            {
                x_throw( "ERROR: Invalid input key file, it must have extension %s we got [%s]", (const char*)FullExtension, (const char*)InputName );
            }
            
            // Store the name so that we can print it nicely
            x_splitpath( InputName, NULL, NULL, g_RscFileName, NULL);
        }
        else if( x_stricmp( CmdName, "BUILDTYPE" ) == 0 )
        {
            xstring BuildType;
            
            BuildType = CmdLine.GetArgument( CmdLine.GetCmdArgumentOffset(i) );
            
            BuildType.MakeUpper();
            
            if( BuildType == X_STR("RELEASE" ) )
            {
                m_BuildType = BUILDTYPE_RELEASE;
            }
            else if( BuildType == X_STR("DEBUG" ) )
            {
                m_BuildType = BUILDTYPE_DEBUG;
            }
            else if( BuildType == X_STR("DEPENDENCY" ) )
            {
                m_BuildType = BUILDTYPE_DEPENDENCY;
            }
            else
            {
                x_throw( "ERROR: Build Type not supported (%s).", (const char*)BuildType );
            }
        }
        else if ( x_stricmp( CmdName, "OVERWRITE_DEFAULTS" ) == 0 )
        {
            m_BuildType = BUILDTYPE_SAVE_KEY_DEFAULTS;
        }
        else
        {
            // We must have forgotten a switch because we should not be able to reach this point
            ASSERT( 0 );
        }
    }
    
    //
    // Set the key file default property directory
    //
    // m_KeyDefaultPath.Format( "%s/GameData/CompilerKeys/Defaults/%s.txt", (const char*)m_ProjectPath, getKeyObject().getCompilerName() );

    //
    // If the user is asking to just save the key defaults lets just do that
    //
    if( m_BuildType == BUILDTYPE_SAVE_KEY_DEFAULTS )
    {
        // There is nothing else to get from here so lets just return
        return TRUE;
    }
    
    //
    // Compute the name of all paths
    //
    char KeyFileDrive[xfile::MAX_DRIVE];
    char KeyFileDir[xfile::MAX_DIR];
    char KeyFileFilename[xfile::MAX_FNAME];
    char KeyFileExt[xfile::MAX_EXT];

    x_splitpath(InputName, KeyFileDrive, KeyFileDir, KeyFileFilename, KeyFileExt);
    
    if( x_stristr( KeyFileExt, ".txt" ) )
    {
        x_throw( "ERROR: While the all key files should have extension .txt at the end you should not enter it. [%s]", (const char*)InputName );
    }
    
    // KeyFile full path name
    m_KeyFilePath.Format( "%s/%s.txt", 
        (const char*)m_ProjectPath, 
        (const char*)InputName );

    m_InputPath.Format( "%s/%s", 
        (const char*)m_ProjectPath,
        (const char*)KeyFileDir );

    m_ProjectPathLength = x_strlen(m_ProjectPath);

    //
    // Split the InputFileName form its guid (if any)
    // The name of the key file should be: NameOfTheAsset--GUID
    //
    char* pGuid = NULL;
    {
        char Buffer[xfile::MAX_FNAME];
        const char* pFileName=NULL;
        
        // Copy the actual file name
        x_strcpy( Buffer, xfile::MAX_FNAME, KeyFileFilename );
        
        pGuid = x_stristr( Buffer, "--" );
        if( pGuid == NULL )
        {
            pGuid = Buffer;
            
            pFileName = "";
        }
        else
        {
            pFileName = Buffer;
            
            // Remove the Guid from the file name
            pGuid[0] = 0;
            pGuid[1] = 0;
            pGuid = &pGuid[2];
        }

        // Now lets backup the name of the asset
        m_RscFileName.Copy( pFileName );

        // Convert the guid string to a numeric value
        m_RscGuid.SetFromAlphaString( pGuid );
        
        // Check the rule that says that resources guids dont use the first bit
        if( !(m_RscGuid.m_Guid&1) )
        {
            x_throw( "ERROR: The reource GUID does NOT have the low bit set to 1. This is not allowed. [Guid:%s Hex:0x%LX]", pGuid, m_RscGuid.m_Guid );
        }
    }
    
    //
    // Create the game data path
    //
    m_GameDataPath.Format( "%s/GameData", (const char*)m_ProjectPath );
    
    //
    // Set the dependency path
    //
    m_ExternalCompilerKeysPath.Format( "%s/Compilation/Dependency/ExternalCompilerKeys", (const char*)m_ProjectPath );
    m_IntermediateDataPath.Format( "%s/Compilation/Dependency/IntermediateData", (const char*)m_ProjectPath );
    m_RscBuildDependsPath.Format( "%s/Compilation/Dependency/RscBuildDepends", (const char*)m_ProjectPath );
    m_ImportedPath.Format( "%s/Compilation/Dependency/ImportedFiles", (const char*)m_ProjectPath );
    m_RscImportedPath.Format( "%s/%s--%s.%s", 
        (const char*)m_ImportedPath,
        (const char*)m_RscFileName,
        (const char*)m_RscGuid.GetAlphaString(),
        (const char*)getKeyObject().getCompiledExt() );
    
    // Make sure that the imported path for this resource exist
    if( x_io::PathExists( m_RscImportedPath ) == FALSE )
        if( x_io::MakeDir( m_RscImportedPath ) == FALSE )
            x_throw("ERROR: Unable to create the import path for this resource [%s]", (const char*)m_RscImportedPath );    

    //
    // Create the log path
    //
    m_LogPath.Format( "%s/Compilation/Dependency/Logs/%s", (const char*)m_ProjectPath, m_BuildType == BUILDTYPE_RELEASE?"RELEASE":"DEBUG" );
    
    //
    // Create the paths for the output and any external dependency key files
    //
    for( s32 i=0; i<m_Target.getCount(); i++ )
    {
        platform& Target = m_Target[i];
        
        // Skip this target if we dont need to work on it
        if( Target.m_bValid == FALSE )
            continue;

        const char* pPlatformString = x_PlatformString( Target.m_Platform );
        
        Target.m_OutputReleaseDir.Format( "%s/Compilation/%s/Release",
                                          (const char*)m_ProjectPath,
                                          pPlatformString );
        
        Target.m_OutputDebugDir.Format( "%s/Compilation/%s/Debug",
                                         (const char*)m_ProjectPath,
                                         pPlatformString );

        Target.m_DependencyDir.Format( "%s/Compilation/%s/Dependency/%s_%s",
                                       (const char*)m_ProjectPath,
                                       pPlatformString,
                                       KeyFileFilename,
                                       KeyFileExt );
    }
    
    //
    // Make sure that the paths exists
    //
    
    return TRUE;
}

//--------------------------------------------------------------------------

void compiler_base::HandleExceptionMessage( const char* pMsg ) const
{
    if( x_strncmp( pMsg, "ERROR:",   6 ) == 0 )
    {
        x_printf( "%s", pMsg );
    }
    else
    {
        x_printf( "ERROR: %s", pMsg );
    }
}

//--------------------------------------------------------------------------

xbool compiler_base::Parse( s32 argc, const char *argv[] )
{
    xbool ret = 1;
    
    x_try;
    {
        ret = InternalParse( argc, argv );
    }
    x_catch_begin;
    {
        HandleExceptionMessage( x_ExceptionMessage() );
        ret = 0;
    }
    x_catch_end;
    
    return ret;
}

//--------------------------------------------------------------------------

void compiler_base::SaveKeyDefaults( void )
{
    compiler_key_object& Key = getKeyObject();
 
    Key.SaveDefaults( m_ProjectPath );
}

//--------------------------------------------------------------------------

void compiler_base::LoadKeyDefaults( void )
{
    compiler_key_object& Key = getKeyObject();
    
    Key.LoadDefaults( m_ProjectPath );
}

//--------------------------------------------------------------------------

void compiler_base::LoadKeyFile( void )
{
    compiler_key_object& Key = getKeyObject();
    
    xtextfile TextFile;
    
    TextFile.OpenForReading( m_KeyFilePath );
    if( TextFile.ReadRecord() )
    {
        Key.Load( TextFile );
    }
    else
    {
        x_throw( "ERROR: Unable to read any records for the key file [%s]", (const char*)m_KeyFilePath );
    }
    
    TextFile.Close();
}

//--------------------------------------------------------------------------

void compiler_base::SaveAssetDependencyList( void )
{
    xtextfile TextFile;
    xstring   Path;
   
    Path.Format( "%s/%s.%s.depens",
                (const char*)m_RscBuildDependsPath,
                (const char*)m_RscGuid.GetAlphaString(),
                getKeyObject().getCompiledExt() );
    
    TextFile.OpenForWriting( Path, xtextfile::FLAGS_BINARY );
    
    //
    // Get the asset dependencies
    //
    {
        xarray2<compiler_key_object::dependency> lDepList;
        
        // Get all the asset dependencies
        getKeyObject().getAssetDependencies( lDepList );
        
        const_ref( rDepList, lDepList );
        TextFile.WriteRecord( "Assets", lDepList.getCount() );
        for( const compiler_key_object::dependency& Dep : rDepList )
        {
            TextFile.WriteField ( "AssetPath:s", (const char*)Dep.m_FilePath );
            TextFile.WriteField ( "Platform:h",  Dep.m_Plaforms );
            TextFile.WriteField ( "ForceKeyBuild:h",  Dep.m_bForceKeyBuild );
            TextFile.WriteLine  ();
        }
    }
    
    //
    // Save External Key Files
    //
    {
        mutable_ref( rExternalDepKeys, m_lExternalDepKeys );

        TextFile.WriteRecord( "ExternalKeys", rExternalDepKeys.getCount() );
        for( xstring& Path : rExternalDepKeys )
        {
            Path.SearchAndReplace( "\\", "/" );

            // Remove the ".txt" extension out of it
            Path[Path.GetLength()-4] = 0;
            
            // Dont save the dependency with the full path. Everything is always relative to the project directory
            ASSERT( x_stristr( Path, m_ProjectPath ) == 0 );
            
            TextFile.WriteField ( "KeyFilePath:s", (const char*)Path );
            TextFile.WriteLine  ();
        }
    }
    
    TextFile.Close();
}

//--------------------------------------------------------------------------

xstring compiler_base::getFinalResourceName( const platform& Target )  const
{
    xstring FilePath;
    
    if( m_BuildType == BUILDTYPE_RELEASE )
    {
        FilePath.Format("%s/%s",
                        (const char*)Target.m_OutputReleaseDir,
                        (const char*)m_RscGuid.GetAlphaString() );
    }
    else if( m_RscFileName.IsEmpty() )
    {
        FilePath.Format("%s/%s.%s",
                        (const char*)Target.m_OutputDebugDir,
                        (const char*)m_RscGuid.GetAlphaString(),
                        (const char*)getKeyObject().getCompiledExt() );
    }
    else
    {
        FilePath.Format("%s/%s--%s.%s",
                        (const char*)Target.m_OutputDebugDir,
                        (const char*)m_RscFileName,
                        (const char*)m_RscGuid.GetAlphaString(),
                        (const char*)getKeyObject().getCompiledExt() );
    }
    
    return FilePath;
}

//--------------------------------------------------------------------------

xbool compiler_base::Compile( void )
{
    x_try;
    
    //
    // We are going to guess that we are multicore focus
    //
    g_Scheduler.Init();
    
    //
    // Lets get going
    //
    x_printf( "INFO: Starting..." );
    m_Timmer.Start();
    
    if( m_BuildType == BUILDTYPE_SAVE_KEY_DEFAULTS )
    {
        SaveKeyDefaults();
    }
    else
    {
        // Load defaults for this key file
        LoadKeyDefaults();
        
        // Load the key instance 
        LoadKeyFile();
        
        // Tell the compiler to start Doing its job
        onCompile();
        
        // Save the Asset dependency list
        SaveAssetDependencyList();
    }
    
    // Get some info to the user
    x_printf("INFO: End. %.1f secs", m_Timmer.TripSec() );
    
    x_catch_begin;
    {
        HandleExceptionMessage( x_ExceptionMessage() );
        return FALSE;
    }
    x_catch_end;
    
    return TRUE;
}

//--------------------------------------------------------------------------

xstring compiler_base::RemoveProjectPath( const xstring& Src ) const
{
    static const char pGameData[]       = "gamedata/";
    static const char pDependency[ ]    = "Compilation/dependency/";
    s32               Index = -1;
    xstring           String;

    String.Copy( Src );

    // Normalize path
    String.SearchAndReplace( "\\", "/" );

    // try to find game data
    Index = String.FindI( pGameData );
    if ( Index != -1 )
    {
        String.Copy( &String[Index] );
        return String;
    }

    Index = String.FindI( pDependency );
    if ( Index != -1 )
    {
        String.Copy( &String[Index] );
        return String;
    }

    ASSERT(0);
    return String;
}

//--------------------------------------------------------------------------
u64 compiler_base::getGuidFromFilePath( const xstring& RSCFilePath )
{
    const char* pStr = x_strstr( RSCFilePath, "--" );
    if ( pStr == NULL ) return 0;

    char Buff[16];
    x_strncpy( Buff, &pStr[2], 14, 32 );

    xguid Guid;

    Guid.SetFromAlphaString( Buff );

    return Guid.m_Guid;
}

//--------------------------------------------------------------------------

void compiler_base::AddExternalDependencyKey( const xstring& FilePath ) const
{
    xscope_atomic( m_CriticalSection );

    if ( FilePath.FindI( ".txt" ) == -1 )
    {
        xstring Temp;

        Temp.Format( "%s.txt", (const char*)FilePath );

        mutable_ref( rEntry, m_lExternalDepKeys );
        for ( const xstring& DepKey : rEntry )
            if ( x_stricmp( DepKey, Temp ) == 0 )
                return;

        rEntry->append( ).Copy(Temp);
    }
    else
    {
        mutable_ref( rEntry, m_lExternalDepKeys );
        for ( const xstring& DepKey : rEntry )
            if ( x_stricmp( DepKey, FilePath ) == 0 )
                return;

        rEntry->append( ).Copy(FilePath );
    }
}

//--------------------------------------------------------------------------

xstring compiler_base::getImportedPathForKeyFile( const xstring& KeyFileName ) const
{
    ASSERT( KeyFileName.Find("--") >= 0 );

    // Now lets 
    xstring ImportPathForResource;

    if( KeyFileName.FindI(".txt") != -1 )
    {
        char FileName[ xfile::MAX_FNAME ];

        // Get the actual file name
        x_splitpath( KeyFileName, NULL, NULL, FileName, NULL );
        ImportPathForResource.Format( "%s/%s",
                                      (const char*)m_ImportedPath,
                                      FileName );
    }
    else
    {
        char FileName[ xfile::MAX_FNAME ];
        char ExtFileName[ xfile::MAX_EXT ];

        // Get the actual file name
        x_splitpath( KeyFileName, NULL, NULL, FileName, ExtFileName );
        ImportPathForResource.Format( "%s/%s%s",
                                      (const char*)m_ImportedPath,
                                      FileName,
                                      ExtFileName );
    
    }

    return ImportPathForResource ;
}

//--------------------------------------------------------------------------

void compiler_base::SaveImportedFile( const char* pFileName, imported_file_fn CallBack ) const
{
    xstring FinalPath;

    FinalPath.AppendFormat( "%s/%s", (const char*) m_RscImportedPath, pFileName );

    CallBack( FinalPath );

    // TODO:
    // Now check that the CRC have change or not
    // if they have change delete old file and move the new one in
}
