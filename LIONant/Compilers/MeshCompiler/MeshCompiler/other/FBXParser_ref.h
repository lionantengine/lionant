#pragma once

#include <fbxsdk.h>
#include <map>
#include <vector>
#include "Material.h"
#include "x_Base.h"
#include "Scene.h"

struct FbxMeshInfo;
struct Material;

class fbx_parser
{
public:
                    fbx_parser          ( void );
                    ~fbx_parser         ( void );

    void            Initialize          ( void );
    xbool           Load                ( const char* FBXFilePath );

protected:
    struct mesh_info
    {
        FbxMesh*    m_Mesh;
        s32         m_VertexStartOffset;
        s32         m_VertexCount;
        s32*        m_Polygons;

        mesh_info   ( FbxMesh* Mesh, s32 Offset, s32 Count )
        {
            m_Mesh = Mesh;
            m_VertexStartOffset = Offset;
            m_VertexCount = Count;
        }
    };

    struct node_info
    {
        FbxNode*    m_Node;
        s32         m_Parent;
    };

    typedef xarray<animation::key_frame> key_frame_array;

    struct track_info
    {
        FbxNode*    m_Node;
        f32         m_Start;
        f32         m_Stop;
        xbool       m_HasTranslation;
        xbool       m_HasRotation;
        xbool       m_HasScaling;
        f32         m_FrameRate;
        s32         m_FramesCount;

        key_frame_array
                    m_KeyFrames;

        track_info  ( void )
        {
            m_Node              = NULL;
            m_Start             = FLT_MAX;
            m_Stop              = -1.0f;
            m_HasTranslation    = FALSE;
            m_HasRotation       = FALSE;
            m_HasScaling        = FALSE;
            m_FrameRate         = 30.0f;
            m_FramesCount       = 0;
        }

        void Merge  ( const track_info& Info )
        {
            m_Start             = x_Min(Info.m_Start, m_Start);
            m_Stop              = x_Max(Info.m_Stop, m_Stop);
            m_FrameRate         = x_Max(Info.m_FrameRate, m_FrameRate);
            m_HasTranslation    = m_HasTranslation || Info.m_HasTranslation;
            m_HasRotation       = m_HasRotation || Info.m_HasRotation;
            m_HasScaling        = m_HasScaling || Info.m_HasScaling;
        }
    };

    typedef std::map<FbxGeometry*, mesh_info* > MeshMap;
    typedef std::vector<mesh_info* > MeshVector;
    typedef std::map<FbxSurfaceMaterial *, Material*> MaterialMap;
    typedef std::map<std::string, TextureFileInfo> TextureMap;
    typedef std::map<FbxNode*, s32> TrackMap;
    typedef std::map<FbxGeometry*, FbxNode*> GeometryMap;

    void            CheckNode           ( FbxNode* const& Node );
    void            CheckNodes          ( void );
    void            CollectMeshes       ( void );
    void            CollectMaterials    ( void );
    Material*       CreateMaterial      ( FbxSurfaceMaterial * material );
    void            GetNormal           ( FbxMesh* Mesh, s32 VertexIndex, s32 VertexCounter, xvector3d& Normal );
    void            GetUV               ( FbxMesh* Mesh, s32 VertexIndex, s32 UVIndex, f32& u, f32& v );
    void            GetColor            ( FbxMesh* Mesh, s32 VertexIndex, s32 VertexCounter);
    void            GetWeights          ( FbxMesh* Mesh, s32 VertexIndex, f32 Weights[3] );

    template<int n> inline static void set(float * const &dest, const FbxDouble * const &source)
    {
        for (int i = 0; i < n; i++)
            dest[i] = (float)source[i];
    }

    template<int n> inline static void set(double * const &dest, const FbxDouble * const &source)
    {
        for (int i = 0; i < n; i++)
            dest[i] = source[i];
    }

    template<class T> inline static void add_if_not_null(std::vector<T *> &dst, T * const &value) {
        if (value != 0)
            dst.push_back(value);
    }

    void            AddTextures         (std::vector<Texture *>& textures, const FbxProperty &prop,  const Texture::Usage &usage);
    Texture*        CreateTexture       (FbxFileTexture* texture, const Texture::Usage &usage);
    void            CollectTextureBounds( FbxNode* Node = NULL );
    void            ConvertMesh         ( void );
    xbool           Generate            ( void );
    u32             ComputeSubMeshCount ( FbxMesh* Mesh );
    void            ProcessSkeletons    ( void );
    void            ProcessSkeleton     ( xarray<node_info>& Joints, FbxNode* Node, s32 ParentIndex );
    FbxAMatrix      GetGeometryTransform( FbxNode* Node );
    s32             FindJointIndex      ( const FbxNode* Node );
    void            ConvertAnimation    ( void );
    void            GetTrackInfo        ( FbxAnimCurve* Curve, track_info& Info, f32 AnimStart, f32 AnimStop );
    char*           AllocString         ( const char* String );

    FbxManager*     m_Manager;
    FbxScene*       m_Scene;
    FbxAxisSystem   m_AxisSystem;
    FbxSystemUnit   m_SystemUnits;
    MeshMap         m_MeshMap;
    MeshVector      m_Meshes;

    MaterialMap     m_MaterialMap;
    TextureMap      m_TextureMap;

    mesh*                   m_Mesh;
    xarray<mesh::vertex>    m_Vertices;
    animation*              m_Animation;
    xarray<node_info>       m_Joints;

    xarray<track_info>      m_AnimTracks;
    TrackMap                m_TrackMap;
    GeometryMap             m_GeometryMap;
};

