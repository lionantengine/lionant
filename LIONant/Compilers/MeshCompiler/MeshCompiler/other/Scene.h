#pragma once
#include <x_base.h>

#define MESH_FILE_VERSION   0x00000001

struct transform
{
    xvector3d       m_Translation;
    xvector3d       m_Scale;
    xquaternion     m_Rotation;

    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize( m_Translation.m_X );
        SerialFile.Serialize( m_Translation.m_Y );
        SerialFile.Serialize( m_Translation.m_Z );
        SerialFile.Serialize( m_Rotation.m_X );
        SerialFile.Serialize( m_Rotation.m_Y );
        SerialFile.Serialize( m_Rotation.m_Z );
        SerialFile.Serialize( m_Rotation.m_W );
        SerialFile.Serialize( m_Scale.m_X );
        SerialFile.Serialize( m_Scale.m_Y );
        SerialFile.Serialize( m_Scale.m_Z );
    }

    void Clear( void )
    {
        m_Translation.Zero();
        m_Scale.Set(1.0f, 1.0f, 1.0f);
        m_Rotation.Identity();
    }
};

struct skeleton
{
    struct joint
    {
        xserialfile::ptr<char>  m_pName;
        s32                     m_Parent;
        transform               m_BindPose;
        transform               m_Local;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize    ( m_pName, x_strlen(m_pName.m_Ptr) + 1, xserialfile::FLAGS_DEFAULT );
            SerialFile.Serialize    ( m_Parent );
            m_BindPose.SerializeIO  ( SerialFile );
            m_Local.SerializeIO     ( SerialFile );
        }

        void Clear( void )
        {
            m_pName.m_Ptr   = NULL;
            m_Parent        = -1;
        }
    };

    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize( m_nJoints );
        SerialFile.Serialize( m_pJoint, m_nJoints, xserialfile::FLAGS_DEFAULT );
    }

    void Clear( void )
    {
        m_nJoints       = 0;
        m_pJoint.m_Ptr  = NULL;
    }

public:
    
    s32                     m_nJoints;
    xserialfile::ptr<joint> m_pJoint;
};

struct mesh
{
    enum
    {
        MAX_BONE_PER_VERTEX = 4
    };
    
    struct vertex
    {
        xvector3d       m_Position;
        xvector3d       m_Normal;
        f32             m_U, m_V;
        f32             m_W[ MAX_BONE_PER_VERTEX ];
        s32             m_B[ MAX_BONE_PER_VERTEX ];

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_Position.m_X );
            SerialFile.Serialize( m_Position.m_Y );
            SerialFile.Serialize( m_Position.m_Z );

            SerialFile.Serialize( m_Normal.m_X );
            SerialFile.Serialize( m_Normal.m_Y );
            SerialFile.Serialize( m_Normal.m_Z );

            SerialFile.Serialize( m_U );
            SerialFile.Serialize( m_V );

            SerialFile.Serialize( m_W, MAX_BONE_PER_VERTEX );
            SerialFile.Serialize( m_B, MAX_BONE_PER_VERTEX );
        }

        void Clear( void )
        {
            m_Position.Zero();
            m_Normal.Zero();
            m_U         = 0.0f;
            m_V         = 0.0f;

            for ( s32 i = 0; i < MAX_BONE_PER_VERTEX; i++ )
            {
                m_W[i]      = 0.0f;
                m_B[i]      = -1;
            }
        }
    };

    struct sub_mesh
    {
        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_Name, x_strlen(m_Name.m_Ptr) + 1, xserialfile::FLAGS_DEFAULT );
            SerialFile.Serialize( m_iInformed );
            SerialFile.Serialize( m_IndexOffset );
            SerialFile.Serialize( m_nIndices );
            SerialFile.Serialize( m_VertexOffset );
            SerialFile.Serialize( m_nVertices );
        }

        void Clear( void )
        {
            m_Name.m_Ptr    = NULL;
            m_iInformed     = -1;
            m_IndexOffset   = 0;
            m_nIndices      = 0;
            m_VertexOffset  = 0;
            m_nVertices     = 0;
        }

        xserialfile::ptr<char>  m_Name;

        s32                     m_iInformed;

        s32                     m_IndexOffset;
        s32                     m_nIndices;

        s32                     m_VertexOffset;
        s32                     m_nVertices;
    };

    struct material
    {
        char*       m_pTexture;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize(m_pTexture, x_strlen(m_pTexture)+1, xserialfile::FLAGS_DEFAULT);
        }
    };

    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize    ( m_nSubMeshes );
        SerialFile.Serialize    ( m_pSubmesh, m_nSubMeshes, xserialfile::FLAGS_DEFAULT );

        //SerialFile.Serialize  ( m_nMaterials );
        //SerialFile.Serialize  ( m_pMaterial, m_nMaterials, xserialfile::FLAGS_DEFAULT );

        SerialFile.Serialize    ( m_nVertices );
        SerialFile.Serialize    ( m_pVertex, m_nVertices, xserialfile::FLAGS_DEFAULT );

        SerialFile.Serialize    ( m_nIndices );
        SerialFile.Serialize    ( m_pIndex, m_nIndices, xserialfile::FLAGS_DEFAULT );

        SerialFile.Serialize    ( m_Skeleton );
    }

    mesh( void )
    {
    }

    // This is the loading constructor by the time is call the file already loaded
    mesh(xserialfile& SerialFile)
    {
    }

    void Clear( void )
    {
        m_nSubMeshes        = 0;
        m_pSubmesh.m_Ptr    = NULL;
        m_nMaterials        = 0;
        m_pMaterial.m_Ptr   = NULL;
        m_nVertices         = 0;
        m_pVertex.m_Ptr     = NULL;
        m_nIndices          = 0;
        m_pIndex.m_Ptr      = NULL;
    }

public:
    
    s32                         m_nSubMeshes;
    xserialfile::ptr<sub_mesh>  m_pSubmesh;

    s32                         m_nMaterials;
    xserialfile::ptr<material>  m_pMaterial;

    s32                         m_nVertices;
    xserialfile::ptr<vertex>    m_pVertex;

    s32                         m_nIndices;
    xserialfile::ptr<u16>       m_pIndex;

    skeleton                    m_Skeleton;
};

struct animation_data
{
    struct key_frame
    {
        f32                 m_Time;
        xvector3d           m_Translation;
        xvector3d           m_Scale;
        f32                 m_Rotation[4];

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_Time );
            SerialFile.Serialize( m_Translation.m_X );
            SerialFile.Serialize( m_Translation.m_Y );
            SerialFile.Serialize( m_Translation.m_Z );
            SerialFile.Serialize( m_Rotation[0] );
            SerialFile.Serialize( m_Rotation[1] );
            SerialFile.Serialize( m_Rotation[2] );
            SerialFile.Serialize( m_Rotation[3] );
            SerialFile.Serialize( m_Scale.m_X );
            SerialFile.Serialize( m_Scale.m_Y );
            SerialFile.Serialize( m_Scale.m_Z );
        }

        void Clear( void )
        {
            m_Time          = 0.0f;
            m_Translation.Zero();
            m_Scale.Zero();
            m_Rotation[0]   = 0.0f;
            m_Rotation[1]   = 0.0f;
            m_Rotation[2]   = 0.0f;
            m_Rotation[3]   = 1.0f;
        }
    };

    struct animation_track
    {
        s32                             m_JointIndex;
        s32                             m_nKeyFrames;
        xserialfile::ptr<key_frame>     m_pKeyFrame;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_JointIndex );
            SerialFile.Serialize( m_nKeyFrames );
            SerialFile.Serialize( m_pKeyFrame, m_nKeyFrames, xserialfile::FLAGS_DEFAULT );
        }

        void Clear( void )
        {
            m_JointIndex        = -1;
            m_nKeyFrames        = 0;
            m_pKeyFrame.m_Ptr   = NULL;
        }
    };

    struct animation_clip
    {
        xserialfile::ptr<char>                  m_pName;
        f32                                     m_Duration;
        s32                                     m_nTracks;
        xserialfile::ptr<animation_track>       m_pTrack;

        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_pName, x_strlen(m_pName.m_Ptr) + 1, xserialfile::FLAGS_DEFAULT );
            SerialFile.Serialize( m_Duration );
            SerialFile.Serialize( m_nTracks );
            SerialFile.Serialize( m_pTrack, m_nTracks, xserialfile::FLAGS_DEFAULT );
        }

        void Clear( void )
        {
            m_pName.m_Ptr   = NULL;
            m_nTracks       = 0;
            m_pTrack.m_Ptr  = NULL;
            m_Duration      = 0.0f;
        }
    };

    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize( m_nClips );
        SerialFile.Serialize( m_pClip, m_nClips, xserialfile::FLAGS_DEFAULT );
    }

    void Clear( void )
    {
        m_nClips       = 0;
        m_pClip.m_Ptr  = NULL;
    }

    s32                                 m_nClips;
    xserialfile::ptr<animation_clip>    m_pClip;


    animation_data( xserialfile& SerialFile )
    {
    }

    animation_data( void )
    {
    }
};