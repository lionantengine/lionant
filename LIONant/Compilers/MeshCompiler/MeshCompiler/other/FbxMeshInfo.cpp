#include "FbxMeshInfo.h"

std::string FbxMeshInfo::GetID(FbxMesh * const& Mesh)
{
    static int idCounter = 0;
    const char *name = Mesh->GetName();
    if (name != 0 && strlen(name) > 1)
        return name;
    else
    {
        std::string ID = "shape";
        char Counter[100] = {0};
        sprintf(Counter, "%d", ++idCounter);
        ID += Counter;
        return ID;
    }
}

FbxMeshInfo::FbxMeshInfo( FbxMesh* Mesh )
    : m_Mesh(Mesh)
    , m_ID(GetID(Mesh))
    , m_MaxVertexBlendWeightCount(4)
    , m_VertexBlendWeightCount(0)
    , m_ForceMaxVertexBlendWeightCount(false)
    , m_BonesOverflow(false)
    , m_MaxNodePartBoneCount(12)
    , m_UsePackedColors(false)
    , m_PolyCount(Mesh->GetPolygonCount())
    , m_PointCount(Mesh->GetControlPointsCount())
    , m_Points(Mesh->GetControlPoints())
    , m_UVCount((unsigned int)(Mesh->GetElementUVCount() > 8 ? 8 : Mesh->GetElementUVCount()))
    , m_ElementMaterialCount(Mesh->GetElementMaterialCount())
    , m_MeshPartCount(0)
    , m_Skin((m_MaxNodePartBoneCount > 0 && m_MaxVertexBlendWeightCount > 0 && (unsigned int)Mesh->GetDeformerCount(FbxDeformer::eSkin) > 0) ? static_cast<FbxSkin*>(Mesh->GetDeformer(0, FbxDeformer::eSkin)) : 0)
    , m_PointBlendWeights(NULL)
    , m_PolyPartMap(new unsigned int[m_PolyCount])
    , m_PolyPartBonesMap(new unsigned int[m_PolyCount])
    , m_PartUVBounds(NULL)
    , m_Normals(NULL)
    , m_NormalIndices(NULL)
    , m_NormalOnPoint(false)
    , m_Tangents(NULL)
    , m_TangentIndices(NULL)
    , m_TangentOnPoint(false)
    , m_Binormals(NULL)
    , m_BinormalIndices(NULL)
    , m_BinormalOnPoint(false)
    , m_Colors(NULL)
    , m_ColorIndices(NULL)
    , m_ColorOnPoint(false)
{
    for ( int i = 0; i < 8; i++ )
    {
        m_UVs[i]        = NULL;
        m_UVIndices[i]  = NULL;
        m_UVOnPoint[i]  = false;
    }

    m_MeshPartCount = CalcMeshPartCount();
    m_PartBones = std::vector<BlendBonesCollection>(m_MeshPartCount, BlendBonesCollection(m_MaxNodePartBoneCount));
    m_PartUVBounds = m_MeshPartCount * m_UVCount > 0 ? new float[4 * m_MeshPartCount * m_UVCount] : 0;
    memset(m_PolyPartMap, -1, sizeof(unsigned int) * m_PolyCount);
    memset(m_PolyPartBonesMap, 0, sizeof(unsigned int) * m_PolyCount);
    if (m_PartUVBounds)
    {
        memset(m_PartUVBounds, -1, sizeof(float) * 4 * m_MeshPartCount * m_UVCount);
    }

    if (m_Skin)
    {
        CollectVertexBlendWeights();
        CollectMeshPartsAndBones();
    }
    else
    {
        CollectMeshParts();
    }

    PrepareAttributes();
    CacheAttributes();
    CollectUVInfo();
}


FbxMeshInfo::~FbxMeshInfo(void)
{
}

unsigned int FbxMeshInfo::CalcMeshPartCount()
{
    int mp, mpc = 0;
    for (unsigned int poly = 0; poly < m_PolyCount; poly++)
    {
        mp = -1;
        for (int i = 0; i < m_ElementMaterialCount && mp < 0; i++)
        {
            mp = m_Mesh->GetElementMaterial(i)->GetIndexArray()[poly];
        }
        if (mp >= mpc)
        {
            mpc = mp+1;
        }
    }
    if (mpc == 0)
        mpc = 1;
    return mpc;
}

// Force it this here for now...
typedef int x_compare_fn( const void* pItem1, const void* pItem2 );
void x_qsort( const void*     pBase,          // Address of first item in array.
    int             NItems,         // Number of items in array.
    int             ItemSize,       // Size of one item.
    x_compare_fn*   pCompare );     // Compare function.

void FbxMeshInfo::CollectVertexBlendWeights( void )
{
    m_PointBlendWeights = new std::vector<BlendWeight>[m_PointCount];
    const int &clusterCount = m_Skin->GetClusterCount();
    // Fetch the blend weights per control point
    for (int i = 0; i < clusterCount; i++)
    {
        const FbxCluster * const &cluster = m_Skin->GetCluster(i);
        const int &indexCount = cluster->GetControlPointIndicesCount();
        const int * const &clusterIndices = cluster->GetControlPointIndices();
        const double * const &clusterWeights = cluster->GetControlPointWeights();
        for (int j = 0; j < indexCount; j++)
        {
            if (clusterIndices[j] < 0 || clusterIndices[j] >= (int)m_PointCount || clusterWeights[j] == 0.0)
            {
                continue;
            }
            m_PointBlendWeights[clusterIndices[j]].push_back(BlendWeight((float)clusterWeights[j], i));
        }
    }
    // Sort the weights, so the most significant weights are first, remove unneeded weights and normalize the remaining
    bool error = false;
    for (unsigned int i = 0; i < m_PointCount; i++)
    {
#ifdef TARGET_PC
        x_qsort( &m_PointBlendWeights[i][0], m_PointBlendWeights[i].size( ), sizeof( BlendWeight ), []( const void* pA, const void*pB ) -> int
        {
            const BlendWeight& A = *( (const BlendWeight*)pA );
            const BlendWeight& B = *( (const BlendWeight*)pB );
            if ( B < A ) return -1;
            return B > A;
        } );
#else
        std::sort(m_PointBlendWeights[i].begin(), m_PointBlendWeights[i].end(), std::greater<BlendWeight>());
#endif

        if (m_PointBlendWeights[i].size() > m_MaxVertexBlendWeightCount)
        {
            m_PointBlendWeights[i].resize(m_MaxVertexBlendWeightCount);
        }
        float len = 0.f;
        for (std::vector<BlendWeight>::const_iterator itr = m_PointBlendWeights[i].begin(); itr != m_PointBlendWeights[i].end(); ++itr)
        {
            len += (*itr).weight;
        }
        if (len == 0.f)
        {
            error = true;
        }
        else
        {
            for (std::vector<BlendWeight>::iterator itr = m_PointBlendWeights[i].begin(); itr != m_PointBlendWeights[i].end(); ++itr)
            {
                (*itr).weight /= len;
            }
        }
        
        if (m_PointBlendWeights[i].size() > m_VertexBlendWeightCount)
        {
            m_VertexBlendWeightCount = (unsigned int)m_PointBlendWeights[i].size();
        }
    }
    if (m_VertexBlendWeightCount > 0 && m_ForceMaxVertexBlendWeightCount)
    {
        m_VertexBlendWeightCount = m_MaxVertexBlendWeightCount;
    }
}

void FbxMeshInfo::CollectMeshPartsAndBones( void )
{
    std::vector<std::vector<BlendWeight>*> polyWeights;
    for (unsigned int poly = 0; poly < m_PolyCount; poly++)
    {
        int mp = -1;
        for (int i = 0; i < m_ElementMaterialCount && mp < 0; i++)
        {
            mp = m_Mesh->GetElementMaterial(i)->GetIndexArray()[poly];
        }
        if (mp < 0 || mp >= m_MeshPartCount)
        {
            m_PolyPartMap[poly] = -1;
        }
        else
        {
            m_PolyPartMap[poly] = mp;
            const unsigned int polySize = m_Mesh->GetPolygonSize(poly);
            polyWeights.clear();
            for (unsigned int i = 0; i < polySize; i++)
            {
                polyWeights.push_back(&m_PointBlendWeights[m_Mesh->GetPolygonVertex(poly, i)]);
            }
            const int sp = m_PartBones[mp].add(polyWeights);
            m_PolyPartBonesMap[poly] = sp < 0 ? 0 : (unsigned int)sp;
            if (sp < 0)
            {
                m_BonesOverflow = true;
            }
        }
    }
}

void FbxMeshInfo::CollectMeshParts( void )
{
    int mp;
    for (unsigned int poly = 0; poly < m_PolyCount; poly++)
    {
        mp = -1;
        for (int i = 0; i < m_ElementMaterialCount && mp < 0; i++)
        {
            mp = m_Mesh->GetElementMaterial(i)->GetIndexArray()[poly];
        }
        if (mp < 0 || mp >= m_MeshPartCount)
        {
            m_PolyPartMap[poly] = -1;
        }
        else
        {
            m_PolyPartMap[poly] = mp;
        }
    }
}

void FbxMeshInfo::CollectUVInfo( void )
{
    FbxStringList uvSetNames;
    m_Mesh->GetUVSetNames(uvSetNames);
    for (unsigned int i = 0; i < m_UVCount; i++)
        m_UVMapping[i] = uvSetNames.GetItemAt(i)->mString.Buffer();

    if (m_PartUVBounds == 0 || m_UVCount == 0)
        return;
    FbxVector2 uv;
    int mp;
    unsigned int idx, pidx = 0, v = 0;
    for (unsigned int poly = 0; poly < m_PolyCount; poly++)
    {
        mp = m_PolyPartMap[poly];

        const unsigned int polySize = m_Mesh->GetPolygonSize(poly);
        for (unsigned int i = 0; i < polySize; i++)
        {
            v = m_Mesh->GetPolygonVertex(poly, i);
            if (mp >= 0)
            {
                for (unsigned int j = 0; j < m_UVCount; j++)
                {
                    getUV(&uv, j, pidx, v);
                    idx = 4 * (mp * m_UVCount + j);
                    if (*(int*)&m_PartUVBounds[idx]==-1 || uv.mData[0] < m_PartUVBounds[idx])
                        m_PartUVBounds[idx] = (float)uv.mData[0];
                    if (*(int*)&m_PartUVBounds[idx+1]==-1 || uv.mData[1] < m_PartUVBounds[idx+1])
                        m_PartUVBounds[idx+1] = (float)uv.mData[1];
                    if (*(int*)&m_PartUVBounds[idx+2]==-1 || uv.mData[0] > m_PartUVBounds[idx+2])
                        m_PartUVBounds[idx+2] = (float)uv.mData[0];
                    if (*(int*)&m_PartUVBounds[idx+3]==-1 || uv.mData[1] > m_PartUVBounds[idx+3])
                        m_PartUVBounds[idx+3] = (float)uv.mData[1];
                }
            }
            pidx++;
        }
    }
}

void FbxMeshInfo::PrepareAttributes( void )
{
    m_Attributes.hasPosition(true);
    m_Attributes.hasNormal(m_Mesh->GetElementNormalCount() > 0);
    m_Attributes.hasColor((!m_UsePackedColors) && (m_Mesh->GetElementVertexColorCount() > 0));
    m_Attributes.hasColorPacked(m_UsePackedColors && (m_Mesh->GetElementVertexColorCount() > 0));
    m_Attributes.hasTangent(m_Mesh->GetElementTangentCount() > 0);
    m_Attributes.hasBinormal(m_Mesh->GetElementBinormalCount() > 0);
    for (unsigned int i = 0; i < 8; i++)
        m_Attributes.hasUV(i, i < m_UVCount);
    for (unsigned int i = 0; i < 8; i++)
        m_Attributes.hasBlendWeight(i, i < m_VertexBlendWeightCount);
}

void FbxMeshInfo::CacheAttributes( void )
{
    // Cache m_Normals, whether they are indexed and if they are located on control points or polygon points.
    m_Normals = m_Attributes.hasNormal() ? &(m_Mesh->GetElementNormal()->GetDirectArray()) : 0;
    m_NormalIndices = m_Attributes.hasNormal() && m_Mesh->GetElementNormal()->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ? &(m_Mesh->GetElementNormal()->GetIndexArray()) : 0;
    m_NormalOnPoint = m_Attributes.hasNormal() ? m_Mesh->GetElementNormal()->GetMappingMode() == FbxGeometryElement::eByControlPoint : false;

    // Cache m_Tangents, whether they are indexed and if they are located on control points or polygon points.
    m_Tangents = m_Attributes.hasTangent() ? &(m_Mesh->GetElementTangent()->GetDirectArray()) : 0;
    m_TangentIndices = m_Attributes.hasTangent() && m_Mesh->GetElementTangent()->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ? &(m_Mesh->GetElementTangent()->GetIndexArray()) : 0;
    m_TangentOnPoint = m_Attributes.hasTangent() ? m_Mesh->GetElementTangent()->GetMappingMode() == FbxGeometryElement::eByControlPoint : false;

    // Cache m_Binormals, whether they are indexed and if they are located on control points or polygon points.
    m_Binormals = m_Attributes.hasBinormal() ? &(m_Mesh->GetElementBinormal()->GetDirectArray()) : 0;
    m_BinormalIndices = m_Attributes.hasBinormal() && m_Mesh->GetElementBinormal()->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ? &(m_Mesh->GetElementBinormal()->GetIndexArray()) : 0;
    m_BinormalOnPoint = m_Attributes.hasBinormal() ? m_Mesh->GetElementBinormal()->GetMappingMode() == FbxGeometryElement::eByControlPoint : false;

    // Cache colors, whether they are indexed and if they are located on control points or polygon points.
    m_Colors = (m_Attributes.hasColor() || m_Attributes.hasColorPacked()) ? &(m_Mesh->GetElementVertexColor()->GetDirectArray()) : 0;
    m_ColorIndices = (m_Attributes.hasColor() || m_Attributes.hasColorPacked()) && m_Mesh->GetElementVertexColor()->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ? 
        &(m_Mesh->GetElementVertexColor()->GetIndexArray()) : 0;
    m_ColorOnPoint = (m_Attributes.hasColor() || m_Attributes.hasColorPacked()) ? m_Mesh->GetElementVertexColor()->GetMappingMode() == FbxGeometryElement::eByControlPoint : false;

    // Cache m_UVs, whether they are indexed and if they are located on control points or polygon points.
    for (unsigned int i = 0; i < m_UVCount; i++)
    {
        m_UVs[i] = &(m_Mesh->GetElementUV(i)->GetDirectArray());
        m_UVIndices[i] = m_Mesh->GetElementUV(i)->GetReferenceMode() == FbxGeometryElement::eIndexToDirect ? &(m_Mesh->GetElementUV(i)->GetIndexArray()) : 0;
        m_UVOnPoint[i] = m_Mesh->GetElementUV(i)->GetMappingMode() == FbxGeometryElement::eByControlPoint;
    }
}