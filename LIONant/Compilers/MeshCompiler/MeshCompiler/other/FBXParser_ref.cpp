#include "FbxMeshInfo.h"
#include "Material.h"
#include "FBXParser.h"


fbx_parser::fbx_parser(void)
    : m_Manager(NULL)
    , m_Mesh(NULL)
{
}


fbx_parser::~fbx_parser(void)
{
    if ( NULL != m_Scene )
    {
        m_Scene->Destroy();
    }

    if ( NULL != m_Manager )
    {
        //for (std::vector<FbxMeshInfo *>::iterator itr = m_Meshes.begin(); itr != m_Meshes.end(); ++itr)
        //    delete (*itr);
        m_Manager->Destroy();
    }
}

void fbx_parser::Initialize( void )
{
    m_Manager = FbxManager::Create();
    m_Manager->SetIOSettings(FbxIOSettings::Create(m_Manager, IOSROOT));
    m_Manager->GetIOSettings()->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);
}

xbool fbx_parser::Load( const char* FBXFilePath )
{
    FbxImporter* const &Importer = FbxImporter::Create(m_Manager, "");

    Importer->ParseForGlobalSettings(true);
    Importer->ParseForStatistics(true);

    if (Importer->Initialize(FBXFilePath, -1, m_Manager->GetIOSettings())) {
        Importer->GetAxisInfo(&m_AxisSystem, &m_SystemUnits);
        m_Scene = FbxScene::Create(Importer,"__FBX_SCENE__");
        Importer->Import(m_Scene);
    }
    else
    {
        return FALSE;
    }

    Importer->Destroy();

    if (m_Scene)
    {
        //FbxAxisSystem Axis(FbxAxisSystem::eYAxis, FbxAxisSystem::eParityOdd, FbxAxisSystem::eRightHanded);
        //Axis.ConvertScene(m_Scene);
    }

    CheckNodes();
    ProcessSkeletons();
    CollectMeshes();
    ConvertAnimation();
    //CollectMaterials();
    //CollectTextureBounds();

    return NULL != m_Scene ;
}

void fbx_parser::CheckNode( FbxNode* const& Node )
{
    if ( Node->GetGeometry() )
    {
        m_GeometryMap[Node->GetGeometry()] = Node;
    }
    FbxTransform::EInheritType InheritType;
    Node->GetTransformationInheritType(InheritType);
    if (FbxTransform::eInheritRrSs == InheritType)
    {
        Node->SetTransformationInheritType(FbxTransform::eInheritRSrs);
    }

    for (int i = 0; i < Node->GetChildCount(); i++)
    {
        CheckNode(Node->GetChild(i));
    }
}

void fbx_parser::CheckNodes( void )
{
    if ( NULL != m_Scene )
    {
        m_GeometryMap.clear();
        FbxNode* Root = m_Scene->GetRootNode();
        for (int i = 0; i < Root->GetChildCount(); i++)
        {
            CheckNode(Root->GetChild(i));
        }
    }
}

void fbx_parser::ProcessSkeletons( void )
{
    if ( NULL != m_Scene )
    {
        m_Joints.DeleteAllNodes();

        for ( s32 i = 0; i < m_Scene->GetRootNode()->GetChildCount(); i++ )
        {
             FbxNode* Node = m_Scene->GetRootNode()->GetChild(i);
             xarray<node_info> SubSkeleton;
             ProcessSkeleton(SubSkeleton, Node, -1);
             if ( SubSkeleton.getCount() > 0 )
             {
                 if ( 0 == m_Joints.getCount() )
                 {
                     m_Joints.appendList(SubSkeleton.getCount());
                     for ( s32 j = 0; j < SubSkeleton.getCount(); j++ )
                     {
                         m_Joints[j] = SubSkeleton[j];
                     }
                 }
                 else
                 {
                     x_printf("More than one skeleton hierarchy is found!\n");
                 }
             }
        }

        if ( m_Joints.getCount() > 0 )
        {
            m_Animation = x_new(animation, 1, 0);
            m_Animation->m_Skeleton.m_nJoints = m_Joints.getCount();
            m_Animation->m_Skeleton.m_pJoint = x_new(animation::skeleton::joint, m_Joints.getCount(), 0);
            for ( s32 i = 0; i < m_Joints.getCount(); i++ )
            {
                m_Animation->m_Skeleton.m_pJoint[i].m_pName = AllocString(m_Joints[i].m_Node->GetName());
                m_Animation->m_Skeleton.m_pJoint[i].m_Parent = m_Joints[i].m_Parent;

                FbxAMatrix& Local = m_Joints[i].m_Node->EvaluateLocalTransform();
            }
        }
    }
}

void fbx_parser::ProcessSkeleton( xarray<node_info>& Joints, FbxNode* Node, s32 ParentIndex )
{
    const char* Name = Node->GetName();
    if(Node->GetNodeAttribute() && Node->GetNodeAttribute()->GetAttributeType() && Node->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton)
    {
        node_info& Info = Joints.append();
        Info.m_Node     = Node;
        Info.m_Parent   = ParentIndex;
    }

    for ( s32 i = 0; i < Node->GetChildCount(); i++ )
    {
        ProcessSkeleton(Joints, Node->GetChild(i), ParentIndex + 1);
    }
}

void fbx_parser::CollectMeshes( void )
{
    if ( NULL != m_Scene )
    {
        if ( NULL != m_Mesh )
        {
            x_delete(m_Mesh);
        }

        m_Mesh = x_new(mesh, 1, 0);

        int GeomegryCount = m_Scene->GetGeometryCount();
        FbxGeometryConverter converter(m_Manager);
        for (int i = 0; i < GeomegryCount; i++)
        {
            FbxGeometry* const Geometry = m_Scene->GetGeometry(i);
            if (m_MeshMap.find(Geometry) == m_MeshMap.end())
            {
                FbxMesh* Mesh;
                if (Geometry->Is<FbxMesh>() && ((FbxMesh*)Geometry)->IsTriangleMesh())
                {
                    Mesh = (FbxMesh*)Geometry;
                }
                else
                {
                    FbxNodeAttribute * const attr = converter.Triangulate(Geometry, false);
                    if (attr->Is<FbxMesh>())
                    {
                        Mesh = (FbxMesh*)attr;
                    }
                    else
                    {
                        continue;
                    }
                }

                s32 StartIndex          = m_Vertices.getCount();
                s32 ControlPointCount   = Mesh->GetControlPointsCount();
                m_Vertices.appendList(ControlPointCount);
                FbxVector4 ControlPoint;
                for ( s32 j = 0; j < ControlPointCount; j++ )
                {
                    ControlPoint = Mesh->GetControlPointAt(j);
                    m_Vertices[StartIndex + j].m_Position.m_X = static_cast<float>(ControlPoint.mData[0]);
                    m_Vertices[StartIndex + j].m_Position.m_Y = static_cast<float>(ControlPoint.mData[1]);
                    m_Vertices[StartIndex + j].m_Position.m_Z = static_cast<float>(ControlPoint.mData[2]);

                    for ( s32 k = 0; k < 3; k++ )
                    {
                        m_Vertices[StartIndex + j].m_B[k] = -1;
                        m_Vertices[StartIndex + j].m_W[k] = 0.0f;
                    }
                }

                mesh_info* Info = new mesh_info(Mesh, StartIndex, ControlPointCount);
                m_Meshes.push_back(Info);
                m_MeshMap[Geometry] = Info;

                s32 TriangleCount = Mesh->GetPolygonCount();
                s32 IndexCount = TriangleCount * 3;
                Info->m_Polygons = x_new(s32, IndexCount, 0);

                s32 VertexCounter = 0;
                for ( s32 j = 0; j < TriangleCount; j++ )
                {
                    for ( s32 k = 0; k < 3; k++ )
                    {
                        s32 VertexIndex = Mesh->GetPolygonVertex(j, k);
                        mesh::vertex& Vertex = m_Vertices[StartIndex + VertexIndex];
                        if ( Mesh->GetElementNormalCount() > 0 )
                        {
                            GetNormal(Mesh, VertexIndex, VertexCounter, Vertex.m_Normal);
                        }
                        else
                        {
                            Vertex.m_Normal.Zero();
                        }

                        if ( Mesh->GetUVLayerCount() > 0 )
                        {
                            GetUV(Mesh, VertexIndex, Mesh->GetTextureUVIndex(j, k), Vertex.m_U, Vertex.m_V);
                        }
                        else
                        {
                            Vertex.m_U = 0.0f;
                            Vertex.m_V = 0.0f;
                        }
                    }
                }

                s32 DeformerCount = Mesh->GetDeformerCount(FbxDeformer::eSkin);
                if ( DeformerCount > 0 )
                {
                    FbxSkin* Skin = static_cast<FbxSkin*>(Mesh->GetDeformer(0, FbxDeformer::eSkin));
                    const s32 ClusterCount = Skin->GetClusterCount();
                    // Fetch the blend weights per control point
                    for (s32 j = 0; j < ClusterCount; j++)
                    {
                        const FbxCluster* Cluster = Skin->GetCluster(j);
                        const s32 IndexCount = Cluster->GetControlPointIndicesCount();
                        const s32* ClusterIndices = Cluster->GetControlPointIndices();
                        const double* ClusterWeights = Cluster->GetControlPointWeights();

                        // Compute bind-pose
                        if ( NULL != m_Animation )
                        {
                            s32 JointIndex = -1;
                            for ( s32 k = 0; k < m_Joints.getCount(); k++ )
                            {
                                if ( m_Joints[k].m_Node == Cluster->GetLink() )
                                {
                                    JointIndex = k;
                                    break;
                                }
                            }

                            if ( -1 != JointIndex )
                            {
                                FbxAMatrix transformMatrix;						
                                FbxAMatrix transformLinkMatrix;					
                                FbxAMatrix globalBindposeInverseMatrix;

                                Cluster->GetTransformMatrix(transformMatrix);
                                Cluster->GetTransformLinkMatrix(transformLinkMatrix);
                                globalBindposeInverseMatrix = transformLinkMatrix.Inverse() * transformMatrix * GetGeometryTransform(m_GeometryMap[Geometry]);

                                FbxVector4 Translation = globalBindposeInverseMatrix.GetT();
                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Translation.Set((f32)Translation.mData[0], (f32)Translation.mData[1], (f32)Translation.mData[2]);
                                FbxQuaternion Rotation = globalBindposeInverseMatrix.GetQ();
                                FbxVector4 Scale = globalBindposeInverseMatrix.GetS();
                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Scale.Set((f32)Scale.mData[0], (f32)Scale.mData[1], (f32)Scale.mData[2]);

                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Rotation[0] = (f32)Rotation.mData[0];
                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Rotation[1] = (f32)Rotation.mData[1];
                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Rotation[2] = (f32)Rotation.mData[2];
                                m_Animation->m_Skeleton.m_pJoint[JointIndex].m_Rotation[3] = (f32)Rotation.mData[3];
                            }
                        }

                        for (s32 k = 0; k < IndexCount; k++)
                        {
                            if (ClusterIndices[k] < 0 || ClusterIndices[k] >= (int)ControlPointCount || ClusterWeights[k] == 0.0)
                                continue;

                            s32 UnusedIndex = -1;
                            for ( s32 Index = 0; Index < 3; Index++ )
                            {
                                if ( -1 == m_Vertices[ClusterIndices[k]].m_B[Index] )
                                {
                                    UnusedIndex = Index;
                                    break;
                                }
                            }

                            if ( -1 != UnusedIndex )
                            {
                                m_Vertices[ClusterIndices[k]].m_W[UnusedIndex] = (f32)ClusterWeights[k];
                                m_Vertices[ClusterIndices[k]].m_B[UnusedIndex] = FindJointIndex(Cluster->GetLink());
                            }
                            else
                            {
                                x_printf("Too much bones bound to vertex:%d\n", ClusterIndices[k]);
                            }
                        }
                    }
                }
            }
        }
    }
}

void fbx_parser::CollectMaterials( void )
{
    if ( NULL != m_Scene )
    {
        int cnt = m_Scene->GetMaterialCount();
        for (int i = 0; i < cnt; i++)
        {
            FbxSurfaceMaterial * const &material = m_Scene->GetMaterial(i);
            if (m_MaterialMap.find(material) == m_MaterialMap.end())
                m_MaterialMap[material] = CreateMaterial(material);
        }
    }
}

Material* fbx_parser::CreateMaterial(FbxSurfaceMaterial * material)
{
    Material * const result = new Material();
    result->source = material;
    result->id = material->GetName();

    if ((!material->Is<FbxSurfaceLambert>()) || GetImplementation(material, FBXSDK_IMPLEMENTATION_HLSL) || GetImplementation(material, FBXSDK_IMPLEMENTATION_CGFX)) {
        printf("Skipping unsupported material: %s, replacing it by a red diffuse color, because:\n", result->id.c_str());
        if (!material->Is<FbxSurfaceLambert>())
            printf("- Material must extend FbxSurfaceLambert\n");
        if (GetImplementation(material, FBXSDK_IMPLEMENTATION_HLSL))
            printf("- HLSL shading implementation not supported");
        if (GetImplementation(material, FBXSDK_IMPLEMENTATION_CGFX))
            printf("- CgFX shading implementation not supported");
        result->diffuse[0] = 1.f;
        result->diffuse[1] = 0.f;
        result->diffuse[2] = 0.f;
        return result;
    }

    FbxSurfaceLambert * const &lambert = (FbxSurfaceLambert *)material;
    set<3>(result->ambient, lambert->Ambient.Get().mData);
    set<3>(result->diffuse, lambert->Diffuse.Get().mData);
    set<3>(result->emissive, lambert->Emissive.Get().mData);

    AddTextures(result->textures, lambert->Ambient, Texture::Ambient);
    AddTextures(result->textures, lambert->Diffuse, Texture::Diffuse);
    AddTextures(result->textures, lambert->Emissive, Texture::Emissive);
    AddTextures(result->textures, lambert->Bump, Texture::Bump);
    AddTextures(result->textures, lambert->NormalMap, Texture::Normal);
    FbxDouble factor = lambert->TransparencyFactor.Get();
    FbxDouble3 color = lambert->TransparentColor.Get();
    FbxDouble trans = (color[0] * factor + color[1] * factor + color[2] * factor) / 3.0;
    result->opacity = 1.f - (float)trans;

    if (!material->Is<FbxSurfacePhong>())
        return result;

    FbxSurfacePhong * const &phong = (FbxSurfacePhong *)material;

    set<3>(result->specular, phong->Specular.Get().mData);
    result->shininess = (float)phong->Shininess.Get();

    AddTextures(result->textures, phong->Specular, Texture::Specular);
    AddTextures(result->textures, phong->Reflection, Texture::Reflection);
    return result;
}

void fbx_parser::AddTextures(std::vector<Texture *>& textures, const FbxProperty &prop,  const Texture::Usage &usage)
{
    const unsigned int n = prop.GetSrcObjectCount<FbxFileTexture>();
    for (unsigned int i = 0; i < n; i++)
        add_if_not_null(textures, CreateTexture(prop.GetSrcObject<FbxFileTexture>(i), usage));
}

Texture* fbx_parser::CreateTexture(FbxFileTexture* texture, const Texture::Usage &usage)
{
    if (texture == 0)
        return 0;
    Texture * const result = new Texture();
    result->source = texture;
    result->id = texture->GetName();
    result->path = texture->GetFileName();
    set<2>(result->uvTranslation, texture->GetUVTranslation().mData);
    set<2>(result->uvScale, texture->GetUVScaling().mData);
    result->usage = usage;
    if (m_TextureMap.find(result->path) == m_TextureMap.end())
        m_TextureMap[result->path].path = result->path;
    m_TextureMap[result->path].textures.push_back(result);
    return result;
}

void fbx_parser::CollectTextureBounds( FbxNode* Node )
{
    //if ( NULL != m_Scene )
    //{
    //    if (Node == 0)
    //        Node = m_Scene->GetRootNode();
    //    const int childCount = Node->GetChildCount();
    //    for (int i = 0; i < childCount; i++)
    //        CollectTextureBounds(Node->GetChild(i));

    //    FbxGeometry *geometry = Node->GetGeometry();
    //    if (m_MeshMap.find(geometry) == m_MeshMap.end())
    //        return;
    //    FbxMeshInfo *meshInfo = m_MeshMap[geometry];
    //    const int matCount = Node->GetMaterialCount();
    //    for (int i = 0; i < matCount; i++)
    //    {
    //        FbxSurfaceMaterial *material = Node->GetMaterial(i);
    //        Material *mat = m_MaterialMap[material];
    //        for (std::vector<Texture *>::iterator it = mat->textures.begin(); it != mat->textures.end(); ++it)
    //        {
    //            FbxFileTexture *texture = (*it)->source;
    //            TextureFileInfo &info = m_TextureMap[texture->GetFileName()];
    //            for (unsigned int k = 0; k < meshInfo->m_UVCount; k++)
    //            {
    //                if (meshInfo->m_UVMapping[k] == texture->UVSet.Get().Buffer())
    //                {
    //                    const int idx = 4 * (i * meshInfo->m_UVCount + k);
    //                    if (*(int*)&info.bounds[0] == -1 || meshInfo->m_PartUVBounds[idx] < info.bounds[0])
    //                        info.bounds[0] = meshInfo->m_PartUVBounds[idx];
    //                    if (*(int*)&info.bounds[1] == -1 || meshInfo->m_PartUVBounds[idx+1] < info.bounds[1])
    //                        info.bounds[1] = meshInfo->m_PartUVBounds[idx+1];
    //                    if (*(int*)&info.bounds[2] == -1 || meshInfo->m_PartUVBounds[idx+2] > info.bounds[2])
    //                        info.bounds[2] = meshInfo->m_PartUVBounds[idx+2];
    //                    if (*(int*)&info.bounds[3] == -1 || meshInfo->m_PartUVBounds[idx+3] > info.bounds[3])
    //                        info.bounds[3] = meshInfo->m_PartUVBounds[idx+3];
    //                    info.nodeCount++;
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //}
}

xbool fbx_parser::Generate( void )
{
    if (!m_Scene)
    {
        return FALSE;
    }
    return TRUE;
}

u32 fbx_parser::ComputeSubMeshCount( FbxMesh* Mesh )
{
    s32 PolyCount = Mesh->GetPolygonCount();
    s32 ElementMaterialCount = Mesh->GetElementMaterialCount();
    s32 MaterialIndex = 0;
    s32 MaterialCount = 0;
    for (s32 i = 0; i < PolyCount; i++)
    {
        MaterialIndex = -1;
        for ( int j = 0; j < ElementMaterialCount && MaterialIndex < 0; j++ )
        {
            MaterialIndex = Mesh->GetElementMaterial(j)->GetIndexArray()[i];
        }
        if (MaterialIndex >= MaterialCount)
        {
            MaterialCount = MaterialIndex + 1;
        }
    }
    if (MaterialCount == 0)
    {
        MaterialCount = 1;
    }
    return MaterialCount;
}

void fbx_parser::GetNormal( FbxMesh* Mesh, int VertexIndex, int VertexCounter, xvector3d& Normal )
{
    FbxGeometryElementNormal* VertexNormal = Mesh->GetElementNormal(0);
    switch(VertexNormal->GetMappingMode())
    {
    case FbxGeometryElement::eByControlPoint:
        switch(VertexNormal->GetReferenceMode())
        {
        case FbxGeometryElement::eDirect:
            {
                Normal.m_X = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexIndex).mData[0]);
                Normal.m_Y = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexIndex).mData[1]);
                Normal.m_Z = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexIndex).mData[2]);
            }
            break;

        case FbxGeometryElement::eIndexToDirect:
            {
                int index = VertexNormal->GetIndexArray().GetAt(VertexIndex);
                Normal.m_X = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[0]);
                Normal.m_Y = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[1]);
                Normal.m_Z = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[2]);
            }
            break;

        default:
            throw std::exception("Invalid Reference");
        }
        break;

    case FbxGeometryElement::eByPolygonVertex:
        switch(VertexNormal->GetReferenceMode())
        {
        case FbxGeometryElement::eDirect:
            {
                Normal.m_X = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexCounter).mData[0]);
                Normal.m_Y = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexCounter).mData[1]);
                Normal.m_Z = static_cast<float>(VertexNormal->GetDirectArray().GetAt(VertexCounter).mData[2]);
            }
            break;

        case FbxGeometryElement::eIndexToDirect:
            {
                int index = VertexNormal->GetIndexArray().GetAt(VertexCounter);
                Normal.m_X = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[0]);
                Normal.m_Y = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[1]);
                Normal.m_Z = static_cast<float>(VertexNormal->GetDirectArray().GetAt(index).mData[2]);
            }
            break;

        default:
            throw std::exception("Invalid Reference");
        }
        break;
    }
}

void fbx_parser::GetUV( FbxMesh* Mesh, s32 VertexIndex, s32 UVIndex, f32& U, f32& V )
{
    FbxGeometryElementUV* VertexUV = Mesh->GetElementUV(0);

    switch(VertexUV->GetMappingMode())
    {
    case FbxGeometryElement::eByControlPoint:
        switch(VertexUV->GetReferenceMode())
        {
        case FbxGeometryElement::eDirect:
            {
                U = static_cast<float>(VertexUV->GetDirectArray().GetAt(VertexIndex).mData[0]);
                V = static_cast<float>(VertexUV->GetDirectArray().GetAt(VertexIndex).mData[1]);
            }
            break;

        case FbxGeometryElement::eIndexToDirect:
            {
                int index = VertexUV->GetIndexArray().GetAt(VertexIndex);
                U = static_cast<float>(VertexUV->GetDirectArray().GetAt(index).mData[0]);
                V = static_cast<float>(VertexUV->GetDirectArray().GetAt(index).mData[1]);
            }
            break;

        default:
            throw std::exception("Invalid Reference");
        }
        break;

    case FbxGeometryElement::eByPolygonVertex:
        switch(VertexUV->GetReferenceMode())
        {
        case FbxGeometryElement::eDirect:
        case FbxGeometryElement::eIndexToDirect:
            {
                U = static_cast<float>(VertexUV->GetDirectArray().GetAt(UVIndex).mData[0]);
                V = static_cast<float>(VertexUV->GetDirectArray().GetAt(UVIndex).mData[1]);
            }
            break;

        default:
            throw std::exception("Invalid Reference");
        }
        break;
    }
}

void fbx_parser::GetWeights( FbxMesh* Mesh, s32 VertexIndex, f32 Weights[3] )
{
}

// Get the geometry offset to a node. It is never inherited by the children.
FbxAMatrix fbx_parser::GetGeometryTransform(FbxNode* Node)
{
    const FbxVector4 lT = Node->GetGeometricTranslation(FbxNode::eSourcePivot);
    const FbxVector4 lR = Node->GetGeometricRotation(FbxNode::eSourcePivot);
    const FbxVector4 lS = Node->GetGeometricScaling(FbxNode::eSourcePivot);

    return FbxAMatrix(lT, lR, lS);
}

s32 fbx_parser::FindJointIndex( const FbxNode* Node )
{
    for ( s32 i = 0; i < m_Joints.getCount(); i++ )
    {
        if ( m_Joints[i].m_Node == Node )
        {
            return i;
        }
    }
    return -1;
}

void fbx_parser::ConvertAnimation( void )
{
    if ( NULL != m_Scene )
    {
        s32 AnimCount = m_Scene->GetSrcObjectCount<FbxAnimStack>();
        if ( AnimCount > 0 )
        {
            FbxAnimStack* AnimStack = m_Scene->GetSrcObject<FbxAnimStack>(0);
            FbxTimeSpan AnimTimeSpan = AnimStack->GetLocalTimeSpan();
            float AnimStart = (f32)(AnimTimeSpan.GetStart().GetMilliSeconds());
            float AnimStop = (f32)(AnimTimeSpan.GetStop().GetMilliSeconds());
            if (AnimStop <= AnimStart)
            {
                AnimStop = 999999999.0f;
            }

            // Could also use animStack->GetLocalTimeSpan and animStack->BakeLayers, but its not guaranteed to be correct
            const s32 LayerCount = AnimStack->GetMemberCount<FbxAnimLayer>();
            for (int i = 0; i < LayerCount; i++)
            {
                FbxAnimLayer *layer = AnimStack->GetMember<FbxAnimLayer>(i);
                // For each layer check which node is affected and within what time frame and rate
                const int curveNodeCount = layer->GetSrcObjectCount<FbxAnimCurveNode>();
                for (int j = 0; j < curveNodeCount; j++)
                {
                    FbxAnimCurveNode *curveNode = layer->GetSrcObject<FbxAnimCurveNode>(j);
                    // Check which properties on this curve are changed
                    const int nc = curveNode->GetDstPropertyCount();
                    for (int o = 0; o < nc; o++)
                    {
                        FbxProperty prop = curveNode->GetDstProperty(o);
                        FbxNode *node = static_cast<FbxNode *>(prop.GetFbxObject());
                        if (node)
                        {
                            FbxString propName = prop.GetName();
                            // Only add translation, scaling or rotation
                            if ((!node->LclTranslation.IsValid() || propName != node->LclTranslation.GetName()) && 
                                (!node->LclScaling.IsValid() || propName != node->LclScaling.GetName()) &&
                                (!node->LclRotation.IsValid() || propName != node->LclRotation.GetName()))
                                continue;

                            track_info* PreviousInfo = NULL;

                            if ( m_TrackMap.find(node) != m_TrackMap.end() )
                            {
                                s32 TrackIndex = m_TrackMap[node];
                                ASSERT(TrackIndex >= 0 && TrackIndex < m_AnimTracks.getCount());
                                PreviousInfo = &m_AnimTracks[TrackIndex];
                            }
                            else
                            {
                                PreviousInfo = &m_AnimTracks.append();
                                m_TrackMap[node] = m_AnimTracks.getCount() - 1;
                            }

                            PreviousInfo->m_Node    = node;

                            FbxAnimCurve *curve;
                            track_info Info;
                            Info.m_HasTranslation   = propName == node->LclTranslation.GetName();
                            Info.m_HasRotation      = propName == node->LclRotation.GetName();
                            Info.m_HasScaling       = propName == node->LclScaling.GetName();
                            if (curve = prop.GetCurve(layer, FBXSDK_CURVENODE_COMPONENT_X))
                                GetTrackInfo(curve, Info, AnimStart, AnimStop);
                            if (curve = prop.GetCurve(layer, FBXSDK_CURVENODE_COMPONENT_Y))
                                GetTrackInfo(curve, Info, AnimStart, AnimStop);
                            if (curve = prop.GetCurve(layer, FBXSDK_CURVENODE_COMPONENT_Z))
                                GetTrackInfo(curve, Info, AnimStart, AnimStop);

                            PreviousInfo->Merge(Info);

                            if ( PreviousInfo->m_Stop < 1.0f )
                            {
                                int a = 0;
                                a++;
                            }
                        }
                    }
                }
            }

            if ( m_AnimTracks.getCount() > 0 )
            {
                m_Animation->m_nClips = 1;
                m_Animation->m_pClip = x_new(animation::animation_clip, m_Animation->m_nClips, XMEM_FLAG_ALIGN_16B);

                animation::animation_clip* Clip = m_Animation->m_pClip;
                Clip->m_pName   = AllocString(AnimStack->GetName());
                Clip->m_pTrack  = x_new(animation::animation_track, m_AnimTracks.getCount(), XMEM_FLAG_ALIGN_16B);
                Clip->m_nTracks = m_AnimTracks.getCount();

                AnimStack->GetScene()->SetCurrentAnimationStack(AnimStack);
                
                FbxTime TempTime;

                for ( s32 i = 0; i < m_AnimTracks.getCount(); i++ )
                {
                    track_info& Info    = m_AnimTracks[i];
                    f32 StepTime        = Info.m_FrameRate <= 0.0f ? Info.m_Stop - Info.m_Start : 1000.0f / Info.m_FrameRate;
                    f32 LastTime        = Info.m_Stop + StepTime * 0.5f;

                    Clip->m_pTrack[i].m_nKeyFrames  = (s32)((LastTime - Info.m_Start) / StepTime);

                    Clip->m_pTrack[i].m_pKeyFrame   = x_new(animation::key_frame, Clip->m_pTrack[i].m_nKeyFrames, XMEM_FLAG_ALIGN_16B);
                    Clip->m_pTrack[i].m_JointIndex  = FindJointIndex(m_AnimTracks[i].m_Node);
                    if ( Clip->m_pTrack[i].m_JointIndex == -1 )
                    {
                        const char* Name = m_AnimTracks[i].m_Node->GetName();
                        int a = 0;
                        a++;
                    }

                    for ( s32 j = 0; j < Clip->m_pTrack[i].m_nKeyFrames; j++ )
                    {
                        f32 CurrentTime = x_Min(Info.m_Stop, (Info.m_Start + j * StepTime));
                        TempTime.SetMilliSeconds((FbxLongLong)CurrentTime);

                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Time = CurrentTime - Info.m_Start;
                        FbxAMatrix* Matrix = &m_AnimTracks[i].m_Node->EvaluateLocalTransform(TempTime);

                        FbxVector4 Translation = Matrix->GetT();
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Translation.m_X = (f32)Translation.mData[0];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Translation.m_Y = (f32)Translation.mData[1];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Translation.m_Z = (f32)Translation.mData[2];

                        FbxQuaternion Rotation = Matrix->GetQ();
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Rotation[0] = (f32)Rotation.mData[0];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Rotation[1] = (f32)Rotation.mData[1];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Rotation[2] = (f32)Rotation.mData[2];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Rotation[3] = (f32)Rotation.mData[3];

                        FbxVector4 Scaling = Matrix->GetS();
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Scale.m_X = (f32)Scaling.mData[0];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Scale.m_Y = (f32)Scaling.mData[1];
                        Clip->m_pTrack[i].m_pKeyFrame[j].m_Scale.m_Z = (f32)Scaling.mData[2];
                    }
                }
            }
        }
    }
}

void fbx_parser::GetTrackInfo( FbxAnimCurve* Curve, track_info& Info, f32 AnimStart, f32 AnimStop )
{
    FbxTimeSpan fts;
    Curve->GetTimeInterval(fts);
    const FbxTime Start = fts.GetStart();
    const FbxTime Stop = fts.GetStop();
    Info.m_Start = x_Max(AnimStart, x_Min(Info.m_Start, (float)(Start.GetMilliSeconds())));
    Info.m_Stop = x_Min(AnimStop, x_Max(Info.m_Stop, (float)Stop.GetMilliSeconds()));
    // Could check the number and type of keys (ie curve->KeyGetInterpolation) to lower the framerate
    Info.m_FrameRate = x_Max(Info.m_FrameRate, (f32)Stop.GetFrameRate(FbxTime::eDefaultMode));
}

char* fbx_parser::AllocString( const char* String )
{
    s32 Size = x_strlen(String) + 1;
    char* Result = x_new(char, Size, 0);
    x_strcpy(Result, Size, String);
    return Result;
}