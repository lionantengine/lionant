#pragma once
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <fbxsdk.h>

enum vertex_attribute
{
    ATTRIBUTE_POSITION      = 1,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_COLORPACKED,
    ATTRIBUTE_TANGENT,
    ATTRIBUTE_BINORMAL,
    ATTRIBUTE_TEXCOORD0,
    ATTRIBUTE_TEXCOORD1,
    ATTRIBUTE_TEXCOORD2,
    ATTRIBUTE_TEXCOORD3,
    ATTRIBUTE_TEXCOORD4,
    ATTRIBUTE_TEXCOORD5,
    ATTRIBUTE_TEXCOORD6,
    ATTRIBUTE_TEXCOORD7,
    ATTRIBUTE_BLENDWEIGHT0,
    ATTRIBUTE_BLENDWEIGHT1,
    ATTRIBUTE_BLENDWEIGHT2,
    ATTRIBUTE_BLENDWEIGHT3,
    ATTRIBUTE_BLENDWEIGHT4,
    ATTRIBUTE_BLENDWEIGHT5,
    ATTRIBUTE_BLENDWEIGHT6,
    ATTRIBUTE_BLENDWEIGHT7,
    ATTRIBUTE_COUNT,

    ATTRIBUTE_PADDING   = 0xffffffff
};

#define ATTRIBUTE_TYPE_SIGNED	0x00
#define ATTRIBUTE_TYPE_UNSIGNED	0x80
#define ATTRIBUTE_TYPE_HEX		0x40
#define ATTRIBUTE_TYPE_FLOAT	0
#define ATTRIBUTE_TYPE_INT		1
#define ATTRIBUTE_TYPE_UINT		(ATTRIBUTE_TYPE_INT | ATTRIBUTE_TYPE_UNSIGNED)
#define ATTRIBUTE_TYPE_UINT_HEX	(ATTRIBUTE_TYPE_UINT | ATTRIBUTE_TYPE_HEX)

static const char * AttributeNames[] = {
    "UNKNOWN", "POSITION", "NORMAL", "COLOR", "COLORPACKED", "TANGENT", "BINORMAL",
    "TEXCOORD0", "TEXCOORD1", "TEXCOORD2", "TEXCOORD3", "TEXCOORD4", "TEXCOORD5", "TEXCOORD6", "TEXCOORD7",
    "BLENDWEIGHT0", "BLENDWEIGHT1", "BLENDWEIGHT2", "BLENDWEIGHT3", "BLENDWEIGHT4", "BLENDWEIGHT5", "BLENDWEIGHT6", "BLENDWEIGHT7"
};

static const unsigned short AttributeTypeV2[]		= {ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT};
static const unsigned short AttributeTypeV4[]		= {ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT};
static const unsigned short AttributeTypeV3[]		= {ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT, ATTRIBUTE_TYPE_FLOAT};
static const unsigned short AttributeTypeUIntHex[]	= {ATTRIBUTE_TYPE_UINT_HEX};
static const unsigned short AttributeTypeBlend[]	= {ATTRIBUTE_TYPE_INT, ATTRIBUTE_TYPE_FLOAT};

#define INIT_VECTOR(T, A) std::vector<T>(A, A + sizeof(A) / sizeof(*A))

static const std::vector<unsigned short> AttributeTypes[] = {
    INIT_VECTOR(unsigned short, AttributeTypeV4),		// Unknown
    INIT_VECTOR(unsigned short, AttributeTypeV3),		// Position
    INIT_VECTOR(unsigned short, AttributeTypeV3),		// Normal
    INIT_VECTOR(unsigned short, AttributeTypeV4),		// Color
    INIT_VECTOR(unsigned short, AttributeTypeUIntHex),  // ColorPacked
    INIT_VECTOR(unsigned short, AttributeTypeV3),		// Tangent
    INIT_VECTOR(unsigned short, AttributeTypeV3),		// Binormal
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord0
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord1
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord2
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord3
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord4
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord5
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord6
    INIT_VECTOR(unsigned short, AttributeTypeV2),		// Texcoord7
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight0
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight1
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight2
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight3
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight4
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight5
    INIT_VECTOR(unsigned short, AttributeTypeBlend),	// Blendweight6
    INIT_VECTOR(unsigned short, AttributeTypeBlend)	// Blendweight7
};

#define ATTRIBUTE_SIZE(idx) (AttributeTypes[idx].size())

struct BlendWeight
{
    float weight;
    int index;
    BlendWeight() : weight(0.f), index(-1) {}
    BlendWeight(float w, int i) : weight(w), index(i) {}
    inline bool operator<(const BlendWeight& rhs) const {
        return weight < rhs.weight;
    }
    inline bool operator>(const BlendWeight& rhs) const {
        return weight > rhs.weight;
    }
    inline bool operator==(const BlendWeight& rhs) const {
        return weight == rhs.weight;
    }
};

// Group of indices for vertex blending
struct BlendBones {
    int *bones;
    unsigned int capacity;
    BlendBones(const unsigned int &capacity = 2) : capacity(capacity) {
        bones = new int[capacity];
        memset(bones, -1, capacity * sizeof(int));
    }
    BlendBones(const BlendBones &rhs) : capacity(rhs.capacity) {
        bones = new int[capacity];
        memcpy(bones, rhs.bones, capacity * sizeof(int));
    }
    ~BlendBones() {
        delete bones;
    }
    inline bool has(const int &bone) const {
        for (unsigned int i = 0; i < capacity; i++)
            if (bones[i] == bone)
                return true;
        return false;
    }
    inline unsigned int size() const {
        for (unsigned int i = 0; i < capacity; i++)
            if (bones[i] < 0)
                return i;
        return capacity;
    }
    inline unsigned int available() const {
        return capacity - size();
    }
    inline int cost(const std::vector<std::vector<BlendWeight>*> &rhs) const {
        int result = 0;
        for (std::vector<std::vector<BlendWeight>*>::const_iterator itr = rhs.begin(); itr != rhs.end(); ++itr)
            for (std::vector<BlendWeight>::const_iterator jtr = (*itr)->begin(); jtr != (*itr)->end(); ++jtr)
                if (!has((*jtr).index))
                    result++;
        return (result > (int)available()) ? -1 : result;
    }
    inline void sort() {
        std::sort(bones, bones + size());
    }
    inline int idx(const int &bone) const {
        for (unsigned int i = 0; i < capacity; i++)
            if (bones[i] == bone)
                return i;
        return -1;
    }
    inline int add(const int &v) {
        for (unsigned int i = 0; i < capacity; i++) {
            if (bones[i] == v)
                return i;
            else if (bones[i] < 0) {
                bones[i] = v;
                return i;
            }
        }
        return -1;
    }
    inline bool add(const std::vector<std::vector<BlendWeight>*> &rhs) {
        for (std::vector<std::vector<BlendWeight>*>::const_iterator itr = rhs.begin(); itr != rhs.end(); ++itr)
            for (std::vector<BlendWeight>::const_iterator jtr = (*itr)->begin(); jtr != (*itr)->end(); ++jtr)
                if (add((*jtr).index)<0)
                    return false;
        return true;
    }
    inline int operator[](const unsigned int idx) const {
        return idx < capacity ? bones[idx] : -1;
    }
    inline BlendBones &operator=(const BlendBones &rhs) {
        if (&rhs == this)
            return *this;
        if (capacity != rhs.capacity) {
            delete[] bones;
            bones = new int[capacity = rhs.capacity];
        }
        memcpy(bones, rhs.bones, capacity * sizeof(int));
        return *this;
    }
};

// Collection of group of indices for vertex blending
struct BlendBonesCollection {
    std::vector<BlendBones> bones;
    unsigned int bonesCapacity;
    BlendBonesCollection(const unsigned int &bonesCapacity) : bonesCapacity(bonesCapacity) { }
    BlendBonesCollection(const BlendBonesCollection &rhs) : bonesCapacity(rhs.bonesCapacity) {
        bones.insert(bones.begin(), rhs.bones.begin(), rhs.bones.end());
    }
    inline BlendBonesCollection &operator=(const BlendBonesCollection &rhs) {
        if (&rhs == this)
            return (*this);
        bones = rhs.bones;
        bonesCapacity = rhs.bonesCapacity;
        return (*this);
    }
    inline unsigned int size() const {
        return (unsigned int)bones.size();
    }
    inline BlendBones &operator[](const unsigned int &idx) {
        return bones[idx];
    }
    inline unsigned int add(const std::vector<std::vector<BlendWeight>*> &rhs) {
        int cost = (int)bonesCapacity, idx = -1, n = (int)bones.size();
        for (int i = 0; i < n; i++) {
            const int c = bones[i].cost(rhs);
            if (c >= 0 && c < cost) {
                cost = c;
                idx = i;
            }
        }
        if (idx < 0) {
            bones.push_back(BlendBones(bonesCapacity));
            idx = n;
        }
        return bones[idx].add(rhs) ? idx : -1;
    }
    inline void sortBones() {
        for (std::vector<BlendBones>::iterator itr = bones.begin(); itr != bones.end(); ++itr)
            (*itr).sort();
    }
};

typedef FbxLayerElementArrayTemplate<FbxVector4> Vector4Array;
typedef FbxLayerElementArrayTemplate<int> IntArray;
typedef FbxLayerElementArrayTemplate<FbxColor> ColorArray;
typedef FbxLayerElementArrayTemplate<FbxVector2> Vector2Array;

struct Attributes
{
    unsigned long value;

    Attributes() : value(0) {}

    Attributes(const unsigned long &v) : value(v) {}

    Attributes(const Attributes &copyFrom) : value(copyFrom.value) {}

    inline bool operator==(const Attributes& rhs) const {
        return value == rhs.value;
    }

    unsigned int size() const {
        unsigned int result = 0;
        for (unsigned int i = 0; i < ATTRIBUTE_COUNT; i++)
            if (has(i))
                result += (unsigned int)ATTRIBUTE_SIZE(i);
        return result;
    }

    unsigned int length() const {
        unsigned int result = 0;
        for (unsigned int i = 0; i < ATTRIBUTE_COUNT; i++)
            if (has(i))
                result++;
        return result;
    }

    /** 0 <= index < length() */
    const int get(unsigned int index) const {
        for (unsigned int i = 0; i < ATTRIBUTE_COUNT; i++)
            if (has(i) && index-- <= 0)
                return i;
        return -1;
    }

    /** 0 <= index < length() */
    const char *name(const unsigned int &index) const {
        const int a = get(index);
        return a < 0 ? 0 : AttributeNames[a];
    }

    /** 0 <= v < size() */
    const unsigned short getType(const unsigned int &v) const {
        unsigned int s = 0;
        for (unsigned int i = 0; i < ATTRIBUTE_COUNT; i++) {
            if (!has(i))
                continue;
            const unsigned short is = (unsigned short)ATTRIBUTE_SIZE(i);
            if ((s + is) > v)
                return AttributeTypes[i][v-s];
            s+=is;
        }
        return 0;
    }

    void set(const unsigned int &attribute, const bool &v) {
        if (v)
            add(attribute);
        else
            remove(attribute);
    }

    void add(const unsigned int &attribute) {
        value |= (1 << attribute);
    }

    void remove(const unsigned int &attribute) {
        value &= -1 ^ (1 << attribute);
    }

    inline bool has(const unsigned int &attribute) const {
        return (value & (1 << attribute)) != 0;
    }

    inline bool hasPosition() const {
        return has(ATTRIBUTE_POSITION);
    }

    void hasPosition(const bool &v) {
        set(ATTRIBUTE_POSITION, v);
    }

    inline bool hasNormal() const {
        return has(ATTRIBUTE_NORMAL);
    }

    void hasNormal(const bool &v) {
        set(ATTRIBUTE_NORMAL, v);
    }

    inline bool hasColor() const {
        return has(ATTRIBUTE_COLOR);
    }

    void hasColor(const bool &v) {
        set(ATTRIBUTE_COLOR, v);
    }

    inline bool hasColorPacked() const {
        return has(ATTRIBUTE_COLORPACKED);
    }

    void hasColorPacked(const bool &v) {
        set(ATTRIBUTE_COLORPACKED, v);
    }

    inline bool hasTangent() const {
        return has(ATTRIBUTE_TANGENT);
    }

    void hasTangent(const bool &v) {
        set(ATTRIBUTE_TANGENT, v);
    }

    inline bool hasBinormal() const {
        return has(ATTRIBUTE_BINORMAL);
    }

    void hasBinormal(const bool &v) {
        set(ATTRIBUTE_BINORMAL, v);
    }

    inline bool hasUV(const unsigned short &uv) const {
        return has(ATTRIBUTE_TEXCOORD0 + uv);
    }

    void hasUV(const unsigned short &uv, const bool &v) {
        set(ATTRIBUTE_TEXCOORD0 + uv, v);
    }

    inline bool hasBlendWeight(const unsigned short &index) const {
        return has(ATTRIBUTE_BLENDWEIGHT0 + index);
    }

    void hasBlendWeight(const unsigned short &index, const bool &v) {
        set(ATTRIBUTE_BLENDWEIGHT0 + index, v);
    }
};

struct FbxMeshInfo
{
    FbxMesh*                            m_Mesh;
    const std::string                   m_ID;
    // The maximum amount of blend weights per vertex
    const unsigned int                  m_MaxVertexBlendWeightCount;
    // The actual amount of blend weights per vertex (<= m_MaxVertexBlendWeightCount)
    unsigned int                        m_VertexBlendWeightCount;
    // Whether to use maxVertexBlendWeightCount even if the actual amount of vertex weights is less than that
    const bool                          m_ForceMaxVertexBlendWeightCount;
    // Whether the required minimum amount of bones (per triangle) exceeds the specified MaxNodePartBoneCount
    bool                                m_BonesOverflow;

    unsigned int                        m_MaxNodePartBoneCount;
    // The vertex attributes
    Attributes                          m_Attributes;
    // Whether to use packed colors
    const bool                          m_UsePackedColors;
    // The number of polygon (triangles if triangulated)
    const unsigned int                  m_PolyCount;
    // The number of control points within the mesh
    const unsigned int                  m_PointCount;
    // The control points within the mesh
    const FbxVector4* const             m_Points;
    // The number of texture coordinates within the mesh
    const unsigned int                  m_UVCount;
    // The number of element materials
    const int                           m_ElementMaterialCount;
    // The number of mash parts within the mesh
    int                                 m_MeshPartCount;
    // The applied skin or 0 if not available
    FbxSkin* const                      m_Skin;
    // The blendweights per control point
    std::vector<BlendWeight>*           m_PointBlendWeights;
    // The collection of bones per mesh part
    std::vector<BlendBonesCollection>   m_PartBones;
    // Mapping between the polygon and the index of its meshpart
    unsigned int* const                 m_PolyPartMap;
    // Mapping between the polygon and the index of its weight bones within its meshpart
    unsigned int* const                 m_PolyPartBonesMap;
    // The UV bounds per part per uv coords
    float*                              m_PartUVBounds;
    // The mapping name of each uv to identify the cooresponding texture
    std::string                         m_UVMapping[8];

    const Vector4Array*                 m_Normals;
    const IntArray*                     m_NormalIndices;
    bool                                m_NormalOnPoint;

    const Vector4Array*                 m_Tangents;
    const IntArray*                     m_TangentIndices;
    bool                                m_TangentOnPoint;

    const Vector4Array*                 m_Binormals;
    const IntArray*                     m_BinormalIndices;
    bool                                m_BinormalOnPoint;

    const ColorArray*                   m_Colors;
    const IntArray*                     m_ColorIndices;
    bool                                m_ColorOnPoint;

    const Vector2Array*                 m_UVs[8];
    const IntArray*                     m_UVIndices[8];
    bool                                m_UVOnPoint[8];

    FbxMeshInfo                         ( FbxMesh* Mesh );
    ~FbxMeshInfo                        ( void );

    inline void getUV(FbxVector2 * const &out, const unsigned int &uvIndex, const unsigned int &polyIndex, const unsigned int &point) const
    {
        ((FbxLayerElementArray*)m_UVs[uvIndex])->GetAt(m_UVOnPoint[uvIndex] ? (m_UVIndices[uvIndex] ? (*m_UVIndices[uvIndex])[point] : point) : (m_UVIndices[uvIndex] ? (*m_UVIndices[uvIndex])[polyIndex] : polyIndex), out);
    }

protected:
    static std::string  GetID                       (FbxMesh * const& Mesh);
    unsigned int        CalcMeshPartCount           ( void );
    void                CollectVertexBlendWeights   ( void );
    void                CollectMeshPartsAndBones    ( void );
    void                CollectMeshParts            ( void );
    void                CollectUVInfo               ( void );
    void                PrepareAttributes           ( void );
    void                CacheAttributes             ( void );
};

