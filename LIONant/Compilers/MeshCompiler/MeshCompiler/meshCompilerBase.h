//
//  meshBaseCompiler.h
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef MESH_COMPILER_BASE_H
#define MESH_COMPILER_BASE_H

#include "MeshCompilerKey.h"
#include "eng_base.h"
#include "RawGeom.h"
#include "eng_Geom.h"

class mesh_compiler_base : public compiler_base
{
public:
    
                                        mesh_compiler_base  ( void ) : m_Key(*this) {}
    virtual void                        onCompile           ( void );
    virtual compiler_key_object&        getKeyObject        ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject        ( void ) const { return m_Key;             }

protected:

    struct infortech_inter
    {
        xstring                         m_TechName;
        s32                             m_iRawInformed;
        s32                             m_InformedTechniqueIndex;
    };

    struct informed_inter
    {
        xbool                           m_bNewInformedFile;
        xstring                         m_InformedORMaterial;
        xstring                         m_NewInformedRSC;
        u64                             m_GuidInformed;
        xarray2<infortech_inter>        m_lInformedTech;
    };

    struct vertex_attribute_list
    {
        xarray<eng_vertex_desc::attribute>          m_AttributeList;
    };

    struct submesh_cmd_map
    {
        s32                                         m_iMesh;                // Index of the mesh (For debug porpuses)
        s32                                         m_iInformed;            // Informed index (for debug porpuses)
        s32                                         m_iCmd;                 // Start list of commands
        s32                                         m_nCmds;                // Count
    };

    struct compiler_data
    {
        s32                                         m_VertexCacheSize;      // Vertex Cache allowed for the platform
        s32                                         m_MartrixCacheSize;     // Matrix Cache possible in the shader
        xarray<eng_geom_rsc::mesh>                  m_Mesh;
        xarray<eng_geom_rsc::submesh>               m_Submesh;
        xarray<eng_geom_rsc::temp_vertex_stream>    m_TempVertices;
        xarray<eng_vertex_desc>                     m_VertexDesc;
        xarray<vertex_attribute_list>               m_GeomAttributeList;
        eng_geom_rsc::geom_rsc                      m_GeomRSC;
        xarray<eng_geom_rsc::temp_index_stream>     m_TempIndices;
        xarray2<eng_geom_rsc::cmd>                  m_lCmds;
        xptr2<eng_geom_rsc::bone>                   m_lBones;
        xarray2<eng_geom_rsc::informed_ref>         m_lImformed;
        xarray2<informed_inter>                     m_lInterInformed;
        xarray2<submesh_cmd_map>                    m_lSubmeshCmdMap;
    };
    
protected:

    void                                AddVertex                       ( eng_geom_rsc::temp_vertex_stream& VStream, const rawgeom::vertex& Vertex, const eng_vertex_desc& Desc ) const;
    void                                ComputeStructures               ( compiler_data&  CompilerData, const rawgeom& RawGeom, const xplatform Platform ) const;
    void                                PreprocessRawGeomWithArmCompiler( compiler_data& CompilerData, rawgeom& Dest, const rawgeom& Src ) const;
    void                                ExportResource                  ( const eng_geom_rsc::geom_rsc& GeomRsc ) const;
    xbool                               CompilationUniqueness           ( void ) const;
    void                                AddExternalKey                  ( const xstring& ExternalKey );
    void                                CleanPathForMaterialsAndTextures( rawgeom& RawGeom ) const;

protected:
    
    mesh_compiler_key                               m_Key;
};

#endif
