
#ifndef GEOM_COMPILER_HPP
#define GEOM_COMPILER_HPP

#include "x_files.hpp"
#include "RawMesh2.hpp"
#include "texinfo.hpp"

struct rawmesh2;
struct skin_geom;
struct rigid_geom;
struct geom;

class geom_compiler
{
public:
        
    enum comp_type
    {
        TYPE_NONE,
        TYPE_RIGID,
        TYPE_SKIN
    };

            geom_compiler       ( void );
    void    AddPlatform         ( platform Platform, const char* pFileName );
    s32     GetPlatformIndex    ( platform Platform );
    void    Export              ( const char* pFileName, comp_type Type, const char* pTexturePath );
    void    AddFastCollision    ( const char* pFileName );

    void    CompileLowCollision ( rigid_geom&   RigidGeom, 
                                  rawmesh2&     LowMesh, 
                                  rawmesh2&     HighMesh,
                                  const char*   pFileName);
   
    void    CompileLowCollisionFromBBox ( rigid_geom& RigidGeom, rawmesh2&   HighMesh );

    void    CompileHighCollisionPC      ( rigid_geom& RigidGeom, u32* pMatList, const char* pFileName );
    void    CompileHighCollisionPS2     ( rigid_geom& RigidGeom, u32* pMatList, const char* pFileName );
    void    CompileHighCollisionXBOX    ( rigid_geom& RigidGeom, u32* pMatList, const char* pFileName );

public:

    struct high_tri
    {
        high_tri*   pNext;
        s32         I;
        vector3     P[3];
        s32         iBone;
        s32         iMesh;
        s32         iDList;
        u32         MatInfo;
        xbool       bFlipOrient;
        plane       Plane;
        bbox        BBox;
    };

protected:

    struct info
    {
        char                FileName[256];
        f32                 MinDistance;
        xbool               BuildCollision;
    };

    struct plat_info
    {
        platform            Platform;
        char                FileName[256];
    };

    struct dlist
    {
        s32                 iMaterial;
        s32                 iBone;
        xarray<s32>         lTri;
    };

    struct sub_mesh
    {
        char                        Name[32];
        const rawmesh2*             pRawMesh;
        const rawmesh2::sub_mesh*   pRawSubMesh;
        xharray<dlist>              lDList;
    };

    struct material
    {
        s32                 iRawMaterial;
        const rawmesh2*     pRawMesh;
        tex_info            TexInfo;
    };

    struct mesh
    {
        xarray<material>    Material;
        xharray<sub_mesh>   SubMesh;
    };

    struct map_slot
    {
        const char*     pFileName;
        s32             nBitmap;
        xarray<xbitmap> lBitmap;
        xarray<xstring> lBitmapName;
        xarray<xstring> lOutBitmapName;
        xbool           bOutOfDate;
    };

protected:
    
    void    BuildBasicStruct    ( geom& Geom, const rawmesh2& RawMesh, mesh& Mesh, xbool IsRigid );

    void    ExportRigidGeom     ( const char* pFileName );
    void    ExportRigidGeomXbox ( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName );
    void    ExportRigidGeomPS2  ( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName );
    void    ExportRigidGeomPC   ( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName );

    void    ExportSkinGeom      ( const char* pFileName );
    void    ExportSkinGeomXbox  ( mesh& Mesh, skin_geom&  SkinGeom,  const char* pFileName );
    void    ExportSkinGeomPS2   ( mesh& Mesh, skin_geom&  SkinGeom,  const char* pFileName );
    void    ExportSkinGeomPC    ( mesh& Mesh, skin_geom&  SkinGeom,  const char* pFileName );

    void    ExportMaterial      ( mesh& Mesh, geom& Geom, s32 PlatformID );

    void    BuildTexturePath    ( char* pPath, const char* pName, s32 PlatformID );
    void    ExportDiffuse       ( const xbitmap& Bitmap, const char* pName, pref_bpp BPP, s32 PlatformID );
    void    ExportEnvironment   ( const xbitmap& Bitmap, const char* pName, s32 PlatformID );
    void    ExportDetail        ( const xbitmap& Bitmap, const char* pName, s32 PlatformID );

    xbool   IsBitmapNew         ( const char* pFileName, map_slot& Map, platform PlatformID, u32 CheckSum, map_slot* pMixMat = NULL );
    void    ReadBitmap          ( const char* pFileName, map_slot& Map );
    void    ProcessIntensityMap ( map_slot& DiffuseMap, map_slot& IntensityMap );
    void    ProcessPunchThruMap ( map_slot& DiffuseMap, map_slot& PunchThruMap );

    void    CompileHighCollision        ( rigid_geom& RigidGeom, high_tri* pTri, s32 nTris, const char* pFileName );

protected:

    xarray<info>        m_InfoList;
    xarray<plat_info>   m_PlatInfo;
    char                m_TexturePath[X_MAX_PATH] ;
    char                m_FastCollision[X_MAX_PATH];
};

#endif
