//=============================================================================
//
//  Rigid and Skin Geom Compiler by JP and TA
//
//=============================================================================

#include "GeomCompiler.hpp"
#include "Render\geom.hpp"
#include "Render\RigidGeom.hpp"
#include "Render\SkinGeom.hpp"
#include "faststrip.hpp"
#include "PS2strip.hpp"
#include "ArmOptimizer.hpp"
#include "PS2SkinOptimizer.hpp"
#include "BMPUtil.hpp"
#include "Auxiliary\Bitmap\aux_Bitmap.hpp"
#include "Parsing\Tokenizer.hpp"
#include <io.h>

// Xbox specific
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <3rdParty/Xbox/Include/D3d8.h>
#include <3rdParty/Xbox/Include/XGraphics.h>
#include <3rdParty/Nvidia/NvTriStrip/NvTriStrip.h>

#define STRIPE_GEOMS 1

//=============================================================================

#define MAX_TEMP_PUSH_BUFFER 262144

#define MAX_VU_VERTS        80
#define BACKFACE_CULLING    TRUE
#define SORT_BONES          FALSE
#define ADCBIT              15
#define CCWBIT               5
#define MAX_UV_KEYS         10000

//=============================================================================

//
// Fixed layout of "Map" buttons in 3DS Max material editor
//

enum
{
    Max_Diffuse1,           // 3DS Max Defaults
    Max_Diffuse2,           // 3DS Max Defaults
    Max_Blend,              // 3DS Max Defaults 
    Max_LightMap,           // 3DS Max Defaults 
    Max_Opacity,            // 3DS Max Defaults 
    Max_Intensity,          // Environment Intensity Map
    Max_Environment,        // Environment Map
    Max_SelfIllumination,   // Per-Pixel Self-Illumination
    Max_DetailMap,          // Detail Map
    Max_PunchThrough,       // Punch-Through Map
    NumMaps
};

//
// Layout of Max material "constants"
//
enum
{
    MaxConst_DetailScale,
    MaxConst_EnvType,
    MaxConst_EnvBlend,
    MaxConst_FixedAlpha,
};


//=============================================================================

//
// Name of Map slots in 3DS Max
//

char* MaxMapNames[ NumMaps ] =
{
    "Diffuse Map",
    "Diffuse Map 2",
    "Diffuse Blend",
    "LightMap",
    "Opacity Map",
    "Environment Intensity",
    "Environment Map",
    "Self-Illumination Map",
    "Detail Map",
    "Punch-Through",
};

const s32 MaxMeshes        =  63;                       // Max number of sub-meshes per RawMesh
const s32 MaxMaterials     =  63;                       // Max number of materials per sub-mesh
const s32 MaxTextureWidth  = 512;
const s32 MaxTextureHeight = 512;


extern xbool g_Verbose;
extern xbool g_ColoredMips;
extern xbool g_ExcludeLowCollision;
extern struct _finddata_t g_ExeData;


//=============================================================================

geom_compiler::geom_compiler( void )
{
    m_FastCollision[0] = 0;
    m_TexturePath  [0] = 0 ;
}

//=============================================================================

void geom_compiler::AddPlatform( platform Platform, const char* pFileName )
{
    plat_info& P = m_PlatInfo.Append();
    x_strcpy( P.FileName, pFileName );
    P.Platform = Platform;
}

//=============================================================================

s32 geom_compiler::GetPlatformIndex( platform Platform )
{
    s32 Index = -1;

    for( s32 i=0; i<m_PlatInfo.GetCount(); i++ )
    {
        if( m_PlatInfo[i].Platform == Platform )
        {
            Index = i;
            break;
        }
    }
    
    if( Index == -1 )
        x_throw( "Unknown platform" );

    return( Index );
}

//=============================================================================

void geom_compiler::AddFastCollision( const char* pFileName )
{
    x_strcpy( m_FastCollision, pFileName );
}

//=============================================================================

void geom_compiler::BuildBasicStruct( geom& Geom, const rawmesh2& RawMesh, mesh& Mesh, xbool IsRigid )
{
    s32             i,j;
    xarray<s32>     MatMap;
    xarray<xhandle> hMatDList;

    // Create bones
    s32 NBones = RawMesh.m_nBones ;
    Geom.m_nBones = NBones ;
    Geom.m_pBone  = new geom::bone[NBones] ;
    if( Geom.m_pBone == NULL )
        x_throw( "Out of memory" );

    // Setup bones
    for (i = 0 ; i < NBones ; i++)
        Geom.m_pBone[i].BBox = RawMesh.m_pBone[i].BBox ;

    // Usage map for the materials
    s8 MatUsed[ MaxMeshes ][ MaxMaterials ];

    x_memset( MatUsed, 0, sizeof( MatUsed ) );

    //
    // Determine the materials that each mesh uses
    //

    if( RawMesh.m_nMaterials > MaxMaterials )
        x_throw( "Too many materials in file" );

    if( RawMesh.m_nMaterials <= 0 )
    {
        x_throw( "ERROR: No materials defined for this MATX... Compile will fail\n" );	
    }
    
    s32 MaterialsUsed = 0;
    
    for( i=0; i<RawMesh.m_nFacets; i++ )
    {
        s32 iMesh     = RawMesh.m_pFacet[i].iMesh;
        s32 iMaterial = RawMesh.m_pFacet[i].iMaterial;

        if( (iMesh < 0) || (iMesh >= MaxMeshes) )
            x_throw( xfs( "Invalid iMesh %d", iMesh ) );
        
        if( (iMaterial < 0) || (iMaterial >= MaxMaterials) )
            x_throw( xfs( "Invalid iMaterial %d", iMaterial ) );
        
        MatUsed[ iMesh ][ iMaterial ] = 1;
        
        if( iMaterial > MaterialsUsed )
            MaterialsUsed = iMaterial;
    }

    MaterialsUsed++;
    if( MaterialsUsed > RawMesh.m_nMaterials )
    {
        x_throw( xfs( "Invalid material was used %d (max %d)", MaterialsUsed, RawMesh.m_nMaterials ) );
    }

    //
    // Find which materials are used and add them to list
    //
    
    for( i=0; i<RawMesh.m_nMaterials; i++ )
    {
        // First lets make sure that this material is used in the mesh
        for( j=0; j<RawMesh.m_nFacets; j++ )
        {
            if( RawMesh.m_pFacet[j].iMaterial == i )
                break;
        }
        
        // If it is used then...
        if( j != RawMesh.m_nFacets )
        {            
            // Search to see if the material already exists
            for( j=0; j<Mesh.Material.GetCount(); j++ )
            {
                // TODO: write a properly tested IsSameMaterial() function.
                // For now just let all materials pass.
            
                //rawmesh2::material& Mat = Mesh.Material[ j ].pRawMesh->m_pMaterial[ Mesh.Material[ j ].iRawMaterial ];
                //
                //if( Mat.IsSameMaterial( RawMesh.m_pMaterial[i] ) )
                //    break;
            }        

            // Add new material if we didn't find it
            if( j == Mesh.Material.GetCount() ) 
            {
                material& Mat    = Mesh.Material.Append();
                Mat.pRawMesh     = &RawMesh;
                Mat.iRawMaterial = i;

                rawmesh2::material& RawMat = RawMesh.m_pMaterial[Mat.iRawMaterial];

                if( RawMat.Map[Max_Diffuse1].iTexture == -1 )
                    x_throw( xfs( "Material does not have a texture <%s>", RawMat.Name ) );

                rawmesh2::texture&  RawTex = RawMesh.m_pTexture[RawMat.Map[Max_Diffuse1].iTexture];
                Mat.TexInfo.Load( RawTex.FileName );
            }
            
            // Add material map entry
            MatMap.Append() = j;
        }
        else
        {
            // Material is not used
            MatMap.Append() = -1;
        }
    }

    if( g_Verbose )
        x_DebugMsg( "%d Materials\n", Mesh.Material.GetCount() );

    //
    // Build the submeshes and create display lists for the materials
    //

    if( IsRigid == TRUE )
    {
        for( s32 n=0; n < RawMesh.m_nSubMeshs; n++ )
        {
            sub_mesh& SubMesh = Mesh.SubMesh.Add();
            
            SubMesh.pRawMesh    = &RawMesh;
            SubMesh.pRawSubMesh = &RawMesh.m_pSubMesh[n] ;
        
            char* pName = RawMesh.m_pSubMesh[n].Name;
            s32   Size  = sizeof( SubMesh.Name );
            
            if( x_strlen( pName ) > (Size-1) )
                x_throw( xfs( "Mesh name [%s] is longer than %d characters", pName, Size-1 ) );
            
            x_strcpy( SubMesh.Name, pName );
        
            // Create a display list for each material used in the SubMesh
            for( i=0; i<RawMesh.m_nMaterials; i++ )
            {
                // Is this material used by the SubMesh?
                if( MatUsed[n][i] && (MatMap[i] != -1) )
                {
                    xharray<dlist> lDList;

                    // Create a new display list for each bone
                    for( j=0; j<RawMesh.m_nBones; j++ )
                    {
                        // Create new dlist
                        dlist& DList = lDList.Add() ;

                        // *INEV* *SB* - Pre-allocate tri indices
                        DList.lTri.SetCapacity(RawMesh.m_nFacets) ;
                    }
                    
                    // Assign facets to the correct display list
                    for( j=0; j<RawMesh.m_nFacets; j++ )
                    {
                        rawmesh2::facet&  Facet   = RawMesh.m_pFacet[j];
                        rawmesh2::vertex* pVertex = RawMesh.m_pVertex;
                        
                        // Get the bone used by the first vertex in the facet
                        s32 Vert0 = Facet.iVertex[0];
                        s32 iBone = pVertex[ Vert0 ].Weight[0].iBone;
                        
                        if( iBone >= lDList.GetCount() )
                            x_throw( "Bone index is out of range" );
                        
                        /*
                        // Check all vertices in the facet use the same bone
                        for( s32 k=1; k<Facet.nVertices; k++ )
                        {
                            s32 VertI = Facet.iVertex[k];
                            s32 Index = pVertex[ VertI ].Weight[0].iBone;
                            
                            if( Index != iBone )
                            {
                                x_throw( "Vertices of the face do not use the same bone" );
                            }
                        }
                        */
                        
                        // Check if facet uses this Material and is in the same mesh
                        if( (RawMesh.m_pFacet[j].iMesh     == n) &&
                            (RawMesh.m_pFacet[j].iMaterial == i) )
                        {
                            // Add the facet to the correct bone DList
                            lDList[ iBone ].lTri.Append( j );
                        }
                    }
                    
                    // Copy the new display lists into the SubMesh (if they were used)
                    for( j=0; j<lDList.GetCount(); j++ )
                    {
                        dlist& BoneDList = lDList[j];
                    
                        // Check if any facets were added to this display list
                        if( BoneDList.lTri.GetCount() > 0 )
                        {
                            // Create a new display list in the SubMesh
                            dlist& DList    = SubMesh.lDList.Add();
                            DList.iMaterial = MatMap[i];
                            DList.iBone     = j;

                            // Copy the facet indices over to the SubMesh display list

                            // *INEV* *SB* - FAST COPY!!!
                            DList.lTri = BoneDList.lTri ;
                            
                            // Copy the facet indices over to the SubMesh display list
                            //for( s32 k=0; k<BoneDList.lTri.GetCount(); k++ )
                            //{
                                //DList.lTri.Append() = BoneDList.lTri[k];
                            //}
                        }
                    }
                }
            }
        }
    }
    else
    {
        for( s32 n=0; n < RawMesh.m_nSubMeshs; n++ )
        {
            xhandle   hSubMesh;
            sub_mesh& SubMesh = Mesh.SubMesh.Add( hSubMesh );
            
            SubMesh.pRawMesh    = &RawMesh;
            SubMesh.pRawSubMesh = &RawMesh.m_pSubMesh[n] ;
        
            char* pName = RawMesh.m_pSubMesh[n].Name;
            s32   Size  = sizeof( SubMesh.Name );
            
            if( x_strlen( pName ) > (Size-1) )
                x_throw( xfs( "Mesh name [%s] is longer than %d characters", pName, Size-1 ) );
            
            x_strcpy( SubMesh.Name, pName );
        
            // Create a display list for each material used in the SubMesh
            for( i=0; i<RawMesh.m_nMaterials; i++ )
            {
                // Is this material used by the SubMesh?
                if( MatUsed[n][i] && (MatMap[i] != -1) )
                {
                    // Create a new dlist for this object
                    xhandle Handle;
                    dlist&  DList    = SubMesh.lDList.Add( Handle );

                    // *INEV* *SB* - Pre-allocate tris!
                    DList.lTri.SetCapacity(RawMesh.m_nFacets) ;

                    DList.iMaterial  = MatMap[ i ];
                    DList.iBone      = -1;

                    // Assign facets to the correct display list
                    for( j=0; j<RawMesh.m_nFacets; j++ )
                    {
                        rawmesh2::facet&  Facet   = RawMesh.m_pFacet[j];
                                                
                        // Check if facet uses this Material and is in the same mesh
                        if( (RawMesh.m_pFacet[j].iMesh     == n) &&
                            (RawMesh.m_pFacet[j].iMaterial == i) )
                        {
                            // Add the facet to the correct bone DList
                            DList.lTri.Append( j );
                        }
                    }
                    
                    if( DList.lTri.GetCount() == 0 )
                        SubMesh.lDList.DeleteByHandle( Handle );
                    
                }
            }

            if( SubMesh.lDList.GetCount() < 0 )
                Mesh.SubMesh.DeleteByHandle( hSubMesh );
        }
    }
    
    if( g_Verbose )
    {
        for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
        {
            sub_mesh& SubMesh = Mesh.SubMesh[i];
        
            x_DebugMsg( "Model %s has %d dlists with materials:\n", SubMesh.Name, SubMesh.lDList.GetCount() );
            for( j=0; j<SubMesh.lDList.GetCount(); j++ )
            {
                x_DebugMsg( "    mat=%d tri=%d\n", SubMesh.lDList[j].iMaterial, SubMesh.lDList[j].lTri.GetCount() );
            }
        }
    }
}

//=============================================================================

void geom_compiler::Export( const char* pFileName, comp_type Type, const char* pTexturePath )
{
    // Keep source path
    if (pTexturePath)
        x_strcpy(m_TexturePath, pTexturePath) ;

    // 
    switch( Type )
    {
        case TYPE_RIGID:
            ExportRigidGeom( pFileName );
            break;
        
        case TYPE_SKIN:
            ExportSkinGeom( pFileName );
            break;

        default:
            x_throw( "Unknown compiler type" );
            break;
    }
}

//=============================================================================

void geom_compiler::ExportRigidGeom( const char* pFileName )
{
    rigid_geom  RigidGeom;
    rawmesh2    RawMesh;
    mesh        Mesh;
    s32         i, j;
    rawmesh2    RawMeshFC;

    //
    // Load the raw-mesh
    //
        
    RawMesh.Load( pFileName );

    if( RawMesh.m_nBones > 1 )
    {
        rawanim     RawAnim;
        xarray<s32> BadSubMesh;
        
        RawAnim.Load( pFileName );

        //
        // Nuke all the submeshes that are bones.
        //
        BadSubMesh.Clear();
        for( j=0; j<RawMesh.m_nSubMeshs; j++ )
        {            
            if( x_stristr( RawMesh.m_pSubMesh[j].Name, "Human_" ) )
                BadSubMesh.Append() = j;
        }
        
        // Nuke all the facets that represent those bones.
        for( j=0; j<RawMesh.m_nFacets; j++ )
        {
            for( s32 t=0; t<BadSubMesh.GetCount(); t++ )
            {
                if( BadSubMesh[t] == RawMesh.m_pFacet[j].iMesh )
                    break;
            }
        
            if( t < BadSubMesh.GetCount() )
            {
                RawMesh.m_pFacet[j] = RawMesh.m_pFacet[ RawMesh.m_nFacets-1 ];
                j--;
                RawMesh.m_nFacets--;
            }
        }            
    
        RawMesh.CleanWeights( 1, 0.00001f );
        RawMesh.CleanMesh();
        RawMesh.SortFacetsByMaterialAndBone();
        
        // Do remapping of the skeleton
        RawAnim.DeleteDummyBones();
        RawMesh.ApplyNewSkeleton( RawAnim );
    }
    else
    {
        RawMesh.CleanMesh();
        RawMesh.SortFacetsByMaterial();
    }


    BuildBasicStruct( RigidGeom, RawMesh, Mesh, TRUE );

    //
    // Check whether we have to load the fast collision data
    //
    if( m_FastCollision[0] )
    {
        RawMeshFC.Load( m_FastCollision );

        // Make sure to simplify the mesh
        for( s32 j=0; j<RawMeshFC.m_nVertices; j++ )
        {
            RawMeshFC.m_pVertex[j].nColors  = 0;
            RawMeshFC.m_pVertex[j].nUVs     = 0;
            RawMeshFC.m_pVertex[j].nNormals = 0;
        }

        RawMeshFC.CleanWeights( 1, 0.00001f );
        RawMeshFC.CleanMesh();

        CompileLowCollision( RigidGeom, RawMeshFC, RawMesh, m_FastCollision );
    }
    else
    {
        CompileLowCollisionFromBBox( RigidGeom, RawMesh );
    }

    // Export to the correct platform
    for( i=0; i<m_PlatInfo.GetCount(); i++ )
    {
        switch( m_PlatInfo[i].Platform )
        {
            case PLATFORM_XBOX:
                ExportRigidGeomXbox( Mesh, RigidGeom, pFileName );
                break;

            case PLATFORM_PS2 :
                ExportRigidGeomPS2( Mesh, RigidGeom, pFileName );
                break;
            
            case PLATFORM_PC :
                ExportRigidGeomPC( Mesh, RigidGeom, pFileName );
                break;
        
            default :
                ASSERT( 0 );
                break;
        }
    }

}

//=============================================================================

f32 ComputeWorldPixelSize( const vector4* pPos, s16* pUV, s32 nVerts, const xbitmap& Bitmap )
{
    f32 WorldPixelSize = 0.0f;
    s32 nTri           = 0;

    for( s32 i=0; i<nVerts; i++, pPos += 1, pUV += 2 )
    {
        f32 ADC = pPos->GetW();
        if( (reinterpret_cast<u32 &>(ADC) & 0x8000) == 0 )
        {
            vector2 Tex[3];
            vector3 V1;
            vector3 V2;

            // Convert UV's from 12:4 fixed point to floating point
            Tex[0].Set( pUV[-4] / (f32)(1 << 12), pUV[-3] / (f32)(1 << 12) );
            Tex[1].Set( pUV[-2] / (f32)(1 << 12), pUV[-1] / (f32)(1 << 12) );
            Tex[2].Set( pUV[ 0] / (f32)(1 << 12), pUV[ 1] / (f32)(1 << 12) );

            // Compute 2 edge vectors on the tri in texel space
            V1.Set( (Tex[2].X - Tex[0].X) * (f32)Bitmap.GetWidth(),
                    (Tex[2].Y - Tex[0].Y) * (f32)Bitmap.GetHeight(),
                    0.0f );
            V2.Set( (Tex[1].X - Tex[0].X) * (f32)Bitmap.GetWidth(),
                    (Tex[1].Y - Tex[0].Y) * (f32)Bitmap.GetHeight(),
                    0.0f );

            // Compute area of texture space used by tri
            f32 TexArea = v3_Cross( V1, V2 ).Length() * 0.5f;

            // Ensure we have at least 1 pixel square of texture
            if( TexArea < 1.0f )
                continue;

            // Compute 2 edge vectors on the tri
            V1 = pPos[ 0] - pPos[-2];
            V2 = pPos[-1] - pPos[-2];
            
            // Compute area of tri
            f32 TriArea = v3_Cross( V1, V2 ).Length() * 0.5f;

            if( (x_isvalid( TriArea ) == FALSE) ||
                (x_isvalid( TexArea ) == FALSE) )
                continue;

            WorldPixelSize += x_sqrt( TriArea ) / x_sqrt( TexArea );
            nTri++;
        }
    }

    f32 Average = 0.001f;

    if( nTri > 0 )
    {
        Average = WorldPixelSize / nTri;
    }

    return( Average );
}

//=============================================================================

void geom_compiler::ExportRigidGeomPS2( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName )
{
    s32         iDList;
    s32         i,j,k;
    faststrip   Strip;
    s32         PlatformID = GetPlatformIndex( PLATFORM_PS2 );
    u32*        pMatList;

    //
    // Allocate memory for the PS2 display list
    //
    
    s32 nPS2DList = 0;
    for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nPS2DList += SubMesh.lDList.GetCount();
    }

    rigid_geom::dlist_ps2*   pPS2DList = new rigid_geom::dlist_ps2[ nPS2DList ];
    if( pPS2DList == NULL )
        x_throw( "Out of memory" );

    //
    // Allocate memory for a temporary material list.
    //

    pMatList = new u32[ nPS2DList ];
    if( !pMatList )
        x_throw( "Out of memory." );

    //
    // Build the display lists
    //
    
    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            //
            // First lets get the indices to be ordered properly
            //
            
            Strip.Open( 0, TRUE ); 
            
            for( k=0; k<DList.lTri.GetCount(); k++ )
            {
                Strip.AddTri(
                    SubMesh.pRawMesh->m_pFacet[ DList.lTri[k] ].iVertex[0], 
                    SubMesh.pRawMesh->m_pFacet[ DList.lTri[k] ].iVertex[1], 
                    SubMesh.pRawMesh->m_pFacet[ DList.lTri[k] ].iVertex[2] );
            }
            
            Strip.Close();

            //
            // Note the material.
            //
            pMatList[iDList] = Mesh.Material[DList.iMaterial].TexInfo.SoundMat;

            //
            // Now build the actual display list
            //
            
            rigid_geom::dlist_ps2& PS2DList = pPS2DList[ iDList++ ];

            s32  nIndices = Strip.GetMaxNumIndices( MAX_VU_VERTS );
            s32* pI = (s32*)x_malloc( nIndices * sizeof( s32 ) );
            ASSERT( pI );
            
            nIndices = Strip.GetIndicesPS2( pI, MAX_VU_VERTS );

            PS2DList.nVerts    = nIndices;
            PS2DList.iBone     = DList.iBone;
            PS2DList.pUV       = new s16    [ PS2DList.nVerts * 2 ];
            PS2DList.pNormal   = new s8     [ PS2DList.nVerts * 3 ];
            PS2DList.pPosition = new vector4[ PS2DList.nVerts * 1 ];
            
            if( PS2DList.pUV       == NULL ||
                PS2DList.pNormal   == NULL ||
                PS2DList.pPosition == NULL )
                x_throw( "Out of memory" );

            for( k=0; k<nIndices; k++ )
            {
                s32             Index   = Strip.GetIndex( pI[ k ] );
                rawmesh2::vertex Vertex = SubMesh.pRawMesh->m_pVertex[ Index ];

                PS2DList.pUV[ k*2+0 ]     = (s16)(Vertex.UV[0].X * (1<<12));
                PS2DList.pUV[ k*2+1 ]     = (s16)(Vertex.UV[0].Y * (1<<12));

                PS2DList.pNormal[ k*3+0 ] = (s8)(Vertex.Normal[0].GetX() * (0xff>>1));
                PS2DList.pNormal[ k*3+1 ] = (s8)(Vertex.Normal[0].GetY() * (0xff>>1));
                PS2DList.pNormal[ k*3+2 ] = (s8)(Vertex.Normal[0].GetZ() * (0xff>>1));

                PS2DList.pPosition[ k ]   = Vertex.Position;

                s32 ExtraBits = 0;
                if( Strip.IsIndexNewStrip( pI[k] ) )   ExtraBits |= (1 << ADCBIT);
                if( Strip.IsIndexCCWTri  ( pI[k] ) )   ExtraBits |= (1 << CCWBIT);

                PS2DList.pPosition[k].GetIW() = ExtraBits;
            }
        }
    }

    //
    // Setup the Geom
    //
    
    geom& Geom       = RigidGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_PS2;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = Mesh.SubMesh.GetCount();
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];
    
    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nPS2DList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //
 
    s32 iColor = 0;

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;


        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before stripping, and the vert count is after stripping
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pPS2DList[iDList].nVerts;

            geom::submesh& SubMesh = Geom.m_pSubMesh[iDList];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;

            pPS2DList[ iDList ].iColor = iColor;

            // Keep each Display Lists color table aligned
            s32 nColor = pPS2DList[ iDList ].nVerts;
            iColor += ALIGN_16( sizeof( u16 ) * nColor ) / sizeof( u16 );
        }
    }        

    ASSERT( iDList == nPS2DList );

    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }

    // Setup the RigidGeom
    RigidGeom.m_nDList      = nPS2DList;
    RigidGeom.m_System.pPS2 = pPS2DList;

    //
    // Save the Materials
    //
    
    ExportMaterial( Mesh, RigidGeom, PlatformID );

    //
    // Compute BBox's and Average World Pixel Size
    //

    RigidGeom.m_BBox.Clear();
    
    char FileName[256];
    x_memset( FileName, 0, sizeof( FileName ) );

    xbitmap Bitmap;
    s32 TotalVertices = 0;        
    for( i=0; i<RigidGeom.m_nMeshes; i++ )
    {
        s32         nVerts = 0;
        geom::mesh& Mesh   = RigidGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = RigidGeom.m_pSubMesh[ iSubMesh ];
            
            rigid_geom::dlist_ps2& DList = RigidGeom.m_System.pPS2[ SubMesh.iDList ];

            nVerts += DList.nVerts;

            for( s32 k=0; k<DList.nVerts; k++ )
            {
                vector3 Pos;
                Pos = DList.pPosition[k];
                Mesh.BBox += Pos;
            }
            
            // Get the first diffuse bitmap
            geom::material& Material  = RigidGeom.m_pMaterial[ SubMesh.iMaterial ];
            const char*     pFileName = RigidGeom.m_pTexture [ Material.iTexture ].FileName;

            char Drive[256];
            char Dir[256];
            char Path[256];

            x_splitpath( m_PlatInfo[ PlatformID ].FileName, Drive, Dir, NULL, NULL );
            x_makepath ( Path, Drive, Dir, NULL, NULL );

            
            if( x_strcmp( pFileName, FileName ) != 0 )
            {
                x_strcpy( FileName, pFileName );

                if( auxbmp_Load( Bitmap, xfs("%s\\%s", Path, pFileName) ) == FALSE )
                    x_throw( xfs( "Unable to load bitmap [%s]", pFileName ) );
            }

            SubMesh.WorldPixelSize = ComputeWorldPixelSize( DList.pPosition, DList.pUV, DList.nVerts, Bitmap );
        }
        
        RigidGeom.m_BBox += Mesh.BBox;

        x_printf( "%30s - %10d Vertices\n", Mesh.Name, nVerts );
        TotalVertices += nVerts;
    }
    x_printf( "%30s - %10d Total Vertices\n", "", TotalVertices );

    //
    // Build the "hi res" collision data.  AndyT
    //
    {
        CompileHighCollisionPS2( RigidGeom, pMatList, pFileName );
    } 

    delete [] pMatList;

    //
    // Save the data
    //
    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName, RigidGeom, FALSE );
}

//=============================================================================

void geom_compiler::ExportRigidGeomXbox( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName )
{
    s32     i,j,k;
    s32     iDList;
    s32     nIndices;
    s32     nXboxDList;
    s32     PlatformID = GetPlatformIndex( PLATFORM_XBOX );
    u32*    pMatList;

    //
    // Allocate memory for the Xbox display list
    //
    
    nXboxDList = 0;
    for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nXboxDList += SubMesh.lDList.GetCount();
    }

    //
    // Allocate memory for a temporary material list.
    //

    pMatList = new u32[ nXboxDList ];
    if( !pMatList )
        x_throw( "Out of memory." );

    //
    // Build the display lists
    //

    const rawmesh2&   RawMesh = *Mesh.SubMesh[0].pRawMesh;
    rawmesh2::vertex* pVertex = RawMesh.m_pVertex;

    rigid_geom::dlist_xbox* pXboxDList     = new rigid_geom::dlist_xbox[ nXboxDList ];
    s32*                  pVertexUsage = new s32[ RawMesh.m_nVertices ];
    
    if( (pXboxDList == NULL) || (pVertexUsage == NULL) )
        x_throw( "Out of memory" );

    s32 TotalVerts   = 0;
    s32 TotalIndices = 0;
 
    // Allocate temporary push buffer
    void* pPushBuffer = x_malloc(( MAX_TEMP_PUSH_BUFFER+D3DPUSHBUFFER_ALIGNMENT )&~D3DPUSHBUFFER_ALIGNMENT );

    s32 iColor = 0;

    // Loop through all SubMeshs
    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        
        // Loop through all Display lists in the SubMesh
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            //
            // Note the material.
            //
            pMatList[iDList] = Mesh.Material[DList.iMaterial].TexInfo.SoundMat;

            rigid_geom::dlist_xbox& XboxDList = pXboxDList[ iDList++ ];
            
            s32 nFacets      = SubMesh.lDList[j].lTri.GetCount();
            nIndices         = nFacets * 3;
            XboxDList.nIndices = nIndices;
            XboxDList.pIndices = new u16[ nIndices ];
            XboxDList.pVert    = new rigid_geom::vertex_xbox[ nIndices ];    // Allocate more than we need
            XboxDList.iBone    = DList.iBone;
            XboxDList.iColor   = -1;

            if( (XboxDList.pIndices == NULL) || (XboxDList.pVert == NULL) )
                x_throw( "Out of memory" );

            TotalIndices += nIndices;

            // Clear vertex usage table
            for( k=0; k<RawMesh.m_nVertices; k++ )
                pVertexUsage[k] = -1;

            s32 nVerts = 0;
            s32 nIndex = 0;
            
            // Loop through all the Tri's in the Display list
            for( k=0; k<DList.lTri.GetCount(); k++ )
            {
                s32 Tri = DList.lTri[k];

                ASSERT( (Tri>=0) && (Tri<RawMesh.m_nFacets) );
                rawmesh2::facet& Facet = RawMesh.m_pFacet[ Tri ];

                // Loop through all verts in the Tri
                for( s32 n=0; n<3; n++ )
                {
                    // Get the index into the global vertex pool.
                    // Convert the index to a DList relative index.
                    s32 GlobalIndex = Facet.iVertex[n];
                    ASSERT( (GlobalIndex>=0) && (GlobalIndex<RawMesh.m_nVertices) );

                    s32 VertexIndex = pVertexUsage[ GlobalIndex ];
                    ASSERT( (VertexIndex==-1) || ((VertexIndex>=0) && (VertexIndex<nVerts)));

                    // Check if we have this vertex already
                    if( VertexIndex == -1 )
                    {
                        // Create the new vertex
                        rigid_geom::vertex_xbox& Vertex = XboxDList.pVert[ nVerts ];

                        Vertex.Pos    = pVertex[ GlobalIndex ].Position;
                        Vertex.UV     = pVertex[ GlobalIndex ].UV[0];

                        // Pack normal
                        vector3& Norm = pVertex[ GlobalIndex ].Normal[0];
                        Vertex.PackedNormal =
                            (((( u32 )( Norm.GetZ() *  511.0f )) & 0x3ff ) << 22L ) |
                            (((( u32 )( Norm.GetY() * 1023.0f )) & 0x7ff ) << 11L ) |
                            (((( u32 )( Norm.GetX() * 1023.0f )) & 0x7ff ) <<  0L );

                        // Store an index to the new vertex
                        pVertexUsage[ GlobalIndex ] = nVerts;
                        VertexIndex = nVerts;
                        nVerts++;
                    }

                    ASSERT( VertexIndex <= 0xFFFF );
                    ASSERT( nIndex < XboxDList.nIndices );
                    
                    // Store the index to the vertex
                    XboxDList.pIndices[ nIndex ] = VertexIndex;
                    nIndex++;
                }
            }

            XboxDList.iColor = iColor;
            XboxDList.nVerts = nVerts;

            iColor     += nVerts;
            TotalVerts += nVerts;

            ASSERT( nIndex == nIndices );

            // Convert to triangle strips
            #if STRIPE_GEOMS
            {
                u16 nGroups;
                PrimitiveGroup* pGroups;
                GenerateStrips( XboxDList.pIndices,XboxDList.nIndices,&pGroups,&nGroups );
                #if _MSC_VER < 1300
                    ASSERTS( 0,"Xbox GeomCompiler MUST be built with Visual Studio.NET 2003\n" ):
                #else
                {
                    if( nGroups > 1 )
                        x_printf( "Warning: %d strips ignored\n",nGroups-1 );
                    u32 TotalSize = 0;
                    if( pPushBuffer )
                    {
                        // Optimise all strips
                        for( u16 i=0;i<nGroups;i++ )
                        {
                            DWORD dwPushSize = MAX_TEMP_PUSH_BUFFER;
                            XGCompileDrawIndexedVertices(
                                ((u8*)pPushBuffer)+TotalSize,
                                & dwPushSize,
                                D3DPT_TRIANGLESTRIP,
                                pGroups[i].numIndices,
                                pGroups[i].indices );
                            TotalSize += dwPushSize;
                        }

                        // Save strips
                        XboxDList.pPushBuffer=( u8*)x_malloc( TotalSize );
                        XboxDList.nPushSize = TotalSize;
                        XboxDList.hPushBuffer = NULL;
                        XboxDList.hVert = NULL;
                        x_memcpy(
                            XboxDList.pPushBuffer,
                            pPushBuffer,
                            TotalSize
                        );
                    }
                }
                #endif
                delete[]pGroups;
            }
            // Optimise small lists( .NET only )
            #elif _MSC_VER >= 1300
            if( pPushBuffer )
            {
                DWORD dwPushSize = MAX_TEMP_PUSH_BUFFER;
                XGCompileDrawIndexedVertices(
                    pPushBuffer,
                    & dwPushSize,
                    D3DPT_TRIANGLELIST,
                    XboxDList.nIndices,
                    XboxDList.pIndices );
                XboxDList.pPushBuffer=( u8*)x_malloc( dwPushSize );
                ASSERT( XboxDList.pPushBuffer );
                XboxDList.nPushSize = dwPushSize;
                XboxDList.hPushBuffer = NULL;
                XboxDList.hVert = NULL;
                x_memcpy(
                    XboxDList.pPushBuffer,
                    pPushBuffer,
                    dwPushSize
                );
            }
            #endif
        }
    }
    x_free( pPushBuffer );

/*
    if( TotalVerts < RawMesh.m_nVertices )
    {
        x_printf("GeomComp TotalVerts:%5d  RawMeshVerts:%5d   FN:%s\n",TotalVerts,RawMesh.m_nVertices,pFileName);
        x_printf("AAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n");
    }
    ASSERT( TotalVerts >= RawMesh.m_nVertices );
*/
    delete pVertexUsage;

    //
    // Setup the Geom
    //
    
    geom& Geom       = RigidGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_XBOX;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = Mesh.SubMesh.GetCount();
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];
    
    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nXboxDList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;

        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before indexing, and the vert count is after indexing
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pXboxDList[ iDList ].nVerts;

            geom::submesh& SubMesh = Geom.m_pSubMesh[ iDList ];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;
        }
    }        

    ASSERT( iDList == nXboxDList );

    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }

    // Setup the RigidGeom
    RigidGeom.m_nDList     = nXboxDList;
    RigidGeom.m_System.pXbox = pXboxDList;

    //
    // Compute BBox's
    //

    RigidGeom.m_BBox.Clear();

    s32 TotalVertices = 0;        

    for( i=0; i<RigidGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = RigidGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        s32 nVerts = 0;
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = RigidGeom.m_pSubMesh[ iSubMesh ];
            
            rigid_geom::dlist_xbox& DList = RigidGeom.m_System.pXbox[ SubMesh.iDList ];
            
            for( s32 k=0; k<DList.nVerts; k++ )
            {
                Mesh.BBox += DList.pVert[k].Pos;
            }

            nVerts += DList.nVerts;
        }

        x_printf( "%30s - %10d Vertices\n", Mesh.Name, nVerts );
        TotalVertices    += nVerts;
        RigidGeom.m_BBox += Mesh.BBox;
    }
    x_printf( "%30s - %10d Total Vertices\n", "", TotalVertices );

    //
    // Save the materials
    //
    
    ExportMaterial( Mesh, RigidGeom, PlatformID );

    //
    // Build the "hi res" collision data.  AndyT
    //
    {
        CompileHighCollisionXBOX( RigidGeom, pMatList, pFileName );
    } 

    delete [] pMatList;

    //
    // Save the data
    //

    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName , RigidGeom, FALSE );
}

//=============================================================================

void geom_compiler::ExportRigidGeomPC( mesh& Mesh, rigid_geom& RigidGeom, const char* pFileName )
{
    s32     i,j,k;
    s32     iDList;
    s32     nIndices;
    s32     nPCDList;
    s32     PlatformID = GetPlatformIndex( PLATFORM_PC );
    u32*    pMatList;

    //
    // Allocate memory for the PC display list
    //
    
    nPCDList = 0;
    for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nPCDList += SubMesh.lDList.GetCount();
    }

    //
    // Allocate memory for a temporary material list.
    //

    pMatList = new u32[ nPCDList ];
    if( !pMatList )
        x_throw( "Out of memory." );

    //
    // Build the display lists
    //

    const rawmesh2&   RawMesh = *Mesh.SubMesh[0].pRawMesh;
    rawmesh2::vertex* pVertex = RawMesh.m_pVertex;

    rigid_geom::dlist_pc* pPCDList     = new rigid_geom::dlist_pc[ nPCDList ];
    s32*                  pVertexUsage = new s32[ RawMesh.m_nVertices ];
    
    if( (pPCDList == NULL) || (pVertexUsage == NULL) )
        x_throw( "Out of memory" );

    s32 TotalVerts   = 0;
    s32 TotalIndices = 0;
 
    // Loop through all SubMeshs
    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        
        // Loop through all Display lists in the SubMesh
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            //
            // Note the material.
            //
            pMatList[iDList] = Mesh.Material[DList.iMaterial].TexInfo.SoundMat;

            rigid_geom::dlist_pc& PCDList = pPCDList[ iDList++ ];
            
            s32 nFacets      = SubMesh.lDList[j].lTri.GetCount();
            nIndices         = nFacets * 3;
            PCDList.nIndices = nIndices;
            PCDList.pIndices = new u16[ nIndices ];
            PCDList.pVert    = new rigid_geom::vertex_pc[ nIndices ];    // Allocate more than we need
            PCDList.iBone    = DList.iBone;
            
            if( (PCDList.pIndices == NULL) || (PCDList.pVert == NULL) )
                x_throw( "Out of memory" );

            TotalIndices += nIndices;

            // Clear vertex usage table
            for( k=0; k<RawMesh.m_nVertices; k++ )
                pVertexUsage[k] = -1;

            s32 nVerts = 0;
            s32 nIndex = 0;
            
            // Loop through all the Tri's in the Display list
            for( k=0; k<DList.lTri.GetCount(); k++ )
            {
                s32 Tri = DList.lTri[k];

                ASSERT( (Tri>=0) && (Tri<RawMesh.m_nFacets) );
                rawmesh2::facet& Facet = RawMesh.m_pFacet[ Tri ];

                // Loop through all verts in the Tri
                for( s32 n=0; n<3; n++ )
                {
                    // Get the index into the global vertex pool.
                    // Convert the index to a DList relative index.
                    s32 GlobalIndex = Facet.iVertex[n];
                    ASSERT( (GlobalIndex>=0) && (GlobalIndex<RawMesh.m_nVertices) );

                    s32 VertexIndex = pVertexUsage[ GlobalIndex ];
                    ASSERT( (VertexIndex==-1) || ((VertexIndex>=0) && (VertexIndex<nVerts)));
                    
                    // Check if we have this vertex already
                    if( VertexIndex == -1 )
                    {
                        // Create the new vertex
                        rigid_geom::vertex_pc& Vertex = PCDList.pVert[ nVerts ];
                        
                        Vertex.Pos    = pVertex[ GlobalIndex ].Position;
                        Vertex.UV     = pVertex[ GlobalIndex ].UV[0];
                        Vertex.Color  = pVertex[ GlobalIndex ].Color[0];
                        Vertex.Normal = pVertex[ GlobalIndex ].Normal[0]; 
                        
                        // Store an index to the new vertex
                        pVertexUsage[ GlobalIndex ] = nVerts;
                        VertexIndex = nVerts;
                        nVerts++;
                    }

                    ASSERT( VertexIndex <= 0xFFFF );
                    ASSERT( nIndex < PCDList.nIndices );
                    
                    // Store the index to the vertex
                    PCDList.pIndices[ nIndex ] = VertexIndex;
                    nIndex++;
                }
            }

            PCDList.nVerts = nVerts;
            TotalVerts    += nVerts;

            ASSERT( nIndex == nIndices );
        }
    }

/*
    if( TotalVerts < RawMesh.m_nVertices )
    {
        x_printf("GeomComp TotalVerts:%5d  RawMeshVerts:%5d   FN:%s\n",TotalVerts,RawMesh.m_nVertices,pFileName);
        x_printf("AAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n");
    }
    ASSERT( TotalVerts >= RawMesh.m_nVertices );
*/
    delete pVertexUsage;

    //
    // Setup the Geom
    //
    
    geom& Geom       = RigidGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_PC;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = Mesh.SubMesh.GetCount();
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];
    
    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nPCDList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;

        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before indexing, and the vert count is after indexing
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pPCDList[ iDList ].nVerts;

            geom::submesh& SubMesh = Geom.m_pSubMesh[ iDList ];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;
        }
    }        

    ASSERT( iDList == nPCDList );

    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }

    // Setup the RigidGeom
    RigidGeom.m_nDList     = nPCDList;
    RigidGeom.m_System.pPC = pPCDList;

    //
    // Compute BBox's
    //

    RigidGeom.m_BBox.Clear();

    s32 TotalVertices = 0;        

    for( i=0; i<RigidGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = RigidGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        s32 nVerts = 0;
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = RigidGeom.m_pSubMesh[ iSubMesh ];
            
            rigid_geom::dlist_pc& DList = RigidGeom.m_System.pPC[ SubMesh.iDList ];
            
            for( s32 k=0; k<DList.nVerts; k++ )
            {
                Mesh.BBox += DList.pVert[k].Pos;
            }

            nVerts        += DList.nVerts;
        }

        x_printf( "%30s - %10d Vertices\n", Mesh.Name, nVerts );
        TotalVertices    += nVerts;
        RigidGeom.m_BBox += Mesh.BBox;
    }
    x_printf( "%30s - %10d Total Vertices\n", "", TotalVertices );

    //
    // Save the materials
    //
    
    ExportMaterial( Mesh, RigidGeom, PlatformID );

    //
    // Build the "hi res" collision data.  AndyT
    //
    {
        CompileHighCollisionPC( RigidGeom, pMatList, pFileName );
    } 

    delete [] pMatList;

    //
    // Save the data
    //

    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName , RigidGeom, FALSE );
}

//=============================================================================

void geom_compiler::ExportSkinGeom( const char* pFileName )
{
    skin_geom   SkinGeom;
    rawmesh2    RawMesh;
    mesh        Mesh;
    s32         i;
    xarray<s32> BadSubMesh;

    //
    // Load the raw-mesh
    //
        
    rawanim RawAnim;
    RawMesh.Load( pFileName );
    RawAnim.Load( pFileName );

// SB 7/1/03 - Removed this "Merridian" special case check - A51 is not using it!
/*
    //
    // Find all the submeshes that are bones
    //
    s32 j ;
    
    for( j=0; j<RawMesh.m_nSubMeshs; j++ )
    {            
        if( x_stristr( RawMesh.m_pSubMesh[j].Name, "Human_" ) )
            BadSubMesh.Append() = j;
    }

    if( RawMesh.m_nSubMeshs == BadSubMesh.GetCount() )
        x_throw( "Unable to find a meshID without 'Human_' which is how I can find the skin" );

    // Nuke all the facets that represent those bones
    for( j=0; j<RawMesh.m_nFacets; j++ )
    {
        for( s32 t=0; t<BadSubMesh.GetCount(); t++ )
        {
            if( BadSubMesh[t] == RawMesh.m_pFacet[j].iMesh )
                break;
        }

        if( t < BadSubMesh.GetCount() )
        {
            RawMesh.m_pFacet[j] = RawMesh.m_pFacet[ RawMesh.m_nFacets-1 ];
            j--;
            RawMesh.m_nFacets--;
        }
    }
*/

    RawMesh.CleanMesh();
    RawMesh.CleanWeights( 2, 0.00001f );
    RawMesh.SortFacetsByMaterialAndBone();

    // Do remapping of the skeleton
    RawAnim.DeleteDummyBones();
    RawMesh.ApplyNewSkeleton( RawAnim );
    
    // Show bones
    if( g_Verbose )
    {
        x_DebugMsg("RawAnim Bone count:%d\n", RawAnim.m_nBones) ;
        for (i = 0 ; i < RawAnim.m_nBones ; i++)
            x_DebugMsg("  Bone:%d %s\n", i, RawAnim.m_pBone[i].Name) ;
    }

    BuildBasicStruct( SkinGeom, RawMesh, Mesh, FALSE );

    // Export to the correct platform
    for( i=0; i<m_PlatInfo.GetCount(); i++ )
    {
        switch( m_PlatInfo[i].Platform )
        {
            case PLATFORM_XBOX :
                ExportSkinGeomXbox( Mesh, SkinGeom, pFileName );
                break;

            case PLATFORM_PS2 :
                ExportSkinGeomPS2( Mesh, SkinGeom, pFileName );
                break;
            
            case PLATFORM_PC :
                ExportSkinGeomPC( Mesh, SkinGeom, pFileName );
                break;

            default :
                ASSERT( 0 );
                break;
        }
    }
}

//=============================================================================

f32 ComputeWorldPixelSize( skin_geom::dlist_ps2& DList, const xbitmap& Bitmap )
{
    // allocate space for verts to be used by the standard ComputeWorldPixelSize function
    vector4* pVert = new vector4[ DList.nPos ];
    s16*     pUV   = new s16[ DList.nPos * 2 ];

    for ( s32 i = 0; i < DList.nPos; i++ )
    {
        pVert[i]         = DList.pPos[i].Pos;
        pVert[i].GetIW() = DList.pPos[i].ADC;
        pUV[i*2+0]       = DList.pUV[i].U;
        pUV[i*2+1]       = DList.pUV[i].V;
    }

    // calculate the world pixel size
    f32 Result = ComputeWorldPixelSize( pVert, pUV, DList.nPos, Bitmap );

    // clean up and return
    delete []pVert;
    delete []pUV;
    
    return Result;
}

//=============================================================================

void geom_compiler::ExportSkinGeomPS2( mesh& Mesh, skin_geom& SkinGeom, const char* pFileName )
{
    s32         iDList;
    s32         i,j,k,l;
    faststrip   Strip;
    s32         PlatformID = GetPlatformIndex( PLATFORM_PS2 );

    //
    // Allocate memory for the PS2 display list
    //
    
    s32 nPS2DList = 0;
    for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nPS2DList += SubMesh.lDList.GetCount();
    }

    skin_geom::dlist_ps2* pPS2DList = new skin_geom::dlist_ps2[ nPS2DList ];
    if( pPS2DList == NULL )
        x_throw( "Out of memory" );

    ps2skin_optimizer SkinOptimizer;
    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            //
            // Add the vertices to the optimizer
            //
            SkinOptimizer.Reset();
            for ( k = 0; k < DList.lTri.GetCount(); k++ )
            {
                ps2skin_optimizer::optimizer_tri Tri;

                for ( s32 iVert = 0; iVert < 3; iVert++ )
                {
                    s32               OrigIndex = SubMesh.pRawMesh->m_pFacet[ DList.lTri[k] ].iVertex[iVert];
                    rawmesh2::vertex* pVert = &SubMesh.pRawMesh->m_pVertex[OrigIndex];

                    Tri.Verts[iVert].iOrigIndex = OrigIndex;
                    Tri.Verts[iVert].nWeights   = pVert->nWeights;
                    for ( s32 iWeight = 0; iWeight < pVert->nWeights; iWeight++ )
                    {
                        Tri.Verts[iVert].iOrigBones[iWeight] = pVert->Weight[iWeight].iBone;
                        Tri.Verts[iVert].fWeights[iWeight]   = pVert->Weight[iWeight].Weight;
                    }
                }
                SkinOptimizer.AddTri( Tri );
            }

            //
            // Optimize for the matrix cache
            //
            SkinOptimizer.Optimize();

            if ( g_Verbose )
            {
                x_printf( "=================================================================\n"  );
                x_printf( "Stats for mesh %d:%d (%s:material #%d)\n",
                          i, j, SubMesh.Name, j );
                SkinOptimizer.PrintStats();
                x_printf( "=================================================================\n"  );
            }

            //
            // Build the final display list
            //
            s32 nMatrixUploads = 0;
            s32 nVertRenders = SkinOptimizer.GetNFinalBatches();
            s32 nVerts = 0;
            for ( k = 0; k < SkinOptimizer.GetNFinalBatches(); k++ )
            {
                const ps2skin_optimizer::final_batch& Batch = SkinOptimizer.GetFinalBatch(k);
                nMatrixUploads += Batch.nBonesToLoad;
                nVerts         += Batch.Verts.GetCount();
            }

            // allocate space for the various display list things
            s32 WorstCasePad     = nVertRenders*4;      // at max 4 extra verts per "render" packet for alignment
            s32 nCmds            = nMatrixUploads + nVertRenders + 1;
            s32 nUVs             = nVerts;
            s32 nPos             = nVerts;
            s32 nBoneIndices     = nVerts;
            s32 nOrigBoneIndices = nVerts;
            s32 nNormals         = nVerts;
            skin_geom::command_ps2*   pCmd       = new skin_geom::command_ps2[nCmds+WorstCasePad];
            skin_geom::uv_ps2*        pUV        = new skin_geom::uv_ps2[nUVs+WorstCasePad];
            skin_geom::pos_ps2*       pPos       = new skin_geom::pos_ps2[nPos+WorstCasePad];
            skin_geom::boneindex_ps2* pBoneIndex = new skin_geom::boneindex_ps2[nBoneIndices+WorstCasePad];
            skin_geom::normal_ps2*    pNormal    = new skin_geom::normal_ps2[nNormals+WorstCasePad];
            if ( !pCmd || !pUV || !pPos || !pBoneIndex || !pNormal )
                x_throw( "Out of Memory" );

            // build the components of the display list
            skin_geom::command_ps2* pCurrCmd  = pCmd;
            s32                     iCurrVert = 0;
            for ( k = 0; k < SkinOptimizer.GetNFinalBatches(); k++ )
            {
                // set up the matrix loads for this batch
                const ps2skin_optimizer::final_batch& Batch = SkinOptimizer.GetFinalBatch(k);
                for ( l = 0; l < Batch.nBonesToLoad; l++ )
                {
                    pCurrCmd->Cmd  = skin_geom::PS2_CMD_UPLOAD_MATRIX;
                    pCurrCmd->Arg1 = Batch.iOrigBones[l];
                    pCurrCmd->Arg2 = Batch.iCacheBones[l];
                    pCurrCmd++;
                }

                // what are the details of this batch?
                skin_geom::command_types_ps2 Cmd  = skin_geom::PS2_CMD_RENDER_VERTS_RIGID;
                s32                          Arg1 = iCurrVert;
                s32                          Arg2 = Batch.Verts.GetCount();

                // add the verts
                for ( l = 0; l < Batch.Verts.GetCount(); l++ )
                {
                    ps2skin_optimizer::optimizer_vert& OptVert = Batch.Verts[l];
                    rawmesh2::vertex*                  pRMVert = &SubMesh.pRawMesh->m_pVertex[OptVert.iOrigIndex];
                    
                    // figure out the weights...
                    ASSERT( (OptVert.nWeights == 1) || (OptVert.nWeights == 2) );
                    ASSERT( OptVert.fWeights[0] >= 0.5f );
                    f32 W0 = OptVert.fWeights[0];
                    f32 W1 = (OptVert.nWeights == 2) ? OptVert.fWeights[1] : 0.0f;
                    ASSERT( W0 >= W1 );
                    ASSERT( (0.0f<=W0) && (W0<=1.0f) );
                    ASSERT( (0.0f<=W1) && (W1<=1.0f) );

                    // if there are two weights, then we need to soft-skin this batch
                    if ( W1 > 0.0f )
                        Cmd = skin_geom::PS2_CMD_RENDER_VERTS_SOFT;

                    // set up the uvs and weights (fixed-point 4.12)
                    pUV[iCurrVert].U  = (s16)(pRMVert->UV[0].X * (1 << 12));
                    pUV[iCurrVert].V  = (s16)(pRMVert->UV[0].Y * (1 << 12));

                    // set up the positions and adcs
                    pPos[iCurrVert].Pos   = pRMVert->Position;
                    pPos[iCurrVert].ADC   = (OptVert.ADC ? (1<<ADCBIT) : 0x0000);
                    pPos[iCurrVert].ADC  |= (OptVert.CCW ? (1<<CCWBIT) : 0x0000);

                    // set up the bone indices (note the bone indices are pre-multiplied by
                    // 4 to avoid calculating the vector offset in microcode)
                    u32 B0, B1;
                    B0 = OptVert.iCacheBones[0];
                    B1 = ((OptVert.nWeights == 2) ? OptVert.iCacheBones[1] : B0);
                    ASSERT( (B0*4 < 256) && (B1*4 < 256) );
                    W0 *= 255.0f;
                    W1 *= 255.0f;
                    W0 = x_round(W0, 1.0f);
                    W1 = x_round(W1, 1.0f);
                    W0 = MIN(255.0f, W0);
                    W1 = MIN(255.0f, W1);
                    W0 = MAX(0.0f, W0);
                    W1 = MAX(0.0f, W1);
                    pBoneIndex[iCurrVert].B0 = (u8)(B0*4);
                    pBoneIndex[iCurrVert].B1 = (u8)(B1*4);
                    pBoneIndex[iCurrVert].W0 = (u8)(W0);
                    pBoneIndex[iCurrVert].W1 = (u8)(W1);
                    s32 Sum = (s32)pBoneIndex[iCurrVert].W0 + (s32)pBoneIndex[iCurrVert].W1;
                    if ( Sum != 255 )
                    {
                        // Sum should normally equal 255, but may be slightly off if there was
                        // any rounding accumulation errors going on above. This safety check
                        // should fix it up.
                        if ( Sum > 255 )
                        {
                            if ( pBoneIndex[iCurrVert].W0 > pBoneIndex[iCurrVert].W1 )
                                pBoneIndex[iCurrVert].W0 -= (Sum-255);
                            else
                                pBoneIndex[iCurrVert].W1 -= (Sum-255);
                        }
                        else if ( Sum < 255 )
                        {
                            if ( pBoneIndex[iCurrVert].W0 > pBoneIndex[iCurrVert].W1 )
                                pBoneIndex[iCurrVert].W1 += (255-Sum);
                            else
                                pBoneIndex[iCurrVert].W0 += (255-Sum);
                        }

                        ASSERT( 255 == ((s32)pBoneIndex[iCurrVert].W0 + (s32)pBoneIndex[iCurrVert].W1) );
                    }

                    // set up the normals (signed fixed-point 1.0.7)
                    pNormal[iCurrVert].Normal[0] = (s8)(pRMVert->Normal[0].GetX() * (0xff>>1));
                    pNormal[iCurrVert].Normal[1] = (s8)(pRMVert->Normal[0].GetY() * (0xff>>1));
                    pNormal[iCurrVert].Normal[2] = (s8)(pRMVert->Normal[0].GetZ() * (0xff>>1));
                    pNormal[iCurrVert].Pad       = 0;

                    // everything is added now...move along to to the next vert
                    iCurrVert++;
                }

                // verts must be a multiple of 4 to avoid alignment issues, so if its less than that
                // pad the data out...
                while ( (iCurrVert % 4) != 0 )
                {
                    pUV[iCurrVert]          = pUV[iCurrVert-1];
                    pPos[iCurrVert]         = pPos[iCurrVert-1];
                    pBoneIndex[iCurrVert]   = pBoneIndex[iCurrVert-1];
                    pNormal[iCurrVert]      = pNormal[iCurrVert-1];

                    // vu1 should never receive this vert or try to render it since its only here for
                    // padding, but just as an added safety measure, we'll set the adc bit
                    pPos[iCurrVert].ADC     = (1<<ADCBIT);

                    iCurrVert++;
                }

                // add the command for this vertex batch
                pCurrCmd->Cmd  = Cmd;
                pCurrCmd->Arg1 = Arg1;
                pCurrCmd->Arg2 = Arg2;
                pCurrCmd++;
            }
            s32 nVertsWithPadding = iCurrVert;

            // looks like we're done
            pCurrCmd->Cmd  = skin_geom::PS2_CMD_END;
            pCurrCmd->Arg1 = 0;
            pCurrCmd->Arg2 = 0;
            pCurrCmd++;

            // sanity check
            ASSERT( pCurrCmd == (pCmd+nCmds) );

            //
            // Setup PS2 Display List
            //
            
            skin_geom::dlist_ps2& PS2DList = pPS2DList[ iDList++ ];
            
            PS2DList.nCmds        = nCmds;
            PS2DList.pCmd         = pCmd;
            PS2DList.nUVs         = nVertsWithPadding;
            PS2DList.pUV          = pUV;
            PS2DList.nPos         = nVertsWithPadding;
            PS2DList.pPos         = pPos;
            PS2DList.nBoneIndices = nVertsWithPadding;
            PS2DList.pBoneIndex   = pBoneIndex;
            PS2DList.nNormals     = nVertsWithPadding;
            PS2DList.pNormal      = pNormal;
        }
    }

    ASSERT( iDList == nPS2DList );

    //
    // Setup the Geom
    //
    
    geom& Geom       = SkinGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_PS2;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = Mesh.SubMesh.GetCount();
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];

    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nPS2DList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;

        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before optimization, and the vert count is after optimization
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pPS2DList[iDList].nPos;

            geom::submesh& SubMesh = Geom.m_pSubMesh[iDList];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;
        }
    }        

    ASSERT( iDList == nPS2DList );

    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }
    
    // Setup the SkinGeom
    SkinGeom.m_nBones      = Mesh.SubMesh[0].pRawMesh->m_nBones;
    SkinGeom.m_nDList      = nPS2DList;
    SkinGeom.m_System.pPS2 = pPS2DList;

    //
    // Save the Materials
    //
    
    ExportMaterial( Mesh, SkinGeom, PlatformID );

    //
    // Compute BBox's
    //

    SkinGeom.m_BBox.Clear();

    char FileName[256];
    x_memset( FileName, 0, sizeof( FileName ) );

    xbitmap Bitmap;
    s32 TotalVertices = 0;
    for( i=0; i<SkinGeom.m_nMeshes; i++ )
    {
        s32         nVerts = 0;
        geom::mesh& Mesh   = SkinGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = SkinGeom.m_pSubMesh[ iSubMesh ];
            
            skin_geom::dlist_ps2& DList = SkinGeom.m_System.pPS2[ SubMesh.iDList ];
            nVerts += DList.nPos;

            for( s32 k=0; k<DList.nPos; k++ )
            {
                vector3 Pos;
                Pos = DList.pPos[k].Pos;
                Mesh.BBox += Pos;
            }

            // Get the first diffuse bitmap
            geom::material& Material  = SkinGeom.m_pMaterial[ SubMesh.iMaterial ];
            const char*     pFileName = SkinGeom.m_pTexture [ Material.iTexture ].FileName;

            char Drive[256];
            char Dir[256];
            char Path[256];

            x_splitpath( m_PlatInfo[ PlatformID ].FileName, Drive, Dir, NULL, NULL );
            x_makepath ( Path, Drive, Dir, NULL, NULL );
            
            if( x_strcmp( pFileName, FileName ) != 0 )
            {
                x_strcpy( FileName, pFileName );

                if( auxbmp_Load( Bitmap, xfs("%s\\%s", Path, pFileName)) == FALSE )
                    x_throw( xfs( "Unable to load bitmap [%s]", pFileName ) );
            }

            SubMesh.WorldPixelSize = ComputeWorldPixelSize( DList, Bitmap );
        }
        
        SkinGeom.m_BBox += Mesh.BBox;
        x_printf( "%30s - %10d Vertices\n", Mesh.Name, nVerts );

        TotalVertices += nVerts;
    }
    x_printf( "%30s - %10d Total Vertices\n", "", TotalVertices );

    //
    // Save the data
    //
    
    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName , SkinGeom, FALSE );
}

//=============================================================================

void geom_compiler::ExportSkinGeomXbox( mesh& Mesh, skin_geom& SkinGeom, const char* pFileName )
{
    s32             i,j,k;
    s32             iDList;
    s32             PlatformID = GetPlatformIndex( PLATFORM_XBOX );

    //
    // Get the total count of facet and indices and dlists
    //

    s32 nXboxDList = 0;
    s32 SubMeshCount = Mesh.SubMesh.GetCount();
    for( i=0; i<SubMeshCount; i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nXboxDList += SubMesh.lDList.GetCount();
    }

    skin_geom::dlist_xbox* pXboxDList = new skin_geom::dlist_xbox[ nXboxDList ];
    if( pXboxDList == NULL )
        x_throw( "Out of memory" );

    ASSERT( Mesh.SubMesh[0].pRawMesh );
    const rawmesh2& RawMesh = *Mesh.SubMesh[0].pRawMesh;

    // Allocate temporary push buffer
    void* pPushBuffer = x_malloc(( MAX_TEMP_PUSH_BUFFER+D3DPUSHBUFFER_ALIGNMENT )&~D3DPUSHBUFFER_ALIGNMENT );

    // Loop through all sub meshes
    for( iDList=i=0; i<SubMeshCount; i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            arm_optimizer Optimizer;
            Optimizer.Build( RawMesh, DList.lTri, (96-16)/4 ); // Leave room for 12 general registers, and 4 reg per bone

            arm_optimizer::section& Section = Optimizer.m_Section;

            //
            //  Copy all the indices
            //

            skin_geom::dlist_xbox& XboxDList = pXboxDList[ iDList++ ];

            XboxDList.nIndices = Section.lTriangle.GetCount() * 3;
            XboxDList.pIndex   = new u16[ XboxDList.nIndices ];
            if( XboxDList.pIndex == NULL )
                x_throw( "Out of memory" );

            for( k=0; k<Section.lTriangle.GetCount(); k++ )
            {
                XboxDList.pIndex[ (k*3) + 0 ] = Section.lTriangle[k].iVertex[0];
                XboxDList.pIndex[ (k*3) + 1 ] = Section.lTriangle[k].iVertex[1];
                XboxDList.pIndex[ (k*3) + 2 ] = Section.lTriangle[k].iVertex[2];
            }

            //
            //  Copy down all the verts
            //  

            XboxDList.nVerts = Section.lVertex.GetCount();
            XboxDList.pVert  = new skin_geom::vertex_xbox[ XboxDList.nVerts ];
            if( !XboxDList.pVert )
                x_throw( "Out of memory");

            for( k=0; k<XboxDList.nVerts; k++ )
            {
                skin_geom::vertex_xbox& Vert = XboxDList.pVert[k];

                // Position
                Vert.Pos = Section.lVertex[k].Position;

                // Bones
                Vert.Bones.X = (f32)Section.lVertex[k].Weight[0].iBone;
                if( Section.lVertex[k].nWeights > 1 )
                    Vert.Bones.Y = (f32)Section.lVertex[k].Weight[1].iBone;
                else
                    Vert.Bones.Y = 0.0f;
                Vert.Bones.X *= 4;
                Vert.Bones.Y *= 4;

                // Pack normal
                vector3& Norm = Section.lVertex[k].Normal[0];
                Vert.PackedNormal =
                    (((( u32 )( Norm.GetZ() *  511.0f )) & 0x3ff ) << 22L ) |
                    (((( u32 )( Norm.GetY() * 1023.0f )) & 0x7ff ) << 11L ) |
                    (((( u32 )( Norm.GetX() * 1023.0f )) & 0x7ff ) <<  0L );

                // UVs
                Vert.UV.X = Section.lVertex[k].UV[0].X;
                Vert.UV.Y = Section.lVertex[k].UV[0].Y;

                // Weights
                Vert.Weights.X = Section.lVertex[k].Weight[0].Weight;
                if( Section.lVertex[k].nWeights > 1 )
                    Vert.Weights.Y = Section.lVertex[k].Weight[1].Weight;
                else
                    Vert.Weights.Y = 0.0f;
            }

            //
            // Copy down all the commands
            //

            XboxDList.nCommands = Section.lCommand.GetCount();
            XboxDList.pCmd      = new skin_geom::command_xbox[ XboxDList.nCommands ];
            if( XboxDList.pCmd == NULL )
                x_throw( "Out of memory" );

            u32 nWritten=0;
            for( k=0; k<XboxDList.nCommands; k++ )
            {
                // Copy the command type
                switch( Section.lCommand[k].Type )
                {
                    // Add matrix to palette
                    case arm_optimizer::UPLOAD_MATRIX: 
                        XboxDList.pCmd[k].Cmd  = skin_geom::XBOX_CMD_UPLOAD_MATRIX;
                        XboxDList.pCmd[k].Arg2 = (u32)Section.lCommand[k].Arg2; // CacheID
                        XboxDList.pCmd[k].Arg1 = (u32)Section.lCommand[k].Arg1; // BoneID
                        break;

                    // Render push buffer
                    case arm_optimizer::DRAW_LIST:

                        #if _MSC_VER < 1300
                            ASSERTS( 0,"Xbox GeomCompiler must be built with Visual Studio.NET 2003\n" );
                        #else
                        {
                            DWORD Start = Section.lCommand[k].Arg1;
                            DWORD End   = Section.lCommand[k].Arg2;

                            #if STRIPE_GEOMS
                            {
                                // Create triangle strips
                                u16 nGroups;
                                PrimitiveGroup* pGroups;
                                GenerateStrips(
                                    XboxDList.pIndex+Start*3,
                                    (End-Start)*3,
                                    &pGroups,
                                    &nGroups );

                                // Increase command buffer if necessary
                                DWORD dwPushSize;
                                if( nGroups > 1 )
                                {
                                    s32 nCmds = XboxDList.nCommands;
                                    XboxDList.nCommands+= nGroups-1;
                                    skin_geom::command_xbox* pNew = new skin_geom::command_xbox[ XboxDList.nCommands ];
                                    x_memcpy(
                                        pNew,
                                        XboxDList.pCmd,
                                        nCmds*sizeof( skin_geom::command_xbox ));
                                    delete[ ]XboxDList.pCmd;
                                    XboxDList.pCmd = pNew;
                                }

                                // Compile all strips
                                for( u16 i=0;i<nGroups;i++ )
                                {
                                    dwPushSize = MAX_TEMP_PUSH_BUFFER-nWritten;
                                    XGCompileDrawIndexedVertices(
                                        ((u8*)pPushBuffer)+nWritten,
                                        &dwPushSize,
                                        D3DPT_TRIANGLESTRIP,
                                        pGroups[i].numIndices,
                                        pGroups[i].indices );

                                    // Save off 
                                    XboxDList.pCmd[k].Cmd = skin_geom::XBOX_CMD_DRAW_SECTION;
                                    XboxDList.pCmd[k].Arg1 = dwPushSize; // NBytes
                                    XboxDList.pCmd[k].Arg2 = nWritten; // Start
                                    nWritten += dwPushSize;
                                }
                                delete[]pGroups;
                            }
                            #else
                            {
                                XGCompileDrawIndexedVertices(
                                    ((u8*)pPushBuffer)+nWritten,
                                    &dwPushSize,
                                    D3DPT_TRIANGLELIST,
                                    (End-Start)*3,
                                    XboxDList.pIndex + Start*3 );
                                XboxDList.pCmd[k].Cmd = skin_geom::XBOX_CMD_DRAW_SECTION;
                                XboxDList.pCmd[k].Arg1 = dwPushSize; // NBytes
                                XboxDList.pCmd[k].Arg2 = nWritten; // Start
                                nWritten += dwPushSize;
                            }
                            #endif

                            // Save strips
                            XboxDList.pPushBuffer=( u8*)x_malloc( nWritten );
                            XboxDList.nPushSize = nWritten;
                            XboxDList.hPushBuffer = NULL;
                            XboxDList.hVert = NULL;
                            x_memcpy(
                                XboxDList.pPushBuffer,
                                pPushBuffer,
                                nWritten
                            );
                        }
                        #endif
                        break;

                    default:
                        x_throw( "Unknown command type while compiling a soft-skin for Xbox" );
                        break;
                }
            }

            //
            //  Copy push buffers into dlist
            //

            if( nWritten )
            {
                XboxDList.pPushBuffer=( u8*)x_malloc( nWritten );
                XboxDList.nPushSize = nWritten;
                XboxDList.hPushBuffer = NULL;
                XboxDList.hVert = NULL;
                x_memcpy(
                    XboxDList.pPushBuffer,
                    pPushBuffer,
                    nWritten
                );
                #ifdef X_DEBUG
                {
                    s32 i;
                    s32 n = XboxDList.nCommands;
                    for( i=0;i<n;i++ )
                    {
                        skin_geom::command_xbox& Cmd = XboxDList.pCmd[i];
                        if( Cmd.Cmd != skin_geom::XBOX_CMD_DRAW_SECTION )
                            continue;
                        u32 Len = Cmd.Arg1; // Push length
                        u32 Off = Cmd.Arg2; // Push offset
                        ASSERT( Off+Len<=XboxDList.nPushSize );
                    }
                }
                #endif
            }
        }
    }
    x_free( pPushBuffer );

    ASSERT( iDList == nXboxDList );

    //
    // Setup the Geom
    //
    
    geom& Geom       = SkinGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_XBOX;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = SubMeshCount;
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];

    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nXboxDList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //

    for( iDList=i=0; i<SubMeshCount; i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;

        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before indexing, and the vert count is after indexing
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pXboxDList[iDList].nVerts;

            geom::submesh& SubMesh = Geom.m_pSubMesh[ iDList ];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;
        }
    }        

    ASSERT( iDList == nXboxDList );
    
    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }

    // Setup the SkinGeom
    SkinGeom.m_nBones       = RawMesh.m_nBones;
    SkinGeom.m_nDList       = nXboxDList;
    SkinGeom.m_System.pXbox = pXboxDList;

    //
    // Compute BBox's
    //

    SkinGeom.m_BBox.Clear();
    
    for( i=0; i<SkinGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = SkinGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = SkinGeom.m_pSubMesh[ iSubMesh ];
            
            skin_geom::dlist_xbox& DList = SkinGeom.m_System.pXbox[ SubMesh.iDList ];

            for( k=0; k<DList.nVerts; k++ )
            {
                vector3 Pos = DList.pVert[k].Pos;
                Mesh.BBox += Pos;
            }
        }
        
        SkinGeom.m_BBox += Mesh.BBox;
    }

    //
    // Save the Materials
    //    
    ExportMaterial( Mesh, SkinGeom, PlatformID );

    //
    // Save the data
    //
    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName , SkinGeom, FALSE );
}

//=============================================================================

void geom_compiler::ExportSkinGeomPC( mesh& Mesh, skin_geom& SkinGeom, const char* pFileName )
{
    s32             i,j,k;
    s32             iDList;
    s32             PlatformID = GetPlatformIndex( PLATFORM_PC );

    //
    // Get the total count of facet and indices and dlists
    //
    
    s32 nPCDList = 0;
    for( i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        nPCDList += SubMesh.lDList.GetCount();
    }

    skin_geom::dlist_pc* pPCDList = new skin_geom::dlist_pc[ nPCDList ];
    if( pPCDList == NULL )
        x_throw( "Out of memory" );

    ASSERT( Mesh.SubMesh[0].pRawMesh );
    const rawmesh2& RawMesh = *Mesh.SubMesh[0].pRawMesh;

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        const sub_mesh& SubMesh = Mesh.SubMesh[i];
        for( j=0; j<SubMesh.lDList.GetCount(); j++ )
        {
            const dlist& DList = SubMesh.lDList[j];

            arm_optimizer Optimizer;
            Optimizer.Build( RawMesh, DList.lTri, (96-10)/4 ); // Leave room for 10 general registers, and 4 reg per bone

            arm_optimizer::section& Section = Optimizer.m_Section;
            
            //
            // Copy all the indices
            //

            skin_geom::dlist_pc& PCDList = pPCDList[ iDList++ ];
            
            PCDList.nIndices = Section.lTriangle.GetCount() * 3;
            PCDList.pIndex   = new s16[ PCDList.nIndices ];
            if( PCDList.pIndex == NULL )
                x_throw( "Out of memory" );
            
            for( k=0; k<Section.lTriangle.GetCount(); k++ )
            {
                PCDList.pIndex[ (k*3) + 0 ] = Section.lTriangle[k].iVertex[0];
                PCDList.pIndex[ (k*3) + 1 ] = Section.lTriangle[k].iVertex[1];
                PCDList.pIndex[ (k*3) + 2 ] = Section.lTriangle[k].iVertex[2];
            }
            
            //
            // Copy down all the verts
            //
            
            PCDList.nVertices = Section.lVertex.GetCount();
            PCDList.pVertex   = new skin_geom::vertex_pc[ PCDList.nVertices ];
            if( PCDList.pVertex == NULL )
                x_throw( "Out of memory");

            for( k=0; k<PCDList.nVertices; k++ )
            {
                PCDList.pVertex[k].Position        = Section.lVertex[k].Position;
                PCDList.pVertex[k].Position.GetW() = (f32)Section.lVertex[k].Weight[0].iBone;
            
                PCDList.pVertex[k].Normal = Section.lVertex[k].Normal[0];
            
                if( Section.lVertex[k].nWeights > 1 )
                    PCDList.pVertex[k].Normal.GetW() = (f32)Section.lVertex[k].Weight[1].iBone;
                else
                    PCDList.pVertex[k].Normal.GetW() = 0.0f;
            
                PCDList.pVertex[k].UVWeights.GetX() = Section.lVertex[k].UV[0].X;
                PCDList.pVertex[k].UVWeights.GetY() = Section.lVertex[k].UV[0].Y;
            
                PCDList.pVertex[k].UVWeights.GetZ() = Section.lVertex[k].Weight[0].Weight;
                if( Section.lVertex[k].nWeights > 1 )
                    PCDList.pVertex[k].UVWeights.GetW() = Section.lVertex[k].Weight[1].Weight;
                else
                    PCDList.pVertex[k].UVWeights.GetW() = 0.0f;
            }
            
            //
            // Copy down all the commands
            //
            
            PCDList.nCommands = Section.lCommand.GetCount();
            PCDList.pCmd      = new skin_geom::command_pc[ PCDList.nCommands ];
            if( PCDList.pCmd == NULL )
                x_throw( "Out of memory" );
            
            for( k=0; k<PCDList.nCommands; k++ )
            {
                // Copy the command type
                switch( Section.lCommand[k].Type )
                {
                case arm_optimizer::DRAW_LIST: 
                    PCDList.pCmd[k].Cmd =  skin_geom::PC_CMD_DRAW_SECTION;
                    break;
            
                case arm_optimizer::UPLOAD_MATRIX: 
                    PCDList.pCmd[k].Cmd =  skin_geom::PC_CMD_UPLOAD_MATRIX;
                    break;
            
                default:
                    x_throw( "Unknown command type while compiling a soft-skin for PC" );
                }
            
                // copy the arguments
                PCDList.pCmd[k].Arg1 = (s16)Section.lCommand[k].Arg1;
                PCDList.pCmd[k].Arg2 = (s16)Section.lCommand[k].Arg2;
            }
        }
    }

    ASSERT( iDList == nPCDList );

    //
    // Setup the Geom
    //
    
    geom& Geom       = SkinGeom;

    Geom.m_nTextures = 0;
    Geom.m_pTexture  = NULL;
    Geom.m_Platform  = PLATFORM_PC;
    
    // Allocate space for all the meshes in the geom
    Geom.m_nMeshes   = Mesh.SubMesh.GetCount();
    Geom.m_pMesh     = new geom::mesh[ Geom.m_nMeshes ];

    // Allocate space for all the submeshes in the geom
    Geom.m_nSubMeshs = nPCDList;
    Geom.m_pSubMesh  = new geom::submesh[ Geom.m_nSubMeshs ];
    
    if( (Geom.m_pMesh == NULL) || (Geom.m_pSubMesh == NULL) )
        x_throw( "Out of memory" );

    // Make sure nobody has fiddled with the array size
    ASSERT( sizeof( Geom.m_pMesh[0].Name ) == sizeof( Mesh.SubMesh[0].Name ) );

    //
    // Setup mesh and submesh structures
    //

    for( iDList=i=0; i<Mesh.SubMesh.GetCount(); i++ )
    {
        // Setup runtime "geom::mesh" name and bone count
        x_strcpy( Geom.m_pMesh[i].Name, Mesh.SubMesh[i].Name );
        Geom.m_pMesh[i].nBones = Mesh.SubMesh[i].pRawSubMesh->nBones ;

        // 1 submesh per display list
        Geom.m_pMesh[i].nSubMeshs = Mesh.SubMesh[i].lDList.GetCount();
        Geom.m_pMesh[i].iSubMesh  = iDList;
        Geom.m_pMesh[i].nVertices = 0;
        Geom.m_pMesh[i].nFaces    = 0;
        
        // Set the material and display list index into the submeshes
        for( k=0; k<Mesh.SubMesh[i].lDList.GetCount(); k++, iDList++ )
        {
            // tally up the vert and face counts...the face count
            // is before indexing, and the vert count is after indexing
            // has occured
            const dlist& DList = Mesh.SubMesh[i].lDList[k];
            Geom.m_pMesh[i].nFaces    += DList.lTri.GetCount();
            Geom.m_pMesh[i].nVertices += pPCDList[iDList].nVertices;

            geom::submesh& SubMesh = Geom.m_pSubMesh[ iDList ];
            
            SubMesh.iDList    = iDList;
            SubMesh.iMaterial = Mesh.SubMesh[i].lDList[k].iMaterial;
        }
    }        

    ASSERT( iDList == nPCDList );
    
    // Count up the faces and vertices
    Geom.m_nFaces    = 0;
    Geom.m_nVertices = 0;
    for ( i = 0; i < Geom.m_nMeshes; i++ )
    {
        Geom.m_nFaces    += Geom.m_pMesh[i].nFaces;
        Geom.m_nVertices += Geom.m_pMesh[i].nVertices;
    }

    // Setup the SkinGeom
    SkinGeom.m_nBones     = RawMesh.m_nBones;
    SkinGeom.m_nDList     = nPCDList;
    SkinGeom.m_System.pPC = pPCDList;

    //
    // Compute BBox's
    //

    SkinGeom.m_BBox.Clear();
    
    for( i=0; i<SkinGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = SkinGeom.m_pMesh[i];
        Mesh.BBox.Clear();
        
        for( s32 j=0; j<Mesh.nSubMeshs; j++ )
        {
            s32            iSubMesh = Mesh.iSubMesh + j;
            geom::submesh& SubMesh  = SkinGeom.m_pSubMesh[ iSubMesh ];
            
            skin_geom::dlist_pc& DList = SkinGeom.m_System.pPC[ SubMesh.iDList ];

            for( k=0; k<DList.nVertices; k++ )
            {
                vector3 Pos;
                Pos = DList.pVertex[k].Position;
                Mesh.BBox += Pos;
            }
        }
        
        SkinGeom.m_BBox += Mesh.BBox;
    }

    //
    // Save the Materials
    //    
    ExportMaterial( Mesh, SkinGeom, PlatformID );

    //
    // Save the data
    //
    
    fileio File;
    File.Save( m_PlatInfo[ PlatformID ].FileName , SkinGeom, FALSE );
}

//=============================================================================

void geom_compiler::BuildTexturePath( char* pPath, const char* pName, s32 PlatformID )
{
    char pFilename[256];
    char pDrive[256];
    char pDir  [256];
    x_splitpath( pName, NULL, NULL, pFilename, NULL );
    x_splitpath( m_PlatInfo[ PlatformID ].FileName, pDrive, pDir, NULL, NULL );
    x_makepath ( pPath, pDrive, pDir, pFilename, ".xbmp" );
}

//=============================================================================

void geom_compiler::ExportDiffuse( const xbitmap& Bitmap, const char* pName, pref_bpp PrefBPP, s32 PlatformID )
{
    // Make a copy of the bitmap since we are going to modify it
    xbitmap BMPToSave = Bitmap;

    // Convert the texture to the correct format
    switch( m_PlatInfo[PlatformID].Platform )
    {
    case PLATFORM_XBOX:
        if( Bitmap.GetWidth( )<16||Bitmap.GetHeight( )<16)
            auxbmp_ConvertToD3D( BMPToSave );
        else
            BMPToSave.CompressDXTC( TRUE );
        break;

    case PLATFORM_PC:
        auxbmp_ConvertToD3D( BMPToSave );
        break;

    case PLATFORM_PS2:
        switch ( PrefBPP )
        {
        default:
            ASSERT( FALSE );

        case PREF_BPP_DEFAULT:
        case PREF_BPP_8:
            BMPToSave.ConvertFormat( xbitmap::FMT_P8_ABGR_8888 );
            break;

        case PREF_BPP_32:
            BMPToSave.ConvertFormat( xbitmap::FMT_32_ABGR_8888 );
            break;

        case PREF_BPP_16:
            BMPToSave.ConvertFormat( xbitmap::FMT_16_ABGR_1555 );
            break;

        case PREF_BPP_4:
            BMPToSave.ConvertFormat( xbitmap::FMT_P4_ABGR_8888 );
            break;
        }
        if ( !BMPToSave.GetNMips()  )
            BMPToSave.BuildMips();

        if ( g_ColoredMips )
            bmp_util::ConvertToColoredMips( BMPToSave );

        bmp_util::ConvertToPS2( BMPToSave, TRUE );
        break;
    }

    // Save the bitmap
    char pPath[256];
    BuildTexturePath( pPath, pName, PlatformID );
    BMPToSave.Save( pPath );
}

//=============================================================================

void geom_compiler::ExportEnvironment( const xbitmap& Bitmap, const char* pName, s32 PlatformID )
{
    // Make a copy of the bitmap since we are going to modify it
    xbitmap BMPToSave = Bitmap;

    // Convert the texture to the correct format
    switch( m_PlatInfo[PlatformID].Platform )
    {
    case PLATFORM_XBOX:
        if( Bitmap.GetWidth( )<16||Bitmap.GetHeight( )<16)
            auxbmp_ConvertToD3D( BMPToSave );
        else
            BMPToSave.CompressDXTC( TRUE );
        break;

    case PLATFORM_PC:
        auxbmp_ConvertToD3D( BMPToSave );
        break;

    case PLATFORM_PS2:
        if ( BMPToSave.GetBPP() > 8 )
            bmp_util::ConvertToPalettized(BMPToSave, 8);
        BMPToSave.ConvertFormat( xbitmap::FMT_P8_ABGR_8888 );
        bmp_util::ConvertToPS2( BMPToSave, TRUE );
        break;
    }

    // Save the bitmap
    char pPath[256];
    BuildTexturePath( pPath, pName, PlatformID );
    BMPToSave.Save( pPath );
}

//=============================================================================

static xbitmap s_BuildFromAlpha( xbitmap& Bitmap )
{
    xbitmap Temp;
    s32 nMips = Bitmap.GetNMips ();
    s32 W     = Bitmap.GetWidth ();
    s32 H     = Bitmap.GetHeight();
    Temp.Setup(
         xbitmap::FMT_A8,
         W,
         H,
         TRUE,
         NULL,
         FALSE,
         NULL,
         -1,
         nMips );
    s32  iMip;
    for( iMip=0;iMip<=nMips;iMip++ )
    {
        u8* pData = (u8*)Temp.GetPixelData( iMip );
        s32 x,y;
        for( y=0;y<H;y++ )
        {
            for( x=0;x<W;x++ )
            {
                pData[0] = Bitmap.GetPixelColor( x,y,iMip ).A;
                pData++;
            }
        }
        W >>= 1;
        H >>= 1;
    }
    return Temp;
}

//=============================================================================

void geom_compiler::ExportDetail( const xbitmap& Bitmap, const char* pName, s32 PlatformID )
{
    // Make a copy of the bitmap since we are going to modify it
    xbitmap BMPToSave = Bitmap;

    // Convert the texture to the correct format
    switch( m_PlatInfo[PlatformID].Platform )
    {
    case PLATFORM_XBOX:
        bmp_util::ProcessDetailMap( BMPToSave, FALSE );
        BMPToSave = s_BuildFromAlpha( BMPToSave );
        break;

    case PLATFORM_PC:
        bmp_util::ProcessDetailMap( BMPToSave, FALSE );
        auxbmp_ConvertToD3D( BMPToSave );
        break;

    case PLATFORM_PS2:
        bmp_util::ProcessDetailMap( BMPToSave, TRUE );
        ASSERT( BMPToSave.GetBPP() <= 8 );
        BMPToSave.ConvertFormat( xbitmap::FMT_P4_ABGR_8888 );
        bmp_util::ConvertToPS2( BMPToSave, FALSE );
        break;
    }

    // Save the bitmap
    char pPath[256];
    BuildTexturePath( pPath, pName, PlatformID );
    BMPToSave.Save( pPath );
}

//=============================================================================

/*
void geom_compiler::ExportTexture( const xbitmap& a_Bitmap, const char* pName, char* pPath, s32 PlatformID, xbool bReducePS2Alpha, xbool bBuildMips )
{
    char PathName[256];
    char FileName[256];

    // Make a copy of the bitmap since we are going to modify it
    xbitmap Bitmap = a_Bitmap;
    
    // Convert the texture to the correct format
    switch( m_PlatInfo[ PlatformID ].Platform )
    {
        case PLATFORM_XBOX:
            auxbmp_ConvertToD3D( Bitmap );
            break;

        case PLATFORM_PS2 :
            if ( Bitmap.GetFormatInfo().BPP > 8 )
            {
                ConvertToPalettized( Bitmap );
            }
            Bitmap.ConvertFormat( xbitmap::FMT_P8_ABGR_8888 );
            if ( !Bitmap.GetNMips() && bBuildMips )
            {
                Bitmap.BuildMips();
            }
            ConvertToPS2( Bitmap, bReducePS2Alpha );
            FixAlpha( Bitmap );
            break;
        
        case PLATFORM_PC :
            auxbmp_ConvertToD3D( Bitmap );
            break;

        default:
            x_throw( "Unknown platform" );
            break;
    }
    
    // Save the bitmap
    char pDrive[256];
    char pDir  [256];
    x_splitpath( pName, NULL, NULL, FileName, NULL );
    x_splitpath( m_PlatInfo[ PlatformID ].FileName, pDrive, pDir, NULL, NULL );
    x_makepath ( PathName, pDrive, pDir, FileName, ".xbmp" );
    Bitmap.Save( PathName );
    
    x_strcpy( pPath, pName );
}
*/


//=============================================================================
// NOTE: MixMat is the material that this bitmap conbines to. Such Specular 
//       texture into a diffuse texture. Note that this assumes that pMixMat is
//       already fill in by this function.
//=============================================================================
xbool geom_compiler::IsBitmapNew( 
    const char* pFileName, 
    map_slot&   Map, 
    platform    PlatformID, 
    u32         CheckSum, 
    map_slot*   pMixMat )
{
    char    pDrive[256];
    char    pDir  [256];
    char    pExt  [256];
    char    pFName[256];
    char    OutputDrive   [256];
    char    OutputDir     [256];
    char    OutputFileName[256];
    s32     TimeBias      = 10;   // This is use to help for the multiprocessing. 
                                  // What happens is that while one process is saving the file the other one
                                  // is trying to load it! ;-(

    x_splitpath( m_PlatInfo[ PlatformID ].FileName, OutputDrive, OutputDir, NULL, NULL );
    x_splitpath( pFileName, pDrive, pDir, pFName, pExt );

    // Check if we have an animated bitmap
    if( x_stricmp( pExt, ".ifl" ) == 0 )
    {
        token_stream T;
        xbool        bFail = TRUE;
         
        // Load IFL file and extract filenames
        if( T.OpenFile( pFileName ) == FALSE )
        {
            x_printf( "ERROR: Unable to load [%s] \n",pFileName );
            return FALSE;
        }

        // Lets assume that is not out of date
        Map.bOutOfDate = FALSE;

        // Load each bitmap listed in IFL file
        while( T.IsEOF() == FALSE )
        {
            char* pFile = T.ReadSymbol();
        
            if( x_strlen( pFile ) > 0 )
            {
                char                SuBFileName[256];
                char                SuBExtName[256];
                char                SubPathName[256];
                struct _finddata_t  SourceData;
                struct _finddata_t  DestData;
                long                hSubFile;
                char                OutputName[256];

                // Get the sub file name
                x_splitpath( pFile, NULL, NULL, SuBFileName, SuBExtName );

                // Create the texture file output name
                x_sprintf  ( OutputName, "%s%d.xbmp", SuBFileName, CheckSum );

                // Make the source file name
                x_makepath ( SubPathName, pDrive, pDir, SuBFileName, SuBExtName);

                // Make the destination file name
                x_makepath ( OutputFileName, OutputDrive, OutputDir, OutputName, NULL );

                // Check times
                hSubFile = _findfirst( SubPathName, &SourceData );
                if( hSubFile == -1 ) 
                {
                    x_printf( "ERROR: Unable to load [%s] \n",SubPathName );
                    bFail = FALSE;
                    continue;
                }
                _findclose( hSubFile );

                if( pMixMat && pMixMat->lOutBitmapName.GetCount() )
                {            
                    s32 Index = iMin( Map.lOutBitmapName.GetCount(), pMixMat->lOutBitmapName.GetCount() );

                    char Path[256];
                    x_makepath( Path, OutputDrive, OutputDir, pMixMat->lOutBitmapName[Index], NULL );

                    hSubFile  = _findfirst( Path, &DestData );
                }
                else
                {    
                    hSubFile = _findfirst( OutputFileName, &DestData );
                }

                if( hSubFile != -1 ) 
                {
                    _findclose( hSubFile );
                    if( (SourceData.time_write > (DestData.time_write-TimeBias)) ||
                        (g_ExeData.time_write > (DestData.time_write-TimeBias)) )
                        Map.bOutOfDate = TRUE;
                    else
                        Map.bOutOfDate = FALSE;
                }
                else
                {
                    Map.bOutOfDate = TRUE;
                }

                Map.lOutBitmapName.Append() = OutputName;
            }
        }

        return bFail;
    }
    else
    {
        struct _finddata_t  SourceData;
        struct _finddata_t  DestData;
        long                hSubFile;
        char                OutputName[256];

        // Create the texture file output name
        x_sprintf  ( OutputName, "%s%d.xbmp", pFName, CheckSum );
        x_makepath ( OutputFileName, OutputDrive, OutputDir, OutputName, NULL );

        // Check times
        hSubFile = _findfirst( pFileName, &SourceData );
        if( hSubFile == -1 ) 
        {
            x_printf( "ERROR: Unable to load [%s] \n",pFileName );
            return FALSE;
        }
        _findclose( hSubFile );

        if( pMixMat && pMixMat->lOutBitmapName.GetCount() )
        {   
            char Path[256];
            x_makepath( Path, OutputDrive, OutputDir, pMixMat->lOutBitmapName[0], NULL );

            hSubFile = _findfirst( Path, &DestData );
        }
        else
        {    
            hSubFile = _findfirst( OutputFileName, &DestData );
        }

        if( hSubFile != -1 ) 
        {
            _findclose( hSubFile );
            if( (SourceData.time_write > (DestData.time_write-TimeBias)) ||
                (g_ExeData.time_write > (DestData.time_write-TimeBias)) )
                Map.bOutOfDate = TRUE;
            else
                Map.bOutOfDate = FALSE;
        }
        else
        {
            Map.bOutOfDate = TRUE;
        }

        Map.lOutBitmapName.Append() = OutputName;
    }

    return TRUE;
}

//=============================================================================

void geom_compiler::ReadBitmap( const char* pFileName, map_slot& Map )
{
    char pDrive[256];
    char pDir  [256];
    char pExt  [256];

    x_splitpath( pFileName, pDrive, pDir, NULL, pExt );

    // Check if we have an animated bitmap
    if( x_stricmp( pExt, ".ifl" ) == 0 )
    {
        token_stream T;
         
        // Load IFL file and extract filenames
        if( T.OpenFile( pFileName ) == FALSE )
            x_throw( xfs( "Unable to open file [%s]", pFileName ) );

        // Load each bitmap listed in IFL file
        while( T.IsEOF() == FALSE )
        {
            char* pFile = T.ReadSymbol();
        
            if( x_strlen( pFile ) > 0 )
            {
                char pPath[256];
                x_strcpy( pPath, xfs( "%s%s%s", pDrive, pDir, pFile ) );
                
                xbitmap& Bitmap = Map.lBitmap.Append();
        
                if( Map.bOutOfDate )
                {
                    if( auxbmp_Load( Bitmap, pPath ) == FALSE )
                        x_throw( xfs( "Unable to load texture [%s]", pPath ) );
                }

                Map.lBitmapName.Append() = pPath;
            }
        }
    }
    else
    {
        // Load a single bitmap
        xbitmap& Bitmap = Map.lBitmap.Append();
        
        if( Map.bOutOfDate )
        {
            if( auxbmp_Load( Bitmap, pFileName ) == FALSE )
                x_throw( xfs( "Unable to load texture [%s]", pFileName ) );
        }

        Map.lBitmapName.Append() = pFileName;
    }
    
    // Convert to ARGB and perform sanity checks on loaded bitmaps
    if( Map.bOutOfDate )
    {
        for( s32 i=0; i<Map.lBitmap.GetCount(); i++ )
        {
            xbitmap& Bitmap = Map.lBitmap[i];

            Bitmap.ConvertFormat( xbitmap::FMT_32_ARGB_8888 );

            // Check the dimensions are correct
            if( (Bitmap.GetWidth() > MaxTextureWidth) || (Bitmap.GetHeight() > MaxTextureHeight) )
                x_throw( xfs( "Texture [%s] (%d x %d) is too big. Max size is (%d x %d)",
                    pFileName, Bitmap.GetWidth(), Bitmap.GetHeight(), MaxTextureWidth, MaxTextureHeight ) );

            if( ( ( (Bitmap.GetWidth() -1) & Bitmap.GetWidth()  ) != 0 ) ||
                ( ( (Bitmap.GetHeight()-1) & Bitmap.GetHeight() ) != 0 ) )
                x_throw( xfs( "Texture [%s] (%d x %d) must have a power of 2 width and height",
                    pFileName, Bitmap.GetWidth(), Bitmap.GetHeight() ) );
        }

        ASSERT( Map.lBitmap.GetCount() == Map.lBitmapName.GetCount() );
    }
}

//=============================================================================

void geom_compiler::ProcessIntensityMap( map_slot& DiffuseMap, map_slot& IntensityMap )
{
    if( IntensityMap.nBitmap == 1 )
    {
        // Copy the single alpha intensity map to all diffuse maps
        for( s32 i=0; i<DiffuseMap.nBitmap; i++ )
        {
            xbitmap& Diffuse   = DiffuseMap.lBitmap[i];
            xbitmap& Intensity = IntensityMap.lBitmap[0];

            if( bmp_util::CopyIntensityToDiffuse( Diffuse, Intensity ) == FALSE )
                x_throw( xfs( "Diffuse [%s] and Map texture [%s] have different dimensions",
                    DiffuseMap.pFileName, IntensityMap.pFileName ) );
        }
    }
    else
    {
        if( DiffuseMap.nBitmap != IntensityMap.nBitmap )
            x_throw( xfs( "Number of frames in [%s] not equal to [%s]",
                DiffuseMap.pFileName, IntensityMap.pFileName ) );
    
        // Copy each alpha intensity map to each diffuse map
        for( s32 i=0; i<DiffuseMap.nBitmap; i++ )
        {
            xbitmap& Diffuse   = DiffuseMap.lBitmap[i];
            xbitmap& Intensity = IntensityMap.lBitmap[i];
        
            if( bmp_util::CopyIntensityToDiffuse( Diffuse, Intensity ) == FALSE )
                x_throw( xfs( "Diffuse [%s] and Map texture [%s] have different dimensions",
                    DiffuseMap.pFileName, IntensityMap.pFileName ) );
        }
    }
}

//=============================================================================

void geom_compiler::ProcessPunchThruMap( map_slot& DiffuseMap, map_slot& PunchThruMap )
{
    if( PunchThruMap.nBitmap == 1 )
    {
        // Copy the single alpha punch-thru map to all diffuse maps
        for( s32 i=0; i<DiffuseMap.nBitmap; i++ )
        {
            xbitmap& Diffuse   = DiffuseMap.lBitmap[i];
            xbitmap& PunchThru = PunchThruMap.lBitmap[0];

            if( bmp_util::SetPunchThrough( Diffuse, PunchThru ) == FALSE )
                x_throw( xfs( "Diffuse [%s] and PunchThru texture [%s] have different dimensions",
                    DiffuseMap.pFileName, PunchThruMap.pFileName ) );
        }
    }
    else
    {
        if( DiffuseMap.nBitmap != PunchThruMap.nBitmap )
            x_throw( xfs( "Number of frames in [%s] not equal to [%s]",
                DiffuseMap.pFileName, PunchThruMap.pFileName ) );
    
        // Copy each alpha punch-thru map to each diffuse map
        for( s32 i=0; i<DiffuseMap.nBitmap; i++ )
        {
            xbitmap& Diffuse   = DiffuseMap.lBitmap[i];
            xbitmap& PunchThru = PunchThruMap.lBitmap[i];
        
            if( bmp_util::SetPunchThrough( Diffuse, PunchThru ) == FALSE )
                x_throw( xfs( "Diffuse [%s] and PunchThru texture [%s] have different dimensions",
                    DiffuseMap.pFileName, PunchThruMap.pFileName ) );
        }
    }
}

//=============================================================================

void ExportUVAnimation( const rawmesh2::material& RawMat,
                        const f32*                pParamKey,
                        geom::material&           Material,
                        geom&                     Geom )
{
    const rawmesh2::param_pkg& Param  = RawMat.Map[ Max_Diffuse1 ].UVTranslation;
    geom::material::uvanim&    UVAnim = Material.UVAnim;

	x_memset( &UVAnim, 0, sizeof( UVAnim ) );

    // Check if we have any animation data
    if( Param.nKeys == 0 )
        return;
    
    // For now we will only handle U and V pairs
    if( Param.nParamsPerKey != 2 )
        x_throw( xfs( "Number of UV Animation Params per Key is not 2! [%s]\n", RawMat.Name ) );
    
    x_DebugMsg( "nKeys=%d iFirstKey=%d nParamsPerKey=%d FPS=%d\n",
        Param.nKeys,
        Param.iFirstKey,
        Param.nParamsPerKey,
        Param.FPS );

    UVAnim.Type  = 0;			// TODO: need to support anim types!
	UVAnim.FPS   = Param.FPS;
    UVAnim.iKey  = Geom.m_nUVKeys;
    UVAnim.nKeys = Param.nKeys;
    
    // Copy the UV key pairs into the Geom
    for( s32 i=0; i<Param.nKeys * Param.nParamsPerKey; i += Param.nParamsPerKey )
    {
        s32 iKey = RawMat.iFirstKey + i;
        
        Geom.m_pUVKey[ Geom.m_nUVKeys ].OffsetU = pParamKey[ iKey + 0 ];
        Geom.m_pUVKey[ Geom.m_nUVKeys ].OffsetV = pParamKey[ iKey + 1 ];
        
        Geom.m_nUVKeys++;
    }
}

//=============================================================================

void geom_compiler::ExportMaterial( mesh& Mesh, geom& Geom, s32 PlatformID )
{
    // Global list of textures used by mesh
    xbool                   bFailure = FALSE;
    xarray<geom::texture>   Textures;
    s32                     nTotalTextures = 0;

    // Allocate space for materials
    Geom.m_nMaterials = Mesh.Material.GetCount();
    Geom.m_pMaterial  = new geom::material[ Geom.m_nMaterials ];

    Geom.m_nUVKeys    = 0;
    Geom.m_pUVKey     = new geom::uvkey[ MAX_UV_KEYS ];

    // animated texture info offset
    s32 iTexAnimFrame = 0;

    if( Geom.m_pMaterial == NULL )
        x_throw( "Out of memory" );

    for( s32 i=0; i<Mesh.Material.GetCount(); i++ )
    {
        // List of maps used by Material Editor in 3DS Max
        map_slot Maps[ NumMaps ];

        // Get the material from the RawMesh
        material&           Mat     = Mesh.Material[i];
        const rawmesh2&     RawMesh = *Mat.pRawMesh;
        rawmesh2::material& RawMat  = RawMesh.m_pMaterial[ Mat.iRawMaterial ];
        s32                 j;

        // Setup some handy references to each map
        map_slot& DiffuseMap     = Maps[ Max_Diffuse1     ];
        map_slot& EnvironmentMap = Maps[ Max_Environment  ];
        map_slot& DetailMap      = Maps[ Max_DetailMap    ];
        map_slot& PunchThruMap   = Maps[ Max_PunchThrough ];

        //
        // Determine whether or not we need an environment map
        //        
        xbool   bNeedsEnviroment = FALSE;
        
        switch( RawMat.Type )
        {
            case Material_Diff : 
            case Material_Alpha :
            case Material_Diff_PerPixelIllum :
            case Material_Alpha_PerPixelIllum :
            case Material_Alpha_PerPolyIllum :
                break;
            
            case Material_Diff_PerPixelEnv :
            case Material_Alpha_PerPolyEnv :
                if ( RawMat.Constants[MaxConst_EnvType].Current[0] != 0.0f )
                {
                    bNeedsEnviroment = TRUE;
                }
                break;
            
            default :
                x_throw( xfs( "Unknown Material Type %d", RawMat.Type ) );
                break;
        }

        // Set whether we have a punch-through image
        xbool     bHasPunchThrough = ( RawMat.Map[ Max_PunchThrough ].iTexture ) != -1;

        //
        // Determine whether we need to export the textures
        //
        for( j=0; j<NumMaps; j++ )
        {
            if ( (j == Max_Intensity) || (j == Max_Opacity) || (j == Max_SelfIllumination) )
            {
                continue;
            }

            // Get the texture index from the material
            s32 iTexture = RawMat.Map[j].iTexture;
                            
            if( iTexture >= RawMesh.m_nTextures )
                x_throw( xfs( "Invalid texture index %d (max %d)\n", iTexture, RawMesh.m_nTextures ) );

            // Is this map used?
            if( iTexture != -1 )
            {
                const char* pFileName = RawMesh.m_pTexture[ iTexture ].FileName;
                u32 CheckSum = 0;

                if( x_stristr( pFileName, m_TexturePath ) == 0 )
                {
                    x_printf( "ERROR: Texture [%s] is not in path [%s]\n", pFileName, m_TexturePath );
                    bFailure = TRUE;
                    continue;
                }

                // If is the diffuse then try to create a check sum for all the other texture combinations
                if( j == Max_Diffuse1 )
                {
                    if( bHasPunchThrough )
                    {
                        s32 Index = RawMat.Map[ Max_PunchThrough ].iTexture;
                        char    PathName[256];
                        x_strcpy( PathName, RawMesh.m_pTexture[ Index ].FileName );
                        x_strtoupper( PathName );
                        CheckSum += x_chksum( PathName, x_strlen(PathName) );
                    }
                }

                // Collect all the output anames and check whether we need to actually load textures
                Maps[j].lOutBitmapName.Clear();
                if( j == Max_PunchThrough )
                {
                    if( IsBitmapNew( pFileName, Maps[j], (platform)PlatformID, CheckSum, &DiffuseMap ) == FALSE )
                    {
                        bFailure = TRUE;
                    }
                }
                else
                {
                    if( IsBitmapNew( pFileName, Maps[j], (platform)PlatformID, CheckSum ) == FALSE )
                    {
                        bFailure = TRUE;
                    }
                }
            }
        }

        if( bFailure == TRUE )
            continue;

        // Set the dependencies for the diffuse texture
        // (If either the punchthrow texture has change we must reload the diffuse
        if( bHasPunchThrough )
        {
            DiffuseMap.bOutOfDate |= Maps[Max_PunchThrough].bOutOfDate;
        }

        // Punchthrow must always be loade if diffuse needs to be loaded
        if( bHasPunchThrough )
        {
            Maps[Max_PunchThrough].bOutOfDate |= DiffuseMap.bOutOfDate;
        }
       
        //
        // Load all bitmaps used by this material
        //
        for( j=0; j<NumMaps; j++ )
        {
            if ( (j == Max_Intensity) || (j == Max_Opacity) || (j == Max_SelfIllumination) )
            {
                continue;
            }
            
            // Get the texture index from the material
            s32 iTexture = RawMat.Map[j].iTexture;
                            
            ASSERT( iTexture < RawMesh.m_nTextures );

            // Is this map used?
            if( iTexture != -1 )
            {
                const char* pFileName = RawMesh.m_pTexture[ iTexture ].FileName;

                Maps[j].lBitmap.Clear();
                Maps[j].lBitmapName.Clear();
                
                ReadBitmap( pFileName, Maps[j] );
                
                Maps[j].pFileName = pFileName;
                Maps[j].nBitmap   = Maps[j].lBitmap.GetCount();
            }
            else
            {
                Maps[j].lBitmap.Clear();
                Maps[j].lBitmapName.Clear();
                Maps[j].pFileName = NULL;
                Maps[j].nBitmap   = 0;
            }
        }
        
        // Must always have at least a diffuse texture
        if( DiffuseMap.pFileName == NULL )
        {
            x_printf( "ERROR: No Diffuse texture was specified\n" );
            bFailure = TRUE;
            continue;
        }

        if( bNeedsEnviroment && EnvironmentMap.nBitmap == 0 )
        {
            x_printf( "ERROR: No Enviroment texture has been specify\n" );
            bFailure = TRUE;
            continue;
        }
    
        //
        // PunchThrough Map
        //        
        if( bHasPunchThrough )//PunchThruMap.pFileName != NULL )
        {
            if( DiffuseMap.bOutOfDate  )
                ProcessPunchThruMap( DiffuseMap, PunchThruMap );
        }
            
        //
        // Export Diffuse Map
        //
        for( j=0; j<DiffuseMap.nBitmap; j++ )
        {
            geom::texture* pTexture = &Textures.Append();
            if( DiffuseMap.bOutOfDate )
                ExportDiffuse( DiffuseMap.lBitmap[j], DiffuseMap.lOutBitmapName[j], Mat.TexInfo.PreferredBPP, PlatformID );

            x_strcpy( pTexture->FileName, DiffuseMap.lOutBitmapName[j] );
        }
        
        //
        // Export Environment Map
        //        
        for( j=0; j<EnvironmentMap.nBitmap; j++ )
        {
            geom::texture* pTexture = &Textures.Append();
            if( EnvironmentMap.bOutOfDate )
                ExportEnvironment( EnvironmentMap.lBitmap[j], EnvironmentMap.lOutBitmapName[j], PlatformID );

            x_strcpy( pTexture->FileName, EnvironmentMap.lOutBitmapName[j] );
        }

        //
        // Export Detail Map
        //       
        for( j=0; j<DetailMap.nBitmap; j++ )
        {
            geom::texture* pTexture = &Textures.Append();
            if( DetailMap.bOutOfDate )
            {
                ExportDetail( DetailMap.lBitmap[j], DetailMap.lOutBitmapName[j], PlatformID );
            }

            x_strcpy( pTexture->FileName, DetailMap.lOutBitmapName[j] );
        }

        //
        // Setup Material
        //
        geom::material& Material = Geom.m_pMaterial[i];
        
        Material.Type            = (material_type)RawMat.Type;
        Material.nTextures       = DiffuseMap.nBitmap + EnvironmentMap.nBitmap + DetailMap.nBitmap;
        Material.iTexture        = nTotalTextures;
        Material.nDiffuse        = DiffuseMap.nBitmap;
        Material.nEnvironment    = EnvironmentMap.nBitmap;
        Material.nDetail         = DetailMap.nBitmap;
        Material.nTexAnimFrames  = MAX( Material.nDiffuse, MAX( Material.nEnvironment, Material.nDetail ) );
        Material.iTexAnimFrame   = iTexAnimFrame;
        iTexAnimFrame += Material.nTexAnimFrames;

        s32 nMaxMaterialParams      = rawmesh2::MATERIAL_MAX_CONSTANTS;
        s32 nCompilerMaterialParams = geom::material::MAX_PARAMS - nMaxMaterialParams;
        for( j=0; j < nMaxMaterialParams; j++ )
        {
            Material.Params[j] = RawMat.Constants[j].Current[0];
        }
        Material.Params[nMaxMaterialParams+0] = bHasPunchThrough ? 1.0f : 0.0f;
        Material.Params[nMaxMaterialParams+1] = 0.0f;       // currently unused
        Material.Params[nMaxMaterialParams+2] = 0.0f;       // currently unused
        Material.Params[nMaxMaterialParams+3] = 0.0f;       // currently unused

        ExportUVAnimation( RawMat, RawMesh.m_pParamKey, Material, Geom );
        
        nTotalTextures += Material.nTextures;
    }

    //
    // Check whether we have already fail
    //
    if( bFailure == TRUE )
        x_throw( "ERROR: Stop compilation due to textures errors\n" );


    //
    // Copy final textures to Geom
    //
    Geom.m_nTextures = nTotalTextures;
    Geom.m_pTexture  = new geom::texture[ Geom.m_nTextures ];
    
    if( Geom.m_pTexture == NULL )
        x_throw( "Out of memory" );

    for( s32 n=0; n<nTotalTextures; n++ )
    {
        Geom.m_pTexture[n] = Textures[n];
    }

    //
    // Create space for the texture animation info
    //
    Geom.m_nTexAnimFrames = iTexAnimFrame;
    Geom.m_pTexAnimFrames = new geom::texanimframe[Geom.m_nTexAnimFrames];
}

//=============================================================================
//=============================================================================
//=============================================================================
// BUILDING LOW POLY COLLISION GEOMETRY -A51
//=============================================================================
//=============================================================================
//=============================================================================

s32 DetermineMeshBone( rawmesh2& Mesh, s32 iMesh )
{
    s32 iBone=-1;

    s32 I;
    for( I=0; I<Mesh.m_nFacets; I++ )
    if( Mesh.m_pFacet[I].iMesh == iMesh )
    {
        iBone = Mesh.m_pVertex[ Mesh.m_pFacet[I].iVertex[0] ].Weight[0].iBone;
        break;
    }

    return iBone;
}
 
//=============================================================================

void geom_compiler::CompileLowCollision( rigid_geom&    RigidGeom, 
                                         rawmesh2&      LowMesh, 
                                         rawmesh2&      HighMesh, 
                                         const char*    pFileName )
{
    s32 i,j;

    collision_data& Coll = RigidGeom.m_Collision;

    rawmesh2& Raw                      = LowMesh;
    rawmesh2& HighRaw                  = HighMesh;
    s32* pHighMeshIndex                = new s32[ HighRaw.m_nSubMeshs ];

    x_memset( pHighMeshIndex, -1, sizeof(s32)*HighRaw.m_nSubMeshs );
    if( Raw.m_nSubMeshs > HighRaw.m_nSubMeshs )
        x_throw( xfs("ERROR: Collision model mesh count exceeds the high model mesh count, File: [%s]", m_FastCollision ) );

    //
    // Check if the collision mesh references the high mesh correctly
    //
    for( i = 0; i < Raw.m_nSubMeshs; i++ )
    {
        s32 iMatch = -1;
        s32 nMatches=0;
        s32 nTies=0;

        char LowName[256];
        x_strcpy(LowName,Raw.m_pSubMesh[i].Name);
        s32 Len = x_strlen(LowName);

        // Be sure _c is present
        if( ((LowName[Len-1] != 'c') && (LowName[Len-1] != 'C')) ||(LowName[Len-2] != '_') )
            x_throw( xfs("ERROR: Collision mesh needs '_c' [%s], File [%s]\n", Raw.m_pSubMesh[i].Name, m_FastCollision) );

        LowName[Len-2] = 0;

        for( j = 0; j < HighRaw.m_nSubMeshs; j++ )
        {
            if( x_stricmp( HighRaw.m_pSubMesh[j].Name, LowName )==0 )
                break;
        }

        if( j == -1 )
            x_throw( xfs("ERROR: Unreferenced collision mesh [%s], File [%s]\n", Raw.m_pSubMesh[i].Name, m_FastCollision) );

        if( pHighMeshIndex[i] != -1 )
            x_throw( xfs("ERROR: Multiple references from the collision mesh [%s] to the High mesh [%s], File [%s]\n", HighRaw.m_pSubMesh[j].Name, Raw.m_pSubMesh[i].Name, m_FastCollision) );
                
        // Set the mesh index.
        pHighMeshIndex[i] = j;
    }


    // Reset the mesh indexes and bones indices inside the facets to match
    // the high geometry mesh
    {
        s32 iHighMesh = -1;
        s32 iHighMeshBone = -1;
        for( i = 0; i < Raw.m_nFacets; i++ )
        {
            if( pHighMeshIndex[ Raw.m_pFacet[i].iMesh ] != iHighMesh )
            {
                iHighMesh = pHighMeshIndex[ Raw.m_pFacet[i].iMesh ];
                iHighMeshBone = DetermineMeshBone( HighRaw, iHighMesh );
            }

            // Copy new iMesh into facet
            Raw.m_pFacet[i].iMesh = iHighMesh;

            // Copy new bone index into verts
            s32 iLowBone = Raw.m_pVertex[Raw.m_pFacet[i].iVertex[0]].Weight[0].iBone;
            Raw.m_pVertex[Raw.m_pFacet[i].iVertex[0]].Weight[0].iBone = iHighMeshBone;
            Raw.m_pVertex[Raw.m_pFacet[i].iVertex[1]].Weight[0].iBone = iHighMeshBone;
            Raw.m_pVertex[Raw.m_pFacet[i].iVertex[2]].Weight[0].iBone = iHighMeshBone;

            //x_DebugMsg("LowBone(%2d,%s)  HighBone(%2d,%s)\n",
            //   iLowBone,Raw.m_pBone[iLowBone].Name,iHighMeshBone,HighRaw.m_pBone[iHighMeshBone].Name);
        }
    }
    
    // This will sort the facets by meshes then material.
    Raw.SortFacetsByMaterial();

    //
    //*********************************************************************
    //
    {

        //
        // Report results!!!
        //
        {
            X_FILE* fp = NULL;//x_fopen("c:/temp/quadlists.txt","at");
            if( fp )
            {
                x_fprintf(fp,"%s\n",pFileName);
                x_fflush(fp);
                x_fclose(fp);
            }
        }

        #define NUM_NORMAL_PER_QFACET 1
        struct qfacet
        {
            xbool   bIsQuad;
            xbool   bIsMerged;
            xbool   bInCluster;

            s32     iMesh;
            s32     iBone;
            //s32     iMaterial;

            vector3 Point[4];
            bbox    BBox;
            plane   Plane;
            vector3 Normal[NUM_NORMAL_PER_QFACET];

            s32     iPoint[4];
            s32     iNormal[NUM_NORMAL_PER_QFACET];
            xbool   bNegateNormal[NUM_NORMAL_PER_QFACET];

        };

        qfacet* pQF = (qfacet*)x_malloc(sizeof(qfacet)*Raw.m_nFacets);
        s32 nQFs = 0;

        // Build a QFacet out of every facet
        for( i = 0; i < Raw.m_nFacets; i++ )
        {
            // See if we already have a cluster for this facet.
            s32 iMesh       = Raw.m_pFacet[i].iMesh;
            s32 iBone       = Raw.m_pVertex[ Raw.m_pFacet[i].iVertex[0] ].Weight[0].iBone;
            u32 iMaterial   = Raw.m_pFacet[i].iMaterial;
        
            //
            // Check if this facet is some dump sliver
            //
            vector3 P0 = Raw.m_pVertex[ Raw.m_pFacet[i].iVertex[0] ].Position;
            vector3 P1 = Raw.m_pVertex[ Raw.m_pFacet[i].iVertex[1] ].Position;
            vector3 P2 = Raw.m_pVertex[ Raw.m_pFacet[i].iVertex[2] ].Position;
            if( ((P0-P1).LengthSquared() < x_sqr(0.5f)) ||
                ((P1-P2).LengthSquared() < x_sqr(0.5f)) ||
                ((P2-P0).LengthSquared() < x_sqr(0.5f)) )
            {
                /*
                static X_FILE* fp = NULL;
                if( !fp ) fp = x_fopen("c:/temp/slivers.txt","at");
                if( fp )
                {
                    x_fprintf(fp,"SLIVER: (%4d) (%10.5f,%10.5f,%10.5f) (%10.5f,%10.5f,%10.5f) (%10.5f,%10.5f,%10.5f)\n",
                        i,
                        P0.X,P0.Y,P0.Z,
                        P1.X,P1.Y,P1.Z,
                        P2.X,P2.Y,P2.Z
                        );
                    x_fflush(fp);
                }
                */
                continue;
            }

            // Check for collapse by colinear
            vector3 Normal = v3_Cross( P1-P0, P2-P0 );
            if( Normal.Length() < 0.01f )
                continue;

            qfacet& QF = pQF[nQFs];
            nQFs++;

            QF.bIsQuad      = FALSE;
            QF.bIsMerged    = FALSE;
            QF.bInCluster   = FALSE;
            QF.iMesh        = Raw.m_pFacet[i].iMesh;
            QF.iBone        = Raw.m_pVertex[ Raw.m_pFacet[i].iVertex[0] ].Weight[0].iBone;
            //QF.iMaterial    = Raw.m_pFacet[i].iMaterial;

            QF.Point[0] = P0;
            QF.Point[1] = P1;
            QF.Point[2] = P2;
            QF.Point[3] = P0;

            QF.Normal[0] = v3_Cross( P1-P0, P2-P0 );
            QF.Normal[0].SafeNormalize();

            QF.Plane.Normal = QF.Normal[0];
            QF.Plane.D      = -QF.Normal[0].Dot( P1 );

            // BBox and EdgeNormals are computed AFTER merging quads
        }

        //
        // Merge qfacets into quads
        //
        for( s32 A=0; A<nQFs; A++ )
        {
            qfacet& QA = pQF[A];
            if( QA.bIsMerged ) continue;

            for( s32 B=A+1; B<nQFs; B++ )
            {
                qfacet& QB = pQF[B];

                // Already merged into a quad
                if( QB.bIsMerged ) continue;

                // Normals don't match up
                if( QB.Normal[0].Dot(QA.Normal[0]) < 0.999999f ) continue;

                // Be sure mesh, bone, and material match
                if( QB.iMesh != QA.iMesh ) continue;
                if( QB.iBone != QA.iBone ) continue;
                //if( QB.iMaterial != QA.iMaterial ) continue;

                vector3 BPoint[3] = {QB.Point[0],QB.Point[1],QB.Point[2]};
                vector3 QPoint[4]; 
 
                // Check if verts match
                s32 S=0;
                for( S=0; S<3; S++ )
                {
                    if( (QA.Point[0] == BPoint[0]) )
                    {
                        if( QA.Point[1] == BPoint[2] )
                        {
                            QPoint[0] = QA.Point[1];
                            QPoint[1] = QA.Point[2];
                            QPoint[2] = QA.Point[0];
                            QPoint[3] = BPoint[1];
                            break;
                        }

                        if( QA.Point[2] == BPoint[1] )
                        {
                            QPoint[0] = QA.Point[0];
                            QPoint[1] = QA.Point[1];
                            QPoint[2] = QA.Point[2];
                            QPoint[3] = BPoint[2];
                            break;
                        }
                    }

                    if( (QA.Point[1] == BPoint[0]) && (QA.Point[2] == BPoint[2]) )
                    {
                        QPoint[0] = QA.Point[2];
                        QPoint[1] = QA.Point[0];
                        QPoint[2] = QA.Point[1];
                        QPoint[3] = BPoint[1];
                        break;
                    }

                    vector3 TP = BPoint[0];
                    BPoint[0] = BPoint[1];
                    BPoint[1] = BPoint[2];
                    BPoint[2] = TP;
                }

                if( S!=3 )
                {
                    //
                    // We can convert this poly into a quad!!!
                    //
                    QA.bIsQuad = TRUE;
                    QA.Point[0] = QPoint[0];
                    QA.Point[1] = QPoint[1];
                    QA.Point[2] = QPoint[2];
                    QA.Point[3] = QPoint[3];
                    QB.bIsMerged = TRUE;

                    // Stop looping through B
                    break;
                }

            }
        }

        //
        // Clear out merged qfacets and finish computing bbox, etc.
        //
        s32 nQuads=0;
        s32 nTris=0;
        s32 i=0;
        for( j=0; j<nQFs; j++ )
        {
            if( pQF[j].bIsMerged ) continue;

            qfacet& QF = pQF[i];
            QF = pQF[j];
 
            i++;

            if( QF.bIsQuad )
                nQuads++;
            else
                nTris++;

            QF.BBox.Clear();
            QF.BBox.AddVerts( QF.Point, 4 );

            /*
            for( s32 k=0; k<4; k++ )
            {
                QF.Normal[k+1] = QF.Normal[0].Cross(QF.Point[(k+1)%4]-QF.Point[k]);
                QF.Normal[k+1].Normalize();
            }
            */
        }
        nQFs = i;

        #define MAX_QFACETS_PER_CLUSTER   32
        #define MAX_POINTS_PER_CLUSTER    32
        #define MAX_NORMALS_PER_CLUSTER   32

        struct qcluster
        {
            bbox    BBox;
            qfacet  QF[MAX_QFACETS_PER_CLUSTER];
            vector3 Point[MAX_POINTS_PER_CLUSTER];
            vector3 Normal[MAX_NORMALS_PER_CLUSTER];
            s32     nQFs;
            s32     nPoints;
            s32     nNormals;
            s32     iMesh;
            s32     iBone;
            //s32     iMaterial;
        };
        qcluster* pQC  = NULL;
        s32       nQCs = 0;

        while( 1 )
        {
            // Find a QF to open a new cluster for
            f32 BestStartingCorner = F32_MAX;
            s32 BestStartingQF = -1;
            {
                for( s32 i=0; i<nQFs; i++ )
                if( !pQF[i].bInCluster )
                {
                    f32 Corner = pQF[i].BBox.Min.GetX() + 
                                 pQF[i].BBox.Min.GetY() + 
                                 pQF[i].BBox.Min.GetZ();

                    if( Corner < BestStartingCorner )
                    {
                        BestStartingCorner = Corner;
                        BestStartingQF = i;
                    }
                }
            }

            // If we couldn't find any unclustered facets then we're finished
            if( BestStartingQF==-1 )
                break;

            // Open a new cluster
            nQCs++;
            pQC = (qcluster*)x_realloc( pQC, sizeof(qcluster)*nQCs );
            qcluster& QC = pQC[nQCs-1];

            // Init cluster with first quad
            {
                QC.iMesh        = pQF[BestStartingQF].iMesh;
                QC.iBone        = pQF[BestStartingQF].iBone;
                //QC.iMaterial    = pQF[BestStartingQF].iMaterial;
                QC.BBox         = pQF[BestStartingQF].BBox;

                QC.QF[0]        = pQF[BestStartingQF];
                QC.nQFs         = 1;

                QC.nPoints      = 4;
                for( s32 i=0; i<4; i++ )
                {
                    QC.Point[i] = QC.QF[0].Point[i];
                    QC.QF[0].iPoint[i] = i;
                }

                QC.nNormals         = 1;
                QC.Normal[0]        = QC.QF[0].Normal[0];
                QC.QF[0].iNormal[0] = 0;
                QC.QF[0].bNegateNormal[0] = FALSE;
                pQF[BestStartingQF].bInCluster = TRUE;
                
            }

            // Loop through QFs and fill cluster
            while( QC.nQFs < MAX_QFACETS_PER_CLUSTER )
            {
                s32 BestQF           = -1;
                s32 BestNPointsAdded = S32_MAX;
                s32 BestNNormalsAdded = S32_MAX;
                s32 BestPointI[4]    = {-1,-1,-1,-1};
                s32 BestNormalI[NUM_NORMAL_PER_QFACET] = {-1};

                //
                // Loop through all QFs picking the one that will add the
                // fewest points to the cluster
                //
                for( s32 i=0; i<nQFs; i++ )
                if( (pQF[i].bInCluster  == FALSE) &&
                    (pQF[i].iMesh       == QC.iMesh) &&
                    (pQF[i].iBone       == QC.iBone) //&&
                    //(pQF[i].iMaterial   == QC.iMaterial) 
                    )
                {
                    // Get short reference to the facet
                    qfacet& QF = pQF[i];

                    //
                    // Be sure points would fit in cluster
                    //
                    xbool bPointsFit = TRUE;
                    s32 PointI[4] = {-1,-1,-1,-1};
                    s32 nPointsAdded = 0;
                    {
                        s32 NewNPoints = QC.nPoints;
                        for( s32 i=0; i<4; i++ )
                        {
                            s32 j;
                            vector3 P = QF.Point[i];

                            for( j=0; j<NewNPoints; j++ )
                            if( QC.Point[j] == P )
                                break;

                            if( j==NewNPoints )
                            {
                                // We can't add any more
                                if( NewNPoints == MAX_POINTS_PER_CLUSTER )
                                {
                                    bPointsFit = FALSE;
                                    break;
                                }

                                // We can add a new one
                                PointI[i] = j;
                                QC.Point[NewNPoints] = P;
                                NewNPoints++;
                                nPointsAdded++;
                            }
                            else
                            {
                                PointI[i] = j;
                            }
                        }
                    }

                    // Don't continue unless this one will be better
                    if( bPointsFit && (nPointsAdded<=BestNPointsAdded) )
                    {

                        //
                        // Be sure normals will fit in cluster
                        //
                        xbool bNormalsFit = TRUE;
                        s32 NormalI[NUM_NORMAL_PER_QFACET] = {-1};
                        xbool NegateNormal[NUM_NORMAL_PER_QFACET] = {0};
                        s32 nNormalsAdded = 0;
                        {
                            s32 NewNNormals = QC.nNormals;
                            for( s32 i=0; i<NUM_NORMAL_PER_QFACET; i++ )
                            {
                                s32 j;
                                vector3 N = QF.Normal[i];

                                for( j=0; j<NewNNormals; j++ )
                                if( (QC.Normal[j].Dot(N)) > 0.999999f )
                                    break;

                                if( j==NewNNormals )
                                {
                                    // We can't add any more
                                    if( NewNNormals == MAX_NORMALS_PER_CLUSTER )
                                    {
                                        bNormalsFit = FALSE;
                                        break;
                                    }

                                    // We can add a new one
                                    NormalI[i] = j;
                                    QC.Normal[NewNNormals] = N;
                                    NewNNormals++;
                                    nNormalsAdded++;
                                }
                                else
                                {
                                    NormalI[i] = j;
                                }
                            }
                        }

                        // If points and normals fit then check if it's better
                        if( bPointsFit && bNormalsFit)
                        {
                            //
                            // If we added less points or added the same number
                            // of points but fewer normals then take this one as the best
                            //
                            if( (nPointsAdded<BestNPointsAdded) ||
                                ((nPointsAdded==BestNPointsAdded) && (nNormalsAdded<BestNNormalsAdded)) )
                            {
                                BestQF   = i;
                                BestNPointsAdded = nPointsAdded;
                                BestNNormalsAdded = nNormalsAdded;

                                for( j=0; j<4; j++ )
                                    BestPointI[j] = PointI[j];

                                for( j=0; j<NUM_NORMAL_PER_QFACET; j++ )
                                {
                                    BestNormalI[j] = NormalI[j];
                                }
                            }
                        }
                    }
                }           
                if( BestQF == -1 )
                    break;

                // Add Best QF to the QC
                {
                    qfacet& QF = QC.QF[QC.nQFs];

                    pQF[BestQF].bInCluster = TRUE;
                    QF = pQF[BestQF];
                    QC.nQFs++;
                    QC.BBox += QF.BBox;

                    // Add points
                    s32 i;
                    for( i=0; i<4; i++ )
                    {
                        ASSERT( BestPointI[i] != -1 );
                        ASSERT( BestPointI[i] < MAX_POINTS_PER_CLUSTER );
                        QC.Point[ BestPointI[i] ] = QF.Point[i];
                        QC.nPoints = MAX( QC.nPoints, (BestPointI[i]+1) );
                        QF.iPoint[i] = BestPointI[i];
                    }

                    // Add normals
                    for( i=0; i<NUM_NORMAL_PER_QFACET; i++ )
                    {
                        ASSERT( BestNormalI[i] != -1 );
                        ASSERT( BestNormalI[i] < MAX_NORMALS_PER_CLUSTER );
                        QC.Normal[ BestNormalI[i] ] = QF.Normal[i];
                        QC.nNormals = MAX( QC.nNormals, (BestNormalI[i]+1) );
                        QF.iNormal[i] = BestNormalI[i];
                    }

                    // Verify normals and points match up
                    #ifdef X_ASSERT
                    {
                        vector3 N1 = QF.Normal[0];
                        vector3 N2 = QC.Normal[ QF.iNormal[0] ];
                        vector3 N3;
                        plane Plane;
                        Plane.Setup( QF.Point[0], QF.Point[1], QF.Point[2] );
                        N3 = Plane.Normal;

                        ASSERT( N1.Dot(N2) >= 0.999f );
                        ASSERT( N2.Dot(N3) >= 0.999f );
                        ASSERT( N3.Dot(N1) >= 0.999f );


                        ASSERT( QC.Point[ QF.iPoint[0] ] == QF.Point[0] );
                        ASSERT( QC.Point[ QF.iPoint[1] ] == QF.Point[1] );
                        ASSERT( QC.Point[ QF.iPoint[2] ] == QF.Point[2] );
                    }
                    #endif
                }
            }
        }

        //
        // Sort the clusters by iBone
        //
        {
            for( s32 A=0; A<nQCs; A++ )
            {
                s32 Best = A;
                for( s32 B=A+1; B<nQCs; B++ )
                {
                    if( pQC[B].iBone < pQC[Best].iBone )
                        Best = B;
                }

                if( Best != A )
                {
                    qcluster TQC = pQC[Best];
                    pQC[Best]     = pQC[A];
                    pQC[A]        = TQC;
                }
            }
        }

        //
        // Report results!!!
        //
        #ifdef X_ASSERT
        {
            X_FILE* fp = x_fopen("c:/temp/quadlists.txt","at");
            if( fp )
            {
                s32 i;

                s32 TotalPoints = 0;
                s32 TotalNormals = 0;
                for( i=0; i<nQCs; i++ )
                {
                    TotalPoints += pQC[i].nPoints;
                    TotalNormals += pQC[i].nNormals;
                }

                s32 TotalSize = 0;
                TotalSize += nQCs * sizeof(collision_data::low_cluster);
                TotalSize += sizeof(vector4)*TotalPoints;
                TotalSize += sizeof(vector4)*TotalNormals;
                TotalSize += nQFs * sizeof(collision_data::low_quad);

                x_fprintf(fp,"%s\n",pFileName);
                x_fprintf(fp,"<<%5d>> %8d QF:%d Q:%d T:%d P:%d N:%d\n",
                    TotalSize,
                    Raw.m_nFacets,
                    nQFs,nQuads,nTris,
                    TotalPoints,TotalNormals);

                for( i=0; i<nQCs; i++ )
                {
                    vector3 Size = pQC[i].BBox.GetSize();
                    x_fprintf(fp,"[%4d]  Q(%3d) P(%3d) N(%3d) MS(%3d) BN(%3d) (%7.0f,%7.0f,%7.0f)\n",
                        i, 
                        pQC[i].nQFs, 
                        pQC[i].nPoints,
                        pQC[i].nNormals,
                        pQC[i].iMesh, 
                        pQC[i].iBone, 
                        //pQC[i].iMaterial,
                        Size.GetX(), Size.GetY(), Size.GetZ() );
                }

                x_fflush(fp);
                x_fclose(fp);
            }
        }
        #endif

        //
        // Build final structures
        //
        {
            s32 i;

            s32 TotalPoints     = 0;
            s32 TotalNormals    = 0;
            s32 TotalQuads      = 0;
            s32 TotalVectors    = 0;
            s32 TotalClusters   = nQCs;

            for( i=0; i<TotalClusters; i++ )
            {
                TotalPoints     += pQC[i].nPoints;
                TotalNormals    += pQC[i].nNormals;
                TotalQuads      += pQC[i].nQFs;
                TotalVectors    += pQC[i].nPoints;
                TotalVectors    += pQC[i].nNormals;
            }

            vector3* pVector                      = (vector3*)x_malloc(sizeof(vector3)*TotalVectors);
            collision_data::low_quad* pQuad       = (collision_data::low_quad*)x_malloc(sizeof(collision_data::low_quad)*TotalQuads);
            collision_data::low_cluster* pCluster = (collision_data::low_cluster*)x_malloc(sizeof(collision_data::low_cluster)*TotalClusters);

            if( (!pQuad) || (!pVector) )
                x_throw("Out of memory");

            //
            // Do we need to exlude
            //
            if( g_ExcludeLowCollision == FALSE )
            {
                s32 QuadOffset = 0;
                s32 VectorOffset = 0;
                for( i=0; i<TotalClusters; i++ )
                {
                    qcluster& QC = pQC[i];
                    collision_data::low_cluster& CL = pCluster[i];

                    CL.iBone            = QC.iBone;
                    CL.iMesh            = QC.iMesh;
                    //CL.iMaterial        = QC.iMaterial;
                    CL.BBox             = QC.BBox;
                    CL.iQuadOffset      = QuadOffset;
                    CL.iVectorOffset    = VectorOffset;
                    CL.nNormals         = QC.nNormals;
                    CL.nPoints          = QC.nPoints;
                    CL.nQuads           = QC.nQFs;

                    QuadOffset   += CL.nQuads;
                    VectorOffset += (CL.nPoints + CL.nNormals);

                    // Fill out points
                    for( j=0; j<CL.nPoints; j++ )
                    {
                        vector3& V = pVector[ CL.iVectorOffset+j ];
                        V        = QC.Point[j];
                    }

                    // Fill out normals
                    for( j=0; j<CL.nNormals; j++ )
                    {
                        vector3& V = pVector[ CL.iVectorOffset+CL.nPoints+j ];
                        V        = QC.Normal[j];
                    }

                    // Fill out quads
                    for( j=0; j<CL.nQuads; j++ )
                    {
                        collision_data::low_quad& QD = pQuad[ CL.iQuadOffset+j ];
                        QD.iP[0] = QC.QF[j].iPoint[0];
                        QD.iP[1] = QC.QF[j].iPoint[1];
                        QD.iP[2] = QC.QF[j].iPoint[2];
                        QD.iP[3] = QC.QF[j].iPoint[3];
                        QD.iN    = QC.QF[j].iNormal[0];
                        QD.Flags = (QC.QF[j].Point[3]==QC.QF[j].Point[0]) ? (0):(1);
                    }
                }

                Coll.nLowClusters   = TotalClusters;
                Coll.nLowQuads      = TotalQuads;
                Coll.nLowVectors    = TotalVectors;
                Coll.pLowCluster    = pCluster;
                Coll.pLowVector     = pVector;
                Coll.pLowQuad       = pQuad;
            }
            else
            {
                // Well the user does want us to have the low collision geometry
                Coll.nLowClusters   = 0;
                Coll.nLowQuads      = 0;
                Coll.nLowVectors    = 0;
                Coll.pLowCluster    = pCluster;
                Coll.pLowVector     = pVector;
                Coll.pLowQuad       = pQuad;
            }
        }

        // Cleanup
        x_free(pQF);
        x_free(pQC);

    }
    //
    //*********************************************************************
    //

}

//=============================================================================

void geom_compiler::CompileLowCollisionFromBBox( rigid_geom& RigidGeom, 
                                                 rawmesh2&   HighMesh )
{
    rawmesh2 BBoxMesh;
    BBoxMesh.m_nBones = 1;
    BBoxMesh.m_pBone = new rawmesh2::bone;
    BBoxMesh.m_nVFrames = 0;
    BBoxMesh.m_nTextures = 0;
    BBoxMesh.m_pTexture = NULL;
    BBoxMesh.m_nMaterials = 1;
    BBoxMesh.m_pMaterial = new rawmesh2::material;
    BBoxMesh.m_nParamKeys = 0;
    BBoxMesh.m_pParamKey = NULL;


    BBoxMesh.m_nVertices = 8;
    BBoxMesh.m_pVertex = new rawmesh2::vertex[8];
    BBoxMesh.m_nFacets = 12;
    BBoxMesh.m_pFacet  = new rawmesh2::facet[12];
    BBoxMesh.m_nSubMeshs = 1;
    BBoxMesh.m_pSubMesh = new rawmesh2::sub_mesh;

    BBoxMesh.m_pBone[0] = HighMesh.m_pBone[0];
    BBoxMesh.m_pMaterial[0] = HighMesh.m_pMaterial[0];
    BBoxMesh.m_pSubMesh[0] = HighMesh.m_pSubMesh[0];
    x_strcpy(BBoxMesh.m_pSubMesh[0].Name,xfs("%s_c",HighMesh.m_pSubMesh[0].Name));

    bbox HighBBox = HighMesh.GetBBox();

    // If Size is too thin along a certain dimension then inflate the bbox.
    {
        f32 MinSize = 1.0f;
        for( s32 i=0; i<3; i++ )
        {
            if( (HighBBox.Max[i]-HighBBox.Min[i]) < MinSize )
            {
                f32 Center = (HighBBox.Max[i]+HighBBox.Min[i])*0.5f;
                HighBBox.Min[i] = Center - MinSize*0.5f;
                HighBBox.Max[i] = Center + MinSize*0.5f;
            }
        }
    }

    // Indices used to convert min + max of bbox into 8 corners
    static byte CornerIndices[8*3]   = { 0,1,2, 4,1,2,
                                         0,5,2, 4,5,2,
                                         0,1,6, 4,1,6,
                                         0,5,6, 4,5,6 };
    // Indices used to convert 8 corners into a 4 sided NGon
    static byte SideIndices[6*4]     = { 0,2,3,1, 1,3,7,5, 5,7,6,4,
                                         4,6,2,0, 2,6,7,3, 4,0,1,5 };

    // Build the corners
    vector3     Corner[8];
    {
        const byte*  pI = CornerIndices;
        const f32*   pBBoxF = &HighBBox.Min.GetX();
        for( s32 i=0; i<8; i++ )
        {
            BBoxMesh.m_pVertex[i].Position.Set( pBBoxF[pI[0]],
                                                pBBoxF[pI[1]],
                                                pBBoxF[pI[2]] );
            BBoxMesh.m_pVertex[i].nColors          = 0;
            BBoxMesh.m_pVertex[i].nNormals         = 0;
            BBoxMesh.m_pVertex[i].nUVs             = 0;
            BBoxMesh.m_pVertex[i].nWeights         = 1;
            BBoxMesh.m_pVertex[i].Weight[0].iBone  = 0;
            BBoxMesh.m_pVertex[i].Weight[0].Weight = 1.0f;
            pI += 3;
        }
    }

    //
    // We need to build 2 triangles for each side
    //
    {
        const byte*  pI = SideIndices;
        for( s32 i=0; i<6; i++ )
        {
            BBoxMesh.m_pFacet[i*2+0].iMaterial = 0;
            BBoxMesh.m_pFacet[i*2+0].iMesh     = 0;
            BBoxMesh.m_pFacet[i*2+0].iVertex[0] = pI[0];
            BBoxMesh.m_pFacet[i*2+0].iVertex[1] = pI[1];
            BBoxMesh.m_pFacet[i*2+0].iVertex[2] = pI[2];
            BBoxMesh.m_pFacet[i*2+0].Plane.Setup(1,0,0,0);

            BBoxMesh.m_pFacet[i*2+1].iMaterial = 0;
            BBoxMesh.m_pFacet[i*2+1].iMesh     = 0;
            BBoxMesh.m_pFacet[i*2+1].iVertex[0] = pI[0];
            BBoxMesh.m_pFacet[i*2+1].iVertex[1] = pI[2];
            BBoxMesh.m_pFacet[i*2+1].iVertex[2] = pI[3];
            BBoxMesh.m_pFacet[i*2+1].Plane.Setup(1,0,0,0);

            pI += 4;
        }
    }

    CompileLowCollision( RigidGeom, BBoxMesh, HighMesh, "CONSTRUCTING BBOX" );
}

//=============================================================================
//=============================================================================
//=============================================================================
// BUILDING HIGH POLY COLLISION GEOMETRY
//=============================================================================
//=============================================================================
//=============================================================================

inline
s32 CompareHighTriangles( const void* pT1, const void* pT2 )
{
    ASSERT( pT1 != NULL );
    ASSERT( pT2 != NULL );

    geom_compiler::high_tri& T1 = *((geom_compiler::high_tri*)pT1);
    geom_compiler::high_tri& T2 = *((geom_compiler::high_tri*)pT2);

    // Compare basic properties
    if( T1.iDList< T2.iDList ) return -1;
    if( T1.iDList> T2.iDList ) return +1;
    if( T1.iBone < T2.iBone ) return -1;
    if( T1.iBone > T2.iBone ) return +1;
    if( T1.iMesh < T2.iMesh ) return -1;
    if( T1.iMesh > T2.iMesh ) return +1;
    if( T1.MatInfo < T2.MatInfo ) return -1;
    if( T1.MatInfo > T2.MatInfo ) return +1;

    // Compare normals of plane
    vector3 N1 = T1.Plane.Normal;
    vector3 N2 = T2.Plane.Normal;
    if( N1.GetX() + 0.001f < N2.GetX() ) return -1;
    if( N1.GetX() - 0.001f > N2.GetX() ) return +1;
    if( N1.GetY() + 0.001f < N2.GetY() ) return -1;
    if( N1.GetY() - 0.001f > N2.GetY() ) return +1;
    if( N1.GetZ() + 0.001f < N2.GetZ() ) return -1;
    if( N1.GetZ() - 0.001f > N2.GetZ() ) return +1;
    
    // Compare actual plane D values
    if( T1.Plane.D + 0.001f < T2.Plane.D ) return -1;
    if( T1.Plane.D - 0.001f > T2.Plane.D ) return +1;

    // Consider these triangles identical
    return 0;
}


//=============================================================================

void geom_compiler::CompileHighCollision( rigid_geom&       RigidGeom, 
                                          high_tri*         pHighTri,
                                          s32               nHighTris,
                                          const char*       pFileName )
{
    s32 i;
    xtimer Timer[5];
    Timer[0].Start();

    static X_FILE* fp = NULL;//x_fopen("c:/temp/highcoll.txt","at");
    if( fp )
    {
        x_fprintf(fp,"FILE: %s\n",pFileName);
        x_fprintf(fp,"nTris %d\n",nHighTris);
    }

    (void)pFileName;

    struct cluster
    {
        bbox        BBox;
        high_tri*   pFirstTri;
        s32         nTris;
        xbool       bFinished;
    };

    // Fill out plane information
    for( i=0; i<nHighTris; i++ )
    {
        pHighTri[i].Plane.Setup( pHighTri[i].P[0], pHighTri[i].P[1], pHighTri[i].P[2] );
        pHighTri[i].BBox.Clear();
        pHighTri[i].BBox.AddVerts( pHighTri[i].P, 3 );
        pHighTri[i].BBox.Inflate(1,1,1);
    }

    // Sort the triangles by similar properties
    qsort( pHighTri, nHighTris, sizeof(high_tri), CompareHighTriangles );

    // Build initial set of clusters
    cluster* pCluster  = (cluster*)x_malloc(sizeof(cluster)*1024);
    s32      nClusters = 0;
    for( i=0; i<1024; i++ )
    {
        pCluster[i].BBox.Clear();
        pCluster[i].nTris = 0;
        pCluster[i].pFirstTri = NULL;
        pCluster[i].bFinished = FALSE;
    }

    // Setup initial values for cluster boundaries
    s32 ClusterBone = -1;
    s32 ClusterMesh = -1;
    s32 ClusterDList = -1;
    u32 ClusterMatInfo = 0xFFFFFFFF;

    // Build initial set of clusters
    for( i=0; i<nHighTris; i++ )
    {
        high_tri& T = pHighTri[i];

        // Do we need to start a new cluster?
        if( (T.iDList   != ClusterDList) ||
            (T.iBone    != ClusterBone) ||
            (T.iMesh    != ClusterMesh) ||
            (T.MatInfo  != ClusterMatInfo ) )
        {
            ASSERT(nClusters < 1024);
            ClusterDList    = T.iDList;
            ClusterBone     = T.iBone;
            ClusterMesh     = T.iMesh;
            ClusterMatInfo  = T.MatInfo;
            nClusters++;
        }

        // Add triangle to cluster
        T.pNext = pCluster[nClusters-1].pFirstTri;
        pCluster[nClusters-1].pFirstTri = &T;
        pCluster[nClusters-1].nTris++;
        pCluster[nClusters-1].BBox.AddVerts( T.P, 3 );
    }

    for( i=0; i<nClusters; i++ )
    {
        ASSERT( pCluster[i].pFirstTri );
        ASSERT( pCluster[i].nTris );
    }

    if( fp )
    {
        for( i=0; i<nClusters; i++ )
        {
            x_fprintf(fp,"%3d] ++ %4d %10.0f\n",i,pCluster[i].nTris,pCluster[i].BBox.GetSurfaceArea());
        }
    }

    //
    // Start loop to break clusters into smaller clusters
    //
    #define MAX_CLUSTERS                (128)
    #define MIN_TRIS_PER_CLUSTER        (32)
    #define NUM_BBOX_DIVISIONS          (5)
    #define MIN_TRIS_PER_SIDE_IN_SPLIT  (8)
    while( 1 )
    {
        // If we've hit an upper limit on clusters then bail
        if( nClusters >= MAX_CLUSTERS )
            break;

Timer[1].Start();
        //
        // Look for the largest cluster
        //
        s32 iBestCluster    = -1;
        f32 BestClusterSize = 0;
        for( i=0; i<nClusters; i++ )
        if( pCluster[i].bFinished == FALSE )
        {
            // Should this cluster be broken or should it be considered finished
            if( pCluster[i].nTris <= MIN_TRIS_PER_CLUSTER )
            {
                pCluster[i].bFinished = TRUE;
                continue;
            }

            // Decide if this cluster is a better break candidate
            f32 SurfaceArea = pCluster[i].BBox.GetSurfaceArea();
            if( SurfaceArea > BestClusterSize )
            {
                iBestCluster = i;
                BestClusterSize = SurfaceArea;
            }
        }
Timer[1].Stop();

        // If we didn't find one then we are finished.
        if( iBestCluster == -1 )
            break;


        // We can break this cluster
        {
Timer[2].Start();
            cluster& CL = pCluster[iBestCluster];

            s32  nBBoxes=0;
            bbox BBoxL[NUM_BBOX_DIVISIONS*3];
            bbox BBoxR[NUM_BBOX_DIVISIONS*3];
            
            // Build new bbox candidates
            for( i=0; i<NUM_BBOX_DIVISIONS; i++ )
            {
                f32 T = (f32)(i+1) / (f32)(NUM_BBOX_DIVISIONS+1);
                f32 XDiv = CL.BBox.Min.GetX() + T*(CL.BBox.Max.GetX() - CL.BBox.Min.GetX());
                f32 YDiv = CL.BBox.Min.GetY() + T*(CL.BBox.Max.GetY() - CL.BBox.Min.GetY());
                f32 ZDiv = CL.BBox.Min.GetZ() + T*(CL.BBox.Max.GetZ() - CL.BBox.Min.GetZ());
                BBoxL[nBBoxes+0] = CL.BBox;     BBoxL[nBBoxes+0].Max.GetX() = XDiv;
                BBoxR[nBBoxes+0] = CL.BBox;     BBoxR[nBBoxes+0].Min.GetX() = XDiv;
                BBoxL[nBBoxes+1] = CL.BBox;     BBoxL[nBBoxes+1].Max.GetY() = YDiv;
                BBoxR[nBBoxes+1] = CL.BBox;     BBoxR[nBBoxes+1].Min.GetY() = YDiv;
                BBoxL[nBBoxes+2] = CL.BBox;     BBoxL[nBBoxes+2].Max.GetZ() = ZDiv;
                BBoxR[nBBoxes+2] = CL.BBox;     BBoxR[nBBoxes+2].Min.GetZ() = ZDiv;
                nBBoxes += 3;
            }

            // Loop through candidates and choose best splitter
            f32 BestSplitterScore = F32_MAX;
            s32 iBestSplitter = -1;
            for( s32 iBB=0; iBB<NUM_BBOX_DIVISIONS*3; iBB++ )
            {
                const bbox& BBL         = BBoxL[iBB];
                const bbox& BBR         = BBoxR[iBB];
                s32         nLeft       = 0;
                s32         nRight      = 0;
                bbox        TightBBoxL;
                bbox        TightBBoxR;

                TightBBoxL.Clear();
                TightBBoxR.Clear();

                // Split triangles into the two camps
                high_tri* pTri = CL.pFirstTri;
                while( pTri )
                {
                    // Should the tri go on the right or left?
                    if( BBL.Intersect(pTri->BBox) )
                    {
                        // LEFT!
                        TightBBoxL += pTri->BBox;
                        nLeft++;
                    }
                    else
                    {
                        // RIGHT!
                        ASSERT( BBR.Intersect(pTri->BBox) );
                        TightBBoxR += pTri->BBox;
                        nRight++;
                    }

                    pTri = pTri->pNext;
                }
                ASSERT( (nLeft+nRight) == CL.nTris );

                // Check if this beats the best score so far
                if( ( nLeft  >= MIN_TRIS_PER_SIDE_IN_SPLIT ) && 
                    ( nRight >= MIN_TRIS_PER_SIDE_IN_SPLIT ) )
                {
                    f32 SurfaceAreaL = TightBBoxL.GetSurfaceArea();
                    f32 SurfaceAreaR = TightBBoxL.GetSurfaceArea();
                    f32 Score = SurfaceAreaL + SurfaceAreaR;

                    if( Score < BestSplitterScore )
                    {
                        BestSplitterScore = Score;
                        iBestSplitter = iBB;
                    }
                }
            }
Timer[2].Stop();

            // If no good splitter was found then mark this cluster as finished
            // we've done the best we can.
            if( iBestSplitter == -1 )
            {
                CL.bFinished = TRUE;
                continue;
            }
Timer[3].Start();

            //
            // Create a new cluster and split the triangles
            //
            {
                // Build new triangle lists
                const bbox& BBL         = BBoxL[iBestSplitter];
                const bbox& BBR         = BBoxR[iBestSplitter];
                high_tri*   pLeft       = NULL;
                high_tri*   pRight      = NULL;
                s32         nLeft       = 0;
                s32         nRight      = 0;
                bbox        TightBBoxL;
                bbox        TightBBoxR;

                TightBBoxL.Clear();
                TightBBoxR.Clear();

                // Split triangles into the two camps
                high_tri* pTri = CL.pFirstTri;
                while( pTri )
                {
                    high_tri* pNext = pTri->pNext;

                    // Should the tri go on the right or left?
                    if( BBL.Intersect(pTri->BBox) )
                    {
                        // LEFT!
                        TightBBoxL += pTri->BBox;
                        pTri->pNext = pLeft;
                        pLeft = pTri;
                        nLeft++;
                    }
                    else
                    {
                        // RIGHT!
                        ASSERT( BBR.Intersect(pTri->BBox) );
                        TightBBoxR += pTri->BBox;
                        pTri->pNext = pRight;
                        pRight = pTri;
                        nRight++;
                    }

                    pTri = pNext;
                }

                // Fill out clusters
                ASSERT( nClusters < 1024 );
                cluster& NCL = pCluster[nClusters];
                nClusters++;

                ASSERT( pLeft && pRight && nLeft && nRight );

                CL.BBox         = TightBBoxL;
                CL.pFirstTri    = pLeft;
                CL.nTris        = nLeft;
                CL.bFinished    = FALSE;
                
                NCL.BBox        = TightBBoxR;
                NCL.pFirstTri   = pRight;
                NCL.nTris       = nRight;
                NCL.bFinished   = FALSE;
            }
Timer[3].Stop();

        }
    }

Timer[4].Start();

    //
    // Sort the clusters by iBone
    //
    {
        for( s32 A=0; A<nClusters; A++ )
        {
            s32 Best = A;
            for( s32 B=A+1; B<nClusters; B++ )
            {
                if( pCluster[B].pFirstTri->iBone < pCluster[Best].pFirstTri->iBone )
                    Best = B;
            }

            if( Best != A )
            {
                cluster TQC        = pCluster[Best];
                pCluster[Best]     = pCluster[A];
                pCluster[A]        = TQC;
            }
        }
    }

    if( fp )
    {
        x_fprintf(fp,"Initial nClusters %d\n",nClusters);
        for( i=0; i<nClusters; i++ )
        {
            x_fprintf(fp,"%3d] ** %4d %10.0f %3d\n",i,pCluster[i].nTris,pCluster[i].BBox.GetSurfaceArea(),pCluster[i].pFirstTri->iBone);
        }
    }

    //
    // Fill out collision geometry
    //
    {
        RigidGeom.m_Collision.nHighClusters = nClusters;
        RigidGeom.m_Collision.nHighIndices  = nHighTris;
        RigidGeom.m_Collision.pHighCluster  = new collision_data::high_cluster[ nClusters ];
        RigidGeom.m_Collision.pHighIndexToVert0 = new u16[ nHighTris ];
    
        s32 iOffset = 0;
        for( i=0; i<nClusters; i++ )
        {
            collision_data::high_cluster& HCL = RigidGeom.m_Collision.pHighCluster[i];
            cluster& CL = pCluster[i];

            HCL.BBox         = CL.BBox;
            HCL.iBone        = CL.pFirstTri->iBone;
            HCL.iMesh        = CL.pFirstTri->iMesh;
            HCL.iOffset      = iOffset;
            HCL.iDList       = CL.pFirstTri->iDList;
            HCL.nTris        = CL.nTris;
            HCL.MaterialInfo = CL.pFirstTri->MatInfo;

            // Loop through triangles and add offsets to list
            high_tri* pTri = CL.pFirstTri;
            while( pTri )
            {
                u16 Index = pTri->I;
                if( pTri->bFlipOrient ) Index |= 0x8000;

                RigidGeom.m_Collision.pHighIndexToVert0[iOffset] = Index;
                iOffset++;

                ASSERT( pTri->iBone == HCL.iBone );
                ASSERT( pTri->iMesh == HCL.iMesh );
                ASSERT( pTri->MatInfo == HCL.MaterialInfo );
                ASSERT( pTri->iDList == HCL.iDList );

                pTri = pTri->pNext;
            }
        }
        ASSERT( iOffset == nHighTris );
    }

    //
    // Fill out main bbox
    //
    {
        RigidGeom.m_Collision.BBox.Clear();
        for( i=0; i<nClusters; i++ )
        {
            RigidGeom.m_Collision.BBox += pCluster[i].BBox;
        }
    }

    // Free allocated memory
    x_free(pCluster);
Timer[4].Stop();

    Timer[0].Stop();
    if( fp )
    {
        x_fprintf(fp,"TIME: %7.3f %7.3f %7.3f %7.3f %7.3f\n",
            Timer[0].ReadSec(),
            Timer[1].ReadSec(),
            Timer[2].ReadSec(),
            Timer[3].ReadSec(),
            Timer[4].ReadSec());
        x_fprintf(fp,"-----------------------------------------------------\n");
    }

}

//=============================================================================

void geom_compiler::CompileHighCollisionPS2( rigid_geom&       RigidGeom, 
                                             u32*              pMatList,
                                             const char*       pFileName )
{
    (void)pFileName;

    s32         nTris = RigidGeom.GetNFaces();
    high_tri*   pTri  = new high_tri[ nTris ];

    //
    // Build the triangles.
    //

    s32 c = 0;
    s32 I = 0;

    // Loop thru meshes.
    for( s32 i = 0; i < RigidGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = RigidGeom.m_pMesh[i];

        // Loop thru all the display lists.
        for( s32 j = 0; j < Mesh.nSubMeshs; j++ )
        {
            s32                      iSubMesh = Mesh.iSubMesh + j;
            geom::submesh&           SubMesh  = RigidGeom.m_pSubMesh[ iSubMesh ];
            rigid_geom::dlist_ps2&   DList    = RigidGeom.m_System.pPS2[ SubMesh.iDList ];

            // Loop thru all the tris.
            for( s32 k = 0; k < DList.nVerts; k++ )
            {
                s32 A = DList.pPosition[k].GetIW();
                if( !(A & (1 << 15)) )
                {
                    pTri[I].I    = k-2;
                    pTri[I].P[0] = *((vector3*)(DList.pPosition + pTri[I].I + 0));
                    pTri[I].P[1] = *((vector3*)(DList.pPosition + pTri[I].I + 1));
                    pTri[I].P[2] = *((vector3*)(DList.pPosition + pTri[I].I + 2));
                    pTri[I].iBone = DList.iBone;
                    pTri[I].iMesh = i;
                    pTri[I].iDList = SubMesh.iDList;
                    pTri[I].MatInfo = pMatList[c];
                    pTri[I].bFlipOrient = (A & (1<<CCWBIT)) ? (FALSE) : (TRUE);
                    I++;
                    if( k < 2 )  x_throw( "Bad ADC." );                        
                }
            }

            c++;
        }
    }

    //
    // Build the collision geometry
    //
    CompileHighCollision( RigidGeom, pTri, nTris, pFileName );

    delete pTri;
}

//=============================================================================

void geom_compiler::CompileHighCollisionXBOX(   rigid_geom&       RigidGeom, 
                                                u32*              pMatList,
                                                const char*       pFileName )
{
    (void)pFileName;

    s32         nTris = RigidGeom.GetNFaces();
    high_tri*   pTri  = new high_tri[ nTris ];

    //
    // Build the triangles.
    //

    s32 c = 0;
    s32 I = 0;

    // Loop thru meshes.
    for( s32 i = 0; i < RigidGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = RigidGeom.m_pMesh[i];

        // Loop thru all the display lists.
        for( s32 j = 0; j < Mesh.nSubMeshs; j++ )
        {
            s32                      iSubMesh = Mesh.iSubMesh + j;
            geom::submesh&            SubMesh = RigidGeom.m_pSubMesh[ iSubMesh ];
            rigid_geom::dlist_xbox&     DList = RigidGeom.m_System.pXbox[ SubMesh.iDList ];

            // Loop thru all the tris.
            for( s32 k = 0; k < DList.nIndices/3; k++ )
            {
                pTri[I].I    = (k * 3);
                pTri[I].P[0] = DList.pVert[DList.pIndices[(k*3)+0]].Pos;
                pTri[I].P[1] = DList.pVert[DList.pIndices[(k*3)+1]].Pos;
                pTri[I].P[2] = DList.pVert[DList.pIndices[(k*3)+2]].Pos;
                pTri[I].iBone = DList.iBone;
                pTri[I].iMesh = i;
                pTri[I].iDList = SubMesh.iDList;
                pTri[I].MatInfo = pMatList[c];
                pTri[I].bFlipOrient = FALSE;
                I++;
            }

            c++;
        }
    }

    //
    // Build the collision geometry
    //
    CompileHighCollision( RigidGeom, pTri, nTris, pFileName );

    delete pTri;
}

//=============================================================================

void geom_compiler::CompileHighCollisionPC( rigid_geom&       RigidGeom, 
                                          u32*              pMatList,
                                          const char*       pFileName )
{
    (void)pFileName;

    s32         nTris = RigidGeom.GetNFaces();
    high_tri*   pTri  = new high_tri[ nTris ];

    //
    // Build the triangles.
    //

    s32 c = 0;
    s32 I = 0;

    // Loop thru meshes.
    for( s32 i = 0; i < RigidGeom.m_nMeshes; i++ )
    {
        geom::mesh& Mesh = RigidGeom.m_pMesh[i];

        // Loop thru all the display lists.
        for( s32 j = 0; j < Mesh.nSubMeshs; j++ )
        {
            s32                      iSubMesh = Mesh.iSubMesh + j;
            geom::submesh&           SubMesh  = RigidGeom.m_pSubMesh[ iSubMesh ];
            rigid_geom::dlist_pc&    DList    = RigidGeom.m_System.pPC[ SubMesh.iDList ];

            // Loop thru all the tris.
            for( s32 k = 0; k < DList.nIndices/3; k++ )
            {
                pTri[I].I    = (k * 3);
                pTri[I].P[0] = DList.pVert[DList.pIndices[(k*3)+0]].Pos;
                pTri[I].P[1] = DList.pVert[DList.pIndices[(k*3)+1]].Pos;
                pTri[I].P[2] = DList.pVert[DList.pIndices[(k*3)+2]].Pos;
                pTri[I].iBone = DList.iBone;
                pTri[I].iMesh = i;
                pTri[I].iDList = SubMesh.iDList;
                pTri[I].MatInfo = pMatList[c];
                pTri[I].bFlipOrient = FALSE;
                I++;
            }

            c++;
        }
    }

    //
    // Build the collision geometry
    //
    CompileHighCollision( RigidGeom, pTri, nTris, pFileName );

    delete pTri;
}

//=============================================================================

xbool RigidGeom_GetTriangle( const rigid_geom*          pRigidGeom,
                             s32                   Key,
                             vector3&              P0,
                             vector3&              P1,
                             vector3&              P2)
{
    // This is here to get the RigidGeom to shutup about needing this function!
    return FALSE;
}

//=============================================================================

