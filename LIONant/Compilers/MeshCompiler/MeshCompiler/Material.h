#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <fbxsdk.h>

struct Texture
{
    enum Usage {
        Unknown = 0,
        None = 1,
        Diffuse = 2, 
        Emissive = 3,
        Ambient = 4,
        Specular = 5,
        Shininess = 6,
        Normal = 7,
        Bump = 8,
        Transparency = 9,
        Reflection = 10
    };

    FbxFileTexture *source;
    std::string id;
    std::string path;
    float uvTranslation[2];
    float uvScale[2];
    // FIXME add Matrix3<float> uvTransform;
    Usage usage;

    Texture() : usage(Unknown), source(0)
    {
        uvTranslation[0] = uvTranslation[1] = 0.f;
        uvScale[0] = uvScale[1] = 1.f;
    }
};

struct Material
{
    FbxSurfaceMaterial *source;
    std::string id;
    float diffuse[3];
    float ambient[3];
    float emissive[3];
    float specular[3];
    float shininess;
    float opacity;
    std::vector<Texture *> textures;

    Material() : source(0) {
        memset(diffuse,  0, sizeof(diffuse));
        memset(ambient,  0, sizeof(ambient));
        memset(emissive, 0, sizeof(emissive));
        memset(specular, 0, sizeof(specular));
        shininess = 0.f;
        opacity = 1.f;
    }

    Material(const Material &copyFrom) {
        id = copyFrom.id;
        memcpy(diffuse,  copyFrom.diffuse,  sizeof(diffuse));
        memcpy(ambient,  copyFrom.ambient,  sizeof(diffuse));
        memcpy(emissive, copyFrom.emissive, sizeof(diffuse));
        memcpy(specular, copyFrom.specular, sizeof(diffuse));
        shininess = copyFrom.shininess;
        opacity = copyFrom.opacity;
        source = copyFrom.source;
    }

    ~Material() {
        for (std::vector<Texture *>::iterator itr = textures.begin(); itr != textures.end(); ++itr)
            delete (*itr);
    }

    Texture *getTexture(const char *id) const {
        for (std::vector<Texture *>::const_iterator itr = textures.begin(); itr != textures.end(); ++itr)
            if ((*itr)->id.compare(id)==0)
                return (*itr);
        return NULL;
    }

    int getTextureIndex(const Texture * const &texture) const {
        int n = (int)textures.size();
        for (int i = 0; i < n; i++)
            if (textures[i] == texture)
                return i;
        return -1;
    }
};

struct TextureFileInfo
{
    std::string path;
    // The uv bounds of this texture that are actually used (x1, y1, x2, y2)
    float bounds[4];
    // The number of nodes that use this texture
    unsigned int nodeCount;
    // The material textures that reference this texture
    std::vector<Texture *> textures;
    TextureFileInfo() : nodeCount(0) {
        memset(bounds, -1, sizeof(float) * 4);
    }
};
