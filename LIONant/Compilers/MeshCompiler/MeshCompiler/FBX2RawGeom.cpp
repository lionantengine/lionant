#include <exception>
#include "Fbx2RawGeom.h"

// Make sure to Link With the right library
#ifdef TARGET_PC
    #ifdef X_TARGET_64BIT
        #define ARCHITEXTURE "x64/" 
    #else
        #define ARCHITEXTURE "x86/" 
    #endif

    #ifdef X_DEBUG
        #define ISDEBUG ARCHITEXTURE ## "debug/libfbxsdk-mt" 
    #else
        #define ISDEBUG ARCHITEXTURE ## "release/libfbxsdk-mt"
    #endif

    #pragma comment(lib, ISDEBUG )
#endif

//
// this libraray was base on : github.com/libgdx/fbx-conv
//

//-----------------------------------------------------------------------------------

fbx_to_rawmesh::fbx_to_rawmesh(void)
    : m_pManager( NULL )
{
}

//-----------------------------------------------------------------------------------

fbx_to_rawmesh::~fbx_to_rawmesh(void)
{
    if ( NULL != m_pScene )
    {
        m_pScene->Destroy();
    }

    if ( NULL != m_pManager )
    {
        //for (std::vector<FbxMeshInfo *>::iterator itr = m_Meshes.begin(); itr != m_Meshes.end(); ++itr)
        //    delete (*itr);
        m_pManager->Destroy();
    }
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::Initialize( void )
{
    m_pManager = FbxManager::Create();
    m_pManager->SetIOSettings( FbxIOSettings::Create( m_pManager, IOSROOT ));
    m_pManager->GetIOSettings()->SetBoolProp( IMP_FBX_GLOBAL_SETTINGS, TRUE );
}

//-----------------------------------------------------------------------------------

xbool fbx_to_rawmesh::Load( const char* FBXFilePath )
{
    // Load FBX scene
    FbxImporter* const& pImporter = FbxImporter::Create( m_pManager, "" );

    pImporter->ParseForGlobalSettings   ( TRUE );
    pImporter->ParseForStatistics       ( TRUE );

    FbxAxisSystem   AxisSystem;
    FbxSystemUnit   SystemUnits;
    if( pImporter->Initialize( FBXFilePath, -1, m_pManager->GetIOSettings()) )
    {
        if( !pImporter->IsFBX() )
        {
            return FALSE;
        }

        pImporter->GetAxisInfo( &AxisSystem, &SystemUnits );
        m_pScene = FbxScene::Create( pImporter, "__FBX_SCENE__" );
        pImporter->Import( m_pScene );

        FbxAxisSystem::OpenGL.ConvertScene( m_pScene );
    }
    else
    {
        x_throw("ERROR: FXB Importer, Unable to open [%s]", (const char*)FBXFilePath );
        return FALSE;
    }

    pImporter->Destroy();
    
    //
    // OK Lets build internal structures
    //
    if( m_pScene )
        Convert();
    
    //
    // Done
    //
    return NULL != m_pScene;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::Convert2RawGeom( rawgeom& RawGeom, const char* pFileName )
{
    m_bArticulated      = FALSE;
    m_bReadAnimation    = FALSE;
    m_bReadMesh         = TRUE;

    Initialize( );
    Load( pFileName );
    BuildRawGeom( RawGeom );

    // No more bones for you!
    RawGeom.m_Bone.Destroy();

    // Reset all the weights
    for ( rawgeom::vertex& Vertex : RawGeom.m_Vertex )
    {
        Vertex.m_nWeights = 0;
        Vertex.m_Weight[ 0 ].m_iBone  = 0;
        Vertex.m_Weight[ 0 ].m_Weight = 1;
    }
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::Convert2RawGeom( rawgeom& RawGeom, rawanim& RawAnim, const char* pFileName )
{
    m_bArticulated      = TRUE;
    m_bReadAnimation    = TRUE;
    m_bReadMesh         = TRUE;

    Initialize( );
    Load( pFileName );
    BuildRawGeom( RawGeom );
    BuildRawAnim( RawAnim );
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::Convert2RawGeom( rawanim& RawAnim, const char* pFileName )
{
    m_bArticulated      = TRUE;
    m_bReadAnimation    = TRUE;
    m_bReadMesh         = FALSE;

    Initialize( );
    Load( pFileName );

    BuildRawAnim( RawAnim );
}


//-----------------------------------------------------------------------------------

char* fbx_to_rawmesh::AllocString( const char* String )
{
    s32 Size = x_strlen(String) + 1;
    char* Result = x_new(char, Size, 0);
    x_strcpy(Result, Size, String);
    return Result;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::Convert( void )
{
    if( NULL != m_pScene )
    {
        FbxNode* pRootNode = m_pScene->GetRootNode();
        
        if( pRootNode )
        {
            xbool IsBone = FALSE;
            ReadNode( pRootNode, IsBone );
            
            if( m_bReadAnimation )
                ReadAnimation();
            
            if ( m_bReadMesh )
                ReadMesh( pRootNode );
        }
    }
}

//-----------------------------------------------------------------------------------

fbx_to_rawmesh::node_info* fbx_to_rawmesh::ReadNode( FbxNode* pNode, xbool& IsBone )
{
    //
    // Make sure that we are not dealing with a higher degree mesh
    //
    if( FbxNodeAttribute* NodeAttribute = pNode->GetNodeAttribute() )
    {
        FbxNodeAttribute::EType AttrType = NodeAttribute->GetAttributeType();
        
        switch( AttrType )
        {
            case FbxNodeAttribute::eNurbs:
            case FbxNodeAttribute::ePatch:
            case FbxNodeAttribute::eNurbsCurve:
            case FbxNodeAttribute::eNurbsSurface:
            {
                FbxGeometryConverter Converter( m_pManager );
                if( !Converter.Triangulate( NodeAttribute, TRUE, FALSE ) )
                {
                    x_printf("Unable to triangulate FBX NURBS %s\n", pNode->GetName());
                }
            }
            break;
                
            default:
                break;
        }
    }

    //
    // Does it have bones?
    //
    IsBone                  = FALSE;

    FbxNodeAttribute::EType AttributeType = FbxNodeAttribute::eUnknown;
    if( pNode->GetNodeAttribute() )
    {
        AttributeType = pNode->GetNodeAttribute()->GetAttributeType();
        if( AttributeType == FbxNodeAttribute::eSkeleton )
        {
            IsBone = TRUE;
        }
    }

    if( !IsBone && FindNodeIndex( pNode ) != -1 )
    {
        IsBone = TRUE;
    }

    //
    // Collect Materials
    //
    s32 MaterialCount = pNode->GetMaterialCount();
    if( m_bReadMesh ) for( s32 i = 0; i < MaterialCount; i++ )
    {
        FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);
        if ( NULL != pMaterial )
        {
            AddMaterial( pMaterial );
        }
    }

    //
    // Collect a skeleton if it has one
    //
    xbool               CreateSkeleton  = FALSE;
    xarray<node_info*>  Skeletons;
    xarray<node_info*>  Children;
    node_info*          pNodeInfo = AddNode( pNode );
    
    if( NULL != pNodeInfo )
    {
        s32 ChildrenCount = pNode->GetChildCount();
        for( s32 i = 0; i < ChildrenCount; i++ )
        {
            FbxNode* pChildNode = pNode->GetChild(i);

            if( pChildNode->GetParent() != pNode )
            {
                continue;
            }

            xbool       ChildIsBone = FALSE;
            node_info*  pNodeInfo   = ReadNode( pChildNode, ChildIsBone );
            
            if( NULL != pNodeInfo )
            {
                if( ChildIsBone )
                {
                    if ( !IsBone )
                    {
                        CreateSkeleton  = TRUE;
                    }
                    Skeletons.append() = pNodeInfo;
                }
                else
                {
                    Children.append() = pNodeInfo;
                }
            }
            else
            {
                return pNodeInfo;
            }
        }

        for( s32 i = 0; i < Skeletons.getCount(); i++ )
        {
            pNodeInfo->m_Children.append()  = Skeletons[i];
            Skeletons[i]->m_pParent         = pNodeInfo;
        }

        for( s32 i = 0; i < Children.getCount(); i++ )
        {
            pNodeInfo->m_Children.append()  = Children[i];
            Children[i]->m_pParent          = pNodeInfo;
        }
    }
    
    return pNodeInfo;
}

//-----------------------------------------------------------------------------------

s32 fbx_to_rawmesh::AddMaterial( FbxSurfaceMaterial* pMaterial )
{
    // This will get the name of the material
    // pMaterial->GetName();

    //
    // First lets see if it is a hardware shader
    //
    FbxString ShaderFileName;
    FbxString lTechniqueName;
    {
        // parameter block index of the effect file
        const FbxImplementation*    pImplementation = GetImplementation( pMaterial, FBXSDK_IMPLEMENTATION_HLSL );
        FbxString                   Type            = "HLSL";

        if ( !pImplementation )
        {
            pImplementation = GetImplementation( pMaterial, FBXSDK_IMPLEMENTATION_CGFX );
            Type            = "CGFX";
        }

        if ( pImplementation )
        {
            //Now we have a hardware shader, let's read it
            const FbxBindingTable* lRootTable = pImplementation->GetRootTable();
            
            ShaderFileName = lRootTable->DescAbsoluteURL.Get( );
            lTechniqueName = lRootTable->DescTAG.Get();
        }
        else
        {
            return -1;
        }
    }

    //
    // Find the material if we can not see it create a new one
    //
    s32 Index;
    if ( TRUE == m_lMaterialKey.BinarySearch( material_temp{pMaterial,0}, Index ) )
        return m_lMaterialKey[Index].m_UID;

    // Create a new key (Note that we must keep it deterministic that is why we use the UID)
    material_temp& MaterialTemp = m_lMaterialKey.Insert( Index );
    MaterialTemp.m_pFBXMaterial = pMaterial;
    MaterialTemp.m_UID          = m_lMaterialKey.getCount()-1;

    //
    // Ok create a raw material and start filling it out
    //
    rawgeom::informed_material& RawMaterial = m_lInformedMaterial.append();

    // Set the shader
    RawMaterial.m_MaterialShader.Copy( (const char*)ShaderFileName );
    RawMaterial.m_Technique.Copy( (const char*)lTechniqueName );
    RawMaterial.m_Name.Copy( MaterialTemp.m_pFBXMaterial->GetName() );

    //
    // Loop first thought the properties and find the one we are interested on
    //
    FbxProperty Prop = pMaterial->GetFirstProperty();
    while ( Prop.IsValid( ) )
    {
        static const FbxString  HwShaderProgram( "3dsMax|HwShaderParams" );
        static const size_t     HwLength = HwShaderProgram.GetLen( ) - 1;
        const FbxString         Name        = Prop.GetName();
        const FbxString         HName       = Prop.GetHierarchicalName( );
        const FbxDataType       Type        = Prop.GetPropertyDataType();
        const FbxString         TypeName    = Type.GetName( );

        // Make sure that it is part of the properties of the hardware shader
        s32 i;
        for ( i = 0; HName[ i ] == HwShaderProgram[ i ] && i < (s32)HwLength; i++ );
        
        // If it is then get the property
        FbxString Val;
        if ( i == HwLength )
        {
            if ( Type == FbxReferenceDT )
            {
                const FbxObject*        pRef            = Prop.GetSrcObject();
                const FbxFileTexture*   pFileTexture    = FbxCast<FbxFileTexture>( pRef );
                const char*             pFileName       = pFileTexture->GetFileName();

                Val = pFileName;
            }
            else
            {
                Val = Prop.Get<FbxString>();
            }

            // Should we add this parameter?
            if ( x_strlen( Val ) > 0 )
            {
                struct table
                {
                    FbxDataType                                 m_FBXType;
                    rawgeom::informed_material::params_type     m_RawGeomType;
                };

                static const table s_Types[ ] =
                {
                    { FbxUndefinedDT,   rawgeom::informed_material::PARAM_TYPE_NULL  },
                    { FbxBoolDT,        rawgeom::informed_material::PARAM_TYPE_BOOL  },
                    { FbxDoubleDT,      rawgeom::informed_material::PARAM_TYPE_F1    },
                    { FbxDouble2DT,     rawgeom::informed_material::PARAM_TYPE_F2    },
                    { FbxDouble3DT,     rawgeom::informed_material::PARAM_TYPE_F3    },
                    { FbxColor3DT,      rawgeom::informed_material::PARAM_TYPE_F3    }, 
                    { FbxDouble4DT,     rawgeom::informed_material::PARAM_TYPE_F4    },
                    { FbxColor4DT,      rawgeom::informed_material::PARAM_TYPE_F4    },
                    { FbxReferenceDT,   rawgeom::informed_material::PARAM_TYPE_TEXTURE   }
                };

                rawgeom::informed_material::params_type ParamType = rawgeom::informed_material::PARAM_TYPE_NULL;
                for ( s32 i = 0; i < sizeof( s_Types ) / sizeof(table); i++ )
                {
                    if ( s_Types[ i ].m_FBXType == Type )
                    {
                        ParamType = s_Types[ i ].m_RawGeomType;
                        break;
                    }
                }
                
                ASSERT( ParamType != rawgeom::informed_material::PARAM_TYPE_NULL );

                if ( ParamType == rawgeom::informed_material::PARAM_TYPE_BOOL )
                {
                    if ( Val == "false" ) Val = "0";
                    else Val = "1";
                }

                rawgeom::informed_material::params& Params = RawMaterial.m_Params.append( );
                x_strcpy( Params.m_Name, 128, Name );
                x_strcpy( Params.m_Value, 128, Val );
                Params.m_Type = ParamType;
            }
        }

        // Get next property
        Prop = pMaterial->GetNextProperty( Prop );
    }



    return Index;
}

//-----------------------------------------------------------------------------------

xbool fbx_to_rawmesh::ReadAnimation( void )
{
    xbool Result = FALSE;

    for( s32 i = 0; i < m_pScene->GetSrcObjectCount<FbxAnimStack>(); ++i )
    {
        FbxAnimStack*   pAnimStack      = FbxCast<FbxAnimStack>( m_pScene->GetSrcObject<FbxAnimStack>(i) );
        s32             AnimLayersCount = pAnimStack->GetMemberCount<FbxAnimLayer>();
        const char*     pTakeName       = pAnimStack->GetName();

        if( !pTakeName || !*pTakeName )
            continue;

        const FbxTimeSpan AnimTimeSpan    = pAnimStack->GetLocalTimeSpan();
        const f32         AnimStart       = f32( AnimTimeSpan.GetStart().GetMilliSeconds() );
        const f32         AnimStop        = f32( AnimTimeSpan.GetStop().GetMilliSeconds() );

     //   const f32         AnimStop        = AnimStart >= TempAnimStop?999999999.0f:TempAnimStop;
        ASSERT( AnimStart <= AnimStop );

   //     ASSERT( 4000 > (AnimStop - AnimStart) );

        s32         Count = 0;
        for( s32 j = 0; j < AnimLayersCount; j++ )
        {
            FbxAnimLayer*   pAnimLayer      = pAnimStack->GetMember<FbxAnimLayer>(j);
            s32             CurveNodeCount  = pAnimLayer->GetSrcObjectCount<FbxAnimCurveNode>();

            for( s32 k = 0; k < CurveNodeCount; k++ )
            {
                FbxAnimCurveNode*   pCurveNode      = pAnimLayer->GetSrcObject<FbxAnimCurveNode>(k);
                s32                 PropertyCount   = pCurveNode->GetDstPropertyCount();

                for( s32 l = 0; l < PropertyCount; l++ )
                {
                    FbxProperty Property    = pCurveNode->GetDstProperty(l);
                    FbxNode*    pNode       = static_cast<FbxNode *>(Property.GetFbxObject());
                    
                    if( NULL != pNode )
                    {
                        NodeIndexMap::iterator It = m_NodeIndexMap.find( pNode );
                        if ( It == m_NodeIndexMap.end() )
                        {
                            continue;
                        }

                        s32         NodeIndex   = (*It).second;
                        node_info*  NodeInfo    = m_Nodes[ NodeIndex ];
                        FbxString   propName    = Property.GetName();

                        // Only add translation, scaling or rotation
                        if ((!pNode->LclTranslation.IsValid() || propName != pNode->LclTranslation.GetName()) &&
                            (!pNode->LclScaling.IsValid()     || propName != pNode->LclScaling.GetName()    ) &&
                            (!pNode->LclRotation.IsValid()    || propName != pNode->LclRotation.GetName())  )
                            continue;

                        NodeInfo->m_HasAnimation = TRUE;
                        
                        FbxAnimCurve*   pCurve = NULL;
                        track_info      TrackInfo;

                        //TrackInfo.m_bHasTranslation  = propName == pNode->LclTranslation.GetName();
                        //TrackInfo.m_bHasRotation     = propName == pNode->LclRotation.GetName();
                        //TrackInfo.m_bHasScaling      = propName == pNode->LclScaling.GetName();

                        TrackInfo.m_bHasTranslation = pNode->LclTranslation.IsValid();
                        TrackInfo.m_bHasRotation = pNode->LclScaling.IsValid();
                        TrackInfo.m_bHasScaling = pNode->LclRotation.IsValid();

                        if( (pCurve = Property.GetCurve( pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X )) )
                            UpdateAnimTime( pCurve, TrackInfo, AnimStart, AnimStop );
                        
                        if( (pCurve = Property.GetCurve( pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y )) )
                            UpdateAnimTime( pCurve, TrackInfo, AnimStart, AnimStop );
                        
                        if( (pCurve = Property.GetCurve( pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z )) )
                            UpdateAnimTime( pCurve, TrackInfo, AnimStart, AnimStop );

                        NodeInfo->m_TrackInfo.Merge(TrackInfo);
                        Count++;
                    }
                }
            }
        }

        if( 0 == Count )
        {
            continue;
        }

        /*
        if( FALSE == m_Animation.IsValid() )
        {
            m_Animation.New();
        }
        */

        f32 StartTime = FLT_MAX;

        for( s32 j = 0; j < m_Nodes.getCount(); j++ )
        {
            if( m_Nodes[j]->m_HasAnimation )
            {
                track_info& Info = m_Nodes[j]->m_TrackInfo;

                if( Info.m_Stop > Info.m_Start )
                {
                    if( Info.m_Start < StartTime )
                    {
                        StartTime = Info.m_Start;
                    }
                }
            }
        }

        if( StartTime == FLT_MAX ) 
        {
            StartTime =  f32( AnimTimeSpan.GetStart().GetMilliSeconds() ); 
        }
        m_AnimationStartTime = StartTime;

        pAnimStack->GetScene()->SetCurrentAnimationStack( pAnimStack );
        FbxTime TempTime;
 
        
        for( s32 j = 0; j < m_Nodes.getCount(); j++ )
        {
            auto GeometryTransform = GetGeometryTransform( m_Nodes[ j ]->m_pNode );
            
            if ( m_Nodes[j]->m_HasAnimation )
            {
                track_info& Info    = m_Nodes[j]->m_TrackInfo;
                Info.m_FrameRate    = 60.0f;
                f32 StepTime        = Info.m_FrameRate <= 0.0f ? Info.m_Stop - Info.m_Start : 1000.0f / Info.m_FrameRate;
                f32 LastTime        = Info.m_Stop + StepTime * 0.5f;

                s32 KeyFramesCount  = (s32)((LastTime - Info.m_Start) / StepTime);
                if( KeyFramesCount <= 0 )
                {
                    m_Nodes[j]->m_HasAnimation = FALSE;
                }
                
                for( s32 k = 0; k < KeyFramesCount; k++ )
                {
                    f32 CurrentTime = x_Min( Info.m_Stop, (Info.m_Start + k * StepTime) );
                    TempTime.SetMilliSeconds( (FbxLongLong)CurrentTime );

                    rawanim::key_frame& KeyFrame = Info.m_KeyFrames.append();


                    FbxAMatrix Matrix = m_Nodes[j]->m_pNode->EvaluateLocalTransform(TempTime);

                    FbxVector4 Translation = Matrix.GetT();
                    KeyFrame.m_Translation.m_X = (f32)Translation.mData[0];
                    KeyFrame.m_Translation.m_Y = (f32)Translation.mData[1];
                    KeyFrame.m_Translation.m_Z = (f32)Translation.mData[2];

                    FbxQuaternion Rotation = Matrix.GetQ();
                    KeyFrame.m_Rotation.m_X = (f32)Rotation.mData[0];
                    KeyFrame.m_Rotation.m_Y = (f32)Rotation.mData[1];
                    KeyFrame.m_Rotation.m_Z = (f32)Rotation.mData[2];
                    KeyFrame.m_Rotation.m_W = (f32)Rotation.mData[3];

                    FbxVector4 Scaling = Matrix.GetS();
                    KeyFrame.m_Scale.m_X = (f32)Scaling.mData[0];
                    KeyFrame.m_Scale.m_Y = (f32)Scaling.mData[1];
                    KeyFrame.m_Scale.m_Z = (f32)Scaling.mData[2];
                }
            }
        }
    }
    
    return Result;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::UpdateAnimTime( FbxAnimCurve* pCurve, track_info& TrackInfo, f32 AnimStart, f32 AnimStop )
{
    FbxTimeSpan fts;
    
    pCurve->GetTimeInterval(fts);
    const FbxTime Start = fts.GetStart();
    const FbxTime Stop  = fts.GetStop();
    
    if( Start <= Stop )
    {
        TrackInfo.m_Start = x_Max( AnimStart, x_Min( TrackInfo.m_Start, (f32)Start.GetMilliSeconds() ));
        TrackInfo.m_Stop  = x_Min( AnimStop,  x_Max( TrackInfo.m_Stop,  (f32)Stop.GetMilliSeconds()  ));
        
        // Could check the number and type of keys (ie curve->KeyGetInterpolation) to lower the framerate
        TrackInfo.m_FrameRate = x_Max( TrackInfo.m_FrameRate, (float)Stop.GetFrameRate( FbxTime::eFrames60 ));

        //ASSERT( TrackInfo.m_Stop < 4000 );
       // ASSERT( 1000 > ((TrackInfo.m_Stop - TrackInfo.m_Start) / TrackInfo.m_FrameRate) );
    }
}

//-----------------------------------------------------------------------------------
struct new_vert
{
    u32     m_CRC       = 0;
    s32     m_iNext     = -1;
};

void fbx_to_rawmesh::ReadMesh( FbxNode* pNode )
{
    FbxGeometry* pGeometry = FbxCast<FbxGeometry>( pNode->GetNodeAttribute() );
    
//    pNode->GetMaterialCount();
//    pNode->GetMaterial();
    

    if( NULL != pGeometry )
    {
        //
        // Make sure we have a mesh made of triangles
        //
        FbxMesh* pMesh = NULL;

        if ( pGeometry->Is<FbxMesh>( ) && ( (FbxMesh*)pGeometry )->IsTriangleMesh( ) )
        {
            pMesh = (FbxMesh*)pGeometry;
        }
        else
        {
            FbxGeometryConverter        Converter( m_pManager );
            FbxNodeAttribute * const    pAttr = Converter.Triangulate( pGeometry, FALSE );
            
            if( pAttr->Is<FbxMesh>() )
            {
                pMesh = (FbxMesh*)pAttr;
            }
        }

        //
        // Collect Mesh Information
        //
        FbxAMatrix M;
        M.SetIdentity();
        if( NULL != pMesh )
        {
            const s32           DeformerCount       = pMesh->GetDeformerCount( FbxDeformer::eSkin );
            const s32           ControlPointCount   = pMesh->GetControlPointsCount( );
            const FbxAMatrix    MaxGlobalTransform  = GetGeometryTransform( pNode ) * pNode->EvaluateGlobalTransform();
            const xmatrix4      FinalMatrix         = ConvertMatrix( MaxGlobalTransform );
            const xmatrix4      NormalMatrix        = FinalMatrix.getAdjoint( );
            s32                 iCommonMaterial     = -1;
//            auto                MeshName            = pMesh->GetName();

            //
            //check whether the material maps with only one mesh
            //
            bool MaterialIndexIsAllSame = true;
            for (s32 l = 0; l < pMesh->GetElementMaterialCount(); l++)
            {

                FbxGeometryElementMaterial* lMaterialElement = pMesh->GetElementMaterial(l);
                if( lMaterialElement->GetMappingMode() == FbxGeometryElement::eByPolygon) 
                {
                    MaterialIndexIsAllSame = false;
                    break;
                }
            }

            if(MaterialIndexIsAllSame && m_bReadMesh )
            {
                const s32 ElementMaterialCount = pMesh->GetElementMaterialCount();
                for (s32 l = 0; l < ElementMaterialCount; l++)
                {
                    FbxGeometryElementMaterial* lMaterialElement = pMesh->GetElementMaterial( l );
                    if( lMaterialElement->GetMappingMode() == FbxGeometryElement::eAllSame) 
                    {
                        FbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(lMaterialElement->GetIndexArray().GetAt(0));    
                        int lMatId = lMaterialElement->GetIndexArray().GetAt(0);
                        if(lMatId >= 0)
                        {
                            // DisplayInt("        All polygons share the same material in mesh ", l);
                            // DisplayMaterialTextureConnections(lMaterial, header, lMatId, l);
                            //iCommonMaterial = lMatId;
                            xbool bFound = m_lMaterialKey.BinarySearch( material_temp{ lMaterial,0}, iCommonMaterial );
                            if( bFound == FALSE )
                                x_throw( "ERROR: I founed a mesh using a not shader based material [%s], Please remove it from the FBX", 
                                     (const char*) lMaterial->GetName() );
                            ASSERT(bFound);

                            iCommonMaterial = m_lMaterialKey[iCommonMaterial].m_UID;
                        }
                    }
                }

                //no material
                if ( pMesh->GetElementMaterialCount() == 0 )
                {
                    x_printf( "WARNING: Found mesh [%s] with not material we will skip this mesh.", (const char*)pMesh->GetName() );
                    return;
                }

                ASSERT( iCommonMaterial != -1 );
            }

            //
            // Collect all the positions
            //
            xarray<rawgeom::vertex>    ControlPoints;
            ControlPoints.appendList( ControlPointCount );
            {
                for ( s32 i = 0; i < ControlPointCount; i++ )
                {
                    const FbxVector4 ControlPoint = pMesh->GetControlPointAt( i );
                    const xvector3   Temp(
                        static_cast<f32>( ControlPoint.mData[ 0 ] ),
                        static_cast<f32>( ControlPoint.mData[ 1 ] ),
                        static_cast<f32>( ControlPoint.mData[ 2 ] ) );

                    ControlPoints[ i ].m_Position = FinalMatrix * Temp;
                }
            }

            //
            // Deal with mesh weights
            //
            if ( m_bArticulated && DeformerCount > 0 )
            {
                FbxSkin* pSkin = static_cast<FbxSkin*>( pMesh->GetDeformer(0, FbxDeformer::eSkin) );
                
                const s32 ClusterCount = pSkin->GetClusterCount();
                
                // Fetch the blend weights per control point
                for (s32 j = 0; j < ClusterCount; j++)
                {
                    const FbxCluster*   pCluster        = pSkin->GetCluster(j);
                    const s32           IndexCount      = pCluster->GetControlPointIndicesCount();
                    const s32*          pClusterIndices = pCluster->GetControlPointIndices();
                    const double*       pClusterWeights = pCluster->GetControlPointWeights();

                    // Compute bind-pose
                    {
                        s32                     JointIndex  = -1;
                        NodeIndexMap::iterator  It          = m_NodeIndexMap.find( (FbxNode*)pCluster->GetLink() );
                        
                        if( It != m_NodeIndexMap.end() )
                        {
                            JointIndex = (*It).second;
                        }

                        if ( -1 != JointIndex )
                        {
                            FbxAMatrix GlobalBindMatrixOfMesh;
                            FbxAMatrix GlobalBindMatrixOfBone;

                            if( pCluster->GetLinkMode() == FbxCluster::eAdditive && pCluster->GetAssociateModel() )
                            {
                                pCluster->GetTransformAssociateModelMatrix( GlobalBindMatrixOfMesh );
                            }
                            else
                            {
                                pCluster->GetTransformMatrix    ( GlobalBindMatrixOfMesh );
                            }

                            pCluster->GetTransformLinkMatrix( GlobalBindMatrixOfBone );
                            
                            //auto MeshGeometry = GetGeometryTransform( pMesh->GetNode() ); 
                            const FbxAMatrix globalBindposeMatrix = GlobalBindMatrixOfBone * GlobalBindMatrixOfMesh * MaxGlobalTransform.Inverse();

                            node_info*  pNodeInfo   = FindNode      ( pCluster->GetLink() );
                            s32         Index       = FindNodeIndex ( pCluster->GetLink() );
                            
                            ASSERT( Index >= 0 );

                            if( NULL != pNodeInfo )
                            {
                                ConvertMatrix( globalBindposeMatrix, pNodeInfo->m_BindPose );
                            }
                        }
                    }

                    if( m_bReadMesh == FALSE )
                        continue;


                    for (s32 k = 0; k < IndexCount; k++)
                    {
                        const s32           Index       = pClusterIndices[k];
                        rawgeom::vertex&    Vertex      = ControlPoints[ Index ];

                        if( pClusterIndices[k]    < 0
                            || pClusterIndices[k] >= (int)ControlPointCount
                            || pClusterWeights[k] == 0.0 )
                            continue;
                        
                        if ( Vertex.m_nWeights >= rawgeom::VERTEX_MAX_WEIGHT )
                        {
                            x_printf( "Too many bones bound to vertex:%d\n", pClusterIndices[k] );
                        }
                        else
                        {
                            rawgeom::weight& Weight = Vertex.m_Weight[ Vertex.m_nWeights ]; 
                            Weight.m_Weight = (f32)pClusterWeights[ k ];
                            Weight.m_iBone  = FindNodeIndex( pCluster->GetLink( ) );
                            ASSERT( Weight.m_iBone >= 0 );
                            ASSERT( Weight.m_iBone < 1000 );

                            Vertex.m_nWeights++;
                        }
                    }
                }
            }
            else
            {
                {
                    s32         Index = FindNodeIndex( pNode );
                    for ( s32 i = 0; i < ControlPointCount; i++ )
                    {
                        ControlPoints[ i ].m_nWeights = 1;
                        ControlPoints[ i ].m_Weight[ 0 ].m_iBone = Index;
                        ControlPoints[ i ].m_Weight[ 0 ].m_Weight = 1;
                    }
                }
            }

            // Record mesh information
            mesh_info& Info             = m_Meshes.append();
            m_MeshMap[ pNode ]          = m_Meshes.getCount() - 1;

            s32 TriangleCount           = pMesh->GetPolygonCount();
            s32 IndexCount              = TriangleCount * 3;
            s32 StartIndex              = m_Vertices.getCount( );
            
            Info.m_Polygons.New( IndexCount );
            Info.m_iMaterial.New( IndexCount/3 );
            Info.m_VertexStartOffset    = StartIndex;
            Info.m_VertexCount          = ControlPointCount; // pMesh->GetPolygonVertexCount();
            Info.m_pMesh                = pMesh;
            Info.m_pNode                = pNode;
            Info.m_PolygonCount         = TriangleCount;

            //
            // HACK: to determine how many vertices this mesh really uses
            // This section has graduated to be a sanity check
            //
            if(1)
            {
                s32 nVerts = 0;
                for ( s32 j = 0; j < TriangleCount; j++ )
                {
                    for ( s32 k = 0; k < 3; k++ )
                    {
                        s32 VertexIndex = pMesh->GetPolygonVertex( j, k );
                        if ( VertexIndex > nVerts ) nVerts = VertexIndex;
                    }
                }

                ASSERT( Info.m_VertexCount == ( nVerts + 1 ) );
            }

            // Build polygon array
            m_Vertices.appendList( Info.m_VertexCount );
            x_memset( &m_Vertices[m_Vertices.getCount()-Info.m_VertexCount], 0, Info.m_VertexCount * sizeof(rawgeom::vertex) );

            xarray<new_vert> Done;
            Done.appendList(Info.m_VertexCount);

            for( s32 j = 0; j < TriangleCount; j++ )
            {
                //
                // Deal with the material for the facet
                //
                Info.m_iMaterial[j] = -1;

                if ( MaterialIndexIsAllSame )
                    Info.m_iMaterial[j] = iCommonMaterial;
                else
                {
                    for ( s32 l = 0; l < pMesh->GetElementMaterialCount(); l++)
                    {
                        FbxGeometryElementMaterial* lMaterialElement = pMesh->GetElementMaterial( l);
                        FbxSurfaceMaterial* lMaterial = NULL;
                        int lMatId = -1;
                        lMaterial = pMesh->GetNode()->GetMaterial(lMaterialElement->GetIndexArray().GetAt(j));
                        lMatId    = lMaterialElement->GetIndexArray().GetAt(j);

                        if(lMatId >= 0)
                        {
                            xbool bFound = m_lMaterialKey.BinarySearch( material_temp{lMaterial,0}, lMatId );
                            ASSERT(bFound);
                            Info.m_iMaterial[j] = m_lMaterialKey[ lMatId ].m_UID;
                            break;      // I assume we break here not sure 
                        }
                        else
                        {
                            ASSERT(0);
                        }
                    }
                }

                //
                // Go through all the vertices of the facet
                //
                for( s32 k = 0; k < 3; k++ )
                {
                    const s32                VertexIndex = pMesh->GetPolygonVertex(j, k);
                    const s32                RawiVertex  = StartIndex + VertexIndex;
                    const s32                RawiFace    = j * 3 + k;
                    rawgeom::vertex*         pVertex     = NULL;
                    rawgeom::vertex          Temp;

                    if ( Done[ VertexIndex ].m_CRC )
                    {
                        pVertex = &Temp;
                    }
                    else
                    {
                        pVertex = &m_Vertices[ RawiVertex ];
                        ASSERT(Done[ VertexIndex ].m_iNext ==-1);
                    }

                    // Copy memory layout of the vertex this will seed the CRC
                    // also copies any other components that are pre-computed such position and such
                    *pVertex                      = ControlPoints[ VertexIndex ];
                                        
                    pVertex->m_nNormals   = pMesh->GetElementNormalCount( );
                    pVertex->m_nTangents  = pMesh->GetElementTangentCount( );
                    pVertex->m_nBinormals = pMesh->GetElementBinormalCount( );

                    const s32 nBTNMax = x_Max( pVertex->m_nNormals, x_Max( pVertex->m_nTangents, pVertex->m_nBinormals ) );
                    if ( nBTNMax > 0 )
                    {
                        for ( s32 inorm = 0; inorm<nBTNMax; inorm++ )
                        {
                            rawgeom::btn& BTN = pVertex->m_BTN[ inorm ];
                     
                            if ( inorm < pVertex->m_nNormals )   BTN.m_Normal   = NormalMatrix * GetBTN( pMesh, inorm, VertexIndex, RawiFace, TYPE_NORMAL );
                            if ( inorm < pVertex->m_nTangents )  BTN.m_Tangent  = NormalMatrix * GetBTN( pMesh, inorm, VertexIndex, RawiFace, TYPE_TANGENT );
                            if ( inorm < pVertex->m_nBinormals ) BTN.m_Binormal = NormalMatrix * GetBTN( pMesh, inorm, VertexIndex, RawiFace, TYPE_BINORMAL );
                        }
                    }

                    if( pMesh->GetUVLayerCount() > 0 )
                    {
                        pVertex->m_nUVs = pMesh->GetUVLayerCount();
                        for( s32 iuv=0; iuv<pVertex->m_nUVs; iuv++ )
                        {
                            GetUV( 
                                pMesh, 
                                iuv, 
                                VertexIndex, 
                                pMesh->GetTextureUVIndex(j, k), 
                                pVertex->m_UV[iuv].m_X, 
                                pVertex->m_UV[iuv].m_Y );

                                // openGL texture flip issue...
                                pVertex->m_UV[iuv].m_Y  = 1-pVertex->m_UV[iuv].m_Y ;
                        }
                    }
                    else
                    {
                        pVertex->m_nUVs = 0;
                    }

                    //
                    // Decide which index this vertex gets
                    //
                    if ( Done[ VertexIndex ].m_CRC )
                    {
                        xbool   Founded = FALSE;
                        u32     CRC     = x_memCRC32( pVertex, sizeof(*pVertex ) );
                        s32     m       = VertexIndex;

                        // make sure that the CRC is never null
                        CRC = CRC ? CRC : u32(xuptr(this));

                        // try to find our vertex
                        do 
                        {
                            if ( CRC == Done[ m ].m_CRC )
                            {
                                // lets check carefully to make sure
                                if ( rawgeom::TempVCompare( *pVertex, m_Vertices[ StartIndex + m ] ) )
                                {
                                    Info.m_Polygons[ RawiFace ] = StartIndex + m;
                                    Founded = TRUE;
                                    break;
                                }
                            }

                            if( Done[ m ].m_iNext == -1 ) 
                                break;

                            m = Done[ m ].m_iNext;

                        } while( 1 );

                        if ( Founded == FALSE )
                        {
                            // Deal with the done list
                            s32 iNew;
                            Done.append(iNew);
                            Done[ iNew ].m_CRC = CRC;
                            Done[ m ].m_iNext = iNew;

                            // Deal with the plygon index
                            Info.m_Polygons[ RawiFace ] = StartIndex + iNew;

                            // Deal with the vertex list
                            s32 l;
                            m_Vertices.append(l) = *pVertex;
                            ASSERT(l== (StartIndex + iNew) );
                        }
                        else
                        {
                            // Deal with the plygon index
                            Info.m_Polygons[ RawiFace ] = StartIndex + m;
                        }
                    }
                    else
                    {
                        // Deal with the plygon index
                        Info.m_Polygons[ RawiFace ] = RawiVertex;
                        Done[ VertexIndex ].m_CRC = x_memCRC32( pVertex, sizeof(*pVertex ) );

                        // make sure that the CRC is never null
                        Done[ VertexIndex ].m_CRC = Done[ VertexIndex ].m_CRC ? Done[ VertexIndex ].m_CRC : u32(xuptr(this));
                    }
                }
            }
        }
    }

    s32 ChildrenCount = pNode->GetChildCount();
    for ( s32 i = 0; i < ChildrenCount; i++ )
    {
        FbxNode* ChildNode = pNode->GetChild(i);

        if ( ChildNode->GetParent() != pNode )
        {
            continue;
        }

        ReadMesh(ChildNode);
    }
}

//-----------------------------------------------------------------------------------

xvector3d fbx_to_rawmesh::GetBTN( FbxMesh* pMesh, s32 iNorm, s32 VertexIndex, s32 VertexCounter, type_btn Type )
{
    xvector3d Normal;
    Normal.Zero();

    FbxLayerElementTemplate<FbxVector4>* pVertexNormal = NULL;
    
    if ( Type == TYPE_NORMAL )
    {
        pVertexNormal = pMesh->GetElementNormal( iNorm );
    } 
    else if ( Type == TYPE_TANGENT )
    {
        pVertexNormal = pMesh->GetElementTangent( iNorm );
    }
    else if ( Type == TYPE_BINORMAL )
    {
        pVertexNormal = pMesh->GetElementBinormal( iNorm );
    }

    if ( pVertexNormal == NULL )
        return Normal;

    switch( pVertexNormal->GetMappingMode() )
    {
        case FbxGeometryElement::eByControlPoint:
        {
            switch( pVertexNormal->GetReferenceMode() )
            {
                case FbxGeometryElement::eDirect:
                {
                    Normal.m_X = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexIndex).mData[0] );
                    Normal.m_Y = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexIndex).mData[1] );
                    Normal.m_Z = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexIndex).mData[2] );
                }
                break;

                case FbxGeometryElement::eIndexToDirect:
                {
                    s32 index = pVertexNormal->GetIndexArray().GetAt(VertexIndex);
                    Normal.m_X = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[0] );
                    Normal.m_Y = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[1] );
                    Normal.m_Z = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[2] );
                }
                break;

                default:
                    x_throw("Invalid Reference");
            }
        }
        break;

        case FbxGeometryElement::eByPolygonVertex:
        {
            switch( pVertexNormal->GetReferenceMode() )
            {
                case FbxGeometryElement::eDirect:
                {
                    Normal.m_X = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexCounter).mData[0] );
                    Normal.m_Y = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexCounter).mData[1] );
                    Normal.m_Z = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(VertexCounter).mData[2] );
                }
                break;

                case FbxGeometryElement::eIndexToDirect:
                {
                    s32 index = pVertexNormal->GetIndexArray().GetAt(VertexCounter);
                    Normal.m_X = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[0] );
                    Normal.m_Y = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[1] );
                    Normal.m_Z = static_cast<f32>( pVertexNormal->GetDirectArray().GetAt(index).mData[2] );
                }
                break;

                default:
                    x_throw("Invalid Reference");
            }
            break;
                
            default:
                ASSERT(0);
        }
        break;
    }

    return Normal;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::GetUV( FbxMesh* pMesh, s32 iUV, s32 VertexIndex, s32 UVIndex, f32& U, f32& V )
{
    FbxGeometryElementUV* pVertexUV = pMesh->GetElementUV(iUV);

    switch( pVertexUV->GetMappingMode() )
    {
        default: ASSERT(0);
        case FbxGeometryElement::eByControlPoint:
        {
            switch( pVertexUV->GetReferenceMode() )
            {
                case FbxGeometryElement::eDirect:
                {
                    U = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(VertexIndex).mData[0] );
                    V = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(VertexIndex).mData[1] );
                }
                break;

            case FbxGeometryElement::eIndexToDirect:
                {
                    s32 index = pVertexUV->GetIndexArray().GetAt(VertexIndex);
                    U = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(index).mData[0] );
                    V = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(index).mData[1] );
                }
                break;

            default:
                x_throw("Invalid Reference");
            }
        }
        break;

        case FbxGeometryElement::eByPolygonVertex:
        {
            switch( pVertexUV->GetReferenceMode() )
            {
                case FbxGeometryElement::eDirect:
                case FbxGeometryElement::eIndexToDirect:
                {
                    U = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(UVIndex).mData[0] );
                    V = static_cast<f32>( pVertexUV->GetDirectArray().GetAt(UVIndex).mData[1] );
                }
                break;

            default:
                x_throw("Invalid Reference");
            }
        }
        break;
    }
}

//-----------------------------------------------------------------------------------

// Get the geometry offset to a node. It is never inherited by the children.
FbxAMatrix fbx_to_rawmesh::GetGeometryTransform( FbxNode* pNode )
{
    const FbxVector4 lT = pNode->GetGeometricTranslation( FbxNode::eSourcePivot );
    const FbxVector4 lR = pNode->GetGeometricRotation   ( FbxNode::eSourcePivot );
    const FbxVector4 lS = pNode->GetGeometricScaling    ( FbxNode::eSourcePivot );

    return FbxAMatrix(lT, lR, lS);
}

//-----------------------------------------------------------------------------------

s32 fbx_to_rawmesh::FindNodeIndex( const FbxNode* pNode )
{
    NodeIndexMap::iterator  It      = m_NodeIndexMap.find( (FbxNode*)pNode );
    s32                     Index   = -1;
    
    if( It != m_NodeIndexMap.end() )
    {
        Index = (*It).second;
    }
    
    return Index;
}

//-----------------------------------------------------------------------------------

fbx_to_rawmesh::node_info* fbx_to_rawmesh::FindNode( const FbxNode* Node )
{
    node_info*  pNodeInfo   = NULL;
    s32         Index       = FindNodeIndex(Node);
    
    if( Index >= 0 && Index < m_Nodes.getCount() )
    {
        pNodeInfo = m_Nodes[Index];
    }
    
    return pNodeInfo;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::ConvertMatrix( const FbxAMatrix& Input, transform& Output )
{
    FbxVector4      Translation = Input.GetT();
    
    Output.m_Translation.Set( (f32)Translation.mData[0], (f32)Translation.mData[1], (f32)Translation.mData[2] );
    
    FbxQuaternion   Rotation    = Input.GetQ();
    FbxVector4      Scale       = Input.GetS();
    
    Output.m_Scale.Set( (f32)Scale.mData[0], (f32)Scale.mData[1], (f32)Scale.mData[2] );

    Output.m_Rotation.m_X = (f32)Rotation.mData[0];
    Output.m_Rotation.m_Y = (f32)Rotation.mData[1];
    Output.m_Rotation.m_Z = (f32)Rotation.mData[2];
    Output.m_Rotation.m_W = (f32)Rotation.mData[3];
}

//-----------------------------------------------------------------------------------

xmatrix4 fbx_to_rawmesh::ConvertMatrix( const FbxAMatrix& Input ) const
{
    xmatrix4 Output;

    for( s32 i = 0; i < 4; i++ )
    {
        Output( 0, i ) = (f32)Input.Get( i, 0 );
        Output( 1, i ) = (f32)Input.Get( i, 1 );
        Output( 2, i ) = (f32)Input.Get( i, 2 );
        Output( 3, i ) = (f32)Input.Get( i, 3 );
    }

    return Output;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::BuildRawGeom( rawgeom& RawGeom )
{
    if( m_Meshes.getCount() <= 0 )
        return;
    
    //
    // Collect all the submeshes
    //
    s32 TotalFacets = 0;
    RawGeom.m_Mesh.New( m_Meshes.getCount( ) );
    for( s32 i = 0; i < m_Meshes.getCount(); i++ )
    {
        rawgeom::mesh&     Mesh         = RawGeom.m_Mesh[i];
        mesh_info&         FBXSubMesh   = m_Meshes[i];
        
        Mesh.m_Name.Copy( FBXSubMesh.m_pNode->GetName() );
        Mesh.m_nBones = m_Nodes.getCount();
        
        // Keep a count of the total number of faces
        TotalFacets += FBXSubMesh.m_PolygonCount;
    }

    //
    // Collect all the faces
    //
    RawGeom.m_Facet.Alloc( TotalFacets );
    s32 iFace = 0;
    for( mesh_info& MeshInfo : m_Meshes )
    {
        s32 nPolys = MeshInfo.m_PolygonCount;
        for ( s32 i = 0; i < nPolys; i++ )
        {
            rawgeom::facet& Facet = RawGeom.m_Facet[ iFace++ ];
            
            Facet.m_iMesh       = s32(&MeshInfo - &m_Meshes[0]);
            Facet.m_nVertices   = 3;
            Facet.m_iVertex[ 0 ] = MeshInfo.m_Polygons[ i * 3 + 0 ]; 
            Facet.m_iVertex[ 1 ] = MeshInfo.m_Polygons[ i * 3 + 1 ]; 
            Facet.m_iVertex[ 2 ] = MeshInfo.m_Polygons[ i * 3 + 2 ];
            
            // Info.            
            s32 iMaxMaterial = MeshInfo.m_iMaterial[ i ];
            Facet.m_iInformed   = iMaxMaterial;
        }
    }
    
    //
    // Collect vertices
    //
    RawGeom.m_Vertex.Alloc( m_Vertices.getCount() );
    for( s32 i=0; i<m_Vertices.getCount(); i++ )
    {
        rawgeom::vertex&        DestVert = RawGeom.m_Vertex[i];
        const rawgeom::vertex&  SrcVert  = m_Vertices[i];
        
        DestVert = SrcVert;
    }
    
    //
    // Collect bones
    //
    RawGeom.m_Bone.New( m_Nodes.getCount( ), XMEM_FLAG_ALIGN_16B );
    
    for( s32 i = 0; i < m_Nodes.getCount(); i++ )
    {
        const node_info& NodeInfo   = *m_Nodes[ i ];
        rawgeom::bone&  Bone        = RawGeom.m_Bone[ i ];
        
        // Get the name for this bone
        Bone.m_Name.Copy( NodeInfo.m_pNode->GetName( ) );
        
        Bone.m_Position = NodeInfo.m_BindPose.m_Translation;
        Bone.m_Rotation = NodeInfo.m_BindPose.m_Rotation;
        Bone.m_Scale    = NodeInfo.m_BindPose.m_Scale;
        
        if ( NULL != NodeInfo.m_pParent )
        {
            Bone.m_iParent = FindNodeIndex( NodeInfo.m_pParent->m_pNode );
        }
        else
        {
            Bone.m_iParent  = -1;
        }
    }

    //
    // Collect Materials
    //
    RawGeom.m_InformedMaterial.New( m_lInformedMaterial.getCount() );
    for ( s32 i = 0; i < m_lInformedMaterial.getCount(); i++ )
    {
        RawGeom.m_InformedMaterial[ i ] = m_lInformedMaterial[ i ];
    }
    
    //
    // Count how many children do the bones have
    //
    for( s32 i = 0; i < m_Nodes.getCount(); i++ )
    {
        rawgeom::bone& Bone = RawGeom.m_Bone[ i ];
        
        Bone.m_nChildren = 0;
        for( s32 j=0; j<m_Nodes.getCount(); j++ )
        {
            rawgeom::bone& BoneJ = RawGeom.m_Bone[ j ];
            if( i == BoneJ.m_iParent )
                Bone.m_nChildren++;
        }
    }

    //
    // Ok we dont need to get the animation information
    //
/*
    if ( m_bArticulated == FALSE )
        return;

    RawAnim.m_FPS = 60;

    //
    // Build Raw Anim Bones
    // 
    RawAnim.m_Bone.New( RawGeom.m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );
    RawAnim.m_nFrames = 0;
    for( const rawgeom::bone& GeomBone : RawGeom.m_Bone )
    {
        const s32           Index       = s32( &GeomBone - &RawGeom.m_Bone[0] );
        rawanim::bone&      AnimBone    = RawAnim.m_Bone[Index];
        const node_info&    NodeInfo    = *m_Nodes[ Index ];

        AnimBone.m_Name             = GeomBone.m_Name;
        AnimBone.m_nChildren        = GeomBone.m_nChildren;
        AnimBone.m_BindScale        = GeomBone.m_Scale;
        AnimBone.m_BindRotation     = GeomBone.m_Rotation;
        AnimBone.m_BindTranslation  = GeomBone.m_Position;
        AnimBone.m_iParent          = GeomBone.m_iParent;
        AnimBone.m_bRotationKeys    = NodeInfo.m_TrackInfo.m_bHasRotation;
        AnimBone.m_bScaleKeys       = NodeInfo.m_TrackInfo.m_bHasScaling;
        AnimBone.m_bTranslationKeys = NodeInfo.m_TrackInfo.m_bHasTranslation;
        AnimBone.m_nChildren        = GeomBone.m_nChildren;
        AnimBone.m_bIsMasked        = FALSE;        // TODO: We need to support this

        AnimBone.m_BindMatrix.setup( AnimBone.m_BindScale, AnimBone.m_BindRotation, AnimBone.m_BindTranslation );
        AnimBone.m_BindMatrixInv = AnimBone.m_BindMatrix;
        AnimBone.m_BindMatrixInv.FullInvert();

        RawAnim.m_nFrames = x_Max( RawAnim.m_nFrames, NodeInfo.m_TrackInfo.m_KeyFrames.getCount() );
        ASSERT( NodeInfo.m_TrackInfo.m_FrameRate == 60 );
    }

    //
    // Build Raw Anim Frames
    //
    RawAnim.m_KeyFrame.New( RawGeom.m_Bone.getCount() * RawAnim.m_nFrames, XMEM_FLAG_ALIGN_16B );

    // Init all of them
    for ( rawanim::key_frame& Frame : RawAnim.m_KeyFrame )
    {
        Frame.m_Scale.Set( 1.f );
        Frame.m_Translation.Set( 0.f );
        Frame.m_Rotation.Identity();
    }

    // Now fill the information
    if( RawAnim.m_nFrames ) for( const rawanim::bone& AnimBone : RawAnim.m_Bone )
    {
        const s32           Index       = s32( &AnimBone - &RawAnim.m_Bone[0] );
        const node_info&    NodeInfo    = *m_Nodes[ Index ];
        rawanim::key_frame* pFrame      = &RawAnim.m_KeyFrame[ Index ];

        s32 Start = s32(NodeInfo.m_TrackInfo.m_Start*60/1000.f);
        for ( s32 j = 0; j < NodeInfo.m_TrackInfo.m_KeyFrames.getCount(); j++ )
        {
            const rawanim::key_frame&   NodeInfoKeyFrame    = NodeInfo.m_TrackInfo.m_KeyFrames[j];
            rawanim::key_frame&         Frame               = pFrame[ (Start + j) * RawAnim.m_Bone.getCount() ];
            ASSERT( (Start + j) < RawAnim.m_nFrames );
            ASSERT( (Start + j) >= 0 );

            if ( NodeInfo.m_TrackInfo.m_bHasRotation )
            {
                Frame.m_Rotation        = NodeInfoKeyFrame.m_Rotation;
            }

            if ( NodeInfo.m_TrackInfo.m_bHasScaling )
            {
                Frame.m_Scale           = NodeInfoKeyFrame.m_Scale;
            }

            if ( NodeInfo.m_TrackInfo.m_bHasTranslation )
            {
                Frame.m_Translation     = NodeInfoKeyFrame.m_Translation;
            }
        }
    }
*/
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::BuildRawAnim( rawanim& RawAnim )
{
    RawAnim.m_FPS = 60;

    //
    // Build Raw Anim Bones
    // 
    RawAnim.m_Bone.New( m_Nodes.getCount(), XMEM_FLAG_ALIGN_16B );
    RawAnim.m_nFrames = 0;
    for( s32 i = 0; i < m_Nodes.getCount(); i++ )
    {
        const s32           Index       = i;
        rawanim::bone&      AnimBone    = RawAnim.m_Bone[Index];
        const node_info&    NodeInfo    = *m_Nodes[ Index ];

        AnimBone.m_Name.Copy( NodeInfo.m_pNode->GetName( ) );
       // AnimBone.m_nChildren        = GeomBone.m_nChildren;
        AnimBone.m_BindScale        = NodeInfo.m_BindPose.m_Scale;
        AnimBone.m_BindRotation     = NodeInfo.m_BindPose.m_Rotation;
        AnimBone.m_BindTranslation  = NodeInfo.m_BindPose.m_Translation;
        AnimBone.m_bRotationKeys    = NodeInfo.m_TrackInfo.m_bHasRotation;
        AnimBone.m_bScaleKeys       = NodeInfo.m_TrackInfo.m_bHasScaling;
        AnimBone.m_bTranslationKeys = NodeInfo.m_TrackInfo.m_bHasTranslation;
        AnimBone.m_bIsMasked        = FALSE;        // TODO: We need to support this

        if ( NULL != NodeInfo.m_pParent )
        {
            AnimBone.m_iParent = FindNodeIndex( NodeInfo.m_pParent->m_pNode );
        }
        else
        {
            AnimBone.m_iParent  = -1;
        }

        AnimBone.m_BindMatrix.setup( AnimBone.m_BindScale, AnimBone.m_BindRotation, AnimBone.m_BindTranslation );
        AnimBone.m_BindMatrixInv = AnimBone.m_BindMatrix;
        AnimBone.m_BindMatrixInv.FullInvert();

        RawAnim.m_nFrames = x_Max( RawAnim.m_nFrames, NodeInfo.m_TrackInfo.m_KeyFrames.getCount() );
        ASSERT( NodeInfo.m_TrackInfo.m_FrameRate == 60 );
    }

    //
    // Count how many children do the bones have
    //
    for( s32 i = 0; i < m_Nodes.getCount(); i++ )
    {
        rawanim::bone&      AnimBone    = RawAnim.m_Bone[i];
        
        AnimBone.m_nChildren = 0;
        for( s32 j=0; j<m_Nodes.getCount(); j++ )
        {
            rawanim::bone& BoneJ = RawAnim.m_Bone[ j ];
            if( i == BoneJ.m_iParent )
                AnimBone.m_nChildren++;
        }
    }

    //
    // Build Raw Anim Frames
    //
    RawAnim.m_KeyFrame.New( RawAnim.m_Bone.getCount() * RawAnim.m_nFrames, XMEM_FLAG_ALIGN_16B );

    // Init all of them
    for ( rawanim::key_frame& Frame : RawAnim.m_KeyFrame )
    {
        Frame.m_Scale.Set( 1.f );
        Frame.m_Translation.Set( 0.f );
        Frame.m_Rotation.Identity();
    }

    // Now fill the information
    if( RawAnim.m_nFrames ) for( const rawanim::bone& AnimBone : RawAnim.m_Bone )
    {
        const s32           Index       = s32( &AnimBone - &RawAnim.m_Bone[0] );
        const node_info&    NodeInfo    = *m_Nodes[ Index ];
        rawanim::key_frame* pFrame      = &RawAnim.m_KeyFrame[ Index ];

        s32 Start = x_Max(0,s32((NodeInfo.m_TrackInfo.m_Start + m_AnimationStartTime)*60/1000.f));
        for ( s32 j = 0; j < NodeInfo.m_TrackInfo.m_KeyFrames.getCount(); j++ )
        {
            const rawanim::key_frame&   NodeInfoKeyFrame    = NodeInfo.m_TrackInfo.m_KeyFrames[j];
            rawanim::key_frame&         Frame               = pFrame[ (Start + j) * RawAnim.m_Bone.getCount() ];
            ASSERT( (Start + j) < RawAnim.m_nFrames );
            ASSERT( (Start + j) >= 0 );

            if ( NodeInfo.m_TrackInfo.m_bHasRotation )
            {
                Frame.m_Rotation        = NodeInfoKeyFrame.m_Rotation;
            }

            if ( NodeInfo.m_TrackInfo.m_bHasScaling )
            {
                Frame.m_Scale           = NodeInfoKeyFrame.m_Scale;
            }

            if ( NodeInfo.m_TrackInfo.m_bHasTranslation )
            {
                Frame.m_Translation     = NodeInfoKeyFrame.m_Translation;
            }
        }
    }
}

//-----------------------------------------------------------------------------------

fbx_to_rawmesh::node_info* fbx_to_rawmesh::AddNode( const FbxNode* pNode )
{
    node_info* pNodeInfo = x_new( node_info, 1, XMEM_FLAG_ALIGN_16B );
    pNodeInfo->m_pNode   = (FbxNode*)pNode;
    
    ConvertMatrix( ((FbxNode*)pNode)->EvaluateLocalTransform(), pNodeInfo->m_Local );

    if( m_NodeIndexMap.find( (FbxNode*)pNode ) == m_NodeIndexMap.end() )
    {
        m_NodeIndexMap[ (FbxNode*)pNode ]  = m_Nodes.getCount();
        m_Nodes.append()                   = pNodeInfo;
    }
    else
    {
        x_delete( pNodeInfo );
        pNodeInfo = NULL;
    }
    
    return pNodeInfo;
}

//-----------------------------------------------------------------------------------

void fbx_to_rawmesh::ConvertMatrix( const transform& Input, xmatrix4& Output )
{
    xmatrix4 Translation;
    xmatrix4 Rotation;
    xmatrix4 Scaling;

    Translation.Identity();
    Rotation.Identity();
    Scaling.Identity();
    
    Translation.setTranslation(Input.m_Translation);
    Rotation.setRotation( Input.m_Rotation );
    Scaling.setScale(Input.m_Scale);

    Output = Translation * Rotation * Scaling;
}


