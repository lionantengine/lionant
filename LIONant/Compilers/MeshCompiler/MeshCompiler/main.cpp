//
//  main.cpp
//  MeshCompiler
//
//  Created by Tomas Arce on 10/30/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "compilerBase.h"
#include "meshCompilerBase.h"

// -OVERWRITE_DEFAULTS -PROJECT "C:\RationWorls\LIONant\xReference\Data"
// -BUILDTYPE -PROJECT "C:\RationWorls\LIONant\xReference\Data" -TARGET PC -INPUT "GameData\CompilerKeys\RPG--56ii_i84y_zsbj.mesh"
// -BUILDTYPE RELEASE -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/LIONant/xReference/Data" -TARGET OSX IOS -INPUT "GameData/CompilerKeys/BaseMale--5dt3_y1j5_zvof.mesh"

int main( int argc, const char * argv[ ] )
{
    mesh_compiler_base Compiler;

    if ( Compiler.Parse( argc, argv ) )
    {
        Compiler.Compile( );
    }

    return 0;
}

