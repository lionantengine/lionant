#ifndef RAW_ANIM_H
#define RAW_ANIM_H

#include "x_base.h"

//=========================================================================
// CLASS
//=========================================================================
class rawanim
{
public:
    
    enum
    {
        NUM_EVENT_STRINGS       = 5,
        NUM_EVENT_INTS          = 5,
        NUM_EVENT_FLOATS        = 8,
        NUM_EVENT_BOOLS         = 8,
        NUM_EVENT_COLORS        = 4,
    };
    
    struct bone
    {
        xstring                 m_Name;
        s32                     m_iParent;
        s32                     m_nChildren;

        xvector3                m_BindTranslation;
        xquaternion             m_BindRotation;
        xvector3                m_BindScale;

        xbool                   m_bScaleKeys;
        xbool                   m_bRotationKeys;
        xbool                   m_bTranslationKeys;
        xbool                   m_bIsMasked;

        xmatrix4                m_BindMatrix;
        xmatrix4                m_BindMatrixInv;
    };

    using key_frame = xtransform;

	struct event
	{
		xstring                 m_Name;
		xstring                 m_ParentName;
		s32                     m_Type;
		f32                     m_Radius;
		s32                     m_Frame0;
		s32                     m_Frame1;
		xvector3                m_Position;
	};

    struct super_event
	{
        xstring                 m_Name;

        s32                     m_Type;
        s32                     m_StartFrame;
        s32                     m_EndFrame;
	    xvector3                m_Position;
        xquaternion             m_Rotation;
        f32                     m_Radius;

        xbool                   m_ShowAxis;
        xbool                   m_ShowSphere;
        xbool                   m_ShowBox;

        f32                     m_AxisSize;

        f32                     m_Width;
        f32                     m_Length;
        f32                     m_Height;

        xsafe_array<xstring,NUM_EVENT_STRINGS>  m_Strings;
        xsafe_array<s32,NUM_EVENT_INTS>         m_Ints;
        xsafe_array<f32,NUM_EVENT_FLOATS>       m_Floats;
        xsafe_array<xbool,NUM_EVENT_BOOLS>      m_Bools;
        xsafe_array<xcolor,NUM_EVENT_COLORS>    m_Colors;
	};
    
    struct prop_frame
    {
        xvector3                m_Scale;
        xquaternion             m_Rotation;
        xvector3                m_Translation;
        xbool                   m_bVisible;
    };

    struct prop
    {
        xstring                 m_Name;
        s32                     m_iParentBone;
        xstring                 m_Type;
    };

public:
    
    rawanim( void );
    ~rawanim( void );

    xbool   Load                    ( const char* pFileName );
    void    Save                    ( const char* pFileName ) const;
    void    CleanUp                 ( void );

    xbool   AreBonesFromSameBranch  ( const s32 iBoneA, const s32 iBoneB ) const;
    void    PutBonesInLODOrder      ( void );

    void    ComputeBonesL2W         ( xmatrix4* pMatrix, f32 Frame ) const;

    void    ComputeBonesL2W         ( xmatrix4* pMatrix,
                                      s32      iFrame,
                                      xbool    bRemoveHorizMotion,
                                      xbool    bRemoveVertMotion,
                                      xbool    bRemoveYawMotion ) const;

    void    ComputeBoneL2W          ( s32 iBone, xmatrix4& Matrix, f32 Frame ) const;
    void    ComputeRawBoneL2W       ( s32 iBone, xmatrix4& Matrix, s32 iFrame ) const;

    s32     GetBoneIDFromName       ( const char* pBoneName ) const;
    void    ComputeBoneKeys         ( xquaternion* pQ, xvector3* pS, xvector3* pT, f32 Frame ) const;

    void    BakeBindingIntoFrames   ( xbool BakeScale, xbool BakeRotation, xbool BakeTranslation );

    void    RemoveFramesFromRage    ( s32 StartingValidRange, s32 EndingValidRange );

    void    DeleteBone              ( s32 iBone );
    void    DeleteBone              ( const char* pBoneName );
    void    DeleteDummyBones        ( void );           // Deletes all bones with "dummy" in the name

    xbool   ApplyNewSkeleton        ( const rawanim& BindAnim );
    xbool   HasSameSkeleton         ( const rawanim& Anim ) const;

    void    SetNewRoot              ( s32 Index );

    void    SanityCheck             ( void ) const;

    xbool   IsMaskedAnim            ( void ) const;

    void    CopyFrames              ( xptr<key_frame>& KeyFrame, s32 iStart, s32 nFrames ) const;
    void    InsertFrames            ( s32 iDestFrame, xptr<key_frame>& KeyFrame );

    void    RencenterAnim           ( xbool TX, xbool TY, xbool TZ, xbool Pitch, xbool Yaw, xbool Roll );
    void    CleanLoopingAnim        ( void );

    rawanim&    operator =          ( const rawanim& Src ) 
    { 
        m_nFrames   = Src.m_nFrames;
        m_FPS       = Src.m_FPS;

        m_Name.Copy         ( Src.m_Name );
        m_Bone.Copy         ( Src.m_Bone );    
        m_KeyFrame.Copy     ( Src.m_KeyFrame);
        m_Event.Copy        ( Src.m_Event );   
        m_SuperEvent.Copy   ( Src.m_SuperEvent );
        m_Prop.Copy         ( Src.m_Prop );    
        m_PropFrame.Copy    ( Src.m_PropFrame );

        return *this;
    }
     
public:

    s32                     m_nFrames               = 0;
    xstring                 m_Name                  = X_STR("");
    s32                     m_FPS                   = 60;                      

    xptr<bone>              m_Bone;
    xptr<key_frame>         m_KeyFrame;                  // Bones in the Columns, key frames on the Rows
	xptr<event>             m_Event;
	xptr<super_event>       m_SuperEvent;
    xptr<prop>              m_Prop;
    xptr<prop_frame>        m_PropFrame;
};

//=========================================================================
// END
//=========================================================================
#endif


