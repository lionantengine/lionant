
#include "ArmOptimizer.h"

xbool s_bVeryVerbose = FALSE;
xbool s_bOutputLog   = FALSE;

///////////////////////////////////////////////////////////////////////////
// TYPES
///////////////////////////////////////////////////////////////////////////

struct weight_list
{
    s32 m_nWeights;
    s32 m_Weight[3*8];
};

///////////////////////////////////////////////////////////////////////////
// FUNCTIONS
///////////////////////////////////////////////////////////////////////////

//=========================================================================
// The goal here is to make the matrices uploads as consecutive as possible
// so that the user can merge them into as few uploads as possible
void arm_optimizer::OptimizeUploads( section& Section ) const
{
    // TODO:
}

//=========================================================================

void arm_optimizer::OpCommands( section& Section ) const
{    
    xfile           Fp;
    list_command    lOptimized; 
    s32             From     = 0;
    s32             To       = 0;
    s32             Flash    = 0;
    s32*            FlushSys = m_pMatrixCacheScore;
    list_command&   lCommand = Section.m_lCommand;
    list_vertex     lNewVertex;

    //
    // Reset the matrix cache
    //
    // The matrix score values will mean the fallowing
    //  1 is lock
    //  2 old matrix
    // -1 is corently not been use
    //
    for( s32 i=0; i<m_MatrixCacheSize; i++ )
    {
        m_pMatrixCache[ i ]     = -1;
        FlushSys[ i ]           = -1;
    }

    for( s32 i=0; i<lCommand.getCount(); i++ )
    {
        command& Command = lCommand[i];

        if( Command.m_Type == UPLOAD_MATRIX )
        {
            m_pMatrixCache[ Command.m_Arg2 ]  = Command.m_Arg1;
            FlushSys[ Command.m_Arg2 ]        = 1;
        }

        if( Command.m_Type == NOP_MATRIX && (Flash == 1) )
        {
            FlushSys[ Command.m_Arg2 ] = -1;
        }

        if( Command.m_Type == DRAW_LIST )
        {
            To = Command.m_Arg2;
            s32 j;
            s32 nFreeNodes=0;

            //
            // Check whether we have a full cache
            //
            for( j=0; j<m_MatrixCacheSize; j++ )
            {
                if( FlushSys[ j ] == -1 ) nFreeNodes++;
            }

            //
            // Check howmany new matrices we are going to load next
            //
            {
                for( j=(i+1); j<lCommand.getCount(); j++ )
                {
                    command& Command = lCommand[j];
                    if( Command.m_Type != UPLOAD_MATRIX ) break;
                    nFreeNodes--;
                }
            }

            // Disactivate cleaning mode
            Flash = 0;

            if( nFreeNodes >= 0  && ((i+1) < lCommand.getCount()) ) continue;

            // Activate cleaning mode
            Flash = 1;

            //
            // Flash whatever matrices are left over if this is the last draw
            //
            //
            for( j=0; j<m_MatrixCacheSize; j++ )
            {
                if( FlushSys[ j ] >= 0 )
                {
                    if( FlushSys[ j ] != -1 )
                    {
                        if( FlushSys[ j ] == 1 )
                            lOptimized.append() =  command( UPLOAD_MATRIX, m_pMatrixCache[ j ], j );

                        FlushSys[ j ] = 2;
                    }
                }
            }

            //
            // Flush the polys
            //
            {
                s32 OldFrom = From;
                lOptimized.append() = command( DRAW_LIST, From, To );
                From  = To;

                //
                // Change the index of the vertex weighting to point at the 
                // right matrix cache entry.
                //
                {
                    list_triangle& lTriangle = Section.m_lTriangle;
                    list_vertex&   lVertex   = Section.m_lVertex;

                    for( s32 j=OldFrom; j<To; j++ )
                    {
                        for( s32 v=0; v<3; v++ )
                        {
                            rawgeom::vertex V = lVertex[ lTriangle[j].m_iVertex[v] ];

                            //
                            // Use the weights in the cache
                            // 
                            for( s32 w = 0; w<V.m_nWeights; w++ )
                            {
                                s32 m;
                                for( m=0; m<m_MatrixCacheSize; m++ )
                                {
                                    if( FlushSys[ m ] >= 0 )
                                    {
                                        if( V.m_Weight[w].m_iBone == m_pMatrixCache[ m ] )
                                        {
                                            V.m_Weight[w].m_iBone = m;
                                            break;
                                        }
                                    }
                                }

                                ASSERT( m != m_MatrixCacheSize );
                            }

                            //
                            // Add new vertices if we need to
                            // 
                            s32 w;
                            for( w=0; w<lNewVertex.getCount(); w++ )
                            {
                                rawgeom::vertex& V2 = lNewVertex[w];

                                if( V.m_Position == V2.m_Position &&
                                    V.m_nWeights == V2.m_nWeights )
                                {   
                                    s32 h;
                                    for( h=0; h<V.m_nWeights; h++ )
                                    {
                                        if( V.m_Weight[h].m_iBone != V2.m_Weight[h].m_iBone )
                                            break;

                                        if( V.m_Weight[h].m_Weight != V2.m_Weight[h].m_Weight )
                                            break;
                                    }

                                    if( h != V.m_nWeights ) continue;

                                    if( V.m_nUVs != V2.m_nUVs ) continue;

                                    for( h=0; h < V.m_nUVs; h++)
                                    {
                                        if( V.m_UV[h].m_X != V2.m_UV[h].m_X ||
                                            V.m_UV[h].m_Y != V2.m_UV[h].m_Y ) break;                    
                                    }
                                    if( h != V.m_nUVs ) continue;


                                    // New vertex index
                                    lTriangle[j].m_iVertex[v] = w;
                                    break;
                                }                                    
                            }

                            if( w == lNewVertex.getCount() )
                            {
                                lTriangle[j].m_iVertex[v] = lNewVertex.getCount();
                                lNewVertex.append();
                                lNewVertex[ lTriangle[j].m_iVertex[v] ] = V;
                            }
                        }
                    }
                }
            }
        }
    }

    //
    // Set the resolt in our list
    //
    lCommand          = lOptimized;
    Section.m_lVertex = lNewVertex;

    //
    // Ouput the log
    //
    if ( s_bOutputLog )
    {
        Fp.Open( xfs("ArmOpLog-%d.txt",this), "at" );

        Fp.Printf( "\n\n" );
        Fp.Printf( "=============================================\n" );
        Fp.Printf( "Optimized Commands\n" );
        Fp.Printf( "=============================================\n" );

        for( s32 i=0; i<lCommand.getCount(); i++ )
        {
            command& Command = lCommand[i];

            switch( Command.m_Type )
            {
            case DRAW_LIST:     Fp.Printf( "DRAW_LIST    :  From (%d) to (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            case UPLOAD_MATRIX: Fp.Printf( "UPLOAD_MATRIX:  BoneID (%d) CacheID (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            case NOP_MATRIX:    Fp.Printf( "NOP_MATRIX   :  BoneID (%d) CacheID (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            }
        }

        Fp.Printf( "\n\n" );
    }
}

//=========================================================================

void arm_optimizer::ResetMatrixCache( void )
{
    for( s32 i=0; i<m_MatrixCacheSize; i++ )
    {
        m_pMatrixCache[i]       = -1;
        m_pMatrixCacheScore[i]  = -1;
    }
}

//=========================================================================

void arm_optimizer::ResetVertexCache( void )
{
    for( s32 i=0; i<m_VertCacheSize; i++ )
    {
        m_pVertexCache[i]       = -1;
        m_pVertexCacheScore[i]  = -1;
    }
}

//=========================================================================

void arm_optimizer::OpSection( section& Section ) const
{
    xarray<weight_list>     WList;
    list_triangle           OptimizeFacet;
    s32                     NTrisTotal  = Section.m_lTriangle.getCount();
    s32                     CurentCount = 0;
    s32                     BrauniPoints[3]={-1,-1,-1};
    xfile                   Fp;
    s32                     nVertsStalls  = 0;
    s32                     nMatrixStalls = 0;
    s32                     LastPolyFlush = 0;

    //
    // Something to help reduce the matrix stalls
    //
    s64* pMatrixAge = x_new( s64, m_MatrixCacheSize, 0);
    ASSERT( pMatrixAge );
    x_memset( pMatrixAge, 0, sizeof(s64)*m_MatrixCacheSize );

    //
    // Create the log file of the optimicer
    //
    if( s_bOutputLog )
        VERIFY( Fp.Open( xfs("ArmOpLog-%d.txt", this), "wt" ) );

    //
    // Create the weight list for each facet
    //
    {
        s32 nFacets = Section.m_lTriangle.getCount();
        for( s32 i=0; i<nFacets;i++)
        {
            triangle&       Facet = Section.m_lTriangle[i];
            WList.append();
            weight_list&    WL    = WList[ WList.getCount()-1 ];

            WL.m_nWeights = 0;
            for( s32 j=0; j<3;j++)
            {
                vertex& V = Section.m_lVertex[ Facet.m_iVertex[j] ];

                for( s32 w=0; w<V.m_nWeights; w++)
                {
                    s32 c;
                    for( c=0; c<WL.m_nWeights; c++ )
                    {
                        if( V.m_Weight[w].m_iBone == WL.m_Weight[c] ) break;
                    }

                    if( c == WL.m_nWeights )
                    {
                        WL.m_Weight[ WL.m_nWeights ] = V.m_Weight[w].m_iBone;
                        WL.m_nWeights++;
                    }
                }
            }

            if( WL.m_nWeights > m_MatrixCacheSize )
            {
                x_throw( "The matrix cache is too small.\n" );
            }
        }
    }

    //
    // Use the first facet in the list for the seek of the algorithum
    // Then Initialize all the caches and move the facet to the optimice grop
    //
    {
        s32 MatrixCount;
        s32 i;

        // Set all the matrices into the cache
        MatrixCount = 0;
        for( i=0; i<WList[0].m_nWeights; i++ )
        {
            m_pMatrixCache[ i ] = WList[0].m_Weight[i];
            Section.m_lCommand.append() = command( UPLOAD_MATRIX, m_pMatrixCache[ i ], i );
            MatrixCount++;
        }

        // invalidate the rest of the matrices
        for( i=MatrixCount; i<m_MatrixCacheSize; i++ )
        {
            m_pMatrixCache[i]      = -1;
            Section.m_lCommand.append() = command( NOP_MATRIX, -1, i );                            
        }

        // Copy all the verts into the cache
        for( i=0; i<3; i++ )
        {
            m_pVertexCache[i] =  Section.m_lTriangle[0].m_iVertex[i];
        }
        
        // invalidate the rest
        for( ; i<m_VertCacheSize; i++ )
        {
            m_pVertexCache[i] = -1 ;
        }

        // Move facet zero from the list
        OptimizeFacet.append() = Section.m_lTriangle[0];
        Section.m_lTriangle.DeleteWithCollapse( 0 );
        WList.DeleteWithCollapse( 0 );
    }

    //
    // Now we must find from our source list of facet which facet is best suted
    // base on what we already have in the cache
    //
    while( Section.m_lTriangle.getCount() )
    {
        s32 nTrisLeft       = Section.m_lTriangle.getCount();
        s32 iBestFacet      = 0;
        f32 BestScore       = -100000000.0f;
        s32 nWeightNotFound = 0;
        s32 nVertsFound     = 0;

        //
        // Reset the score of the verts and matrices
        //
        CurentCount++;
        for( s32 f=0; f< m_MatrixCacheSize; f++ )
        {
            m_pMatrixCacheScore[f] = -(m_pMatrixCache[f] == -1);
        }

        for( s32 f=0; f< m_VertCacheSize; f++ )
        {
            m_pVertexCacheScore[f] = -(m_pVertexCache[f] == -1);
        }

        if( s_bVeryVerbose && ((CurentCount%100) == 0) )
        {
            x_DebugMsg( "%d out of %d there are left %d\n", CurentCount, NTrisTotal, nTrisLeft );
        }

        //
        // Find the next best facet
        //
        for( s32 f=0; f< nTrisLeft; f++ )
        {
            s32             i,c=0,k;
            triangle&       Facet = Section.m_lTriangle[f];
            weight_list&    WL    = WList[f];
            f32             Score = 0;

            // get the score base on the matrix cache
            for( c=i=0; i<WL.m_nWeights; i++ )
            {
                s32 j;
                for( j=0; j<m_MatrixCacheSize; j++ )
                {
                    if( m_pMatrixCache[j] == WL.m_Weight[i] ) 
                    {
                        m_pMatrixCacheScore[j]++;
                        Score += 100;
                        break;
                    }
                }

                if( j == m_MatrixCacheSize )
                {
                    c++; 
                }
            }

            // For every matrix it miss we are going to punish it 
            Score -= c * 50;

            // IF we have found all the bones for this facet we like it
            // allot.
            if( c == 0 ) Score = 10000; 

            // Don't bather checking anything else if we alredy lost
            if( BestScore > (Score+3) ) continue;

            // Get score base on the vertex cache
            for( k=i=0; i<3; i++)
            {
                const s32 v = Facet.m_iVertex[i];

                for( s32 j=0; j<m_VertCacheSize; j++ )
                {
                    if( m_pVertexCache[j] == v ) 
                    {
                        m_pVertexCacheScore[j]++;
                        Score += 1;
                        k++;
                        break;
                    }
                }
            }

            // Give brauni points from keeping facets to jump all over the place
            if( k == 3 )
            {
                for( k=0; k<3; k++)
                for( s32 l=0; l<3; l++)
                {
                    if( BrauniPoints[k] == Facet.m_iVertex[l] )
                    {
                        Score += 1;
                        break;
                    }
                }
            }

            // Now chech if we got a winer so far
            if( Score > BestScore )
            {
                iBestFacet      = f;
                BestScore       = Score;
                nWeightNotFound = c;
                nVertsFound     = k;

                const s32 MaxScore = 10000 + 3*5*100 + 3;

                if( Score == MaxScore ) break;
            }
        }

        //
        // Check whether we have to invalidate any of the caches
        //
        {
            triangle&       Facet = Section.m_lTriangle[ iBestFacet ];
            weight_list&    WL    = WList[ iBestFacet ];

            //
            // Check whether we have to invalidate any of the matrices
            //
            if( nWeightNotFound )
            {
                //
                // Flush polys
                //
                if( OptimizeFacet.getCount() != LastPolyFlush )
                {
                    Section.m_lCommand.append() = command( DRAW_LIST, LastPolyFlush, OptimizeFacet.getCount() );                            
                    LastPolyFlush = OptimizeFacet.getCount();
                }

                //
                // The hardware probably invalidates the verts we should do the same
                //
                /*
                {
                    ResetVertexCache();
                    nVertsFound=0;
                }
                */

                //
                // Find the best score and upload the matrices
                //
                for( s32 v=0; v<WL.m_nWeights; v++)
                {
                    s32 i;
                    s32 iMatrix = 0;
                    BestScore   = 999999999.0f;

                    // Find the best score               
                    for( i=0; i<m_MatrixCacheSize; i++ )
                    {
                        if( WL.m_Weight[v] == m_pMatrixCache[i] ) break;

                        // We want to take the place of a matrix that most
                        // polygons agree that it sucks
                        if( m_pMatrixCacheScore[i] == BestScore )
                        {
                            if( pMatrixAge[i] > pMatrixAge[ iMatrix ] )
                            {
                                BestScore = (f32)m_pMatrixCacheScore[i];
                                iMatrix   = i;
                            }
                        }
                        else if( m_pMatrixCacheScore[i] < BestScore )
                        {
                            BestScore = (f32)m_pMatrixCacheScore[i];
                            iMatrix   = i;
                        }
                    }

                    // We found that matrix in the cache
                    if( i != m_MatrixCacheSize ) continue;

                    // Upload one of the matrixces that we need
                    ASSERT( m_pMatrixCacheScore[ iMatrix ] < (10000000-1) ); 
                    
                    if( m_pMatrixCacheScore[ iMatrix ] != -1 ) nMatrixStalls++;

                    // Add the command to add the matrices
                    Section.m_lCommand.append() = command( UPLOAD_MATRIX, WL.m_Weight[v], iMatrix );

                    // Add and update the score
                    m_pMatrixCache[ iMatrix ]         = WL.m_Weight[v];
                    m_pMatrixCacheScore[ iMatrix ]   += 10000000;
                    pMatrixAge[ iMatrix ]             = 0;
                }

                //
                // Set the Null commands for the matrices
                //
                {
                    for( s32 i=0; i<m_MatrixCacheSize; i++ )
                    {
                        if( m_pMatrixCacheScore[i] <= 0 )
                        {
                            Section.m_lCommand.append() = command( NOP_MATRIX, m_pMatrixCache[ i ], i );                            

                            // make older matrix suck more
                            pMatrixAge[ i ]++;
                        }
                    }
                }
            }

            //
            // Check whether we have to invalidate any of the verts
            //
            if( nVertsFound < 3 )
            {
                for( s32 v=0; v<3; v++ )
                {
                    s32 i;
                    s32 m_iVertex = 0;
                    BestScore   = 999999999.0f;

                    for( i=0; i<m_VertCacheSize; i++ )
                    {
                        // Make sure that is none of the matrices what we
                        // need for the new facet
                        if( Facet.m_iVertex[v] == m_pVertexCache[i] ) 
                        {
                            break;
                        }

                        // We want to take the place of a matrix that most
                        // polygons agree that it sucks
                        if( m_pVertexCacheScore[i] < BestScore )
                        {
                            BestScore = (f32)m_pVertexCacheScore[i];
                            m_iVertex   = i;
                        }
                    }

                    // We have found the vertex in the cache
                    if( i < m_VertCacheSize ) continue;
                    m_pVertexCache[ m_iVertex ]       = Facet.m_iVertex[v];

                    ASSERT( m_pVertexCacheScore[ m_iVertex ] < (10000000-1) );
                    if( m_pVertexCacheScore[ m_iVertex ] != -1 ) nVertsStalls++;
                    m_pVertexCacheScore[ m_iVertex ] += 10000000;
                }
            }
        }

        //
        // Do some sanity checks
        //
        {
            weight_list& WL = WList[ iBestFacet ];
//            triangle& Facet = Section.m_lTriangle[ iBestFacet ];
            for( s32 v=0; v<WL.m_nWeights; v++ )
            {
                s32 i;
                for( i=0;i<m_MatrixCacheSize; i++ )
                {
                    if( m_pMatrixCache[i] == WL.m_Weight[v] ) break; 
                }

                ASSERT( i < m_MatrixCacheSize );
            }
        }

        //
        // Do the log file
        //
        if ( s_bOutputLog )
        {
            //
            // Print the vertex and matrix caches
            //
            Fp.Printf( "MatrixCache[ " );
            for( s32 i=0;i<m_MatrixCacheSize; i++ )
            {
                Fp.Printf( "%4d ", m_pMatrixCache[i] );
            }
            Fp.Printf( "]\n" );

            Fp.Printf( "MatrixCacheScore[ " );
            for( s32 i=0;i<m_MatrixCacheSize; i++ )
            {
                if( m_pMatrixCacheScore[i] >= (10000000-1) )
                {
                    Fp.Printf( "#%2d# ", m_pMatrixCacheScore[i] - 10000000 );
                }
                else
                {
                    Fp.Printf( "%4d ", m_pMatrixCacheScore[i] );
                }
            }
            Fp.Printf( "]\n" );

            Fp.Printf( "VertCache[ " );
            for( s32 i=0;i<m_VertCacheSize; i++ )
            {
                Fp.Printf( "%4d ", m_pVertexCache[i] );
            }
            Fp.Printf( "]\n" );

            Fp.Printf( "VertCacheScore[ " );
            for( s32 i=0;i<m_VertCacheSize; i++ )
            {
                if( m_pVertexCacheScore[i] >= (10000000-1) )
                {
                    Fp.Printf( "X%2dX ", m_pVertexCacheScore[i] - 10000000 );
                }
                else
                {
                    Fp.Printf( "%4d ", m_pVertexCacheScore[i] );
                }
            }
            Fp.Printf( "]\n" );

            //
            // Print the vertex
            //
            {
                weight_list& WL = WList[ iBestFacet ];

//                triangle& Facet = Section.m_lTriangle[ iBestFacet ];

                Fp.Printf( "Vert[%4d %4d %4d] ",
                    Section.m_lTriangle[iBestFacet].m_iVertex[0],
                    Section.m_lTriangle[iBestFacet].m_iVertex[1],
                    Section.m_lTriangle[iBestFacet].m_iVertex[2] );

                Fp.Printf( "Matrix[" );
                for( s32 v=0; v<WL.m_nWeights; v++ )
                {
                    Fp.Printf( " %4d",WL.m_Weight[v] ); 
                }
                Fp.Printf( " ]\n\n\n" );
            }
        }

        //
        // Set the brauni point hehehe...
        //
        BrauniPoints[0] = Section.m_lTriangle[iBestFacet].m_iVertex[0];
        BrauniPoints[1] = Section.m_lTriangle[iBestFacet].m_iVertex[1];
        BrauniPoints[2] = Section.m_lTriangle[iBestFacet].m_iVertex[2];

        //
        // we can remove the facet from the source list to the dest list
        //
        OptimizeFacet.append() = Section.m_lTriangle[iBestFacet];
        Section.m_lTriangle.DeleteWithCollapse( iBestFacet );
        WList.DeleteWithCollapse( iBestFacet );
    }

    //
    // Flush what ever remaining polygos
    //
    if( OptimizeFacet.getCount() != LastPolyFlush )
    {
        Section.m_lCommand.append() = command( DRAW_LIST, LastPolyFlush, OptimizeFacet.getCount() );                            
        LastPolyFlush = OptimizeFacet.getCount();
    }

    //
    // Copy to the section all the facets
    //
    Section.m_lTriangle = OptimizeFacet;

    //
    // Delete the matrix age
    //
    x_delete( pMatrixAge );

    //
    // Printout the commands
    //
    if ( s_bOutputLog )
    {
        s32 nComands = Section.m_lCommand.getCount();

        Fp.Printf( "\n\n" );
        Fp.Printf(  "=============================================\n" );
        Fp.Printf(  "Preoptimized Commands\n" );
        Fp.Printf(  "=============================================\n" );

        for( s32 i=0; i<nComands; i++ )
        {
            command& Command = Section.m_lCommand[i];

            switch( Command.m_Type )
            {
            case DRAW_LIST:     Fp.Printf(  "DRAW_LIST    :  From (%d) to (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            case UPLOAD_MATRIX: Fp.Printf( "UPLOAD_MATRIX:  BoneID (%d) CacheID (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            case NOP_MATRIX:    Fp.Printf(  "NOP_MATRIX   :  BoneID (%d) CacheID (%d) \n", Command.m_Arg1, Command.m_Arg2 ); 
                break;
            }
        }
        Fp.Printf(  "\n\n" );

        //
        // Close the log file
        //
        Fp.Printf(  "\n\n------------------------------------------\n");
        Fp.Printf(  "NVerts  Stalls per triangle: %f\n", nVertsStalls/(f32)OptimizeFacet.getCount() );
        Fp.Printf(  "NMatrix Stalls: %d\n", nMatrixStalls );
        Fp.Printf(  "------------------------------------------\n");
    }
}

//=========================================================================

void arm_optimizer::Build( const rawgeom& RawGeom, const s32 SrcFaceOffset, const s32 SrcFaceCount, s32 MatrixCacheSize, s32 VertexCacheSize )
{
    //
    // Set the optimize parameters
    //
    m_MatrixCacheSize   = MatrixCacheSize;
    m_VertCacheSize     = VertexCacheSize;
    m_MaxVertsSection   = 100000;

    for( s32 i=0; i<SrcFaceCount; i++ )
    {
        m_Section.m_lTriangle.append() = RawGeom.m_Facet[ SrcFaceOffset + i ];
    }

    //
    // Copy each vertex and fix up the index
    //
    
    if( s_bVeryVerbose )
        x_DebugMsg("Coping Vertex into the right section...\n");

    list_vertex&    lVert   = m_Section.m_lVertex;
    list_triangle&  lTri    = m_Section.m_lTriangle;
    s32             nTris   = lTri.getCount();
    
    for( s32 j=0; j<nTris; j++ )
    {
        triangle& Triangle = lTri[j];

        if( s_bVeryVerbose && ((j%1000)==0) ) 
            x_DebugMsg("Facet %d of %d\n", j,nTris );  
        
        for( s32 k=0; k<3; k++ )
        {
            const vertex&   Vertex = RawGeom.m_Vertex[ Triangle.m_iVertex[k] ];
            s32             nVerts = lVert.getCount();
            s32             l;

            // Linear search for the particular vertex
            for( l=0; l<nVerts; l++ )
            {
                // Do we have this vertex already?         
                if( x_memcmp( &Vertex, &lVert[l], sizeof(Vertex) ) == 0 ) //RawGeom.TempVCompare(  ) )
                    break;
            }

            // if we haven't found it the add it
            if( l == nVerts ) 
            {
                l              = lVert.getCount();
                lVert.append() = Vertex;
            }

            // Set the index for this facet
            Triangle.m_iVertex[k] = l;
        }
    }

    //
    // Allocate all the cache system 
    //
    m_pMatrixCache        = x_new( s32, m_MatrixCacheSize, 0 );
    ASSERT( m_pMatrixCache );

    m_pMatrixCacheScore   = x_new( s32, m_MatrixCacheSize, 0 );
    ASSERT( m_pMatrixCacheScore );

    m_pVertexCache        = x_new( s32, m_VertCacheSize, 0 );
    ASSERT( m_pVertexCache );

    m_pVertexCacheScore   = x_new( s32, m_VertCacheSize, 0 );
    ASSERT( m_pVertexCacheScore );

    //
    // Create the file
    //
    if( s_bOutputLog )
    { 
        xfile Fp; 
        Fp.Open( xfs("ArmOpLog-%d.txt",this), "wt" ); 
    }

    s32 FinalVertexCount=0;
    s32 FinalFacetCount=0;

    OpSection( m_Section );
    OpCommands( m_Section );

    FinalVertexCount += m_Section.m_lVertex.getCount();
    FinalFacetCount  += m_Section.m_lTriangle.getCount();

    //
    // Print some more stats
    //
    if ( s_bOutputLog )
    {
        xfile Fp;
        Fp.Open( xfs("ArmOpLog-%d.txt",this), "at" );

        Fp.Printf( "Original Vertex Count: %d\n", RawGeom.m_Vertex.getCount() );
        Fp.Printf( "Final Vertex Count   : %d\n", FinalVertexCount );

        Fp.Printf( "Original Facet Count: %d\n", RawGeom.m_Facet.getCount() );
        Fp.Printf( "Final Facet Count   : %d\n", FinalFacetCount );
    }


    //
    // Clean up
    //
    x_delete( m_pMatrixCache )     ;
    x_delete( m_pMatrixCacheScore );
    x_delete( m_pVertexCache )     ;
    x_delete( m_pVertexCacheScore );
}
