#ifndef ARM_OPTIMIZER_H
#define ARM_OPTIMIZER_H

///////////////////////////////////////////////////////////////////////////
// INCLUDES
///////////////////////////////////////////////////////////////////////////

#include "rawgeom.h"

///////////////////////////////////////////////////////////////////////////
// TYPES
///////////////////////////////////////////////////////////////////////////

struct arm_optimizer
{
///////////////////////////////////////////////////////////////////////////
                   
    enum commnad_type
    {
        DRAW_LIST,       // Arg1 = Start Face,  Arg2 = End Face
        UPLOAD_MATRIX,   // Arg1 = BoneID, Arg2 = CacheID
        NOP_MATRIX       // Arg1 = BoneID, Arg2 = CacheID
    };

    struct command
    {
        inline command( void ){}
        inline command( commnad_type T, s32 a1, s32 a2 ) { m_Type = T; m_Arg1 = a1; m_Arg2 = a2; }

        commnad_type m_Type;
        s32          m_Arg1;
        s32          m_Arg2;
    };

    typedef rawgeom::vertex             vertex;
    typedef rawgeom::weight             weight;
    typedef rawgeom::facet              triangle;
    typedef xarray<vertex>              list_vertex;
    typedef xarray<triangle>            list_triangle;
    typedef xarray<command>             list_command;

    struct section
    {
        list_vertex         m_lVertex;
        list_triangle       m_lTriangle;
        list_command        m_lCommand;
    };

///////////////////////////////////////////////////////////////////////////

    void Build              ( const rawgeom& RawGeom, const s32 SrcFaceOffset, const s32 SrcFaceCount,  s32 MatrixCacheSize = 64, s32 VertexCacheSize = 17 );
    void OpSection          ( section& Section ) const;
    void OpCommands         ( section& Section ) const;
    void OptimizeUploads    ( section& Section ) const;
    void ResetMatrixCache   ( void );
    void ResetVertexCache   ( void );

///////////////////////////////////////////////////////////////////////////

    section                 m_Section;
    s32                     m_MatrixCacheSize;
    s32                     m_VertCacheSize;
    s32                     m_MaxVertsSection;
    s32*                    m_pMatrixCache;
    s32*                    m_pMatrixCacheScore;
    s32*                    m_pVertexCache;
    s32*                    m_pVertexCacheScore;
};

///////////////////////////////////////////////////////////////////////////
// END
///////////////////////////////////////////////////////////////////////////
#endif 