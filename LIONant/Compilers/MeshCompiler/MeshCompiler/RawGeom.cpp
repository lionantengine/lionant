
#include "RawGeom.h"

//=========================================================================
// FUNCTIONS
//=========================================================================

//#define RMESH_USE_SANITY

#ifdef RMESH_USE_SANITY
    #define RMESH_SANITY    x_MemSanity();
#else
    #define RMESH_SANITY
#endif


//=========================================================================
// PROTOTYPES
//=========================================================================

//=========================================================================

void rawgeom::Kill( void )
{
}

//=========================================================================

xbool rawgeom::Load( const char* pFileName )
{
    Kill( );

    //  First, we need to crack the file open, and
    //  figure out which version it is.
    xtextfile   File;

    // Open the file
    File.OpenForReading( xstring::BuildFromFormat( pFileName ) );

    while ( File.ReadRecord() == TRUE )
    {
        if ( x_stricmp( File.GetRecordName(), "Hierarchy" ) == 0 )
        {
            // Allocate the bone count
            m_Bone.New( File.GetRecordCount( ), XMEM_FLAG_ALIGN_16B );

            // Read each of the fields in the file
            for ( s32 i = 0; i < m_Bone.getCount(); i++ )
            {
                bone& Bone = m_Bone[ i ];
                s32   Index;

                File.ReadLine();

                File.ReadField( "Index:d", &Index );
                File.ReadFieldXString( "Name:s", Bone.m_Name );
                File.ReadField( "nChildren:d", &Bone.m_nChildren );
                File.ReadField( "iParent:d", &Bone.m_iParent );
                File.ReadField( "Scale:fff", &Bone.m_Scale.m_X, &Bone.m_Scale.m_Y, &Bone.m_Scale.m_Z );
                File.ReadField( "Rotate:ffff", &Bone.m_Rotation.m_X, &Bone.m_Rotation.m_Y, &Bone.m_Rotation.m_Z, &Bone.m_Rotation.m_W );
                File.ReadField( "Pos:fff", &Bone.m_Position.m_X, &Bone.m_Position.m_Y, &Bone.m_Position.m_Z );
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "InformedMaterials" ) == 0 )
        {
            // Allocate the bone count
            m_InformedMaterial.New( File.GetRecordCount( ), XMEM_FLAG_ALIGN_16B );

            for ( informed_material& Material : m_InformedMaterial )
            {
                s32   Index;
                File.ReadLine();
                File.ReadField( "Index:d", &Index );
                File.ReadFieldXString( "Name:s", Material.m_Name );
                File.ReadFieldXString( "Shader:s", Material.m_MaterialShader );
                File.ReadFieldXString( "Technique:s", Material.m_Technique );
            }
        }
        else if ( x_stricmp( File.GetRecordName( ), "InformedMaterialParams" ) == 0 )
        {
            for ( s32 i = 0; i < File.GetRecordCount(); i++ )
            {
                s32  iMaterial;
                s32  iParam;

                File.ReadLine( );
                File.ReadField( "iInformed:d", &iMaterial );
                File.ReadField( "iParam:d", &iParam );

                informed_material&           Material    = m_InformedMaterial[ iMaterial ];
                informed_material::params&   Param       = Material.m_Params.append();
                char                         TypeEnum[ 256 ];

                File.ReadField( "Name:s", &Param.m_Name[0] );
                File.ReadField( "Type:e", &TypeEnum[0] );
                File.ReadField( "Value:s", &Param.m_Value[0] );

                for ( s32 k = informed_material::PARAM_TYPE_NULL; k <= informed_material::PARAM_TYPE_TEXTURE; k++ )
                {
                    if ( x_strcmp( informed_material::getTypeString( informed_material::params_type( k ) ), TypeEnum ) == 0 )
                    {
                        Param.m_Type = informed_material::params_type( k );
                        break;
                    }
                }
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "Vertices" ) == 0 )
        {
            // Allocate the vertex count
            m_Vertex.Alloc( File.GetRecordCount( ), XMEM_FLAG_ALIGN_16B );
            m_Vertex.SetMemory( 0 );

            // Read each of the fields in the file
            for ( s32 i = 0; i < m_Vertex.getCount(); i++ )
            {
                vertex& Vertex = m_Vertex[ i ];
                s32     Index;

                File.ReadLine();

                File.ReadField( "Index:d", &Index );
                File.ReadField( "Pos:fff", &Vertex.m_Position.m_X, &Vertex.m_Position.m_Y, &Vertex.m_Position.m_Z );
                File.ReadField( "nBinormals:d", &Vertex.m_nBinormals );
                File.ReadField( "nTangents:d", &Vertex.m_nTangents );
                File.ReadField( "nNormals:d", &Vertex.m_nNormals );
                File.ReadField( "nUVSets:d", &Vertex.m_nUVs );
                File.ReadField( "nColors:d", &Vertex.m_nColors );
                File.ReadField( "nWeights:d", &Vertex.m_nWeights );
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "Colors" ) == 0 )
        {
            // Allocate the vertex count
            const s32 nColors = File.GetRecordCount();

            // Read each of the fields in the file
            for ( s32 i = 0; i < nColors; i++ )
            {
                s32     iVertex, SubIndex;
                f32     R, G, B, A;

                File.ReadLine();

                File.ReadField( "iVertex:d", &iVertex );
                vertex& Vertex = m_Vertex[ iVertex ];

                File.ReadField( "Index:d", &SubIndex );
                xcolor& C = Vertex.m_Color[ SubIndex ];

                File.ReadField( "Color:ffff", &R, &G, &B, &A );
                C.SetFromRGBA( R, G, B, A );
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "BTNs" ) == 0 )
        {
            const s32   nNormals = File.GetRecordCount();

            for ( s32 i = 0; i < nNormals; i++ )
            {
                s32 Index, SubIndex;

                File.ReadLine();

                File.ReadField( "iVertex:d", &Index );
                File.ReadField( "Index:d", &SubIndex );

                vertex& Vertex = m_Vertex[ Index ];

                if ( SubIndex < VERTEX_MAX_NORMAL )
                {
                    s32 bReaded=0;
                    bReaded += File.ReadField( "Binormals:fff",
                        &Vertex.m_BTN[ SubIndex ].m_Binormal.m_X,
                        &Vertex.m_BTN[ SubIndex ].m_Binormal.m_Y,
                        &Vertex.m_BTN[ SubIndex ].m_Binormal.m_Z );

                    bReaded += File.ReadField( "Tangents:fff",
                        &Vertex.m_BTN[ SubIndex ].m_Tangent.m_X,
                        &Vertex.m_BTN[ SubIndex ].m_Tangent.m_Y,
                        &Vertex.m_BTN[ SubIndex ].m_Tangent.m_Z );

                    bReaded += File.ReadField( "Normals:fff",
                        &Vertex.m_BTN[ SubIndex ].m_Normal.m_X,
                        &Vertex.m_BTN[ SubIndex ].m_Normal.m_Y,
                        &Vertex.m_BTN[ SubIndex ].m_Normal.m_Z );

                    ASSERT( bReaded > 1 );
                }
                else
                {
                    x_LogWarning( "rawgeom", "Mesh has too many Normals" );
                }
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "UVs" ) == 0 )
        {
            const s32 nUVs = File.GetRecordCount();

            for ( s32 i = 0; i < nUVs; i++ )
            {
                s32 Index, SubIndex;

                File.ReadLine();

                File.ReadField( "iVertex:d", &Index );
                File.ReadField( "Index:d", &SubIndex );

                vertex& Vertex = m_Vertex[ Index ];

                if ( SubIndex < VERTEX_MAX_UV )
                {
                    File.ReadField( "UV:ff",
                        &Vertex.m_UV[ SubIndex ].m_X,
                        &Vertex.m_UV[ SubIndex ].m_Y );
                }
                else
                {
                    x_LogWarning( "rawgeom", "Mesh has too many UVs" );
                }
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "Skin" ) == 0 )
        {
            const s32 nSkin = File.GetRecordCount();

            for ( s32 i = 0; i < nSkin; i++ )
            {
                s32     Index, SubIndex;

                File.ReadLine();

                File.ReadField( "iVertex:d", &Index );
                File.ReadField( "Index:d", &SubIndex );

                vertex& Vertex = m_Vertex[ Index ];

                if ( SubIndex < VERTEX_MAX_WEIGHT )
                {
                    File.ReadField( "iBone:d", &Vertex.m_Weight[ SubIndex ].m_iBone );
                    File.ReadField( "Weight:f", &Vertex.m_Weight[ SubIndex ].m_Weight );
                }
                else
                {
                    x_LogWarning( "rawgeom", "Mesh has too many weights" );
                }
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "Polygons" ) == 0 )
        {
            m_Facet.Alloc( File.GetRecordCount( ), XMEM_FLAG_ALIGN_16B );
            m_Facet.SetMemory( 0 );

            // Read each of the fields in the file
            for ( facet& Facet : m_Facet )
            {
                File.ReadLine();

                File.ReadField( "iMesh:d", &Facet.m_iMesh );
                File.ReadField( "nVerts:d", &Facet.m_nVertices );
                File.ReadField( "Plane:ffff", &Facet.m_Plane.m_Normal.m_X, &Facet.m_Plane.m_Normal.m_Y, &Facet.m_Plane.m_Normal.m_Z, &Facet.m_Plane.m_D );
                File.ReadField( "iMaterial:d", &Facet.m_iInformed );
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "FacetIndex" ) == 0 )
        {
            const s32 nIFacets = File.GetRecordCount();

            for ( s32 i = 0; i < nIFacets; i++ )
            {
                s32     Index, SubIndex;

                File.ReadLine();

                File.ReadField( "iFacet:d", &Index );
                File.ReadField( "Index:d", &SubIndex );

                facet& Facet = m_Facet[ Index ];

                File.ReadField( "iVertex:d", &Facet.m_iVertex[ SubIndex ] );
            }
        }
        else if ( x_stricmp( File.GetRecordName(), "Mesh" ) == 0 )
        {
            m_Mesh.New( File.GetRecordCount( ), XMEM_FLAG_ALIGN_16B );

            for ( mesh& Mesh : m_Mesh )
            {
                File.ReadLine();
                File.ReadFieldXString( "Name:s", Mesh.m_Name );
            }

            // Rename duplicated names if we found any
            for ( mesh& Mesh : m_Mesh )
            {
                s32 Count = 0;
                for ( s32 j = 1 + (s32)(&Mesh - &m_Mesh[ 0 ]); j < m_Mesh.getCount( ); j++ )
                {
                    if ( x_stricmp( Mesh.m_Name, m_Mesh[ j ].m_Name ) == 0 )
                    {
                        m_Mesh[ j ].m_Name.AppendFormat( "__%d", Count++ );
                    }
                }
            }
        }
    }
        
    // Compute all bone related info
    ComputeBoneInfo() ;
    return TRUE;
}


//=========================================================================

void rawgeom::Save( const char* pFileName ) const
{
    s32         nColors     = 0;
    s32         nNormals    = 0;
    s32         nUVs        = 0;
    s32         nWeights    = 0;
    s32         nFIndices   = 0;
    s32         nMatParams  = 0;
    s32         i;
    xtextfile   File;

    ASSERT( pFileName );

    x_try;

    File.OpenForWriting( xstring::BuildFromFormat( pFileName ) );

    x_catch_append( xfs( "Unable to open the file %s, for saving", pFileName ) );


    File.WriteRecord( "Hierarchy", m_Bone.getCount() );
    for ( i = 0; i<m_Bone.getCount( ); i++ )
    {
        const bone& Bone = m_Bone[ i ];

        File.WriteField( "Index:d", i );
        File.WriteField( "Name:s", (const char*)Bone.m_Name );
        File.WriteField( "nChildren:d", Bone.m_nChildren );
        File.WriteField( "iParent:d", Bone.m_iParent );
        File.WriteField( "Scale:fff", Bone.m_Scale.GetX( ), Bone.m_Scale.GetY( ), Bone.m_Scale.GetZ( ) );
        File.WriteField( "Rotate:ffff", Bone.m_Rotation.m_X, Bone.m_Rotation.m_Y, Bone.m_Rotation.m_Z, Bone.m_Rotation.m_W );
        File.WriteField( "Pos:fff", Bone.m_Position.GetX( ), Bone.m_Position.GetY( ), Bone.m_Position.GetZ( ) );
        File.WriteLine( );
    }

    File.WriteRecord( "Mesh", m_Mesh.getCount( ) );
    for ( i = 0; i<m_Mesh.getCount( ); i++ )
    {
        const mesh& Mesh = m_Mesh[ i ];

        File.WriteField( "Index:d", i );
        File.WriteField( "Name:s", (const char*)Mesh.m_Name );
        File.WriteLine( );
    }

    File.WriteRecord( "InformedMaterials", m_InformedMaterial.getCount( ) );
    for ( const informed_material& Material : m_InformedMaterial )
    {
        File.WriteField( "Index:d", s32( &Material - &m_InformedMaterial[ 0 ] ) );
        File.WriteField( "Name:s", (const char*)Material.m_Name );
        File.WriteField( "Shader:s", (const char*)Material.m_MaterialShader );
        File.WriteField( "Technique:s", (const char*)Material.m_Technique );
        File.WriteField( "nParameters:d", Material.m_Params.getCount( ) );
        File.WriteLine( );

        nMatParams += Material.m_Params.getCount( );

        /*
        File.AddField( "IlluminationType:s", IlluminationType );
        File.AddField( "CompositionType:s", CompositionType );
        File.AddField( "nTexMaterials:d", Material.nTexMaterials );
        File.AddField( "bDoubleSide:d", Material.bDoubleSide );
        File.AddField( "nConstants:d", Material.nConstans );
        */
    }

    File.WriteRecord( "InformedMaterialParams", nMatParams );
    for ( const informed_material& Material : m_InformedMaterial )
    for ( const informed_material::params& Param : Material.m_Params )
    {
        File.WriteField( "iInformed:d", s32( &Material - &m_InformedMaterial[ 0 ] ) );
        File.WriteField( "iParam:d", s32( &Param - &Material.m_Params[ 0 ] ) );
        File.WriteField( "Name:s", (const char*)Param.m_Name );
        File.WriteField( "Type:e", informed_material::getTypeString( Param.m_Type ) );
        File.WriteField( "Value:s", (const char*)Param.m_Value );
        File.WriteLine( );
    }

    File.WriteRecord( "Vertices", m_Vertex.getCount() );
    for ( i = 0; i<m_Vertex.getCount( ); i++ )
    {
        const vertex& Vertex = m_Vertex[ i ];

        File.WriteField( "Index:d", i );
        File.WriteField( "Pos:fff", Vertex.m_Position.GetX( ), Vertex.m_Position.GetY( ), Vertex.m_Position.GetZ( ) );
        File.WriteField( "nBinormals:d", Vertex.m_nBinormals );
        File.WriteField( "nTangents:d", Vertex.m_nTangents );
        File.WriteField( "nNormals:d", Vertex.m_nNormals );
        File.WriteField( "nUVSets:d", Vertex.m_nUVs );
        File.WriteField( "nColors:d", Vertex.m_nColors );
        File.WriteField( "nWeights:d", Vertex.m_nWeights );
        File.WriteLine( );

        nColors     += Vertex.m_nColors;
        nNormals    += Vertex.m_nNormals;
        nUVs        += Vertex.m_nUVs;
        nWeights    += Vertex.m_nWeights;
    }

    File.WriteRecord( "Colors", nColors );
    for ( i = 0; i<m_Vertex.getCount( ); i++ )
    {
        const vertex& V = m_Vertex[ i ];
        for ( s32 j = 0; j<V.m_nColors; j++ )
        {
            const xcolor& C = V.m_Color[ j ];
            const f32     R = C.m_R * ( 1 / 255.0f );
            const f32     G = C.m_G * ( 1 / 255.0f );
            const f32     B = C.m_B * ( 1 / 255.0f );
            const f32     A = C.m_A * ( 1 / 255.0f );

            File.WriteField( "iVertex:d", i );
            File.WriteField( "Index:d", j );
            File.WriteField( "Color:ffff", R, G, B, A );
            File.WriteLine( );
        }
    }

    File.WriteRecord( "BTNs", nNormals );
    for ( i = 0; i<m_Vertex.getCount( ); i++ )
    {
        const vertex& V   = m_Vertex[ i ];
        const s32     Max = x_Max( V.m_nTangents, x_Max( V.m_nTangents, V.m_nNormals ) );
        for ( s32 j = 0; j<Max; j++ )
        {
            const btn& BTN = V.m_BTN[ j ];

            File.WriteField( "iVertex:d", i );
            File.WriteField( "Index:d", j );
            File.WriteField( "Binormals:fff", BTN.m_Binormal.GetX( ), BTN.m_Binormal.GetY( ), BTN.m_Binormal.GetZ( ) );
            File.WriteField( "Tangents:fff", BTN.m_Tangent.GetX( ), BTN.m_Tangent.GetY( ), BTN.m_Tangent.GetZ( ) );
            File.WriteField( "Normals:fff", BTN.m_Normal.GetX( ), BTN.m_Normal.GetY( ), BTN.m_Normal.GetZ( ) );
            File.WriteLine( );
        }
    }

    File.WriteRecord( "UVs", nUVs );
    for ( i = 0; i<m_Vertex.getCount( ); i++ )
    {
        const vertex& V = m_Vertex[ i ];
        for ( s32 j = 0; j<V.m_nUVs; j++ )
        {
            const xvector2 UV = V.m_UV[ j ];

            File.WriteField( "iVertex:d", i );
            File.WriteField( "Index:d", j );
            File.WriteField( "UV:ff", UV.m_X, UV.m_Y );
            File.WriteLine( );
        }
    }

    File.WriteRecord( "Skin", nWeights );
    for ( i = 0; i<m_Vertex.getCount( ); i++ )
    {
        const vertex& V = m_Vertex[ i ];
        for ( s32 j = 0; j<V.m_nWeights; j++ )
        {
            const weight W = V.m_Weight[ j ];

            File.WriteField( "iVertex:d", i );
            File.WriteField( "Index:d", j );
            File.WriteField( "iBone:d", W.m_iBone );
            File.WriteField( "Weight:f", W.m_Weight );
            File.WriteLine( );
        }
    }

    File.WriteRecord( "Polygons", m_Facet.getCount() );
    for ( i = 0; i<m_Facet.getCount( ); i++ )
    {
        const facet&  Facet = m_Facet[ i ];

        File.WriteField( "Index:d", i );
        File.WriteField( "iMesh:d", Facet.m_iMesh );
        File.WriteField( "nVerts:d", Facet.m_nVertices );
        File.WriteField( "Plane:ffff", Facet.m_Plane.m_Normal.GetX( ), Facet.m_Plane.m_Normal.GetY( ), Facet.m_Plane.m_Normal.GetZ( ), Facet.m_Plane.m_D );
        File.WriteField( "iInformed:d", Facet.m_iInformed );
        File.WriteLine( );

        nFIndices += Facet.m_nVertices;
    }

    File.WriteRecord( "FacetIndex", nFIndices );
    for ( i = 0; i<m_Facet.getCount( ); i++ )
    {
        const facet& F = m_Facet[ i ];

        for ( s32 j = 0; j<F.m_nVertices; j++ )
        {
            const s32 Index = F.m_iVertex[ j ];

            File.WriteField( "iFacet:d", i );
            File.WriteField( "Index:d", j );
            File.WriteField( "iVertex:d", Index );
            File.WriteLine( );
        }
    }

    /*
    File.AddHeader( "UVAnimation", m_nUVWAnimFrames );
    for ( i = 0; i<m_nUVWAnimFrames; i++ )
    {
        uvanim_frame& Frame = m_pUVWAnimFrame[ i ];

        File.AddField( "iAnimID:d", Frame.ID );
        File.AddField( "Index:d", Frame.iFrame );
        File.AddField( "Scale:fff", Frame.Scale.GetX( ), Frame.Scale.GetY( ), Frame.Scale.GetZ( ) );
        File.AddField( "Rotate:fff", Frame.Rotation.Roll, Frame.Rotation.Pitch, Frame.Rotation.Yaw );
        File.AddField( "Position:fff", Frame.Position.GetX( ), Frame.Position.GetY( ), Frame.Position.GetZ( ) );
        File.AddEndLine( );
    }
    */

    // Done!
    File.Close( );
}

//=========================================================================

xbbox rawgeom::getBBox( void )
{
    xbbox BBox;
    BBox.Zero();
    for( s32 i=0; i<m_Vertex.getCount(); i++ )
    {
        BBox += m_Vertex[i].m_Position;
    }
    return BBox;
}

//=========================================================================

void rawgeom::SortFacetsByMaterial( void )
{
    x_qsort( &m_Facet[0], m_Facet.getCount(), sizeof(facet),
            [](const void* paA, const void* paB ) -> s32
            {
                const rawgeom::facet* pA = (const rawgeom::facet*)paA;
                const rawgeom::facet* pB = (const rawgeom::facet*)paB;
                
                if( pA->m_iMesh < pB->m_iMesh ) return -1;
                if( pA->m_iMesh > pB->m_iMesh ) return  1;
                
                if( pA->m_iInformed < pB->m_iInformed ) return -1;
                return pA->m_iInformed > pB->m_iInformed;
            });
}

//=========================================================================

void rawgeom::SortFacetsByMeshMaterialBone( void )
{
   static rawgeom* g_pCompare;
    
   g_pCompare = this;
    
   x_qsort( &m_Facet[0], m_Facet.getCount(), sizeof(facet),
            [](const void* paA, const void* paB ) -> s32
            {
                const rawgeom::facet* pA = (const rawgeom::facet*)paA;
                const rawgeom::facet* pB = (const rawgeom::facet*)paB;
                
                if( pA->m_iMesh < pB->m_iMesh ) return -1;
                if( pA->m_iMesh > pB->m_iMesh ) return  1;
                
                if( pA->m_iInformed < pB->m_iInformed ) return -1;
                
                if( pA->m_iInformed > pB->m_iInformed ) return 1;
                
                if( pA->m_iInformed == pB->m_iInformed )
                {
                    if( g_pCompare->m_Vertex[pA->m_iVertex[0]].m_Weight[0].m_iBone >
                        g_pCompare->m_Vertex[pB->m_iVertex[0]].m_Weight[0].m_iBone )
                        return 1;
                    
                    if( g_pCompare->m_Vertex[pA->m_iVertex[0]].m_Weight[0].m_iBone <
                        g_pCompare->m_Vertex[pB->m_iVertex[0]].m_Weight[0].m_iBone )
                        return -1;
                }
                return 0;
            });
}

//=========================================================================

xbool rawgeom::TempVCompare( const vertex& A, const vertex& B )
{
    s32 i;

    // Check position first
    {
        static const f32 PEpsilon = 0.001f; 
        xvector3 T;
        T = A.m_Position - B.m_Position;
        f32 d = T.Dot( T );
        if( d > PEpsilon ) return FALSE;
    }
    
    if( A.m_nWeights != B.m_nWeights ) return FALSE;
    for( i=0; i<A.m_nWeights; i++ )
    {
        static const f32 WEpsilon = 0.001f;
        f32 d = (A.m_Weight[i].m_Weight*A.m_Weight[i].m_Weight) - (B.m_Weight[i].m_Weight*B.m_Weight[i].m_Weight);
        if( d > WEpsilon ) return FALSE;
        if( A.m_Weight[i].m_iBone != B.m_Weight[i].m_iBone ) return FALSE;
    }

    if( A.m_nNormals != B.m_nNormals ) return FALSE;
    for( i=0; i<A.m_nNormals; i++ )
    {
        static const f32 NEpsilon = 0.001f;
        xvector3 T;
        T = B.m_BTN[ i ].m_Normal - A.m_BTN[ i ].m_Normal;
        f32 d = T.Dot( T );
        if( d > NEpsilon ) return FALSE;
    }

    if( A.m_nTangents != B.m_nTangents ) return FALSE;
    for( i=0; i<A.m_nTangents; i++ )
    {
        static const f32 NEpsilon = 0.001f;
        xvector3 T;
        T = B.m_BTN[ i ].m_Tangent - A.m_BTN[ i ].m_Tangent;
        f32 d = T.Dot( T );
        if( d > NEpsilon ) return FALSE;
    }

    if( A.m_nBinormals != B.m_nBinormals ) return FALSE;
    for( i=0; i<A.m_nBinormals; i++ )
    {
        static const f32 NEpsilon = 0.001f;
        xvector3 T;
        T = B.m_BTN[ i ].m_Binormal - A.m_BTN[ i ].m_Binormal;
        f32 d = T.Dot( T );
        if( d > NEpsilon ) return FALSE;
    }

    if( A.m_nUVs != B.m_nUVs ) return FALSE;
    for( i=0; i<A.m_nUVs; i++ )
    {
        static const f32 UVEpsilon = 0.001f;
        xvector2 T;
        T = B.m_UV[i] - A.m_UV[i];
        f32 d = T.Dot( T );
        if( d > UVEpsilon ) return FALSE;
    }

    if( A.m_nColors != B.m_nColors ) return FALSE;
    for( i=0; i<A.m_nColors; i++ )
    {
        static const f32 CEpsilon = 3.0f;
        xvector4 C1( A.m_Color[i].m_R, A.m_Color[i].m_G, A.m_Color[i].m_B, A.m_Color[i].m_A );
        xvector4 C2( B.m_Color[i].m_R, B.m_Color[i].m_G, B.m_Color[i].m_B, B.m_Color[i].m_A );

        xvector4 T = C1 - C2;
        f32 d = T.Dot( T );
        if( d > CEpsilon ) return FALSE;
    }

    return TRUE;
}

//=========================================================================

xbool rawgeom::CompareFaces( const rawgeom::facet& A, const rawgeom::facet& B )
{
    s32 i;

    if( A.m_iMesh     != B.m_iMesh     ) return FALSE;
    if( A.m_nVertices != B.m_nVertices ) return FALSE;
    if( A.m_iInformed != B.m_iInformed ) return FALSE;

    for( i=0; i<A.m_nVertices; i++ )
    {
        if( A.m_iVertex[i] == B.m_iVertex[0] )
            break;
    }
    if( i == A.m_nVertices ) return FALSE;

    s32 Index = i;
    for( i=0; i<B.m_nVertices; i++ )
    {
        if( B.m_iVertex[i] != A.m_iVertex[(i+Index)%A.m_nVertices] )
            return FALSE;
    }

    return TRUE;
}

//=========================================================================

void rawgeom::CleanMesh( s32 iMesh /* = -1 */ ) // Remove this Mesh
{
    s32 TotalFacetsRemoved = 0;
    s32 TotalVerticesRemoved = 0;
    s32 TotalMaterialsRemoved = 0;

    RMESH_SANITY

    //
    // Make sure that all normals are normalied
    //
    {
        for ( s32 i = 0; i < m_Vertex.getCount(); i++ )
        {
            auto& Vertex = m_Vertex[i];

            if( Vertex.m_nBinormals != Vertex.m_nTangents )
                x_throw("ERROR: The mesh has a different number of Binormals To Tangents");

            if( Vertex.m_nNormals < Vertex.m_nBinormals )
                x_throw( "ERROR: We have more Binormals than Normals" );

            for ( s32 j = 0; j < Vertex.m_nNormals; j++ )
            {
                Vertex.m_BTN[j].m_Normal.NormalizeSafe();
                if( j < Vertex.m_nBinormals )
                {
                    Vertex.m_BTN[ j ].m_Binormal.NormalizeSafe();
                    Vertex.m_BTN[ j ].m_Tangent.NormalizeSafe();
                }
            }
        }
    }
    //
    // Sort weights from largest to smallest
    //
    {
        s32 i, j, k;
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            for ( j = 0; j < m_Vertex[ i ].m_nWeights; j++ )
            {
                s32 BestW = j;
                for ( k = j + 1; k<m_Vertex[ i ].m_nWeights; k++ )
                {
                    if ( m_Vertex[ i ].m_Weight[ k ].m_Weight > m_Vertex[ i ].m_Weight[ BestW ].m_Weight )
                        BestW = k;
                }

                weight TW = m_Vertex[ i ].m_Weight[ j ];
                m_Vertex[ i ].m_Weight[ j ] = m_Vertex[ i ].m_Weight[ BestW ];
                m_Vertex[ i ].m_Weight[ BestW ] = TW;
            }
        }
    }

    RMESH_SANITY

    //
    // Collapse vertices that are too close from each other and have 
    // the same properties.
    //
    if (1)
    {
        struct hash
        {
            s32     m_iVRemap;
            s32     m_iNext;
        };

        struct tempv
        {
            s32     m_RemapIndex;     // Which vertex Index it shold now use.
            s32     m_Index;          // Inde to the original vertex
            s32     m_iNext;          // next node in the has
        };

        if ( m_Vertex.getCount() <= 0 )
            x_throw( "rawgeom has not vertices" );

        s32             i;
        xptr<hash>      Hash;
        xptr<tempv>     TempV;
        const s32       HashDimension = x_Max( 20, s32(x_Sqrt((f32)m_Vertex.getCount())) );
        const s32       HashSize = x_Sqr( HashDimension );
        f32             MaxX, MinX, XShift;
        f32             MaxZ, MinZ, ZShift;

        // Allocate memory
        Hash.New( HashSize );
        TempV.New( m_Vertex.getCount() );

        // Initialize the hash with terminators
        for ( i = 0; i < HashSize; i++ )
        {
            Hash[ i ].m_iNext = -1;
        }

        // Fill the nodes for each of the dimensions
        MaxX = m_Vertex[ 0 ].m_Position.m_X;
        MaxZ = m_Vertex[ 0 ].m_Position.m_Z;
        MinX = MaxX;
        MinZ = MaxZ;
        {
            s32 TotalCrazyVerts = 0;
            for ( i = 0; i < m_Vertex.getCount(); i++ )
            {
                static const f32 CrazyMax = 10000.f;

                TempV[ i ].m_Index = i;
                TempV[ i ].m_iNext = -1;
                TempV[ i ].m_RemapIndex = i;

                //
                // Watch out for crazy verts
                //
                const f32 XDisMin = x_Abs( MaxX - m_Vertex[ i ].m_Position.m_X );
                const f32 XDisMax = x_Abs( m_Vertex[ i ].m_Position.m_X-MinX   );

                if ( XDisMin > CrazyMax || XDisMax > CrazyMax )
                {
                    TotalCrazyVerts++;
                    continue;
                }

                const f32 ZDisMin = x_Abs( MaxZ - m_Vertex[ i ].m_Position.m_Z );
                const f32 ZDisMax = x_Abs( m_Vertex[ i ].m_Position.m_Z-MinZ   );

                if ( ZDisMin > CrazyMax || ZDisMax > CrazyMax )
                {
                    TotalCrazyVerts++;
                    continue;
                }

                //
                // Get the max
                //
                MaxX = x_Max( MaxX, m_Vertex[ i ].m_Position.m_X );
                MinX = x_Min( MinX, m_Vertex[ i ].m_Position.m_X );
                MaxZ = x_Max( MaxZ, m_Vertex[ i ].m_Position.m_Z );
                MinZ = x_Min( MinZ, m_Vertex[ i ].m_Position.m_Z );
            }

            if ( TotalCrazyVerts > 5000 )
                x_throw( "ERROR: We have too many vertices that are outside an acceptable range" );

        }

        // Hash all the vertices into the hash table
        XShift = ( HashDimension - 1 ) / ( (MaxX - MinX) + 1 );
        ZShift = ( HashDimension - 1 ) / ( (MaxZ - MinZ) + 1 );
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            const s32 XOffSet = (s32)x_Range( ( ( m_Vertex[ i ].m_Position.m_X - MinX ) * XShift ), 0.f, (f32)HashDimension );
            const s32 ZOffSet = (s32)x_Range( ( ( m_Vertex[ i ].m_Position.m_Z - MinZ ) * ZShift ), 0.f, (f32)HashDimension );

            ASSERT( XOffSet >= 0 );
            ASSERT( XOffSet < HashDimension );
            ASSERT( ZOffSet >= 0 );
            ASSERT( ZOffSet < HashDimension );

            const s32   iEntry      = XOffSet + HashDimension * ZOffSet;
            hash&       HashEntry   = Hash[ iEntry ];

            TempV[ i ].m_iNext      = HashEntry.m_iNext;
            HashEntry.m_iNext       = i;
        }

        x_inline_light_jobs_block<8> BlockJobs;

        // Now do a seach for each vertex
        for ( s32 i = 0; i < HashSize; i++ )
        {
            const s32 XCell     = (i%HashDimension);
            const s32 ZCell     = (i/HashDimension);
            const s32 XFrom     = x_Max( 0, XCell );
            const s32 ZFrom     = x_Max( 0, ZCell );
            const s32 XTo       = x_Min( HashDimension-1, XCell + 1);
            const s32 ZTo       = x_Min( HashDimension-1, ZCell + 1);

            for ( s32 k = Hash[ i ].m_iNext; k != -1; k = TempV[ k ].m_iNext )
            {
                const tempv& TempVKeyEntry = TempV[ k ];

                // This vertex has been remap
                if ( TempVKeyEntry.m_RemapIndex != TempVKeyEntry.m_Index )
                    continue;

                for ( s32 x=XFrom; x!=XTo; ++x )
                for ( s32 z=ZFrom; z!=ZTo; ++z )
                {
                    const s32   iHash       = x + z * HashDimension;
                    const hash& ExploreHash = Hash[ iHash ];
                    const xbool bSameHash   = iHash == i;

                    BlockJobs.SubmitJob( [this, &ExploreHash, &TempVKeyEntry, k, &TempV, bSameHash ]()
                    {
                        s32 TotalEntryPerCell = 0;
                        s32 iStartNode;

                        if ( bSameHash ) 
                            iStartNode = TempV[ k ].m_iNext;
                        else
                            iStartNode = ExploreHash.m_iNext;

                        // Seach all the nodes inside this hash
                        for ( s32 j = iStartNode; j != -1; j = TempV[ j ].m_iNext )
                        {
                            tempv& VEntryTest = TempV[ j ];

                            TotalEntryPerCell++;

                            ASSERT ( &VEntryTest != &TempVKeyEntry );

                            // This vertex has been remap
                            if ( VEntryTest.m_RemapIndex != VEntryTest.m_Index )
                                continue;

                            // If both vertices are close then remap vertex
                            if ( TempVCompare( m_Vertex[ TempVKeyEntry.m_RemapIndex ], m_Vertex[ VEntryTest.m_Index ] ) )
                                VEntryTest.m_RemapIndex = TempVKeyEntry.m_RemapIndex;
                        }

                        ASSERT( TotalEntryPerCell < (m_Vertex.getCount()/4) );
                    });
                }

                BlockJobs.FinishJobs();
            }
        }
        RMESH_SANITY

        // Okay now we must collapse all the unuse vertices
        s32 nVerts = 0;
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            if ( TempV[ i ].m_RemapIndex == TempV[ i ].m_Index )
            {
                TempV[ i ].m_RemapIndex = nVerts;
                TempV[ i ].m_Index      = -1;      // Mark as we have cranch it
                nVerts++;
            }
        }

        RMESH_SANITY

        // OKay now get all the facets and remap their indices
        for ( i = 0; i < m_Facet.getCount(); i++ )
        for ( s32 j = 0; j < m_Facet[ i ].m_nVertices; j++ )
        {
            s32&    iVert  = m_Facet[ i ].m_iVertex[ j ];
            s32     iRemap = TempV[ iVert ].m_RemapIndex;

            if ( TempV[ iVert ].m_Index == -1 )
            {
                iVert = iRemap;
            }
            else
            {
                iVert = TempV[ iRemap ].m_RemapIndex;
                ASSERT( TempV[ iRemap ].m_Index == -1 );
            }
        }

        RMESH_SANITY

        // Now copy the vertices to their final location
        xptr<vertex>    Vertex;
        Vertex.Alloc( nVerts );

        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            s32 iRemap = TempV[ i ].m_RemapIndex;

            if ( TempV[ i ].m_Index == -1 )
            {
                Vertex[ iRemap ] = m_Vertex[ i ];
            }
            /*
            else
            {
                Vertex[ TempV[ iRemap ].m_RemapIndex ] = m_Vertex[ i ];
            }
            */
        }

        RMESH_SANITY

        // Finally set the new count and
        TotalVerticesRemoved += m_Vertex.getCount() - nVerts;
        m_Vertex = Vertex;

        RMESH_SANITY
    }

    RMESH_SANITY

        //
        // Elliminate any digenerated facets
        //
    {
        s32 i;
        s32 nFacets = 0;

        for ( i = 0; i < m_Facet.getCount(); i++ )
        {
            xvector3 Normal = m_Vertex[ m_Facet[ i ].m_iVertex[ 1 ] ].m_Position - m_Vertex[ m_Facet[ i ].m_iVertex[ 0 ] ].m_Position.Cross(
                m_Vertex[ m_Facet[ i ].m_iVertex[ 2 ] ].m_Position - m_Vertex[ m_Facet[ i ].m_iVertex[ 0 ] ].m_Position );

            // Remove this facet if we're dumping out this Mesh.
            if ( ( iMesh != -1 && m_Facet[ i ].m_iMesh == iMesh )
                || Normal.GetLength() < 0.00001f )
            {
                // Skip Facet
                //x_DebugMsg("Removing face %1d, (%1d,%1d,%1d)\n",i,m_pFacet[i].iVertex[0],m_pFacet[i].iVertex[1],m_pFacet[i].iVertex[2]);
            }
            else
            {
                m_Facet[ nFacets ] = m_Facet[ i ];
                nFacets++;
            }
        }

        // Set the new count
        TotalFacetsRemoved += m_Facet.getCount() - nFacets;

        if ( TotalFacetsRemoved )
            m_Facet.Resize( nFacets );

        // No facets left!
        if ( m_Facet.getCount() <= 0 )
            x_throw( "rawgeom has not facets" );
    }

    RMESH_SANITY

        //
        // Elliminate any unuse vertices
        //
    REMOVE_VERTS_AGAIN :
    {
        s32     i, j;

        if ( m_Vertex.getCount() <= 0 )
            x_throw( "rawgeom has no vertices" );

        // Allocat the remap table
        xptr<s32> VRemap;

        VRemap.Alloc( m_Vertex.getCount() );

        // Fill the remap table
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            VRemap[ i ] = -1;
        }

        // Mark all the used vertices
        for ( facet& Face : m_Facet )
        for ( s32 j = 0; j < Face.m_nVertices; j++ )
        {
            if ( Face.m_iVertex[ j ] < 0 ||
                Face.m_iVertex[ j ] >= m_Vertex.getCount() )
                x_throw( "Found a facet that was indexing a vertex out of range! FaceID = %d VertexID = %d",
                i, Face.m_iVertex[ j ] );

            VRemap[ Face.m_iVertex[ j ] ] = -2;
        }

        // Create the remap table
        // and compact the vertices to the new location
        for ( j = i = 0; i < m_Vertex.getCount(); i++ )
        {
            s32 Value = VRemap[ i ];

            VRemap[ i ] = j;
            m_Vertex[ j ] = m_Vertex[ i ];

            if ( Value == -2 ) j++;
        }

        // Set the final vertex count
        TotalVerticesRemoved += m_Vertex.getCount() - j;
        if ( TotalVerticesRemoved )
            m_Vertex.Resize( j );

        // Remap all the faces to point to the new location of verts
        for ( facet& Face : m_Facet )
        for ( s32 j = 0; j < Face.m_nVertices; j++ )
        {
            Face.m_iVertex[ j ] = VRemap[ Face.m_iVertex[ j ] ];
        }
    }

    RMESH_SANITY


    //
    // Nuke any facets that has the same vert indices and properties
    //
    {
        struct fref
        {
            s32 m_iFacet;
            s32 m_iNext;
        };

        s32         i;
        s32         nFacets;
        xptr<s32>   VNode;
        xptr<fref>  FRef;
        s32         nRefs;
        s32         iRef;

        // Make sure that we have vertices
        if ( m_Vertex.getCount() <= 0 )
            x_throw( "rawgeom has not vertices" );

        // Get how many ref we should have
        nRefs = 0;
        for ( i = 0; i < m_Facet.getCount(); i++ )
        {
            nRefs += m_Facet[ i ].m_nVertices;
        }

        // Allocate hash, and refs
        VNode.New( m_Vertex.getCount() );
        FRef.New( nRefs );

        // Initalize the hash entries to null
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        {
            VNode[ i ] = -1;
        }

        // Insert all the face references into the hash
        iRef = 0;
        for ( i = 0; i < m_Facet.getCount(); i++ )
        for ( s32 j = 0; j < m_Facet[ i ].m_nVertices; j++ )
        {
            ASSERT( iRef < nRefs );
            FRef[ iRef ].m_iFacet = i;
            FRef[ iRef ].m_iNext = VNode[ m_Facet[ i ].m_iVertex[ j ] ];
            VNode[ m_Facet[ i ].m_iVertex[ j ] ] = iRef;
            iRef++;
        }

        // Find duplicate facets
        for ( i = 0; i < m_Vertex.getCount(); i++ )
        for ( s32 j = VNode[ i ]; j != -1; j = FRef[ j ].m_iNext )
        {
            facet& A = m_Facet[ FRef[ j ].m_iFacet ];

            // This facet has been removed
            if ( A.m_nVertices < 0 )
                continue;

            for ( s32 k = FRef[ j ].m_iNext; k != -1; k = FRef[ k ].m_iNext )
            {
                facet& B = m_Facet[ FRef[ k ].m_iFacet ];

                // This facet has been removed
                if ( B.m_nVertices < 0 )
                    continue;

                // Check whether the two facets are the same
                if ( CompareFaces( A, B ) )
                {
                    // Mark for removal
                    B.m_nVertices = 0;
                }
            }
        }

        // Remove any unwanted facets
        nFacets = 0;
        for ( i = 0; i < m_Facet.getCount(); i++ )
        {
            if ( m_Facet[ i ].m_nVertices == 0 )
            {
                // Skip Facet
            }
            else
            {
                m_Facet[ nFacets ] = m_Facet[ i ];
                nFacets++;
            }
        }

        // Set the new count
        const s32 nFacesRemoved = m_Facet.getCount() - nFacets;
        TotalFacetsRemoved += nFacesRemoved;
        if ( TotalFacetsRemoved )
            m_Facet.Resize( nFacets );

        // No facets left!
        if ( m_Facet.getCount() <= 0 )
            x_throw( "rawgeom has not facets" );

        if ( nFacesRemoved > 0 )
            goto REMOVE_VERTS_AGAIN;
    }


    RMESH_SANITY

    //
    // Remove materials that are not been use
    //
    if( m_InformedMaterial.getCount() > 0 )
    {
        struct mat
        {
            xbool   m_Used;
            s32     m_ReIndex;
        };

        s32         i;
        xptr<mat>   Used;
        
        Used.New( m_InformedMaterial.getCount( ) );

        for ( mat& Mat : Used )
        {
            Mat.m_ReIndex = -1;
            Mat.m_Used    = FALSE;
        }
        
        // Go throw the facets and mark all the used materials
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            const facet& Facet = m_Facet[i];
             
            if( Facet.m_iInformed < 0 ||
                Facet.m_iInformed > m_InformedMaterial.getCount() )
                x_throw( "Found a face from mesh [%s] which was using an unknow material FaceID=%d MaterialID =%d", 
                    (const char*) m_Mesh[ Facet.m_iMesh ].m_Name,
                    i, Facet.m_iInformed );

            Used[ Facet.m_iInformed ].m_Used = TRUE;
        }

        // Collapse all the material in order
        s32     nMaterials      = 0;
        xbool   bAnyCollapse    = FALSE;
        for( i=0; i<m_InformedMaterial.getCount(); i++ )
        {
            if( Used[i].m_Used == FALSE )
                continue;

            if ( i == nMaterials )
            {
                Used[nMaterials].m_ReIndex  = i;
            }
            else
            {
                bAnyCollapse = TRUE;
                m_InformedMaterial[ nMaterials ] = m_InformedMaterial[ i ];
                Used[i].m_ReIndex                = nMaterials;
            }

            nMaterials++;
        }

        // Update the material indices for the facets
        if ( bAnyCollapse )
        {
            for( i=0; i<m_Facet.getCount(); i++ )
            {
                if( Used[ m_Facet[i].m_iInformed ].m_ReIndex < 0 ||
                    Used[ m_Facet[i].m_iInformed ].m_ReIndex >= nMaterials )
                    x_throw( "Error while cleaning the materials in the rawgeom2" );

                m_Facet[i].m_iInformed = Used[ m_Facet[i].m_iInformed ].m_ReIndex;
            }

            // Set the new material count
            TotalMaterialsRemoved += m_InformedMaterial.getCount() - nMaterials;
            m_InformedMaterial.Resize(nMaterials);
        }

        // Sort material parameters
        for ( informed_material& Material : m_InformedMaterial )
        {
            Material.m_Params.Sort( );
        }
    }

    RMESH_SANITY

    //
    // Remove unwanted meshes
    //
    {
        s32 i;
        xptr<s32> SMesh;
        
        SMesh.New(m_Mesh.getCount());
        SMesh.SetMemory(~0);
        
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            SMesh[ m_Facet[i].m_iMesh ] = 1;
        }

        s32 nSubs = 0;
        for( i=0; i<m_Mesh.getCount(); i++ )
        {
            if( SMesh[ i ] == 1 )
            {
                SMesh[i] = nSubs;
                m_Mesh[nSubs++] = m_Mesh[i];
            }
            else
            {
                SMesh[i] = -1;
            }
        }

        // Set the new count
        if( m_Mesh.getCount() != nSubs )
            m_Mesh.Resize( nSubs );
        
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            ASSERT( SMesh[ m_Facet[i].m_iMesh ] >= 0 );
            m_Facet[i].m_iMesh = SMesh[ m_Facet[i].m_iMesh ];
        }
    }

    //
    // Sort the meshes so that they are in alphabetical order
    //    
    {
        s32 i, j;

        struct mesh_info
        {
            mesh    m_Mesh;
            s32         m_iOriginal;
        };

        xptr<mesh_info> Mesh;
        xptr<s32>       Remap;
        
        Mesh.New( m_Mesh.getCount() );
        Remap.New( m_Mesh.getCount() );
        
        for( i=0; i<m_Mesh.getCount(); i++ )
        {
            Mesh[i].m_Mesh       = m_Mesh[i];
            Mesh[i].m_iOriginal  = i;
        }

        // sort the meshes
        xbool bSorted = FALSE;
        for ( j = 0; j < m_Mesh.getCount() && !bSorted; j++ )
        {
            bSorted = TRUE;
            for ( i = 0; i < m_Mesh.getCount()-1-j; i++ )
            {
                if( x_strcmp( Mesh[i].m_Mesh.m_Name, Mesh[i+1].m_Mesh.m_Name ) > 0 )
                {
                    bSorted        = FALSE;
                    mesh_info Temp = Mesh[i+1];
                    Mesh[i+1]      = Mesh[i];
                    Mesh[i]        = Temp;
                }
            }
        }
        
        // sanity check to make sure we know how to sort
        for ( i = 0; i < m_Mesh.getCount(); i++ )
        {
            if ( i < m_Mesh.getCount()-1 )
            {
                ASSERT( x_strcmp( Mesh[i].m_Mesh.m_Name, Mesh[i+1].m_Mesh.m_Name ) <= 0 );
            }
        }

        // copy over the original Meshes
        for( i=0; i<m_Mesh.getCount(); i++ )
        {
            m_Mesh[i] = Mesh[i].m_Mesh;
        }

        // build a remap table for the faces
        for( i=0; i<m_Mesh.getCount(); i++ )
        {
            for ( j=0; j<m_Mesh.getCount(); j++ )
            {
                if ( Mesh[j].m_iOriginal == i )
                {
                    Remap[i] = j;
                    break;
                }
            }
        }

        // remap the faces
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            m_Facet[i].m_iMesh = Remap[ m_Facet[i].m_iMesh ];
        }
    }

    //
    // Stats
    //
    x_printf( "INFO: Total Facets Removed: %d",    TotalFacetsRemoved );
    x_printf( "INFO: Total Verts  Removed: %d",    TotalVerticesRemoved );
    x_printf( "INFO: Total Materials Removed: %d", TotalMaterialsRemoved );
}

//=========================================================================
/*
void CalculateTangentArray( 
    const s32           vertexCount, 
    const xvector3d*    vertex, 
    const xvector3d*    normal,
    const xvector2*     texcoord, 
    const s32           triangleCount, 
    const s32[3]*       triangle, 
    Vector4D*           tangent )
{
    Vector3D *tan1 = new Vector3D[ vertexCount * 2 ];
    Vector3D *tan2 = tan1 + vertexCount;
    ZeroMemory( tan1, vertexCount * sizeof(Vector3D)* 2 );

    for ( long a = 0; a < triangleCount; a++ )
    {
        long i1 = triangle->index[ 0 ];
        long i2 = triangle->index[ 1 ];
        long i3 = triangle->index[ 2 ];

        const Point3D& v1 = vertex[ i1 ];
        const Point3D& v2 = vertex[ i2 ];
        const Point3D& v3 = vertex[ i3 ];

        const Point2D& w1 = texcoord[ i1 ];
        const Point2D& w2 = texcoord[ i2 ];
        const Point2D& w3 = texcoord[ i3 ];

        float x1 = v2.x - v1.x;
        float x2 = v3.x - v1.x;
        float y1 = v2.y - v1.y;
        float y2 = v3.y - v1.y;
        float z1 = v2.z - v1.z;
        float z2 = v3.z - v1.z;

        float s1 = w2.x - w1.x;
        float s2 = w3.x - w1.x;
        float t1 = w2.y - w1.y;
        float t2 = w3.y - w1.y;

        float r = 1.0F / ( s1 * t2 - s2 * t1 );
        Vector3D sdir( ( t2 * x1 - t1 * x2 ) * r, ( t2 * y1 - t1 * y2 ) * r,
            ( t2 * z1 - t1 * z2 ) * r );
        Vector3D tdir( ( s1 * x2 - s2 * x1 ) * r, ( s1 * y2 - s2 * y1 ) * r,
            ( s1 * z2 - s2 * z1 ) * r );

        tan1[ i1 ] += sdir;
        tan1[ i2 ] += sdir;
        tan1[ i3 ] += sdir;

        tan2[ i1 ] += tdir;
        tan2[ i2 ] += tdir;
        tan2[ i3 ] += tdir;

        triangle++;
    }

    for ( long a = 0; a < vertexCount; a++ )
    {
        const Vector3D& n = normal[ a ];
        const Vector3D& t = tan1[ a ];

        // Gram-Schmidt orthogonalize
        tangent[ a ] = ( t - n * Dot( n, t ) ).Normalize( );

        // Calculate handedness
        tangent[ a ].w = ( Dot( Cross( n, t ), tan2[ a ] ) < 0.0F ) ? -1.0F : 1.0F;
    }

    delete[ ] tan1;
}
*/

//=========================================================================

void rawgeom::SanityCheck( void ) const
{
    //
    // Check that we have a valid number of materials
    //
    if( m_InformedMaterial.getCount() < 0 )
        x_throw( "The rawgeom it saying that it has a negative number of materials!");

    if( m_InformedMaterial.getCount() > 1000 )
        x_throw( "The rawgeom2 has more than 1000 materials right now that is a sign of a problem" );
    
    if( m_Vertex.getCount() < 0 )
        x_throw( "THe rawgeom2 has a negative number of vertices!" );

    if( m_Vertex.getCount() > 100000000 )
        x_throw( "The rawgeom2 seems to have more that 100 million vertices that is consider bad" );

    if( m_Facet.getCount() < 0 )
        x_throw( "We have a negative number of facets" );

    if( m_Facet.getCount() > 100000000 )
        x_throw( "THe rawgeom2 has more thatn 100 million facets that is consider worng" );

    if( m_Bone.getCount() < 0 )
        x_throw ("We have a negative count for bones" );
    
    if( m_Bone.getCount() > 100000 )
        x_throw ("We have more than 100,000 bones this may be a problem" );

    if( m_InformedMaterial.getCount() < 0 )
        x_throw( "Found a negative number of textures" );

    if( m_Mesh.getCount() < 0 )
        x_throw( "We have a negative number of meshes this is not good" );

    if( m_Mesh.getCount() > 100000 )
        x_throw( "We have more than 100,000 meshes this may be a problem" );

    //
    // Check the facets.
    //
    {
        s32 i;
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            const facet& Facet = m_Facet[i];

            if( Facet.m_nVertices < 0 )
                x_throw( "I found a facet that had a negative number of indices to vertices Face#:%d", i );

            if( Facet.m_nVertices < 3 )
                x_throw( "I found a facet with less than 3 vertices Face#:%d", i);

            if( Facet.m_nVertices > 16 )
                x_throw( "I found a facet with more thatn 16 indices to vertices. The max is 16 this means memory overwun Face#:%d", i );

            if( Facet.m_iInformed < 0 )
                x_throw( "I found a facet with a negative material. That is not allow. Facet#:%d",i);

            if( Facet.m_iInformed >= m_InformedMaterial.getCount() )
                x_throw( "I found a facet that had an index to a material which is bad. Facet#:%d",i);

            if( Facet.m_iMesh < 0 )
                x_throw( "I found a facet indexing to a negative offset for the meshID Facet#:%d",i);

            if( Facet.m_iMesh >= m_Mesh.getCount() )
                x_throw( "I found a facet indexing to a not exiting mesh Facet#:%d",i);

            for( s32 j=0; j<Facet.m_nVertices; j++ )
            {
                if( Facet.m_iVertex[j] < 0 )
                    x_throw( "I found a facet with a negative index to vertices. Facet#:%d",i);

                if( Facet.m_iVertex[j] >= m_Vertex.getCount() )
                    x_throw( "I found a facet with a index to a non-exiting vertex. Facet#:%d",i);
            }
        }
    }

    //
    // Check the vertices.
    //
    {
        s32 i,j;
        for( i=0; i<m_Facet.getCount(); i++ )
        {
            const facet& Facet = m_Facet[i];
            for( j=0; j<Facet.m_nVertices; j++ )
            {
                const vertex& V = m_Vertex[ Facet.m_iVertex[j] ];

                if( x_isValid( V.m_Position.m_X ) == FALSE ||
                    x_isValid( V.m_Position.m_Y ) == FALSE ||
                    x_isValid( V.m_Position.m_Z ) == FALSE )
                    x_throw( "Just got a infinete vertex position: Vertex#:%d",j);

                if( V.m_nWeights < 0 )
                    x_throw( "We have a negative count of weights for one of the vertices. V#:%d",j);

                if( V.m_nWeights >= VERTEX_MAX_WEIGHT )
                    x_throw( "Found a vertex with way too many weights. V#:%d",j);

                if( V.m_nWeights > m_Bone.getCount() )
                    x_throw( "Found a vertex pointing to a non-exiting bone: V#:d",j);

                if( V.m_nNormals < 0 )
                    x_throw( "Found a vertex with a negative number of Normals. V#:%d",j);

                if( V.m_nNormals >= VERTEX_MAX_NORMAL )
                    x_throw( "Found a vertex with way too many normals. V3:%d", j);

                if( V.m_nColors < 0 )
                    x_throw( "I found a vertex with a negative number of colors. V#:%d", j);

                if( V.m_nColors >= VERTEX_MAX_COLOR )
                    x_throw( "I found a vertex with way too many colors. V#:%d", j);

                if( V.m_nUVs < 0 )
                    x_throw( "I found a vertex with a negative count for UVs. V#:%d",j);

                if( V.m_nUVs > VERTEX_MAX_UV )
                    x_throw( "I found a vertex with way too many UVs. V#:%d", j);

                //if( V.nUVs != m_pMaterial[ Facet.iMaterial ].GetUVChanelCount() )
                //    x_throw( xfs("I found a vertex that didn't have the right number of UVs for the material been used. V#:%d, F#:%d",j,i));

            }
        }
    }
}

//=========================================================================

void rawgeom::CleanWeights( s32 MaxNumWeights, f32 MinWeightValue )
{
    s32 i,j,k;

    //
    // Sort weights from largest to smallest
    //
    for( i=0; i<m_Vertex.getCount(); i++ )
    {
        for( j=0; j<m_Vertex[i].m_nWeights; j++ )
        {
            s32 BestW = j;
            for( k=j+1; k<m_Vertex[i].m_nWeights; k++ )
            {
                if( m_Vertex[i].m_Weight[k].m_Weight > m_Vertex[i].m_Weight[BestW].m_Weight )
                    BestW = k;
            }

            weight TW = m_Vertex[i].m_Weight[j];
            m_Vertex[i].m_Weight[j] = m_Vertex[i].m_Weight[BestW];
            m_Vertex[i].m_Weight[BestW] = TW;

            ASSERT(  m_Vertex[i].m_Weight[j].m_iBone >= 0 );
            ASSERT( m_Vertex[i].m_Weight[j].m_iBone < m_Bone.getCount() );
        }
    }

    //
    // Cull any extra weights
    //
    for( i=0; i<m_Vertex.getCount(); i++ )
        if( m_Vertex[i].m_nWeights > MaxNumWeights )
        {
            m_Vertex[i].m_nWeights = MaxNumWeights;

            // Normalize weights
            f32 TotalW=0.0f;
            for( j=0; j<m_Vertex[i].m_nWeights; j++ )
                TotalW += m_Vertex[i].m_Weight[j].m_Weight;

            for( j=0; j<m_Vertex[i].m_nWeights; j++ )
                m_Vertex[i].m_Weight[j].m_Weight /= TotalW;
        }

    //
    // Throw out all weights below MinWeightValue
    //
    for( i=0; i<m_Vertex.getCount(); i++ )
    {
        if ( m_Vertex[i].m_nWeights == 0 )
        {
            m_Vertex[ i ].m_Weight[ 0 ].m_iBone = 0;
            m_Vertex[ i ].m_Weight[ 0 ].m_Weight = 1;
            m_Vertex[i].m_nWeights = 1;
            continue;
        }

       // Keep weights above MinWeight
        s32 nWeights = 0;
        for( j=0; j<m_Vertex[i].m_nWeights; j++ )
        {
            if( m_Vertex[i].m_Weight[j].m_Weight >= MinWeightValue )
            {
                m_Vertex[i].m_Weight[nWeights] = m_Vertex[i].m_Weight[j];
                nWeights++;
            }
        }
        
        nWeights = x_Max(1,nWeights);
        m_Vertex[i].m_nWeights = nWeights;

        // Normalize weights
        f32 TotalW=0.0f;
        for( j=0; j<m_Vertex[i].m_nWeights; j++ )
            TotalW += m_Vertex[i].m_Weight[j].m_Weight;
        
        for( j=0; j<m_Vertex[i].m_nWeights; j++ )
            m_Vertex[i].m_Weight[j].m_Weight /= TotalW;


        for ( j = 0; j < m_Vertex[ i ].m_nWeights; j++ )
        {
            ASSERT( m_Vertex[ i ].m_Weight[ j ].m_iBone >= 0 );
            ASSERT( m_Vertex[ i ].m_Weight[ j ].m_iBone < m_Bone.getCount() );
        }
    }
}

//=========================================================================

void rawgeom::CollapseMeshes( const char* pMeshName )
{
    for( s32 i=0; i<m_Facet.getCount(); i++ )
    {
        m_Facet[i].m_iMesh = 0;
    }

    m_Mesh.Resize(1);    
    m_Mesh[0].m_Name.Copy( pMeshName );
}

//=========================================================================

void rawgeom::ComputeMeshBBox( s32 iMesh, xbbox& BBox )
{
    s32 i,j;

    BBox.Zero();

    for( i=0; i<m_Facet.getCount(); i++ )
    {
        if( m_Facet[i].m_iMesh == iMesh )
        {
            for( j=0; j<m_Facet[i].m_nVertices; j++ )
                BBox += m_Vertex[ m_Facet[i].m_iVertex[j] ].m_Position;
        }
    }
}

//=========================================================================

// Computes bone bboxes and the number of bones used by each Mesh
void rawgeom::ComputeBoneInfo( void )
{
    s32 i,j,k ;

    //=====================================================================
    // Compute bone bboxes
    //=====================================================================

    // Clear all bone bboxes
    for (i = 0 ; i < m_Bone.getCount() ; i++)
        m_Bone[i].m_BBox.Zero() ;

    // Loop through all the verts and add to bone bboxes
    for (i = 0 ; i < m_Vertex.getCount() ; i++)
    {
        // Lookup vert
        vertex& Vertex = m_Vertex[i] ;

        // Loop through all weights in vertex
        for (s32 j = 0 ; j < Vertex.m_nWeights ; j++)
        {
            // Lookup bone that vert is attached to
            s32 iBone = Vertex.m_Weight[j].m_iBone ;
            ASSERT(iBone >= 0) ;
            ASSERT(iBone < m_Bone.getCount() ) ;

            // Add to bone bbox
            m_Bone[iBone].m_BBox += Vertex.m_Position ;
        }
    }

    // If a bone has no geometry attached, then set bounds to the bone position
    for (i = 0 ; i < m_Bone.getCount() ; i++)
    {
        // Lookup bone
        bone& Bone = m_Bone[i] ;

        // If bbox is empty, just use the bone position
        if (Bone.m_BBox.m_Min.m_X > Bone.m_BBox.m_Max.m_X )
            Bone.m_BBox += Bone.m_Position ;

        // Inflate slightly do get rid of any degenerate (flat) sides
        Bone.m_BBox.Inflate( xvector3d(0.1f, 0.1f, 0.1f) ) ;
    }

    //=====================================================================
    // Compute # of bones used by each sub-mesh
    // Bones are arranged in LOD order, so we can just use the (MaxBoneUsed+1)
    //=====================================================================
    
    // Clear values
    for (i = 0 ; i < m_Mesh.getCount(); i++)
        m_Mesh[i].m_nBones = 0 ;

    // Loop through all faces
    for (i = 0 ; i < m_Facet.getCount() ; i++)
    {
        // Lookup face and the mesh it's part of
        facet&      Face = m_Facet[i] ;
        mesh&   Mesh = m_Mesh[Face.m_iMesh] ;

        // Loop through all verts in each face
        for (j = 0 ; j < Face.m_nVertices ; j++)
        {
            // Lookup vert
            vertex& Vert = m_Vertex[Face.m_iVertex[j]] ;

            // Loop through all weights in vert
            for (k = 0 ; k < Vert.m_nWeights ; k++)
            {
                // Update mesh bone count
                Mesh.m_nBones = x_Max( Mesh.m_nBones, Vert.m_Weight[k].m_iBone) ;
            }
        }
    }

    // We want the actual number of bones used so fix up
    for (i = 0 ; i < m_Mesh.getCount() ; i++)
        m_Mesh[i].m_nBones++ ;
}

//=========================================================================

xbool rawgeom::IsolateMesh( s32 iMesh, rawgeom& NewMesh,
                               xbool RemoveFromrawgeom /* = FALSE */)
{
    NewMesh.Kill();

    if( iMesh < 0 )
        return FALSE;
    
    if(iMesh >= m_Mesh.getCount() )
        return FALSE;

    //
    //  Make straight copies of data we don't affect
    //
    //  BONES
    //
    if( m_Bone.getCount() > 0 )
    {
        NewMesh.m_Bone.New(m_Bone.getCount());
        NewMesh.m_Bone.Copy( 0, m_Bone, 0, m_Bone.getCount() );
    }

    //
    //  Meshes
    //
    ASSERT( m_Mesh.getCount() > 0 );
    
    NewMesh.m_Mesh.New(1);
    NewMesh.m_Mesh.Copy(0, m_Mesh, iMesh, 1 );
    
    
    //
    // Materials:
    //
    if( m_InformedMaterial.getCount() > 0 )
    {
        NewMesh.m_InformedMaterial.New( m_InformedMaterial.getCount() );
        NewMesh.m_InformedMaterial.Copy( 0, m_InformedMaterial, 0, m_InformedMaterial.getCount() );
    }

    // Verts and Facets:
    //
    //  Each facet will generate 3 unique verts, which will be consolidated later.
    //  These are done before the material construction, but the facet iMaterial
    //  member will be touched up in the next step.
    //
    s32     i;
    s32     nFacets = 0;

    for (i=0;i<m_Facet.getCount();i++)
    {
        if (m_Facet[i].m_iMesh == iMesh)
            nFacets++;
    }

    NewMesh.m_Vertex.New( nFacets * 3 );
    NewMesh.m_Facet.New( nFacets );

    rawgeom::vertex*    pVert     = &NewMesh.m_Vertex[0];
    s32                 iVert     = 0;
    rawgeom::facet*     pFacet    = &NewMesh.m_Facet[0];
    
    for (i=0;i<m_Facet.getCount();i++)
    {
        if (m_Facet[i].m_iMesh == iMesh)
        {
            *pFacet = m_Facet[i];

            pFacet->m_iVertex[0] = iVert+0;
            pFacet->m_iVertex[1] = iVert+1;
            pFacet->m_iVertex[2] = iVert+2;
            iVert+=3;       

            pFacet->m_iMesh = 0;
        
            *pVert = m_Vertex[ m_Facet[i].m_iVertex[0] ];
            pVert++;

            *pVert = m_Vertex[ m_Facet[i].m_iVertex[1] ];
            pVert++;

            *pVert = m_Vertex[ m_Facet[i].m_iVertex[2] ];
            pVert++;

            pFacet++;
        }
    }

    //
    // Clear the mesh
    //
    NewMesh.CleanMesh();
    if (RemoveFromrawgeom)
        CleanMesh(iMesh);

    return TRUE;
}

//=========================================================================

xbool rawgeom::IsolateMesh( const char* pMeshName, rawgeom& NewMesh )
{
    s32  i;

    if (pMeshName == NULL)
        return FALSE;

    for( i=0;i<m_Mesh.getCount();i++ )
    {
        if( x_strcmp( m_Mesh[i].m_Name, pMeshName ) == 0 )
        {
            return IsolateMesh( i, NewMesh );
        }
    }

    return FALSE;
}

//=========================================================================

xbool rawgeom::isBoneUsed( s32 iBone )
{
    s32 i,j;

    for( i=0; i<m_Vertex.getCount(); i++ )
    {
        for( j=0; j<m_Vertex[i].m_nWeights; j++ )
        if( m_Vertex[i].m_Weight[j].m_iBone == iBone )
            return TRUE;
    }

    return FALSE;
}

//=========================================================================

void rawgeom::CollapseNormals( xradian ThresholdAngle )
{
    f32     TargetAngle = x_Cos( ThresholdAngle );

    struct hash
    {
        s32         m_iNext;
    };

    struct tempv
    {
        xvector3    m_NewNormal;
        s32         m_Index;          // Inde to the original vertex
        s32         m_iNext;          // next node in the has
    };

    if( m_Vertex.getCount() <= 0 )
        x_throw( "rawgeom has no vertices" );

    s32         i;
    s32         HashSize  = x_Max( 20, m_Vertex.getCount()*10 );
    f32         MaxX, MinX, Shift;

    // Allocate memory
    xptr<hash>       Hash;
    xptr<tempv>      TempV;
    
    Hash.New( HashSize );
    TempV.New( m_Vertex.getCount() );
    
    // Initialize the hash with terminators
    for( i=0; i<HashSize; i++) 
    {
        Hash[i].m_iNext = -1;
    }

    // Fill the nodes for each of the dimensions
    MaxX = m_Vertex[0].m_Position.m_X;
    MinX = MaxX;
    for( i=0; i<m_Vertex.getCount(); i++)
    {
        TempV[i].m_Index         =  i;
        TempV[i].m_iNext         = -1;
        TempV[i].m_NewNormal.Set(0,0,0);
       
        MaxX = x_Max( MaxX, m_Vertex[i].m_Position.m_X );
        MinX = x_Min( MinX, m_Vertex[i].m_Position.m_X );
    }

    // Hash all the vertices into the hash table
    Shift = HashSize/(MaxX-MinX+1);
    for( i=0; i<m_Vertex.getCount(); i++)
    {
        s32 OffSet = (s32)(( m_Vertex[i].m_Position.m_X - MinX ) * Shift);

        ASSERT(OffSet >= 0 );
        ASSERT(OffSet < HashSize );

        TempV[i].m_iNext  = Hash[ OffSet ].m_iNext;
        Hash[ OffSet ].m_iNext = i;
    }

    // Loop through all hash entries, and begin the collapse process
    for( i=0; i<HashSize; i++ )
    {
        for( s32 k = Hash[i].m_iNext; k != -1; k = TempV[k].m_iNext )
        {
            s32         j;
            xvector3    SrcN = m_Vertex[ TempV[k].m_Index ].m_BTN[0].m_Normal;
            xvector3    SrcP = m_Vertex[ TempV[k].m_Index ].m_Position;
            xvector3    ResultN = SrcN;
            
            for( j = Hash[i].m_iNext; j != -1; j = TempV[j].m_iNext )
            {                
                if (j==k)
                    continue;
                
                xvector3 D = m_Vertex[ TempV[j].m_Index ].m_Position - SrcP;

                //  If the verts don't share the same position, continue
                if( D.GetLength() > 0.001f )
                    continue;

                //
                //  Check the normals to see if the 2nd vert's norm is within the
                //  allowable threshold
                //
                xvector3 N = m_Vertex[ TempV[j].m_Index ].m_BTN[0].m_Normal;

                f32      T = SrcN.Dot( N );
                if ( T >= TargetAngle )
                {
                    // Merge in this normal
                    ResultN += N;
                }
            }

            // Search in the hash on the right
            if( (i+1)< HashSize )
            {
                for( j = Hash[i+1].m_iNext; j != -1; j = TempV[j].m_iNext )
                {                
                    xvector3 D = m_Vertex[ TempV[j].m_Index ].m_Position - SrcP;

                    //  If the verts don't share the same position, continue
                    if (D.GetLength() > 0.001f)
                        continue;

                    //
                    //  Check the normals to see if the 2nd vert's norm is within the
                    //  allowable threshold
                    //
                    xvector3 N = m_Vertex[ TempV[j].m_Index ].m_BTN[0].m_Normal;

                    f32     T = SrcN.Dot( N );
                    if ( T >= TargetAngle )
                    {
                        // Merge in this normal
                        ResultN += N;
                    }
                }
            }
            
            // Renormalize the resultant normal
            ResultN.Normalize();

            TempV[k].m_NewNormal = ResultN;
        }
    }

    for (i=0;i<m_Vertex.getCount();i++)
    {
        m_Vertex[ TempV[i].m_Index ].m_BTN[0].m_Normal = TempV[i].m_NewNormal;
    }
}

//=========================================================================

void rawgeom::DeleteBone( const char* pBoneName )
{
    s32 iBone = getBoneIDFromName( pBoneName );
    if(iBone != -1)
        DeleteBone(iBone);
}

//=========================================================================

void rawgeom::DeleteBone( s32 iBone )
{
    //x_DebugMsg("MESH: Deleting bone: '%s'\n", m_pBone[iBone].Name);
    s32 i,j;
    ASSERT( m_Bone.getCount() > 1 );

    //
    // Allocate new bones and frames
    //
    xptr<bone> NewBone;
    NewBone.New( m_Bone.getCount() - 1 );
    
    //
    // Build new hierarchy
    //
    {
        // Copy over remaining bones
        j=0;
        for( i=0; i<m_Bone.getCount(); i++ )
        if( i != iBone )
        {
            NewBone[j] = m_Bone[i];
            j++;
        }

        // Patch children of bone
        for( i=0; i<NewBone.getCount(); i++ )
        if( NewBone[i].m_iParent == iBone )
        {
            NewBone[i].m_iParent = m_Bone[iBone].m_iParent;
        }

        // Patch references to any bone > iBone
        for( i=0; i<NewBone.getCount(); i++ )
        if( NewBone[i].m_iParent > iBone )
        {
            NewBone[i].m_iParent--;
        }
    }

    // free current allocations
    m_Bone = NewBone;
}

//=========================================================================

s32 rawgeom::getBoneIDFromName( const char* pBoneName ) const
{
    s32 i;
    for( i=0; i<m_Bone.getCount(); i++ )
    if( x_stricmp( pBoneName, m_Bone[i].m_Name) == 0 )
        return i;
    return -1;
}

//=========================================================================

void rawgeom::ApplyNewSkeleton( const rawanim& Skel )
{
    s32 i,j;

    // Transform all verts into local space of current skeleton
    if(/* DISABLES CODE */ (0))
    for(i = 0; i < m_Vertex.getCount(); i++)
    {
        auto& Vertex = m_Vertex[i];

        s32         iBone   = Vertex.m_Weight[0].m_iBone;
        xvector3    P       = Vertex.m_Position;
        xmatrix4    BM;
        
        BM.Identity();
        BM.Scale( m_Bone[iBone].m_Scale );
        BM.Rotate( m_Bone[iBone].m_Rotation );
        BM.Translate( m_Bone[iBone].m_Position );
        BM.InvertSRT();
        
        Vertex.m_Position = BM * P;
    }

    // Remap bone indices
    for(i = 0; i < m_Vertex.getCount(); i++)
    {
        rawgeom::vertex* pVertex = &m_Vertex[i];
        for(s32 iWeight = 0; iWeight < pVertex->m_nWeights; iWeight++)
        {
            rawgeom::weight* pWeight = &pVertex->m_Weight[iWeight];

            s32 oldBoneId = pWeight->m_iBone;
            s32 newBoneId = -1;
            s32 curBoneId = oldBoneId;
            while( (newBoneId==-1) && (curBoneId!=-1) )
            {
                const char* curBoneName = m_Bone[curBoneId].m_Name;

                // Look for matching bone in Skel
                for( j=0; j<Skel.m_Bone.getCount(); j++ )
                    if( x_stricmp( curBoneName, Skel.m_Bone[j].m_Name ) == 0 )
                        break;

                if( j != Skel.m_Bone.getCount() )
                {
                    newBoneId = j;
                    break;
                }

                // Move up hierarchy to parent
                curBoneId = m_Bone[curBoneId].m_iParent;
            }

            if ( newBoneId == -1 )
            {
                newBoneId = 0;
                x_printf( "WARNING: Unable to remap Bone Vertex[%d] to new bone", i );
            }

            //ASSERT( m_pBone[pWeight->iBone].Position.Difference( Skel.GetBone(newBoneId).BindTranslation ) < 0.0001f );
            //ASSERT( m_pBone[pWeight->iBone].Rotation.Difference( Skel.GetBone(newBoneId).BindRotation ) < 0.0001f );
            //x_DebugMsg("For old bone of %d, found new bone %d\n", pWeight->iBone, newBoneId);
            pWeight->m_iBone = newBoneId;
            //x_DebugMsg("%s -> %s\n",m_pBone[oldBoneId].Name,Skel.m_pBone[newBoneId].Name);
        }
    }

    //
    // Copy new bone information in
    //
    m_Bone.New( Skel.m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );
    for( s32 count = 0; count < m_Bone.getCount(); count++ )
    {
        m_Bone[count].m_iParent     =   Skel.m_Bone[count].m_iParent;
        m_Bone[count].m_nChildren   =   Skel.m_Bone[count].m_nChildren;
        
        m_Bone[count].m_Name.Copy( Skel.m_Bone[count].m_Name );

        m_Bone[count].m_Position    =   Skel.m_Bone[count].m_BindTranslation;
        m_Bone[count].m_Rotation    =   Skel.m_Bone[count].m_BindRotation;
        m_Bone[count].m_Scale       =   Skel.m_Bone[count].m_BindScale;
    }

    // Transform all verts into model space of new skeleton
    if ( /* DISABLES CODE */ (0) )
    for(i = 0; i < m_Vertex.getCount(); i++)
    {
        s32         iBone   = m_Vertex[i].m_Weight[0].m_iBone;
        xvector3    P       = m_Vertex[i].m_Position;
        xmatrix4    BM;
        BM.Identity();
        BM.Scale        ( m_Bone[iBone].m_Scale );
        BM.Rotate       ( m_Bone[iBone].m_Rotation );
        BM.Translate    ( m_Bone[iBone].m_Position );
        m_Vertex[i].m_Position = BM * P;
    }

    // Compute all bone related info
    ComputeBoneInfo() ;
}

//=========================================================================

void rawgeom::ApplyNewSkeleton( const rawgeom& Skel )
{
    s32 i;

    // Transform all verts into local space of current skeleton
    for(i = 0; i < m_Vertex.getCount(); i++)
    {
        s32         iBone   = m_Vertex[i].m_Weight[0].m_iBone;
        xvector3    P       = m_Vertex[i].m_Position;
        xmatrix4    BM;
        
        BM.Identity();
        BM.Scale    ( m_Bone[iBone].m_Scale );
        BM.Rotate   ( m_Bone[iBone].m_Rotation );
        BM.Translate( m_Bone[iBone].m_Position );
        BM.InvertSRT();
        m_Vertex[i].m_Position = BM * P;
    }

    for( s32 iVertex = 0; iVertex < m_Vertex.getCount(); iVertex++ )
    {
        vertex* pVertex = &m_Vertex[iVertex];
        for(s32 iWeight = 0; iWeight < pVertex->m_nWeights; iWeight++)
        {
            weight*     pWeight     = &pVertex->m_Weight[iWeight];
            s32         oldBoneId   = pWeight->m_iBone;
            const char* oldBoneName = m_Bone[oldBoneId].m_Name;
            s32         newBoneId   = -1;
            s32         curBoneId   = oldBoneId;
            const char* curBoneName = oldBoneName;
            
            while(newBoneId == -1)
            {
                //x_DebugMsg("Looking in new skeleton for name '%s'\n", curBoneName);
                newBoneId = Skel.getBoneIDFromName(curBoneName);
                curBoneId = m_Bone[curBoneId].m_iParent;
                ASSERT((newBoneId !=-1) || (curBoneId != -1));
                curBoneName = m_Bone[curBoneId].m_Name;
            }
            //x_DebugMsg("For old bone of %d, found new bone %d\n", pWeight->iBone, newBoneId);
            pWeight->m_iBone = newBoneId;
        }
    }

    // Copy new bone information in
    m_Bone.New( Skel.m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );

    for(s32 count = 0; count < m_Bone.getCount(); count++)
    {
        const bone& Bone = Skel.m_Bone[count];
        m_Bone[count] = Bone;
    }

    // Transform all verts into local space of current skeleton
    for(i = 0; i < m_Vertex.getCount(); i++)
    {
        s32         iBone   = m_Vertex[i].m_Weight[0].m_iBone;
        xvector3    P       = m_Vertex[i].m_Position;
        xmatrix4    BM;
        
        BM.Identity();
        BM.Scale    ( m_Bone[iBone].m_Scale );
        BM.Rotate   ( m_Bone[iBone].m_Rotation );
        BM.Translate( m_Bone[iBone].m_Position );
        
        m_Vertex[i].m_Position = BM * P;
    }
}

//=========================================================================

void rawgeom::PrintStats( void )
{
//    x_printf("Number Of Meshes:%d")

}