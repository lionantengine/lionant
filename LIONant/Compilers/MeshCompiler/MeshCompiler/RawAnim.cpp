
#include "RawAnim.h"

//--------------------------------------------------------------------------
// FUNCTIONS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------

rawanim::rawanim( void )
{
}

//--------------------------------------------------------------------------

rawanim::~rawanim( void )
{    
}

//--------------------------------------------------------------------------

xbool rawanim::AreBonesFromSameBranch( s32 iBoneA, s32 iBoneB ) const
{
    s32 iParent ;

    // Check for finding B in A
    iParent =  m_Bone[ iBoneA ].m_iParent;// GetBoneIDFromName( m_Bone[ iBoneA ].m_ParentName );
    while( iParent != -1 )
    {
        if( iParent == iBoneB )
            return TRUE ;

        iParent = m_Bone[iParent].m_iParent;// GetBoneIDFromName( m_Bone[iParent].m_ParentName );
    }

    // Check for finding A in B
    iParent = m_Bone[iBoneB].m_iParent;// GetBoneIDFromName( m_Bone[iBoneB].m_ParentName );
    while(iParent != -1)
    {
        if (iParent == iBoneA)
            return TRUE ;

        iParent = m_Bone[iParent].m_iParent; // GetBoneIDFromName( m_Bone[iParent].m_ParentName );
    }

    return FALSE ;
}

//--------------------------------------------------------------------------

struct temp_bone : public rawanim::bone
{
    s32                     m_iBone;
    s32                     m_iBoneRemap;
    s32                     m_LODGroup;
    s32                     m_Depth;
};

//--------------------------------------------------------------------------

// Gets bones in optimzal order ready for skeleton LOD
void rawanim::PutBonesInLODOrder( void )
{
    // Create temp bones
    xptr<temp_bone> TempBone;
    TempBone.New( m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );

    //
    // Initialize structures
    //
    for ( temp_bone& TBone : TempBone )
    {
        const s32   Index   = s32(&TBone - &TempBone[0]);
        const bone& Bone    = m_Bone[ Index ];

        *(bone*)&TBone      = Bone;
        TBone.m_iBoneRemap  = Index;
        TBone.m_iBone       = Index;

        //
        // Set the LOD group of the bone
        //
        s32 iLodGroupStart = Bone.m_Name.FindI( "LOD[" );
        if ( iLodGroupStart != -1 )
        {
            s32 iLodGroupEnd = Bone.m_Name.FindI( "]", iLodGroupStart );
            if ( iLodGroupEnd == -1 )
                x_throw( "ERROR: We found a bone[%s] with an LOD group but with a missing ']' ",
                    (const char*) Bone.m_Name );

            char Buffer[32];
            s32  Length = iLodGroupEnd - iLodGroupStart;
            ASSERT( Length < sizeof(Buffer) );

            x_strncpy( Buffer, &Bone.m_Name[ iLodGroupStart + 4 ], Length, sizeof(Buffer) );

            TBone.m_LODGroup = x_atod32( Buffer, 10 );
            ASSERT( TBone.m_LODGroup >= 0 );
            ASSERT( TBone.m_LODGroup <= 1000 );
        }
        else
        {
            if ( TBone.m_iParent == -1 ) 
                TBone.m_LODGroup = -2;
            else
                TBone.m_LODGroup = -1;
        }

        //
        // Setup depths
        //
        TBone.m_Depth = 0;
        for( s32 i = TBone.m_iParent; i != -1; i = m_Bone[i].m_iParent )
        {
            ASSERT( TBone.m_iParent != -1 );

            // Cascade the LOD group down if the user did not specify any new groups
            if ( TBone.m_LODGroup == -1 && TempBone[ TBone.m_iParent ].m_LODGroup >= 0 )
            {
                TBone.m_LODGroup = TempBone[TBone.m_iParent].m_LODGroup;
            }
            else if ( TempBone[ TBone.m_iParent ].m_LODGroup > TBone.m_LODGroup )
            {
                x_printf( "WARNING: You have specify a bone[%s] LOD group that is lower than the parent[%s], we will assign the same group as the parent.",
                    (const char*) TBone.m_Name,
                    (const char*) TempBone[TBone.m_iParent].m_Name );

                TBone.m_LODGroup = TempBone[TBone.m_iParent].m_LODGroup;
            }

            // Increment the depth
            ++TBone.m_Depth;
        }
    }

    //
    // Short bones properly
    //
    x_qsort( TempBone, TempBone.getCount(), sizeof( temp_bone ), []( const void* pA, const void* pB ) -> s32
    {
        const temp_bone& A = *(const temp_bone*)pA;
        const temp_bone& B = *(const temp_bone*)pB;

        if ( A.m_LODGroup < B.m_LODGroup ) return -1;
        if ( A.m_LODGroup > B.m_LODGroup ) return  1;

        if ( A.m_iParent < B.m_iParent ) return -1;
        if ( A.m_iParent > B.m_iParent ) return  1;

        if ( A.m_iBone < B.m_iBone ) return -1;
        return ( A.m_iBone > B.m_iBone );
    } );

    // Setup remap bone indices
    for( s32 i = 0 ; i < TempBone.getCount(); i++ )
        TempBone[ TempBone[i].m_iBone ].m_iBoneRemap = i ;

    // Remap parent indices
    for( s32 i = 0 ; i < TempBone.getCount(); i++ )
    {
        s32 iParent = TempBone[i].m_iParent ;
        if (iParent != -1)
            TempBone[i].m_iParent = TempBone[iParent].m_iBoneRemap ;
    }

    // Copy out the temp bones into the real bones
    for( s32 i = 0 ; i < TempBone.getCount(); i++ )
    {
        m_Bone[i] = TempBone[i];
    }

    // Validate
    for (s32 i = 0 ; i < m_Bone.getCount() ; i++)
    {
        const auto& Bone = m_Bone[i];
        
        // Parent should always be above child!
        if( Bone.m_iParent >= i )
            x_throw("ERROR: Bone LOD Sort has failed!!! Make sure that LOD groups are setup correctly\n") ;
    }

    // Remap all the keys
    xptr<key_frame> NewKeys;
    NewKeys.New( m_KeyFrame.getCount(), XMEM_FLAG_ALIGN_16B );
    const s32 nBones = m_Bone.getCount();
    for ( s32 iKey = 0; iKey < m_KeyFrame.getCount(); iKey++ )
    {
        const s32 iFrame    = iKey / nBones;
        const s32 iBone     = iKey % m_Bone.getCount();
        const s32 iOld      = (iFrame * nBones) + TempBone[ iBone ].m_iBone; 
        
        NewKeys[ iKey ] = m_KeyFrame[ iOld ];
    }

    // set the new list
    m_KeyFrame = NewKeys;

    // Show hierarchy in debug window
    /*
    x_DebugMsg("\n\n\nLOD optimized bone order:\n") ;
    for( s32 i = 0 ; i < m_Bone.getCount() ; i++)
    {
        s32 iParent ;

        // Print indent
        iParent = m_Bone[i].m_iParent ;
        while(iParent != -1)
        {
            x_DebugMsg(" ") ;
            iParent = m_Bone[iParent].m_iParent ;
        }

        // Print name
        iParent = m_Bone[i].m_iParent ;
        ASSERT(iParent < i) ;
        if (iParent != -1)
            x_DebugMsg("%s (Parent=%s)\n", (const char*)m_Bone[i].m_Name, (const char*)m_Bone[iParent].m_Name) ;
        else
            x_DebugMsg("%s\n", (const char*)m_Bone[i].m_Name) ;
    }
    x_DebugMsg("\n\n\n") ;
    */
}

//--------------------------------------------------------------------------

void rawanim::Save( const char* pFileName ) const
{
    xtextfile File;

    //
    // Open the file
    //
    File.OpenForWriting( xstring::BuildFromFormat( pFileName ) );

    File.WriteRecord( "AnimInfo" );
    {
        File.WriteField ( "Name:s", (const char*)m_Name );
        File.WriteField ( "FPS:d", m_FPS );
        File.WriteField ( "nFrames:d", m_nFrames );

        File.WriteLine  ();
    }

    File.WriteRecord( "Skeleton", m_Bone.getCount() );
    for ( const bone& Bone : m_Bone )
    {
        const s32 Index = s32( &Bone - &m_Bone[0] );

        File.WriteField ( "Index:d",        Index);
        File.WriteField ( "Name:s",         (const char*)Bone.m_Name );
        File.WriteField ( "nChildren:d",    Bone.m_nChildren);
        File.WriteField ( "iParent:d",      Bone.m_iParent);
        File.WriteField ( "Scale:fff",      Bone.m_BindScale.m_X,       Bone.m_BindScale.m_Y,       Bone.m_BindScale.m_Z);
        File.WriteField ( "Rotate:ffff",    Bone.m_BindRotation.m_X,    Bone.m_BindRotation.m_Y,    Bone.m_BindRotation.m_Z,    Bone.m_BindRotation.m_W );
        File.WriteField ( "Pos:fff",        Bone.m_BindTranslation.m_X, Bone.m_BindTranslation.m_Y, Bone.m_BindTranslation.m_Z);
        File.WriteField ( "bScaleKeys:d",   Bone.m_bScaleKeys);
        File.WriteField ( "bRotKeys:d",     Bone.m_bRotationKeys);
        File.WriteField ( "bPosKeys:d",     Bone.m_bTranslationKeys);

        File.WriteLine  ();
    }

    File.WriteRecord( "KeyFrames", m_KeyFrame.getCount() );
    for( const key_frame& Frame : m_KeyFrame )
    {
        const s32 Index  = s32(&Frame - &m_KeyFrame[0]);
        const s32 iFrame = Index / m_Bone.getCount();
        const s32 iBone  = Index % m_Bone.getCount();

        File.WriteField( "iKey:d",      Index     );
        File.WriteField( "iBone:d",     iBone     );
        File.WriteField( "iFrame:d",    iFrame    );
        File.WriteField( "Scale:fff",       Frame.m_Scale.m_X,          Frame.m_Scale.m_Y,      Frame.m_Scale.m_Z );
        File.WriteField( "Rotate:ffff",     Frame.m_Rotation.m_X,       Frame.m_Rotation.m_Y,   Frame.m_Rotation.m_Z,   Frame.m_Rotation.m_W );
        File.WriteField( "Translate:fff",   Frame.m_Translation.m_X,    Frame.m_Translation.m_Y,Frame.m_Translation.m_Z );

        File.WriteLine  ();
    }
}

//--------------------------------------------------------------------------

xbool rawanim::Load( const char* pFileName )
{
    xtextfile File;

    //
    // Open the file
    //
    File.OpenForReading( xstring::BuildFromFormat( pFileName ) );

    while( File.ReadRecord() )
    {
        if ( x_strcmp( File.GetRecordName(), "AnimInfo" ) == 0 )
        {
            File.ReadLine();

            File.ReadFieldXString ( "Name:s", m_Name );
            File.ReadField ( "FPS:d", &m_FPS );
            File.ReadField ( "nFrames:d", &m_nFrames );
        }
        else if( x_strcmp( File.GetRecordName(), "Skeleton" ) == 0 )
        {
            const s32 nBones = File.GetRecordCount();
            m_Bone.New( nBones, XMEM_FLAG_ALIGN_16B );

            for( bone& Bone : m_Bone )
            {
                s32 iBone;

                File.ReadLine();

                File.ReadField          ( "Index:d",        &iBone);
                File.ReadFieldXString   ( "Name:s",         Bone.m_Name);
                File.ReadField          ( "nChildren:d",    &Bone.m_nChildren);
                File.ReadField          ( "iParent:d",      &Bone.m_iParent);
                File.ReadField          ( "Scale:fff",      &Bone.m_BindScale.m_X, &Bone.m_BindScale.m_Y,&Bone.m_BindScale.m_Z);
                File.ReadField          ( "Rotate:ffff",    &Bone.m_BindRotation.m_X,&Bone.m_BindRotation.m_Y,&Bone.m_BindRotation.m_Z,&Bone.m_BindRotation.m_W );
                File.ReadField          ( "Pos:fff",        &Bone.m_BindTranslation.m_X,&Bone.m_BindTranslation.m_Y,&Bone.m_BindTranslation.m_Z);
                File.ReadField          ( "bScaleKeys:d",   &Bone.m_bScaleKeys);
                File.ReadField          ( "bRotKeys:d",     &Bone.m_bRotationKeys);
                File.ReadField          ( "bPosKeys:d",     &Bone.m_bTranslationKeys);

                // read in if bone is part of a layer
                Bone.m_bIsMasked = FALSE;
                File.ReadField("bIsMasked", &Bone.m_bIsMasked);

                // Build bind matrices
                Bone.m_BindMatrix.setup( Bone.m_BindScale,
                                         Bone.m_BindRotation,
                                         Bone.m_BindTranslation);
                Bone.m_BindMatrixInv = Bone.m_BindMatrix;
                Bone.m_BindMatrixInv.InvertSRT();
            }
        }
        else if( x_strcmp( File.GetRecordName(), "KeyFrames" ) == 0 )
        {
            const s32 nKeys              = File.GetRecordCount();
            const s32 nBones             = m_Bone.getCount();
            
            // Alloc new data
            ASSERT( m_nFrames = nKeys / nBones );
            m_KeyFrame.New( m_nFrames * m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );

            // Data for each loop
            for( key_frame& Frame : m_KeyFrame )
            {
                s32     iFrame;
                s32     iBone;
                s32     iKey;

                File.ReadLine();

                File.ReadField( "iKey:d",   &iKey      );
                File.ReadField( "iBone:d",  &iBone     );
                File.ReadField( "iFrame:d", &iFrame    );

                File.ReadField( "Scale:fff",        &Frame.m_Scale.m_X,         &Frame.m_Scale.m_Y,         &Frame.m_Scale.m_Z );
                File.ReadField( "Rotate:ffff",      &Frame.m_Rotation.m_X,      &Frame.m_Rotation.m_Y,      &Frame.m_Rotation.m_Z,      &Frame.m_Rotation.m_W );
                File.ReadField( "Translate:fff",    &Frame.m_Translation.m_X,   &Frame.m_Translation.m_Y,   &Frame.m_Translation.m_Z );
            }
        }
        else if( x_stricmp( File.GetRecordName(), "Events" ) == 0 )
		{
		}
        else if( x_stricmp( File.GetRecordName(), "SuperEvents" ) == 0 )
		{
		}
        else if( x_stricmp( File.GetRecordName(), "Props" ) == 0 )
		{
		}
        else if( x_stricmp( File.GetRecordName(), "PropFrames" ) == 0 )
        {
		}
		else
        {
            File.ReadNextRecord();
        }
    }
    
    SanityCheck();

    return TRUE;
}


//--------------------------------------------------------------------------

void rawanim::ComputeBonesL2W( xmatrix4* pMatrix, f32 Frame ) const
{
    s32 i;

    // Keep frame in range
    Frame = x_FMod(Frame,(f32)(m_nFrames-1));

    // Compute integer and 
    s32 iFrame0 = (s32)Frame;
    s32 iFrame1 = (iFrame0+1)%m_nFrames;
    f32 fFrame  = Frame - iFrame0;

    // Loop through bones and build matrices
    const key_frame* pF0 = &m_KeyFrame[ iFrame0*m_Bone.getCount() ];
    const key_frame* pF1 = &m_KeyFrame[ iFrame1*m_Bone.getCount() ];
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        xquaternion R = pF0->m_Rotation.BlendAccurate( fFrame, pF1->m_Rotation );
        xvector3    S = pF0->m_Scale       + fFrame*(pF1->m_Scale       - pF0->m_Scale);
        xvector3    T = pF0->m_Translation + fFrame*(pF1->m_Translation - pF0->m_Translation);
        pF0++;
        pF1++;
        pMatrix[i].setup( S, R, T );

        // Concatenate with parent
        if( m_Bone[i].m_iParent != -1 )
        {
            ASSERT( m_Bone[i].m_iParent < i );
            pMatrix[i] = pMatrix[ m_Bone[i].m_iParent ] * pMatrix[i];
        }
    }

    // Apply bind matrices
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        pMatrix[i] = pMatrix[i] * m_Bone[i].m_BindMatrixInv;
    }
}

//--------------------------------------------------------------------------

void rawanim::ComputeBonesL2W( xmatrix4*    pMatrix,
                               s32          iFrame,
                               xbool        bRemoveHorizMotion,
                               xbool        bRemoveVertMotion,
                               xbool        bRemoveYawMotion ) const
{
    s32 i;

    // Keep frame in range
    iFrame = iFrame % (m_nFrames-1) ;

    // Loop through bones and build matrices
    const key_frame* pF0 = &m_KeyFrame[ iFrame * m_Bone.getCount() ];
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        // Root bone mayhem?
        if (i == 0)
        {
            // Lookup info
            xvector3     Scale = pF0->m_Scale ;
            xvector3     Trans = pF0->m_Translation ;
            xquaternion  Rot   = pF0->m_Rotation ;

            // Remove horiz motion?
            if( bRemoveHorizMotion )
                Trans.m_X = Trans.m_Z = 0.0f ;

            // Remove vert motion?
            if( bRemoveVertMotion )
                Trans.m_Y = 0.0f ;

            // Remove yaw motion?
            if( bRemoveYawMotion )
            {
                // TO DO...
            }

            // Setup matrix from frame
            pMatrix[i].setup( Scale, Rot, Trans );
        }
        else
        {
            // Setup matrix from frame
            pMatrix[i].setup( pF0->m_Scale, pF0->m_Rotation, pF0->m_Translation );
        }

        // Next frame
        pF0++;

        // Concatenate with parent
        if( m_Bone[i].m_iParent != -1 )
            pMatrix[i] = pMatrix[ m_Bone[i].m_iParent ] * pMatrix[i];
    }

    // Apply bind matrices
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        pMatrix[i] = pMatrix[i] * m_Bone[i].m_BindMatrixInv;
    }
}

//--------------------------------------------------------------------------

void rawanim::ComputeBoneL2W( s32 iBone, xmatrix4& Matrix, f32 Frame ) const
{
    // Keep frame in range
    Frame = x_FMod( Frame, (f32)( m_nFrames - 1) );

    // Compute integer and 
    s32 iFrame0 = (s32)Frame;
    s32 iFrame1 = (iFrame0+1)%m_nFrames;
    f32 fFrame  = Frame - iFrame0;

    // Loop through bones and build matrices
    const key_frame* pF0 = &m_KeyFrame[ iFrame0 * m_Bone.getCount() ];
    const key_frame* pF1 = &m_KeyFrame[ iFrame1 * m_Bone.getCount() ];

    // Clear bone matrix
    Matrix.Identity();

    // Run hierarchy from bone to root node
    s32 I = iBone;
    while( I != -1 )
    {
        xquaternion R = pF0[I].m_Rotation.BlendAccurate( fFrame, pF1[I].m_Rotation );
        xvector3    S = pF0[I].m_Scale       + fFrame*(pF1[I].m_Scale       - pF0[I].m_Scale);
        xvector3    T = pF0[I].m_Translation + fFrame*(pF1[I].m_Translation - pF0[I].m_Translation);

        xmatrix4 LM;
        LM.setup( S, R, T );

        Matrix = LM * Matrix;
        I = m_Bone[I].m_iParent;
    }

    // Apply bind matrix
    Matrix = Matrix * m_Bone[iBone].m_BindMatrixInv;
}

//--------------------------------------------------------------------------

void rawanim::ComputeRawBoneL2W( s32 iBone, xmatrix4& Matrix, s32 iFrame ) const
{
    // Keep frame in range
    ASSERT( (iFrame>=0) && (iFrame<m_nFrames) );

    // Loop through bones and build matrices
    const key_frame* pF = &m_KeyFrame[ iFrame * m_Bone.getCount() ];

    // Clear bone matrix
    Matrix.Identity();

    // Run hierarchy from bone to root node
    s32 I = iBone;
    while( I != -1 )
    {
        xquaternion R = pF[I].m_Rotation;
        xvector3    S = pF[I].m_Scale;
        xvector3    T = pF[I].m_Translation;

        xmatrix4 LM;
        LM.setup( S, R, T );

        Matrix = LM * Matrix;
        I = m_Bone[I].m_iParent;
    }

    // Apply bind matrix
    Matrix = Matrix * m_Bone[iBone].m_BindMatrixInv;
}

//--------------------------------------------------------------------------

void rawanim::ComputeBoneKeys( xquaternion* pQ, xvector3* pS, xvector3* pT, f32 Frame ) const
{
    s32 i;

    // Keep frame in range
    Frame = x_FMod(Frame,(f32)(m_nFrames-1));

    // Compute integer and 
    s32 iFrame0 = (s32)Frame;
    s32 iFrame1 = (iFrame0+1)%m_nFrames;
    f32 fFrame  = Frame - iFrame0;

    // Loop through bones and build matrices
    const key_frame* pF0 = &m_KeyFrame[ iFrame0 * m_Bone.getCount() ];
    const key_frame* pF1 = &m_KeyFrame[ iFrame1 * m_Bone.getCount() ];
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        pQ[i] = pF0->m_Rotation.BlendAccurate( fFrame, pF1->m_Rotation );
        pS[i] = pF0->m_Scale       + fFrame*(pF1->m_Scale       - pF0->m_Scale);
        pT[i] = pF0->m_Translation + fFrame*(pF1->m_Translation - pF0->m_Translation);
        pF0++;
        pF1++;
    }
}

//--------------------------------------------------------------------------

s32 rawanim::GetBoneIDFromName( const char* pBoneName ) const
{
    s32 i;
    for( i=0; i<m_Bone.getCount(); i++ )
    if( x_stricmp( pBoneName, m_Bone[i].m_Name ) == 0 )
        return i;
    return -1;
}

//--------------------------------------------------------------------------

void rawanim::RemoveFramesFromRage( s32 StartingValidRange, s32 EndingValidRange )
{
    ASSERT( StartingValidRange >= 0 );
    ASSERT( EndingValidRange >= 0 );
    ASSERT( EndingValidRange <= m_nFrames );
    ASSERT( StartingValidRange <= m_nFrames );

    if( StartingValidRange == 0 && EndingValidRange == m_nFrames )
        return;

    xptr<key_frame> NewRange;

    const s32 nFrames = EndingValidRange - StartingValidRange;

    NewRange.New( nFrames * m_Bone.getCount() );
    for( s32 i = StartingValidRange; i <EndingValidRange; i++ )
    {
        const s32 iNewSpace = i - StartingValidRange;
        x_memcpy( &NewRange[ iNewSpace * m_Bone.getCount() ], &m_KeyFrame[ i * m_Bone.getCount()], sizeof(key_frame) * m_Bone.getCount() );
    }     

    // Set the new key frames
    m_KeyFrame = NewRange;
    m_nFrames  = nFrames;
}

//--------------------------------------------------------------------------

void rawanim::BakeBindingIntoFrames( xbool DoScale, xbool DoRotation, xbool DoTranslation )
{
    s32 i,j;

    //
    // Loop through frames of animation
    //
    xptr<xmatrix4> L2W;
    L2W.New( m_Bone.getCount() );

    for( i=0; i<m_nFrames; i++ )
    {
        //
        // Compute matrices for current animation.
        // No binding is applied
        //
        for( j=0; j<m_Bone.getCount(); j++ )
        {
            key_frame* pF = &m_KeyFrame[ i*m_Bone.getCount() + j ];

            L2W[j].setup( pF->m_Scale, pF->m_Rotation, pF->m_Translation );

            // Concatenate with parent
            if( m_Bone[j].m_iParent != -1 )
            {
                L2W[j] = L2W[m_Bone[j].m_iParent] * L2W[j];
            }
        }

        //
        // Apply original bind matrices
        //
        for( j=0; j<m_Bone.getCount(); j++ )
        {
            L2W[j] = L2W[j] * m_Bone[j].m_BindMatrixInv;
        }

        //
        // Remove bind translation and scale matrices
        //
        for( j=0; j<m_Bone.getCount(); j++ )
        {
            xquaternion R = m_Bone[j].m_BindRotation;
            xvector3    S = m_Bone[j].m_BindScale;
            xvector3    T = m_Bone[j].m_BindTranslation;

            if( DoScale )       S.Set(1,1,1);
            if( DoRotation )    R.Identity();
            if( DoTranslation ) T.Set(0,0,0);

            xmatrix4 BindMatrix;
            BindMatrix.setup( S, R, T );
            L2W[j] = L2W[j] * BindMatrix;
        }

        // Convert back to local space transform
        for( j = m_Bone.getCount()-1; j>0; j-- )
            if( m_Bone[j].m_iParent != -1 )
            {
                xmatrix4 PM = L2W[ m_Bone[j].m_iParent ];
                PM.InvertSRT();
                L2W[j] = PM * L2W[j];
            }

        // Pull out rotation scale and translation
        for( j=0; j<m_Bone.getCount(); j++ )
        {
            key_frame* pF       = &m_KeyFrame[i * m_Bone.getCount() + j ];
            
            pF->m_Scale         = L2W[j].getScale();
            pF->m_Rotation.setup( L2W[j] );
            pF->m_Translation   = L2W[j].getTranslation();
            
        }
    }

    // Remove wanted attributes out of the binding
    for( i=0; i<m_Bone.getCount(); i++ )
    {
        if ( DoTranslation )
            m_Bone[ i ].m_BindTranslation.Set(0);

        if( DoScale ) 
            m_Bone[i].m_BindScale.Set(1,1,1);

        if( DoRotation )
            m_Bone[i].m_BindRotation.Identity();

        m_Bone[i].m_BindMatrix.setup( m_Bone[i].m_BindScale, m_Bone[i].m_BindRotation, m_Bone[i].m_BindTranslation );
        m_Bone[i].m_BindMatrixInv = m_Bone[i].m_BindMatrix;
        m_Bone[i].m_BindMatrixInv.InvertSRT();
    }
}

//--------------------------------------------------------------------------

void rawanim::DeleteDummyBones( void )
{
    s32 iBone = 0;
    while( iBone < m_Bone.getCount() )
    {
        xstring S( m_Bone[iBone].m_Name );

        if(S.FindI("dummy") != -1)
        {
            //Check if it is the root.  If it is, make sure it is not the only root; that is,
            // we can only delete a root bone if it only has one child (because then its child
            // can become the new root)
            if(m_Bone[iBone].m_iParent == -1)
            {
                s32 nChildren = 0;
                for(s32 count = 0; count < m_Bone.getCount(); count++)
                {
                    if( m_Bone[count].m_iParent == iBone )
                        nChildren++;
                }

                if(nChildren == 1)
                {
                    //x_DebugMsg("Bone is root, but can be removed: '%s'\n", m_pBone[iBone].Name);                
                    DeleteBone(iBone);
                    iBone = 0;
                }
                else
                {
                    //x_DebugMsg("Bone is sole remaining root: '%s'\n", m_pBone[iBone].Name);
                    iBone++;
                }
            }
            else
            {
                DeleteBone(iBone);
                iBone = 0;
            }
        }
        else
        {
            iBone++;
        }
    }

/*
    for(iBone = 0; iBone < m_nBones; iBone++)
    {
        x_DebugMsg("Bone Index: %3d Parent: %3d Name: '%s'\n", iBone, m_pBone[iBone].iParent, m_pBone[iBone].Name);
    }
*/
}
    
//--------------------------------------------------------------------------

void rawanim::DeleteBone          ( const char* pBoneName )
{
    s32 iBone = this->GetBoneIDFromName(pBoneName);
    if(iBone != -1)
        DeleteBone(iBone);
    return;
}

//--------------------------------------------------------------------------

void rawanim::DeleteBone( s32 iBone )
{
    //x_DebugMsg("Deleting bone: '%s'\n", m_pBone[iBone].Name);
    s32 i,j;
    //ASSERTS( m_Bone.getCount() > 1, TempDebugFileName );

    //
    // Allocate new bones and frames
    //
    s32                 nNewBones = m_Bone.getCount()-1;
    xptr<bone>          NewBone;
    xptr<key_frame>     NewFrame;
    
    NewBone.New( nNewBones, XMEM_FLAG_ALIGN_16B );
    NewFrame.New( nNewBones * m_nFrames, XMEM_FLAG_ALIGN_16B );
    
    //
    // Check and see if bone has any children
    //
    xbool HasChildren = FALSE;
    for( i=0; i<m_Bone.getCount(); i++ )
        if( m_Bone[i].m_iParent == iBone )
            HasChildren = TRUE;

    //
    // Build new hierarchy
    //
    {
        // Copy over remaining bones
        j=0;
        for( i=0; i<m_Bone.getCount(); i++ )
            if( i != iBone )
            {
                NewBone[j] = m_Bone[i];
                j++;
            }

        // Patch children of bone
        for( i=0; i<nNewBones; i++ )
            if( NewBone[i].m_iParent == iBone )
            {
                NewBone[i].m_iParent = m_Bone[iBone].m_iParent;
            }

        // Patch references to any bone > iBone
        for( i=0; i<nNewBones; i++ )
            if( NewBone[i].m_iParent > iBone )
            {
                NewBone[i].m_iParent--;
            }
    }


    // 
    // If there were no children then we can quickly copy over the keys
    //
    if( !HasChildren )
    {
        //
        // Loop through frames of animation
        //
        s32 k=0;
        for( i=0; i<m_nFrames; i++ )
        for( j=0; j<m_Bone.getCount(); j++ )
        {
            if( j!=iBone )
                NewFrame[k++] = m_KeyFrame[ i*m_Bone.getCount() + j ];
        }
    }
    else
    {
        //
        // Loop through frames of animation
        //
        xptr<xmatrix4> L2W;
        L2W.Alloc( m_Bone.getCount() );

        for( i=0; i<m_nFrames; i++ )
        {
            // Compute matrices for current animation.
            for( j=0; j<m_Bone.getCount(); j++ )
            {
                const key_frame* pF = &m_KeyFrame[ i*m_Bone.getCount()+j ];

                L2W[j].setup( pF->m_Scale, pF->m_Rotation, pF->m_Translation );

                // Concatenate with parent
                if( m_Bone[j].m_iParent != -1 )
                {
                    L2W[j] = L2W[ m_Bone[j].m_iParent ] * L2W[j];
                }
            }

            // Apply original bind matrices
            //for( j=0; j<m_Bone.getCount(); j++ )
            //{
            //    L2W[j] = L2W[j] * m_Bone[j].m_BindMatrixInv;
            //}

            // Shift bones down to align with NewBones
            if( iBone != (m_Bone.getCount()-1) ) 
                x_memmove( &L2W[iBone], &L2W[ iBone+1 ], sizeof(xmatrix4)*(m_Bone.getCount()-iBone-1) );

            //for( j=iBone+1; j<m_Bone.getCount(); j++ )
            //    L2W[j-1] = L2W[j];


            // Remove bind translation and scale matrices
            //for( j=0; j<nNewBones; j++ )
            //{
            //    L2W[j] = L2W[j] * NewBone[j].m_BindMatrix;
            //}

            // Convert back to local space transform
            for( j=nNewBones-1; j>0; j-- )
                if( NewBone[j].m_iParent != -1 )
                {
                    xmatrix4 PM = L2W[ NewBone[j].m_iParent ];
                    PM.InvertSRT();
                    L2W[j] = PM * L2W[j];
                }

            // Pull out rotation scale and translation
            for( j=0; j<nNewBones; j++ )
            {
                key_frame* pF       = &NewFrame[i*nNewBones + j];
                
                pF->m_Scale = L2W[j].getScale();
                pF->m_Rotation.setup( L2W[j] );
                pF->m_Translation = L2W[j].getTranslation();
            }
        }
    }

    // free current allocations
    m_Bone.Destroy();
    m_KeyFrame.Destroy();

    m_Bone     = NewBone;
    m_KeyFrame = NewFrame;    
}

//--------------------------------------------------------------------------

xbool rawanim::ApplyNewSkeleton( const rawanim& BindAnim )
{
    s32 i,j,k;
    xbool Problem=FALSE;

    //
    // Handle setting the new root
    //
    {
        ASSERT( -1 == BindAnim.m_Bone[0].m_iParent );
        s32 iRoot = GetBoneIDFromName( BindAnim.m_Bone[0].m_Name );
        if( iRoot == -1 )
            x_throw("ERROR: Failt to apply the new skeleton because I was not able to find the rootbone in the old skeleton");

        // Set the new root first then we can deal with more complex bone removal stuff
        if( iRoot != 0 )
            SetNewRoot( iRoot );
    }

    //
    // Remove all bones not in BindAnim
    //
	i = 0;
	while( i < m_Bone.getCount() )
	{
		for( j=0; j<BindAnim.m_Bone.getCount(); j++ )
		if( x_stricmp( m_Bone[i].m_Name, BindAnim.m_Bone[j].m_Name ) == 0 )
			break;

		if( j == BindAnim.m_Bone.getCount() )
		{
			if( m_Bone.getCount() == 1 )
			{
				x_throw( "ERROR: has no bones in bind." );
			}

			DeleteBone( i );
			i=-1;
		}

		i++;
	}


    //
    // Allocate new bones and frames
    //
    xptr<bone>  NewBone;
    xptr<key_frame> NewFrame;
    NewBone.New( BindAnim.m_Bone.getCount(), XMEM_FLAG_ALIGN_16B );
    NewFrame.New( BindAnim.m_Bone.getCount() * m_nFrames, XMEM_FLAG_ALIGN_16B );
    
    //
    // Copy over bind skeleton
    //
    x_memcpy( NewBone, BindAnim.m_Bone, sizeof(bone)*BindAnim.m_Bone.getCount() );

    //
    // Construct frames
    //
    for( i=0; i<BindAnim.m_Bone.getCount(); i++ )
    {
        // Lookup bone in current anim
        for( j=0; j<m_Bone.getCount(); j++ )
            if( x_stricmp( m_Bone[j].m_Name, NewBone[i].m_Name ) == 0 )
                break;

        // Check if bone is present
        if( j == m_Bone.getCount() )
        {
            Problem = TRUE;

            // No bone present.  
            // Copy over first frame of BindAnim

            key_frame Key;
            Key.m_Rotation = BindAnim.m_Bone[i].m_BindRotation;
            Key.m_Scale    = BindAnim.m_Bone[ i ].m_BindScale;
            Key.m_Translation.Set(0);

            for( k=0; k<m_nFrames; k++ )
                NewFrame[k*BindAnim.m_Bone.getCount() + i] = Key; //.Identity();// = BindAnim.m_KeyFrame[i];
        }
        else
        {
            // Copy IsLayer over to new bones
            NewBone[i].m_bIsMasked = m_Bone[j].m_bIsMasked;

            // Copy data into new bone slot
            for( k=0; k<m_nFrames; k++ )
                NewFrame[k*BindAnim.m_Bone.getCount() + i] = m_KeyFrame[k*m_Bone.getCount()+j];
        }
    }

    //
    // Free current allocations and provide new ones
    //
    m_Bone.Destroy();
    m_KeyFrame.Destroy();
    
    m_Bone      = NewBone;
    m_KeyFrame  = NewFrame;

    return !Problem;
}

//--------------------------------------------------------------------------

xbool rawanim::HasSameSkeleton( const rawanim& Anim ) const
{
    s32 i;

    if( m_Bone.getCount() != Anim.m_Bone.getCount() )
        return FALSE;

    for( i=0; i<m_Bone.getCount(); i++ )
    {
        const bone& B0 = m_Bone[i];
        const bone& B1 = Anim.m_Bone[i];

        if( x_stricmp( B0.m_Name, B1.m_Name ) != 0 )
            return FALSE;

        if( B0.m_iParent != B1.m_iParent )
            return FALSE;

        if( B0.m_nChildren != B1.m_nChildren )
            return FALSE;

        for( s32 j=0; j<4*4; j++ )
        {
            f32 D = x_Abs( ((f32*)&B0)[j] - ((f32*)&B1)[j] );
            if( D > 0.0001f )
                return FALSE;
        }
    }

    return TRUE;
}

//--------------------------------------------------------------------------

void rawanim::SanityCheck( void ) const
{
    ASSERT( (m_Bone.getCount()>0) && (m_Bone.getCount()<2048) );
    ASSERT( (m_nFrames>=0) && (m_nFrames<65536) );
}

//--------------------------------------------------------------------------

xbool rawanim::IsMaskedAnim( void ) const
{
    for( s32 i=0; i<m_Bone.getCount(); i++ )
        if( m_Bone[i].m_bIsMasked )
            return TRUE;

    return FALSE;
}

//--------------------------------------------------------------------------

void rawanim::CleanUp( void )
{
    PutBonesInLODOrder();
    DeleteDummyBones();
}

//--------------------------------------------------------------------------

void rawanim::SetNewRoot( s32 Index )
{

    //
    // Allocate new bones and frames
    //
    const s32           nParentsBones   = Index;
    const s32           nNewBones       = m_Bone.getCount() - nParentsBones;
    xptr<bone>          NewBone;
    xptr<key_frame>     NewFrame;

    NewBone.New( nNewBones, XMEM_FLAG_ALIGN_16B );
    NewFrame.New( nNewBones * m_nFrames, XMEM_FLAG_ALIGN_16B );

    //
    // Build new hierarchy
    //
    {
        // Copy over remaining bones
        for( s32 i=0; i<nNewBones; i++ )
            NewBone[i] = m_Bone[ Index + i ];

        // Patch children of bone
        for ( s32 i = 1; i < nNewBones; i++ )
            if ( NewBone[ i ].m_iParent < Index )
                x_throw("ERROR: While setting the new root bone I found children accessing its parents");
          
        // Patch references to any bone > Index
        NewBone[ 0 ].m_iParent = -1;
        for ( s32 i = 1; i<nNewBones; i++ )
        {
            NewBone[ i ].m_iParent -= nParentsBones;
            ASSERT( NewBone[ i ].m_iParent >=0 );
        }
    }

    //
    // Loop through frames of animation
    //
    xptr<xmatrix4> L2W;
    L2W.Alloc( m_Bone.getCount() );

    for ( s32 iFrame = 0; iFrame < m_nFrames; iFrame++ )
    {
        // Compute matrices for current animation.
        for ( s32 j = 0; j < m_Bone.getCount(); j++ )
        {
            const key_frame& F = m_KeyFrame[ iFrame*m_Bone.getCount() + j ];

            L2W[ j ].setup( F.m_Scale, F.m_Rotation, F.m_Translation );

            // Concatenate with parent
            if ( m_Bone[ j ].m_iParent != -1 )
            {
                L2W[ j ] = L2W[ m_Bone[ j ].m_iParent ] * L2W[ j ];
            }
        }

        // Shift bones down to align with NewBones
        x_memmove( &L2W[0], &L2W[ Index ], sizeof(xmatrix4)*nNewBones );

        // Convert back to local space transform
        for ( s32 j = nNewBones - 1; j>0; j-- )
        {
            ASSERT( NewBone[ j ].m_iParent != -1 );
            xmatrix4 PM = L2W[ NewBone[ j ].m_iParent ];
            PM.InvertSRT();
            L2W[ j ] = PM * L2W[ j ];
        }

        // Pull out rotation scale and translation
        for (s32  j = 0; j < nNewBones; j++ )
        {
            key_frame& Frame = NewFrame[ iFrame*nNewBones + j ];

            Frame.m_Scale = L2W[ j ].getScale();
            Frame.m_Rotation.setup( L2W[ j ] );
            Frame.m_Translation = L2W[ j ].getTranslation();

        }
    }

    // set the new data
    m_Bone      = NewBone;
    m_KeyFrame  = NewFrame;
}


//--------------------------------------------------------------------------

void rawanim::CopyFrames( xptr<key_frame>& KeyFrame, s32 iStart, s32 nFrames ) const
{
    ASSERT( iStart >= 0 );
    ASSERT( nFrames > 0 );
    ASSERT( iStart + nFrames <= m_nFrames );

    const s32 nBones = m_Bone.getCount();
    KeyFrame.Alloc( nBones * nFrames );
    x_memcpy( &KeyFrame[0], &m_KeyFrame[ iStart * nBones ], sizeof(key_frame)*nBones*nFrames );
}

//--------------------------------------------------------------------------

void rawanim::InsertFrames( s32 iDestFrame, xptr<key_frame>& KeyFrame )
{
    ASSERT( iDestFrame >= 0 );
    if( KeyFrame.getCount() == 0 )
        return;

    ASSERT( KeyFrame.getCount() > 0 );
    ASSERT( iDestFrame <= m_nFrames );

    const s32 nBones        = m_Bone.getCount();
    const s32 oldnFrames    = m_nFrames;

    // Update the number of frames in the anim
    m_nFrames += KeyFrame.getCount()/nBones;

    if( m_KeyFrame.getCount() == 0 )
    {
        m_KeyFrame.Copy( KeyFrame );
        return;
    }

    m_KeyFrame.Resize( m_KeyFrame.getCount() + KeyFrame.getCount() );
    
    if( iDestFrame != oldnFrames )
    {
        x_memmove( &m_KeyFrame[ iDestFrame * nBones + KeyFrame.getCount() ], 
                   &m_KeyFrame[ iDestFrame * nBones ], 
                   sizeof(key_frame)*KeyFrame.getCount() );
    }

    x_memcpy( &m_KeyFrame[ iDestFrame * nBones ], &KeyFrame[0], sizeof(key_frame)*KeyFrame.getCount() );
}

//--------------------------------------------------------------------------

void rawanim::RencenterAnim( xbool TX, xbool TY, xbool TZ, xbool Pitch, xbool Yaw, xbool Roll )
{
    if( TX | TY | TZ )
    {
        const s32       nBones          =   m_Bone.getCount();
        const xvector3  LinearVelocity  =   m_KeyFrame[ nBones * 1 ].m_Translation -
                                            m_KeyFrame[ nBones * 0 ].m_Translation;
        const xvector3  CurrentCenter   =   m_KeyFrame[ nBones * 0 ].m_Translation;
         xvector3       NewCenterDelta(0);

        if( TX )
            NewCenterDelta.m_X = LinearVelocity.m_X - CurrentCenter.m_X;

        if ( TY )
            NewCenterDelta.m_Y = LinearVelocity.m_Y - CurrentCenter.m_Y;

        if ( TZ )
            NewCenterDelta.m_Z = LinearVelocity.m_Z - CurrentCenter.m_Z;

        for( s32 i=0; i<m_nFrames; i++ )
        {
            auto& Frame = m_KeyFrame[ nBones * i ]; 
            Frame.m_Translation += NewCenterDelta; 
        }
    }

    //
    // Re-aligned Rotation
    //
    if( Pitch | Yaw | Roll )
    {
        const s32 nBones          = m_Bone.getCount();
        auto      InvRotation     = xquaternion( m_KeyFrame[ 0 ].m_Rotation ).Invert().getRotation();

        if( !Pitch ) InvRotation.m_Pitch = 0;
        if( !Yaw )   InvRotation.m_Yaw   = 0;
        if( !Roll )  InvRotation.m_Roll  = 0;

        const xquaternion InvRotFiltered( InvRotation );

        for( s32 i=0; i<m_nFrames; i++ )
        {
            auto& Frame = m_KeyFrame[ nBones * i ]; 
            Frame.m_Rotation    = InvRotFiltered * Frame.m_Rotation;
            Frame.m_Translation = InvRotFiltered * Frame.m_Translation;
        }
    }
}

//--------------------------------------------------------------------------

void rawanim::CleanLoopingAnim( void )
{
    const s32           nFrames         = m_nFrames;
    const s32           nBones          = m_Bone.getCount();
    const s32           nAffectedFrames = m_FPS / 19;
    xptr<xtransform>    ContinuesFrames;
    xptr<xtransform>    NewFrames;

    //
    // Create the array of the new frames and fill it with information
    //
    NewFrames.New( nAffectedFrames * 2 * nBones );
    ContinuesFrames.New( nAffectedFrames * 2 * nBones );
     
    //
    // Create the frames in the same loop space
    //
    {
        const auto&         RootLastFrame   = m_KeyFrame[ (nFrames-1) * nBones ];
        //const auto&         RootFirstFrame  = m_KeyFrame[ 0 ];
        const f32           DeltaYaw        = RootLastFrame.m_Rotation.getRotation().m_Yaw;
        const xquaternion   DeltaRot        = xquaternion( xradian3(0,DeltaYaw,0) );

        for( s32 i=0; i<nAffectedFrames*2; i++ )
        {
            const s32       iFrame    = ( nFrames - (nAffectedFrames - i) ) % nFrames;
            auto&           NewFrame  =  ContinuesFrames[ i * nBones ];
            const auto&     Frame     =  m_KeyFrame[iFrame * nBones ];

            // Copy one frame worth of data
            x_memcpy( &NewFrame, &Frame, sizeof(xtransform) * nBones );

            // Set the base frames to be in the space of the space of the last frame loop
            if( iFrame < nAffectedFrames )
            {
                NewFrame.m_Rotation     = DeltaRot * Frame.m_Rotation;

                const xvector3 RelTrans = DeltaRot * Frame.m_Translation;

                NewFrame.m_Translation = RootLastFrame.m_Translation + RelTrans;
                NewFrame.m_Translation.m_Y = Frame.m_Translation.m_Y;
            }
        }
    }

    //
    // Interpolate Frames
    //
    {
        const s32 iFrame0 = 0;
        const s32 iFrame1 = (nAffectedFrames*2)-1;
        for( s32 i=0; i<nAffectedFrames*2; i++ )
        {
            f32 T = (i + 0.5f)/(f32)(nAffectedFrames*2);

            // Blend Frames
            for( s32 b=0;b<nBones;b++)
            {
                const auto      KeyFrame0    =  ContinuesFrames[ iFrame0 * nBones + b ];
                const auto      KeyFrame1    =  ContinuesFrames[ iFrame1 * nBones + b ];
                auto&           KeyFrameDest =  NewFrames[ i       * nBones + b ];
            
                KeyFrameDest.Blend( KeyFrame0, T, KeyFrame1 );
            }
        }
    }

    //
    // New Loops interpolates between the new computed frames and the blends
    //
    for( s32 j=0; j<nAffectedFrames; j++ )
    {
        const s32 nNewAffectedFrames = nAffectedFrames-j;
        const s32 iFrame0 = j;
        const s32 iFrame1 = (nNewAffectedFrames*2)-1;
    
        for( s32 i=0; i<nNewAffectedFrames*2; i++ )
        {
            f32 T = (i + 0.5f)/(f32)(nNewAffectedFrames*2);

            // Blend Frames
            for( s32 b=0;b<nBones;b++)
            {
                const auto&     KeyFrame0     =  ContinuesFrames[ iFrame0 * nBones + b ];
                const auto&     KeyFrame1     =  ContinuesFrames[ iFrame1 * nBones + b ];
                xtransform      KeyFrameDest;

                KeyFrameDest.Blend( KeyFrame0, T, KeyFrame1);
                
                auto&            KeyFrameFinal = NewFrames[ (j+i) * nBones + b ];

                KeyFrameFinal.Blend( 0.5, KeyFrameDest );
            }
        }
    }

    //
    // Save back the frames
    //
    {
        const auto          LastNewFrame  = NewFrames[ (nAffectedFrames-1) * nBones ];
        const auto          FirstNewFrame = NewFrames[ nAffectedFrames * nBones ];  
        const f32           DeltaYaw      = LastNewFrame.m_Rotation.getRotation().m_Yaw; 
        const xquaternion   DeltaRot      = xquaternion( xradian3(0,-DeltaYaw,0) );

        for( s32 i=0; i<nAffectedFrames*2; i++ )
        {
            const s32       iFrame      = ( nFrames - (nAffectedFrames - i) ) % nFrames;
            auto&           NewFrame    =  NewFrames[ i * nBones ];
            auto&           Frame       =  m_KeyFrame[iFrame * nBones ];

            if( iFrame < nAffectedFrames )
            {
                NewFrame.m_Rotation     = DeltaRot * NewFrame.m_Rotation;

                xvector3 Trans    = NewFrame.m_Translation - LastNewFrame.m_Translation;
                Trans.m_Y = NewFrame.m_Translation.m_Y;

                NewFrame.m_Translation = DeltaRot * Trans;
            }

            // Copy one frame worth of data
            x_memcpy( &Frame, &NewFrame, sizeof(xtransform) * nBones );
        }
    }
}
