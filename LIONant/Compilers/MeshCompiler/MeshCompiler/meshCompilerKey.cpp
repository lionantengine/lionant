//
//  fontCompilerKeyObject.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "meshCompilerKey.h"

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void mesh_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "SrcFilePath",      OVERRIDE_MAIN_FILEPATH,                         "This could be a rawmesh or a fbx file.", "*.fbx"                   )
    KEY_PROP_ENUM( "ForceStatic",           OVERRIDE_FORCE_STATIC,          g_PropBool,     "Forces the compile to compile this geom as a Non-Animated object"  )
    KEY_PROP_ENUM( "KeepAttachAnimation",   OVERRIDE_KEEP_ATTACH_ANIMATION, g_PropBool,     "If the source file includes an animation keep it."                 )
    KEY_PROP_ENUM( "nWeights",              OVERRIDE_MAIN_MAX_WEIGHTS,      g_PropInt,      "Animated objects number of bones influence per vertex"             )
    KEY_PROP_ENUM_FILE( "SkeletonFile",     OVERRIDE_SKELETON_FILE,                         "This could be a rawmesh or a fbx file.", "*.fbx"                   )
}

//-------------------------------------------------------------------------------------------------

xbool mesh_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "SrcFilePath",              OVERRIDE_MAIN_FILEPATH,             g_PropFilePath.Query( Query, m_FilePath )                   )
    KEY_PROP_QUERY( "ForceStatic",              OVERRIDE_FORCE_STATIC,              g_PropBool.Query( Query, m_bForceStatic )                   )
    KEY_PROP_QUERY( "KeepAttachAnimation",      OVERRIDE_KEEP_ATTACH_ANIMATION,     g_PropBool.Query( Query, m_bKeepAnimation )                 )
    KEY_PROP_QUERY( "nWeights",                 OVERRIDE_MAIN_MAX_WEIGHTS,          g_PropInt.Query( Query, m_MaxNumberOfWeights, 0, 4 )        )
    KEY_PROP_QUERY( "SkeletonFile",             OVERRIDE_SKELETON_FILE,             g_PropFilePath.Query( Query, m_SkeletonFile )               )

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void mesh_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_FilePath,                 OVERRIDE_MAIN_FILEPATH,             .Copy       )
    KEY_PROP_UPDATE( m_MaxNumberOfWeights,       OVERRIDE_FORCE_STATIC,              =           )
    KEY_PROP_UPDATE( m_bKeepAnimation,           OVERRIDE_KEEP_ATTACH_ANIMATION,     =           )
    KEY_PROP_UPDATE( m_MaxNumberOfWeights,       OVERRIDE_MAIN_MAX_WEIGHTS,          =           )
    KEY_PROP_UPDATE( m_SkeletonFile,             OVERRIDE_SKELETON_FILE,             .Copy       )
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
