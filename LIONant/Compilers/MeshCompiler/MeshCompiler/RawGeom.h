
#ifndef RAW_GEOM_H
#define RAW_GEOM_H

#include "x_base.h"
#include "RawAnim.h"

class rawgeom
{
public:

    enum max
    {
        VERTEX_MAX_UV               = 8,
        VERTEX_MAX_NORMAL           = 3,
        VERTEX_MAX_COLOR            = 4,
        VERTEX_MAX_WEIGHT           = 16,
        FACET_MAX_VERTICES          = 8,
        MATERIAL_MAX_MAPS           = 16,
        MATERIAL_MAX_CONSTANTS      = 8,
        PARAM_PKG_MAX_ITEMS         = 4,
    };

    struct bone
    {
        xstring                                     m_Name;
        s32                                         m_nChildren;
        s32                                         m_iParent;
        xvector3d                                   m_Scale;
        xquaternion                                 m_Rotation;
        xvector3d                                   m_Position;
        xbbox                                       m_BBox;
    };

    struct mesh
    {
        xstring                                     m_Name;
        s32                                         m_nBones;

        const mesh& operator = ( const mesh&A )
        {
            m_Name   = A.m_Name;
            m_nBones = A.m_nBones;
            return *this;
        }
    };

    struct weight
    {
        s32                                         m_iBone;
        f32                                         m_Weight;
    };

    struct btn
    {
        xvector3d                                   m_Binormal;
        xvector3d                                   m_Tangent;
        xvector3d                                   m_Normal;
    };

    struct vertex
    {
        xvector3d                                   m_Position;

        s32                                         m_iFrame        = 0;
        s32                                         m_nWeights      = 0;
        s32                                         m_nNormals      = 0;
        s32                                         m_nTangents     = 0;
        s32                                         m_nBinormals    = 0;
        s32                                         m_nUVs          = 0;
        s32                                         m_nColors       = 0;

        xsafe_array<xvector2,VERTEX_MAX_UV>         m_UV;
        xsafe_array<xcolor,VERTEX_MAX_COLOR>        m_Color;
        xsafe_array<weight,VERTEX_MAX_WEIGHT>       m_Weight;
        xsafe_array<btn, VERTEX_MAX_NORMAL>         m_BTN;
    };

    struct facet
    {
        s32                                         m_iMesh;
        s32                                         m_nVertices;
        xsafe_array<s32,FACET_MAX_VERTICES>         m_iVertex;
        s32                                         m_iInformed;
        xplane                                      m_Plane;
    };

    // Material refers to a material instance not a material-shader/type
    // The material insance has a reference of the material shader such multiple instances
    // could in refer to the same material shader. Ideally a mesh should just point to
    // an actual material instance so that hopefully many meshes use a single instance.
    // this will the best case in terms of performance. Because all the meshes that can be
    // render with the game material instance dont need any state changes except for vertex/index buffers.
    // However most games dont really use this concept very much and most meshes have their own material instances,
    // with custom textures and tweaked parameters.
    struct informed_material
    {
        enum params_type : u8
        {
            PARAM_TYPE_NULL,
            PARAM_TYPE_BOOL,
            PARAM_TYPE_F1,
            PARAM_TYPE_F2,
            PARAM_TYPE_F3,
            PARAM_TYPE_F4,
            PARAM_TYPE_TEXTURE,
        };

        static const char* getTypeString( params_type e )
        {
            static const char* pStrings[] =
            {
                "NULL",
                "BOOL",
                "F1",
                "F2",
                "F3",
                "F4",
                "TEXTURE"
            };

            ASSERT( e <= PARAM_TYPE_TEXTURE );
            const char* pAnswer = pStrings[ e ];

            ASSERT( pAnswer );
            return pAnswer;
        }

        struct params
        {
            xbool operator < ( const params& B ) const
            {
                s32 Answer = x_strcmp( m_Name, B.m_Name );
                return Answer <= 0;
            }

            params_type                             m_Type;
            xsafe_array<char,256>                   m_Name;
            xsafe_array<char,256>                   m_Value;
        };

        informed_material& operator = ( const informed_material& Material )
        {
            if ( this != &Material )
            {
                m_Name           = Material.m_Name;
                m_MaterialShader = Material.m_MaterialShader;
                m_Technique      = Material.m_Technique;
                m_Params.Copy( Material.m_Params );
            }
            return *this;
        }

        xstring                                     m_Name;
        xstring                                     m_MaterialShader;
        xstring                                     m_Technique;
        xarray<params>                              m_Params;
    };

public:

    xbool           Load                        ( const char* pFileName );
    void            Save                        ( const char* pFileName ) const;
    void            Kill                        ( void );
    void            SanityCheck                 ( void ) const;

    void            CleanMesh                   ( s32 iSubMesh = -1 );
    void            CleanWeights                ( s32 MaxNumWeights, f32 MinWeightValue );

    void            CollapseMeshes              ( const char* pMeshName );
    void            CollapseNormals             ( xradian ThresholdAngle = X_RADIAN(20) );

    xbbox           getBBox                     ( void );
    void            ComputeMeshBBox             ( s32 iMesh, xbbox& BBox );
    void            ComputeBoneInfo             ( void ) ;

    xbool           IsolateMesh                 ( s32 iSubmesh, rawgeom& NewMesh, xbool RemoveFromRawMesh = FALSE );
    xbool           IsolateMesh                 ( const char* pMeshName, rawgeom& NewMesh );

    xbool           isBoneUsed                  ( s32 iBone );
    s32             getBoneIDFromName           ( const char* pBoneName ) const;
    void            DeleteBone                  ( s32 iBone );
    void            DeleteBone                  ( const char* pBoneName );
    
    void            ApplyNewSkeleton            ( const rawanim& RawAnim );
    void            ApplyNewSkeleton            ( const rawgeom& Skel );

    void            SortFacetsByMaterial        ( void );
    void            SortFacetsByMeshMaterialBone( void );

    static xbool    TempVCompare                ( const rawgeom::vertex& A, const rawgeom::vertex& B );
    static xbool    CompareFaces                ( const rawgeom::facet&  A, const rawgeom::facet&  B );

    void            PrintStats                  ( void );

public:

    xptr<bone>                  m_Bone;
    xptr<vertex>                m_Vertex;
    xptr<facet>                 m_Facet;
    xptr<informed_material>     m_InformedMaterial;
    xptr<mesh>                  m_Mesh;
};

#endif
