//
//  meshBaseCompiler.cpp
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include"forsyth/forsythtriangleorderoptimizer.h"
#include "FBX2RawGeom.h"
#include "meshCompilerBase.h"
#include "informedMaterialCompilerKey.h"
#include "ArmOptimizer/ArmOptimizer.h"
#include "skeletonCompilerKey.h"

// Make sure to Link With the right library
#ifdef TARGET_PC
    #ifdef X_TARGET_64BIT
        #define ARCHITEXTURE "x64/" 
    #else
        #define ARCHITEXTURE "x86/" 
    #endif

    #ifdef X_DEBUG
        #define ISDEBUG ARCHITEXTURE ## "debug/libfbxsdk-mt" 
    #else
        #define ISDEBUG ARCHITEXTURE ## "release/libfbxsdk-mt"
    #endif

    #pragma comment(lib, ISDEBUG )
#endif

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
class vertex_cache_stats 
{
public:

    // Get Average Cache Miss Ratio
    f32     getACMR             ( void ) const           { return m_nCacheMisses / (f32)m_nFaces; }
    
    // Get Average Transform to Vertex Ratio
    f32     getATVR             ( s32 nVertices ) const  { return m_nCacheMisses / (f32)nVertices; }

    // Compute cache misses
    void    ComputeCacheMisses  ( const u16* pIndices, s32 Count, s32 CacheSize ) { DoComputeCacheMisses( pIndices, Count, CacheSize ); }
    void    ComputeCacheMisses  ( const u32* pIndices, s32 Count, s32 CacheSize ) { DoComputeCacheMisses( pIndices, Count, CacheSize ); }

protected:

    template<class T>
    void DoComputeCacheMisses( T* pIndices, s32 Count, s32 CacheSize )
    {
        if ( Count <= 0 )
        {
            m_nCacheMisses = 0;
            return;
        }

        ASSERT( pIndices );
        ASSERT( CacheSize >= 1 );

        // initialize the cache 
        Init( CacheSize );
        m_nFaces = Count/3;

        for( s32 i = 0;i < Count; i++) 
            AddEntry( pIndices[i] );
    }
    void Init( const s32 CacheSize )
    {
        m_CacheSize    = CacheSize;
        m_nCacheMisses = 0;

        // initialize cache entries to an invalid value 
        for( s32 i = 0; i < m_CacheSize;i++)
            m_lEntry[i] = -1;
    }

    xbool CheckEntry( const s32 iEntry ) const
    {
        for (s32 i = 0;i < m_CacheSize; i++) 
            if ( m_lEntry[i] == iEntry )
                return TRUE;

        return FALSE;
    }

    void AddEntry( const s32 iEntry )
    {
        if ( CheckEntry( iEntry ) )
            return;

        // fifo
        for( s32 i = m_CacheSize; i > 0; --i ) 
            m_lEntry[i] = m_lEntry[i-1];

        m_lEntry[0] = iEntry;
        m_nCacheMisses++;
    }

protected:

    xsafe_array<s32, 128>   m_lEntry;
    s32                     m_CacheSize;
    s32                     m_nCacheMisses;
    s32                     m_nFaces;
};

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void mesh_compiler_base::AddVertex( eng_geom_rsc::temp_vertex_stream& VStream, const rawgeom::vertex& Vertex, const eng_vertex_desc& Desc ) const
{
    ASSERT( Desc.getVertSize() == VStream.m_VertSize );

    u8* const pVert = &VStream.m_pVertices.m_Ptr[ Desc.getVertSize() * VStream.m_nVertices ];

    for ( s32 i = 0; i < Desc.getAttributeCount(); i++ )
    {
        const eng_vertex_desc::attribute& Attribute = Desc.getAttribute( i );

        switch ( Attribute.m_UsageType )
        {
            case eng_vertex_desc::ATTR_USAGE_POSITION:
            {
                eng_geom_rsc::v_pos_f32 V;

                ASSERT( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_F32x3 );

                V.m_Position[ 0 ] = Vertex.m_Position.m_X;
                V.m_Position[ 1 ] = Vertex.m_Position.m_Y;
                V.m_Position[ 2 ] = Vertex.m_Position.m_Z;

                x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof( V ) );

                break;
            }
            case eng_vertex_desc::ATTR_USAGE_4_INDICES:
            case eng_vertex_desc::ATTR_USAGE_4_WEIGHTS:
            {
                rawgeom::weight RawWeight[4] = { Vertex.m_Weight[ 0 ], Vertex.m_Weight[ 1 ], Vertex.m_Weight[ 2 ], Vertex.m_Weight[ 3 ] };

                // make sure that if we dont have info for them it is set to zero
                for ( s32 i = 0; i<4; i++ )
                    if( i >= Vertex.m_nWeights )
                    {
                        RawWeight[i].m_iBone  = 0;
                        RawWeight[i].m_Weight = 0;
                    }

                if( Attribute.m_UsageType == eng_vertex_desc::ATTR_USAGE_4_INDICES )
                {
                    eng_geom_rsc::v_4_bone_index   BI;
                    
                    ASSERT( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_U8x4_I );
                    for ( s32 i = 0; i < 4; i++ )
                        BI.m_BoneIndex[i] = u8(RawWeight[i].m_iBone);

                    // copy to dest
                    x_memcpy( &pVert[Attribute.m_Offset], &BI, sizeof( BI ) );
                }
                else
                {
                    eng_geom_rsc::v_4_bone_weights   W;

                    ASSERT( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_U8x4_F );
                    for ( s32 i = 0; i < 4; i++ )
                        W.m_BoneWeights[i] = u8(x_Min( 0xff,RawWeight[i].m_Weight*0xff));

                    // copy to dest
                    x_memcpy( &pVert[Attribute.m_Offset], &W, sizeof( W ) );
                }
                break;
            }
            case eng_vertex_desc::ATTR_USAGE_2_COMPACT_WEIGHTS:
            {
                rawgeom::weight RawWeight[2] = { Vertex.m_Weight[ 0 ], Vertex.m_Weight[ 1 ] };

                // make sure that if we dont have info for them it is set to zero
                for ( s32 i = 0; i<2; i++ )
                    if( i >= Vertex.m_nWeights )
                    {
                        RawWeight[i].m_iBone  = 0;
                        RawWeight[i].m_Weight = 0;
                    }

                ASSERT( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_U8x4_F );

                {
                    eng_geom_rsc::v_2_bone_and_weight   Weight;

                    Weight.m_BoneInfo[0] = u8(RawWeight[0].m_iBone);
                    Weight.m_BoneInfo[1] = u8(x_Min( 0xff,RawWeight[0].m_Weight*0xff));

                    Weight.m_BoneInfo[2] = u8(RawWeight[1].m_iBone);
                    Weight.m_BoneInfo[3] = u8(x_Min( 0xff, RawWeight[1].m_Weight*0xff));

                    // copy to dest
                    x_memcpy( &pVert[Attribute.m_Offset], &Weight, sizeof( Weight ) );
                }
                break;
            }
            case eng_vertex_desc::ATTR_USAGE_NORMAL:
            {
                eng_geom_rsc::v_normal V;
                
                if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_S8x4_F )
                {
                    V.m_Normal[ 0 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Normal.m_X * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 1 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Normal.m_Y * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 2 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Normal.m_Z * 128, S8_MIN, S8_MAX ) );

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof(V) );
                }
                else
                {
                    ASSERT(0 );
                }

                break;
            }
            case eng_vertex_desc::ATTR_USAGE_BINORMAL:
            {
                eng_geom_rsc::v_normal V;
                
                if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_S8x4_F )
                {
                    V.m_Normal[ 0 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Binormal.m_X * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 1 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Binormal.m_Y * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 2 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Binormal.m_Z * 128, S8_MIN, S8_MAX ) );

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof(V) );
                }
                else
                {
                    ASSERT(0 );
                }

                break;
            }
            case eng_vertex_desc::ATTR_USAGE_TANGENT:
            {
                eng_geom_rsc::v_normal V;
                
                if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_S8x4_F )
                {
                    V.m_Normal[ 0 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Tangent.m_X * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 1 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Tangent.m_Y * 128, S8_MIN, S8_MAX ) );
                    V.m_Normal[ 2 ] = s8( x_Range( Vertex.m_BTN[ 0 ].m_Tangent.m_Z * 128, S8_MIN, S8_MAX ) );

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof(V) );
                }
                else
                {
                    ASSERT(0 );
                }

                break;
            }
            case eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_01_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_02_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_03_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_04_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_05_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_06_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_07_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_08_FULLRANGE_UV:
            case eng_vertex_desc::ATTR_USAGE_09_FULLRANGE_UV:
            {
                const s32              iUV       = Attribute.m_UsageType - eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV;

                if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_F32x2 )
                {
                    eng_geom_rsc::v_uv_f32 V;

                    V.m_UV[ 0 ] = Vertex.m_UV[ iUV ].m_X;
                    V.m_UV[ 1 ] = Vertex.m_UV[ iUV ].m_Y;

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof( V ) );
                }
                else
                {
                    ASSERT(0 );
                }
                break;
            }
            case eng_vertex_desc::ATTR_USAGE_00_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_01_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_02_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_03_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_04_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_05_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_06_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_07_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_08_PARAMETRIC_UV:
            case eng_vertex_desc::ATTR_USAGE_09_PARAMETRIC_UV:
            {
                const s32 iUV = Attribute.m_UsageType - eng_vertex_desc::ATTR_USAGE_00_PARAMETRIC_UV;

                if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_U16x2_F )
                {
                    eng_geom_rsc::v_uv_u16 V;

                    V.m_UV[ 0 ] = u16( x_Range( Vertex.m_UV[ iUV ].m_X * 0xffff, 0, 0xffff ) );
                    V.m_UV[ 1 ] = u16( x_Range( Vertex.m_UV[ iUV ].m_Y * 0xffff, 0, 0xffff ) );

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof( V ) );
                }
                else if ( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_F32x2 )
                {
                    eng_geom_rsc::v_uv_f32 V;

                    V.m_UV[ 0 ] = Vertex.m_UV[ iUV ].m_X;
                    V.m_UV[ 1 ] = Vertex.m_UV[ iUV ].m_Y;

                    x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof( V ) );
                }
                else
                {
                    ASSERT(0 );
                }
                break;
            }
            case eng_vertex_desc::ATTR_USAGE_00_RGBA:
            case eng_vertex_desc::ATTR_USAGE_01_RGBA:
            case eng_vertex_desc::ATTR_USAGE_02_RGBA:
            case eng_vertex_desc::ATTR_USAGE_03_RGBA:
            case eng_vertex_desc::ATTR_USAGE_04_RGBA:
            case eng_vertex_desc::ATTR_USAGE_05_RGBA:
            case eng_vertex_desc::ATTR_USAGE_06_RGBA:
            {
                eng_geom_rsc::v_color   V;
                const s32               iCol = Attribute.m_UsageType - eng_vertex_desc::ATTR_USAGE_00_RGBA;

                ASSERT( Attribute.m_DataType == eng_vertex_desc::ATTR_SRC_U8x4_F );

                V.m_Color[ 0 ] = Vertex.m_Color[ iCol ].m_R;
                V.m_Color[ 1 ] = Vertex.m_Color[ iCol ].m_G;
                V.m_Color[ 2 ] = Vertex.m_Color[ iCol ].m_B;
                V.m_Color[ 3 ] = Vertex.m_Color[ iCol ].m_A;

                x_memcpy( &pVert[Attribute.m_Offset], &V, sizeof( V ) );
                break;
            }
            case eng_vertex_desc::ATTR_USAGE_00_GENERIC_V1:
            case eng_vertex_desc::ATTR_USAGE_01_GENERIC_V1:
            case eng_vertex_desc::ATTR_USAGE_02_GENERIC_V1:
            case eng_vertex_desc::ATTR_USAGE_03_GENERIC_V1:
            case eng_vertex_desc::ATTR_USAGE_04_GENERIC_V1:

            case eng_vertex_desc::ATTR_USAGE_00_GENERIC_V2:
            case eng_vertex_desc::ATTR_USAGE_01_GENERIC_V2:
            case eng_vertex_desc::ATTR_USAGE_02_GENERIC_V2:
            case eng_vertex_desc::ATTR_USAGE_03_GENERIC_V2:
            case eng_vertex_desc::ATTR_USAGE_04_GENERIC_V2:

            case eng_vertex_desc::ATTR_USAGE_00_GENERIC_V3:
            case eng_vertex_desc::ATTR_USAGE_01_GENERIC_V3:
            case eng_vertex_desc::ATTR_USAGE_02_GENERIC_V3:
            case eng_vertex_desc::ATTR_USAGE_03_GENERIC_V3:
            case eng_vertex_desc::ATTR_USAGE_04_GENERIC_V3:

            case eng_vertex_desc::ATTR_USAGE_00_GENERIC_V4:
            case eng_vertex_desc::ATTR_USAGE_01_GENERIC_V4:
            case eng_vertex_desc::ATTR_USAGE_02_GENERIC_V4:
            case eng_vertex_desc::ATTR_USAGE_03_GENERIC_V4:
            case eng_vertex_desc::ATTR_USAGE_04_GENERIC_V4:

            default:
                // REPORT ERROR HERE
                ASSERT( 0 );
        }
    }

    //
    // Increment the total vertex count
    //
    VStream.m_nVertices++;
}

//-------------------------------------------------------------------------------------------------

struct some
{
    s32                         m_iFaceStart;
    s32                         m_iFaceCount;
    xptr2<rawgeom::facet>       m_lFinalFace;
    xptr2<rawgeom::vertex>      m_lFinalVertex;
    xarray2<eng_geom_rsc::cmd>  m_lFinalCmds;
};

void mesh_compiler_base::PreprocessRawGeomWithArmCompiler( 
    compiler_data&                  CompilerData,
    rawgeom&                        Dest, 
    const rawgeom&                  Src ) const
{
    xarray2<some>                   lSubmesh;
    x_inline_light_jobs_block<8>    JobBlock;
    x_qt_counter                    TotalFacets;
    x_qt_counter                    TotalVerts;
    x_qt_counter                    TotalCmd;

    //
    // Create the initial display lists based on informated material
    //
    {
        mutable_ref( rSubmesh, lSubmesh );
        mutable_ref( rSubmeshMap, CompilerData.m_lSubmeshCmdMap );
        s32 iLastInformed = -1;
        s32 iLastMesh     = -1; 
        for ( s32 i = 0; i < Src.m_Facet.getCount(); i++ )
        {
            if ( iLastInformed != Src.m_Facet[ i ].m_iInformed ||
                 iLastMesh     != Src.m_Facet[ i ].m_iMesh )
            {
                const auto& SrcFacet = Src.m_Facet[ i ];

                // set the end of the last one
                if ( i )
                {
                    some& DList = rSubmesh[ lSubmesh.getCount()-1 ];
                    DList.m_iFaceCount = i - DList.m_iFaceStart;
                }

                // New DList
                some& DList = rSubmesh->append();
                DList.m_iFaceStart   = i;
                DList.m_iFaceCount   = Src.m_Facet.getCount() - i;

                // Set the map
                submesh_cmd_map& SubmeshMap = rSubmeshMap->append();
                SubmeshMap.m_iInformed = SrcFacet.m_iInformed;
                SubmeshMap.m_iMesh     = SrcFacet.m_iMesh;

                // set the new informed
                iLastInformed = SrcFacet.m_iInformed;
                iLastMesh     = SrcFacet.m_iMesh;
            }
        }
    }

    //
    // Optimize the rawgeom into sections
    //
    xptr2<rawgeom::facet>  Face;
    xptr2<rawgeom::vertex> Vertex;

    Face.Alloc( Src.m_Facet.getCount() );
    Vertex.Alloc( Src.m_Vertex.getCount()*2 );
//    s32 iFacet  = 0;
//    s32 iVertex = 0;
    {
        lSubmesh.ChangeBehavior( xptr2_flags::FLAGS_QT_MUTABLE );
        mutable_ref( rSubmesh, lSubmesh );
        for ( auto& DList : rSubmesh )
        {
            const s32 Index = rSubmesh.getIterator();
            JobBlock.SubmitJob( [Index, &Src, &DList, &CompilerData, &TotalVerts, &TotalFacets, &TotalCmd ]()
            {
                arm_optimizer Optimizer;
                Optimizer.Build( 
                    Src, 
                    DList.m_iFaceStart, 
                    DList.m_iFaceCount, 
                    CompilerData.m_MartrixCacheSize, 
                    CompilerData.m_VertexCacheSize );
            
                x_inline_light_jobs_block<8>    JobBlock;
            
                JobBlock.SubmitJob( [&]()
                {
                    const s32 nFaces = Optimizer.m_Section.m_lTriangle.getCount(); 
                    TotalFacets.Add( nFaces );
                    DList.m_lFinalFace.Alloc( nFaces );
                    mutable_ref( rFinalFacet, DList.m_lFinalFace );
                    x_memcpy( &rFinalFacet[0], &Optimizer.m_Section.m_lTriangle[0], nFaces * sizeof(rawgeom::facet) );
                });

                JobBlock.SubmitJob( [&]()
                {
                    const s32 nVerts = Optimizer.m_Section.m_lVertex.getCount();
                    TotalVerts.Add( nVerts );
                    DList.m_lFinalVertex.Alloc( nVerts );
                    mutable_ref( rFinalVertex, DList.m_lFinalVertex );
                    x_memcpy( &rFinalVertex[0], &Optimizer.m_Section.m_lVertex[0], nVerts * sizeof(rawgeom::vertex) );
                });

                JobBlock.SubmitJob( [&]()
                {
                    mutable_ref( rFinalCmds, DList.m_lFinalCmds );
                    for ( s32 i = 0; i<Optimizer.m_Section.m_lCommand.getCount(); i++ )
                    {   
                        const arm_optimizer::command& ArmCmd = Optimizer.m_Section.m_lCommand[ i ];

                        switch ( ArmCmd.m_Type )
                        {
                        case arm_optimizer::DRAW_LIST:
                        {
                            eng_geom_rsc::cmd& Cmd = rFinalCmds->append();
                            Cmd.m_Type       = eng_geom_rsc::CMD_RENDER;
                            Cmd.m_IndexCount = 3 * (ArmCmd.m_Arg2 - ArmCmd.m_Arg1);
                            break;
                        }
                        case arm_optimizer::UPLOAD_MATRIX:
                        {
                            eng_geom_rsc::cmd& Cmd = rFinalCmds->append();
                            Cmd.m_Type                   = eng_geom_rsc::CMD_LOAD_MATRIX;
                            Cmd.m_iSrcMatrix             = ArmCmd.m_Arg1;
                            Cmd.m_iMatrixCacheOffset     = ArmCmd.m_Arg2;
                            ASSERT( Cmd.m_iSrcMatrix < Src.m_Bone.getCount() );
                            ASSERT( Cmd.m_iSrcMatrix >= 0 );
                            break;
                        }
                        default: ASSERT(0);
                        }
                    }

                    //
                    // Done with the cmds
                    //
                    DList.m_lFinalCmds.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );

                    //
                    // Add the commands at the end after collecing and merging
                    //
                    TotalCmd.Add( rFinalCmds.getCount() );
                });

                JobBlock.FinishJobs();
            });
        }

        // Finish
        JobBlock.FinishJobs();
        lSubmesh.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
    }

    //
    // - Copy all the cmds into the final memory
    // - Fill the Submesh Remap to where the commands are
    //
    JobBlock.SubmitJob( [&]()
    {
        // Allocate a list of all the comamnds
        mutable_ref( rCmd, CompilerData.m_lCmds );
        rCmd->appendList( TotalCmd.get() );

        mutable_ref( rSubmeshMap, CompilerData.m_lSubmeshCmdMap );
        const_ref( rSubmesh, lSubmesh );
        s32 iCmdOffset=0;
        for ( auto& Submesh : rSubmesh )
        {
            // Set the mapping
            auto& Mapping = rSubmeshMap[ rSubmesh.getIterator() ];
            Mapping.m_iCmd  = iCmdOffset;
            Mapping.m_nCmds = Submesh.m_lFinalCmds.getCount( );

            // Copy the commands to the final place
            const_ref( rFinalCmds, Submesh.m_lFinalCmds );
            mutable_ref( rCmds, CompilerData.m_lCmds );
            
            x_memcpy( &rCmds[ iCmdOffset ], &rFinalCmds[ 0 ], Mapping.m_nCmds * sizeof(eng_geom_rsc::cmd) );
            
            iCmdOffset += Mapping.m_nCmds;
        }

        // Lock these lists because they are done
        CompilerData.m_lCmds.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
        CompilerData.m_lSubmeshCmdMap.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
    } );


    //
    // Merge everything into a new rawgeom
    //
    Dest.m_Bone             = Src.m_Bone;
    Dest.m_InformedMaterial = Src.m_InformedMaterial;
    Dest.m_Mesh             = Src.m_Mesh;
    Dest.m_Facet.Alloc( TotalFacets.get() );
    Dest.m_Vertex.Alloc( TotalVerts.get() );
 
    //
    // Copy all the facets
    //
    JobBlock.SubmitJob( [&]()
    {
        const_ref( rSubmesh, lSubmesh );
        s32 iFacetOffset = 0;
        s32 iVertOffset  = 0;
        for ( auto& DList : rSubmesh )
        {
            const_ref( rFinalFace, DList.m_lFinalFace );
            for ( auto& FinalFacet : rFinalFace )
            {
                auto& DestFacet = Dest.m_Facet[ iFacetOffset + rFinalFace.getIterator() ];
                
                // Copy all the info over
                DestFacet = FinalFacet;
                for ( s32 i = 0; i < DestFacet.m_nVertices; i++ )
                {
                    DestFacet.m_iVertex[i] += iVertOffset;
                }
            }

            // Increment the offset
            iFacetOffset += DList.m_lFinalFace.getCount();
            iVertOffset  += DList.m_lFinalVertex.getCount();
        }
    });

    //
    // copy over the verices
    //
    JobBlock.SubmitJob( [&]()
    {
        s32 iVertOffset  = 0;

        const_ref( rSubmesh, lSubmesh );
        for ( auto& DList : rSubmesh )
        {
            const s32 nVerts = DList.m_lFinalVertex.getCount();

            const_ref( rFinalVert, DList.m_lFinalVertex );
            x_memcpy( &Dest.m_Vertex[ iVertOffset ], &rFinalVert[0], sizeof(rawgeom::vertex) * nVerts  );
            
            iVertOffset += nVerts;
        }
    });

    //
    // Done
    //
    JobBlock.FinishJobs();
}

//-------------------------------------------------------------------------------------------------

struct inter_to_rawinformed
{
    s32     m_iInter;                   // Final index to informed_inter which also is the final index of the informed guid
    s32     m_iInformedTech;            // Final index of the technique
};

void mesh_compiler_base::ComputeStructures( compiler_data& CompilerData, const rawgeom& RawGeom, const xplatform Platform ) const
{
    x_inline_light_jobs_block<8>    JobBlock;
    xarray2<inter_to_rawinformed>   lRawInformedToInterInformed;

    CompilerData.m_Mesh.appendList( RawGeom.m_Mesh.getCount() );

    //
    // Dealt with bones since it is easy
    //
    JobBlock.SubmitJob( [&]()
    {
        if ( RawGeom.m_Bone.getCount() )
        {
            CompilerData.m_lBones.Alloc( RawGeom.m_Bone.getCount() );

            mutable_ref( rBone, CompilerData.m_lBones );
            for ( eng_geom_rsc::bone& Bone : rBone )
            {
                Bone.m_BBox = RawGeom.m_Bone[ rBone.getIterator() ].m_BBox;
            }

            CompilerData.m_GeomRSC.m_nBones = CompilerData.m_lBones.getCount();
            CompilerData.m_GeomRSC.m_pBone.m_Ptr = &rBone[0];

            // make the bones const
            CompilerData.m_lBones.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
        }
        else
        {
            CompilerData.m_GeomRSC.m_nBones = 0;
            CompilerData.m_GeomRSC.m_pBone.m_Ptr = NULL;
        }
    });
    
    //
    // Deal with refactoring informed materials
    //
    JobBlock.SubmitJob( [&]()
    {
        mutable_ref( rRawInformedToInterInformed, lRawInformedToInterInformed );
        rRawInformedToInterInformed->appendList( RawGeom.m_InformedMaterial.getCount() );

        mutable_ref( rInterInformed, CompilerData.m_lInterInformed );    
        for ( const rawgeom::informed_material& RawInformed : RawGeom.m_InformedMaterial )
        {
            const s32               iRawInformed        = s32( &RawInformed - &RawGeom.m_InformedMaterial[0] );
            inter_to_rawinformed&   InterToRaw          = rRawInformedToInterInformed[ iRawInformed ];
            s32                     iInformedTechnique  = -1;
            u64                     Guid;
            xbool                   isInformed;
            xstring                 InformedORMaterial;

            //
            // Get the guid from the informed name
            //
            {
                char* pGuid = x_stristr( RawInformed.m_MaterialShader, "--" );
                if ( pGuid == NULL )
                    x_throw( "ERROR: All materials must have a guid, this one [%s] did not have one.",
                        (const char*)RawInformed.m_MaterialShader );

                // TODO: Verify that this resource exits
                char Buffer[16];
                x_strncpy( Buffer, &pGuid[2], 14, sizeof(Buffer) );

                xguid  TempGuid;
                TempGuid.SetFromAlphaString( Buffer );
                Guid = TempGuid.m_Guid;
            }

            //
            // determine the type of resource we are pointing
            // and do some sanity checks
            //
            if ( eng_resource_guid::getType(Guid) == eng_material_rsc::UID )
            {
                isInformed = FALSE;

                eng_resource_guid  RscGuid;
                Guid = (Guid >> 32) ^ (Guid&0xffffffff);
                Guid = ((Guid>>16) ^ Guid)&0xffff;
                Guid = m_RscGuid.m_Guid ^ ( Guid ) ;
                RscGuid.setup( Guid, eng_informed_material_rsc::UID );
                Guid = RscGuid.m_UID;

                ASSERT(  eng_resource_guid::getType(Guid) ==  eng_informed_material_rsc::UID );

                if( NULL == x_strstr( RawInformed.m_MaterialShader, "M-" ) )
                    x_throw(  "ERROR: The .fx file which should be a material is missing the 'M-' [%s]",
                    (const char*)RawInformed.m_MaterialShader   );

                if( NULL == x_strstr( RawInformed.m_MaterialShader, ".fx" ) )
                    x_throw(  "ERROR: The material does not have a .fx extension [%s]",
                    (const char*)RawInformed.m_MaterialShader   );

                // Create the mapping back to the key material file
                char FileName[xfile::MAX_FNAME];
                x_splitpath( RawInformed.m_MaterialShader, NULL, NULL, FileName, NULL );
                InformedORMaterial.Format( "GameData/CompilerKeys/%s.material", FileName );
                InformedORMaterial.SearchAndReplace( "M-", "" );

                if ( FALSE == x_io::FileExists( xfs( "%s/%s.txt",
                    (const char*)m_ProjectPath,
                    (const char*)InformedORMaterial ) ) )
                    x_throw(  "ERROR: Unable to find the material file [%s]",
                    (const char*)InformedORMaterial   );
            }
            else if ( eng_resource_guid::getType(Guid) == eng_informed_material_rsc::UID )
            {
                isInformed = TRUE;

                char        Number[16];
                const char* pInformedTechnique = x_strstr( RawInformed.m_MaterialShader, "I[");
                const char* pCloseBraket       = x_strstr( RawInformed.m_MaterialShader, "]-");
                const s32   Length             = s32(pCloseBraket - pInformedTechnique)-3;
                if ( pInformedTechnique == NULL || pCloseBraket == NULL || Length <= 0 || Length > 8 )
                    x_throw(  "ERROR: The name of the informed material did not have a 'I[informedTechnique#]-' as part of its name [%s]."
                                  "Please make sure that it does have one and that it matches with the informed material key",
                        (const char*)RawInformed.m_MaterialShader ) ;

                x_strncpy( Number, &pInformedTechnique[ 2 ], Length, sizeof(Number) );
                iInformedTechnique = x_atod32( Number, 10 );

                InformedORMaterial.Copy( pCloseBraket+2 );
                InformedORMaterial.SearchAndReplace( ".fx", ".informed" );
            }
            else
            {
                x_throw(  "ERROR: The guid from the material [%s] was neither an informed or an actual material",
                    (const char*)RawInformed.m_MaterialShader  );
            }

            //
            // Find Intern entry for this guid
            //
            for ( informed_inter& InterInformed : rInterInformed )
            {
                if ( InterInformed.m_GuidInformed == Guid )
                    break;
            }

            //
            // Add new entries
            // and fill the mapping
            //
            if ( rInterInformed.isIteratorAtEnd() )
            {
                informed_inter& InterInformed = rInterInformed->append();

                ASSERT( eng_resource_guid::getType(Guid) == eng_informed_material_rsc::UID );
                InterInformed.m_GuidInformed       = Guid;
                InterInformed.m_bNewInformedFile   = !isInformed;
                InterInformed.m_InformedORMaterial = InformedORMaterial;
                
                if ( InterInformed.m_bNewInformedFile )
                    InterInformed.m_NewInformedRSC.Format( "%s/%s--%s.informed",
                        (const char*) &m_ExternalCompilerKeysPath[ m_ProjectPathLength+1 ],
                        (const char*) m_RscFileName,
                        (const char*) xguid(Guid).GetAlphaString() );

                mutable_ref( rInformedTech, InterInformed.m_lInformedTech );
                infortech_inter& InformedTech = rInformedTech->append();
                InformedTech.m_iRawInformed   = iRawInformed;
                InformedTech.m_TechName       = RawInformed.m_Technique;

                if ( InterInformed.m_bNewInformedFile )
                    InformedTech.m_InformedTechniqueIndex = 0;
                else
                    InformedTech.m_InformedTechniqueIndex = iInformedTechnique;

                //
                // Set the mapping
                //
                InterToRaw.m_iInformedTech = InformedTech.m_InformedTechniqueIndex;
                InterToRaw.m_iInter        = rInterInformed.getCount()-1;
            }
            else
            {
                informed_inter& InterInformed = rInterInformed[rInterInformed.getIterator()];
                mutable_ref( rInformedTech, InterInformed.m_lInformedTech );

                // Find the right technique
                for ( infortech_inter& InformedTech : rInformedTech )
                {
                    if ( isInformed )
                    {
                        if( InformedTech.m_InformedTechniqueIndex == iInformedTechnique )
                            break;
                    }
                    else if( InformedTech.m_iRawInformed == iRawInformed )
                            break;
                }

                // if we did not find it we need to add one
                if ( rInformedTech.isIteratorAtEnd() )
                {
                    infortech_inter& InformedTech = rInformedTech->append();
                    InformedTech.m_iRawInformed   = iRawInformed;
                    InformedTech.m_TechName       = RawInformed.m_Technique;

                    if ( InterInformed.m_bNewInformedFile )
                        InformedTech.m_InformedTechniqueIndex = rInformedTech.getCount()-1;
                    else
                        InformedTech.m_InformedTechniqueIndex = iInformedTechnique;

                    //
                    // Set the mapping
                    //
                    InterToRaw.m_iInformedTech = InformedTech.m_InformedTechniqueIndex;
                    InterToRaw.m_iInter        = rInterInformed.getIterator();
                }
                else
                {
                    //
                    // Set the mapping
                    //
                    InterToRaw.m_iInformedTech = rInformedTech[ rInformedTech.getIterator() ].m_InformedTechniqueIndex;
                    InterToRaw.m_iInter        = rInterInformed.getIterator();
                }
            }
        }

        //
        // Do a bit of sanity checks
        //
        x_inline_light_jobs_block<8>    JobBlock;
        for ( const informed_inter& InterInformed : rInterInformed ) 
        {
            // If it is a new resource that we have to create then there is nothing to load
            if ( InterInformed.m_bNewInformedFile )
                continue;

            JobBlock.SubmitJob( [&]()
            {
                informed_material_compiler_key  InformedKey(*this);
                xstring                         KeyPath;

                KeyPath.Format( "%s/%s",
                    (const char* )m_ProjectPath,
                    (const char* )InterInformed.m_InformedORMaterial );
                
                InformedKey.LoadDefaults( m_ProjectPath );
                InformedKey.Load( KeyPath );

                const_ref( rInformedTech, InterInformed.m_lInformedTech );
                for ( const infortech_inter& InformedTech : rInformedTech )
                {
                    if( InformedTech.m_InformedTechniqueIndex > InformedKey.m_lEntry.getCount() )
                        x_throw(  "ERROR: The Informed index from the 3DS material does not match the key file for the informed instance."
                                      " Informed[%s] InformedIndex[%s] this is the informed key count[%d]. Seems 3DS Max informed material is out of date.",
                                      (const char*)InterInformed.m_InformedORMaterial,
                                      InformedTech.m_InformedTechniqueIndex,
                                      InformedKey.m_Main.getCount() );

                    const_ref( rEntry, InformedKey.m_lEntry );
                    auto& KeyInfo = rEntry[InformedTech.m_InformedTechniqueIndex][Platform];
                    if ( x_strcmp( KeyInfo.m_TechniqueName, InformedTech.m_TechName ) )
                        x_throw(  "ERROR: Expecting a technique name [%s] but found [%s] in the informed key file [%s]. Seems 3DS Max informed material is out of date.",
                                 (const char*)InformedTech.m_TechName,
                                 (const char*)KeyInfo.m_TechniqueName,
                                 (const char*)InterInformed.m_InformedORMaterial
                                  ) ;
               }
            } );
        }
        JobBlock.FinishJobs();

        //
        // Done with this lists
        //
        lRawInformedToInterInformed.ChangeBehavior  ( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
        CompilerData.m_lInterInformed.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );
    });    
    
        
    //
    // Create all the sub-meshes
    // A submesh is define by:
    // 1. a diferent mesh
    // 2. a different material
    // Note that we assume that we already shorted the faces by mesh and material
    //
    JobBlock.SubmitJob( [&]()
    {
        s32 iLastMesh       = -1;
        s32 iLastMaterial   = -1;
        s32 iLastSubmesh    = -1;
        for ( s32 i = 0; i < RawGeom.m_Facet.getCount(); i++ )
        {
            const rawgeom::facet& RawFacet = RawGeom.m_Facet[ i ];

            if ( RawFacet.m_iMesh == iLastMesh )
            {
                // Ok nothing to do
            }
            else
            {
                ASSERT( iLastMesh < RawFacet.m_iMesh );
                iLastMesh     = RawFacet.m_iMesh;
                iLastMaterial = -1;

                // Initialize the new mesh
                eng_geom_rsc::mesh& RSCMesh = CompilerData.m_Mesh[ iLastMesh ];

                x_strcpy( RSCMesh.m_Name, RSCMesh.m_Name.getCount(), ( const char* )RawGeom.m_Mesh[ iLastMesh ].m_Name );
                RSCMesh.m_nBones    = RawGeom.m_Mesh[ iLastMesh ].m_nBones;
                RSCMesh.m_iSubMesh  = iLastSubmesh + 1;
                RSCMesh.m_nSubMeshs = 0;
                RSCMesh.m_nFaces    = 0;
                RSCMesh.m_BBox.Identity();
            }

            // Add the vertices of the facet into the mesh bbox
            // to compute bbox size
            eng_geom_rsc::mesh& RSCMesh = CompilerData.m_Mesh[ iLastMesh ];
            RSCMesh.m_BBox.AddVerts( &RawGeom.m_Vertex[ RawFacet.m_iVertex[ 0 ] ].m_Position, 1 );
            RSCMesh.m_BBox.AddVerts( &RawGeom.m_Vertex[ RawFacet.m_iVertex[ 1 ] ].m_Position, 1 );
            RSCMesh.m_BBox.AddVerts( &RawGeom.m_Vertex[ RawFacet.m_iVertex[ 2 ] ].m_Position, 1 );

            // Create additional submeshes if needed base on material
            if ( RawFacet.m_iInformed == iLastMaterial )
            {
                RSCMesh.m_nFaces++;
                CompilerData.m_Submesh[ iLastSubmesh ].m_nFaces++;
            }
            else
            {
                ASSERT( iLastMaterial < RawFacet.m_iInformed );
                iLastMaterial = RawFacet.m_iInformed;

                // Initialize the new submesh
                RSCMesh.m_nSubMeshs++;
                eng_geom_rsc::submesh& Submesh = CompilerData.m_Submesh.append( iLastSubmesh );

                Submesh.m_iInformed         = iLastMaterial;
                Submesh.m_iColor            = -1;
                Submesh.m_iCmd              = -1;
                Submesh.m_nCmds             =  0;
                Submesh.m_nFaces            =  1;
                Submesh.m_WorldPixelSize    = -1;
                Submesh.m_iMesh             = iLastMesh;

                if ( CompilerData.m_lSubmeshCmdMap.getCount() )
                {
                    const_ref( rSubmeshCmdMap, CompilerData.m_lSubmeshCmdMap );
                    auto& SubmeshCmdMap = rSubmeshCmdMap[ iLastSubmesh ];

                    // make sure we are reading the right mapping
                    ASSERT( SubmeshCmdMap.m_iInformed == iLastMaterial );
                    ASSERT( SubmeshCmdMap.m_iMesh     == iLastMesh     );

                    // Set the command information
                    Submesh.m_iCmd              = SubmeshCmdMap.m_iCmd;
                    Submesh.m_nCmds             = SubmeshCmdMap.m_nCmds;
                }
            }
        }
    } );

    JobBlock.FinishJobs();

    //
    // Determine chich kind of submesh each one of them is
    // also allocate the different vertex buffers that we will need
    //
    JobBlock.SubmitJob( [&]()
    {
        s32 iFirstFace = 0;
        for ( eng_geom_rsc::submesh& Submesh : CompilerData.m_Submesh )
        {
//            const s32 iSubmesh  = s32(&Submesh - &CompilerData.m_Submesh[ 0 ]);

            s32 MaxWeights      = 0;
            s32 MaxUVs          = 0;
            s32 MaxColors       = 0;
            s32 MaxNormals      = 0;
            s32 MaxTangents     = 0;
            s32 MaxBinormals    = 0;
            s32 MinBinormals    = 10000;
            s32 MinTangents     = 10000;
            s32 MinNormals      = 10000;
            xsafe_array<xvector2, rawgeom::VERTEX_MAX_UV> MaxRangeUVs;
            xsafe_array<xvector2, rawgeom::VERTEX_MAX_UV> MinRangeUVs;
            xarray<eng_vertex_desc::attribute>            VertexAttributes;
            
            for ( s32 j = 0; j < rawgeom::VERTEX_MAX_UV; j++ )
            {
                MaxRangeUVs[ j ].m_X = MaxRangeUVs[ j ].m_Y = -100000;
                MinRangeUVs[ j ] = -MaxRangeUVs[ j ];
            }

            for ( s32 i = 0; i < Submesh.m_nFaces; i++ )
            for ( s32 n = 0; n < 3; n++ )
            {
                const rawgeom::vertex&  RawVert = RawGeom.m_Vertex[ RawGeom.m_Facet[ iFirstFace + i ].m_iVertex[ n ] ];
                MaxWeights      = x_Max( MaxWeights,    RawVert.m_nWeights );
                MaxUVs          = x_Max( MaxUVs,        RawVert.m_nUVs );
                MaxColors       = x_Max( MaxColors,     RawVert.m_nColors );
                MaxNormals      = x_Max( MaxNormals,    RawVert.m_nNormals );
                MaxTangents     = x_Max( MaxTangents,   RawVert.m_nTangents );
                MaxBinormals    = x_Max( MaxBinormals,  RawVert.m_nBinormals );
                MinNormals      = x_Min( MinNormals,    RawVert.m_nNormals );
                MinTangents     = x_Min( MinTangents,   RawVert.m_nTangents );
                MinBinormals    = x_Min( MinBinormals,  RawVert.m_nBinormals );

                for ( s32 j = 0; j < RawVert.m_nUVs; j++ )
                {
                    const xvector2& UV  = RawVert.m_UV[ j ];
                    xvector2&       Min = MinRangeUVs[ j ];
                    xvector2&       Max = MaxRangeUVs[ j ];

                    Min.m_X = x_Min( UV.m_X, Min.m_X );
                    Min.m_Y = x_Min( UV.m_Y, Min.m_Y );

                    Max.m_X = x_Max( UV.m_X, Max.m_X );
                    Max.m_Y = x_Max( UV.m_Y, Max.m_Y );
                }
            }

            //
            // After collecting all the info about the vertices then we must decide
            // what format our vertices are going to be for this submesh
            //
            if ( (  MinNormals   != MaxNormals )
               || ( MinTangents  != MaxTangents )
               || ( MinBinormals != MaxBinormals )
               || ( MaxTangents  != MaxBinormals )
               || ( MaxTangents  != MaxNormals ) )
            {
                x_throw(  "ERROR:ERROR: There is a miss match between Normals, Tangents, and Binormals" );
            }

            //
            // Decide which type of UVs to use
            //
            for ( s32 i = 0; i < MaxUVs; i++ )
            {
                const xvector2& MaxRange = MaxRangeUVs[ i ];
                const xvector2& MinRange = MinRangeUVs[ i ];
                xbool           bParametric = FALSE;

                // Are the UVs parametric?
                // They may be but the artist may have map them incorrectly
                // Lets fix it here if we need to
                if( (MaxRange.m_X - MinRange.m_X) <=1 &&
                    (MaxRange.m_Y - MinRange.m_Y) <=1 )
                {
                    const s32 iVU = i;
                    bParametric   = TRUE;

                    // TODO: This should be done in the rawgeom clean function
                    for ( s32 i = 0; i < Submesh.m_nFaces; i++ )
                    for ( s32 n = 0; n < 3; n++ )
                    {
                        const rawgeom::vertex&  RawVert = RawGeom.m_Vertex[ RawGeom.m_Facet[ iFirstFace + i ].m_iVertex[ n ] ];
                        xvector2& UV = const_cast<xvector2&>(RawVert.m_UV[ iVU ]);

                        if( UV.m_X < 0 )    
                            UV.m_X = x_FMod( -UV.m_X, 1 );
                        else
                            UV.m_X = x_FMod(  UV.m_X, 1 ); 
                        
                        if( UV.m_Y < 0 )    
                            UV.m_Y = x_FMod( -UV.m_Y, 1 );
                        else                
                            UV.m_Y = x_FMod(  UV.m_Y, 1 ); 
                    }
                }

                // Can we use the more compress version of the UVs?
                if ( bParametric )
                {
                    eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                    Attr.m_UsageType = eng_vertex_desc::attribute_usage(eng_vertex_desc::ATTR_USAGE_00_PARAMETRIC_UV + i);
                    Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_U16x2_F;
                }
                else
                {
                    // If it is 10% within the range of 0-1 issue a warning
                    if ( MaxRangeUVs[ i ].InRange( -0.98f, 1.01f )
                        && MinRangeUVs[ i ].InRange( -0.98f, 1.01f ) )
                    {
                        // TODO: In the warning we need to set the name of the mesh
                        x_printf("WARNING: This UVs wont be compress because they are outside the [0-1] range...");
                    }

                    eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                    Attr.m_UsageType = eng_vertex_desc::attribute_usage(eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV + i);
                    Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_F32x2;
                }
            }

            //
            // Create the vertex description for the submesh
            //
//            s32                     iDesc = 0;

            // Set the position as part of the vertex desc
            {
                eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                Attr.m_UsageType = eng_vertex_desc::ATTR_USAGE_POSITION;
                Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_F32x3;
            }

            //
            // Set the weights
            //
            {
                const_ref( rMain, m_Key.m_Main );
                // If we have any weights we must make all the mesh uniform based on the user requirements
                if ( MaxWeights >= 1 ) 
                    MaxWeights = rMain[Platform].m_MaxNumberOfWeights;

                switch ( MaxWeights )
                {
                    case 0:
                    {
                        Submesh.m_TechniqueType = eng_material_rsc::TECH_TYPE_RIGID;
                        Submesh.m_nWeights      = 0;
                        break;
                    }
                    case 1:
                    {
                        Submesh.m_TechniqueType = eng_material_rsc::TECH_TYPE_MATRIX_SKIN;
                        Submesh.m_nWeights      = 1;

                        eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                        Attr.m_UsageType = eng_vertex_desc::ATTR_USAGE_2_COMPACT_WEIGHTS;
                        Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_F; 
                        break;
                    }
                    case 2:
                    {
                        Submesh.m_TechniqueType = eng_material_rsc::TECH_TYPE_MATRIX_SKIN;
                        Submesh.m_nWeights      = 2;

                        eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                        Attr.m_UsageType = eng_vertex_desc::ATTR_USAGE_2_COMPACT_WEIGHTS;
                        Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_F; 
                        break;
                    }
                    case 3:
                    {
                        Submesh.m_TechniqueType = eng_material_rsc::TECH_TYPE_MATRIX_SKIN;
                        Submesh.m_nWeights      = 3;

                        eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                        Attr.m_UsageType = eng_vertex_desc::ATTR_USAGE_4_WEIGHTS;
                        Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_F; 

                        eng_vertex_desc::attribute& Attr2 = VertexAttributes.append();
                        Attr2.m_UsageType = eng_vertex_desc::ATTR_USAGE_4_INDICES;
                        Attr2.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_I; 
                        break;
                    }
                    case 4:
                    {
                        Submesh.m_TechniqueType = eng_material_rsc::TECH_TYPE_MATRIX_SKIN;
                        Submesh.m_nWeights      = 4;

                        eng_vertex_desc::attribute& Attr = VertexAttributes.append();
                        Attr.m_UsageType = eng_vertex_desc::ATTR_USAGE_4_WEIGHTS;
                        Attr.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_F; 

                        eng_vertex_desc::attribute& Attr2 = VertexAttributes.append();
                        Attr2.m_UsageType = eng_vertex_desc::ATTR_USAGE_4_INDICES;
                        Attr2.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_I; 
                        break;
                    }
                    default:
                    {
                        ASSERT(0);
                    }
                };
            }

            //
            // Lighting?
            //
            if ( MaxTangents > 0 )
            {
                eng_vertex_desc::attribute& AttrT = VertexAttributes.append();
                AttrT.m_UsageType = eng_vertex_desc::ATTR_USAGE_TANGENT;
                AttrT.m_DataType  = eng_vertex_desc::ATTR_SRC_S8x4_F;

                eng_vertex_desc::attribute& AttrB = VertexAttributes.append();
                AttrB.m_UsageType = eng_vertex_desc::ATTR_USAGE_BINORMAL;
                AttrB.m_DataType  = eng_vertex_desc::ATTR_SRC_S8x4_F;

                eng_vertex_desc::attribute& AttrN = VertexAttributes.append();
                AttrN.m_UsageType = eng_vertex_desc::ATTR_USAGE_NORMAL;
                AttrN.m_DataType  = eng_vertex_desc::ATTR_SRC_S8x4_F;
            }
            else if ( MaxNormals )
            {
                eng_vertex_desc::attribute& AttrN = VertexAttributes.append();
                AttrN.m_UsageType = eng_vertex_desc::ATTR_USAGE_NORMAL;
                AttrN.m_DataType  = eng_vertex_desc::ATTR_SRC_S8x4_F;
            }

            //
            // Colors
            //
            for ( s32 j = 0; j < MaxColors; j++ )
            {
                eng_vertex_desc::attribute& AttrColor = VertexAttributes.append();

                AttrColor.m_UsageType = eng_vertex_desc::attribute_usage(eng_vertex_desc::ATTR_USAGE_00_RGBA + j);
                AttrColor.m_DataType  = eng_vertex_desc::ATTR_SRC_U8x4_F;
            }

            //
            // Determine if we have any other vertex stream with this format
            //
            s32 iFounded = -1;
            for ( eng_geom_rsc::temp_vertex_stream& VertStream : CompilerData.m_TempVertices )
            {
                const s32 Index = s32( &VertStream - &CompilerData.m_TempVertices[0] );
                s32 k;

                if ( CompilerData.m_VertexDesc[ Index ].getAttributeCount() != VertexAttributes.getCount() )
                    continue;

                for( k = 0; k<VertexAttributes.getCount(); k++ )
                {
                    if ( VertexAttributes[k].m_DataType != CompilerData.m_VertexDesc[ Index ].getAttribute(k).m_DataType )
                    {
                        break;
                    }
                    if ( VertexAttributes[k].m_UsageType != CompilerData.m_VertexDesc[ Index ].getAttribute(k).m_UsageType )
                    {
                        break;
                    }
                }
                
                if ( k == VertexAttributes.getCount() )
                {
                    iFounded = s32(&VertStream - &CompilerData.m_TempVertices[0]);
                    break;
                }
            }

            // There is not a vert stream with this format
            if ( iFounded == -1 )
            {
                s32                                 iNewDesc;
                s32                                 iNewAttrList;
                eng_geom_rsc::temp_vertex_stream&   VertStream        = CompilerData.m_TempVertices.append( iFounded );
                vertex_attribute_list&              AttrList          = CompilerData.m_GeomAttributeList.append( iNewAttrList );
                // eng_vertex_desc&                    NewDesc           = CompilerData.m_VertexDesc.append( iNewDesc );
                
                CompilerData.m_VertexDesc.append( iNewDesc );

                ASSERT( iNewDesc == iFounded );
                ASSERT( iNewDesc == iNewAttrList );
//                const s32 iAttrBase = CompilerData.m_GeomAttributeList.getCount();
                AttrList.m_AttributeList.Copy( VertexAttributes );

                VertStream.m_nVertices                              = Submesh.m_nFaces * 3; // worse case
            }
            else
            {
                eng_geom_rsc::temp_vertex_stream& VertStream        = CompilerData.m_TempVertices[ iFounded ];
                VertStream.m_nVertices                             += Submesh.m_nFaces * 3; // worse case
            }

            // Set the vert stream id for this submesh. Since we only have one vertex stream per submesh right now
            // we just set it
            Submesh.m_StreamRef.m_ICount    = -1;
            Submesh.m_StreamRef.m_iIBase    = -1;
            Submesh.m_StreamRef.m_iIStream  = -1;

            Submesh.m_StreamRef.m_VCount    = -1;
            Submesh.m_StreamRef.m_iVBase    = -1;
            Submesh.m_StreamRef.m_iVStream  = iFounded;

            //
            // Get ready for the next sub mesh
            //
            iFirstFace = iFirstFace + Submesh.m_nFaces;
        }
    } );

    //
    // Fix up the submeshes so that the index to the right final informed list and its technique
    //
    JobBlock.SubmitJob( [&]()
    {
        const_ref( rRawToInter, lRawInformedToInterInformed );
        for ( eng_geom_rsc::submesh& Submesh : CompilerData.m_Submesh )
        {
            const inter_to_rawinformed& RawToInter = rRawToInter[ Submesh.m_iInformed ];

            Submesh.m_iInformed     = RawToInter.m_iInter;
            Submesh.m_iInformedTech = RawToInter.m_iInformedTech;
        }
    });

    //
    // fill the final informs 
    // Add external key dependencies
    //
    JobBlock.SubmitJob( [&]()
    {
        x_inline_light_jobs_block<8>    JobBlock;
        mesh_compiler_base& This = const_cast<mesh_compiler_base&>( *this );

        mutable_ref( rImformed, CompilerData.m_lImformed );
        rImformed->appendList( CompilerData.m_lInterInformed.getCount() );

        CompilerData.m_lImformed.ChangeBehavior( xptr2_flags::FLAGS_QT_MUTABLE );

        const_ref( rInterInformed, CompilerData.m_lInterInformed );
        for ( const auto& InterInformed : rInterInformed ) 
        {
            const s32 Index = rInterInformed.getIterator();
            JobBlock.SubmitJob( [Index, &This, &CompilerData, &InterInformed ]()
            {
                // Add the external key
                if( InterInformed.m_bNewInformedFile )
                {
                    This.AddExternalDependencyKey( InterInformed.m_NewInformedRSC );
                }
                else
                {
                    This.AddExternalDependencyKey( InterInformed.m_InformedORMaterial );
                }

                //
                // Set the final GUID
                //
                mutable_ref( rImformed, CompilerData.m_lImformed );
                const_ref( rInterInformed, CompilerData.m_lInterInformed );
                eng_geom_rsc::informed_ref& Informed = rImformed[ Index ];
                Informed.m_Guid = InterInformed.m_GuidInformed;

                ASSERT( eng_resource_guid::getType( Informed.m_Guid ) == eng_informed_material_rsc::UID );
            }); 
        }        
        JobBlock.FinishJobs();

        // Lets mark it as done
        CompilerData.m_lImformed.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
    });

    JobBlock.FinishJobs();

    //
    // Setup descriptors
    //
    {
        for ( eng_vertex_desc& Descriptor : CompilerData.m_VertexDesc ) JobBlock.SubmitJob( [&]()
        {
            const s32                   Index       = s32( &Descriptor - &CompilerData.m_VertexDesc[0] );
            vertex_attribute_list&      AttrList    = CompilerData.m_GeomAttributeList[ Index ];

            // Need to short the attributes first
            x_qsort( &AttrList.m_AttributeList[0], 
                     AttrList.m_AttributeList.getCount(), 
                     sizeof( eng_vertex_desc::attribute ), 
                     []( const void* pA, const void* pB ) -> s32
            {
                const eng_vertex_desc::attribute& A = *(const eng_vertex_desc::attribute *)pA;
                const eng_vertex_desc::attribute& B = *(const eng_vertex_desc::attribute *)pB;
                if ( A.m_UsageType < B.m_UsageType ) return -1;
                ASSERT(A.m_UsageType != B.m_UsageType);
                return A.m_UsageType > B.m_UsageType;
            });

            // Need to fill in the attribute offsets
            s32 VertexSize=0;
            for ( eng_vertex_desc::attribute& Attribute : AttrList.m_AttributeList )
            {
                Attribute.m_Offset = VertexSize;

                switch ( Attribute.m_DataType )
                {
                case eng_vertex_desc::ATTR_SRC_U8x4_I:
                case eng_vertex_desc::ATTR_SRC_U8x4_F:  VertexSize += 4; break;
                case eng_vertex_desc::ATTR_SRC_S8x4_I:
                case eng_vertex_desc::ATTR_SRC_S8x4_F:  VertexSize += 4; break;
                case eng_vertex_desc::ATTR_SRC_U16x2_I:
                case eng_vertex_desc::ATTR_SRC_U16x2_F: VertexSize += 4; break;
                case eng_vertex_desc::ATTR_SRC_U16x4_I:
                case eng_vertex_desc::ATTR_SRC_U16x4_F: VertexSize += 8; break;
                case eng_vertex_desc::ATTR_SRC_S16x2_I:
                case eng_vertex_desc::ATTR_SRC_S16x2_F: VertexSize += 4; break;
                case eng_vertex_desc::ATTR_SRC_S16x4_I:
                case eng_vertex_desc::ATTR_SRC_S16x4_F: VertexSize += 8; break;
                case eng_vertex_desc::ATTR_SRC_F32x1: VertexSize += 4; break;
                case eng_vertex_desc::ATTR_SRC_F32x2: VertexSize += 8; break;
                case eng_vertex_desc::ATTR_SRC_F32x3: VertexSize += 12; break;
                case eng_vertex_desc::ATTR_SRC_F32x4: VertexSize += 16; break;
                default: ASSERT(0 );
                }
            }

            // Need to setup the descriptors no memory owning
            Descriptor.SubmitAttributes( VertexSize, AttrList.m_AttributeList.getCount(), &AttrList.m_AttributeList[0], FALSE );
        } );

        //
        // Link the Geom to the descriptors
        //
        CompilerData.m_GeomRSC.m_pVertDesc.m_Ptr = &CompilerData.m_VertexDesc[0];
    } 

    //
    // Create all the key files that we need to create
    //

    JobBlock.FinishJobs();

    //
    // Allocate all the vertex buffers
    //
    for ( eng_geom_rsc::temp_vertex_stream& VertStream : CompilerData.m_TempVertices )
    {
        const s32 Index = s32( &VertStream - &CompilerData.m_TempVertices[0] );

        VertStream.m_pVertices.m_Ptr = (u8*)x_malloc( CompilerData.m_VertexDesc[Index].getVertSize(), VertStream.m_nVertices, 0 );

        // Reset the number of vertices of the bufer. Next time we will set the real number.
        VertStream.m_nVertices = 0;
        VertStream.m_VertSize  = CompilerData.m_VertexDesc[Index].getVertSize();
    }

    //
    // Allocate Indices both 16bits and 32bits, however at the end we may only use the 16bits ones
    //
    for ( s32 i = 0; i < 2; i++ )
    {
        eng_geom_rsc::temp_index_stream& TempIStream = CompilerData.m_TempIndices.append();

        TempIStream.m_nIndices = 0;
        if ( i == 0 )
        {
            TempIStream.m_IndexSize = 2;
            TempIStream.m_p16Indices.m_Ptr = (u16*)x_malloc( TempIStream.m_IndexSize, RawGeom.m_Facet.getCount() * 3, 0 );
        }
        else
        {
            TempIStream.m_IndexSize = 4;
            TempIStream.m_p32Indices.m_Ptr = (u32*)x_malloc( TempIStream.m_IndexSize, RawGeom.m_Facet.getCount() * 3, 0 );
        }
    }

    //
    // Copy vertices 
    //
    xptr<s32>   VMap;

    // Alloc the remap index table for the vertices and set the memory to invalid
    VMap.Alloc( RawGeom.m_Vertex.getCount() );
    VMap.SetMemory( ~0 );

    s32 iRawFacet  = 0;
    s32 iRawIndex  = 0;
    for ( eng_geom_rsc::submesh& Submesh : CompilerData.m_Submesh )
    {
        eng_geom_rsc::temp_vertex_stream& VStream       = CompilerData.m_TempVertices[ Submesh.m_StreamRef.m_iVStream ];
        u32                               iMinRawIndex  = ~0;
        u32                               MaxRawIndex   = 0;

        // Initialize the index stream reference
        Submesh.m_StreamRef.m_iIStream  = 0;  // Give 16bit index a chance
        Submesh.m_StreamRef.m_iIBase    = CompilerData.m_TempIndices[ Submesh.m_StreamRef.m_iIStream ].m_nIndices;

        // Update the very first vertex stream (where the base is at)
        Submesh.m_StreamRef.m_iVBase    = VStream.m_nVertices;

        for ( s32 j = 0; j < Submesh.m_nFaces; j++ )
        {
            // Do all indices and vertex for this facet
            for ( s32 i = 0; i < 3; i++ )
            {
                s32 Index       = RawGeom.m_Facet[ iRawFacet ].m_iVertex[ i ];
                iMinRawIndex    = x_Min( iMinRawIndex, (u32)Index );
                MaxRawIndex     = x_Max( MaxRawIndex, (u32)Index );

                if ( ~0 == VMap[ Index ] )
                {
                    VMap[ Index ] = VStream.m_nVertices - Submesh.m_StreamRef.m_iVBase;
                    AddVertex( VStream, RawGeom.m_Vertex[ Index ], CompilerData.m_VertexDesc[Submesh.m_StreamRef.m_iVStream] );
                }

                //
                // Copy index
                //
                {
                    eng_geom_rsc::temp_index_stream& TempIStream = CompilerData.m_TempIndices[ Submesh.m_StreamRef.m_iIStream ];
                    if ( TempIStream.m_IndexSize == 2 )
                    {
                        // Are we trying to access something outside the u16 range? if so we must change this 
                        // submesh to use 32bits indices
                        // TODO:
                        // There is allot of fertile research that can be done respect to what is the right thing 
                        // to do is here, since there may be multiple solutions to this problem:
                        // * Rebase if the u16 limit is exceeded we could create a new submesh or cmds and rebase streams
                        //   to it. This way you will keep u16 index at the cost of draw calls and some copy verts
                        // The current solution is base on simplicity and large numbers... (if you have such a large mesh
                        // having 32bits indices is the list of your problems.)
                        if ( VMap[ Index ] >= 0xffff )
                        {
                            // Access the 32bit version
                            eng_geom_rsc::temp_index_stream& TempI32Stream = CompilerData.m_TempIndices[ 1 ];

                            const s32 oldBase           = Submesh.m_StreamRef.m_iIBase;
                            const s32 nIndicesToCopy    = TempIStream.m_nIndices - oldBase;

                            // reset the old index stream 
                            TempIStream.m_nIndices      = oldBase;

                            // Set the new base 
                            Submesh.m_StreamRef.m_iIStream  = 1;
                            Submesh.m_StreamRef.m_iIBase    = TempI32Stream.m_nIndices;

                            // copy all the indices
                            for ( s32 k = 0; k < nIndicesToCopy; k++ )
                            {
                                TempI32Stream.m_p32Indices.m_Ptr[ TempI32Stream.m_nIndices++ ] = TempIStream.m_p16Indices.m_Ptr[ oldBase + k ];
                            }

                            // Now we can copy our new index
                            TempIStream.m_p32Indices.m_Ptr[ TempI32Stream.m_nIndices++ ] = VMap[ Index ];
                        }
                        else
                        {
                            TempIStream.m_p16Indices.m_Ptr[ TempIStream.m_nIndices++ ] = VMap[ Index ];
                        }
                    }
                    else
                    {
                        TempIStream.m_p32Indices.m_Ptr[ TempIStream.m_nIndices++ ] = VMap[ Index ];
                    }
                }
                    
                // get the next raw index 
                iRawIndex++;
            }

            // How many faces we have done so far
            iRawFacet++;
        }

        // Set the Vertex/index count for the streams
        Submesh.m_StreamRef.m_VCount = VStream.m_nVertices - Submesh.m_StreamRef.m_iVBase;
        Submesh.m_StreamRef.m_ICount = CompilerData.m_TempIndices[ Submesh.m_StreamRef.m_iIStream ].m_nIndices - Submesh.m_StreamRef.m_iIBase;

        // Reset the VMap Table for the next submesh
        x_memset( &VMap[ iMinRawIndex ], ~0, MaxRawIndex - iMinRawIndex + 1 );
    }

    //
    // Optimization phase 
    //
    for ( eng_geom_rsc::submesh& Submesh : CompilerData.m_Submesh ) JobBlock.SubmitJob( [&]()
    {
        if ( Submesh.m_TechniqueType == eng_material_rsc::TECH_TYPE_RIGID )
        {
            eng_geom_rsc::temp_index_stream& TempIStream = CompilerData.m_TempIndices[ Submesh.m_StreamRef.m_iIStream ];

            xptr2<uint32_t> lNewIndices;
            lNewIndices.Alloc( Submesh.m_StreamRef.m_ICount );

            //
            // Optimize indices
            //
            {
                s32             MaxIndexRef = 0;    

                xptr2<uint32_t> lIndices;
                lIndices.Alloc( Submesh.m_StreamRef.m_ICount );

                mutable_ref( rIndex, lIndices );

                if ( TempIStream.m_IndexSize == 2 )
                {
                    const u16* const pIndex = TempIStream.m_p16Indices.m_Ptr;
                    for ( s32 i = 0; i < Submesh.m_StreamRef.m_ICount; i++ )
                    {
                        rIndex[ i ] = pIndex[ i + Submesh.m_StreamRef.m_iIBase ];
                        MaxIndexRef = x_Max( MaxIndexRef, (s32)rIndex[ i ] );
                    }
                }
                else
                {
                    const u32* const pIndex = TempIStream.m_p32Indices.m_Ptr;
                    for ( s32 i = 0; i < Submesh.m_StreamRef.m_ICount; i++ )
                    {
                        rIndex[ i ] = pIndex[ i + Submesh.m_StreamRef.m_iIBase ];
                        MaxIndexRef = x_Max( MaxIndexRef, (s32)rIndex[ i ] );
                    }
                }

                //
                // Optimize this submesh
                //
                mutable_ref( rNewIndex, lNewIndices );
                Forsyth::OptimizeFaces( 
                    &rIndex[ 0 ], 
                    lIndices.getCount(), 
                    MaxIndexRef + 1, 
                    &rNewIndex[0], 
                    CompilerData.m_VertexCacheSize );

                {
                    vertex_cache_stats VertStats;
                    VertStats.ComputeCacheMisses( &rIndex[ 0 ], lIndices.getCount(), CompilerData.m_VertexCacheSize );
                    x_printf( "INFO: ACMR: %f", VertStats.getACMR() );
                    x_printf( "INFO: ATMR: %f", VertStats.getATVR( Submesh.m_StreamRef.m_VCount ) );
                }

            }
            
            //
            // Copy indices back
            //
            {
                const_ref( rNewIndex, lNewIndices );
                if ( TempIStream.m_IndexSize == 2 )
                {
                    u16* const pIndex = TempIStream.m_p16Indices.m_Ptr;
                    for ( s32 i = 0; i < Submesh.m_StreamRef.m_ICount; i++ )
                    {
                        pIndex[ i + Submesh.m_StreamRef.m_iIBase ] = rNewIndex[ i ];
                    }
                }
                else
                {
                    u32* const pIndex = TempIStream.m_p32Indices.m_Ptr;
                    for ( s32 i = 0; i < Submesh.m_StreamRef.m_ICount; i++ )
                    {
                        pIndex[ i + Submesh.m_StreamRef.m_iIBase ] = rNewIndex[ i ];
                    }
                }
            }
        }
        else
        {
            const eng_geom_rsc::temp_index_stream&  TempIStream = CompilerData.m_TempIndices[ Submesh.m_StreamRef.m_iIStream ];

            if ( TempIStream.m_IndexSize == 2 )
            {
                const u16* const pIndex = TempIStream.m_p16Indices.m_Ptr;
                vertex_cache_stats VertStats;
                VertStats.ComputeCacheMisses( 
                    &pIndex[ Submesh.m_StreamRef.m_iIBase ],  
                    Submesh.m_StreamRef.m_ICount, 
                    CompilerData.m_VertexCacheSize );
                x_printf( "INFO: ACMR: %f", VertStats.getACMR() );
                x_printf( "INFO: ATMR: %f", VertStats.getATVR( Submesh.m_StreamRef.m_VCount ) );
            }
            else
            {
                const u32* const pIndex = TempIStream.m_p32Indices.m_Ptr;
                vertex_cache_stats VertStats;
                VertStats.ComputeCacheMisses( 
                    &pIndex[ Submesh.m_StreamRef.m_iIBase ],  
                    Submesh.m_StreamRef.m_ICount, 
                    CompilerData.m_VertexCacheSize );
                x_printf( "INFO: ACMR: %f", VertStats.getACMR() );
                x_printf( "INFO: ATMR: %f", VertStats.getATVR( Submesh.m_StreamRef.m_VCount ) );
            }
        }
    });

    //
    // TODO: Short vertices based on indices so that the memory is more continuous when the GPU pulls the data
    //

    JobBlock.FinishJobs();

    //
    // Tell the mesh how many submeshes it has
    //
    CompilerData.m_GeomRSC.m_nBones              = RawGeom.m_Bone.getCount();
    CompilerData.m_GeomRSC.m_nFaces              = RawGeom.m_Facet.getCount();
    CompilerData.m_GeomRSC.m_nVertexStreams      = CompilerData.m_TempVertices.getCount();
    CompilerData.m_GeomRSC.m_pTempVertexStream.m_Ptr = &CompilerData.m_TempVertices[ 0 ];
    CompilerData.m_GeomRSC.m_nSubMeshs           = CompilerData.m_Submesh.getCount();
    CompilerData.m_GeomRSC.m_pSubMesh.m_Ptr      = &CompilerData.m_Submesh[0];
    CompilerData.m_GeomRSC.m_nMeshes             = CompilerData.m_Mesh.getCount();
    CompilerData.m_GeomRSC.m_pMesh.m_Ptr         = &CompilerData.m_Mesh[ 0 ];

    // Set the cmds
    {
        const_ref( rCmd, CompilerData.m_lCmds );
        CompilerData.m_GeomRSC.m_nCmds               = CompilerData.m_lCmds.getCount();
        CompilerData.m_GeomRSC.m_pCmds.m_Ptr         = (eng_geom_rsc::cmd*) (CompilerData.m_GeomRSC.m_nCmds?&rCmd[ 0 ]:NULL);
    }

    // Set the informed materials
    {
        const_ref( rlInfored, CompilerData.m_lImformed );
        CompilerData.m_GeomRSC.m_pInformed.m_Ptr    = (eng_geom_rsc::informed_ref*)&rlInfored[0];
        CompilerData.m_GeomRSC.m_nInformeds         = rlInfored.getCount();

        ASSERT( eng_resource_guid::getType( rlInfored[0].m_Guid ) == eng_informed_material_rsc::UID );
    }


    // Deal with setting the index streams 
    if( CompilerData.m_TempIndices[ 0 ].m_nIndices == 0 )
    {
        ASSERT( CompilerData.m_TempIndices[ 1 ].m_nIndices > 0 );
        CompilerData.m_GeomRSC.m_nIndexStreams      = 1;
        CompilerData.m_GeomRSC.m_pTempIndexStream.m_Ptr = &CompilerData.m_TempIndices[ 1 ];

        // Need to reindex Index streams References to zero, since there is only one stream now
        for ( eng_geom_rsc::submesh& Submesh : CompilerData.m_Submesh )
        {
            ASSERT( Submesh.m_StreamRef.m_iIStream == 1 );
            Submesh.m_StreamRef.m_iIStream = 0;
        }
    }
    else if ( CompilerData.m_TempIndices[ 1 ].m_nIndices == 0 )
    {
        ASSERT( CompilerData.m_TempIndices[ 0 ].m_nIndices > 0 );
        CompilerData.m_GeomRSC.m_nIndexStreams = 1;
        CompilerData.m_GeomRSC.m_pTempIndexStream.m_Ptr = &CompilerData.m_TempIndices[ 0 ];
    }
    else
    {
        CompilerData.m_GeomRSC.m_nIndexStreams = 2;
        CompilerData.m_GeomRSC.m_pTempIndexStream.m_Ptr = &CompilerData.m_TempIndices[ 0 ];
    }

    // Deal with setting the bbox for the geom
    CompilerData.m_GeomRSC.m_BBox.Identity( );
    for ( const eng_geom_rsc::mesh& Mesh : CompilerData.m_Mesh )
    {
        CompilerData.m_GeomRSC.m_BBox += Mesh.m_BBox;
    }
}

//-------------------------------------------------------------------------------------------------

void mesh_compiler_base::ExportResource( const eng_geom_rsc::geom_rsc& GeomRsc ) const
{
    //
    // Go though all the targets
    //
    for( const platform& Target : m_Target )
    {
        if( Target.m_bValid == FALSE )
            continue;

        //
        // Ok Ready to save everything then
        //
        xserialfile     SerialFile;
        xstring         FinalRscPath;
        
        FinalRscPath = getFinalResourceName( Target );
        
        SerialFile.Save( (const char*)FinalRscPath, GeomRsc, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
        
        // for debugging
        if( (1) )
        {
            xserialfile   test;
            eng_geom_rsc::geom_rsc* pGeom;
            test.Load( (const char*)FinalRscPath, pGeom );
            
            ASSERT( eng_resource_guid::getType( pGeom->m_pInformed.m_Ptr[0].m_Guid ) == eng_informed_material_rsc::UID );

            ASSERT( pGeom );
            x_delete( pGeom );
        }
    }
}

//-------------------------------------------------------------------------------------------------

xbool mesh_compiler_base::CompilationUniqueness( void ) const
{
    //
    // Lets go through the list of platforms and find out if 
    //
    xbool bSingleCompile = TRUE;
    const_ref( rMain, m_Key.m_Main );
    for ( s32 j = 0; j < m_Target.getCount(); j++ )
    {
        // We are not compiling for this platform so skip it
        if ( !m_Target[ j ].m_bValid )
            continue;

        const mesh_compiler_key::main_params& KeyParams = rMain[ j ];

        if ( x_FlagIsOn( KeyParams.m_OverrideBits, mesh_compiler_key::main_params::SINGLE_COMPILATION_MAIN_FLAGS ) )
        {
            bSingleCompile = FALSE;
            break;
        }
    }

    return bSingleCompile;
}

//-------------------------------------------------------------------------------------------------

void mesh_compiler_base::CleanPathForMaterialsAndTextures( rawgeom& RawGeom ) const
{
    for ( rawgeom::informed_material& Material : RawGeom.m_InformedMaterial )
    {
        Material.m_MaterialShader = RemoveProjectPath( Material.m_MaterialShader );
        for ( rawgeom::informed_material::params& Param : Material.m_Params )
        {
            if ( Param.m_Type == rawgeom::informed_material::PARAM_TYPE_TEXTURE )
            {
                xstring Temp;
                Temp.Copy( Param.m_Value );
                Temp = RemoveProjectPath( Temp );
                x_strcpy( Param.m_Value, Param.m_Value.getCount(), Temp );
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------
// References:
// https://docs.unrealengine.com/latest/INT/Engine/Content/FBX/ImportOptions/index.html
// https://docs.unrealengine.com/latest/INT/Engine/Content/FBX/StaticMeshes/index.html
// http://docs.unity3d.com/Manual/FBXImporter-Model.html
//
//-------------------------------------------------------------------------------------------------
// Notes:
//
// -- VERTEX ORGANIZATION
// Because it is possible for the vertices to have many possible descriptions. (Multiple UVS, COLORS, etc)
// we will break the vertices into two different sections; a Standard Stream, and a Material Stream.
// * Standard Stream
//   - Position
//   - UV0
// * BTN Stream
//   - Binormal
//   - Tangent
//   - Normal
// * Extended streams (These ideally should be broken up in streams based on the material)
//   - UV1..n
//   - Color0..n
//
// -- CONVENTIONS
// Units are in cm (centemeters)
// Name Conventions in the mesh: LOD-#,
//
//--- OPTIONS
// BOOL - Clean Mesh
// BOOL - Recompute Normals  --- This option is not needed max can export and generate these
// BOOL - Recompute Tangents --- This option is not needed max can export and generate these
// FLOAT - Scale FBX         --- This is kind of dangerous and should really be the responsability of the user
// BOOL - Use Pitvot
// INT - Max Weights
// BOOL  - Compress BTN         --- Compresses the BTN to u8
// BOOL  - Compress Weights     --- Compresses weights to u8
// BOOL  - Allow UV Compression --- Any UVs in range [0-1] will be converted to u16
// BOOL  - Compress Position    --- Not supported yet in the future will compress verticess to s16 fixed point
// BOOL  - Force Single Vertex Format --- Forces the hold mesh to be completly uniform from a vertex point of view... do we need this?
// BOOL  - Articulated          --- If not articulated removes any hirarchy and it will have a single matrix
// BOOL  - MergeMeshes          --- Merges all meshes into a single one
//
//--- COLLISIONS
//
//--- MATERIALS
// Materials instances must provide information:
// - Vertex descriptions (What is the spected organization of the vertices)
// - Constants and their values
// - 
//
// [ Material Type + Default values for its parameters + verts desc == Shader (VS/PS) ] <- [ Material Instance == Overwrites of the default values ] 
// <- [ Material Instace == Can override another material instance values ].
//
// Way 1
// A DirectX Shader is provided to max which uses all the right names for the properties of the material.
// Then the mesh should contain these properties linked with the directx shader. This directX shader will be a mirror of
// what is going on in the game. The shader name could event contain the --guid of the real game shader.
// Way 2
// When the user imports the mesh all the materials base are presented to the user using the MAX material names but the guids to the material instance is not there. 
// Then the user just assign a material instance to the user object.
//-------------------------------------------------------------------------------------------------
void mesh_compiler_base::onCompile( void )
{
    xsafe_array<compiler_data, X_PLATFORM_COUNT>    Platform;
    xsafe_array<rawgeom, X_PLATFORM_COUNT>          RawGeom;
    xsafe_array<rawanim, X_PLATFORM_COUNT>          RawAnim;
    xbool                                           bSingleCompile;

    //
    // Determine Uniqueness of the compilation
    //
    bSingleCompile = CompilationUniqueness( );

    //
    // Load all the rawmesh
    //
    m_Key.m_Main.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    const_ref( rMain, m_Key.m_Main );
    if ( bSingleCompile )
    {
        //
        // Load the mesh
        //
        fbx_to_rawmesh  FbxToRaw;

        // IOS vertex cache size is 12
        Platform[ 0 ].m_VertexCacheSize     = 32;
        Platform[ 0 ].m_MartrixCacheSize    = 64;

        //
        // Save Log Information
        // delcaration vars
        //
        x_light_trigger         LocalTrigger;
        x_inline_light_jobs     LightJob;
        rawgeom                 TempRawGeom;


        //
        // Decide how we are going to compile
        //
        if ( rMain[ 0 ].m_bForceStatic )
        {
            FbxToRaw.Convert2RawGeom( RawGeom[ 0 ], xfs( "%s/%s",
                (const char*)m_ProjectPath, 
                (const char*)rMain[ 0 ].m_FilePath ) );

            RawGeom[ 0 ].CleanMesh( );
            RawGeom[ 0 ].SortFacetsByMeshMaterialBone( );

            CleanPathForMaterialsAndTextures( RawGeom[ 0 ] );

            //
            // Dump usefull log
            //
            LightJob.setup( LocalTrigger, [&]()
            {
                char FileName[ xfile::MAX_FNAME ];
                x_splitpath( rMain[ 0 ].m_FilePath, NULL, NULL, FileName, NULL );

                RawGeom[ 0 ].Save( xfs( "%s/%s.rawgeom", (const char*)m_LogPath, FileName) );
                RawAnim[ 0 ].Save( xfs( "%s/%s.rawanim", (const char*)m_LogPath, FileName) );
            });

            g_Scheduler.StartLightJobChain( LightJob );
        }
        else
        {
            x_inline_light_jobs_block<8>    JobBlock;
            rawanim                         Skeleton;

            FbxToRaw.Convert2RawGeom( TempRawGeom, RawAnim[0], xfs( "%s/%s",
                (const char*)m_ProjectPath, 
                (const char*)rMain[ 0 ].m_FilePath ) );

            JobBlock.SubmitJob( [&]()
            {
                const_ref( rMain, m_Key.m_Main );
                TempRawGeom.CleanWeights( rMain[0].m_MaxNumberOfWeights, 0.001f );
                TempRawGeom.CleanMesh( );
                TempRawGeom.SortFacetsByMeshMaterialBone( );
                //TempRawGeom.ComputeBoneInfo();
            });

            JobBlock.SubmitJob( [&]()
            {
                if ( rMain[ 0 ].m_SkeletonFile.IsEmpty() == FALSE )
                {
                    skeleton_compiler_key SkeletonKey( *this );
                    SkeletonKey.LoadDefaults( m_ProjectPath );

                    xstring KeyFilePath;
                    KeyFilePath.Format( "%s/%s.txt",
                                        (const char*)m_ProjectPath,
                                        (const char*)rMain[ 0 ].m_SkeletonFile );

                    SkeletonKey.Load( KeyFilePath );

                    const_ref( rSkelMain, SkeletonKey.m_Main );

                    xstring Skeletonpath;
                    Skeletonpath.Format("%s/%s",
                        (const char*)getImportedPathForKeyFile( rMain[ 0 ].m_SkeletonFile ),
                        (const char*)rSkelMain[ 0 ].getImportedSkeletonFileName() );

                    RawAnim[0].Load( Skeletonpath );
                }
                else
                {
                    RawAnim[ 0 ].CleanUp();
                }
            });

            ////////////////////////////////////////////////////////////////
            JobBlock.FinishJobs();

            JobBlock.SubmitJob( [&]()
            {
                TempRawGeom.ApplyNewSkeleton( RawAnim[ 0 ] );
            });

            JobBlock.SubmitJob( [&]()
            {
                CleanPathForMaterialsAndTextures( TempRawGeom );
            });

            ////////////////////////////////////////////////////////////////
            JobBlock.FinishJobs();

            //
            // Dump useful log
            //
            LightJob.setup( LocalTrigger, [&]()
            {
                char FileName[ xfile::MAX_FNAME ];
                x_splitpath( m_RscFileName, NULL, NULL, FileName, NULL );

                TempRawGeom.Save( xfs( "%s/%s_ALLPLATFORMS.rawgeom", (const char*)m_LogPath, FileName) );
                RawAnim[ 0 ].Save( xfs( "%s/%s_ALLPLATFORMS.rawanim", (const char*)m_LogPath, FileName) );
            });
            g_Scheduler.StartLightJobChain( LightJob );

            //
            // Finally get ready for the action
            //
            PreprocessRawGeomWithArmCompiler( 
                Platform[ 0 ],
                RawGeom[ 0 ], 
                TempRawGeom );
        }

        //
        // Compile the structures
        //

        ComputeStructures( Platform[ 0 ], RawGeom[ 0 ], xplatform( 0 ) );

        //
        // Export the geom resource
        //
        ExportResource( Platform[ 0 ].m_GeomRSC );

        //
        // Create key files for any informed material
        //
        {
            compiler_data&  CompilerData = Platform[ 0 ];
            const rawgeom&  aRawGeom     = RawGeom[ 0 ];
            const xplatform Platform     = xplatform( 0 );

            const_ref( rInterInformed, CompilerData.m_lInterInformed );
            for ( const informed_inter& InterInformed : rInterInformed )
            {
                if ( InterInformed.m_bNewInformedFile == FALSE )
                    continue;

                informed_material_compiler_key InformedKey(*this);
                InformedKey.LoadDefaults( m_ProjectPath );

                g_PropFilePath.Set( InformedKey, "Key/Main/Global/MaterialShader", InterInformed.m_InformedORMaterial );  

                //
                // Write all properties
                //
                InformedKey.setEntryParamCount( InterInformed.m_lInformedTech.getCount() );
                const_ref( m_rInformedTech, InterInformed.m_lInformedTech )
                for( const infortech_inter& InformedTech : m_rInformedTech )
                {
                    xstring     TechniquePlatform;
                    const char* pScopeName = "Global";
              
                    // Set the basic string      
                    if( Platform ) pScopeName = x_PlatformString( Platform );
                    TechniquePlatform.Format( "Key/Entries/Entry[%d]/%s", m_rInformedTech.getIterator(), pScopeName );

                    // Set the name of the material technique
                    g_PropString.Set( InformedKey, xfs("%s/TechniqueName", (const char*)TechniquePlatform ), InformedTech.m_TechName );  
                    
                    const xarray<rawgeom::informed_material::params>& lParams = aRawGeom.m_InformedMaterial[ InformedTech.m_iRawInformed ].m_Params;
                    for ( const rawgeom::informed_material::params& Param : lParams )
                    {
                        xstring     UniformID;
                        const char* pIDStart = x_strstr( Param.m_Name, "_" );
                        const char* pIDEnd   = pIDStart?x_strstr( pIDStart+1, "_" ):NULL;
                        const s32   Length   = s32(pIDEnd - pIDStart - 1);

                        if ( pIDStart == NULL || pIDEnd == NULL || Length <= 0 || Length > 8 )
                            continue;

                        char Buffer[16];
                        x_strncpy( Buffer, pIDStart+1, Length, sizeof(Buffer) );

                        s32 ID = x_atod32( Buffer, 10 );
                        UniformID.Format( "%s/UniformUID[%d]",
                            (const char*)TechniquePlatform,
                            ID );

                        switch ( Param.m_Type )
                        {
                        case rawgeom::informed_material::PARAM_TYPE_BOOL:
                            {
                                xbool bValue = x_stristr(Param.m_Value,"true")!=0;
                                g_PropBool.Set( InformedKey, xfs("%s/Bool", (const char*)UniformID ), bValue );  
                                break;
                            }
                        case rawgeom::informed_material::PARAM_TYPE_F1:
                            {
                                f32 Value = x_atof32( Param.m_Value );
                                g_PropFloat.Set( InformedKey, xfs("%s/V1", (const char*)UniformID ), Value );
                                break;  
                            }
                       case rawgeom::informed_material::PARAM_TYPE_F2:
                            {
                                break;
                            }
                       case rawgeom::informed_material::PARAM_TYPE_F4:
                       case rawgeom::informed_material::PARAM_TYPE_F3:
                            {
                                xvector3d Value;
                                char* pComman;
                                Value.m_X = x_atof32( Param.m_Value );
                                Value.m_Y = x_atof32( pComman = (x_strstr( Param.m_Value, "," ) + 1) );
                                Value.m_Z = x_atof32( x_strstr( pComman, "," ) + 1 );
                                g_PropVector3.Set( InformedKey, xfs("%s/V3", (const char*)UniformID ), Value );
                                break;  
                            }
                                                                         /*
                       case rawgeom::informed_material::PARAM_TYPE_F4:
                            {
                                xvector4 Value;
                                char* pComman;
                                Value.m_X = x_atof32( Param.m_Value );
                                Value.m_Y = x_atof32( pComman = (x_strstr( Param.m_Value, "," ) + 1) );
                                Value.m_Z = x_atof32( pComman = (x_strstr( pComman, "," ) + 1) );
                                Value.m_W = x_atof32( x_strstr( pComman, "," ) + 1 );
                                g_PropVector4.Set( InformedKey, xfs("%s/V4", (const char*)TechniquePlatform ), Value );
                                break;
                            }
                                */
                       case rawgeom::informed_material::PARAM_TYPE_TEXTURE:
                            {
                                g_PropFilePath.Set( InformedKey, xfs("%s/TextureRSC", (const char*)UniformID ), Param.m_Value );
                                break;
                            }
                       default:
                           {
                                  ASSERT(0);
                           }
                        };
                    }
                }

                //
                // Save Key file
                //
                xstring FinalPath;

                FinalPath.Format( "%s/%s.txt", 
                                  (const char*)m_ProjectPath,
                                  (const char*)InterInformed.m_NewInformedRSC );

                InformedKey.Save( FinalPath );
            }
        }



        //
        // Sync all the jobs
        //
        LocalTrigger.LocalSync();
    }
    else
    {
        //
        // TODO: Here we could make our system more optimum by deciding perhaps which rawmeshes are really unique
        //       But we will wait untill this issue comes up to add additional optimizations
        //
        for ( s32 j = 0; j < m_Target.getCount( ); j++ )
        {
            // We are not compiling for this platform so skip it
            if ( !m_Target[ j ].m_bValid )
                continue;

            fbx_to_rawmesh                          FbxToRaw;
//            const mesh_compiler_key::main_params&   KeyParams = rMain[ j ];

           // FbxToRaw.Convert2RawMesh( m_lRawMesh[ j ], m_Key.m_Main[ j ].m_FilePath );
        }
    }

    //
    // Export Files
    //
//    ExportAtlas( m_ExternalKeys );
//    ExportResource();

}
