#pragma once

#include <fbxsdk.h>
#include <map>
#include "x_Base.h"
#include "RawGeom.h"

class fbx_to_rawmesh
{
public:
                            fbx_to_rawmesh          ( void );
                            ~fbx_to_rawmesh         ( void );
    void                    Convert2RawGeom         ( rawanim& RawAnim, const char* pFileName );
    void                    Convert2RawGeom         ( rawgeom& RawGeom, rawanim& RawAnim, const char* pFileName );
    void                    Convert2RawGeom         ( rawgeom& RawGeom, const char* pFileName );

    
protected:
    
    typedef std::map<FbxNode*, s32>      NodeIndexMap;
    typedef xarray<rawanim::key_frame>   key_frame_array;

    enum type_btn
    {
        TYPE_NORMAL,
        TYPE_BINORMAL,
        TYPE_TANGENT
    };

    struct transform
    {
        xvector3d       m_Translation;
        xvector3d       m_Scale;
        xquaternion     m_Rotation;

        void Clear( void )
        {
            m_Translation.Zero();
            m_Scale.Set(1.0f, 1.0f, 1.0f);
            m_Rotation.Identity();
        }
    };

    struct track_info
    {
        f32                     m_Start;
        f32                     m_Stop;
        xbool                   m_bHasTranslation;
        xbool                   m_bHasRotation;
        xbool                   m_bHasScaling;
        f32                     m_FrameRate;
        s32                     m_FramesCount;
        key_frame_array         m_KeyFrames;

        track_info( void ) : m_KeyFrames( XMEM_FLAG_ALIGN_16B )
        {
            m_Start             = 0;
            m_Stop              = 0;
            m_bHasTranslation   = FALSE;
            m_bHasRotation      = FALSE;
            m_bHasScaling       = FALSE;
            m_FrameRate         = 60.0f;
            m_FramesCount       = 0;
        }

        void Merge( const track_info& Info )
        {
            m_Start             = x_Min(Info.m_Start, m_Start);
            m_Stop              = x_Max(Info.m_Stop, m_Stop);
            m_FrameRate         = x_Max(Info.m_FrameRate, m_FrameRate);
            m_bHasTranslation   = m_bHasTranslation  || Info.m_bHasTranslation;
            m_bHasRotation      = m_bHasRotation     || Info.m_bHasRotation;
            m_bHasScaling       = m_bHasScaling      || Info.m_bHasScaling;
        }
    };

    struct node_info
    {
        FbxNode*                m_pNode;
        node_info*              m_pParent;
        xarray<node_info*>      m_Children;
        track_info              m_TrackInfo;
        xbool                   m_HasAnimation;

        transform               m_BindPose;
        transform               m_InvBindPose;
        transform               m_Local;

        node_info( void )
        {
            m_pNode         = NULL;
            m_pParent       = NULL;
            m_HasAnimation  = FALSE;

            m_BindPose.Clear();
            m_InvBindPose.Clear();
            m_Local.Clear();
        }
    };

    struct mesh_info
    {
        FbxMesh*                m_pMesh;
        FbxNode*                m_pNode;
        s32                     m_VertexStartOffset;
        s32                     m_VertexCount;
        xptr<u32>               m_Polygons;
        xptr<s32>               m_iMaterial;
        s32                     m_PolygonCount;

        mesh_info( void )
        {
            m_pMesh             = NULL;
            m_pNode             = NULL;
            m_VertexStartOffset = 0;
            m_VertexCount       = 0;
            m_PolygonCount      = 0;
        }
    };

    struct material_temp
    {
        bool operator < ( const material_temp& X ) const { return m_pFBXMaterial < X.m_pFBXMaterial; }
        FbxSurfaceMaterial*     m_pFBXMaterial;
        s32                     m_UID;
    };

protected:

    void                    Initialize          ( void );
    xbool                   Load                ( const char* FBXFilePath );
    void                    Convert             ( void );
    void                    BuildRawGeom        ( rawgeom& RawMesh );
    void                    BuildRawAnim        ( rawanim& RawAnim );

    char*                   AllocString         ( const char* String );
    node_info*              ReadNode            ( FbxNode* Node, xbool& IsBone );
    s32                     AddMaterial         ( FbxSurfaceMaterial* Material );
    xbool                   ReadAnimation       ( void );
    void                    UpdateAnimTime      ( FbxAnimCurve* Curve, track_info& TrackInfo, f32 AnimStart, f32 AnimStop );
    void                    ReadMesh            ( FbxNode* Node );
    xvector3d               GetBTN              ( FbxMesh* Mesh, s32 iNorm, s32 VertexIndex, s32 VertexCounter, type_btn Type );
    void                    GetUV               ( FbxMesh* Mesh, s32 iUV, s32 VertexIndex, s32 UVIndex, f32& u, f32& v );
    FbxAMatrix              GetGeometryTransform( FbxNode* Node );
    s32                     FindJointIndex      ( const FbxNode* Node );
    s32                     FindNodeIndex       ( const FbxNode* Node );
    node_info*              FindNode            ( const FbxNode* Node );
    void                    ConvertMatrix       ( const FbxAMatrix& Input, transform& Output );
    xmatrix4                ConvertMatrix       ( const FbxAMatrix& Input ) const;
    node_info*              AddNode             ( const FbxNode* Node );
    void                    ConvertMatrix       ( const transform& Input, xmatrix4& Output );


    FbxManager*                             m_pManager              = NULL;
    FbxScene*                               m_pScene                = NULL;
    xarray<material_temp>                   m_lMaterialKey;
    xarray<rawgeom::informed_material>      m_lInformedMaterial;

    xarray<node_info*>                      m_Nodes;
    NodeIndexMap                            m_NodeIndexMap;

    xarray<rawgeom::vertex>                 m_Vertices;
    NodeIndexMap                            m_MeshMap;
    xarray<mesh_info>                       m_Meshes;
    f32                                     m_AnimationStartTime    =   0;
    xbool                                   m_bArticulated          = FALSE;
    xbool                                   m_bReadAnimation;
    xbool                                   m_bReadMesh;
};

