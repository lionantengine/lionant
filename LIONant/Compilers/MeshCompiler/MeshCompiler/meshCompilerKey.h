//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef MESH_COMPILER_KEY_OBJECT_H
#define MESH_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"

class mesh_compiler_key : public compiler_key_object
{
public:

    struct entry_params : public entry_params_base {};
    struct main_params : main_params_base_link<main_params>
    {
        enum param_flags
        {
            OVERRIDE_MAIN_FILEPATH          = X_BIT(0),
            OVERRIDE_MAIN_MAX_WEIGHTS       = X_BIT(1),
            OVERRIDE_FORCE_STATIC           = X_BIT(2),
            OVERRIDE_SKELETON_FILE          = X_BIT(3),
            OVERRIDE_KEEP_ATTACH_ANIMATION  = X_BIT(4),

            SINGLE_COMPILATION_MAIN_FLAGS   = 0
                                              | OVERRIDE_MAIN_FILEPATH  
                                              | OVERRIDE_MAIN_MAX_WEIGHTS
                                              | OVERRIDE_FORCE_STATIC
                                              | OVERRIDE_SKELETON_FILE
                                              | OVERRIDE_KEEP_ATTACH_ANIMATION
        };

        virtual void  onPropEnum        ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const;
        virtual xbool onPropQuery       ( xproperty_query& Query );
                void  onUpdateFromSrc   ( const main_params& Src, const u64 Masks );
 
        xstring     m_FilePath              = X_STR("");            // Path to the source data
        s32         m_MaxNumberOfWeights    = 4;                    // Maximun amount of weights supported by the mesh
        xbool       m_bForceStatic          = FALSE;
        xbool       m_bKeepAnimation        = TRUE;
        xstring     m_SkeletonFile          = X_STR( "" );
    };
        
public:
    
                                mesh_compiler_key           ( const compiler_base& CompilerBase ) : compiler_key_object(CompilerBase){}
    virtual const char*         getCompilerName             ( void ) const { return "MeshCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const { return "mesh";         }
    
    KEY_STANDARD_STUFF
            
protected:
    
    friend class mesh_compiler_base;
};


#endif
