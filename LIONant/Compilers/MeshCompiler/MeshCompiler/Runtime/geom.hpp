
#ifndef GEOM_HPP
#define GEOM_HPP

//=========================================================================
// INCLUDES
//=========================================================================

#include "x_files.hpp"
#include "Auxiliary\MiscUtils\Fileio.hpp"
#include "Material_Prefs.hpp"

//=========================================================================
// GEOM
//=========================================================================

//
// A single "geom" can be made of multiple "meshs".
// This allows multiple objects to be stored in the same file.
// For example: all LOD levels, a single character object with different heads.
// 
// A "mesh" represents a complete object within a geom.
// For example: an LOD or a model of a head.
//    
// Each mesh is made from one or more "submeshs".
// There is a submesh for every material used by the mesh.
//

struct geom
{
    enum
    {
        VERSION = 13,
    };

    //-------------------------------------------------------------------------

    struct bone
    {
        bbox            BBox ;

        void FileIO( fileio& File );
    } ;

    struct mesh
    {
        char            Name[32];
        bbox            BBox;
        s32             nSubMeshs;
        s32             iSubMesh;
        s32             nBones ;            // Number of bones used
        s16             nFaces;
        s16             nVertices;

        void FileIO( fileio& File );
    };

    struct submesh
    {
        s32             iDList;             // Index into list of display lists
        s32             iMaterial;          // Index of the Material that this SubMesh uses
        f32             WorldPixelSize;     // Average World Pixel size for this SubMesh
        u32             BaseSortKey;        // used internally by the rendering system

        void FileIO( fileio& File );
    };

    struct material
    {
        struct uvanim
        {
			// TODO: support different anim types (eg ping-pong)
			s32 Type;
            s32 nKeys;
            s32 iKey;
            s32 FPS;
        
            void FileIO( fileio& File );
        };
    
        enum
        {
            MAX_PARAMS          = 12,
        }; 

        material_type   Type;
        s32             nTextures;                      // Total number of textures used in the material
        s32             iTexture;                       // Index into global texture list for the Geom
        s32             nDiffuse;                       // Number of Diffuse Maps
        s32             nEnvironment;                   // Number of Environment Maps
        s32             nDetail;                        // Number of Detail Maps
        f32             Params[ MAX_PARAMS ];           // Material parameters
        uvanim          UVAnim;                         // UV Animation data
        s32             nTexAnimFrames;
        s32             iTexAnimFrame;

        void FileIO( fileio& File );
    };

    struct texture
    {
        char FileName[256];      
        
        void FileIO( fileio& File );
    };

    struct uvkey
    {
        f32 OffsetU;
        f32 OffsetV;
        
        void FileIO( fileio& File );
    };

    struct texanimframe
    {
        xhandle MatHandle;

        void FileIO( fileio& File );
    };

    //-------------------------------------------------------------------------
            
            geom            ( void );
            geom            ( fileio& File );
            ~geom           ( void );
    void    FileIO          ( fileio& File );
    s32     GetNFaces       ( void )          const;
    s32     GetNVerts       ( void )          const;
    xbool   HasBitmapAnim   ( s32 iMaterial ) const;
    xbool   HasUVAnim       ( s32 iMaterial ) const;
    
    s32     AddRef          ( void );
    s32     Release         ( void );
    s32     GetRefCount     ( void )          const;

    s32     GetMeshIndex    ( const char* pName )        const;
    s32     GetSubMeshIndex ( s32 iMesh, s32 iMaterial ) const;
    xbool   HasBitmapAnim   ( s32 iMesh, s32 iMaterial ) const;
    xbool   HasUVAnim       ( s32 iMesh, s32 iMaterial ) const;
    void    GetTexAnimCounts( s32& nBitmapAnims,
                              s32& nUVAnims )            const;

    bbox            m_BBox;
    s32             m_RefCount;
    platform        m_Platform;
    s32             m_Version;
    s32             m_nFaces;       // including all meshes/lods
    s32             m_nVertices;    // including all meshes/lods
    s32             m_nBones;
    bone*           m_pBone;
    s32             m_nMeshes;
    mesh*           m_pMesh;
    s32             m_nSubMeshs;
    submesh*        m_pSubMesh;
    s32             m_nMaterials;
    material*       m_pMaterial;
    s32             m_nTextures;
    texture*        m_pTexture;        
    s32             m_nUVKeys;
    uvkey*          m_pUVKey;
    s32             m_nTexAnimFrames;
    texanimframe*   m_pTexAnimFrames;   // corresponds to registered materials
    xhandle         m_hGeom;            // handle to the registered geom
};

//=========================================================================

inline s32 geom::AddRef( void )
{
    return (++m_RefCount);
}

//=========================================================================

inline s32 geom::Release( void )
{
    ASSERT( m_RefCount > 0 );
    return (--m_RefCount);
}

//=========================================================================

inline s32 geom::GetRefCount( void ) const
{
    return m_RefCount;
}

//=========================================================================

inline s32 geom::GetNFaces( void ) const
{
    s32 Total = 0;
    for ( s32 i = 0; i < m_nMeshes; i++ )
    {
        Total += m_pMesh[i].nFaces;
    }

    return Total;
}

//=========================================================================

inline s32 geom::GetNVerts( void ) const
{
    s32 Total = 0;
    for ( s32 i = 0; i < m_nMeshes; i++ )
    {
        Total += m_pMesh[i].nVertices;
    }

    return Total;
}

//=========================================================================

inline xbool geom::HasBitmapAnim( s32 iMaterial ) const
{
    ASSERT( (iMaterial >= 0) && (iMaterial < m_nMaterials) );
    return (m_pMaterial[iMaterial].nTexAnimFrames > 1);
}

//=========================================================================

inline xbool geom::HasUVAnim( s32 iMaterial ) const
{
    ASSERT( (iMaterial >= 0) && (iMaterial < m_nMaterials) );
    return (m_pMaterial[iMaterial].UVAnim.nKeys > 0);
}

//=========================================================================

inline s32 geom::GetMeshIndex( const char* pName ) const
{
    for ( s32 i = 0; i < m_nMeshes; i++ )
    {
        if ( !x_strcmp(m_pMesh[i].Name, pName) )
        {
            return i;
        }
    }

    return -1;
}

//=========================================================================

inline s32 geom::GetSubMeshIndex( s32 iMesh, s32 iMaterial ) const
{
    ASSERT( (iMesh>=0) && (iMesh<m_nMeshes) );
    for ( s32 i = m_pMesh[iMesh].iSubMesh;
          i < m_pMesh[iMesh].iSubMesh+m_pMesh[iMesh].nSubMeshs;
          i++ )
    {
        if ( m_pSubMesh[i].iMaterial == iMaterial )
            return i;
    }

    return -1;
}

//=========================================================================

inline xbool geom::HasBitmapAnim( s32 iMesh, s32 iMaterial ) const
{
    s32 iSubMesh = GetSubMeshIndex( iMesh, iMaterial );
    if ( iSubMesh < 0 )
        return FALSE;
    else
        return HasBitmapAnim( m_pSubMesh[iSubMesh].iMaterial );
}

//=========================================================================

inline xbool geom::HasUVAnim( s32 iMesh, s32 iMaterial ) const
{
    s32 iSubMesh = GetSubMeshIndex( iMesh, iMaterial );
    if ( iSubMesh < 0 )
        return FALSE;
    else
        return HasUVAnim( m_pSubMesh[iSubMesh].iMaterial );
}

//=========================================================================

inline void geom::GetTexAnimCounts( s32& nBitmapAnims, s32& nUVAnims ) const
{
    nBitmapAnims = 0;
    nUVAnims     = 0;
    for ( s32 iMesh = 0; iMesh < m_nMeshes; iMesh++ )
    {
        for ( s32 iMat = 0; iMat < m_nMaterials; iMat++ )
        {
            if ( HasBitmapAnim( iMesh, iMat ) )
                nBitmapAnims++;
            if ( HasUVAnim( iMesh, iMat ) )
                nUVAnims++;
        }
    }
}

//=========================================================================
#endif
//=========================================================================

