//==============================================================================
//
//  GeomBone.hpp
//
//==============================================================================

#ifndef __GEOM_BONE_HPP__
#define __GEOM_BONE_HPP__

//==============================================================================
// INCLUDES
//==============================================================================
#include "x_files.hpp"
#include "x_math.hpp"
#include "StickBone.hpp"


//==============================================================================
// CLASSES
//==============================================================================

// Class that maps a geometry bone to a stick bone
struct geom_bone
{
// Data
public:
    matrix4         m_SkinBind ;        // Skin bind matrix of this bone
    matrix4         m_RagdollInvBind ;  // Inverse ragdoll bind matrix of this bone
    const char*     m_pName ;           // Name
    s32             m_StickBone ;       // Index of stick bone that it's attached too
    f32             m_BindT ;           // BindT of stick bone that it's attached too
    bbox            m_LocalBBox ;       // Local space bounding box of bones verts
    xcolor          m_Color ;           // Debug color
                    
// Functions
public:

    // Constructor
         geom_bone() ;

    // Initialization functions
    void Init( const char*      pName,
               stick_bone       StickBones[],
               s32              StickBone,
               f32              BindT,
               const matrix4&   SkinBind,
               bbox&            LocalBBox,
               xcolor           Color,
               xbool            bAnimController = FALSE );
         
    // Matrix functions
    matrix4& GetRagdollL2W( stick_bone StickBones[] ) ;
    void     GetSkinL2W   ( stick_bone StickBones[], matrix4& SkinL2W ) ;
    void     GetBBoxL2W   ( stick_bone StickBones[], matrix4& L2W ) ;

    // Render functions
    void Render( stick_bone StickBones[] ) ;

} PS2_ALIGNMENT(16) ;

//==============================================================================

// Returns local to world matrix for ragdoll stick bones
inline
matrix4& geom_bone::GetRagdollL2W( stick_bone StickBones[] )
{
    ASSERT(m_StickBone != -1) ;
    return StickBones[m_StickBone].GetL2W() ;
}

//==============================================================================

#endif  // #ifndef __GEOM_BONE_HPP__
