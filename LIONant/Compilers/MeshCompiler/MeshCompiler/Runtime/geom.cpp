
#include "geom.hpp"

//=========================================================================

geom::geom( void )
{    
    x_memset( this, 0, sizeof(geom) );
    m_Version = geom::VERSION; 
    m_hGeom   = HNULL;
}

//=========================================================================

geom::geom( fileio& File )
{
	(void)File;
    m_RefCount = 0;
    m_hGeom    = HNULL;

    if( m_Version != geom::VERSION )
        x_throw( xfs("Version error: App wants %d, geometry is %d", geom::VERSION, m_Version ) );
}

//=========================================================================

geom::~geom( void )
{
    ASSERT( GetRefCount()==0 );
}

//=========================================================================

void geom::FileIO( fileio& File )
{
    File.Static( m_BBox );

    // load in the file data
    ASSERT( sizeof(platform) == sizeof(s32) );
    File.Static( *((u32*)&m_Platform) );

    File.Static( m_Version );

    File.Static( m_nFaces );
    File.Static( m_nVertices );
    
    File.Static( m_nBones );
    File.Static( m_pBone, m_nBones );

    File.Static( m_nMeshes );
    File.Static( m_pMesh, m_nMeshes );

    File.Static( m_nSubMeshs );
    File.Static( m_pSubMesh, m_nSubMeshs );

    File.Static( m_nMaterials );
    File.Static( m_pMaterial, m_nMaterials );

    File.Static( m_nTextures );
    File.Static( m_pTexture, m_nTextures );

    File.Static( m_nUVKeys );
    File.Static( m_pUVKey, m_nUVKeys );

    File.Static( m_nTexAnimFrames );
    File.Static( m_pTexAnimFrames, m_nTexAnimFrames );
}

//=========================================================================

void geom::texture::FileIO( fileio& File )
{
    File.Static( FileName, 256 );
}

//=========================================================================

void geom::material::FileIO( fileio& File )
{
    File.Static ( *((s32*)&Type)     );
    File.Static ( nTextures          );
    File.Static ( iTexture           );
    File.Static ( nDiffuse           );
    File.Static ( nEnvironment       );
    File.Static ( nDetail            );
    File.Static ( Params, MAX_PARAMS );
	File.Static ( UVAnim             );
    File.Static ( nTexAnimFrames     );
    File.Static ( iTexAnimFrame      );
}

//=========================================================================

void geom::material::uvanim::FileIO( fileio& File )
{
	File.Static( Type  );
    File.Static( nKeys );
    File.Static( iKey  );
    File.Static( FPS   );
}

//=========================================================================

void geom::uvkey::FileIO( fileio& File )
{
    File.Static( OffsetU );
    File.Static( OffsetV );
}

//=========================================================================

void geom::texanimframe::FileIO( fileio& File )
{
    (void)File;
}

//=========================================================================

void geom::submesh::FileIO( fileio& File )
{
    File.Static ( iDList    );
    File.Static ( iMaterial );
    File.Static ( WorldPixelSize );
}

//=========================================================================

void geom::mesh::FileIO( fileio& File )
{
    File.Static ( Name, sizeof( Name ) );
    File.Static ( BBox );        
    File.Static ( nSubMeshs );
    File.Static ( iSubMesh );
    File.Static ( nBones );
    File.Static ( nFaces );
    File.Static ( nVertices );
}

//=========================================================================

void geom::bone::FileIO( fileio& File )
{
    File.Static ( BBox );        
}

//=========================================================================

