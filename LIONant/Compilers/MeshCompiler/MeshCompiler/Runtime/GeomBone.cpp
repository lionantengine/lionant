//==============================================================================
//
//  GeomBone.cpp
//
//==============================================================================

//==============================================================================
// INCLUDES
//==============================================================================
#include "GeomBone.hpp"
#include "Entropy.hpp"


//==============================================================================
// CLASSES
//==============================================================================

geom_bone::geom_bone()
{
    m_pName       = NULL ;
    m_StickBone   = -1 ;
    m_BindT       = 0 ;
    m_SkinBind.Identity() ;
    m_RagdollInvBind.Identity() ;
    m_LocalBBox.Clear() ;
    m_Color = XCOLOR_WHITE ;
}

//==============================================================================

void geom_bone::Init( const char*       pName,
                      stick_bone        StickBones[],
                      s32               StickBone,
                      f32               BindT,
                      const matrix4&    SkinBind,
                      bbox&             LocalBBox,
                      xcolor            Color,
                      xbool             bAnimController )
{
    // Keep info
    m_pName       = pName ;
    m_StickBone   = StickBone ;
    m_BindT       = BindT ;
    m_SkinBind    = SkinBind ;
    m_LocalBBox   = LocalBBox ;
    m_Color       = Color ;

    // Compute ragdoll inverse bind matrix
    m_RagdollInvBind = GetRagdollL2W(StickBones) ;  // Get ragdoll bind
    m_RagdollInvBind.Invert() ;                     // Convert to inverse

    // Setup for correct playback in an animation controller
    // (animation performs a multiply by the SkinInvBind at the very end,
    //  so this counter-acts it)
    if(bAnimController)
        m_RagdollInvBind = m_RagdollInvBind * SkinBind ;
}

//==============================================================================

// Returns local to world for skinned geometry
void geom_bone::GetSkinL2W( stick_bone StickBones[], matrix4& SkinL2W )
{
/*
    We need the mapping from from RagBind -> SkinBind
    i.e. RagBind * Offset = SkinBind

    Now:
    matrix4 Offset = m_RagdollInvBind * m_SkinBind ;

    So we have:
    return RagdollL2W * (m_RagdollInvBind * m_SkinBind) * m_SkinInvBind ;

    // Which nicely optimizes down to the simple:
    RagdollL2W * m_RagdollInvBind
*/
    
    // Lookup stick bone L2W
    matrix4& RagdollL2W = GetRagdollL2W( StickBones ) ;

    SkinL2W = RagdollL2W * m_RagdollInvBind ;
}

//==============================================================================

// Returns local to world for local bounding box
// (not time critical - only used to init ragdoll def)
void geom_bone::GetBBoxL2W ( stick_bone StickBones[], matrix4& L2W )
{
    matrix4& RagdollL2W = GetRagdollL2W(StickBones) ;
    L2W = RagdollL2W * m_RagdollInvBind * m_SkinBind ;
}

//==============================================================================

void geom_bone::Render( stick_bone StickBones[] )
{
    ASSERT(m_StickBone != -1) ;

    matrix4 L2W ;
    
    GetBBoxL2W(StickBones, L2W) ;
    draw_SetL2W(L2W) ;
    draw_BBox(m_LocalBBox, m_Color) ;

    draw_ClearL2W() ;
    
    // Draw bind point
    //StickBones[m_StickBone].GetL2W(L2W, m_BindT) ;
    //L2W = StickBones[m_StickBone].GetL2W() ;
    //draw_Point(L2W.GetTranslation(), XCOLOR_WHITE) ;
}

//==============================================================================
