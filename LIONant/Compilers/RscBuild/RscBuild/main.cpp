//
//  main.cpp
//  RscBuild
//
//  Created by Tomas Arce on 9/7/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include <stdio.h>
#include "x_base.h"
#include "buildbase.h"

//---------------------------------------------------------------------------------------

void PrintF( const char* pString )
{
    // Call the system printf.
    printf( "[RSB] %s", pString );
    LOGD(pString);
    fflush(stdout);
}



// -NEWGUID -PROJECT "C:\RationWorls\ProjectAlpha\Data\Bin\.."
// -BUILDTYPE DEBUG -PROJECT "C:\RationWorls\ProjectAlpha\Data\Bin\.." -TARGET PC
// -BUILDTYPE DEBUG -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/ProjectAlpha/Data" -TARGET OSX IOS
//---------------------------------------------------------------------------

int main(int argc, const char* argv[])
{
    build_base BuildBase;
    s32 Err = 0;
    
    g_bExceptionBreak = FALSE;
    
    // Have a nice printf
    x_SetFunctionPrintF( PrintF );
    
    x_try;
    {
        if( BuildBase.Parse( argc, argv ) )
        {
            BuildBase.StartBuild();
        }
    }
    x_catch_begin;
    {
        const char* pMsg = x_ExceptionMessage();
        if( x_strncmp( pMsg, "ERROR:", 6 ) == 0 )
        {
            x_printf( "%s\n", pMsg );
        }
        else
        {
            x_printf( "ERROR: %s\n", pMsg );
        }
        
        Err = 1;
    }
    x_catch_end;
    
    return Err;
}

