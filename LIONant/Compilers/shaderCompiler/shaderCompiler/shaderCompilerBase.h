//
//  fontBaseCompiler.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef SHADER_COMPILER_BASE_H
#define SHADER_COMPILER_BASE_H

#include "shaderCompilerKey.h"
#include "eng_base.h"

class shader_compiler_base : public compiler_base
{
public:
    
                                        shader_compiler_base    ( void ) : m_Key( *this ) {}
                                       ~shader_compiler_base    ( void ){}

    virtual void                        onCompile               ( void );
    virtual compiler_key_object&        getKeyObject            ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject            ( void ) const { return m_Key;             }
    
protected:

    struct compilation_data
    {
        eng_shader_rsc::shader_rsc              m_ShaderRSC;
        xarray<eng_vertex_desc::attribute_link> m_VertexAttributeLink;
        xarray<eng_shader_rsc::uniform_type>    m_Uniforms;
    };
    
protected:

    void                                CompilationUniqueness   ( void );
    void                                CompileKeyPlatform      ( compilation_data& CompilationData, const shader_compiler_key::main_params& MainParams ) const;
    void                                ExportResource          ( const compilation_data& CompilationData, const platform& Target ) const;
    
protected:
    
    shader_compiler_key                             m_Key;
    xbool                                           m_bSingleCompilation;
    xsafe_array<compilation_data, X_PLATFORM_COUNT> m_lCompilationData;
};


#endif
