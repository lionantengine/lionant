//
//  fontBaseCompiler.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "x_base.h"
#include "shaderCompilerBase.h"

#pragma comment(lib, "OpenGL32.lib")


//-------------------------------------------------------------------------------------------------

void shader_compiler_base::CompilationUniqueness( void )
{
    m_bSingleCompilation = TRUE;
    
    //
    // Lets go throught the key platforms
    //
    const_ref( rMain, m_Key.m_Main );
    for ( s32 j = 0; j<m_Target.getCount( ); j++ )
    {
        // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
        if ( !m_Target[ j ].m_bValid )
            continue;

        const shader_compiler_key::main_params& MainParams = rMain[ j ];

        if ( x_FlagIsOn( MainParams.m_OverrideBits, shader_compiler_key::main_params::SINGLE_COMPILATION_FLAGS ) )
        {
            m_bSingleCompilation = FALSE;
            break;
        }
    }
}

//-------------------------------------------------------------------------------------------------

void shader_compiler_base::CompileKeyPlatform( compilation_data& CompilationData, const shader_compiler_key::main_params& MainParams ) const
{
    x_inline_light_jobs_block<8> Block;

    //
    // Do a Sanity check for the data
    //
    Block.SubmitJob( [this, &CompilationData, &MainParams]()
    {
        if ( MainParams.m_lAttribute.getCount() == 0 && MainParams.m_ShaderType == eng_shader_rsc::shader_type::SHADER_TYPE_VERTEX )
        {
            x_throw( "ERROR: All vertex shader must have Attributes" );
        }

        if ( MainParams.m_lUniformDef.getCount() == 0 && MainParams.m_ShaderType == eng_shader_rsc::shader_type::SHADER_TYPE_VERTEX )
        {
            x_throw( "ERROR: All vertex shader must have at least one uniform" );
        }

        //
        // make sure that there are not duplicate attributes
        //
        for ( s32 i = 0; i < MainParams.m_lAttribute.getCount(); i++ )
        {
            const shader_compiler_key::vertex_attribute& MainAttr = MainParams.m_lAttribute[i];
            
            for ( s32 j = i+1; j < MainParams.m_lAttribute.getCount(); j++ )
            {
                const shader_compiler_key::vertex_attribute& Attr = MainParams.m_lAttribute[j];

                if ( MainAttr.m_Name == Attr.m_Name )
                {
                    x_throw( "ERROR: I found a duplicate attribute in the shader %s and %s",
                        (const char*)MainAttr.m_Name,
                        (const char*)Attr.m_Name  );
                }

                if ( MainAttr.m_UsageType == Attr.m_UsageType )
                {
                    x_throw( "ERROR: I found a duplicate attribute usage type the shader %s and %s",
                        (const char*)MainAttr.m_Name,
                        (const char*)Attr.m_Name  );
                }

                if ( MainAttr.m_UsageType > Attr.m_UsageType )
                {
                    x_throw( "ERROR: Internal compiler error attributes dont seem to be shorted properly( %s and %s )",
                        (const char*)MainAttr.m_Name,
                        (const char*)Attr.m_Name  );
                }
            }
        }

        //
        // Make sure that there are not duplicates in the uniforms
        //
        for ( s32 i = 0; i < MainParams.m_lUniformDef.getCount(); i++ )
        {
            const shader_compiler_key::uniform& MainUniform = MainParams.m_lUniformDef[i];
            
            for ( s32 j = i+1; j < MainParams.m_lUniformDef.getCount(); j++ )
            {
                const shader_compiler_key::uniform& Uniform = MainParams.m_lUniformDef[j];

                if ( MainUniform.m_Name == Uniform.m_Name )
                {
                    x_throw( "ERROR: I found a duplicate uniform in the shader %s and %s",
                        (const char*)MainUniform.m_Name,
                        (const char*)Uniform.m_Name  );
                }

                if ( MainUniform.m_Type > Uniform.m_Type )
                {
                    x_throw( "ERROR: Internal compiler error uniforms dont seem to be shorted properly( %s and %s )",
                        (const char*)MainUniform.m_Name,
                        (const char*)Uniform.m_Name  );
                }

                if ( MainUniform.m_UID == Uniform.m_UID )
                {
                    x_throw( "ERROR: There are two different uniforms with the same UID( %s and %s )",
                        (const char*)MainUniform.m_Name,
                        (const char*)Uniform.m_Name  );
                }
            }
        }
    });

    //
    // Do a Sanity check for the data
    //
    Block.SubmitJob( [this, &CompilationData, &MainParams]()
    {
        for ( const shader_compiler_key::vertex_attribute& MainAttr : MainParams.m_lAttribute )
        {
            eng_vertex_desc::attribute_link& Dest = CompilationData.m_VertexAttributeLink.append();

            Dest.m_pVarName.m_Ptr = &MainAttr.m_Name[0];
            Dest.m_UsageType      = MainAttr.m_UsageType;
        }

        for ( const shader_compiler_key::uniform& MainUniform : MainParams.m_lUniformDef )
        {
            eng_shader_rsc::uniform_type& Dest = CompilationData.m_Uniforms.append();
            Dest.m_pName.m_Ptr      = &MainUniform.m_Name[0];
            Dest.m_Type             =  MainUniform.m_Type;
        }

        //
        // Link all the data
        //
        CompilationData.m_ShaderRSC.m_nAttributes           = CompilationData.m_VertexAttributeLink.getCount();
        CompilationData.m_ShaderRSC.m_nUniforms             = CompilationData.m_Uniforms.getCount();
        
        if( CompilationData.m_VertexAttributeLink.getCount() )
            CompilationData.m_ShaderRSC.m_pAttributeLink.m_Ptr  = &CompilationData.m_VertexAttributeLink[0];
        else
            CompilationData.m_ShaderRSC.m_pAttributeLink.m_Ptr  = NULL;

        if( CompilationData.m_Uniforms.getCount() )
            CompilationData.m_ShaderRSC.m_pUniform.m_Ptr        = &CompilationData.m_Uniforms[0];
        else
            CompilationData.m_ShaderRSC.m_pUniform.m_Ptr        = NULL;

        CompilationData.m_ShaderRSC.m_ShaderType            = MainParams.m_ShaderType;
        CompilationData.m_ShaderRSC.m_pShader.m_Ptr         = (const char*)MainParams.m_ShaderFile;
    });

    Block.FinishJobs();
}

//-------------------------------------------------------------------------------------------------

void shader_compiler_base::ExportResource( const compilation_data& CompilationData, const platform& Target ) const
{
    //
    // Ok Ready to save everything then
    //
    xserialfile     SerialFile;
    xstring         FinalRscPath;
    
    FinalRscPath = getFinalResourceName( Target );
    
    SerialFile.Save( (const char*)FinalRscPath, CompilationData.m_ShaderRSC, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
    
    // for debugging
    if( (1) )
    {
        xserialfile                 test;
        eng_shader_rsc::shader_rsc* pMyShader;
        test.Load( (const char*)FinalRscPath, pMyShader );
        
        ASSERT( pMyShader );
        x_delete( pMyShader );
    }
}

//-------------------------------------------------------------------------------------------------
#ifdef TARGET_PC
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
  switch (uMsg){
  case WM_DESTROY:
    PostQuitMessage(0);
    break;
  default:
    break;
  }
  return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

//-------------------------------------------------------------------------------------------------
static HDC     s_hDC = NULL;
static HGLRC   s_hRC = NULL;
static HWND    s_hWnd = NULL;

xbool InitOpenGL( void )
{
    HINSTANCE hInstance;

    hInstance = GetModuleHandle(NULL);  // Grab An Instance For Our Window

    const char* class_name = "DUMMY_CLASS";
 
    WNDCLASSEX wx;
    ZeroMemory(&wx, sizeof(WNDCLASSEX));
    wx.cbSize           = sizeof(WNDCLASSEX);
    wx.style            = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wx.lpfnWndProc      = WindowProc;
    wx.hInstance        = hInstance;
    wx.hCursor          = LoadCursor(nullptr, IDC_ARROW);
    wx.lpszClassName    = class_name;
    if( !RegisterClassEx(&wx) ) 
        return FALSE;

    s_hWnd = CreateWindowEx( 0, class_name, "dummy_name", 0, 0, 0, 0, 0, NULL, NULL, hInstance, NULL );
    if ( s_hWnd ==0  ) 
        return FALSE;

    if (!(s_hDC = GetDC(s_hWnd))) {
        return FALSE;
    }
    
    // pixel format
    PIXELFORMATDESCRIPTOR pfd;
    x_memset(&pfd, 0, sizeof(pfd));
	pfd.nSize = sizeof( pfd );
	pfd.nVersion        = 1;
	pfd.dwFlags         = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType      = PFD_TYPE_RGBA;
	pfd.cColorBits      = 24;
	pfd.cDepthBits      = 16;
	pfd.iLayerType      = PFD_MAIN_PLANE;

    const s32 pf  = ChoosePixelFormat(s_hDC, &pfd);
    const s32 res = SetPixelFormat(s_hDC, pf, &pfd);

    if ( pf == 0 || res == 0 )
        return FALSE;

   // DescribePixelFormat(s_hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

    if (!(s_hRC = wglCreateContext(s_hDC))) {
        return FALSE;
    }

    if(!wglMakeCurrent(s_hDC,s_hRC)) { 
        return FALSE;
    }

    glewInit();

    return TRUE;
}

//-------------------------------------------------------------------------------------------------

void KillOpenGL( void )
{
    wglMakeCurrent(NULL, NULL);         // make our context not current 
    wglDeleteContext(s_hRC);            // delete the rendering context 
    ReleaseDC(s_hWnd, s_hDC);           // release handle to DC 
}

#else
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
xbool InitOpenGL( void )
{
    static const int SCREEN_WIDTH   = 1024;
    static const int SCREEN_HEIGHT  = 768;
    
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init();
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);


    
    return TRUE;
}

//-------------------------------------------------------------------------------------------------

void KillOpenGL( void )
{
    eng_Kill();
}

#endif

//-------------------------------------------------------------------------------------------------
static
void CompileShaders( const eng_shader_rsc::shader_rsc& Shader )
{
    eng_gshader GShader;

    //
    // Compile Fragment Shaders
    //
    if ( Shader.m_ShaderType == eng_shader_rsc::SHADER_TYPE_VERTEX )
    {
        eng_vshader VShader;

        VShader.LoadFromMemory( 
            Shader.m_pShader.m_Ptr, 
            x_strlen(Shader.m_pShader.m_Ptr)-1,
            Shader.m_pAttributeLink.m_Ptr,
            Shader.m_nAttributes,
            FALSE );
    }
    else if ( Shader.m_ShaderType == eng_shader_rsc::SHADER_TYPE_FRAGMENT )
    {
        eng_fshader FShader;
        FShader.LoadFromMemory( Shader.m_pShader.m_Ptr, x_strlen(Shader.m_pShader.m_Ptr)-1 );
    }
    else if ( Shader.m_ShaderType == eng_shader_rsc::SHADER_TYPE_GEOMETRY )
    {
    }
}

//-------------------------------------------------------------------------------------------------

void shader_compiler_base::onCompile( void )
{
    //
    // Init OpenGL
    //
    if( InitOpenGL() == FALSE )
        x_throw( "ERROR: Failt to initialize OPENGL" );

    //
    // Determine Uniqueness of the compilation
    //
    CompilationUniqueness();

    //
    // Single compilation vs multiple
    //
    if( m_bSingleCompilation )
    {
        //
        // Prepare the data structures
        //
        {
            const_ref( rMain, m_Key.m_Main );
            CompileKeyPlatform( m_lCompilationData[0], rMain[0] );
        }

        //
        // Test the shaders
        //
        CompileShaders( m_lCompilationData[0].m_ShaderRSC );

        //
        // Export Files
        //
        x_inline_light_jobs_block<8> Block;
        
        for( const platform& Target : m_Target )
        {
            if( Target.m_bValid == FALSE )
                continue;
            
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[0], Target );
            });
        }
        Block.FinishJobs();   
    }
    else
    {
        x_inline_light_jobs_block<8> Block;
        
        //
        // Process for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;
            
            // Compile
            Block.SubmitJob( [this, &Target]()
            {
                const_ref( rMain, m_Key.m_Main );
                CompileKeyPlatform( m_lCompilationData[Target.m_Platform], rMain[Target.m_Platform] );
            });
        }
        
        Block.FinishJobs();

        //
        // Process for each platform
        //
        for ( const platform& Target : m_Target )
        {
            // We are not compiling for this platform so skip it (we always skip there for zero, it is never a valid platform)
            if ( Target.m_bValid == FALSE )
                continue;

            //
            // Test the shaders (this must run in the main thread)
            //
            CompileShaders( m_lCompilationData[Target.m_Platform].m_ShaderRSC );

            //
            // Export
            //
            Block.SubmitJob( [this, &Target]()
            {
                ExportResource( m_lCompilationData[ Target.m_Platform ], Target);
            });
        }
        Block.FinishJobs();
    }

    KillOpenGL();
}

