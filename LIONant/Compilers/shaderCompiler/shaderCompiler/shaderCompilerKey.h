//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef SHADER_COMPILER_KEY_OBJECT_H
#define SHADER_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"
#include "eng_base.h"

class shader_compiler_key : public compiler_key_object
{
public:
    
    typedef eng_vertex_desc::attribute_usage attr_usage;

    struct vertex_attribute
    {
        xstring                     m_Name          = X_STR("");
        attr_usage                  m_UsageType     = attr_usage::ATTR_USAGE_NULL;
    };

    typedef eng_shader_program::uniform_type uniform_type;

    struct uniform
    {
        xstring                     m_Name          = X_STR("");
        uniform_type                m_Type          = uniform_type::UNIFORM_TYPE_NULL;
        s32                         m_UID           = -1;
    };

    typedef eng_shader_rsc::shader_type shader_type;
    struct main_params : public main_params_base_link< main_params >
    {
        enum main_param_flags
        {
            OVERRIDE_SHADER_FILEPATH            =   X_BIT( 0 ),
            SINGLE_COMPILATION_FLAGS            =   0
                                                    | OVERRIDE_SHADER_FILEPATH
        };

        virtual void  onPropEnum        ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool onPropQuery       ( xproperty_query& Query ) override final;
                void  onUpdateFromSrc   ( const main_params& Src, const u64 Masks );

        static const xprop_enum::entry  m_pUniformTypeEnum[];
        static const xprop_enum::entry  m_pAttributeTypeEnum[ ];

        shader_type                 m_ShaderType            = shader_type::SHADER_TYPE_NULL;
        xstring                     m_ShaderSrcPath         = X_STR("");
        xarray<vertex_attribute>    m_lAttribute;
        xarray<uniform>             m_lUniformDef;
        xstring                     m_ShaderFile;
    };

    struct entry_params : public entry_params_base {};

public:
    
                                shader_compiler_key         ( const compiler_base& Base ) : compiler_key_object( Base ) {}
                               ~shader_compiler_key         ( void ) {}
    virtual const char*         getCompilerName             ( void ) const override { return "ShaderCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const override { return "shader";         }
    
protected:

    virtual xbool               onUniquePropQuery           ( xproperty_query& Query ) override final;
    virtual void                LoadDefaults                ( const xstring& ProjectPath ) override final;
            void                ParseShaderFile             ( main_params& MainParams ) const;
    
protected:

    KEY_STANDARD_STUFF

    xstring     m_ProjectPath;
    
protected:
    
    friend class shader_compiler_base;
    friend class informed_material_compiler_base;
    friend class material_compiler_base;
    friend struct key_passthrough;
};

#endif
