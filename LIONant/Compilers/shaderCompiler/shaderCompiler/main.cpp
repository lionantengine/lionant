//
//  main.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "compilerBase.h"
#include "shaderCompilerBase.h"

// -BUILDTYPE RELEASE -PROJECT "/Users/Tomas/Documents/Rational Worlds/Source/LIONant/xReference/Data" -TARGET OSX IOS -INPUT "GameData/CompilerKeys/Diffuse-V--7vie_jqvi_f9jz.shader"

int main(int argc, const char * argv[])
{
    shader_compiler_base Compiler;
    
    if( Compiler.Parse( argc, argv ) )
    {
         Compiler.Compile();
    }
    
    return 0;
}
