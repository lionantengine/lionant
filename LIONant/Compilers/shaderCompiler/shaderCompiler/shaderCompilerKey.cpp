//
//  materialCompilerKeyObject.cpp
//  materialCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "shaderCompilerKey.h"

const xprop_enum::entry shader_compiler_key::main_params::m_pUniformTypeEnum[ ] =
{
    { eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA,    X_STR( "INFORMED_TEXTURE_RGBA" )     },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGB,     X_STR( "INFORMED_TEXTURE_RGB" )      },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL,  X_STR( "INFORMED_TEXTURE_NORMAL" )   },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL,            X_STR( "INFORMED_BOOL" )             },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT,           X_STR( "INFORMED_FLOAT" )            },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_V2,              X_STR( "INFORMED_V2" )               },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_V3,              X_STR( "INFORMED_V3" )               },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_V4,              X_STR( "INFORMED_V4" )               },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_M3,              X_STR( "INFORMED_M3" )               },
    { eng_shader_program::UNIFORM_TYPE_INFORMED_M4,              X_STR( "INFORMED_M4" )               },
    { eng_shader_program::UNIFORM_TYPE_SYSTEM_L2C,               X_STR( "SYSTEM_L2C" )                },
    { eng_shader_program::UNIFORM_TYPE_SYSTEM_L2V,               X_STR( "SYSTEM_L2V" )                },
    { eng_shader_program::UNIFORM_TYPE_SYSTEM_V2C,               X_STR( "SYSTEM_V2C" )                },
    { eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_L,        X_STR( "SYSTEM_LIGHT_IN_L" )         },
    { eng_shader_program::UNIFORM_TYPE_SYSTEM_LIGHT_IN_V,        X_STR( "SYSTEM_LIGHT_IN_V" )         }
};

const xprop_enum::entry shader_compiler_key::main_params::m_pAttributeTypeEnum[] =
{
    { attr_usage::ATTR_USAGE_POSITION,              X_STR( "POSITION" )             },
    { attr_usage::ATTR_USAGE_2_COMPACT_WEIGHTS,     X_STR( "2_COMPACT_WEIGHTS" )    },
    { attr_usage::ATTR_USAGE_4_WEIGHTS,             X_STR( "4_BONE_WEIGHTS" )       },
    { attr_usage::ATTR_USAGE_4_INDICES,             X_STR( "4_BONE_INDICES" )       },
    { attr_usage::ATTR_USAGE_TANGENT,               X_STR( "TANGENT" )              },
    { attr_usage::ATTR_USAGE_BINORMAL,              X_STR( "BINORMAL" )             },
    { attr_usage::ATTR_USAGE_NORMAL,                X_STR( "NORMAL" )               },
    { attr_usage::ATTR_USAGE_00_PARAMETRIC_UV,      X_STR( "00_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_01_PARAMETRIC_UV,      X_STR( "01_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_02_PARAMETRIC_UV,      X_STR( "02_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_03_PARAMETRIC_UV,      X_STR( "03_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_04_PARAMETRIC_UV,      X_STR( "04_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_05_PARAMETRIC_UV,      X_STR( "05_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_06_PARAMETRIC_UV,      X_STR( "06_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_07_PARAMETRIC_UV,      X_STR( "07_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_08_PARAMETRIC_UV,      X_STR( "08_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_09_PARAMETRIC_UV,      X_STR( "09_PARAMETRIC_UV" )     },
    { attr_usage::ATTR_USAGE_00_FULLRANGE_UV,       X_STR( "00_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_01_FULLRANGE_UV,       X_STR( "01_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_02_FULLRANGE_UV,       X_STR( "02_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_03_FULLRANGE_UV,       X_STR( "03_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_04_FULLRANGE_UV,       X_STR( "04_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_05_FULLRANGE_UV,       X_STR( "05_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_06_FULLRANGE_UV,       X_STR( "06_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_07_FULLRANGE_UV,       X_STR( "07_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_00_FULLRANGE_UV,       X_STR( "08_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_09_FULLRANGE_UV,       X_STR( "09_FULLRANGE_UV" )      },
    { attr_usage::ATTR_USAGE_00_RGBA,               X_STR( "00_RGBA" )              },
    { attr_usage::ATTR_USAGE_01_RGBA,               X_STR( "01_RGBA" )              },
    { attr_usage::ATTR_USAGE_02_RGBA,               X_STR( "02_RGBA" )              },
    { attr_usage::ATTR_USAGE_03_RGBA,               X_STR( "03_RGBA" )              },
    { attr_usage::ATTR_USAGE_04_RGBA,               X_STR( "04_RGBA" )              },
    { attr_usage::ATTR_USAGE_05_RGBA,               X_STR( "05_RGBA" )              },
    { attr_usage::ATTR_USAGE_06_RGBA,               X_STR( "06_RGBA" )              },
    { attr_usage::ATTR_USAGE_00_GENERIC_V1,         X_STR( "00_GENERIC_V1" )        },
    { attr_usage::ATTR_USAGE_01_GENERIC_V1,         X_STR( "01_GENERIC_V1" )        },
    { attr_usage::ATTR_USAGE_02_GENERIC_V1,         X_STR( "02_GENERIC_V1" )        },
    { attr_usage::ATTR_USAGE_03_GENERIC_V1,         X_STR( "03_GENERIC_V1" )        },
    { attr_usage::ATTR_USAGE_04_GENERIC_V1,         X_STR( "04_GENERIC_V1" )        },
    { attr_usage::ATTR_USAGE_00_GENERIC_V2,         X_STR( "00_GENERIC_V2" )        },
    { attr_usage::ATTR_USAGE_01_GENERIC_V2,         X_STR( "01_GENERIC_V2" )        },
    { attr_usage::ATTR_USAGE_02_GENERIC_V2,         X_STR( "02_GENERIC_V2" )        },
    { attr_usage::ATTR_USAGE_03_GENERIC_V2,         X_STR( "03_GENERIC_V2" )        },
    { attr_usage::ATTR_USAGE_04_GENERIC_V2,         X_STR( "04_GENERIC_V2" )        },
    { attr_usage::ATTR_USAGE_00_GENERIC_V3,         X_STR( "00_GENERIC_V3" )        },
    { attr_usage::ATTR_USAGE_01_GENERIC_V3,         X_STR( "01_GENERIC_V3" )        },
    { attr_usage::ATTR_USAGE_02_GENERIC_V3,         X_STR( "02_GENERIC_V3" )        },
    { attr_usage::ATTR_USAGE_03_GENERIC_V3,         X_STR( "03_GENERIC_V3" )        },
    { attr_usage::ATTR_USAGE_04_GENERIC_V3,         X_STR( "04_GENERIC_V3" )        },
    { attr_usage::ATTR_USAGE_00_GENERIC_V4,         X_STR( "00_GENERIC_V4" )        },
    { attr_usage::ATTR_USAGE_01_GENERIC_V4,         X_STR( "01_GENERIC_V4" )        },
    { attr_usage::ATTR_USAGE_02_GENERIC_V4,         X_STR( "02_GENERIC_V4" )        },
    { attr_usage::ATTR_USAGE_03_GENERIC_V4,         X_STR( "03_GENERIC_V4" )        },
    { attr_usage::ATTR_USAGE_04_GENERIC_V4,         X_STR( "04_GENERIC_V4" )        }
};

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void shader_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    if ( x_FlagIsOn( Mask, OVERRIDE_SHADER_FILEPATH ) )
        g_PropFilePath.Enum( Enum, iScope, "ShaderFileName",  "The file which contains the source code for the shader", "*.vsh,*.fsh,*gsh", "" );

    //
    // Deal with the attributes 
    //
    {
        const s32 iVAScope = Enum.AddScope( iScope, "VertexAttributes", "Vertex Shader Definitions" );
        g_PropInt.Enum( Enum, iVAScope, "Count", "How many entries does this definition have.", PROP_FLAGS_READ_ONLY );

        for ( const vertex_attribute& Attribute : m_lAttribute )
        {
            const s32 iVDef     = s32(&Attribute - &m_lAttribute[ 0 ]);
            const s32 iAScope   = Enum.AddScope( iVAScope, xfs( "Attribute[%d]", iVDef ), "Vertex Shader Definitions" );

            g_PropString.Enum( Enum, iAScope, "VarName", "Name of the Attribute used in the vertex shader.", PROP_FLAGS_READ_ONLY );
            g_PropEnum.Enum  ( Enum, iAScope, "Type",    "Type of the Vertex Attribute.", PROP_FLAGS_READ_ONLY );
        }
    }

    //
    // Deal with uniforms
    //
    {
        const s32 iUScope = Enum.AddScope( iScope, "UniformLinks", "Uniform Links Definition" );
        g_PropInt.Enum( Enum, iUScope, "Count", "How many uniform entries does this definition have.", PROP_FLAGS_READ_ONLY );
        for ( const uniform& UniformDef : m_lUniformDef )
        {
            const s32 iUniform    = s32(&UniformDef - &m_lUniformDef[ 0 ]);
            const s32 iEntryScope = Enum.AddScope( iUScope, xfs( "Link[%d]", iUniform ), "Uniform Definition" );

            g_PropString.Enum( Enum, iEntryScope, "VarName", "Uniform variable name used in the shader", PROP_FLAGS_READ_ONLY );
            g_PropEnum.Enum( Enum, iEntryScope, "Type", "Uniform Type", PROP_FLAGS_READ_ONLY );
        }
    }
}

//-------------------------------------------------------------------------------------------------

xbool shader_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    if ( Query.Var( "ShaderFileName" ) && g_PropFilePath.Query( Query, m_ShaderSrcPath ) )
    {
        if ( FALSE == Query.IsUserGet( ) ) x_FlagOn( m_OverrideBits, OVERRIDE_SHADER_FILEPATH );
        return TRUE;
    }
    
    if ( Query.Scope( "VertexAttributes" ) )
    {
        s32 Count = m_lAttribute.getCount( );

        if ( Query.Var( "Count" ) && g_PropInt.Query( Query, Count, 0, 100 ) )
        {
            if ( FALSE == Query.IsUserGet() )
            {
                // Only reading
                ASSERT(0);
            }

            return TRUE;
        }

        if ( Query.Scope( "Attribute[]" ) )
        {
            const s32           iVertexDef  = Query.GetArrayIndex( Count );
            vertex_attribute&   Attribute   = m_lAttribute[ iVertexDef ];

            if ( Query.Var( "VarName" ) && g_PropString.Query( Query, Attribute.m_Name ) )
                return TRUE;

            if ( Query.Var( "Type" ) && g_PropEnum.Query( Query, Attribute.m_UsageType, m_pAttributeTypeEnum ) )
                return TRUE;
        }
    }

    if ( Query.Scope( "UniformLinks" ) )
    {
        s32 Count = m_lUniformDef.getCount();

        if ( Query.Var( "Count" ) && g_PropInt.Query( Query, Count, 0, 100 ) )
        {
            if ( FALSE == Query.IsUserGet() )
            {
                // Only reading
                ASSERT(0);
            }

            return TRUE;
        }

        if ( Query.Scope( "Link[]" ) )
        {
            const s32       iUniform    = Query.GetArrayIndex( Count );
            uniform&        UDef        = m_lUniformDef[ iUniform ];

            if ( Query.Var( "VarName" ) && g_PropString.Query( Query, UDef.m_Name ) )
               return TRUE;

            if ( Query.Var( "Type" ) && g_PropEnum.Query( Query, UDef.m_Type, m_pUniformTypeEnum ) )
                return TRUE;
        }
    }

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void shader_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    if ( !x_FlagIsOn( Masks, OVERRIDE_SHADER_FILEPATH ) )
    {
        m_ShaderSrcPath = Src.m_ShaderSrcPath;
        m_lAttribute.Copy( Src.m_lAttribute );
        m_lUniformDef.Copy( Src.m_lUniformDef );
        m_ShaderFile.Copy( Src.m_ShaderFile );
    }
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------

xbool shader_compiler_key::onUniquePropQuery( xproperty_query& Query )
{
    //
    // We need to load the shader file when it is the right time for it
    //
    if( compiler_key_object::onUniquePropQuery( Query ) )
    {
        if ( FALSE == Query.IsUserGet( ) && Query.Var( "ShaderFileName" )  )
        {
            for( s32 i=0; i<X_PLATFORM_COUNT; i++ )
            {
                const char* pScopeName = "Global";
                if(i) pScopeName = x_PlatformString( xplatform(i) );

                if( x_stristr( Query.GetPropertyName( ), pScopeName ) )
                {
                    mutable_ref( rMain, m_Main );
                    ParseShaderFile( rMain[i] );
                    break;
                }
            }
        }

        return TRUE;
    }

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void shader_compiler_key::LoadDefaults( const xstring& ProjectPath )
{
    m_ProjectPath.Copy( ProjectPath );
    compiler_key_object::LoadDefaults( ProjectPath );
}

//---------------------------------------------------------------------------------------

void shader_compiler_key::ParseShaderFile( main_params& MainParams ) const
{
    //
    // Clear all first
    //
    MainParams.m_ShaderType = eng_shader_rsc::SHADER_TYPE_NULL;
    MainParams.m_lAttribute.DeleteAllNodes();
    MainParams.m_lUniformDef.DeleteAllNodes();
    MainParams.m_ShaderFile.Clear();

    // Do we have anything to do?
    if ( MainParams.m_ShaderSrcPath.IsEmpty() )
        return;

    //
    // Determine Shader Type
    //
    if ( MainParams.m_ShaderSrcPath.FindI( "vsh.txt" ) != -1 )
    {
        MainParams.m_ShaderType = eng_shader_rsc::SHADER_TYPE_VERTEX;
    }
    else if ( MainParams.m_ShaderSrcPath.FindI( "fsh.txt" ) != -1  )
    {
        MainParams.m_ShaderType = eng_shader_rsc::SHADER_TYPE_FRAGMENT;
    }
    else if ( MainParams.m_ShaderSrcPath.FindI( "gsh.txt" ) != -1  )
    {
        MainParams.m_ShaderType = eng_shader_rsc::SHADER_TYPE_GEOMETRY;
    }
    else
    {
        x_throw( "ERROR: The Shader [%s] Doesn not have supported extensions '.vsh.txt' or 'fsh.txt' or 'gsh.txt'",  
            (const char*)MainParams.m_ShaderSrcPath );
    }

    //
    // Read the hold file in
    //
    {
        xfile   File;
        xstring Path;

        Path.Format( "%s/%s", 
                     (const char*) m_ProjectPath,
                     (const char*) MainParams.m_ShaderSrcPath );

        if ( FALSE == File.Open( Path, "rb" ) )
            x_throw(  "ERROR: Shader File [%s] can not be open" );
    
        xptr<char> Buffer;
    
        Buffer.Alloc( File.GetFileLength()+1);
        File.ReadRaw( Buffer, 1, File.GetFileLength() );
        Buffer[ File.GetFileLength() ] = 0;

        // Copy to a string
        MainParams.m_ShaderFile.Copy( &Buffer[0],  File.GetFileLength()+1 );
    }

    //
    // Get ready to work
    //
    x_inline_light_jobs_block<8>    Block;
    const xstring&                  ShaderFile = MainParams.m_ShaderFile;

    //
    // Find Attributes
    //
    if( MainParams.m_ShaderType == eng_shader_rsc::SHADER_TYPE_VERTEX ) Block.SubmitJob( [&ShaderFile, &MainParams]()
    { 
        xsafe_array<char,256> Type;
        xsafe_array<char,256> Name;
        xsafe_array<char,256> Flags;
        s32                   Len=0;

        s32 iAttribute = ShaderFile.Find( "attribute", 0 );
        if ( iAttribute < 0 )
            x_throw(  "ERROR: Vertex shader [%s] does not have any attributes", (const char*)MainParams.m_ShaderSrcPath );

        //
        // Read attribute
        //
        do
        {
            iAttribute += x_strlen( "attribute" );
        
            // Skip spaces
            if( x_isspace( ShaderFile[iAttribute]) ) 
                while ( x_isspace( ShaderFile[++iAttribute] ) );

            // Read type
            Len = 0;
            while ( x_isspace( Type[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
            Type[ Len ] = 0;

            // Skip spaces
            if( x_isspace( ShaderFile[iAttribute]) ) 
                while ( x_isspace( ShaderFile[++iAttribute] ) );

            // Read Name
            Len = 0;
            while ( x_isspace( Name[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
            Name[ Len ] = 0;

            // Remove the ';' out of the name
            if ( x_strstr( Name, ";" ) )
            {
                x_strstr( Name, ";" )[0] = 0;
            }

            // if it is an array remove the array descriptor
            if ( x_strstr( Name, "[" ) )
            {
                x_strstr( Name, "[" )[0] = 0;
            }

            while ( 1 )
            {
                // Skip spaces
                if( x_isspace( ShaderFile[iAttribute]) ) 
                    while ( x_isspace( ShaderFile[++iAttribute] ) );

                // Read '//'
                Len = 0;
                while ( x_isspace( Flags[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
                Flags[ Len ] = 0;

                // Skip in case the user put space between ; and the //
                if ( x_strstr( Flags, ";" ) )
                    continue;

                if ( x_strstr( Flags,"//") == NULL )
                    x_throw(  "ERROR: Parsing vertex shader [%s] trying to read attributes flags for variable [%s]",
                    (const char*)MainParams.m_ShaderSrcPath,  
                    &Name[0] );

                break;
            }
        
            // Skip spaces
            if( x_isspace( ShaderFile[iAttribute]) ) 
                while ( x_isspace( ShaderFile[++iAttribute] ) );

            // Read flags
            Len = 0;
            while ( x_isspace( Flags[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
            Flags[ Len ] = 0;

            //
            // Identify type
            //
            static const char* const pTypes[]=
            {
                "float",
                "vec2",
                "vec3",
                "vec4",
                "mat3",
                "mat4",
                NULL
            };

            s32 iType = -1;
            for ( s32 i = 0; pTypes[i]; i++ )
            {
                if ( x_strstr( Type, pTypes[i] ) == 0 )
                {
                    iType = i;
                    break;
                }
            }

            if ( iType == -1 )
                x_throw(  "ERROR: Shader [%s] with variable [%s] we were unable to identify the type [%s]",
                    (const char*)MainParams.m_ShaderSrcPath ,  
                    &Name[0],
                    &Type[0] );

            //
            // Identify attr flag
            //
            attr_usage AttrUsage = attr_usage::ATTR_USAGE_NULL;

            for ( s32 i = 0; i < sizeof( MainParams.m_pAttributeTypeEnum ) / sizeof( xprop_enum::entry ); i++ )
            {
                const xprop_enum::entry& Entry = MainParams.m_pAttributeTypeEnum[ i ];
                if ( x_strstr( Flags, Entry.m_EnumName ) )
                {
                    AttrUsage = attr_usage( Entry.m_EnumID );
                    break;
                }
            }

            if ( AttrUsage == attr_usage::ATTR_USAGE_NULL )
                x_throw(  "ERROR: Shader [%s] with variable [%s] we found not identifiable flag [%s]",
                    (const char*)MainParams.m_ShaderSrcPath ,  
                    &Name[0],
                    &Flags[0] );

            //
            // Create an attribute entry
            //
            vertex_attribute& Attribute = MainParams.m_lAttribute.append();
            Attribute.m_Name.Copy( Name );
            Attribute.m_UsageType = AttrUsage;

            //
            // Read more attributes
            //
            iAttribute = ShaderFile.Find( "attribute", iAttribute );
        } while (iAttribute>0);

        //
        // Sort attributes links
        //
        if( MainParams.m_lAttribute.getCount() )
            x_qsort( &MainParams.m_lAttribute[0],
                        MainParams.m_lAttribute.getCount(),
                        sizeof( vertex_attribute ),
                        []( const void* pA, const void* pB ) -> s32
            {
                const vertex_attribute& A = *( const vertex_attribute * )pA;
                const vertex_attribute& B = *( const vertex_attribute * )pB;
                if( A.m_UsageType < B.m_UsageType ) return -1;
                if( A.m_UsageType == B.m_UsageType )
                    x_throw( "ERROR: I found two attributes that have the same flags [%s] [%s] ",
                        (const char*)A.m_Name,
                        (const char*)B.m_Name,
                        (const char*)shader_compiler_key::main_params::m_pUniformTypeEnum[A.m_UsageType].m_EnumName );
                return A.m_UsageType > B.m_UsageType;
            } );
    } );

    //
    // Read uniforms
    //
    Block.SubmitJob( [&ShaderFile, &MainParams]()
    { 
        xsafe_array<char,256> Type;
        xsafe_array<char,256> Name;
        xsafe_array<char,256> Flags;
        s32                   Len=0;

        s32 iAttribute = ShaderFile.Find( "uniform", 0 );
        if ( iAttribute < 0 )
        {   
            // Vertex shader must have uniforms
            if (  MainParams.m_ShaderType == eng_shader_rsc::SHADER_TYPE_VERTEX )
                x_throw(  "ERROR: Vertex shader [%s] does not have any uniforms", (const char*)MainParams.m_ShaderSrcPath );
        }

        //
        // Read uniform
        //
        do
        {
            iAttribute += x_strlen( "uniform" );
        
            // Skip spaces
            if( x_isspace( ShaderFile[iAttribute]) ) 
                while ( x_isspace( ShaderFile[++iAttribute] ) );

            // Read type
            Len = 0;
            while ( x_isspace( Type[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
            Type[ Len ] = 0;

            // Skip spaces
            if( x_isspace( ShaderFile[iAttribute]) ) 
                while ( x_isspace( ShaderFile[++iAttribute] ) );

            // Read Name
            Len = 0;
            while ( x_isspace( Name[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
            Name[ Len ] = 0;

            // Remove the ';' out of the name
            if ( x_strstr( Name, ";" ) )
            {
                x_strstr( Name, ";" )[0] = 0;
            }

            // if it is an array remove the array descriptor
            if ( x_strstr( Name, "[" ) )
            {
                x_strstr( Name, "[" )[0] = 0;
            }

            while ( 1 )
            {
                // Skip spaces
                if( x_isspace( ShaderFile[iAttribute]) ) 
                    while ( x_isspace( ShaderFile[++iAttribute] ) );

                // Read '//'
                Len = 0;
                while ( x_isspace( Flags[ Len++ ] = ShaderFile[ iAttribute++ ] ) == FALSE );
                Flags[ Len ] = 0;

                // Skip in case the user put space between ; and the //
                if ( x_strstr( Flags, ";" ) )
                    continue;

                if ( x_strstr( Flags,"//") == 0 )
                    x_throw(  "ERROR: Parsing vertex shader [%s] trying to read attributes flags for variable [%s]",
                    (const char*)MainParams.m_ShaderSrcPath ,  
                    &Name[0] );

                break;
            }
        
            //
            // Identify type
            //
            static const char* const pTypes[]=
            {
                "sampler2D",
                "float",
                "vec2",
                "vec3",
                "vec4",
                "mat3",
                "mat4",
                "bool",
                NULL
            };

            s32 iType = -1;
            for ( s32 i = 0; pTypes[i]; i++ )
            {
                if ( x_strstr( Type, pTypes[i] ) )
                {
                    iType = i;
                    break;
                }
            }

            if ( iType == -1 )
                x_throw(  "ERROR: Shader [%s] with uniform variable [%s] we were unable to identify the type [%s]",
                    (const char*)MainParams.m_ShaderSrcPath ,  
                    &Name[0],
                    &Type[0] );

            //
            // Identify uniform flags
            //
            s32                              UID;
            eng_shader_program::uniform_type UniformType = eng_shader_program::UNIFORM_TYPE_NULL;
            {
                // for non-textures we expect 2 parameters
                // If it is a texture we expect 3 parameters
                s32 iFirst   = ShaderFile.Find( "[", iAttribute );
                s32 iUID     = ShaderFile.Find( "UID:", iFirst + 1 );
                s32 iSecond  = ShaderFile.Find( ",", iUID + 1 );
                s32 iLast    = ShaderFile.Find( "]", iSecond + 1 );
                
                iAttribute = iLast + 1;
                 
                if ( iFirst >= iLast
                     || iUID > iLast
                     || iUID <= 0 
                     || x_InRange( iSecond - iFirst,  3, 24 ) == FALSE 
                     || x_InRange( iLast  - iSecond,  3, 40 ) == FALSE 
                     || iFirst  ==-1 
                     || iSecond ==-1 
                     || iLast   ==-1 )
                    x_throw(  "ERROR: Shader [%s] with uniform variable [%s] we were unable to read its flags",
                        (const char*)MainParams.m_ShaderSrcPath ,  
                        &Name[0] );

                //
                // Read the Unique ID
                //
                iUID += x_strlen( "UID:" );
                char Number[32];

                // Skip any white space
                if( x_isspace( ShaderFile[iUID]) ) 
                    while ( x_isspace( ShaderFile[++iUID] ) );

                x_strncpy( Number, &ShaderFile[iUID], iSecond - iUID, 32 );

                UID = x_atod32( Number, 10 );
                if ( UID < 0 || UID > 1000 )
                    x_throw(  "ERROR: I readed a var(%s) has a UID of value[%d] but it can only be in the value [0,1000]", 
                        &Name[0],
                        UID );
                //
                // Read the symbolic type
                //
                iSecond++; // skip the ,

                // Skip any white space
                if( x_isspace( ShaderFile[iSecond]) ) 
                    while ( x_isspace( ShaderFile[++iSecond] ) );

                if ( (iLast - iSecond) <= 0 )
                    x_throw(  "ERROR: Unable to find the Symbolic type for uniform [%s]",
                        &Name[0] );

                // Read the Type
                xsafe_array<char,128> SymbolicType;

                SymbolicType[0]=0;
                iLast++;                // Move iLast one character more
                for ( s32 i = 0; iSecond < iLast; iSecond++, i++ )
                {
                    SymbolicType[ i ] = ShaderFile[iSecond];
                    if ( x_isspace( SymbolicType[ i ] ) || SymbolicType[ i ] == ']' )
                    {
                        SymbolicType[ i ] = 0;
                        break;
                    }
                }

                if ( iSecond == iLast )
                    x_throw(  "ERROR: Unable to find the Symbolic end for uniform [%s]",
                        &Name[0] );

                if ( x_strstr( "INFORMED", SymbolicType ) )
                {
                    switch ( iType )
                    {
                    case 1: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_FLOAT; break;
                    case 2: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_V2;    break;
                    case 3: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_V3;    break;
                    case 4: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_V4;    break;
                    case 5: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_M3;    break;
                    case 6: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_M4;    break;
                    case 7: UniformType = eng_shader_program::UNIFORM_TYPE_INFORMED_BOOL;    break;
                    default:
                        {
                            x_throw(  "ERROR: you mark this uniform [%s] as INFORMED but its atomic type does not match to expected values",
                                &Name[0] );
                        }
                    }; 
                }
                else
                {
                    for ( s32 i = 0; i < sizeof( main_params::m_pUniformTypeEnum ) / sizeof( xprop_enum::entry ); i++ )
                    {
                        const xprop_enum::entry& Entry = main_params::m_pUniformTypeEnum[i];
                        if ( x_strstr( SymbolicType, Entry.m_EnumName ) )
                        {
                            UniformType = eng_shader_program::uniform_type( Entry.m_EnumID );
                            break;
                        }
                    }

                    if ( UniformType == 0 )
                        x_throw(  "ERROR: Unable to find a known Symbolic type for [%s] uniform name [%s]",
                            &SymbolicType[0],
                            &Name[0] );

                    if ( iType == 0 )
                    {
                        if ( UniformType < eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_RGBA ||
                             UniformType > eng_shader_program::UNIFORM_TYPE_INFORMED_TEXTURE_NORMAL )
                            x_throw(  "ERROR: We found a uniform that is a texture but does not match with symbolic type [%s] uniform name [%s]",
                                &SymbolicType[0],
                                &Name[0] );
                    }
                }
            }

            //
            // Create an attribute entry
            //
            uniform& Attribute = MainParams.m_lUniformDef.append();
            Attribute.m_Name.Copy( Name );
            Attribute.m_Type    = UniformType;
            Attribute.m_UID     = UID;

            //
            // Read more attributes
            //
            iAttribute = ShaderFile.Find( "uniform", iAttribute );

        } while (iAttribute>0);

        //
        // Sort uniforms
        //
        if( MainParams.m_lUniformDef.getCount() )
            x_qsort( &MainParams.m_lUniformDef[0],
                        MainParams.m_lUniformDef.getCount(),
                        sizeof( uniform ),
                        []( const void* pA, const void* pB ) -> s32
            {
                const uniform& A = *( const uniform *)pA;
                const uniform& B = *( const uniform *)pB;
                if ( A.m_Type < B.m_Type ) return -1;
                return A.m_Type > B.m_Type;
            } );
    } );

    Block.FinishJobs();
}
