
#include "eng_base.h"

class anim_compress : protected eng_anim_group_compressor
{
public:

    void CompressAnimationData(
        xbitstream&                             BitStream, 
        const xtransform*                       pKeys,
        const xvector3*                         pLocalSpaceTranslation,
        s32                                     nBones,
        s32                                     TotalFrames,
        s32                                     TotalStreams,
        s32                                     StartFrame,
        s32                                     EndFrame,
        const f32*                              pLenOfBonesToVerts );

protected:

    void DisplayAnimCompressStats( void );

    void DeltaCompress( 
        xbitstream& Bitstream, 
        const f32*  pSample, 
        s32         Stride, 
        s32         nSamples, 
        f32         Precision );

    void CompressScale( 
        xbitstream&                             BitStream,
        s32                                     iStream,
        const xtransform*                       pKeys,
        s32                                     StartFrame,
        s32                                     EndFrame,
        s32                                     TotalFrames,
        f32                                     DistToChilden );

    void CompressRotation( 
        xbitstream&                             Bitstream,
        s32                                     iStream,
        const xtransform*                       pKeys,
        s32                                     StartFrame,
        s32                                     EndFrame,
        s32                                     TotalFrames,
        f32                                     DistToChildren );

    void CompressTranslation( 
        xbitstream&                             Bitstream,
        s32                                     iStream,
        const xtransform*                       pKeys,
        s32                                     StartFrame,
        s32                                     EndFrame,
        s32                                     TotalFrames,
        f32                                     DistToChilden,
        const xvector3*                         pLocalSpaceTranslation,
        s32                                     nBones );

protected:

    x_qt_counter            m_qtTOTAL_BITS;
    x_qt_counter            m_qtTOTAL_OVERHEAD;
    xsafe_array<s32,8>      m_ScaleCompUses             = {0};
    xsafe_array<s32,8>      m_ScaleCompSize             = {0};
    xsafe_array<s32,8>      m_ScaleCompSamples          = {0};
    xsafe_array<s32,8>      m_RotationCompUses          = {0};
    xsafe_array<s32,8>      m_RotationCompSize          = {0};
    xsafe_array<s32,8>      m_RotationCompSamples       = {0};
    xsafe_array<s32,8>      m_TranslationCompUses       = {0};
    xsafe_array<s32,8>      m_TranslationCompSize       = {0};
    xsafe_array<s32,8>      m_TranslationCompSamples    = {0};
};