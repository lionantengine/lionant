//
//  meshBaseCompiler.h
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef ANIMATION_COMPILER_BASE_H
#define ANIMATION_COMPILER_BASE_H

#include "AnimationCompilerKey.h"
#include "eng_base.h"
#include "RawGeom.h"
#include "eng_Geom.h"

class animation_compiler_base : public compiler_base
{
public:
    
                                        animation_compiler_base     ( void ) : m_Key( *this ) {}
    virtual void                        onCompile                   ( void );
    virtual compiler_key_object&        getKeyObject                ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject                ( void ) const { return m_Key;             }

protected:

    struct shorted_rawanims
    {
        rawanim                                         m_RawAnim;
        const animation_compiler_key::entry_params*     m_pEntry;
        xptr2<xbitstream>                               m_lBitStream;
        xptr2<u8>                                       m_lBoneFlags;
    };

    struct compiler_data
    {
        xvar<eng_anim_group_rsc::anim_group_rsc>    m_varAnimGroupRSC;
        xptr2<eng_anim_group_rsc::anim_info>        m_lAnimInfo;
        xptr2<eng_anim_group_rsc::anim_key_block>   m_lAnimKeyBlock;
        xptr2<eng_anim_group_rsc::anim_prop>        m_lAnimProp;
        xptr2<eng_anim_group_rsc::hash_entry>       m_lAnimHash;

        xptr2<shorted_rawanims>                     m_lRawAnim;
        xvar<rawanim>                               m_varSkeleton;
        xvar<rawgeom>                               m_varSkeletonGeom;
    };


protected:

    void    CompileSpecificPlatform ( compiler_data& CompilerData, xplatform iPlatform ) const;
    void    LoadSkeleton            ( rawgeom& RawGeom, rawanim& RawAnim, const xstring& FilePath ) const;  
    void    LoadAnim                ( rawanim& RawAnim, const xstring& FilePath ) const;  
    void    ExportResource          ( const eng_anim_group_rsc::anim_group_rsc& GeomRsc ) const;

protected:

    animation_compiler_key                              m_Key;

};

#endif
