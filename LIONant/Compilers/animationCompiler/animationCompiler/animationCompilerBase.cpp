//
//  meshBaseCompiler.cpp
//  meshCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "FBX2RawGeom.h"
#include "animationCompilerBase.h"
#include "AnimCompress.h"
#include "skeletonCompilerKey.h"

//-------------------------------------------------------------------------------------------------

void animation_compiler_base::LoadSkeleton( rawgeom& RawGeom, rawanim& RawAnim, const xstring& FilePath ) const
{
    xstring Temp;

    // Remove any path that has to do with the project
    Temp.Copy( FilePath );
    Temp = RemoveProjectPath( Temp );

    // choose base on type
    if( x_stristr( Temp, ".rawanim" ) )
    {
        RawAnim.Load( xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );
    }
    else if( x_stristr( Temp, ".fbx" ) )
    {
        fbx_to_rawmesh FBXToGeom;
        FBXToGeom.Convert2RawGeom( RawGeom, RawAnim, 
            xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );               
    }
    else
    {
        x_throw( "ERROR: Unkown extension for skeleton file [%s]",
            (const char*) FilePath );
    }
}

//-------------------------------------------------------------------------------------------------

void animation_compiler_base::LoadAnim( rawanim& RawAnim, const xstring& FilePath ) const
{
    xstring Temp;

    // Remove any path that has to do with the project
    Temp.Copy( FilePath );
    Temp = RemoveProjectPath( Temp );

    // choose base on type
    if( x_stristr( Temp, ".rawanim" ) )
    {
        RawAnim.Load( xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );
    }
    else if( x_stristr( Temp, ".fbx" ) )
    {
        fbx_to_rawmesh  FBXToGeom;
        FBXToGeom.Convert2RawGeom( RawAnim, 
            xfs("%s/%s", 
            (const char*)m_ProjectPath, 
            (const char*)Temp ) );               
    }
    else
    {
        x_throw("ERROR: Unkown extension for animation file [%s]",
            (const char*) FilePath );
    }
}  

//-------------------------------------------------------------------------------------------------

struct shorted_rawanims
{
    rawanim                                         m_RawAnim;
    const animation_compiler_key::entry_params*     m_pEntry;
};

void animation_compiler_base::CompileSpecificPlatform( compiler_data& CompilerData, xplatform iPlatform ) const
{
    x_inline_light_jobs_block<8>    BlockJobs;
    xbool                           bKeepBind;
    char                            SkeletonFileName[xfile::MAX_FNAME];
    skeleton_compiler_key           SkeletonKey( *this );

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // PRAPARE DATA 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Set the anim group to zero
    // 
    {
        mutable_ref( rAnimGroupRSC, CompilerData.m_varAnimGroupRSC );
        eng_anim_group_rsc::anim_group_rsc& Rsc = rAnimGroupRSC;
        x_memset( &Rsc, 0, sizeof(Rsc) );
    }

    //
    // Load the skeleton key file
    //
    {
        const_ref( rKeyMainPlatform, m_Key.m_Main );
        SkeletonKey.LoadDefaults( m_ProjectPath );
        SkeletonKey.Load( xstring::BuildFromFormat( "%s/%s.txt", 
            (const char*)m_ProjectPath, 
            (const char*)rKeyMainPlatform[ iPlatform ].m_SkeletonPath ));
    
    }

    //
    // Set the bKeepBind Local var
    // Load the skeleton
    // Clean skeleton
    //
    BlockJobs.SubmitJob( [&]()
    {
        const_ref( rvarSkeleton, SkeletonKey.m_Main );
        const_ref( rKeyMainPlatform, m_Key.m_Main );
        bKeepBind = rKeyMainPlatform[ iPlatform ].m_bKeepBind;

        // Store the skeleton name
        x_splitpath( rvarSkeleton[ iPlatform ].m_FilePath, NULL,NULL, SkeletonFileName, NULL );
        

        mutable_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );
        mutable_ref( rSkeleton,     CompilerData.m_varSkeleton );

        auto& KeyMain = rKeyMainPlatform[ iPlatform ];
        if( KeyMain.m_SkeletonPath.IsEmpty() )
            x_throw( "ERROR: Please enter a filepath for the skeleton" );


        xstring ImportKeyPath = getImportedPathForKeyFile( rKeyMainPlatform[ iPlatform ].m_SkeletonPath );

        ImportKeyPath.AppendFormat( "/%s", (const char*)rvarSkeleton[ iPlatform ].getImportedSkeletonFileName() );

        LoadSkeleton( rSkeletonGeom, rSkeleton, ImportKeyPath );

        rSkeleton.ReleaseAndQTReadOnly();
    } );

    //
    // Create a shorted list of raw anims
    //
    BlockJobs.SubmitJob( [&]()
    {
        CompilerData.m_lRawAnim.New( m_Key.m_lEntry.getCount() );

        const_ref( rKeyEntryPlatform, m_Key.m_lEntry );
        mutable_ref( rRawAnim, CompilerData.m_lRawAnim );

        // Set up the pointers
        for( const auto& KeyEntryPlatform : rKeyEntryPlatform )
        {
            auto& RawAnim        = rRawAnim[ rKeyEntryPlatform.getIterator() ];
            RawAnim.m_pEntry     = &KeyEntryPlatform[ iPlatform ];
        }
        
        // Now lets short our list base on the animation name
        x_qsort( &rRawAnim[0], rRawAnim.getCount(), rRawAnim.getEntrySize(), []( const void* pA, const void* pB ) -> s32
        {
            const shorted_rawanims& A = *(const shorted_rawanims*)pA;
            const shorted_rawanims& B = *(const shorted_rawanims*)pB;
            
            // First short everything by then name
            s32 Ret = x_stricmp( A.m_pEntry->m_Name, B.m_pEntry->m_Name );
            if( Ret ) return Ret;

            // if the same name then we short by the weight since playing back will 
            // probabilistically hit the biggest one first 
            if( A.m_pEntry->m_Weight > B.m_pEntry->m_Weight ) return -1;
            return A.m_pEntry->m_Weight < B.m_pEntry->m_Weight;
        } );        
    });

    BlockJobs.FinishJobs();
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // PHASE LOAD ANIMS 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Clean the Skeleton Geom vertices
    //
    BlockJobs.SubmitJob( [ this, &CompilerData ]()
    {
        const_ref( rSkeleton,       CompilerData.m_varSkeleton );
        mutable_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );

        rSkeletonGeom->CleanWeights( 4, 0.001f );
        rSkeletonGeom->ApplyNewSkeleton( rSkeleton );
        rSkeletonGeom.ReleaseAndChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
    });    

    //
    // Collect the bones
    //
    xptr2<xvector3> lLocalTranslation;

    BlockJobs.SubmitJob( [this, &CompilerData, bKeepBind, &lLocalTranslation ]()
    {
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        lLocalTranslation.New( rSkeleton->m_Bone.getCount() );
        
        rawanim NewSkeleton;
        NewSkeleton = rSkeleton;
        NewSkeleton.BakeBindingIntoFrames( TRUE, TRUE, FALSE );

        mutable_ref( rlLocalTranslation, lLocalTranslation );
        for( auto& RawBone : NewSkeleton.m_Bone )
        {
            const s32 Index     = s32( &RawBone - &NewSkeleton.m_Bone[0] );
            auto&     LocalT    = rlLocalTranslation[ Index ];
            
            LocalT = RawBone.m_BindMatrix.getTranslation();

            //AnimBone.m_iBone            = Index;
            //AnimBone.m_iParent          = RawBone.m_iParent;
            //AnimBone.m_nChildren        = RawBone.m_nChildren;
            //AnimBone.m_BindMatrixInv    = RawBone.m_BindMatrixInv;
            //AnimBone.m_BindTranslation  = RawBone.m_BindMatrix.getTranslation(); //NewSkeleton.m_KeyFrame[Index].m_Translation;
                        
            //x_strncpy( &AnimBone.m_Name[0], &RawBone.m_Name[0], sizeof(AnimBone.m_Name)-1, sizeof(AnimBone.m_Name) );
        }
    } );

    //
    // Load all the raw animations
    //
    {
        mutable_ref( rRawAnim, CompilerData.m_lRawAnim );
        for( auto& RawAnim : rRawAnim  )
        {
            // Sadly the FBX is not a multi threading API
            LoadAnim( RawAnim.m_RawAnim, RawAnim.m_pEntry->m_FilePath );
            BlockJobs.SubmitJob( [this, &RawAnim, &CompilerData, bKeepBind, iPlatform, &SkeletonFileName ]()
            {
                // Remove unwanted frames
                RawAnim.m_RawAnim.RemoveFramesFromRage( RawAnim.m_pEntry->m_StartAnimationFrame, RawAnim.m_RawAnim.m_nFrames );

                // Apply the new skeleton to the animation
                const_ref( rSkeleton, CompilerData.m_varSkeleton );
    
    if(1)
    {
        rawanim NewSkeleton;
        NewSkeleton = rSkeleton;
        NewSkeleton.BakeBindingIntoFrames( FALSE, TRUE, FALSE );
        RawAnim.m_RawAnim.ApplyNewSkeleton( NewSkeleton );//rSkeleton );
    }
    else
    {
        RawAnim.m_RawAnim.ApplyNewSkeleton( rSkeleton );
    }

                const char* pPlatformName = "ALLPLATFORMS";
                if( iPlatform ) pPlatformName = x_PlatformString( iPlatform );

                // same log file
                RawAnim.m_RawAnim.Save( xfs("%s/%s_%s_%s--%s.rawanim", 
                    (const char*)m_LogPath,
                    SkeletonFileName,
                    (const char*)RawAnim.m_pEntry->m_Name,
                    pPlatformName,
                    (const char*)m_RscGuid.GetAlphaString() ) );

                // bake the binding but don't do the translation as it creates too much data
                RawAnim.m_RawAnim.BakeBindingIntoFrames( !bKeepBind, !bKeepBind, FALSE );
                
                //
                // Bake the animation re-centering
                //
                if( RawAnim.m_pEntry->m_XZRecenterAnim )
                {
                    RawAnim.m_RawAnim.RencenterAnim( 
                        TRUE,
                        FALSE,
                        TRUE,
                        FALSE,
                        TRUE,
                        FALSE ); 
                }
                else if( RawAnim.m_pEntry->m_bAccRecenterAnim )
                {
                    RawAnim.m_RawAnim.RencenterAnim( 
                        RawAnim.m_pEntry->m_bAccumTranslationX,
                        RawAnim.m_pEntry->m_bAccumTranslationY,
                        RawAnim.m_pEntry->m_bAccumTranslationZ,
                        RawAnim.m_pEntry->m_bAccumRotationPitch,
                        RawAnim.m_pEntry->m_bAccumRotationYaw,
                        RawAnim.m_pEntry->m_bAccumRotationRoll ); 
                }

                if( RawAnim.m_pEntry->m_LoopType == animation_compiler_key::entry_params::LOOP_TYPE_LOOP &&
                    RawAnim.m_pEntry->m_bCleanLoop )
                {
                    RawAnim.m_RawAnim.CleanLoopingAnim();
                    RawAnim.m_RawAnim.RencenterAnim( 
                        TRUE,
                        FALSE,
                        TRUE,
                        FALSE,
                        TRUE,
                        FALSE ); 
                }


                if( 0&&RawAnim.m_pEntry->m_LoopType == animation_compiler_key::entry_params::LOOP_TYPE_LOOP
                    && RawAnim.m_pEntry->m_bCopyFirstFrameAtEnd )
                {
                    auto& RA = RawAnim.m_RawAnim;
                    xptr<rawanim::key_frame> Frames;
                    RA.CopyFrames( Frames, 0, 1 );

                    auto& LF  = RA.m_KeyFrame[ RA.m_KeyFrame.getCount() - RA.m_Bone.getCount() * 1 ];
                    auto& LLF = RA.m_KeyFrame[ RA.m_KeyFrame.getCount() - RA.m_Bone.getCount() * 2 ];

                    // Keep the angular momentum constant
                    Frames[0].m_Rotation = LF.m_Rotation.getDeltaRotation( LLF.m_Rotation ) * LF.m_Rotation; 
                    
                    // Keep the linear velocity constant
                    Frames[0].m_Translation = 2*(LF.m_Translation) - LLF.m_Translation;
                    
                    // Add the new frame
                    RawAnim.m_RawAnim.InsertFrames( RawAnim.m_RawAnim.m_nFrames, Frames );
                }
            });
        }
    }

    BlockJobs.FinishJobs();
    CompilerData.m_lRawAnim.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );
//    CompilerData.m_lAnimBone.ChangeBehavior( xptr2_flags::FLAGS_QT_READABLE | xptr2_flags::FLAGS_READ_ONLY );

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // BUILD BBOXES 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //
    // Build the Hash entries
    //
    BlockJobs.SubmitJob( [&CompilerData ]()
    {
        CompilerData.m_lAnimHash.New( CompilerData.m_lRawAnim.getCount() );
        mutable_ref( rAnimHash, CompilerData.m_lAnimHash );

        const_ref( rRawAnim, CompilerData.m_lRawAnim );
        for( auto& RawAnim : rRawAnim )
        {
            xstring NameLower;
            NameLower.Copy( RawAnim.m_pEntry->m_Name );
            NameLower.MakeLower();

            auto& AnimHash   = rAnimHash[ rRawAnim.getIterator() ]; 
            AnimHash.m_Hash          = eng_anim_group_rsc::ianimhash(x_strHash( NameLower ));
            AnimHash.m_iAnim.m_Value = rRawAnim.getIterator();
            AnimHash.m_iBone.m_Value = -1;
            
            // Search for hash collisions
            for( s32 i = rRawAnim.getIterator() -1; i != -1; --i )
            {
                auto& OtherHash = rAnimHash[i];
                if( (OtherHash.m_Hash.m_Value & 0xffffff00) == (AnimHash.m_Hash.m_Value & 0xffffff00) )
                {
                    // Make sure that every hash has a unique id
                    // for hashes that are the same we will order base on the current rawanim order
                    // this will mean that the user can only access the firt Anim for a given hash.
                    // However this is ok since picking such animations become procedural at that point 
                    // this is where the weights come in
                    AnimHash.m_Hash.m_Value = OtherHash.m_Hash.m_Value + 1;

                    // lets make sure that we wont get collisions
                    auto& OtherRawAnim = rRawAnim[ OtherHash.m_iAnim.m_Value ];
                    if( x_stricmp( OtherRawAnim.m_pEntry->m_Name, RawAnim.m_pEntry->m_Name ) )
                    {
                        x_throw("ERROR: You have been selected by the universe as a looser, 24 bit hash collision. Try to rename anims with [%s] or [%s]",
                            (const char*)OtherRawAnim.m_pEntry->m_Name,
                            (const char*) RawAnim.m_pEntry->m_Name );
                    }
                }
            }                       
        }

        // Make sure that they are shorted
        x_qsort( 
            &rAnimHash[0], 
            rAnimHash.getCount(), 
            sizeof(eng_anim_group_rsc::hash_entry),
            eng_anim_group_rsc::hash_entry::CompareFunction );
         
        // Set it in the anim group
        mutable_ref( rAnimGroup, CompilerData.m_varAnimGroupRSC );
        rAnimGroup->m_pHashTable.m_Ptr = &rAnimHash[0];

        // Done with the anim hash
        rAnimHash.ReleaseAndQTReadOnly();
    });

    //
    // Compute the bounding box for the anim group
    // set the TotalNumber of Bones too
    //
    BlockJobs.SubmitJob( [&CompilerData ]()
    {
        x_inline_light_jobs_block<8>    BlockJobs;
        xptr2<xbbox>                    lBBox;

        lBBox.New( CompilerData.m_lRawAnim.getCount() );

        mutable_ref( rBBox, lBBox );
        const_ref( rRawAnim, CompilerData.m_lRawAnim );
        for( auto& RawAnim : rRawAnim )
        {
            xbbox& BBox = rBBox[ rRawAnim.getIterator() ];

            BlockJobs.SubmitJob( [&CompilerData, &RawAnim, &BBox ]()
            {
                // Allocate matrices
                xptr<xmatrix4>   Matrix;
                Matrix.New( RawAnim.m_RawAnim.m_Bone.getCount() );

                // First compute the bbox base on the bind pose
                BBox.Identity();
                const_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );
                for( auto& Vert : rSkeletonGeom->m_Vertex )
                {
                    BBox += Vert.m_Position;
                }

                // resize BBox based on animation pouses 
                for( s32 iFrame=0; iFrame < RawAnim.m_RawAnim.m_nFrames; iFrame++ )
                {
                    //
                    // Compute the matrices for the frame
                    //
                    RawAnim.m_RawAnim.ComputeBonesL2W( 
                        &Matrix[0], 
                        iFrame,
                        FALSE, //RawAnim.m_pEntry->m_bAccumHorizMotion,
                        FALSE, //RawAnim.m_pEntry->m_bAccumVertMotion,
                        FALSE );//RawAnim.m_pEntry->m_bAccumYawMotion     );

                    //
                    // Go through all the vertices and transform + skin them to find their new location
                    //
                    for( auto& Vert : rSkeletonGeom->m_Vertex )
                    {
                        // Sum up total of weights to compute local pos?
                        xvector3 NewPose(0);
                        for( s32 w = 0 ; w < Vert.m_nWeights; w++ ) 
                        {
                            auto& Weight = Vert.m_Weight[w];
                            NewPose += (Matrix[ Weight.m_iBone ] * Vert.m_Position) * Weight.m_Weight;
                        }

                        BBox +=  NewPose ;
                    }
                }
            });
        }

        // Wait untill everything is done
        BlockJobs.FinishJobs();

        //
        // Compute the BBOX for the anim group
        //
        mutable_ref( rAnimGroupRSC, CompilerData.m_varAnimGroupRSC );
        rAnimGroupRSC->m_BBox.Identity();
        rAnimGroupRSC->m_BBox.Inflate( xvector3d(0.1f) );

        for( auto& BBox : rBBox )
            rAnimGroupRSC->m_BBox += BBox;

        // Set the number of bones
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        rAnimGroupRSC->m_nTotalBones =  rSkeleton->m_Bone.getCount();
        rAnimGroupRSC->m_nTotalAnims = CompilerData.m_lRawAnim.getCount();
        rRawAnim.ReleaseAndQTReadOnly();
    });

    //
    // Compute the MAX distance of vertices (including children) to each bone 
    //
    xptr2<f32>  lMaxDistanceToBone;

    BlockJobs.SubmitJob( [&CompilerData, &lMaxDistanceToBone ]()
    {
        const_ref( rSkeletonGeom, CompilerData.m_varSkeletonGeom );
        const_ref( rSkeleton, CompilerData.m_varSkeleton );
        lMaxDistanceToBone.Alloc( rSkeleton->m_Bone.getCount() );

        const rawgeom& SkeletonGeom = rSkeletonGeom;
         
        // Loop through bones from child to parent
        mutable_ref( rMaxDistance, lMaxDistanceToBone );
        for( s32 i = lMaxDistanceToBone.getCount() - 1; i >= 0 ; --i )
        {
            auto& MaxDistance = rMaxDistance[i];
            MaxDistance =  0;

            // Initialize distance to distance of verts from bone
            for( s32 j=0; j<SkeletonGeom.m_Vertex.getCount(); j++ )
                for( s32 k=0; k<SkeletonGeom.m_Vertex[j].m_nWeights; k++ )
                    if( SkeletonGeom.m_Vertex[j].m_Weight[k].m_iBone == i )
                    {
                        f32 BoneToVertDist = (SkeletonGeom.m_Vertex[j].m_Position - SkeletonGeom.m_Bone[i].m_Position).GetLength();
                        MaxDistance = x_Max( MaxDistance, BoneToVertDist );
                    }

            // Loop through children and see if their distance is greater
            for( s32 j = i+1; j<lMaxDistanceToBone.getCount(); j++ )
                if( SkeletonGeom.m_Bone[j].m_iParent == i )
                {
                    MaxDistance = x_Max( MaxDistance,  rMaxDistance[j] + SkeletonGeom.m_Bone[i].m_Position.GetLength() );
                }

            // Move to next bone higher in hierarchy
            i--;
        }

        for( s32 i=0; i<lMaxDistanceToBone.getCount(); i++ )
            x_DebugMsg("Max Distance Bone:%3d[%s] %f\n",
            i, 
            (const char*)rSkeleton->m_Bone[i].m_Name, 
            rMaxDistance[i] );

        rMaxDistance.ReleaseAndQTReadOnly();
    } );

    BlockJobs.FinishJobs();

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // COMPUTE ANIM INFO
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Compute AnimInfo
    // Also set the nEvents and nProps in the anim_group
    // Creates and compresses all the blocks (a block is a set # frames in the anim)
    // Creates the bone flags and sets them
    //
    {
        x_qt_counter nEvents;
        x_qt_counter nProps;

        const s32 nAnims = CompilerData.m_lRawAnim.getCount();
        CompilerData.m_lAnimInfo.New( nAnims );
        
        mutable_ref( rlAnimInfo, CompilerData.m_lAnimInfo );
        rlAnimInfo->setMemory(0);

        const_ref( rlRawAnim, CompilerData.m_lRawAnim )
        for( auto& RawAnim : rlRawAnim )
        {
            auto&       AnimInfo = rlAnimInfo[ rlRawAnim.getIterator() ];
            const s32   Index    = rlRawAnim.getIterator();

            BlockJobs.SubmitJob( [&CompilerData, &AnimInfo, Index, nAnims, &RawAnim ]()
            {
                const_ref( rlRawAnim, CompilerData.m_lRawAnim );

                 // Setup properties
                x_strcpy( AnimInfo.m_Name, sizeof(AnimInfo.m_Name), RawAnim.m_pEntry->m_Name ) ;

                //AnimInfo.m_nEvents      = SrcRawAnim.m_nEvents + SrcRawAnim.m_nSuperEvents;
                //AnimInfo.m_iEvent       = nEvents;
                //nEvents            += Anim.m_nEvents;
                //AnimInfo.m_nProps       = SrcRawAnim.m_nProps;
                //AnimInfo.m_iProp        = nProps;
                //nProps             += Anim.m_nProps;


                AnimInfo.m_FPS          = RawAnim.m_RawAnim.m_FPS;
//                AnimInfo.m_nFrames      = RawAnim.m_RawAnim.m_nFrames;
                AnimInfo.m_Weight       = RawAnim.m_pEntry->m_Weight;
                AnimInfo.m_AnimsWeight  = AnimInfo.m_Weight;
                AnimInfo.m_nSameAnims   = 1;
                for( s32 i = Index + 1; i<nAnims; i++ )
                {
                    auto& OtherRawAnim = rlRawAnim[i];
                    if( x_stricmp (RawAnim.m_pEntry->m_Name, OtherRawAnim.m_pEntry->m_Name ) == 0 )
                    {
                        AnimInfo.m_nSameAnims++;    
                        AnimInfo.m_AnimsWeight  += OtherRawAnim.m_pEntry->m_Weight;
                    }
                    else break;
                }

                AnimInfo.m_Flags  = 0;
                if( RawAnim.m_pEntry->m_LoopType == animation_compiler_key::entry_params::LOOP_TYPE_LOOP ) 
                    AnimInfo.m_Flags |= eng_anim_group_rsc::anim_info::FLAGS_LOOP;
                else if( RawAnim.m_pEntry->m_LoopType == animation_compiler_key::entry_params::LOOP_TYPE_PING_PONG )  
                    AnimInfo.m_Flags |= eng_anim_group_rsc::anim_info::FLAGS_PING_PONG;

                //AnimInfo.m_Flags |= SrcRawAnim.IsMaskedAnim())           ? (ANIM_DATA_FLAG_HAS_MASKS) : (0);

                if( RawAnim.m_pEntry->m_LoopType == animation_compiler_key::entry_params::LOOP_TYPE_LOOP )
                    AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bLoopInterpolation ? eng_anim_group_rsc::anim_info::FLAGS_LOOP_INTERPOLATION : 0;

                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumTranslationX  ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_X  : 0;
                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumTranslationY  ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Y  : 0;
                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumTranslationZ  ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_TRANSLATION_Z  : 0;

                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumRotationPitch ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_PITCH : 0;
                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumRotationYaw   ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_YAW   : 0;
                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bAccumRotationRoll  ? eng_anim_group_rsc::anim_info::FLAGS_ACCUM_ROTATION_ROLL  : 0;

                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bGravity            ? eng_anim_group_rsc::anim_info::FLAGS_GRAVITY              : 0;
                AnimInfo.m_Flags |= RawAnim.m_pEntry->m_bWorldCollision     ? eng_anim_group_rsc::anim_info::FLAGS_WORLD_COLLISION      : 0;
 
                if( RawAnim.m_pEntry->m_FPS < 1 || RawAnim.m_pEntry->m_FPS > 120 )
                    x_throw( "ERROR: Animation[%s] with name [%s] has an FPS [%d] That is not acceptable",
                                (const char*) RawAnim.m_pEntry->m_FilePath,
                                (const char*) RawAnim.m_pEntry->m_Name,
                                RawAnim.m_pEntry->m_FPS );
        
                // Setup computed info about animation
                {
                    const s32   nBones      = RawAnim.m_RawAnim.m_Bone.getCount(); 
                    auto&       KeyFrame0   = RawAnim.m_RawAnim.m_KeyFrame[ 0 * nBones ];
                    auto&       KeyFrame1   = RawAnim.m_RawAnim.m_KeyFrame[ (RawAnim.m_RawAnim.m_nFrames-1) * nBones ];
                    xradian     Radian;

                    AnimInfo.m_TotalTranslation = KeyFrame1.m_Translation - KeyFrame0.m_Translation;

                    Radian = X_RADIAN( RawAnim.m_pEntry->m_HandleAngle );
                    while( Radian > PI2 ) Radian -= PI2;
                    while( Radian < 0   ) Radian += PI2;
                    AnimInfo.m_HandleAngle = (u16)( Radian * 0xffff / PI2 );

                    Radian = x_MinAngleDiff( KeyFrame1.m_Rotation.getRotation().m_Yaw, 
                                             KeyFrame0.m_Rotation.getRotation().m_Yaw );
                    while( Radian > PI2 ) Radian -= PI2;
                    while( Radian < 0   ) Radian += PI2;
                    AnimInfo.m_TotalYaw = (u16)( Radian * 0xffff / PI2 );

                    Radian = AnimInfo.m_TotalTranslation.GetYaw();
                    while( Radian > PI2 ) Radian -= PI2;
                    while( Radian < 0   ) Radian += PI2;
                    AnimInfo.m_TotalMoveDir = (u16)( Radian * 0xffff / PI2 );
                }
            });
        }

        //
        // Set the total Number of props and events into anim group
        //
        BlockJobs.FinishJobs();

        mutable_ref( rAnimGroupRSC, CompilerData.m_varAnimGroupRSC );
        rAnimGroupRSC->m_nTotalEvents = nEvents.get();
        rAnimGroupRSC->m_nTotalProps  = nProps.get();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // ALLOCATE KEYBLOCKS AND SET THE ANIMKEYS
    //////////////////////////////////////////////////////////////////////////////////////////////////
    {
        //
        // * Determine the number of blocks for each animation
        // * AnimGroup compute total frames and keys
        // * AnimGroup Total Key Blocks
        // * Allocate the Keyblocks
        //
        xptr<s32> lAnimBlockStartingIndex;
        lAnimBlockStartingIndex.New( CompilerData.m_lRawAnim.getCount() + 1 );
        {
            s32 CurrentIndex = 0;

            mutable_ref( rAnimGroup, CompilerData.m_varAnimGroupRSC );
            eng_anim_group_rsc::anim_group_rsc& AnimGroup = rAnimGroup;

            const_ref( rlRawAnim, CompilerData.m_lRawAnim );
            for( auto& RawAnim : rlRawAnim )
            {
                lAnimBlockStartingIndex[ rlRawAnim.getIterator() ] = CurrentIndex;
                for( s32 i=0; i<RawAnim.m_RawAnim.m_nFrames-1; )
                {
                    CurrentIndex++;
                    i += eng_anim_group_rsc::anim_keys::MAX_FRAMES_PER_BLOCK;
                }

                // Update anim group
                ASSERT( AnimGroup.m_nTotalBones == RawAnim.m_RawAnim.m_Bone.getCount() );
                AnimGroup.m_nTotalFrames        += RawAnim.m_RawAnim.m_nFrames;
                AnimGroup.m_nTotalKeys          += ( RawAnim.m_RawAnim.m_Bone.getCount() + 
                                                        RawAnim.m_RawAnim.m_Prop.getCount() ) * RawAnim.m_RawAnim.m_nFrames;
            }
            lAnimBlockStartingIndex[ rlRawAnim.getIterator() ] = CurrentIndex;

            //
            // Set the anim group that key block count
            //
            AnimGroup.m_nTotalKeyBlocks = CurrentIndex;

            //
            // Allocate the keyblocks
            //
            {
                mutable_ref( rAnimKeyBlock, CompilerData.m_lAnimKeyBlock );
                CompilerData.m_lAnimKeyBlock.Alloc( AnimGroup.m_nTotalKeyBlocks );
                rAnimKeyBlock->setMemory(0);
            }
        }

        //
        // Compute the the Anim Keys
        //
        {
            CompilerData.m_lRawAnim.ChangeBehavior(0);
            mutable_ref( rlRawAnim, CompilerData.m_lRawAnim );
        
            for( auto& RawAnim : rlRawAnim )  
            {
                const s32 Index                     = rlRawAnim.getIterator();
                const s32& AnimBlockStartingIndex   = lAnimBlockStartingIndex[ Index ];
                const s32  AnimBlockCount           = lAnimBlockStartingIndex[ Index + 1] - AnimBlockStartingIndex;
             
                mutable_ref( rlAnimInfo, CompilerData.m_lAnimInfo );
                const_ref( rAnimGroup, CompilerData.m_varAnimGroupRSC );
                const eng_anim_group_rsc::anim_group_rsc& AnimGroup = rAnimGroup;

                auto&       AnimInfo        = rlAnimInfo[ Index ];
                auto&       AnimKeys        = AnimInfo.m_AnimKeys;

                AnimKeys.m_nBones           = AnimGroup.m_nTotalBones;
                AnimKeys.m_nProps           = AnimInfo.m_nProps;
                AnimKeys.m_nFrames          = RawAnim.m_RawAnim.m_nFrames;
                AnimKeys.m_igKeyBlockBase   = eng_anim_group_rsc::igblock( AnimBlockStartingIndex );
                AnimKeys.m_nKeyBlocks       = AnimBlockCount;
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // COMPRESS ALL THE KEY FRAMES
    //////////////////////////////////////////////////////////////////////////////////////////////////

    {
//        const s32   nAnims = CompilerData.m_lRawAnim.getCount();
        
        //
        // Start looping thought all the animations
        //
        CompilerData.m_lRawAnim.ChangeBehavior(0);
        mutable_ref( rlRawAnim, CompilerData.m_lRawAnim );
        
        // Going to give this guy dangerous powers.
        CompilerData.m_lAnimInfo.ChangeBehavior( xptr2_flags::FLAGS_QT_MUTABLE );
        CompilerData.m_lAnimKeyBlock.ChangeBehavior( xptr2_flags::FLAGS_QT_MUTABLE );

        for( auto& RawAnim : rlRawAnim )  
        {
            const s32 Index = rlRawAnim.getIterator();
             
            BlockJobs.SubmitJob( [&CompilerData, &lMaxDistanceToBone, Index, &RawAnim, &lLocalTranslation ]()
            {
                mutable_ref( rlAnimInfo, CompilerData.m_lAnimInfo );
 
                auto&                               AnimInfo            = rlAnimInfo[ Index ];
                auto&                               AnimKeys            = AnimInfo.m_AnimKeys;
//                auto*                               pRawKey             = &RawAnim.m_RawAnim.m_KeyFrame[0];
                //auto*                             pPropKey            = &RawAnim.m_RawAnim.m_PropFrame[0];

               //
                // Compute size of rawkey buffer and realloc if we need to
                //
                xptr<xtransform>  lTempKey;
                {
                    s32 nTempKeys = (AnimKeys.m_nBones + AnimKeys.m_nProps) * AnimKeys.m_nFrames;
                    lTempKey.New( nTempKeys );
                }
                
                //
                // Create an flag array for all the bones and props that need flags
                //
                for( s32 i=0; i<RawAnim.m_RawAnim.m_Bone.getCount(); i++ )
                    if( RawAnim.m_RawAnim.m_Bone[i].m_bIsMasked )
                    {
                        if( RawAnim.m_lBoneFlags.getCount() == 0 )
                        {
                            s32 nStreamFlags = (AnimKeys.m_nBones + AnimKeys.m_nProps);
                            RawAnim.m_lBoneFlags.Alloc( nStreamFlags );

                            {
                                mutable_ref( rBoneFlags, RawAnim.m_lBoneFlags );
                                rBoneFlags->setMemory(0);

                                // Set the anim info with the memory
                                AnimInfo.m_pBoneFlags.m_Ptr = &rBoneFlags[0];
                            }

                            // It is a bit of cheating since we have gone around it but at least we make sure
                            // not one else is trying anything funny
                             RawAnim.m_lBoneFlags.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY );
                        }

                        AnimInfo.m_pBoneFlags.m_Ptr[i] |= eng_anim_group_rsc::anim_info::BONE_FLAG_MASKED;
                    }

                //
                // I - is a running index in the raw key data
                //
                s32 I=0;

                //
                // Copy Bone keys into raw buffer
                //
                for( s32 i=0; i<AnimKeys.m_nBones; i++ )
                {
                    // If bone is masked then nuke values
                    if( AnimInfo.m_pBoneFlags.m_Ptr && AnimInfo.m_pBoneFlags.m_Ptr[i] & eng_anim_group_rsc::anim_info::BONE_FLAG_MASKED )
                    {
                        for( s32 f=0; f<AnimKeys.m_nFrames; f++ )
                        {
                            auto& TempKey = lTempKey[I++];
                         
                            TempKey.m_Scale.Set(1);
                            TempKey.m_Rotation.Identity();
                            TempKey.m_Translation.Zero();
                        }
                    }
                    else
                    {
                        for( s32 f=0; f<AnimKeys.m_nFrames; f++ )
                        {
                            auto& TempKey = lTempKey[I++];
                            auto& RawKey  = RawAnim.m_RawAnim.m_KeyFrame[ f * AnimKeys.m_nBones + i ];
                         
                            TempKey.m_Scale       = RawKey.m_Scale;
                            TempKey.m_Rotation    = RawKey.m_Rotation;
                            TempKey.m_Translation = RawKey.m_Translation;
                        }
                    }
                }

                //
                // Convert props to always be realtive to its parent
                // Copy Prop keys into bottom of raw buffer
                //
                /*
                mutable_ref( rAnimProp, CompilerData.m_lAnimProp );
                for( s32 i=0; i<AnimKeys.m_nProps; i++ )
                {
                    // Gather all the keys for this prop
                    for( s32 f=0; f<AnimKeys.m_nFrames; f++ )
                    {
                        xmatrix4    PropMatrix;
                        auto&       PropKeyFrame = RawAnim.m_RawAnim.m_PropFrame[ f * AnimKeys.m_nProps + i ];
                    
                        PropMatrix.setup( PropKeyFrame.m_Scale, PropKeyFrame.m_Rotation, PropKeyFrame.m_Translation );

                        // Make relative to parent if attached to one
                        s32 iParent = RawAnim.m_RawAnim.m_Prop[ i ].m_iParentBone;
                        if( iParent != -1 )
                        {
                            xmatrix4 ParentInvM;
                        
                            RawAnim.m_RawAnim.ComputeRawBoneL2W( 
                                rAnimProp[ AnimInfo.m_iProp + i ].m_iParentBone, 
                                ParentInvM, 
                                f );

                            ParentInvM.FullInvert();
                            PropMatrix = ParentInvM * PropMatrix;
                        }

                        // Copy info
                        auto& TempKey = lTempKey[I++];
                        TempKey.m_Scale       = PropMatrix.getScale();
                        TempKey.m_Rotation.Setup( PropMatrix );
                        TempKey.m_Translation = PropMatrix.getTranslation();
                    }
                }
                */

                //
                // Force all quaternions to have positive W
                // TODO: This probably should be remove from here since it is a compression detail
                if( 1 )
                {
                    s32 Count = ( AnimKeys.m_nBones + AnimKeys.m_nProps ) * AnimKeys.m_nFrames;
                    for( s32 i=0; i<Count; i++ )
                    {
                        auto& Key = lTempKey[i];

                        if( Key.m_Rotation.m_W < 0 )
                            Key.m_Rotation = -Key.m_Rotation;
                    }
                }

                //
                // The entire raw key buffer is setup.
                // Now loop through each key_block to be built
                //
                s32                 StatsCompressedSize  = 0;
                s32                 StatsDecompressSized = 0;

                RawAnim.m_lBitStream.New( AnimKeys.m_nKeyBlocks );

                const_ref       ( rMaxDistance,     lMaxDistanceToBone );
                mutable_ref     ( rBitStream,       RawAnim.m_lBitStream );
                mutable_ref     ( rAnimKeyBlock,    CompilerData.m_lAnimKeyBlock );
                const_ref       ( rlLocalTranslation,        lLocalTranslation );

                for( s32 i=0; i<AnimKeys.m_nKeyBlocks; i++ )
                {
                    // Get access to keyblock structure
                    auto& KeyBlock  = rAnimKeyBlock[ AnimKeys.m_igKeyBlockBase.m_Value + i ];
                    auto& Bitstream = rBitStream[i];

                    // Compute starting and ending frames for this block
                    s32 iStartKeyFrames = i * eng_anim_group_rsc::anim_keys::MAX_FRAMES_PER_BLOCK;
                    s32 iEndKeyFrames   = x_Min( iStartKeyFrames + eng_anim_group_rsc::anim_keys::MAX_FRAMES_PER_BLOCK, AnimKeys.m_nFrames-1 );

                    KeyBlock.m_nFrames  = (iEndKeyFrames-iStartKeyFrames)+1;
                
                    anim_compress   AnimCompress;

                    // Setup the processing flags for runtime
                    x_Construct( (xsemaphore_lock*)&KeyBlock.m_Processing );

                    // Call the compression routine
                    KeyBlock.m_igBlock.m_Value           = i;
                    KeyBlock.m_nStreams                  = AnimKeys.m_nBones + AnimKeys.m_nProps;
                    KeyBlock.m_KeyCount                  = KeyBlock.m_nStreams * AnimKeys.m_nFrames;
                    AnimCompress.CompressAnimationData(  
                                        Bitstream,
                                        &lTempKey[0],
                                        &rlLocalTranslation[0],
                                        lLocalTranslation.getCount(),
                                        AnimKeys.m_nFrames,
                                        KeyBlock.m_nStreams,
                                        iStartKeyFrames, 
                                        iEndKeyFrames,
                                        &rMaxDistance[0] );

                    // Keep a reference to the allocated memory
                    xptr<xbyte> Result;
                    s32         Length;
                    Bitstream.getResult(Result, Length );
                    KeyBlock.m_pCompressStream.m_Ptr = &Result[0];
                    KeyBlock.m_CompressedDataSize    = Length;

                    //
                    // Debug
                    //
                    if(0)
                    {
                        {
                            xfile F;
                            F.Open(xfs("%d",Index), "wb" );
                            F.WriteRaw( &Result[0], 1, Length );
                        }

                        if( Index == 0 ) X_BREAK;

                        xfile F;
                        F.Open(xfs("%d",Index), "rb" );
                        xptr<xbyte> RRR;
                        RRR.Alloc( Length );
                        F.ReadRaw( &RRR[0], 1, Length );

                        xbitstream XX;
                        XX.setMode( FALSE );
                        XX.setPackData( RRR, Length );

                       // ASSERT( 0 == x_memcmp( &RRR[0], &Result[0], Length ) );

                        s32 s,q,t;
                        for( s32 i=0; i<100; i++ )
                        {
                            XX.SerializeIn( s, 0, 10 );
                            XX.SerializeIn( q, 0, 10 );
                            XX.SerializeIn( t, 0, 10 );
                        }
                    }

                    // Keep track of stats
                    StatsCompressedSize  += KeyBlock.m_CompressedDataSize;
                    StatsDecompressSized += KeyBlock.m_KeyCount * sizeof(xtransform); 
                }                                      

                x_printf("INFO: Anim[%s] Compresed To[%3.1f%%] #Blocks:[%d] #Frames[%d] FPS[%d] #Props[%d] #Events[%d]",
                    (const char*)RawAnim.m_pEntry->m_Name,
                    (StatsCompressedSize * 100.f ) / (f32)StatsDecompressSized,
                     AnimKeys.m_nKeyBlocks,
                     AnimInfo.m_AnimKeys.m_nFrames,
                     AnimInfo.m_FPS,
                     AnimInfo.m_nProps,
                     AnimInfo.m_nEvents );
            });

BlockJobs.FinishJobs();
        }
    }

    BlockJobs.FinishJobs();
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // WRAP UP 
    //////////////////////////////////////////////////////////////////////////////////////////////////

    const_ref( rKeyMainPlatform, m_Key.m_Main );
    const_ref( rKeyBlock, CompilerData.m_lAnimKeyBlock );
    const_ref( rAnimInfo, CompilerData.m_lAnimInfo );
    mutable_ref( rAnimGroupRSC, CompilerData.m_varAnimGroupRSC );
    rAnimGroupRSC->m_pAnimInfo.m_Ptr    = const_cast<decltype(rAnimGroupRSC->m_pAnimInfo.m_Ptr)>(&rAnimInfo[0]);
    rAnimGroupRSC->m_pKeyBlock.m_Ptr    = const_cast<decltype(rAnimGroupRSC->m_pKeyBlock.m_Ptr)>(&rKeyBlock[0]);
    rAnimGroupRSC->m_SkeletonGuid       = getGuidFromFilePath( rKeyMainPlatform[ iPlatform ].m_SkeletonPath );
}

//-------------------------------------------------------------------------------------------------

void animation_compiler_base::ExportResource( const eng_anim_group_rsc::anim_group_rsc& GeomRsc ) const
{
    //
    // Go though all the targets
    //
    for( const platform& Target : m_Target )
    {
        if( Target.m_bValid == FALSE )
            continue;

        //
        // Ok Ready to save everything then
        //
        xserialfile     SerialFile;
        xstring         FinalRscPath;
        
        FinalRscPath = getFinalResourceName( Target );
        
        SerialFile.Save( (const char*)FinalRscPath, GeomRsc, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
        
        // for debugging
        if( (1) )
        {
            xserialfile   test;
            eng_anim_group_rsc::anim_group_rsc* pGeom;
            test.Load( (const char*)FinalRscPath, pGeom );
            
           // ASSERT( eng_resource_guid::getType( pGeom->m_pInformed.m_Ptr[0].m_Guid ) == eng_informed_material_rsc::UID );

            ASSERT( pGeom );
            x_delete( pGeom );
        }
    }
}

//-------------------------------------------------------------------------------------------------

void animation_compiler_base::onCompile( void )
{
    xsafe_array<compiler_data, X_PLATFORM_COUNT>    CompileData;

    CompileSpecificPlatform( CompileData[0], xplatform(0) );
    
    const_ref( rAnimGroup, CompileData[0].m_varAnimGroupRSC );
    ExportResource( rAnimGroup );
}

