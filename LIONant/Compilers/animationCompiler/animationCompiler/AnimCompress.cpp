
#include "AnimCompress.h"

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// SUPPORT FUNCTIONS
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

static inline
s32 F32ToS32( f32 F )
{
    ASSERT(F >= (-S32_MAX)) ;
    ASSERT(F <= (+S32_MAX)) ;

    if( F >= 0 ) 
        return (s32)(F+0.5f);
    else
        return -((s32)((-F)+0.5f));
}

//-----------------------------------------------------------------------------------

void anim_compress::DisplayAnimCompressStats( void )
{
    s32 i;
    s32 Sum[3]={0};

    for( i=0; i<8; i++ )
    {
        if( m_ScaleCompSamples[i] == 0 ) m_ScaleCompSamples[i] = 1;
        if( m_RotationCompSamples[i] == 0 ) m_RotationCompSamples[i] = 1;
        if( m_TranslationCompSamples[i] == 0 ) m_TranslationCompSamples[i] = 1;
    }


    x_DebugMsg("------------------------------------------------------------------------\n");
    x_DebugMsg("--                       ANIM COMPRESS STATS                          --\n");
    x_DebugMsg("------------------------------------------------------------------------\n");

    for( i=0; i<6; i++ )
    {
        Sum[0] += m_ScaleCompSize[i];
        x_DebugMsg("%1d Scale       %5d = %8d bytes = %5.2f bytes per key\n", 
            i, 
            m_ScaleCompUses[i], 
            (m_ScaleCompSize[i]+7)/8, 
            ((m_ScaleCompSize[i]+7)/8)/(f32)m_ScaleCompSamples[i]);
    }

    x_DebugMsg("------------------------------------------------------------------------\n");

    for( i=0; i<6; i++ )
    {
        Sum[1] += m_RotationCompSize[i];
        x_DebugMsg("%1d Rotation    %5d = %8d bytes = %5.2f bytes per key\n", 
            i, 
            m_RotationCompUses[i], 
            (m_RotationCompSize[i]+7)/8, 
            ((m_RotationCompSize[i]+7)/8)/(f32)m_RotationCompSamples[i] );
    }

    x_DebugMsg("------------------------------------------------------------------------\n");

    for( i=0; i<6; i++ )
    {
        Sum[2] += m_TranslationCompSize[i];
        x_DebugMsg("%1d Translation %5d = %8d bytes = %5.2f bytes per key\n", 
            i, 
            m_TranslationCompUses[i], 
            (m_TranslationCompSize[i]+7)/8, 
            ((m_TranslationCompSize[i]+7)/8)/(f32)m_TranslationCompSamples[i] );
    }

    x_DebugMsg("------------------------------------------------------------------------\n");
    x_DebugMsg("Scale       bytes = %8d\n",(Sum[0]+7)/8);
    x_DebugMsg("Rotation    bytes = %8d\n",(Sum[1]+7)/8);
    x_DebugMsg("Translation bytes = %8d\n",(Sum[2]+7)/8);
    x_DebugMsg("------------------------------------------------------------------------\n");
}

//-----------------------------------------------------------------------------------

void anim_compress::DeltaCompress( 
    xbitstream& Bitstream, 
    const f32*  pSample, 
    s32         Stride, 
    s32         nSamples, 
    f32         Precision )
{
    s32 i;
    s32 TotalBitsWritten = Bitstream.getPackedLength();

    //
    // Get min and max of deltas
    //
    s32 MinDelta =  S32_MAX;
    s32 MaxDelta = -S32_MAX;
    for( i=1; i<nSamples; i++ )
    {
        s32 S0 = F32ToS32( pSample[Stride*(i-1)] * Precision );
        s32 S1 = F32ToS32( pSample[Stride*(i+0)] * Precision );
        s32 Delta = S1 - S0;

        MinDelta = x_Min(MinDelta,Delta);
        MaxDelta = x_Max(MaxDelta,Delta);
    }

    //
    // Compute number of bits necessary to hold deltas
    //
    s32 nDeltaBits = x_Pow2MinimumRequire( MaxDelta - MinDelta );    

    //
    // Pack initial sample
    //
    {
        // Convert first sample to integer
        const s32 S = F32ToS32( pSample[0] * Precision );
        Bitstream.SerializeOut( S );
    }

    //
    // Pack min delta
    //
    Bitstream.SerializeOut( MinDelta );

    //
    // Pack number of bits for deltas
    //
    Bitstream.SerializeOut( nDeltaBits, 5 );

    s32 OverheadBits = Bitstream.getPackedLength() - TotalBitsWritten;

    //
    // Pack the deltas
    //
    //x_DebugMsg("[%2d] ",nDeltaBits);
    for( i=1; i<nSamples; i++ )
    {
        s32 S0 = F32ToS32( pSample[Stride*(i-1)] * Precision );
        s32 S1 = F32ToS32( pSample[Stride*(i+0)] * Precision );
        s32 Delta = (S1 - S0) - MinDelta;
        ASSERT( Delta >= 0 );
        Bitstream.Serialize( Delta, nDeltaBits );
        //x_DebugMsg("%2d ",x_Pow2MinimumRequire( Delta ));
        //x_DebugMsg("%4d ",Delta);
    }
    //x_DebugMsg("\n");

    TotalBitsWritten = Bitstream.getPackedLength() - TotalBitsWritten;
    //x_DebugMsg("DeltaCompress: %d samples into %1.2f bytes. Overhead %1.2f\n",nSamples,(TotalBitsWritten+7)/(f32)8,(OverheadBits+7)/(f32)8);
    m_qtTOTAL_BITS.Add( TotalBitsWritten );
    m_qtTOTAL_OVERHEAD.Add( OverheadBits );
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// SCALE
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

void anim_compress::CompressScale( 
    xbitstream&                             Bitstream,
    s32                                     iStream,
    const xtransform*                       pKeys,
    s32                                     StartFrame,
    s32                                     EndFrame,
    s32                                     TotalFrames,
    f32                                     DistToChilden )
{
    (void)DistToChilden;
    s32     i,j;
    s32     nFrames = EndFrame - StartFrame + 1;

    //
    // Compute range of values
    //
    f32 MinValue[3] = {+F32_MAX,+F32_MAX,+F32_MAX};
    f32 MaxValue[3] = {-F32_MAX,-F32_MAX,-F32_MAX};
    for( i=StartFrame; i<=EndFrame; i++ )
    {
        const f32* pF = &pKeys[ iStream*TotalFrames + i ].m_Scale.m_X;
        for( j=0; j<3; j++ )
        {
            ASSERT(pF[j] >= (-S32_MAX)) ;
            ASSERT(pF[j] <= (+S32_MAX)) ;

            MinValue[j] = x_Min(MinValue[j],pF[j]);
            MaxValue[j] = x_Max(MaxValue[j],pF[j]);
        }
    }

    //
    // Check if all scales = 1.0f
    //
    if( 1 )
    {
        f32 E = 0.005f;
        for( i=0; i<3; i++ )
        {
            if( x_Abs(1.0f - MinValue[i]) > E ) break;
            if( x_Abs(1.0f - MaxValue[i]) > E ) break;
        }

        if( i==3 )
        {
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_IDENTITY );
            return;
        }
    }

    //
    // Check if all scales are a single value
    //
    if( 1 )
    {
        f32 E = 0.01f;
        for( i=0; i<3; i++ )
            if( (MaxValue[i] - MinValue[i]) > E ) break;

        if( i==3 )
        {
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_CONST_VALUE );

            Bitstream.SerializeOut( F32ToS32( (MaxValue[0] + MinValue[0]) * 0.5f * (1<<7) ) );
            Bitstream.SerializeOut( F32ToS32( (MaxValue[1] + MinValue[1]) * 0.5f * (1<<7) ) );
            Bitstream.SerializeOut( F32ToS32( (MaxValue[2] + MinValue[2]) * 0.5f * (1<<7) ) );

            return;
        }
    }

    //
    // Use delta compression
    //
    if( 1 )
    {
        const xvector3& Scale                   = pKeys[ iStream*TotalFrames + StartFrame ].m_Scale;
        s32             Stride                  = sizeof(xtransform)/sizeof(f32);

        eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DELTA_LOW );

        DeltaCompress( Bitstream, &Scale.m_X, Stride, nFrames, f32(1<<7) );
        DeltaCompress( Bitstream, &Scale.m_Y, Stride, nFrames, f32(1<<7) );
        DeltaCompress( Bitstream, &Scale.m_Z, Stride, nFrames, f32(1<<7) );

        return;
    }

    //
    // Store as full precision
    //
    if( 1 )
    {
        eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_FULLRANGE );
 
        for( i=StartFrame; i<=EndFrame; i++ )
        {
            const xtransform& Key = pKeys[ iStream*TotalFrames + i ];
            Bitstream.SerializeOut( Key.m_Scale.m_X );
            Bitstream.SerializeOut( Key.m_Scale.m_Y );
            Bitstream.SerializeOut( Key.m_Scale.m_Z );
        }

        return; 
    }
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// ROTATION
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

void anim_compress::CompressRotation( 
    xbitstream&                             Bitstream,
    s32                                     iStream,
    const xtransform*                       pKeys,
    s32                                     StartFrame,
    s32                                     EndFrame,
    s32                                     TotalFrames,
    f32                                     DistToChildren )
{
    s32     i,j;
    s32     nFrames = EndFrame - StartFrame + 1;

    //
    // Compute range of values
    //
    f32 MinValue[4] = {+F32_MAX,+F32_MAX,+F32_MAX,+F32_MAX};
    f32 MaxValue[4] = {-F32_MAX,-F32_MAX,-F32_MAX,-F32_MAX};
    for( i=StartFrame; i<=EndFrame; i++ )
    {
        f32* pF = (f32*)&pKeys[ iStream*TotalFrames + i ].m_Rotation;
        for( j=0; j<4; j++ )
        {
            MinValue[j] = x_Min(MinValue[j],pF[j]);
            MaxValue[j] = x_Max(MaxValue[j],pF[j]);
        }
    }

    //
    // Check if all rotations are the identity
    //
    if( 1 )
    {
        f32 QV[4] = {0,0,0,1};
        f32 E = 0.001f;

        for( i=StartFrame; i<=EndFrame; i++ )   
        {
            const xquaternion& KQ = pKeys[ iStream*TotalFrames + i ].m_Rotation;
            if( x_Abs( KQ.m_X - QV[0] ) > E ) break;
            if( x_Abs( KQ.m_Y - QV[1] ) > E ) break;
            if( x_Abs( KQ.m_Z - QV[2] ) > E ) break;
            if( x_Abs( KQ.m_W - QV[3] ) > E ) break;
        }

        if( i > EndFrame )
        {
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_IDENTITY );
            return;
        }
    }

    //
    // Check if all quaternions are a single value
    //
    if( 1 )
    {
        f32 E = 0.001f;
        for( i=0; i<4; i++ )
            if( (MaxValue[i] - MinValue[i]) > E ) 
                break;

        if( i==4 )
        {
            xquaternion Q;
            Q.m_X = ( MaxValue[0] + MinValue[0] ) * 0.5f;
            Q.m_Y = ( MaxValue[1] + MinValue[1] ) * 0.5f;
            Q.m_Z = ( MaxValue[2] + MinValue[2] ) * 0.5f;
            Q.m_W = ( MaxValue[3] + MinValue[3] ) * 0.5f;
            Q.Normalize();

            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_CONST_VALUE );

            Bitstream.SerializeOut( F32ToS32( Q.m_X * f32(1<<10) ), -(1<<10), (1<<10) );
            Bitstream.SerializeOut( F32ToS32( Q.m_Y * f32(1<<10) ), -(1<<10), (1<<10) );
            Bitstream.SerializeOut( F32ToS32( Q.m_Z * f32(1<<10) ), -(1<<10), (1<<10) );
            Bitstream.SerializeOut( F32ToS32( Q.m_W * f32(1<<10) ), -(1<<10), (1<<10) );

            return;
        }
    }


    //
    // Use delta compression
    //
    if( 1 )//&& (DistToChildren < 800) )
    {
        f32 Prec;

        // Decide on Precision
        if( DistToChildren < 300 )
        {
            Prec = f32(1<<10);
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DELTA_LOW );
        }
        else
        if( DistToChildren < 500 )
        {
            Prec = (f32)(1<<14);
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DELTA_MID );
        }
        else
        {
            Prec = (f32)(1<<16);
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DELTA_HIGH );
        }

        const xquaternion&  Q                       = pKeys[ iStream*TotalFrames + StartFrame ].m_Rotation;
        s32                 Stride                  = sizeof(xtransform)/sizeof(f32);

        DeltaCompress( Bitstream, &Q.m_X, Stride, nFrames, Prec );
        DeltaCompress( Bitstream, &Q.m_Y, Stride, nFrames, Prec );
        DeltaCompress( Bitstream, &Q.m_Z, Stride, nFrames, Prec );
        DeltaCompress( Bitstream, &Q.m_W, Stride, nFrames, Prec );

        return;
    }

    //
    // Pack with full precision
    //
    if( 1 )
    {
        eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_FULLRANGE );

        for( i=StartFrame; i<=EndFrame; i++ )
        {
            const xtransform& Key = pKeys[ iStream*TotalFrames + i ];
            Bitstream.SerializeOut( Key.m_Rotation.m_X );
            Bitstream.SerializeOut( Key.m_Rotation.m_Y );
            Bitstream.SerializeOut( Key.m_Rotation.m_Z );
            Bitstream.SerializeOut( Key.m_Rotation.m_W );
        }

        return;
    }
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// TRANSLATION
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

void anim_compress::CompressTranslation( 
    xbitstream&                             Bitstream,
    s32                                     iStream,
    const xtransform*                       pKeys,
    s32                                     StartFrame,
    s32                                     EndFrame,
    s32                                     TotalFrames,
    f32                                     DistToChilden,
    const xvector3*                         pBone,
    s32                                     nBones )
{
    (void)DistToChilden;
    s32     i,j;
    s32     nFrames = EndFrame - StartFrame + 1;

    //
    // Compute range of values
    //
    f32 MinValue[3] = {+F32_MAX,+F32_MAX,+F32_MAX};
    f32 MaxValue[3] = {-F32_MAX,-F32_MAX,-F32_MAX};
    for( i=StartFrame; i<=EndFrame; i++ )
    {
        const f32* pF = &pKeys[ iStream*TotalFrames + i ].m_Translation.m_X;
        for( j=0; j<3; j++ )
        {
            MinValue[j] = x_Min( MinValue[j], pF[j] );
            MaxValue[j] = x_Max( MaxValue[j], pF[j] );
        }
    }

    //
    // Check if all translations are 0
    //
    if( 1 )
    {
        f32 E = 0.01f;
        for( i=0; i<3; i++ )
        {
            if( x_Abs(0.0f - MinValue[i]) > E ) break;
            if( x_Abs(0.0f - MaxValue[i]) > E ) break;
        }

        if( i==3 )
        {
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_IDENTITY );
            return;
        }
    }

    //
    // Check if all translations are equal to the local translation
    //
    if( 1 )
    {
        if( iStream < nBones )
        {
            const f32* pLT = &pBone[iStream].m_X;
            f32 E = 0.1f;
            for( i=0; i<3; i++ )
            {
                if( x_Abs( pLT[i] - MinValue[i]) > E ) break;
                if( x_Abs( pLT[i] - MaxValue[i]) > E ) break;
            }

            // TODO: Add this case...
            if( 0 && i==3 )
            {
                eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DICTIONARY );
                return;
            }
        }
    }

    //
    // Check if all translations are a single value
    //
    if( 1 )
    {
        f32 E = 0.1f;
        for( i=0; i<3; i++ )
            if( (MaxValue[i] - MinValue[i]) > E ) 
                break;

        if( i==3 )
        {
            eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_CONST_VALUE );

            Bitstream.SerializeOut( F32ToS32( (MaxValue[0] + MinValue[0]) * 0.5f * f32(1<<7) ) );
            Bitstream.SerializeOut( F32ToS32( (MaxValue[1] + MinValue[1]) * 0.5f * f32(1<<7) ) );
            Bitstream.SerializeOut( F32ToS32( (MaxValue[2] + MinValue[2]) * 0.5f * f32(1<<7) ) );
            
            return;
        }
    }

    //
    // Use delta compression
    //
    if( 1 )
    {
        const xvector3& Trans                   = pKeys[ iStream*TotalFrames + StartFrame ].m_Translation;
        s32             Stride                  = sizeof(xtransform)/sizeof(f32);

        eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_DELTA_LOW );

        DeltaCompress( Bitstream, &Trans.m_X, Stride, nFrames, f32(1<<5) );
        DeltaCompress( Bitstream, &Trans.m_Y, Stride, nFrames, f32(1<<5) );
        DeltaCompress( Bitstream, &Trans.m_Z, Stride, nFrames, f32(1<<5) );

        return;
    }

    //
    // Store as full precision
    //
    if( 1 )
    {
        eng_anim_group_compressor::SerializeTypeOut( Bitstream, TYPE_FULLRANGE );

        for( i=StartFrame; i<=EndFrame; i++ )
        {
            const xtransform& Key = pKeys[ iStream*TotalFrames + i ];
            Bitstream.SerializeOut( Key.m_Translation.m_X );
            Bitstream.SerializeOut( Key.m_Translation.m_Y );
            Bitstream.SerializeOut( Key.m_Translation.m_Z );
        }

        return;
    }
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
// ANIM_COMPRESS
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

void anim_compress::CompressAnimationData(
    xbitstream&                             BitStream, 
    const xtransform*                       pKeys,
    const xvector3*                         pBone,
    s32                                     nBones,
    s32                                     TotalFrames,
    s32                                     TotalStreams,
    s32                                     StartFrame,
    s32                                     EndFrame,
    const f32*                              pLenOfBonesToVerts )
{
    for( s32 i=0; i<TotalStreams; i++ )
    {
        s32 nFrames         = EndFrame - StartFrame + 1;
        f32 LenToChildren   = (i < nBones) ? (pLenOfBonesToVerts[i]) : (pLenOfBonesToVerts[0]);

        CompressScale       ( BitStream, i, pKeys, StartFrame, EndFrame, TotalFrames, LenToChildren );
        CompressRotation    ( BitStream, i, pKeys, StartFrame, EndFrame, TotalFrames, LenToChildren );
        CompressTranslation ( BitStream, i, pKeys, StartFrame, EndFrame, TotalFrames, LenToChildren, pBone, nBones );
    }
}
