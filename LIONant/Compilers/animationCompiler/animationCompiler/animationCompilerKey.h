//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#ifndef ANIMATION_COMPILER_KEY_OBJECT_H
#define ANIMATION_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"

class animation_compiler_key : public compiler_key_object
{
public:

    struct main_params : public main_params_base_link<main_params>
    {
        enum override_main_flags
        {
            OVERRIDE_MAIN_SKELETON_FILEPATH     =   X_BIT(0),
            OVERRIDE_MAIN_KEEP_BINDING          =   X_BIT(1),
            SINGLE_COMPILATION_FLAGS            =   0
                                                    | OVERRIDE_MAIN_SKELETON_FILEPATH  
        };

        virtual void        onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const  override final;
        virtual xbool       onPropQuery                 ( xproperty_query& Query )  override final;
                void        onUpdateFromSrc             ( const main_params& Src, const u64 Masks );

        xstring     m_SkeletonPath              = X_STR("");        // Path to the source data
        xbool       m_bKeepBind                 = FALSE;            // FALSE to bake the binding pouse inside the frames
    };
    
    struct entry_params : public entry_params_base_link<entry_params>
    {
        enum loop_type
        {
            LOOP_TYPE_NONE,
            LOOP_TYPE_LOOP,
            LOOP_TYPE_PING_PONG
        };

        enum override_entry_flags
        {
            OVERRIDE_ENTRY_FILEPATH                 =   X_BIT(0),
            OVERRIDE_ENTRY_NAME                     =   X_BIT(1),
            OVERRIDE_ENTRY_HANDLE_ANGLE             =   X_BIT(2),
            OVERRIDE_ENTRY_FPS                      =   X_BIT(3),
            OVERRIDE_ENTRY_LOOP_TYPE                =   X_BIT(4),
            OVERRIDE_ENTRY_ACC_TRANSLATION_X        =   X_BIT(5),
            OVERRIDE_ENTRY_ACC_TRANSLATION_Y        =   X_BIT(6),
            OVERRIDE_ENTRY_ACC_TRANSLATION_Z        =   X_BIT(7),
            OVERRIDE_ENTRY_ACC_ROTATION_PITCH       =   X_BIT(8),
            OVERRIDE_ENTRY_ACC_ROTATION_YAW         =   X_BIT(9),
            OVERRIDE_ENTRY_ACC_ROTATION_ROLL        =   X_BIT(10),
            OVERRIDE_ENTRY_GRAVITY                  =   X_BIT(11),
            OVERRIDE_ENTRY_WORLD_COLLISION          =   X_BIT(12),
            OVERRIDE_ENTRY_WEIGHT                   =   X_BIT(13),
            OVERRIDE_ENTRY_RESAMPLE_FPS             =   X_BIT(14),
            OVERRIDE_ENTRY_START_ANIMATION_FRAME    =   X_BIT(15),
            OVERRIDE_ENTRY_LOOP_INTERPOLATION       =   X_BIT(16),
            OVERRIDE_ENTRY_RECENTER_ACCANIM         =   X_BIT(17),

            SINGLE_COMPILATION_FLAGS            =   0
                                                    | OVERRIDE_ENTRY_FILEPATH  
                                                    | OVERRIDE_ENTRY_NAME
                                                    | OVERRIDE_ENTRY_RESAMPLE_FPS
        };

        virtual void        onPropEnum                  ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool       onPropQuery                 ( xproperty_query& Query ) override final;
                void        onUpdateFromSrc             ( const entry_params& Src, const u64 Masks );

        static xprop_enum::entry s_LoopTypeEnumList[];

        xstring     m_FilePath              = X_STR("");        // Path to the source data
        xstring     m_Name                  = X_STR("");        // Game name of anim
        xdegree     m_HandleAngle           = 0;                // Yaw handle angle
        s32         m_FPS                   = 60;               // Speed of playback in frames per sec
        s32         m_ResampleFPS           = 60;               // Resample the animation to the specify fps
        loop_type   m_LoopType              = LOOP_TYPE_NONE;   // No Looping is the default
        xbool       m_bLoopInterpolation    = TRUE;             // Allows looping interpolation
        xbool       m_bAccumTranslationX    = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bAccumTranslationY    = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bAccumTranslationZ    = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bAccumRotationPitch   = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bAccumRotationYaw     = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bAccumRotationRoll    = FALSE;            // FALSE if XZ motion updates position
        xbool       m_bGravity              = TRUE;             // TRUE if gravity should be used on anim
        xbool       m_bWorldCollision       = TRUE;             // TRUE if world collision should be used with anim
        xbool       m_bCopyFirstFrameAtEnd  = TRUE;             // For looping if the artist dont do a good job this will help
        xbool       m_bAccRecenterAnim      = TRUE;             // For re centering of ACC translational animations
        xbool       m_XZRecenterAnim        = TRUE;             // For re centering any non acc animations
        xbool       m_bCleanLoop            = TRUE;             // If we should try to clean the loop to be smoother
        f32         m_Weight                = 1.0f;             // Weight of animation (compared for anims with same name)
        s32         m_StartAnimationFrame   = 0;                // Which frame the animation will start
    };

public:
        
    animation_compiler_key( compiler_base& Base ) : compiler_key_object( Base ) {}

protected:
    
    virtual const char*         getCompilerName             ( void ) const { return "AnimationCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const { return "animgroup";         }

protected:

    KEY_STANDARD_STUFF

protected:
    
    friend class animation_compiler_base;
};


#endif
