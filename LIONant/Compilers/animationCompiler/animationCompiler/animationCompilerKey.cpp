//
//  fontCompilerKeyObject.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "animationCompilerKey.h"

xprop_enum::entry animation_compiler_key::entry_params::s_LoopTypeEnumList[]=
{
    { xprop_enum::entry( LOOP_TYPE_NONE,        X_STR("NO_LOOP") )     },
    { xprop_enum::entry( LOOP_TYPE_LOOP,        X_STR("LOOP") )        },
    { xprop_enum::entry( LOOP_TYPE_PING_PONG,   X_STR("PINGPONG") )    },
    { xprop_enum::entry() }
};

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void animation_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "SkeletonFilePath", OVERRIDE_MAIN_SKELETON_FILEPATH, "This is the name of the skeletong to be used in this anim group. rawanim or a fbx file.", "*.fbx,*.rawanim" )
    KEY_PROP_ENUM( "bKeepBind",             OVERRIDE_MAIN_KEEP_BINDING, g_PropBool, "Will bake the bind pouse into the keyframes which means less computations at runtime" )
}

//-------------------------------------------------------------------------------------------------

xbool animation_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "SkeletonFilePath", OVERRIDE_MAIN_SKELETON_FILEPATH,    g_PropFilePath.Query( Query, m_SkeletonPath ) )              
    KEY_PROP_QUERY( "bKeepBind",        OVERRIDE_MAIN_KEEP_BINDING,         g_PropBool.Query( Query, m_bKeepBind ) )              

    return FALSE;
}        

//-------------------------------------------------------------------------------------------------

void animation_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_SkeletonPath,            OVERRIDE_MAIN_SKELETON_FILEPATH,        .Copy   )
    KEY_PROP_UPDATE( m_bKeepBind,               OVERRIDE_MAIN_KEEP_BINDING,             =       )
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// entry_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void animation_compiler_key::entry_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "AnimFile",             OVERRIDE_ENTRY_FILEPATH,                            "This is the name of the animation to be used in this anim group. rawanim or a fbx file.", "*.fbx,*.rawanim" )
    KEY_PROP_ENUM( "Name",                      OVERRIDE_ENTRY_NAME,                    g_PropString,   "Name of the animation" )
    KEY_PROP_ENUM( "HandleAngle",               OVERRIDE_ENTRY_HANDLE_ANGLE,            g_PropFloat,    "This is the angle which the move handle is facing" )
    KEY_PROP_ENUM( "FPS",                       OVERRIDE_ENTRY_FPS,                     g_PropInt,      "What frame rate should the animation be sample at" )
    KEY_PROP_ENUM( "ResampleFPS",               OVERRIDE_ENTRY_RESAMPLE_FPS,            g_PropInt,      "Resample the animation to match the FPS specify here. This is a way to reduce memory at the cost of quality." )
    KEY_PROP_ENUM( "Loop",                      OVERRIDE_ENTRY_LOOP_TYPE,               g_PropEnum,     "Flags this animation as a (NOT_LOOP, LOOP, or PINGPONG)" )

    KEY_PROP_ENUM( "LoopInterpolation",         OVERRIDE_ENTRY_LOOP_INTERPOLATION,      g_PropBool,     "Will blend the begging of the last frame with the beging of the next frame, this should not be require of the animator setup the loops properly. This is also ignore for pingpoing loops." )

    KEY_PROP_ENUM( "AcumulateTransmationX",     OVERRIDE_ENTRY_ACC_TRANSLATION_X,       g_PropBool,     "This allows the animation to move in the world (XZ plane) based on its translations" )
    KEY_PROP_ENUM( "AcumulateTransmationY",     OVERRIDE_ENTRY_ACC_TRANSLATION_Y,       g_PropBool,     "This allows the animation to move in the world (XZ plane) based on its translations" )
    KEY_PROP_ENUM( "AcumulateTransmationZ",     OVERRIDE_ENTRY_ACC_TRANSLATION_Z,       g_PropBool,     "This allows the animation to move in the world (XZ plane) based on its translations" )

    KEY_PROP_ENUM( "AcumulateRotationPitch",    OVERRIDE_ENTRY_ACC_ROTATION_PITCH,      g_PropBool,     "This allows the animation to move in the world (Y plane) based on its translations" )
    KEY_PROP_ENUM( "AcumulateRotationYaw",      OVERRIDE_ENTRY_ACC_ROTATION_YAW,        g_PropBool,     "This allows the animation to move in the world (Y plane) based on its translations" )
    KEY_PROP_ENUM( "AcumulateRotationRoll",     OVERRIDE_ENTRY_ACC_ROTATION_ROLL,       g_PropBool,     "This allows the animation to move in the world (Y plane) based on its translations" )

    KEY_PROP_ENUM( "UseGravity",                OVERRIDE_ENTRY_GRAVITY,                 g_PropBool,     "This allows the animation to use physics motions such gravity" )
    KEY_PROP_ENUM( "UseCollision",              OVERRIDE_ENTRY_WORLD_COLLISION,         g_PropBool,     "This allows the animation to use colission in the physics" )
    KEY_PROP_ENUM( "AnimationWeight",           OVERRIDE_ENTRY_WEIGHT,                  g_PropFloat,    "Indicates the likelihood of this particular animation been selected from a group of animation with the same name"  )

    KEY_PROP_ENUM( "StartingAnimationFrame",    OVERRIDE_ENTRY_START_ANIMATION_FRAME,   g_PropInt,      "At which frame will the animation will beging, any frames previous will be remove." )

    KEY_PROP_ENUM( "RecenterAccAnim",           OVERRIDE_ENTRY_RECENTER_ACCANIM,        g_PropBool,     "The offset from (0,0,0) is usefull for looping animations. The distance in frame zero from the origin is taken as the translational distance from the end of the frame to zero. This option will use the linear velocity of frame0 to 1 to compute the centering." )
}

//-------------------------------------------------------------------------------------------------

xbool animation_compiler_key::entry_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "AnimFile",                 OVERRIDE_ENTRY_FILEPATH,                g_PropFilePath.Query( Query, m_FilePath ) )              
    KEY_PROP_QUERY( "Name",                     OVERRIDE_ENTRY_NAME,                    g_PropString.Query( Query, m_Name ) )                    
    KEY_PROP_QUERY( "HandleAngle",              OVERRIDE_ENTRY_HANDLE_ANGLE,            g_PropFloat.Query( Query, m_HandleAngle, 0.f, 360.f ) )  
    KEY_PROP_QUERY( "FPS",                      OVERRIDE_ENTRY_FPS,                     g_PropInt.Query( Query, m_FPS, 5, 120 ) )               
    KEY_PROP_QUERY( "ResampleFPS",              OVERRIDE_ENTRY_RESAMPLE_FPS,            g_PropInt.Query( Query, m_FPS, 5, 120 ) )               
    KEY_PROP_QUERY( "Loop",                     OVERRIDE_ENTRY_LOOP_TYPE,               g_PropEnum.Query( Query, m_LoopType, s_LoopTypeEnumList ) )                  

    KEY_PROP_QUERY( "LoopInterpolation",        OVERRIDE_ENTRY_LOOP_INTERPOLATION,      g_PropBool.Query( Query, m_bLoopInterpolation ) )

    KEY_PROP_QUERY( "AcumulateTransmationX",    OVERRIDE_ENTRY_ACC_TRANSLATION_X,       g_PropBool.Query( Query, m_bAccumTranslationX ) )
    KEY_PROP_QUERY( "AcumulateTransmationY",    OVERRIDE_ENTRY_ACC_TRANSLATION_Y,       g_PropBool.Query( Query, m_bAccumTranslationY ) )
    KEY_PROP_QUERY( "AcumulateTransmationZ",    OVERRIDE_ENTRY_ACC_TRANSLATION_Z,       g_PropBool.Query( Query, m_bAccumTranslationZ ) )

    KEY_PROP_QUERY( "AcumulateRotationPitch",   OVERRIDE_ENTRY_ACC_ROTATION_PITCH,      g_PropBool.Query( Query, m_bAccumRotationPitch ) )         
    KEY_PROP_QUERY( "AcumulateRotationYaw",     OVERRIDE_ENTRY_ACC_ROTATION_YAW,        g_PropBool.Query( Query, m_bAccumRotationYaw ) )
    KEY_PROP_QUERY( "AcumulateRotationRoll",    OVERRIDE_ENTRY_ACC_ROTATION_ROLL,       g_PropBool.Query( Query, m_bAccumRotationRoll ) )

    KEY_PROP_QUERY( "UseGravity",               OVERRIDE_ENTRY_GRAVITY,                 g_PropBool.Query( Query, m_bGravity ) )                  
    KEY_PROP_QUERY( "UseCollision",             OVERRIDE_ENTRY_WORLD_COLLISION,         g_PropBool.Query( Query, m_bWorldCollision ) )           
    KEY_PROP_QUERY( "AnimationWeight",          OVERRIDE_ENTRY_WEIGHT,                  g_PropFloat.Query( Query, m_Weight, 0, 1 ) )                    

    KEY_PROP_QUERY( "StartingAnimationFrame",   OVERRIDE_ENTRY_START_ANIMATION_FRAME,   g_PropInt.Query( Query, m_StartAnimationFrame, 0, S32_MAX ))

    KEY_PROP_QUERY( "RecenterAccAnim",          OVERRIDE_ENTRY_RECENTER_ACCANIM,        g_PropBool.Query( Query, m_bAccRecenterAnim ))

    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void animation_compiler_key::entry_params::onUpdateFromSrc( const entry_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_FilePath,                OVERRIDE_ENTRY_FILEPATH,                .Copy )
    KEY_PROP_UPDATE( m_Name,                    OVERRIDE_ENTRY_NAME,                    .Copy )
    KEY_PROP_UPDATE( m_FPS,                     OVERRIDE_ENTRY_FPS,                     = )
    KEY_PROP_UPDATE( m_ResampleFPS,             OVERRIDE_ENTRY_RESAMPLE_FPS,            = )
    KEY_PROP_UPDATE( m_LoopType,                OVERRIDE_ENTRY_LOOP_TYPE,               = )

    KEY_PROP_UPDATE( m_bLoopInterpolation,      OVERRIDE_ENTRY_LOOP_INTERPOLATION,      = )

    KEY_PROP_UPDATE( m_bAccumTranslationX,      OVERRIDE_ENTRY_ACC_TRANSLATION_X,       = )
    KEY_PROP_UPDATE( m_bAccumTranslationY,      OVERRIDE_ENTRY_ACC_TRANSLATION_Y,       = )
    KEY_PROP_UPDATE( m_bAccumTranslationZ,      OVERRIDE_ENTRY_ACC_TRANSLATION_Z,       = )

    KEY_PROP_UPDATE( m_bAccumRotationPitch,     OVERRIDE_ENTRY_ACC_ROTATION_PITCH,      = )
    KEY_PROP_UPDATE( m_bAccumRotationYaw,       OVERRIDE_ENTRY_ACC_ROTATION_YAW,        = )
    KEY_PROP_UPDATE( m_bAccumRotationRoll,      OVERRIDE_ENTRY_ACC_ROTATION_ROLL,       = )

    KEY_PROP_UPDATE( m_bGravity,                OVERRIDE_ENTRY_GRAVITY,                 = )
    KEY_PROP_UPDATE( m_bWorldCollision,         OVERRIDE_ENTRY_WORLD_COLLISION,         = )
    KEY_PROP_UPDATE( m_Weight,                  OVERRIDE_ENTRY_WEIGHT,                  = )

    KEY_PROP_UPDATE( m_StartAnimationFrame,     OVERRIDE_ENTRY_START_ANIMATION_FRAME,   = )
    KEY_PROP_UPDATE( m_bAccRecenterAnim,           OVERRIDE_ENTRY_RECENTER_ACCANIM,        = )
}

