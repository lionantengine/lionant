//
//  fontGlyph.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/29/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef FONT_GLYPH_H
#define FONT_GLYPH_H

struct FT_FaceRec_;

class font_glyph
{
public:
    
    struct glypth_info
    {
        xbitmap     m_Bitmap;
        f32         m_XOffset;
        f32         m_YOffset;
        f32         m_XAdvance;
        f32         m_YAdvance;
        u16         m_Unicode;
    };
    
public:
                        font_glyph      ( void );
                       ~font_glyph      ( void );
    void                LoadFont        ( const char* pFileName, s32 psize, xbool bDistanceField );
    
    xbool               hasGlyph        ( s32 unicode );
    void                CreateGlyph     ( glypth_info& Info, s32 unicode );
    
private:
    
    FT_FaceRec_*    m_pFace         {NULL};
    s32             m_HighResSize   {0};          // Size associated with this face.
    s32             m_LowResSize    {0};
    xbool           m_bDistanceField{FALSE};
};


#endif
