//
//  fontBaseCompiler.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "FontCompilerBase.h"
#include "PerfectHashing.h"
#include "SpriteCompilerKey.h"

//-------------------------------------------------------------------------------------------------

void font_compiler_base::ComputeGlypths(
    compiler_font&                  CompilerFont,
    const xbool                     bDistanceField,
    const s32                       Size,
    const xstring&                  FontFileName,
    const char_file&                TextFile ) const
{
    font_glyph                      FontGlyph;
    xstring                         FontPath;
    char                            FontName[xfile::MAX_FNAME];
    xptr<font_glyph::glypth_info>&  CharInfoList = CompilerFont.m_ClyphInfo;
    
    //
    // Backup some parameters
    //
    CompilerFont.m_FontSize = Size;
    
    //
    // Load the font
    //
    FontPath.Format( "%s/%s",
                    (const char*)m_ProjectPath,
                    (const char*)FontFileName );
    
    FontGlyph.LoadFont( FontPath, Size, bDistanceField );
    
    // Get out the file name
    x_splitpath( FontFileName, NULL, NULL, FontName, NULL );
    
    // Save out the font name
    CompilerFont.m_FontName.Copy(FontName);
    
    //
    // Create and save all clypths
    //
    xstring DependencyPath;
    
    DependencyPath.Format("%s/%s_%d_%d",
                          (const char*)m_IntermediateDataPath,
                          (const char*)FontName,
                          Size,
                          bDistanceField );
    
    if( x_io::MakeDir( DependencyPath ) == FALSE )
    {
        x_throw("ERROR: Unable to create the dependency directory [%s]", (const char*)DependencyPath );
    }
    
    //
    // Allocate all the char infos
    //
    CharInfoList.New( TextFile.m_Characters.getCount() );
    
    //
    // Create the characters
    //
    f32 XMin = 9999999;
    f32 YMin = XMin;
    for( const u16& Char : TextFile.m_Characters )
    {
        const s32                   Index       = s32( &Char - &TextFile.m_Characters [0] );
        font_glyph::glypth_info&    GlyphInfo   = CharInfoList[Index];

        FontGlyph.CreateGlyph( GlyphInfo, Char );
     
        XMin = x_Min( XMin, GlyphInfo.m_XOffset );
        YMin = x_Min( YMin, GlyphInfo.m_YOffset );
    }
    
    //
    // Normalize the Hotpoint for all the characters
    //
    for( font_glyph::glypth_info& GlyphInfo : CharInfoList )
    {
        // Give
        const s32 DeltaX = (s32)x_Round( GlyphInfo.m_XOffset - XMin, 1 );
        const s32 DeltaY = (s32)x_Round( GlyphInfo.m_YOffset - YMin, 1 );
   
        if( DeltaX == 0 && DeltaY == 0 )
            continue;
     
        const s32 OldW = GlyphInfo.m_Bitmap.getWidth();
        const s32 OldH = GlyphInfo.m_Bitmap.getHeight();
        const s32 NewW = OldW + DeltaX;
        const s32 NewH = OldH + DeltaY;
        
        xbitmap TempBitmap;
        TempBitmap.CreateBitmap( NewW, NewH );
        xcolor* pData = (xcolor*)TempBitmap.getMip(0);
        x_memset( pData, 0, sizeof(xcolor) * NewW * NewH );
        
        const xcolor* pSrcData = (const xcolor* )GlyphInfo.m_Bitmap.getMip(0);
        for( s32 y=0; y<OldH; y++ )
        for( s32 x=0; x<OldW; x++ )
        {
            pData[ (DeltaX + x) + y*NewW ] = pSrcData[ x + y*OldW ];
        }

        // Setup the new bitmap
        GlyphInfo.m_Bitmap.Kill();
        x_memcpy(&GlyphInfo.m_Bitmap,&TempBitmap,sizeof( TempBitmap ));
        TempBitmap.setOwnMemory(FALSE);
        
        // Update their offset to be the same as the Mins
        GlyphInfo.m_XOffset = XMin;
        GlyphInfo.m_YOffset = YMin;
    }
    
    //
    // Save all the files
    //
    for( const u16& Char : TextFile.m_Characters )
    {
        const s32                   Index       = s32( &Char - &TextFile.m_Characters [0] );
        font_glyph::glypth_info&    GlyphInfo   = CharInfoList[Index];
        
        //
        // Get all the info out
        //
        xstring DepenFileName;
        DepenFileName.Format("%s/%s%03d.tga",
                             (const char*)DependencyPath,
                             (const char*)FontName,
                             Index );
        
        // save frame zero for future reference
        if( Index == 0 )
        {
            CompilerFont.m_SrcSpriteFileName = DepenFileName;
        }
        
        GlyphInfo.m_Bitmap.SaveTGA( DepenFileName );
    }
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::ReadTextFile( const char* pFileName, char_file& TextFile ) const
{
    //
    // Create the string for the path
    //
    xstring TextFilePath;
    TextFilePath.Format( "%s/%s", (const char*)m_ProjectPath, pFileName );
    
    //
    // Mark this entry as valid
    //
    TextFile.m_bValid   = TRUE;
    
    //
    // Read all the characters
    //
    {
        xfile File;
        if( File.Open( TextFilePath, "rb" ) == FALSE )
            x_throw( "ERROR: Unable to open [%s] please check to make sure the file is ok.", pFileName);
        
        // Allocate memory for our characters
        const s32 CharCount = File.GetFileLength()/2;
        TextFile.m_Characters.Alloc( CharCount );
        
        // make sure that we have some characters
        if( CharCount == 0 )
        {
            x_throw("ERROR: We could not find any characters inside the file[%s]. Please make sure that the txt file is unicode.",pFileName);
        }
        
        // Read all the characters in one step
        if( FALSE == File.Read( &TextFile.m_Characters[0], CharCount ) )
        {
            x_throw( "ERROR: Fail to read characters from the text file [%s]",pFileName);
        }
     
        File.Close();
    }
    
    //
    // Short all the character that we found
    //
    x_qsort( &TextFile.m_Characters[0], TextFile.m_Characters.getCount(), sizeof(u16), []( const void* pA, const void* pB )->s32
    {
        const u16& A = *(const u16*)pA;
        const u16& B = *(const u16*)pB;
        
        if( A < B ) return -1;
        return A > B;
    });
    
    ASSERT( TextFile.m_Characters[0] <= TextFile.m_Characters[TextFile.m_Characters.getCount()-1] );
    
    //
    // Remove all duplicated characters
    //
    s32 iFinalLength=0;
    TextFile.m_Characters[ 0 ] = TextFile.m_Characters[0];
    for( s32 i=1; i<TextFile.m_Characters.getCount()-1; i++ )
    {
        if( TextFile.m_Characters[ iFinalLength ] == TextFile.m_Characters[i+1] )
            continue;
        
        iFinalLength++;
        TextFile.m_Characters[ iFinalLength ] = TextFile.m_Characters[i+1];
    }
    
    // Count for the first character
    iFinalLength++;
    
    // Resize the buffer to the final count
    if( iFinalLength != TextFile.m_Characters.getCount() )
        TextFile.m_Characters.Resize( iFinalLength );
    
    //
    // Lets create a Hash Table for this
    //
    buid_perfect_hashing Hashing;
    
    Hashing.Compile( TextFile.m_HashTableSize, TextFile.m_PHTable, TextFile.m_Characters );

    //
    // Fill all the values of the hash table
    //
    TextFile.m_HashTable.Alloc( TextFile.m_HashTableSize );
    TextFile.m_HashTable.SetMemory(0xffffffff);
    
    for( s32 i=0; i < TextFile.m_Characters.getCount(); i++ )
    {
        const u16 Key    = TextFile.m_Characters[ i ];
        const s32 Row    = Key / TextFile.m_PHTable.getCount();
        const s32 Column = Key % TextFile.m_PHTable.getCount();
        const s32 iHash  = Column + TextFile.m_PHTable[Row];
        
        TextFile.m_HashTable[iHash] = i;
    }
    
    //
    // Test the hold Hash table thing...
    //
    if( 1 )
    {
        // Try to get a few of the characters using the hash table
        xrandom_small X;
        
        for( s32 i=0; i<20; i++ )
        {
            const u16 iOri   = X.Rand32(0, iFinalLength );
            const u16 Key    = TextFile.m_Characters[ iOri ];
            const s32 Row    = Key / TextFile.m_PHTable.getCount();
            const s32 Column = Key % TextFile.m_PHTable.getCount();
            const s32 iHash  = Column + TextFile.m_PHTable[Row];
         
            const u16 FoundKey = TextFile.m_HashTable[iHash];
            ASSERT( FoundKey == iOri );
        }
    }
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::ExportAtlas( void ) const
{
    //
    // Get the length of the project path
    //
    const s32 ProjLenght = m_ProjectPath.GetLength()+1;
    
    //
    // Export External dependency for the atlas
    //
    compile_sprite_atlas_key SpriteKey( *this );
    
    // Load the key file defaultsthis
    SpriteKey.LoadDefaults( m_ProjectPath );
    
    const_ref( rMain, m_Key.m_Main );
    // First lets set some Global PArameters
    g_PropBool.Set ( SpriteKey, "Key/Main/Global/bDisableAlpha", 1 );
    g_PropFloat.Set( SpriteKey, "Key/Main/Global/AtlasCompression", rMain[0].m_CompressionLevel );
    g_PropEnum.Set ( SpriteKey, "Key/Main/Global/FilterMig", SpriteKey.getFilterMode( eng_texture::FILTER_MINMAG_LINEAR ) );
    g_PropEnum.Set ( SpriteKey, "Key/Main/Global/FilterMag", SpriteKey.getFilterMode( eng_texture::FILTER_MINMAG_LINEAR ) );
    g_PropInt.Set  ( SpriteKey, "Key/Entries/Count",  m_FontList.getCount() );
    
    for( s32 i=0; i<m_FontList.getCount(); i ++ )
    {
        g_PropEnum.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/Global/HotpointXMode",i), SpriteKey.getHotpointEnum( compile_sprite_atlas_key::HOTPOINT_ZERO_RELATIVE ) );
        g_PropEnum.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/Global/HotpointYMode",i), SpriteKey.getHotpointEnum( compile_sprite_atlas_key::HOTPOINT_ZERO_RELATIVE ) );
        
        if( m_FontList[i].m_bSingleCompile )
        {
            g_PropFilePath.Set( SpriteKey, xfs("Key/Entries/Entry[%d]/Global/FileName",i),     &m_FontList[i].m_Platform[0].m_SrcSpriteFileName[ProjLenght] );
            g_PropFloat.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/Global/HotpointXOffset",i), m_FontList[i].m_Platform[0].m_ClyphInfo[0].m_XOffset );
            g_PropFloat.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/Global/HotpointYOffset",i), m_FontList[i].m_Platform[0].m_ClyphInfo[0].m_YOffset );
        }
        else
        {
            for( s32 j=0; j<X_PLATFORM_COUNT; j++ )
            {
                // We are not compiling for this platform so skip it
                if( !(m_PlatformMask&(1<<j)) )
                    continue;
                
                const char* pT = x_PlatformString( xplatform(j) );
                g_PropFilePath.Set( SpriteKey, xfs("Key/Entries/Entry[%d]/%s/FileName",i,pT), &m_FontList[i].m_Platform[0].m_SrcSpriteFileName[ProjLenght] );
                g_PropFloat.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/%s/HotpointXOffset",i,pT), m_FontList[i].m_Platform[j].m_ClyphInfo[0].m_XOffset );
                g_PropFloat.Set ( SpriteKey, xfs("Key/Entries/Entry[%d]/%s/HotpointYOffset",i,pT), m_FontList[i].m_Platform[j].m_ClyphInfo[0].m_YOffset );
                
            }
        }
    }
    
    xstring                 ExternalKeyPath;
    eng_resource_guid       Guid;
    Guid.setup( m_RscGuid, eng_sprite_rsc::UID );
    
    if( m_RscFileName.IsEmpty() )
    {
        ExternalKeyPath.Format( "%s/%s.%s.txt",
                               (const char*)m_ExternalCompilerKeysPath,
                               (const char*)Guid.getAlphaString(),
                               (const char*)SpriteKey.getCompiledExt() );
    }
    else
    {
        ExternalKeyPath.Format( "%s/%s--%s.%s.txt",
                               (const char*)m_ExternalCompilerKeysPath,
                               (const char*)m_RscFileName,
                               (const char*)Guid.getAlphaString(),
                               (const char*)SpriteKey.getCompiledExt() );
    }
    
    AddExternalDependencyKey( xstring::BuildFromFormat( &ExternalKeyPath[ ProjLenght ] ) );
    SpriteKey.Save( ExternalKeyPath );
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::ExportResource( void ) const
{
    //
    // Go though all the targets
    //
    for( const platform& Target : m_Target )
    {
        if( Target.m_bValid == FALSE )
            continue;

        eng_font_rsc::font_rsc                          FontRsc;
        xptr<eng_font_rsc::font>                        FontList;
        xarray< xptr<eng_font_rsc::character_info> >    CharInfoList;
        xarray<eng_font_rsc::char_set>                  CharSet;
        xarray<s32>                                     ListOfCharSet;
        
        //
        // Create the list of fonts that we are going to export
        //
        FontRsc.m_nFonts = m_FontList.getCount();
        
        FontList.Alloc( FontRsc.m_nFonts  );
        FontRsc.m_Font.m_Ptr = &FontList[0];
        
        for( s32 i=0; i<FontRsc.m_nFonts ; i++ )
        {
            const per_platform&     BasePlatform    = m_FontList[i];
            const compiler_font&    SrcFont         = BasePlatform.m_Platform[ BasePlatform.m_bSingleCompile?0:Target.m_Platform ];
            const char_file&        TextFile        =  m_CharFileList[ SrcFont.m_iCharFile ];
            eng_font_rsc::font&     DestFont        = FontList[i];
            s32                     iCharSet        = -1;
            xptr<eng_font_rsc::character_info>& CharInfoMemory = CharInfoList.append();
            
            //
            // Creat the character set
            //
            
            // First lets see if we already have it in our list
            for( s32 c=0;c<ListOfCharSet.getCount(); c++ )
            {
                if( ListOfCharSet[c] ==  SrcFont.m_iCharFile )
                {
                    iCharSet = c;
                    break;
                }
            }
            
            // if not then lets create it
            if( iCharSet == -1 )
            {
                iCharSet = CharSet.getCount();
                
                eng_font_rsc::char_set& CS = CharSet.append();
                
                // add a referece of this index for the future
                ListOfCharSet.append() = SrcFont.m_iCharFile;
                
                CS.m_PHTable.m_Ptr    = &TextFile.m_PHTable[0];
                CS.m_PHTableSize      = TextFile.m_PHTable.getCount();
                CS.m_HashTable.m_Ptr  = &TextFile.m_HashTable[0];
                CS.m_HashSize         = TextFile.m_HashTableSize;
                CS.m_nCharacters      = TextFile.m_Characters.getCount();
            }
            
            //
            // Copy the char info into the final struct
            //
            DestFont.m_nCharacterInfos    = TextFile.m_Characters.getCount();
            
            CharInfoMemory.Alloc( DestFont.m_nCharacterInfos );
            
            DestFont.m_CharacterInfo.m_Ptr = &CharInfoMemory[0];
            
            for( s32 j=0; j<DestFont.m_nCharacterInfos; j++ )
            {
                DestFont.m_CharacterInfo.m_Ptr[j].m_XAdvance = (s32) x_Round( SrcFont.m_ClyphInfo[j].m_XAdvance, 1 );
                DestFont.m_CharacterInfo.m_Ptr[j].m_YAdvance = (s32) x_Round( SrcFont.m_ClyphInfo[j].m_YAdvance, 1 );
                DestFont.m_CharacterInfo.m_Ptr[j].m_Original = SrcFont.m_ClyphInfo[j].m_Unicode;
            }
            
            //
            // Set the rest
            // We append 000 since in the atlas is treated as an animated sprite
            //
            DestFont.m_iCharSet         = iCharSet;
            DestFont.m_FontHashName     = x_strHash( xfs("%s000",(const char*)SrcFont.m_FontName), 0xffff );
            DestFont.m_FontSize         = SrcFont.m_FontSize;
            DestFont.m_Flags            = 0;
            
            ASSERT(TextFile.m_HashTable.getCount() == TextFile.m_HashTableSize );
        }
        
        //
        // Set our charset point and count
        //
        FontRsc.m_nCharSets     = CharSet.getCount();
        FontRsc.m_CharSet.m_Ptr = &CharSet[0];
        
        //
        // Make sure that fonts are shorted by the hash name to match the sprite atlas format
        //
        x_qsort( FontList, FontList.getCount(), sizeof(FontList[0]), [](const void* pA, const void* pB ) -> s32
        {
            const eng_font_rsc::font& A = *(const eng_font_rsc::font*)pA;
            const eng_font_rsc::font& B = *(const eng_font_rsc::font*)pB;
            
            if( A.m_FontHashName < B.m_FontHashName ) return -1;
            return A.m_FontHashName > B.m_FontHashName;
        });
        
        //
        // Ok Ready to save everything then
        //
        xserialfile     SerialFile;
        xstring         FinalRscPath;
        
        FinalRscPath = getFinalResourceName( Target );
        
        SerialFile.Save( (const char*)FinalRscPath, FontRsc, xserialfile::FLAGS_DEFAULT, x_PlatformSwapEndian( Target.m_Platform ) );
        
        // for debugging
        if( (1) )
        {
            xserialfile             test;
            eng_font_rsc::font_rsc* pMyFont;
            test.Load( (const char*)FinalRscPath, pMyFont );
            
            ASSERT( pMyFont );
            x_delete( pMyFont );
        }
    }
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::CompilationUniqueness( void )
{
    const_ref( rEntries, m_Key.m_lEntry );
    const_ref( rMain, m_Key.m_Main );
    for( s32 i=0; i<rEntries.getCount(); i ++ )
    {
        const font_compiler_key::entry_ppflist&  KeyPlatform  = rEntries[i];
        per_platform&                            BasePlatform = m_FontList[i];
        
        BasePlatform.m_bSingleCompile = TRUE;
        BasePlatform.m_bSingleText    = TRUE;
        
        //
        // Lets go throught the key platforms
        //
        for( s32 j=0; j<m_Target.getCount(); j++ )
        {
            // We are not compiling for this platform so skip it
            if( !m_Target[j].m_bValid )
                continue;
            
            const font_compiler_key::entry_params& KeyParams = KeyPlatform[j];
            
            if( x_FlagIsOn( KeyParams.m_OverrideBits, font_compiler_key::entry_params::SINGLE_COMPILATION_FLAGS ) )
            {
                BasePlatform.m_bSingleCompile = FALSE;
            }
            
            if( x_FlagIsOn( rMain[j].m_OverrideBits, font_compiler_key::main_params::SINGLE_COMPILATION_MAIN_FLAGS )  )
            {
                BasePlatform.m_bSingleCompile = FALSE;
            }
            
            if( x_FlagIsOn( KeyParams.m_OverrideBits, font_compiler_key::entry_params::OVERRIDE_TEXT_FILEPATH) )
            {
                BasePlatform.m_bSingleText    = FALSE;
                BasePlatform.m_bSingleCompile = FALSE;
            }
        }
        
        //
        // Make sure everything is ok
        //
        const font_compiler_key::entry_params& KeyParams = KeyPlatform[0];
        
        // If still true then the base must be overwritten
        if( BasePlatform.m_bSingleCompile )
        {
            if( x_FlagIsOn( KeyParams.m_OverrideBits, font_compiler_key::entry_params::SINGLE_COMPILATION_FLAGS ) == FALSE )
            {
                x_throw("ERROR: The Free Type Source file never got set");
            }
        }
        
        if( 0&&BasePlatform.m_bSingleText )
        {
            if( x_FlagIsOn( KeyParams.m_OverrideBits, font_compiler_key::entry_params::OVERRIDE_TEXT_FILEPATH ) == FALSE )
            {
                x_throw("ERROR: The source Text File was never set");
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::CompileAllCharFiles( void )
{
    //
    // First lets factor them out into a unique list
    //
    const_ref( rEntries, m_Key.m_lEntry );
    for( s32 i=0; i<rEntries.getCount(); i ++ )
    {
        const font_compiler_key::entry_ppflist&  KeyPlatform  = rEntries[i];
        per_platform&                            BasePlatform = m_FontList[i];
        
        //
        // Lets go throught the key platforms
        //
        for( s32 j=0; j<m_Target.getCount(); j++ )
        {
            // We are not compiling for this platform so skip it
            if( j && !m_Target[j].m_bValid )
                continue;
            
            const font_compiler_key::entry_params& KeyParams = KeyPlatform[j];
            
            xstring FileName;
            xbool   iCharFile=-1;
            
            FileName.Copy( KeyParams.m_UnicodeTextFilePath );
            FileName.MakeLower();
            
            for( s32 i=0; i<m_CharFileList.getCount(); i++ )
            {
                if( m_CharFileList[i].m_CharFileName == FileName )
                {
                    iCharFile = i;
                    break;
                }
            }

            // We dont find it add one entry
            if( iCharFile == -1 )
            {
                char_file& CharFile     = m_CharFileList.append();
                iCharFile               = m_CharFileList.getCount() - 1;
            
                CharFile.m_CharFileName = FileName;
            }

            // Set the index of the char file
            BasePlatform.m_Platform[j].m_iCharFile = iCharFile;

            // We get to vail early if there is a single common charfile
            if( BasePlatform.m_bSingleText )
            {
                // lets be nice all set the rest of them (probably not needed but just in case)
                for( ; j<m_Target.getCount(); j++ )
                {
                    BasePlatform.m_Platform[j].m_iCharFile = iCharFile;
                }
                break;
            }
        }
    }
    
    //
    // Compile all the text files
    //
    for( char_file& CharFile: m_CharFileList )
    {
        ReadTextFile( CharFile.m_CharFileName, CharFile );
    }
    
    
    //
    // Compile all the text files
    //
    /*
    for( s32 i=0; i<m_Key.m_EntryList.getCount(); i ++ )
    {
        const font_compiler_key::platform_param& KeyPlatform  = m_Key.m_EntryList[i];
        per_platform&                            BasePlatform = m_FontList[i];
        
        if( BasePlatform.m_bSingleText )
        {
            ReadTextFile( KeyPlatform.m_Platform[0].m_UnicodeTextFilePath, BasePlatform.m_Platform[0].m_TextFileList );
        }
        else
        {
            for( s32 j=0; j<m_Target.getCount(); j++ )
            {
                // We are not compiling for this platform so skip it
                if( !m_Target[j].m_bValid )
                    continue;
                
                ReadTextFile( m_Key.m_EntryList[i].m_Platform[j].m_UnicodeTextFilePath, m_FontList[i].m_Platform[j].m_TextFileList );
            }
        }
    }
     */
}

//-------------------------------------------------------------------------------------------------

void font_compiler_base::onCompile( void )
{
    font_glyph      Font;
    xbitmap         Bitmap;
    
    //
    // Allocate all our fonts
    //
    m_FontList.appendList( m_Key.m_lEntry.getCount() );
    
    //
    // Determine Uniqueness of the compilation
    //
    CompilationUniqueness();
    
    //
    // Solve for all unique char file sets
    //
    CompileAllCharFiles();
    
    //
    // Compile all the fonts
    //
    const_ref( rEntryList,  m_Key.m_lEntry );
    const_ref( rMain,       m_Key.m_Main );
    for( s32 i=0; i<rEntryList.getCount(); i ++ )
    {
        const font_compiler_key::entry_ppflist&  KeyPlatform  = rEntryList[i];
        per_platform&                            BasePlatform = m_FontList[i];

        if( BasePlatform.m_bSingleCompile )
        {
            ComputeGlypths( BasePlatform.m_Platform[0],
                            rMain[0].m_bDistanceField,
                            KeyPlatform[0].m_BaseFontSize,
                            KeyPlatform[0].m_SrcFontFilePath,
                            m_CharFileList[ BasePlatform.m_Platform[0].m_iCharFile ] );
        }
        else
        {
            for( s32 j=0; j<m_Target.getCount(); j++ )
            {
                if( !m_Target[j].m_bValid )
                    continue;
                
                const s32 K = BasePlatform.m_bSingleText ?0:j;
                
                ComputeGlypths( BasePlatform.m_Platform[j],
                               rMain[j].m_bDistanceField,
                               KeyPlatform[j].m_BaseFontSize,
                               KeyPlatform[j].m_SrcFontFilePath,
                               m_CharFileList[ BasePlatform.m_Platform[K].m_iCharFile ] );
            }
        }
    }
    
    //
    // Export Files
    //
    ExportAtlas();
    ExportResource();
}
