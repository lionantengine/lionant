//
//  fontBaseCompiler.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef FONT_COMPILER_BASE_H
#define FONT_COMPILER_BASE_H

#include "FontCompilerKey.h"
#include "eng_base.h"
#include "fontGlyph.h"

class font_compiler_base : public compiler_base
{
public:
    
                                        font_compiler_base  ( void ) : m_Key( *this ) {}
    virtual void                        onCompile           ( void );
    virtual compiler_key_object&        getKeyObject        ( void )       { return m_Key;             }
    virtual const compiler_key_object&  getKeyObject        ( void ) const { return m_Key;             }

protected:

    struct char_file
    {
        xstring     m_CharFileName;
        xbool       m_bValid;
        xptr<u16>   m_Characters;
        s32         m_HashTableSize;
        xptr<u16>   m_PHTable;
        xptr<u16>   m_HashTable;
    };
    
    struct compiler_font
    {
        s32                                         m_FontSize;
        s32                                         m_iCharFile;
        xptr<font_glyph::glypth_info>               m_ClyphInfo;
        xstring                                     m_SrcSpriteFileName;
        xstring                                     m_FontName;
    };
    
    struct per_platform
    {
        xsafe_array<compiler_font,X_PLATFORM_COUNT>  m_Platform;
        xbool                                        m_bSingleCompile;
        xbool                                        m_bSingleText;
    };
    
protected:

    void                                ReadTextFile            ( const char* pFileName, char_file& TextFile ) const;
    void                                ComputeGlypths          ( compiler_font&        CompilerFont,
                                                                    const xbool         bDistanceField,
                                                                    const s32           Size,
                                                                    const xstring&      FontFileName,
                                                                    const char_file&    TextFile ) const;
    void                                ExportAtlas             ( void ) const;
    void                                ExportResource          ( void ) const;
    void                                CompileAllCharFiles     ( void );
    void                                CompilationUniqueness   ( void );
    
protected:
    
    font_compiler_key                           m_Key;
    xarray<per_platform>                        m_FontList;
    xarray<char_file>                           m_CharFileList;
};


#endif
