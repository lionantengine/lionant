//
//  fontGlyph.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/29/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//
#include "ft2build.h"
#include FT_FREETYPE_H
#include FT_BITMAP_H

#include "edtaa3func.h"

#include "x_base.h"
#include "fontGlyph.h"

#include "DistanceField.h"

static FT_LibraryRec_* g_pLibRef  = NULL;
static s32             s_RefCount = 0;

// Global variables require by freetype
s32 z_error=1;
s32 z_verbose=1;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


#define min(A,B) x_Min(A,B)
#define max(A,B) x_Max(A,B)

// ------------------------------------------------------ make_distance_map ---
static
void distance_map( double *data, int width, int height )
{
    xptr<xbyte> Buffer;
    
    Buffer.Alloc( 4 * width * height * sizeof(double) + 2 * width * height * sizeof(short)  );
    Buffer.SetMemory(0);
    
    double * gx      = (double *) &Buffer           [0];
    double * gy      = (double *) &gx               [width * height];
    double * outside = (double *) &gy               [width * height];
    double * inside  = (double *) &outside          [width * height];
    short  * xdist   = (short *)  &inside           [width * height];
    short  * ydist   = (short *)  &xdist            [width * height];
    
    int i;
    
    // Compute outside = edtaa3(bitmap); % Transform background (0's)
    computegradient( data, height, width, gx, gy );
    edtaa3(data, gx, gy, width, height, xdist, ydist, outside);
    for( i=0; i<width*height; ++i)
    {
        if( outside[i] < 0.0 )
        {
            outside[i] = 0.0;
        }
    }
    
    // Compute inside = edtaa3(1-bitmap); % Transform foreground (1's)
    memset( gx, 0, sizeof(double)*width*height );
    memset( gy, 0, sizeof(double)*width*height );
    for( i=0; i<width*height; ++i)
        data[i] = 1 - data[i];
    computegradient( data, height, width, gx, gy );
    edtaa3( data, gx, gy, width, height, xdist, ydist, inside );
    for( i=0; i<width*height; ++i )
    {
        if( inside[i] < 0 )
        {
            inside[i] = 0.0;
        }
    }
    
    // distmap = outside - inside; % Bipolar distance field
    float vmin = +INFINITY;
    for( i=0; i<width*height; ++i)
    {
        outside[i] -= inside[i];
        if( outside[i] < vmin )
        {
            vmin = (f32)outside[i];
        }
    }
    vmin = x_Abs(vmin);
    for( i=0; i<width*height; ++i)
    {
        float v = (f32)outside[i];
        if     ( v < -vmin) outside[i] = -vmin;
        else if( v > +vmin) outside[i] = +vmin;
        data[i] = (outside[i]+vmin)/(2*vmin);
    }
}


// ------------------------------------------------------ MitchellNetravali ---
// Mitchell Netravali reconstruction filter
static
float MitchellNetravali( float x )
{
    const float B = 1/3.0f, C = 1/3.0f; // Recommended
    // const float B =   1.0, C =   0.0; // Cubic B-spline (smoother results)
    // const float B =   0.0, C = 1/2.0; // Catmull-Rom spline (sharper results)
    x = (float)fabs(x);
    if( x < 1 )
        return ( ( 12 -  9 * B - 6 * C) * x * x * x
                + (-18 + 12 * B + 6 * C) * x * x
                + (  6 -  2 * B) ) / 6;
    else if( x < 2 )
        return ( (     -B -  6 * C) * x * x * x
                + (  6 * B + 30 * C) * x * x
                + (-12 * B - 48 * C) * x
                + (  8 * B + 24 * C) ) / 6;
    else
        return 0;
}


// ------------------------------------------------------------ interpolate ---
static
float interpolate( float x, float y0, float y1, float y2, float y3 )
{
    float c0 = MitchellNetravali(x-1);
    float c1 = MitchellNetravali(x  );
    float c2 = MitchellNetravali(x+1);
    float c3 = MitchellNetravali(x+2);
    float r =  c0*y0 + c1*y1 + c2*y2 + c3*y3;
    return (f32)min( max( r, 0.0 ), 1.0 );
}


// ------------------------------------------------------------------ scale ---
static
int resize( double *src_data, size_t src_width, size_t src_height,
            double *dst_data, size_t dst_width, size_t dst_height )
{
    if( (src_width == dst_width) && (src_height == dst_height) )
    {
        memcpy( dst_data, src_data, src_width*src_height*sizeof(double));
        return 0;
    }
    size_t i,j;
    float xscale = src_width / (float) dst_width;
    float yscale = src_height / (float) dst_height;
    for( j=0; j < dst_height; ++j )
    {
        for( i=0; i < dst_width; ++i )
        {
            int src_i = (int) floor( i * xscale );
            int src_j = (int) floor( j * yscale );
            int i0 = (int)min( max( 0, src_i-1 ), src_width-1 );
            int i1 = (int)min( max( 0, src_i ), src_width - 1 );
            int i2 = (int)min( max( 0, src_i + 1 ), src_width - 1 );
            int i3 = (int)min( max( 0, src_i + 2 ), src_width - 1 );
            int j0 = (int)min( max( 0, src_j - 1 ), src_height - 1 );
            int j1 = (int)min( max( 0, src_j ), src_height - 1 );
            int j2 = (int)min( max( 0, src_j + 1 ), src_height - 1 );
            int j3 = (int)min( max( 0, src_j + 2 ), src_height - 1 );
            float t0 = (f32)interpolate( i / (float)dst_width,
                                   (float)src_data[j0*src_width+i0],
                                   (float)src_data[j0*src_width+i1],
                                   (float)src_data[j0*src_width+i2],
                                   (float)src_data[j0*src_width+i3] );
            float t1 = (f32)interpolate( i / (float) dst_width,
                                   (float)src_data[j1*src_width+i0],
                                   (float)src_data[j1*src_width+i1],
                                   (float)src_data[j1*src_width+i2],
                                   (float)src_data[j1*src_width+i3] );
            float t2 = (f32)interpolate( i / (float)dst_width,
                                   (float)src_data[j2*src_width+i0],
                                   (float)src_data[j2*src_width+i1],
                                   (float)src_data[j2*src_width+i2],
                                   (float)src_data[j2*src_width+i3] );
            float t3 = (f32)interpolate( i / (float)dst_width,
                                   (float)src_data[j3*src_width+i0],
                                   (float)src_data[j3*src_width+i1],
                                   (float)src_data[j3*src_width+i2],
                                   (float)src_data[j3*src_width+i3] );
            float y = (f32)interpolate( j / (float)dst_height, t0, t1, t2, t3 );
            dst_data[j*dst_width+i] = y;
        }
    }
    return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

font_glyph::font_glyph( void )
{
    if( s_RefCount == 0 )
    {
        s_RefCount++;
        
        //
        // Init the library
        //
        FT_Error err = FT_Init_FreeType( &g_pLibRef );
        
        if(0 != err)
        {
            x_throw( "ERROR: could not init FreeType: %d", err );
        }
    }
}

//------------------------------------------------------------------------------

font_glyph::~font_glyph( void )
{
    if( NULL != m_pFace )
    {
        FT_Done_Face( m_pFace );
    }
    
    //
    // Kill the library
    //
    s_RefCount--;
    if( s_RefCount == 0 )
    {
        FT_Error err = FT_Done_FreeType( g_pLibRef );
        
        if(0 != err)
        {
            x_throw( "ERROR: could not close FreeType: %d", err );
        }
    }
}


//------------------------------------------------------------------------------

void font_glyph::LoadFont( const char* pFilename, s32 psize, xbool bDistanceField )
{
    m_pFace             = NULL;
    m_LowResSize        = psize;
    m_bDistanceField    = bDistanceField;
    m_HighResSize       = m_bDistanceField?m_LowResSize*8:m_LowResSize;
    
    if( FT_New_Face( g_pLibRef, pFilename, 0, &m_pFace) )
    {
        x_throw( "ERROR: could not load font: %s", pFilename );
    }
    
    FT_Select_Charmap( m_pFace, FT_ENCODING_UNICODE );
    if( FT_Set_Pixel_Sizes( m_pFace, 0, m_HighResSize ))
    {
        x_throw( "ERROR: could not set font size to %d", m_HighResSize );
    }
}

//------------------------------------------------------------------------------

xbool font_glyph::hasGlyph(int unicode)
{
    return (FT_Get_Char_Index(m_pFace, unicode) > 0);
}

//------------------------------------------------------------------------------

void font_glyph::CreateGlyph( glypth_info& Info, s32 unicode )
{
    // Set the unicode for this glypth
    Info.m_Unicode = unicode;
    
    //
    // Get the character in question
    //
    s32 idx = FT_Get_Char_Index( m_pFace, unicode );
    if(0 == idx)
    {
        x_throw( "ERROR: could not find character of index %d", unicode );
    }
    
    //
    // Make sure that we have the right face
    //
    const s32 DpiToPixels = 64;
    FT_Set_Char_Size( m_pFace, m_HighResSize*DpiToPixels, 0, 72, 72 );
    if(FT_Load_Glyph(m_pFace, idx, FT_LOAD_RENDER | FT_LOAD_NO_HINTING | FT_LOAD_NO_AUTOHINT))
    {
        x_throw( "ERROR: could not load glyph %d", unicode );
    }
    
    //
    // Make sure that we can render it out
    //
    FT_GlyphSlot pFTGlyph = m_pFace->glyph;
    if( pFTGlyph->format != FT_GLYPH_FORMAT_BITMAP )
    {
        FT_Error err = FT_Render_Glyph( pFTGlyph, FT_RENDER_MODE_NORMAL );
        
        if( 0 != err )
        {
            x_throw( "ERROR: could not render glyph: %d", unicode );
        }
    }
    
    FT_GlyphSlot    slot    = m_pFace->glyph;
    FT_Bitmap       bitmap  = slot->bitmap;
    
    
    //
    // Deal wih the proper format
    //
    if( m_bDistanceField )
    {
        const s32       PaddingSize     = s32( 0.1f * m_HighResSize )&(~1);
        s32             highres_width   = bitmap.width + 2*PaddingSize;
        s32             highres_height  = bitmap.rows  + 2*PaddingSize;
        xptr<double>    highres_data;
        
        highres_data.Alloc( highres_width*highres_height );
        highres_data.SetMemory(0);
        
        // Copy high resolution bitmap with padding and normalize values
        for( s32 j=0; j < bitmap.rows; ++j )
        {
            for( s32 i=0; i < bitmap.width; ++i )
            {
                const s32 x = i + PaddingSize;
                const s32 y = j + PaddingSize;
                highres_data[ x + y*highres_width ] = bitmap.buffer[ i + j*bitmap.width ]/255.0;
            }
        }
        
        if( 1 )
        {
            distance_field SDF;
            xptr<double>    temp_highres_data;
            temp_highres_data.Alloc( highres_width*highres_height );
            SDF.Generate( &highres_data[0], highres_width, highres_height, &temp_highres_data[ 0 ], PaddingSize, PaddingSize, 0 );
            highres_data = temp_highres_data;
            for( auto& X : highres_data ) X = 1-X;
        }
        else
        {
            distance_map( highres_data, highres_width, highres_height );
        }
        
        
        
        // Allocate low resolution buffer
        const f32       ratio         = m_LowResSize / (f32)m_HighResSize;
        const s32       lowres_width  = (s32)round((f64) highres_width * ratio );
        const s32       lowres_height = (s32)round(highres_height * lowres_width/(f64) highres_width);
        xptr<double>    lowres_data;
        
        lowres_data.Alloc( lowres_width * lowres_height );
        lowres_data.SetMemory(0);
        
        // Scale down highres buffer into lowres buffer
        resize( highres_data, highres_width, highres_height,
                lowres_data,  lowres_width,  lowres_height );
        
        
        //
        // Create the bitmap for it
        //
        Info.m_Bitmap.CreateBitmap( lowres_width, lowres_height );
        xcolor* pData = (xcolor*)Info.m_Bitmap.getMip(0);
        for( s32 i=0; i<Info.m_Bitmap.getWidth()*Info.m_Bitmap.getHeight(); i++ )
        {
            const u8 I = (u8)x_Range( f32(1-lowres_data[i])*255, 0, 0xff);
            pData[i].Set(I,0,0,I);
        }
        
        //
        // Set all the information about this clypth
        //        
        Info.m_XOffset = ( m_pFace->glyph->bitmap_left + PaddingSize ) * ratio;
        Info.m_YOffset = ( m_pFace->glyph->bitmap_top - m_pFace->glyph->bitmap.rows + PaddingSize ) * ratio;

        Info.m_XAdvance = ratio * m_pFace->glyph->advance.x/(f32)DpiToPixels;
        Info.m_YAdvance = ratio * m_pFace->glyph->advance.y/(f32)DpiToPixels;
    }
    else
    {
        //
        // Create the bitmap for it
        //
        Info.m_Bitmap.CreateBitmap( bitmap.width, bitmap.rows  );
        xcolor* pData = (xcolor*)Info.m_Bitmap.getMip(0);
        for( s32 i=0; i<Info.m_Bitmap.getWidth()*Info.m_Bitmap.getHeight(); i++ )
        {
            const u8 I = bitmap.buffer[ i ];
            pData[i].Set(I,I,I,I);
        }
        
        Info.m_XOffset  = /*slot->bitmap_left +*/ f32(m_pFace->glyph->bitmap_left );
        Info.m_YOffset  = /*slot->bitmap_top  +*/ f32( m_pFace->glyph->bitmap_top - m_pFace->glyph->bitmap.rows );
        Info.m_XAdvance = m_pFace->glyph->advance.x/(f32)DpiToPixels;
        Info.m_YAdvance = m_pFace->glyph->advance.y/(f32)DpiToPixels;
    }
}
