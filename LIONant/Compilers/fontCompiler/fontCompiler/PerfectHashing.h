//
//  PerfectHashing.h
//  endBaseFontExample
//
//  Created by Tomas Arce on 8/31/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "x_base.h"

//
// To hash into the right place do the following:
// const s32 Row    = Key / PHTable.getCount();
// const s32 Column = Key % PHTable.getCount();
// const s32 iHash  = Column + PHTable[Row];
class buid_perfect_hashing
{
public:

    void                Compile             ( s32& HashTableSize, xptr<u16>& PHTable, const xptr<u16> Keys );
    
protected:
    
    struct row_struct
    {
        s32 m_RowNumber;                      // the row number in array A[][]
        s32 m_RowItemCnt;                     // the # of items in this row of A[][]
    };
    
    enum constants:s32
    {
        INVALID_KEY = -1,
    };
    
protected:
    
    void                InitArrays          ( void );
    void                SortRows            ( int t );
    
protected:
    
    s32                 m_tMax;
    s32                 m_HashTableMax;
    
    // the arrays A[][], r[] and C[] are those in the article "Perfect Hashing"
    xptr<s32>           m_A;           // A[i][j]=K (i=K/t, j=K mod t) for each key K
    xptr<s32>           m_R;           // r[R]=amount row A[R][] was shifted
    xptr<s32>           m_C;           // the shifted rows of A[][] collapse into C[]
    
    // Row[] exists to facilitate sorting the rows of A[][] by their "fullness"
    xptr<row_struct>    m_Row;         // entry counts for the rows in A[][]
};