//
//  main.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "compilerBase.h"
#include "fontCompilerBase.h"


int main(int argc, const char * argv[])
{
    font_compiler_base Compiler;
    
    if( Compiler.Parse( argc, argv ) )
    {
         Compiler.Compile();
    }
    
    return 0;
}
