

class distance_field
{
public:

    /// Fill a texture with a signed distance field generated from a source texture.
    /// Source -        Source texture. Alpha values of 1 are considered inside, values of 0 are considered outside, 
    ///                 and any other values are considered to be on the edge.Must be readable.
    /// Destination -   Destination texture. Must be the same size as the source texture. Must be readable.
    ///                 The texture change does not get applied automatically, you need to do that yourself.
    /// MaxInside -     Maximum pixel distance measured inside the edge, resulting in an alpha value of 1.
    ///                 If set to or below 0, everything inside will have an alpha value of 1.
    /// MaxOutside -    Maximum pixel distance measured outside the edge, resulting in an alpha value of 0.
    ///                 If set to or below 0, everything outside will have an alpha value of 0.
    /// postProcessD -  Pixel distance from the edge within which pixels will be post-processed using the edge gradient.
    void Generate(
        const f64*  pSrc,
        const s32   W,
        const s32   H,
        f64*        pDest,
        const s32   MaxInside,
        const s32   MaxOutside,
        const f32   postProcessD
    )
    {
        //
        // Init class Vars
        //
        m_Width  = W;
        m_Height = H;
        m_Data.Alloc( W*H );
        m_Data.SetMemory(0);
        
        if( MaxInside > 0 )
        {
            for( s32 y = 0; y < H; y++ ) 
            for( s32 x = 0; x < W; x++ ) 
            {
                MutablePixel( x, y ).m_Alpha = 1 - getPixel( pSrc, x, y );
            }

            ComputeEdgeGradients( );
            GenerateDistanceTransform( );
            if ( postProcessD > 0 )
            {
                PostProcess( postProcessD );
            }

            const auto scale = 1. / MaxInside;
            for ( s32 y = 0; y < m_Height; y++ )
            for ( s32 x = 0; x < m_Width; x++ ) 
            {
                auto c = Pixel( x, y ).m_Distance * scale;
                setPixel( pDest, x, y, c );
            }
        }

        if ( MaxOutside > 0 ) 
        {
            for ( s32 y = 0; y < m_Height; y++ )
            for ( s32 x = 0; x < m_Width; x++ ) 
            {
                MutablePixel( x, y ).m_Alpha = getPixel( pSrc, x, y );
            }

            ComputeEdgeGradients();
            GenerateDistanceTransform();
            if ( postProcessD > 0 )
            {
                PostProcess( postProcessD );
            }

            const auto scale = 1. / MaxOutside;
            if ( MaxOutside > 0 ) 
            {
                for ( s32 y = 0; y < m_Height; y++ )
                for ( s32 x = 0; x < m_Width;  x++ ) 
                {
                    auto p1 = getPixel( pDest, x, y );
                    setPixel( pDest, x, y, Pixel( x, y ).m_Distance * scale );

                    auto c = 0.5f + ( p1 - getPixel( pDest, x, y ) ) * 0.5f;
                    setPixel( pDest, x, y, c );
                }
            }
            else 
            {
                for( s32 y = 0; y < m_Height; y++ )
                for( s32 x = 0; x < m_Width; x++ ) 
                {
                    auto c = 1 - Pixel( x, y ).m_Distance * scale;
                    setPixel( pDest, x, y, c );
                }
            }
        }
    }

protected:

    struct pixel
    {
        f64         m_Alpha;
        f64         m_Distance;
        f64         m_GradientX;
        f64         m_GradientY;
        s32         m_DX;
        s32         m_DY;
    };

    using data = xptr<pixel>;

protected:

    pixel& MutablePixel( s32 X, s32 Y ) const
    { 
        return const_cast<pixel&>(m_Data[ X + Y * m_Width]); 
    } 

    const pixel& Pixel( s32 X, s32 Y ) const
    {
        return m_Data[ X + Y * m_Width ];
    }

    void setPixel( f64* pData, s32 X, s32 Y, f64 val ) const
    {
        val = val < 0? 0 : val;
        val = val > 1? 1 : val;
        pData[ X + Y * m_Width ] = val;
    }

    f64 getPixel( const f64* pData, s32 X, s32 Y ) const
    {
        return pData[ X + Y * m_Width ];
    }

    //---------------------------------------------------------------------------------------

    void ComputeEdgeGradients( void ) const
    {
        const f64 sqrt2 = 1.414213562373095;
        for( s32 y = 1; y < m_Height - 1; y++ )
        for( s32 x = 1; x < m_Width - 1; x++ ) 
        {
            auto& p = MutablePixel( x, y );
            if ( p.m_Alpha > 0 && p.m_Alpha < 1 )
            {
                // estimate gradient of edge pixel using surrounding pixels
                const auto g =
                    - Pixel( x - 1, y - 1 ).m_Alpha
                    - Pixel( x - 1, y + 1 ).m_Alpha
                    + Pixel( x + 1, y - 1 ).m_Alpha
                    + Pixel( x + 1, y + 1 ).m_Alpha;
                p.m_GradientX = g + ( Pixel( x + 1, y ).m_Alpha - Pixel( x - 1, y ).m_Alpha ) * sqrt2;
                p.m_GradientY = g + ( Pixel( x, y + 1 ).m_Alpha - Pixel( x, y - 1 ).m_Alpha ) * sqrt2;

                // Normalize                
                {
                    const auto d = 1/sqrt( p.m_GradientX*p.m_GradientX + p.m_GradientY*p.m_GradientY );
                    p.m_GradientX *= d;
                    p.m_GradientY *= d;
                }
            }
        }
    }

    //---------------------------------------------------------------------------------------

    // perform anti-aliased Euclidean distance transform
    void GenerateDistanceTransform( void ) const
    {
        //
        // initialize distances
        //
        for ( s32 y = 0; y < m_Height; y++ )
        for ( s32 x = 0; x < m_Width; x++ ) 
        {
            auto& p = MutablePixel( x, y );
            p.m_DX = 0;
            p.m_DY = 0;
            if ( p.m_Alpha <= 0 )
            {
                // outside
                p.m_Distance = 1000000;
            }
            else if ( p.m_Alpha < 1 )
            {
                // on the edge
                p.m_Distance = ApproximateEdgeDelta( p.m_GradientX, p.m_GradientY, p.m_Alpha );
            }
            else 
            {
                // inside
                p.m_Distance = 0;
            }
        }

        //
        // perform 8SSED (eight-points signed sequential Euclidean distance transform)
        // scan up
        //
        for( s32 y = 1; y < m_Height; y++ ) 
        {
            // |P.
            // |XX
            {
                auto& p = MutablePixel( 0, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, 0, y, 0, -1 );
                    UpdateDistance( p, 0, y, 1, -1 );
                }
            }

            // -->
            // XP.
            // XXX
            for ( s32 x = 1; x < m_Width - 1; x++ ) 
            {
                auto& p = MutablePixel( x, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, x, y, -1,  0 );
                    UpdateDistance( p, x, y, -1, -1 );
                    UpdateDistance( p, x, y,  0, -1 );
                    UpdateDistance( p, x, y,  1, -1 );
                }
            }

            // XP|
            // XX|
            {
                auto& p = MutablePixel( m_Width - 1, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, m_Width - 1, y, -1,  0 );
                    UpdateDistance( p, m_Width - 1, y, -1, -1 );
                    UpdateDistance( p, m_Width - 1, y,  0, -1 );
                }
            }

            // <--
            // .PX
            for( s32 x = m_Width - 2; x >= 0; x-- ) 
            {
                auto& p = MutablePixel( x, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, x, y, 1, 0 );
                }
            }
        }

        //
        // scan down
        //
        for( s32 y = m_Height - 2; y >= 0; y-- ) 
        {
            // XX|
            // .P|
            {
                auto& p = MutablePixel( m_Width - 1, y );
                if ( p.m_Distance > 0 )
                {
                    UpdateDistance( p, m_Width - 1, y, 0, 1 );
                    UpdateDistance( p, m_Width - 1, y, -1, 1 );
                }
            }

            // <--
            // XXX
            // .PX
            for( s32 x = m_Width - 2; x > 0; x-- ) 
            {
                auto& p = MutablePixel( x, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, x, y,  1, 0 );
                    UpdateDistance( p, x, y,  1, 1 );
                    UpdateDistance( p, x, y,  0, 1 );
                    UpdateDistance( p, x, y, -1, 1 );
                }
            }

            // |XX
            // |PX
            {
                auto& p = MutablePixel( 0, y );
                if ( p.m_Distance > 0 )
                {
                    UpdateDistance( p, 0, y, 1, 0 );
                    UpdateDistance( p, 0, y, 1, 1 );
                    UpdateDistance( p, 0, y, 0, 1 );
                }
            }

            // -->
            // XP.
            for ( s32 x = 1; x < m_Width; x++ ) 
            {
                auto& p = MutablePixel( x, y );
                if ( p.m_Distance > 0 ) 
                {
                    UpdateDistance( p, x, y, -1, 0 );
                }
            }
        }
    }

    //---------------------------------------------------------------------------------------

    f64 ApproximateEdgeDelta( f64 gx, f64 gy, const f64 a ) const
    {
        // (gx, gy) can be either the local pixel gradient or the direction to the pixel
        if ( gx == 0 || gy == 0 ) 
        {
            // linear function is correct if both gx and gy are zero
            // and still fair if only one of them is zero
            return 0.5 - a;
        }

        // normalize (gx, gy)
        {
            auto length = 1 / sqrt( gx * gx + gy * gy );
            gx *= length;
            gy *= length;
        }

        // reduce symmetrical equation to first octant only
        // gx >= 0, gy >= 0, gx >= gy
        gx = x_Abs( gx );
        gy = x_Abs( gy );
        if ( gx < gy ) 
        {
            auto temp = gx;
            gx = gy;
            gy = temp;
        }

        // compute delta
        const auto a1 = 0.5 * gy / gx;
        if ( a < a1 ) 
        {
            // 0 <= a < a1
            return 0.5 * ( gx + gy ) - sqrt( 2 * gx * gy * a );
        }

        if ( a < ( 1 - a1 ) ) 
        {
            // a1 <= a <= 1 - a1
            return ( 0.5 - a ) * gx;
        }

        // 1-a1 < a <= 1
        return -0.5 * ( gx + gy ) + sqrt( 2 * gx * gy * ( 1 - a ) );
    }

    //---------------------------------------------------------------------------------------

    void UpdateDistance( pixel& p, const s32 x, const s32 y, const s32 oX, const s32 oY ) const
    {
        const auto& neighbor = Pixel( x + oX, y + oY );
        const auto& closest  = Pixel( x + oX - neighbor.m_DX, y + oY - neighbor.m_DY );

        if( closest.m_Alpha == 0 || &closest == &p ) 
        {
            // neighbor has no closest yet
            // or neighbor's closest is p itself
            return;
        }

        const s32   dX       = neighbor.m_DX - oX;
        const s32   dY       = neighbor.m_DY - oY;
        const auto  distance = sqrt( dX * dX + dY * dY ) + ApproximateEdgeDelta( dX, dY, closest.m_Alpha );
        if ( distance < p.m_Distance ) 
        {
            p.m_Distance = distance;
            p.m_DX = dX;
            p.m_DY = dY;
        }
    }

    //---------------------------------------------------------------------------------------

    void PostProcess( float maxDistance )
    {
        // adjust distances near edges based on the local edge gradient
        for ( s32 y = 0; y < m_Height; y++ ) 
        for ( s32 x = 0; x < m_Width; x++ ) 
        {
            auto& p = MutablePixel( x, y );
            if ( ( p.m_DX == 0 && p.m_DY == 0 ) || p.m_Distance >= maxDistance ) 
            {
                // ignore edge, inside, and beyond max distance
                continue;
            }

            const auto& closest = Pixel( x - p.m_DX, y - p.m_DY );
            const auto  gx      = closest.m_GradientX;
            const auto  gy      = closest.m_GradientY;

            if ( gx == 0 && gy == 0 ) 
            {
                // ignore unknown gradients (inside)
                continue;
            }

            // compute hit point offset on gradient inside pixel
            const auto dX   = p.m_DX;
            const auto dY   = p.m_DY;
            const auto df   = ApproximateEdgeDelta( gx, gy, closest.m_Alpha );
            const auto t    =  dY * gx - dX * gy;
            const auto u    = -df * gx +  t * gy;
            const auto v    = -df * gy -  t * gx;

            // use hit point to compute distance
            if ( x_Abs( u ) <= 0.5f && x_Abs( v ) <= 0.5 ) 
            {
                p.m_Distance = sqrt( ( dX + u ) * ( dX + u ) + ( dY + v ) * ( dY + v ) );
            }
        }
    }

protected:

    s32      m_Width;
    s32      m_Height;
    data     m_Data;
};