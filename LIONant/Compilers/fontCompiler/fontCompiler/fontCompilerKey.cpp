//
//  fontCompilerKeyObject.cpp
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "fontCompilerKey.h"

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// entry_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void font_compiler_key::entry_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM_FILE( "FreeTypeFileName",         OVERRIDE_FONT_FILEPATH,                     "File name of the source True Type font that needs compiling.", "*.tff" )
    KEY_PROP_ENUM_FILE( "UnicodeFileName",          OVERRIDE_TEXT_FILEPATH,                     "File name of the source Unicode text file which all the characters we need to compile.", "*.txt" )
    KEY_PROP_ENUM( "FontSize",                      OVERRIDE_FONTSIZE,          g_PropInt,      "The Size of glyphs of the final font." )
}

//-------------------------------------------------------------------------------------------------

xbool font_compiler_key::entry_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "FreeTypeFileName",     OVERRIDE_FONT_FILEPATH,         g_PropFilePath.Query( Query, m_SrcFontFilePath )        )                
    KEY_PROP_QUERY( "UnicodeFileName",      OVERRIDE_TEXT_FILEPATH,         g_PropFilePath.Query( Query, m_UnicodeTextFilePath )    )                
    KEY_PROP_QUERY( "FontSize",             OVERRIDE_FONTSIZE,              g_PropInt.Query( Query, m_BaseFontSize, 1, 500)         )                
    
    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void font_compiler_key::entry_params::onUpdateFromSrc( const entry_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_SrcFontFilePath,      OVERRIDE_FONT_FILEPATH,    .Copy  )                
    KEY_PROP_UPDATE( m_UnicodeTextFilePath,  OVERRIDE_TEXT_FILEPATH,    .Copy  )                
    KEY_PROP_UPDATE( m_BaseFontSize,         OVERRIDE_FONTSIZE,         =      )     
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// main_params
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

void font_compiler_key::main_params::onPropEnum( xproperty_enum& Enum, s32 iScope, u64 Mask ) const
{
    KEY_PROP_ENUM( "bDistanceField",        OVERRIDE_MAIN_DISTANCE_FIELD,             g_PropBool,      "Compiled the fonts using a distance field rather than just a bitmap. Note distance field only works for non-color fonts." )
    KEY_PROP_ENUM( "CompressionLevel",      OVERRIDE_MAIN_COMPRESSION_LEVEL,          g_PropFloat,       "Compression level to be pass to the texture compiler." )
}

//-------------------------------------------------------------------------------------------------

xbool font_compiler_key::main_params::onPropQuery( xproperty_query& Query )
{
    KEY_PROP_QUERY( "bDistanceField",       OVERRIDE_MAIN_DISTANCE_FIELD,           g_PropBool.Query( Query, m_bDistanceField )             )                
    KEY_PROP_QUERY( "CompressionLevel",     OVERRIDE_MAIN_COMPRESSION_LEVEL,        g_PropFloat.Query( Query,  m_CompressionLevel, 0, 1)    )                
   
    return FALSE;
}

//-------------------------------------------------------------------------------------------------

void font_compiler_key::main_params::onUpdateFromSrc( const main_params& Src, const u64 Masks )
{
    KEY_PROP_UPDATE( m_bDistanceField,      OVERRIDE_MAIN_DISTANCE_FIELD,           =      )     
    KEY_PROP_UPDATE( m_CompressionLevel,    OVERRIDE_MAIN_COMPRESSION_LEVEL,        =      )     
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
/*
void font_compiler_key::getAssetDependencies( xarray<dependency>& List )
{
    //
    // Collect FONT_FILEPATH
    //
    for( s32 i=0; i<m_EntryList.getCount(); i++ )
    {
        // If the user over wrote the All the Platforms then it is easy just get it from there
        if( x_FlagIsOn( m_EntryList[i].m_Platform[0].m_OverrideBits, OVERRIDE_FONT_FILEPATH ) )
        {
            dependency&         Dep = List.append();
            
            // Copy the dependency out
            Dep.m_FilePath = m_EntryList[i].m_Platform[0].m_SrcFontFilePath;
            
            // All the platforms is the same input (at least for right now)
            Dep.m_Plaforms = ~0;
            continue;
        }
        
        // If not we are going to have to add each dependency base on if it exist in the given platform
        s32 iBase = List.getCount();
        for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
        {
            const base_params&  Desc = m_EntryList[i].m_Platform[p];
            
            if( x_FlagIsOn( Desc.m_OverrideBits, OVERRIDE_FONT_FILEPATH ) == FALSE )
                continue;
            
            xbool bFound = FALSE;
            for( s32 k=iBase; k<List.getCount(); k++ )
            {
                dependency&     Dep = List[k];
                
                if( Dep.m_FilePath == Desc.m_SrcFontFilePath )
                {
                    bFound = TRUE;
                    Dep.m_Plaforms |= p;
                    break;
                }
            }
            
            if( bFound == FALSE )
            {
                dependency&         Dep = List.append();
                
                // Copy the dependency out
                Dep.m_FilePath = Desc.m_SrcFontFilePath;
                
                // All the platforms is the same input (at least for right now)
                Dep.m_Plaforms = p;
            }
        }
    }
    
    //
    // Collect FONT_FILEPATH
    //
    for( s32 i=0; i<m_EntryList.getCount(); i++ )
    {
        // If the user over wrote the All the Platforms then it is easy just get it from there
        if( x_FlagIsOn( m_EntryList[i].m_Platform[0].m_OverrideBits, OVERRIDE_TEXT_FILEPATH ) )
        {
            dependency&         Dep = List.append();
            
            // Copy the dependency out
            Dep.m_FilePath = m_EntryList[i].m_Platform[0].m_UnicodeTextFilePath;
            
            // All the platforms is the same input (at least for right now)
            Dep.m_Plaforms = ~0;
            continue;
        }
        
        // If not we are going to have to add each dependency base on if it exist in the given platform
        s32 iBase = List.getCount();
        for( s32 p=1; p<X_PLATFORM_COUNT; p++ )
        {
            const base_params&  Desc = m_EntryList[i].m_Platform[p];
            
            if( x_FlagIsOn( Desc.m_OverrideBits, OVERRIDE_TEXT_FILEPATH ) == FALSE )
                continue;
            
            xbool bFound = FALSE;
            for( s32 k=iBase; k<List.getCount(); k++ )
            {
                dependency&     Dep = List[k];
                
                if( Dep.m_FilePath == Desc.m_UnicodeTextFilePath )
                {
                    bFound = TRUE;
                    Dep.m_Plaforms |= p;
                    break;
                }
            }
            
            if( bFound == FALSE )
            {
                dependency&         Dep = List.append();
                
                // Copy the dependency out
                Dep.m_FilePath = Desc.m_UnicodeTextFilePath;
                
                // All the platforms is the same input (at least for right now)
                Dep.m_Plaforms = p;
            }
        }
    }
    
}

*/