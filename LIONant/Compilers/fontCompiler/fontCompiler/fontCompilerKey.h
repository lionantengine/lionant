//
//  fontCompilerKeyObject.h
//  fontCompiler
//
//  Created by Tomas Arce on 9/28/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#ifndef FONT_COMPILER_KEY_OBJECT_H
#define FONT_COMPILER_KEY_OBJECT_H

#include "compilerbase.h"

class font_compiler_key : public compiler_key_object
{
public:

    enum mode
    {
        MODE_TRUETYPE,          // Support shart/scalable fonts
        MODE_SRC_BITMAP         // Support full color fonts
    };
    
    struct main_params : main_params_base_link<main_params>
    {
        enum
        {
            OVERRIDE_MAIN_DISTANCE_FIELD    = X_BIT(0),
            OVERRIDE_MAIN_COMPRESSION_LEVEL = X_BIT(1),
        
            SINGLE_COMPILATION_MAIN_FLAGS   = OVERRIDE_MAIN_DISTANCE_FIELD,
        };

        virtual void        onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const;
        virtual xbool       onPropQuery         ( xproperty_query& Query );
        void                onUpdateFromSrc     ( const main_params& Src, const u64 Masks );
        
        //        mode        m_Mode;                               // Mode of the font pipeline
        xbool       m_bDistanceField       =    TRUE;         // If we should render this font with distance field
        f32         m_CompressionLevel     =    0;            // Compression level to be past to the texture compiler
    };
    
    struct entry_params : entry_params_base_link<entry_params>
    {
        enum param_flags
        {
            OVERRIDE_FONT_FILEPATH          = X_BIT(0),
            OVERRIDE_TEXT_FILEPATH          = X_BIT(1),
            OVERRIDE_FONTSIZE               = X_BIT(2),
        
            SINGLE_COMPILATION_FLAGS        =   OVERRIDE_FONT_FILEPATH  |
                                                OVERRIDE_TEXT_FILEPATH  |
                                                OVERRIDE_FONTSIZE,
        };

        virtual void        onPropEnum          ( xproperty_enum& Enum, s32 iScope, u64 Mask ) const override final;
        virtual xbool       onPropQuery         ( xproperty_query& Query )override final;
        void                onUpdateFromSrc     ( const entry_params& Src, const u64 Masks );
        
        xstring     m_SrcFontFilePath       =   X_STR("");    // The font source file
        xstring     m_UnicodeTextFilePath   =   X_STR("");    // A text file containing all the characters that the user wish to use at run time
        s32         m_BaseFontSize          =   32;           // How big the font should be
    };
    
    
public:
    
                                font_compiler_key           ( const compiler_base& Base ) : compiler_key_object( Base ){}
    virtual const char*         getCompilerName             ( void ) const { return "FontCompiler"; }
    virtual const char*         getCompiledExt              ( void ) const { return "font";         }

protected:
    
    KEY_STANDARD_STUFF
    
protected:
    
    friend class font_compiler_base;
};


#endif
