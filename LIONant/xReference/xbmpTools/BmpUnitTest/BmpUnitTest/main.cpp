//
//  main.cpp
//  BmpUnitTest
//
//  Created by Tomas Arce on 9/15/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <iostream>
#include "xbmp_tools.h"

int main(int argc, const char * argv[])
{
    const char* pFileName[]={
        "LIONant_LOGO.bmp",
        "LIONant_LOGO.tga",
        "LIONant_LOGO.psd",
        "LIONant_LOGO.png"
    };
    
    xsafe_array<xbitmap,sizeof(pFileName)/sizeof(char*)>  Bitmap;

    //
    // First test is whether we can load them at all
    //
    for( s32 i=0; i<Bitmap.getCount(); i++ )
    {
        xbmp_Load(  Bitmap[i], pFileName[i] );
    }
    
    
    
    
    if( xbmp_Load( Bitmap[0], "EnvMapV2.png" ) )
    {
        //xbmp_Load( Bitmap[0], "EnvMapV2.png" );
        
        Bitmap[0].Save( "EnvMapV2.xbmp" );
    }
    
    return 0;
}

