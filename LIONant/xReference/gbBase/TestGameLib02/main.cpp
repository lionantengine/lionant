//
//  main.cpp
//  TestGameLib
//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
// This demo introduces the concept of rendering and loading resources.
// Simple sprite rendering...
#include "gb_Base.h"

/////////////////////////////////////////////////////////////////////////////////
// Basic render manager
/////////////////////////////////////////////////////////////////////////////////
static eng_sprite_render_2dgroup s_SpriteGroup;

struct my_sprite
{
    void Init( eng_sprite_rsc::ref& Ref, const char* pString )
    {
        m_Z = x_frand();
        s32 XRes, YRes;
        eng_GetCurrentContext().GetScreenResolution( XRes, YRes );
        m_Pos.Set( x_frand( 40, XRes-100 ), x_frand( 40, YRes-100 ) );
        m_Sprite.setup( Ref, pString );
    }
    
    f32         m_Z;
    xvector2    m_Pos;
    eng_sprite  m_Sprite;
};

static xsafe_array<my_sprite,40>   s_Sprite;

class gb_render_mgr : public gb_default_component_mgr
{
public:
    GB_MANAGER_RTTI(gb_render_mgr, gb_default_component_mgr);
    
    gb_render_mgr( const char* pName ) : gb_default_component_mgr(pName) { setupAffinity( x_base_job::AFFINITY_MAIN_THREAD ); }
    
protected:
    
    virtual void onUpdate( void )
    {
        eng_context& DisplayContext = eng_GetCurrentContext();
        
        x_printfxy( 1,2,"THIS IS A TEST" );
        
        //
        // Render the sprites
        //
        DisplayContext.Begin( "Render the sprites" );
        {
            s_SpriteGroup.Begin();
            {
                for( auto& Entry:s_Sprite )
                {
                    xmatrix4 Matrix;
                    
                    Matrix.Identity();
                    Matrix.setTranslation( xvector3( Entry.m_Pos.m_X, Entry.m_Pos.m_Y, Entry.m_Z) );
                    s_SpriteGroup.qtAddSprite( Entry.m_Sprite, Matrix, xcolor(~0) );
                }
            }
            s_SpriteGroup.Flush();
        }
        DisplayContext.End();
    
        //
        // rendering jobs hapens inside the pageflip as a serial work.
        //
        DisplayContext.PageFlip( FALSE );
    }
};

/////////////////////////////////////////////////////////////////////////////////
// Main functions
/////////////////////////////////////////////////////////////////////////////////

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

static gb_cloud_state   s_GameWorld;
static gb_sync_point    s_GameWorld_Input;
static gb_sync_point    s_GameWorld_Render;
static gb_render_mgr    g_RenderMgr("RenderMgr");

//-------------------------------------------------------------------------------

void Initialize( void )
{
    xguid gMyBlueprint;
    xguid gMyBulletBlueprint;
    
    //----------------------------------------------------------------------------
    // Initialize the engine
    //----------------------------------------------------------------------------
    eng_Init();
    g_Scheduler.Init( 2 );
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);
    
    //----------------------------------------------------------------------------
    // Setup a sprite
    //----------------------------------------------------------------------------
    {
        const static u64 SPRITE_ATLAS = X_GUID( 1ui1_jzcw_tukx );

        eng_sprite_rsc::ref Atlast;
        Atlast.setup( SPRITE_ATLAS );
        
        s32 i=1;
        for( auto& Entry:s_Sprite )
        {
            Entry.Init( Atlast, xfs("%03d", i++) );
        }
    }
    
    //
    // Create our sprite group
    //
    s_SpriteGroup.Init( 100, 1, 0, 100, TRUE );
    
    
    //----------------------------------------------------------------------------
    // Now deal with the game system
    //----------------------------------------------------------------------------

    s_GameWorld.setupBeging     ( "Game World" );
    s_GameWorld_Input.setup     ( "Input", s_GameWorld,
                                 x_va_list( (xuptr)&s_GameWorld.getStartSyncP() ),
                                 x_va_list( (xuptr)&g_InputMgr )   );
    s_GameWorld_Render.setup    ( "Rendering", s_GameWorld,
                                 x_va_list( (xuptr)&s_GameWorld_Input ),
                                 x_va_list( (xuptr)&g_RenderMgr )  );
    s_GameWorld.setupEnd();
    
    //----------------------------------------------------------------------------
    // Initialize the game
    //----------------------------------------------------------------------------
    
    // TODO: This should be put in a nicer more intuitive function somewhere
    gb_base_mgr::InitAll();

    //
    // Get the game manager to warm up a bit
    //
    g_StateMgr.setActiveState(s_GameWorld);
    g_GameMgr.msgStart();
}

//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    Initialize();
    
    // By doing this the main thread basically becomes a worker
    while( 1 )
    {
        g_Scheduler.MainThreadRunJobs( TRUE );
    }
}