//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "PhysicsComponent.h"
#include "PhysicsMgr.h"

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

physics_mgr                       g_PhysicsMgr("PhysicsMgr");

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS ISLAND FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS MANAGER FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

physics_mgr::physics_mgr( const char* pName ) : 
    gb_base_component_mgr(pName),
    m_pWorld( NULL )
{
}
    
//-------------------------------------------------------------------------------

physics_mgr::~physics_mgr( void )
{
    if( m_pWorld ) 
    {
        x_delete( m_pWorld );
        m_pWorld = NULL;
    }
}

//-------------------------------------------------------------------------------

struct my_b2ContactListener : public b2ContactListener
{
	virtual void BeginContact(b2Contact* contact) 
    { 
        physics_component* pComA = ((physics_component*)contact->GetFixtureA()->GetUserData());
        if( pComA==NULL ) 
            return;

        physics_component* pComB = ((physics_component*)contact->GetFixtureB()->GetUserData());
        if( pComB==NULL ) 
            return;
        /*

        if( pComA->isDeleted() ) 
            return;

        if( pComB->isDeleted() ) 
            return;

        if( pComA->m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ) == FALSE )
            return;

        if( pComB->m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ) == FALSE )
            return;
*/

        if( pComA->m_bHandleCollisions==FALSE )
        {
            x_Swap( pComA, pComB );
        }

        if( pComA->m_bHandleCollisions==FALSE )
        {
            return;
        }

        pComA->HandleCollisions( pComB, contact );
    }

} s_ContactListener;

//-------------------------------------------------------------------------------

void physics_mgr::vInit( void )
{
    gb_base_component_mgr::vInit();
    
    s32 W, H;
    eng_GetCurrentContext().GetScreenResolution( W, H );
    m_Bounds.Set(0.f, 0.f, (f32)W, (f32)H );

    //
    // Create the world in box 2d
    // 
    m_pWorld = x_new( b2World, 1, 0 );

    // Make sure that we handle collisions ourselves
    m_pWorld->SetContactListener( &s_ContactListener );

    //
    // Create the bounds of the world
    //
	{
		b2BodyDef bd;
		b2Body* ground = m_pWorld->CreateBody(&bd);

		b2EdgeShape shape;
        
		// Floor
        shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(m_Bounds.GetWidth(), 0.0f));
		ground->CreateFixture(&shape, 0.0f);

		// Left wall
        shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(0.0f, m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);

		// Right wall
		shape.Set(b2Vec2(m_Bounds.GetWidth(), 0.0f), b2Vec2(m_Bounds.GetWidth(), m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);

		// Roof
		shape.Set(b2Vec2(0.0f, m_Bounds.GetHeight()), b2Vec2(m_Bounds.GetWidth(), m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);
        
	}
}

//-------------------------------------------------------------------------------

void physics_mgr::vKill( void )
{
}

//-------------------------------------------------------------------------------

void physics_mgr::onUpdate( void )
{
    ASSERT( g_order == 1 ); g_order = 3;

    f32 DeltaTime   = g_GameMgr.getDeltaTime();

    // 
    // Simulate the Box 2d World
    //
	m_pWorld->Step( DeltaTime, 1, 1 );
}

//-------------------------------------------------------------------------------

struct my_b2QueryCallback : public b2QueryCallback
{
    gb_component*       m_pSelf;
    gb_component*       m_pClosest;
    gb_component_type*  m_pTypeFilter;
    f32                 m_DistanceSquare;
    b2Vec2              m_Pos;

         my_b2QueryCallback ( void ) : m_pClosest( NULL ), m_pTypeFilter( NULL ) {};

    bool Filter             ( gb_component* pComp )
    {
        if( pComp == NULL )
            return true;

        if( pComp == m_pSelf )
            return true;

        if( pComp->isDeleted() ) 
            return true;

        if( m_pTypeFilter && m_pTypeFilter == &pComp->getType() )
            return true;
        
        return false;
    }

	bool ReportFixture      ( b2Fixture* fixture )
    {
        if( m_pClosest == NULL )
        {
            gb_component* pComp = (gb_component*)fixture->GetUserData();

            if( Filter( pComp ) )
                return true;

            m_pClosest       = (gb_component*)fixture->GetUserData();
            m_DistanceSquare = (m_Pos - fixture->GetBody()->GetPosition()).LengthSquared();
        }
        else
        {
            f32 dd = (m_Pos - fixture->GetBody()->GetPosition()).LengthSquared();

            if( dd < m_DistanceSquare ) 
            {
                gb_component* pComp = (gb_component*)fixture->GetUserData();

                if( Filter( pComp ) )
                    return true;

                m_pClosest       = pComp;
                m_DistanceSquare = dd;
            }
        }

        return true;
    }
};

//-------------------------------------------------------------------------------

struct closest_components : public x_simple_job<1,x_light_job>
{
    virtual void            onRun           ( void );
    xarray<gb_component*>*  m_pArray;
    f32                     m_R;
    my_b2QueryCallback      m_MyCallBack;
};

//-------------------------------------------------------------------------------

void closest_components::onRun( void )
{
    xrect Bounds(m_MyCallBack.m_Pos.x-m_R,
                 m_MyCallBack.m_Pos.y-m_R,
                 m_MyCallBack.m_Pos.x+m_R,
                 m_MyCallBack.m_Pos.y+m_R);

    b2AABB aabb;
    
    // Create the bbox for the components
    aabb.lowerBound.Set( Bounds.m_Left,  Bounds.m_Top );
    aabb.upperBound.Set( Bounds.m_Right, Bounds.m_Bottom);

    // Query the world for overlapping shapes.
    b2World&            World = g_PhysicsMgr.getWorld();
    World.QueryAABB( &m_MyCallBack, aabb );

    if( m_MyCallBack.m_pClosest ) m_pArray->append() = m_MyCallBack.m_pClosest;
}


x_light_job* physics_mgr::ClosestComponents( xarray<gb_component*>& lComp, const xvector2& Point, f32 Radious, gb_component* pSelf, gb_component_type* pTypeFilter )
{
    closest_components* pClosest = x_new(closest_components, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pClosest);

    pClosest->setupAutoDelete();
    pClosest->m_pArray      = &lComp;
    pClosest->m_R           = Radious;
    pClosest->m_MyCallBack.m_Pos.Set( Point.m_X, Point.m_Y );
    pClosest->m_MyCallBack.m_pTypeFilter = pTypeFilter;
    pClosest->m_MyCallBack.m_pSelf = pSelf;

    return pClosest;
}
