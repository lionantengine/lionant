//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "PhysicsComponent.h"
#include "PhysicsMgr.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Physics Component
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE( physics_component, gb_component_type::FLAGS_REALTIME_DYNAMIC )

//-------------------------------------------------------------------------------

physics_component::physics_component( void )
{   
    m_bHandleCollisions = FALSE;
    m_pBody             = NULL;
}

//-------------------------------------------------------------------------------

physics_component::~physics_component( void )
{
    if( m_pBody ) 
    {
        b2World& World = g_PhysicsMgr.getWorld();

        World.DestroyBody( m_pBody );

        m_pBody = NULL;
    }
}

//-------------------------------------------------------------------------------

void physics_component::vInWorld( void )
{
    //
    // Initialize some values
    //
    xvector2 Pos;
    Pos = m_PreWorldPos;

    xvector2 Vel;
    Vel = m_PreWorldVel;

    //
    // Initialize the object for Box2D
    //
    b2World&        World = g_PhysicsMgr.getWorld();    

    b2CircleShape   Shape;
    Shape.m_p.SetZero();
	Shape.m_radius = 1.0f;

	b2FixtureDef fd;
    fd.userData     = this;
	fd.shape        = &Shape;
	fd.density      = 1.0f;
	fd.friction     = 0.0f;
    fd.restitution  = 1.0f;

	b2BodyDef bd;
	bd.type     = b2_dynamicBody;
	bd.position.Set( Pos.m_X, Pos.m_Y );
    bd.linearVelocity.Set( Vel.m_X, Vel.m_Y );

	m_pBody = World.CreateBody(&bd);

	m_pBody->CreateFixture(&fd);
}

//-------------------------------------------------------------------------------

void physics_component::vSetup( void )
{
    //
    // Replicate public data 
    //
    const xrect& Bounds = g_PhysicsMgr.getBounds();

    //
    // Initialize some values
    //
    m_PreWorldPos.m_X = f32(x_rand() % (s32)Bounds.GetWidth()  + Bounds.m_Left);
    m_PreWorldPos.m_Y = f32(x_rand() % (s32)Bounds.GetHeight() + Bounds.m_Top);

    m_PreWorldVel.m_X = f32(x_rand() % 9 - 4)*6.0f;    // per millisecond
    m_PreWorldVel.m_Y = f32(x_rand() % 9 - 4)*6.0f;

    //TODO: This should be call by a manager?
    SetupContexTransferC02C1();
}

//-------------------------------------------------------------------------------

void physics_component::setupPosition( const xvector2& Pos )
{
    m_PreWorldPos = Pos;
}

//-------------------------------------------------------------------------------
void physics_component::setupVelocity( const xvector2& Vel )
{
    m_PreWorldVel = Vel;
}

//-------------------------------------------------------------------------------

void physics_component::vLoadComponent( xtextfile& File )
{
    vSetup();
}

//-------------------------------------------------------------------------------

void physics_component::onUpdate( const gb_msg& aMsg )
{
//    context&                        WContext = getWriteContextT1();
//    const physics_onupdate_msg&     Msg      = physics_onupdate_msg::SafeCast(aMsg);

//    WContext.m_Pos = Msg.m_NewPos;
//    WContext.m_Vel = Msg.m_NewVel;
}

//-------------------------------------------------------------------------------

void physics_component::setVel( const xvector2& Vel )
{
    m_pBody->SetLinearVelocity( b2Vec2( Vel.m_X, Vel.m_Y ) );
}

//-------------------------------------------------------------------------------

void physics_component::setPos( const xvector2& Pos )
{
//    if(m_pBody)
    {
        m_pBody->SetTransform( b2Vec2( Pos.m_X, Pos.m_Y ), 0 );
    }
}

/*
//------------------------------------------------------------------------------

void physics_component::compileProperty( xtextfile& File )
{
    gb_component::compileProperty(File);

    File.writeField("Dependency:d", x_va_list(-1));
    File.writeField("Position:ff", x_va_list(readContext().m_Pos.m_X, readContext().m_Pos.m_Y));
    File.writeField("Velocity:ff", x_va_list(readContext().m_Vel.m_X, readContext().m_Vel.m_Y));
}

//------------------------------------------------------------------------------

void physics_component::loadProperty( xtextfile& File )
{
    gb_component::loadProperty(File);

    context& Data = writeContext();

    // Load the default data
    File.readField("Dependency:d", x_va_r_list());
    File.readField("Position:ff", x_va_r_list(&Data.m_Pos.m_X, &Data.m_Pos.m_Y));
    File.readField("Velocity:ff", x_va_r_list(&Data.m_Vel.m_X, &Data.m_Vel.m_Y));

    //
    // Random value in our case
    //
    const xrect& Bounds = g_PhysicsMgr.getBounds();

    Data.m_Pos.m_X = f32(x_rand() % (s32)Bounds.getWidth() + Bounds.mLeft);
    Data.m_Pos.m_Y = f32(x_rand() % (s32)Bounds.getHeight() + Bounds.mBottom);

    Data.m_Vel.m_X = f32(x_rand() % 9 - 4)*6.0f;    // per millisecond
    Data.m_Vel.m_Y = f32(x_rand() % 9 - 4)*6.0f;

    //
    // Replicate public data 
    //
    ((context*)&readContext())->TransferData( &Data );
}
*/

//===============================================================================
//===============================================================================
// physics_component::context
//===============================================================================
//===============================================================================

void physics_component::context::vTransferData( const gb_component::context& pSrc )
{
    //const context& obj = *((const context*)&pSrc);
}

