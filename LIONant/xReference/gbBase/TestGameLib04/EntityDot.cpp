//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "EntityDot.h"
#include "BulletPhysics.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ENTITY DOT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE(entity_dot, gb_component_type::FLAGS_REALTIME_DYNAMIC)


//-------------------------------------------------------------------------------

void entity_dot::presets::vLoad( xtextfile& File )
{
    File.ReadField("gBulletBP:g", &m_gBullet );
}

//-------------------------------------------------------------------------------

void entity_dot::presets::vSave( xtextfile& File )
{
    File.WriteField("gBulletBP:g", m_gBullet );
}

//-------------------------------------------------------------------------------
entity_dot::entity_dot( void )
{
    m_Timer = x_frand(0.0f,20.0f);
}

//-------------------------------------------------------------------------------

void entity_dot::vResolveDependencies( void ) 
{
    m_pPhysics = &physics_component::SafeCast( *(physics_component*)findInterface( physics_component::getRTTI() ));
    ASSERT(m_pPhysics);
}

//-------------------------------------------------------------------------------

void entity_dot::vSaveComponent( xtextfile& File )
{
    gb_entity::vSaveComponent( File );
}

//-------------------------------------------------------------------------------

void entity_dot::vLoadComponent( xtextfile& File )
{
    gb_entity::vLoadComponent( File );
}

//-------------------------------------------------------------------------------

void entity_dot::vUpdate( void )
{
    //
    // We can only shot after sometime
    //
    m_Timer += g_GameMgr.getDeltaTime();
    if( m_Timer < 20.0f ) 
    {
        gb_entity::vUpdate();
        return;
    }

    xvector2                MyPos = m_pPhysics->getPos(1);
    xarray<gb_component*>   List;
    
    //
    // Collect the other ships
    //
    x_light_trigger     Trig;
    x_light_job*        pJob = g_PhysicsMgr.ClosestComponents( List, MyPos, 50.0f, m_pPhysics, &getType() ); 

    // Make the job depend on our trigger
    pJob->AndThenRuns(Trig);

    // Start processing the job
    g_Scheduler.StartLightJobChain( *pJob );

    //
    // Handle all messages
    //
    gb_entity::vUpdate();

    //
    // Ok get the list now
    //
    Trig.LocalSync();

    // We do not have anyone close to us
    if( List.getCount() == 0 )
        return;

    //
    // Find the closest dot from me.
    //
    physics_component&                  EnemyPhysics = physics_component::SafeCast( *List[0] );
    xvector2                            Direction;
    Direction = (EnemyPhysics.getVel(1) + EnemyPhysics.getPos(1)) - MyPos ;
    Direction.Normalize();

    //
    // Create a bullet
    //
	
    xarray<gb_entity*>  Entities;
    xtransform          Location;

    // create the bullet
	const presets&  Presets = getPresets();
    g_BlueprintMgr.createEntities( Entities, Presets.m_gBullet, Location );

    // set the bullet position and velocity
    bullet_physics& Bullet = bullet_physics::SafeCast( Entities[0]->getComponent( bullet_physics::s_Type ) );

    Bullet.setupPosition( MyPos );
    Bullet.setupVelocity( Direction * 60.0f );
    Bullet.setupParent  ( getGuid() );

    // Inser the bullet into the world
    g_EntityMgr.msgAppendEntity( *Entities[0] );

    // Okay we shot our first shot
    m_Timer = 0;
}

