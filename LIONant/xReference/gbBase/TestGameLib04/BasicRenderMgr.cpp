//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "BasicRenderMgr.h"
#include "PhysicsMgr.h"


//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////

basic_render_mgr        g_RenderMgr("RenderMgr");

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// RENDER COMPONENT
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE( render_component, gb_component_type::FLAGS_REALTIME_DYNAMIC )

//-------------------------------------------------------------------------------

void render_component::presets::vLoad( xtextfile& File )
{
    File.ReadField("Shape:h", &m_Shape );
}

//-------------------------------------------------------------------------------

void render_component::presets::vSave( xtextfile& File )
{
    File.WriteField("Shape:h", m_Shape );
}

//-------------------------------------------------------------------------------

void render_component::vResolveDependencies( void )
{
    void* pInterface = findInterface(i_2d_transform::getRTTI());
    ASSERT(pInterface);
    m_pTransform = &i_2d_transform::SafeCast( *((i_2d_transform*)pInterface) );
}

//-------------------------------------------------------------------------------

void render_component::onUpdate( const gb_msg& Msg )
{
    const presets&  Presets = getPresets();

    xvector2  Pos = m_pTransform->getPos(1);
    xvector2  Vel = m_pTransform->getVel(1);
    const f32 s   = 4/2.0f; 

    eng_context& Context = eng_GetCurrentContext();
    eng_draw&    Draw    = Context.getDraw();
    
    if( Presets.m_Shape == SHAPE_TRIANGLE )
    {
        if( Vel.GetLengthSquared() < 0.0001f )
        {
            Vel.m_X = 0;
            Vel.m_Y = 1;
        }
        else
        {
            Vel.Normalize();
        }

        xvector2 V1 = Vel * s*4;
        xvector2 V2 = Vel * s;
        V2.Rotate( PI/2 );

        draw_vertex* pVertex;
        u16*         pIndex;
        
        Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_ZBUFFER_OFF );
        Draw.ClearL2W();
        Draw.ClearTexture();
        Draw.GetBuffers( &pVertex, 3, &pIndex, 3 );
        
        pVertex[0].setup( xvector3d( Pos.m_X + V1.m_X, Pos.m_Y + V1.m_Y, 0), xcolor( xvector3d(0.7f, 0.7f, 0.7f) ) );
        pVertex[1].setup( xvector3d( Pos.m_X + V2.m_X, Pos.m_Y + V2.m_Y, 0), xcolor( xvector3d(0.7f, 0.7f, 0.7f) ) );
        pVertex[2].setup( xvector3d( Pos.m_X - V2.m_X, Pos.m_Y - V2.m_Y, 0), xcolor( xvector3d(0.7f, 0.7f, 0.7f) ) );
        
        pIndex[0] = 0;
        pIndex[1] = 1;
        pIndex[2] = 2;
        
        Draw.DrawBufferTriangles();
        Draw.End();
    }
    else if( Presets.m_Shape == SHAPE_SQUARE )
    {
        draw_vertex* pVertex;
        u16*         pIndex;
        
        Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_ZBUFFER_OFF );
        Draw.ClearL2W();
        Draw.ClearTexture();
        Draw.GetBuffers( &pVertex, 4, &pIndex, 6 );
        
        pVertex[0].setup( xvector3d( Pos.m_X-s, Pos.m_Y-s, 0), xcolor( xvector3d(0.2f, 0.9f, 0.2f) ) );
        pVertex[1].setup( xvector3d( Pos.m_X+s, Pos.m_Y-s, 0), xcolor( xvector3d(0.2f, 0.9f, 0.2f) ) );
        pVertex[2].setup( xvector3d( Pos.m_X+s, Pos.m_Y+s, 0), xcolor( xvector3d(0.2f, 0.9f, 0.2f) ) );
        pVertex[3].setup( xvector3d( Pos.m_X-s, Pos.m_Y+s, 0), xcolor( xvector3d(0.2f, 0.9f, 0.2f) ) );
        
        pIndex[0] = 0;
        pIndex[1] = 1;
        pIndex[2] = 2;
        
        pIndex[3] = 0;
        pIndex[4] = 2;
        pIndex[5] = 3;
        
        Draw.DrawBufferTriangles();
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void render_component::vLoadComponent( xtextfile& File )
{
    m_CurrentBuffer = 0;
    gb_component::vLoadComponent(File);
}

//-------------------------------------------------------------------------------

void render_component::vSaveComponent( xtextfile& File )
{
    gb_component::vSaveComponent(File);
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// BASIC RENDER COMPONENT
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------

void basic_render_mgr::draw_all::onRunRenderJob( void ) const
{
    basic_render_mgr& This = *m_pThis;
    
    // Update the time for the message
    m_Msg.m_DeltaTime = g_GameMgr.getDeltaTime();
    
    //
    // Render all the instances
    //
    eng_GetCurrentContext().Begin( "DrawAll" );
    for( s32 i=0; i<This.m_lActiveType.getCount(); i++ )
    {
        s32                     iType       = This.m_lActiveType[i];
        xharray<gb_component*>& CompList    = g_ComponentTypeMgr.GetComponentList(iType);
        s32                     nComponents = g_ComponentTypeMgr.GetActiveCount(iType);
        
        m_Msg.m_RefCount  = nComponents;
        for( s32 j=0; j<nComponents; j++ )
        {
            gb_component& Comp = *CompList[j];
            
            // send our update message
            Comp.sendMessage( m_Msg );
            
            // Call the component to handle all its messages
            Comp.vUpdate();
        }
    }
    eng_GetCurrentContext().End();
    
}

//-------------------------------------------------------------------------------

basic_render_mgr::basic_render_mgr( const char* pName ) :
    gb_base_component_mgr(pName)
{
    setupAffinity( x_base_job::AFFINITY_MAIN_THREAD );
    
    //
    // Initialize the draw all job
    //
    m_DrawAll.m_pThis = this;
  
    // Set the standard message
    m_DrawAll.m_Msg.setupSystemVars( gb_component::callback_fn(&render_component::onUpdate),
                                     gb_msg::FLAGS_ONUPDATE_MESSAGE |
                                     gb_msg::FLAGS_DO_NOT_DELETE );
	m_DrawAll.m_Msg.m_MessageFunc = gb_component::callback_fn(&render_component::onUpdate);
}

//-------------------------------------------------------------------------------

void basic_render_mgr::onUpdate( void )
{
    eng_context& Context = eng_GetCurrentContext();
    
    // Lets add the render all job
    Context.qtAddRenderJob( m_DrawAll );
    
    // Ok we are done with this frame
    Context.PageFlip( FALSE );
}
