//
//  AppDelegate.h
//  TestGameLib04iOS
//
//  Created by Tomas Arce on 7/6/14.
//  Copyright (c) 2014 Tomas Arce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
