//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef PHYSICS_COMPONENT_H
#define PHYSICS_COMPONENT_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "physicsmgr.h"

extern physics_mgr g_PhysicsMgr;

/////////////////////////////////////////////////////////////////////////////////
// INTERFACE TRANSFORM
/////////////////////////////////////////////////////////////////////////////////
class i_2d_transform
{
public:
    x_rtti_base(i_2d_transform);

    virtual xvector2    getPos           ( s32 Tx )=0;
    virtual xvector2    getVel           ( s32 Tx )=0;

protected:

    virtual void		setVel           ( const xvector2& Vel )=0;
    virtual void		setPos           ( const xvector2& Pos )=0;
};

/////////////////////////////////////////////////////////////////////////////////
// Physics Component
/////////////////////////////////////////////////////////////////////////////////
class physics_component : public gb_component, 
                          public i_2d_transform
{
public:
    GB_COMPONENT_RTTI2_CONTEXT( physics_component, gb_component, i_2d_transform );
    GB_COMPONENT_TYPEBASE( g_PhysicsMgr, XMEM_FLAG_ALIGN_8B )

public:

    //
    // Public access variables
    //
    struct context : public gb_component::context
    {
                            context         ( void )            {}
        virtual            ~context         ( void )            {}
        virtual void        vTransferData   ( const gb_component::context& Src );

        xvector2            m_Pos;
        xvector2            m_Vel;
    };

public:

										physics_component       ( void );
    virtual                            ~physics_component       ( void );

            void                        setupPosition           ( const xvector2& Pos );
            void                        setupVelocity           ( const xvector2& Vel );

    virtual xvector2 getPos( s32 Tx ) 
    { 
        if( m_Body.m_pBody )
        {
            auto Origin = m_Body.m_pBody->getWorldTransform().getOrigin();
            return xvector2( Origin.m_floats[0],  Origin.m_floats[1] );
        } 
        return m_PreWorldPos; 
    }

    virtual xvector2 getVel( s32 Tx ) 
    { 
        if( m_Body.m_pBody ) 
        {
            auto curVel = m_Body.m_pBody->getLinearVelocity();
            return xvector2( curVel.m_floats[0], curVel.m_floats[1] ); 
        }
        return m_PreWorldVel; 
    }

protected:

    struct body
    {
        btCollisionShape*   m_pShape = NULL;
        btRigidBody*        m_pBody  = NULL;
    };

protected:

    virtual void						setVel                  ( const xvector2& Vel );
    virtual void						setPos                  ( const xvector2& Pos );

    virtual void                        vSetup                  ( void );
    virtual void                        vResolveDependencies    ( void ) {}
    virtual void                        vLoadComponent          ( xtextfile& File );
    virtual void                        vInWorld                ( void );

    void								onUpdate                ( const gb_msg& Msg );

    virtual void                        HandleCollisions        ( physics_component* pCom, b2Contact* contact ) { ASSERT(0); }


protected:

    u32                     m_bHandleCollisions:1;
//    b2Body*                 m_pBody;
    body                    m_Body;
    xvector2                m_PreWorldPos;
    xvector2                m_PreWorldVel;
    friend struct my_b2ContactListener;
};

/////////////////////////////////////////////////////////////////////////////////
// MESSAGES
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

class physics_onupdate_msg : public gb_msg
{
public:
	GB_MSG_UID(physics_onupdate_msg);

    xvector2    m_NewPos;
    xvector2    m_NewVel;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif