//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "PhysicsComponent.h"
#include "PhysicsMgr.h"

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

physics_mgr                       g_PhysicsMgr("PhysicsMgr");

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS ISLAND FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS MANAGER FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

physics_mgr::physics_mgr( const char* pName ) : 
    gb_base_component_mgr(pName)
{
}
    
//-------------------------------------------------------------------------------

physics_mgr::~physics_mgr( void )
{
/*
    if( m_pWorld ) 
    {
        x_delete( m_pWorld );
        m_pWorld = NULL;
    }
    */
}

//-------------------------------------------------------------------------------

struct my_b2ContactListener : public b2ContactListener
{
	virtual void BeginContact(b2Contact* contact) 
    { 
        physics_component* pComA = ((physics_component*)contact->GetFixtureA()->GetUserData());
        if( pComA==NULL ) 
            return;

        physics_component* pComB = ((physics_component*)contact->GetFixtureB()->GetUserData());
        if( pComB==NULL ) 
            return;
        /*

        if( pComA->isDeleted() ) 
            return;

        if( pComB->isDeleted() ) 
            return;

        if( pComA->m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ) == FALSE )
            return;

        if( pComB->m_qtAttrFlags.isFlagOn( gb_component::QT_ATTR_IN_WORLD ) == FALSE )
            return;
*/

        if( pComA->m_bHandleCollisions==FALSE )
        {
            x_Swap( pComA, pComB );
        }

        if( pComA->m_bHandleCollisions==FALSE )
        {
            return;
        }

        pComA->HandleCollisions( pComB, contact );
    }

} s_ContactListener;

//-------------------------------------------------------------------------------
bool MyContactProcessedCallback( btManifoldPoint& cp, void* body0, void* body1 )
{
    const btCollisionObject& Col0 = *(btCollisionObject*)body0;
    const btCollisionObject& Col1 = *(btCollisionObject*)body1;
     
    return FALSE;
}

void physics_mgr::vInit( void )
{    
    gb_base_component_mgr::vInit();

    gContactProcessedCallback = MyContactProcessedCallback;

    // collision configuration contains default setup for memory , collision setup . Advanced
    // users can create their own configuration .
    btDefaultCollisionConfiguration* pCollisionConfiguration = new btDefaultCollisionConfiguration();
    
    // use the default collision dispatcher . For parallel processing you can use a different
    // dispatcher (see Extras / BulletMultiThreaded )
    m_pDispatcher = new btCollisionDispatcher( pCollisionConfiguration );

    // btDbvtBroadphase is a good general purpose broad phase . You can also try out btAxis3Sweep.
    m_pOverlappingPairCache = new btDbvtBroadphase ();

    // the default constraint solver . For parallel processing you can use a different solver
    // (see Extras / BulletMultiThreaded )
    m_pSolver = new btSequentialImpulseConstraintSolver ;

    m_pDynamicsWorld = new btDiscreteDynamicsWorld ( m_pDispatcher, m_pOverlappingPairCache, m_pSolver, pCollisionConfiguration );
    
    m_pDynamicsWorld->setGravity( btVector3(0, 0, 0) ); //btVector3(0, -10 ,0) );

    s32 W, H;
    eng_GetCurrentContext().GetScreenResolution( W, H );
    m_Bounds.Set(0.f, 0.f, (f32)W, (f32)H );

    m_Planes[0].m_pPlane = new btStaticPlaneShape( btVector3(  1,  0,  0 ),   0.0f      );
    m_Planes[1].m_pPlane = new btStaticPlaneShape( btVector3( -1,  0,  0 ),   -f32(W)   );
    m_Planes[2].m_pPlane = new btStaticPlaneShape( btVector3(  0,  1,  0 ),   0.0f      );
    m_Planes[3].m_pPlane = new btStaticPlaneShape( btVector3(  0, -1,  0 ),   -f32(H)   );
    m_Planes[4].m_pPlane = new btStaticPlaneShape( btVector3(  0,  0,  1 ),   2.0f      );
    m_Planes[5].m_pPlane = new btStaticPlaneShape( btVector3(  0,  0, -1 ),  -2.0f      );

	const btScalar  Mass(0.);
	btTransform     IdentityTransform;
	IdentityTransform.setIdentity();

    for( s32 i=0; i<m_Planes.getCount(); i++ )
	{
        m_Planes[i].m_pBody = new btRigidBody( Mass, 0, m_Planes[i].m_pPlane, btVector3(0,0,0) );
        m_Planes[i].m_pBody->setFriction( 0 );
        m_Planes[i].m_pBody->setRollingFriction(0);
        m_Planes[i].m_pBody->setRestitution( 1 );

        m_Planes[i].m_pBody->setWorldTransform(IdentityTransform);
        m_Planes[i].m_pBody->setUserIndex(-1);
		m_pDynamicsWorld->addRigidBody( m_Planes[i].m_pBody );
	}

/*
    gb_base_component_mgr::vInit();
    
    s32 W, H;
    eng_GetCurrentContext().GetScreenResolution( W, H );
    m_Bounds.Set(0.f, 0.f, (f32)W, (f32)H );

    //
    // Create the world in box 2d
    // 
    m_pWorld = x_new( b2World, 1, 0 );

    // Make sure that we handle collisions ourselves
    m_pWorld->SetContactListener( &s_ContactListener );

    //
    // Create the bounds of the world
    //
	{
		b2BodyDef bd;
		b2Body* ground = m_pWorld->CreateBody(&bd);

		b2EdgeShape shape;
        
		// Floor
        shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(m_Bounds.GetWidth(), 0.0f));
		ground->CreateFixture(&shape, 0.0f);

		// Left wall
        shape.Set(b2Vec2(0.0f, 0.0f), b2Vec2(0.0f, m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);

		// Right wall
		shape.Set(b2Vec2(m_Bounds.GetWidth(), 0.0f), b2Vec2(m_Bounds.GetWidth(), m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);

		// Roof
		shape.Set(b2Vec2(0.0f, m_Bounds.GetHeight()), b2Vec2(m_Bounds.GetWidth(), m_Bounds.GetHeight()));
		ground->CreateFixture(&shape, 0.0f);
        
	}
*/
}

//-------------------------------------------------------------------------------

void physics_mgr::vKill( void )
{
    // remove the rigid bodies from the dynamics world and delete them
    for( s32 i = m_pDynamicsWorld->getNumCollisionObjects() - 1; i >= 0 ; i-- )
    {
        btCollisionObject*  pObj    = m_pDynamicsWorld->getCollisionObjectArray()[ i ];
        btRigidBody*        pBody   = btRigidBody::upcast( pObj );
        
        if( pBody && pBody->getMotionState () )
        {
            delete pBody->getMotionState();
        }

        m_pDynamicsWorld->removeCollisionObject( pObj );
        delete pObj;
    }
    
    // delete collision shapes
    for( s32 j = 0; j < m_Planes.getCount(); j++ )
    {
        btCollisionShape* pShape = m_Planes[j].m_pPlane;
        m_Planes[j].m_pPlane = NULL;
        delete pShape ;
    }
    
    // delete dynamics world
    delete m_pDynamicsWorld;
    
    // delete solver
    delete m_pSolver;
    
    // delete broadphase
    delete m_pOverlappingPairCache;
    
    // delete dispatcher
    delete m_pDispatcher;
}

//-------------------------------------------------------------------------------

void physics_mgr::onUpdate( void )
{
    ASSERT( g_order == 1 ); g_order = 3;

    f32 DeltaTime   = g_GameMgr.getDeltaTime();

    // 
    // Simulate the Box 2d World
    //
    m_pDynamicsWorld->stepSimulation( DeltaTime );

}

//-------------------------------------------------------------------------------

struct my_b2QueryCallback : public b2QueryCallback
{
    gb_component*       m_pSelf;
    gb_component*       m_pClosest;
    gb_component_type*  m_pTypeFilter;
    f32                 m_DistanceSquare;
    b2Vec2              m_Pos;

         my_b2QueryCallback ( void ) : m_pClosest( NULL ), m_pTypeFilter( NULL ) {};

    bool Filter             ( gb_component* pComp )
    {
        if( pComp == NULL )
            return true;

        if( pComp == m_pSelf )
            return true;

        if( pComp->isDeleted() ) 
            return true;

        if( m_pTypeFilter && m_pTypeFilter == &pComp->getType() )
            return true;
        
        return false;
    }

	bool ReportFixture      ( b2Fixture* fixture )
    {
        if( m_pClosest == NULL )
        {
            gb_component* pComp = (gb_component*)fixture->GetUserData();

            if( Filter( pComp ) )
                return true;

            m_pClosest       = (gb_component*)fixture->GetUserData();
            m_DistanceSquare = (m_Pos - fixture->GetBody()->GetPosition()).LengthSquared();
        }
        else
        {
            f32 dd = (m_Pos - fixture->GetBody()->GetPosition()).LengthSquared();

            if( dd < m_DistanceSquare ) 
            {
                gb_component* pComp = (gb_component*)fixture->GetUserData();

                if( Filter( pComp ) )
                    return true;

                m_pClosest       = pComp;
                m_DistanceSquare = dd;
            }
        }

        return true;
    }
};

//-------------------------------------------------------------------------------

struct closest_components : public x_simple_job<1,x_light_job>
{
    virtual void            onRun           ( void );
    xarray<gb_component*>*  m_pArray;
    f32                     m_R;
    my_b2QueryCallback      m_MyCallBack;
};

//-------------------------------------------------------------------------------

void closest_components::onRun( void )
{
    xrect Bounds(m_MyCallBack.m_Pos.x-m_R,
                 m_MyCallBack.m_Pos.y-m_R,
                 m_MyCallBack.m_Pos.x+m_R,
                 m_MyCallBack.m_Pos.y+m_R);

    b2AABB aabb;
    
    // Create the bbox for the components
    aabb.lowerBound.Set( Bounds.m_Left,  Bounds.m_Top );
    aabb.upperBound.Set( Bounds.m_Right, Bounds.m_Bottom);

    // Query the world for overlapping shapes.
//    b2World&            World = g_PhysicsMgr.getWorld();
//    World.QueryAABB( &m_MyCallBack, aabb );

    if( m_MyCallBack.m_pClosest ) m_pArray->append() = m_MyCallBack.m_pClosest;
}


x_light_job* physics_mgr::ClosestComponents( xarray<gb_component*>& lComp, const xvector2& Point, f32 Radious, gb_component* pSelf, gb_component_type* pTypeFilter )
{
    closest_components* pClosest = x_new(closest_components, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pClosest);

    pClosest->setupAutoDelete();
    pClosest->m_pArray      = &lComp;
    pClosest->m_R           = Radious;
    pClosest->m_MyCallBack.m_Pos.Set( Point.m_X, Point.m_Y );
    pClosest->m_MyCallBack.m_pTypeFilter = pTypeFilter;
    pClosest->m_MyCallBack.m_pSelf = pSelf;

    return pClosest;
}
