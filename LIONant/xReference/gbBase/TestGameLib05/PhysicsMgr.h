//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef GB_PHYSICS_MGR_H
#define GB_PHYSICS_MGR_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "2dworld.h"

#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

/////////////////////////////////////////////////////////////////////////////////
// physics_mgr
/////////////////////////////////////////////////////////////////////////////////
class physics_mgr : public gb_base_component_mgr
{
public:
                                            GB_MANAGER_RTTI(physics_mgr, gb_base_component_mgr);

                                            physics_mgr                 ( const char* pName );
                                           ~physics_mgr                 ( void );
    const xrect&                            getBounds                   ( void ) { return m_Bounds; }
    x_light_job*                            ClosestComponents           ( xarray<gb_component*>& lComp, const xvector2& Point, f32 Radious, gb_component* pSelf, gb_component_type* pTypeFilter=NULL );        
    btDiscreteDynamicsWorld&                getWorld                    ( void ) { return *m_pDynamicsWorld; }

protected:

    struct body_planes
    {
        btStaticPlaneShape* m_pPlane;
        btRigidBody*        m_pBody;
    };

protected:

    virtual void                            vInit                   ( void );
    virtual void                            vKill                   ( void );

    virtual void                            onUpdate                ( void );

protected:

    // TODO: how to record all the components. 
    xrect                                   m_Bounds;
    btDiscreteDynamicsWorld*                m_pDynamicsWorld            = NULL; 
    btSequentialImpulseConstraintSolver*    m_pSolver                   = NULL;
    btBroadphaseInterface*                  m_pOverlappingPairCache     = NULL;
    btCollisionDispatcher*                  m_pDispatcher               = NULL;
    xsafe_array<body_planes,6>              m_Planes;
};

//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////
extern physics_mgr g_PhysicsMgr;

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif 
