//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef BULLET_PHYSICS_H
#define BULLET_PHYSICS_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "2DWorld.h"
#include "physicsmgr.h"
#include "PhysicsComponent.h"

/////////////////////////////////////////////////////////////////////////////////
// BULLET PHYSICS
/////////////////////////////////////////////////////////////////////////////////

class bullet_physics : public physics_component
{
    GB_COMPONENT_RTTI1( bullet_physics, physics_component );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )
        
/*
    GB_BEGIN_MSG_GROUP
    GB_END_MSG_GROUP
*/

public:
                                        bullet_physics          ( void );

            void                        setupParent             ( xguid Guid );

protected:

    virtual void                        HandleCollisions        ( physics_component* pCom, b2Contact* contact );

protected:

    xguid       m_gParent;                  // The thing that just shoted this bullet from
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif