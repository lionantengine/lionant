//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

// This test is basically like the test03 except we use box2D and the physics engine.
// The main point of this demo is to add the additional dependency of box2D.
// which should show that this multicore engine hapily lives with 3rd party libraries

#include "2DWorld.h"

#include "EntityDot.h"
#include "PhysicsComponent.h"
#include "BasicRenderMgr.h"
#include "entitybullet.h"
#include "BulletPhysics.h"

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

//////////////////////////////////////////////////////////////////////////////////
// Create the state of the game
//////////////////////////////////////////////////////////////////////////////////

static gb_cloud_state   s_GameWorld;
static gb_sync_point    s_GameWorld_Input;
static gb_sync_point    s_GameWorld_Physics;
static gb_sync_point    s_GameWorld_AI;
static gb_sync_point    s_GameWorld_Rendering;

//-------------------------------------------------------------------------------
// This is an example on how to create an entity from an editor perspective. As you can see
// we are not assuming that we know anything that the editor would not have access to.
//-------------------------------------------------------------------------------
xguid BuildDotEntity( xguid gBulletBlueprint )
{
    const char* pComponentsTypesNames[]= { entity_dot::s_Type.getName(), 
                                           physics_component::s_Type.getName(),
                                           render_component::s_Type.getName(), 
                                           "" };
    xarray<gb_component*> ComponentList;
    xguid PresetGuid;

    PresetGuid.SetNull();

    // First the user will select the components to create which could be base from a name list
    for( s32 i=0; i<3; i++ )
    {
        // Find the type
        gb_component_type* pType = g_ComponentTypeMgr.findComponentType( pComponentsTypesNames[i] );
        ASSERT(pType);

        // Now create the component
        ComponentList.append() = pType->createComponent(PresetGuid);

        // Here we assume that the user will first choose the entity then from there he would choose
        // the components for that entity. However if the user wanted to change the entity this should
        // not be a problem in the editor it self because it should be able to delete the entity without
        // deleting their components.
        if( i > 0 )
            gb_entity::SafeCast( *ComponentList[0] ).addComponent( *ComponentList[i] );
    }


    // After the components are created the user should be able to setup the properties for 
    // each of the components. This case since the property system is not yet implemented 
    // we will emulate it with just setting the function ourselfs
	entity_dot::presets& DotPresets = entity_dot::SafeCast( *ComponentList[0] ).setupNewPresets();
	DotPresets.m_gBullet = gBulletBlueprint;

	render_component::presets& RenderPresets =	render_component::SafeCast( *ComponentList[2] ).setupNewPresets();
	RenderPresets.m_Shape = render_component::SHAPE_SQUARE;

    //
    // Creation of the blue print in the editor should be just like one button press. 
    // From there the following list of commands will be issue.
    //

    // Create our blue print
    xguid gMyBlueprint = g_BlueprintMgr.createBlueprint("temp:/TestObject.txt");

    // Append Our entity to create the blue print that we want
    g_BlueprintMgr.appendEntity( gMyBlueprint, gb_entity::SafeCast( *ComponentList[0] ) );

    // Lets compile it
    g_BlueprintMgr.compileBlueprint( gMyBlueprint );

    return gMyBlueprint;
}

//-------------------------------------------------------------------------------
// This is an example on how to create an entity directly from code.
// As you can see there is not much code to write and it should be fairly clean.
// However this should not really happen very commonly. Most if not all the entities
// will be created in the editor itself.
//-------------------------------------------------------------------------------
xguid BuildBullet( void )
{
    xguid PresetGuid;
    PresetGuid.SetNull();

    // Create the components                
    entity_bullet&      Bullet   = entity_bullet::SafeCast( *entity_bullet::s_Type.createComponent(PresetGuid) );
    bullet_physics&     Physics  = bullet_physics::SafeCast( *bullet_physics::s_Type.createComponent(PresetGuid) );
    render_component&   Render   = render_component::SafeCast( *render_component::s_Type.createComponent(PresetGuid) );
    
    // Set the properties for each of the components
	render_component::presets& RenderPresets =	Render.setupNewPresets();
	RenderPresets.m_Shape = render_component::SHAPE_TRIANGLE;

    // Build the entity
    Bullet.addComponent( Physics );
    Bullet.addComponent( Render );

    // Create our blue print
    xguid gMyBlueprint = g_BlueprintMgr.createBlueprint("temp:/TestObject2.txt");

    // Append Our entity to create the blue print that we want
    g_BlueprintMgr.appendEntity( gMyBlueprint, Bullet );

    // Compilation shoudl delete this entity as this is the master entity
    // From this moment forward you should be able to create this type of entity
    // by using the blueprint guid.
    g_BlueprintMgr.compileBlueprint( gMyBlueprint );

    return gMyBlueprint;
}

//-------------------------------------------------------------------------------

void Initialize( void )
{
    xguid gMyBlueprint;
    xguid gMyBulletBlueprint;

    //----------------------------------------------------------------------------
    // Initialize the engine
    //----------------------------------------------------------------------------
    eng_Init();
    g_Scheduler.Init( 2 );
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);

    //----------------------------------------------------------------------------
    // Now deal with the game system
    //----------------------------------------------------------------------------

    //
    // Build the graph
    //

    s_GameWorld.setupBeging( "Game World" );
    s_GameWorld_Input.setup     ("Input", s_GameWorld,
                                    x_va_list( (xuptr)&s_GameWorld.getStartSyncP() ),
                                    x_va_list( (xuptr)&g_InputMgr )   );
    s_GameWorld_Physics.setup   ("Physics", s_GameWorld,    
                                    x_va_list( (xuptr)&s_GameWorld_Input ),
                                    x_va_list( (xuptr)&g_PhysicsMgr ) );
    s_GameWorld_AI.setup        ("AI", s_GameWorld,
                                    x_va_list( (xuptr)&s_GameWorld_Physics ),
                                    x_va_list( (xuptr)&g_EntityMgr )  );
    s_GameWorld_Rendering.setup ("Rendering", s_GameWorld, 
                                    x_va_list( (xuptr)&s_GameWorld_AI ),
                                    x_va_list( (xuptr)&g_RenderMgr )  );

    s_GameWorld.setupEnd();

    //----------------------------------------------------------------------------
    // Initialize the game
    //----------------------------------------------------------------------------

    // TODO: This should be put in a nicer more intuitive function somewhere
    gb_base_mgr::InitAll();

    // We create our blue prints that we will use in our game
    gMyBulletBlueprint  = BuildBullet();
    gMyBlueprint        = BuildDotEntity( gMyBulletBlueprint );

    // Now lets save our blueprint
    g_BlueprintMgr.saveBlueprints();
	g_BlueprintMgr.savePresets("temp:/");

    //
    // Lets create a few instances in our world
    //
    xtransform Transform;

    for( s32 i=0; i<300; i++)
    {
        g_EntityMgr.msgCreateEntity( gMyBlueprint, Transform );
    }

    //
    // Get the game manager to warm up a bit
    //
    g_StateMgr.setActiveState(s_GameWorld);
    g_GameMgr.msgStart();
}


//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    Initialize();
    
    // By doing this the main thread basically becomes a worker
    while( 1 )
    {
        g_Scheduler.MainThreadRunJobs( TRUE );
    }
}