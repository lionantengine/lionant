//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "EntityBullet.h"

/////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE(entity_bullet, gb_component_type::FLAGS_REALTIME_DYNAMIC)

//-------------------------------------------------------------------------------

entity_bullet::entity_bullet( void )
{
    m_Timer = 0;
}

//-------------------------------------------------------------------------------

void entity_bullet::onUpdate( const gb_msg& msg )
{
    m_Timer += msg.m_DeltaTime;

    // wait for about 5 seconds before we blow up
    if( m_Timer > 6  &&  m_Timer < 100 )
    {
        // Okay we are going to be deleted next frame...
        g_EntityMgr.msgRemoveEntity( getEntity() );

        // Make sure we only issue this meesage ones
        m_Timer = 100;
    }
}
