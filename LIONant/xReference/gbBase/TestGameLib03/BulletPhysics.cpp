//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "BulletPhysics.h"

/////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE(bullet_physics,gb_component_type::FLAGS_REALTIME_DYNAMIC)

//-------------------------------------------------------------------------------

bullet_physics::bullet_physics( void )
{
    m_bIgnoreCollisions = FALSE;    
    m_bHandleCollisions = TRUE;        
}

//-------------------------------------------------------------------------------

void bullet_physics::HandleCollisions( pos_vel& At0, pos_vel& At1, pos_vel& Bt0, pos_vel& Bt1 )
{
    // Do do not collide with our own ship
    if( m_gParent == Bt0.m_pComponent->getEntity().getGuid() )
        return;

    // if both are bullets then the one with the biggest pointer is the one incharge 
    // to delete the other one. If not we can have the case that the two bullets are 
    // trying to delete eachother, creating double deletes.
    if( &bullet_physics::s_Type == &Bt0.m_pComponent->getType() )
        if( this < Bt0.m_pComponent ) return;

    // The case where two bullets are killing the same object at the same time is not handle
    // at this moment. So the object will be mark for deletion twice.

    //
    // Handle my friend
    //
    g_EntityMgr.msgRemoveEntity( Bt0.m_pComponent->getEntity() );

    //
    // Handle me
    //
    g_EntityMgr.msgRemoveEntity( getEntity() );
}

//-------------------------------------------------------------------------------

void bullet_physics::setupParent( xguid Guid )
{
    m_gParent = Guid;
}
