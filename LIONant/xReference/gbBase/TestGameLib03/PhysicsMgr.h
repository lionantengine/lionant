//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef GB_PHYSICS_MGR_H
#define GB_PHYSICS_MGR_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "gb_base.h"

/////////////////////////////////////////////////////////////////////////////////
// physics_mgr
/////////////////////////////////////////////////////////////////////////////////
class physics_mgr : public gb_base_component_mgr
{
public:
                                            GB_MANAGER_RTTI(physics_mgr, gb_base_component_mgr);

                                            physics_mgr                 ( const char* pName ) : gb_base_component_mgr(pName) {}
                                           ~physics_mgr                 ( void ){}
    const xrect&                            getBounds                   ( void ) { return m_Bounds; }
    x_light_job*                            ClosestComponents           ( xarray<gb_component*>& lComp, const xvector2& Point, f32 Radious, s32 Tx );        

protected:

    virtual void                            vInit                   ( void );
    virtual void                            vKill                   ( void );

    virtual void                            onUpdate                ( void );

protected:

    // TODO: how to record all the components. 
    xrect        m_Bounds; 
};

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif 
