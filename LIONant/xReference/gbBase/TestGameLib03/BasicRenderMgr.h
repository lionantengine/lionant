//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef BASIC_RENDER_MGR_H
#define BASIC_RENDER_MGR_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////////////////////////////////////
#include "PhysicsComponent.h"
#include "eng_base.h"

class basic_render_mgr;
extern class basic_render_mgr g_RenderMgr;

/////////////////////////////////////////////////////////////////////////////////
// Render Component... hacky place to put it...
/////////////////////////////////////////////////////////////////////////////////
class render_component : public gb_component
{
public:
    GB_COMPONENT_RTTI1_PRESETS( render_component, gb_component );
    GB_COMPONENT_TYPEBASE( g_RenderMgr, XMEM_FLAG_ALIGN_8B )
/*
    GB_BEGIN_MSG_GROUP
	    GB_MSG_GROUP_ENTRY(render_onupdate_msg)
    GB_END_MSG_GROUP
*/

public:

    enum shapes
    {
        SHAPE_SQUARE,
        SHAPE_TRIANGLE,
        SHAPE_LAST = 0xffffffff
    };

    struct presets : public gb_component::presets
    {
	    shapes   m_Shape;
						presets			( void ){m_Shape=SHAPE_TRIANGLE;}
        virtual void    vLoad           ( xtextfile& File );
        virtual void    vSave           ( xtextfile& File );
	};
    
protected:

    virtual void                        vResolveDependencies    ( void );
    virtual void                        vSetup                  ( void ){}
    virtual void                        vLoadComponent          ( xtextfile& File );
    virtual void                        vSaveComponent          ( xtextfile& File );
    virtual void						onUpdate                ( const gb_msg& Msg );

protected:

    i_2d_transform*     m_pTransform;
    float               m_VertexBuffer[2][8];
    int                 m_CurrentBuffer;

protected:

	friend class basic_render_mgr;
};

/////////////////////////////////////////////////////////////////////////////////
// MESSAGES
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

class render_onupdate_msg : public gb_msg
{
public:
	GB_MSG_UID(render_onupdate_msg);
};

/////////////////////////////////////////////////////////////////////////////////
// basic_render_mgr
/////////////////////////////////////////////////////////////////////////////////
class basic_render_mgr : public gb_base_component_mgr
{
public:
                                            GB_MANAGER_RTTI(basic_render_mgr, gb_base_component_mgr);

                                            basic_render_mgr        ( const char* pName );
protected:
    
    struct draw_all : public eng_renderjob
    {
        basic_render_mgr*       m_pThis;
        render_onupdate_msg     m_Msg;
        
        virtual void onRunRenderJob ( void ) const override;
    };

protected:

    virtual void                            onUpdate                ( void );

protected:

    draw_all    m_DrawAll;
};

//////////////////////////////////////////////////////////////////////////////////
// END
//////////////////////////////////////////////////////////////////////////////////
#endif 

