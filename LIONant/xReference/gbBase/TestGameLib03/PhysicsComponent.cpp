//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "PhysicsComponent.h"
#include "PhysicsMgr.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Physics Component
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
GB_CLASS_TYPE( physics_component, gb_component_type::FLAGS_REALTIME_DYNAMIC )

//-------------------------------------------------------------------------------

physics_component::physics_component( void )
{   
    m_bIgnoreCollisions = FALSE;
    m_bHandleCollisions = FALSE;
}

//-------------------------------------------------------------------------------

physics_component::~physics_component( void )
{

}

//-------------------------------------------------------------------------------

void physics_component::vSetup( void )
{
//    gb_component::vSetup();

    const xrect& Bounds = g_PhysicsMgr.getBounds();

    //
    // Initialize some values
    //
    context& Data = getRawContext(0);
    Data.m_Pos.m_X = f32(x_rand() % (s32)Bounds.GetWidth()  + Bounds.m_Left);
    Data.m_Pos.m_Y = f32(x_rand() % (s32)Bounds.GetHeight() + Bounds.m_Top);

    Data.m_Vel.m_X = f32(x_rand() % 9 - 4)*6.0f;    // per millisecond
    Data.m_Vel.m_Y = f32(x_rand() % 9 - 4)*6.0f;

    //
    // Replicate public data 
    //

    //TODO: This should be call by a manager?
    SetupContexTransferC02C1();
}

//-------------------------------------------------------------------------------

void physics_component::setupPosition( const xvector2& Pos )
{
    context& Data = getRawContext(0);
    Data.m_Pos = Pos;
}

//-------------------------------------------------------------------------------
void physics_component::setupVelocity( const xvector2& Vel )
{
    context& Data = getRawContext(0);
    Data.m_Vel = Vel;
}

//-------------------------------------------------------------------------------

void physics_component::vLoadComponent( xtextfile& File )
{
    vSetup();
}

//-------------------------------------------------------------------------------

void physics_component::onUpdate( const gb_msg& aMsg )
{
    context&                        WContext = getWriteContextT1();
    const physics_onupdate_msg&     Msg      = physics_onupdate_msg::SafeCast(aMsg);

    WContext.m_Pos = Msg.m_NewPos;
    WContext.m_Vel = Msg.m_NewVel;
}

//-------------------------------------------------------------------------------

xrect physics_component::getBounds( f32 delta )
{   
    const context& Context = getReadContextLatest();

    const f32   R = 1;
    f32         X = Context.m_Pos.m_X;
    f32         Y = Context.m_Pos.m_Y;

    xrect Bounds = xrect(X - R, Y - R, X + R, Y + R);

    f32 EnergyR = (Context.m_Vel * delta).GetLength();

    Bounds.Inflate( EnergyR, EnergyR );

    return Bounds;
}

//-------------------------------------------------------------------------------

void physics_component::setVel( const xvector2& Vel )
{
    getWriteContextT1().m_Vel = Vel;
}

//-------------------------------------------------------------------------------

void physics_component::setPos( const xvector2& Pos )
{
    getWriteContextT1().m_Pos = Pos;
}

/*
//------------------------------------------------------------------------------

void physics_component::compileProperty( xtextfile& File )
{
    gb_component::compileProperty(File);

    File.writeField("Dependency:d", x_va_list(-1));
    File.writeField("Position:ff", x_va_list(readContext().m_Pos.m_X, readContext().m_Pos.m_Y));
    File.writeField("Velocity:ff", x_va_list(readContext().m_Vel.m_X, readContext().m_Vel.m_Y));
}

//------------------------------------------------------------------------------

void physics_component::loadProperty( xtextfile& File )
{
    gb_component::loadProperty(File);

    context& Data = writeContext();

    // Load the default data
    File.readField("Dependency:d", x_va_r_list());
    File.readField("Position:ff", x_va_r_list(&Data.m_Pos.m_X, &Data.m_Pos.m_Y));
    File.readField("Velocity:ff", x_va_r_list(&Data.m_Vel.m_X, &Data.m_Vel.m_Y));

    //
    // Random value in our case
    //
    const xrect& Bounds = g_PhysicsMgr.getBounds();

    Data.m_Pos.m_X = f32(x_rand() % (s32)Bounds.getWidth() + Bounds.mLeft);
    Data.m_Pos.m_Y = f32(x_rand() % (s32)Bounds.getHeight() + Bounds.mBottom);

    Data.m_Vel.m_X = f32(x_rand() % 9 - 4)*6.0f;    // per millisecond
    Data.m_Vel.m_Y = f32(x_rand() % 9 - 4)*6.0f;

    //
    // Replicate public data 
    //
    ((context*)&readContext())->TransferData( &Data );
}
*/

//===============================================================================
//===============================================================================
// physics_component::context
//===============================================================================
//===============================================================================

void physics_component::context::vTransferData( const gb_component::context& pSrc )
{
    const context& obj = *((const context*)&pSrc);
    m_Pos = obj.m_Pos;
    m_Vel = obj.m_Vel;
}

