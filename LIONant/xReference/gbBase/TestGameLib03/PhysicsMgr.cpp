//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include "PhysicsComponent.h"
#include "PhysicsMgr.h"
#include "eng_base.h"

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS ISLANDS TYPE
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

struct physics_island : public x_simple_job<1>
{
                            physics_island  ( void ) { m_Bounds.setMax(); }
    virtual void            onRun           ( void );

    f32                             m_DeltaTime;
    xrect                           m_Bounds;
    xarray<physics_component*>      m_lObjects;
    s32                             m_T0;
    xrect                           m_MgrBounds;
};

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

physics_mgr                       g_PhysicsMgr("PhysicsMgr");
static xarray<physics_island>     s_Islands( 0, XMEM_FLAG_ALIGN_8B );

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS ISLAND FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

void RenderIslands(void)
{
    draw_vertex* pBaseVertex;
    u16*         pBaseIndex;
    
    eng_context& Context = eng_GetCurrentContext();
    eng_draw&    Draw    = Context.getDraw();
    
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_ZBUFFER_OFF );
    Draw.ClearL2W();
    Draw.ClearTexture();
    
    const s32 nIslands = s_Islands.getCount();
    
    Draw.GetBuffers( &pBaseVertex, nIslands*8, &pBaseIndex, nIslands*8 );
    
    for( s32 i=0; i<s_Islands.getCount(); i++ )
    {
        physics_island& Island  = s_Islands[i];
        xrect&          Rect    = Island.m_Bounds;
        const s32       offset  = i*8;
        draw_vertex*    pVertex = &pBaseVertex[ offset ];
        u16*            pIndex  = &pBaseIndex[ offset ];
        

        pVertex[0].setup( xvector3d( Rect.m_Left,   Rect.m_Top, 0),     xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[1].setup( xvector3d( Rect.m_Left,   Rect.m_Bottom, 0),  xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[2].setup( xvector3d( Rect.m_Right,  Rect.m_Top, 0),     xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[3].setup( xvector3d( Rect.m_Right,  Rect.m_Bottom, 0),  xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );

        pVertex[4].setup( xvector3d( Rect.m_Left,   Rect.m_Bottom, 0),  xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[5].setup( xvector3d( Rect.m_Right,  Rect.m_Bottom, 0),  xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[6].setup( xvector3d( Rect.m_Left,   Rect.m_Top, 0),     xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        pVertex[7].setup( xvector3d( Rect.m_Right,  Rect.m_Top, 0),     xcolor( xvector3d(1.0f, 0.0f, 0.0f) ) );
        
        pIndex[0] = offset + 0;
        pIndex[1] = offset + 1;
        pIndex[2] = offset + 2;
        pIndex[3] = offset + 3;
        
        pIndex[4] = offset + 4;
        pIndex[5] = offset + 5;
        pIndex[6] = offset + 6;
        pIndex[7] = offset + 7;
    }
    
    Draw.DrawBufferLines();
    Draw.End();
}

//-------------------------------------------------------------------------------

void physics_island::onRun( void )
{
    f32 TimeStep;
    s32 TimeCount;

    //
    // Determine the time steps we need to take to advance the island
    //
    if( m_DeltaTime < 1/60.0f )
    {
        TimeStep  = m_DeltaTime;
        TimeCount = 1;
    }
    else
    {
        TimeStep  = 1 / 60.0f;
        TimeCount = s32(x_Round( m_DeltaTime / TimeStep, 1));
    }

    ASSERT(TimeCount > 0);

    //
    // Allocate buffers of positions and velocities
    //
    xptr<physics_component::pos_vel>    Buffers[2];
    s32                                 ObjectCount = m_lObjects.getCount();

    Buffers[0].Alloc( ObjectCount );
    Buffers[1].Alloc( ObjectCount );

    physics_component::pos_vel*    pT0 = &Buffers[0][0];
    physics_component::pos_vel*    pT1 = &Buffers[1][0];

    ASSERT(pT0);
    ASSERT(pT1);

    for (s32 i = 0; i < ObjectCount; ++i)
    {
        physics_component&                  Object  = physics_component::SafeCast(*m_lObjects[i]);
        const physics_component::context&   Context = *(const physics_component::context*)&Object.getRawContext(m_T0);

        pT0[i].m_Pos            = Context.m_Pos;
        pT0[i].m_Vel            = Context.m_Vel;
        pT0[i].m_pComponent = 
        pT1[i].m_pComponent = &Object;  
    }

    //
    // Addvance the logic
    //
    for( s32 iT = 0; iT < TimeCount; ++iT )
    {
        // Update the theoretical position
        for (s32 i = 0; i < ObjectCount; ++i)
        {
            pT1[i].m_Pos = pT0[i].m_Pos + pT0[i].m_Vel * TimeStep;
            pT1[i].m_Vel = pT0[i].m_Vel;
        }

        // update the collisions of all the particles
        for (s32 i = 0; i < ObjectCount; ++i)
        {
            // If object has been deleted please ignore it
            if( pT1[i].m_pComponent->isDeleted() )
                continue;

            for (s32 j = i+1; j < ObjectCount; ++j)
            {
                // Collision with other objects
                if( pT1[i].m_Pos.GetDistance(pT1[j].m_Pos) <= 10.0f)
                {
                    // If object has been deleted please ignore it
                    if( pT1[j].m_pComponent->isDeleted() )
                        continue;

                    if( pT1[i].m_pComponent->m_bIgnoreCollisions || 
                        pT1[j].m_pComponent->m_bIgnoreCollisions )
                    {
 //                       int a=0;
                    }
                    else if( pT1[i].m_pComponent->m_bHandleCollisions )
                    {
                        pT1[i].m_pComponent->HandleCollisions( pT0[i], pT1[i], pT0[j], pT1[j] );
                    }
                    else if( pT1[j].m_pComponent->m_bHandleCollisions )
                    {
                        pT1[j].m_pComponent->HandleCollisions( pT0[j], pT1[j], pT0[i], pT1[i] );
                    }
                    else
                    {
                        xvector2 ColNormal1 = pT1[i].m_Pos - pT1[j].m_Pos;
                        ColNormal1.NormalizeSafe();
                        xvector2 ColNormal2 = -ColNormal1;

                        pT1[i].m_Vel  = 2 * pT1[i].m_Vel.Dot( ColNormal1 ) * ColNormal1 - pT1[i].m_Vel;
                        pT1[j].m_Vel  = 2 * pT1[j].m_Vel.Dot( ColNormal2 ) * ColNormal2 - pT1[j].m_Vel;
                        pT1[i].m_Pos  = pT0[i].m_Pos;
                        pT1[j].m_Pos  = pT0[j].m_Pos;
                    }
                }                
            }

            //
            // Check bound with world
            //

            if( pT1[i].m_Pos.m_X > m_MgrBounds.m_Right  || pT1[i].m_Pos.m_X < m_MgrBounds.m_Left ) 
            { 
                pT1[i].m_Pos.m_X =  pT0[i].m_Pos.m_X; 
                pT1[i].m_Vel.m_X = -pT1[i].m_Vel.m_X; 
            } 

            if( pT1[i].m_Pos.m_Y > m_MgrBounds.m_Bottom || pT1[i].m_Pos.m_Y < m_MgrBounds.m_Top  ) 
            { 
                pT1[i].m_Pos.m_Y =  pT0[i].m_Pos.m_Y; 
                pT1[i].m_Vel.m_Y = -pT1[i].m_Vel.m_Y; 
            } 
        }

        //
        // Swap times and get ready for the next loop
        //
        x_Swap(pT0,pT1);
    }

    //
    // Physically update the components
    //
    physics_onupdate_msg    Msg;

    // Initialize basic info for the message
    Msg.setupSystemVars( gb_component::callback_fn(&physics_component::onUpdate), 
							gb_msg::FLAGS_ONUPDATE_MESSAGE | 
							gb_msg::FLAGS_DO_NOT_DELETE );
	Msg.m_DeltaTime = m_DeltaTime;

    // Fill up the rest of the message and send it to the objects
    for (s32 i = 0; i < ObjectCount; ++i)
    {
        gb_component& Comp = *m_lObjects[i];

        Msg.m_RefCount++;
        Msg.m_NewPos    = pT0[i].m_Pos;
        Msg.m_NewVel    = pT0[i].m_Vel;

        // send our update message
        Comp.sendMessage( Msg );

        // Ask the object to process all its messages
        Comp.vUpdate();
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// PHYSICS MANAGER FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------

void physics_mgr::vInit( void )
{
    gb_base_component_mgr::vInit();
    
    eng_context& Context = eng_GetCurrentContext();

    s32 W, H;
    Context.GetScreenResolution( W, H );
    m_Bounds.Set(0, 0, W, H );
}

//-------------------------------------------------------------------------------

void physics_mgr::vKill( void )
{
    /*
    m_JobGroup.destroy();
    m_Components.destroy();
    m_Islands.destroy();
    */
}

//-------------------------------------------------------------------------------

void physics_mgr::onUpdate( void )
{
    ASSERT( g_order == 1 ); g_order = 3;

    //    show_func_timecost TimeCost("refreshCells");

    f32 DeltaTime   = g_GameMgr.getDeltaTime();
    s32 T0          = m_pSyncPoint->getTxRead(0);

    //
    // Start inserting objects into islands
    // *** HACK *** Note that the method of dealing with islands is neither fast
    // nor acurate as islands should be mergable and here is not happening.
    //
    s_Islands.Kill();
    for( s32 j=0; j<m_lActiveType.getCount(); j++ )
    {
        s32                     iType       = m_lActiveType[j];
        xharray<gb_component*>& CompList    = g_ComponentTypeMgr.GetComponentList(iType);
        s32                     nComponents = g_ComponentTypeMgr.GetActiveCount(iType);

        for (s32 i = 0; i < nComponents; ++i)
        {
            physics_component*  pObject     = &physics_component::SafeCast(*CompList[i]);
            xrect               ObjBounds   = pObject->getBounds(DeltaTime);
            xbool               bSettled    = FALSE;
            s32                 IslandCount = s_Islands.getCount();

            // Give a bigger radious for debugging
            ObjBounds.Inflate(10,10);

            for( s32 j = 0; j < IslandCount; ++j )
            {
                physics_island& Island = s_Islands[j];

                if( Island.m_Bounds.Intersect(ObjBounds) )
                {
                    Island.m_Bounds.AddRect(ObjBounds);
                    Island.m_lObjects.append() = pObject;
                    bSettled = TRUE;
                    break;
                }
            }

            // New cell
            if (!bSettled)
            {
                physics_island& NewIsland = s_Islands.append();

                NewIsland.m_lObjects.append() = pObject;
                NewIsland.m_Bounds.AddRect(ObjBounds);

                NewIsland.m_DeltaTime = DeltaTime;
                NewIsland.m_T0        = T0;
                NewIsland.m_MgrBounds = m_Bounds;
            }
        }
    }

    //
    // Finally insert the islands into the scheguler
    //
    for( s32 i = 0; i < s_Islands.getCount(); ++i )
    {
        m_pSyncPoint->AndWaitFor ( s_Islands[i] );
        g_Scheduler.StartJobChain( s_Islands[i] );
    }
}

//-------------------------------------------------------------------------------

struct closest_components : public x_simple_job<1,x_light_job>
{
    virtual void            onRun           ( void );
    xarray<gb_component*>*  m_pArray;
    xvector2                m_Point;
    f32                     m_R;
    s32                     m_Tx;
};

void closest_components::onRun( void )
{
    xrect Bounds(m_Point.m_X-m_R,m_Point.m_Y-m_R,m_Point.m_X+m_R,m_Point.m_Y+m_R);

    for( s32 i=0; i<s_Islands.getCount(); i++ )
    {
        physics_island& Island = s_Islands[i];

        // Does the physics island touches the bounds?
        if( Bounds.Intersect( Island.m_Bounds ) )
        {            
            //
            // Okay they touch so lets check objects in here
            //
            for( s32 j=0; j<Island.m_lObjects.getCount(); j++ )
            {
                physics_component&  Object  = *Island.m_lObjects[j];
                xvector2            Pos     = Object.getPos( m_Tx ); 

                if( Bounds.PointInRect( Pos.m_X, Pos.m_Y ) )
                {
                    m_pArray->append() = &Object;
                }
            }
        }
    }

    return;
}


x_light_job* physics_mgr::ClosestComponents( xarray<gb_component*>& lComp, const xvector2& Point, f32 Radious, s32 Tx )
{
    // We can not solve for T1 if we have not computed yet
    ASSERT( Tx <= m_pSyncPoint->hasTimeAdvance() );

    closest_components* pClosest = x_new(closest_components, 1, XMEM_FLAG_ALIGN_8B );
    ASSERT(pClosest);

    pClosest->setupAutoDelete();
    pClosest->m_pArray = &lComp;
    pClosest->m_Point  = Point;
    pClosest->m_R      = Radious;
    pClosest->m_Tx     = Tx;

    return pClosest;
}
