//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef PHYSICS_COMPONENT_H
#define PHYSICS_COMPONENT_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "gb_base.h"
#include "physicsmgr.h"

extern physics_mgr g_PhysicsMgr;

/////////////////////////////////////////////////////////////////////////////////
// INTERFACE TRANSFORM
/////////////////////////////////////////////////////////////////////////////////
class i_2d_transform
{
public:
    x_rtti_base(i_2d_transform);

    virtual xvector2    getPos           ( s32 Tx )=0;
    virtual xvector2    getVel           ( s32 Tx )=0;

protected:

    virtual void		setVel           ( const xvector2& Vel )=0;
    virtual void		setPos           ( const xvector2& Pos )=0;
};

/////////////////////////////////////////////////////////////////////////////////
// Physics Component
/////////////////////////////////////////////////////////////////////////////////
class physics_component : public gb_component, 
                          public i_2d_transform
{
public:
    GB_COMPONENT_RTTI2_CONTEXT( physics_component, gb_component, i_2d_transform );
    GB_COMPONENT_TYPEBASE( g_PhysicsMgr, XMEM_FLAG_ALIGN_8B )

public:

    //
    // Public access variables
    //
    struct context : public gb_component::context
    {
                            context         ( void )            {}
        virtual            ~context         ( void )            {}
        virtual void        vTransferData   ( const gb_component::context& Src );

        xvector2            m_Pos;
        xvector2            m_Vel;
    };

public:

										physics_component       ( void );
    virtual                            ~physics_component       ( void );

            void                        setupPosition           ( const xvector2& Pos );
            void                        setupVelocity           ( const xvector2& Vel );

    virtual xvector2					getPos                  ( s32 Tx ) { return getReadContextTx(Tx).m_Pos; }
    virtual xvector2					getVel                  ( s32 Tx ) { return getReadContextTx(Tx).m_Vel; }
            xrect						getBounds               ( f32 delta );

protected:

    struct pos_vel
    {
        xvector2            m_Pos;
        xvector2            m_Vel;
        physics_component*  m_pComponent;
    };

protected:

    virtual void						setVel                  ( const xvector2& Vel );
    virtual void						setPos                  ( const xvector2& Pos );

    virtual void                        vSetup                  ( void );
    virtual void                        vResolveDependencies    ( void ) {}
    virtual void                        vLoadComponent          ( xtextfile& File );

    void								onUpdate                ( const gb_msg& Msg );

    virtual void                        HandleCollisions        ( pos_vel& At0, pos_vel& At1,
                                                                  pos_vel& Bt0, pos_vel& Bt1 ) { ASSERT(0); }
protected:

    u32                     m_bIgnoreCollisions:1,
                            m_bHandleCollisions:1;

    friend struct physics_island;
};

/////////////////////////////////////////////////////////////////////////////////
// MESSAGES
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

class physics_onupdate_msg : public gb_msg
{
public:
	GB_MSG_UID(physics_onupdate_msg);

    xvector2    m_NewPos;
    xvector2    m_NewVel;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif