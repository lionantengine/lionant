//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#ifndef ENTITY_DOT_H
#define ENTITY_DOT_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDE
/////////////////////////////////////////////////////////////////////////////////
#include "gb_base.h"
#include "PhysicsComponent.h"

/////////////////////////////////////////////////////////////////////////////////
// ENTITY DOT
/////////////////////////////////////////////////////////////////////////////////
class entity_dot : public gb_entity
{
public:
    GB_COMPONENT_RTTI1_PRESETS( entity_dot, gb_entity );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

                        entity_dot              ( void );

public:

	struct presets : public gb_entity::presets
	{
		xguid      m_gBullet;
		
						presets			( void ){}
        virtual void    vLoad           ( xtextfile& File );
        virtual void    vSave           ( xtextfile& File );
	};

protected:

    virtual void                    vSetup                  ( void ) {}
    virtual void                    vResolveDependencies    ( void );
            void                    vUpdate                 ( void );
    virtual void                    vSaveComponent          ( xtextfile& File );
    virtual void                    vLoadComponent          ( xtextfile& File );
            void                    onUpdate                ( const gb_msg& msg ) {}

protected:

    physics_component*              m_pPhysics;
    f32                             m_Timer;
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif