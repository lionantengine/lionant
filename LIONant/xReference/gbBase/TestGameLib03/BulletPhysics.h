//
//  Created by Tomas Arce on 11/29/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#ifndef BULLET_PHYSICS_H
#define BULLET_PHYSICS_H

/////////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////////
#include "gb_base.h"
#include "physicsmgr.h"
#include "PhysicsComponent.h"

/////////////////////////////////////////////////////////////////////////////////
// BULLET PHYSICS
/////////////////////////////////////////////////////////////////////////////////

class bullet_physics : public physics_component
{
    GB_COMPONENT_RTTI1( bullet_physics, physics_component );
    GB_COMPONENT_TYPE( XMEM_FLAG_ALIGN_8B )

public:
                                        bullet_physics          ( void );

            void                        setupParent             ( xguid Guid );

protected:

    virtual void                        HandleCollisions        ( pos_vel& At0, pos_vel& At1,
                                                                  pos_vel& Bt0, pos_vel& Bt1 );

protected:

    xguid       m_gParent;                  // The thing that just shoted this bullet from
};

/////////////////////////////////////////////////////////////////////////////////
// END
/////////////////////////////////////////////////////////////////////////////////
#endif