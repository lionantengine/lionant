//////////////////////////////////////////////////////////////
// simple Duffuse bump - adapted from the nVidia cgFX samples
// This sample supports Render To Texture and is setup to use
// Mapping channel 3 - it just needs to be uncommented.
//////////////////////////////////////////////////////////////

//
// Material properties
// http://help.autodesk.com/view/3DSMAX/2015/ENU/?guid=__files_GUID_D6E12734_4D95_4C03_932A_6BA6080B65E8_htm
//
bool _000_Instance <
    string UIName = "Instance Material";
> = true;

texture _100_diffuseTexture : DiffuseMap< 
    string UIName       = "Diffuse Texture";
	string name 		= "C:\\RationWorls\\LIONant\\xReference\\Data\\GameData\\General\\Mesh\\RPG\\Tex\\Tex_0006_1.png"; 
    string ResourceType = "2D";
	>;
	
texture _101_normalMap : NormalMap < 
    string UIName       = "Normal Texture";
	string name 		= "C:\\RationWorls\\LIONant\\xReference\\Data\\GameData\\General\\Mesh\\RPG\\Tex\\Tex_0006_3.png"; 
    string ResourceType = "2D";
>;

float4 _102_ambientColor : Ambient
<
    string UIName = "Ambient Light";
> = { 0.1, 0.1, 0.1, 1.0};

//
// Include the actual material
//
#define _INSTANCE_
#include "M-DiffuseBump--40us_zb65_rhur.fx"