//////////////////////////////////////////////////////////////
// simple Duffuse bump - adapted from the nVidia cgFX samples
// This sample supports Render To Texture and is setup to use
// Mapping channel 3 - it just needs to be uncommented.
//////////////////////////////////////////////////////////////
float4x4 worldMatrix        : 	World;	        // World or Model matrix
float4x4 WorldIMatrix       : 	WorldI;	        // World Inverse or Model Inverse matrix
float4x4 mvpMatrix          : 	WorldViewProj;	// Model*View*Projection
float4x4 worldViewMatrix    : 	WorldView;
float4x4 viewInverseMatrix  :	ViewI;

//
// Deal with possible instances
//
#ifndef _INSTANCE_

	//
	// Material properties
	// http://help.autodesk.com/view/3DSMAX/2015/ENU/?guid=__files_GUID_D6E12734_4D95_4C03_932A_6BA6080B65E8_htm
	//
	texture _100_diffuseTexture : DiffuseMap< 
		string UIName       = "Diffuse Texture";
		string ResourceType = "2D";
		>;
		
	texture _101_normalMap : NormalMap < 
		string UIName       = "Normal Texture";
		string ResourceType = "2D";
	>;

	float4 _102_ambientColor : Ambient
	<
		string UIName = "Ambient Light";
	> = { 0.1, 0.1, 0.1, 1.0};

#endif

//
// Additional non instance related params
//
float4 lightDir : Direction <  
	string UIName = "Light Direction"; 
	string Object = "TargetLight";
	int     RefID = 0;
	> = {-0.577, -0.577, 0.577,1.0};

//
// Vertex Desc
//
struct a2v 
{
	float4 Position : POSITION;         // in object space
	float3 Normal   : NORMAL;           // in object space
	float2 TexCoord : TEXCOORD0;
	float3 T        : TANGENT;          // in object space
	float3 B        : BINORMAL;         // in object space
};

struct v2f 
{
	float4 Position     : POSITION;     // in projection space
	float2 TexCoord0    : TEXCOORD0;
	float3 LightVector  : TEXCOORD1;
};

struct f2fb 
{
	float4 col : COLOR;
};

//#define LIGHT_AS_COLOR

//
// Vertex Shader
//
v2f DiffuseBumpVS( a2v IN,
    uniform float4x4 WorldViewProj,
    uniform float4x4 WorldIMatrix,
    uniform float4   LightDir )
{
	v2f OUT;

	// pass texture coordinates for fetching the diffuse map
	OUT.TexCoord0.xy = IN.TexCoord.xy;

	// compute the 3x3 tranform from tangent space to object space
	float3x3 objToTangentSpace;
	objToTangentSpace[0] = IN.B;
	objToTangentSpace[1] = IN.T;
	objToTangentSpace[2] = IN.Normal;

    float4 objectLightDir   = mul( LightDir, WorldIMatrix );
	float3 vertnormLightVec = normalize( objectLightDir.xyz );

	// transform light vector from object space to tangent space and pass it as a color 
#ifdef LIGHT_AS_COLOR
	OUT.LightVector.xyz = 0.5 * mul( vertnormLightVec.xyz, objToTangentSpace ) + 0.5.xxx;
#else
   // objToTangentSpace = transpose( objToTangentSpace );
	OUT.LightVector.xyz = mul( objToTangentSpace, vertnormLightVec.xyz );
#endif
	// transform position to projection space
	OUT.Position = mul( IN.Position, WorldViewProj).xyzw;

	return OUT;
}

sampler2D diffuseSampler = sampler_state
{
    Texture = <_100_diffuseTexture>;


    /*
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    ADDRESSU = WRAP;
    ADDRESSV = WRAP;
    */
};

sampler2D normalSampler = sampler_state
{
    Texture = <_101_normalMap>;

    /*
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
    AddressW = Clamp;

    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    ADDRESSU = WRAP;
    ADDRESSV = WRAP;
    */
};

//
// Fragment Shader
//
f2fb DiffuseBumpPS( 
    v2f                 IN,
    uniform sampler2D   uDiffuseMap,
    uniform sampler2D   uNormalMap,
    uniform float4      uAmbientColor )
{
    f2fb OUT;

    //fetch base color
    float4 color = tex2D( uDiffuseMap, IN.TexCoord0 );

    //fetch bump normal
    float4 cNormalMap = tex2D( uNormalMap, IN.TexCoord0 );
    float3 bumpNormal = ( 2 * ( cNormalMap.xyz - 0.5 ) );
 
    //expand iterated light vector to [-1,1]
#ifdef LIGHT_AS_COLOR
      float3 lightVector = normalize( 2.0 * ( IN.LightVector - 0.5) );
#else
	  float3 lightVector = normalize( IN.LightVector );
#endif

    //compute final color (diffuse + ambient)
    float4 lightIntensity = clamp( dot( bumpNormal.xyz, lightVector.xyz ), 0, 1 );
    OUT.col.xyz = ( color * lightIntensity ) + ( color * uAmbientColor );
    OUT.col.a   = uAmbientColor.a;

    return OUT;
}

//
// Supported Techniques
//
technique11 Main 
<
    string Script = "Pass=p0;";
> 
{
    pass p0 
    <
        string Script = "Draw=geometry;";
    >
    {
        SetGeometryShader( NULL );
        VertexShader = compile vs_4_0 DiffuseBumpVS( mvpMatrix, WorldIMatrix, lightDir );
        PixelShader  = compile ps_4_0 DiffuseBumpPS( diffuseSampler, normalSampler, _102_ambientColor ); 
    }
}

