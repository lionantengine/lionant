cd "$(dirname "$0")"

../Compilers/animationCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/fontCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/informedMaterialCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/materialCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/meshCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/shaderCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/spriteCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/textureCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
../Compilers/skeletonCompiler -OVERWRITE_DEFAULTS -PROJECT "$(dirname "$0")"/../..
