cd %CD%
SET MSBUILD=C:\Program Files (x86)\MSBuild\12.0\Bin\MsBuild.exe
IF EXIST "%MSBUILD%" GOTO :PROCESS
:NOMSB
echo. 
echo MSBUILD not found 
echo. 
GOTO :PAUSE
:PROCESS

"%MSBUILD%" "%CD%\..\..\..\..\Compilers\fontCompiler\fontCompiler.vs2013\fontCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\informedMaterialCompiler\informedMaterialCompiler.vs2013\informedMaterialCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\materialCompiler\materialCompiler.vs2013\materialCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\shaderCompiler\shaderCompiler.vs2013\shaderCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\shaderCompiler\shaderCompiler.vs2013\shaderCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\SpriteCompiler\SpriteCompiler.vs2013\SpriteCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\TextureCompiler\TextureCompiler.vs2013\TextureCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 

if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\MeshCompiler\MeshCompiler.vs2013\MeshCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\animationCompiler\animationCompiler.vs2013\AnimationCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\SkeletonCompiler\SkeletonCompiler.vs2013\SkeletonCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE

"%MSBUILD%" "%CD%\..\..\..\..\Compilers\RscBuild\RscBuild.vs2013\RscBuild.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE

.\CopyNewCompilers.bat

:PAUSE
pause 
