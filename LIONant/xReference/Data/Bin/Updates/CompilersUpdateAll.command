cd "$(dirname "$0")"

### REFERENCE FOR THE XCODEBUILD
# https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man1/xcodebuild.1.html

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/RscBuild/RscBuild.xcodeproj -target "RscBuildOSX-64-EDITOR" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/RscBuild-asksxckzbbngevextuhyplqxdbol/Build/Products
if [[ $? -ne 0 ]]; 
	then goto error
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/fontCompiler/fontCompiler.xcodeproj -target "fontCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/fontCompiler-fidvgzjjsmryyxhcloqrjspswjxx/Build/Products
if [[ $? -ne 0 ]]; 
	then goto error
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/textureCompiler/textureCompiler.xcodeproj -target "TextureCompilerOSX-64-EDITOR" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/TextureCompiler-fbdetivgagwgrjerfgfaclpappsl/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/spriteCompiler/spriteCompiler.xcodeproj -target "SpriteCompilerOSX-64-EDITOR" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/SpriteCompiler-egccqadcbyivemfdwxlvliwgbicr/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/shaderCompiler/shaderCompiler.xcodeproj -target "shaderCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/shaderCompiler-gdrrumdjjdfuwmgfnlwhhekszkcm/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/materialCompiler/materialCompiler.xcodeproj -target "materialCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/materialCompiler-eeknkhoigwfdpebullkvzelyogub/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/informedmaterialCompiler/informedmaterialCompiler.xcodeproj -target "informedMaterialCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/informedMaterialCompiler-ejqrvsjeordzzpbcfnhcqkfxuind/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/meshCompiler/meshCompiler.xcodeproj -target "MeshCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/MeshCompiler-gcuvsxibywppgwcbwaabwsffuynu/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/skeletonCompiler/skeletonCompiler.xcodeproj -target "skeletonCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/skeletonCompiler-bbfgqlmiskljjhfadvuqihuuwexa/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi

xcodebuild -project "$(dirname "$0")"/../../../../Compilers/animationCompiler/animationCompiler.xcodeproj -target "animationCompiler" -configuration Debug BUILD_DIR=/Users/Tomas/Library/Developer/Xcode/DerivedData/animationCompiler-dqyxxjxbxhcsmacqmaxtafqkleoo/Build/Products
if [[ $? -ne 0 ]]; 
	then exit
fi


# -sdk iphonesimulator7.0 BUILD_DIR=/Users/jnaguin/Library/Developer/Xcode/DerivedData/Project-fkcaraphakyczyfrezqbqpbipwax/Build/Products CONFIGURATION_TEMP_DIR=/Users/jnaguin/Library/Developer/Xcode/DerivedData/Project-fkcaraphakyczyfrezqbqpbipwax/Build/Intermediates/Project.build/Debug-iphonesimulator build
