cd %CD%

..\Compilers\animationCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\fontCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\informedMaterialCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\materialCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\MeshCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\shaderCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\SpriteCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\TextureCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\.."
..\Compilers\SkeletonCompiler.exe -OVERWRITE_DEFAULTS -PROJECT "%CD%\..\..

pause