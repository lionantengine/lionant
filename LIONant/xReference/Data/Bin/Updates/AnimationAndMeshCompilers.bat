@echo OFF
cd %CD%
SET MSBUILD=C:\Program Files (x86)\MSBuild\12.0\Bin\MsBuild.exe
IF EXIST "%MSBUILD%" GOTO :PROCESS
:NOMSB
echo. 
echo MSBUILD not found 
echo. 
GOTO :PAUSE
:PROCESS

@echo ON
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\animationCompiler\animationCompiler.vs2013\AnimationCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\MeshCompiler\MeshCompiler.vs2013\MeshCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE
"%MSBUILD%" "%CD%\..\..\..\..\Compilers\SkeletonCompiler\SkeletonCompiler.vs2013\SkeletonCompiler.sln" /t:build /p:configuration=Debug /p:Platform="x64" /verbosity:minimal 
if %ERRORLEVEL% GEQ 1 goto :PAUSE

@echo | call .\CopyNewCompilers.bat

cd..
@echo | call PC_RscBuild-DEBUG.bat

:PAUSE
pause 
