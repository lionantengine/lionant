#include "ui_base.h"

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

static eng_keyboard                 s_Keyboard;
static eng_mouse                    s_Mouse;
static eng_touchpad                 s_TouchPad;

static uimanager                    s_UIManager;

//-------------------------------------------------------------------------------

struct test : public xmsg_receiver
{
    X_MESSAGE_MAP_DECL
    
    void OnStartUp( const uiregion::on_startup& Msg )
    {
        //s32 a=5;
    }
};

X_BEGIN_MESSAGE_MAP( test, xmsg_receiver )                               // Message declaration table
X_MESSAGE( uiregion::on_startup, OnStartUp, 0 )                    // 1 is the button sub_id (when not id needed set to 0)
X_END_MESSAGE_MAP


//-------------------------------------------------------------------------------

void Render( void )
{
    static f64 RTSec =  0;
    static f64 LastTime = x_GetTimeSec();

    s32 XRes, YRes;
    eng_GetCurrentContext().GetScreenResolution( XRes, YRes );

    // Skip to next test
    if( s_Keyboard.wasPressed( eng_keyboard::KEY_SPACE ) )
    {
        RTSec = 0;
    }
    
    // Precision is not an issue
    f32 DeltaTime( f32((x_GetTimeSec() - LastTime)/1000.0f) );
    LastTime  = x_GetTimeSec();
    
    xvector2 Pos( s_Mouse.getValue( eng_mouse::ANALOG_POS_ABS ) );
    
    Pos.m_Y = YRes - Pos.m_Y;
    s_UIManager.Input( Pos, s_Mouse.isPressed( eng_mouse::BTN_LEFT ), s_Mouse.wasPressed( eng_mouse::BTN_LEFT ) );
    s_UIManager.Render( DeltaTime );

#ifdef X_DEBUG
    x_printfxy( 0, 1, "DEBUG! %f %f", Pos.m_X, Pos.m_Y );
#endif 
}

//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init();
    
    //
    // Init the engine
    //
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
    
    uicontainer_area&                   Container       = s_UIManager.CreateRegion<uicontainer_area>();
    uicontrol_button&                   Region          = s_UIManager.CreateRegion<uicontrol_button>( Container );
    uicontrol_style_sheet&              StyleSheet      = s_UIManager.CreateStyle<uicontrol_style_sheet>();
    uicontrol_style_sheet&              StyleSheet1     = s_UIManager.CreateStyle<uicontrol_style_sheet>();
    uicontrol_style_sheet&              StyleSheet2     = s_UIManager.CreateStyle<uicontrol_style_sheet>();
    uistyle_background&                 Background      = s_UIManager.CreateStyle<uistyle_background>();
    uistyle_background&                 Background1     = s_UIManager.CreateStyle<uistyle_background>();
    uistyle_background&                 Background2     = s_UIManager.CreateStyle<uistyle_background>();
    uistyle_boder&                      Frame           = s_UIManager.CreateStyle<uistyle_boder>();
    uistyle_boder&                      Margin          = s_UIManager.CreateStyle<uistyle_boder>();
    uistyle_content&                    ContentStyle    = s_UIManager.CreateStyle<uistyle_content>();
    uicontrol_base_style&               UIStyle         = s_UIManager.CreateStyle<uicontrol_base_style>();
    uistyle_transform&                  Transform       = s_UIManager.CreateStyle<uistyle_transform>();
    uicontrol_style_transition_boing&   Boing           = s_UIManager.CreateStyle<uicontrol_style_transition_boing>();
    uicontent_text&                     TextContent     = s_UIManager.CreateContent<uicontent_text>();
    uicontent_sprite&                   SpriteContent   = s_UIManager.CreateContent<uicontent_sprite>();
    uistyle_font&                       StyleFont       = s_UIManager.CreateStyle<uistyle_font>();

    StyleFont.m_Font.setup          ( X_GUID( 3ds2_xb7l_gkdb ), "OpenSans-Regular" );  
    SpriteContent.m_Sprite.setup      ( X_GUID( 1ui1_jzcw_tukx ), "009" );

    ContentStyle.m_Color.Set        ( ~0 );
    ContentStyle.m_HAligment        = uistyle_content::HALIGMENT_LEFT;
    ContentStyle.m_VAligment        = uistyle_content::VALIGMENT_TOP;
    ContentStyle.m_gContentStyle    = StyleFont.getGuid();
    ContentStyle.m_ContentScale     = xvector2(0.5f);
    
    Background.setColor             ( xcolor( 57, 57, 57, 255 )     );
    Frame.setColor                  ( xcolor(~0)                    );
    Margin.setColor                 ( xcolor( 255,255,0,255)        );
    
    StyleSheet.setBackground        ( Background.getGuid()   );
    StyleSheet.setFrame             ( Frame.getGuid()        );
    StyleSheet.setMargin            ( Margin.getGuid()       );
    StyleSheet.setElement           ( ContentStyle.getGuid() );

    StyleSheet.setFrame             ( xrect( 2,2,2,2) );
    StyleSheet.setMargin            ( xrect( 5,5,5,5) );
    StyleSheet.setPadding           ( xrect( 5,5,5,5) );
    
    StyleSheet1                     = StyleSheet;
    StyleSheet2                     = StyleSheet;

    TextContent.m_String.Copy        ( "The quick brown fox jumps over the lazy dog. 1234567890" );

    Region.setContent               ( TextContent.getGuid() );
    Region.setRect                  ( uirect( uirect::TYPE_ABS_DELTA, xrect( 10, 10, 10, 100 ) ) );
    Region.setStyleSheet            ( UIStyle.getGuid() );
    
    StyleSheet.setTimeToTransition  ( 0.0f );
    StyleSheet1.setTimeToTransition ( 0.1f );
    StyleSheet2.setTimeToTransition ( 0.0f );
    
    Background1.setColor            ( xcolor (200, 0,   0, 255 )  );
    Background2.setColor            ( xcolor (0, 255, 0, 255 )  );
    
    StyleSheet1.setBackground       ( Background1.getGuid() );
    //StyleSheet2.setGeometryTransition( Boing.getGuid() );
    //StyleSheet1.setGeometryTransition( Boing.getGuid() );
    UIStyle.setGeometryTransition   ( Boing.getGuid() );

    StyleSheet2.setBackground       ( Background2.getGuid() );
    
    
    UIStyle.setStyle                ( uicontrol_base_style::STATE_DISABLE,  StyleSheet.getGuid() );
    UIStyle.setStyle                ( uicontrol_base_style::STATE_HOVER,    StyleSheet1.getGuid() );
    UIStyle.setStyle                ( uicontrol_base_style::STATE_ACTIVE,   StyleSheet.getGuid() );
    UIStyle.setStyle                ( uicontrol_base_style::STATE_SELECTED,   StyleSheet2.getGuid() );

    Region.setState                 ( uicontrol_base_style::STATE_ACTIVE );
    
    Container.setRect               ( uirect( uirect::TYPE_ABSOLUTE, xrect( 150, 150, 700, 400 ) ) );
    
    Transform.m_KeyFrame.Identity   ( );
    Transform.m_KeyFrame.m_Scale.Set( 0.9f );
    Container.setStyleSheet         ( StyleSheet2.getGuid() );
    StyleSheet2.setTransform        ( Transform.getGuid() );
    
    //
    // Init the engine
    //
    eng_context& DisplayContext         = eng_CreateContext();
    DisplayContext.PresetSetRes         ( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle   ( MainWindow );
    DisplayContext.Init                 ( );

    eng_SetCurrentContext               ( DisplayContext );

    // Now we will sleep for a while then exit
    f32 r = 1;
    while( DisplayContext.HandleEvents() )
    {
        //x_Sleep( 30 );
        // Handle window been resize
        eng_SetCurrentContext(DisplayContext);
        g_Scheduler.MainThreadRunJobs( TRUE, TRUE, FALSE );

 //       Transform.m_KeyFrame.m_Rotation.RotateZ( 0.005f );
 //       Transform.m_KeyFrame.m_Scale.Set( 0.3f+x_Abs(sin(r+=0.004f)) );
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();

        DisplayContext.PageFlip(TRUE);
        x_Sleep( 16 );
    }
}