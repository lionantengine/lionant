//
//  vector3d.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "vector3d.h"

//-------------------------------------------------------------------------

void vector3d::Dot( void )
{
    xvector3d A( 100, 0, 0 );
    f32 dis = x_Sqrt( A.Dot(A) );
    CPPUNIT_ASSERT( x_FEqual( 100.0f, dis ) );
}

//-------------------------------------------------------------------------

void vector3d::GetLength( void )
{
    xvector3d A( 0, 100, 0 );
    f32 dis = A.GetLength();
    CPPUNIT_ASSERT( x_FEqual( 100.0f, dis ) );
}

//-------------------------------------------------------------------------

void vector3d::GetLengthSquared( void )
{
    xvector3d A( 0, 0, 100 );
    f32 dis = A.GetLengthSquared();
    CPPUNIT_ASSERT( x_FEqual( 100.0f*100.0f, dis ) );
}

//-------------------------------------------------------------------------

void vector3d::GetPitch( void )
{
    xradian R;
    
    for( R=-PI2; R<PI_OVER2; R += X_RADIAN(1) )
    {
        xvector3d A( 0, 0, 1 );
        A.RotateX(R);
        xradian R2 = A.GetPitch();
        
        R  = x_ModAngle(R+0.00001f);
        R2 = x_ModAngle(R2+0.00001f);
        CPPUNIT_ASSERT( x_FEqual( R, R2, X_RADIAN(1) ) );
    }
    
    CPPUNIT_ASSERT( R >= PI_OVER2 );
}
    
//-------------------------------------------------------------------------

void vector3d::GetYaw( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector3d A( 0, 0, 1 );
        A.RotateY(R);
        xradian R2 = A.GetYaw();
        
        R  = x_ModAngle(R+0.00001f);
        R2 = x_ModAngle(R2+0.00001f);
        CPPUNIT_ASSERT( x_FEqual( R, R2 ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector3d::RotateX( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector3d A( 0, 0, 1 );
        A.RotateX(R);
        CPPUNIT_ASSERT( x_FEqual( A.m_Z, x_Cos(R) ) && x_FEqual( A.m_Y, -x_Sin(R) ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector3d::RotateY( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector3d A( 0, 0, 1 );
        A.RotateY(R);
        CPPUNIT_ASSERT( x_FEqual( A.m_Z, x_Cos(R) ) && x_FEqual( A.m_X, x_Sin(R) ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector3d::RotateZ( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector3d A( 1, 0, 0 );
        A.RotateZ(R);
        CPPUNIT_ASSERT( x_FEqual( A.m_X, x_Cos(R) ) && x_FEqual( A.m_Y, x_Sin(R) ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector3d::Normalize( void )
{
    s32   i;
    for( i=0; i<1000; i++ )
    {
        xvector3d A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.Normalize();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
    
    CPPUNIT_ASSERT( i >= 1000 );
}

//-------------------------------------------------------------------------

void vector3d::NormalizeSafe( void )
{
    s32 i;
    for(  i=0; i<1000; i++ )
    {
        xvector3d A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.NormalizeSafe();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
    
    CPPUNIT_ASSERT( i >= 1000 );
}

//-------------------------------------------------------------------------

void vector3d::Operators( void )
{
    xvector3d A( 100, 0, 0), B(1,1,1), C;
    
    B = (B+B) / xvector3d( 2 );
    C = A * B;
    C *= C;
    B -= B;
    C += B;
    
    // x_Math::xvector3d.+-*etc OKAY
    CPPUNIT_ASSERT( x_FEqual( C.GetLength(), A.GetLengthSquared() ) );
}
