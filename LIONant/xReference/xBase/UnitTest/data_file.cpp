//
//  data_file.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "data_file.h"


//------------------------------------------------------------------------------
// TYPES
//------------------------------------------------------------------------------

struct data1
{
    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize( m_A );
    }
    s16     m_A;
};

struct data3
{
    void SerializeIO( xserialfile& SerialFile ) const
    {
        SerialFile.Serialize( m_Count );
        SerialFile.Serialize( m_Data, m_Count );
    }
    
    s32     m_Count;
    xserialfile::ptr<data1>  m_Data;
};

struct data2 : public data1
{
    // data
    data3                   m_GoInStatic;
    data3                   m_DontDynamic;
    xsafe_array<data3,8>    m_GoTemp;

    enum
    {
        DYNAMIC_COUNT = (1024*1024)/sizeof(data1) + 4,
        STATIC_COUNT  = (1024*1024)/sizeof(data1) + 4,
        STATIC_MAX    = 0xffffffff
    };
    
    // Initialize all the data
    data2( void )
    {
        m_A = 100;
        
        m_DontDynamic.m_Count = DYNAMIC_COUNT;
        m_DontDynamic.m_Data.m_Ptr = (data1*)x_malloc( sizeof(data1), m_DontDynamic.m_Count, 0 );
        for( s32 i=0; i<m_DontDynamic.m_Count; i++ ) { m_DontDynamic.m_Data.m_Ptr[i].m_A = 22+i; }
        
        m_GoInStatic.m_Count = STATIC_COUNT;
        m_GoInStatic.m_Data.m_Ptr = (data1*)x_malloc( sizeof(data1), m_GoInStatic.m_Count, 0 );
        for( s32 i=0; i<m_GoInStatic.m_Count; i++ ) { m_GoInStatic.m_Data.m_Ptr[i].m_A = (s16)(100/(i+1)); }

        for ( data3& Temp : m_GoTemp )
        {
            Temp.m_Count = STATIC_COUNT;
            Temp.m_Data.m_Ptr = (data1*)x_malloc( sizeof( data1 ), Temp.m_Count, 0 );
            for ( s32 i = 0; i<Temp.m_Count; i++ ) { Temp.m_Data.m_Ptr[ i ].m_A = (s16)( 100 / ( i + 1 ) ); }
        }
    }
    
    // Sanity check
    void SanityCheck( void )
    {
        CPPUNIT_ASSERT( m_A == 100 );
        
        CPPUNIT_ASSERT( m_DontDynamic.m_Count == DYNAMIC_COUNT );
		for (s32 i = 0; i < m_DontDynamic.m_Count; i++)
		{
			if (m_DontDynamic.m_Data.m_Ptr[i].m_A != (s16)(22 + i))
			{
				CPPUNIT_ASSERT(m_DontDynamic.m_Data.m_Ptr[i].m_A == (s16)(22 + i));
			}            
        }
        
        CPPUNIT_ASSERT( m_GoInStatic.m_Count == STATIC_COUNT );
        for( s32 i=0; i<m_GoInStatic.m_Count; i++ )
        {
			if (m_GoInStatic.m_Data.m_Ptr[i].m_A != (s16)(100 / (i + 1)))
			{
				CPPUNIT_ASSERT(m_GoInStatic.m_Data.m_Ptr[i].m_A == (s16)(100 / (i + 1)));
			}
        }

        for ( data3& Temp : m_GoTemp )
        {
            CPPUNIT_ASSERT( Temp.m_Count == STATIC_COUNT );
            for ( s32 i = 0; i<Temp.m_Count; i++ )
            {
                if ( Temp.m_Data.m_Ptr[ i ].m_A != (s16)( 100 / ( i + 1 ) ) )
                {
                    CPPUNIT_ASSERT( Temp.m_Data.m_Ptr[ i ].m_A == (s16)( 100 / ( i + 1 ) ) );
                }
            }
        }
    }
    
    // Save the data
    void SerializeIO( xserialfile& SerialFile ) const
    {
        // Make sure that it is the first version
        SerialFile.SetResourceVersion( 1 );
        
        // Tell the structure to save it self
        SerialFile.Serialize( m_GoInStatic );
        
        // Don't always need to go into structures
        SerialFile.Serialize ( m_DontDynamic.m_Count );
        SerialFile.Serialize( m_DontDynamic.m_Data, m_DontDynamic.m_Count, xserialfile::FLAGS_UNIQUE );
        
        // Serialize the temp
        for ( const data3& Temp : m_GoTemp )
        {
            SerialFile.Serialize( Temp.m_Count );
            SerialFile.Serialize( Temp.m_Data, Temp.m_Count, xserialfile::FLAGS_TEMP_MEMORY );
        }

        // Tell our parent to save it self
        data1::SerializeIO( SerialFile );
    }
    
    // This is the loading constructor by the time is call the file already loaded
    data2( xserialfile& SerialFile )
    {
        CPPUNIT_ASSERT( SerialFile.GetResourceVersion() == 1 );
        
        // *** Only reason to have a something inside this constructor is to deal with dynamic data
        // We move the memory to some other random place
        data1*  pData = (data1*)x_malloc( sizeof(data1),m_DontDynamic.m_Count, 0);
        x_memcpy( pData, m_DontDynamic.m_Data.m_Ptr, m_DontDynamic.m_Count*sizeof(data1) );
        
        // Now we can overwrite the dynamic pointer without a worry
        x_free(m_DontDynamic.m_Data.m_Ptr);
        m_DontDynamic.m_Data.m_Ptr = pData;

        // make sure everything is ok
        SanityCheck();
    }
    
    ~data2( void )
    {
        // Here to deal with dynamic stuff
        if(m_DontDynamic.m_Data.m_Ptr) x_free( m_DontDynamic.m_Data.m_Ptr );
    }
};


//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void data_file::SerialFileTest( void )
{
    xstring     FileName( X_STR("temp:/SerialFile.bin") );
    
    // Save
    {
        xserialfile SerialFile;
        data2     TheData;
        TheData.SanityCheck();
        SerialFile.Save( FileName, TheData );
    }
    
    // Load
    {
        xserialfile  SerialFile;
        data2*     pTheData;
        
        // This hold thing could happen in one thread
        SerialFile.Load( FileName, pTheData );
        
        // Okay one pointer to nuck
        x_delete( pTheData );
    }
}

//------------------------------------------------------------------------------

static const f32           af  = -50.0f;
static const f32           bf  = -0.0001f;
static const f32           cf  = -50000.0f;
static const s32           ai  = -50;
static const char* const   pT1  = "String Test";
static const char* const   pT2  = "TEST1";
static const u64           llI = 0xffffffff88888888LL;
static const f64           FF  = PI;


//------------------------------------------------------------------------------
static
void TextFileWrite( const xstring& FileName, u32 Flags )
{
    xtextfile   TextFile;
    
    TextFile.OpenForWriting  ( FileName, Flags );
    
    TextFile.WriteRecord( "FistRecord", 512 );
    for( s32 i=0; i<512; i++ )
    {
        TextFile.WriteField ( "Description:s", pT1 );
        TextFile.WriteField ( "Position:fff", af+i, bf+i*2, cf+i*3 );
        TextFile.WriteField ( "Number:d", ai+i );
        TextFile.WriteField ( "EnumParam:e", pT2 );
        TextFile.WriteLine  ();
    }
    
    //
    // Big types
    //
    TextFile.WriteRecord( "SecondRecord" );
    TextFile.WriteField ( "pepe:DF", llI, FF );
    TextFile.WriteLine  ();
    
    //
    // Dynamic Type
    //
    TextFile.WriteRecord( "TheirdRecord", 3 );
    
    // 1
    TextFile.WriteField( "type:<?>", "fff" );
    TextFile.WriteField( "value:<type>", af, bf, cf );
    TextFile.WriteLine ();
    
    // 2
    TextFile.WriteField( "type:<?>", "s" );
    TextFile.WriteField( "value:<type>", pT1 );
    TextFile.WriteLine ();
    
    // 3
    TextFile.WriteField( "type:<?>", "DF" );
    TextFile.WriteField( "value:<type>", llI, FF );
    TextFile.WriteLine ();

    //
    // Lets test user types
    //
    TextFile.AddUserType( "fff", ".V3",   0 );
    TextFile.AddUserType( "fff", ".V3D",  1 );
    TextFile.AddUserType( "f",   ".F",    2 );
    TextFile.AddUserType( "sf",  ".SS",   3 );

    TextFile.WriteRecord( "UsersTypes", 2 );
    
    // 0
    TextFile.WriteField ( "mytype:<?>", ".V3" );
    TextFile.WriteField ( "myvalue:<mytype>", af, bf, cf );
    TextFile.WriteLine  ();
    
    // 1
    TextFile.WriteField ( "mytype:<?>", ".SS" );
    TextFile.WriteField ( "myvalue:<mytype>", pT1, af );
    TextFile.WriteLine  ();
    
    // Other record
    TextFile.WriteRecord( "UsersTypes2" );
    TextFile.WriteField ( "One:.F", af );
    TextFile.WriteField ( "Two:.V3D", af, bf, cf );
    TextFile.WriteLine  ();
    
    TextFile.Close();
}

//------------------------------------------------------------------------------
static
void TextFileRead( const xstring& FileName )
{
    xtextfile   TextFile;
    
    TextFile.OpenForReading( FileName );
    CPPUNIT_ASSERT( TextFile.ReadRecord() );
    for( s32 i=0; i<TextFile.GetRecordCount(); i++ )
    {
        xstring String;
        xstring String2;
        f32 A, B, C;
        s32 I;
        TextFile.ReadLine();
        TextFile.ReadFieldXString ( "Description:s", String );
        TextFile.ReadField        ( "Position:fff", &A, &B, &C );
        TextFile.ReadField        ( "Number:d", &I );
        TextFile.ReadFieldXString ( "EnumParam:e", String2 );
        
        CPPUNIT_ASSERT( String == pT1 );
        CPPUNIT_ASSERT( x_FEqual( af+i, A ) );
        CPPUNIT_ASSERT( x_FEqual( bf+i*2, B ) );
        CPPUNIT_ASSERT( x_FEqual( cf+i*3, C ) );
        CPPUNIT_ASSERT( ai+i == I );
        CPPUNIT_ASSERT( String2 == pT2 );
    }
    
    //
    // Big types
    //
    u64  BigD;
    f64  BigF;
    f32  A, B, C;
    xstring String;
    xstring Typess;
    char Buff[256];
    
    CPPUNIT_ASSERT( TextFile.ReadRecord() );
    TextFile.ReadLine();
    TextFile.ReadField ( "pepe:DF", &BigD, &BigF );
	CPPUNIT_ASSERT(BigD == (f64)llI);
	CPPUNIT_ASSERT(x_FEqual((const f32)BigF, (const f32)FF));

    //
    // Dynamic Type
    //
    CPPUNIT_ASSERT( TextFile.ReadRecord() );
    
    // 1
    TextFile.ReadLine();
    TextFile.ReadField( "type:<?>", &Typess );
    TextFile.ReadField( "value:fff", &A, &B, &C );
    CPPUNIT_ASSERT( x_FEqual( af, A ) );
    CPPUNIT_ASSERT( x_FEqual( bf, B ) );
    CPPUNIT_ASSERT( x_FEqual( cf, C ) );
    
    // 2
    TextFile.ReadLine();
    TextFile.ReadField( "type:<?>", &Typess );
    TextFile.ReadFieldXString ( "value:s", String );
    CPPUNIT_ASSERT( String == pT1 );
    
    // 3
    TextFile.ReadLine();
    TextFile.ReadField( "type:<?>", &Typess );
    TextFile.ReadField( "value:DF", &BigD, &BigF );
	CPPUNIT_ASSERT(BigD == (f64)llI);
	CPPUNIT_ASSERT(x_FEqual((f32)BigF, (const f32)FF));
    
    //
    // Lets test user types
    //
    CPPUNIT_ASSERT( TextFile.ReadRecord() );
    TextFile.ReadLine();
    TextFile.ReadField ( "mytype:<?>", &Typess );
    TextFile.ReadField ( "myvalue:.V3", &A, &B, &C );
    CPPUNIT_ASSERT( x_FEqual( af, A ) );
    CPPUNIT_ASSERT( x_FEqual( bf, B ) );
    CPPUNIT_ASSERT( x_FEqual( cf, C ) );
    
    TextFile.ReadLine();
    TextFile.ReadField ( "mytype:<?>", &Typess );
    TextFile.ReadField ( "myvalue:.SS", &Buff, &A );
    CPPUNIT_ASSERT( x_FEqual( af, A ) );
    CPPUNIT_ASSERT( x_strcmp( Buff, pT1) == 0 );

    
    CPPUNIT_ASSERT( TextFile.ReadRecord() );
    TextFile.ReadLine();
    TextFile.ReadField ( "One:.F", &A );
    CPPUNIT_ASSERT( x_FEqual( af, A ) );
    
    TextFile.ReadField ( "Two:.V3D", &A, &B, &C);
    CPPUNIT_ASSERT( x_FEqual( af, A ) );
    CPPUNIT_ASSERT( x_FEqual( bf, B ) );
    CPPUNIT_ASSERT( x_FEqual( cf, C ) );
    
    TextFile.Close();
}

//------------------------------------------------------------------------------

void data_file::TextFileTest( void )
{
    const xstring TextFileName( X_STR( "temp:TextFileTest" ) ); 
    const xstring BinaryFileName( X_STR( "temp:BinaryFileTest" ) );

    //
    // Test write and read (Text Style)
    //
    TextFileWrite( TextFileName, 0 );
    TextFileRead( TextFileName );
    
    //
    // Test write and read (Binary Style)
    //
    TextFileWrite( TextFileName, xtextfile::FLAGS_BINARY );
    TextFileRead( TextFileName );

    //
    // Transform to binary format
    //
    xtextfile::TransformTo( TextFileName, BinaryFileName, xtextfile::FLAGS_BINARY );
    TextFileRead( BinaryFileName );
    
    //
    // Transform to text format
    //
    xtextfile::TransformTo( BinaryFileName, TextFileName, 0 );
    TextFileRead( TextFileName );
    
}

