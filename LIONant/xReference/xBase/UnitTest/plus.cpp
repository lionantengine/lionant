//
//  plus.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "plus.h"

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void plus::EncryptFunctionsTest( void )
{
    const char* pSomething = "What were you doing Sunday, February 17, 2002 around 2PM? Well, some of us went down to"
    "Tower Records in Sherman Oaks and saw Something Corporate perform live at an in store for"
    "the EP, 'Audioboxer.' I recorded this footage when I was with MCA Records but unfortunately,"
    "I never was able to get it up on the SoCo site because I lost the tape. Well, I found it. Four"
    "years later. So, here it is, enjoy.";
    
    
    
    s32 Level           = 1;
    s32 InLength        = x_strlen( pSomething );
    s32 KeySize         = x_EncryptComputeKeySize( Level );
    s32 EncryptDestSize = x_EncryptComputeDestDataSize(InLength);
    
    xptr<xbyte> Key;
    xptr<xbyte> Dest;
    xptr<xbyte> Out;
    
    Key.Alloc( KeySize );
    Dest.Alloc( EncryptDestSize );
    Out.Alloc( InLength );
    
    //
    // compute a key for the encryption
    //
    x_EncryptBuildKeyFromPassword( &Key[0], KeySize, Level, "My Password" );
    
    //
    // encrypt
    //
    x_EncryptMem( &Dest[0], EncryptDestSize, Level, &Key[0], KeySize, pSomething, InLength );
    
    //
    // dencrypt
    //
    x_DencryptMem( &Out[0], InLength, Level, &Key[0], KeySize, &Dest[0], EncryptDestSize );
    
    //
    // Compare both blocks
    //
    CPPUNIT_ASSERT( x_memcmp( pSomething, &Out[0], InLength ) == 0 );
}

//------------------------------------------------------------------------------

void plus::CompressionFunctionsTest( void )
{
    const char* pSomething = "What were you doing Sunday, February 17, 2002 around 2PM? Well, some of us went down to"
    "Tower Records in Sherman Oaks and saw Something Corporate perform live at an in store for"
    "the EP, 'Audioboxer.' I recorded this footage when I was with MCA Records but unfortunately,"
    "I never was able to get it up on the SoCo site because I lost the tape. Well, I found it. Four"
    "years later. So, here it is, enjoy.";
    s32         InLength        = x_strlen( pSomething ) + 1;
    xptr<xbyte> Decompress;
    xptr<xbyte> CompressBlock;
    s32         Size;
    
    CompressBlock.Alloc( InLength );
    CompressBlock.SetMemory( 0xBE );
    
    Size = x_CompressMem( CompressBlock, pSomething, InLength );
    
    // Fill bytes that should be not used with trash
    for( s32 i=Size; i<InLength; i++ )
    {
        CompressBlock[i] = 0xBE;
    }
    
    
    const f32 CompressionRatio = 100.0f*(1.0f - Size/(f32)InLength);
    
    CPPUNIT_ASSERT( CompressionRatio );
    Decompress.Alloc( InLength );
    Decompress.SetMemory(0xAA);
    
    // Copy to another buffer to test in place decompression
    // NO INPLACE DECOMPRESSION FOR LZ4
    // s32 Offset = InLength - Size;
    // x_memcpy( &Decompress[ Offset ], &CompressBlock[0], Size );
    
    // Now we can decompress
    x_DecompressMem( &Decompress[0], InLength, &CompressBlock[0], Size );
    
    //
    // Compare both blocks
    //
    CPPUNIT_ASSERT( x_memcmp( pSomething, &Decompress[0], InLength ) == 0 );
    
    //
    // One more test... what about data that can not be compress?
    //
    const char* pData = "12 345 6790 QSAEFG ";
    
    InLength = x_strlen( pData ) + 1;
    Size = x_CompressMem( CompressBlock, pData, InLength );
    CPPUNIT_ASSERT( Size == InLength );
    
    // Fill bytes that should be not used with trash
    for( s32 i=Size; i<InLength; i++ )
    {
        CompressBlock[i] = 0xBE;
    }

    // Now we can decompress
    x_DecompressMem( &Decompress[0], InLength, &CompressBlock[0], Size );
    CPPUNIT_ASSERT( x_memcmp( pData, &Decompress[0], InLength ) == 0 );
}

