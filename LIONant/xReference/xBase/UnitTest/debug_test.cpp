//
//  debug_test.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "debug_test.h"


//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void debug_test::DebugLevelTest( void )
{
    // These test conflic with the actual running of other test as we need to set the global xdebug to full
    xdebug_level Level = XDEBUG_LEVEL_BASIC;
    
    CPPUNIT_ASSERT( x_ChooseDebugLevel( Level ) == XDEBUG_LEVEL_BASIC );
    CPPUNIT_ASSERT( x_ChooseDebugLevel( Level ) == XDEBUG_LEVEL_BASIC );
    
    x_SetDebugLevel( XDEBUG_LEVEL_FORCE_FULL );
    
    CPPUNIT_ASSERT( x_ChooseDebugLevel( Level ) == XDEBUG_LEVEL_FULL );
    CPPUNIT_ASSERT( x_ChooseDebugLevel( Level ) == XDEBUG_LEVEL_FULL );
}

//------------------------------------------------------------------------------

void debug_test::LogFunctionsTest( void )
{
    x_LogMessage( "Test", "x_Debug::LogFunctions Testing Functions" );
    x_LogMessage( "Test", "This is a test" );
    x_LogError  ( "Test", "This is a test" );
    x_LogWarning( "Test", "This is a test" );
    x_LogMessage( "Test", "x_Debug::LogFunctions OK" );
}

//------------------------------------------------------------------------------

void debug_test::AssertsThrowsTest( void )
{
    x_try;
    
    int a;
    a=22;
    
    x_throw( "Bottom!!!!") ;
    x_catch_begin;
    x_append_exception_msg( "Testing the appending" );
    x_ExceptionDisplay();
    x_catch_end;
}
//------------------------------------------------------------------------------

void debug_test::CallStackTest( void )
{
    ASSERT(0); // TODO: Implementation needed
    /*
    for( s32 i=0; i<10; i++ )
    {
        char* pMemory = x_DebugGetCallStack();
        if( pMemory )
        {
            x_free( pMemory );
        }
    }
     */
}

