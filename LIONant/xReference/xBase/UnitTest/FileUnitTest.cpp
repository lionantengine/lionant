//
//  FileUnitTest.cpp
//  xBaseUnitTest
//
//  Created by Simon Hudson on 9/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifdef TARGET_3DS
#define  SYNC_MODE_FILE_NAME_PACH "data:/test.dat"
#define  ASYNC_MODE_FILE_NAME_PACH "data:/asyncMode.dat"
#define  DATA_SIZE 1024*225/4
#else
#define  SYNC_MODE_FILE_NAME_PACH "temp:test.dat"
#define  ASYNC_MODE_FILE_NAME_PACH "temp:asyncMode.dat"
#define  DATA_SIZE  1024*1024*10
#endif

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "FileUnitTest.h"

//-----------------------------------------------------------------------------------------

void FileUnitTest::syncModeTest()
{
    //
    // Write file
    //
    xfile file;
    xbool result = file.Open( X_STR(SYNC_MODE_FILE_NAME_PACH), "w");
    CPPUNIT_ASSERT_MESSAGE("Open file 'test.dat' failed!", result );
    
    const xstring header( X_STR("TestFileHeader") );
    file.Write(header);
    
    s32 FileSize = file.Tell();
    s32 RealSize = header.GetLength()+1;
    CPPUNIT_ASSERT( FileSize == RealSize );
    
    xsafe_array<s32, 3245> buffer;
    for ( s32 i = 0; i < buffer.getCount(); i++ )
    {
        buffer[i] = i;
    }
    
    file.Write( &buffer[0], buffer.getCount());
    
    FileSize  = file.Tell();
    RealSize += buffer.getCount()*sizeof(s32);
    CPPUNIT_ASSERT( FileSize == RealSize );
    
    file.Write( buffer.getCount() );
    
    FileSize  = file.Tell();
    RealSize += sizeof(s32);
    CPPUNIT_ASSERT( FileSize == RealSize );
    
    // Done
    file.Close();
    
    //
    // Clear the buffer
    //
    for( s32 i = 0; i < buffer.getCount(); i++ )
    {
        buffer[i] = 0;
    }
    
    //
    // Read file
    //
    result = file.Open( X_STR(SYNC_MODE_FILE_NAME_PACH), "r");
    CPPUNIT_ASSERT_MESSAGE( "Open file 'test.dat' failed!", result );
    
    FileSize  = file.GetFileLength();
    RealSize  = header.GetLength()+1;
    RealSize += sizeof(s32)*buffer.getCount() + sizeof(s32);
    CPPUNIT_ASSERT( FileSize == RealSize );
    
    // Read the first string
    xstring NewHeader;
    file.Read(NewHeader);
    CPPUNIT_ASSERT( NewHeader == header );
    
    // Read the buffer size
    s32 position = file.Tell();
    file.SeekCurrent( sizeof(s32)*buffer.getCount() );
    
    s32 BufferCount;
    file.Read( BufferCount );
    CPPUNIT_ASSERT( BufferCount == buffer.getCount() );
    
    // Read the buffer
    file.SeekOrigin( position );
    file.Read( &buffer[0], BufferCount );
    
    for( s32 i = 0; i < buffer.getCount(); i++ )
    {
        CPPUNIT_ASSERT(buffer[i] == i);
    }
    
    // Done
    file.Close();
}

//----------------------------------------------------------------------------------------------

void FileUnitTest::asyncModeTest()
{
    xptr<s32> Buffer;
    Buffer.Alloc(DATA_SIZE);
    
    for( s32 t=0; t<10; t++ )
    {
        //
        // Write something
        //
        if( 1 )
        {
            xfile file;
            
            //
            // Write Something
            //
            xbool result = file.Open(ASYNC_MODE_FILE_NAME_PACH, "w@");
            CPPUNIT_ASSERT_MESSAGE("Open file 'asyncMode.dat' for writting failed!", result );
            
            for ( s32 i = 0; i < Buffer.getCount(); i++ )
            {
                Buffer[i] = i;
            }
            
            s32 step = Buffer.getCount()/10;
            for( s32 i=0; i<10; i++ )
            {
                xbool bOK = file.Write( &Buffer[i*step], step );
                CPPUNIT_ASSERT( bOK );
                file.Synchronize(TRUE);
            }
            
            file.Close();
        }
        
        //
        // Clear buffer
        //
        Buffer.SetMemory(0);
        
        //
        // Read something
        //
        if( 1 )
        {
            xfile file;
            
            xbool result = file.Open(ASYNC_MODE_FILE_NAME_PACH, "r@");
            CPPUNIT_ASSERT_MESSAGE("Open file 'asyncMode.dat' for reading failed!", result );
            x_Yield();
            
            s32 step = Buffer.getCount()/10;
            for( s32 i=0; i<10; i++ )
            {
                xbool bOK = file.Read( &Buffer[i*step], step );
                CPPUNIT_ASSERT( bOK );
                file.Synchronize(TRUE);
            }
            
            file.Close();
            
            for ( s32 i = 0; i < 10*step; i++ )
            {
                if( Buffer[i] != i )
                {
                    ASSERT( Buffer[i] == i ); // CPPUNIT_ASSERT
                }
            }
        }
    }
}
