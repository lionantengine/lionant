//
//  xPtrTest.cpp
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "xPtr2Test.h"

//
// Test Basic Iterators
//
struct some_struct
{
    s32 m_Val;
};
void xptr2_test::TestLoops( void )
{
    //
    // Test xvar
    //
    {
        xvar<s32>           IntSomeVal;
        xvar<some_struct>   SomeVal;

        // Can we set a value for it?
        {
            mutable_ref( rSomeVal, SomeVal );
            mutable_ref( rVal, IntSomeVal );
            
            s32& V = *rVal;
            V = 7;

            rSomeVal->m_Val = 3;
            SomeVal.ChangeBehavior( xvar_flags::FLAGS_READ_ONLY );
        }

        // Can we set a value for it?
        {
            const_ref( rSomeVal, SomeVal );
            CPPUNIT_ASSERT( rSomeVal->m_Val == 3 );
        }
        SomeVal.ChangeBehavior( xvar_flags::FLAGS_QT_READABLE );
        
        // Multiple threads access
        x_inline_light_jobs_block<8>    BlockJobs;
        for ( s32 i=0; i<10; i++ )
        {
            BlockJobs.SubmitJob( [&]()
            {
                const_ref( rSomeVal, SomeVal );
                CPPUNIT_ASSERT( rSomeVal->m_Val == 3 );
            });
        }
        BlockJobs.FinishJobs();
    }
    

    // Check the C++11 compatibility with xptr2
    xptr2<s32>          IntPtr;
    IntPtr.Alloc( 100 );

    {
        mutable_ref( rIntPtr, IntPtr );
        for( s32& Val : rIntPtr )
        {
            Val = 100;
        }
    }

    // Check clasical iterations
    {
        const_ref( rIntPtr, IntPtr );
        for( s32 i=0, e=rIntPtr.getCount(); i<e; ++i )
        {
            CPPUNIT_ASSERT( rIntPtr[i] == 100 );
        }
    }
}

//
// Test References...
//
void xptr2_test::TestReferences( void )
{
    // Create some variables with xptr
    xptr2<s32> A;
    A.Alloc(100);
    {
        mutable_ref( rIntPtr, A );
        for( s32& Val : rIntPtr )
        {
            Val = 100;
        }
    }
    
    //
    // See if going out of the scope kills A..
    //
    {
        const xptr2<s32> B( A );
        const_ref( rIntPtr, B );
        for( const s32& Val : rIntPtr )
        {
            CPPUNIT_ASSERT( Val == 100 );

            // Try to ref one up again
            [](  xptr2<s32> C )
            {
                mutable_ref( rIntPtr2, C );
                for( s32& Val : rIntPtr2 )
                    CPPUNIT_ASSERT( Val == 100 );
                
            }( A );
        }
    }

    //
    // See if going out of the scope kills A..
    //
    xptr2<char> Buffer1;
    Buffer1.New( 100 );

    //
    // Initialize Buffer with some values 
    //
    {
        mutable_ref( rInitVals, Buffer1 );

        rInitVals->setMemory(0 );

        for ( char& Val : rInitVals )
        {
            Val = x_rand()&0xff;
        }

        //
        // Lets sort the array
        //
        for ( char& Val : rInitVals )
        for ( s32 j = rInitVals.getIterator()+1; j < rInitVals.getCount(); j++  )
        {
            char& T = rInitVals[ j ];
            if ( Val < T ) x_Swap( Val, T );
        }
    }

    //
    // Make sure we change behavior for our bufer
    //
    Buffer1.ChangeBehavior( xptr2_flags::FLAGS_READ_ONLY | xptr2_flags::FLAGS_QT_READABLE );

    //
    // Now lets add all the numbers
    //
    {
        x_inline_light_jobs_block<8>    BlockJobs;
        const_ref                       ( rlVals, Buffer1 );
        x_qt_counter                    Total;
        for ( s32 i=0; i<rlVals.getCount(); i+=10 )
        {
            BlockJobs.SubmitJob( [&Buffer1,i,&Total]()
            {
                const_ref( rlVals, Buffer1 );
                for ( s32 j = 0; j < 10; j++ )
                {
                    Total.Add( rlVals[i+j] );
                    x_Sleep(5);
                }
            });
        } 
        
        BlockJobs.FinishJobs();

        x_printf( "Total:%d", Total );
    }

    //
    // Grow by one 
    //
    {
        xarray2<char> XArray;
        const_ref   ( rlVals, Buffer1 );
        mutable_ref ( rArray, XArray );
        for( const char& Src : rlVals )
        {
            // this IF statement emulates the tipical issue of growing and array in a different
            // place and then crashing because the memory got reallocated
            if ( 0 && rlVals.getIterator() > 38 )
            {
                mutable_ref( rArray2, XArray );

                rArray2->append();
            }
            else
            {
                rArray->append() = Src;
            }   
        }
    }

    //
    // Grow fast
    //
    {
        // create an array with values using the buffer
        xarray2<char> XArray;
        {
            const_ref   ( rlVals, Buffer1 );
            mutable_ref ( rArray, XArray );

            rArray->appendList( rlVals.getCount() );
            for( char& Dst : rArray )
            {
                Dst = rlVals[ rArray.getIterator() ];
            }
        }
        
        // copy previos array
        xarray2<char> XArrayCopy;
        {
            mutable_ref ( rCopyArray, XArrayCopy );
            rCopyArray->Copy( XArray );
        }

        // Compare both
        {
            const_ref( rCopyArray, XArrayCopy );
            const_ref( rArray, XArray );
            for ( const char& X : rArray )
            {
                CPPUNIT_ASSERT( X == rCopyArray[ rArray.getIterator() ] );
            }
        }

        // Now insert zeros between the nodes in the copy
        {
            mutable_ref( rCopyArray, XArrayCopy );
            for ( const char& X : rCopyArray )
            {
                (void)X;
                // Insert the new node
                rCopyArray->Insert( rCopyArray.getIterator() ) = 0;

                // Skip the current one
                rCopyArray.getMutableIterator()++;
            }
        }

        // delete all the zeros now
        {
            mutable_ref( rCopyArray, XArrayCopy );
            for ( const char& X : rCopyArray )
            {
                (void)X;
                // Insert the new node
                rCopyArray->DeleteWithCollapse( rCopyArray.getIterator() );
            }
        }

        // Make sure that all our entries still the same
        {
            const_ref( rCopyArray, XArrayCopy );
            const_ref( rlVals, Buffer1 );
            for ( const char& X : rCopyArray )
            {
                CPPUNIT_ASSERT( X == rlVals[ rCopyArray.getIterator() ] );
            }
        }
    } 
}

//
// Test References...
//
void xptr2_test::Test_xptr2_smaphore_lock( void )
{
    xarray2<s32,xptr2_smaphore_lock> lArray;

    // Fill the list with some values
    {
        mutable_ref( rArray, lArray );
        rArray->appendList( 100 );

        for( auto& Array : rArray )       
            Array = rArray.getIterator();
    }

    // Read out those values
    {
        const_ref( rArray, lArray );

        for( auto& Array : rArray )       
            CPPUNIT_ASSERT( Array == rArray.getIterator() );
    }

    // Do some multi processing
    {
        x_inline_light_jobs_block<8>    BlockJobs;
        for( s32 i=0; i<10; i++ ) BlockJobs.SubmitJob( [ &lArray ]()
        {
            xrandom_small Small;
            s32           Total=0;

            Small.setSeed32( (s32)(xuptr)&Small );
            for( s32 i=0; i<1000; i++ )
            {
                if( Small.Rand32(0,10) )
                {
                    const_ref( rArray, lArray );

                    Total = 0;       
                    for( auto& Array : rArray )
                        Total += Array;        
                }
                else
                {
                    mutable_ref( rArray, lArray );
                    rArray->append() = Total; 

                    for( auto& Array : rArray )
                        Total += Array;        
                }
            }
        }); 

        BlockJobs.FinishJobs();
    }

}