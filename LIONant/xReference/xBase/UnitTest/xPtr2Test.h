//
//  xPtrTest.h
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class xptr2_test : public TestFixture
{
public:
    
    void TestLoops( void );
    void TestReferences( void );
    void Test_xptr2_smaphore_lock( void );

    CPPUNIT_TEST_SUITE(xptr2_test);
    CPPUNIT_TEST(TestLoops);
    CPPUNIT_TEST(TestReferences);
    CPPUNIT_TEST(Test_xptr2_smaphore_lock);
	CPPUNIT_TEST_SUITE_END();
};

