//
//  xPtrTest.h
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class xptr_test : public TestFixture
{
public:
    
    void TestLoops( void );
    void TestReferences( void );
    
    CPPUNIT_TEST_SUITE(xptr_test);
    CPPUNIT_TEST(TestLoops);
    CPPUNIT_TEST(TestReferences);
	CPPUNIT_TEST_SUITE_END();
};

