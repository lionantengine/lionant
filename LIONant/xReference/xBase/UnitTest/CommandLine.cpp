//
//  CommandLine.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/config/SourcePrefix.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

CPPUNIT_NS_BEGIN

#include "x_base.h"

#include "CommandLine.h"


void command_line::Test( void )
{
    xcmdline        CmdLine;
    const char*     pString = "-FileName SomeFile.txt -Res 640 480";
    
    // Add the command switches
    CmdLine.AddCmdSwitch( "FileName", 1, 1, 1, 1, FALSE, xcmdline::TYPE_STRING );
    CmdLine.AddCmdSwitch( "Res", 2, 2, 1, 1, FALSE, xcmdline::TYPE_INT );
    
    // handy way of dealing with string compares
    static u32 FileNameCRC = x_strCRC( "FileName" );
    static u32 ResCRC      = x_strCRC( "Res" );
    
    // Parse the command line
    CmdLine.Parse( pString );
    if( CmdLine.DoesUserNeedsHelp() )
    {
        x_printf( "-FileName <filename> -Res <xres> <yres> \n" );
        return;
    }
    
    // Handle parameters
    for( s32 i=0; i<CmdLine.GetCommandCount(); i++ )
    {
        u32 CmdCRC = CmdLine.GetCmdCRC(i);
        if( CmdCRC == FileNameCRC )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            const xstring& String = CmdLine.GetArgument( Offset );
            CPPUNIT_ASSERT( x_strcmp( "SomeFile.txt", String ) == 0 );
            
        }
        else if( CmdCRC == ResCRC )
        {
            s32 Offset = CmdLine.GetCmdArgumentOffset(i);
            s32 XRes   = x_atoi32( CmdLine.GetArgument( Offset + 0 ) );
            s32 YRes   = x_atoi32( CmdLine.GetArgument( Offset + 1 ) );
            CPPUNIT_ASSERT( XRes == 640 );
            CPPUNIT_ASSERT( YRes == 480 );
        }
    }
}

CPPUNIT_NS_END