//
//  vector2.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class vector2 : public TestFixture
{
public:
    
    void Dot( void );
    void GetLength( void );
    void GetLengthSquared( void );
    void GetAngle( void );
    void Rotate( void );
    void Normalize( void );
    void NormalizeSafe( void );
    
    CPPUNIT_TEST_SUITE(vector2);
    CPPUNIT_TEST(Dot);
    CPPUNIT_TEST(GetLength);
    CPPUNIT_TEST(GetLengthSquared);
    CPPUNIT_TEST(GetAngle);
    CPPUNIT_TEST(Rotate);
    CPPUNIT_TEST(Normalize);
    CPPUNIT_TEST(NormalizeSafe);
	CPPUNIT_TEST_SUITE_END();
};

