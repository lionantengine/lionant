//
//  container.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "bitstream.h"

#undef CPPUNIT_ASSERT
#define CPPUNIT_ASSERT(A) ASSERT(A)

//------------------------------------------------------------------------------

 void bitstream::TestInts( void )
 {
    xptr<xbyte> Data;
    s32         Length;
    xbitstream BitStream;

    //
    // Test 32 bits flags
    //
    {
        u32 Full32bits      = 0xf2309abc;
        u32 Full32bitsTest  = 0;

        BitStream.setMode(TRUE);
        BitStream.Serialize( Full32bits, 32 );
        BitStream.getResult( Data, Length );
        CPPUNIT_ASSERT( Length ==  4 );

        BitStream.setMode(FALSE);
        BitStream.setPackData( Data, Length );
        BitStream.Serialize( Full32bitsTest, 32 );

        CPPUNIT_ASSERT( Full32bitsTest ==  Full32bits );
    }

    //
    // Test 64 bits flags
    //
    {
        u64 Full64bits      = 0xf2309abcf2309abc;
        u64 Full64bitsTest  = 0;

        BitStream.setMode(TRUE);
        BitStream.Serialize64( Full64bits, 64 );
        BitStream.getResult( Data, Length );
        CPPUNIT_ASSERT( Length ==  8 );

        BitStream.setMode(FALSE);
        BitStream.setPackData( Data, Length );
        BitStream.Serialize64( Full64bitsTest, 64 );

        CPPUNIT_ASSERT( Full64bitsTest ==  Full64bits );
    }

    //
    // Test different ranges of flags
    //
    {
        const u64 Full64bits      = 0xf2309abcf2309abc;
        //u64 Full64bitsTest  = 0;

        BitStream.setMode(TRUE);
        for( s32 i=1; i<65; i++ )
        {
            if( i <= 32 ) 
            {
                u32 Val = u32(Full64bits&((1<<i)-1));
                BitStream.Serialize( Val, i );
            }
            else
            {
                u64 Val = Full64bits&((u64(1)<<i)-1);
                BitStream.Serialize64( Val, i );
            }
        }
        BitStream.getResult( Data, Length );

        BitStream.setMode(FALSE);
        BitStream.setPackData( Data, Length );

        for( s32 i=1; i<65; i++ )
        {
            if( i <= 32 ) 
            {
                u32 ValOriginal = u32(Full64bits&((1<<i)-1));
                u32 Val;
                BitStream.Serialize( Val, i );
                CPPUNIT_ASSERT( ValOriginal ==  Val );
            }
            else
            {
                u64 ValOriginal = Full64bits&((u64(1)<<i)-1);
                u64 Val;
                BitStream.Serialize64( Val, i );
                CPPUNIT_ASSERT( ValOriginal ==  Val );
            }
        }
    }

    //
    // Test ranges of random values
    //
    {
        xrandom_small Rand;
        const s32 NumberOfValues = 100000;

        Rand.setSeed64( 0xf2309abcf2309abc );
        BitStream.setMode(TRUE);
        for( s32 i=0; i< NumberOfValues; i++ )
        {
            s64 Range = x_Abs((s64)(Rand.Rand32() | (((u64)Rand.Rand32())<<32)) )+1;
            s64 Val   = ((s32)Rand.Rand32() | (((u64)Rand.Rand32())<<32) )%Range;
            BitStream.Serialize64( Val, 0, Range );
        }
        BitStream.getResult( Data, Length );

        BitStream.setMode(FALSE);
        BitStream.setPackData( Data, Length );

        Rand.setSeed64( 0xf2309abcf2309abc );
        for( s32 i=0; i< NumberOfValues; i++ )
        {
            s64 Range = x_Abs((s64)(Rand.Rand32() | (((u64)Rand.Rand32())<<32)) )+1;
            u64 ValOld = ((s32)Rand.Rand32() | (((u64)Rand.Rand32())<<32) )%Range;
            u64 NewVal;
            BitStream.Serialize64( NewVal, 0, Range );
            CPPUNIT_ASSERT( NewVal ==  ValOld );
        }
    }

    //
    // Test  RLE
    //
    {
        const xbitmap&  Bitmap = xbitmap::getDefaultBitmap();
        xptr<xbyte>     BitPacked;
        s32             Length;

        BitStream.setMode(TRUE);
        BitStream.SerializeOutRLE( (s8*)Bitmap.getMip(0), Bitmap.getDataSize() );
        BitStream.getResult( BitPacked, Length );
        
        xptr<s8> Buffer;
        Buffer.Alloc( Bitmap.getDataSize() );        

        BitStream.setMode(FALSE);
        BitStream.setPackData( BitPacked, Length );
        BitStream.SerializeInRLE( &Buffer[0], Bitmap.getDataSize() );

        for( s32 i=0; i<Bitmap.getDataSize(); i++ )
           CPPUNIT_ASSERT( Buffer[i] == ((s8*)Bitmap.getMip(0))[i] );
    }
 }

//------------------------------------------------------------------------------

 void bitstream::TestFloats( void )
 {
 
 }
//------------------------------------------------------------------------------

 void bitstream::TestMix( void )
 {
 
 }

/*
//------------------------------------------------------------------------------
// TYPES
//------------------------------------------------------------------------------
struct some_entity
{
public:
    some_entity*  			    m_pPrev;			// link list of entities
    some_entity*				m_pNext;
    
    ~some_entity( void )
    {
        int a;
        a=22;
    }
};

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void container::TestXArray( void )
{
    xarray<some_entity> lsome_entity;
    
    some_entity& some_entity = lsome_entity.append();
    some_entity.m_pNext = NULL;
}

//------------------------------------------------------------------------------

void container::TestXHArray( void )
{
    xharray<some_entity> lSomeEntity;
    
    xhandle Handle;
    some_entity& SomeEntity = lSomeEntity.append(Handle);
    xptr_lock    Ref( lSomeEntity );
    
    SomeEntity.m_pNext = NULL;
}

//------------------------------------------------------------------------------

struct my_command1 : public xcmd_array::cmd_entry
{
    x_rtti_class1( my_command1, xcmd_array::cmd_entry );
    f32 X, Y, Z;
};

struct my_command2 : public xcmd_array::cmd_entry
{
    x_rtti_class1( my_command2, xcmd_array::cmd_entry );
    s32 Size;
};

void container::TestXCmd_Array( void )
{
    xcmd_array CmdList;
    
    // Write some commands
    my_command1& Cmd1 = CmdList.append<my_command1>();
    Cmd1.X = 2.0f;
    Cmd1.Y = Cmd1.Z = 3.0f;
    
    my_command2& Cmd2 = CmdList.append<my_command2>();
    Cmd2.Size = 100;
    
    // Read some commands
    for( s32 Index=0; Index < CmdList.GetEndIndex(); Index = CmdList.GetNextIndex( Index ) )
    {
        const xcmd_array::cmd_entry& Entry = CmdList.GetEntry( Index );
        
        if( Entry.isKindOf( my_command1::getRTTI() ) )
        {
            const my_command1& Cmd1 = my_command1::SafeCast( Entry );
            CPPUNIT_ASSERT( Cmd1.X == 2.0f );
            CPPUNIT_ASSERT( Cmd1.Y == Cmd1.Z );
        }
        else if( Entry.isKindOf( my_command2::getRTTI() ) )
        {
            const my_command2& Cmd2 = my_command2::SafeCast( Entry );
            CPPUNIT_ASSERT( Cmd2.Size == 100 );
        }
        else
        {
            // "UNKNOWN COMMAND"
            CPPUNIT_ASSERT( 0 );
        }
    }
}

  */