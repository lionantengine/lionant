//
//  ThreadUnitTest.cpp
//  xBaseUnitTest
//
//  Created by Simon Hudson on 9/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "ThreadUnitTest.h"

class WorkerThread : public xthread
{
public:
                                WorkerThread();
    virtual                     ~WorkerThread();
    void                        init(xbool isConsumer = FALSE);
    virtual void                onRun();
    static xbool                sHasThread();
    static void                 sKillAllThreads();

    void                        produce();
    void                        consume();
    static xbool                sNeedEnd();
    
    xbool                       mIsComsumer;
    s32                         mId;
    static s32                  sProductionCount;
    static s32                  sThreadCount;
    static xcritical_section    sCriticalSection;
    static xbool                sNeedEndFlag;
    static xcritical_section    sEndCriticalSection;
};

s32                 WorkerThread::sProductionCount;
s32                 WorkerThread::sThreadCount = 0;
xcritical_section   WorkerThread::sCriticalSection;
xbool               WorkerThread::sNeedEndFlag = FALSE;
xcritical_section   WorkerThread::sEndCriticalSection;

WorkerThread::WorkerThread()
: xthread()
, mIsComsumer(FALSE)
{
    mId = sThreadCount;
}

WorkerThread::~WorkerThread()
{
}

void WorkerThread::init(xbool isConsumer)
{
    mIsComsumer = isConsumer;
    Create("WorkerThread", TRUE, xthread::PRIORITY_NORMAL, 4*1024, TRUE );
}

void WorkerThread::onRun()
{
    // Lets say that we are ready to work
    {
        xscope_atomic( sCriticalSection );
        sThreadCount++;
    }


    while(!sNeedEnd())
    {
        if ( mIsComsumer )
        {
            consume();
        }
        else
        {
            produce();
        }
        x_Yield();
    }

    // Ok we are exiting so lets do this
    {
        xscope_atomic( sCriticalSection );
        ASSERT(sThreadCount > 0);
        sThreadCount--;
    }
}

void WorkerThread::produce()
{
    xscope_atomic( sCriticalSection );
    sProductionCount++;
}

void WorkerThread::consume()
{
    xscope_atomic( sCriticalSection );
    if ( sProductionCount > 0 )
    {
        sProductionCount--;
    }
}

xbool WorkerThread::sHasThread()
{
    xscope_atomic( sCriticalSection );
    return sThreadCount > 0;
}

void WorkerThread::sKillAllThreads()
{
    xscope_atomic( sCriticalSection );
    sNeedEndFlag = TRUE;
}

xbool WorkerThread::sNeedEnd()
{
    xscope_atomic( sCriticalSection );
    return sNeedEndFlag;
}

//
// Test the unitest
//
void thread_unitest::ThreadUnitTest( void )
{
    const s32 Count = 10;
     
    for ( int i = 0; i < Count; i++ )
    {
        WorkerThread* pThread = x_new( WorkerThread, 1, 0 );
        pThread->init(0 != (i % 3));
    }
    
    // wait untill all the threads are active
    while ( WorkerThread::sThreadCount < Count )
    {
        x_Sleep( 10 );
    }

    // Let them do some work
    x_Sleep( 10 );

    // Now kill them all!
    WorkerThread::sKillAllThreads();

    while( WorkerThread::sHasThread() )
    {
        x_Yield();
    }
}