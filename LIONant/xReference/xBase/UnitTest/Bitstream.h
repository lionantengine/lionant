//
//  container.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class bitstream : public TestFixture
{
public:
    
    void TestInts( void );
    void TestFloats( void );
    void TestMix( void );
    
    CPPUNIT_TEST_SUITE(bitstream);
    CPPUNIT_TEST(TestInts);
    CPPUNIT_TEST(TestFloats);
    CPPUNIT_TEST(TestMix);
	CPPUNIT_TEST_SUITE_END();
};

