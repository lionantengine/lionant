//
//  plus.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class plus : public TestFixture
{
public:
    
    void EncryptFunctionsTest( void );
    void CompressionFunctionsTest( void );
    
    CPPUNIT_TEST_SUITE(plus);
    CPPUNIT_TEST(EncryptFunctionsTest);
    CPPUNIT_TEST(CompressionFunctionsTest);
	CPPUNIT_TEST_SUITE_END();
};

