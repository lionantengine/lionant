//
//  LockLessJobs.cpp
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/6/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "LockLessJobs.h"

#ifdef TARGET_3DS
const s32 WORKER_THREAD_COUNT = 12;
#else
const s32 WORKER_THREAD_COUNT = 100;
#endif 

/////////////////////////////////////////////////////////////////////////////////////////////
// TestBasicCasting
/////////////////////////////////////////////////////////////////////////////////////////////
static x_qt_counter g_Counter01;
static u32          g_Counter02;
static u64          g_Counter03;
struct BasicCastingWorker : public xthread
{
    int Done;
#ifdef TARGET_3DS
    xcritical_section m_CriticalSection;
#endif
    
    virtual void onRun( void )
    {
        for( s32 i=0; i<10000; i++ )
        {
            g_Counter01.Inc();
            
            // increment counter 02
            while( 1 )
            {
                s32 l32 = g_Counter02;
#ifdef TARGET_3DS
                m_CriticalSection.BeginAtomic();
                g_Counter02 = l32 + 1;
                m_CriticalSection.EndAtomic();
                break;
#else
                if( x_cas32( &g_Counter02, l32, l32+1 ) ) break;
#endif
            }
            
            // increment counter 03
            while( 1 )
            {
                u64 l64 = g_Counter03;
#ifdef TARGET_3DS
                m_CriticalSection.BeginAtomic();
                g_Counter02 = l64 + 1;
                m_CriticalSection.EndAtomic();
                break;
#else
                if( x_cas64( &g_Counter03, l64, l64+1 ) ) break;
#endif
            }
        }
        
        Done = 1;
    }
};

//------------------------------------------------------------------------------------

void LocklessJobs::TestBasicLockless( void )
{
    xsafe_array<BasicCastingWorker, WORKER_THREAD_COUNT> ThreadList;
    
    g_Counter01.setup(0);
    g_Counter02=0;
    g_Counter03=0;
    
    for ( int i = 0; i < ThreadList.getCount(); i++ )
    {
        ThreadList[i].Done = 0;
        ThreadList[i].Create( "test", TRUE );
    }
    
    for ( int i = 0; i < ThreadList.getCount(); i++ )
    {
        x_Yield();
        if( ThreadList[i].Done == 0 )
        {
            i=-1;
            continue;
        }
    }

    u64 Count = g_Counter01.get();
    CPPUNIT_ASSERT( Count == ThreadList.getCount()*10000 );
    
    Count = g_Counter02;
    CPPUNIT_ASSERT( Count == ThreadList.getCount()*10000 );

    Count = g_Counter03;
    CPPUNIT_ASSERT( Count == ThreadList.getCount()*10000 );
}

/////////////////////////////////////////////////////////////////////////////////////////////
// TestQuantumPointers
/////////////////////////////////////////////////////////////////////////////////////////////

struct lpnode : public x_qt_ptr
{

};

void LocklessJobs::TestQuantumPointers( void )
{
    xsafe_array<lpnode,100> Array;

    for( s32 i=0; i<(Array.getCount()-1); i++ )
    {
        Array[i].SetPtr( &Array[i+1] );
    }

    for( s32 i=0; i<(Array.getCount()-1); i++ )
    {
       CPPUNIT_ASSERT( Array[i].casPointTo( Array[i], &Array[i+1] ) );
       CPPUNIT_ASSERT( Array[i].GetPtr() == &Array[i+1] );
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// TestLocklessHash
/////////////////////////////////////////////////////////////////////////////////////////////
struct node : x_qt_ptr
{
    xguid m_Guid;
};

void LocklessJobs::TestLocklessHash( void )
{
    x_inline_light_jobs_block<8>    JobBlock;
    x_qt_hash<s32, u64>             Hash;
    const s32                       SizeOfThelist = 1024;
    x_qt_fixed_pool<node>           Pool;
    x_qt_fober_queue                Queue; 
    x_qt_counter                    Counter;
    x_qt_counter                    Counter2;


    Hash.Initialize( SizeOfThelist );
    Pool.Init( SizeOfThelist );

    //
    // Basic addition and deletion
    //
    xguid Guid;
    Guid.ResetValue();
    
    // Add a node
    Hash.cpAddEntry( Guid.m_Guid, []( s32& Entry )
    {
        Entry = 22;
    });

    // Make sure it is there
    CPPUNIT_ASSERT( Hash.isEntryInHash( Guid) );

    // Delete it
    Hash.DeleteEntry( Guid.m_Guid );

    // Make sure it is not there
    CPPUNIT_ASSERT( FALSE == Hash.isEntryInHash( Guid) );

    //
    // Add and delete entries at random
    // This test really acomplish two different tests. One is to test the hash
    // and the other to test the guid generator.
    //    
    for( s32 i=0; i<16; i++ ) JobBlock.SubmitJob( [&]()
    {
        xrandom_small Rand;
        xbool         CrazyDeleteGuy = FALSE;
        xbool         CrazyAddGuy = FALSE;

        Rand.setSeed64( u64(&Rand) ^ 0x4230984298234f1  );
        for( s32 i=0; i<1000; i++ )
        {
            if( Rand.Rand32(0, 100) == 0 )
            {
                CrazyDeleteGuy = TRUE;
            }

            if( Rand.Rand32(0, 100) == 1 )
            {
                CrazyAddGuy = TRUE;
            }

            if( CrazyAddGuy || (!CrazyDeleteGuy && Rand.Rand32()&1) )
            {
                node* pNode = (node*)Pool.Alloc();
                if( pNode == NULL ) 
                { 
                    CrazyAddGuy = FALSE;
                    continue; 
                }
          
                if( Rand.Rand32()&1) pNode->m_Guid.m_Guid = Counter2.Inc();
                else                 pNode->m_Guid.ResetValue();

                Hash.cpAddEntry( pNode->m_Guid, [CrazyAddGuy]( s32& Entry )
                {
                    Entry = CrazyAddGuy;
                });

                Counter.Inc();
                Queue.push( pNode );
            }
            else
            {
                node* pNode = (node*)Queue.pop();
                if( pNode == NULL )
                {
                    CrazyDeleteGuy = FALSE; 
                    continue;
                }

                Counter.Dec();
                Hash.DeleteEntry( pNode->m_Guid );
                
                Pool.Free( pNode );
            }
        } 
    } );

    JobBlock.FinishJobs();
}

/////////////////////////////////////////////////////////////////////////////////////////////
// TestQueue
/////////////////////////////////////////////////////////////////////////////////////////////

struct queue_node : public x_qt_ptr
{
    s32 m_X;
};

static const s32                                            MaxNodes = 10000;
static x_qt_fixed_pool_static_mem< queue_node, MaxNodes >   s_FixedPool01;
static x_qt_fober_queue                                     s_Queue01;
static x_qt_counter                                         s_QCounter;

struct QueueWorker : public xthread
{
    int Done;
    
    virtual void onRun( void )
    {
        s32 StackOut=0;
        
        for( s32 i=0; i<MaxNodes; i++ )
        {
            if( s_QCounter.Inc()&1 && ( StackOut > 0 ) )
            {
                x_qt_ptr* pNode = s_Queue01.pop();
                if( pNode )
                {
                    StackOut--;
                    s_FixedPool01.FreeByPtr( pNode );
                }
            }
            else
            {
                queue_node* pNode = (queue_node*)s_FixedPool01.AllocByPtr();
                if( pNode )
                {
                    StackOut++;
                    s_Queue01.push( pNode );
                }
            }
        }
        
        CPPUNIT_ASSERT( StackOut >= 0 );
        
        // send back all the nodes we own to send back
        for( s32 i=0; i<StackOut; i++ )
        {
            x_qt_ptr* pNode = s_Queue01.pop();
            if( pNode ) s_FixedPool01.FreeByPtr( pNode );
            else
            {
                i--;
                x_Yield();
            }
        }
        
        // we are done
        Done=1;
    }
};

//------------------------------------------------------------------------------------

void LocklessJobs::TestLocklessQueue( void )
{
    xsafe_array<QueueWorker, WORKER_THREAD_COUNT> ThreadList;
    
    s_QCounter.setup(0);
    
    //
    // Single Thread tests
    //
    
    // Count to make sure we have the right number of nodes
    s32 Count=0;
    while( queue_node* pNode = (queue_node*)s_FixedPool01.AllocByPtr() )
    {
        pNode->m_X = Count;
        Count++;
        s_Queue01.push( pNode );
    }
    CPPUNIT_ASSERT( Count == MaxNodes );
    
    // Now put back all the nodes into the stack and empty the queue
    while( queue_node* pNode = (queue_node*)s_Queue01.pop() )
    {
        // Make sure the nodes are in the right order
        CPPUNIT_ASSERT( pNode->m_X == MaxNodes - Count );
        Count--;
        s_FixedPool01.FreeByPtr( pNode );
    }
    CPPUNIT_ASSERT( Count == 0 );
    
    
    //
    // Multi-threaded tests
    //
    
    for ( int i = 0; i < ThreadList.getCount(); i++ )
    {
        ThreadList[i].Done = 0;
        ThreadList[i].Create( "test", TRUE );
    }
    
    for ( int i = 0; i < ThreadList.getCount(); i++ )
    {
        x_Yield();
        if( ThreadList[i].Done == 0 )
        {
            i=-1;
            continue;
        }
    }
    
    // should not be any nodes in the queue at this point
    CPPUNIT_ASSERT( s_Queue01.pop() == NULL );
    
    // make sure we have all the nodes back in the stack
    for( int i=0; i<MaxNodes; i++ )
    {
        CPPUNIT_ASSERT( s_FixedPool01.AllocByPtr() );
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// TestLocklessScheguler
/////////////////////////////////////////////////////////////////////////////////////////////

struct xx : public x_qt_ptr
{
    int X;
};

static x_qt_counter     s_SQCounter;

struct a : public x_simple_job<1>
{
    a(s32 ID, int* pArray ) : m_ID(ID), m_pArray(pArray) {}
    virtual void onRun( void )
    {
        m_pArray[m_ID] = s_SQCounter.Inc();
    }
    
    s32     m_ID;
    int*    m_pArray;
};

static x_qt_popless_queue s_Queue;


//-------------------------------------------------------------------------------
void Test01( void )
{
    int LocalArray[3]={0};
    a A(0,LocalArray),B(1,LocalArray),C(2,LocalArray);
    
    s_SQCounter.setup(0);
    g_Scheduler.StartJobChain(A);
    g_Scheduler.StartJobChain(B);
    g_Scheduler.StartJobChain(C);

    // Wait for all to be done
    while(1)
    {
        x_Yield();
        s32 c=0;
        for( s32 i=0; i<3; i++ )
        {
            c += LocalArray[i]?1:0;
        }
        
        if( c==3 ) return;
    }
}

//-------------------------------------------------------------------------------
void Test02( void )
{
    int LocalArray[3]={0};
    a A(2,LocalArray),B(1,LocalArray),C(0,LocalArray);
    
    s_SQCounter.setup(0);
    C.AndThenRuns( B );
    B.AndThenRuns( A );
    
    g_Scheduler.StartJobChain(C);
    
    // Wait for all to be done
    while(1)
    {
        x_Yield();
        s32 c=0;
        for( s32 i=0; i<3; i++ )
        {
            if( (1+i) == LocalArray[i] ) c++;
        }
        
        if( c==3 ) return;
    }
}

 //-------------------------------------------------------------------------------

void LocklessJobs::TestLocklessScheguler ( void )
{
    g_Scheduler.Init(4);
    
    Test01();
    Test02();
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Circle Queue
/////////////////////////////////////////////////////////////////////////////////////////////

struct entry
{
    entry*  m_pPtr;
};

void LocklessJobs::TestLocklessCircularQueue ( void )
{
    x_inline_light_jobs_block<4>            Block;
    xptr< x_qt_circular_queue<512*64> >     PtrCircularQueue;

    PtrCircularQueue.New( 1, xmem_aligment( x_qt_circular_queue<512*64> ) );

    auto& CircularQueue = PtrCircularQueue[0]; 

    for( s32 j=0; j<100; j++ ) Block.SubmitJob( [&CircularQueue]()
    {
        xarray<entry> List;
        xrandom_small Rand;
        
        Rand.setSeed64( *(u64*)&Rand );

        for( s32 i=0; i<1000; i++ )
        {
            if( Rand.Rand32()&1 && List.getCount() )
            {
                if( (Rand.Rand32()%10) == 0 )
                {
                    while( List.getCount() )
                    {
                        CircularQueue.Free( List[0].m_pPtr );
                        List.DeleteWithCollapse( 0 );
                    }
                }
                else
                {
                    CircularQueue.Free( List[0].m_pPtr );
                    List.DeleteWithCollapse( 0 );
                }
            }
            else
            {
                entry& Entry = List.append();
                Entry.m_pPtr = &CircularQueue.Alloc<entry>();
            }
        }

        while( List.getCount() )
        {
            CircularQueue.Free( List[0].m_pPtr );
            List.DeleteWithCollapse( 0 );
        }

    } );

    Block.FinishJobs();

}
