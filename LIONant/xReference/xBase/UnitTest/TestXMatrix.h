
// ---------------------------------------------------------------------------
class TestXMatrix4: public TestFixture
{
public:

	void setUp() 
	{
	}

	void tearDown() 
	{
	}
	
	void	testSetup()
	{
		// test SRT setup - rotation is radian
		 const xmatrix4	expect( 0.649f,		0.125f,     0.75f,      10.0f,
                                0.433f,		0.75f,     -0.5f,       20.0f,
                               -0.625f,     0.649f,     0.433f,     30.0f,
                                0.0f,		0.0f,       0.0f,        1.0f) ;

		
        // Basic getters
        CPPUNIT_ASSERT( expect(0,0) == 0.649f );
        CPPUNIT_ASSERT( expect(0,1) == 0.125f );
        CPPUNIT_ASSERT( expect(0,2) == 0.75f );
        CPPUNIT_ASSERT( expect(0,3) == 10.0f );
        
        CPPUNIT_ASSERT( expect(1,0) == 0.433f );
        CPPUNIT_ASSERT( expect(1,1) == 0.75f );
        CPPUNIT_ASSERT( expect(1,2) == -0.5f );
        CPPUNIT_ASSERT( expect(1,3) == 20.0f );
        
        CPPUNIT_ASSERT( expect(2,0) == -0.625f );
        CPPUNIT_ASSERT( expect(2,1) == 0.649f );
        CPPUNIT_ASSERT( expect(2,2) == 0.433f );
        CPPUNIT_ASSERT( expect(2,3) == 30.0f );
        
        CPPUNIT_ASSERT( expect(3,0) == 0.0f );
        CPPUNIT_ASSERT( expect(3,1) == 0.0f );
        CPPUNIT_ASSERT( expect(3,2) == 0.0f );
        CPPUNIT_ASSERT( expect(3,3) == 1.0f );
        
        
        // Build equivalent matrix someother way...
		xmatrix4 M1;
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );
        
        
		CPPUNIT_ASSERT( expect.getUp()			== M1.getUp() );
		CPPUNIT_ASSERT( expect.getRight()		== M1.getRight() );
		CPPUNIT_ASSERT( expect.getBack()		== M1.getBack() );
		CPPUNIT_ASSERT( expect.getTranslation()	== M1.getTranslation() );

		// test SRT setup but rotation is quaternion
		xmatrix4 M2;
		xquaternion	quat(Rot);
		M2.setup(xvector3(1,1,1), quat, xvector3(10,20,30));
        
		CPPUNIT_ASSERT( expect.getUp()			== M2.getUp() );
		CPPUNIT_ASSERT( expect.getRight()		== M2.getRight() );
		CPPUNIT_ASSERT( expect.getBack()		== M2.getBack() );
		CPPUNIT_ASSERT( expect.getTranslation()	== M2.getTranslation() );
	

		//test Angle & Axis Setup

	}

	void	testMatrix4MultplyVector()
	{
 		xmatrix4 M1;
 		xvector3 V1(4,2,1);
 		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );

		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );

		//test multiply vector3
		xvector3 Res1( M1 * V1 );
		xvector3 Res2(13.598f,22.732f,29.232f);
		CPPUNIT_ASSERT( Res2==Res1 );

		//test multiply vector4
		xvector4 V2(4,2,1,1);
		xvector4 Res3( M1 * V2 );
		xvector4 Res4(13.598f,22.732f,29.232f,1);
		CPPUNIT_ASSERT(Res3 == Res4);
	}

	void	testFullInvert()
	{
		xmatrix4 M1;
		xvector3 V1(4,2,1);
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );

		M1.FullInvert();

		xmatrix4 expect (	0.649f,	0.433f, -0.625f,3.594f,
							0.125f,	0.75f,	0.649f,	-35.735f,
							0.75f,	-0.5f,	0.433f,	-10.49f,
							0.0f,	0.0f,	0.0f,	1.0f);
		CPPUNIT_ASSERT( expect.getUp()			== M1.getUp() );
		CPPUNIT_ASSERT( expect.getRight()		== M1.getRight() );
		CPPUNIT_ASSERT( expect.getBack()		== M1.getBack() );
		CPPUNIT_ASSERT( expect.getTranslation()	== M1.getTranslation() );

	}

	void	testAdjoint()
	{
		xmatrix4 M1;
		xvector3 V1(4,2,1);
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );


		xmatrix4 expect (	0.65f,	0.125f, 0.75f,	0.0f,
							0.433f, 0.75f,	-0.5f,	0.0f,
							-0.625f,0.649f,	0.433f,	0.0f,
							0.0f,	0.0f,	0.0f,	1.0f);


		xmatrix4 res = M1.getAdjoint();
		CPPUNIT_ASSERT( expect.getUp()			== res.getUp() );
		CPPUNIT_ASSERT( expect.getRight()		== res.getRight() );
		CPPUNIT_ASSERT( expect.getBack()		== res.getBack() );
		CPPUNIT_ASSERT( expect.getTranslation()	== res.getTranslation() );

	}
	
	void	testIsIdentity()
	{

		xmatrix4 expect1 (	0.65f,	0.125f, 0.75f,	0.0f,
							0.433f, 0.75f,	-0.5f,	0.0f,
							-0.625f,0.649f,	0.433f,	0.0f,
							0.0f,	0.0f,	0.0f,	1.0f);

		CPPUNIT_ASSERT(expect1.isIdentity() == false);

		xmatrix4 expect2 (	1,	0,	0,	0,
							0,	1,	0,	0,
							0,	0,	1,	0,
							0,	0,	0,	1);

		CPPUNIT_ASSERT(expect2.isIdentity());

	}

	void	testPreScale()
	{
		//test single scaler
		xmatrix4 testMat (	1,1,1,1,
							2,2,2,2,
							3,3,3,3,
							5,5,5,5);
		f32	scale = 5.0f;

		testMat.PreScale(scale);

		xmatrix4 expect1 (	5,5,5,1,
							10,10,10,2,
							15,15,15,3,
							25,25,25,5);


		CPPUNIT_ASSERT( expect1.getUp()             == testMat.getUp());
		CPPUNIT_ASSERT( expect1.getRight()          == testMat.getRight());
		CPPUNIT_ASSERT( expect1.getBack()           == testMat.getBack());
		CPPUNIT_ASSERT( expect1.getTranslation()	== testMat.getTranslation());

		//test scale by vector3
		xvector3	scaleVec(0.1f,0.2f,0.2f);
		testMat.PreScale(scaleVec);
		xmatrix4 expect2 (	0.5f,	1,	1,	1,
							1,		2,	2,	2,
							1.5f,	3,	3,	3,
							2.5f,	5,	5,	5);


		CPPUNIT_ASSERT( expect2.getUp()             == testMat.getUp());
		CPPUNIT_ASSERT( expect2.getRight()          == testMat.getRight());
		CPPUNIT_ASSERT( expect2.getBack()           == testMat.getBack());
		CPPUNIT_ASSERT( expect2.getTranslation()	== testMat.getTranslation());
	}

	void	testScale()
	{
		//test vector3d scaler
		xmatrix4 testMat (	1,1,1,1,
							2,2,2,2,
							3,3,3,3,
							0,0,0,1);

		xvector3d	scaleVec(0.1f,0.2f,0.2f);

		testMat.Scale(scaleVec);

		xmatrix4 expect (	0.1f,	0.1f,	0.1f,	0.1f,
							0.4f,	0.4f,	0.4f,	0.4f,
							0.6f,	0.6f,	0.6f,	0.6f,
							0,		0,		0,		1);

		CPPUNIT_ASSERT( expect.getUp()          == testMat.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == testMat.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == testMat.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== testMat.getTranslation());

	}

	void	testRotateVector()
	{
		xmatrix4 M1;
		xvector3 V1(4,2,1);
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );

		xvector3 res = M1.RotateVector(V1);
		xvector3 expect(3.598f,2.732f,-0.768f);
		
		CPPUNIT_ASSERT( res == expect);
	}

	void	testInvRotateVector()
	{
		xmatrix4 M1;
		xvector3 V1(4,2,1);
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );

		xvector3 res = M1.InvRotateVector(V1);
		xvector3 expect(6.098f,1.366f,0.458f);

		CPPUNIT_ASSERT( res == expect );

	}

	void	testOperatorAddEqual()
	{
		xmatrix4 matA ( 1,1,1,1,
						2,2,2,2,
						3,3,3,3,
						4,4,4,4);

		xmatrix4 matB ( 2,2,2,2,
						3,3,3,3,
						4,4,4,4,
						5,5,5,5);

		matA += matB;

		xmatrix4 expect ( 3,3,3,3,
						  5,5,5,5,
						  7,7,7,7,
						  9,9,9,9);

		CPPUNIT_ASSERT( expect.getUp()          == matA.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == matA.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == matA.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== matA.getTranslation());

	}

	void	testOperatorMinusEqual()
	{
	
		xmatrix4 matA ( 1,1,1,1,
			2,2,2,2,
			3,3,3,3,
			4,4,4,4);

		xmatrix4 matB ( 2,2,2,2,
			3,3,3,3,
			4,4,4,4,
			5,5,5,5);

		matA -= matB;

		xmatrix4 expect ( -1,-1,-1,-1,
						  -1,-1,-1,-1,
						  -1,-1,-1,-1,
						  -1,-1,-1,-1);

		CPPUNIT_ASSERT( expect.getUp()          == matA.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == matA.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == matA.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== matA.getTranslation());
			
	}

	void	testOperatorAdd()
	{
		xmatrix4 matA ( 1,1,1,1,
			2,2,2,2,
			3,3,3,3,
			4,4,4,4);

		xmatrix4 matB ( 2,2,2,2,
			3,3,3,3,
			4,4,4,4,
			5,5,5,5);

		xmatrix4 matC = matA + matB;

		xmatrix4 expect (	3,3,3,3,
							5,5,5,5,
							7,7,7,7,
							9,9,9,9);

		CPPUNIT_ASSERT( expect.getUp()          == matC.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == matC.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == matC.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== matC.getTranslation());
	}

	void	testOperatorMinus()
	{
		xmatrix4 matA ( 1,1,1,1,
			2,2,2,2,
			3,3,3,3,
			4,4,4,4);

		xmatrix4 matB ( 2,2,2,2,
			3,3,3,3,
			4,4,4,4,
			5,5,5,5);

		xmatrix4 matC = matA - matB;

		xmatrix4 expect (	-1,-1,-1,-1,
							-1,-1,-1,-1,
							-1,-1,-1,-1,
							-1,-1,-1,-1);

		CPPUNIT_ASSERT( expect.getUp()          == matC.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == matC.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == matC.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== matC.getTranslation());
	}

	void testTransform3D()
	{
		xmatrix4 M1;
		xvector3 V1(4,2,1);
		xradian3 Rot( X_RADIAN(30), X_RADIAN(60), X_RADIAN(30) );
		M1.setup( xvector3(1,1,1), Rot, xvector3(10, 20, 30) );

		xvector3 res = M1.Transform3D(V1);
		xvector3 expect (13.598f, 22.732f, 29.232f);

		CPPUNIT_ASSERT( res == expect );
		
	}

	void testLookAt()
	{
		xvector3 at(10,5.0f,-14.0);
		xvector3 up(0.f,1.0f,0.0f);
		xvector3 pos(0,0,60);
		xvector3 to = pos - at;

		xmatrix4 perpsMat;
		perpsMat.LookAt(pos,to,up);

		xmatrix4 expect (  -0.814f,	    0.0f,	-0.581f,    34.8786f,
						   -0.162f,	  0.960f,    0.227f,   -13.6239f,
							0.558f,   0.279f,	-0.781f,	46.8732f,
							  0.0f,		0.0f,      0.0f,        1.0f);

		CPPUNIT_ASSERT( expect.getUp()          == perpsMat.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == perpsMat.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == perpsMat.getBack());
		CPPUNIT_ASSERT(expect.getTranslation().GetDistance(perpsMat.getTranslation() ) < 1.0f );

	}

	void testOrthographicProjection()
	{
		xmatrix4 orthProj;
		orthProj.OrthographicProjection(1024,790,1,1000);

		xmatrix4 expect (	0.0019f,0,		 0,		0,
							0,		0.0025f, 0,		0,
							0,		0,		 -0.0010f, - 0.001f,
							0,		0,		0,			1);


		CPPUNIT_ASSERT( expect.getUp()          == orthProj.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == orthProj.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == orthProj.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== orthProj.getTranslation());


	}

	void testPerspectiveProjection()
	{
		xmatrix4 perspMat;
		perspMat.PerspectiveProjection(-10,10,-20,20,1,1000);

		xmatrix4 expect (	0.1f,	0,		0,		0,
							0,		0.05f,	0,		0,
							0,		0,		1.001f,	1.001f,
							0,		0,		-1,		0);

		CPPUNIT_ASSERT( expect.getUp()          == perspMat.getUp());
		CPPUNIT_ASSERT( expect.getRight()       == perspMat.getRight());
		CPPUNIT_ASSERT( expect.getBack()        == perspMat.getBack());
		CPPUNIT_ASSERT( expect.getTranslation()	== perspMat.getTranslation());

	}


	
	CPPUNIT_TEST_SUITE(TestXMatrix4);
	CPPUNIT_TEST(testSetup);
	CPPUNIT_TEST(testMatrix4MultplyVector);
	CPPUNIT_TEST(testFullInvert);
	CPPUNIT_TEST(testAdjoint);
	CPPUNIT_TEST(testIsIdentity);
	CPPUNIT_TEST(testPreScale);
	CPPUNIT_TEST(testScale);
	CPPUNIT_TEST(testRotateVector);
	CPPUNIT_TEST(testInvRotateVector);
	CPPUNIT_TEST(testOperatorAddEqual);
	CPPUNIT_TEST(testOperatorMinusEqual);
	CPPUNIT_TEST(testOperatorAdd);
	CPPUNIT_TEST(testOperatorMinus);
	CPPUNIT_TEST(testTransform3D);
	CPPUNIT_TEST(testLookAt);
	CPPUNIT_TEST(testOrthographicProjection);
	CPPUNIT_TEST(testPerspectiveProjection);
	CPPUNIT_TEST(testSetup);
	CPPUNIT_TEST(testLookAt);
	CPPUNIT_TEST(testOrthographicProjection);
	CPPUNIT_TEST(testPerspectiveProjection);
	CPPUNIT_TEST_SUITE_END();

private:

};

