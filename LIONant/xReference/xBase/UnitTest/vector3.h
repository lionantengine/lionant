//
//  vector3.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class vector3 : public TestFixture
{
public:
    
    void Normalize( void );
    void NormalizeSafe( void );
    void LengthSquared( void );
    
    CPPUNIT_TEST_SUITE(vector3);
    CPPUNIT_TEST(Normalize);
    CPPUNIT_TEST(NormalizeSafe);
    CPPUNIT_TEST(LengthSquared);
	CPPUNIT_TEST_SUITE_END();
};

