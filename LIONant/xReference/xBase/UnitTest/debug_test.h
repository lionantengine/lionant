//
//  debug_test.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class debug_test : public TestFixture
{
public:
    
    void DebugLevelTest( void );
    void LogFunctionsTest( void );
    void AssertsThrowsTest( void );
    void CallStackTest( void );
    
    CPPUNIT_TEST_SUITE(debug_test);
    CPPUNIT_TEST(DebugLevelTest);
    CPPUNIT_TEST(LogFunctionsTest);
    CPPUNIT_TEST(AssertsThrowsTest);
    CPPUNIT_TEST(CallStackTest);
	CPPUNIT_TEST_SUITE_END();
};

