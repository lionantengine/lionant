//
//  BasicXBase.h
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/6/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class BasicXBaseTest: public TestFixture
{
public:
    void TypeTest( void )
    {
        //
        // Basic types all defined correctly
        //
        CPPUNIT_ASSERT( sizeof(s8)  == sizeof(u8) );
        CPPUNIT_ASSERT( sizeof(s16) == sizeof(u16) );
        CPPUNIT_ASSERT( sizeof(s32) == sizeof(u32) );
        CPPUNIT_ASSERT( sizeof(s64) == sizeof(u64) );
        
        CPPUNIT_ASSERT( sizeof(xbyte)  == 1 );
        CPPUNIT_ASSERT( sizeof(s8)     == 1 );
        CPPUNIT_ASSERT( sizeof(s16)    == 2 );
        CPPUNIT_ASSERT( sizeof(s32)    == 4 );
        CPPUNIT_ASSERT( sizeof(s64)    == 8 );

        CPPUNIT_ASSERT( sizeof( char )  == 1 );
        CPPUNIT_ASSERT( sizeof( wchar ) == 2 );
        
        CPPUNIT_ASSERT( sizeof(f32)   == 4 );
        CPPUNIT_ASSERT( sizeof(f64)   == 8 );
        CPPUNIT_ASSERT( sizeof(f32x4) == 16 );
       
        //
        // Make sure that the endian macros are defined correctly
        //
        static const char* Signature = "ZENX";
        
#ifdef X_LITTLE_ENDIAN
        CPPUNIT_ASSERT( *((u32*)Signature) == u32('XNEZ') );
#elif defined X_BIG_ENDIAN
        CPPUNIT_ASSERT( *((u32*)Signature) == u32('ZENX') );
#else
    #error "Not Endian defined"
#endif
    }
    
    
    CPPUNIT_TEST_SUITE(BasicXBaseTest);
    CPPUNIT_TEST(TypeTest);
	CPPUNIT_TEST_SUITE_END();
};

