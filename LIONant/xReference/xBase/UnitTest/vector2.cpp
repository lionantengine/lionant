//
//  vector2.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "vector2.h"

//-------------------------------------------------------------------------

void vector2::Dot( void )
{
    xvector2 A( 100, 0 );
    f32 dis = x_Sqrt( A.Dot(A) );
    CPPUNIT_ASSERT( x_FEqual( 100.0f, dis ) );
}


//-------------------------------------------------------------------------

void vector2::GetLength( void )
{
    xvector2 A( 100, 0 );
    f32 dis = A.GetLength();
    
    CPPUNIT_ASSERT( x_FEqual( 100.0f, dis ) );
}

//-------------------------------------------------------------------------

void vector2::GetLengthSquared( void )
{
    xvector2 A( 100, 0 );
    f32 dis = A.GetLengthSquared();
    
    CPPUNIT_ASSERT( x_FEqual( 100.0f*100.0f, dis ) );
}

//-------------------------------------------------------------------------

void vector2::GetAngle( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector2 A( 1, 0 );
        A.Rotate(R);
        xradian R2 = A.GetAngle();
        
        R  = x_ModAngle(R+0.00001f);
        R2 = x_ModAngle(R2+0.00001f);
        CPPUNIT_ASSERT( x_FEqual( R, R2 ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector2::Rotate( void )
{
    xradian R;
    
    for( R=-PI2; R<PI2; R += X_RADIAN(1) )
    {
        xvector2 A( 1, 0 );
        A.Rotate(R);
        CPPUNIT_ASSERT( x_FEqual( A.m_X, x_Cos(R) ) && x_FEqual( A.m_Y, x_Sin(R) ) );
    }
    
    CPPUNIT_ASSERT( R >= PI2 );
}

//-------------------------------------------------------------------------

void vector2::Normalize( void )
{
    for( s32 i=0; i<1000; i++ )
    {
        xvector2 A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.Normalize();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
}

//-------------------------------------------------------------------------

void vector2::NormalizeSafe( void )
{
    for( s32 i=0; i<1000; i++ )
    {
        xvector2 A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.NormalizeSafe();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
}