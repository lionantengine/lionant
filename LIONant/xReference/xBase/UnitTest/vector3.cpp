//
//  vector3.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "vector3.h"

//-------------------------------------------------------------------------

void vector3::Normalize( void )
{
    s32 i;
    
    for( i=0; i<1000; i++ )
    {
        xvector3 A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.Normalize();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
}

//-------------------------------------------------------------------------

void vector3::NormalizeSafe( void )
{
    s32 i;
    for( i=0; i<1000; i++ )
    {
        xvector3 A( x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0), x_frand(-100000.0f, 1000000.0) );
        
        A.NormalizeSafe();
        CPPUNIT_ASSERT( x_FEqual( A.GetLength(), 1.0f ) );
    }
}

//-------------------------------------------------------------------------

void vector3::LengthSquared( void )
{
    xvector3 A( 100, 0, 0), B(1,1,1), C;
    
    B = (B+B) / xvector3( 2 );
    C = A * B;
    C *= C;
    B -= B;
    C += B;
    
    // x_Math::xvector3.+-*etc OKAY
    CPPUNIT_ASSERT( x_FEqual( C.GetLength(), A.GetLengthSquared() ) );
}

