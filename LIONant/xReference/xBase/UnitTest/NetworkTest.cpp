//
//  NetworkTest.cpp
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "NetworkTest.h"

// ERROR CODES FOR NETWORK
// https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man2/intro.2.html

///////////////////////////////////////////////////////////////////////////////////////////
// UDP Test
///////////////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

struct computer_02
{
    xhandle         m_hLocalSocket;
    xnet_udp_mesh   m_NetMesh;
    
    computer_02( void )
    {
        static s32 Ports[] = {4001,4002,4003,4004};
        static s32 Index=0;
        
        // Create a socket to talk to the outside world
        m_hLocalSocket = m_NetMesh.OpenSocket( Ports[Index] );
        
        // make sure it is valid
        CPPUNIT_ASSERT( m_hLocalSocket.isValid() );
        
        // Create a client
        xhandle hClientID = m_NetMesh.CreateClient( xnet_address(xnet_socket::getLocalIP(), Ports[1-Index]) );
        
        // make sure it is valid
        CPPUNIT_ASSERT( hClientID.isValid() );
        
        // Mark client as certified
        m_NetMesh.getClient( hClientID ).CertifyClient();
        
        // Increment the index for the other computer
        Index++;
    }
    
    void onRun( void )
    {
        xhandle         hClient;
        s32             AllocSize   = 1024;
        xptr<xbyte>     xPacket;
        static s32      SomeInteger=0;
        xbool           bDisconnected=FALSE;
        
        // Alloc the packet
        xPacket.Alloc( AllocSize );
        
        //
        // Send some data to our only client
        //
        xfs SomeString( "This is a test %d\n", SomeInteger++ );
        
        if( m_NetMesh.getClientCount() )
        {
            if( 0 )
            {
                if(SomeInteger&1)
                    bDisconnected = !m_NetMesh.getClient( 0 ).SendReliablePacket( m_hLocalSocket, SomeString, x_strlen(SomeString)+1 );
            }
            else if( 1 )
            {
                if(SomeInteger&1)
                {
                    bDisconnected = !m_NetMesh.getClient( 0 ).SendInOrderPacket( m_hLocalSocket, SomeString, x_strlen(SomeString)+1 );
                }
                else
                {
                    //   bDisconnected = !m_NetMesh.getClient( 0 ).SendReliablePacket( m_hLocalSocket, "A\n", 3 );//SomeString, x_strlen(SomeString)+1 );
                    
                }
            }
            else if( 0 )
            {
                bDisconnected = !m_NetMesh.getClient( 0 ).SendNonreliablePacket( m_hLocalSocket, SomeString, x_strlen(SomeString)+1 );
            }
        }
        
        //
        // Has a client already been disconnected (probably due to memory overflow due to a timeout)
        //
        if( bDisconnected == TRUE )
        {
            // We should wait to delete the client since we may have some pending packets from it
        }
        
        //
        // Recive information
        //
        for( s32 Size = AllocSize; m_NetMesh.getSocketPackets( m_hLocalSocket, &xPacket[0], Size, hClient ); Size = AllocSize )
        {
            xnet_udp_client& Client = m_NetMesh.getClient( hClient );
            
            // If we get a negative address then the client has timedout
            if( Size < 0 )
            {
                //printf( "Client[%s] Has timeout and disconnected\n", (const char*)Client.getAddress().GetStrAddress() );
                m_NetMesh.DeleteClient( hClient );
            }
            else if( Client.getClientType() == xnet_udp_client::TYPE_UNKOWN )
            {
                //printf( "Client[%s] Has tried to connect to us (we are not accepting new clients)\n", (const char*)Client.getAddress().GetStrAddress() );
                m_NetMesh.DeleteClient( hClient );
            }
            else
            {
                printf("."); fflush(stdout);
                //printf( "Client[%s] Gave us: %s\n", (const char*)Client.getAddress().GetStrAddress(), (char*)&xPacket[0] );
            }
        }
    }
};

//-------------------------------------------------------------------------------

void network_tests::BasicUDPConnection( void )
{
    xsafe_array<xptr<computer_02>,2> Computer;
    s32 ComputerCount = Computer.getCount();
    for( s32 i = 0; i < ComputerCount; i++ )
    {
        Computer[i].New();
    }
    
    // Very bad network most games I think will die right away
    // The system can survive for a while if we increase the buffer size per client
    if( 0 )
    {
        x_NetV2EnableLagSimulator(45, 10, 5, 20, 100 );
    }
    else if(0)
    {
        // May be typical worse case
        x_NetV2EnableLagSimulator(30, 5, 5, 30, 60 );
    }
    
    // Start the show
    for( s32 i=0; i<500; i++)
    {
        for( s32 j = 0; j < ComputerCount; j++ )
        {
            Computer[j]->onRun();
            x_Sleep(10);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
// TCP Test
///////////////////////////////////////////////////////////////////////////////////////////

struct computer_03
{
    xnet_tcp_server     m_Server;
    xnet_tcp_client     m_Client;
    xbool               m_bServer;
    
    computer_03( void )
    {
        static s32 Ports[] = {4001,4002};
        static s32 Index=0;
        xbool      bSuccess;
        
        m_bServer = !Index;
        
        // Create the ports and or the connections
        if( m_bServer )
        {
            bSuccess = m_Server.OpenSocket( Ports[Index] );
            CPPUNIT_ASSERT( bSuccess );
        }
        else // client
        {
            m_Client.OpenSocket( Ports[Index] );
            
            s32 nRetries=10;
            while( FALSE == m_Client.ConnectToServer( xnet_address(xnet_socket::getLocalIP(), Ports[1-Index]) ) )
            {
                nRetries--;
                x_Sleep(100);
                if( nRetries<0)
                {
                    CPPUNIT_ASSERT(0);
                    break;
                }
            }
        }
        
        // Increment the index for the other computer
        Index++;
    }
    
    void onRun( void )
    {
        const s32       AllocSize   = 1024;
        xptr<xbyte>     xPacket;
        
        // Alloc the packet
        xPacket.Alloc( AllocSize );
        
        //
        // Deal with server/client
        //
        if( m_bServer )
        {
            //
            // Let the server accept any incoming connections
            //
            s32 nClients = m_Server.getClientCount();
            if( m_Server.AcceptConnections() )
            {
                // We got new clients
                for( s32 i=nClients; i<m_Server.getClientCount(); i++ )
                {
                    xnet_tcp_client& Client = m_Server.getClient(i);
                    ASSERT( Client.getClientType() == xnet_tcp_client::TYPE_UNKOWN );
                    
                    // Tell the system that we are all good
                    // This should be done really after talking to the client for a bit
                    Client.CertifyClient();
                }
                
                // Update the client count
                nClients = m_Server.getClientCount();
            }
            
            //
            // Get any packets
            //
            for( s32 i=0; i<nClients; i++ )
            {
                xnet_tcp_client& Client = m_Server.getClient(i);
                s32              Size   = AllocSize;
                
                // Check to see if you have recived anything from the client
                while( Client.Recive( &xPacket[0], Size ) )
                {
                    //printf("Server: I got this message: %s\n",&xPacket[0] );
                    xbool bSuccess = Client.Send( &xPacket[0], Size );
                    CPPUNIT_ASSERT( bSuccess );

                    
                    // Get more packets
                    Size   = AllocSize;
                }
            }
        }
        else // client
        {
            static s32      SomeInteger=0;
            
            //
            // Send some data to our only client
            //
            xfs SomeString( "This is a test %d\n", SomeInteger++ );
            
            // Send a packet
            xbool bSuccess = m_Client.Send( SomeString, x_strlen(SomeString)+1 );
            CPPUNIT_ASSERT( bSuccess );
            
            //
            // Deal with reciving packets
            //
            s32 Size = AllocSize;
            
            while( m_Client.Recive( &xPacket[0], Size ) )
            {
                printf("."); fflush(stdout);

                //printf("Client: I got this message: %s\n",&xPacket[0] );
                
                // Get more packets
                Size   = AllocSize;
            }
        }
    }
};

//-------------------------------------------------------------------------------

void network_tests::BasicTCPConnection( void )
{
    xsafe_array<xptr<computer_03>,2> Computer;
    
    // Give some time before the system frees up the previous test ports...
    x_Sleep(500);
    
    for( s32 i=0; i<Computer.getCount(); i++ )
    {
        Computer[i].New();
    }
    
    // Let some time pass to make sure they connect... note this is a hack
    // we should be asking to a function rather than just waiting...
    x_Sleep(500);
    
    // Start the show
    for( s32 i=0; i<500; i++ )
    {
        for( s32 i=0; i<Computer.getCount(); i++ )
        {
            Computer[i]->onRun();
            x_Sleep(10);
        }
    }
}



