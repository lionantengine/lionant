//
//  container.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "container.h"


//------------------------------------------------------------------------------
// TYPES
//------------------------------------------------------------------------------
struct some_entity
{
public:
    some_entity*  			    m_pPrev;			// link list of entities
    some_entity*				m_pNext;
    
    ~some_entity( void )
    {
        int a;
        a=22;
    }
};

//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void container::TestXArray( void )
{
    xarray<some_entity> lsome_entity;
    
    some_entity& some_entity = lsome_entity.append();
    some_entity.m_pNext = NULL;
}

//------------------------------------------------------------------------------

void container::TestXHArray( void )
{
    xharray<some_entity> lSomeEntity;
    
    xhandle Handle;
    some_entity& SomeEntity = lSomeEntity.append(Handle);
    xptr_lock    Ref( lSomeEntity );
    
    SomeEntity.m_pNext = NULL;
}

//------------------------------------------------------------------------------

void container::TestXPArray( void )
{
    xparray<s32> Buffer;

    for( s32 i=0; i<1000; i++ )
    {
        s32& X = Buffer.append();
        X = i;
        Buffer.SanityCheck();
    }

    for( s32 i=0; i<1000; i++ )
    {
        s32& X = Buffer[i];
        ASSERT( X == i );
    }

    for( s32 i=0; i<1000; i++ )
    {
        Buffer.Delete( x_rand()%Buffer.getCount() );
        Buffer.SanityCheck();
    }

    s32 Run=0;
    for( s32 i=0; i<1000; i++ )
    {
        if( Run < 2 && (x_rand()&1) )
        {
            if( Buffer.getCount() ) 
                Buffer.Delete( x_rand()%Buffer.getCount() );
            
            if( (x_rand()%100) == 50 ) 
                Run = 1-Run;
        }
        else
        {
            Buffer.append();
            if( (x_rand()%100) == 50 )    
                Run = 2-Run;
        }
        Buffer.SanityCheck();
    }
}

//------------------------------------------------------------------------------

struct my_command1 : public xcmd_array::cmd_entry
{
    x_rtti_class1( my_command1, xcmd_array::cmd_entry );
    f32 X, Y, Z;
};

struct my_command2 : public xcmd_array::cmd_entry
{
    x_rtti_class1( my_command2, xcmd_array::cmd_entry );
    s32 Size;
};

void container::TestXCmd_Array( void )
{
    xcmd_array CmdList;
    
    // Write some commands
    my_command1& Cmd1 = CmdList.append<my_command1>();
    Cmd1.X = 2.0f;
    Cmd1.Y = Cmd1.Z = 3.0f;
    
    my_command2& Cmd2 = CmdList.append<my_command2>();
    Cmd2.Size = 100;
    
    // Read some commands
    for( s32 Index=0; Index < CmdList.GetEndIndex(); Index = CmdList.GetNextIndex( Index ) )
    {
        const xcmd_array::cmd_entry& Entry = CmdList.GetEntry( Index );
        
        if( Entry.isKindOf( my_command1::getRTTI() ) )
        {
            const my_command1& Cmd1 = my_command1::SafeCast( Entry );
            CPPUNIT_ASSERT( Cmd1.X == 2.0f );
            CPPUNIT_ASSERT( Cmd1.Y == Cmd1.Z );
        }
        else if( Entry.isKindOf( my_command2::getRTTI() ) )
        {
            const my_command2& Cmd2 = my_command2::SafeCast( Entry );
            CPPUNIT_ASSERT( Cmd2.Size == 100 );
        }
        else
        {
            // "UNKNOWN COMMAND"
            CPPUNIT_ASSERT( 0 );
        }
    }
}

