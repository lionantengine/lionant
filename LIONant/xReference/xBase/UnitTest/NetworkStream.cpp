//
//  NetworkStream.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "NetworkStream.h"

void network_stream::Test( void )
{
    xbitstream              BitStream;
    xsafe_array<s32, 100>   LimitArray;      // The range of each item we pack
    xsafe_array<s32, 100>   Value2Pack;      // The actual value to pack (between 0 and limit_array[i] - 1)
    xsafe_array<f32, 100>   FloatPack;       // Some floating point to pack as well
    xsafe_array<xbool,100>  DoInt;           // Should we compress ints or floats
    
    // This following process of "first pick a range, then pick a value"
    // might seem a little weird, but all we're doing here is simulating
    // conditions that you'd have in a real program.  You might have one
    // enumerated type WEAPON_TYPE with 50 possibilities, and then another
    // CHARACTER_CLASS with 4 possibilities, then you might want to pack the
    // number of hit points that some player has, which is a number between
    // 0 and 100.  These ranges would all be stored in "limit_array".  If
    // the current weapon is WEAPON_SWORD=7, and the character class is
    // CHARACTER_CLASS_THIEF=2, and the player has 87 hit points, you
    // would pack the following sequence of numbers:
    
    // value =  7, limit = 50        //// The weapon type is 7
    // value =  2, limit = 4         //// The character class is 2
    // value = 87, limit = 100       //// The player has 87 hit points
    
    s32 i;
    const f32 FloatRange = 1.5f;
    for (i = 0; i < LimitArray.getCount(); i++)
    {
        s32 Limit = (x_rand() % 2000) + 1;      // Pick a random range for each item.
        
        LimitArray[i] = Limit;
        Value2Pack[i] = x_rand() % Limit;    // Pick a number within that range.
        FloatPack[i]  = x_frand( -FloatRange, FloatRange );
        
        DoInt[i] = x_rand()%2;
    }
    
    // Actually pack all these values we just chose.
    for (i = 0; i < LimitArray.getCount(); i++)
    {
        if( DoInt[i] )
        {
            BitStream.Serialize( Value2Pack[i], 0, LimitArray[i] );
        }
        else
        {
            BitStream.Serialize( FloatPack[i], -FloatRange, FloatRange, 15 );
        }
    }
    
    //
    // Now decompress and compare
    //
    
    // Get the result out of the arithmetic coder (as an array of bytes
    // that we can put into a file or transmit over the network).
    xptr<xbyte>     xData;
    s32             Len;
    BitStream.getResult( xData, Len );
    
    // At this point we would, in a real app, transmit this string
    // over the network.  We're going to pretend we just transmitted
    // it, and just received it, and now we'll put it into an
    // Arithmetic_Decoder for processing.
    BitStream.setMode( FALSE );
    BitStream.setPackData( xData, Len );
    
    for( i = 0; i < LimitArray.getCount(); i++ )
    {
        s32 Value;
        if( DoInt[i] )
        {
            BitStream.Serialize( Value, 0, LimitArray[i] );
            CPPUNIT_ASSERT( Value == Value2Pack[i] );
        }
        else
        {
            f32 Value;
            BitStream.Serialize( Value, -FloatRange, FloatRange, 15 );
            f32 Diff = x_Abs( Value - FloatPack[i] );
            CPPUNIT_ASSERT( Diff < FloatRange/10000.0f );
        }
    }

    //
    // Test bit stream with raw bytes
    //
    {
        xptr<xbyte> Data1;
        xptr<xbyte> Data2;
        
        Data1.Alloc( 1024 );
        Data2.Alloc( 1024 );
        
        for( s32 i=0; i<Data1.getCount(); i++ )
        {
            Data1[i] = (xbyte)i;
        }
        // Pack
        xptr<xbyte> Data;
        s32         Length;
        xbitstream  Stream;
        for( s32 i=0; i<Data1.getCount(); i++ )
        {
            Stream.Serialize( Data1[i], 0, 0xff );
        }
        Stream.getResult( Data, Length );
        
        // Unpack
        Stream.setMode( FALSE );
        Stream.setPackData( Data, Length );
        for( s32 i=0; i<Data1.getCount(); i++ )
        {
            Stream.Serialize( Data2[i], 0, 0xff );
        }
        
        CPPUNIT_ASSERT( x_memcmp( &Data2[0], &Data1[0], Data2.getByteCount() ) == 0 );
    }
}
