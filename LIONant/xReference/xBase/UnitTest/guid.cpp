//
//  guid.cpp
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "guid.h"

//------------------------------------------------------------------------------

void guid::Test( void )
{
    xguid Guid;
    
    for( s32 i=0; i<100; i++ )
    {
        //
        // Create a new guid
        //
        Guid.ResetValue();
        
        //
        // Get alpha string
        //
        xstring AlphaString;
        xstring HexString;
        AlphaString = Guid.GetAlphaString();
        xguid Guid2;
        Guid2.SetFromAlphaString( AlphaString );
        CPPUNIT_ASSERT( Guid == Guid2 );
        
        //
        // Get hex string
        //
        HexString = Guid.GetHexString();
        xguid Guid3;
        Guid3.SetFromHexString(HexString);
        CPPUNIT_ASSERT( Guid == Guid3 );
    }
}