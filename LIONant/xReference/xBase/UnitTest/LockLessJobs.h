//
//  LockLessJobs.h
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/6/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class LocklessJobs: public TestFixture
{
public:

    void TestBasicLockless          ( void );
    void TestQuantumPointers        ( void );
    void TestLocklessQueue          ( void );
    void TestLocklessCircularQueue  ( void );
    void TestLocklessHash           ( void );
    void TestLocklessScheguler      ( void );
    
    CPPUNIT_TEST_SUITE(LocklessJobs);
    CPPUNIT_TEST(TestBasicLockless);
    CPPUNIT_TEST(TestQuantumPointers);
    CPPUNIT_TEST(TestLocklessQueue);
    CPPUNIT_TEST(TestLocklessCircularQueue);
    CPPUNIT_TEST(TestLocklessHash);
    CPPUNIT_TEST(TestLocklessScheguler);
	CPPUNIT_TEST_SUITE_END();
};
