//
//  vector3d.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//


class vector3d : public TestFixture
{
public:
    
    void Dot( void );
    void GetLength( void );
    void GetLengthSquared( void );
    void GetPitch( void );
    void GetYaw( void );
    void RotateX( void );
    void RotateY( void );
    void RotateZ( void );
    void Normalize( void );
    void NormalizeSafe( void );
    void Operators( void );
    
    CPPUNIT_TEST_SUITE(vector3d);
    CPPUNIT_TEST(Dot);
    CPPUNIT_TEST(GetLength);
    CPPUNIT_TEST(GetLengthSquared);
    CPPUNIT_TEST(GetPitch);
    CPPUNIT_TEST(GetYaw);
    CPPUNIT_TEST(RotateX);
    CPPUNIT_TEST(RotateY);
    CPPUNIT_TEST(RotateZ);
    CPPUNIT_TEST(Normalize);
    CPPUNIT_TEST(NormalizeSafe);
    CPPUNIT_TEST(Operators);
	CPPUNIT_TEST_SUITE_END();
};

