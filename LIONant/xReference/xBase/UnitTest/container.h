//
//  container.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class container : public TestFixture
{
public:
    
    void TestXArray( void );
    void TestXHArray( void );
    void TestXCmd_Array( void );
    void TestXPArray( void );
    
    CPPUNIT_TEST_SUITE(container);
    CPPUNIT_TEST(TestXArray);
    CPPUNIT_TEST(TestXHArray);
    CPPUNIT_TEST(TestXCmd_Array);
    CPPUNIT_TEST(TestXPArray);
	CPPUNIT_TEST_SUITE_END();
};

