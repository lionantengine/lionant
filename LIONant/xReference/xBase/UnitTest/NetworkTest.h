//
//  NetworkTest.h
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/8/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class network_tests: public TestFixture
{
public:
    
    void BasicUDPConnection( void );
    void BasicTCPConnection( void );

    CPPUNIT_TEST_SUITE(network_tests);
    CPPUNIT_TEST(BasicUDPConnection);
    CPPUNIT_TEST(BasicTCPConnection);
	CPPUNIT_TEST_SUITE_END();
};
