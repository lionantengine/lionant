//
//  data_file.h
//  XBaseUnitest
//
//  Created by Tomas Arce on 9/19/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class data_file : public TestFixture
{
public:
    
    void SerialFileTest( void );
    void TextFileTest( void );
    
    CPPUNIT_TEST_SUITE(data_file);
    CPPUNIT_TEST(SerialFileTest);
    CPPUNIT_TEST(TextFileTest);
	CPPUNIT_TEST_SUITE_END();
};

