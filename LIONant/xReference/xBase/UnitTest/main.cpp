// Problems with libraries and VS2013 http://blogs.msdn.com/b/vcblog/archive/2010/05/03/flexible-project-to-project-references.aspx

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"

#include "plus.h"
#include "data_file.h"
#include "container.h"
#include "CommandLine.h"
#include "TestXMatrix.h"
#include "ThreadUnitTest.h"
#include "FileUnitTest.h"
#include "BasicXBase.h"
#include "LockLessJobs.h"
#include "NetworkTest.h"
#include "guid.h"
#include "math_test.h"
#include "vector3.h"
#include "vector3d.h"
#include "vector2.h"
#include "xPtrTest.h"
#include "xPtr2Test.h"
#include "Bitstream.h"
#include "NetworkStream.h"
#include "StringUnit.h"

// Easy Macros to turn on/off tests
#define CPPUNIT_TEST_SUITE_REGISTRATION_1(A)   
#define CPPUNIT_TEST_SUITE_REGISTRATION_0(A)    CPPUNIT_TEST_SUITE_REGISTRATION(A)

// Actual tests to run
CPPUNIT_TEST_SUITE_REGISTRATION_0( string_unit );
CPPUNIT_TEST_SUITE_REGISTRATION_1( bitstream );
CPPUNIT_TEST_SUITE_REGISTRATION_1( thread_unitest );
CPPUNIT_TEST_SUITE_REGISTRATION_1( FileUnitTest );
CPPUNIT_TEST_SUITE_REGISTRATION_1( plus );
CPPUNIT_TEST_SUITE_REGISTRATION_1( data_file );
CPPUNIT_TEST_SUITE_REGISTRATION_1( container );
CPPUNIT_TEST_SUITE_REGISTRATION_1( command_line );
CPPUNIT_TEST_SUITE_REGISTRATION_1( BasicXBaseTest );
CPPUNIT_TEST_SUITE_REGISTRATION_1( TestXMatrix4 );
CPPUNIT_TEST_SUITE_REGISTRATION_1( vector2 );
CPPUNIT_TEST_SUITE_REGISTRATION_1( vector3d );
CPPUNIT_TEST_SUITE_REGISTRATION_1( vector3 );
CPPUNIT_TEST_SUITE_REGISTRATION_1( math_test );
CPPUNIT_TEST_SUITE_REGISTRATION_1( guid );
CPPUNIT_TEST_SUITE_REGISTRATION_1( xptr_test );
CPPUNIT_TEST_SUITE_REGISTRATION_1( xptr2_test );
CPPUNIT_TEST_SUITE_REGISTRATION_1( network_stream );
CPPUNIT_TEST_SUITE_REGISTRATION_1( command_line );
CPPUNIT_TEST_SUITE_REGISTRATION_1( LocklessJobs );
CPPUNIT_TEST_SUITE_REGISTRATION_1( network_tests );

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

void DoTests( void )
{
    g_Scheduler.Init();

    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;
    
    // Add a listener that collects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener( &result );        
    
    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener( &progress );      
    
    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
    runner.run( controller );
    
    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
    outputter.write(); 
}

//---------------------------------------------------------------
#ifdef TARGET_3DS
extern "C"
{
    void  nnMain(void)
    {
        x_BaseInit();
        DoTests();
        x_BaseKill();

    }
}
#else
int main()
{
    DoTests();
    return 0;
}
#endif