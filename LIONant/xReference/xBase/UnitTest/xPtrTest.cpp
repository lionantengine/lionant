//
//  xPtrTest.cpp
//  XBaseUnitestOSX
//
//  Created by Tomas Arce on 6/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "xPtrTest.h"

//
// Test Basic Iterators
//
void xptr_test::TestLoops( void )
{
    // Check the C++11 compatibility with basic array
    xsafe_array<s32,100> Array;
    for( xiter< xsafe_array<s32,100>, s32 > b=Array.begin(), e=Array.end(); b!=e; ++b)
    {
        *b = b.m_i;
    }
    
    // Check the C++11 compatibility with xptr
    xptr<s32> IntPtr;
    IntPtr.Alloc(100);
    for( s32 i = 0; i < IntPtr.getCount(); i++ )
    {
        IntPtr[i] = 100;

    }
    // Check clasical iterations
    for( s32 i=0, e=IntPtr.getCount(); i<e; ++i )
    {
        CPPUNIT_ASSERT( IntPtr[i] == 100 );
        CPPUNIT_ASSERT( Array[i]  == i );
    }
}

//
// Test References...
//
void xptr_test::TestReferences( void )
{
    // Create some variables with xptr
    xptr<s32> A;
    A.Alloc(100);
    for( s32 i = 0; i < A.getCount(); i++ )
    {
        A[i] = 100;
    }
    
    //
    // See if going out of the scope kills A..
    //
    {
        const xptr<s32> B( A );
        for( s32 i=0, e=B.getCount(); i<e; ++i )
        {
            CPPUNIT_ASSERT( B[i] == 100 );
            
#ifndef TARGET_3DS
            // Try to ref one up again
            [&A,i](  xptr<s32> C ){
                s32 Count = A.getXPtrCount();
                CPPUNIT_ASSERT( Count == 3 );
                
            }( A );
#endif
        }
        
        // Two xptr pointing at the same memory
        CPPUNIT_ASSERT( A.getXPtrCount() == 2 );
    }
    
    // Two xptr pointing at the same memory
    CPPUNIT_ASSERT( A.getXPtrCount() == 1 );


    //
    // Test xptr with static memory
    //
    {
        char Buffer[12];
        xptr<char> X( Buffer, 12, xptr<char>::FLAG_STATIC_MEMORY );

        for( auto& Y : X )
            Y = 127;
    }
}

