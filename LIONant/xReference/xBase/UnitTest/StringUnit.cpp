#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "x_base.h"
#include "StringUnit.h"

template<class T, s32 ENTRY_PER_PAGE>
class x_qt_page_pool
{
public:

    using entry = T;

public:

    void Free( entry* pPtr )
    {
        ASSERT( pPtr );
        ASSERT( u64( pPtr ) % xalingof( entry ) == 0 );

        m_nEntries.Dec();

        do
        {
            const x_qt_ptr Local( m_FreeEntryHead );
            x_qt_ptr       New( Local, pPtr );

            // set the next pointer
            *((entry**)&pPtr) = (entry*)Local.GetPtr();

            if( m_FreeEntryHead.casCopyPtr( Local, New ) ) 
                break;

        } while(1);
    }

    entry* Alloc( void )
    {
        // minimum requirements for the entry
        ASSERTCT( sizeof( entry )   >= xalingof( entry ) );
        ASSERTCT( sizeof( entry )   >= sizeof( u64 ) );
        ASSERTCT( xalingof( entry ) >= sizeof( u64 ) );
        ASSERTCT( sizeof( entry )   %  xalingof( entry ) == 0 );
        ASSERTCT( x_has_vtable_type( entry ) == FALSE );

        m_nEntries.Inc();

        //
        // return allocated entry
        // 
        do
        {
            const x_qt_ptr  Local( m_FreeEntryHead );
            entry*          pEntry = (entry*)Local.GetPtr();
            
            if( pEntry == NULL )
                break;

            x_qt_ptr       New( Local, &Local.DereferencePtr() );

            if ( m_FreeEntryHead.casCopyPtr( Local, New ) )
                return pEntry;

        } while ( 1 );

        //
        // If not entries then allocate a page
        //
        page& Page = *x_new( page, 1, 0 );

        //
        // Link all the entries together
        //
        for ( s32 e = 1; e<( ENTRY_PER_PAGE - 1 ); e++ )
        {
            *( (entry**)&Page.m_Mem[ e ] ) = &Page.m_Mem[ e + 1 ];
        }

        //
        // Insert the page into the page list
        //
        do
        {
            const x_qt_ptr  Local( m_PageHead );
            const x_qt_ptr  New( Local, &Page );

            Page.m_pNext = (page*)Local.GetPtr();

            if ( m_PageHead.casCopyPtr( Local, New ) )
                break;

        } while ( 1 );

        //
        // Insert all the new entries 
        // 
        do
        {
            const x_qt_ptr  Local( m_FreeEntryHead );
            const x_qt_ptr  New( Local, &Page.m_Mem[ 1 ] );

            *((entry**)&Page.m_Mem[ ENTRY_PER_PAGE - 1 ]) = (entry*) Local.GetPtr();

            if ( m_FreeEntryHead.casCopyPtr( Local, New ) )
                break;

        } while ( 1 );

        // return one entry to the user
        return &Page.m_Mem[ 0 ];
    }

    ~x_qt_page_pool( void )
    {
        page* pPage = (page*)m_PageHead.GetPtr();
        m_PageHead.setNull();

        while( pPage )
        {
            page* pTempPage = pPage;
            pPage = pPage->m_pNext;
            x_delete( pTempPage );
        }
    }

protected:

    // PROPOSAL:   
    // To iterate go through the pages
    // then in each of the pages go through all the entries
    // entries with m_bUsed == TRUE are been used
    struct iteratable_entry
    {
        entry               m_Entry;
        u8                  m_bUsed = FALSE;    
    };

    struct page
    {
        xsafe_array<entry, ENTRY_PER_PAGE>  m_Mem;
        page*                               m_pNext = NULL;
    };

protected:

    x_qt_ptr                                m_FreeEntryHead = NULL;
    x_qt_ptr                                m_PageHead      = NULL;
    x_qt_counter                            m_nEntries;
};

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

template< class T >
class dereference_ptr
{
public:

    using id = u16;

public:

    dereference_node* getNode( void ) 
    { 
        if( !m_pNode ) return NULL;
        if( m_ID != m_pNode->m_ID ) return NULL;
        return m_pNode; 
    } 

    xbool isValid( void ) const { return !!m_pNode; }

protected:

    id                  m_ID;
    dereference_node*   m_pNode;
};

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

class dereference_node
{
public:

    using id = u16;

public:

    T* getElement( void )
    {
        return m_pPtr;
    }

    void init( dereference_ptr& Ptr, void* pData )
    {
        m_RefCount = 1;
        m_ID++;
        m_pPtr     = pData;

        Ptr.m_ID    = m_ID;
        Ptr.m_pNode = this;
    }

    void* Release( void )
    {
        do
        {
            const ref_count Local( m_Ref );
            ref_count       New( Local );

            if( New.m_Count <= 0 )
                return NULL;

            New.m_Count--;

            if( x_cas32( (u32*)&m_Ref, Local, New ) )
            {
                if( New.m_Count == 0 )
                    return m_pPtr;

                break;
            }

        } while(1);

        return NULL;
    }

protected:

    struct ref_count
    {
        s16         m_Count;
        id          m_ID;
    };

protected:

    ref_count   m_Ref;
    T*          m_pPtr;       // Pointer to memory
};

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

class derefence_ptr_pool
{
public:

    template<class T>
    T* getElement( dereference_ptr& Ptr )
    {
        ASSERT( Ptr.isValid() );

        dereference_node* pNode  = Ptr.getNode();
        if( !pNode ) return NULL;
        return pNode->getElement();
    }               

    dereference_node& Alloc( dereference_ptr& Ptr, void* pData )
    {
        ASSERT( FALSE == Ptr.isValid() );

        dereference_node& Node = m_Pool.Alloc();
        Node.init( Ptr, pData );
        return Node;
    }

    template<class T>
    T* Free( dereference_ptr& Ptr )
    {
        ASSERT( Ptr.isValid() );

        dereference_node* pNode = Ptr.getNode();
        ASSERT( pNode );

        T* pData = (T*)pNode->Release();
        if( pData )
        {
            m_Pool.Free( pNode );
        }

        return pData;
    }

protected:

    x_qt_page_pool<dereference_node, 512>  m_Pool;
};

static derefence_ptr_pool s_DereferencePool;

//----------------------------------------------------------------------------------
template<class T>
struct xstring_internal
{
public:

    using internal = xstring_internal<T>;

public:

    //------------------------------------------------------------------------------

    static internal& StringToInternal( T* pString )
    {
        internal& Alloc = *( internal* )&( ( (u8*)pString )[ -INTERNAL_SIZE ] );
        return Alloc;
    }

    //------------------------------------------------------------------------------

    static T* InternalToString( internal& Internal )
    {
        T* pString = (T*)&( ( (u8*)&Internal )[ INTERNAL_SIZE ] );
        return pString;
    }

public:

    T               m_Flags;
};

template< class T >
class xstring_global_pool
{    
public:
    
    //------------------------------------------------------------------------------
    T* Alloc( s32 Length )
    {
        internal* pAlloc = NULL;
        T         Flags  = T( xstring::FLAGS_NON_SYSTEM | xstring::FLAGS_CAN_GROW );

        //
        // Handle the allocation from our pools
        //
        if ( Length <= 32 )       { pAlloc = (internal*)m_Pool32.Alloc();  Flags |= xstring::FLAGS_FIXED_SIZE_32;  }
        else if ( Length <= 64 )  { pAlloc = (internal*)m_Pool64.Alloc();  Flags |= xstring::FLAGS_FIXED_SIZE_64;  }
        else if ( Length <= 128 ) { pAlloc = (internal*)m_Pool128.Alloc(); Flags |= xstring::FLAGS_FIXED_SIZE_128; }
        else if ( Length <= 256 ) { pAlloc = (internal*)m_Pool256.Alloc(); Flags |= xstring::FLAGS_FIXED_SIZE_256; }
        else
        {
            //
            // If we made it here the string must be big so lets call malloc
            //
            pAlloc = (internal*)x_malloc( sizeof( T ), Length + internal::INTERNAL_SIZE, xmem_aligment( internal ) );
        }

        //
        // Init the ref count and the flags
        //
        pAlloc->m_Ref.m_Count = 1;
        pAlloc->m_Ref.m_UID++;
        pAlloc->m_Flags = Flags;

        //
        // Init the string
        //
        T* pString = internal::InternalToString( *pAlloc );
        pString[0] = 0;

        return pString;
    }

    //------------------------------------------------------------------------------

    void Free( T* pString, u16 UID )
    {
        ASSERT( pString );

        //
        // Get the flags and make sure everything is sound
        //
        internal& Alloc = internal::StringToInternal( pString );
        ASSERT( x_FlagIsOn( Alloc.m_Flags, xstring::FLAGS_NON_SYSTEM ) );
        ASSERT( x_FlagIsOn( Alloc.m_Flags, xstring::FLAGS_CAN_GROW ) );
        ASSERT( Alloc.m_Ref.m_UID == UID );

        //
        // Handle reference count here
        //        
        do 
        {
            const internal:ref_count Local = Alloc.m_Ref;
            internal::ref_count      New   = Local;

            New.m_Count--;

            if( x_cas32( &Alloc.m_Ref, Local, New ) )
            {
                if( New.m_Count != 0 )
                    return;

                break;
            }

        } while(1);

        //
        // Is it allocated from one of our pools?
        //
        if ( x_FlagIsOn( Alloc.m_Flags, xstring::FLAGS_FIXED_SIZE_MASK ) )
        {
            switch ( Alloc.m_Flags & xstring::FLAGS_FIXED_SIZE_MASK )
            {
            default:                           ASSERT( 0 ); break;    // We dont have support for this pool
            case xstring::FLAGS_FIXED_SIZE_8:  ASSERT( 0 ); break;    // We dont have support for this pool
            case xstring::FLAGS_FIXED_SIZE_16: ASSERT( 0 ); break;    // We dont have support for this pool

            case xstring::FLAGS_FIXED_SIZE_32:  m_Pool32.Free ( (data32*) &Alloc ); break;
            case xstring::FLAGS_FIXED_SIZE_64:  m_Pool64.Free ( (data64*) &Alloc ); break;
            case xstring::FLAGS_FIXED_SIZE_128: m_Pool128.Free( (data128*)&Alloc ); break;
            case xstring::FLAGS_FIXED_SIZE_256: m_Pool256.Free( (data256*)&Alloc ); break;
            }
        }
        else
        {
            //
            // It must have been a huge string
            //
            x_free( &Alloc );
        }
    }

protected:

    template< s32 ENTRY_COUNT >
    struct data
    {
        alignas(u64) T m_Data[ ENTRY_COUNT ];
    };

    using internal = xstring_internal<T>;
    using data32   = data< 32  + internal::INTERNAL_SIZE >;
    using data64   = data< 64  + internal::INTERNAL_SIZE >;
    using data128  = data< 128 + internal::INTERNAL_SIZE >;
    using data256  = data< 256 + internal::INTERNAL_SIZE >;

protected:

    x_qt_page_pool< data32,  512 > m_Pool32;
    x_qt_page_pool< data64,  256 > m_Pool64;
    x_qt_page_pool< data128, 128 > m_Pool128;
    x_qt_page_pool< data256, 128 > m_Pool256;
};

static xstring_global_pool<char>  s_CharPool;
static xstring_global_pool<wchar> s_WCharPool;


template<class T>
class xstorage_string_dynamic
{
protected:

    T*      Alloc( s32 Count );
    void    Free ( T* );

protected:

    dereference_ptr m_Ptr;

protected:

};

//----------------------------------------------------------------------------------

template<class T, s32 ENTRY_COUNT>
class xstorage_fix
{
protected:

    T*      Alloc( s32 Count ) { ASSERT( Count < ENTRY_COUNT ); }
    void    Free( T* )         {}

protected:
    xsafe_array<T,ENTRY_COUNT + internal::INTERNAL_SIZE/sizeof(T)>  m_Entry;
};

//----------------------------------------------------------------------------------

template<class T, class S>
class xstring_base : public S
{
public:

protected:

};

//----------------------------------------------------------------------------------

class xstring2 : public xstring_base< char, xstorage_string_dynamic<char> >
{

protected:
};

//----------------------------------------------------------------------------------

template< s32 ENTRY_COUNT >
class xstring2_fix : public xstring_base< char, xstorage_fix<char, ENTRY_COUNT> >
{
protected:
};

//----------------------------------------------------------------------------------

class xwstring2 : public xstring_base< wchar, xstorage_string_dynamic<wchar> >
{
protected:
};


 
alignas( 16 )
struct xx
{
   // virtual void test(void){}
    s32 x,y,z;
    char xxxx[64];
};

void string_unit::BasicTest( void )
{
    auto     pString1 = X_STR("Hello world");
    auto     pString2 = X_WSTR("Hello world");

    //
    // Basic text of the page pool
    //    
    {
        x_qt_page_pool< xx, 3 >   PagePool;
        xarray<xx*>                Entries;

        for ( s32 i = 0; i<100; i++ )
            Entries.append() = PagePool.Alloc();

        for ( s32 i = 0; i<100; i++ )
            PagePool.Free( Entries[ i ] );
    }

    //
    // multijob test
    //    
    {
        x_qt_page_pool< xx, 3 >         PagePool;
        x_inline_light_jobs_block<8>    Block;
        for( s32 i=0; i<100; i++ ) Block.SubmitJob( [&PagePool]()
        {
            xarray<xx*>     Entries;
            xrandom_small   Random;

            Random.setSeed64( u64(&Random) );

            for( s32 i=0; i<10000; i++ )
            {
                if( Entries.getCount() < 100 && Random.Rand32()&1 )
                {
                    Entries.append() = PagePool.Alloc();
                }
                else if( Entries.getCount() > 0 )
                {
                    s32 Index = Random.Rand32( 0, Entries.getCount() );
                    PagePool.Free( Entries[ Index ] );
                    Entries.DeleteWithSwap( Index );
                }
            }

        }); Block.FinishJobs();

        s32 x = 0;
    }

    //
    // Alloc a string
    //
    char* pString = s_CharPool.Alloc( 64 );
    wchar* pStr12 = s_WCharPool.Alloc( 64 );

    s_CharPool.Free( pString );
    s_WCharPool.Free( pStr12 );

    s32 x=0;
}