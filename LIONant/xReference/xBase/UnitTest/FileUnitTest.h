//
//  FileUnitTest.h
//  xBaseUnitTest
//
//  Created by Simon Hudson on 9/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

class FileUnitTest: public TestFixture
{
public:
    void syncModeTest();
    void asyncModeTest();
    
    CPPUNIT_TEST_SUITE(FileUnitTest);
    CPPUNIT_TEST(syncModeTest);
    CPPUNIT_TEST(asyncModeTest);
	CPPUNIT_TEST_SUITE_END();
};
