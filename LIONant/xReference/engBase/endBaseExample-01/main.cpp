#include "eng_base.h"

static eng_render_buffer    s_RenderToBuffer;
static eng_texture          s_TextureFromBuffer;

static eng_keyboard         s_Keyboard;
static eng_mouse            s_Mouse;
static eng_touchpad         s_TouchPad;


static eng_sprite_render_2dgroup s_SpriteGroup;

static eng_texture_rsc::ref s_TextRef;
static eng_texture_rsc::ref s_EnvTextRef;
static eng_texture_rsc::ref s_NormalMapDetailRef;

static eng_geom s_Geom;

struct my_sprite
{
    void Init( eng_sprite_rsc::ref& Ref, const char* pString )
    {
        m_Sprite.setup( Ref, pString );
    }
    
    eng_sprite  m_Sprite;
};

static xsafe_array<my_sprite,40>                s_Sprite;
static xsafe_array<eng_sprite_animated,10>      s_AnimatedSprite;
static eng_sprite_rsc::ref                      s_DeamonKingAtlast;

static xsafe_array<eng_font,2>                  s_Font;


//-------------------------------------------------------------------------------

void Draw_SimpleColorChange( f32 DeltaTime )
{
    static f32 r = 0.2f;
    static f32 g = 0.3f;
    static f32 b = 0.4f;
    
    r += 300*DeltaTime;
    g += 300*DeltaTime;
    b += 300*DeltaTime;
    
    eng_GetCurrentContext().SetClearColor( xcolor( xvector3(r,g,b)));
    
    if(r>1) r = 0;
    if(g>1) g = 0;
    if(b>1) b = 0;
}

//-------------------------------------------------------------------------------

void Draw_TestLines_Parametric( f32 DeltaTime )
{
    eng_draw& Draw = eng_GetCurrentContext().getDraw();

    Draw.Begin( ENG_DRAW_MODE_2D_PARAMETRIC );

    const f32 Half = 0.5f;
    
    for( f32 i=0;i<1; i+=0.001f )
    {
        Draw.DrawLine( draw_vertex( 0, (i+Half)*Half, -i, xcolor(255,0,0,255)),
                      draw_vertex( 1, (i+Half)*Half, -i, xcolor(0,255,255,255)) );
    }

    for( f32 i=0;i<1; i+=0.001f )
    {
        Draw.DrawLine( draw_vertex( (i+Half)*Half, 0, -i, xcolor(0,255,0,255)), 
                      draw_vertex( (i+Half)*Half, 1, -i, xcolor(0,0,255,255)) );
    }

    for( s32 i=0;i<1000; i++ )
    {
        u8     I = 128 + (x_rand()&0x5f);
        xcolor  C( I,I,I,255);
        Draw.DrawLine( draw_vertex( x_frand(-1.0f, 1.0f), x_frand(-1.0f, 1.0f), 0, C ),
                      draw_vertex( x_frand(-1.0f, 1.0f), x_frand(-1.0f, 1.0f), 0, C ) );
    }

    
    // Render the half rectangle
    Draw.DrawLine( draw_vertex( -Half, Half, 0, xcolor(255,0,0,255)),
                  draw_vertex(  Half, Half, 0, xcolor(255,0,0,255)) );
    
    Draw.DrawLine( draw_vertex( Half, Half,  0, xcolor(255,0,0,255)),
                 draw_vertex(  Half, -Half, 0, xcolor(255,0,0,255)) );

    Draw.DrawLine( draw_vertex( Half, -Half, 0, xcolor(255,0,0,255)),
                 draw_vertex( -Half, -Half, 0, xcolor(255,0,0,255)) );
    
    Draw.DrawLine( draw_vertex( -Half, -Half,0, xcolor(255,0,0,255)),
                 draw_vertex(  -Half, Half, 0, xcolor(255,0,0,255)) );
    
    
    Draw.End();
}

//-------------------------------------------------------------------------------

void Draw_TestLines( f32 DeltaTime )
{
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    Draw.Begin( ENG_DRAW_MODE_2D_LT );

    const xirect& Rect =  eng_GetCurrentContext().GetActiveView().getViewport( );


    for( s32 i=0;i<1000; i++ )
    {
        u8     Ir = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        u8     Ig = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        u8     Ib = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        xcolor C( Ir,Ig,Ib,255);

        Draw.DrawLine( draw_vertex( x_frand((f32)Rect.m_Left, (f32)Rect.m_Right), x_frand((f32)Rect.m_Top, (f32)Rect.m_Bottom), 0, C ),
                      draw_vertex( x_frand((f32)Rect.m_Left, (f32)Rect.m_Right), x_frand((f32)Rect.m_Top, (f32)Rect.m_Bottom), 0, C ));
    }
    Draw.End();
}

//-------------------------------------------------------------------------------

void Draw_TestMixture_Parametric( f32 DeltaTime )
{
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    Draw.Begin( ENG_DRAW_MODE_2D_PARAMETRIC | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_ZBUFFER_OFF );

    for( s32 i=0;i<1000; i++ )
    {
        u8     Ir = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        u8     Ig = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        u8     Ib = x_Min( 0xff, 0x4f + (x_rand()&0x9f) );
        xcolor C( Ir,Ig,Ib,255);

        Draw.DrawLine( draw_vertex( x_frand(-1.0f, 1.0f), x_frand(-1.0f, 1.0f), 0, C ),
                      draw_vertex( x_frand(-1.0f, 1.0f), x_frand(-1.0f, 1.0f), 0, C ) );
    }

    for( s32 i=0;i<1000; i++ )
    {
        xcolor C( x_rand(),x_rand(),x_rand(), 128 );
        Draw.DrawTriangle( draw_vertex( x_frand(0.0f, 1.0f), x_frand(0.0f, 1.0f), 0, C ), 
                          draw_vertex( x_frand(0.0f, 1.0f), x_frand(0.0f, 1.0f), 0, C ),
                          draw_vertex( x_frand(0.0f, 1.0f), x_frand(0.0f, 1.0f), 0, C ));
    }

    Draw.End();
}

//-------------------------------------------------------------------------------

void Test3D( f32 DeltaTime )
{
    static f32 Angle=0;
    xmatrix4   Matrix;

    eng_view View;
    View.LookAt( 50.f, xradian3( X_RADIAN( -45 ), 0, 0 ), xvector3( 0, 0, 0 ) );
    eng_GetCurrentContext().SetActiveView( View );


    Matrix.Identity();
    Matrix.setRotation( xradian3( Angle, Angle*0.5f, Angle*0.05f) );
    Matrix.setTranslation( xvector3( x_Sin(Angle)*5, 0, x_Cos(Angle)*5 ) );
    Angle += 300*DeltaTime;

    eng_draw& Draw = eng_GetCurrentContext().getDraw();

    if( 1 )
    {
        Draw.DrawMarker( Matrix.getTranslation(), xcolor(255,0,0,255) );
    }

    if( 1 )
    {
        Draw.Begin( ENG_DRAW_MODE_3D | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_ZBUFFER_READ_ONLY );
            Draw.SetL2W( Matrix );
            Draw.DrawBBox( xbbox( xvector3( 0,0,0 ), 3 ), xcolor(~0), TRUE );
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void TestTexture( f32 DeltaTime, u32 DrawFlags = 0 )
{
    static f32 Angle=0;
    xmatrix4   Matrix;
    xmatrix4   Matrix2;
    
    const eng_texture* pMyTexture = s_TextRef.getAsset();
    if( pMyTexture == NULL )
        return;
    
    Matrix.Identity();
    Matrix.setRotation( xradian3( Angle, Angle*0.5f, Angle*0.05f) );
    Matrix.setTranslation( xvector3(0,0,-5) );
    
    Matrix2.Identity();
    Matrix2.setScale( 0.2f );
    Matrix2.Rotate( xradian3( 2*Angle, Angle, Angle) );
    Matrix2.setTranslation( xvector3( x_Sin(Angle*4)*10, 0, x_Cos(Angle*4)*10 ) );
    
    Angle += 300*DeltaTime;
    
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    if( 1 )
    {
        Draw.Begin( ENG_DRAW_MODE_3D | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_TEXTURE_ON | DrawFlags );
        
        Draw.SetL2W( Matrix );
        Draw.SetTexture( *pMyTexture );//s_TestTexture );//eng_texture::getDefaultTexture() );
        Draw.DrawBBox( xbbox( xvector3( 0,0,0 ), 3 ), xcolor(~0), TRUE );
        
        Draw.SetL2W( Matrix2 );
        Draw.ClearTexture();
        Draw.DrawBBox( xbbox( xvector3( 0,0,0 ), 3 ), xcolor(180,255,180,255), TRUE );
        
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void TestRenderToTexture( f32 DeltaTime )
{
    //
    // Render the the scene in as a texture
    //
    eng_view View;
    
    View.setViewport( xirect( 0,0,s_RenderToBuffer.getWidth(), s_RenderToBuffer.getHeight() ));
    
    s_RenderToBuffer.Activate( View, s_TextureFromBuffer );
    
    // Clear Screen And Depth Buffer
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
    eng_CheckForError();
    
    TestTexture( DeltaTime, ENG_DRAW_MODE_MISC_FLUSH );
    
    s_RenderToBuffer.Deactivate();
    
    //
    // Render with a cube...
    //
    static f32 Angle=0;
    xmatrix4   Matrix;
    
    Matrix.Identity();
    Matrix.setScale( 2 );
    Matrix.Rotate( xradian3( Angle, Angle*0.5f, Angle*0.05f) );
    Matrix.setTranslation( xvector3(0,0,-5) );
    
    Angle += 300*DeltaTime;
    
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    if( 1 )
    {
        Draw.Begin( ENG_DRAW_MODE_3D | ENG_DRAW_MODE_BLEND_ALPHA | ENG_DRAW_MODE_TEXTURE_ON );
        
        Draw.SetL2W( Matrix );
        Draw.SetTexture( s_TextureFromBuffer );
        Draw.DrawBBox( xbbox( xvector3( 0,0,0 ), 3 ), xcolor(~0), TRUE );
        
        Draw.End();
    }    
}

//-------------------------------------------------------------------------------

void InputTest( f32 DeltaTime )
{
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    //
    // Handle the mouse
    //
    {
        xrect Rect;
        
        Rect.setup( s_Mouse.getValue( eng_mouse::ANALOG_POS_ABS ).m_X,
                    s_Mouse.getValue( eng_mouse::ANALOG_POS_ABS ).m_Y,
                    40 );
        
        Draw.Begin( ENG_DRAW_MODE_2D_LT );
        Draw.DrawSolidRect( Rect, xcolor(0,255,0,255) );
        Draw.End();
    }
    
    //
    // Handle the TouchPad
    //
    for( s32 i=0; i<eng_touchpad::MAX_TOUCHES; i++ )
    {
        if( s_TouchPad.isPressed( s_TouchPad.getTouchBtn(i) ) )
        {
            xrect Rect;
            
            Rect.setup( s_TouchPad.getValue( s_TouchPad.getTouchAbs(i) ).m_X,
                        s_TouchPad.getValue( s_TouchPad.getTouchAbs(i) ).m_Y,
                        40 );
            
            Draw.Begin( ENG_DRAW_MODE_2D_LB );
            Draw.DrawSolidRect( Rect, xcolor(255,0,0,255) );
            Draw.End();
        }
    }
}


//-------------------------------------------------------------------------------

void TestFont( f32 DeltaTime )
{
   // eng_GetCurrentContext().SetClearColor(xcolor(0xf5f500ff));
    
    static f32 Scale = 1.0;
    static f32 Dir   = 1;

    if( Scale > 50 )
    {
        Dir = -1;
        Scale = 50;
    }
    else if( Scale < 0.3 )
    {
        Dir = 1;
        Scale = 0.3f;
    }
    
    xrandom_small Small;
    Small.setSeed32( 333 );

    for( s32 i=0; i<5; i++ )
    {
        Scale += Dir*Scale*Scale*DeltaTime*60;

        s_Font[1].DrawText( eng_GetCurrentContext().getDraw(), xvector2(300-i*50.f,500+i*50.f), "Hellow World", 5/Scale,
            xcolor( Small.Rand32( 0, 255 ), Small.Rand32( 0, 255 ), Small.Rand32( 0, 255 ), 128 ) );
        
        s_Font[0].DrawText( eng_GetCurrentContext().getDraw(), xvector2(100+i*50.f,100+i*50.f), "Hellow World", 5/Scale, 
            xcolor( Small.Rand32( 0, 255 ), Small.Rand32( 0, 255 ), Small.Rand32( 0, 255 ), 255 ) );
    }
    
     s_Font[1].DrawText( eng_GetCurrentContext().getDraw(), xvector2(0,20), xfs("The quick brown fox jumps over the lazy dog, Scale: %f",5/Scale), 0.8f, xcolor(~0) );

     s32 W, H;
     eng_GetCurrentContext().GetScreenResolution(W,H);
     auto& Draw = eng_GetCurrentContext().getDraw();
     Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_BLEND_ALPHA );
     Draw.DrawLine( draw_vertex( 0, H-23, 0, xcolor(0xff, 0xff, 0xff, 128) ),
                    draw_vertex( W, H- 23, 0, xcolor( 0xff, 0xff, 0xff, 128 ) ) );
     Draw.End();
}

//-------------------------------------------------------------------------------

void TestSprite( f32 DeltaTime )
{
  //  eng_GetCurrentContext().SetClearColor(xcolor(0xf5f500ff));

    //
    // Make sure that we have all the sprites loaded
    //
    for( auto& AnimSprite : s_AnimatedSprite )
    {
        if( AnimSprite.Resolve() == NULL )
            return;
    }
    
    //
    // Advance the animation
    //
    for( auto& AnimSprite : s_AnimatedSprite )
    {
        eng_sprite_animated::event Event;
        AnimSprite.BeginAdvanceTime( DeltaTime );
        while( AnimSprite.WhileAdvanceTime( Event ) )
        {
            // Handle events here
        }
    }
    
    //
    // Render the sprite
    //
    xmatrix4 Matrix;
    Matrix.Identity();
    
    s32 XRes, YRes;
    eng_GetCurrentContext().GetScreenResolution( XRes, YRes );

    s_SpriteGroup.Begin();
    {
        xrandom_small Small;
        Small.setSeed32(333);
        
        for( auto& Entry:s_Sprite )
        {
            const f32 X = (f32)Small.Rand32( 100, XRes - 100 );
            const f32 Y = (f32)Small.Rand32( 0, YRes - 100 );
            const f32 Z = (f32)Small.Rand32( 0,100 );
            
            if ( Small.Rand32( ) & 1 ) Matrix.setScale( xvector3( -1, 1, 1 ) );
            else                       Matrix.setScale( xvector3(1,1,1));
            
            Matrix.setTranslation( xvector3( X, Y, Z) );
            s_SpriteGroup.qtAddSprite( Entry.m_Sprite, Matrix, xcolor(~0) );
        }

        for( auto& AnimSprite : s_AnimatedSprite )
        {
            const f32 X = (f32)Small.Rand32( 100, XRes - 100 );
            const f32 Y = (f32)Small.Rand32( 0, YRes - 110 );
            const f32 Z = (f32)Small.Rand32( 0, 50 );

            if(Small.Rand32()&1) Matrix.setScale( xvector3(-1,1,1));
            else                 Matrix.setScale( xvector3(1,1,1));
            
            Matrix.setTranslation( xvector3( X, Y, Z ) );
            s_SpriteGroup.qtAddSprite( AnimSprite, Matrix, xcolor(~0) );
        }
    }
    s_SpriteGroup.Flush();
}

//-------------------------------------------------------------------------------

void RenderCustomShader( f32 DeltaTime )
{
    static xbool                bInit = FALSE;
    static eng_simple_shader    Shader;

    if( bInit == FALSE )
    {
        bInit = TRUE;
        static char pFShader[] = R"(
            #ifdef GL_ES
            precision mediump float; // precision highp float;
            #endif

            varying vec4        Color;
            varying vec2        Texcoord;

            void main()
            {
                const vec2  pitch               = vec2( 50., 50. );
                const float scaleFactor         = 1000.0;
                const float thickness           = 0.98;
                const float Sharpness           = 9.;
                const float precisionThickness  = thickness * 0.5;

                vec2  offset = Texcoord * scaleFactor ;
                vec2  tiles1 = pitch * thickness - mod( offset - precisionThickness, pitch );
                vec2  tiles2 = pitch * thickness * 0.2 - mod( offset - precisionThickness, pitch/5. );
                vec2  r      = vec2( tiles1.x * tiles1.y * Sharpness, tiles2.x * tiles2.y * Sharpness * 8. );
                vec2  width  = fwidth(r);
                vec2  alpha  = 1. - clamp(smoothstep(0.5-width, 0.5+width, r), 0.2, 0.8 );
                gl_FragColor = vec4( Texcoord.x, Texcoord.y, Color.z, alpha.y * .5 + alpha.x );
            }
        )";

        Shader.m_FShader.LoadFromMemory( pFShader, x_strlen(pFShader)+1 );
        Shader.CompileProgram();
        Shader.CompileGeometry( eng_simple_shader::GEOM_TYPE_XZ_PLANE, xcolor(~0) );
    }

    eng_draw& Draw = eng_GetCurrentContext().getDraw();

    eng_view View;
    static f32 t=0;
    t+= DeltaTime*8000;

    View.LookAt( 1000, xradian3(X_RADIAN(-45),X_RADIAN(t), 0 ), xvector3( 0, 0, 0 ) );
    eng_GetCurrentContext().SetActiveView( View );

    const eng_texture* pMyTexture = s_TextRef.getAsset();
    if ( pMyTexture )
    {
        Draw.Begin(ENG_DRAW_MODE_TEXTURE_ON);
        Draw.ClearL2W();
        Draw.SetTexture( *pMyTexture );
        Draw.DrawBBox( xbbox(xvector3d(0,0,0), 100 ), xcolor(187,187,187,0xff) );
        Draw.End();
    }
    //
    // Render the shader
    //
    xmatrix4 L2W;
    L2W.Identity();
    L2W.setScale( 1000.f );
    
    Shader.Render( L2W );
}

//-------------------------------------------------------------------------------

struct pvert : draw_vertex
{
    xvector3    m_Normal;
};


void SolidSphere( xptr<u16>& Index, xptr<pvert>& Vertex, f32 radius, s32 rings, s32 sectors )
{
    const f32 R = 1/(f32)(rings-1);
    const f32 S = 1/(f32)(sectors-1);
 
    Vertex.New( rings * sectors );
    pvert* pV = &Vertex[0];

    for( s32 r = 0; r < rings; r++ ) 
    for( s32 s = 0; s < sectors; s++) 
    {
        f32 const y = x_Sin( -PI/2 + PI * r * R );
        f32 const x = x_Cos( PI*2 * s * S ) * x_Sin(PI * r * R );
        f32 const z = x_Sin( PI*2 * s * S ) * x_Sin( PI * r * R );
 
        pV->m_U = 1.f - s*S;
        pV->m_V = 1.f - r*R;
 
        pV->m_X = x * radius;
        pV->m_Y = y * radius;
        pV->m_Z = z * radius;
 
        pV->m_Normal.m_X = x;
        pV->m_Normal.m_Y = y;
        pV->m_Normal.m_Z = z;

      //  pV->m_Normal += xvector3( x_frand(-0.3f, 0.3f),x_frand(-0.3f, 0.3f),x_frand(-0.3f, 0.3f) ); 
      //  pV->m_Normal.NormalizeSafe();

        pV++;
    }
 
    Index.New( rings * sectors * 6 );
    u16* pI = &Index[0];

    for( s32 r = 0; r < rings; r++) 
    for( s32 s = 0; s < sectors; s++) 
    {
        *pI++ = r * sectors + s;
        *pI++ = r * sectors + (s+1);
        *pI++ = (r+1) * sectors + (s+1);

        *pI++ = r * sectors + s;
        *pI++ = (r+1) * sectors + (s+1);
        *pI++ = (r+1) * sectors + s;
    }
}

void PhysicalBasedShader( f32 DeltaTime )
{
    static xbool                bInit = FALSE;
    static eng_simple_shader    Shader;
    static f32                  Angle=0;;

    if( bInit == FALSE )
    {
        bInit = TRUE;
        static char pFShader[] = R"(
            #version 130

            in vec3 normal;  
            in vec3 eye;       // from vertex to eye/origin
            in vec3 light;     // from vertex to lightsource
            in vec2 Texcoord;
            in vec2 EnvTexcoord;
            uniform sampler2D   uAlbedoMap;                  // [UID:100, INFORMED_TEXTURE_RGB]
            uniform sampler2D   uEnvMap;                  // [UID:100, INFORMED_TEXTURE_RGB]

            uniform float uRoughness;
            uniform float uSpecular;
            uniform float uMetalic;

            const float ppp = 0.8 ;
            const vec3 LIGHT_COLOR = vec3(ppp);//vec3( 1.0 * ppp, 0.7* ppp, 0.4* ppp );
            const vec3 AMBIENT_LIGHT = vec3( 0.2, 0.2, 0.2 );

            // created textures have to have realistic values
            // Fresh asphalt  0.04
            // Worn asphalt   0.12
            // Bare soil      0.17
            // Green grass    0.25
            // Desert sand    0.40
            // New concrete   0.55
            // Fresh snow     0.80�0.90
            // const vec3 directDiffuse = vec3( 0.5 );       //normal diffuse color, from a texture i.e.

            // the following values should be used:
            // No value under 0.02
            //   gemstones 0.05-0.17
            //   liquids 0.02-0.04
            //   Skin      0.028
            // when no idea set value of 0.04 (around plastic)
            // for metals:
            //   Silver      0.971519    0.959915    0.915324
            //   Aluminium   0.913183    0.921494    0.924524
            //   Gold        1           0.765557    0.336057
            //   Copper      0.955008    0.637427    0.538163
            // const vec3 specColor = vec3( 0.0225 );

            // how shiny things should be
            const float gloss = 0.2;//0.9;

            const float SpecularPower = exp2( 10.0 * gloss + 1.0 );
            const float normFactor    = ( (SpecularPower + 2.0) / 8.0 );

            // about textures: directDiffuse, specColor or gloss can of course be read from textures or mixed 
            // with uniforms before they are used. remember when you want to use a diffuse texture that to have 
            // gamma correct colors to linearize the color after the texture read example: 
            // vec4 diff = toLinear(texture(sampler, uv))

            // Note: Alpha is always in linear space
            // textures always use gamma 2.2
            // don't need to use this if the texture is encoded as sRGB
            vec4 toLinear(vec4 x){ return vec4( pow(x.rgb, vec3(2.2)), x.a); }

            // for best result let the user specify the gamma of its monitor
            // usually from 2.0 to 2.5 a cheat for this function is assuming 
            // gamma 2.0 then just use sqrt()
            vec4 toGamma(vec4 x) { return vec4( pow(x.rgb, vec3(1.0/2.2)), x.a); } 

            float saturate(float a)
            {
                return min(1.0, max( 0.0, a ) );
            }

            #define OneOnLN2_x6 8.656170 // == 1.0/ln(2.0) * 6.   (6 is SpecularPower of 5 + 1)
            vec3 FresnelSchlick(vec3 E,vec3 H,vec4 SpecularColor)   { return SpecularColor.rgb + (1.0 - SpecularColor.rgb) * exp2(-OneOnLN2_x6 * saturate(dot(E, H))); }
            float BlinnPhong(vec3 N, vec3 H)                        { return pow(saturate(dot(N, H)), SpecularPower); }
            vec3 lightCom(vec3 N, vec3 V, vec3 L, vec3 lightColor, vec4 Albedo, vec4 SpecularColor )
            {
                vec3 H = normalize(L+V);
                float NdotL = dot(N, L);

                vec3 directSpecular = FresnelSchlick(L, H, SpecularColor) * BlinnPhong(N, H) * normFactor;
                return (Albedo.rgb + directSpecular) * lightColor * max(0.0, NdotL);
            }


            // EnvBRDF approximation
            vec3 EnvBRDFApprox( vec3 SpecularColor, float Roughness, float NoV )
            {   
                vec4 c0 = vec4(-1.0, -0.0275, -0.572,  0.22 );
                vec4 c1 = vec4( 1.0,  0.0425,  1.04,  -0.04 );
                vec4 rb = Roughness * c0 + c1;
                float a004 = min( rb.x * rb.x, exp2( -9.28 * NoV ) ) * rb.x + rb.y;
                vec2 AB = vec2( -1.04, 1.04 ) * a004 + rb.zw;
                AB.y *= saturate( 50.0 * SpecularColor.g );
                return SpecularColor * AB.x + AB.y;
            }

            // Phong Approximation
            float PhongApprox( float Roughness, float RoL )
            {
                float a         = Roughness*Roughness;
                float a2        = a*a;
                float rcp_a2    = 1./a2;  
                                                    // 0.5 / ln(2), 0.275 / ln(2)
                //vec2  c = vec2( 0.72134752, 0.25 ) * rcp_a2 + vec2( 0.39674113, 0.75 ); 
                //return c.y * exp2( c.x * RoL - c.x );
                float c = 0.72134752 * rcp_a2 + 0.39674113;
                return rcp_a2 * exp2( c * RoL - c );      
            }    

            vec3 lightCom2(vec3 N, vec3 V, vec3 L, vec3 lightColor, vec3 Albedo, float Roughness, float Specular, float Metallic )
            {
                float   NoV = max( 0.0, dot( N, V ) );
                vec3    R   = (2.*NoV) * N - V;
                float   RoL = max( 0.,dot( R, L )); 
                float   NoL = max( 0.,dot(N, L) );
                const float Shadow = 1.;

                float DielectricSpec = 0.08 * Specular;
                vec3  DiffColor      = Albedo - Albedo * Metallic;
                vec3  SpecularColor  =  ( DielectricSpec - DielectricSpec * Metallic ) + Albedo * Metallic; 
                
                vec3 EnvMap         = toLinear( textureLod( uEnvMap, EnvTexcoord, log2(Roughness*1.5+1)*9. ) ).rgb;
                EnvMap = EnvMap * EnvMap * Metallic;// * AMBIENT_LIGHT;

                Roughness = max( Roughness, 0.13 );
                SpecularColor       = EnvBRDFApprox( SpecularColor, Roughness, NoV );
                vec3 directSpecular =  SpecularColor
                                      * PhongApprox( Roughness, RoL )
                                      * pow(Roughness, 2.8 );// * 5.2;

                return DiffColor * AMBIENT_LIGHT
                                      +  
                (DiffColor + directSpecular) * lightColor * NoL * Shadow + EnvMap.xyz * SpecularColor;// 
            }

            out vec4 FragColor;
            void main()
            {   
                vec3 N = normalize(normal);
                vec3 V = normalize(eye);
                vec3 L = -normalize(light);

                vec4 AlbedoMap         = toLinear( textureLod( uAlbedoMap, Texcoord, 0 ) );
                 
                vec3 Albedo = AlbedoMap.rgb;// * 0.05 + vec3(0.0271519, 0.0159915, 0.0115324);

                vec3 colorFinal;// = Albedo*AMBIENT_LIGHT;
                colorFinal = lightCom2(N, V, L, LIGHT_COLOR, Albedo, uRoughness, uSpecular, uMetalic );
                FragColor = toGamma(vec4(colorFinal, 1.0));
            }
        )";

        static const char pVShader[] = R"(
            #version 120
            attribute vec3  aPosition;              // [POSITION]
            attribute vec3  aNormal;                // [NORMAL]
            attribute vec2  aTextureCoord00;        // [00_FULLRANGE_UV]

            uniform mat4     L2C;                   // [UID:01, SYSTEM_L2C ]
            uniform vec3    uLightPos;              // [UID:02, SYSTEM_LIGHT_IN_L ]
            uniform vec3    uEye;                   // [UID:03, SYSTEM_LIGHT_IN_L ]

            varying vec3    light;
            varying vec3    eye;
            varying vec3    normal;  
            varying vec2    Texcoord;
            varying vec2    EnvTexcoord;

            void main()
            {
                eye         = uEye;
                light       = aPosition - uLightPos;
                normal      = aNormal;
                Texcoord    = aTextureCoord00;

                vec3 viewspacenormal = mat3( L2C ) * normal;

                viewspacenormal = normalize( viewspacenormal );
                EnvTexcoord.x = viewspacenormal.x/2.1 + 0.5;
                EnvTexcoord.y = viewspacenormal.y/-2.1 + 0.5;
                
                // transform position to projection space
                gl_Position = L2C * vec4( aPosition, 1. );
            }
        )";

        static eng_vertex_desc::attribute s_VAttr[] =
        {
            { u16(X_MEMBER_OFFSET( pvert, m_X )),       eng_vertex_desc::ATTR_SRC_F32x3,    eng_vertex_desc::ATTR_USAGE_POSITION           },
            { u16(X_MEMBER_OFFSET( pvert, m_U )),       eng_vertex_desc::ATTR_SRC_F32x2,    eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV    },
            { u16(X_MEMBER_OFFSET( pvert, m_Normal )),  eng_vertex_desc::ATTR_SRC_F32x3,    eng_vertex_desc::ATTR_USAGE_NORMAL             },
            { u16(X_MEMBER_OFFSET( pvert, m_Color )),   eng_vertex_desc::ATTR_SRC_U8x4_F,   eng_vertex_desc::ATTR_USAGE_00_RGBA            }
        };

        static eng_simple_shader::attrlnk s_VAttrLnk[] =
        {
            { xserialfile::ptr<const char>((const char*)&"aPosition"), eng_vertex_desc::ATTR_USAGE_POSITION },
            { xserialfile::ptr<const char>((const char*)&"aTexCoord"), eng_vertex_desc::ATTR_USAGE_00_FULLRANGE_UV },
            { xserialfile::ptr<const char>((const char*)&"aNormal"),   eng_vertex_desc::ATTR_USAGE_NORMAL },
            { xserialfile::ptr<const char>((const char*)&"aColor"),    eng_vertex_desc::ATTR_USAGE_00_RGBA }
        };

        Shader.m_FShader.LoadFromMemory( pFShader, x_strlen(pFShader)+1 );
        Shader.m_VShader.LoadFromMemory( pVShader, x_strlen(pVShader)+1, s_VAttrLnk, 4, FALSE );
        Shader.CompileProgram();

        Shader.m_Program.LinkRegisterToUniformVariable( Shader.m_iL2CRegister, "L2C" );
        Shader.m_Program.LinkRegisterToUniformVariable( 6, "uLightPos" );
        Shader.m_Program.LinkRegisterToUniformVariable( 7, "uEye" );

        Shader.m_Program.LinkRegisterToUniformVariable( 8, "uMetalic" );
        Shader.m_Program.LinkRegisterToUniformVariable( 9, "uRoughness" );
        Shader.m_Program.LinkRegisterToUniformVariable( 10, "uSpecular" );

        Shader.m_Program.LinkTextureRegisterWithUniform( 0, "uAlbedoMap" );
        Shader.m_Program.LinkTextureRegisterWithUniform( 1, "uEnvMap" );
        
        xptr<u16>       Index;
        xptr<pvert>     Vertex;
        SolidSphere( Index, Vertex, 100, 60, 60 );

        Shader.m_VertexBufer.CreateStaticBuffer( sizeof(pvert), Vertex.getCount(), &Vertex[0] );
        Shader.m_IndexBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, Index.getCount(), &Index[0] );
        Shader.m_VertDesc.SubmitAttributes( sizeof(pvert), 4, s_VAttr, FALSE );
    }

    eng_draw& Draw = eng_GetCurrentContext().getDraw();

    eng_view View;
    static f32 t=0;
  //  t+= DeltaTime*8000;

  //  View.setFov( PI*0.2 );
    View.LookAt( 500, xradian3(X_RADIAN(-45),X_RADIAN(t), 0 ), xvector3( 0, 0, 0 ) );
    eng_GetCurrentContext().SetActiveView( View );

    //
    // Render the shader
    //
    xmatrix4 L2W;
    L2W.Identity();
    
    Shader.m_Program.Activate();

    Angle += DeltaTime*1000;

    eng_texture* pMyTexture = (eng_texture*)s_TextRef.getAsset();
    eng_texture* pEnvTexture = (eng_texture*)s_EnvTextRef.getAsset();
    
    if( !(pMyTexture && pEnvTexture) )
        return;

    pEnvTexture->setMinMagFilterMode( eng_texture::FILTER_MINMAG_LINEAR_MIPMAP_LINEAR, eng_texture::FILTER_MINMAG_LINEAR );

    const xvector3 Light ( View.getPosition().Rotate( xradian3( 0, Angle, Angle ) ) ); 

    L2W.Scale( 0.3f );
    const s32 Count = 8;
    for( s32 i=0; i<Count; i++ )
    {
        {
            f32 x = f32(0 + 60*i - 180);
            f32 y = 50 - 60;
            f32 z = 60;

            L2W.setTranslation( xvector3( x, y, z ) );

            Shader.m_Program.Activate();
            Shader.m_Program.setUniformVariable( 8, 0.0f );   //metalic
            Shader.m_Program.setUniformVariable( 9, (i+1)/f32(Count) );      // roughtness
            Shader.m_Program.setUniformVariable( 10, 0.5f );     // specular
        
            pMyTexture ->Activate(0);
            pEnvTexture ->Activate(1);
            Shader.m_Program.setUniformVariable( 6, Light );
            Shader.m_Program.setUniformVariable( 7, View.getPosition() );


            Shader.Render( L2W );
        
        }

        {
            f32 x = f32(0 + 60*i - 180);
            f32 y = 50;
            f32 z = 0;

            L2W.setTranslation( xvector3( x, y, z ) );

            Shader.m_Program.Activate();
            Shader.m_Program.setUniformVariable( 8, 1.f );   //metalic
            Shader.m_Program.setUniformVariable( 9, (i+1)/f32(Count) );      // roughtness
            Shader.m_Program.setUniformVariable( 10, 0.5f );     // specular
        
            pMyTexture ->Activate(0);
            pEnvTexture ->Activate(1);
            Shader.m_Program.setUniformVariable( 6, Light );
            Shader.m_Program.setUniformVariable( 7, View.getPosition() );


            Shader.Render( L2W );
        
        }
    }
}

//-------------------------------------------------------------------------------

void RenderStaticMesh( f32 DeltaTime )
{
    if( NULL == s_Geom.Resolve() )
        return;

    eng_view View;
    static f32 t=0;
    t+= DeltaTime*80000;

    View.LookAt( 130, xradian3(X_RADIAN(-15),X_RADIAN(t), 0 ), xvector3( 0, 35, 0 ) );
    eng_GetCurrentContext().SetActiveView( View );

    s_Geom.RenderGeom( View.getW2C(), View.getPosition().Normalize() );
}


//-------------------------------------------------------------------------------

void Render( void )
{
    static f64 RTSec =  0;
    static s32 Case  = -1;
    static f64 LastTime = x_GetTimeSec();

    // Skip to next test
    if( s_Keyboard.wasPressed( eng_keyboard::KEY_SPACE ) )
    {
        RTSec = 0;
    }
    
    if( x_GetTimeSec() > RTSec )
    {
        Case++;
        RTSec = x_GetTimeSec() + 10;
        eng_GetCurrentContext().SetClearColor( xcolor( 72, 98, 123, 0xff ));
    }
    
    // Precision is not an issue
    f32 DeltaTime( f32(x_GetTimeSec() - LastTime)/1000.0f );
    LastTime  = x_GetTimeSec();

    
   // Case = 9;
    
    //
    // Render
    //
    switch( Case )
    {
        case 0:
        {
            Draw_SimpleColorChange( DeltaTime );
            x_printfxy( 0, 1, "Draw Simple Color");
            break;
        }
        case 1:
        {
            Draw_TestLines( DeltaTime );
            x_printfxy( 0, 1, "Draw Regular Lines");
            break;
        }
        case 2:
        {
            Draw_TestLines_Parametric( DeltaTime );
            x_printfxy( 0, 1, "Draw Parametric Lines");
            break;
        }
        case 3:
        {
            Draw_TestMixture_Parametric( DeltaTime );
            x_printfxy( 0, 1, "Draw Parametric lines and triangles");
            break;
        }
        case 4:
        {
            Test3D( DeltaTime );
            x_printfxy( 0, 1, "Draw 3D");
            break;
        }
        case 5:
        {
            TestTexture( DeltaTime );
            x_printfxy( 0, 1, "Draw 3D with texture");
            break;
        }
        case 6:
        {
            TestRenderToTexture( DeltaTime );
            x_printfxy( 0, 1, "Draw render to texture");
            break;
        }
        case 7:
        {
            InputTest( DeltaTime );
            x_printfxy( 0, 1, "Input Test");
            break;
        }
        case 8:
        {
            TestSprite( DeltaTime );
            x_printfxy( 0, 1, "Test Sprite");
            break;
        }
        case 9:
        {
            TestFont( DeltaTime );
            x_printfxy( 0, 1, "Test Font effects");
            break;
        }
        case 10:
        {
            TestFont( DeltaTime );
            x_printfxy( 0, 1, "Test text" );
            break;
        }
        case 11:
        {
            RenderCustomShader( DeltaTime );
            x_printfxy( 0, 1, "Render Custom Shader" );
            break;
        }
        case 12:
        {
         //   PhysicalBasedShader( DeltaTime );
            x_printfxy( 0, 1, "Physical Based Shader" );
        //    break;
        }
        case 13:
        {
            RenderStaticMesh( DeltaTime );
            x_printfxy( 0, 1, "Static Mesh" );
            break;
        }
        default:
        {
            Case = 0;
            RTSec = 0;
        }
    };
    
#ifdef X_DEBUG
    x_printfxy( 0, 2, "DEBUG!");
#endif
    
}


//-------------------------------------------------------------------------------

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;


void AppMain( s32 argc, char* argv[] )
{
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init();
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    eng_context& DisplayContext = eng_CreateContext();
    
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();

    eng_SetCurrentContext(DisplayContext);

    //
    // Initialize resources
    //
    s_TextureFromBuffer.CreateTexture( 256, 256 );
    s_RenderToBuffer.CreateRenderBuffer( s_TextureFromBuffer );
    

    //
    // Load test texture
    //
    {
        const static u64 MY_TEXTURE = X_GUID( 2xx2_4is5_ay37 );
        s_TextRef.setup( MY_TEXTURE );
        s_EnvTextRef.setup( X_GUID(2knf_3ug7_wie7) );
        s_EnvTextRef.getAsset();

        s_NormalMapDetailRef.setup( X_GUID( 2o3f_22cf_c6qn ) );
        s_NormalMapDetailRef.getAsset();
    }
    
    //
    // Setup a sprite
    //
    {
        const static u64 SPRITE_ATLAS       = X_GUID( 1ui1_jzcw_tukx );
        const static u64 DEAMONKING_ATLAS   = X_GUID( 1gjb_d1ne_tpgj );
        
        s_DeamonKingAtlast.setup( DEAMONKING_ATLAS );
        s_AnimatedSprite[0].setup( s_DeamonKingAtlast, "ani_xemB_move000" );
        s_AnimatedSprite[1].setup( s_DeamonKingAtlast, "ani_xemB_attack000" );
        s_AnimatedSprite[2].setup( s_DeamonKingAtlast, "ani_xemA_idle000" );
        s_AnimatedSprite[3].setup( s_DeamonKingAtlast, "ani_xemA_move000" );
        s_AnimatedSprite[4].setup( s_DeamonKingAtlast, "ani_ems_attack000" );
        s_AnimatedSprite[5].setup( s_DeamonKingAtlast, "ani_ems_idle000" );
        s_AnimatedSprite[6].setup( s_DeamonKingAtlast, "ani_mm_attack000" );
        s_AnimatedSprite[7].setup( s_DeamonKingAtlast, "ani_mm_attack2000" );
        s_AnimatedSprite[8].setup( s_DeamonKingAtlast, "ani_mm_idle000" );
        s_AnimatedSprite[9].setup( s_DeamonKingAtlast, "ani_stjr_attackA000" );

        eng_sprite_rsc::ref Atlast;
        Atlast.setup( SPRITE_ATLAS );
        Atlast.getAsset();

        s32 i=1;
        for( auto& Entry:s_Sprite )
        {
            Entry.Init( Atlast, xfs("%03d", i++) );
        }
    }
    
    //
    // Load a font
    //
    {
        const static u64 THE_TEST_FONT = X_GUID( 40us_zb65_rhur );
        
        s_Font[0].setup( THE_TEST_FONT, "ObelixPro" );
        s_Font[1].setup( THE_TEST_FONT, "VeraMoBd" );

        s_Font[ 0 ].Resolve();
        s_Font[ 1 ].Resolve();

        //s_Font[ 0 ].setup( X_GUID( 3ds2_xb7l_gkdb ), "OpenSans-Regular" );
        //s_Font[ 1 ].setup( X_GUID( 3ds2_xb7l_gkdb ), "OpenSans-Regular" );

    }

    //
    // Setup the static mesh
    //
    {
        const eng_geom_rsc::ref GeomRef( X_GUID( 4hvx_3faz_zk7z ) );
        s_Geom.setup( GeomRef );
        s_Geom.Resolve();
    }

    //
    // Create our sprite group
    //
    s_SpriteGroup.Init( 100, 1, 0, 100, TRUE );

    // Now we will sleep for a while then exit
    while( DisplayContext.HandleEvents() )
    {
        eng_SetCurrentContext(DisplayContext);

        // Give the scheduler a chance to run main thread jobs
        g_Scheduler.MainThreadRunJobs( TRUE, TRUE, FALSE );
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();

        DisplayContext.PageFlip(TRUE);
    }
}