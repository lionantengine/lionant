#include "eng_base.h"
#include "anim_base.h"
#include "RawGeom.h"

static eng_render_buffer            s_RenderToBuffer;
static eng_texture                  s_TextureFromBuffer;

static eng_keyboard                 s_Keyboard;
static eng_mouse                    s_Mouse;
static eng_touchpad                 s_TouchPad;

static rawanim                      s_RawAnim;
static rawgeom                      s_RawGeom;
static eng_geom                     s_Geom;

static eng_texture                  s_DefaultTexture;

static f32                          s_LookDistance = 600.f;
static xradian3                     s_LookAngles( 0, 0, 0 );
static xvector3                     s_LookAtTo (0, 150, 0 );

static eng_anim_track_playaback     s_AnimTrack;
//static eng_anim_group_player        s_AnimPlayer;
static anim_track_mixer             s_AnimMixer;
static anim_char_player             s_AnimCharPlayer;

struct graph
{
public:

    void push( f32 Value )
    {
        if( m_Data.getCount() == m_nData )
            x_memmove( &m_Data[0], &m_Data[1], sizeof(f32)*(--m_nData) );
    
        const  f32 Range = x_Abs( m_Max ) + x_Abs( m_Min );
        m_Data[m_nData++] = Value / Range;

        xbool bRenormalize = FALSE;
        f32 Min = m_Min;
        f32 Max = m_Max;
        if( Value < Min ) 
        {
            Min =  Value - 0.005f;
            bRenormalize = TRUE;
        }
        if ( Value > Max )
        {
            Max = Value + 0.005f;
            bRenormalize = TRUE;
        }

        if( bRenormalize )
        {
            const f32 NewRange = x_Abs(Max) + x_Abs(Min);

            if( m_nData == 1 )
            {
                if(Max == Min)
                {
                    m_Data[0] = 0;
                }
                else
                {
                    m_Data[ 0 ] = Value/NewRange;
                }

                ASSERT( m_Data[0] >= -1 );
                ASSERT( m_Data[0] <= 1  );
            }
            else for( s32 i=0; i<m_nData; i++ )
            {
                 f32 v;
                if ( NewRange<0.001f )
                {
                    m_Data[ i ] = 0;
                }
                else
                {
                    v = m_Data[ i ] * Range;
                    m_Data[ i ] = v / NewRange;
                }
                ASSERT( m_Data[ i ] >= -1 );
                ASSERT( m_Data[ i ] <= 1 );
            }

            m_Max = Max;
            m_Min = Min;
        }
    }

    void renderNormal( xcolor C )
    {
        eng_context&    Context = eng_GetCurrentContext();
        eng_draw&       Draw = Context.getDraw();
        s32 XRes, YRes;
        
        Context.GetScreenResolution( XRes, YRes );

        Draw.Begin( ENG_DRAW_MODE_2D_LT );

        f32 XScale  = f32( XRes / (f32)m_nData ); 
        f32 YScale  = 50;
        f32 YOffset = 700;

        draw_vertex Ver[2];
        
        f32 DY = m_Data[ 0 ];
        Ver[ 1 ].setup( 0* XScale, YOffset + m_Data[ 0 ] * YScale, 0, C );
        for( s32 i=1; i<m_nData; i++)
        {
            Ver[i&1].setup( i* XScale, YOffset + (m_Data[i] - DY) * YScale, 0, C );
            
            DY = m_Data[i]; 
            
            Draw.DrawLine( Ver[0], Ver[1] );
        }

        Ver[ 0 ].setup( 0, YOffset + 0 * YScale, 0, xcolor( 255,0,0,255 ) );
        Ver[ 1 ].setup( f32(XRes), YOffset + 0 * YScale, 0, xcolor( 255, 0, 0, 255 ) );
        Draw.DrawLine( Ver[ 0 ], Ver[ 1 ] );

        Draw.End();
    }

    void renderDerivative( xcolor C )
    {
        eng_context&    Context = eng_GetCurrentContext();
        eng_draw&       Draw = Context.getDraw();
        s32 XRes, YRes;

        eng_scrach_memory::xscope_stack Stack( Context.getScrachMem() );

        f32* pData = Stack.Push<f32>( m_Data.getCount() );

        f32 Min = 1000000, Max = -Min;
        {
            const  f32 Range = x_Abs( m_Max ) + x_Abs( m_Min );
            for ( s32 i = 0; i < m_nData - 1; i++ )
            {
                pData[ i ] = (m_Data[ i ] - m_Data[ i + 1 ])*Range;
                if ( pData[ i ] < Min ) Min = pData[ i ];
                if ( pData[ i ] > Max ) Max = pData[ i ];
            }
        }


        Context.GetScreenResolution( XRes, YRes );

        Draw.Begin( ENG_DRAW_MODE_2D_LT );

        f32 XScale = f32( XRes / (f32)m_nData );
        f32 Range = x_Abs( Max ) + x_Abs( Min );
        f32 YScale = 100 / Range;
        f32 YOffset = 700;

        draw_vertex Ver[ 2 ];

        Ver[ 1 ].setup( 0 * XScale, YOffset + pData[ 0 ] * YScale, 0, C );
        for ( s32 i = 1; i < m_nData-1; i++ )
        {
            Ver[ i & 1 ].setup( i* XScale, YOffset + pData[ i ] * YScale, 0, C );
            Draw.DrawLine( Ver[ 0 ], Ver[ 1 ] );
        }

        Ver[ 0 ].setup( 0, YOffset + 0 * YScale, 0, xcolor( 255, 0, 0, 255 ) );
        Ver[ 1 ].setup( f32( XRes ), YOffset + 0 * YScale, 0, xcolor( 255, 0, 0, 255 ) );
        Draw.DrawLine( Ver[ 0 ], Ver[ 1 ] );

        Draw.End();
    }

    f32 getCurrMax( void )
    {
        const f32 Range = x_Abs(m_Max) + x_Abs(m_Min);
        f32 Max = m_Min;
        for ( s32 i = 0; i < m_nData; i++ )
        {
            auto v = m_Data[i] * Range;
            if( v > Max ) Max = v;
        }
        return Max;
    }

    f32 getCurrMin( void )
    {
        const f32 Range = x_Abs( m_Max ) + x_Abs( m_Min );
        f32 Min = m_Max;
        for ( s32 i = 0; i < m_nData; i++ )
        {
            auto v = m_Data[ i ] * Range;
            if ( v < Min ) Min = v;
        }
        return Min;
    }

public:

    f32                              m_Min          =  1000000;
    f32                              m_Max          = -1000000;
    s32                              m_nData        = 0;
    xsafe_array<f32,500>             m_Data;
};


static graph             s_TransX;
static graph             s_TransY;
static graph             s_TransZ;


//-------------------------------------------------------------------------------
static f32   s_Ambient = 0.3f;
static f32   s_LightI  = 0.8f;

void draw_RenderSolidMesh( f32 DeltaTime,  xbool bUseDraw )
{
//    s32 iLastTex = -1;

    static f32  Ry = DEG_TO_RAD(180);
    xmatrix4    L2W;

  //  Ry += DeltaTime * 1000;

    L2W.Identity( );
    L2W.RotateY( Ry );

    eng_context&    Context = eng_GetCurrentContext();
    eng_draw&       Draw = Context.getDraw();
    const eng_view& View = Context.GetActiveView( );

    if ( bUseDraw == FALSE )
    {
        x_printfxy( 0, 1, "Render Native");
        s_Geom.RenderGeom( L2W, xmatrix4(L2W).InvertSRT().Transform3D( View.getPosition() ).NormalizeSafe( ) );
    }
    else 
    {
        x_printfxy( 0, 1, "Render with Draw");
        Draw.Begin( ENG_DRAW_MODE_TEXTURE_ON );

        Draw.SetTexture( s_DefaultTexture  );
        Draw.SetL2W( L2W );

        const xvector3 LighDir = xmatrix4(L2W).InvertSRT().Transform3D( View.getPosition() ).NormalizeSafe( );

        for ( const rawgeom::facet& Facet : s_RawGeom.m_Facet )
        {
            draw_vertex DrawV[ 3 ];

            for ( s32 j = 0; j < 3; j++ )
            {
                const rawgeom::vertex&  Vertex = s_RawGeom.m_Vertex[ Facet.m_iVertex[ j ] ];
                draw_vertex&            V = DrawV[ j ];

                f32 I = s_Ambient + x_Max( 0, LighDir.Dot( Vertex.m_BTN[ 0 ].m_Normal )) * s_LightI;
                xcolor C( ~0 );

                C.SetFromRGBA( I, I, I, 1 );

                V.setup(
                    Vertex.m_Position.m_X,
                    Vertex.m_Position.m_Y,
                    Vertex.m_Position.m_Z,
                    Vertex.m_UV[ 0 ].m_X,
                    Vertex.m_UV[ 0 ].m_Y,
                    C );
            }

            Draw.DrawTriangle( DrawV[ 0 ], DrawV[ 1 ], DrawV[ 2 ] );
        }

        Draw.End();
    }

    //
    // Set the world axis
    //
    {
        Draw.Begin();

        Draw.SetL2W( L2W );
        //Draw.ClearL2W( );
        Draw.DrawLine(
            draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 0, 0, 255, 255 ) ),
            draw_vertex( xvector3d( 0, 0, 1000 ), xcolor( 0, 0, 255, 255 ) ) );

        Draw.DrawLine(
            draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 0, 255, 0, 255 ) ),
            draw_vertex( xvector3d( 0, 1000, 0 ), xcolor( 0, 255, 0, 255 ) ) );

        Draw.DrawLine(
            draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 255, 0, 0, 255 ) ),
            draw_vertex( xvector3d( 1000, 0, 0 ), xcolor( 255, 0, 0, 255 ) ) );

        Draw.End();
    }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
static xbool s_IsPlaying                = TRUE;
static xbool s_bPlayInPlace             = FALSE;
static xbool s_bRenderToBind            = FALSE;
static xbool s_bRenderToBind_T_Pouse    = TRUE;
static xbool s_bRenderToBind_Spread     = FALSE;
static xbool s_bRenderSkeleton          = FALSE;
static xbool s_bRenderSkeletonLabels    = FALSE;
static xbool s_RenderMesh               = TRUE;
static xbool s_RenderWeights            = FALSE;
static f32   s_FrameTime                = 0;
static xbool s_bDraw                    = FALSE;

//-----------------------------------------------------------------------------------------------

void draw_RenderSkinMesh( void )
{
    eng_context& Context = eng_GetCurrentContext();
    eng_draw&    Draw    = Context.getDraw();
    auto&        View    = Context.GetActiveView();

    xmatrix4        L2W;
    L2W.Identity();

    const s32        nBones   = s_RawAnim.m_Bone.getCount();
    xmatrix4*  const pMatrix  = Context.getScrachMem( ).BufferAlloc<xmatrix4>( s_RawAnim.m_Bone.getCount() );
    xvector3d* const pBonePos = Context.getScrachMem( ).BufferAlloc<xvector3d>( s_RawAnim.m_Bone.getCount() );

    s_RawAnim.ComputeBonesL2W( pMatrix, s_FrameTime );

    // Compute final matrices
#ifdef X_DEBUG
    for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
    {
        pMatrix[i].SanityCheck();
    }
#endif

    if( s_bPlayInPlace )
    {
        xvector3 Pos = pMatrix[0].getTranslation();
        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            pMatrix[i].setTranslation( pMatrix[i].getTranslation() - Pos );
        }
    }   

    // Renders the mesh to the bind position this is useful for debugging
    if( s_bRenderToBind )
    {
        f32 MaxDistance = 100;
        f32 Step        = (MaxDistance*2)/10.f;
        f32 xDistance = -MaxDistance;
        f32 yDistance = -MaxDistance;

        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            auto& Bone = s_RawAnim.m_Bone[i];
            if( s_bRenderToBind_T_Pouse )       pMatrix[i].Identity();
            else if( s_bRenderToBind_Spread )
            {
                pMatrix[i].Identity();
                pMatrix[i].setTranslation( xvector3(xDistance, yDistance, 0) );

                xDistance += Step;
                if( xDistance > MaxDistance ) 
                {
                    xDistance = -MaxDistance;
                    yDistance += Step;
                }
            }
            else                                pMatrix[i]  = Bone.m_BindMatrixInv;
        }
    }

    // Compute final bone pos
    for ( s32 i = 0; i < s_RawAnim.m_Bone.getCount(); i++ )
    {
        pBonePos[ i ] = pMatrix[ i ] * s_RawAnim.m_Bone[ i ].m_BindTranslation;
    }

    // Draw the weights
    if( s_RenderWeights )
    {
        Draw.Begin();
        for( s32 i=0; i<s_RawGeom.m_Facet.getCount(); i++ )
        {
            const rawgeom::facet&   Facet = s_RawGeom.m_Facet[i];

            for ( s32 j = 0; j < 3; j++ )
            {
                const rawgeom::vertex&      Vert = s_RawGeom.m_Vertex[ Facet.m_iVertex[ j ] ];
                xvector3                    P;
                xsafe_array<xvector3,16>    To;

                P.Zero();
                for( s32 w=0; w<Vert.m_nWeights; w++ )
                {
                    const rawgeom::weight& W        = Vert.m_Weight[w];
                    const rawgeom::bone&   Bone     = s_RawGeom.m_Bone[ W.m_iBone ];
                    const auto&            BonePos  = pBonePos[ W.m_iBone ];

                    P           += (pMatrix[ W.m_iBone ] * Vert.m_Position) * W.m_Weight;

                    if ( Bone.m_iParent != -1 )
                    {
                        To[w] = ( BonePos + pBonePos[ Bone.m_iParent ] ) / 2.0;
                    }
                    else
                    {
                        To[w] = BonePos;
                    }
                }
                
                for ( s32 w = 0; w < Vert.m_nWeights; w++ )
                {
                    const rawgeom::weight&  W       = Vert.m_Weight[ w ];
                    const u8                Value = u8( 0xff * W.m_Weight );

                    Draw.DrawLine( draw_vertex( P, xcolor( 0, 0, Value, 0xff ) ), draw_vertex( To[w], xcolor( Value, 0, Value, 0xff ) ) );
                }
            }
        }
        Draw.End();
    }

    // Renders the skeleton handy for debugging
    if( s_bRenderSkeletonLabels )
    {
        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            // TODO:
            // draw_Label( m_BonePos[i], XCOLOR_WHITE, m_Anim[nMesh].m_pBone[i].Name );
        }
    }

    if( s_RenderMesh )
    {
        Draw.Begin( ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_3D | ENG_DRAW_MODE_RASTER_CULL_NONE );
        Draw.SetL2W( L2W );
        Draw.SetTexture( s_DefaultTexture );
    
        xmatrix4            W2V         = View.getW2V( );
        xvector3d* const    pLightDir   = Context.getScrachMem( ).BufferAlloc<xvector3d>( nBones );
        for ( s32 i=0;i<nBones;i++)
        {
            auto& LightDir = pLightDir[i];
            LightDir = xmatrix4( W2V * pMatrix[i] ).InvertSRT( ).Transform3D( View.getWorldZVector( ) );
            LightDir.NormalizeSafe( );
        }

        // Render triangles
  //      s32 iLastTex = -1;
        for( s32 i=0; i<s_RawGeom.m_Facet.getCount(); i++ )
        {
            draw_vertex             V[3];
            const rawgeom::facet&   Facet = s_RawGeom.m_Facet[i];
            f32                     MaxWeight = 0;
            //s32                     iBone;

            // This needs to update
            {
                for( s32 j=0; j<3; j++ )
                {
                    const rawgeom::vertex&  Vert = s_RawGeom.m_Vertex[ Facet.m_iVertex[j] ];
                    xvector3                P;
                    xvector3                LightDir;

                    P.Zero();

                    V[ j ].m_U = Vert.m_UV[0].m_X*2;
                    V[ j ].m_V = Vert.m_UV[0].m_Y*2;

                    LightDir.Zero();
            
                    for( s32 w=0; w<Vert.m_nWeights; w++ )
                    {
                        const rawgeom::weight& W = Vert.m_Weight[w];

                        if( W.m_Weight > MaxWeight )
                        {
                            MaxWeight = W.m_Weight;
                            //iBone     = W.m_iBone;
                        }

                        P           += (pMatrix[ W.m_iBone ] * Vert.m_Position) * W.m_Weight;
                        LightDir    += pLightDir[ W.m_iBone ] * W.m_Weight;
                    }

                    V[j].m_X  = P.m_X;
                    V[j].m_Y  = P.m_Y;
                    V[j].m_Z  = P.m_Z;

                    /*
                        rawgeom::vertex&  Vertex = s_RawGeom.m_Vertex[ Facet.m_iVertex[j] ];
                    V[j].setup(
                        Vertex.m_Position.m_X,
                        Vertex.m_Position.m_Y,
                        Vertex.m_Position.m_Z,
                        Vertex.m_UV[ 0 ].m_X,
                        Vertex.m_UV[ 0 ].m_Y,
                        xcolor(~0) );
                        */ 


                    LightDir.Normalize();
                    f32 I     = x_Max( 0, LightDir.Dot( Vert.m_BTN[0].m_Normal ) ) * s_LightI;

                    I += s_Ambient;
                    V[j].m_Color.SetFromRGBA( I, I, I, 1 );
                }        
            }

            // Render the triangle
            Draw.DrawTriangle( V[0], V[1], V[2] );
        }

        Draw.End( );
    }

    // Renders the skeleton handy for debugging
    if ( s_bRenderSkeleton )
    {
        if( s_bRenderSkeleton == 1)
        {
            for ( s32 i = 0; i < s_RawAnim.m_Bone.getCount(); i++ )
            {
                Draw.DrawMarker( pBonePos[ i ], xcolor( 0xff, 0xff, 0, 0xff ) );
                if ( s_RawAnim.m_Bone[ i ].m_iParent != -1 )
                {
                    Draw.Begin(ENG_DRAW_MODE_ZBUFFER_OFF);
                    Draw.DrawLine( draw_vertex( pBonePos[ i ], xcolor( 0xff, 0xff, 0, 0xff ) ), draw_vertex( pBonePos[ s_RawAnim.m_Bone[ i ].m_iParent ], xcolor( 0xff, 0xff, 0, 0xff ) ) );
                    Draw.End();
                }
            }
        }

        //
        // Set the world axis
        //
        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            Draw.Begin( ENG_DRAW_MODE_ZBUFFER_OFF );

            {
                auto& Bone = s_RawAnim.m_Bone[ i ];
                xmatrix4 I( Bone.m_BindScale, Bone.m_BindRotation, Bone.m_BindTranslation );
                I = pMatrix[ i ] * I;
                Draw.SetL2W( I );

            }

            Draw.DrawLine(
                draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 0, 0, 255, 255 ) ),
                draw_vertex( xvector3d( 0, 0, 10 ), xcolor( 0, 0, 255, 255 ) ) );

            Draw.DrawLine(
                draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 0, 255, 0, 255 ) ),
                draw_vertex( xvector3d( 0, 10, 0 ), xcolor( 0, 255, 0, 255 ) ) );

            Draw.DrawLine(
                draw_vertex( xvector3d( 0, 0, 0 ), xcolor( 255, 0, 0, 255 ) ),
                draw_vertex( xvector3d( 10, 0, 0 ), xcolor( 255, 0, 0, 255 ) ) );

            Draw.End();
        }
    }
}

//-----------------------------------------------------------------------------------------------

void native_RenderSkinMesh( void )
{
    eng_context& Context = eng_GetCurrentContext();
    eng_draw&    Draw    = Context.getDraw();

    xmatrix4     L2W;

    L2W.Identity();

    // Allocate all the matrices
    const s32        nBones   = s_RawAnim.m_Bone.getCount();

    // Get the matrices from the player    
    xmatrix4* pMatrix = NULL;
    
    if( s_AnimCharPlayer.Resolve() && s_Geom.Resolve() )
    {
        pMatrix = s_AnimCharPlayer.getMatrices();
        ASSERT( x_IsAlign( pMatrix, 16) );
    }
    else
    {
        return;
    }

    if( pMatrix == NULL )
        return;

    // Sanity check the matrices
#ifdef X_DEBUG
    for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
    {
        pMatrix[i].SanityCheck();
    }
#endif

    if( s_bPlayInPlace )
    {
        xvector3 Pos = pMatrix[0].getTranslation();
        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            pMatrix[i].setTranslation( pMatrix[i].getTranslation() - Pos );
        }
    }   

    // Renders the mesh to the bind position this is useful for debugging
    if( s_bRenderToBind )
    {
        for( s32 i=0; i<s_RawAnim.m_Bone.getCount(); i++ )
        {
            if( s_bRenderToBind_T_Pouse )   pMatrix[i].Identity();
            else                            pMatrix[i] = s_RawAnim.m_Bone[i].m_BindMatrixInv;
        }
    }

    // readers the skeleton handy for debugging
    if( s_bRenderSkeleton )
    {
        auto* pAnimGroup = s_AnimTrack.getAnimGroupRef().getAsset();
        if( pAnimGroup == NULL ) return;

        auto& Skeleton = *pAnimGroup->getSkeletonRef().getAsset();

        // Render the bind pose using the animation as the anim frames contain the anim pose already
        for( s32 i=0; i<Skeleton.m_nBones; i++ )
        {
            auto& Bone          = Skeleton.m_pBone.m_Ptr[i];
                
            xvector3 Pos = pMatrix[i] * -Bone.m_BindMatrixInv.getTranslation();
            Draw.DrawMarker(Pos, xcolor(0xff,0,0,0xff) );
                
                
            if( Bone.m_iParent != -1 )
            {
                auto&    ParentBone  = Skeleton.m_pBone.m_Ptr[ Bone.m_iParent ];
                xvector3 ParentPos   = pMatrix[Bone.m_iParent] * -ParentBone.m_BindMatrixInv.getTranslation();
                Draw.Begin( );
                Draw.DrawLine( draw_vertex( Pos, xcolor(0xff,0,0,0xff) ), 
                                draw_vertex( ParentPos, 
                                xcolor(0xff,0,0,0xff) ) );
                Draw.End( );
            }
        }

        // render skeleton directly using the bind pose
        if(0)
        {
            auto& Skeleton = *pAnimGroup->getSkeletonRef().getAsset();
            for ( s32 i = 0; i < Skeleton.m_nBones; i++ )
            {
                auto& Bone = Skeleton.m_pBone.m_Ptr[ i ];
                Draw.DrawMarker( Bone.m_BindTranslation, xcolor( 0xff, 0, 0, 0xff ) );
                if ( Bone.m_iParent != -1 )
                {
                    Draw.Begin();
                    Draw.DrawLine( draw_vertex( Bone.m_BindTranslation, xcolor( 0xff, 0, 0, 0xff ) ),
                                   draw_vertex( Skeleton.m_pBone.m_Ptr[ Bone.m_iParent ].m_BindTranslation,
                                   xcolor( 0xff, 0, 0, 0xff ) ) );
                    Draw.End();
                }
            }
        }
        
    }

    eng_render_state RenderState;

    {
        x_printfxy( 0, 1, "Render Native");
        const eng_view& View    = Context.GetActiveView();
        xmatrix4 W2C = View.getW2C( );

        xmatrix4 Scale;
        Scale.Identity();
        Scale.setScale( 1.0f );

        for ( s32 i=0;i<nBones;i++)
        {
            pMatrix[i].SanityCheck();
            pMatrix[i] = W2C * Scale * pMatrix[i];
        }

        xvector3d* const pLightDir = Context.getScrachMem( ).BufferAlloc<xvector3d>( nBones );
        for ( s32 i=0;i<nBones;i++)
        {
            auto& LightDir = pLightDir[i];
            LightDir = xmatrix4( pMatrix[i] ).InvertSRT( ).Transform3D( View.getWorldZVector( ) );
            LightDir.NormalizeSafe( );
        }

        RenderState.Activate();
        s_Geom.RenderGeom( pMatrix, pLightDir, nBones );
    }
}

//--------------------------------------------------------------------------------

void RenderGrid( void )
{
    static xbool                bInit = FALSE;
    static eng_simple_shader    Shader;

    if ( bInit == FALSE )
    {
        bInit = TRUE;
        static char pFShader[] = R"(
            #ifdef GL_ES
            precision mediump float; // precision highp float;
            #endif

            varying vec4        Color;
            varying vec2        Texcoord;

            void main()
            {
                const float GlobalScale         = 1.;
                const vec2  pitch               = vec2( 50. * GlobalScale, 50. * GlobalScale );
                const float scaleFactor         = 1000.0 * GlobalScale * GlobalScale;
                const float thickness           = 0.98;
                const float Sharpness           = 9.;
                const float precisionThickness  = thickness * 0.5;

                vec2  offset = Texcoord * scaleFactor ;
                vec2  tiles1 = pitch * thickness - mod( offset - precisionThickness, pitch );
                vec2  tiles2 = pitch * thickness * 0.2 - mod( offset - precisionThickness, pitch/5. );
                vec2  tiles3 = pitch * thickness * 0.1 - mod( offset*2.5 - precisionThickness, pitch/10. );
                vec3  r      = vec3( tiles1.x * tiles1.y * Sharpness, tiles2.x * tiles2.y * Sharpness * 8., tiles3.x * tiles3.y * Sharpness * 8. );
                vec3  width  = fwidth(r);
                vec3  alpha  = 1. - clamp(smoothstep(0.5-width, 0.5+width, r), 0.65, 0.7 );
                float falpha = alpha.y * .5 + alpha.x * 0.5 + alpha.z;
                gl_FragColor = vec4( Color.xyz * falpha, Color.a  );
            }
        )";

        Shader.m_FShader.LoadFromMemory( pFShader, x_strlen( pFShader ) + 1 );
        Shader.CompileProgram();
        Shader.CompileGeometry( eng_simple_shader::GEOM_TYPE_XZ_PLANE, xcolor( 187,187,187, 0xff ) );
    }

    eng_render_state RenderState;

    //
    // Render the shader
    //
    xmatrix4 L2W;
    L2W.Identity();
    L2W.setScale( 2000.f );

    auto Trans = s_AnimCharPlayer.getRootDetailTranslation();
    L2W.setTranslation( xvector3(x_Round( Trans.m_X, 200.f ), 0, x_Round( Trans.m_Z, 200.f )) );
    RenderState.Activate();
    Shader.Render( L2W );
}

//--------------------------------------------------------------------------------

void RenderCicle( void )
{
    static xbool                bInit = FALSE;
    static eng_simple_shader    Shader;

    if ( bInit == FALSE )
    {
        bInit = TRUE;
        static char pFShader[] = R"(
            #ifdef GL_ES
            precision mediump float; // precision highp float;
            #endif

            varying vec4        Color;
            varying vec2        Texcoord;

            void main()
            {
                // outher circle
                float dist   = distance( Texcoord, vec2(0.5, 0.5) )*0.9;
                float delta  = fwidth( dist );
                float alpha1 = 1.0 - smoothstep( 0.45 - delta, 0.45, dist );

                // Inner circle
                dist         = dist * 1.2;
                delta        = fwidth( dist );
                float alpha2 = smoothstep( 0.45 - delta, 0.45, dist );

                gl_FragColor = vec4( Color.xyz, alpha2*alpha1*Color.a );
            }
        )";

        Shader.m_FShader.LoadFromMemory( pFShader, x_strlen( pFShader ) + 1 );
        Shader.CompileProgram();
        Shader.CompileGeometry( eng_simple_shader::GEOM_TYPE_XZ_PLANE, xcolor( 255,100,100, 128 ) );
    }

    eng_render_state RenderState;
    RenderState.setup( eng_render_state::BLEND_ALPHA, 
                       eng_render_state::ZBUFFER_OFF );

    //
    // Render the shader
    //
    xmatrix4 L2W;
    L2W.Identity();
    L2W.setScale( 80.f );

    auto Trans = s_AnimCharPlayer.getRootDetailTranslation();
    Trans.m_Y = 1;
    L2W.setTranslation( Trans );//xvector3(x_Round( Trans.m_X, 200.f ), 0, x_Round( Trans.m_Z, 200.f )) );
    RenderState.Activate();
    Shader.Render( L2W );
}

//--------------------------------------------------------------------------------

void RenderArrow( void )
{
    static xbool                bInit = FALSE;
    static eng_simple_shader    Shader;

    if ( bInit == FALSE )
    {
        bInit = TRUE;
        static char pFShader[] = R"(
            #ifdef GL_ES
            precision mediump float; // precision highp float;
            #endif

            varying vec4        Color;
            varying vec2        Texcoord;

            float pin( vec2 P, float size, float thickness ) 
            {
                P        = (P - vec2(0.5,0.5))*-1.;
                float r1 = max(abs(P.x)- size/2.0, abs(P.y)- size/thickness);
                float r2 = abs(P.x-size/1.5)+abs(P.y)-size;
                return max(r1,0.75*r2)*3.5;
            }

            void main()
            {
                float dist   = pin( Texcoord, 1., 64. ); 
                float delta  = fwidth( dist );
                float alpha  = 1.-smoothstep( 0.45 - delta, 0.45, dist );

                gl_FragColor = vec4( Color.xyz*alpha, 1. );//Color.a*alpha );
            }
        )";

        Shader.m_FShader.LoadFromMemory( pFShader, x_strlen( pFShader ) + 1 );
        Shader.CompileProgram();
        Shader.CompileGeometry( eng_simple_shader::GEOM_TYPE_XZ_PLANE, xcolor( 10, 10, 64, 255 ) );
    }

    eng_render_state RenderState;
    RenderState.setup( eng_render_state::BLEND_ADD, 
                       eng_render_state::ZBUFFER_OFF );
    //
    // Render the shader
    //
    xmatrix4 L2W;
    L2W.Identity();
    L2W.setScale( 30.f );

    auto Trans  = s_AnimCharPlayer.getRootDetailTranslation();
    auto Rot    = s_AnimCharPlayer.getRootRotation();// .getRootDetailRotation();

    Rot.m_Pitch = 0;
    Rot.m_Roll  = 0;

    Trans.m_Y = 1;
    L2W.setup( xvector3d(20.f), Rot, Trans );
    L2W.PreTranslate( xvector3d(0,0,1.0f) );
    RenderState.Activate();
    Shader.Render( L2W );

    Rot             = s_AnimCharPlayer.getRootDetailRotation();
    Rot.m_Pitch     = 0;
    Rot.m_Roll      = 0;

    Trans.m_Y = 0;
    L2W.setup( xvector3d(10.f,1.f,30.f), Rot, Trans );
    L2W.PreTranslate( xvector3d(0,0,1.0f) );
    Shader.Render( L2W );

}

//-------------------------------------------------------------------------------

void Render( void )
{
    static f64      RTSec =  0;
    static f64      LastTime = x_GetTimeSec();

    static s32 iAnim=0;
    // Skip to next test
    if( s_Keyboard.wasPressed( eng_keyboard::KEY_SPACE ) )
    {
        RTSec = 0;
        s_AnimTrack.setAnimation( eng_anim_group_player::ianim(iAnim), 0.5, 1 );

 //       if( s_AnimPlayer.m_AnimGroupRef.getAsset() )
 //           iAnim = (iAnim+1)%s_AnimPlayer.m_AnimGroupRef.getAsset()->m_nTotalAnims;
    }

    if ( s_Keyboard.wasPressed( eng_keyboard::KEY_D ) )
    {
        s_bDraw = !s_bDraw;
    }

    if ( s_Keyboard.wasPressed( eng_keyboard::KEY_S ) )
    {
        s_bRenderSkeleton = (s_bRenderSkeleton+1)%3;
    }

    if( s_Keyboard.wasPressed( eng_keyboard::KEY_B ) )
    {
        s_bRenderToBind = !s_bRenderToBind;
    }
    
    xvector2 Pos = s_Mouse.getValue( eng_mouse::ANALOG_POS_REL );

    if ( s_Mouse.isPressed( eng_mouse::BTN_RIGHT ) )
    {
        if ( x_Abs( Pos.m_X ) > 1 )
        {
            s_LookAngles.m_Yaw +=( DEG_TO_RAD(-Pos.m_X) );
        }

        if ( s_Mouse.isPressed( eng_mouse::BTN_MIDDLE ) )
        {
            s_LookAtTo.m_Y += Pos.m_Y*0.1f;
            s_LookDistance = x_Max( s_LookDistance, 1 );
        }
        else if ( x_Abs( Pos.m_Y ) > 1 )
        {
            s_LookAngles.m_Pitch +=( DEG_TO_RAD(-Pos.m_Y) );
        }
    }
    else if (s_Mouse.isPressed( eng_mouse::BTN_LEFT ) )
    {
        if ( x_Abs( Pos.m_Y ) > 1 )
        {
            s_LookDistance += -Pos.m_Y;
            s_LookDistance  = x_Max( s_LookDistance, 1 );
        }
    }

    if( s_AnimTrack.getAnimGroupRef().getAsset() )
    {
        if ( s_Keyboard.isPressed( eng_keyboard::KEY_1 )  )
        {
            s_AnimTrack.setFrame(76);
        }
            
        xbool bAnyThingPressed = FALSE;
        if(  s_Keyboard.isPressed( eng_keyboard::KEY_UP ) && !bAnyThingPressed )
        {
            static auto iAnim = s_AnimTrack.getAnimationIndex( eng_anim_track_playaback::ianimhash(x_strHash("walk_foward")) );
            if ( s_AnimTrack.getAnimation().m_Value != iAnim.m_Value )
                s_AnimTrack.setAnimation( iAnim, 1.5, 1 );
            bAnyThingPressed = TRUE;
        }

        if ( s_Keyboard.isPressed( eng_keyboard::KEY_RIGHT )  && !bAnyThingPressed)
        {
            static auto iAnim = s_AnimTrack.getAnimationIndex( eng_anim_track_playaback::ianimhash(x_strHash("walk_right")) );
            if ( s_AnimTrack.getAnimation().m_Value != iAnim.m_Value )
                s_AnimTrack.setAnimation( iAnim, 1.5, 1 );
            bAnyThingPressed = TRUE;
        }

        if ( s_Keyboard.isPressed( eng_keyboard::KEY_LEFT )  && !bAnyThingPressed)
        {
            static auto iAnim = s_AnimTrack.getAnimationIndex( eng_anim_track_playaback::ianimhash(x_strHash("walk_left")) );
            if( s_AnimTrack.getAnimation().m_Value != iAnim.m_Value )
                s_AnimTrack.setAnimation( iAnim, 1.5, 1 );
            bAnyThingPressed = TRUE;
        }

        if ( !bAnyThingPressed )
        {
            static auto iAnim = s_AnimTrack.getAnimationIndex( eng_anim_track_playaback::ianimhash(x_strHash("idle")) );
            if ( s_AnimTrack.getAnimation().m_Value != iAnim.m_Value )
                s_AnimTrack.setAnimation( iAnim, 0.5, 1 );
            bAnyThingPressed = TRUE;
        }
    }

    // Precision is not an issue                 // /1000.0f
    f32 DeltaTime( f32(x_GetTimeSec() - LastTime) );
    LastTime  = x_GetTimeSec();

    //
    // Render
    //
    if( 1 )
    {
      // DeltaTime = 0.016f*0.1f;//DeltaTime*0.1f;

        // Keep both player synchronized 
        if( s_IsPlaying )
        {
//            s_AnimPlayer.BeginAdvanceTime( DeltaTime );
            s_FrameTime += DeltaTime * 60;
            if( s_AnimCharPlayer.Resolve() )
            {
                s_AnimCharPlayer.BeginAdvanceTime( DeltaTime );
            }

  //          if( s_AnimTrack.getAnimGroupRef().getAsset() )
    //            if( s_AnimTrack.getAnimation().m_Value == eng_anim_group_player::ianim::INVALID )
      //              s_AnimTrack.setAnimation( s_AnimPlayer.m_iCurrAnim, 0, 1 );
        }

        RenderGrid();
        RenderCicle();
        RenderArrow();

        if( s_bDraw )
        {
            //draw_RenderSolidMesh( DeltaTime, TRUE );
            draw_RenderSkinMesh();
         }
        else
        {
            native_RenderSkinMesh();
            s_LookAtTo = s_AnimCharPlayer.getRootTranslation();
        }
    }
    else
    {
        //draw_RenderSolidMesh( DeltaTime, bDraw );
    }


    auto Trans = s_AnimCharPlayer.getRootDetailTranslation();
    auto R     = s_AnimCharPlayer.getRootDetailRotation();

    x_printfxy( 0,10, "Translation: %4.4f %4.4f %4.4f", s_TransX.getCurrMax() - s_TransX.getCurrMin(),
                                                        s_TransY.getCurrMax() - s_TransY.getCurrMin(),
                                                        s_TransZ.getCurrMax() - s_TransZ.getCurrMin() ); 
    x_printfxy( 0, 11, "Rotations: %4.4f %4.4f %4.4f", 
        RAD_TO_DEG(R.m_Roll),
        RAD_TO_DEG(R.m_Pitch), 
        RAD_TO_DEG(R.m_Yaw)  );
     
    s_TransX.push( Trans.m_X );
    s_TransX.renderDerivative( xcolor(0xff,0xf,0,0xff) );

    s_TransY.push( Trans.m_Y );
    s_TransY.renderDerivative( xcolor( 0xf, 0xff, 0, 0xff ) );

    s_TransZ.push( Trans.m_Z );
    s_TransZ.renderDerivative( xcolor( 0xf, 0xf, 0xff, 0xff ) );
        
        
#ifdef X_DEBUG
    x_printfxy( 0, 2, "DEBUG!");
#endif
    
}

//-------------------------------------------------------------------------------

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

void AppMain( s32 argc, char* argv[] )
{
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init();
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    eng_context& DisplayContext = eng_CreateContext();
    
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.PresetSetupScrachMemSize( 1024*400 );
    DisplayContext.Init();

    eng_SetCurrentContext(DisplayContext);
    DisplayContext.SetClearColor( xcolor( 187, 187,187,255 ));

    // Now we will sleep for a while then exit
    //s_RawGeom.Load( "../../Dependency/Logs/Debug/BaseMale_ALLPLATFORMS.rawgeom" );
    //s_RawAnim.Load( "../../Dependency/Logs/Debug/baseMale_Walk_ALLPLATFORMS--9u3t_rn7y_y87z.rawanim" );
    
    
   
    {
        const static eng_geom_rsc::ref  RPG_MESH               ( X_GUID( 56ii_i84y_zsbj )  );
        const static eng_geom_rsc::ref  MURO_MESH              ( X_GUID( 4hvx_3faz_zk7z )  );
        const static eng_geom_rsc::ref  RUNINS_MESH            ( X_GUID( 4l9r_ma8e_4qan )  );
        const static eng_geom_rsc::ref  ALTAIR_MESH            ( X_GUID( 4oj1_zkou_1nof )  );
        const static eng_geom_rsc::ref  SPYDER_MESH            ( X_GUID( 4ucl_h296_ls7z )  );
        const static eng_geom_rsc::ref  MIAO_MESH              ( X_GUID( 4yi8_i4ki_ltsv )  );

        if( 1 )
        {
            const static eng_skeleton_rsc::ref      MOCAP_SKELETON          ( X_GUID( axcp_5tf5_zyu7 ) );
            const static eng_anim_group_rsc::ref    MOCAP_ANIM_GROUP        ( X_GUID( 9ty5_jn7m_dxcv ) );
            const static eng_geom_rsc::ref          MOCAP_GEOM              ( X_GUID( 5esm_abbu_k467 ) );
            s_RawAnim.Load( "../../Dependency/Logs/Debug/MvnPuppet_walk_left_ALLPLATFORMS--9ty5_jn7m_dxcv.rawanim" );
            s_RawGeom.Load( "../../Dependency/Logs/Debug/mocap_ALLPLATFORMS.rawgeom" );

            s_AnimTrack.setup( MOCAP_ANIM_GROUP );
            s_AnimMixer.setup( MOCAP_SKELETON );
            s_AnimMixer.AddTrack( s_AnimTrack, TRUE );
            s_AnimCharPlayer.setup( MOCAP_SKELETON, xvector3(0,95,0) );
            s_AnimCharPlayer.AddMixer( s_AnimMixer, TRUE );
            s_Geom.setup( MOCAP_GEOM );
            s_Geom.Resolve();
        }
        else if(1)
        {
            const static eng_geom_rsc::ref          BASEMALE_MESH           ( X_GUID( 5dt3_y1j5_zvof ) );
            const static eng_anim_group_rsc::ref    BASEMALE_ANIM_GROUP     ( X_GUID( 9u3t_rn7y_y87z ) );
            const static eng_skeleton_rsc::ref      BASEMALE_SKELETON       ( X_GUID( aq2g_6iqe_8wf3 ) );
            s_RawGeom.Load( "../../Dependency/Logs/Debug/BaseMale_ALLPLATFORMS.rawgeom" );
            s_RawAnim.Load( "../../Dependency/Logs/Debug/baseMale_crouch_ALLPLATFORMS--9u3t_rn7y_y87z.rawanim" );
        //    s_RawAnim.BakeBindingIntoFrames( TRUE, TRUE, FALSE ); 

            s_AnimMixer.setup( BASEMALE_SKELETON );
            s_AnimMixer.AddTrack( s_AnimTrack, TRUE );
//            s_AnimPlayer.setup( BASEMALE_ANIM_GROUP, 0x333 );
            s_Geom.setup( BASEMALE_MESH );

  //          s_AnimPlayer.TransitionToAnimation( eng_anim_group_player::ianimhash( x_strHash( "crouch" ) ) );
            s_Geom.Resolve();
        }

    }

    s_DefaultTexture.CreateTexture( xbitmap::getDefaultBitmap() );
    s_DefaultTexture.setWrappingMode( eng_texture::WRAP_MODE_TILE, eng_texture::WRAP_MODE_TILE );

    while( DisplayContext.HandleEvents() )
    {
        eng_SetCurrentContext(DisplayContext);

 //       static f32 x=0;

       // x += 0.01f; 
        eng_view View = DisplayContext.GetActiveView();

        //-400*x_Sin(x)
        View.LookAt( s_LookDistance, s_LookAngles, s_LookAtTo );
        DisplayContext.SetActiveView( View );

        // Give the scheduler a chance to run main thread jobs
        g_Scheduler.MainThreadRunJobs( TRUE, TRUE, FALSE );
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();

        DisplayContext.PageFlip(TRUE);
    }
}