//varying vec2 vUv;
varying float noise;
varying  float Intensity;
varying  vec3 normal;
varying  vec3 vPosition;
varying  float Alpha;
uniform sampler2D iChannel0;

void main()
{
    vec3 N = normalize(normal);
    vec3 I = normalize(vPosition);
    
    float cosTheta = abs(dot(I, N));
    float fresnel = pow(1.0 - cosTheta, 4.0);
    
	// compose the colour using the UV coordinate
	// and modulate it with the noise like ambient occlusion
    
    // Version 1
    // vec3 color   = vec3( 0.4 + (Intensity+fresnel) * ( 1. - 2. * noise  ) );

    // Version 2
    //vec3 color   = vec3( 0.9 + 0.3 * ( (Intensity + fresnel)) * ( 1. - 3. * noise ) );

    // Version 3
    //vec3 color   = vec3( 0.95 + 0.3 * ( (Intensity + fresnel)) * ( 0.5 - 2.5 * noise ) );

    // Version 4
    //vec3 color   = vec3( 1. + 0.3 * ( (Intensity + fresnel)) * ( 0.0 - 2.5 * noise ) );
    
    // Version 5
    //vec3 color   = vec3( 1. + 0.5 * ( exp( -.2*(Intensity + fresnel))) * ( 0. - 1.5 * (noise )) );

    // Version 6
    //vec3 color   = vec3( 1. + 0.5 * ( exp( -.9*(Intensity + fresnel))) * ( - 5.5 * (noise*noise )) );

    // Version 7
    //float Ambient = 0.8;
    //float light = Intensity;
   // vec3 color   = vec3( (2.7*Ambient + 3.*light * (1.-Ambient)) * exp(Ambient*0.3 + 0.2*fresnel) * ( 0.3-(noise*noise*noise) ) );
    
    // Version 8
    // float Ambient = 0.6;
    // float light = Intensity;
    // vec3 color   = vec3( (2.7*Ambient + 3.*light * (1.-Ambient)) * exp(Ambient*0.3 + 0.2*fresnel) * ( 0.3-(noise*noise*noise) ) );
    //  gl_FragColor = vec4( color.xyz, 0.8 );

    // Version 9
    float Ambient = 0.50;
    float light = Intensity;
    vec3 color   = vec3( (2.7*Ambient + 3.*light * (1.-Ambient)) * exp(Ambient*0.5 + 0.4*fresnel) * ( 0.2-(noise*noise*noise) ) );

    gl_FragColor = vec4( texture2D( iChannel0, vec2(0.50+color.r*0.5,0.5) ).xyz, 0.95);
}