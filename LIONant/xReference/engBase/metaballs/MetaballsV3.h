//
//  MetaballsV3.h
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/7/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class metaballsv3
{
public:
    
    enum max:s32
    {
        MAX_VERTICES = 60000,
        MAX_INDICES  = 60000
    };
    
    struct vertex
    {
        xvector3d   m_Pos;
        xvector3d   m_Normal;
        xvector2    m_UV;
    };
    
public:
    
                        metaballsv3                         ( void );
    virtual            ~metaballsv3                         ( void ){}
            void        Initialize                          ( s32 GridSize );
    
            void        Update                              ( f32 DeltaTime );
            void        Render                              ( vertex* pDrawVertex, s32& nVertex, u16* pDrawIndex, s32& nIndices  );
    
protected:
    
    struct ball
    {
        xvector3    m_Pos;
        xvector3    m_Vel;
        xvector3    m_AttractionPoint;
        f32         m_Time;
        f32         m_R;
    };
    
    struct ivec
    {
        s32 m_X, m_Y, m_Z;
    };
    
protected:
    
            void        Compute                             ( void );
            f32         ComputeEnergy                       ( xvector3& Point );
            void        ComputeNormal                       ( vertex* pVertex );
            f32         ComputeGridPointEnergy              ( s32 x, s32 y, s32 z );
            s32         ComputeGridVoxel                    ( s32 x, s32 y, s32 z );
    
            xbool       isGridPointComputed                 ( s32 x, s32 y, s32 z ) const;
            xbool       isGridVoxelComputed                 ( s32 x, s32 y, s32 z ) const;
            xbool       isGridVoxelInList                   ( s32 x, s32 y, s32 z ) const;
    
            void        setGridPointComputed                ( s32 x, s32 y, s32 z );
            void        setGridVoxelComputed                ( s32 x, s32 y, s32 z );
            void        setGridVoxelInList                  ( s32 x, s32 y, s32 z );
    
            f32         ConvertGridPointToWorldCoordinate   ( s32 x );
            int         ConvertWorldCoordinateToGridPoint   ( f32 x );
	
            void        AddNeighborsToList                  ( s32 nCase, s32 x, s32 y, s32 z );
            void        AddNeighbor                         ( s32 x, s32 y, s32 z );
    
public:
    
	f32                         m_EnergyBoundaryLevel;   // The level of energy which descrives the edge
    
	xsafe_array<ball,32>        m_Balls;
    
	xptr<ivec>                  m_OpenVoxels;           // queue of open voxels that need to be computed
	s32                         m_nOpenVoxels;          // Number of voxels inside the queue
    
	s32                         m_nGridSize;
	f32                         m_VoxelSize;
    
    xptr<f32>                   m_GridEnergy;           // Energy Level in the Grid
    xptr<u8>                    m_GridPoint;            // Status of the Point in the grid
    xptr<u8>                    m_GridVoxel;            // Status of the Voxel in the grid
    
    // For rendering
	s32                         m_nNumVertices;
	s32                         m_nNumIndices;
	vertex*                     m_pVertices;
	u16*                        m_pIndices;
};

