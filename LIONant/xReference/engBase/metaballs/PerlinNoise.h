//
//  PerlinNoise.h
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//



f32 PerlinNoise1D(f32,f32,f32,int);
f32 PerlinNoise2D(f32,f32,f32,f32,int);
f32 PerlinNoise3D(f32,f32,f32,f32,f32,int);
