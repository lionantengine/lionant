//
//  MetaBalls.cpp
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/7/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "x_base.h"
#include "MetaBalls.h"

//--------------------------------------------------------------------------------

metaballs::metaballs( void )
{
}

//--------------------------------------------------------------------------------

void metaballs::Init( void )
{
	setSize( xvector3(-350, -350, -350), xvector3(350, 350, 350), 40, 40, 40);
    
	for( s32 i = 0; i < m_Balls.getCount(); i++ )
    {
        metaball&  Ball = m_Balls[i];
        
        Ball.m_OneOverRadius = 1/50.0f;//1.0f / x_frand( 5.0f, 60.f );
		Ball.m_Dir.Zero();
        Ball.m_Pos.Set( x_frand( -100.0f, 100.0f ),
                        x_frand( -100.0f, 100.0f ),
                        x_frand( -100.0f, 100.0f ) );
	}
}

//--------------------------------------------------------------------------------

f32 metaballs::FieldFunction( const xvector3& Pos )
{
 	f32 Energy = -1;
        
	for( auto& Ball:m_Balls )
	{
		// The formula for the energy is
		//
		//   e += mass/distance^2
        const xvector3  v = (Ball.m_Pos - Pos) * Ball.m_OneOverRadius*xvector3(1,1.5,1);
         f32            d = v.Dot(v);
        
		if( d < 0.0001f ) d = 0.0001f;
        
		Energy += 1/d;
	}
    
	return Energy;
    
    /*
    f32     Sum  = 0.0f;
    f32     Prod = 1.0f;
    
    for( s32 i = 0; i < m_Balls.getCount(); i++ )
    {
        const metaball&   Ball = m_Balls[i];
        const xvector3    d = (Ball.m_Pos - Pos) * Ball.m_OneOverRadius;
        const f32         b = d.Dot(d);
        
        // Refactorize so that we only need a single divide in the end
        Sum     *= b;
        Sum     += Prod;
        Prod    *= b;
    }
    
    f32 Energy = Sum / Prod - 1.0f;
    ASSERT( x_IsValid( Energy ));
    
    return Energy;
     */
}

//--------------------------------------------------------------------------------

void metaballs::Update( f32 DeltaTime )
{
    //
    // Add some velocity to the balls
    //
	for( s32 i = 0; i < m_Balls.getCount(); i++)
    {
        metaball&   Ball = m_Balls[i];
        
		xvector3    d    = -Ball.m_Pos;
        
		Ball.m_Dir += d * (40000 * DeltaTime / (5000.0f + d.Dot(d) ));
        
		for( s32 j = 0; j < m_Balls.getCount(); j++ )
        {
			if( j != i && j&1 )
            {
				xvector3 d = m_Balls[j].m_Pos - Ball.m_Pos;
				Ball.m_Dir += d * (20000 * DeltaTime / (5000.0f + d.Dot(d) ));
			}
		}
	}
    
    //
    // Update their position
    //
	for( s32 i = 0; i < m_Balls.getCount(); i++ )
    {
        metaball&  Ball = m_Balls[i];
		Ball.m_Pos += Ball.m_Dir * DeltaTime;
	}

    //
    // Now compute the isosurface
    //
    isosurface::Update();
}


