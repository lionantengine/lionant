#include "eng_base.h"
#include "Metaballs.h"
#include "MetaballsV3.h"
#include "Metaballs2.h"
#include "MarchingCubes.h"
#include "xbmp_tools.h"

#include "NoiseShader.fsh.h"
#include "NoiseShader.vsh.h"
#include "NoiseShader.gsh.h"

static metaballs                    s_MetaBalls;
static CMetaballs                   s_MetaBalls2;
static xptr<SVertex>                s_VertexMetaBalls2;
static xptr<u16>                    s_IndexMetaBalls2;
static metaballsv3                  s_MetaBalls3;
static xptr<metaballsv3::vertex>    s_VertexMetaBalls3;
static xptr<u16>                    s_IndexMetaBalls3;

static eng_vshader                  s_VertexShader;
static eng_fshader                  s_FragmentShader;
static eng_gshader                  s_GeometryShader;
static eng_shader_program           s_ShaderProgram;

static eng_vertex_desc              s_VertexDesc;
static eng_vbuffer                  s_VertexBuffer;
static eng_ibuffer                  s_IndexBuffer;
static xbitarray                    s_CheckTable;

static eng_texture                  s_Texture;
static eng_texture                  s_BlueSkyTexture;

struct metaball_vertex
{
    xvector3d   m_Pos;
    xvector3d   m_Normal;
    xvector3d   m_UV;
};

// Worth Reseaching:
// http://www.markmark.net/PDFs/RTCloudsForGames_HarrisGDC2002.pdf
// http://wscg.zcu.cz/wscg2012/full/E41-full.pdf
// http://developer.download.nvidia.com/SDK/10/direct3d/Source/Fur/doc/FurShellsAndFins.pdf
// http://www.sandia.gov/~kmorel/documents/ParallelVolRen.pdf
// http://lodev.org/cgtutor/randomnoise.html -- Nice link about texture noise
// http://prideout.net/blog/?tag=opengl-glass -- nice glass and transparencies
// http://www.khayton-portfolio.com/samples_silhouette.html -- fins with code
// http://prideout.net/blog/?p=54 -- look pretty cool for fins...
// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch01.html -- cool read
//-------------------------------------------------------------------------------


void MetaBalls( u32 Mode )
{
    static f64 InitialTime = x_GetTime();
    s_MetaBalls.Update( 0.004f );
    
    eng_draw& Draw = eng_GetCurrentContext().getDraw();
    //
    // Draw the sky
    //
    if( 1 )
    {
        Draw.Begin( ENG_DRAW_MODE_2D_PARAMETRIC | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_ZBUFFER_OFF | ENG_DRAW_MODE_RASTER_CULL_NONE | ENG_DRAW_MODE_MISC_FLUSH );
        Draw.SetTexture( s_BlueSkyTexture );
        
        xrect Rect( -1, -1, 1, 1 );
        
        Draw.DrawSolidRect( Rect, xcolor(~0) );
        Draw.End();
    }
    
    //
    // Handle wire frame mode
    //
    if( Mode == ENG_DRAW_MODE_RASTER_WIRE_FRAME )
    {
 //       glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
    
    //
    // Draw something
    //
    if( 0 )
    {
        Draw.Begin( ENG_DRAW_MODE_3D | ENG_DRAW_MODE_BLEND_ALPHA | Mode | ENG_DRAW_MODE_MISC_FLUSH | ENG_DRAW_MODE_ZBUFFER_OFF );//| ENG_DRAW_MODE_RASTER_WIRE_FRAME);
        Draw.ClearL2W();
        Draw.DrawSphere(xcolor(255,0,0,255));
        Draw.End();
    }
    
    //
    // Render the actual isosurface
    //
	s32                         nVertices = s_MetaBalls.getVertexCount();
	s32                         nIndices  = s_MetaBalls.getIndexCount();
    
    // Check to see if we have data to render
    if( !nVertices || !nIndices ) return;
    
    //
    // Copy the data into the v/i buffer
    //
    metaball_vertex*            pvBuff      = (metaball_vertex*)s_VertexBuffer.LockData();
    u16*                        piBuff      = (u16*)s_IndexBuffer.LockData();
    const isosurface::vertex*   pVertex     = s_MetaBalls.getVertexArray();
    const u16*                  pIndex      = s_MetaBalls.getIndexArray();
    
    for( s32 i=0; i<nVertices; i++ )
    {
        const isosurface::vertex&   SrcV        = pVertex[i];
        metaball_vertex&            DestV       = pvBuff[i];
        
        DestV.m_Pos    = SrcV.m_Pos;
        DestV.m_Normal = SrcV.m_Normal;
        DestV.m_UV.Set( 0, 0, 0 );
    }

    // Take care first of the main cloud
    x_memcpy( piBuff, pIndex, sizeof(u16)*nIndices );

    //
    // Build the fins
    //
    /*
    // Now it is time to build the fins for our cloud
    s32                 nFinIndices = 0;
    s32                 nVertsFins  = 0;
    u16*                pIndexFin   = &piBuff[nIndices];
    metaball_vertex*    pVertexFin  = &pvBuff[nVertices];
    
    
    for( s32 i=0; i<nIndices; i += (3*5) )
    {
        const u16 Index[] = { piBuff[i+0], piBuff[i+1], piBuff[i+2], piBuff[i+0] };
        
        for( s32 j=0; j<3; j++ )
        {
            const u16           Index1 = Index[j+0];
            const u16           Index2 = Index[j+1];
            metaball_vertex&    V1     = pvBuff[Index1+nVertices];
            metaball_vertex&    V2     = pvBuff[Index2+nVertices];

            //
            // Create all the vertices that we will need
            //
            pVertexFin[nVertsFins+0] = V1;
            pVertexFin[nVertsFins+1] = V1;
            pVertexFin[nVertsFins+2] = V2;
            pVertexFin[nVertsFins+3] = V2;
            
           // pVertexFin[nVertsFins+1].m_Pos = V1.m_Normal * 5 + V1.m_Pos;
           // pVertexFin[nVertsFins+3].m_Pos = V2.m_Normal * 5 + V2.m_Pos;
            
            pVertexFin[nVertsFins+0].m_UV.Set(0,0,0);
            pVertexFin[nVertsFins+1].m_UV.Set(0,1,1);
            pVertexFin[nVertsFins+2].m_UV.Set(1,0,0);
            pVertexFin[nVertsFins+3].m_UV.Set(1,1,1);
            
            pIndexFin[nFinIndices+0] = nVertsFins+0;
            pIndexFin[nFinIndices+1] = nVertsFins+2;
            pIndexFin[nFinIndices+2] = nVertsFins+1;
            
            pIndexFin[nFinIndices+3] = nVertsFins+2;
            pIndexFin[nFinIndices+4] = nVertsFins+3;
            pIndexFin[nFinIndices+5] = nVertsFins+1;
            
            nVertsFins  += 4;
            nFinIndices += 6;
        }
    }
   */
    
/*
    // Now it is time to build the fins for our cloud
    s32                 nFinIndices = 0;
    u16*                pIndexFin   = &piBuff[nIndices];
    metaball_vertex*    pVertexFin  = &pvBuff[nVertices];

    
    for( s32 i=0; i<nIndices; i += 6 )
    {
        const u16 Index[] = { piBuff[i+0], piBuff[i+1], piBuff[i+2], piBuff[i+0], piBuff[i+4], piBuff[i+5] };
        
        for( s32 j=0; j<3; j++ )
        {
            const u16           Index1 = Index[j+0];
            const u16           Index2 = Index[j+1];
            metaball_vertex&    V1     = pvBuff[Index1+nVertices];
            metaball_vertex&    V2     = pvBuff[Index2+nVertices];
            const s32           iCheck = (Index1 < Index2)?(Index1 + Index2*nVertices):(Index2 + Index1*nVertices);
            xbool               bCreateFin = FALSE;
            
            
            // Build the first edge
            if( (V1.m_UV.m_Z + V2.m_UV.m_Z) != 2 )
            {
                bCreateFin = TRUE;
            }
            else if( !s_CheckTable.Get( iCheck ) )
            {
                //bCreateFin = TRUE;
            }
             
            
            if( bCreateFin )
            {
                // First triangle
                pIndexFin[0] = Index1;
                pIndexFin[1] = Index1+nVertices;
                pIndexFin[2] = Index2+nVertices;
                pIndexFin += 3;
                
                // First triangle
                pIndexFin[0] = Index1;
                pIndexFin[1] = Index2+nVertices;
                pIndexFin[2] = Index2;
                pIndexFin += 3;
                
                // Flag the vertices
                V1.m_UV.m_Z = V2.m_UV.m_Z = 1;
                
                // Keep count
                nFinIndices += 6;
                
                // Create a check in our table
              //  ASSERT( !s_CheckTable.Get( iCheck ) );
              //  s_CheckTable.Set( iCheck, 1 );
              //  ASSERT( s_CheckTable.Get( iCheck ) );
            }
        }
    }
    */
    s_VertexBuffer.UnlockData();
    s_IndexBuffer.UnlockData();
    
    eng_context& Context = eng_GetCurrentContext();
    
    //
    // Ok Lets Draw it!
    //
    const eng_view& View = Context.GetActiveView();
    

    // Since we always set the projection matrix set it here ones.
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf( (f32*)&View.getW2C() );
    
    // Activate everything
    s_VertexBuffer.Activate();
    s_IndexBuffer.Activate();
    s_ShaderProgram.Activate();
    
    // Set a few constants in the shader
    s_ShaderProgram.setUniformVariable(0, (f32)((x_GetTime()-InitialTime)*0.00000004) );
    
    // Make it eassy to deal with in the shader 
    xmatrix4 NormalMatrix(View.getW2V());
    NormalMatrix.Transpose();
    
    s_ShaderProgram.setUniformVariable(1, NormalMatrix );
    
    glEnable( GL_BLEND );
    //glCullFace(GL_BACK);
    glBlendEquationSeparate( GL_FUNC_ADD,
                            GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA,
                        GL_ONE_MINUS_SRC_ALPHA,
                        GL_ONE,
                        GL_ZERO);
    
    
    // activate the texture
    s_Texture.Activate();
    
    // Render the triangles
    s_IndexBuffer.RenderTriangles( nIndices );
    
    // Render the faces a little special
    //glDisable(GL_CULL_FACE);
   // s_IndexBuffer.RenderTriangles( nFinIndices, nIndices );
    //glEnable(GL_CULL_FACE);
    
    // Deactivate Everything
    s_VertexBuffer.Deactivate();
    s_IndexBuffer.Deactivate();
    s_ShaderProgram.Deactivate();
    
    Draw.Begin( ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_ZBUFFER_OFF | ENG_DRAW_MODE_RASTER_CULL_NONE );
    Draw.SetTexture( s_Texture );
    
    xrect Rect( 10, 50, s_Texture.getWidth() + 10, s_Texture.getHeight() + 50 );
    
    Draw.DrawSolidRect( Rect, xcolor(~0) );
    Draw.End();
}

//-------------------------------------------------------------------------------

void MetaBalls2( u32 Mode )
{
    s_MetaBalls2.Update( 0.04f );
    
    //
    // Render the actual isosurface
    //
    s32 nVertices;
    s32 nIndices;
    s_MetaBalls2.Render( &s_VertexMetaBalls2[0], nVertices, &s_IndexMetaBalls2[0], nIndices );
    
    eng_context& Contex = eng_GetCurrentContext();
    eng_draw& Draw = Contex.getDraw();
    
    if( nVertices && nIndices )
    {
        draw_vertex*                pDrawVertex;
        u16*                        pDrawIndices;
        xmatrix4                    L2W;
        
        L2W.Identity();
        L2W.setScale( 320 );
        
        Draw.Begin( ENG_DRAW_MODE_3D | Mode );
        {
            Draw.SetL2W( L2W );
            Draw.GetBuffers( &pDrawVertex, nVertices, &pDrawIndices, nIndices );
            
            x_memcpy( pDrawIndices, &s_IndexMetaBalls2[0], sizeof(u16)*nIndices );
            
            const xvector3 LightDir(1,1,0);
            for( s32 i=0; i<nVertices; i++ )
            {
                f32 I = x_Max( xvector3d( s_VertexMetaBalls2[i].n[0],
                                          s_VertexMetaBalls2[i].n[1],
                                          s_VertexMetaBalls2[i].n[2] ).Dot( LightDir ), 0);
                pDrawVertex[i].setup( xvector3d( s_VertexMetaBalls2[i].v[0],
                                               s_VertexMetaBalls2[i].v[1],
                                               s_VertexMetaBalls2[i].v[2] ),
                                    0, 0, xcolor(xvector3d(0,I,0)));
            }
            Draw.DrawBufferTriangles();
        }
        Draw.End();
    }    
}

//-------------------------------------------------------------------------------

void MetaBalls3( u32 Mode )
{
    s_MetaBalls3.Update( 0.04f );
    
    //
    // Render the actual isosurface
    //
    s32 nVertices;
    s32 nIndices;
    s_MetaBalls3.Render( &s_VertexMetaBalls3[0], nVertices, &s_IndexMetaBalls3[0], nIndices );
    
    
    if( nVertices && nIndices )
    {
        draw_vertex*                pDrawVertex;
        u16*                        pDrawIndices;
        xmatrix4                    L2W;
        
        L2W.Identity();
        L2W.setScale( 420 );
        
        eng_draw& Draw = eng_GetCurrentContext().getDraw();
        
        Draw.Begin( ENG_DRAW_MODE_3D | Mode );
        {
            Draw.SetL2W( L2W );
            Draw.GetBuffers( &pDrawVertex, nVertices, &pDrawIndices, nIndices );
            
            x_memcpy( pDrawIndices, &s_IndexMetaBalls3[0], sizeof(u16)*nIndices );
            
            const xvector3 LightDir(1,1,0);
            for( s32 i=0; i<nVertices; i++ )
            {
                f32 I = x_Max( s_VertexMetaBalls3[i].m_Normal.Dot( LightDir ), 0);
                pDrawVertex[i].setup( s_VertexMetaBalls3[i].m_Pos,
                                   0, 0, xcolor(xvector3d(0,0,I)));
            }
            Draw.DrawBufferTriangles();
        }
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void Render( void )
{
    static f64 RTSec =  0;
    static s32 Case  = -1;
    static s32 Mode  =  0;
    
    if( x_GetTimeSec() > RTSec )
    {
        Case = (Case+1)%(3+1);
        RTSec = x_GetTimeSec() + 20;
    }

#ifdef X_DEBUG
    x_printfxy( 0, 2, "DEBUG!");
#endif
    
   // Case = 0;
    
    //
    // Render 
    //
    switch( Case )
    {
        case 0:
        {
            x_printfxy( 0, 1, "METHOD 1");
            MetaBalls( Mode );
            break;
        }
        case 1:
        {
            x_printfxy( 0, 1, "METHOD 2");
            MetaBalls2( Mode );
            break;
        }
        case 2:
        {
            x_printfxy( 0, 1, "METHOD 3");
            MetaBalls3( Mode );
            break;
        }
        default:
        {
            RTSec = 0;
            if( Mode ) Mode = 0;
            else       Mode = ENG_DRAW_MODE_RASTER_WIRE_FRAME;
        }
    };
}

//-------------------------------------------------------------------------------
// http://aras-p.info/blog/2010/09/29/glsl-optimizer/
//

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

void AppMain( s32 argc, char* argv[] )
{
    // Initialize the engine
    eng_Init();
    g_Scheduler.Init( 2 );
    
    //Group.Clear();
    
    eng_hwin MainWindow;
#ifdef TARGET_3DS
    MainWindow  = SCREEN_TOP;
#else
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
#endif
    
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
    eng_SetCurrentContext(DisplayContext);
    
    // Initialize the engine
    
    DisplayContext.SetClearColor( xcolor( 0, 0, 255,255) );
    
    eng_view View = DisplayContext.GetActiveView();
    View.LookAt( 700, xvector3(0,0,0) );
    
    //
    // Create the shader and vertex/index buffers
    //
    s_VertexShader.LoadFromMemory   ( NoiseShader_vsh, NoiseShader_vsh_len );
    s_FragmentShader.LoadFromMemory ( NoiseShader_fsh, NoiseShader_fsh_len );
    s_GeometryShader.LoadFromMemory ( NoiseShader_gsh, NoiseShader_gsh_len );
    
    // Create the vertex descriptors
    s_VertexDesc.DescriveVertex( "Inposition", eng_vertex_desc::DATA_DESC_F32X3, eng_vertex_desc::USE_DESC_POSITION,
                                sizeof(metaball_vertex), sizeof(f32)*0 );
    s_VertexDesc.DescriveVertex( "Innormal",   eng_vertex_desc::DATA_DESC_F32X3, eng_vertex_desc::USE_DESC_NORMAL,
                                sizeof(metaball_vertex),sizeof(f32)*3 );
    s_VertexDesc.DescriveVertex( "IntexCoord",  eng_vertex_desc::DATA_DESC_F32X2, eng_vertex_desc::USE_DESC_TEXCOORD,
                                sizeof(metaball_vertex),sizeof(f32)*6 );
    
    //s_ShaderProgram.LinkShaders( s_VertexShader, s_GeometryShader, s_FragmentShader );
    s_ShaderProgram.LinkShaders( s_VertexDesc, s_VertexShader, s_FragmentShader );
    s_ShaderProgram.LinkRegisterToUniformVariable( 0, "time" );
    s_ShaderProgram.LinkRegisterToUniformVariable( 1, "NormalMatrix" );
    
    s_IndexBuffer.CreateDynamicBuffer(eng_ibuffer::DATA_DESC_U16, 60000 );
    s_VertexBuffer.CreateDynamicBuffer( s_VertexDesc, 60000 );
    
    // Create our check table and init to zeros
    s_CheckTable.Init( (8000*8000) );
    
    //
    // Create a texture
    //
    {
        xbitmap Bitmap;
        xbmp_Load( Bitmap, "CloudLight.tga");
        s_Texture.CreateTexture( Bitmap, FALSE );
        s_Texture.setWrappingMode(eng_texture::WRAP_MODE_CLAMP, eng_texture::WRAP_MODE_CLAMP );
        
    }
    
    //
    // Create a texture
    //
    {
        xbitmap Bitmap;
        xbmp_Load( Bitmap, "BlueSky.tga");
        s_BlueSkyTexture.CreateTexture( Bitmap, FALSE );
        s_BlueSkyTexture.setWrappingMode(eng_texture::WRAP_MODE_CLAMP, eng_texture::WRAP_MODE_CLAMP );
    }
    
    
    
    //
    // Initialize all the metaball stuff
    //
    s_MetaBalls.Init();
    
    s_VertexMetaBalls2.Alloc( CMAX_VERTICES );
    s_IndexMetaBalls2.Alloc( CMAX_INDICES );
	s_MetaBalls2.SetGridSize(48);
    
	CMarchingCubes::BuildTables();
    
    s_VertexMetaBalls3.Alloc( metaballsv3::MAX_VERTICES );
    s_IndexMetaBalls3.Alloc( metaballsv3::MAX_INDICES );
	s_MetaBalls3.Initialize(48);
    
    // Main message loop
    while( DisplayContext.HandleEvents() )
    {
        //View.setViewport( xirect( 0, 0, Width, Height ) );
        DisplayContext.SetActiveView( View );
        
        // Now draw something
        DisplayContext.Begin( "Main Rendering" );
        Render();
        DisplayContext.End();
        
        DisplayContext.PageFlip( FALSE );
    }
}

// Good explanation of iso-surfaces: http://morgan-davidson.com/2012/07/31/metaballs/
