//
//  isoSurface.h
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/6/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

class isosurface
{
public:
    
    struct vertex
    {
        xvector3d       m_Pos;
        xvector3d       m_Normal;
    };
    
public:
    
                    isosurface          ( void );
    virtual        ~isosurface          ( void );
    
	void            setSize             ( const xvector3& minLim, const xvector3& maxLim, const s32 sX, const s32 sY, const s32 sZ );
	void            Update              ( void );
	s32             getVertexCount      ( void ) const { return m_nVertices; }
	s32             getIndexCount       ( void ) const { return m_nIndices;  }
	const vertex*   getVertexArray      ( void ) const { return &m_VertexArray[0]; }
	const u16*      getIndexArray       ( void ) const { return &m_IndexArray[0]; }
    
protected:
      
    struct grid_node
    {
        xvector3        m_Pos;
        xvector3        m_Normal;
        f32             m_Value;
        f32             m_Pad[3];
    };
    
    struct edge
    {
        grid_node*      m_pNode0;
        grid_node*      m_pNode1;
        s32             m_Index;
    };
    
    struct cell
    {
        xsafe_array<grid_node*,8>   m_Nodes;
        xsafe_array<edge*,12>       m_Edges;
    };

protected:
    
    virtual f32     FieldFunction       ( const xvector3& Pos )=0;
    
protected:
    
	s32             getEdgeCount        ( void ) const;
	void            computeField        ( const s32 start, const s32 end );
	void            computeVertexArray  ( const s32 start, const s32 end );
	void            computeIndexArray   ( void );
    
protected:
    
	s32             m_SizeX, m_SizeY, m_SizeZ;
	xvector3        m_DX, m_DY, m_DZ;
    
    xptr<edge*>     m_IntersectedEdges;
    xptr<cell>      m_Cells;
    xptr<edge>      m_Edges;
    xptr<grid_node> m_Nodes;
	xptr<vertex>    m_VertexArray;
	xptr<u16>       m_IndexArray;
    
	s32             m_nVertices;
	s32             m_nIndices;
};


