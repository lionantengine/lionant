//
//  PerlinNoise.cpp
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/9/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "x_base.h"
#include "PerlinNoise.h"

#define B 0x100
#define BM 0xff
#define N 0x1000
#define NP 12   /* 2^N */
#define NM 0xfff

#define s_curve(t) ( t * t * (3. - 2. * t) )
#define lerp(t, a, b) ( a + t * (b - a) )
#define setup(i,b0,b1,r0,r1)\
t = vec[i] + N;\
b0 = ((int)t) & BM;\
b1 = (b0+1) & BM;\
r0 = t - (int)t;\
r1 = r0 - 1.;
#define at2(rx,ry) ( rx * q[0] + ry * q[1] )
#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )


/* Coherent noise function over 1, 2 or 3 dimensions */
/* (copyright Ken Perlin) */


static int p[B + B + 2];
static f32 g3[B + B + 2][3];
static f32 g2[B + B + 2][2];
static f32 g1[B + B + 2];
static int start = 1;


void        init(void);
f32         noise1(f32);
f32         noise2(f32 *);
f32         noise3(f32 *);
void        normalize3(f32 *);
void        normalize2(f32 *);

void normalize2( f32* v )
{
    f32 r = x_InvSqrt( v[0]*v[0]+v[1]*v[1] );
    v[0] *= r;
    v[1] *= r;
}

void normalize3( f32* v )
{
    f32 r = x_InvSqrt( v[0]*v[0]+v[1]*v[1]+v[2]*v[2] );
    v[0] *= r;
    v[1] *= r;
    v[2] *= r;
}

f32 noise1(f32 arg)
{
    int bx0, bx1;
    f32 rx0, rx1, sx, t, u, v, vec[1];
    
    vec[0] = arg;
    if (start) {
        start = 0;
        init();
    }
    
    setup(0,bx0,bx1,rx0,rx1);
    
    sx = s_curve(rx0);
    u = rx0 * g1[ p[ bx0 ] ];
    v = rx1 * g1[ p[ bx1 ] ];
    
    return(lerp(sx, u, v));
}

f32 noise2(f32 vec[2])
{
    int bx0, bx1, by0, by1, b00, b10, b01, b11;
    f32 rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
    int i, j;
    
    if (start) {
        start = 0;
        init();
    }
    
    setup(0, bx0,bx1, rx0,rx1);
    setup(1, by0,by1, ry0,ry1);
    
    i = p[ bx0 ];
    j = p[ bx1 ];
    
    b00 = p[ i + by0 ];
    b10 = p[ j + by0 ];
    b01 = p[ i + by1 ];
    b11 = p[ j + by1 ];
    
    sx = s_curve(rx0);
    sy = s_curve(ry0);
    
    q = g2[ b00 ] ; u = at2(rx0,ry0);
    q = g2[ b10 ] ; v = at2(rx1,ry0);
    a = lerp(sx, u, v);
    
    q = g2[ b01 ] ; u = at2(rx0,ry1);
    q = g2[ b11 ] ; v = at2(rx1,ry1);
    b = lerp(sx, u, v);
    
    return lerp(sy, a, b);
}

f32 noise3(f32 vec[3])
{
    int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
    f32 rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u, v;
    int i, j;
    
    if (start) {
        start = 0;
        init();
    }
    
    setup(0, bx0,bx1, rx0,rx1);
    setup(1, by0,by1, ry0,ry1);
    setup(2, bz0,bz1, rz0,rz1);
    
    i = p[ bx0 ];
    j = p[ bx1 ];
    
    b00 = p[ i + by0 ];
    b10 = p[ j + by0 ];
    b01 = p[ i + by1 ];
    b11 = p[ j + by1 ];
    
    t  = s_curve(rx0);
    sy = s_curve(ry0);
    sz = s_curve(rz0);
    
    q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
    q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
    a = lerp(t, u, v);
    
    q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
    q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
    b = lerp(t, u, v);
    
    c = lerp(sy, a, b);
    
    q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
    q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
    a = lerp(t, u, v);
    
    q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
    q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
    b = lerp(t, u, v);
    
    d = lerp(sy, a, b);
    
    return lerp(sz, c, d);
}

void init(void)
{
    int i, j, k;
    
    for (i = 0 ; i < B ; i++) {
        p[i] = i;
        g1[i] = (f32)((random() % (B + B)) - B) / B;
        
        for (j = 0 ; j < 2 ; j++)
            g2[i][j] = (f32)((random() % (B + B)) - B) / B;
        normalize2(g2[i]);
        
        for (j = 0 ; j < 3 ; j++)
            g3[i][j] = (f32)((random() % (B + B)) - B) / B;
        normalize3(g3[i]);
    }
    
    while (--i) {
        k = p[i];
        p[i] = p[j = random() % B];
        p[j] = k;
    }
    
    for (i = 0 ; i < B + 2 ; i++) {
        p[B + i] = p[i];
        g1[B + i] = g1[i];
        for (j = 0 ; j < 2 ; j++)
            g2[B + i][j] = g2[i][j];
        for (j = 0 ; j < 3 ; j++)
            g3[B + i][j] = g3[i][j];
    }
}

/* --- My harmonic summing functions - PDB --------------------------*/

/*
 In what follows "alpha" is the weight when the sum is formed.
 Typically it is 2, As this approaches 1 the function is noisier.
 "beta" is the harmonic scaling/spacing, typically 2.
 */

f32 PerlinNoise1D(f32 x,f32 alpha,f32 beta,int n)
{
    int i;
    f32 val,sum = 0;
    f32 p,scale = 1;
    
    p = x;
    for (i=0;i<n;i++) {
        val = noise1(p);
        sum += val / scale;
        scale *= alpha;
        p *= beta;
    }
    return(sum);
}

f32 PerlinNoise2D(f32 x,f32 y,f32 alpha,f32 beta,int n)
{
    int i;
    f32 val,sum = 0;
    f32 p[2],scale = 1;
    
    p[0] = x;
    p[1] = y;
    for (i=0;i<n;i++) {
        val = noise2(p);
        sum += val / scale;
        scale *= alpha;
        p[0] *= beta;
        p[1] *= beta;
    }
    return(sum);
}

f32 PerlinNoise3D(f32 x,f32 y,f32 z,f32 alpha,f32 beta,int n)
{
    int i;
    f32 val,sum = 0;
    f32 p[3],scale = 1;
    
    p[0] = x;
    p[1] = y;
    p[2] = z;
    for (i=0;i<n;i++) {
        val = noise3(p);
        sum += val / scale;
        scale *= alpha;
        p[0] *= beta;
        p[1] *= beta;
        p[2] *= beta;
    }
    return(sum);
}
