//
//  MetaBalls.h
//  engBaseExample-02
//
//  Created by Tomas Arce on 7/7/13.
//  Copyright (c) 2013 Tomas Arce. All rights reserved.
//

#include "isoSurface.h"


class metaballs : public isosurface
{
public:
    
    struct metaball
    {
        xvector3        m_Pos;
        xvector3        m_Dir;
        f32             m_OneOverRadius;
        f32             m_Pad[3];
    };

public:
    
                    metaballs           ( void );
    void            Update              ( f32 DeltaTime );
    void            Init                ( void );

public:

    xsafe_array<metaball,32>    m_Balls;

protected:
    
    virtual f32     FieldFunction       ( const xvector3& Pos );
};

