//#version 120
//#extension GL_EXT_geometry_shader4: enable

varying in  vec3 NormalIn[3];
varying out vec3 Normal;

varying in  vec3 MultiTexCoord0In[3];
varying out vec3 MultiTexCoord0;

void main()
{
    for(int i = 0; i < gl_VerticesIn; ++i)
    {
        // copy color
        //gl_FrontColor = gl_FrontColorIn[i];
        
        // copy position
        gl_Position         = gl_PositionIn[i];
        Normal              = NormalIn[i];
        MultiTexCoord0      = MultiTexCoord0In[i];
        
        // done with the vertex
        EmitVertex();
    }
}
