// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

uniform vec4      iResolution;     // viewport resolution (in pixels) and offset
uniform float     iGlobalTime;     // shader playback time (in seconds)
uniform vec4      iMouse;          // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;       // input channel. XX = 2D/Cube

vec4 iDate = vec4(1.);

float distanceToSegment( vec2 a, vec2 b, vec2 p )
{
	vec2 pa = p - a;
	vec2 ba = b - a;
	float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
	
	return length( pa - ba*h );
}

void main(void)
{
// Tomas: Just hacked this here
    iDate = vec4(iGlobalTime);

    // get time
	float secs = mod( floor(iDate.w),        60.0 );
	float mins = mod( floor(iDate.w/60.0),   60.0 );
	float hors = mod( floor(iDate.w/3600.0), 24.0 );

	vec2 uv = -1.0 + 2.0*(gl_FragCoord.xy - iResolution.zw).xy / iResolution.xy;
	uv.x *= iResolution.x/iResolution.y;
	float r = length( uv );
	float a = atan( uv.y, uv.x )+3.1415926;
    
	// background color
	vec3 nightColor = vec3( 0.2, 0.2, 0.2 ) + 0.1*uv.y;
	vec3 dayColor   = vec3( 0.5, 0.6, 0.7 ) + 0.2*uv.y;
	vec3 col = mix( nightColor, dayColor, smoothstep( 5.0, 7.0, hors ) - 
				                          smoothstep(19.0,21.0, hors ) );

    // inner watch body	
	col = mix( col, vec3(0.9-0.4*pow(r,4.0)), 1.0-smoothstep(0.94,0.95,r) );

    // 5 minute marks	
	float g = 1.0-smoothstep( 0.0, 0.1, abs(fract(a*12.0/6.2831) ) );
    float f = 1.0-smoothstep( 0.0, 0.1, -0.1*g+abs(fract(a*60.0/6.2831) ) );
	f *= smoothstep( 0.85, 0.86, r+0.05*g ) - smoothstep( 0.94, 0.95, r );
	col = mix( col, vec3(0.0), f );

    // seconds hand
	vec2 dir;
	dir = vec2( sin(6.2831*secs/60.0), cos(6.2831*secs/60.0) );
	f = distanceToSegment( vec2(0.0), dir*0.9, uv );
	f = 1.0 - smoothstep( 0.005, 0.006, f );
	col = mix( col, vec3(0.5,0.0,0.0), f );

	// minutes hand
	dir = vec2( sin(6.2831*mins/60.0), cos(6.2831*mins/60.0) );
	f = distanceToSegment( vec2(0.0), dir*0.7, uv );
	f = 1.0 - smoothstep( 0.01, 0.012, f );
	col = mix( col, vec3(0.0), f );

    // hours hand
	dir = vec2( sin(6.2831*hors/12.0), cos(6.2831*hors/12.0) );
	f = distanceToSegment( vec2(0.0), dir*0.4, uv );
	f = 1.0 - smoothstep( 0.01, 0.02, f );
	col = mix( col, vec3(0.0), f );

    // center mini circle	
	col = mix( col, vec3(0.5), 1.0-smoothstep(0.050,0.055,r) );
	col = mix( col, vec3(0.0), 1.0-smoothstep(0.005,0.01,abs(r-0.055)) );

    // border of watch
	col = mix( col, vec3(0.0), 1.0-smoothstep(0.01,0.02,abs(r-0.95)) );

	gl_FragColor = vec4( col,1.0 );
}