uniform vec4      iResolution;     // viewport resolution (in pixels) and offset
uniform float     iGlobalTime;     // shader playback time (in seconds)
uniform vec4      iMouse;          // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;       // input channel. XX = 2D/Cube



void main()
{
	float v,t=v=.001;
	for (float s=.0; s<2.; s+=.01) {
		vec3 p=s*vec3((gl_FragCoord.xy - iResolution.zw).xy,1.)*t+vec3(.1,.2,fract(s+floor(iGlobalTime*25.)*.01));
		for (int i=0; i<8; i++) p=abs(p)/dot(p,p)-.8;
		v+=dot(p,p)*t;
	}
	gl_FragColor=vec4(v);
}