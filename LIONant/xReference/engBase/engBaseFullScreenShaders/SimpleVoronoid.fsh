

uniform vec4      iResolution;     // viewport resolution (in pixels) and offset
uniform float     iGlobalTime;     // shader playback time (in seconds)
uniform vec4      iMouse;          // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;       // input channel. XX = 2D/Cube

// LUT based 3d value noise
float random2f( in vec2 x )
{
    vec2 p = floor(x);
    vec2 f = fract(x);
	f = f*f*(3.0-2.0*f);
	
	vec2 uv = (p.xy+vec2(37.0,17.0)*1.) + f.xy;
	vec2 rg = texture2D( iChannel0, (uv+ 0.5)/256.0, -100.0 ).yx;
	return mix( rg.x, rg.y, 1. );
}

float voronoi( vec2 x )
{
    ivec2 p = ivec2( floor( x ) );
    vec2  f = fract( x );

    float res = 8.0;
    for( int j=-1; j<=1; j++ )
    for( int i=-1; i<=1; i++ )
    {
        ivec2 b = ivec2( i, j );
        vec2  r = vec2( b ) - f + random2f( vec2(p + b) );
        float d = dot( r, r );

        res = min( res, d );
    }
    return sqrt( res );
}

void main(void)
{
    vec2 q = (gl_FragCoord.xy - iResolution.zw) / iResolution.xy;
    
    float i = voronoi( q * 10. + iGlobalTime );
    
    gl_FragColor = vec4( i,i,i, 1.0 );
}


