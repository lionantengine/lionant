attribute vec3      aPos;

uniform mat4        L2C;

void main()
{
    gl_Position = L2C * vec4( aPos, 1. );
}
