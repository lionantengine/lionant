#include "eng_base.h"
#include "xbmp_tools.h"

struct vertex
{
    xvector3d   m_Pos;
};

//-------------------------------------------------------------------------------

#include "passthough.vsh.h"
#include "clouds.fsh.h"
#include "SimpleVoronoid.fsh.h"
#include "VoronoidEdges.fsh.h"
#include "InversionMac.fsh.h"
#include "Roses.fsh.h"
#include "MineCraft.fsh.h"
#include "MoonLight.fsh.h"
#include "Electron.fsh.h"
#include "Saturn.fsh.h"
#include "TruchetTentacles.fsh.h"
#include "FogMountains.fsh.h"
#include "MoreSpheres.fsh.h"
#include "WhaterAndFire.fsh.h"
#include "MetaLights.fsh.h"
#include "LogoType.fsh.h"
#include "VoxelTire.fsh.h"
#include "Thumper.fsh.h"
#include "Tripping.fsh.h"
#include "IntervalMapping.fsh.h"
#include "ReflectingFloor.fsh.h"
#include "RoadToHell.fsh.h"
#include "CoolCPU.fsh.h"
#include "FireWall.fsh.h"
#include "FurBall.fsh.h"
#include "Volcanic.fsh.h"
#include "JuliaBulb.fsh.h"
#include "Maldelbrot.fsh.h"
#include "StarNursery.fsh.h"
#include "HautedForest.fsh.h"
#include "Explosion.fsh.h"
#include "Angels.fsh.h"
#include "Oceanic.fsh.h"
#include "SadRobot.fsh.h"
#include "Journy.fsh.h"
#include "MengerSponge.fsh.h"
#include "Love.fsh.h"
#include "Simplicity.fsh.h"
#include "Iterations.fsh.h"
#include "Fire.fsh.h"
#include "Apple.fsh.h"
#include "Apollonian.fsh.h"
#include "Cosmos.fsh.h"
#include "SimplyCloud.fsh.h"
#include "FractalFire.fsh.h"
#include "Galaxy.fsh.h"
#include "Siggraph.fsh.h"
#include "Blobs.fsh.h"
#include "MetaHexaBalls.fsh.h"
#include "RadialBlur.fsh.h"
#include "JuliaDistance.fsh.h"
#include "DigitalBrain.fsh.h"
#include "Bubbles.fsh.h"
#include "DeformFlower.fsh.h"
#include "Shapeshifter.fsh.h"
#include "RoadToRi.fsh.h"
#include "StrangWarpping.fsh.h"
#include "ChaosTrent.fsh.h"
#include "Cells.fsh.h"
#include "A2DKIFS.fsh.h"
#include "PerlinPlanet.fsh.h"
#include "Industry.fsh.h"
#include "Nautilus.fsh.h"
#include "SteelCubes.fsh.h"
#include "enlightened.fsh.h"
#include "DataTransfer.fsh.h"
#include "MirrorCube.fsh.h"
#include "NanoTubes.fsh.h"
#include "D704.fsh.h"
#include "LoadingBox.fsh.h"
#include "NoiseTest.fsh.h"
#include "RaySoft.fsh.h"
#include "Barbarella.fsh.h"
#include "LivingKIFS.fsh.h"
#include "BlobBox.fsh.h"
#include "MoreJulia.fsh.h"
#include "TunnelX.fsh.h"
#include "Auralights.fsh.h"
#include "Hypnoticripples.fsh.h"
#include "MandlebulbEvolution.fsh.h"
#include "Perlin.fsh.h"
#include "Voronoitoy.fsh.h"
#include "Watch.fsh.h"
#include "FractalNoise.fsh.h"
#include "FlappingWings.fsh.h"
#include "Blueserpents.fsh.h"
#include "SpiralRepetition.fsh.h"
#include "EyeOfSauron.fsh.h"
#include "Circuits.fsh.h"
#include "avatar.fsh.h"
#include "Magica.fsh.h"
#include "SpaceRaceLight.fsh.h"
#include "FilteringProcedurals.fsh.h"
#include "FractalCartoon.fsh.h"
#include "Spaceship.fsh.h"
#include "RandomSpheres.fsh.h"
#include "DualComplexNumbers.fsh.h"
#include "GenerationRedux.fsh.h"
#include "Quadrics.fsh.h"
#include "AncientTemple.fsh.h"
#include "Monster.fsh.h"
#include "noxiousBox.fsh.h"
#include "GlassEel.fsh.h"
#include "FoggyMountains2.fsh.h"

static const int SCREEN_WIDTH   = 1024;
static const int SCREEN_HEIGHT  = 768;

eng_vshader                         s_vPassthough;
s32                                 s_nShaders=0;
xsafe_array<eng_fshader,150>        s_Shaders;
xsafe_array<s32,150>                s_ExpedtedFPS;
eng_shader_program                  s_Program;
eng_texture                         s_PerlingNoiseTexture;
eng_texture                         s_LIONantTexture;
eng_vertex_desc                     s_VertexDesc;
eng_ibuffer                         s_IndexBuffer;
eng_vbuffer                         s_VertexBuffer;
vertex                              s_Vertex[4];
eng_keyboard                        s_Keyboard;

enum shader_variables
{
    SHADER_VS_L2C,
    SHADER_PS_RESOLUTION,
    SHADER_PS_GLOBALTIME
};

// The site that inspire this demo.... http://www.iquilezles.org/default.html
// Sources from: https://www.shadertoy.com

//-------------------------------------------------------------------------------

void Init( void )
{
    // Initialize the engine
    eng_Init();
    
    eng_hwin MainWindow;
    MainWindow  = eng_CreateWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    eng_context& DisplayContext = eng_CreateContext();
    DisplayContext.PresetSetRes( SCREEN_WIDTH, SCREEN_HEIGHT );
    DisplayContext.PresetWindowHandle(MainWindow);
    DisplayContext.Init();
    
  //  DisplayContext.SetClearColor( xcolor(~0));
    
    eng_SetCurrentContext(DisplayContext);
    
    eng_view View = DisplayContext.GetActiveView();
    
    // Load tehe vertex shader first
    s_vPassthough.LoadFromMemory( passthough_vsh, passthough_vsh_len );
    
    // Load all the pixel shaders
    {
        s_ExpedtedFPS[s_nShaders] = 4;
        s_Shaders[s_nShaders++].LoadFromMemory( clouds_fsh, clouds_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 40;
        s_Shaders[s_nShaders++].LoadFromMemory( SimpleVoronoid_fsh, SimpleVoronoid_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 27;
        s_Shaders[s_nShaders++].LoadFromMemory( VoronoidEdges_fsh, VoronoidEdges_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 16;
        s_Shaders[s_nShaders++].LoadFromMemory( InversionMac_fsh, InversionMac_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( Roses_fsh, Roses_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 32;
        s_Shaders[s_nShaders++].LoadFromMemory( MineCraft_fsh, MineCraft_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 25;
        s_Shaders[s_nShaders++].LoadFromMemory( MoonLight_fsh, MoonLight_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 5;
        s_Shaders[s_nShaders++].LoadFromMemory( Electron_fsh, Electron_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 4;
        s_Shaders[s_nShaders++].LoadFromMemory( Saturn_fsh, Saturn_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 10;
        s_Shaders[s_nShaders++].LoadFromMemory( TruchetTentacles_fsh, TruchetTentacles_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 3;
        s_Shaders[s_nShaders++].LoadFromMemory( FogMountains_fsh, FogMountains_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 1;
        s_Shaders[s_nShaders++].LoadFromMemory( MoreSpheres_fsh, MoreSpheres_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 64;
        s_Shaders[s_nShaders++].LoadFromMemory( WhaterAndFire_fsh, WhaterAndFire_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 200;
        s_Shaders[s_nShaders++].LoadFromMemory( MetaLights_fsh, MetaLights_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 22;
        s_Shaders[s_nShaders++].LoadFromMemory( LogoType_fsh, LogoType_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 3;
        s_Shaders[s_nShaders++].LoadFromMemory( VoxelTire_fsh, VoxelTire_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 3;
        s_Shaders[s_nShaders++].LoadFromMemory( Thumper_fsh, Thumper_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 67;
        s_Shaders[s_nShaders++].LoadFromMemory( Tripping_fsh, Tripping_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 10;
        s_Shaders[s_nShaders++].LoadFromMemory( IntervalMapping_fsh, IntervalMapping_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 14;
        s_Shaders[s_nShaders++].LoadFromMemory( ReflectingFloor_fsh, ReflectingFloor_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 4;
        s_Shaders[s_nShaders++].LoadFromMemory( RoadToHell_fsh, RoadToHell_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 7;
        s_Shaders[s_nShaders++].LoadFromMemory( CoolCPU_fsh, CoolCPU_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 4;
        s_Shaders[s_nShaders++].LoadFromMemory( FireWall_fsh, FireWall_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 10;
        s_Shaders[s_nShaders++].LoadFromMemory( FurBall_fsh, FurBall_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 1;
        s_Shaders[s_nShaders++].LoadFromMemory( Volcanic_fsh, Volcanic_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( JuliaBulb_fsh, JuliaBulb_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 11;
        s_Shaders[s_nShaders++].LoadFromMemory( Maldelbrot_fsh, Maldelbrot_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 4;
        s_Shaders[s_nShaders++].LoadFromMemory( StarNursery_fsh, StarNursery_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( HautedForest_fsh, HautedForest_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( Explosion_fsh, Explosion_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 1;
        s_Shaders[s_nShaders++].LoadFromMemory( Angels_fsh, Angels_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 1;
        s_Shaders[s_nShaders++].LoadFromMemory( Oceanic_fsh, Oceanic_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 1;
        s_Shaders[s_nShaders++].LoadFromMemory( SadRobot_fsh, SadRobot_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 5;
        s_Shaders[s_nShaders++].LoadFromMemory( Journy_fsh, Journy_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( MengerSponge_fsh, MengerSponge_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 200;
        s_Shaders[s_nShaders++].LoadFromMemory( Love_fsh, Love_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 48;
        s_Shaders[s_nShaders++].LoadFromMemory( Simplicity_fsh, Simplicity_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 9;
        s_Shaders[s_nShaders++].LoadFromMemory( Iterations_fsh, Iterations_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 34;
        s_Shaders[s_nShaders++].LoadFromMemory( Fire_fsh, Fire_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 11;
        s_Shaders[s_nShaders++].LoadFromMemory( Apple_fsh, Apple_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 5;
        s_Shaders[s_nShaders++].LoadFromMemory( Apollonian_fsh, Apollonian_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( Cosmos_fsh, Cosmos_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( SimplyCloud_fsh, SimplyCloud_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( FractalFire_fsh, FractalFire_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 34;
        s_Shaders[s_nShaders++].LoadFromMemory( Galaxy_fsh, Galaxy_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 16;
        s_Shaders[s_nShaders++].LoadFromMemory( Siggraph_fsh, Siggraph_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 119;
        s_Shaders[s_nShaders++].LoadFromMemory( Blobs_fsh, Blobs_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 10;
        s_Shaders[s_nShaders++].LoadFromMemory( MetaHexaBalls_fsh, MetaHexaBalls_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 32;
        s_Shaders[s_nShaders++].LoadFromMemory( RadialBlur_fsh, RadialBlur_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 23;
        s_Shaders[s_nShaders++].LoadFromMemory( JuliaDistance_fsh, JuliaDistance_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 30;
        s_Shaders[s_nShaders++].LoadFromMemory( DigitalBrain_fsh, DigitalBrain_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 18;
        s_Shaders[s_nShaders++].LoadFromMemory( Bubbles_fsh, Bubbles_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 200;
        s_Shaders[s_nShaders++].LoadFromMemory( DeformFlower_fsh, DeformFlower_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 13;
        s_Shaders[s_nShaders++].LoadFromMemory( Shapeshifter_fsh, Shapeshifter_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 12;
        s_Shaders[s_nShaders++].LoadFromMemory( RoadToRi_fsh, RoadToRi_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 88;
        s_Shaders[s_nShaders++].LoadFromMemory( StrangWarpping_fsh, StrangWarpping_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 18;
        s_Shaders[s_nShaders++].LoadFromMemory( ChaosTrent_fsh, ChaosTrent_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 13;
        s_Shaders[s_nShaders++].LoadFromMemory( Cells_fsh, Cells_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 20;
        s_Shaders[s_nShaders++].LoadFromMemory( A2DKIFS_fsh, A2DKIFS_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 54;
        s_Shaders[s_nShaders++].LoadFromMemory( PerlinPlanet_fsh, PerlinPlanet_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 25;
        s_Shaders[s_nShaders++].LoadFromMemory( Industry_fsh, Industry_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 11;
        s_Shaders[s_nShaders++].LoadFromMemory( Nautilus_fsh, Nautilus_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 22;
        s_Shaders[s_nShaders++].LoadFromMemory( SteelCubes_fsh, SteelCubes_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 40;
        s_Shaders[s_nShaders++].LoadFromMemory( enlightened_fsh, enlightened_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 13;
        s_Shaders[s_nShaders++].LoadFromMemory( DataTransfer_fsh, DataTransfer_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 21;
        s_Shaders[s_nShaders++].LoadFromMemory( MirrorCube_fsh, MirrorCube_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 17;
        s_Shaders[s_nShaders++].LoadFromMemory( NanoTubes_fsh, NanoTubes_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 70;
        s_Shaders[s_nShaders++].LoadFromMemory( D704_fsh, D704_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 220;
        s_Shaders[s_nShaders++].LoadFromMemory( LoadingBox_fsh, LoadingBox_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 48;
        s_Shaders[s_nShaders++].LoadFromMemory( NoiseTest_fsh, NoiseTest_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( RaySoft_fsh, RaySoft_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Barbarella_fsh, Barbarella_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( LivingKIFS_fsh, LivingKIFS_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 35;
        s_Shaders[s_nShaders++].LoadFromMemory( BlobBox_fsh, BlobBox_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 40;
        s_Shaders[s_nShaders++].LoadFromMemory( MoreJulia_fsh, MoreJulia_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 18;
        s_Shaders[s_nShaders++].LoadFromMemory( TunnelX_fsh, TunnelX_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 200;
        s_Shaders[s_nShaders++].LoadFromMemory( Auralights_fsh, Auralights_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 200;
        s_Shaders[s_nShaders++].LoadFromMemory( Hypnoticripples_fsh, Hypnoticripples_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 2;
        s_Shaders[s_nShaders++].LoadFromMemory( MandlebulbEvolution_fsh,MandlebulbEvolution_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 93;
        s_Shaders[s_nShaders++].LoadFromMemory( Perlin_fsh, Perlin_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 49;
        s_Shaders[s_nShaders++].LoadFromMemory( Voronoitoy_fsh, Voronoitoy_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 130;
        s_Shaders[s_nShaders++].LoadFromMemory( Watch_fsh, Watch_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 47;
        s_Shaders[s_nShaders++].LoadFromMemory( FractalNoise_fsh, FractalNoise_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 54;
        s_Shaders[s_nShaders++].LoadFromMemory( FlappingWings_fsh,FlappingWings_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 100;
        s_Shaders[s_nShaders++].LoadFromMemory( Blueserpents_fsh, Blueserpents_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( SpiralRepetition_fsh, SpiralRepetition_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( EyeOfSauron_fsh, EyeOfSauron_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Circuits_fsh, Circuits_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( avatar_fsh, avatar_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Magica_fsh, Magica_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( SpaceRaceLight_fsh, SpaceRaceLight_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( FilteringProcedurals_fsh, FilteringProcedurals_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( FractalCartoon_fsh, FractalCartoon_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Spaceship_fsh, Spaceship_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( RandomSpheres_fsh, RandomSpheres_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( DualComplexNumbers_fsh, DualComplexNumbers_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( GenerationRedux_fsh, GenerationRedux_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Quadrics_fsh, Quadrics_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( AncientTemple_fsh, AncientTemple_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( Monster_fsh, Monster_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( noxiousBox_fsh, noxiousBox_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( GlassEel_fsh, GlassEel_fsh_len );
        
        s_ExpedtedFPS[s_nShaders] = 15;
        s_Shaders[s_nShaders++].LoadFromMemory( FoggyMountains2_fsh, FoggyMountains2_fsh_len );
    }
    
    // Load the noise texture
    if( 1 )
    {
        xbitmap Bitmap;
        xbmp_Load( Bitmap, "NoiseTexture.tga" );
        s_PerlingNoiseTexture.CreateTexture( Bitmap );
        s_PerlingNoiseTexture.setWrappingMode( eng_texture::WRAP_MODE_MIRROR, eng_texture::WRAP_MODE_MIRROR );
    }
    
    // Load the noise texture
    if( 1 )
    {
        xbitmap Bitmap;
        xbmp_Load( Bitmap, "LIONant_LOGO.tga" );
        s_LIONantTexture.CreateTexture( Bitmap );
    }
    
    // Setup the indices
    u16 Index[] = { 0,1,2, 0,2,3 };
    s_IndexBuffer.CreateStaticBuffer( eng_ibuffer::DATA_DESC_U16, sizeof(Index)/sizeof(Index[0]) );
    s_IndexBuffer.UploadData( 0, sizeof(Index)/sizeof(Index[0]), Index );
    
    s_VertexDesc.DescriveVertex( "aPos",
                                eng_vertex_desc::DATA_DESC_F32X3,
                                eng_vertex_desc::USE_DESC_POSITION,
                                sizeof(vertex),
                                X_MEMBER_OFFSET( vertex, m_Pos) );
    
    // Initialize the vertex buffer
    s_VertexBuffer.CreateDynamicBuffer( s_VertexDesc, 4 );
}

//-------------------------------------------------------------------------------

void DrawShader( f64 StartTime, s32 DownXRes, s32 DownYRes  )
{
    s32 W, H;
    
    eng_GetCurrentContext().GetScreenResolution( W, H );
    
    // Offsets(X,Y), Size(Z,W)
    const s32      SizeW = W >> DownXRes;
    const s32      SizeH = H >> DownYRes;
    const xvector4 Rect( (W-SizeW)/2.f, (H-SizeH)/2.f, (f32)SizeW, (f32)SizeH );
    const xvector4 Rect2( Rect.m_Z, Rect.m_W, Rect.m_X, H-(Rect.m_Y+Rect.m_W));
    const xmatrix4 L2C_2D( xvector3d( 2.0f/W, 2.0f/H, 1 ), xradian3(0,0,0), xvector3d( -1, -1, 0 ) );

    //
    // Set the blend modes
    //

    // No alpha need it
    glDisable( GL_BLEND );

    // No zbuffer need it
    glDepthMask( FALSE );
    glDisable( GL_DEPTH_TEST );
    glDepthFunc( GL_ALWAYS );
 
    //
    // Activate the program
    //
    s_Program.Activate();
    s_Program.setUniformVariable( SHADER_PS_RESOLUTION, Rect2 );
    s_Program.setUniformVariable( SHADER_PS_GLOBALTIME, (f32)(x_GetTimeSec()-StartTime) );
    s_Program.setUniformVariable( SHADER_VS_L2C,        L2C_2D );

    // Set the texture
    s_PerlingNoiseTexture.Activate();

    // Set the vertices to the new quad dimension
    s_Vertex[0].m_Pos.Set( Rect.m_X,            Rect.m_Y,           0 );
    s_Vertex[1].m_Pos.Set( Rect.m_X+Rect.m_Z,   Rect.m_Y ,          0 );
    s_Vertex[2].m_Pos.Set( Rect.m_X+Rect.m_Z,   Rect.m_Y+Rect.m_W,  0 );
    s_Vertex[3].m_Pos.Set( Rect.m_X,            Rect.m_Y+Rect.m_W,  0 );

    // Upload the new vertices
    s_VertexBuffer.UploadData( 0, 4, s_Vertex );
    s_VertexBuffer.Activate();
    
    // Now lets render the mesh
    s_IndexBuffer.Activate();
    s_IndexBuffer.RenderTriangles();
    
    s_IndexBuffer.Deactivate();
    s_VertexBuffer.Deactivate();
    s_Program.Deactivate();
    
    //
    // Render logo
    //
    if( 0 )
    {
        xvector4 Rect;
        Rect.Set( 10.0f, 20.0f, (f32)s_LIONantTexture.getWidth(), (f32)s_LIONantTexture.getHeight() );

        eng_draw& Draw = eng_GetCurrentContext().getDraw();
        
        Draw.Begin(ENG_DRAW_MODE_2D_LT | ENG_DRAW_MODE_TEXTURE_ON | ENG_DRAW_MODE_BLEND_ALPHA );
        Draw.SetTexture( s_LIONantTexture );
        Draw.ClearL2W();
        Draw.DrawSolidRect( xrect( Rect.m_X, Rect.m_Y, Rect.m_X+Rect.m_Z, Rect.m_Y+Rect.m_W), xcolor(~0) );
        Draw.End();
    }
}

//-------------------------------------------------------------------------------

void Render( void )
{
    static f64 RTSec     =  0;
    static s32 Case      = -1;
    static f64 StartTime = 0;
    static f64 LastTime  = 0;
    static s32 DownXRes  = 0;
    static s32 DownYRes  = 0;
    static f64 WaitDealy = 0;
    
    f64 Time        = x_GetTimeSec();
    f64 DeltaTime   = (Time - LastTime);
    
    WaitDealy -= DeltaTime;
    
    if( Time > RTSec || s_Keyboard.wasPressed(eng_keyboard::KEY_SPACE) )
    {
        WaitDealy   = 1;
        DownXRes    = 0;
        DownYRes    = 0;
        
        // This code is used to skip very heavy (slow) shaders 
        do {
            
            Case = (Case+1)%s_nShaders;
            
        } while( s_ExpedtedFPS[Case] < 5 );
        
       // Case = s_nShaders-1;
        
        RTSec = x_GetTimeSec() + 20;
        
        if(!StartTime)StartTime = x_GetTimeSec();
        
        s_Program.LinkShaders( s_VertexDesc, s_vPassthough, s_Shaders[Case] );
        s_Program.LinkRegisterToUniformVariable( SHADER_PS_RESOLUTION,  "iResolution" );
        s_Program.LinkRegisterToUniformVariable( SHADER_PS_GLOBALTIME,  "iGlobalTime" );
        
        s_Program.LinkRegisterToUniformVariable( SHADER_VS_L2C,         "L2C" );
    }
    else if( (eng_GetCurrentContext().GetFPS() < 8 ) && (WaitDealy <= 0))
    {
        if( DownYRes > DownXRes ) DownXRes = x_Min( 2, DownXRes + 1 );
        else                      DownYRes = x_Min( 3, DownYRes + 1 );
        WaitDealy = 1;
    }
    
    //DownXRes = 0;
    //DownYRes = 1;
    //x_printfxy( 40, 0, "Test: %d", Case );
    
    LastTime = Time;
    
    // Render the stuff
    DrawShader( StartTime, DownXRes, DownYRes );
}

//-------------------------------------------------------------------------------

void AppMain( s32 argc, char* argv[] )
{
    Init();
    
    eng_context& Context = eng_GetCurrentContext();
    
    // Main message loop
    while( Context.HandleEvents() )
    {
        // Now draw something
        Context.Begin( "Main Rendering" );
        Render();
        Context.End();
        
        Context.PageFlip(FALSE);
    }
}